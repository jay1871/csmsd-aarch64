//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by LicenseManager.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_LICENSEMANAGER_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDC_prectime                    1000
#define IDC_80MHZ                       1001
#define IDC_CHECK3                      1002
#define IDC_HFENABLED                   1002
#define IDC_VUHF                        1003
#define IDGENERATE                      1004
#define IDC_MACHINEID                   1005
#define IDC_CUSTOMER                    1006
#define IDC_HOST                        1007
#define IDC_EDIT1                       1008
#define IDC_SERIALNUM                   1008
#define IDC_HFENABLED2                  1009
#define IDC_SHFEXTENABLED               1009
#define ID_FILE_LOADLICENSE             32771

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
