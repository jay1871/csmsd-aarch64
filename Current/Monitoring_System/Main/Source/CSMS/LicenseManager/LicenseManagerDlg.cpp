
// LicenseManagerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "afxdialogex.h"
#include <cctype>
#include <string>
#include <ctime>
#include <fstream>
#include "CsmsLicense.h"
#include "LicenseManager.h"
#include "LicenseManagerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnFileLoadlicense();
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CLicenseManagerDlg dialog


#ifndef __GNUG__
const char* CCsmsLicense::MEMERR = "memory allocation error %d";
#endif

CLicenseManagerDlg::CLicenseManagerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLicenseManagerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLicenseManagerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CLicenseManagerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CLicenseManagerDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDGENERATE, &CLicenseManagerDlg::OnBnClickedGenerate)
	ON_COMMAND(ID_FILE_LOADLICENSE, &CLicenseManagerDlg::OnFileLoadlicense)
END_MESSAGE_MAP()


// CLicenseManagerDlg message handlers

BOOL CLicenseManagerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	m_filemenu.LoadMenuW(IDR_MENU1);
	SetMenu(&m_filemenu);
	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLicenseManagerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLicenseManagerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLicenseManagerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CLicenseManagerDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnOK();
}

bool CLicenseManagerDlg::ValidateInputs()
{
	bool success = true;
	CString vuhfmhz;
	GetDlgItemText(IDC_VUHF, vuhfmhz);
	for (int i = 0; i < vuhfmhz.GetLength(); i++)
	{
		if (!std::isdigit(vuhfmhz[i]))
		{
			AfxMessageBox(L"VUHF field contains non numeric characters", MB_ICONERROR);
			success = false;
			break;
		}

	}
	CString machineId;
	GetDlgItemText(IDC_MACHINEID, machineId);
	if (machineId.IsEmpty())
	{
		AfxMessageBox(L"Must have a valid machine ID of system to generate license!", MB_ICONERROR);
		success = false;
	}

	CString serialnum;
	GetDlgItemText(IDC_SERIALNUM, serialnum);
	if (serialnum.IsEmpty())
	{
		AfxMessageBox(L"Must have a valid serial number of system to generate license!", MB_ICONERROR);
		success = false;
	}
	return success;
}

void CLicenseManagerDlg::GenerateSecuritykeyandLicenseFile()
{
	std::ofstream wfile;
	std::string secData;
	std::string stringbuf;
	CString OutputFolder = L"c:\\ProgramData\\TCI";
	CString serialnum;
	GetDlgItemText(IDC_SERIALNUM, serialnum);
	if (CreateDirectory(OutputFolder, NULL) ||
		ERROR_ALREADY_EXISTS == GetLastError())
	{
	}
	else
	{

	}
	wfile.open(filename, std::ios_base::in | std::ios_base::out);
	if (!wfile)
	{
		//filename = L"C:\\ProgramData\\TCI\\CsmsLicense.lic";
		filename = L"C:\\ProgramData\\TCI\\" + serialnum + L".lic";
		wfile.open(filename, std::ios_base::out);
	}
	if (wfile.is_open())
	{
		CString customername;
		GetDlgItemText(IDC_CUSTOMER, customername);
		customername = _T("; CsmsServer License File - ") + customername;
		secData += (CT2CA)customername;
		customername += "\n";
		wfile.write((CT2CA)customername, customername.GetLength());

		CString hostname;
		GetDlgItemText(IDC_HOST, hostname);
		hostname = _T("; Hostname: ") + hostname;
		secData += (CT2CA)hostname;
		hostname += "\n\n"; // end of line erased when loading so do not put it as part of encryption
		wfile.write((CT2CA)hostname, hostname.GetLength());

		stringbuf = "[CsmsServer]";
		secData += stringbuf;
		stringbuf += "\n";
		wfile.write(stringbuf.c_str(), stringbuf.length());
		stringbuf = VERSION;
		secData += stringbuf;
		stringbuf += "\n";
		wfile.write(stringbuf.c_str(), stringbuf.length());
		std::string hasprecisiontime;
		if (IsDlgButtonChecked(IDC_prectime))
			hasprecisiontime = PRECISION_TIMESTAMP + "=1";
		else
			hasprecisiontime = PRECISION_TIMESTAMP + "=0";
		secData += hasprecisiontime;
		hasprecisiontime += "\n";
		wfile.write(hasprecisiontime.c_str(), hasprecisiontime.length());
		std::string has80Mhz;
		if (IsDlgButtonChecked(IDC_80MHZ))
			has80Mhz = EIGHTY_MHZ + "=1";
		else
			has80Mhz = EIGHTY_MHZ + "=0";
		secData += has80Mhz;
		has80Mhz += "\n";
		wfile.write(has80Mhz.c_str(), has80Mhz.length());
		std::string hasHF;
		if (IsDlgButtonChecked(IDC_HFENABLED))
			hasHF = HF_ENABLED + "=1";
		else
			hasHF = HF_ENABLED + "=0";
		secData += hasHF;
		hasHF += "\n";
		wfile.write(hasHF.c_str(), hasHF.length());

		std::string hasSHFExt;
		if (IsDlgButtonChecked(IDC_SHFEXTENABLED))
			hasSHFExt = SHFEXT_ENABLED + "=1";
		else
			hasSHFExt = SHFEXT_ENABLED + "=0";
		secData += hasSHFExt;
		hasSHFExt += "\n";
		wfile.write(hasSHFExt.c_str(), hasSHFExt.length());

		CString hasvuhf;
		GetDlgItemText(IDC_VUHF, hasvuhf);
		hasvuhf.Format(_T("%d"), std::stoi((LPCTSTR)hasvuhf)); // remove leading zeros just in case
		hasvuhf = (CA2CT)VUHF_MHZ.c_str() + _T("=") + hasvuhf;
		secData += (CT2CA)hasvuhf;
		hasvuhf += +_T("\n\n");
		wfile.write((CT2CA)hasvuhf, hasvuhf.GetLength());
		stringbuf = "[Security]";
		secData += stringbuf;
		stringbuf += "\n";
		wfile.write(stringbuf.c_str(), 11);
		CString machineID;
		GetDlgItemText(IDC_MACHINEID, machineID);
		machineID = (CA2CT)MACHINEID.c_str() + _T("=") + machineID;
		secData += (CT2CA)machineID;
		machineID += +_T("\n");
		wfile.write((CT2CA)machineID, machineID.GetLength());
		GetDlgItemText(IDC_SERIALNUM, serialnum);
		serialnum = (CA2CT)SERIALNUM.c_str() + _T("=") + serialnum;
		secData += (CT2CA)serialnum;
		serialnum += +_T("\n");
		wfile.write((CT2CA)serialnum, serialnum.GetLength());
		secData += CCsmsLicense::MEMERR;
		CCsmsLicense::HashToString(secData, stringbuf);
		// write the security line
		stringbuf = "SecurityCode=" + stringbuf + "\n";
		wfile.write(stringbuf.c_str(), stringbuf.length());
		auto t = std::time(nullptr);
		struct tm ttm;
		char tstr[20] = { "0000/00/00 00:00:00" };
		if (gmtime_s(&ttm, &t) == 0)
		{
			std::strftime(tstr, sizeof(tstr), "%Y/%m/%d %H:%M:%S", &ttm);
		}

		stringbuf = "SignDate=" + std::string(tstr) + "\n";
		wfile.write(stringbuf.c_str(), stringbuf.length());
		wfile.close();
		AfxMessageBox(L"new license file generated!");
	}
}

void CLicenseManagerDlg::OnBnClickedGenerate()
{
	if (ValidateInputs())
	{
		GenerateSecuritykeyandLicenseFile();
	}
}


void CLicenseManagerDlg::OnFileLoadlicense()
{
	std::string errmsg;
	CFileDialog fOpenDlg(TRUE, _T("lic"), NULL, OFN_FILEMUSTEXIST,
		_T("GeoServer Config Data (*.lic)|*.lic;(*.txt)|*.txt;(*.*)|*.*||"), this);

	fOpenDlg.m_pOFN->lpstrTitle = _T("csmsd license file");

	fOpenDlg.m_pOFN->lpstrInitialDir = _T("C:\\ProgramData\\TCI");
	if (fOpenDlg.DoModal() == IDOK)
	{
		filename = fOpenDlg.GetPathName();
		if (!PathFileExists(filename))
		{
			//AfxMessageBox((LPCTSTR)fOpenDlg.GetPathName());
			//::MessageBox(NULL, (LPCTSTR)fOpenDlg.GetPathName(), _T("File path name"), MB_OK);
			::MessageBox(NULL, _T("File Not Found or Invalid File"),
				_T("File Not Found"), MB_OK);
			OnOK();
		}

		// load existing license file
		std::ifstream file(filename);
		if (!file)
		{
			char errbuf[256];
			if (strerror_s(errbuf, errno) == 0)
			{
				errmsg = "Failed to open file ";
				errmsg.append(errbuf);
				CString tempstr;
				tempstr.Format(_T("%S"), errmsg.c_str());
				AfxMessageBox(tempstr, MB_ICONERROR);
			}
			else
			{
				errmsg = "Failed to open file ";
				CString tempstr;
				tempstr.Format(_T("%S"), errmsg.c_str());
				AfxMessageBox(tempstr, MB_ICONERROR);
			}

		}
		else
		{
			std::string line;

			// Read contents of license file up to security code
			std::string secCode;
			std::string secData;
			std::streamoff secCodeOffset = 0;
			std::streamoff offset = 0;
			while (!std::getline(file, line).eof())
			{
				line.erase(line.find_last_not_of(" \t\n\r") + 1);
				if (line.find(PRECISION_TIMESTAMP.c_str(), 0, 21) == 0)
				{
					int hasprecisiontime = stoi(line.substr(line.find("=", 0, 1) + 1));
					CheckDlgButton(IDC_prectime, hasprecisiontime > 0 ? BST_CHECKED : BST_UNCHECKED);
				}
				else if (line.find(EIGHTY_MHZ.c_str(), 0, 13) == 0)
				{
					int has80MhzRadio = stoi(line.substr(line.find("=", 0, 1) + 1));
					CheckDlgButton(IDC_80MHZ, has80MhzRadio > 0 ? BST_CHECKED : BST_UNCHECKED);
				}
				else if (line.find(HF_ENABLED.c_str(), 0, 5) == 0)
				{
					int hashf = stoi(line.substr(line.find("=", 0, 1) + 1));
					CheckDlgButton(IDC_HFENABLED, hashf > 0 ? BST_CHECKED : BST_UNCHECKED);
				}
				else if (line.find(VUHF_MHZ.c_str(), 0, 7) == 0)
				{
					std::string vuhfmhz = line.substr(line.find("=", 0, 1) + 1);
					CString tempstr;
					tempstr.Format(_T("%S"), vuhfmhz.c_str());
					SetDlgItemText(IDC_VUHF, tempstr);
				}
				else if (line.find(MACHINEID.c_str(), 0, 9) == 0)
				{
					std::string machineId = line.substr(line.find("=", 0, 1) + 1);
					CString tempstr;
					tempstr.Format(_T("%S"), machineId.c_str());
					SetDlgItemText(IDC_MACHINEID, tempstr);
				}
				else if (line.find(SERIALNUM.c_str(), 0, SERIALNUM.length()) == 0)
				{
					std::string serialnum = line.substr(line.find("=", 0, 1) + 1);
					CString tempstr;
					tempstr.Format(_T("%S"), serialnum.c_str());
					SetDlgItemText(IDC_SERIALNUM, tempstr);
				}
				else if (line.find(CUSTOMER.c_str(), 0, CUSTOMER.length()) == 0)
				{
					std::string customer = line.substr(line.find("-", 0, 1) + 1);
					CString tempstr;
					tempstr.Format(_T("%S"), customer.c_str());
					SetDlgItemText(IDC_CUSTOMER, tempstr);
				}
				else if (line.find(HOST.c_str(), 0, HOST.length()) == 0)
				{
					std::string host = line.substr(line.find(":", 0, 1) + 1);
					CString tempstr;
					tempstr.Format(_T("%S"), host.c_str());
					SetDlgItemText(IDC_HOST, tempstr);
				}

			}
		}
		file.close();
	}

}
