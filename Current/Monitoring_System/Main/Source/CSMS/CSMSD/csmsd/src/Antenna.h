/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "EquipCtrlMsg.h"
#include "Ne10Vec.h"
#include "Units.h"

// This class may be moved to its own file.
#include "Log.h"
#include "Utility.h"
class CIniFile
{
public:
	CIniFile(const std::string& filename, const std::string& section)
	{
		ParseIniFile(filename, section, m_keyvalMap);
//		for (auto& it : m_keyvalMap)
//		{
//			TRACE("key %s val %s\n", it.first.c_str(), it.second.c_str());
//		}
	}
	~CIniFile()
	{
		m_keyvalMap.clear();
	}

	static void strToVal(const std::string& s, unsigned long& val) { val = std::stoul(s); }
	static void strToVal(const std::string& s, float& val) { val = std::stof(s); }
	static void strToVal(const std::string& s, std::string& val) { val = s; }
	static void strToVal(const std::string& s, Units::Frequency& val) { val = Units::Frequency(std::stoull(s)); }

	template <typename T> T GetEntry(const std::string& section, const std::string& entry, T def, bool notify = true, int index1 = -1, int index2 = -1)
	{
		T value;

		std::string key = BuildKey(entry, index1, index2);
		if (m_keyvalMap.find(key) == m_keyvalMap.end())
		{
			value = def;
			if (notify)
			{
				CLog::Log(CLog::WARNINGS, "Config data missing: key %s not found in section %s\n", key.c_str(), section.c_str());
			}
		}
		else
		{
			strToVal(m_keyvalMap[key], value);
		}
		return value;
	}

private:
	static std::string BuildKey(const std::string& entry, int index1, int index2)
	{
		std::string key;
		char keystr[64];
		if (index1 >= 0)
		{
			if (index2 >= 0)
			{
				snprintf(keystr, 64, "%s%u", entry.c_str(), static_cast<unsigned int>(100 * index1 + index2));
			}
			else
			{
				snprintf(keystr, 64, "%s%u", entry.c_str(), static_cast<unsigned int>(index1));
			}
			key = keystr;
		}
		else
		{
			key = entry;
		}
		std::transform(key.begin(), key.end(), key.begin(), [](char c) { return std::tolower(c); });
		return key;
	}

	//////////////////////////////////////////////////////////////////////
	//
	// Parse an ini file section
	//
	static void ParseIniFile(const std::string& filename, const std::string& section, std::map<std::string, std::string>& keyvalMap)
	{
		printf("ParseIniFile from %s %s\n", filename.c_str(), section.c_str());

		// Read in data from file and section and store them in keyvalMap

		std::ifstream file(filename.c_str());
		if (!file)
		{
			TRACE("File %s notfound!\n", filename.c_str());
			return;
		}
		std::string tempSec = section;
		std::transform(tempSec.begin(), tempSec.end(), tempSec.begin(), [](char c) { return std::tolower(c); });
		std::string secStr = "[" + tempSec + "]";

		std::string line;
		bool inSection = false;

		while(!std::getline(file, line).eof())
		{
			// Read until section is found - discard comments (start with ;)
			if (line.length() == 0 || line[0] == ';')
				continue;

			if (line[0] == '[')	// looks like a section
			{
				std::transform(line.begin(), line.end(), line.begin(), [](char c) { return std::tolower(c); });
				inSection = (line.find(secStr) == 0);
			}
			else if (inSection)
			{
				// Split on "=" and trim to get key and value
				auto equals = line.find('=');
				if (equals == std::string::npos)
				{
					continue;
				}
				std::string key = line.substr(0, equals);
				line.erase(0, equals + 1);
				std::string val = line.substr(0, line.find(';'));
				Utility::Trim(key);
				Utility::Trim(val);
				// Store lower case keys in the map
				std::transform(key.begin(), key.end(), key.begin(), [](char c) { return std::tolower(c); });
				keyvalMap[key] = val;
			}
		}
		return;
	}
	std::map<std::string, std::string> m_keyvalMap;

};

class CAntenna
{
public:
	// Types
	enum EDfAlgorithm { NONE, INTERFEROMETRY, WATSON_WATT, WATSON_WATT620, WFA, SYMMETRY, SYMMETRY_HORIZ, SYMMETRY_SHF, WFA649LOOPS_NOTUSED };

	enum class EAntennaType : uint32_t
	{
		NO_ANTENNA = 0x00,
		UHF_641 = 0x30, UHF_641_COMPASS = 0x31, UHF_643_3 = 0x32, UHF_643_3_COMPASS = 0x33,
		UHF_645 = 0x34, UHF_645_COMPASS = 0x35, UHF_645_8 = 0x36, UHF_645_8_COMPASS = 0x37,
		USHF_647 = 0x38, USHF_647_COMPASS = 0x39, USHF_647D = 0x3A, USHF_647D_COMPASS = 0x3B,
		USHF_649_8 = 0x3C, USHF_649_8_COMPASS = 0x3D,
		HF_632 = 0x40, HF_7235_COMPASS = 0x48, HF_7235 = 0x49, HF_620 = 0x4A,
		HF_7031 = 0x0101, VUHF_640_DSC = 0x0102, SHF_640_2 = 0x0103,
		VUHF_AUX1 = 0x0201, CUSTOM_ANT = 0x0204, CUSTOM_LPA_ANT = 0x205,
		//The following are derived monitor antenna
		USHF_647D_HOR_MON_SWTCH = 0x0301, USHF_647D_VERT_MON_SWTCH = 0x0302,
		USHF_DUAL_AUX1 = 0x0303, //Dual Aux1 in DF system has VUHF_AUX1 in Master & CUSTOM_ANT in Slave RF2
	};

	// Types
	typedef std::map<Units::Frequency, float> FreqDepParams;
	typedef std::vector<Ne10F32cVec> WfaConjPattern; // Complex conjugate of pattern voltages[az][ant]
	typedef std::map<Units::Frequency, WfaConjPattern> WfaConjPatterns; // Lookup by frequency

	struct DualPolPattern
	{
		unsigned long maxMode;
		float normSame;					// norm of pattern coefficients for same polarization as antenna
		float normOther;				// norm of pattern coefficients for opposite polarization as antenna
		ne10_fft_cpx_float32_t otherSame;				// dot product of same and conj(other) coefficients
		Ne10F32cVec coefSame;			// conjugate pattern coefficients for same polarization as antenna
		Ne10F32cVec coefOther;			// conjugate pattern coefficients for same polarization as antenna
	};

	struct SDualPolCoef
	{
		SDualPolCoef(void) : amplSame(-1.f), phaseSame(-99999.f), amplOther(-1.f), phaseOther(-99999.f) {}		// Illegal values
		float amplSame;
		float phaseSame;
		float amplOther;
		float phaseOther;
	};

	struct DualPolFourierPattern
	{
		std::vector<SDualPolCoef> coef;		// normalized coefficients for same and opposite polarization as antenna
	};
	typedef std::map<Units::Frequency, DualPolFourierPattern> DualPolFourierPatterns; // Lookup by frequency

	struct SWatsonWattParams
	{
		std::vector<unsigned long> phaseFreq;
		std::vector<short> phase;
	};

	struct SWfaParams
	{
		int coarseAzStep;
		float uniqueConf;
	};

	struct SWfaParams2
	{
		enum EWfaType { SINGLE_POL, DUAL_POL, SINGLE_POL_FOURIER, DUAL_POL_FOURIER };
		EWfaType wfaType;
		unsigned long maxMode;
		DualPolFourierPatterns dualPolPat;
	};

	struct SSymmetryDfParams // Symmetry algorithm data
	{
		unsigned char numAnts; // Number of antennas
		float arrayOffset; // Angle of element 1 (degrees)

		// Algorithm methods
		//  Frequencies must be in ascending order
		//  The designated mode applies when DF freq <= freq
		struct SMethod
		{
			enum EMethod { MODES_ONLY = 0, MODES_FINE = 1, GRID_FINE = 2, AMP_FINE = 3, MODES_INV_FINE = 4, AMP_PAIRS_FINE = 5,
				AMP_TRIPLET_FINE = 6, AMP_DOUBLE_FINE = 7, AMP_ONLY = 8, MODES23_FINE = 9, AMP_DOUBLE2_FINE = 10,
				MODESCF_FINE = 11, MODESCF_INV_FINE = 12,	// Note: these 2 methods are not currently used anywhere
				MODES_INV_ONLY = 13, AMP_DOUBLE_ONLY = 14, WFA = 15, WFA_MODES_INV = 16,
				WATSON_WATT = 17, WATSON_WATT620 = 18, CIRCULAR_INTERFEROMETRY = 19
			};

			Units::Frequency freq;
			EMethod method;
			float uniqueConf; // "Good enough" confidence used to terminate grid / peak search
			unsigned char gridWidth; // Degrees for grid search and fine peak search
			unsigned short mainLobeWidth; // Degrees. Elements outside this will not be used in fine calculation
			bool phaseAdjust;	// Adjust phase by 180 deg in backlobe for horiz polarization
		};

		std::vector<SMethod> methods;

		// Antenna radius table
		FreqDepParams radiusMap;

		// Antenna coarse offset table
		FreqDepParams offsetMap;

		// Antenna mode0 correction factor table
		FreqDepParams modesfactorMap;

		// Post-calculation correction factor table
		FreqDepParams postcorrMap;

		// Special azimuth correction for modes
		FreqDepParams modesazimMap;

		float pairFactor;
		// Data for wfa algorithm
		SWfaParams2 wfaParams2;
	};

	// Functions
	virtual ~CAntenna(void) {}

	static EAntennaType StrToEnum(const std::string& str)
	{
#define ANT_STR_TO_ENUM(X) if (str == #X) return EAntennaType::X
		ANT_STR_TO_ENUM(UHF_641);
		ANT_STR_TO_ENUM(UHF_641_COMPASS);
		ANT_STR_TO_ENUM(UHF_643_3);
		ANT_STR_TO_ENUM(UHF_643_3_COMPASS);
		ANT_STR_TO_ENUM(UHF_645);
		ANT_STR_TO_ENUM(UHF_645_COMPASS);
		ANT_STR_TO_ENUM(UHF_645_8);
		ANT_STR_TO_ENUM(UHF_645_8_COMPASS);
		ANT_STR_TO_ENUM(USHF_647);
		ANT_STR_TO_ENUM(USHF_647_COMPASS);
		ANT_STR_TO_ENUM(USHF_647D);
		ANT_STR_TO_ENUM(USHF_647D_COMPASS);
		ANT_STR_TO_ENUM(USHF_649_8);
		ANT_STR_TO_ENUM(USHF_649_8_COMPASS);
		ANT_STR_TO_ENUM(HF_632);
		ANT_STR_TO_ENUM(HF_7235_COMPASS);
		ANT_STR_TO_ENUM(HF_7235);
		ANT_STR_TO_ENUM(HF_620);
		ANT_STR_TO_ENUM(HF_7031);
		ANT_STR_TO_ENUM(VUHF_640_DSC);
		ANT_STR_TO_ENUM(SHF_640_2);
		ANT_STR_TO_ENUM(VUHF_AUX1);
		ANT_STR_TO_ENUM(CUSTOM_ANT);
		ANT_STR_TO_ENUM(USHF_DUAL_AUX1);
		return EAntennaType::NO_ANTENNA;
#undef ANT_STR_TO_ENUM
	}
	static std::string EnumToStr(const EAntennaType& e)
	{
#define ANT_ENUM_TO_STR(X) if (e == EAntennaType::X) return #X
		ANT_ENUM_TO_STR(UHF_641);
		ANT_ENUM_TO_STR(UHF_641_COMPASS);
		ANT_ENUM_TO_STR(UHF_643_3);
		ANT_ENUM_TO_STR(UHF_643_3_COMPASS);
		ANT_ENUM_TO_STR(UHF_645);
		ANT_ENUM_TO_STR(UHF_645_COMPASS);
		ANT_ENUM_TO_STR(UHF_645_8);
		ANT_ENUM_TO_STR(UHF_645_8_COMPASS);
		ANT_ENUM_TO_STR(USHF_647);
		ANT_ENUM_TO_STR(USHF_647_COMPASS);
		ANT_ENUM_TO_STR(USHF_647D);
		ANT_ENUM_TO_STR(USHF_647D_COMPASS);
		ANT_ENUM_TO_STR(USHF_649_8);
		ANT_ENUM_TO_STR(USHF_649_8_COMPASS);
		ANT_ENUM_TO_STR(HF_632);
		ANT_ENUM_TO_STR(HF_7235_COMPASS);
		ANT_ENUM_TO_STR(HF_7235);
		ANT_ENUM_TO_STR(HF_620);
		ANT_ENUM_TO_STR(HF_7031);
		ANT_ENUM_TO_STR(VUHF_640_DSC);
		ANT_ENUM_TO_STR(SHF_640_2);
		ANT_ENUM_TO_STR(VUHF_AUX1);
		ANT_ENUM_TO_STR(CUSTOM_ANT);
		ANT_ENUM_TO_STR(USHF_DUAL_AUX1);
		return "None";
#undef ANT_ENUM_TO_STR
	}
	static CAntenna* Create(const EAntennaType& e);
	static bool CanSSL(const CAntenna* p) { return (p ? p->CanSSL() : false); }
	static EDfAlgorithm DfAlgorithm(CAntenna* p, Units::Frequency freq, bool horiz) { return (p ? p->DfAlgorithm(freq, horiz) : EDfAlgorithm::NONE); }
	static const std::string& GetAntFactorFilename(const CAntenna* p) { return (p ? p->m_antFactorFileName : m_none); }
	static Units::Frequency GetHighBandCrossoverFreq(const CAntenna* p) { return (p ? p->GetHighBandCrossoverFreq() : Units::Frequency(0)); }
	static FreqDepParams& GetHAntennaFactors(CAntenna* p) { return (p ? p->GetHAntennaFactors() : m_emptyFactors); }
	static FreqDepParams& GetVAntennaFactors(CAntenna* p) { return (p ? p->GetVAntennaFactors() : m_emptyFactors); }
	static FreqDepParams& GetHSampleGainOffset(CAntenna* p) { return (p ? p->GetHSampleGainOffset() : m_emptyFactors); }
	static FreqDepParams& GetVSampleGainOffset(CAntenna* p) { return (p ? p->GetVSampleGainOffset() : m_emptyFactors); }
	static SSmsMsg::EAnt GetMAnt(const CAntenna* p, bool horiz, Units::Frequency freq, bool forceHigh =false)
	{ return (p ? p->GetMAnt(horiz, freq, forceHigh) : SSmsMsg::INVALID_ANT); }
	static const std::string& GetBaseName(const CAntenna* p) { return (p ? p->m_baseName : m_none); }
	static const std::string& GetFullName(const CAntenna* p) { return (p ? p->m_name : m_none); }
	static bool HasHorizontalPolarization(const CAntenna* p) { return (p ? p->HasHorizontalPolarization() : false); }
	static bool HasHorizontalPolarization(const CAntenna* p, Units::Frequency freq) { return (p ? p->HasHorizontalPolarization(freq) : false); }
	static bool HasLowbandSkippedElement(const CAntenna* p) { return (p ? p->HasLowbandSkippedElement() : false); }
	static bool HasVerticalPolarization(const CAntenna* p, Units::Frequency freq) { return (p ? p->HasVerticalPolarization(freq) : false); }
	static bool IsAntFactorLoaded(const CAntenna* p)  { return (p ? p->m_antFactorLoaded : false); }
	static bool IsDfAntenna(const CAntenna* p)  { return (p ? p->IsDfAntenna() : false); }
	static bool IsMobileAntenna(const CAntenna* p) { return (p ? p->IsMobileAntenna() : false); }
	static bool IsValidDfElement(const CAntenna* p, SSmsMsg::EAnt dfElem) { return (p ? p->IsValidDfElement(dfElem) : false); }
	static unsigned char NumAnts(const CAntenna* p, Units::Frequency freq) { return (p ? p->NumAnts(freq) : 1); }
	static Units::Frequency GetVLowFreq(const CAntenna* p) { return (p ? p->GetVLowFreq() : Units::Frequency(0)); }
	static Units::Frequency GetVHighFreq(const CAntenna* p) { return (p ? p->GetVHighFreq() : Units::Frequency(0)); }
	static Units::Frequency GetHLowFreq(const CAntenna* p) { return (p ? p->GetHLowFreq() : Units::Frequency(0)); }
	static Units::Frequency GetHHighFreq(const CAntenna* p) { return (p ? p->GetHHighFreq() : Units::Frequency(0)); }
	static Units::Frequency GetDfVLowFreq(const CAntenna* p) { return (p ? p->GetDfVLowFreq() : Units::Frequency(0)); }
	static Units::Frequency GetDfVHighFreq(const CAntenna* p) { return (p ? p->GetDfVHighFreq() : Units::Frequency(0)); }
	static Units::Frequency GetDfHLowFreq(const CAntenna* p) { return (p ? p->GetDfHLowFreq() : Units::Frequency(0)); }
	static Units::Frequency GetDfHHighFreq(const CAntenna* p) { return (p ? p->GetDfHHighFreq() : Units::Frequency(0)); }
	static SSymmetryDfParams* GetSymmetryDfParams(CAntenna* p, bool horiz, bool shf)
		{ return (p ? p->GetSymmetryDfParams(horiz, shf) : nullptr); }
	static const SWfaParams* GetWfaParams(CAntenna* p) { return (p ? p->GetWfaParams() : nullptr); }
	static const WfaConjPatterns& GetWfaConjPatterns(CAntenna* p) { return (p ? p->GetWfaConjPatterns() : m_emptyPatterns); }
	static const WfaConjPattern* GetWfaConjPattern(CAntenna* p, Units::Frequency freq)
		{ return (p ? p->GetWfaConjPattern(freq) : nullptr); }
	static bool GetDualPolFourierPattern(const SWfaParams2& wfaParams2, size_t numAnts, Units::Frequency freq, bool interpolate,
		DualPolPattern& outPat);
	static const SWatsonWattParams* GetWatsonWattParams(CAntenna* p) { return (p ? p->GetWatsonWattParams() : nullptr); }
	static void LoadDfParams(CAntenna* p, const std::string& filename) { if (p && p->IsDfAntenna()) p->LoadDfParams(filename); }
	static void SaveAntFactorFilename(CAntenna* p, const std::string& filename) {  if (p) p->m_antFactorFileName = filename; }
	static void SetAntFactorLoadedFlag(CAntenna* p, bool loaded) {  if (p) p->m_antFactorLoaded = loaded; }
	static void SetVHighFreq(CAntenna* p, Units::Frequency freq) { if (p) p->SetVHighFreq(freq); }
	static void SetVLowFreq(CAntenna* p, Units::Frequency freq) { if (p) p->SetVLowFreq(freq); }

protected:
	// Functions
	CAntenna(void) :
		m_antFactorLoaded(false),
		m_canSSL(false),
		m_VLowFreq(0),
		m_VHighFreq(0),
		m_HLowFreq(0),
		m_HHighFreq(0),
		m_dfVLowFreq(0),
		m_dfVHighFreq(0),
		m_dfHLowFreq(0),
		m_dfHHighFreq(0),
		m_highBandCrossoverFreq(0),
		m_hDfAlg(EDfAlgorithm::NONE),
		m_vDfAlgLo(EDfAlgorithm::NONE),
		m_vDfAlgHi(EDfAlgorithm::NONE),
		m_hMAnt(SSmsMsg::EAnt::INVALID_ANT),
		m_vMAntLo(SSmsMsg::EAnt::INVALID_ANT),
		m_vMAntHi(SSmsMsg::EAnt::INVALID_ANT),
		m_hasHorizontalPolarization(false),
		m_hasVerticalPolarization(false),
		m_isDfAntenna(false),
		m_isMobileAntenna(false),
		m_numAntsLo(1),
		m_numAntsHi(1),
		m_hSymmetryDfParams(),
		m_vSymmetryDfParams(),
		m_sSymmetryDfParams(),
		m_wfaParams(),
		m_watsonWatt(),
		m_wfaConjPatterns()
	{
		m_antFactorFileName = "Unknown";
	}

	void LoadSymmetryDfParams(SSymmetryDfParams& p, const std::string& filename, const std::string& section);
	void LoadWfaDfParams2(const std::string& filename, SWfaParams2& wfaParams2);
	void LoadWatsonWattDfParams(SWatsonWattParams& p, const std::string& filename, const std::string& section);
	void LoadWfaDfParams(SWfaParams& p, const std::string& filename, const std::string& section);

	// Data
	bool m_antFactorLoaded;
	std::string m_antFactorFileName;
	bool m_canSSL;
	Units::Frequency m_VLowFreq;
	Units::Frequency m_VHighFreq;
	Units::Frequency m_HLowFreq;
	Units::Frequency m_HHighFreq;
	Units::Frequency m_dfVLowFreq;
	Units::Frequency m_dfVHighFreq;
	Units::Frequency m_dfHLowFreq;
	Units::Frequency m_dfHHighFreq;
	Units::Frequency m_highBandCrossoverFreq;
	EDfAlgorithm m_hDfAlg;
	EDfAlgorithm m_vDfAlgLo;
	EDfAlgorithm m_vDfAlgHi;
	SSmsMsg::EAnt m_hMAnt;
	SSmsMsg::EAnt m_vMAntLo;
	SSmsMsg::EAnt m_vMAntHi;
	bool m_hasHorizontalPolarization;
	bool m_hasVerticalPolarization;
	bool m_isDfAntenna;
	bool m_isMobileAntenna;
	unsigned char m_numAntsLo;
	unsigned char m_numAntsHi;
	std::string m_name;
	std::string m_baseName;

	SSymmetryDfParams m_hSymmetryDfParams;
	SSymmetryDfParams m_vSymmetryDfParams;
	SSymmetryDfParams m_sSymmetryDfParams;
	SWfaParams m_wfaParams;
	SWatsonWattParams m_watsonWatt;

private:
	// Constants

	// Types

	// Functions
	bool CanSSL(void) const
	{
		return m_canSSL;
	}
	EDfAlgorithm DfAlgorithm(Units::Frequency freq, bool horiz)
	{
		return horiz ? m_hDfAlg : (freq < m_highBandCrossoverFreq ? m_vDfAlgLo : m_vDfAlgHi);
	}
	FreqDepParams& GetHAntennaFactors(void)
	{
		return m_hAntFactors;
	}
	FreqDepParams& GetVAntennaFactors(void)
	{
		return m_vAntFactors;
	}
	FreqDepParams& GetHSampleGainOffset(void)
	{
		return m_hSampleGainOffset;
	}
	FreqDepParams& GetVSampleGainOffset(void)
	{
		return m_vSampleGainOffset;
	}
	SSymmetryDfParams* GetSymmetryDfParams(bool horiz, bool shf)
	{
		if (horiz) return &m_hSymmetryDfParams;
		if (shf) return &m_sSymmetryDfParams;
		return &m_vSymmetryDfParams;
	}
	const SWfaParams* GetWfaParams(void)
	{
		return &m_wfaParams;
	}
	const WfaConjPatterns& GetWfaConjPatterns(void)
	{
		return m_wfaConjPatterns;
	}
	const SWatsonWattParams* GetWatsonWattParams(void)
	{
		return &m_watsonWatt;
	}

	const WfaConjPattern* GetWfaConjPattern(Units::Frequency freq);
	Units::Frequency GetVLowFreq(void) const { return m_VLowFreq; }
	Units::Frequency GetVHighFreq(void) const { return m_VHighFreq; }
	Units::Frequency GetHLowFreq(void) const { return m_HLowFreq; }
	Units::Frequency GetHHighFreq(void) const { return m_HHighFreq; }
	Units::Frequency GetDfVLowFreq(void) const { return m_dfVLowFreq; }
	Units::Frequency GetDfVHighFreq(void) const { return m_dfVHighFreq; }
	Units::Frequency GetDfHLowFreq(void) const { return m_dfHLowFreq; }
	Units::Frequency GetDfHHighFreq(void) const { return m_dfHHighFreq; }
	Units::Frequency GetHighBandCrossoverFreq(void) const { return m_highBandCrossoverFreq; }
	SSmsMsg::EAnt GetMAnt(bool horiz, Units::Frequency freq, bool forceHigh) const
	{
		if (horiz)
		{
			if (freq < m_dfHLowFreq || freq > m_dfHHighFreq) return SSmsMsg::EAnt::INVALID_ANT;
			return m_hMAnt;
		}
		else
		{
			if (freq < m_dfVLowFreq || freq > m_dfVHighFreq) return SSmsMsg::EAnt::INVALID_ANT;
			return ((forceHigh || freq >= m_highBandCrossoverFreq) ? m_vMAntHi : m_vMAntLo);
		}
	}
//	const std::string& GetFullName(void) {return m_name; }
	bool HasHorizontalPolarization(void) const {return m_hasHorizontalPolarization; }
	bool HasHorizontalPolarization(Units::Frequency freq) const {return m_hasHorizontalPolarization && freq < m_dfHHighFreq; }
	bool HasVerticalPolarization(Units::Frequency /*freq*/) const { return m_hasVerticalPolarization; }
	bool IsDfAntenna(void) const { return m_isDfAntenna; }

	//HasLowbandSkippedElement: true Low band has 5 elements mapping to 9 elements switch.
	//							false low band has the same number of antenna elements as high band.
	bool HasLowbandSkippedElement(void) const { return(m_numAntsLo != m_numAntsHi); }
	bool IsMobileAntenna(void) const { return m_isMobileAntenna; }
	bool IsValidDfElement(SSmsMsg::EAnt dfElem) const { return m_isDfAntenna &&
		((m_hasVerticalPolarization && dfElem >= SSmsMsg::DF_ANT_1V && dfElem <= std::max(m_vMAntLo, m_vMAntHi)) ||
		 (m_hasHorizontalPolarization && dfElem >= SSmsMsg::DF_ANT_1H && dfElem <= m_hMAnt)); }
	unsigned char NumAnts(Units::Frequency freq) const { return freq < m_highBandCrossoverFreq ? m_numAntsLo : m_numAntsHi; }
	virtual void LoadDfParams(const std::string&)
	{
		throw std::logic_error("no implementation defined for LoadDfParams");
	}
	void SetVHighFreq(Units::Frequency freq) { m_VHighFreq = freq; }
	void SetVLowFreq(Units::Frequency freq) { m_VLowFreq = freq; }

	// Data
	static const std::string m_none;
	static FreqDepParams m_emptyFactors;
	static WfaConjPatterns m_emptyPatterns;

	FreqDepParams m_hAntFactors;
	FreqDepParams m_vAntFactors;
	FreqDepParams m_hSampleGainOffset;
	FreqDepParams m_vSampleGainOffset;
	WfaConjPatterns m_wfaConjPatterns;
};

__attribute__((weak)) CAntenna::FreqDepParams CAntenna::m_emptyFactors;
__attribute__((weak)) CAntenna::WfaConjPatterns CAntenna::m_emptyPatterns;
const __attribute__((weak)) std::string CAntenna::m_none = "None";

class C620_ant : public CAntenna
{
public:
	C620_ant(void) : CAntenna()
	{
		// Define base class members
		m_baseName = "HF_620";
		m_name = "HF_620";
		m_VLowFreq = Units::Frequency(285000UL);
		m_VHighFreq = Units::Frequency(30000000UL);
		m_HLowFreq = Units::Frequency(0);
		m_HHighFreq = Units::Frequency(0);
		m_dfVLowFreq = Units::Frequency(285000UL);
		m_dfVHighFreq = Units::Frequency(30000000UL);
		m_dfHLowFreq = Units::Frequency(0);
		m_dfHHighFreq = Units::Frequency(0);
		m_highBandCrossoverFreq = Units::Frequency(285000UL);
		m_vDfAlgLo = EDfAlgorithm::WATSON_WATT620;
		m_vDfAlgHi = EDfAlgorithm::WATSON_WATT620;
		m_vMAntLo = SEquipCtrlMsg::DF_ANT_3V;
		m_vMAntHi = SEquipCtrlMsg::DF_ANT_3V;
		m_hasHorizontalPolarization = false;
		m_hasVerticalPolarization = true;
		m_isDfAntenna = true;
		m_isMobileAntenna = false;
		m_numAntsLo = 3;
		m_numAntsHi = 3;
	}
	~C620_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadWatsonWattDfParams(m_watsonWatt, filename, "Watsonwatt620");
	}
};

class C632_ant : public CAntenna
{
public:
	C632_ant(void) : CAntenna()
	{
		// Define base class members
		m_baseName = "HF_632";
		m_name = "HF_632";
		m_canSSL = true;
		m_VLowFreq = Units::Frequency(285000UL);
		m_VHighFreq = Units::Frequency(30000000UL);
		m_HLowFreq = Units::Frequency(0);
		m_HHighFreq = Units::Frequency(0);
		m_dfVLowFreq = Units::Frequency(285000UL);
		m_dfVHighFreq = Units::Frequency(30000000UL);
		m_dfHLowFreq = Units::Frequency(0);
		m_dfHHighFreq = Units::Frequency(0);
		m_highBandCrossoverFreq = Units::Frequency(285000UL);
		m_vDfAlgLo = EDfAlgorithm::INTERFEROMETRY;
		m_vDfAlgHi = EDfAlgorithm::INTERFEROMETRY;
		m_vMAntLo = SEquipCtrlMsg::DF_ANT_9V;
		m_vMAntHi = SEquipCtrlMsg::DF_ANT_9V;
		m_hasHorizontalPolarization = false;
		m_hasVerticalPolarization = true;
		m_isDfAntenna = true;
		m_isMobileAntenna = false;
		m_numAntsLo = 9;
		m_numAntsHi = 9;
	}
	~C632_ant(void) {}

	virtual void LoadDfParams(const std::string&)
	{
		// This antenna uses the antCoords in the hfConfig.
	}
};

class C640_2_ant : public CAntenna
{
public:
	C640_2_ant(void) : CAntenna()
	{
		// Define base class members
		m_baseName = "SHF_640_2";
		m_name = "SHF_640_2";
		m_VLowFreq = Units::Frequency(3000000000UL);
		m_VHighFreq = Units::Frequency(8500000000ULL);
		m_hasHorizontalPolarization = false;
		m_hasVerticalPolarization = true;
	}
	~C640_2_ant(void) {};
};

class C640_DSC_ant : public CAntenna
{
public:
	C640_DSC_ant(void) : CAntenna()
	{
		// Define base class members
		m_baseName = "VUHF_640_DSC";
		m_name = "VUHF_640_DSC";
		m_VLowFreq = Units::Frequency(20000000UL);
		m_VHighFreq = Units::Frequency(3000000000UL);
		m_hasHorizontalPolarization = false;
		m_hasVerticalPolarization = true;
	}
	~C640_DSC_ant(void) {};
};

class C641_ant : public CAntenna
{
public:
	C641_ant(void) : CAntenna()
	{
		// Define base class members
		m_baseName = "UHF_641";
		m_name = "UHF_641";
		m_VLowFreq = Units::Frequency(20000000UL);
		m_VHighFreq = Units::Frequency(3000000000UL);
		m_dfVLowFreq = Units::Frequency(20000000UL);
		m_dfVHighFreq = Units::Frequency(3000000000UL);
		m_highBandCrossoverFreq = Units::Frequency(3000000000UL);
		m_vDfAlgLo = EDfAlgorithm::SYMMETRY;
		m_vDfAlgHi = EDfAlgorithm::SYMMETRY;
		m_vMAntLo = SEquipCtrlMsg::DF_ANT_9V;
		m_vMAntHi = SEquipCtrlMsg::DF_ANT_9V;
		m_hasHorizontalPolarization = false;
		m_hasVerticalPolarization = true;
		m_isDfAntenna = true;
		m_isMobileAntenna = false;
		m_numAntsLo = 9;
		m_numAntsHi = 9;
	}
	~C641_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadSymmetryDfParams(m_vSymmetryDfParams, filename, "symmetrydf641");
	}
};

class C641_compass_ant : public C641_ant
{
public:
	C641_compass_ant(void) : C641_ant()
	{
		// Override base class members
		m_name = "UHF_641_COMPASS";
		m_isMobileAntenna = true;
	}
	~C641_compass_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadSymmetryDfParams(m_vSymmetryDfParams, filename, "symmetrydf641");
	}
};

class C643_3_ant : public CAntenna
{
public:
	C643_3_ant(void) : CAntenna()
	{
		// Define base class members
		m_baseName = "UHF_643_3";
		m_name = "UHF_643_3";
		m_VLowFreq = Units::Frequency(20000000UL);
		m_VHighFreq = Units::Frequency(3000000000UL);
		m_HLowFreq = Units::Frequency(20000000UL);
		m_HHighFreq = Units::Frequency(3000000000UL);
		m_dfVLowFreq = Units::Frequency(20000000UL);
		m_dfVHighFreq = Units::Frequency(3000000000UL);
		m_dfHLowFreq = Units::Frequency(20000000UL);
		m_dfHHighFreq = Units::Frequency(3000000000UL);
		m_highBandCrossoverFreq = Units::Frequency(3000000000UL);
		m_hDfAlg = EDfAlgorithm::SYMMETRY_HORIZ;
		m_vDfAlgLo = EDfAlgorithm::SYMMETRY;
		m_vDfAlgHi = EDfAlgorithm::SYMMETRY;
		m_hMAnt = SEquipCtrlMsg::DF_ANT_9H;
		m_vMAntLo = SEquipCtrlMsg::DF_ANT_9V;
		m_vMAntHi = SEquipCtrlMsg::DF_ANT_9V;
		m_hasHorizontalPolarization = true;
		m_hasVerticalPolarization = true;
		m_isDfAntenna = true;
		m_isMobileAntenna = false;
		m_numAntsLo = 9;
		m_numAntsHi = 9;
	}
	~C643_3_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadSymmetryDfParams(m_hSymmetryDfParams, filename, "symmetrydf643_3h");
		LoadSymmetryDfParams(m_vSymmetryDfParams, filename, "symmetrydf643_3v");
	}
};

class C643_3_compass_ant : public C643_3_ant
{
public:
	C643_3_compass_ant(void) : C643_3_ant()
	{
		// Override base class members
		m_name = "UHF_643_3_COMPASS";
		m_isMobileAntenna = true;
	}
	~C643_3_compass_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadSymmetryDfParams(m_hSymmetryDfParams, filename, "symmetrydf643_3h");
		LoadSymmetryDfParams(m_vSymmetryDfParams, filename, "symmetrydf643_3v");
	}
};

class C645_ant : public CAntenna
{
public:
	C645_ant(void) : CAntenna()
	{
		// Define base class members
		m_baseName = "UHF_645";
		m_name = "UHF_645";
		m_VLowFreq = Units::Frequency(20000000UL);
		m_VHighFreq = Units::Frequency(3000000000UL);
		m_dfVLowFreq = Units::Frequency(20000000UL);
		m_dfVHighFreq = Units::Frequency(3000000000UL);
		m_highBandCrossoverFreq = Units::Frequency(3000000000UL);
		m_vDfAlgLo = EDfAlgorithm::SYMMETRY;
		m_vDfAlgHi = EDfAlgorithm::SYMMETRY;
		m_vMAntLo = SEquipCtrlMsg::DF_ANT_9V;
		m_vMAntHi = SEquipCtrlMsg::DF_ANT_9V;
		m_hasHorizontalPolarization = false;
		m_hasVerticalPolarization = true;
		m_isDfAntenna = true;
		m_isMobileAntenna = false;
		m_numAntsLo = 9;
		m_numAntsHi = 9;
	}
	~C645_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadSymmetryDfParams(m_vSymmetryDfParams, filename, "symmetrydf645");
	}
};

class C645_compass_ant : public C645_ant
{
public:
	C645_compass_ant(void) : C645_ant()
	{
		// Override base class members
		m_name = "UHF_645_COMPASS";
		m_isMobileAntenna = true;
	}
	~C645_compass_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadSymmetryDfParams(m_vSymmetryDfParams, filename, "symmetrydf645");
	}
};

class C645_8_ant : public CAntenna
{
public:
	C645_8_ant(void) : CAntenna()
	{
		// Define base class members
		m_baseName = "UHF_645_8";
		m_name = "UHF_645_8";
		m_VLowFreq = Units::Frequency(20000000UL);
		m_VHighFreq = Units::Frequency(8500000000ULL);
		m_dfVLowFreq = Units::Frequency(20000000UL);
		m_dfVHighFreq = Units::Frequency(8500000000ULL);
		m_highBandCrossoverFreq = Units::Frequency(2500000000UL);
		m_vDfAlgLo = EDfAlgorithm::SYMMETRY;
		m_vDfAlgHi = EDfAlgorithm::SYMMETRY_SHF;
		m_vMAntLo = SEquipCtrlMsg::DF_ANT_9V;
		m_vMAntHi = SEquipCtrlMsg::DF_ANT_9V;
		m_hasHorizontalPolarization = false;
		m_hasVerticalPolarization = true;
		m_isDfAntenna = true;
		m_isMobileAntenna = false;
		m_numAntsLo = 9;
		m_numAntsHi = 9;
	}
	~C645_8_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadSymmetryDfParams(m_vSymmetryDfParams, filename, "symmetrydf645_8u");
		LoadSymmetryDfParams(m_sSymmetryDfParams, filename, "symmetrydf645_8s");
	}
};

class C645_8_compass_ant : public C645_8_ant
{
public:
	C645_8_compass_ant(void) : C645_8_ant()
	{
		// Override base class members
		m_name = "UHF_645_8_COMPASS";
		m_isMobileAntenna = true;
	}
	~C645_8_compass_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadSymmetryDfParams(m_vSymmetryDfParams, filename, "symmetrydf645_8u");
		LoadSymmetryDfParams(m_sSymmetryDfParams, filename, "symmetrydf645_8s");
	}
};

class C647_ant : public CAntenna
{
public:
	C647_ant(void) : CAntenna()
	{
		// Define base class members
		m_baseName = "USHF_647";
		m_name = "USHF_647";
		m_VLowFreq = Units::Frequency(20000000UL);
		m_VHighFreq = Units::Frequency(8500000000ULL);
		m_dfVLowFreq = Units::Frequency(20000000UL);
		m_dfVHighFreq = Units::Frequency(8500000000ULL);
		m_highBandCrossoverFreq = Units::Frequency(2500000000UL);
		m_vDfAlgLo = EDfAlgorithm::SYMMETRY;
		m_vDfAlgHi = EDfAlgorithm::SYMMETRY_SHF;
		m_vMAntLo = SEquipCtrlMsg::DF_ANT_9V;
		m_vMAntHi = SEquipCtrlMsg::DF_ANT_9V;
		m_hasHorizontalPolarization = false;
		m_hasVerticalPolarization = true;
		m_isDfAntenna = true;
		m_isMobileAntenna = false;
		m_numAntsLo = 9;
		m_numAntsHi = 9;
	}
	~C647_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadSymmetryDfParams(m_vSymmetryDfParams, filename, "symmetrydf647u");
		LoadSymmetryDfParams(m_sSymmetryDfParams, filename, "symmetrydf647s");
	}
};

class C647_compass_ant : public C647_ant
{
public:
	C647_compass_ant(void) : C647_ant()
	{
		// Override base class members
		m_name = "USHF_647_COMPASS";
		m_isMobileAntenna = true;
	}
	~C647_compass_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadSymmetryDfParams(m_vSymmetryDfParams, filename, "symmetrydf647u");
		LoadSymmetryDfParams(m_sSymmetryDfParams, filename, "symmetrydf647s");
	}
};

class C647D_ant : public CAntenna
{
public:
	C647D_ant(void) : CAntenna()
	{
		// Define base class members
		m_baseName = "USHF_647D";
		m_name = "USHF_647D";
		m_VLowFreq = Units::Frequency(20000000UL);
		m_VHighFreq = Units::Frequency(8500000000ULL);
		m_HLowFreq = Units::Frequency(20000000UL);
		m_HHighFreq = Units::Frequency(3000000000UL);
		m_dfVLowFreq = Units::Frequency(20000000UL);
		m_dfVHighFreq = Units::Frequency(8500000000ULL);
		m_dfHLowFreq = Units::Frequency(20000000UL);
		m_dfHHighFreq = Units::Frequency(3000000000UL);
		m_highBandCrossoverFreq = Units::Frequency(2500000000UL);
		m_hDfAlg = EDfAlgorithm::SYMMETRY_HORIZ;
		m_vDfAlgLo = EDfAlgorithm::SYMMETRY;
		m_vDfAlgHi = EDfAlgorithm::SYMMETRY_SHF;
		m_hMAnt = SEquipCtrlMsg::DF_ANT_9H;
		m_vMAntLo = SEquipCtrlMsg::DF_ANT_9V;
		m_vMAntHi = SEquipCtrlMsg::DF_ANT_9V;
		m_hasHorizontalPolarization = true;
		m_hasVerticalPolarization = true;
		m_isDfAntenna = true;
		m_isMobileAntenna = false;
		m_numAntsLo = 9;
		m_numAntsHi = 9;
	}
	~C647D_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadSymmetryDfParams(m_hSymmetryDfParams, filename, "symmetrydf647D_hU");
		LoadSymmetryDfParams(m_vSymmetryDfParams, filename, "symmetrydf647D_vU");
		LoadSymmetryDfParams(m_sSymmetryDfParams, filename, "symmetrydf647D_vS");
	}
};

class C647D_compass_ant : public C647D_ant
{
public:
	C647D_compass_ant(void) : C647D_ant()
	{
		// Override base class members
		m_name = "USHF_647D_COMPASS";
		m_isMobileAntenna = true;
	}
	~C647D_compass_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadSymmetryDfParams(m_hSymmetryDfParams, filename, "symmetrydf647D_hU");
		LoadSymmetryDfParams(m_vSymmetryDfParams, filename, "symmetrydf647D_vU");
		LoadSymmetryDfParams(m_sSymmetryDfParams, filename, "symmetrydf647D_vS");
	}
};

class C647D_HMon_Switch_ant : public CAntenna
{
public:
	C647D_HMon_Switch_ant(void) : CAntenna()
	{
		//Only initialize values that are different from default setting of CAntenna.
		// Define base class members
		m_baseName = "USHF_647D_HORIZONTAL_MONITOR";
		m_name = "USHF_647D_HORIZONTAL_MONITOR";
		m_HLowFreq = Units::Frequency(20000000UL);
		m_HHighFreq = Units::Frequency(3000000000ULL); //647D Horizontal is 3 GHz.
		m_hasHorizontalPolarization = true;
		m_hasVerticalPolarization = false;
	}
	~C647D_HMon_Switch_ant(void) {};
};

class C647D_VMon_Switch_ant : public CAntenna
{
public:
	C647D_VMon_Switch_ant(void) : CAntenna()
	{
		//Only initialize values that are different from default setting of CAntenna.
		// Define base class members
		m_baseName = "USHF_647D_VERTICAL_MONITOR";
		m_name = "USHF_647D_VERTICAL_MONITOR";
		m_VLowFreq = Units::Frequency(20000000UL);
		m_VHighFreq = Units::Frequency(6000000000ULL); //RF Switch used has 6 GHz limitation.
		m_hasHorizontalPolarization = false;
		m_hasVerticalPolarization = true;
		m_isDfAntenna = false;
	}
	~C647D_VMon_Switch_ant(void) {};
};

class C649_8_ant : public CAntenna
{ ///@note: This antenna is defined as 649-8(Type2) in SMS.
public:
	C649_8_ant(void) : CAntenna()
	{
		// Define base class members
		m_baseName = "USHF_649_8";
		m_name = "USHF_649_8";
		m_VLowFreq = Units::Frequency(20000000UL);
		m_VHighFreq = Units::Frequency(8500000000ULL);
		m_dfVLowFreq = Units::Frequency(20000000UL);
		m_dfVHighFreq = Units::Frequency(8500000000ULL);
		m_highBandCrossoverFreq = Units::Frequency(550000000UL); //550MHz same as SMS, RF/DF.
		m_vDfAlgLo = EDfAlgorithm::WFA;
		m_vDfAlgHi = EDfAlgorithm::SYMMETRY_SHF;
		m_vMAntLo = SEquipCtrlMsg::DF_ANT_5V;
		m_vMAntHi = SEquipCtrlMsg::DF_ANT_9V;
		m_hasHorizontalPolarization = false;
		m_hasVerticalPolarization = true;
		m_isDfAntenna = true;
		m_isMobileAntenna = false;
		m_numAntsLo = 5;
		m_numAntsHi = 9;
	}
	~C649_8_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadSymmetryDfParams(m_sSymmetryDfParams, filename, "symmetrydf649_8");
		TRACE("symmetrydf649_8 numAnts[%u] arrayOffset %f, methods.size %u\n",
				unsigned(m_sSymmetryDfParams.numAnts), m_sSymmetryDfParams.arrayOffset,
				m_sSymmetryDfParams.methods.size());
		TRACE("symmetrydf649_8.methods[0] freq[%f] method %d, uniqueConf %f, gridWidth %u, mainLobeWidth %u, phaseAdjust%d\n",
				m_sSymmetryDfParams.methods[0].freq.Hz<float>(), int(m_sSymmetryDfParams.methods[0].method),
				m_sSymmetryDfParams.methods[0].uniqueConf, unsigned(m_sSymmetryDfParams.methods[0].gridWidth),
				unsigned(m_sSymmetryDfParams.methods[0].mainLobeWidth),
				(m_sSymmetryDfParams.methods[0].phaseAdjust ? 1 : 0));
		TRACE("symmetrydf649_8.methods[4] freq[%f] method %d, uniqueConf %f, gridWidth %u, mainLobeWidth %u, phaseAdjust%d\n",
				m_sSymmetryDfParams.methods[4].freq.Hz<float>(), int(m_sSymmetryDfParams.methods[4].method),
				m_sSymmetryDfParams.methods[4].uniqueConf, unsigned(m_sSymmetryDfParams.methods[4].gridWidth),
				unsigned(m_sSymmetryDfParams.methods[4].mainLobeWidth),
				(m_sSymmetryDfParams.methods[4].phaseAdjust ? 1 : 0));
		LoadWfaDfParams(m_wfaParams, filename, "wfadf649_8");
	}
};

class C649_8_compass_ant : public C649_8_ant
{ ///@note: This antenna is defined as 649-8(Type2)M in SMS.
public:
	C649_8_compass_ant(void) : C649_8_ant()
	{
		// Override base class members
		m_name = "USHF_649_8_COMPASS";
		m_isMobileAntenna = true;
	}
	~C649_8_compass_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadSymmetryDfParams(m_sSymmetryDfParams, filename, "symmetrydf649_8");
		LoadWfaDfParams(m_wfaParams, filename, "wfadf649_8");
	}
};

class C7031_ant : public CAntenna
{
public:
	C7031_ant(void) : CAntenna()
	{
		// Define base class members
		m_baseName = "HF_7031";
		m_name = "HF_7031";
		m_VLowFreq = Units::Frequency(9000UL);
		m_VHighFreq = Units::Frequency(30000000UL);
		m_hasHorizontalPolarization = false;
		m_hasVerticalPolarization = true;
	}
	~C7031_ant(void) {};
};

class C7235_ant : public CAntenna
{
public:
	C7235_ant(void) : CAntenna()
	{
		// Define base class members
		m_baseName = "HF_7235";
		m_name = "HF_7235";
		m_VLowFreq = Units::Frequency(285000UL);
		m_VHighFreq = Units::Frequency(30000000UL);
		m_dfVLowFreq = Units::Frequency(285000UL);
		m_dfVHighFreq = Units::Frequency(30000000UL);
		m_highBandCrossoverFreq = Units::Frequency(285000UL);
		m_vDfAlgLo = EDfAlgorithm::WATSON_WATT;
		m_vDfAlgHi = EDfAlgorithm::WATSON_WATT;
		m_vMAntLo = SEquipCtrlMsg::DF_ANT_3V;
		m_vMAntHi = SEquipCtrlMsg::DF_ANT_3V;
		m_hasHorizontalPolarization = false;
		m_hasVerticalPolarization = true;
		m_isDfAntenna = true;
		m_isMobileAntenna = false;
		m_numAntsLo = 3;
		m_numAntsHi = 3;
	}
	~C7235_ant(void) {};

	virtual void LoadDfParams(const std::string& filename)
	{
		LoadWatsonWattDfParams(m_watsonWatt, filename, "watsonwatt");
	}
};

class C7235_compass_ant : public C7235_ant
{
public:
	C7235_compass_ant(void) : C7235_ant()
	{
		// Override base class members
		m_name = "HF_7235_COMPASS";
		m_isMobileAntenna = true;
	}
	~C7235_compass_ant(void) {};
};

class CAux1_ant : public CAntenna
{
public:
	CAux1_ant(void) : CAntenna()
	{
		// Define base class members
		m_baseName = "VUHF_AUX1";
		m_name = "VUHF_AUX1";
		m_VLowFreq = Units::Frequency(20000000UL);
		m_VHighFreq = Units::Frequency(8500000000ULL);
		m_hasHorizontalPolarization = false;
		m_hasVerticalPolarization = true;
	}
	~CAux1_ant(void) {};
};

class CCustom_ant : public CAntenna
{
public:
	CCustom_ant(void) : CAntenna()
	{
		// Define base class members
		m_baseName = "CUSTOM_ANT";
		m_name = "CUSTOM_ANT";
		m_VLowFreq = Units::Frequency(20000000UL);
		m_VHighFreq = Units::Frequency(8500000000ULL);
		m_hasHorizontalPolarization = false;
		m_hasVerticalPolarization = true;
	}
	~CCustom_ant(void) {};
};

class CCustom_LPA_ant : public CAntenna
{
public:
	CCustom_LPA_ant(void) : CAntenna()
	{
		//Only initialize values that are different from default setting of CAntenna.
		// Define base class members
		m_baseName = "LPA_ANT";
		m_name = "LPA_ANT";
		m_VLowFreq = Units::Frequency(20000000UL);
		m_VHighFreq = Units::Frequency(6000000000ULL);
		m_HLowFreq = Units::Frequency(20000000UL);
		m_HHighFreq = Units::Frequency(6000000000ULL);
		m_hasHorizontalPolarization = true;
		m_hasVerticalPolarization = true;
	}
	~CCustom_LPA_ant(void) {};
};
