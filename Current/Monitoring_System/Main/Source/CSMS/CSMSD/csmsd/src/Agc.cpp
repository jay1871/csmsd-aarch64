/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2017 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"
#include "Agc.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
constexpr const std::chrono::seconds CAgc::AGC_IDLE_TIMEOUT;
#ifdef CSMS_DEBUG
static bool s_traceOut = false;
#endif

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CAgc::CAgc(void) :
	m_agc(true),
	m_agcMap(),
	m_digitizer(),
	m_radioEquip()
{

}


//////////////////////////////////////////////////////////////////////
//
// Add initial AGC to map - must be called if TryGetAgc returns false, after initialAgc is done
//
void CAgc::AddInitialAgc(Units::Frequency freq, Units::Frequency rxBw, CRadioEquip::EBandSelect band, SEquipCtrlMsg::EAnt eAnt,
	unsigned char initAtten, signed char attenAdj, bool& gainChange, unsigned char& atten, std::chrono::milliseconds agcDecayTime)
{
	SAgcKey key = { freq, rxBw, band, eAnt };

	// Create a new entry
	std::lock_guard<std::mutex> lock(m_agcMutex);

	AgcMap::iterator agc = m_agcMap.insert(AgcMap::value_type(key, SAgcValue(initAtten, m_radioEquip->RoundAtten((initAtten + attenAdj))))).first;
#ifdef CSMS_DEBUG
	if(s_traceOut)
	{
		TRACE("AddInitialAgc: %u -> %u dB @ %.6f MHz, %.1f MHz, band %d, eAnt %u\n", agc->second.currAtten, agc->second.nextAtten,
			freq.Hz<double>() / 1e6, rxBw.Hz<double>() / 1e6, int(band), eAnt);
	}
#endif
	if (m_agcMap.size() == 1)
	{
		// Only entry in map, so set initial timeout
		m_agcCond.notify_all();
	}
	SetGainChange(agc, attenAdj, gainChange, agcDecayTime);

	atten = agc->second.currAtten;
	return;

}


//////////////////////////////////////////////////////////////////////
//
// Calculate attenuation adjustment from threshold counts
//
signed char CAgc::CalcAttenAdjust(unsigned char prevAtten, unsigned int total,
	const unsigned int (&counts)[4], const unsigned short (&thresh)[4], int adcFullScale,
	unsigned char minAtten, unsigned char maxAtten)
{
	if (total < 32)
	{
		// Not enough data
		return 0;
	}

	float strength = 0;
	int goodCounters = 0;
	int attenAdj = 0;

	for (unsigned int counter = 0; counter < 4; ++counter)
	{
		// Calculate fraction of samples exceding threshold
		float fraction = float(counts[counter]) / total;

		// Sum "good" signal strength estimates using empirically determined Rician equation
		if (fraction <= 0.05f)
		{
			attenAdj = -MAX_AGC_ATTEN_ADJ;
		}
		else if (fraction < 0.95f)
		{
			strength += thresh[counter] / pow(-2 * log(fraction), 0.9f);
			++goodCounters;
		}
		else
		{
			attenAdj = MAX_AGC_ATTEN_ADJ;
		}
	}

	// Drive average estimate to AGC set point
	if (goodCounters > 0)
	{
		attenAdj = int(20 * log10(strength / (AGC_SET_POINT * adcFullScale * goodCounters)));
	}

//	printf("CalcAttenAdj: %u %u %u %u %u %d %f %d\n", counts[0], counts[1], counts[2], counts[3], total, goodCounters, strength, attenAdj);

	// Limit adjustment
	if (attenAdj > MAX_AGC_ATTEN_ADJ)
	{
		attenAdj = MAX_AGC_ATTEN_ADJ;
	}

	if (attenAdj < -MAX_AGC_ATTEN_ADJ)
	{
		attenAdj = -MAX_AGC_ATTEN_ADJ;
	}

	if (attenAdj < 0 && prevAtten < minAtten + static_cast<unsigned char>(-attenAdj))
	{
		attenAdj = -int(prevAtten - minAtten);
	}
	else if (prevAtten + attenAdj > maxAtten)
	{
		attenAdj = maxAtten - prevAtten;
	}

	return static_cast<signed char>(attenAdj);
}


//////////////////////////////////////////////////////////////////////
//
// Get AGC attenuation needed for master receiver
//
bool CAgc::GetAgcMaster(unsigned char& atten, Units::Frequency freq, Units::Frequency rxBw, CRadioEquip::EBandSelect band,
	SEquipCtrlMsg::EAnt eAnt, CRadioEquip::EGainMode gainMode, bool direct, std::chrono::milliseconds agcDecayTime)
{
	// See if we have a stored setting
	bool gainChange = false;
#ifdef CSMS_DEBUG
	static Units::Frequency l_prevFreq(0);
	static SEquipCtrlMsg::EAnt l_ant(SEquipCtrlMsg::INVALID_ANT);

	if((l_prevFreq != freq) || (l_ant != eAnt) )
	{
		l_prevFreq = freq;
		l_ant = eAnt;
		s_traceOut = true;
	}
#endif
	if (!TryGetAgc(freq, rxBw, band, eAnt, gainChange, atten, agcDecayTime))
	{
		// Perform initial AGC
		unsigned char initAtten;
		signed char attenAdj;
		gainChange = InitialAgc(gainMode, direct, initAtten, attenAdj);

		// Create a new entry
		AddInitialAgc(freq, rxBw, band, eAnt, initAtten, attenAdj, gainChange, atten, agcDecayTime);
	}

#ifdef CSMS_DEBUG
	s_traceOut = false;
#endif
	return gainChange;
}


//////////////////////////////////////////////////////////////////////
//
// Do initial AGC
//
bool CAgc::InitialAgc(C2630::EGainMode gainMode, bool direct, unsigned char& initAtten, signed char& attenAdj) const
{
	attenAdj = 0;

	// Ensure that the number of adc counts is at at least twice the current decimation value
	unsigned int adcCount = 102400;
	auto decimation = static_cast<unsigned long>(m_digitizer->GetDecimation() + 0.5);
	if (adcCount < 2 * decimation)
	{
		adcCount = 2 * decimation;
	}

	unsigned int counts[4];
	unsigned short thresh[4];

	unsigned char minAtten;
	unsigned char maxAtten;
	m_radioEquip->GetCurrentAttenLimits(minAtten, maxAtten);

	for (unsigned int iter = 0; iter < 4; ++iter)
	{
		m_digitizer->CollectCounts(direct, adcCount, counts, thresh);
		m_radioEquip->GetAtten(initAtten);
		attenAdj = CalcAttenAdjust(initAtten, adcCount, counts, thresh, m_digitizer->GetAdcFullScaleCounts(), minAtten, maxAtten);
		if (abs(attenAdj) < MAX_AGC_ATTEN_ADJ)
		{
#ifdef CSMS_DEBUG
			if(s_traceOut)
			{
				timespec ts;
				clock_gettime(CLOCK_MONOTONIC, &ts);
				TRACE("CAgc::InitialAgc[%u] sec = %ld nsec =%ld; %lld setting to mode(%u) initAtten = %u, adj = %d < 30 -> break\n", iter,
					ts.tv_sec, ts.tv_nsec,
					std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count(),
					unsigned(gainMode), unsigned(initAtten), int(attenAdj));
			}
#endif
			break;
		}

		unsigned char setAtten;
		C2630::EGainMode setGainMode;
		bool setLNA;
		m_radioEquip->SetAttenuation(gainMode, m_radioEquip->RoundAtten(initAtten + attenAdj), setAtten, setGainMode, setLNA);
#ifdef CSMS_DEBUG
		if(s_traceOut)
		{
			TRACE("CAgc::InitialAgc[%u] %lld setting to %u %u %u\n", iter,
			      std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count(),
					unsigned(gainMode), initAtten + attenAdj, setAtten);
		}
#endif
	}
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Do timeout processing
//
void CAgc::Timeout(void)
{
	// NOTE: This function does not return until woken up, either by external notify or timeout

//	auto wakeup = std::chrono::time_point<std::chrono::system_clock>::max(); // GCC bug 58931 - doesn't work for wait_until
	auto wakeup = std::chrono::system_clock::now() + std::chrono::hours(24);

	std::unique_lock<std::mutex> lock(m_agcMutex);
	for (auto agc = m_agcMap.begin(); agc != m_agcMap.end();)
	{
		if (agc->second.lastAgcUse != std::chrono::time_point<std::chrono::system_clock>::min())
		{
			if (agc->second.lastAgcUse + AGC_IDLE_TIMEOUT < std::chrono::system_clock::now())
			{
				agc = m_agcMap.erase(agc);
			}
			else
			{
				if (agc->second.lastAgcUse + AGC_IDLE_TIMEOUT < wakeup)
				{
					wakeup = agc->second.lastAgcUse + AGC_IDLE_TIMEOUT;
				}

				++agc;
			}
		}
	}

	m_agcCond.wait_until(lock, wakeup);
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Try to get Agc from map
//
bool CAgc::TryGetAgc(Units::Frequency freq, Units::Frequency rxBw, CRadioEquip::EBandSelect band, SEquipCtrlMsg::EAnt eAnt,
	bool& gainChange, unsigned char& atten, std::chrono::milliseconds agcDecayTime)
{
	// See if we have a stored setting
	signed char attenAdj = 0;
	gainChange = false;

	SAgcKey key = { freq, rxBw, band, eAnt };

	std::lock_guard<std::mutex> lock(m_agcMutex);
	AgcMap::iterator agc = m_agcMap.find(key);
	if (agc == m_agcMap.end())
	{
		return false;	// Need to do initial AGC and then call AddInitialAgc()
	}
	// Get settings
	attenAdj = agc->second.nextAtten - agc->second.currAtten;

	// Reset AGC idle timer
	agc->second.lastAgcUse = std::chrono::system_clock::now();

	// Don't AGC for "small" changes or if inside AGC time or if AGC off
	SetGainChange(agc, attenAdj, gainChange,  agcDecayTime);
	atten = agc->second.currAtten;
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Helper to determine if gainChange has occurred
//
void CAgc::SetGainChange(AgcMap::iterator& agc, signed char attenAdj, bool& gainChange, std::chrono::milliseconds agcDecayTime)
{
	// Must be called with lock held on the agcMap

	// Don't AGC for "small" changes or if inside AGC time or if AGC off
	auto now = std::chrono::system_clock::now();

	if ((!m_agc && agc->second.currAtten != agc->second.nextAtten) || abs(attenAdj) >= 4)
	{
		if (attenAdj > 0 || now > agc->second.lastNoAgcChange + agcDecayTime)
		{
#ifdef CSMS_DEBUG
			if(s_traceOut)
			{
			    TRACE("CAgc::SetGainChange setting currAtten. currAtten was %u. nextAtten = %u\n", agc->second.currAtten, agc->second.nextAtten);

			}
#endif

			agc->second.currAtten = agc->second.nextAtten;
			gainChange = true;
		}
	}
	else
	{
		agc->second.lastNoAgcChange = now;
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update and check AGC values. Return false if bad collection or no AGC. For master csmsd.
//
bool CAgc::UpdateAgc(unsigned char numAnts, Units::Frequency freq, Units::Frequency rxBw, CRadioEquip::EBandSelect band,
	SEquipCtrlMsg::EAnt input, unsigned long adcCount)
{
	// Find the AGC settings
	std::lock_guard<std::mutex> lock(m_agcMutex);
	bool attenOk = false;
	SAgcKey agcKey = { freq, rxBw, band, input };
	AgcMap::iterator agc = m_agcMap.find(agcKey);

#ifdef CSMS_DEBUG
	const static Units::Frequency C_500MHZ(500000000);
	const static Units::Frequency C_TRACE_FREQ(2200000000);
	const static size_t C_TRACE_CNT = 25;
	static Units::Frequency l_prevFreq(0);
	static size_t l_count = 0;
	static size_t l_resetCount = 0;
	static size_t l_updtCount = 0;
	static SEquipCtrlMsg::EAnt l_input(SEquipCtrlMsg::INVALID_ANT);
	bool traceOut = false;
	++l_count;

	if((l_prevFreq != freq) || (l_input != input))
	{
		if(l_input != input || (l_prevFreq + Units::Frequency(80000000)) < freq || C_500MHZ == freq)
		{
			if(agc != m_agcMap.end() || (l_count % C_TRACE_CNT) == 0 || l_count == 1)
			{
				traceOut = true;
			}
		}

		l_prevFreq = freq;
		l_input = input;

		//if(freq == C_TRACE_FREQ && traceOut)
		if(traceOut)
		{
			TRACE("CAgc::UpdateAgc: l_count = %u, input = %u band = %d freq = %fMHz rxBw = %fHz, reset = %u, updt =%u\n",
					l_count, input, int(band), freq.Hz<float>()/ 1000000, rxBw.Hz<float>(),
					l_resetCount, l_updtCount);
		}
	}
#endif

	if (m_agc && agc != m_agcMap.end())
	{
#ifdef CSMS_DEBUG
		++l_updtCount;
#endif
		unsigned char minAtten;
		unsigned char maxAtten;
		m_radioEquip->GetCurrentAttenLimits(minAtten, maxAtten);
		for (unsigned char block = 0; block < numAnts; ++block)
		{
			if (block == 0)
			{
				attenOk = true;
				agc->second.nextAtten = 0;
#ifdef CSMS_DEBUG
				if(traceOut)
				{
					TRACE("UpdateAgc setting nextAtten = 0\n");
				}
#endif
			}

			unsigned int counts[4];
			unsigned short thresh[4];

			if (m_digitizer->GetThresholdCounters(counts, thresh))
			{
#ifdef CSMS_DEBUG
				if(traceOut)
				{
				    TRACE("UpdateAgc: Tone Power = %f\n", m_digitizer->CalcToneDbm(adcCount, counts, thresh));
				}
#endif
				signed char adj = CalcAttenAdjust(agc->second.currAtten, adcCount, counts, thresh, m_digitizer->GetAdcFullScaleCounts(),
					minAtten, maxAtten);

				if (agc->second.currAtten + adj > agc->second.nextAtten)
				{
					agc->second.nextAtten = m_radioEquip->RoundAtten(agc->second.currAtten + adj);
					attenOk = (adj < MAX_GOOD_ATTEN_ADJ);
#ifdef CSMS_DEBUG
					if(traceOut || !attenOk)
					{
					    TRACE("UpdateAgc: block %u %d -> attenOk now = %d\n", block, (adj < MAX_GOOD_ATTEN_ADJ), attenOk);
					    TRACE("UpdateAgc: %u -> %u dB @ %.6f MHz, %.1f MHz, band %d, eAnt %u, ok %d\n", agc->second.currAtten, agc->second.nextAtten,
					    						freq.Hz<double>() / 1e6, rxBw.Hz<double>() / 1e6, int(band), input, attenOk);
					}
#endif
				}
			}
		}
	}
	else
	{
#ifdef CSMS_DEBUG
		++l_resetCount;

		TRACE("CAgc::UpdateAgc(reset): l_count = %u, input = %u band = %d freq = %fMHz rxBw = %fHz, reset = %u, updt =%u, m_agc = %d\n",
				l_count, input, int(band), freq.Hz<float>()/ 1000000, rxBw.Hz<float>(),
				l_resetCount, l_updtCount, (m_agc ? 1 :0));
		m_digitizer->ResetThresholdCounters();
#else
		m_digitizer->ResetThresholdCounters();
#endif
	}

	return attenOk;
}


//////////////////////////////////////////////////////////////////////
//
// Update and check AGC values. Return false if bad collection or no AGC. For slave csmsd.
//
bool CAgc::UpdateAgc(Units::Frequency freq, Units::Frequency rxBw, CRadioEquip::EBandSelect band,
	SEquipCtrlMsg::EAnt input, unsigned long adcCount, const unsigned int (&counts)[4], const unsigned short (&thresh)[4])
{
	// Find the AGC settings
	std::lock_guard<std::mutex> lock(m_agcMutex);
	bool attenOk = false;
	SAgcKey agcKey = { freq, rxBw, band, input };
	AgcMap::iterator agc = m_agcMap.find(agcKey);

	if (m_agc && agc != m_agcMap.end())
	{
		unsigned char minAtten;
		unsigned char maxAtten;
		m_radioEquip->GetCurrentAttenLimits(minAtten, maxAtten);

		attenOk = true;
		agc->second.nextAtten = 0;

		signed char adj = CalcAttenAdjust(agc->second.currAtten, adcCount, counts, thresh, m_digitizer->GetAdcFullScaleCounts(),
			minAtten, maxAtten);

		if (agc->second.currAtten + adj > agc->second.nextAtten)
		{
			agc->second.nextAtten = m_radioEquip->RoundAtten(agc->second.currAtten + adj);
			attenOk = (adj < MAX_GOOD_ATTEN_ADJ);
		}

#ifdef CSMS_DEBUG
		if(!attenOk)
		{
			TRACE("CAgc::UpdateAgc(!attenOk): %u -> %u dB, adj %d, @ %.6f MHz, %.1f MHz, band %d, eAnt %d\n",
					agc->second.currAtten, agc->second.nextAtten, int(adj),
					freq.Hz<double>() / 1e6, rxBw.Hz<double>() / 1e6, int(band), int(input));

		}
#endif
	}

	return attenOk;
}

