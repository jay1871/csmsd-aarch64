/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2014 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

class CFailure
{
	CFailure(void) = delete;
	CFailure(const CFailure&) = delete;
	~CFailure(void) = delete;
	CFailure& operator=(const CFailure&) = delete;

public:
	// Functions
	static void CheckFail(void);
	static void OnFail(std::exception_ptr exception);

private:
	// Data
	static std::exception_ptr m_exception;
	static pthread_t m_main;
	static std::mutex m_mutex;
};
