/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2014 TCI International, Inc. All rights reserved              *
**************************************************************************/

/////////////////////////////////////////////////////////////////////////////
//
// These classes provide C++ wrappers for the pthread reader/writer lock.
////
// In debug mode, usage is checked for illegal recursion
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include <assert.h>
#include <chrono>
#include <mutex>
#include <pthread.h>


/////////////////////////////////////////////////////////////////////////////
//
// Debug support
//
#ifndef NDEBUG
#include <map>
#include <set>

__attribute__((__weak__)) std::mutex g_rwlockDebugLock;
__attribute__((__weak__)) std::map<void*, std::set<std::thread::id>> g_rwlockHeldBy;
#endif


/////////////////////////////////////////////////////////////////////////////
//
// Wrapper for pthread_rwlock_t to make it TimedLockable and SharedTimedLockable
//
class CRWLock
{
	CRWLock(const CRWLock&) = delete;
	CRWLock(CRWLock&&) = delete;
	CRWLock& operator=(const CRWLock&) = delete;
	CRWLock& operator=(CRWLock&&) = delete;

	friend class CLockGuard;
	friend class CSharedLockGuard;

public:
	CRWLock(void) { pthread_rwlock_init(&rwlock, nullptr); }
	~CRWLock(void) { pthread_rwlock_destroy(&rwlock); }

	void lock(void)
	{
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		auto holders = g_rwlockHeldBy.find(this);
		auto me = std::this_thread::get_id();
		assert(holders == g_rwlockHeldBy.end() || holders->second.find(me) == holders->second.end());
		g_rwlockDebugLock.unlock();
#endif
		pthread_rwlock_wrlock(&rwlock);
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		g_rwlockHeldBy[this].insert(me);
		g_rwlockDebugLock.unlock();
#endif
	}

	void lock_shared(void)
	{
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		auto holders = g_rwlockHeldBy.find(&rwlock);
		auto me = std::this_thread::get_id();
		assert(holders == g_rwlockHeldBy.end() || holders->second.find(me) == holders->second.end());
		g_rwlockDebugLock.unlock();
#endif
		pthread_rwlock_rdlock(&rwlock);
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		g_rwlockHeldBy[this].insert(me);
		g_rwlockDebugLock.unlock();
#endif
	}

	bool try_lock(void)
	{
#ifndef NDEBUG
		if(pthread_rwlock_trywrlock(&rwlock) == 0)
		{
			g_rwlockDebugLock.lock();
			g_rwlockHeldBy[this].insert(std::this_thread::get_id());
			g_rwlockDebugLock.unlock();
			return true;
		}
		else
		{
			return false;
		}
#else
		return pthread_rwlock_trywrlock(&rwlock) == 0;
#endif
	}

	bool try_lock_shared(void)
	{
#ifndef NDEBUG
		if(pthread_rwlock_tryrdlock(&rwlock) == 0)
		{
			g_rwlockDebugLock.lock();
			g_rwlockHeldBy[this].insert(std::this_thread::get_id());
			g_rwlockDebugLock.unlock();
			return true;
		}
		else
		{
			return false;
		}
#else
		return pthread_rwlock_tryrdlock(&rwlock) == 0;
#endif
	}

	template<typename R, typename P = std::ratio<1>> bool try_lock_for(std::chrono::duration<R, P> rel_time)
	{
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		auto holders = g_rwlockHeldBy.find(this);
		auto me = std::this_thread::get_id();
		assert(holders == g_rwlockHeldBy.end() || holders->second.find(me) == holders->second.end());
		g_rwlockDebugLock.unlock();
#endif
		auto abs_time = std::chrono::system_clock::now() + rel_time;
		auto time = std::chrono::system_clock::to_time_t(abs_time);
		typedef std::ratio_divide<std::chrono::system_clock::period, std::nano> Clock2Nano;
		timespec timeout = { time, (abs_time - std::chrono::system_clock::from_time_t(time)).count() * Clock2Nano::num / Clock2Nano::den };
#ifndef NDEBUG

		if(pthread_rwlock_timedwrlock(&rwlock, &timeout) == 0)
		{
			g_rwlockDebugLock.lock();
			g_rwlockHeldBy[this].insert(me);
			g_rwlockDebugLock.unlock();
			return true;
		}
		else
		{
			return false;
		}
#else
		return pthread_rwlock_timedwrlock(&rwlock, &timeout) == 0;
#endif
	}

	template<typename R, typename P = std::ratio<1>> bool try_lock_shared_for(std::chrono::duration<R, P> rel_time)
	{
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		auto holders = g_rwlockHeldBy.find(this);
		auto me = std::this_thread::get_id();
		assert(holders == g_rwlockHeldBy.end() || holders->second.find(me) == holders->second.end());
		g_rwlockDebugLock.unlock();
#endif
		auto abs_time = std::chrono::system_clock::now() + rel_time;
		auto time = std::chrono::system_clock::to_time_t(abs_time);
		typedef std::ratio_divide<std::chrono::system_clock::period, std::nano> Clock2Nano;
		timespec timeout = { time, (abs_time - std::chrono::system_clock::from_time_t(time)).count() * Clock2Nano::num / Clock2Nano::den };
#ifndef NDEBUG

		if(pthread_rwlock_timedrdlock(&rwlock, &timeout) == 0)
		{
			g_rwlockDebugLock.lock();
			g_rwlockHeldBy[this].insert(me);
			g_rwlockDebugLock.unlock();
			return true;
		}
		else
		{
			return false;
		}
#else
		return pthread_rwlock_timedwrlock(&rwlock, &timeout) == 0;
#endif
	}

	template<typename C, typename D = typename C::duration> bool try_lock_until(std::chrono::time_point<C, D> abs_time)
	{
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		auto holders = g_rwlockHeldBy.find(this);
		auto me = std::this_thread::get_id();
		assert(holders == g_rwlockHeldBy.end() || holders->second.find(me) == holders->second.end());
		g_rwlockDebugLock.unlock();
#endif
		auto time = C::to_time_t(abs_time);
		typedef std::ratio_divide<typename D::period, std::nano> D2Nano;
		timespec timeout = { time, (abs_time - C::from_time_t(time)).count() * D2Nano::num / D2Nano::den };
#ifndef NDEBUG

		if(pthread_rwlock_timedwrlock(&rwlock, &timeout) == 0)
		{
			g_rwlockDebugLock.lock();
			g_rwlockHeldBy[this].insert(me);
			g_rwlockDebugLock.unlock();
			return true;
		}
		else
		{
			return false;
		}
#else
		return pthread_rwlock_timedrdlock(&rwlock, &timeout) == 0;
#endif
	}

	template<typename C, typename D = typename C::duration> bool try_lock_shared_until(std::chrono::time_point<C, D> abs_time)
	{
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		auto holders = g_rwlockHeldBy.find(this);
		auto me = std::this_thread::get_id();
		assert(holders == g_rwlockHeldBy.end() || holders->second.find(me) == holders->second.end());
		g_rwlockDebugLock.unlock();
#endif
		auto time = C::to_time_t(abs_time);
		typedef std::ratio_divide<typename D::period, std::nano> D2Nano;
		timespec timeout = { time, (abs_time - C::from_time_t(time)).count() * D2Nano::num / D2Nano::den };
#ifndef NDEBUG

		if(pthread_rwlock_timedrdlock(&rwlock, &timeout) == 0)
		{
			g_rwlockDebugLock.lock();
			g_rwlockHeldBy[this].insert(me);
			g_rwlockDebugLock.unlock();
			return true;
		}
		else
		{
			return false;
		}
#else
		return pthread_rwlock_timedrdlock(&rwlock, &timeout) == 0;
#endif
	}

	void unlock(void)
	{
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		auto holders = g_rwlockHeldBy.find(this);
		auto me = std::this_thread::get_id();
		assert(holders != g_rwlockHeldBy.end() && holders->second.size() == 1 && holders->second.count(me) == 1);
#endif
		pthread_rwlock_unlock(&rwlock);
#ifndef NDEBUG
		g_rwlockHeldBy.find(this)->second.erase(me);
		g_rwlockDebugLock.unlock();
#endif
	}

	void unlock_shared(void)
	{
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		auto holders = g_rwlockHeldBy.find(this);
		auto me = std::this_thread::get_id();
		assert(holders != g_rwlockHeldBy.end() && holders->second.count(me) == 1);
#endif
		pthread_rwlock_unlock(&rwlock);
#ifndef NDEBUG
		g_rwlockHeldBy.find(this)->second.erase(me);
		g_rwlockDebugLock.unlock();
#endif
	}

private:
	mutable pthread_rwlock_t rwlock;
};


/////////////////////////////////////////////////////////////////////////////
//
// Lock guard for exclusive lock
//
// CallLocked allows an object to be initialized while the lock is held.
//
// CallUnlocked allows lock to be released, do something, then reacquire the lock
//  in an exception-safe manner.
//
//  Typically, the arguments to CallLocked and CallUnlocked are lambdas
//
class CLockGuard
{
	CLockGuard(const CLockGuard&) = delete;
	CLockGuard(CLockGuard&&) = delete;
	CLockGuard& operator=(const CLockGuard&) = delete;
	CLockGuard& operator=(CLockGuard&&) = delete;

public:
	// Functions
	CLockGuard(std::mutex& mutex) : locked(false), mutex(&mutex), rwlock(nullptr) { Lock(); }
	CLockGuard(CRWLock& rwlock) : locked(false), mutex(nullptr), rwlock(&rwlock) { Lock(); }
	~CLockGuard(void) { if(locked) Unlock(); }

	template<typename L, typename F> static auto CallLocked(L& lock, F f) -> decltype(f())
	{
		CLockGuard guard(lock);
		
		return f();
	}

	template<typename F> auto CallUnlocked(F f) -> decltype(f())
{
	struct Unlocker // Releases lock while instance is in scope
	{
			Unlocker(CLockGuard& lock_) : lock(lock_) { lock.Unlock(); }
			~Unlocker(void) { lock.Lock(); }
			CLockGuard& lock;
		} unlock(*this);

		return f();
	}

	void Lock(void)
	{
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		auto lock = (rwlock == nullptr ? static_cast<void*>(mutex) : static_cast<void*>(rwlock));
		auto holders = g_rwlockHeldBy.find(lock);
		auto me = std::this_thread::get_id();
		assert(holders == g_rwlockHeldBy.end() || holders->second.find(me) == holders->second.end());
		g_rwlockDebugLock.unlock();
#endif
		if(rwlock == nullptr)
		{
			mutex->lock();
		}
		else
		{
			pthread_rwlock_wrlock(&rwlock->rwlock);
		}

		locked = true;
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		g_rwlockHeldBy[lock].insert(me);
		g_rwlockDebugLock.unlock();
#endif
	}

	void Unlock(void)
	{
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		auto lock = (rwlock == nullptr ? static_cast<void*>(mutex) : static_cast<void*>(rwlock));
		auto holders = g_rwlockHeldBy.find(lock);
		auto me = std::this_thread::get_id();
		assert(holders != g_rwlockHeldBy.end() && holders->second.size() == 1 && holders->second.count(me) == 1);
#endif
		locked = false;

		if(rwlock == nullptr)
		{
			mutex->unlock();
		}
		else
		{
			pthread_rwlock_unlock(&rwlock->rwlock);
		}

#ifndef NDEBUG
		g_rwlockHeldBy.find(lock)->second.erase(me);
		g_rwlockDebugLock.unlock();
#endif
	}

private:
	// Data
	bool locked;
	std::mutex* mutex;
	CRWLock* rwlock;
};


/////////////////////////////////////////////////////////////////////////////
//
// Lock guard for shared lock
//
// CallLocked allows an object to be initialized while the lock is held.
//
// CallUnlocked allows lock to be released, do something, then reacquire the lock
//  in an exception-safe manner.
//
//  Typically, the arguments to CallLocked and CallUnlocked are lambdas
//
class CSharedLockGuard
{
	CSharedLockGuard(const CSharedLockGuard&) = delete;
	CSharedLockGuard(CSharedLockGuard&&) = delete;
	CSharedLockGuard& operator=(const CSharedLockGuard&) = delete;
	CSharedLockGuard& operator=(CSharedLockGuard&&) = delete;

public:
	// Functions

	CSharedLockGuard(CRWLock& rwlock) : locked(false), rwlock(rwlock) { Lock(); }
	~CSharedLockGuard(void) { if (locked) Unlock(); }

	template<typename F> static auto CallLocked(CRWLock& rwlock, F f) -> decltype(f())
	{
		CSharedLockGuard lock(rwlock);
		return f();
	}

	template<typename F> auto CallUnlocked(F f) -> decltype(f())
	{
		struct Unlocker // Releases lock while instance is in scope
	{
			Unlocker(CSharedLockGuard& lock) : lock(lock) { lock.Unlock(); }
			~Unlocker(void) { lock.Lock(); }
			CSharedLockGuard& lock;
		} unlock(*this);

		return f();
	}

	void Lock(void)
	{
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		auto holders = g_rwlockHeldBy.find(&rwlock);
		auto me = std::this_thread::get_id();
		assert(holders == g_rwlockHeldBy.end() || holders->second.find(me) == holders->second.end());
		g_rwlockDebugLock.unlock();
#endif
		pthread_rwlock_rdlock(&rwlock.rwlock);
		locked = true;
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		g_rwlockHeldBy[&rwlock].insert(me);
		g_rwlockDebugLock.unlock();
#endif
	}

	void Unlock(void)
	{
#ifndef NDEBUG
		g_rwlockDebugLock.lock();
		auto holders = g_rwlockHeldBy.find(&rwlock);
		auto me = std::this_thread::get_id();
		assert(holders != g_rwlockHeldBy.end() && holders->second.count(me) == 1);
#endif
		locked = false;
		pthread_rwlock_unlock(&rwlock.rwlock);
#ifndef NDEBUG
		g_rwlockHeldBy.find(&rwlock)->second.erase(me);
		g_rwlockDebugLock.unlock();
#endif
	}

private:
	// Data
	bool locked;
	CRWLock& rwlock;
};
