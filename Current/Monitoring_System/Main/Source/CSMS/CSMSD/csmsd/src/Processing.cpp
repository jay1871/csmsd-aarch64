/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"

#include "Antenna.h"
#include "AvdTask.h"
#include "Df.h"
#include "EquipControl.h"
#include "Failure.h"
#include "Fft.h"
#include "MeasurementTask.h"
#include "Ne10Vec.h"
#include "OccupancyTask.h"
#include "PanTask.h"
#include "PersistentData.h"
#include "Processing.h"
#include "ScanDfTask.h"
#include "Singleton.h"
#include "Utility.h"

bool CProcessing::m_debug = false;

#ifdef CSMS_DEBUG
//This flag is also used in CProcessing::FindDataBlocks
bool CProcessing::s_traceAvd = false;
#endif

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CProcessing::CProcessing(void) :
	m_realtimeNet(_T("3307")),
	m_config(),
	m_digitizer(),
	m_noise(1),
	m_pbCal(),
	m_queue(),
	m_radioEquip(),
	m_refVolts(1),
	m_sampleVolts(1),
	m_watchdog()
{
	m_thread = std::thread(&CProcessing::Thread, this);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CProcessing::~CProcessing(void)
{
	Stop();
	TRACE("CProcessing dtor\n");
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Add a task to the queue
//
void CProcessing::AddTask(_In_ const CTask::Task& task, int priority, unsigned int timeout)
{
	m_queue.AddItem(task, priority, timeout);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Band-limit spectrum
// Returns number of suppressed bins (positive if at beginning of band; negative if at end)
//
std::pair<int, int> CProcessing::BandLimit(Ne10F32Vec& data, const Units::FreqPair& dataLimits, const Units::FreqPair& freqLimits)
{
	std::pair<int, int> suppressed = { 0, 0 };

	if(freqLimits.second < dataLimits.first || freqLimits.first > dataLimits.second)
	{
		suppressed.first = int(data.size());
		data = 0.0f;
	}
	else
	{
		if (freqLimits.first > dataLimits.first)
		{
			size_t bin = size_t(data.size() * (freqLimits.first - dataLimits.first) / (dataLimits.second - dataLimits.first));
			suppressed.first = int(bin);
			///@note: No need to waste CPU cycle in zeroing the data
			//ne10sSet(0.f, &data[0], bin);
		}
		if (freqLimits.second < dataLimits.second)
		{
			size_t bin = size_t(data.size() * (freqLimits.second - dataLimits.first) / (dataLimits.second - dataLimits.first));
			suppressed.second = int(data.size() - bin);
			///@note: No need to waste CPU cycle in zeroing the data
			//ne10sSet(0.f, &data[bin], data.size() - bin);
		}
	}

	return suppressed;
}


Units::Frequency CProcessing::BandMidFrequency(const Units::FreqPair& dataLimits, const Units::FreqPair& freqLimits)
{
	if(freqLimits.second < dataLimits.first || freqLimits.first > dataLimits.second)
	{
		return Units::Frequency();
	}

	auto lowFreq = dataLimits.first;
	if (freqLimits.first > dataLimits.first)
	{
		lowFreq = freqLimits.first;
	}
	auto highFreq = dataLimits.second;
	if (freqLimits.second < dataLimits.second)
	{
		highFreq = freqLimits.second;
	}
	Units::Frequency midFrequency = (lowFreq + highFreq) / 2;
	return midFrequency;
}

//////////////////////////////////////////////////////////////////////
/// Calculate SHF extension gain if it exists, and adjust it with the
///  the cable loss from Analyzer to the csms RF input.
///
///@param[in] eAnt: User selected antenna.
///
///@param[in] tunedFreq: Tuned frequency of SHF Extension.
///
///@return SHF Extension Gain in dB afjusted by cable loss,
///	0 if no SHF extension.
///
inline float CProcessing::CalcShfExtGain(SEquipCtrlMsg::EAnt eAnt, Units::Frequency tunedFreq)
{
	float shfGain = 0.0f;

	if(m_config->IsShfExtUsed(eAnt, tunedFreq))
	{
		///@note: For now: SHF Extension analyzer is always connected to RF2(aux1) input.
		shfGain = (m_config->GetShfExtensionGain(tunedFreq) -
		    m_config->GetShfExtIfCableLoss(CConfig::ERfPort::C_RF2));
	}

	return(shfGain);
}

//////////////////////////////////////////////////////////////////////
//
// Perform AVD processing
//
void CProcessing::DoAvd(_In_ const CTask::Task& task)
{
	const CConfig::SProcParams& procParams = task->GetProcParams();
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CTask::SBlockState& blockState = task->GetBlockState();
	SEquipCtrlMsg::EAnt eAnt = CTask::GetEAntValue(task->m_ant, taskParams.pol);
	Units::Frequency rfFreq = task->GetTuneFreq();
	auto usedShf = m_config->IsShfExtUsed(eAnt, rfFreq);

	// Get parent task
	CAvdTask::AvdTask avdTask(std::dynamic_pointer_cast<CAvdTask>(task->GetParent()));

	if (!avdTask)
	{
		// Parent task does not exist or is wrong type
		THROW_LOGIC_ERROR();
	}

	bool needSlave = m_config->IsSlaveAntenna(task->m_ant, taskParams.hf);
	Ne10F32Vec watts;

	unsigned long numBins = procParams.GetNumBins(task->GetProcBw());
	SDataPointers ptrs;
#ifdef CSMS_DEBUG
	static Units::Frequency l_prevFreq(0);
	static bool l_TraceOutput = false;
	static size_t l_count = 0;
	static size_t l_dataBlkErr = 0;

	++l_count;
#ifdef CSMS_2016
	if(s_traceAvd)
	{
		timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		TRACE("CProcessing::DoAvd: sec = %ld nsec =%ld; numBins= %lu, rfFreq = %lf, usedShf = %d, eAnt =%u, l_count =%u, hrd2ProcCnt =%u, 3220Cnt = %u, BlkErr = %u\n",
				ts.tv_sec, ts.tv_nsec, numBins, rfFreq.Hz<double>(), (usedShf ? 1: 0), unsigned(eAnt),
				l_count, CHwControl::s_addAvd2ProcCnt, C3230::s_psdSampleCnt, l_dataBlkErr);
	}
#endif
#endif

	if (FindDataBlocks(task, 1U, ptrs))
	{
		GetData(task, needSlave, ptrs, numBins, watts, usedShf);
	}
#ifdef CSMS_DEBUG
	else
	{
		++l_dataBlkErr;
		TRACE("CProcessing::DoAvd: No FindDataBlocks, numBins= %lu, rfFreq = %lf, usedShf = %d, eAnt =%u, l_count =%u, hrd2ProcCnt =%u, BlkErr = %u\n",
				numBins, rfFreq.Hz<double>(), (usedShf ? 1: 0), unsigned(eAnt),
				l_count, CHwControl::s_addAvd2ProcCnt, l_dataBlkErr);
		//ASSERT(false);
	}
#endif
	m_digitizer->m_sdma.DoneWithSamples(blockState.sampleCount);

	if (!CTask::FindTask(avdTask->m_taskIdKey))		// Can this happen?
	{
		TRACE(">>>>>>>>>>>>>>>>>> CProcessing::DoAvd task not found\n");
		// Task has ended
		return;
	}

	if(task->GetAbandonDwell())
	{
		TRACE(">>>>>>>>>>>>>>>>>> CProcessing::DoAvd task abandoned\n");
		// Task has been interrupted by SYSTEM priority task
		avdTask->AbandonDwell();
		return;
	}

	bool partialBlock = false;
	float enbw = 1.f;

	if (watts.size() > 0)
	{
		enbw = CFft::GetEnbw(procParams.sampleSize);

		Units::Frequency rxFreq(usedShf ? m_config->GetShfExtIfFreq() : task->GetTuneFreq());

		BandLimit(watts, Units::FreqPair(rxFreq - task->GetProcBw() / 2, rxFreq + task->GetProcBw() / 2),
			blockState.freqLimits);

		if (task->InLastBlock())		// TODO: Is this correct? In DoOccupancy, it uses AtStepEnd()
		{
			// Last block - truncate numBins
			numBins = procParams.GetNumBins((taskParams.stopFreq - task->GetTuneFreq() + task->GetProcBw() / 2));
			watts.resize(numBins);		// TODO: This is not done in DoOccupancy?
		}

		partialBlock = blockState.freqLimits.second < rxFreq + task->GetProcBw() / 2 &&
			blockState.freqLimits.second < taskParams.stopFreq;
#ifdef CSMS_DEBUG
		if(l_prevFreq != rfFreq || (l_count % 100 == 0))
		{
			l_prevFreq = rfFreq;
			l_TraceOutput = true;
			TRACE("CProcessing::DoAvd: AddBlock %u %d, rfFreq = %lf, usedShf = %d, eAnt =%u, l_count =%u\n",
					watts.size(), partialBlock, rfFreq.Hz<double>(),
					(usedShf ? 1: 0), unsigned(eAnt), l_count);
		}
#endif
		avdTask->AddBlock(watts, partialBlock);
	}

	if(task->AtDwellEnd() && !partialBlock)
	{
		Units::Frequency firstChanFreq = task->GetFirstChanFreq();
		unsigned long binsPerChan = procParams.GetNumBins(taskParams.bw);
		unsigned long numChan = numBins / binsPerChan;
		Ne10F32Vec chanWatts(numChan);
		avdTask->GetChanWatts(binsPerChan, numChan, chanWatts, enbw);

#ifdef CSMS_DEBUG
		if(l_TraceOutput)
		{
			l_TraceOutput = false;
			TRACE("CProcessing::DoAvd: SendRealtimeSpectrumMsg firstChanFreq = %fMhz binsPerChan = %lu numChan = %lu chanWatts.size = %u\n",
			      firstChanFreq.Hz<float>()/1000000, binsPerChan, numChan, chanWatts.size());
		}
#endif
		SendRealtimeSpectrumMsg(task, chanWatts);

		// Update signal states
		avdTask->UpdateBlock(task->GetStep(), task->GetBlock(), firstChanFreq);
	}

	if (task->AtEnd())
	{
		if (task->ReportNow() || task->Completed())
		{
//			TRACE("Processing: AtEnd && ReportNow || Completed\n");
			// Send task state
			SEquipCtrlMsg resp;
			resp.hdr = task->m_msgHdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::OCCUPANCY_STATE_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SStateResp);
			resp.body.stateResp = CSuspendableTask::GetState(task->m_taskIdKey);
			CEquipControl::Send(task->m_source, resp, _T("OCCUPANCY_STATE_RESP"));

			// Report via EquipCtrlMsg
			avdTask->SendReports(task->Completed());
		}
		// Are we done?
		if (task->Completed())
		{
//			TRACE("Processing: AtEnd && Completed\n");
			// Send task completed status
			SEquipCtrlMsg resp;
			resp.hdr = task->m_msgHdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::TASK_STATUS;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SEquipTaskStatusResp);
			resp.body.equipTaskStatusResp.dateTime = Utility::CurrentTimeAsDATE();
			resp.body.equipTaskStatusResp.key = task->m_taskIdKey.key;
			resp.body.equipTaskStatusResp.status = (task->Terminated() ?
				SEquipCtrlMsg::SEquipTaskStatusResp::TERMINATED : SEquipCtrlMsg::SEquipTaskStatusResp::COMPLETED);
			resp.body.equipTaskStatusResp.taskId = task->m_taskIdKey.taskId;
			CEquipControl::Send(task->m_source, resp, _T("TASK_STATUS"));

			// Send realtime spectrum stop message
			SSmsRealtimeMsg realtimeMsg;
			realtimeMsg.hdr.msgType = SSmsRealtimeMsg::RT_SPECTRUM_STOP;
			realtimeMsg.hdr.msgTypeVersion = 1;
			realtimeMsg.hdr.bodySize = sizeof(SSmsRealtimeMsg::SStop);
			realtimeMsg.body.stop.taskId = task->m_taskIdKey.taskId;
			m_realtimeNet.SendToAllClients(&realtimeMsg);

			// Delete the parent task
			TRACE("DoAvd: deleting task\n");
			try
			{
				dynamic_cast<CHwControl&>(*this).DeleteTask(task->m_taskIdKey);
			}
			catch(const std::bad_cast&)
			{
				TRACE("bad cast to CHwControl\n");
			}
			CTask::DeleteTask(avdTask->m_taskIdKey);
		}
		else
		{
			// Update measurement tasks
//			TRACE("Avd task UpdateTask\n");
			avdTask->UpdateTask();
			if (task->GetTaskParams().mode == CConfig::EMode::MEASURE)
			{
				TRACE("Avd task is now a MEASURE task\n");
			}

			if (task->ReportNow() && avdTask->m_source == nullptr)
			{
				SaveRestartData(avdTask);
			}
			// Make task runnable again
			try
			{
				dynamic_cast<CHwControl&>(*this).SetRunnable(task->m_taskIdKey, true, false);
			}
			catch(const std::bad_cast&)
			{
				TRACE("bad cast to CHwControl\n");
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Perform measurement processing
//
void CProcessing::DoDf(const CTask::Task& task)
{
	const CConfig::SProcParams& procParams = task->GetProcParams();
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CTask::SBlockState& blockState = task->GetBlockState();

	CMeasurementTask::MeasurementTask measTask(std::dynamic_pointer_cast<CMeasurementTask>(task->GetParent()));

	auto mutableConfig = m_config->GetMutableConfig();

	unsigned int numAnts = CAntenna::NumAnts(
		taskParams.hf ? mutableConfig.hfConfig.antCable.antenna.get() : mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get(),
		task->GetTuneFreq());
	unsigned long numDfBins = measTask->GetDfNumBins();

	std::vector<Ne10F32Vec> wattsBins;
	bool failed = false;
	SDataPointers ptrs;
	bool found = FindDataBlocks(task, numAnts, ptrs);
	if (found)
	{
		Ne10F32Vec terminatedData;
		failed = GetDfData(task, ptrs, numDfBins, wattsBins, terminatedData);

		if (blockState.withTerm)
		{
			TRACE("Processing::DoDf: has terminated data\n");
			measTask->SetTerminatedData(terminatedData);
		}
		m_digitizer->m_sdma.DoneWithSamples(blockState.sampleCount);
	}
	else	// didn't find any data
	{
		failed = true;
	}

	if (!failed)
	{
		// Apply EDR, if enabled
		if (m_config->HasEnhancedDynamicRange())
		{
			auto& terminatedData = measTask->GetTerminatedData();
			if (terminatedData.size() == numDfBins)
			{
				for (size_t ant = 0; ant < wattsBins.size(); ++ant)
				{
					wattsBins[ant] -= terminatedData;
					ne10sThreshold_LTVal(wattsBins[ant], SPUR_CAL_NOISE_FLOOR_FACTOR * procParams.GetBinSize().Hz<float>(), m_noise[ant]->GetNoise());
				}
			}
			else
			{
				TRACE("DoDf: Terminated data size error\n");
			}
		}
		float refWatts = 0;
		float sampleWattsMax = 0;
		size_t antMax = 0;
		size_t polBlock = (taskParams.pol == CConfig::EPolarization::BOTH ? task->GetBlock() / 2 : task->GetBlock());
		if (polBlock < SEquipCtrlMsg::DFCUTS_MAX)
		{
			// Find reference power
			for (size_t ant = 0; ant < wattsBins.size(); ++ant)
			{
				refWatts += wattsBins[ant].sum();
			}

			// Find largest sample voltage
			for (size_t ant = 0; ant < m_dfWatts.size(); ++ant)
			{
				float sampleWatts = m_dfWatts[ant].sum();
				if (sampleWatts > sampleWattsMax)
				{
					sampleWattsMax = sampleWatts;
					antMax = ant;
				}
			}
		}
		float refDbm = 10.f * log10f(refWatts / CFft::GetEnbw(procParams.sampleSize) / numAnts) + 30.f;
		float arg = sampleWattsMax / CFft::GetEnbw(procParams.sampleSize);
		static const float MINDBM = -200.f;	// dbm
		static const float MINWATTS = pow(10.f, (MINDBM - 30.f)/10.f);
		float sampleDbm = (arg > MINWATTS ? 10.f * log10f(arg) + 30.f : MINDBM);

		// Filter bins by adaptive threshold and signal consistency across elements
		size_t ant;
		std::vector<float> thresh(numAnts);

		for (ant = 0; ant < numAnts; ++ant)
		{
			// Set threshold 1/4 between noise and peak (in dB), but no less than 10 dB SNR
			float noise = m_noise[ant]->GetNoise();
			thresh[ant] =  expf(0.75f * logf(noise) + 0.25f * logf(wattsBins[ant].maximum()));

			if (thresh[ant] < noise * 10)
			{
				thresh[ant] = noise * 10;
			}
		}

		Ne10F32cVec volts(numAnts, Ne10F32cVec::ZERO);
		Ne10F32cVec denom(numAnts, Ne10F32cVec::ZERO);

		for (size_t bin = 1; bin < numDfBins - 1; ++bin)
		{
			for (ant = 0; ant < numAnts; ++ant)
			{
				if (wattsBins[ant][bin] < thresh[ant])
				{
					// Signal missing on this element
					break;
				}
			}

			if (ant == numAnts)
			{
				// Signal present on all elements
				for (ant = 0; ant < numAnts; ++ant)
				{
					volts[ant] += {
						m_sampleVolts[ant].real(bin) * m_refVolts[ant].real(bin) + m_sampleVolts[ant].imag(bin) * m_refVolts[ant].imag(bin),
						m_sampleVolts[ant].imag(bin) * m_refVolts[ant].real(bin) - m_sampleVolts[ant].real(bin) * m_refVolts[ant].imag(bin) };
					denom[ant].r += m_refVolts[ant].real(bin) * m_refVolts[ant].real(bin) +
						m_refVolts[ant].imag(bin) * m_refVolts[ant].imag(bin);
				}
			}
		}

#if 0
		printf("\nblock %u freq = %f gainAdj = %15.5e\n", polBlock, task->GetTuneFreq().Hz<double>(), blockState.gainAdj);
		size_t i1 = (numDfBins > 7 ? (numDfBins / 2) - 2 : 1);
		size_t i2 = std::min(i1 + 5, size_t(numDfBins - 1));
		for (size_t i = i1; i < i2; ++i)
		{
			printf("%u ref:", i);
			for (size_t j = 0; j < numAnts; ++j)
			{
				printf(" %.9f %.9f", m_refVolts[j].real(i), m_refVolts[j].imag(i));
			}
			printf("\n");
			printf("%u smp:", i);
			for (size_t j = 0; j < numAnts; ++j)
			{
				printf(" %.9f %.9f", m_sampleVolts[j].real(i), m_sampleVolts[j].imag(i));
			}
			printf("\n");
			Ne10F32cVec v(numAnts);
			Ne10F32cVec ref(numAnts);
			for (size_t j = 0; j < numAnts; ++j)
			{
				v[j] = { m_sampleVolts[j].real(i), m_sampleVolts[j].imag(i) };
				ref[j] = { m_refVolts[j].real(i), m_refVolts[j].imag(i) };
			}
			Ne10F32Vec mag;
			Ne10F32Vec phas;
			ne10sCartToPolar(ref, mag, phas);
			printf("%u ref mag.phas:", i);
			for (size_t j = 0; j < numAnts; ++j)
			{
				printf(" %.9f %.9f", mag[j], phas[j] * Units::R2D);
			}
			printf("\n");
			ne10sCartToPolar(v, mag, phas);
			printf("%u smp mag.phas:", i);
			for (size_t j = 0; j < numAnts; ++j)
			{
				printf(" %.9f %.9f", mag[j], phas[j] * Units::R2D);
			}
			printf("\n");
			v /= ref;
			ne10sCartToPolar(v, mag, phas);
			printf("%u v mag.phas:", i);
			for (size_t j = 0; j < numAnts; ++j)
			{
				printf(" %.4f %.4f", mag[j], phas[j] * Units::R2D);
			}
			printf("\n");
		}
#endif

		bool horiz = (taskParams.pol == CConfig::EPolarization::HORIZ) ||
			(taskParams.pol == CConfig::EPolarization::BOTH && task->GetVHCurrentPolarization() == CConfig::EPolarization::HORIZ);

		CAntenna::EDfAlgorithm alg = CAntenna::DfAlgorithm(
			taskParams.hf ? mutableConfig.hfConfig.antCable.antenna.get() : mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get(),
			task->GetTuneFreq(), horiz);

		if (denom[0].r != 0)
		{
			// Good cut
			volts /= denom;
			CDf df(volts, taskParams.startFreq, alg, measTask->GetPrevAz(horiz));
#ifdef CSMS_DEBUG
			auto uncorrectedAz = df.GetUncorrectedAz()[0];
			auto el = df.GetEl()[0];
			auto conf = df.GetConf()[0];
			measTask->AddDf(uncorrectedAz, el, conf, blockState.sampleTime.GetDATE(), refDbm,
				sampleDbm, antMax, horiz);
			TRACE("CProcessing::DoDf: volts[%u], uncorrectedAz[0] %f; el[0] %f, conf[0] %f, startFreq %lfMHz,alg %d, refDbm %f, sampleDbm %f, antMax %u. horiz %d\n",
					volts.size(), uncorrectedAz, el, conf, taskParams.startFreq.Hz<double>()/1000000,
					int(alg), refDbm, sampleDbm, antMax, (horiz ? 1 : 0));

#else
			measTask->AddDf(df.GetUncorrectedAz()[0], df.GetEl()[0], df.GetConf()[0], blockState.sampleTime.GetDATE(), refDbm,
				sampleDbm, antMax, horiz);
#endif
		}
		else
		{
			// Bad cut
			for (ant = 0; ant < numAnts; ++ant)
			{
				for (unsigned short bin = 1; bin < numDfBins - 1; ++ bin)
				{
					volts[ant] += {
						m_sampleVolts[ant].real(bin) * m_refVolts[ant].real(bin) + m_sampleVolts[ant].imag(bin) * m_refVolts[ant].imag(bin),
						m_sampleVolts[ant].imag(bin) * m_refVolts[ant].real(bin) - m_sampleVolts[ant].real(bin) * m_refVolts[ant].imag(bin) };
					denom[ant].r += m_refVolts[ant].real(bin) * m_refVolts[ant].real(bin) +
						m_refVolts[ant].imag(bin) * m_refVolts[ant].imag(bin);
				}
			}

			volts /= denom;
			measTask->AddDf(0, 0, 0, blockState.sampleTime.GetDATE(), refDbm, sampleDbm, antMax, horiz);
		}

		if (polBlock < SEquipCtrlMsg::DFVOLTS_MAX)
		{
			if (polBlock == 0)
			{
				// SNR in DF measurement bandwidth
				float noise = 0;

				for (ant = 0; ant < numAnts; ++ant)
				{
					noise += m_noise[ant]->GetNoise();
				}

				measTask->AddPower(refDbm, refDbm - 10 * log10(numDfBins * noise / numAnts) - 30, horiz);
			}

			// Voltage vectors and powers
			measTask->AddVolts(volts, refDbm, sampleDbm, 0, antMax, polBlock, horiz);
		}
	}	// (!failed)

	if (task->AtEnd())
	{
		measTask->CalcDfSummary();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Perform measurement processing
//
void CProcessing::DoMeasure(_In_ const CTask::Task& task)
{
	const CConfig::SProcParams& procParams = task->GetProcParams();
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CTask::SBlockState& blockState = task->GetBlockState();

	// Get parent task
	CMeasurementTask::MeasurementTask measTask(std::dynamic_pointer_cast<CMeasurementTask>(task->GetParent()));

	if (!measTask)
	{
		// Parent task does not exist or is wrong type
		THROW_LOGIC_ERROR();
	}
	bool needSlave = m_config->IsSlaveAntenna(task->m_ant, taskParams.hf);

	if (taskParams.mode == CConfig::EMode::IQDATA)
	{
		SDataPointers ptrs;
		if (!FindDataBlocks(task, 1U, ptrs))
		{
			measTask->UpdateIqData(uncached_ptr(), std::vector<unsigned long>(), 0, 0, task, true);	// just clears CMeasurementTask::m_resp
		}
		else
		{
			std::vector< std::vector<unsigned long> > slaveSamples;
			if (needSlave)
			{
				bool slaveDataOk = false;
				MsgMapValue slaveData;
				if (blockState.slaveBufferKey != 0)	// Probably should always be true
				{
					slaveDataOk = WaitForSlaves(blockState.slaveSentIndex, blockState.slaveBufferKey, slaveData);
					if (!slaveDataOk)
					{
						TRACE("CProcessing::WaitForSlaves failed\n");
					}
				}
				if (slaveDataOk && slaveData)
				{
					slaveSamples = std::move(slaveData->iq);
				}
			}
			TRACE("UpdateIqData samplesize=%lu\n", procParams.sampleSize);
			unsigned int xferSize = SEquipCtrlMsg::SIqDataResp::IQDATA_SIZE;
			bool lastPacket = false;
			auto samples = ptrs.data[0].iq;
			int slavesampleindex = 0;
			for (unsigned long numSamplesRemaining = procParams.sampleSize, packetIndex = 0; numSamplesRemaining > 0;
				numSamplesRemaining -= xferSize, ++packetIndex, samples += xferSize)
			{
				if (numSamplesRemaining <= SEquipCtrlMsg::SIqDataResp::IQDATA_SIZE)
				{
					xferSize = numSamplesRemaining;
					lastPacket = true;
				}
				if (needSlave)
				{
					if (!slaveSamples.empty())
					{
						measTask->UpdateIqData(uncached_ptr(), slaveSamples[0], xferSize, packetIndex, task, lastPacket, slavesampleindex);
						slavesampleindex += xferSize;
					}
				}
				else
				{
					measTask->UpdateIqData(samples, std::vector<unsigned long>(), xferSize, packetIndex, task, lastPacket);
				}
				// Can remain in this loop for extended time period
				m_watchdog->Ping(m_processingPingSource);
			}
		}
		m_digitizer->m_sdma.DoneWithSamples(blockState.sampleCount);
	}
	else if(taskParams.mode == CConfig::EMode::MEASURE)
	{
//		TRACE("Processing MEASURE samplesize=%lu @ %lu\n", procParams.sampleSize, blockState.sampleCount);
		// Since, with EDR, the terminated data is collected after the non-terminated data, they must be retrieved
		// in the same order!

		Ne10F32Vec terminatedData;
		Ne10F32Vec watts;
		Ne10F32cVec samples;
		unsigned long numBins = procParams.GetNumBins(taskParams.bw) + 1;

		bool usedShf = false;

		if(!needSlave)
		{
			SEquipCtrlMsg::EAnt eAnt = CTask::GetEAntValue(task->m_ant, taskParams.pol);
	    	Units::Frequency rfFreq = task->GetTuneFreq();
	    	usedShf = m_config->IsShfExtUsed(eAnt, rfFreq);
		}

#ifdef CSMS_DEBUG
		bool dataBlockFound = false;
#endif
		SDataPointers ptrs;
		if (FindDataBlocks(task, 1U, ptrs))
		{
			GetData(task, needSlave, ptrs, numBins, watts, samples, terminatedData, usedShf);
#ifdef CSMS_DEBUG
			dataBlockFound = true;
#endif
		}
		m_digitizer->m_sdma.DoneWithSamples(blockState.sampleCount);

		// Now, master/slave data in samples, watts(gain corrected). Both have terminatedData.
		if (blockState.withTerm)
		{
			// Finish processing the terminated data
			TRACE("Processing::DoMeasure: has withTerm set\n");
			if (terminatedData.size() >= numBins)
			{
				ne10sThreshold_LTVal(terminatedData,
					SPUR_CAL_THRESHOLD * CNoiseEstimate::Create(terminatedData, numBins, 0)->GetNoise(), 0);
				terminatedData *= TWO_DB;  // Raise nonzero values by 2 dB
				measTask->SetTerminatedData(terminatedData);
			}
			else
			{
				if (terminatedData.size() > 0 || taskParams.withPsd)
					printf("DoMeasure missing terminated data\n");
			}
		}

		// Process the non-terminated data
		{
			// Apply EDR, if wanted
			if (watts.size() > 0)
			{
				if (m_config->HasEnhancedDynamicRange())
				{
					if (watts.size() == measTask->GetTerminatedData().size())
					{
						float noise = CNoiseEstimate::Create(watts, numBins, 0)->GetNoise();
						watts -= measTask->GetTerminatedData();
						ne10sThreshold_LTVal(watts, CProcessing::SPUR_CAL_NOISE_FLOOR_FACTOR * procParams.GetBinSize().Hz<float>(), noise);
					}
					else
					{
						printf("CProcessing::DoMeasure: watts.size != terminatedData.size %u %u\n", watts.size(), measTask->GetTerminatedData().size());
					}
				}

				Units::Frequency rxFreq(usedShf ? m_config->GetShfExtIfFreq() : taskParams.startFreq);

				// Now, both have samples and watts(gain corrected, EDR)
				// Bandlimit the spectrum
				BandLimit(watts, Units::FreqPair(rxFreq - taskParams.bw / 2, rxFreq + taskParams.bw / 2),
					blockState.freqLimits);
			}
#ifdef CSMS_DEBUG
			if (watts.size() == 0)
			{
				SEquipCtrlMsg::EAnt eAnt = CTask::GetEAntValue(task->m_ant, taskParams.pol);
		    	Units::Frequency rfFreq = task->GetTuneFreq();
				TRACE("Processing::DoMeasure(watts.size==0): dataBlockFound = %d, eAnt = %u, rfFreq = %lfMHz, numBins = %lu, freqLimits (%fMHz: %fMHz): radioFreq %fMHz, inverted %d,pbCalBand %u,pbCalDirect %d,sampleCount %lu,switchTemp %f,withTerm %d\n",
						(dataBlockFound ? 1 : 0), unsigned(eAnt),
						rfFreq.Hz<double>()/1000000, numBins,
						blockState.freqLimits.first.Hz<float>()/1000000, blockState.freqLimits.second.Hz<float>()/1000000,
						blockState.radioFreq.Hz<float>()/1000000, (blockState.inverted ? 1 :0), blockState.pbCalBand,
						(blockState.pbCalDirect ? 1 :0), blockState.sampleCount, blockState.switchTemp,
						(blockState.withTerm ? 1 :0));
			}
			else
			{
				// Measurement
				measTask->AddBlock(samples, watts, task);
			}
#else
			// Measurement
			measTask->AddBlock(samples, watts, task);
#endif
		}
	}
	else if(taskParams.mode == CConfig::EMode::DF)
	{
		DoDf(task);
	}
	else
	{
		THROW_LOGIC_ERROR();
	}

	if (!measTask->IsAvd() && task->AtEnd())
	{
		CEquipControl::Send(measTask->m_source, measTask->GetResponse(), _T("GET_MEAS_RESPONSE"));	// Sends last packet
		try
		{
			dynamic_cast<CHwControl&>(*this).DeleteTask(task->m_taskIdKey);
		}
		catch(const std::bad_cast&)
		{
			TRACE("bad cast to CHwControl\n");
		}
		CTask::DeleteTask(task->m_taskIdKey);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Perform occupancy processing
//
void CProcessing::DoOccupancy(_In_ const CTask::Task& task)
{
//	TRACE("CProcessing::DoOccupancy entered %lld\n", Utility::GetTickCount());
	const CConfig::SProcParams& procParams = task->GetProcParams();
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CTask::SBlockState& blockState = task->GetBlockState();

	SEquipCtrlMsg::EAnt eAnt = CTask::GetEAntValue(task->m_ant, taskParams.pol);
	Units::Frequency rxFreq = task->GetTuneFreq();
	auto usedShf = m_config->IsShfExtUsed(eAnt, rxFreq);

	if(usedShf)
	{
	    rxFreq = m_config->GetShfExtIfFreq();
	}

	// Get parent task
	COccupancyTask::OccupancyTask occupTask(std::dynamic_pointer_cast<COccupancyTask>(task->GetParent()));

	if (!occupTask)
	{
		// Parent task does not exist or is wrong type
		THROW_LOGIC_ERROR();
	}

	bool needSlave = m_config->IsSlaveAntenna(task->m_ant, taskParams.hf);
	SDataPointers ptrs;
	bool found = FindDataBlocks(task, 1U, ptrs);

	if (!CTask::FindTask(occupTask->m_taskIdKey))		// Can this happen?
	{
		TRACE(">>>>>>>>>>>>>>>>>> CProcessing::DoOccupancy task not found\n");
		FlushData(needSlave, blockState);
		return;
	}

	if (task->GetAbandonDwell())
	{
		TRACE(">>>>>>>>>>>>>>>>>> CProcessing::DoOccupancy task abandoned %p\n", task.get());
		// Task has been interrupted by SYSTEM priority task
		FlushData(needSlave, blockState);
		occupTask->AbandonDwell();
		return;
	}
	unsigned long numBins = procParams.GetNumBins(task->GetProcBw());

	// Since, with EDR, the terminated data is collected after the non-terminated data, they must be retrieved
	// in the same order!
	Ne10F32Vec watts;
	Ne10F32Vec terminatedData;

	if (found)
	{
		if (occupTask->HasTerminatedData(task->GetStep(), task->GetBlock()))
		{
			GetData(task, needSlave, ptrs, numBins, watts, usedShf);
		}
		else
		{
			GetData(task, needSlave, ptrs, numBins, watts, terminatedData, usedShf);
		}
	}
	m_digitizer->m_sdma.DoneWithSamples(blockState.sampleCount);

	// Now, master or slave data is in terminatedData and watts - no EDR yet nor bandlimiting
	if (watts.size() == 0)
	{
		printf("CProcessing::DoOccupancy: watts is empty task id = %lu\n", task->m_taskIdKey.key);
		// TODO: any point in continuing??
		return;
	}

	// Finish processing the terminated data
	if (terminatedData.size() > 0)
	{
		ne10sThreshold_LTVal(terminatedData,
			SPUR_CAL_THRESHOLD_PAN * CNoiseEstimate::Create(terminatedData, numBins, 0)->GetNoise(), 0);
		terminatedData *= TWO_DB;  // Raise nonzero values by 2 dB
		///@note: The checking of blockState.freqLimits.second < rxFreq + task->GetProcBw() / 2
		///	  is enough for SHF Extension. blockState.freqLimits.second < taskParams.stopFreq
		///	  is kept to conform with pre-existing code, as this will always be true for
		///   SHF Extension.
		auto partialBlock = blockState.freqLimits.second < rxFreq + task->GetProcBw() / 2 &&
			blockState.freqLimits.second < taskParams.stopFreq;
//		printf("Processing::DoOccupancy: SetTerminatedData %f %f %lu %u %u %u\n",
//			task->GetTuneFreq().Hz<double>(), blockState.radioFreq.Hz<double>(), numBins, task->GetStep(), task->GetBlock(), partialBlock);
		BandLimit(terminatedData, Units::FreqPair(rxFreq - task->GetProcBw() / 2, rxFreq + task->GetProcBw() / 2),
			blockState.freqLimits);
		occupTask->AddTerminatedDataBlock(terminatedData, task->GetStep(), task->GetBlock(), partialBlock);		// Must be called before AddBlock
	}
	// Now watts is gain corrected data from either master or slave and terminatedData is bandlimited and saved.

	// Process the non-terminated data
	{
		Units::Frequency firstChanFreq = task->GetFirstChanFreq();
		bool partialBlock = false;
		float enbw = 1.f;

		if (watts.size() > 0)
		{
			enbw = CFft::GetEnbw(procParams.sampleSize);
			// Bandlimit the spectrum
			BandLimit(watts, Units::FreqPair(rxFreq - task->GetProcBw() / 2, rxFreq + task->GetProcBw() / 2),
				blockState.freqLimits);

			if (task->AtStepEnd())
			{
				// Last block - truncate numBins
				numBins = procParams.GetNumBins(taskParams.stopFreq - task->GetTuneFreq() + task->GetProcBw() / 2);
			}

			///@note: The checking of blockState.freqLimits.second < rxFreq + task->GetProcBw() / 2
			///	  is enough for SHF Extension. blockState.freqLimits.second < taskParams.stopFreq
			///	  is kept to conform with pre-existing code, as this will always be true for
			///   SHF Extension.
			partialBlock = blockState.freqLimits.second < rxFreq + task->GetProcBw() / 2 &&
				blockState.freqLimits.second < taskParams.stopFreq;

//			TRACE("DoOccupancy: firstChanFreq = %f numBins = %lu datalimits = %f-%f freqlimits = %f-%f partialBlock %d\n",
//				firstChanFreq.Hz<double>(), numBins, (task->GetTuneFreq() - task->GetProcBw() / 2).Hz<double>(),
//				(task->GetTuneFreq() + task->GetProcBw() / 2).Hz<double>(),
//				blockState.freqLimits.first.Hz<double>(), blockState.freqLimits.second.Hz<double>(), partialBlock);
			occupTask->AddBlock(watts, partialBlock);	// Add block from master or slave, bandlimited but no EDR yet.
		}

		if (task->AtDwellEnd() && !partialBlock)
		{
			unsigned long binsPerChan = procParams.GetNumBins(taskParams.bw);
			size_t band = task->GetStep();
			size_t block = task->GetBlock();
			unsigned long numChan = numBins / binsPerChan;

			// Handle enhanced dynamic range, if enabled

			if (m_config->HasEnhancedDynamicRange() && !occupTask->IsTerminatedDataEmpty(band, block))
			{
				occupTask->EnhanceBlock(occupTask->GetTerminatedData(band, block), numBins, SPUR_CAL_NOISE_FLOOR_FACTOR * procParams.GetBinSize().Hz<float>());
			}

//			TRACE("Dwell end: band,block,numChan = %u,%u,%lu\n", band, block, numChan);
			Ne10F32Vec chanWatts(numChan);
			occupTask->GetChanWatts(band, block, binsPerChan, numChan, chanWatts, enbw);
			/*for (size_t i = 0; i < chanWatts.size(); i++) // debug only
			{
				if (chanWatts[i] > 1.e-13)
					printf("large sample found at index %d value = %.5e watt size = %d\n", i, chanWatts[i], chanWatts.size());
				if (chanWatts[i] < 7.e-26)
					printf("small sample found at index %d value = %.5e watt size = %d\n", i, chanWatts[i], chanWatts.size());
			}*/
			SendRealtimeSpectrumMsg(task, chanWatts);

			if (occupTask->m_reportInterval > 0)
			{
				// Update
				occupTask->Update(band, block, firstChanFreq, blockState.switchTemp, blockState.sampleTime.GetDATE());
			}

			occupTask->NextDwell();
		}

		if (task->AtEnd() && occupTask->m_reportInterval > 0)
		{
			occupTask->UpdateCompletionTime(blockState.sampleTime.GetDATE());
		}

		// Send reports
		if (task->AtEnd() && (task->ReportNow() || task->Completed()))
//		if (task->AtEnd() && task->Completed())
		{
			///  WE ARE HERE
			// Send task state
			SEquipCtrlMsg resp;
			resp.hdr = task->m_msgHdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::OCCUPANCY_STATE_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SStateResp);
			resp.body.stateResp = CSuspendableTask::GetState(task->m_taskIdKey);
			CEquipControl::Send(task->m_source, resp, _T("OCCUPANCY_STATE_RESP"));

			if (occupTask->m_reportInterval > 0)
			{
				// Report via EquipCtrlMsg
				occupTask->SendReports(task->Completed());
			}

			// Are we done?
			if (task->Completed())
			{
				// Send task completed status
				resp.hdr.msgSubType = SEquipCtrlMsg::TASK_STATUS;
				resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SEquipTaskStatusResp);
				resp.body.equipTaskStatusResp.dateTime = Utility::CurrentTimeAsDATE();
				resp.body.equipTaskStatusResp.key = task->m_taskIdKey.key;
				resp.body.equipTaskStatusResp.status = (task->Terminated() ?
					SEquipCtrlMsg::SEquipTaskStatusResp::TERMINATED : SEquipCtrlMsg::SEquipTaskStatusResp::COMPLETED);
				resp.body.equipTaskStatusResp.taskId = task->m_taskIdKey.taskId;
				CEquipControl::Send(task->m_source, resp, _T("TASK_STATUS"));

				// Send realtime spectrum stop message
				SSmsRealtimeMsg realtimeMsg;
				realtimeMsg.hdr.msgType = SSmsRealtimeMsg::RT_SPECTRUM_STOP;
				realtimeMsg.hdr.msgTypeVersion = 1;
				realtimeMsg.hdr.bodySize = sizeof(SSmsRealtimeMsg::SStop);
				realtimeMsg.body.stop.taskId = task->m_taskIdKey.taskId;
				m_realtimeNet.SendToAllClients(&realtimeMsg);

				// Delete the parent task
				TRACE("DoOccupancy: deleting task\n");
				try
				{
					dynamic_cast<CHwControl&>(*this).DeleteTask(task->m_taskIdKey);
				}
				catch(const std::bad_cast&)
				{
					TRACE("bad cast to CHwControl\n");
				}
				CTask::DeleteTask(occupTask->m_taskIdKey);
			}
			else if (occupTask->m_source == nullptr)
			{
				SaveRestartData(occupTask);
			}
		}
	}

//	TRACE("CProcessing::DoOccupancy exited %lld\n", Utility::GetTickCount());
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Perform pan processing
//
void CProcessing::DoPan(_In_ const CTask::Task& task)
{
//	TRACE("CProcessing::DoPan entered %lld\n", Utility::GetTickCount());
	const CConfig::SProcParams& procParams = task->GetProcParams();
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CTask::SBlockState& blockState = task->GetBlockState();

	bool needSlave = m_config->IsSlaveAntenna(task->m_ant, taskParams.hf);

	CPanTask::PanTask panTask(std::dynamic_pointer_cast<CPanTask>(task->GetParent()));
	SDataPointers ptrs;
	bool found = FindDataBlocks(task, 1U, ptrs);

	if (task->m_source == nullptr)	// No point in continuing
	{
		std::vector<Ne10F32Vec> slaveWatts;
		// Flush master and, if necessary, slave data
		FlushData(needSlave, blockState);
		DATE now = Utility::CurrentTimeAsDATE();
		panTask->UpdateCompletionTime(now + PAN_TIMEOUT);
		return;
	}
	unsigned long numBins = procParams.GetNumBins(taskParams.bw) + 1;

	SEquipCtrlMsg::EAnt eAnt = CTask::GetEAntValue(task->m_ant, taskParams.pol);
	Units::Frequency rfFreq = task->GetTuneFreq();
	auto usedShf = m_config->IsShfExtUsed(eAnt, rfFreq);

	// Since, with EDR, the terminated data is collected after the non-terminated data, they must be retrieved
	// in the same order!

	Ne10F32Vec watts;

	// Get the collected data
	if (found)
	{
		if (taskParams.withPsd)
		{
			// Found master data, see if we have slave data, if needed
			GetData(task, needSlave, ptrs, numBins, watts, panTask->m_terminatedData, usedShf);
		}
		if (!ptrs.data[0].psd || (blockState.withTerm && !ptrs.term.psd))	// TODO: Remove this check?
		{
			printf("DoPan: required psd data is missing\n");
			m_digitizer->m_sdma.DoneWithSamples(blockState.sampleCount);
			DATE now = Utility::CurrentTimeAsDATE();
			panTask->UpdateCompletionTime(now + PAN_TIMEOUT);
			return;
		}
	}
	m_digitizer->m_sdma.DoneWithSamples(blockState.sampleCount);

	// Now, master or slave data is in m_terminatedData and watts.
	if (watts.size() == 0)
	{
		printf("CProcessing::DoPan: watts is empty\n");
		DATE now = Utility::CurrentTimeAsDATE();
		panTask->UpdateCompletionTime(now + PAN_TIMEOUT);
		return;
	}

	if (blockState.withTerm)
	{
		// Finish processing the terminated data
		if (panTask->m_terminatedData.size() > 0)
		{
			ne10sThreshold_LTVal(panTask->m_terminatedData,
				SPUR_CAL_THRESHOLD_PAN * CNoiseEstimate::Create(panTask->m_terminatedData, numBins, 0)->GetNoise(), 0);
			panTask->m_terminatedData *= TWO_DB;  // Raise nonzero values by 2 dB
			printf("Processing::DoPan: saving terminated data %d %f %f %lu %u\n", needSlave,
				taskParams.startFreq.Hz<double>(), taskParams.bw.Hz<double>(), numBins, panTask->m_terminatedData.size());
		}
		else
		{
			printf("CProcessing::DoPan: withTerm and no terminated data needSlave=%d\n", needSlave);
		}
	}

	// Now, watts is the gain corrected data from either master or slave.

	// Process the non-terminated data
	{
		// Apply EDR, if wanted

		if (m_config->HasEnhancedDynamicRange())
		{
			if (watts.size() == panTask->m_terminatedData.size())
			{
//				CFft::TraceSpectrum("Before watts", 0, numBins, &watts[0]);
//				CFft::TraceSpectrum("Before term", 0, numBins, &panTask->m_terminatedData[0]);
				float noise = CNoiseEstimate::Create(watts, numBins, 0)->GetNoise();
				watts -= panTask->m_terminatedData;
				ne10sThreshold_LTVal(watts, SPUR_CAL_NOISE_FLOOR_FACTOR * procParams.GetBinSize().Hz<float>(), noise);
//				CFft::TraceSpectrum("After watts", 0, numBins, &watts[0]);
			}
			else
			{
				float noise = CNoiseEstimate::Create(watts, numBins, 0)->GetNoise();
				ne10sThreshold_LTVal(watts, SPUR_CAL_NOISE_FLOOR_FACTOR * procParams.GetBinSize().Hz<float>(), noise);
				printf("CProcessing::DoPan: watts.size() != panTask->m_terminatedData.size() %u %u\n", watts.size(), panTask->m_terminatedData.size());
			}
		}
		else
		{
			float noise = CNoiseEstimate::Create(watts, numBins, 0)->GetNoise();
			ne10sThreshold_LTVal(watts, SPUR_CAL_NOISE_FLOOR_FACTOR * procParams.GetBinSize().Hz<float>(), noise);
		}

		Units::Frequency freq = (usedShf ? m_config->GetShfExtIfFreq() : taskParams.startFreq);

#ifdef CSMS_DEBUG
		static Units::Frequency l_prevFreq(0);

		if(l_prevFreq != taskParams.startFreq)
		{
			l_prevFreq = taskParams.startFreq;
			TRACE("CProcessing::DoPan: eAnt = %u startFreq = %fMhz stopFreq = %fMhz freq = %fMhz bw = %f freqLimits.first =%fMhz freqLimits.second =%fMhz\n",
			      eAnt, taskParams.startFreq.Hz<float>()/1000000, taskParams.stopFreq.Hz<float>()/1000000,
			      freq.Hz<float>()/1000000, taskParams.bw.Hz<float>(), blockState.freqLimits.first.Hz<float>()/1000000,
			      blockState.freqLimits.second.Hz<float>()/1000000);

		}
#endif
		// Bandlimit the spectrum
		BandLimit(watts, Units::FreqPair(freq - taskParams.bw / 2, freq + taskParams.bw / 2),
			blockState.freqLimits);

		if((taskParams.numBlocks == 2) && panTask->PartialData() &&
		    (taskParams.startFreq < panTask->GetCenterFreq()))
		{
			//Clear the saved data since this is the 1st block.
			panTask->ClearSavedPanData();

		}

		panTask->AddPan(watts);
	}

	// Task is AtEnd, send result
	// Find the task in the scheduler and check that it matches
	CTask::Task currentPan;
	try
	{
		currentPan = dynamic_cast<CHwControl&>(*this).GetTask(task->m_taskIdKey);
	}
	catch(const std::bad_cast&)
	{
		TRACE("bad cast to CHwControl\n");
	}

	if (!currentPan)
	{
		TRACE("Processing::DoPan mismatch (%lu)\n", task->m_taskIdKey.key);
		DATE now = Utility::CurrentTimeAsDATE();
		panTask->UpdateCompletionTime(now + PAN_TIMEOUT);
	}
	else if(!panTask->PartialData())
	{
		auto r = panTask->GetResponse();
		CEquipControl::Send(panTask->m_source, *r, nullptr);		// Don't log these
#ifdef CSMS_DEBUG
		static Units::Frequency l_rawFreq(0);

		if(l_rawFreq != Units::Frequency(r->body.getPanResp.freq))
		{
		    l_rawFreq = Units::Frequency(r->body.getPanResp.freq);
		    TRACE("CProcessing::DoPan Send: freq = %fMhz numBins = %u powerDbm = %u rcvrAtten = %u status = %d\n",
		          l_rawFreq.Hz<float>()/1000000, unsigned(r->body.getPanResp.numBins),
		          unsigned(r->body.getPanResp.powerDbm), unsigned(r->body.getPanResp.rcvrAtten),
		          int(r->body.getPanResp.status));

		}
#endif
		DATE now = Utility::CurrentTimeAsDATE();
		panTask->UpdateCompletionTime(now + PAN_TIMEOUT);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Perform passband calibration processing
//
void CProcessing::DoPbCal(_In_ const CTask::Task& task)
{
	static size_t timeouts = 0;

	if (task->GetStep() == 0 && task->GetBlock() == 0)
	{
		timeouts = 0;
		if (task->GetNumSteps() > 1)
		{
			// Clear the cal data
			m_pbCal->Clear();
		}
	}

	const CConfig::SProcParams& procParams = task->GetProcParams();
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CTask::SBlockState& blockState = task->GetBlockState();

	if (taskParams.numBlocks > 0 && timeouts < 10)
	{
		auto mutableConfig = m_config->GetMutableConfig();

		// Skip FindSamples cautiously - TODO: skip completely
		unsigned long timeout = (blockState.failedDf ? 10 : CSBufferPSdma::SAMPLES_TIMEOUT);
		SDataPointers ptrs;
		bool found = FindDataBlocks(task, 1U, ptrs, timeout);

		bool failed = false;
		m_refVolts.resize(1);
		m_sampleVolts.resize(1);
		unsigned long numBins = procParams.GetNumBins(task->GetProcBw());

//		TRACE("CProcessing::DoPbCal %f %lu %lu %u %u %u %d\n",
//			task->GetTuneFreq().Hz<double>(), procParams.sampleSize, numBins, blockState.pbCalBand, task->GetStep(), task->GetBlock(), task->AtStepEnd());

		if (found)
		{
			bool slaveDataOk = false;
			MsgMapValue slaveData;
			if (blockState.slaveBufferKey != 0)	// Probably should always be true
			{
				slaveDataOk = WaitForSlaves(blockState.slaveSentIndex, blockState.slaveBufferKey, slaveData);
				if (!slaveDataOk)
				{
					TRACE("CProcessing::WaitForSlaves failed\n");
				}
			}
			if (slaveDataOk && slaveData)
			{
				// Have master and slave data

				// Do we want to get gain equalization data?
//				Ne10F32Vec adjPower(numBins);		// Use for power not voltages, for now
//				if (m_config->HasGainEqualization())
//				{
//					m_radioEquip->GetCorrection(task->GetTuneFreq(), blockState.radioFreq, taskParams.rxHwBw, blockState.rfGainTableIdx,
//						blockState.ifGainTableIdx, procParams.GetBinSize(), taskParams.hf, blockState.rcvrAtten, adjPower);
//				}
				if (!ptrs.data[0].psd || !ptrs.data[0].fftI || !ptrs.data[0].fftQ)
				{
					failed = true;
				}
				else
				{
					// Sample voltages are in slaveData
					m_sampleVolts = std::move(slaveData->volts);
				}
			}
			else	// !slaveDataOk
			{
				failed = true;
				TRACE("CProcessing::DoPbCal: slave data not available\n");
			}
		}
		else
		{
			failed = true;	// No master data found
		}

//		TRACE("CProcessing::DoPbCal failed = %d\n", failed);
		if (!failed)
		{
			CFft::GetVoltages2(numBins, procParams.sampleSize, ptrs.data[0].fftI, ptrs.data[0].fftQ, blockState.gainAdj, m_refVolts[0]);
			// Apply gain equalization if wanted - NOTE: not currently doing gain equalization on voltages

			// Filter out weakest 5% of ref bins - TODO: using magnitudes like SMS. Could use equalized power instead?
			static Ne10F32Vec mag;
			static Ne10F32Vec amplitude[2];
			ne10sMagnitude(m_refVolts[0], amplitude[0]);
			float threshold = CStats::Percentile(amplitude[0], 5);
			ne10sThreshold_LTVal(m_refVolts[0], threshold, Ne10F32cVec::ZERO);

			if (m_refVolts[0].size() != m_sampleVolts[0].size())
			{
				TRACE("ref != sample\n");
			}
			// Get ratio between channels
			m_refVolts[0] /= m_sampleVolts[0];

			// Filter out weakest 5% of sample bins
			ne10sMagnitude(m_sampleVolts[0], amplitude[1]);
			threshold = CStats::Percentile(amplitude[1], 5);
			ne10sThreshold_LTVal(m_sampleVolts[0], threshold, Ne10F32cVec::ZERO);
			ne10sThreshold_GTVal(m_sampleVolts[0], 0, Ne10F32cVec::ONE);
			m_refVolts[0] *= m_sampleVolts[0];
			Units::Frequency freq = (taskParams.startFreq + taskParams.stopFreq) / 2;
			m_pbCal->AddBlock(freq, taskParams.rxHwBw, blockState.pbCalBand, procParams.GetBinSize(),
				taskParams.hf, blockState.pbCalDirect, Ne10F32cVec(m_refVolts[0]), amplitude, task->GetStep() == 0 && task->GetBlock() == 0, task->AtStepEnd());

			timeouts = 0;
		}
		else
		{
			++timeouts;
		}
	}
	m_digitizer->m_sdma.DoneWithSamples(blockState.sampleCount);

	if (task->AtEnd())
	{
		if (task->m_source != nullptr)
		{
			// Send response
			std::vector<SEquipCtrlMsg::SInitializeDfResp::SPbCalStatus> status;
			m_pbCal->GetStatus(status);
			auto bodySize = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SInitializeDfResp, pbCalStatus) +
				status.size() * sizeof(SEquipCtrlMsg::SInitializeDfResp::SPbCalStatus));
			std::unique_ptr<SEquipCtrlMsg> resp(new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg);
			resp->hdr = task->m_msgHdr;
			resp->hdr.msgSubType = SEquipCtrlMsg::INITIALIZE_DF_RESPONSE;
			resp->hdr.bodySize = bodySize;
			resp->body.initializeDfResp.initIntStatus = ErrorCodes::SUCCESS;
			resp->body.initializeDfResp.initSymStatus = ErrorCodes::SUCCESS;
			resp->body.initializeDfResp.initWfaStatus = ErrorCodes::SUCCESS;
			resp->body.initializeDfResp.initWwStatus = ErrorCodes::SUCCESS;
			resp->body.initializeDfResp.vhfAntCalStatus = ErrorCodes::SUCCESS;
			resp->body.initializeDfResp.numPbCalStatus = static_cast<unsigned long>(status.size());

			for(size_t i = 0; i < status.size(); ++i)
			{
				resp->body.initializeDfResp.pbCalStatus[i] = status[i];
			}

			CEquipControl::Send(task->m_source, *resp, _T("INITIALIZE_DF_RESPONSE"));
		}
		TRACE("deleting pbcal task\n");
		try
		{
			dynamic_cast<CHwControl&>(*this).DeleteTask(task->m_taskIdKey);
		}
		catch(const std::bad_cast&)
		{
			TRACE("bad cast to CHwControl\n");
		}
		CTask::DeleteTask(task->m_taskIdKey);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Perform scan DF processing
//
void CProcessing::DoScanDf(_In_ const CTask::Task& task)
{
	const CConfig::SProcParams& procParams = task->GetProcParams();
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CTask::SBlockState& blockState = task->GetBlockState();

	// Get parent task
	CScanDfTask::ScanDfTask scanDfTask(std::dynamic_pointer_cast<CScanDfTask>(task->GetParent()));

	if (!scanDfTask)
	{
		// Parent task does not exist or is wrong type
		THROW_LOGIC_ERROR();
	}

	auto mutableConfig = m_config->GetMutableConfig();
	Units::Frequency tunedFreq(task->GetTuneFreq());
	unsigned int numAnts = CAntenna::NumAnts(
		taskParams.hf ? mutableConfig.hfConfig.antCable.antenna.get() : mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get(),
		tunedFreq);

	// Skip FindSamples cautiously - TODO: skip completely
	unsigned long timeout = (blockState.failedDf ? 10 : CSBufferPSdma::SAMPLES_TIMEOUT);
	SDataPointers ptrs;
	bool found = FindDataBlocks(task, numAnts, ptrs, timeout);

	if (!CTask::FindTask(scanDfTask->m_taskIdKey))		// Can this happen?
	{
		printf(">>>>>>>>>>>>>>>>>> CProcessing::DoScanDf task not found\n");
		// Task has ended
		FlushData(true, blockState);
		return;
	}

	if(task->GetAbandonDwell())
	{
		printf(">>>>>>>>>>>>>>>>>> CProcessing::DoScanDf task abandoned %p\n", task.get());
		// Task has been interrupted by SYSTEM priority task
		FlushData(true, blockState);
		scanDfTask->AbandonDwell();
		return;
	}

	unsigned long numBins = procParams.GetNumBins(task->GetProcBw());

	static std::vector<Ne10F32Vec> mwatts;
	bool failed = false;
	if (found)
	{
		Ne10F32Vec terminatedData;
		failed = GetDfData(task, ptrs, numBins, mwatts, terminatedData);

		if (blockState.withTerm)
		{
			scanDfTask->SetTerminatedData(terminatedData, task->GetStep(), task->GetBlock());
		}
		m_digitizer->m_sdma.DoneWithSamples(blockState.sampleCount);
	}
	else
	{
		printf("DoScanDf: No master data found\n");
		failed = true;		// No master data found
	}

	bool partialBlock = false;
	if (!failed)
	{
		std::pair<int, int> suppressed = { 0, 0 };
		for (size_t ant = 0; ant < mwatts.size(); ++ant)
		{
			// Apply EDR, if enabled, and do bandlimiting
			if (m_config->HasEnhancedDynamicRange())
			{
				auto& terminatedData = scanDfTask->GetTerminatedData(task->GetStep(), task->GetBlock());
				if (terminatedData.size() == numBins)
				{
					mwatts[ant] -= terminatedData;
					ne10sThreshold_LTVal(mwatts[ant], SPUR_CAL_NOISE_FLOOR_FACTOR * procParams.GetBinSize().Hz<float>(), m_noise[ant]->GetNoise());
				}
				else
				{
					printf("terminatedData size mismatch %u %lu\n", terminatedData.size(), numBins);
				}
			}

			// Band limit
			suppressed = BandLimit(mwatts[ant],
				Units::FreqPair(tunedFreq - task->GetProcBw() / 2, tunedFreq + task->GetProcBw() / 2), blockState.freqLimits);
		}
		unsigned long binsPerChan = procParams.GetNumBins(taskParams.bw);
		suppressed.first = suppressed.first / int(binsPerChan) * int(binsPerChan);
		suppressed.second = suppressed.second / int(binsPerChan) * int(binsPerChan);
		TRACE("CProcessing::DoScanDf tunedFreq=%lf procbw=%lf blockState<%lf,%lf>, noExcudes =%d\n",
				tunedFreq.Hz<double>(), task->GetProcBw().Hz<double>(),
				blockState.freqLimits.first.Hz<double>(), blockState.freqLimits.second.Hz<double>(),
				(scanDfTask->NoExcludes() ? 1 :0));

		if (task->AtStepEnd())
		{
			partialBlock = blockState.freqLimits.second < tunedFreq + task->GetProcBw() / 2 &&
				blockState.freqLimits.second < taskParams.stopFreq;

			// Last step - truncate numBins
			unsigned long truncated = procParams.GetNumBins(taskParams.stopFreq - tunedFreq + task->GetProcBw() / 2);

			if (suppressed.second > 0)	// removing bins at end
			{
				numBins = std::min(static_cast<unsigned long>(numBins - suppressed.second), truncated);
			}
			else
			{
				numBins = truncated;
			}
			TRACE("CProcessing::DoScanDf: step end. suppressed = %d, %d, truncated = %lu, numBins = %lu, partialBlock = %d\n",
				suppressed.first, suppressed.second, truncated, numBins, partialBlock);
		}
		else if (suppressed.second > 0)		// removing bins at end
		{
			numBins = numBins - static_cast<unsigned long>(suppressed.second);
			TRACE("CProcessing::DoScanDf: not step end. suppressed = %d, %d, numBins = %lu\n", suppressed.first, suppressed.second, numBins);
		}

#ifdef CSMS_DEBUG
		TRACE("CProcessing::DoScanDf: m_refVolts %u, numBins %lu\n", m_refVolts[0].size(), numBins);
		//OutVoltages(1614, 1619);
#endif
		if (m_refVolts[0].size() != numBins)
		{
			TRACE("CProcessing::DoScanDf: resizing volts from %u,%u  to %lu\n", m_refVolts[0].size(), m_sampleVolts[0].size(), numBins);
			for (size_t ant = 0; ant < m_refVolts.size(); ++ant)
			{
				m_refVolts[ant].resize(numBins);
				m_sampleVolts[ant].resize(numBins);

				if(mwatts[ant].size() > numBins)
				{
					mwatts[ant].resize(numBins);
				}
			}
		}
		unsigned long numChan = numBins / binsPerChan;
		Units::Frequency firstChanFreq = task->GetFirstChanFreq();

		// Amplitude stuff
		static std::vector<size_t> validBins;
		validBins.resize(0);
#ifdef CSMS_DEBUG
		float maxNoise, maxWatt, minNoise, minWatt, sumNoise, sumWatt;
#endif
		if (scanDfTask->NoExcludes())
		{
			for (size_t bin = suppressed.first > 0 ? suppressed.first : 0; bin < numBins; ++bin)
			{
				if (binsPerChan < 3 || (bin % binsPerChan != 0 && bin % binsPerChan != binsPerChan - 1))
				{
					// Not a guard bin
					size_t ant;

					for (ant = 0; ant < numAnts; ++ant)
					{
#ifdef CSMS_DEBUG
						float noiseWatt = m_noise[ant]->GetNoise(bin);
						if(ant == 0 && (bin == 0 || int(bin) == suppressed.first))
						{
							sumWatt = maxWatt = minWatt = mwatts[0][bin];
							sumNoise = maxNoise = minNoise = noiseWatt;
						}
						else
						{
							sumWatt += mwatts[ant][bin];
							sumNoise += noiseWatt;
							if(minWatt > mwatts[ant][bin])
							{
								minWatt = mwatts[ant][bin];
							}
							else if(maxWatt < mwatts[ant][bin])
							{
								maxWatt = mwatts[ant][bin];
								TRACE("CProcessing::DoScanDf maxWatt assigned %f, ant=%u bin=%u\n",
										maxWatt, ant, bin);
							}

							if(minNoise > noiseWatt)
							{
								minNoise = noiseWatt;
							}
							else if(maxNoise < noiseWatt)
							{
								maxNoise = noiseWatt;
							}
						}
#endif

						// <= vs. old "<" so that a 0 value in mwatts & GetNoise will break.
						if (mwatts[ant][bin] <= m_noise[ant]->GetNoise(bin) * scanDfTask->m_snr)	// using noise riding threshold
						{
							break;
						}
					}

					if (ant == numAnts)
					{
						validBins.push_back(bin);
						TRACE("CProcessing::DoScanDf NoExcludes validBins mwatts[5]=%f, minWatt %f, maxWatt %f, sumWatt %f, m_noise[5]=%f, minNoise %f, maxNoise %f, sumNoise %f, m_snr=%f bin=%u\n",
								mwatts[5][bin], minWatt, maxWatt, sumWatt,
								m_noise[5]->GetNoise(bin), minNoise, maxNoise, sumNoise,
								scanDfTask->m_snr, bin);
					}
				}
			}
		}
		else
		{
			for (size_t bin = suppressed.first > 0 ? suppressed.first : 0; bin < numBins; ++bin)
			{
				if (!scanDfTask->Excluded(firstChanFreq - taskParams.bw / 2 + bin * taskParams.bw / binsPerChan, taskParams.bw, taskParams.pol) &&
					(binsPerChan < 3 || (bin % binsPerChan != 0 && bin % binsPerChan != binsPerChan - 1)))
				{
					// Not a guard bin
					size_t ant;

					for (ant = 0; ant < numAnts; ++ant)
					{
						// <= vs. old "<" so that a 0 value in mwatts & GetNoise will break.
						if (mwatts[ant][bin] <= m_noise[ant]->GetNoise(bin) * scanDfTask->m_snr)
						{
							break;
						}
					}

					if (ant == numAnts)
					{
						validBins.push_back(bin);
						TRACE("CProcessing::DoScanDf validBins mwatts[0]=%f m_noise[0]=%f m_snr=%f bin=%u\n",
								mwatts[0][bin], m_noise[0]->GetNoise(bin), scanDfTask->m_snr, bin);
					}
				}
			}
		}

		// Average the reference power over the elements
		for (size_t ant = 1; ant < numAnts; ++ant)
		{
			mwatts[0] += mwatts[ant];
		}

		mwatts[0] /= float(numAnts);

		// Do the DF
		bool horiz = (taskParams.pol == CConfig::EPolarization::HORIZ) ||
			(taskParams.pol == CConfig::EPolarization::BOTH && task->GetVHCurrentPolarization() == CConfig::EPolarization::HORIZ);
		CAntenna::EDfAlgorithm alg = CAntenna::DfAlgorithm(
			taskParams.hf ? mutableConfig.hfConfig.antCable.antenna.get() : mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get(),
			tunedFreq, horiz);

		size_t band = task->GetStep();
		size_t block = task->GetBlock();
#ifdef CSMS_DEBUG
		auto azVec = scanDfTask->GetPrevAz(band, block);
		CDf df(m_refVolts, m_sampleVolts, validBins, firstChanFreq, taskParams.bw, binsPerChan, alg,
				azVec);
		TRACE("CProcessing::DoScanDf(%u, %u): m_refVolts[%u], m_sampleVolts[%u]; validBins[%u], freq = %lfMHz, Bw %f,binsPerChan %lu, alg %d, PrevAz[%u]\n",
				band, block, m_refVolts.size(), m_sampleVolts.size(), validBins.size(), firstChanFreq.Hz<double>()/1000000,
				taskParams.bw.Hz<float>()/1000000, binsPerChan, int(alg), azVec.size());
		// OutVoltages(1614, 1619);
#else
		CDf df(m_refVolts, m_sampleVolts, validBins, firstChanFreq, taskParams.bw, binsPerChan, alg,
			scanDfTask->GetPrevAz(band, block));
#endif
		if (m_debug)
		{
			m_debug = false;
			auto& az = df.GetUncorrectedAz();
			auto& cf = df.GetConf();
			for (size_t i = 0; i < az.size(); ++i)
			{
				if (i < cf.size() && cf[i] >= 0)
				{
					printf("%u az,cf: %f %f\n", i, az[i], cf[i]);
				}
			}
			for (size_t k = 0; k < validBins.size(); ++k)
			{
				size_t i = validBins[k];
				if (i < m_refVolts[0].size() && i < m_sampleVolts[0].size())
				{
					printf("%u ref:", i);
					for (size_t j = 0; j < m_refVolts.size(); ++j)
					{
						printf(" %.9f %.9f", m_refVolts[j].real(i), m_refVolts[j].imag(i));
					}
					printf("\n");
					printf("%u smp:", i);
					for (size_t j = 0; j < m_sampleVolts.size(); ++j)
					{
						printf(" %.9f %.9f", m_sampleVolts[j].real(i), m_sampleVolts[j].imag(i));
					}
					printf("\n");
					Ne10F32cVec v(9);
					Ne10F32cVec ref(9);
					for (size_t ant = 0; ant < m_sampleVolts.size(); ++ant)
					{
						v[ant] = { m_sampleVolts[ant].real(i), m_sampleVolts[ant].imag(i) };
						ref[ant] = { m_refVolts[ant].real(i), m_refVolts[ant].imag(i) };
					}
					v /= ref;
					Ne10F32Vec mag;
					Ne10F32Vec phas;
					ne10sCartToPolar(v, mag, phas);
					printf("%u mag.phas:", i);
					for (size_t j = 0; j < m_sampleVolts.size(); ++j)
					{
						printf(" %.4f %.4f", mag[j], phas[j] * Units::R2D);
					}
					printf("\n");
				}
			}
		}

		unsigned long offset = (suppressed.first > 0 ? suppressed.first / binsPerChan : 0);
		scanDfTask->SaveCurrentAz(band, block, df.GetUncorrectedAz(), df.GetConf(), offset, numChan);

		// Get the power per channel
		static Ne10F32Vec chanWatts;
		chanWatts.resize(numChan);

		for (unsigned int chan = 0; chan < numChan; ++chan)
		{
//			ne10sMax(&mwatts[0][chan * binsPerChan + 1], binsPerChan - 2, &chanWatts[chan]); // Exclude guard bins
			ne10sSum(&mwatts[0][chan * binsPerChan + 1], binsPerChan - 2, &chanWatts[chan]); // Exclude guard bins
		}
		float enbw = CFft::GetEnbw(procParams.sampleSize);
		chanWatts /= enbw;	// SS 05/04/2017

		if (scanDfTask->m_reportInterval > 0)
		{
			// Accumulate the data
			scanDfTask->AddBlock(band, block, offset, firstChanFreq, df.GetUncorrectedAz(), df.GetEl(), df.GetConf(),
				chanWatts, blockState.switchTemp);
		}

		// Send variable length realtime DF messages
#ifdef CSMS_DEBUG
		timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		TRACE("DoScanDf(sec = %ld nsec =%ld): sendRealtime offset = %lu, size = %u, firstChanFreq =%f\n",
				ts.tv_sec, ts.tv_nsec, offset, chanWatts.size(), firstChanFreq.Hz<float>()/1000000);
#endif
		SendRealtimeDfMsg(task, chanWatts, df, alg == CAntenna::EDfAlgorithm::SYMMETRY_HORIZ, offset);
	}

	// Send reports
	if(task->AtEnd() && !partialBlock && (task->ReportNow() || task->Completed()))
	{
		// Send task state
		SEquipCtrlMsg resp;
		resp.hdr = task->m_msgHdr;
		resp.hdr.msgSubType = SEquipCtrlMsg::OCCUPANCY_STATE_RESP;
		resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SStateResp);
		resp.body.stateResp = CSuspendableTask::GetState(task->m_taskIdKey);
		CEquipControl::Send(task->m_source, resp, _T("OCCUPANCY_STATE_RESP"));

		if (scanDfTask->m_reportInterval > 0)
		{
			// Report via EquipCtrlMsg
			scanDfTask->SendReports(task->Completed());
		}
	}

	// Save restart data
	if (task->AtEnd() && !partialBlock && task->ReportNow() && scanDfTask->m_source == nullptr)
	{
		SaveRestartData(scanDfTask);
	}

	// Are we done?
//	if (!partialBlock && ((task->AtEnd() && task->Completed()) || task->Terminated()))
	if (!partialBlock && task->AtEnd() && task->Completed())	// SS 05/26/2017
	{
		// Send task completed status
		SEquipCtrlMsg resp;
		resp.hdr = task->m_msgHdr;
		resp.hdr.msgSubType = SEquipCtrlMsg::TASK_STATUS;
		resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SEquipTaskStatusResp);
		resp.body.equipTaskStatusResp.dateTime = Utility::CurrentTimeAsDATE();
		resp.body.equipTaskStatusResp.key = task->m_taskIdKey.key;
		resp.body.equipTaskStatusResp.status = (task->Terminated() ?
			SEquipCtrlMsg::SEquipTaskStatusResp::TERMINATED : SEquipCtrlMsg::SEquipTaskStatusResp::COMPLETED);
		resp.body.equipTaskStatusResp.taskId = task->m_taskIdKey.taskId;
		CEquipControl::Send(task->m_source, resp, _T("TASK_STATUS"));

		// Send realtime DF stop message
		SSmsRealtimeMsg realtimeMsg;
		realtimeMsg.hdr.msgType = SSmsRealtimeMsg::RT_DF_STOP;
		realtimeMsg.hdr.msgTypeVersion = 1;
		realtimeMsg.hdr.bodySize = sizeof(SSmsRealtimeMsg::SStop);
		realtimeMsg.body.stop.taskId = task->m_taskIdKey.taskId;
		m_realtimeNet.SendToAllClients(&realtimeMsg);

		// Delete the parent task
		TRACE("DoScanDf: deleting task\n");
		try
		{
			dynamic_cast<CHwControl&>(*this).DeleteTask(task->m_taskIdKey);
		}
		catch(const std::bad_cast&)
		{
			TRACE("bad cast to CHwControl\n");
		}
		CTask::DeleteTask(scanDfTask->m_taskIdKey);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Find the (master) data and the locations of the data blocks
//
bool CProcessing::FindDataBlocks(const CTask::Task& task, unsigned int numAnts, SDataPointers& ptrs, unsigned int timeout)
{
	const CConfig::SProcParams& procParams = task->GetProcParams();
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CTask::SBlockState& blockState = task->GetBlockState();

	unsigned long ddrCount = CHwControl::CalcDdrCount(procParams.sampleSize, taskParams);
	unsigned long spaceNeeded = numAnts * ddrCount;

	if (blockState.withTerm)
	{
		spaceNeeded += ddrCount;
	}

#ifdef CSMS_DEBUG
	if(s_traceAvd)
	{
		static size_t l_withTermCnt = 0;

		if(blockState.withTerm)
		{
			++l_withTermCnt;
			//C3230::IntrOutTrace();
		}

		timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		Units::Frequency rfFreq = task->GetTuneFreq();
		TRACE("CProcessing::FindDataBlocks: sec = %ld nsec =%ld; numAnts %u, count %lu, ddr = %lu, spaceNeeded = %lu, rfFreq = %lfMHz, sampleSize %lu,rxHwBw %f,withTerm %d, termCnt %u\n",
				ts.tv_sec, ts.tv_nsec, numAnts, blockState.sampleCount, ddrCount, spaceNeeded, rfFreq.Hz<double>()/1000000,
				procParams.sampleSize, taskParams.rxHwBw.Hz<float>()/1000000, (blockState.withTerm ? 1 : 0), l_withTermCnt);
 #ifdef CSMS_2016
		if(s_traceAvd && C3230::ScratchRegsValid())
		{
			C3230::ScratchRegsOutTrace();
		}
 #endif
	}
#endif

	uncached_ptr found;
	auto foundData = m_digitizer->m_sdma.FindSamples(blockState.sampleCount, spaceNeeded, found, timeout);
	if (foundData && found && (numAnts > 0))
	{
		ptrs.data.resize(numAnts);
		for (size_t i = 0; i < numAnts; ++i)
		{
			if (taskParams.withIq)
			{
				ptrs.data[i].iq = found;		// address of iq data block
				found += procParams.sampleSize;
			}
			if (taskParams.withPsd)
			{
				ptrs.data[i].psd = found;	// address of psd data block
				found += procParams.sampleSize;
			}
			if (taskParams.withFft)
			{
				ptrs.data[i].fftI = found;	// address of fftI data block
				found += procParams.sampleSize;
				ptrs.data[i].fftQ = found;	// address of fftQ data block
				found += procParams.sampleSize;
			}
		}
		// Now, found = address of term data
		if (blockState.withTerm)
		{
			if (taskParams.withIq)
			{
				ptrs.term.iq = found;
				found += procParams.sampleSize;
			}
			if (taskParams.withPsd)
			{
				ptrs.term.psd = found;	// address of term data, after all data blocks
				found += procParams.sampleSize;
			}
			if (taskParams.withFft)
			{
				ptrs.term.fftI = found;	// address of first fftI data block
				found += procParams.sampleSize;
				ptrs.term.fftQ = found;	// address of first fftQ data block
				found += procParams.sampleSize;
			}
		}
		return true;
	}
#ifdef CSMS_DEBUG
	else
	{
		TRACE("CProcessing::FindDataBlocks: Not Found.\n");
		ASSERT(false);
	}
#endif

	ptrs.data.clear();
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Flush master and slave data
//
void CProcessing::FlushData(bool needSlave, const CTask::SBlockState& blockState)
{
	// Flush master data
	m_digitizer->m_sdma.DoneWithSamples(blockState.sampleCount);

	if (needSlave)
	{
		std::vector<Ne10F32Vec> slaveWatts;
		MsgMapValue slaveData;
		if (blockState.slaveBufferKey != 0)
		{
			if (WaitForSlaves(blockState.slaveSentIndex, blockState.slaveBufferKey, slaveData))
			{
				if (slaveData) slaveWatts = std::move(slaveData->watts);
			}
		}
		if (blockState.slaveTermBufferKey != 0)
		{
			if (WaitForSlaves(blockState.slaveTermSentIndex, blockState.slaveTermBufferKey, slaveData))
			{
				if (slaveData) slaveWatts = std::move(slaveData->watts);
			}
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get gain-corrected power spectrum
//
void CProcessing::GetCorrectedSpectrum(const CTask::Task& task, int numBins, const uncached_ptr& psdData, Ne10F32Vec& watts, bool usedShf) const
{
	unsigned long sampleSize = task->GetProcParams().sampleSize;

	float gainAdj;
	Ne10F32Vec adjPower;
	GetCorrections(task, numBins, gainAdj, adjPower, usedShf);

	CFft::GetPowerSpectrumWatts2(numBins, sampleSize, psdData, gainAdj, watts);
	if (m_config->HasGainEqualization())
	{
		if (watts.size() != 0)
		{
			watts /= adjPower;
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get gain corrections
//
void CProcessing::GetCorrections(const CTask::Task& task, int numBins, float& gainAdj, Ne10F32Vec& adjPower, bool usedShf) const
{
	const CConfig::SProcParams& procParams = task->GetProcParams();
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CTask::SBlockState& blockState = task->GetBlockState();

//	printf("GetCorrections: rxGain,rfGainTableIdx,ifGainTableIdx,gainAdjTemp,gainAdj %f %u %ld %f %f\n",
//		blockState.rxGain, blockState.rfGainTableIdx, blockState.ifGainTableIdx, blockState.gainAdjTemp, blockState.gainAdj);

	gainAdj = blockState.gainAdj;
	Units::Frequency rxFreq = (usedShf ? m_config->GetShfExtIfFreq() : task->GetTuneFreq());

	if (m_config->HasGainEqualization())
	{
		// Remove radio portion of gain, leaving temp correction in
		gainAdj *= (blockState.rxGain / blockState.gainAdjTemp);
		adjPower.resize(numBins);
		m_radioEquip->GetCorrection(rxFreq, blockState.radioFreq, taskParams.rxHwBw, blockState.rfGainTableIdx,
			blockState.ifGainTableIdx, procParams.GetBinSize(), taskParams.hf, blockState.rcvrAtten, adjPower);
	}
	else
	{
		// Correct gain for center of actual data used
		Units::Frequency midFrequency = BandMidFrequency(Units::FreqPair(rxFreq - task->GetProcBw() / 2, rxFreq + task->GetProcBw() / 2),
			blockState.freqLimits);
		if (midFrequency != 0)
		{
			auto adj = m_radioEquip->GetCorrection(midFrequency, blockState.radioFreq, blockState.rfGainTableIdx,
				blockState.ifGainTableIdx, taskParams.hf, blockState.rcvrAtten);
			float midCorrection = blockState.rxGain / (blockState.gainAdjTemp * adj);
			if (fabsf(midCorrection - 1.0f) > 1.e-6)
			{
				gainAdj *= midCorrection;
			}
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get data from master or slave, not both, gain corrected but not EDR'd - multiple variants
//
void CProcessing::GetData(const CTask::Task& task, bool needSlave, const SDataPointers& ptrs, int numBins, Ne10F32Vec& watts, bool usedShf) const
{
	const CTask::SBlockState& blockState = task->GetBlockState();
	if (needSlave)
	{
		// Must get the real data before getting the term data since that's how they were collected and put into the queue
		std::vector<Ne10F32Vec> slaveWatts;
		GetSlaveWatts(blockState.slaveBufferKey, blockState.slaveSentIndex, slaveWatts);
		if (!slaveWatts.empty()) watts = std::move(slaveWatts[0]);
	}
	else
	{
		if (ptrs.data[0].psd)
		{
			GetCorrectedSpectrum(task, numBins, ptrs.data[0].psd, watts, usedShf);
		}
	}
	return;
}

void CProcessing::GetData(const CTask::Task& task, bool needSlave, const SDataPointers& ptrs, int numBins, Ne10F32Vec& watts, Ne10F32Vec& terminatedData, bool usedShf) const
{
	const CTask::SBlockState& blockState = task->GetBlockState();
	if (needSlave)
	{
		// Must get the real data before getting the term data since that's how they were collected and put into the queue
		std::vector<Ne10F32Vec> slaveWatts;
		GetSlaveWatts(blockState.slaveBufferKey, blockState.slaveSentIndex, slaveWatts);
//		TRACE("CProcessing::GetData: GetSlaveWatts %lu %u %u\n", blockState.slaveBufferKey, slaveWatts.size(), slaveWatts.empty() ? 0 : slaveWatts[0].size());
		if (!slaveWatts.empty()) watts = std::move(slaveWatts[0]);
		if (blockState.withTerm)
		{
			GetSlaveWatts(blockState.slaveTermBufferKey, blockState.slaveTermSentIndex, slaveWatts);
//			TRACE("CProcessing::GetData: GetSlaveWatts term %lu %u %u\n", blockState.slaveTermBufferKey, slaveWatts.size(), slaveWatts.empty() ? 0 : slaveWatts[0].size());
			if (!slaveWatts.empty()) terminatedData = std::move(slaveWatts[0]);
		}
	}
	else
	{
		if (ptrs.data[0].psd)
		{
			GetCorrectedSpectrum(task, numBins, ptrs.data[0].psd, watts, usedShf);
		}
		if (blockState.withTerm)
		{
			if (ptrs.term.psd)
			{
				GetCorrectedSpectrum(task, numBins, ptrs.term.psd, terminatedData, usedShf);
			}
		}
	}
	return;
}

void CProcessing::GetData(const CTask::Task& task, bool needSlave, const SDataPointers& ptrs, int numBins, Ne10F32Vec& watts, Ne10F32cVec& samples, Ne10F32Vec& terminatedData, bool usedShf) const
{
	auto sampleSize = task->GetProcParams().sampleSize;
	const CTask::SBlockState& blockState = task->GetBlockState();
	if (needSlave)
	{
		// Must get the real data before getting the term data since that's how they were collected and put into the queue
		std::vector<Ne10F32Vec> slaveWatts;
		std::vector< std::vector<unsigned long> > slaveIq;
		GetSlaveData(blockState.slaveBufferKey, blockState.slaveSentIndex, slaveWatts, slaveIq);
		if (!slaveIq.empty() && !slaveIq[0].empty())
		{
			samples.resize(sampleSize);
			ne10sConvert(reinterpret_cast<const ne10_int16_t*>(&slaveIq[0][0]),
				reinterpret_cast<ne10_float32_t*>(&samples[0]), 2 * sampleSize, 15);
		}
		if (!slaveWatts.empty()) watts = std::move(slaveWatts[0]);
		if (blockState.withTerm)
		{
			GetSlaveWatts(blockState.slaveTermBufferKey, blockState.slaveTermSentIndex, slaveWatts);
			if (!slaveWatts.empty()) terminatedData = std::move(slaveWatts[0]);
		}
	}
	else
	{
		if (ptrs.data[0].iq)
		{
			std::vector<unsigned long> samples1(sampleSize);	// make a temporary copy
			ne10sCopyUncached(const_cast<const unsigned long*>(ptrs.data[0].iq.get()), &samples1[0], sampleSize);
			samples.resize(sampleSize);
			ne10sConvert(reinterpret_cast<const ne10_int16_t*>(&samples1[0]),
				reinterpret_cast<ne10_float32_t*>(&samples[0]), 2 * sampleSize, 15);
		}
		if (ptrs.data[0].psd)
		{
			GetCorrectedSpectrum(task, numBins, ptrs.data[0].psd, watts, usedShf);
		}
		if (blockState.withTerm)
		{
			if (ptrs.term.psd)
			{
				GetCorrectedSpectrum(task, numBins, ptrs.term.psd, terminatedData, usedShf);
			}
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get data from master and slave, gain corrected but not EDR'd - outputs also in m_refVolts, m_sampleVolts, m_dfWatts, m_noise
//
bool CProcessing::GetDfData(const CTask::Task& task, const SDataPointers& ptrs, unsigned long numBins, std::vector<Ne10F32Vec>& wattsBins, Ne10F32Vec& terminatedData)
{
	const CConfig::SProcParams& procParams = task->GetProcParams();
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CTask::SBlockState& blockState = task->GetBlockState();
	auto mutableConfig = m_config->GetMutableConfig();

	unsigned int numAnts = CAntenna::NumAnts(
		taskParams.hf ? mutableConfig.hfConfig.antCable.antenna.get() : mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get(),
		task->GetTuneFreq());

	if (blockState.withTerm)
	{
		// For DF, only the master ever has terminated data
//		TRACE("Processing::GetDfData: has terminated data\n");
		if (ptrs.term.psd)
		{
			GetCorrectedSpectrum(task, numBins, ptrs.term.psd, terminatedData, false);
		}
		else
		{
			printf("CProcessing::GetDfData no terminated data found to process!\n");
		}
		// Finish processing terminated data  --  TODO: add this to the GetData() method also.
		if (terminatedData.size() > 0)
		{
			ne10sThreshold_LTVal(terminatedData,
				SPUR_CAL_THRESHOLD * CNoiseEstimate::Create(terminatedData, numBins, 0)->GetNoise(), 0);
			terminatedData *= TWO_DB;  // Raise nonzero values by 2 dB
		}
		// terminated data ready to be used
	}

	m_noise.resize(numAnts);
	m_refVolts.resize(numAnts);
	m_sampleVolts.resize(numAnts);
	wattsBins.resize(numAnts);
	m_dfWatts.resize(numAnts);
	bool failed = false;

	// Get the slave data - TODO: could delay this until after handling reference data to give it more time.
	bool slaveDataOk = false;
	MsgMapValue slaveData;
	if (blockState.slaveBufferKey != 0)
	{
		slaveDataOk = WaitForSlaves(blockState.slaveSentIndex, blockState.slaveBufferKey, slaveData);
		if (!slaveDataOk)
		{
			printf("CProcessing::GetDfData: WaitForSlaves failed\n");
		}
	}
	if (slaveDataOk && slaveData)
	{
		unsigned long numIfBins = std::max(numBins, static_cast<unsigned long>(procParams.sampleSize * 0.8));		// Get larger of numIfBins, numDfBins
		//	Get gain Equalization if wanted - power only?? - independent of subblock
		Ne10F32Vec adjPower;
		float gainAdj;
		GetCorrections(task, numIfBins, gainAdj, adjPower, false);

		for (size_t subBlock = 0; subBlock < numAnts; ++subBlock)	// loop over subblocks (antennas)
		{
			if (!ptrs.data[subBlock].psd || !ptrs.data[subBlock].fftI || !ptrs.data[subBlock].fftQ)
			{
				failed = true;
				break;
			}

			// Find antenna number from the subblock
			size_t ant = CConfig::GetAntFromSubBlock(subBlock, task->m_ant, taskParams.hf);

			CFft::GetVoltages2(numBins, procParams.sampleSize, ptrs.data[subBlock].fftI, ptrs.data[subBlock].fftQ,
				blockState.gainAdj, m_refVolts[ant]);	// Use uncorrected gainAdj. TODO: use gain equalization if enabled?

			// Get the noise in max(DF,IF) bandwidth (reference) - use gain equalized data
			CFft::GetPowerSpectrumWatts2(numIfBins, procParams.sampleSize, ptrs.data[subBlock].psd,
				gainAdj, wattsBins[ant]);
			//	Apply Gain Equalization if wanted - power and complex fft?
			if (m_config->HasGainEqualization())
			{
				if (adjPower.size() == wattsBins[ant].size())
				{
					wattsBins[ant] /= adjPower;
				}
				else
				{
					printf("GetDfData: adjPower size error\n");
				}
			}
			m_noise[ant] = CNoiseEstimate::Create(wattsBins[ant], numIfBins, 0);

			// numIfBins >= numBins, guaranteed.
			// Just extract the df bins from the spectrum above
			auto rot = numIfBins / 2 - numBins / 2;
			if (rot > 0)
			{
				ne10sMove(&wattsBins[ant][rot], &wattsBins[ant][0], numBins);
			}
			wattsBins[ant].resize(numBins);
			// wattsBins[ant] contains gain adjusted and equalized reference power spectrum

		}	// end of loop over numAnts

		// Move sample data to where it is needed
		m_sampleVolts = std::move(slaveData->volts);
		m_dfWatts = std::move(slaveData->watts);
	}
	else	// getting slave data failed
	{
		failed = true;
		printf("CProcessing::GetDfData: slave data not available\n");
	}
	return failed;
}


//////////////////////////////////////////////////////////////////////
//
// Get slave data from response queue
//
void CProcessing::GetSlaveData(unsigned long bufferKey, const std::vector<size_t>& sentIndex, std::vector<Ne10F32Vec>& slaveWatts,
	std::vector< std::vector<unsigned long> >& slaveIq) const
{
	slaveIq.clear();
	slaveWatts.clear();
	bool slaveDataOk = false;
	MsgMapValue slaveData;
	if (bufferKey != 0)
	{
//		printf("CProcessing::GetSlaveData getting data with bufferKey %lu\n", bufferKey);
		slaveDataOk = WaitForSlaves(sentIndex, bufferKey, slaveData);
		if (!slaveDataOk)
		{
			TRACE("CProcessing::GetSlaveData: WaitForSlaves failed\n");
		}
	}
	else
		printf("buffery in GetSlaveData empty!\n");
	if (slaveDataOk && slaveData)
	{
		slaveWatts = std::move(slaveData->watts);
		slaveIq = std::move(slaveData->iq);
	}
	return;
}

void CProcessing::GetSlaveWatts(unsigned long bufferKey, const std::vector<size_t>& sentIndex, std::vector<Ne10F32Vec>& slaveWatts) const
{
	slaveWatts.clear();
	bool slaveDataOk = false;
	MsgMapValue slaveData;
	if (bufferKey != 0)
	{
//		printf("CProcessing::GetSlaveWatts getting data with bufferKey %lu\n", bufferKey);
		slaveDataOk = WaitForSlaves(sentIndex, bufferKey, slaveData);
		if (!slaveDataOk)
		{
			TRACE("CProcessing::GetSlaveWatts: WaitForSlaves failed\n");
		}
	}
	if (slaveDataOk && slaveData)
	{
		slaveWatts = std::move(slaveData->watts);
	}
	return;
}

#ifdef CSMS_DEBUG
//////////////////////////////////////////////////////////////////////
//
// Print the values of m_refVolts & m_sampleVolts.
//
void CProcessing::OutVoltages(size_t firstBin, size_t lastBin) const
{
	ASSERT(firstBin < m_refVolts[0].size() &&  lastBin < m_refVolts[0].size() );

	for (size_t k = 0; k < m_refVolts.size(); ++k)
	{
		TRACE("m_refVolts[%u][%u - %u]:\n", k, firstBin, lastBin);
		for (size_t i = firstBin; i <= lastBin; ++i)
		{
			TRACE(" %.9f %.9f", m_refVolts[k].real(i), m_refVolts[k].imag(i));
		}
		TRACE("\nm_sampleVolts[%u][%u - %u]:\n", k, firstBin, lastBin);
		for (size_t i = firstBin; i <= lastBin; ++i)
		{
			TRACE(" %.9f %.9f", m_sampleVolts[k].real(i), m_sampleVolts[k].imag(i));
		}
		TRACE("\n");
	}
}
#endif

//////////////////////////////////////////////////////////////////////
//
// Save restart data to database
//
void CProcessing::SaveRestartData(_In_ const CTask::Task& task)
{
	// Save restart data
	std::vector<unsigned char> restartData;
	restartData.reserve(65536); // 64 KB inital capacity
	task->SaveRestartData(restartData);
	unsigned long restartLen = restartData.size();
	SEquipCtrlMsg* msg = new(offsetof(SEquipCtrlMsg, body) + restartLen) SEquipCtrlMsg;

	msg->hdr = task->m_msgHdr;
	msg->hdr.msgType = SEquipCtrlMsg::METRICS_CTRL;
	msg->hdr.msgSubType = task->RestartDataType();
	msg->hdr.bodySize = restartLen;
	memcpy(reinterpret_cast<char*>(&msg->body), &restartData[0], restartLen);

	// Add to database
	try
	{
		CWeakSingleton<CPersistentData>()->AddMsg(CPersistentData::QueueMsg(msg));
	}
	catch(CWeakSingleton<CPersistentData>::NoInstance&)
	{
		// Shutting down
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send variable length realtime Df message
//
void CProcessing::SendRealtimeDfMsg(const CTask::Task& task, const Ne10F32Vec& chanWatts,
		const CDf& df, bool horiz, unsigned long offset)
{
	// Sends realtime df and realtime spectrum. Order depends on versions requested by client.
	// Special case: if DF_DATA version is 1, spectrum data is not sent (see CRealTimeNet::SendMsg)

	if (m_realtimeNet.GetNumConnections() == 0)
		return;

	CScanDfTask::ScanDfTask scanDfTask(std::dynamic_pointer_cast<CScanDfTask>(task->GetParent()));
	const CConfig::SProcParams& procParams = task->GetProcParams();
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	Units::Frequency firstChanFreq = task->GetFirstChanFreq();
	unsigned long numChan = chanWatts.size() - offset;

	// Convert watts to scaled dBm and efield
	static Ne10F32Vec chanDbm;
	std::vector<ne10_int32_t> iChanEField;
	ne10sLog10(chanWatts, chanDbm);
	chanDbm *= 10.0f;

	// Calculate efield factors if required
	if (m_realtimeNet.GetNumSpectrumClients(3) > 0)
	{
		Ne10F32Vec chanEField = chanDbm;
		chanEField += scanDfTask->GetFieldStrengthFactors(firstChanFreq, taskParams.bw, chanWatts.size(), task->GetStep());
		iChanEField.resize(chanEField.size());
		ne10sConvert(&chanEField[0], &iChanEField[0], chanEField.size(), -128, 126);	// round to int and clamp
	}
	chanDbm += 230.f;	// offset and dbw -> dbm (convert later on rounds)

	unsigned long binsPerChan = procParams.GetNumBins(taskParams.bw);

	// Calculate numUpChans
	unsigned long numUpChans = 0;
	if (scanDfTask->NoExcludes())
	{
		for (unsigned int chan = 0; chan < numChan; ++chan)
		{
			if (df.GetCorrectedAz(chan + offset, taskParams.hf, scanDfTask->m_conf) != 0xffff)
			{
				++numUpChans;
			}
		}
	}
	else
	{
		for (unsigned int chan = 0; chan < numChan; ++chan)
		{
			if (!scanDfTask->Excluded(firstChanFreq + chan * taskParams.bw, taskParams.bw, taskParams.pol))
			{
				if (df.GetCorrectedAz(chan + offset, taskParams.hf, scanDfTask->m_conf) != 0xffff)
				{
					++numUpChans;
				}
			}
		}
	}
	// RT_DF_DATA version 1
	if (m_realtimeNet.GetNumDfClients(1) > 0)
	{
		try
		{
			// AllocateMsgBuffer
			size_t length = offsetof(SSmsRealtimeMsg, body.dfData.chanData) + numChan * sizeof(SSmsRealtimeMsg::SDfData::SChanData);
			if (length > m_realtimeBuf.size())
			{
				m_realtimeBuf.resize(length);
			}
			auto realtimeMsg = reinterpret_cast<SSmsRealtimeMsg*>(&m_realtimeBuf[0]);
			realtimeMsg->hdr.msgType = SSmsRealtimeMsg::RT_DF_DATA;
			realtimeMsg->hdr.msgTypeVersion = 1;
			realtimeMsg->hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SDfData, chanData) + numChan * sizeof(SSmsRealtimeMsg::SDfData::SChanData));
			realtimeMsg->body.dfData.taskId = task->m_taskIdKey.taskId;
			realtimeMsg->body.dfData.bandIndex = static_cast<unsigned long>(task->GetStep());
			realtimeMsg->body.dfData.chanSize = taskParams.bw.Hz<unsigned long>();
			realtimeMsg->body.dfData.numChan = numChan;
			realtimeMsg->body.dfData.noiseFloor = static_cast<unsigned char>(10 * log10(m_noise[0]->GetNoise()) + 230.5f);
			TRACE("CProcessing::SendRealtimeDfMsg 1: bandIndex %lu, chanSize %lu, numChan = %lu, noiseFloor = %u\n",
					realtimeMsg->body.dfData.bandIndex, realtimeMsg->body.dfData.chanSize,
					numChan, unsigned(realtimeMsg->body.dfData.noiseFloor));

			if (scanDfTask->NoExcludes())
			{
				for (unsigned int chan = 0; chan < numChan; ++chan)
				{
					unsigned short azimData = df.GetCorrectedAz(chan + offset, taskParams.hf, scanDfTask->m_conf);
					realtimeMsg->body.dfData.chanData[chan].azimData = azimData;
					realtimeMsg->body.dfData.chanData[chan].specData = static_cast<unsigned char>(chanDbm[chan + offset]);
				}
			}
			else
			{
				for (unsigned int chan = 0; chan < numChan; ++chan)
				{
					if (scanDfTask->Excluded(firstChanFreq + chan * taskParams.bw, taskParams.bw, taskParams.pol))
					{
						realtimeMsg->body.dfData.chanData[chan].azimData = 0xffff;
						realtimeMsg->body.dfData.chanData[chan].specData = 0;
					}
					else
					{
						unsigned short azimData = df.GetCorrectedAz(chan + offset, taskParams.hf, scanDfTask->m_conf);
						realtimeMsg->body.dfData.chanData[chan].azimData = azimData;
						realtimeMsg->body.dfData.chanData[chan].specData = static_cast<unsigned char>(chanDbm[chan + offset]);
					}
				}
			}
			// Convert frequency last as it may throw an exception
			realtimeMsg->body.dfData.firstChanFreq = (firstChanFreq + offset * taskParams.bw).Hz<unsigned long>(true);
			m_realtimeNet.SendToAllClients(realtimeMsg);
		}
		catch(std::overflow_error&)
		{
			// Frequency out of 32-bit range so skip
		}
	}

	// RT_SPECTRUM_DATA version 1 - same as in SendRealtimeSpectrum except for m_noise, scanDfTask, offset
	if (m_realtimeNet.GetNumSpectrumClients(1) > 0)
	{
		try
		{
			// AllocateMsgBuffer
			size_t length = offsetof(SSmsRealtimeMsg, body.spectrum.chanData) + numChan * sizeof(unsigned char);
			if (length > m_realtimeBuf.size())
			{
				m_realtimeBuf.resize(length);
			}
			auto realtimeMsg = reinterpret_cast<SSmsRealtimeMsg*>(&m_realtimeBuf[0]);
			realtimeMsg->hdr.msgType = SSmsRealtimeMsg::RT_SPECTRUM;
			realtimeMsg->hdr.msgTypeVersion = 1;
			realtimeMsg->hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SSpectrum, chanData) + numChan * sizeof(unsigned char));
			realtimeMsg->body.spectrum.taskId = task->m_taskIdKey.taskId;
			realtimeMsg->body.spectrum.bandIndex = static_cast<unsigned long>(task->GetStep());
			realtimeMsg->body.spectrum.firstChanFreq = (firstChanFreq + offset * taskParams.bw).Hz<unsigned long>(true);
			realtimeMsg->body.spectrum.chanSize = taskParams.bw.Hz<unsigned long>();
			realtimeMsg->body.spectrum.numChan = numChan;
			realtimeMsg->body.spectrum.noiseFloor = static_cast<unsigned char>(10 * log10(m_noise[0]->GetNoise()) + 230.5f);

			ne10sConvert(&chanDbm[offset], realtimeMsg->body.spectrum.chanData, numChan, 0);
			if (!scanDfTask->NoExcludes())
			{
				for (unsigned int chan = 0; chan < numChan; ++chan)
				{
					if (scanDfTask->Excluded(firstChanFreq + (chan + offset) * taskParams.bw, taskParams.bw, taskParams.pol))
					{
						realtimeMsg->body.spectrum.chanData[chan] = 0;
					}
				}
			}
			m_realtimeNet.SendToAllClients(realtimeMsg);
		}
		catch(std::overflow_error&)
		{
			// Frequency out of 32-bit range so skip
		}
	}

	// RT_DF_DATA version 2
	if (m_realtimeNet.GetNumDfClients(2) > 0 && numUpChans > 0)
	{
		try
		{
			// AllocateMsgBuffer
			size_t length = offsetof(SSmsRealtimeMsg, body.dfDataV2.chanData) + numUpChans * sizeof(SSmsRealtimeMsg::SDfDataV2::SChanData);
			if (length > m_realtimeBuf.size())
			{
				m_realtimeBuf.resize(length);
			}
			auto realtimeMsg = reinterpret_cast<SSmsRealtimeMsg*>(&m_realtimeBuf[0]);
			realtimeMsg->hdr.msgType = SSmsRealtimeMsg::RT_DF_DATA;
			realtimeMsg->hdr.msgTypeVersion = 2;
			realtimeMsg->hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SDfDataV2, chanData) + numUpChans * sizeof(SSmsRealtimeMsg::SDfDataV2::SChanData));
			realtimeMsg->body.dfDataV2.taskId = task->m_taskIdKey.taskId;
			realtimeMsg->body.dfDataV2.bandIndex = static_cast<unsigned long>(task->GetStep());
			realtimeMsg->body.dfDataV2.firstChanFreq = (firstChanFreq + offset * taskParams.bw).Hz<unsigned long>(true);
			realtimeMsg->body.dfDataV2.chanSize = taskParams.bw.Hz<unsigned long>();
			realtimeMsg->body.dfDataV2.numChan = numUpChans;
			realtimeMsg->body.dfDataV2.noiseFloor = static_cast<unsigned char>(10 * log10(m_noise[0]->GetNoise()) + 230.5f);
			realtimeMsg->body.dfDataV2.horizPol = horiz;
			size_t upChan = 0;
			TRACE("CProcessing::SendRealtimeDfMsg 2: bandIndex %lu, chanSize %lu, numUpChan = %lu, noiseFloor = %u\n",
					realtimeMsg->body.dfDataV2.bandIndex, realtimeMsg->body.dfDataV2.chanSize,
					numUpChans, unsigned(realtimeMsg->body.dfDataV2.noiseFloor));

			for (unsigned int chan = 0; chan < numChan; ++chan)
			{
				unsigned short azimData = df.GetCorrectedAz(chan + offset, taskParams.hf, scanDfTask->m_conf);
				if (!scanDfTask->Excluded(firstChanFreq + (chan + offset) * taskParams.bw, taskParams.bw, taskParams.pol) && azimData != 0xffff)
				{
					realtimeMsg->body.dfDataV2.chanData[upChan].chan = static_cast<unsigned short>(chan);
					realtimeMsg->body.dfDataV2.chanData[upChan].azimData = azimData;
					realtimeMsg->body.dfDataV2.chanData[upChan].conf = static_cast<unsigned short>(1000 * df.GetConf()[chan + offset] + 0.5f);
					realtimeMsg->body.dfDataV2.chanData[upChan].specData = static_cast<unsigned char>(chanDbm[chan + offset]);

					// Get maximum power from a DF element
					float dfChanWatts;
					unsigned char dfSpecData = 0;
					size_t numAnts = m_dfWatts.size();
					for (size_t ant = 0; ant < numAnts; ++ant)
					{
						ne10sMax(&m_dfWatts[ant][(chan + offset) * binsPerChan + 1], binsPerChan - 2, &dfChanWatts); // Exclude guard bins
						unsigned char offsetDbm = static_cast<unsigned char>(10 * log10(dfChanWatts) + 230.5f);

						if (offsetDbm > dfSpecData)
						{
							dfSpecData = offsetDbm;
						}
					}

					realtimeMsg->body.dfDataV2.chanData[upChan++].dfSpecData = dfSpecData;
				}
			}

			m_realtimeNet.SendToAllClients(realtimeMsg);
		}
		catch(std::overflow_error&)
		{
			// Frequency out of 32-bit range so skip
		}
	}

	// RT_SPECTRUM_DATA version 2
	if (m_realtimeNet.GetNumSpectrumClients(2) > 0)
	{
		// AllocateMsgBuffer
		size_t length = offsetof(SSmsRealtimeMsg, body.spectrumV2.chanData) + numChan * sizeof(unsigned char);
		if (length > m_realtimeBuf.size())
		{
			m_realtimeBuf.resize(length);
		}
		auto realtimeMsg = reinterpret_cast<SSmsRealtimeMsg*>(&m_realtimeBuf[0]);
		realtimeMsg->hdr.msgType = SSmsRealtimeMsg::RT_SPECTRUM;
		realtimeMsg->hdr.msgTypeVersion = 2;
		realtimeMsg->hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SSpectrumV2, chanData) + numChan * sizeof(unsigned char));
		realtimeMsg->body.spectrumV2.taskId = task->m_taskIdKey.taskId;
		realtimeMsg->body.spectrumV2.bandIndex = static_cast<unsigned long>(task->GetStep());
		realtimeMsg->body.spectrumV2.firstChanFreq = (firstChanFreq + offset * taskParams.bw).GetRaw();
		realtimeMsg->body.spectrumV2.chanSize = taskParams.bw.GetRaw();
		realtimeMsg->body.spectrumV2.numChan = numChan;
		realtimeMsg->body.spectrumV2.noiseFloor = static_cast<unsigned char>(10 * log10(m_noise[0]->GetNoise()) + 230.5f);

		ne10sConvert(&chanDbm[offset], realtimeMsg->body.spectrumV2.chanData, numChan, 0);
		if (!scanDfTask->NoExcludes())
		{
			for (unsigned int chan = 0; chan < numChan; ++chan)
			{
				if (scanDfTask->Excluded(firstChanFreq + (chan + offset) * taskParams.bw, taskParams.bw, taskParams.pol))
				{
					realtimeMsg->body.spectrumV2.chanData[chan] = 0;
				}
			}
		}
		m_realtimeNet.SendToAllClients(realtimeMsg);
	}

	// RT_SPECTRUM_DATA version 3
	if (m_realtimeNet.GetNumSpectrumClients(3) > 0)
	{
		// AllocateMsgBuffer
		size_t length = offsetof(SSmsRealtimeMsg, body.spectrumV3.chanData) + numChan * (sizeof(unsigned char) + sizeof(signed char));
		if (length > m_realtimeBuf.size())
		{
			m_realtimeBuf.resize(length);
		}
		auto realtimeMsg = reinterpret_cast<SSmsRealtimeMsg*>(&m_realtimeBuf[0]);
		realtimeMsg->hdr.msgType = SSmsRealtimeMsg::RT_SPECTRUM;
		realtimeMsg->hdr.msgTypeVersion = 3;
		realtimeMsg->hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SSpectrumV3, chanData) + numChan * (sizeof(unsigned char) + sizeof(signed char)));
		realtimeMsg->body.spectrumV3.taskId = task->m_taskIdKey.taskId;
		realtimeMsg->body.spectrumV3.bandIndex = static_cast<unsigned long>(task->GetStep());
		realtimeMsg->body.spectrumV3.firstChanFreq = (firstChanFreq + offset * taskParams.bw).GetRaw();
		realtimeMsg->body.spectrumV3.chanSize = taskParams.bw.GetRaw();
		realtimeMsg->body.spectrumV3.zeroVal = 0;
		realtimeMsg->body.spectrumV3.numChan = numChan;
		realtimeMsg->body.spectrumV3.noiseFloor = static_cast<unsigned char>(10 * log10(m_noise[0]->GetNoise()) + 230.5f);

		auto eField = reinterpret_cast<signed char*>(&realtimeMsg->body.spectrumV3.chanData[numChan]);

		ne10sConvert(&chanDbm[offset], realtimeMsg->body.spectrumV3.chanData, numChan, 0);
		ne10sConvert(&iChanEField[offset], &eField[0], numChan);
		if (!scanDfTask->NoExcludes())
		{
			for (unsigned int chan = 0; chan < numChan; ++chan)
			{
				auto freq = firstChanFreq + (chan + offset) * taskParams.bw;
				if (scanDfTask->Excluded(freq, taskParams.bw, taskParams.pol))
				{
					realtimeMsg->body.spectrumV3.chanData[chan] = 0;
					eField[chan] = 0;
				}
			}
		}
		m_realtimeNet.SendToAllClients(realtimeMsg);
	}

	// RT_DF_DATA version 3
	if (m_realtimeNet.GetNumDfClients(3) > 0 && numUpChans > 0)
	{
		// AllocateMsgBuffer
		size_t length = offsetof(SSmsRealtimeMsg, body.dfDataV3.chanData) + numUpChans * sizeof(SSmsRealtimeMsg::SDfDataV3::SChanData);
		if (length > m_realtimeBuf.size())
		{
			m_realtimeBuf.resize(length);
		}
		auto realtimeMsg = reinterpret_cast<SSmsRealtimeMsg*>(&m_realtimeBuf[0]);
		realtimeMsg->hdr.msgType = SSmsRealtimeMsg::RT_DF_DATA;
		realtimeMsg->hdr.msgTypeVersion = 3;
		realtimeMsg->hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SDfDataV3, chanData) +
			numUpChans * sizeof(SSmsRealtimeMsg::SDfDataV3::SChanData));
		realtimeMsg->body.dfDataV3.taskId = task->m_taskIdKey.taskId;
		realtimeMsg->body.dfDataV3.bandIndex = static_cast<unsigned long>(task->GetStep());
		realtimeMsg->body.dfDataV3.firstChanFreq = (firstChanFreq + offset * taskParams.bw).GetRaw();
		realtimeMsg->body.dfDataV3.chanSize = taskParams.bw.GetRaw();
		realtimeMsg->body.dfDataV3.numChan = numUpChans;
		realtimeMsg->body.dfDataV3.noiseFloor = static_cast<unsigned char>(10 * log10(m_noise[0]->GetNoise()) + 230.5f);
		realtimeMsg->body.dfDataV3.horizPol = horiz;
		size_t upChan = 0;
		TRACE("CProcessing::SendRealtimeDfMsg 3: bandIndex %lu, chanSize %lu, numUpChan = %lu, noiseFloor = %u\n",
				realtimeMsg->body.dfDataV3.bandIndex, realtimeMsg->body.dfDataV3.chanSize,
				numUpChans, unsigned(realtimeMsg->body.dfDataV3.noiseFloor));

		bool noExcludes = scanDfTask->NoExcludes();
		for (unsigned int chan = 0; chan < numChan; ++chan)
		{
			unsigned short azimData = df.GetCorrectedAz(chan + offset, taskParams.hf, scanDfTask->m_conf);

			if ((noExcludes || !scanDfTask->Excluded(firstChanFreq + (chan + offset) * taskParams.bw, taskParams.bw, taskParams.pol)) && azimData != 0xffff)
			{
				realtimeMsg->body.dfDataV3.chanData[upChan].chan = static_cast<unsigned short>(chan);
				realtimeMsg->body.dfDataV3.chanData[upChan].azimData = azimData;
				realtimeMsg->body.dfDataV3.chanData[upChan].conf = static_cast<unsigned short>(1000 * df.GetConf()[chan + offset] + 0.5f);
				realtimeMsg->body.dfDataV3.chanData[upChan].specData = static_cast<unsigned char>(chanDbm[chan + offset]);

				// Get maximum power from a DF element
				float dfChanWatts;
				size_t numAnts = m_dfWatts.size();
				float maxDfChanWatts = 0;

				for (size_t ant = 0; ant < numAnts; ++ant)
				{
					ne10sMax(&m_dfWatts[ant][(chan + offset) * binsPerChan + 1], binsPerChan - 2, &dfChanWatts); // Exclude guard bins
					if (dfChanWatts > maxDfChanWatts)
					{
						maxDfChanWatts = dfChanWatts;
					}
				}
				realtimeMsg->body.dfDataV3.chanData[upChan++].dfSpecData = static_cast<unsigned char>(10 * log10(maxDfChanWatts) + 230.5f);
			}
		}

		m_realtimeNet.SendToAllClients(realtimeMsg);
	}
}


//////////////////////////////////////////////////////////////////////
//
// Send variable length realtime spectrum message
//
void CProcessing::SendRealtimeSpectrumMsg(const CTask::Task& task, const Ne10F32Vec& chanWatts)
{
	if (m_realtimeNet.GetNumConnections() == 0)
		return;

	CSuspendableTask::SuspendableTask suspTask(std::dynamic_pointer_cast<CSuspendableTask>(task->GetParent()));
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	Units::Frequency firstChanFreq = task->GetFirstChanFreq();
	unsigned long numChan = chanWatts.size();

	// Convert to dbm and efield to use in all versions
	static Ne10F32Vec chanDbm;
	std::vector<ne10_int32_t> iChanEField;
	ne10sLog10(chanWatts, chanDbm);
	chanDbm *= 10.0f;

	// Calculate efield factors and field strength, if required
	if (m_realtimeNet.GetNumSpectrumClients(3) > 0)
	{
		Ne10F32Vec chanEField = chanDbm;
		chanEField += suspTask->GetFieldStrengthFactors(firstChanFreq, taskParams.bw, chanWatts.size(), task->GetStep());
		iChanEField.resize(chanEField.size());
		ne10sConvert(&chanEField[0], &iChanEField[0], chanEField.size(), -128, 126);	// round to int and clamp
	}

	SEquipCtrlMsg::EAnt eAnt = CTask::GetEAntValue(task->m_ant, taskParams.pol);
	Units::Frequency tunedFreq = task->GetTuneFreq();

	// offset and dbw -> dbm 230.f (convert later on does rounding)
	float dbmAdj = 230.f - CalcShfExtGain(eAnt, tunedFreq);
	chanDbm += dbmAdj;

	/*for (unsigned int i = 0; i < chanDbm.size(); i++)
	{
		if (chanDbm[i] > 130)
		{
			printf("chanDbm is larger than 130 at index %d value=%.2e dbm size = %d dbmAdj = %.2f!\n", i, chanDbm[i], chanDbm.size(), dbmAdj);
			chanDbm[i] = chanDbm[i] - 130;
			printf("updating to new value %.2e\n", chanDbm[i]);
		}
		if (chanDbm[i] < 0)
		{
			printf("chanDbm is less than 0 at index %d value = %.2e dbm size = %d dbmAdj = %.2f!\n", i, chanDbm[i], chanDbm.size(), dbmAdj); // debug only
			chanDbm[i] = 0;
		}
	}*/

	// Version 1
	if (m_realtimeNet.GetNumSpectrumClients(1) > 0)
	{
		try
		{
			// AllocateMsgBuffer
			size_t length = offsetof(SSmsRealtimeMsg, body.spectrum.chanData) + numChan * sizeof(unsigned char);
			if (length > m_realtimeBuf.size())
			{
				m_realtimeBuf.resize(length);
			}
			auto realtimeMsg = reinterpret_cast<SSmsRealtimeMsg*>(&m_realtimeBuf[0]);
			realtimeMsg->hdr.msgType = SSmsRealtimeMsg::RT_SPECTRUM;
			realtimeMsg->hdr.msgTypeVersion = 1;
			realtimeMsg->hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SSpectrum, chanData) + numChan * sizeof(unsigned char));
			realtimeMsg->body.spectrum.taskId = task->m_taskIdKey.taskId;
			realtimeMsg->body.spectrum.bandIndex = static_cast<unsigned long>(task->GetStep());
			realtimeMsg->body.spectrum.firstChanFreq = firstChanFreq.Hz<unsigned long>(true);
			realtimeMsg->body.spectrum.chanSize = taskParams.bw.Hz<unsigned long>();
			realtimeMsg->body.spectrum.numChan = numChan;
			CNoiseEstimate::NoiseEstimate noise = CNoiseEstimate::Create(chanWatts, numChan, 0);
			realtimeMsg->body.spectrum.noiseFloor = static_cast<unsigned char>(10 * log10(noise->GetNoise()) + 230.5f);

			ne10sConvert(&chanDbm[0], realtimeMsg->body.spectrum.chanData, numChan, 0);
			if (!suspTask->NoExcludes())
			{
				for (unsigned int chan = 0; chan < numChan; ++chan)
				{
					if (suspTask->Excluded(firstChanFreq + chan * taskParams.bw, taskParams.bw, taskParams.pol))
					{
						realtimeMsg->body.spectrum.chanData[chan] = 0;
					}
				}
			}
			m_realtimeNet.SendToAllClients(realtimeMsg);
		}
		catch(std::overflow_error&)
		{
			// Frequency out of 32-bit range so skip
		}
	}
	// Version 2
	if (m_realtimeNet.GetNumSpectrumClients(2) > 0)
	{
		// AllocateMsgBuffer
		size_t length = offsetof(SSmsRealtimeMsg, body.spectrumV2.chanData) + numChan * sizeof(unsigned char);
		if (length > m_realtimeBuf.size())
		{
			m_realtimeBuf.resize(length);
		}
		auto realtimeMsg = reinterpret_cast<SSmsRealtimeMsg*>(&m_realtimeBuf[0]);
		realtimeMsg->hdr.msgType = SSmsRealtimeMsg::RT_SPECTRUM;
		realtimeMsg->hdr.msgTypeVersion = 2;
		realtimeMsg->hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SSpectrumV2, chanData) + numChan * sizeof(unsigned char));
		realtimeMsg->body.spectrumV2.taskId = task->m_taskIdKey.taskId;
		realtimeMsg->body.spectrumV2.bandIndex = static_cast<unsigned long>(task->GetStep());
		realtimeMsg->body.spectrumV2.firstChanFreq = firstChanFreq.GetRaw();
		realtimeMsg->body.spectrumV2.chanSize = taskParams.bw.GetRaw();
		realtimeMsg->body.spectrumV2.numChan = numChan;
		CNoiseEstimate::NoiseEstimate noise = CNoiseEstimate::Create(chanWatts, numChan, 0);
		realtimeMsg->body.spectrumV2.noiseFloor = static_cast<unsigned char>(10 * log10(noise->GetNoise()) + 230.5f);

		ne10sConvert(&chanDbm[0], realtimeMsg->body.spectrumV2.chanData, numChan, 0);
		if (!suspTask->NoExcludes())
		{
			for (unsigned int chan = 0; chan < numChan; ++chan)
			{
				if (suspTask->Excluded(firstChanFreq + chan * taskParams.bw, taskParams.bw, taskParams.pol))
				{
					realtimeMsg->body.spectrumV2.chanData[chan] = 0;
				}
			}
		}
		m_realtimeNet.SendToAllClients(realtimeMsg);
	}
	// Version 3
	if (m_realtimeNet.GetNumSpectrumClients(3) > 0)
	{
		// AllocateMsgBuffer
		size_t length = offsetof(SSmsRealtimeMsg, body.spectrumV3.chanData) + numChan * (sizeof(unsigned char) + sizeof(signed char));
		if (length > m_realtimeBuf.size())
		{
			m_realtimeBuf.resize(length);
		}
		auto realtimeMsg = reinterpret_cast<SSmsRealtimeMsg*>(&m_realtimeBuf[0]);
		realtimeMsg->hdr.msgType = SSmsRealtimeMsg::RT_SPECTRUM;
		realtimeMsg->hdr.msgTypeVersion = 3;
		realtimeMsg->hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SSpectrumV3, chanData) + numChan * (sizeof(unsigned char) + sizeof(signed char)));
		realtimeMsg->body.spectrumV3.taskId = task->m_taskIdKey.taskId;
		realtimeMsg->body.spectrumV3.bandIndex = static_cast<unsigned long>(task->GetStep());
		realtimeMsg->body.spectrumV3.firstChanFreq = firstChanFreq.GetRaw();
		realtimeMsg->body.spectrumV3.chanSize = taskParams.bw.GetRaw();
		realtimeMsg->body.spectrumV3.zeroVal = 0;
		realtimeMsg->body.spectrumV3.numChan = numChan;
		CNoiseEstimate::NoiseEstimate noise = CNoiseEstimate::Create(chanWatts, numChan, 0);
		realtimeMsg->body.spectrumV3.noiseFloor = static_cast<unsigned char>(10 * log10(noise->GetNoise()) + 230.5f);

		auto eField = reinterpret_cast<signed char*>(&realtimeMsg->body.spectrumV3.chanData[numChan]);

		ne10sConvert(&chanDbm[0], realtimeMsg->body.spectrumV3.chanData, numChan, 0);
		ne10sConvert(&iChanEField[0], &eField[0], numChan);
		if (!suspTask->NoExcludes())
		{
			for (unsigned int chan = 0; chan < numChan; ++chan)
			{
				auto freq = firstChanFreq + chan * taskParams.bw;
				if (suspTask->Excluded(freq, taskParams.bw, taskParams.pol))
				{
					realtimeMsg->body.spectrumV3.chanData[chan] = 0;
					eField[chan] = 0;
				}
			}
		}
		m_realtimeNet.SendToAllClients(realtimeMsg);
	}

}


//////////////////////////////////////////////////////////////////////
//
// Stop threads
//
void CProcessing::Stop(void)
{
	if(m_thread.joinable())
	{
		m_queue.Close();
		m_thread.join();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Processing thread
//
void CProcessing::Thread(void)
{
	m_processingPingSource = m_watchdog->RegisterPingSource(_T("data processing thread"));

	try
	{
		while(true)
		{
			m_watchdog->Ping(m_processingPingSource);

			try
			{
				static const unsigned int TIMEOUT = 1000;
				CTask::Task task(m_queue.GetItem(TIMEOUT));

				switch(task->GetTaskParams().mode)
				{
				case CConfig::EMode::AVD:
					DoAvd(task);
					break;

				case CConfig::EMode::DF:
				case CConfig::EMode::MEASURE:
				case CConfig::EMode::IQDATA:
					DoMeasure(task);
					break;

				case CConfig::EMode::OCCUPANCY:
					DoOccupancy(task);
					break;

				case CConfig::EMode::PAN:
					DoPan(task);
					break;

				case CConfig::EMode::PB_CAL:
					DoPbCal(task);
					break;

				case CConfig::EMode::FAST_SCAN_DF:
				case CConfig::EMode::SCAN_DF:
					DoScanDf(task);
					break;
#if 0
				case CConfig::MANUAL:
					DoManual(task);
					break;
#endif
				default:
					throw std::logic_error("CProcessing::Thread: Invalid task type");
				}
			}
			catch(TaskQueue::EProblem& problem)
			{
				switch(problem)
				{
				case TaskQueue::TIMEDOUT:
					// Need to ping watchdog
					break;

				case TaskQueue::CLOSED:
					// Shutdown
					m_watchdog->UnregisterPingSource(m_processingPingSource);
					printf("CProcessing::Thread exiting\n");
					return;

				default:
					throw std::logic_error("Unexpected CProcessing::TaskQueue problem");
				}
			}
		}
	}
	catch(...)
	{
		CFailure::OnFail(std::current_exception());
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for slaves to have sent the data
//
bool CProcessing::WaitForSlaves(const std::vector<size_t>& sentIndex, unsigned long tag, CHwControl::MsgMapValue& responses) const
{
//	printf("CProcessing::WaitForSlaves waiting for tag = %lu %u\n", tag, sentIndex.size());
	bool failed = false;
	responses.reset();
	try
	{
		CWeakSingleton<CProcessorNode> procNode;
		if (!procNode->WaitForAllResponses(sentIndex, tag, responses))
		{
			failed = true;
		}
	}
	catch(CWeakSingleton<CProcessorNode>::NoInstance&)
	{
		TRACE("CHwControl::WaitForSlaves Unable to access processorNode\n");
	}
	// Check that responses are success, otherwise fail
	auto& r = responses;
	if (!r)
	{
		failed = true;
	}
	else
	{
		if (r->status == NTaggedQueue::SDfData::FAILURE)
		{
			failed = true;
			TRACE("CHwControl::WaitForSlaves received dfdata with status FAILURE\n");
		}
	}
	return !failed;
}
