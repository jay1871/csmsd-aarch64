/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "NetConnection.h"
#include "DfCtrlMsg.h"
#include "DfCtrlNet.h"
#include "ErrorCodes.h"
#include "VCPMsg.h"
#include "Failure.h"
#include "Log.h"
#include "ProcessorNode.h"
#include "Utility.h"
#include "ProductVersion.h"
#include <codecvt>   // for codecvt_utf8
#include <string>
//////////////////////////////////////////////////////////////////////
//
// Static data
//
SSmsMsg::VersionMap CDfCtrlNet::m_versionMap;
//bool CDfCtrlNet::VERBOSE = true;
bool CDfCtrlNet::m_startupDone = false;
//CDfCtrlNet::SDfDataQueue CDfCtrlNet::m_dfDataQueue;
char SDfCtrlMsg::slaveipaddress[16];
//////////////////////////////////////////////////////////////////////
//
// Constructor - server
//
CDfCtrlNet::CDfCtrlNet(CProcessorNode* const proc, unsigned long myNodeId, LPCTSTR service) :
	CNetConnection<CDfCtrlNet>(service),
	m_bistResultDone(false),
	m_clientControlMsgMap(),
	m_faultRespDone(false),
	m_proc(proc),
	m_remoteNodeId(0),
	m_thisNodeId(myNodeId),
	m_usingExternalClock(false),
	m_setAudioParamsDone(false)
{
	// Build message version maps
	for(unsigned int entry = 0; entry < std::extent<decltype(SDfCtrlMsg::VERSION_DATA)>::value; ++entry)
	{
		m_versionMap[SDfCtrlMsg::VERSION_DATA[entry].msgId] = SDfCtrlMsg::VERSION_DATA[entry].version;
	}
	for(unsigned int entry = 0; entry < std::extent<decltype(SVCPMsg::VERSION_DATA)>::value; ++entry)
	{
		m_versionMap[SVCPMsg::VERSION_DATA[entry].msgId] = SVCPMsg::VERSION_DATA[entry].version;
	}

	if (m_thisNodeId != 0)
	{
		// Bring up complete slave if we are on external clock
		CRadioEquip::EClockState clockState;
		if (!C2630::ReadOrSetClockStateStatic(false, C2630::EClockState(0), clockState))
		{
			TRACE("Failed to get clock state\n");
			throw std::runtime_error("DfCtrlNet failed to read clock state!\n");
		}
		if (clockState == C2630::EClockState::EXTERNAL)
		{
			// Slave on external clock - bring it all the way up
			m_slaveControl.reset(new CSingleton<CSlaveControl>);
		}
		else
		{
			// Slave on internal clock - bring up digitizer to handle shutdown interrupt, just in case
			m_digitizer.reset(new CSingleton<CDigitizer>);
		}
	}
	SetFail(&CFailure::OnFail);
	Startup();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Constructor - dynamic
//
CDfCtrlNet::CDfCtrlNet(SOCKET socket, _In_ CDfCtrlNet* server) :
#ifdef CSMS_DEBUG
	CNetConnection<CDfCtrlNet>(socket),
#else
	CNetConnection<CDfCtrlNet>(socket, 30000, 5000),
#endif
	m_bistResultDone(false),
	m_clientControlMsgMap(),
	m_faultRespDone(false),
	m_proc(server->m_proc),		// using raw pointer should be ok since proc won't be deleted while there are connections?
	m_remoteNodeId(0),
	m_thisNodeId(server->m_thisNodeId),
	m_server(server),
	m_usingExternalClock(false),
	m_setAudioParamsDone(false)
{
	static const int ON = 1;
	if (!SetSockOpt(TCP_NODELAY, &ON, sizeof(ON), IPPROTO_TCP))
	{
		printf("CDfCtrlNet dynamic Set TCP_NODELAY failed\n");
	}
	static const int SNDBUF = 1048576;
	if (!SetSockOpt(SO_SNDBUF, &SNDBUF, sizeof(SNDBUF), SOL_SOCKET))
	{
		printf("CDfCtrlNet dynamic Set SO_SNDBUF to %d failed\n", SNDBUF);
	}

	SetNoReverse(true);

	Startup();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Constructor - client
//
CDfCtrlNet::CDfCtrlNet(CProcessorNode* const proc, const CConfig::SProcessorNodes::SNode& node, unsigned long myNodeId, LPCTSTR service) :
	CNetConnection<CDfCtrlNet>(node.ipAddress.c_str(), service, 10000),		// 10 second retry connect
	m_bistResultDone(false),
	m_clientControlMsgMap(),
	m_faultRespDone(false),
	m_proc(proc),
	m_remoteNodeId(node.id),
	m_thisNodeId(myNodeId),
	m_usingExternalClock(false),
	m_setAudioParamsDone(false)
{
	if (m_versionMap.size() == 0)
	{
		// Build message version maps
		for(unsigned int entry = 0; entry < std::extent<decltype(SDfCtrlMsg::VERSION_DATA)>::value; ++entry)
		{
			m_versionMap[SDfCtrlMsg::VERSION_DATA[entry].msgId] = SDfCtrlMsg::VERSION_DATA[entry].version;
		}
		for(unsigned int entry = 0; entry < std::extent<decltype(SVCPMsg::VERSION_DATA)>::value; ++entry)
		{
			m_versionMap[SVCPMsg::VERSION_DATA[entry].msgId] = SVCPMsg::VERSION_DATA[entry].version;
		}
	}

	static const int ON = 1;
	if (!SetSockOpt(TCP_NODELAY, &ON, sizeof(ON), IPPROTO_TCP))
	{
		printf("CDfCtrlNet client Set TCP_NODELAY failed\n");
	}

	int value;
	auto sizeOfValue = sizeof(value);
	if (GetSockOpt(SO_SNDBUF, &value, &sizeOfValue, SOL_SOCKET))
	{
		printf("CDfCtrlNet client initially SO_SNDBUF is %d\n", value);
	}
	static const int SNDBUF = 1048576;
	if (!SetSockOpt(SO_SNDBUF, &SNDBUF, sizeof(SNDBUF), SOL_SOCKET))
	{
		printf("CDfCtrlNet client Set SO_SNDBUF to %d failed\n", SNDBUF);
	}

	sizeOfValue = sizeof(value);
	if (GetSockOpt(SO_SNDBUF, &value, &sizeOfValue, SOL_SOCKET))
	{
		printf("CDfCtrlNet client finally SO_SNDBUF is %d\n", value);
	}

	Startup();
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CDfCtrlNet::~CDfCtrlNet(void)
{
	if (GetMode() == CLIENT)
	{
		CLog::Log(CLog::INFORMATION, "CDfCtrlNet client shutdown connection");
		m_taggedResponseQueue.Close();
		size_t c1 = m_taggedResponseQueue.Flush();	// avoids assert in PriorityQueue dtor

		if (c1 > 0)
		{
			TRACE("CDfCtrlNet flushed %u items from tagged queue\n", c1);
		}
	}
	Shutdown();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Add this node's bist results to allresults
//
void CDfCtrlNet::AddBistResults(std::vector<CBist::SBistResult>& allResults) const
{
	CBist::SBistResult r;
	CSharedLockGuard lock(m_bistResultLock);
	std::wstring_convert<codecvt<char16_t,char,std::mbstate_t>,char16_t> convert16;

	if (m_bistResult.empty())
	{
		// Common data
		r.result = SSmsMsg::SGetBistResp::WARNING;
		r.test = std::u16string(u"Built-In Self Test");
		r.send = true;

		char nodeChar[6];
		snprintf(nodeChar, 6, "[%lu]", m_remoteNodeId);
		std::u16string nodeStr;
		Utility::ToU16String(nodeChar, 6, nodeStr);
		r.test += nodeStr;

		// Specific result data
		r.text = std::u16string(u"Diagnostics never run");
		r.last = false;
		allResults.push_back(r);
		r.text = std::u16string(u"");
		r.last = true;
		allResults.push_back(r);
	}
	else
	{
		for (auto& it : m_bistResult)
		{
			r.result = it.result;
			r.test.assign(it.test, it.testLen);
			r.text.assign(it.text, it.textLen);
			r.last = it.last;
			r.send = it.send;
			// record slave ip address // to do?
			if (r.text.find(u"IP Address") != std::u16string::npos)
			{
				//CLog::Log(CLog::INFORMATION, _T("slave IP address= %s length = %u"), r.text.c_str(), r.text.length());
				std::string ipaddr = convert16.to_bytes(r.text);
				if (ipaddr.find(",")!= std::string::npos)
				{
					int commapos = ipaddr.find(",");
					strcpy(SDfCtrlMsg::slaveipaddress, ipaddr.substr(12, commapos).c_str());
					SDfCtrlMsg::slaveipaddress[commapos-12]='\0'; // 12 is the characters in bist before ip addr
				}
				else
				{
					if (!ipaddr.empty())
					{
						 // external primary
						 strcpy(SDfCtrlMsg::slaveipaddress, ipaddr.substr(12, ipaddr.length()).c_str());
						 SDfCtrlMsg::slaveipaddress[ipaddr.length()-12] = '\0';

					}
					else // use internal fall back from xml
					{
						SDfCtrlMsg::slaveipaddress[0] = '\0';

					}
				}
				std::cout << "slave ip = " << SDfCtrlMsg::slaveipaddress << std::endl;
			}

			allResults.push_back(r);
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the body size. Protects against buffer overruns
//
size_t CDfCtrlNet::BodySize(_In_ const void* hdr) const
{
	return static_cast<const SDfCtrlMsg::SHdr*>(hdr)->bodySize;
}


//////////////////////////////////////////////////////////////////////
//
// Downconvert a response message to the requested version
//
bool CDfCtrlNet::DownConvert(_Inout_ const SDfCtrlMsg*& msg)
{
//	SDfCtrlMsg* newMsg = nullptr;

	switch(msg->hdr.msgType)
	{
	default:
		return false;
	}

//	ASSERT(newMsg);
//	msg = newMsg;

	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Downconvert a response message to the requested version
//
bool CDfCtrlNet::DownConvert(_Inout_ const SVCPMsg*& msg)
{
//	SVCPMsg* newMsg = nullptr;

	switch(msg->hdr.msgType)
	{
	default:
		return false;
	}

//	ASSERT(newMsg);
//	msg = newMsg;

	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Get this node's fault response
//
bool CDfCtrlNet::GetFaultResp(SDfCtrlMsg::SGetSlaveCsmsFaultResp& resp) const
{
	CSharedLockGuard lock(m_faultRespLock);
	if (!m_faultRespDone)
		return false;

	resp = m_faultResp;
	return true;
}

//////////////////////////////////////////////////////////////////////
//
// Get this node's set audio params response
//
bool CDfCtrlNet::GetSlaveAudioParamsResp(SDfCtrlMsg::SSetSlaveAudioParamsResp& resp) const
{
	CSharedLockGuard lock(m_SetAudioParamsLock);
	if (!m_setAudioParamsDone)
		return false;

	resp = m_setslaveaudioparamsResp;
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Check for repeated client control message
//
bool CDfCtrlNet::IsRepeatClientControlMsg(const SDfCtrlMsg* msg)
{
	// TODO: Decide if this function is really helping

	// If returns false, then message will be sent.
	// If true, then message will not be sent (duplicate) and a dummy response will be enqueued

	if (GetMode() != CLIENT)
		return false;

	// Check if this is one of the control messages
	if (msg->hdr.msgType != SDfCtrlMsg::INTERNAL_DF_CTRL)
		return false;

	// NOTE: This only deals with SLAVE_TUNE messages that have decimation (old DF_TUNE type)
	if (msg->hdr.msgSubType != SDfCtrlMsg::SLAVE_TUNE)
		return false;

	SDfCtrlMsg::SVersionKey key = { msg->hdr.msgType, msg->hdr.msgSubType };
	SDfCtrlMsg::SVersionKey respKey = { msg->hdr.msgType, SDfCtrlMsg::SLAVE_TUNE_RESP };
	// If this message if "DF_TUNE" and has been previously sent, return true (repeated message)
	if (msg->body.slaveTune.tuneDigitizer)
	{
		std::lock_guard<std::mutex> lock(m_clientControlMsgMap.msgMapLock);
		auto req = m_clientControlMsgMap.msgMap.find(key);
		if (req != m_clientControlMsgMap.msgMap.end())
		{
			// This type of message has been sent previously, see if this one is identical
			if (msg->body.slaveTune == req->second.body.slaveTune)
			{
				auto resp = m_clientControlMsgMap.msgMap.find(respKey);
				if (resp != m_clientControlMsgMap.msgMap.end())
				{
					// duplicate and we have a response available. Just fix its tag.
					resp->second.hdr.sourceAddr = msg->hdr.sourceAddr;
					m_taggedResponseQueue.Enqueue(resp->second);
					return true;		// duplicate, no need to update the map
				}
				else
				{
					TRACE("SLAVE_TUNE_RESP not available\n");
				}
			}
		}
	}

	// Save the last client message sent in a map keyed by msgType, msgSubType
	m_clientControlMsgMap.msgMap[key] = *msg;
	m_clientControlMsgMap.msgMap.erase(respKey);
	return false;	// not a duplicate
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CDfCtrlNet::OnClose(int errorCode)
{
	if(auto peer = GetPeerAddress())
	{
		CLog::Log(CLog::INFORMATION, "%s disconnected from DfControl port, code = %d", peer->ai_canonname, errorCode);
	}
	m_usingExternalClock = false;
	std::lock_guard<std::mutex> lock(m_clientControlMsgMap.msgMapLock);
	m_clientControlMsgMap.msgMap.clear();
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CDfCtrlNet::OnConnect(void)
{
	bool clientmode = (GetMode() == CLIENT);
	if(auto peer = GetPeerAddress())
	{
		if (clientmode)
			CLog::Log(CLog::INFORMATION, "%s connected to DfControl port as client", peer->ai_canonname);
		else
			CLog::Log(CLog::INFORMATION, "%s connected to DfControl port", peer->ai_canonname);
	}

	if (clientmode)
	{
		if (m_thisNodeId == 0)
		{
			// Initialize master client data
			{
				CLockGuard lock(m_bistResultLock);
				m_bistResultDone = false;
				m_bistResult.clear();
			}
			CLockGuard::CallLocked(m_faultRespLock, [&] { m_faultRespDone = false; });
			CLockGuard::CallLocked(m_SetAudioParamsLock, [&] { m_setAudioParamsDone = false; });

			// Start a timer (30 seconds)
			sigevent sigEvent;
			sigEvent.sigev_notify = SIGEV_THREAD;
			sigEvent.sigev_value.sival_ptr = this;
			sigEvent.sigev_notify_function = TimerThread;
			sigEvent.sigev_notify_attributes = nullptr;

			if (timer_create(CLOCK_MONOTONIC, &sigEvent, &m_timerId) == -1)
			{
				TRACE("Unable to create timer\n");
			}
			else
			{
				TRACE("Created timer %08lx\n", long(m_timerId));
				itimerspec ts;
				ts.it_value.tv_sec = 30;
				ts.it_value.tv_nsec = 0;
				ts.it_interval.tv_sec = 0;
				ts.it_interval.tv_nsec = 0;
				if (timer_settime(m_timerId, 0, &ts, 0) == -1)
				{
					TRACE("Unable to settime\n");
				}
				else
				{
					TRACE("Set timer\n");
				}
			}
		}
		// Send my message versions
		SendMsgVersions(SDfCtrlMsg::GET_MSG_VERSIONS);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CDfCtrlNet::OnMessage(_In_bytecount_(size) const void* msg, size_t size)
{
	const SDfCtrlMsg* dfMsg = static_cast<const SDfCtrlMsg*>(msg);

	ASSERT(size == offsetof(SDfCtrlMsg, body) + dfMsg->hdr.bodySize);
	auto peer = GetPeerAddress();
	LPCTSTR peername(peer == nullptr ? _T("Unknown") : peer->ai_canonname);

	if(size != offsetof(SDfCtrlMsg, body) + dfMsg->hdr.bodySize)
	{
		// Corrupt message
		CLog::Log(CLog::ERRORS, "DfControl: corrupt message from %s", peername);
		Close();
		return;
	}

	// Check command version
	SDfCtrlMsg::SVersionKey key = { dfMsg->hdr.msgType, dfMsg->hdr.msgSubType };
	SDfCtrlMsg::VersionMap::const_iterator entry = m_clientVersionMap.find(key);

	if(m_clientVersionMap.size() > 0 &&
		((entry == m_clientVersionMap.end() && (dfMsg->hdr.cmdVersion > 0 || dfMsg->hdr.respVersion > 0)) ||
		(entry != m_clientVersionMap.end() &&
		(entry->second.cmdVersion != dfMsg->hdr.cmdVersion || entry->second.respVersion != dfMsg->hdr.respVersion))))
	{
		// Invalid version (client mismatch with itself!)
		SDfCtrlMsg::SHdr hdr(dfMsg->hdr);

		if(entry == m_clientVersionMap.end())
		{
			hdr.cmdVersion = 0;
			hdr.respVersion = 0;
		}
		else
		{
			hdr.cmdVersion = entry->second.cmdVersion;
			hdr.respVersion = entry->second.respVersion;
		}

		CLog::Log(CLog::ERRORS, "DfControl: bad version from %s: type=%u subtype=%lu cmdVer=%u respVer=%u", peername,
			dfMsg->hdr.msgType, dfMsg->hdr.msgSubType, dfMsg->hdr.cmdVersion, dfMsg->hdr.respVersion);
		SendError(hdr, ErrorCodes::INVALIDVERSION);

		return;
	}

	entry = m_versionMap.find(key);

	if((entry == m_versionMap.end() && dfMsg->hdr.cmdVersion > 0) ||
		(entry != m_versionMap.end() && entry->second.cmdVersion < dfMsg->hdr.cmdVersion))
	{
		// Invalid version (client mismatch with me)
		SDfCtrlMsg::SHdr hdr(dfMsg->hdr);

		if(entry == m_versionMap.end())
		{
			hdr.cmdVersion = 0;
			hdr.respVersion = 0;
		}
		else
		{
			hdr.cmdVersion = entry->second.cmdVersion;
			hdr.respVersion = entry->second.respVersion;
		}

		CLog::Log(CLog::ERRORS, "DfControl: bad version from %s: type=%u subtype=%lu cmdVer=%u", peername,
				dfMsg->hdr.msgType, dfMsg->hdr.msgSubType, dfMsg->hdr.cmdVersion);
		SendError(hdr, ErrorCodes::INVALIDVERSION);
		return;
	}

	// Handle GET_MSG_VERSIONS here
	if(dfMsg->hdr.msgType == SDfCtrlMsg::STATUS_CTRL && dfMsg->hdr.msgSubType == SDfCtrlMsg::GET_MSG_VERSIONS && GetMode() != CLIENT)
	{
		CLog::Log(CLog::INFORMATION, "DfControl: received from %s: GET_MSG_VERSIONS", peername);

		if (m_thisNodeId == 0)
		{
			m_proc->ConnectMasterAsClient();
		}

		// Remember merged client versions
		for(size_t i = 0; dfMsg->hdr.bodySize > 0 && i < dfMsg->body.getMsgVersionsData.count; ++i)
		{
			entry = m_versionMap.find(dfMsg->body.getMsgVersionsData.versionData[i].msgId);

			if(entry != m_versionMap.end())
			{
				SDfCtrlMsg::SVersionValue version;
				version.cmdVersion = std::min(dfMsg->body.getMsgVersionsData.versionData[i].version.cmdVersion, entry->second.cmdVersion);
				version.respVersion = std::min(dfMsg->body.getMsgVersionsData.versionData[i].version.respVersion, entry->second.respVersion);
				m_clientVersionMap[dfMsg->body.getMsgVersionsData.versionData[i].msgId] = version;
			}
		}
		TRACE("Server created clientVersionMap\n");

		unsigned long count = m_versionMap.size();
		auto msgSize = offsetof(SDfCtrlMsg, body.getMsgVersionsData.versionData) + count * sizeof(SDfCtrlMsg::SVersionData);
		std::shared_ptr<SDfCtrlMsg> resp(new(msgSize) SDfCtrlMsg);
		resp->hdr = dfMsg->hdr;
		resp->hdr.msgSubType = SDfCtrlMsg::GET_MSG_VERSIONS_RESP;
		resp->hdr.bodySize = offsetof(SDfCtrlMsg::SGetMsgVersionsData, versionData) + count * sizeof(SDfCtrlMsg::SVersionData);
		resp->body.getMsgVersionsData.count = count;
		size_t index = 0;

		for(entry = m_versionMap.begin(); entry != m_versionMap.end(); ++entry)
		{
			resp->body.getMsgVersionsData.versionData[index].msgId = entry->first;
			resp->body.getMsgVersionsData.versionData[index++].version = entry->second;
		}

		if (Send(resp.get(), offsetof(SDfCtrlMsg, body) + resp->hdr.bodySize))
		{
			CLog::Log(CLog::INFORMATION, "DfControl: sent GET_MSG_VERSIONS_RESP to %s", peername);
		}
		else
		{
			CLog::Log(CLog::INFORMATION, "DfControl: send failed GET_MSG_VERSIONS_RESP to %s", peername);
		}
		return;
	}
	// Handle GET_MSG_VERSIONS_RESP here
	if(dfMsg->hdr.msgType == SDfCtrlMsg::STATUS_CTRL && dfMsg->hdr.msgSubType == SDfCtrlMsg::GET_MSG_VERSIONS_RESP && GetMode() == CLIENT)
	{
		CLog::Log(CLog::INFORMATION, "DfControl: received from %s: GET_MSG_VERSIONS_RESP", peername);

		// Build this client's version map (highest version of messages to send to server)
		for (size_t i = 0; i < dfMsg->body.getMsgVersionsData.count; ++i)
		{
			// Look for this message id in my message map
			entry = m_versionMap.find(dfMsg->body.getMsgVersionsData.versionData[i].msgId);
			if (entry != m_versionMap.end())
			{
				SDfCtrlMsg::SVersionValue version;
				version.cmdVersion = std::min(dfMsg->body.getMsgVersionsData.versionData[i].version.cmdVersion, entry->second.cmdVersion);
				version.respVersion = std::min(dfMsg->body.getMsgVersionsData.versionData[i].version.respVersion, entry->second.respVersion);
				m_clientVersionMap[dfMsg->body.getMsgVersionsData.versionData[i].msgId] = version;
			}
		}
		TRACE("Client created clientVersionMap\n");

		if (m_thisNodeId == 0)	// master client
		{
			// Send a request to switch to external clock - response must come within 10 seconds, otherwise repeat n times
			SDfCtrlMsg ctrlMsg;
			ctrlMsg.hdr.msgType = SDfCtrlMsg::INTERNAL_DF_CTRL;
			ctrlMsg.hdr.msgSubType = SDfCtrlMsg::SET_EXTERNAL_CLOCK;
			ctrlMsg.hdr.sourceAddr = 0;
			ctrlMsg.hdr.destAddr = 0;
			ctrlMsg.hdr.bodySize = 0;
			if (Send(&ctrlMsg, offsetof(SDfCtrlMsg, body)))
			{
				CLog::Log(CLog::INFORMATION, "sent SET_EXTERNAL_CLOCK to %s", peername);
			}
			else
			{
				CLog::Log(CLog::INFORMATION, "send failed SET_EXTERNAL_CLOCK to %s", peername);
			}
		}
		return;
	}

	bool errorReturn = false;
	bool converted = false;
	std::unique_ptr<SDfCtrlMsg> tmpMsg;	// For modifying dfMsg, if necessary
	try
	{
		if(entry != m_versionMap.end() && entry->second.cmdVersion > dfMsg->hdr.cmdVersion)
		{
			// Upconvert to latest version
			UpConvert(dfMsg);
			converted = true;
		}

		// Pass to Internal Df Control
		switch(dfMsg->hdr.msgType)
		{
		case SDfCtrlMsg::INTERNAL_DF_CTRL:
			OnInternalDfCtrl(dfMsg, this);
		break;

		case SDfCtrlMsg::SMS_ERROR_REPORT:
			CLog::Log(CLog::INFORMATION, "rcvd SMS_ERROR_REPORT from %s code:%lu tag:%lu", peername,
				dfMsg->hdr.msgSubType, dfMsg->hdr.sourceAddr);
			if (GetMode() == CLIENT && m_thisNodeId == 0)
			{
				m_taggedResponseQueue.Enqueue(*dfMsg);
			}
			break;

		default:
			CLog::Log(CLog::ERRORS, "DfControl: invalid message type from %s: type %u subtype %lu", peername,
				dfMsg->hdr.msgType, dfMsg->hdr.msgSubType);
			throw ErrorCodes::INVALIDMSGTYPE;
		}
	}
	catch(ErrorCodes::EErrorCode& error)
	{
		SendError(dfMsg->hdr, error);
	}
	// Catch all the possible fatal exceptions in as much detail as possible
	catch(const std::exception& ex)
	{
		CLog::Log(CLog::ERRORS, "DfControl: service failed %s", ex.what());
		errorReturn = true;
	}
	catch(...)
	{
		CLog::Log(CLog::ERRORS, "DfControl: service failed An unexpected error occurred");
		errorReturn = true;
	}

	if(converted)
	{
		// Delete the copy
		delete dfMsg;
	}

	if(errorReturn)
	{
		throw std::runtime_error("Fatal error");
//		CService::Instance()->OnAbort(errorReturn);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle internal df control messages
//
void CDfCtrlNet::OnInternalDfCtrl(const SDfCtrlMsg* msg, CDfCtrlNet* source)
{
	auto peer = (source != nullptr ? source->GetPeerAddress() : nullptr);
	auto peername = (peer != nullptr ? peer->ai_canonname : "unknown");
	auto mode = GetMode();
	SDfCtrlMsg resp;

	if (mode == CLIENT)
	{
		if (m_thisNodeId > 0)
		{
			CLog::Log(CLog::INFORMATION, _T("rcvd unknown INTERNAL_DF_CTRL subType %d from %s"), msg->hdr.msgSubType, peername);
		}
		else
		{
			// These should only be received by master client
			SDfCtrlMsg::SVersionKey key = { msg->hdr.msgType, msg->hdr.msgSubType };

			// These should only be received by the master client
			switch(msg->hdr.msgSubType)
			{
			case SDfCtrlMsg::SET_EXTERNAL_CLOCK_RESP:
				CLog::Log(CLog::INFORMATION, _T("rcvd SET_EXTERNAL_CLOCK_RESP: %lu from %s"), msg->body.setExternalClockResp.status, peername);
				// Cancel the timeout timer and check the status
				if (timer_delete(m_timerId) == -1)
				{
					TRACE("timer_delete failed\n");
				}
				m_usingExternalClock = (msg->body.setExternalClockResp.status == SDfCtrlMsg::SSetExternalClockResp::EClockStatus::ALREADY ||
					msg->body.setExternalClockResp.status == SDfCtrlMsg::SSetExternalClockResp::EClockStatus::SUCCESS);
				if (m_usingExternalClock)
				{
					if (strcmp(msg->body.setExternalClockResp.softwarever, VersionInfo::FileVersion) != 0)
						Utility::SetSoftwareCompatibleoff();
					// Request BIST from this node and save the results
	//				if (m_startupDone)
	//				{
						SendDoBist();
	//				}
				}
				break;

			case SDfCtrlMsg::DO_BIST_RESP:
				if (msg->body.doBistResp.last)
				{
					CLog::Log(CLog::INFORMATION, _T("rcvd DO_BIST_RESP from %s"), peername);
				}
				// Save the response in a per-client collection
	//			TRACE("DO_BIST_RESP %u %hu %hu %d %d\n", msg->body.doBistResp.result, msg->body.doBistResp.testLen, msg->body.doBistResp.textLen,
	//				msg->body.doBistResp.last, msg->body.doBistResp.send);
				{
					CLockGuard lock(m_bistResultLock);
					m_bistResult.push_back(msg->body.doBistResp);
					if (msg->body.doBistResp.last)
						m_bistResultDone = true;
				}
				break;

			case SDfCtrlMsg::SLAVE_TUNE_RESP:
	//			CLog::Log(CLog::INFORMATION, "rcvd SLAVE_TUNE_RESP from %s", peername);	// Too verbose
				m_taggedResponseQueue.Enqueue(*msg);
				{
					std::lock_guard<std::mutex> lock(m_clientControlMsgMap.msgMapLock);
					m_clientControlMsgMap.msgMap[key] = *msg;	// Save response to re-use for repeated requests
				}
				break;

			case SDfCtrlMsg::SLAVE_SETUP_COLLECT_READY:
	//			CLog::Log(CLog::INFORMATION, "rcvd SLAVE_SETUP_COLLECT_READY from %s", peername);	// Too verbose
				m_taggedResponseQueue.Enqueue(*msg);
				break;

			case SDfCtrlMsg::SLAVE_SETUP_COLLECT_RESP:
	//			CLog::Log(CLog::INFORMATION, "rcvd SLAVE_SETUP_COLLECT_RESP from %s", peername);	// Too verbose
				m_taggedResponseQueue.Enqueue(*msg);
				break;

			case SDfCtrlMsg::GET_CSMS_FAULT_RESP:
				if (msg->hdr.bodySize != sizeof(SDfCtrlMsg::SGetSlaveCsmsFaultResp))
				{
					CLog::Log(CLog::ERRORS, "GET_CSMS_FAULT_RESP invalid bodySize from %s", peername);
				}
				else
				{
	//				CLog::Log(CLog::INFORMATION, _T("rcvd GET_CSMS_FAULT_RESP from %s"), peername);	// too verbose

					// Save the response in a per-client collection
					CLockGuard::CallLocked(m_faultRespLock, [&]
					{
						// TODO: Should we check that m_bistFaultResp = false?
						m_faultResp = msg->body.getSlaveCsmsFaultResp;
						m_faultRespDone = true;
					});
				}
				break;

			case SDfCtrlMsg::SET_SLAVE_AUDIO_PARMS_RESP:
				if (msg->hdr.bodySize != sizeof(SDfCtrlMsg::SSetSlaveAudioParamsResp))
				{
					CLog::Log(CLog::ERRORS, "SET_SLAVE_AUDIO_PARMS_RESP invalid bodySize from %s", peername);
				}
				else
				{
	//				CLog::Log(CLog::INFORMATION, _T("rcvd GET_CSMS_FAULT_RESP from %s"), peername);	// too verbose

					// Save the response in a per-client collection
					CLockGuard::CallLocked(m_SetAudioParamsLock, [&]
					{
						// TODO: Should we check that m_bistFaultResp = false?
						m_setslaveaudioparamsResp = msg->body.setSlaveAudioParamResp;
						m_setAudioParamsDone = true;
					});
				}
				break;

			case SDfCtrlMsg::CCDF_COLLECT_RESP:
//				CLog::Log(CLog::INFORMATION, "rcvd CCDF_COLLECT_RESP from %s", peername);
				m_taggedResponseQueue.Enqueue(*msg);
				break;

			case SDfCtrlMsg::DO_INITIAL_AGC_RESP:
//				CLog::Log(CLog::INFORMATION, "rcvd DO_INITIAL_AGC_RESP from %s", peername);	// Too verbose
				m_taggedResponseQueue.Enqueue(*msg);
				break;
#if NO_SLAVE_CLIENT == 1
			// - restore to collect slave samples back using single socket connection method, so slave is not sending back as client and master receiving it as client instead of dynamic server
			case SDfCtrlMsg::SLAVE_GET_SAMPLES_RESP:
				if (msg->hdr.bodySize != offsetof(SDfCtrlMsg::SSlaveGetSamplesResp, data) +
					SDfCtrlMsg::GetDataSize(msg->body.slaveGetSamplesResp.dataTypes, msg->body.slaveGetSamplesResp.numIq, msg->body.slaveGetSamplesResp.numPoints) * sizeof(unsigned long))
				{
					CLog::Log(CLog::ERRORS, "SLAVE_GET_SAMPLES_RESP invalid bodySize from %s", peername);
				}
				else
				{
					if (m_proc)
						m_proc->AddSlaveDfData(msg, peername);
				}
				break;
#endif

			default:
				CLog::Log(CLog::INFORMATION, _T("rcvd unknown INTERNAL_DF_CTRL subType %d from %s"), msg->hdr.msgSubType, peername);
				break;
			}
		}
	}
	else	// server / dynamic
	{
		if (m_thisNodeId == 0)
		{
			// These should only be received by master dynamic
			switch(msg->hdr.msgSubType)
			{
			case SDfCtrlMsg::SLAVE_GET_SAMPLES_RESP:
				if (msg->hdr.bodySize != offsetof(SDfCtrlMsg::SSlaveGetSamplesResp, data) +
					SDfCtrlMsg::GetDataSize(msg->body.slaveGetSamplesResp.dataTypes, msg->body.slaveGetSamplesResp.numIq, msg->body.slaveGetSamplesResp.numPoints) * sizeof(unsigned long))
				{
					CLog::Log(CLog::ERRORS, "SLAVE_GET_SAMPLES_RESP invalid bodySize from %s", peername);
				}
				else
				{
					//CLog::Log(CLog::INFORMATION, _T("master got slave_get_samples_resp"));//debug only
					if (m_proc)
						m_proc->AddSlaveDfData(msg, peername);
				}
				break;
			default:
				CLog::Log(CLog::INFORMATION, _T("rcvd unknown INTERNAL_DF_CTRL subType %d from %s"), msg->hdr.msgSubType, peername);
				break;
			}
		}
		else
		{
			// These should only be received by slave dynamic
			if (msg->hdr.msgSubType == SDfCtrlMsg::SET_EXTERNAL_CLOCK)
			{
				// Handle this message here
				CLog::Log(CLog::INFORMATION, _T("rcvd SET_EXTERNAL_CLOCK from %s"), peername);

		//		if (CProcessorNode::UseExternalClock())
		//			TRACE("Already on external clock\n");

				// Build response
				resp.hdr = msg->hdr;
				resp.hdr.msgSubType = SDfCtrlMsg::SET_EXTERNAL_CLOCK_RESP;
				resp.hdr.bodySize = sizeof(SDfCtrlMsg::SSetExternalClockResp);

				C2630::EClockState clockState;
				if (!C2630::ReadOrSetClockStateStatic(false, C2630::EClockState(0), clockState))
				{
					TRACE("Failed to get clock state\n");
					resp.body.setExternalClockResp.status = SDfCtrlMsg::SSetExternalClockResp::UNKNOWN;
				}
				else
				{
					strcpy(resp.body.setExternalClockResp.softwarever, VersionInfo::FileVersion) ; // add software ver so master can compare during bist
					resp.body.setExternalClockResp.status = (clockState == CRadioEquip::EClockState::INTERNAL
							? SDfCtrlMsg::SSetExternalClockResp::SUCCESS
							: SDfCtrlMsg::SSetExternalClockResp::ALREADY);
				}

				// Send response to client before switching clock, which will kill this processor!
				if (Send(&resp, offsetof(SDfCtrlMsg, body) + resp.hdr.bodySize))
				{
					CLog::Log(CLog::INFORMATION, "sent SET_EXTERNAL_CLOCK_RESP: %lu to %s", resp.body.setExternalClockResp.status, peername);
				}
				else
				{
					CLog::Log(CLog::INFORMATION, "send failed SET_EXTERNAL_CLOCK_RESP: %lu to %s", resp.body.setExternalClockResp.status, peername);
				}

				if (resp.body.setExternalClockResp.status != SDfCtrlMsg::SSetExternalClockResp::ALREADY)
				{
					TRACE("Disabling new network connections\n");
					m_server->DisableNewConnections(true);
					TRACE("Closing network connection\n");
					Close();
					TRACE("Deleting digitizer\n");
					m_digitizer.reset();
					Utility::Delay(1000000);	// 1 sec

					// Send command to radio and wait for response or timeout
					C2630::EClockState setState;
					if (!C2630::ReadOrSetClockStateStatic(true, CRadioEquip::EClockState::EXTERNAL, setState))
					{
						throw ErrorCodes::HARDWAREDOWN;
					}
					// If we get here, then clock didn't really switch
					TRACE("Re-enabling new network connections\n");
					m_server->DisableNewConnections(false);
					// Client (master) will eventually re-connect
				}
				return;
			}

			// This is slave dynamic - pass message to SlaveControl
			if (!m_server->m_slaveControl)
			{
				m_server->m_slaveControl.reset(new CSingleton<CSlaveControl>);
			}
			assert(m_server->m_slaveControl);
			if (m_server->m_slaveControl)
			{
				(*m_server->m_slaveControl)->OnInternalDfCtrl(msg, this);
			}
		}
	}
}


//////////////////////////////////////////////////////////////////////
//
// Ping the slave
//
void CDfCtrlNet::Ping(void)
{
	if (!IsConnected())
	{
		return;
	}

	auto peer = GetPeerAddress();
	LPCTSTR peerName(peer == nullptr ? _T("Unknown") : peer->ai_canonname);

	SDfCtrlMsg ctrlMsg;
	ctrlMsg.hdr.msgType = SDfCtrlMsg::INTERNAL_DF_CTRL;
	ctrlMsg.hdr.msgSubType = SDfCtrlMsg::PING;
	ctrlMsg.hdr.sourceAddr = 0;
	ctrlMsg.hdr.destAddr = 0;
	ctrlMsg.hdr.bodySize = 0;
	if (!Send(&ctrlMsg, offsetof(SDfCtrlMsg, body)))
	{
		CLog::Log(CLog::INFORMATION, "send failed PING to %s", peerName);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send message, downconverting if necessary
//
bool CDfCtrlNet::Send(_In_bytecount_(size) const void* sendMsg, size_t /*size*/)
{
	const SDfCtrlMsg* msg = static_cast<const SDfCtrlMsg*>(sendMsg);

	// Check response version
	SSmsMsg::SVersionKey key = { msg->hdr.msgType, msg->hdr.msgSubType };
	bool converted = false;

	if (GetMode() == CLIENT)
	{
		// This is the client send, put the versions into the header
		SSmsMsg::VersionMap::const_iterator entry = m_clientVersionMap.find(key);
		SDfCtrlMsg::SHdr& hdr = const_cast<SDfCtrlMsg::SHdr&>(msg->hdr);
		if (entry != m_clientVersionMap.end())
		{
			hdr.cmdVersion = entry->second.cmdVersion;
			hdr.respVersion = entry->second.respVersion;
		}
		else
		{
			hdr.cmdVersion = 0;
			hdr.respVersion = 0;
		}
		// Check if down conversion is necessary
		entry = m_versionMap.find(key);
		unsigned char myVersion = (entry != m_versionMap.end() ? entry->second.cmdVersion : 0);
		if (myVersion > msg->hdr.cmdVersion)
		{
			// Downconvert to requested version (making a local copy)
			if (!DownConvert(msg))
			{
				return false;
			}
			converted = true;
		}
		else if (myVersion < msg->hdr.cmdVersion)
		{
			// Make a local copy and set command version to my version
			size_t size = offsetof(SDfCtrlMsg, body) + msg->hdr.bodySize;
			SDfCtrlMsg* newMsg = new(size) SDfCtrlMsg;
			memcpy(newMsg, msg, size);
			newMsg->hdr.cmdVersion = myVersion;
			msg = newMsg;
			converted = true;
		}
	}
	else	// not client
	{
		// Check if down conversion is necessary
		SSmsMsg::VersionMap::const_iterator entry = m_versionMap.find(key);
		unsigned char myVersion = (entry != m_versionMap.end() ? entry->second.respVersion : 0);

		if (myVersion > msg->hdr.respVersion)
		{
			// Downconvert to requested version (making a local copy)
			if (!DownConvert(msg))
			{
				return false;
			}
			converted = true;
		}
		else if (myVersion < msg->hdr.respVersion)
		{
			// Make a local copy and set response version to my version
			size_t size = offsetof(SDfCtrlMsg, body) + msg->hdr.bodySize;
			SDfCtrlMsg* newMsg = new(size) SDfCtrlMsg;
			memcpy(newMsg, msg, size);
			newMsg->hdr.respVersion = myVersion;
			msg = newMsg;
			converted = true;
		}
	}

	bool status;
	if (IsRepeatClientControlMsg(msg))
	{
		status = true;
	}
	else
	{
		status = CNetConnection<CDfCtrlNet>::Send(msg, offsetof(SDfCtrlMsg, body) + msg->hdr.bodySize, m_clientSendTimeout);
	}
	if (converted)
	{
		// Delete the local copy
		delete msg;
	}

	return status;
}


//////////////////////////////////////////////////////////////////////
//
// Send a bist request
//
bool CDfCtrlNet::SendDoBist(void)
{
	if (!IsConnected())
	{
		CLockGuard lock(m_bistResultLock);
		m_bistResult.clear();
		m_bistResultDone = true;
		return false;
	}
	auto peer = GetPeerAddress();
	LPCTSTR peerName(peer == nullptr ? _T("Unknown") : peer->ai_canonname);
	{
		CLockGuard lock(m_bistResultLock);
		m_bistResult.clear();
		m_bistResultDone = false;
	}
	SDfCtrlMsg ctrlMsg;
	ctrlMsg.hdr.msgType = SDfCtrlMsg::INTERNAL_DF_CTRL;
	ctrlMsg.hdr.msgSubType = SDfCtrlMsg::DO_BIST;
	ctrlMsg.hdr.sourceAddr = 0;
	ctrlMsg.hdr.destAddr = 0;
	ctrlMsg.hdr.bodySize = sizeof(SDfCtrlMsg::SDoBist);
	ctrlMsg.body.doBist.bistType = SDfCtrlMsg::SDoBist::PERFORM_DIAGNOSTICS;
	if (Send(&ctrlMsg, offsetof(SDfCtrlMsg, body)))
	{
		CLog::Log(CLog::INFORMATION, "sent DO_BIST to %s", peerName);
		return true;
	}
	CLog::Log(CLog::INFORMATION, "send failed DO_BIST to %s", peerName);
	m_bistResultDone = true;
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Send a get fault request
//
bool CDfCtrlNet::SendGetFault(void)
{
	// Clear the fault info
	CLockGuard::CallLocked(m_faultRespLock, [&] { m_faultRespDone = false;} );

	if (!IsConnected())
	{
		return false;
	}

	auto peer = GetPeerAddress();
	LPCTSTR peerName(peer == nullptr ? _T("Unknown") : peer->ai_canonname);

	SDfCtrlMsg ctrlMsg;
	ctrlMsg.hdr.msgType = SDfCtrlMsg::INTERNAL_DF_CTRL;
	ctrlMsg.hdr.msgSubType = SDfCtrlMsg::GET_CSMS_FAULT;
	ctrlMsg.hdr.sourceAddr = 0;
	ctrlMsg.hdr.destAddr = 0;
	ctrlMsg.hdr.bodySize = 0;
	if (Send(&ctrlMsg, offsetof(SDfCtrlMsg, body)))
	{
//		CLog::Log(CLog::INFORMATION, "sent GET_CSMS_FAULT to %s", peerName);	// too verbose
		return true;
	}
	CLog::Log(CLog::INFORMATION, "send failed GET_CSMS_FAULT to %s", peerName);

	return false;
}

//////////////////////////////////////////////////////////////////////
//
// Send a get fault request
//
bool CDfCtrlNet::SendSetAudioParams(const C3230::SDecimation& decimations, const SEquipCtrlMsg::SAudioParamsCmd& audiocmd, const CNetConnection<void>* audiosource)
{
	// Clear the fault info
	CLockGuard::CallLocked(m_SetAudioParamsLock, [&] { m_setAudioParamsDone = false;} );

	if (!IsConnected())
	{
		return false;
	}

	auto peer = GetPeerAddress();
	LPCTSTR peerName(peer == nullptr ? _T("Unknown") : peer->ai_canonname);

	SDfCtrlMsg ctrlMsg;
	ctrlMsg.hdr.msgType = SDfCtrlMsg::INTERNAL_DF_CTRL;
	ctrlMsg.hdr.msgSubType = SDfCtrlMsg::SET_SLAVE_AUDIO_PARMS;
	ctrlMsg.hdr.sourceAddr = 0;
	ctrlMsg.hdr.destAddr = 0;
	ctrlMsg.hdr.bodySize = sizeof(SDfCtrlMsg::SSetSlaveAudioParams);
	//printf("audiocmd freq = %f", Units::Frequency(audiocmd.freq).Hz<double>());
	ctrlMsg.body.setSlaveAudioParam.freechannels = false;
	ctrlMsg.body.setSlaveAudioParam.audiocmd = audiocmd;
	ctrlMsg.body.setSlaveAudioParam.decimations.zifCICDecimation = decimations.zifCICDecimation;
	ctrlMsg.body.setSlaveAudioParam.decimations.zifFIRDecimation = decimations.zifFIRDecimation;
	ctrlMsg.body.setSlaveAudioParam.decimations.upResample = decimations.upResample;
	ctrlMsg.body.setSlaveAudioParam.decimations.downResample = decimations.downResample;
	ctrlMsg.body.setSlaveAudioParam.decimations.ddcCICDecimation = decimations.ddcCICDecimation;
	ctrlMsg.body.setSlaveAudioParam.decimations.ddcFIRDecimation = decimations.ddcFIRDecimation;
	ctrlMsg.body.setSlaveAudioParam.audiosource = audiosource;
	if (Send(&ctrlMsg, offsetof(SDfCtrlMsg, body)))
	{
//		CLog::Log(CLog::INFORMATION, "sent GET_CSMS_FAULT to %s", peerName);	// too verbose
		return true;
	}
	CLog::Log(CLog::INFORMATION, "send Set Slave Audio Params to %s", peerName);

	return false;
}

bool CDfCtrlNet::SendFreeSlaveAudioChannel(unsigned long channelnum, bool freeall, const CNetConnection<void>* audiosource)
{
	// Clear the fault info
	CLockGuard::CallLocked(m_SetAudioParamsLock, [&] { m_setAudioParamsDone = false;} );

	if (!IsConnected())
	{
		return false;
	}

	auto peer = GetPeerAddress();
	LPCTSTR peerName(peer == nullptr ? _T("Unknown") : peer->ai_canonname);

	SDfCtrlMsg ctrlMsg;
	ctrlMsg.hdr.msgType = SDfCtrlMsg::INTERNAL_DF_CTRL;
	ctrlMsg.hdr.msgSubType = SDfCtrlMsg::SET_SLAVE_AUDIO_PARMS;
	ctrlMsg.hdr.sourceAddr = 0;
	ctrlMsg.hdr.destAddr = 0;
	ctrlMsg.hdr.bodySize = sizeof(SDfCtrlMsg::SSetSlaveAudioParams);
	ctrlMsg.body.setSlaveAudioParam.freechannels = true;
	ctrlMsg.body.setSlaveAudioParam.audiocmd.channel = channelnum;
	ctrlMsg.body.setSlaveAudioParam.audiosource = audiosource;
	if (freeall)
		ctrlMsg.body.setSlaveAudioParam.audiocmd.anyChannel = true;
	else
		ctrlMsg.body.setSlaveAudioParam.audiocmd.anyChannel = false;
	if (Send(&ctrlMsg, offsetof(SDfCtrlMsg, body)))
	{
		CLog::Log(CLog::INFORMATION, "send free Slave Audio channel %u to %s", peerName);
//		CLog::Log(CLog::INFORMATION, "sent GET_CSMS_FAULT to %s", peerName);	// too verbose
		return true;
	}

	return false;
}

//////////////////////////////////////////////////////////////////////
//
// Send an error message
//
void CDfCtrlNet::SendError(_In_ const SDfCtrlMsg::SHdr& hdr, unsigned long code)
{
	if (GetMode() == CLIENT)
		return;

	SDfCtrlMsg::SHdr msg(hdr);
	msg.msgType = SDfCtrlMsg::SMS_ERROR_REPORT;
	msg.msgSubType = code;
	msg.cmdVersion = 0;
	msg.respVersion = 0;
	msg.bodySize = 0;

	if(Send(&msg, sizeof(msg)))
	{
		CLog::Log(CLog::INFORMATION, "DfControl: sent SMS_ERROR_REPORT %lu", code);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send the message versions
//
void CDfCtrlNet::SendMsgVersions(unsigned long subType) const
{
	auto peer = GetPeerAddress();
	LPCTSTR peerName(peer == nullptr ? _T("Unknown") : peer->ai_canonname);

	size_t numVersions = m_versionMap.size();
	size_t bodySize = offsetof(SDfCtrlMsg::SGetMsgVersionsData, versionData) + numVersions * sizeof(SDfCtrlMsg::SVersionData);
	size_t msgSize = offsetof(SDfCtrlMsg, body) + bodySize;
	std::unique_ptr<SDfCtrlMsg> msg(new(msgSize) SDfCtrlMsg);
	msg->hdr.msgType = SDfCtrlMsg::STATUS_CTRL;
	msg->hdr.msgSubType = subType;
	msg->hdr.sourceAddr = 0;
	msg->hdr.destAddr = 0;
	msg->hdr.bodySize = bodySize;
	msg->body.getMsgVersionsData.count = numVersions;
	auto& versions = msg->body.getMsgVersionsData.versionData;
	size_t i = 0;
	for (auto& it : m_versionMap)
	{
		versions[i].msgId = it.first;
		versions[i++].version = it.second;
	}
	if (const_cast<CDfCtrlNet*>(this)->Send(msg.get(), msgSize))
	{
		CLog::Log(CLog::INFORMATION, "CDfCtrlNet: sent %s to %s", (subType == SDfCtrlMsg::GET_MSG_VERSIONS ? "GET_MSG_VERSIONS" : "GET_MSG_VERSIONS_RESP"), peerName);
	}
	else
	{
		CLog::Log(CLog::INFORMATION, "CDfCtrlNet: failed to send %s to %s", (subType == SDfCtrlMsg::GET_MSG_VERSIONS ? "GET_MSG_VERSIONS" : "GET_MSG_VERSIONS_RESP"), peerName);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Timer Thread Callback
//
void CDfCtrlNet::TimerThread(void)
{
	TRACE("TimerThread: m_timerId = %08lx\n", long(m_timerId));
	if (timer_delete(m_timerId) == -1)
	{
		TRACE("timer_delete failed\n");
	}
	CLog::Log(CLog::ERRORS, "Node %lu timed out waiting for external clock switchover\n", m_remoteNodeId);
	Close();
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Upconvert a message to the latest version
//
void CDfCtrlNet::UpConvert(_Inout_ const SDfCtrlMsg*& msg)
{
	switch(msg->hdr.msgType)
	{
	default:
		THROW_LOGIC_ERROR();
	}

	return;
}
