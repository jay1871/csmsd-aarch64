/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "gps.h"
#include "Config.h"
#include "RWLock.h"

//namespace TciGps
//{
//	bool GetDATE(_Out_ DATE& date);
//}

class CNtpQuery
{
public:
	// Functions
	CNtpQuery(bool dfSlave, const std::string& masterIp) :
		m_sd(-1),
		m_dfSlave(dfSlave),
		m_expectedRefid(dfSlave ? masterIp : "PPS")
	{
		memset(&m_sock, 0, sizeof(m_sock));
		m_sock.sin_family = AF_INET;
		m_sock.sin_port = htons(123);
		if (inet_aton("127.0.0.1", &m_sock.sin_addr) == 0)
		{
			TRACE("CNtpQuery unable to convert address\n");
			return;
		}
		if ((m_sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
		{
			TRACE("CNtpQuery unable to open socket\n");
			return;
		}
		if (fcntl(m_sd, F_SETFL, O_NONBLOCK) != 0)
		{
			TRACE("CNtpQuery failed to set non blocking socket\n");
		}
	}

	~CNtpQuery(void) { if (m_sd >= 0) close(m_sd); }

	std::string GetNtpRef(void) const { return m_expectedRefid; }
	std::string GetVersion(void) const { return m_version; }

	bool GetStatus(float &offsetms)
	{
		memset(&m_sendmsg, 0, sizeof(m_sendmsg));
		m_sendmsg.byte1 =  0x16;	// VN = 2, Mode = 6
		m_sendmsg.byte2 =  2;	// Response = 0; (command) Error = 0; More = 0; Op Code  = 2 (read variables)
		m_sendmsg.sequence = htons(1);
		if (sendto(m_sd, &m_sendmsg, sizeof(m_sendmsg), 0, (sockaddr*)&m_sock, sizeof(m_sock)) < 0)
		{
			TRACE("unable to send command to NTP port\n");
			return false;
		}

		memset(&m_recvmsg, 0, sizeof(m_recvmsg));
		socklen_t socklen = sizeof(m_sock);
		if (recvfrom(m_sd, &m_recvmsg, sizeof(m_recvmsg), 0, (sockaddr*)&m_sock, &socklen) < 0)
		{
			TRACE("unable to recv from ntpd\n");
			return false;
		}

//		printf("cmd = %0x%0x\n", m_recvmsg.byte1, m_recvmsg.byte2);
//		printf("status = %0x%0x\n", m_recvmsg.status1, m_recvmsg.status2);
//		printf("AssocID = %0x\n", m_recvmsg.AssocID);
//		printf("Offset = %0x\n", m_recvmsg.Offset);
//		printf("%s\n\n", m_recvmsg.payload);
//		version="ntpd 4.2.8p2@1.3265 Fri May 15 22:08:31 UTC 2015 (1)",
//		processor="armv7l", system="Linux/3.17.0-xilinx", leap=0, stratum=1,
//		precision=-19, rootdelay=0.000, rootdisp=1.180, refid=PPS,
//		reftime=0xd9ca99dc.fc3c69a1, clock=0xd9ca99e9.16b801a4, peer=25205,
//		tc=4, mintc=3, offset=0.000056, frequency=-1.060, sys_jitter=0.004286,
//		clk_jitter=0.005, clk_wander=0.001

		// Get the version and save it (once) - during constructor
		if (m_version.empty())
		{
			m_version = GetKeyValue("version");
			if (m_version.empty())
			{
				m_version = "ntpd ?.?.?";
			}
			else
			{
				if (m_version.front() == '"')
				{
					m_version.erase(0,1);
					m_version.erase(m_version.size() - 1);
				}
				TRACE("ntp version = %s\n", m_version.c_str());
			}
		}

		if ((m_recvmsg.byte1 & 0x3F) != 0x16 || (m_recvmsg.byte2 & 0xEF) != 0x82)
		{
			TRACE("recvmsg has invalid status word\n");
			return false;
		}

		if ((m_recvmsg.status1 & 0xC0) == 0xC0)
		{
//			TRACE("recvmsg leap indicator is unsynchronized\n");
		}

		// find the refid and offset keys in the payload
		std::string refid = GetKeyValue("refid");
		if (refid.empty())
		{
			TRACE("recvmsg missing refid\n");
			return false;
		}
		std::string offset = GetKeyValue("offset");
		if (offset.empty())
		{
			TRACE("recvmsg missing offset\n");
			return false;
		}
		if (refid != m_expectedRefid)
		{
			TRACE("ntpd not using %s: %s\n", m_expectedRefid.c_str(), refid.c_str());
			return false;
		}
		float off = std::stof(offset);
		offsetms = off;
		if (fabsf(off) > NTPD_OFFSETTOL)	// msec
		{
//			TRACE("ntpd PPS offset too large: %f\n", off);
			return false;
		}
		return true;
	}

private:
	// Constants
	const float NTPD_OFFSETTOL = 0.500f;		// tolerance before declare ntpd synced
	// Types
	struct SNTPMsg
	{
		// RFC-1305 NTP control message format
		unsigned char byte1;	// Version Number: bits 3 - 5; Mode: bits 0 - 2;
		unsigned char byte2;	// Response: bit 7;
								// Error: bit 6;
								// More: bit 5;
								// Op code: bits 0 - 4
		unsigned short sequence;
		unsigned char  status1;  // LI and clock source
		unsigned char  status2;  // count and code
		unsigned short AssocID;
		unsigned short Offset;
		unsigned short Count;
		char payload[468];
		char authenticator[96];
	};

	// Functions
	std::string GetKeyValue(const std::string& key)
	{
		std::string keystr = key + "=";
		char *newstr;
		if ((newstr = (char*)memmem(m_recvmsg.payload, sizeof(m_recvmsg.payload), keystr.c_str(), keystr.size())) == nullptr)
		{
			TRACE("recvmsg missing %s\n", key.c_str());
			return std::string();
		}
		char* valstr = strtok(newstr + keystr.size(), ",");
		if (valstr == nullptr)
		{
			TRACE("recvmsg missing %s\n", key.c_str());
			return std::string();
		}
//		TRACE("%s = %s\n", key.c_str(), valstr);
		return std::string(valstr);
	}

	// Data
	int m_sd;
	bool m_dfSlave;
	std::string m_expectedRefid;
	sockaddr_in m_sock;
	SNTPMsg m_sendmsg;
	SNTPMsg m_recvmsg;
	std::string m_version;
};

class CTciGps
{
public:
protected:
	// Functions
	CTciGps(void);
	~CTciGps(void);
	bool GetTime(double& date) const;
	bool GetGpsTime(std::string& str) const;
	int GetMode(void) const;
	int GetNumSatellites(void) const;
	bool IsNtpSyncd(void) const { return m_ntpPpsSync; }
	std::string GetNtpVersion(void) const { return m_ntpQuery.GetVersion(); }
	std::string GetNtpRef(void) const { return m_ntpQuery.GetNtpRef(); }
	bool GetPosition(double& lat, double& lon, double& alt) const;
	bool GetSpeed(double& speed);
	bool GetTrack(double& track);
	bool IsConnected(void) const;
	double GetOffset(void) const {return m_offsetms; }

private:
	// Types
	struct SGpsData
	{
	    gps_fix_t fix;	/* accumulated PVT data */
	    timestamp_t online;
	    int satellites_used;	/* Number of satellites used in solution */
		int satellites_visible;	/* # of satellites in view */
	};

	// Functions
	bool GpsWait(void);
	void Thread(void);
	template <typename T> static void TraceGps(const T& gpsdata);

	// Data
	CSingleton<const CConfig> m_config;
	bool m_connected;
	double m_fixedLatitude;
	double m_fixedLongitude;
	double m_fixedAltitude;
	CConfig::EGpsAvailable m_gpsAvailable;
	gps_data_t m_gpsdataRaw;
	SGpsData m_gpsdataShared;
	std::atomic_bool m_ntpPpsSync;
	CNtpQuery m_ntpQuery;
	mutable CRWLock m_rwLock;
	bool m_shutdown;
	std::condition_variable m_shutdownCond;
	mutable std::mutex m_shutdownMutex;
	std::thread m_thread;
	float m_offsetms;
};

//#define TRACE_TCIGPS
/////////////////////////////////////////////////////////////////////
//
// Print debug output
//
#ifndef TRACE_TCIGPS
template <typename T> void CTciGps::TraceGps(const T&) { return; }
#else
template <typename T> void CTciGps::TraceGps(const T& gpsdata)
{
	if (gpsdata.satellites_visible != 0)
	{
		TRACE("satellites_visible = %d\n", gpsdata.satellites_visible);
	}
	if (gpsdata.satellites_used != 0)
	{
		TRACE("satellites_used = %d\n", gpsdata.satellites_used);
	}
	if (isnan(gpsdata.fix.time) == 0)
	{
		char s[20];
		unix_to_iso8601(gpsdata.fix.time, s, sizeof(s));
		TRACE("fix.time = %s\n", s);
	}
	if (gpsdata.fix.mode >= MODE_2D && isnan(gpsdata.fix.latitude) == 0)
	{
		TRACE("latitude = %.9f\n", gpsdata.fix.latitude);
	}
	if (gpsdata.fix.mode >= MODE_2D && isnan(gpsdata.fix.longitude) == 0)
	{
		TRACE("longitude = %.9f\n", gpsdata.fix.longitude);
	}
	if (gpsdata.fix.mode >= MODE_3D && isnan(gpsdata.fix.altitude) == 0)
	{
		TRACE("altitude = %.1f\n", gpsdata.fix.altitude);
	}
	if (gpsdata.fix.mode >= MODE_2D && isnan(gpsdata.fix.track) == 0)
	{
		TRACE("track = %.1f deg (true)\n", gpsdata.fix.track);
	}
	if (gpsdata.fix.mode >= MODE_2D && isnan(gpsdata.fix.speed) == 0)
	{
		TRACE("speed = %.1f m/s\n", gpsdata.fix.speed);
	}
	if (gpsdata.online == 0)
	{
		TRACE("GPS OFFLINE\n");
	}
	return;
}
#endif

