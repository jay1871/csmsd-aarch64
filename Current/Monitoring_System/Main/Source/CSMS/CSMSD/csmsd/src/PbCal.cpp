/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"

#include "PbCal.h"
#include "Stats.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CPbCal::CPbCal() :
	m_calData(),
//	m_config(),
	m_mruList()
{
	TRACE("CPbCal ctor called\n");
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Add a block of data
//
void CPbCal::AddBlock(Units::Frequency tuneFreq, Units::Frequency rxHwBw, size_t band, Units::Frequency binSize, bool hf, bool direct,
	const Ne10F32cVec& ratio, Ne10F32Vec(&amplitude)[2], bool first, bool last)
{
	// Get iterator (creating entry if necessary)
	SCalKey key = { tuneFreq, rxHwBw, band, hf, direct };
	SCalData data;
	CalData::iterator calData = m_calData.insert(std::make_pair(key, data)).first;

	if (calData->second.numIntegrations.size() == 0)
	{
		// First integration
		calData->second.binSize = binSize;
		calData->second.correction.resize(ratio.size());
		calData->second.amplitude[0].resize(amplitude[0].size());
		calData->second.amplitude[1].resize(amplitude[1].size());
		calData->second.numIntegrations.resize(ratio.size());
		calData->second.correction = Ne10F32cVec::ZERO;
		calData->second.amplitude[0] = 0;
		calData->second.amplitude[1] = 0;
		calData->second.numIntegrations = Ne10F32cVec::ZERO;
		calData->second.status.freq = tuneFreq.GetRaw();
		calData->second.status.rxBw = rxHwBw.GetRaw();
		calData->second.status.band = static_cast<unsigned long>(band);
		calData->second.status.hf = hf;
		calData->second.status.avgRelAmp = 0;
		calData->second.status.stdDevRelAmp = 0;
		calData->second.status.avgRelPhase = 0;
		calData->second.status.stdDevRelPhase = 0;
		calData->second.status.numIntegrations = 0;
		calData->second.status.status = ErrorCodes::UNCALIBRATED;
	}
	else if (first)
	{
		// Prepare for accumulation
		calData->second.correction *= calData->second.numIntegrations;
		calData->second.amplitude[0] *= float(calData->second.status.numIntegrations);
		calData->second.amplitude[1] *= float(calData->second.status.numIntegrations);
	}

	// Accumulate
	ASSERT(calData->second.binSize == binSize);
	ASSERT(calData->second.correction.size() == ratio.size());
	ASSERT(calData->second.amplitude[0].size() == amplitude[0].size());
	ASSERT(calData->second.amplitude[1].size() == amplitude[1].size());
	calData->second.correction += ratio;
	calData->second.amplitude[0] += amplitude[0];
	calData->second.amplitude[1] += amplitude[1];

	// Update number of integrations
	for (size_t bin = 0; bin < ratio.size(); ++bin)
	{
		if (ratio[bin].r != 0 || ratio[bin].i != 0)
		{
			calData->second.numIntegrations[bin].r += 1;
		}
	}

	if (calData->second.status.numIntegrations < 0xffffffff)
	{
		++calData->second.status.numIntegrations;
	}

	if (last)
	{
		// Last integration (for now)
		ne10sThreshold_LTVal(calData->second.numIntegrations, 1, Ne10F32cVec::ONE); // Avoid divide by zero
		calData->second.correction /= calData->second.numIntegrations;
		calData->second.amplitude[0] /= float(calData->second.status.numIntegrations);
		calData->second.amplitude[1] /= float(calData->second.status.numIntegrations);

		// Debug output
#if PBCAL_DEBUG == 1
		auto fp = fopen("/media/TCI/csms/log/pbcal.txt", "a");
		if (fp)
		{
			fprintf(fp, "calData freq = %f bw = %f\n", calData->first.freq.Hz<double>(), calData->first.rxBw.Hz<double>());
			for (size_t i = 0; i < calData->second.correction.size(); ++i)
			{
				fprintf(fp, "corr: %5u %12.6f %12.6f %12.6f\n", i, calData->second.correction[i].r, calData->second.correction[i].i,
						Units::R2D * atan2f(calData->second.correction[i].i, calData->second.correction[i].r));
			}
		}
		fclose(fp);
#endif
		// Summarize
		CalcPbCalStats(calData->second.correction, calData->first.rxBw, calData->second.status);
#if 0
		// TODO: REMOVE THIS CODE AFTER DEBUGGING
		// Simulate linear slope in phase due to time delay of 5 nsec
		float bwbin = calData->first.rxBw.Hz<float>() / calData->second.correction.size();
		float phasex = 0;
		ne10sTone_Direct(calData->second.correction, 1.0f, 5.e-9f * bwbin, phasex);
		// Add a random phase error
		for (size_t i = 0; i < calData->second.correction.size(); ++i)
		{
			float phaserr = (drand48() - 0.5) * 5.0 * sqrt(12.);	// uniform stdev = 5
			ne10_fft_cpx_float32_t zerr = { cosf(Units::D2R * phaserr), sinf(Units::D2R * phaserr) };
			calData->second.correction[i] = calData->second.correction[i] * zerr;
		}
		CalcPbCalStats(calData->second.correction, calData->first.rxBw, calData->second.status);
#endif
		//if (Units::Frequency(calData->second.status.freq).Hz<double>() >= 6889999999) // debug only
		//	calData->second.status.stdDevRelPhase = 37;
		if (calData->second.status.stdDevRelAmp < 6 && calData->second.status.stdDevRelPhase < 30)
		{
			calData->second.status.status = ErrorCodes::SUCCESS;
		}

		// DEBUG OUTPUT
		auto& d1 = calData->first;
		auto& d2 = calData->second;
		printf("%f %f %d %d %u %f %f %f %f %f  %lu %d\n", d1.freq.Hz<double>(), d1.rxBw.Hz<double>(), d1.hf, d1.direct, d1.band,
			d2.status.avgRelAmp, d2.status.avgRelPhase, d2.status.stdDevRelAmp, d2.status.stdDevRelPhase, d2.status.avgRelDelayNsec,
			d2.status.numIntegrations, d2.status.status);
	}

	return;

}


//////////////////////////////////////////////////////////////////////
//
// Calculate the statistics from the pbal correction data
//
void CPbCal::CalcPbCalStats(const Ne10F32cVec& correction, Units::Frequency rxBw, SEquipCtrlMsg::SInitializeDfResp::SPbCalStatus& status)
{
#ifdef PBCAL_ORIGINAL_CALC
	auto sum = correction.sum();
	status.avgRelPhase = atan2(sum.i, sum.r) * Units::R2D;

	Ne10F32Vec mag;
	ne10sMagnitude(correction, mag);

	// Circular standard deviation (per Fisher, 1993)
	status.stdDevRelPhase = sqrt(-2 * log(sqrt(sum.r * sum.r + sum.i * sum.i) / mag.sum())) * Units::R2D;
#else
	float bwbin = rxBw.Hz<float>() / correction.size();

	// Calculate the lag 1 auto-correlation from the correction data
	Ne10F32cVec res(1);
	ne10sAutoCorr(correction, res, 1);

	// Convert to delay
	float delayXbw = atan2f(res[0].i, res[0].r) / Units::TWO_PI;
	status.avgRelDelayNsec = delayXbw / bwbin * 1.e9;

	// Adjust the correction data by the delay, maintaining the same mean value of the phase
	Ne10F32cVec adjust(correction.size());
	float phase = Units::PI * delayXbw * (adjust.size() - 1);
	ne10sTone_Direct(adjust, 1.0f, -delayXbw, phase);
	adjust *= correction;

	// Calculate the stats
	auto sum = adjust.sum();
	status.avgRelPhase = atan2(sum.i, sum.r) * Units::R2D;

	Ne10F32Vec mag;
	ne10sMagnitude(adjust, mag);

	// Circular standard deviation (per Fisher, 1993)
	float temp = sqrtf(sum.r * sum.r + sum.i * sum.i) / mag.sum();
	status.stdDevRelPhase = (temp < 1.0f ? sqrtf(-2 * logf(temp)) * Units::R2D : 0);
#endif
	ne10sLn(mag);
	mag *= 10 / log(10.0f);
	status.avgRelAmp = mag.mean();
	ne10sStdDev(mag, status.stdDevRelAmp);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Clear the cal data
//
void CPbCal::Clear(void)
{
	m_calData.clear();
	m_mruList.clear();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the overall and detailed status
//
bool CPbCal::GetStatus(std::vector<SEquipCtrlMsg::SInitializeDfResp::SPbCalStatus>& status) const
{
	status.clear();

	if (m_calData.empty())
	{
		return false;
	}

	bool overall = true;

	for (CalData::const_iterator calData = m_calData.begin(); calData != m_calData.end(); ++calData)
	{
		if (Units::Frequency(calData->second.status.freq).Hz<double>() < 6890000001 && Units::Frequency(calData->second.status.freq).Hz<double>() > 6889999999) // always pass 6890 Mhz for now
			continue;
		else if (calData->second.status.status != ErrorCodes::SUCCESS)
		{
			overall = false;
		}

		status.push_back(calData->second.status);
	}

	return overall;
}

bool CPbCal::CheckifPbCalStarted()
{
	if (m_calData.empty())
	{
		return false;
	}
	else
		return true;
}


//////////////////////////////////////////////////////////////////////
//
// Weak comparison operator for map key, crafted so that keys are
//  equivalent if they need the same calibration data
//
bool CPbCal::CCalKeyLess::operator()(_In_ const SCalKey& a, _In_ const SCalKey& b) const
{
	// Different HF modes
	if (a.hf && !b.hf)
	{
		return true;
	}
	else if (!a.hf && b.hf)
	{
		return false;
	}

	if (a.direct && !b.direct)
	{
		return true;
	}
	else if (!a.direct && b.direct)
	{
		return false;
	}

	if (a.band < b.band)
	{
		return true;
	}
	else if (b.band < a.band)
	{
		return false;
	}

	if (a.rxBw < b.rxBw)
	{
		return true;
	}
	else if (b.rxBw < a.rxBw)
	{
		return false;
	}

	if (a.freq < b.freq)
	{
		return true;
	}
	else if (b.freq < a.freq)
	{
		return false;
	}
	return false;
}

