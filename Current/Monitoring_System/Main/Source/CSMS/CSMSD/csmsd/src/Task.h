/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once
#include "Config.h"
#include "CsmsLicense.h"
#include "DfCtrlMsg.h"
#include "EquipCtrlMsg.h"
#include "RadioEquip.h"

template <typename T> class CNetConnection;

class CTask :
	public std::enable_shared_from_this<CTask>
{
	// Friends
	friend class CAvdTask;
	friend class CMeasurementTask;
	friend class CSuspendableTask;
	friend class CEquipControl;

public:
	// Types
	enum class ETaskGroup : int { PAN, MEASURE, BKGD, NUM_TASKGROUP, SYSTEM = NUM_TASKGROUP };
	struct SBlockState
	{
		Units::FreqPair freqLimits;
		Units::Frequency radioFreq;		// Actual radio (2630) frequency
		float gainAdj;				// Power ratio Adj.
		float gainAdjdB;			// Gain adjustment in dB (used to record SHF Extension Gain).
		bool inverted;
		size_t pbCalBand;
		bool pbCalDirect;
		unsigned char rcvrAtten;
		unsigned long sampleCount;
		Units::Timestamp sampleTime;
		float switchTemp;
		size_t rfGainTableIdx;
		long ifGainTableIdx;
		float rxGain;
		float gainAdjTemp;
		bool withTerm;
		unsigned long slaveBufferKey;
		std::vector<size_t> slaveSentIndex;
		bool failedDf;
		unsigned char slaveAtten;
		unsigned long slaveTermBufferKey;
		std::vector<size_t> slaveTermSentIndex;

		SBlockState(void) : gainAdjdB(0.0f) {}
	};

	typedef std::vector<CConfig::SProcParams> ProcParams;
	typedef std::shared_ptr<CTask> Task;
	typedef std::vector<CConfig::STaskParams> TaskParams;

	struct SRestartData
	{
		SRestartData(void) : pos(0) {}
		size_t Read(void* const variable, size_t size)
		{
			if (data.size() < pos + size)
			{
//				TRACE("SRestartData::Read failed @ %u len %u total %u\n", pos, size, data.size());
				return 0;
			}
			memcpy(variable, &data[pos], size);
//			TRACE("SRestartData::Read @ %u len %u total %u\n", pos, size, data.size());
			pos += size;
			return size;
		}

		size_t pos;
		std::vector<unsigned char> data;
	};

	// Constants

	// Functions
	CTask(const SEquipCtrlMsg* msg, CNetConnection<void>* source,
		SEquipCtrlMsg::EAnt ant = SEquipCtrlMsg::ANT1);
	CTask(const CTask& task, const Task& parent);
	virtual ~CTask(void);
	CTask& operator=(const CTask& right);
	virtual void AbandonDwell(void);
	bool AtDwellEnd(void) const { return m_endOfDwell; }
	bool AtEnd(void) const
	{
//		printf("AtEnd: %u %u %u %u\n", m_currentStep, m_lastStep, m_currentBlock, m_taskParams[m_currentStep].numBlocks);
		return m_endOfDwell && m_currentStep == m_lastStep &&
			(m_taskParams[m_currentStep].numBlocks == 0 || m_currentBlock == m_taskParams[m_currentStep].numBlocks - 1);
	}
	bool AtStepEnd(void) const { return m_endOfDwell && InLastBlock(); }
	SBlockState& BlockState(void) { return m_blockState; }
	static void CleanUp(void);
	void ClearEndOfDwell(void) { m_endOfDwell = false; }
	bool Completed(void) const { std::lock_guard<std::mutex> lock(m_taskIdMutex); return m_blockState.sampleTime.GetDATE() > const_cast<volatile DATE&>(m_completionTime); }
	static void DeleteTask(_In_ const SEquipCtrlMsg::STaskIdKey& taskIdKey);
	static Task FindTask(_In_ const SEquipCtrlMsg::STaskIdKey& taskIdKey);
	bool GetAbandonDwell(void) { return m_abandonDwell; }
	const TaskParams& GetAllTaskParams(void) const { return m_taskParams; }
	size_t GetBlock(void) const { return m_currentBlock; }
	const SBlockState& GetBlockState(void) const { return m_blockState; }
	CConfig::EPolarization GetVHCurrentPolarization(void) const {
		return (m_currentBlock & 1) ? CConfig::EPolarization::HORIZ : CConfig::EPolarization::VERT;
	}
	SEquipCtrlMsg::EAnt GetCmdAnt(void) const {return(m_cmdAnt);}
	static SEquipCtrlMsg::EAnt GetEAntValue(SEquipCtrlMsg::EAnt eAnt, CConfig::EPolarization pol);
	Units::Frequency GetFirstChanFreq(size_t step, size_t block) const;
	Units::Frequency GetFirstChanFreq(void) const { return GetFirstChanFreq(m_currentStep, m_currentBlock); }
	void GetMeasureSteps(size_t& firstMeasureStep, size_t& numMeasureSteps) const;
	size_t GetNumBlocks(void) const { return m_taskParams[m_currentStep].numBlocks; }
	size_t GetNumSteps(void) const { return m_taskParams.size(); }
	Task GetParent(void) const { return m_parent; }
	Units::Frequency GetProcBw(size_t step) const { return GetProcBw(m_taskParams[step], m_procParams[step].rxProcBw); }
	Units::Frequency GetProcBw(void) const { return GetProcBw(m_taskParams[m_currentStep], m_procParams[m_currentStep].rxProcBw); }
	Units::Frequency GetProcOffset(void) const;
	CConfig::SProcParams& GetProcParams(void) { return m_procParams[m_currentStep]; }
	const CConfig::SProcParams& GetProcParams(void) const { return m_procParams[m_currentStep]; }
	void GetStartStopFreq(Units::Frequency & startFreq, Units::Frequency & stopFreq) const;
	size_t GetStep(void) const { return m_currentStep; }
	ETaskGroup GetTaskGroup(void) const { return m_taskGroup; }
	const CConfig::STaskParams& GetTaskParams(void) const { return m_taskParams[m_currentStep]; }
	Units::Frequency GetTuneFreq(size_t step, size_t block) const;
	Units::Frequency GetTuneFreq(void) const { return GetTuneFreq(m_currentStep, m_currentBlock); }
	bool InLastBlock(void) const { return m_taskParams[m_currentStep].numBlocks == 0 || m_currentBlock == m_taskParams[m_currentStep].numBlocks - 1; }
	void NextBlock(void);
	void NextStep(void);
	void RegisterTask(bool addToMap);
	virtual unsigned long RestartDataType(void) const { return 0; }
	bool ReportNow(void) const { return m_reportInterval > 0 && m_blockState.sampleTime.GetDATE() > m_reportTime; }
	virtual void SaveRestartData(_Inout_ std::vector<unsigned char>& data) const;
	void SetAbandonDwell(bool state) { m_abandonDwell = state; }
	void SetEndOfDwell(void) { m_endOfDwell = true; }
	void SetEndOfTask(void)
		{ m_endOfDwell = true; m_currentStep = m_taskParams.size() - 1;
			m_currentBlock = (m_taskParams[m_currentStep].numBlocks == 0 ? 0 : m_taskParams[m_currentStep].numBlocks - 1); }
	void SetStartStopFreq(Units::Frequency startFreq, Units::Frequency stopFreq);
	Task SpawnTask(void);
	bool Terminated(void) const { std::lock_guard<std::mutex> lock(m_taskIdMutex); return const_cast<volatile DATE&>(m_completionTime) == 0; }
	void UpdateReportTime(void);
	bool UsingDf(size_t step) const;
	bool UsingDf(void) const { return UsingDf(m_currentStep); }

	// Data
	std::chrono::milliseconds m_agcTime;
	SEquipCtrlMsg::EAnt m_ant;

	///@note: Need to save the specified antenna in the cmd message because
	///		  the above m_ant is used to specify the RF input, while m_cmdAnt
	///		  usually has the same value as m_ant except the case when the
	///		  single RF input is connected to multiple antennas that client can
	///		  dynamically specified.
	SEquipCtrlMsg::EAnt m_cmdAnt;
	DATE m_completionTime;
	SEquipCtrlMsg::SHdr m_msgHdr;
	Task m_parent;
	Units::Frequency m_powerBw;
	unsigned char m_rcvrAtten;
	DATE m_reportInterval;
	CNetConnection<void>* m_source;
	bool m_spurCal;
	bool m_spurCalEnabled;
	SEquipCtrlMsg::STaskIdKey m_taskIdKey;
//	mutable std::mutex m_taskStateMutex;		// protects m_suspended, m_completionTime, ... for this task  TODO: rwlock?

private:
	// Types
	struct BandLess :
		public std::binary_function<const CConfig::STaskParams, const CConfig::STaskParams, bool>
	{
		bool operator()(const CConfig::STaskParams& left, const CConfig::STaskParams& right) const
		{
			if (left.bw == right.bw && left.pol == right.pol)
			{
				return left.stopFreq < right.startFreq;
			}
			else if (left.bw < right.bw)
			{
				return true;
			}
			else if (left.bw == right.bw && left.pol < right.pol)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	};

protected:
	// Types
	typedef std::map<unsigned long, Task> TaskMap;
	typedef std::set<CConfig::STaskParams, BandLess> ExcludeBands;

	CTask(_In_ SRestartData& restartData);
	void AdjustFreqLimits(_Inout_ CConfig::STaskParams& taskParams);
	template<typename T> void ReadData(_In_ SRestartData& restartData, _Out_ T& variable);
	template<typename T, typename U> void ReadData(_In_ SRestartData& restartData, _Out_ std::map<T, U>& map);
	template<typename T> void ReadData(_In_ SRestartData& restartData, _Out_ std::vector<T>& vector);
	template<typename T> void ReadData(_In_ SRestartData& restartData, _Out_ std::vector<std::vector<T> >& matrix);
	static void RegisterTask(_In_ const Task& task);
	template<typename T> static void SaveData(_Inout_ std::vector<unsigned char>& buffer, _In_ const T& variable);
	template<typename T, typename U> static void SaveData(_Inout_ std::vector<unsigned char>& buffer, const std::map<T, U>& map);
	template<typename T> static void SaveData(_Inout_ std::vector<unsigned char>& buffer, _In_ const std::vector<T>& vector);
	template<typename T> static void SaveData(_Inout_ std::vector<unsigned char>& buffer, _In_ const std::vector<std::vector<T> >& matrix);
	static void TaskIdKeyCheck(const SEquipCtrlMsg::STaskIdKey& taskIdKey);
	bool ValidateFrequency(Units::Frequency freq, Units::Frequency bw, bool isHorizontal, bool isDf,
		SEquipCtrlMsg::EAnt ant, bool forceVuhf = false) const;
	bool ValidateFrequencyRange(Units::Frequency freq1, Units::Frequency freq2,
		bool isHorizontal, bool isDf, SEquipCtrlMsg::EAnt ant, bool forceVuhf = false) const;

	// Data
	bool m_abandonDwell;
	SBlockState m_blockState;
	CSingleton<CConfig> m_config;
	size_t m_currentBlock;
	size_t m_currentStep;
	ExcludeBands m_excludeBands;
	size_t m_firstStep;
	size_t m_lastStep;
	CSingleton<CCsmsLicense> m_license;
	ProcParams m_procParams;
	DATE m_reportTime;
	static std::mutex m_taskIdMutex;		// protects m_nextTaskId, m_suspended, m_completionTime
	static TaskMap m_taskMap;
	TaskParams m_taskParams;

private:
	// Functions
	void AdjustShfExtPanTaskParams(CConfig::STaskParams & taskParams,
	                               Units::Frequency freq, Units::Frequency bw);
	void BuildExcludeBands(void);
	void CheckAntenna(void) const { CheckAntenna(m_ant, GetTaskParams().hf); }
	void CheckAntenna(SEquipCtrlMsg::EAnt ant, bool hf) const;
	static Units::Frequency GetProcBw(const CConfig::STaskParams& taskParams, Units::Frequency rxProcBw);
	void InitAntCtrl(const SEquipCtrlMsg* msg);
	void InitAvdCtrl(const SEquipCtrlMsg* msg);
	void InitBistCtrl(const SEquipCtrlMsg* msg);
	void InitDemodCtrl(const SEquipCtrlMsg* msg);
	void InitDfCtrl(const SEquipCtrlMsg* msg);
	void InitSounderCtrl(const SEquipCtrlMsg* msg);
	void InitDspCtrl(const SEquipCtrlMsg* msg);
	void InitEquipmentCtrl(const SEquipCtrlMsg* msg);
	void InitMetricsCtrl(const SEquipCtrlMsg* msg);
	void InitOccupancyCtrl(const SEquipCtrlMsg* msg);
	void InitOccupancyDfCtrl(const SEquipCtrlMsg* msg);
	void InitPanDispCtrl(const SEquipCtrlMsg* msg);
	void InitStatusCtrl(const SEquipCtrlMsg* msg);
	void InitWbDigitizerSample(const SEquipCtrlMsg* msg);
	template<typename T> static bool RangeCheck(T test, T low, T high) { return test >= low && test <= high; }
	void UpdatePan(Units::Frequency freq, Units::Frequency bw);
	void UpdatePan(void) { if (m_taskParams.empty()) return; UpdatePan(m_taskParams[0].startFreq, m_taskParams[0].bw); }

	// Data
	bool m_endOfDwell;
	const CConfig::SMutableConfig& m_mutableConfig; // Must be after m_config
	ETaskGroup m_taskGroup;
	static unsigned long m_nextTaskId;
	CSingleton<CRadioEquip> m_radioEquip;
};

//////////////////////////////////////////////////////////////////////
///<summary>
/// Get the current-step start & stop frequency.
///</summary>
///
/// <param name="startFreq">
///@param[out] On exit will have the value of
///		m_taskParams[m_currentStep].startFreq.
/// </param>
///
/// <param name="stopFreq">
///@param[out] On exit will have the value of
///		m_taskParams[m_currentStep].stopFreq.
/// </param>
///
inline void CTask::GetStartStopFreq(Units::Frequency & startFreq, Units::Frequency & stopFreq) const
{
	startFreq = m_taskParams[m_currentStep].startFreq;
	stopFreq = m_taskParams[m_currentStep].stopFreq;
	return;
}

//////////////////////////////////////////////////////////////////////
//
// Read a member variable from a buffer
//
template <typename T> void CTask::ReadData(_In_ SRestartData& restartData, _Out_ T& variable)
{
	if (restartData.Read(&variable, sizeof(T)) != sizeof(T))
	{
		throw ErrorCodes::UNABLETOGETDATA;
	}

//	UpdateChecksum(variable);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Read a std::map from a buffer
//
template<typename T, typename U> void CTask::ReadData(_In_ SRestartData& restartData, _Out_ std::map<T, U>& map)
{
	size_t size;

	if (restartData.Read(&size, sizeof(size)) != sizeof(size))
	{
		throw ErrorCodes::UNABLETOGETDATA;
	}

//	UpdateChecksum(size);

	if (size > 0)
	{
		T key;
		U data;

		for (size_t i = 0; i < size; ++i)
		{
			if (restartData.Read(&key, sizeof(key)) != sizeof(key))
			{
				throw ErrorCodes::UNABLETOGETDATA;
			}

//			UpdateChecksum(key);

			if (restartData.Read(&data, sizeof(data)) != sizeof(data))
			{
				throw ErrorCodes::UNABLETOGETDATA;
			}

//			UpdateChecksum(data);
			map.insert(typename std::map<T, U>::value_type(key, data));
		}
	}
	else
	{
		map.clear();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Read a std::vector of std::vectors from a buffer
//
template<typename T> void CTask::ReadData(_In_ SRestartData& restartData, _Out_ std::vector<std::vector<T> >& matrix)
{
	size_t size;

	if (restartData.Read(&size, sizeof(size)) != sizeof(size))
	{
		throw ErrorCodes::UNABLETOGETDATA;
	}

//	UpdateChecksum(size);
	matrix.resize(size);

	for (size_t i = 0; i < matrix.size(); ++i)
	{
		if (restartData.Read(&size, sizeof(size)) != sizeof(size))
		{
			throw ErrorCodes::UNABLETOGETDATA;
		}

//		UpdateChecksum(size);
		matrix[i].resize(size);

		if (size > 0)
		{
			if (restartData.Read(&matrix[i][0], size * sizeof(T)) != size * sizeof(T))
			{
				throw ErrorCodes::UNABLETOGETDATA;
			}

//			UpdateChecksum(&matrix[i][0], ULONG(size * sizeof(T)));
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Read a std::vector from a buffer
//
template <typename T> void CTask::ReadData(_In_ SRestartData& restartData, _Out_ std::vector<T>& vector)
{
	size_t size;

	if (restartData.Read(&size, sizeof(size)) != sizeof(size))
	{
		throw ErrorCodes::UNABLETOGETDATA;
	}

//	UpdateChecksum(size);
	vector.resize(size);

	if (size > 0)
	{
		if (restartData.Read(&vector[0], size * sizeof(T)) != size * sizeof(T))
		{
			throw ErrorCodes::UNABLETOGETDATA;
		}

//		UpdateChecksum(&vector[0], ULONG(size * sizeof(T)));
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Save a variable to a buffer
//
template<typename T> void CTask::SaveData(_Inout_ std::vector<unsigned char>& buffer, _In_ const T& variable)
{
	size_t end = buffer.size();
	buffer.resize(end + sizeof(T));
	memcpy(&buffer[end], &variable, sizeof(T));
//	TRACE("SaveData @ %u len %u\n", end, sizeof(T));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Save a std::map to a buffer
//
template<typename T, typename U> void CTask::SaveData(_Inout_ std::vector<unsigned char>& buffer, _In_ const std::map<T, U>& map)
{
	size_t end = buffer.size();
	size_t size = map.size();
	buffer.resize(end + sizeof(size) + size * (sizeof(T) + sizeof(U)));
	memcpy(&buffer[end], &size, sizeof(size));
//	TRACE("SaveData @ %u len %u\n", end, sizeof(size));
	end += sizeof(size);

	for (typename std::map<T, U>::const_iterator i = map.begin(); i != map.end(); ++i)
	{
		memcpy(&buffer[end], &i->first, sizeof(T));
//		TRACE("SaveData @ %u len %u\n", end, sizeof(T));
		end += sizeof(T);
		memcpy(&buffer[end], &i->second, sizeof(U));
//		TRACE("SaveData @ %u len %u\n", end, sizeof(U));
		end += sizeof(U);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Save a std::vector to a buffer
//
template<typename T> void CTask::SaveData(_Inout_ std::vector<unsigned char>& buffer, _In_ const std::vector<T>& vector)
{
	size_t end = buffer.size();
	size_t size = vector.size();
	buffer.resize(end + sizeof(size) + size * sizeof(T));
	memcpy(&buffer[end], &size, sizeof(size));
//	TRACE("SaveData @ %u len %u\n", end, sizeof(size));
	end += sizeof(size);

	if (size > 0)
	{
		memcpy(&buffer[end], &vector[0], size * sizeof(T));
//		TRACE("SaveData @ %u len %u\n", end, size * sizeof(T));
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Save a std::vector of std::vectors to a buffer
//
template<typename T> void CTask::SaveData(_Inout_ std::vector<unsigned char>& buffer, _In_ const std::vector<std::vector<T> >& matrix)
{
	size_t end = buffer.size();
	size_t size = matrix.size();
	buffer.resize(end + sizeof(size));
	memcpy(&buffer[end], &size, sizeof(size));
//	TRACE("SaveData @ %u len %u\n", end, sizeof(size));
	end += sizeof(size);

	for (size_t i = 0; i < matrix.size(); ++i)
	{
		size = matrix[i].size();
		buffer.resize(buffer.size() + sizeof(size) + size * sizeof(T));
		memcpy(&buffer[end], &size, sizeof(size));
//		TRACE("SaveData @ %u len %u\n", end, sizeof(size));
		end += sizeof(size);

		if (size > 0)
		{
			memcpy(&buffer[end], &matrix[i][0], size * sizeof(T));
//			TRACE("SaveData @ %u len %u\n", end, size * sizeof(T));
			end += size * sizeof(T);
		}
	}

	return;
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Set the current-step start & stop frequency.
///</summary>
///
/// <param name="startFreq">
///@param[in] Has the current-step start frequency.
/// </param>
///
/// <param name="stopFreq">
///@param[in] Has the current-step stop frequency.
/// </param>
///
inline void CTask::SetStartStopFreq(Units::Frequency startFreq, Units::Frequency stopFreq)
{
	m_taskParams[m_currentStep].startFreq = startFreq;
	m_taskParams[m_currentStep].stopFreq = stopFreq;
	return;
}

