/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "Config.h"
#include "EquipCtrlMsg.h"
#include "RWLock.h"
#include "Singleton.h"
#include "TciGps.h"

class CSerialException : public std::runtime_error
{
private:
	static std::string ErrorStr(void)
	{
		char buffer[256];
		return strerror_r(errno, buffer, sizeof(buffer));
	}
public:
	CSerialException(void) : std::runtime_error(ErrorStr().c_str()) {}
	virtual ~CSerialException(void) throw() {}
};

class CSerial
{
public:
protected:
	// Types
	enum Parity
	{
		NoParity = 0,
		OddParity = 1,
		EvenParity = 2,
		MarkParity = 3,
		SpaceParity = 4
	};

	enum StopBits
	{
		OneStopBit,
		OnePointFiveStopBits,
		TwoStopBits
	};

	enum EReturnVals { BYTE_READ, TIMEOUT_ERR, LINE_ERR };

	CSerial(void) : m_fd(-1), m_timeout10(0) {}
	virtual ~CSerial(void) {}

	void Close(void);
	bool IsOpen(void) const { return m_fd != -1; };
	void Open(const char* port, unsigned int baud);
	EReturnVals Read(unsigned char& result, unsigned int timeoutMsec);
	int Write(const std::string& buf, size_t count);

private:
	int m_fd;
	unsigned int m_timeout10;
};

class CNavigation : public CTciGps, public CSerial
{
	// Friends
	friend class CSingleton<CNavigation>;

public:
	// Types
	typedef std::map<float, float> MastDownCorrections;

	struct SCalStatus
	{
		unsigned short noise;
		unsigned short environment;
		unsigned short count;
	};

	// Functions
	SCalStatus GetCalStatus(void);
	float GetDeclination(bool forceCalc = false) const;
	ErrorCodes::EErrorCode GetGpsResponse(_Out_ SEquipCtrlMsg::SGpsResponse& gpsResponse) const;
	float GetMagHeading(bool hf, Units::Frequency freq) const;
	MastDownCorrections GetMastDownCorrections(void) const;
	bool GetPosition(double& lat, double& lon, double& alt) const;
	float GetRawMagHeading(bool hf, Units::Frequency freq) const;
	bool GetGpsTime(_Out_ std::string& str) const	{ return CTciGps::GetGpsTime(str); }
	ErrorCodes::EErrorCode GetStatus(void) const { std::lock_guard<std::mutex> lock(m_critSect); return m_status; }
	float GetTrueHeading(bool hf, Units::Frequency freq) const;
	bool GpsIsConnected(void) const { return CTciGps::IsConnected(); }
	std::string GetCompassVersion(void) const { return m_version; }
	unsigned long GetNumSatellites(void) const { return CTciGps::GetNumSatellites(); }
	bool IsCompassPresent(void) const { std::lock_guard<std::mutex> lock(m_critSect); return m_status == ErrorCodes::SUCCESS; }
	bool IsNtpSyncd(void) const { return CTciGps::IsNtpSyncd(); }
	std::string GetNtpVersion(void) const { return CTciGps::GetNtpVersion(); }
	std::string GetNtpRef(void) const { return CTciGps::GetNtpRef(); }
	bool MastDown(void) const { std::lock_guard<std::mutex> lock(m_critSect); return m_mastDown; };
	void SetMastDownStatus(bool mastDown);
	float GetOffset(void) const { return CTciGps::GetOffset(); }

private:
	// Constants
	static const size_t NUM_CORR_BINS = 36;
	enum EException { TIMEOUT, ABORTED, LINE_ERROR, CHECKSUM, NOT_IMPLEMENTED, OTHER };
	static const float MAX_CORRECTION;
	static const float MIN_HEADING_CHANGE;
	static const float SMOOTHING_FACTOR;
	static const double SPEED_THRESHOLD;

	// Types
//	typedef std::map<float, float> MastDownCorrections;

	// Functions
	CNavigation(void);
	~CNavigation(void);
	void CompassThread(void);
	void HandleMastStatusCorrections(float& lastGpsHeading, bool& updated);
	static float FixUp(float heading);
	void Init(void);
	static void ParseNmea183(const std::string& line, std::string& talker, std::string& sentence, std::vector<std::string>& data);
	std::string ReadResponse(unsigned int timeout, char end = '\r');
	void ReadC100(void);
	char ReadChar(unsigned int timeout);
	void ReadNmea183(void);
	void SaveCorrections(void);
	void SendCommand(const std::string& cmd);
	void Start(void);
	void Stop(void);

	// Data
	double m_alt;
	CSingleton<const CConfig> m_config;
	const CConfig::SMutableConfig& m_mutableConfig; // Must be after m_config and before m_hfTrueHeading

	mutable std::mutex m_critSect;
	float m_compassHeading;
	mutable float m_declination;
	bool m_fixNoise;
	mutable bool m_goodLocation;
	mutable SEquipCtrlMsg::SGpsResponse m_gpsResponse;
	float m_hfTrueHeading;
	bool m_logged;
	bool m_mastDown;
	MastDownCorrections m_mastDownCorrectionsFromCompass;
	MastDownCorrections m_mastDownCorrectionsFromGps;
	std::string m_mastFile;
	bool m_orientusVersionSet;
//	mutable CRWLock m_rwLock;
	ErrorCodes::EErrorCode m_status;
	bool m_stop;
	std::condition_variable m_stopCond;
	mutable std::mutex m_stopMutex;
	std::thread m_thread;
	float m_ushfTrueHeading[2];
	std::string m_version;
};
