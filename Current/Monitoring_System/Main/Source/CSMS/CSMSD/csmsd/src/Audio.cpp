/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"
#include "Audio.h"
#include "Failure.h"
#include "Processing.h"
#include "HwControl.h"

// Static data
const Units::TimeSpan CAudio::CIqScaleFactor::DEPTH = Units::TimeSpan(10.0);	// 10 seconds for depth of data
const float CAudio::CIqScaleFactor::DEFAULT = 1.0f;	// 1 for default scaleFactor
//static FILE * fp1=nullptr; // debug only
//static long audionum = 0;
//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CAudio::CAudio() :
	m_config(),
	m_critSect(),
	m_demodQueue(),
	m_digitizer(),
	m_enabled(false),
	m_finalIfFreq(-1.),
	m_freqLimits(),
	m_invertRadio(false),
	m_rfFreq(),
	m_scaleFactor()
{
	TRACE("CAudio::CAudio\n");

	auto numAudio = m_digitizer->GetVersionInfo().audioDDCs;
	m_param.resize(numAudio);
	m_IQparam.resize(numAudio);
	m_streamidmaplist.resize(numAudio +1);
	for (int i = 0; i<numAudio; i++)
	{
		m_IQparam[i].streamid = 0; // stream id only used for IQ data
	}
	m_Widebandparam.streamid = 0;
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CAudio::~CAudio()
{
	TRACE("CAudio::~CAudio\n");

	{
		std::lock_guard<std::mutex> lock(m_critSect);
		for (unsigned char audioChannel = 0; audioChannel < m_param.size(); ++audioChannel)
		{
			Stop(C3230::EDemodMode::IQ, audioChannel);
		}
		for (unsigned char audioChannel = 0; audioChannel < m_IQparam.size(); ++audioChannel)
		{
			Stop(C3230::EDemodMode::OFF, audioChannel);
		}
		Stop(C3230::EDemodMode::WIDE_IQ, 0);
	}

//	m_digitizer->UnregisterNewSamplesCallback(1);	// Audio
	m_digitizer->UnregisterNewSamplesCallback(2);	// Realtime IQ
	m_digitizer->UnregisterNewSamplesCallback(3);
	// Stop thread
	m_demodQueue.Close();
	if (m_demodThread.joinable())
	{
		m_demodThread.join();
	}
}


//////////////////////////////////////////////////////////////////////
//
// Allocate an audio channel for a client
//
bool CAudio::AllocateAudioChannel(const CNetConnection<void>* client, Units::Frequency demodFreq, const C3230::SDecimation& decimation,
		C3230::EDemodMode mode, Units::Frequency bw, Units::Frequency bfo, unsigned char& channel, unsigned long streamid)
{
	channel = 0xff;
	std::lock_guard<std::mutex> lock(m_critSect);

	if (mode == C3230::EDemodMode::IQ)
	{
		//debug only
		/*audionum =0; // BC debug only
		if (fp1)
		{
			fclose(fp1); // BC debug only
			fp1 = fopen("/media/audiotest.txt", "w");
		}
		//debug end;
		else
			fp1 = fopen("/media/audiotest.txt", "w"); // BC debug only*/
		for (unsigned char audioChannel = 0; audioChannel < m_IQparam.size(); ++audioChannel)
		{
			if (m_IQparam[audioChannel].client == nullptr)
			{
				// This channel is unallocated
				m_IQparam[audioChannel].client = client;
				m_IQparam[audioChannel].streamid++;
				if (m_IQparam[audioChannel].streamid > 15) // FPGA can only support up to 4 bits so roll back to 0
					m_IQparam[audioChannel].streamid = 0;
				SetDemodParam(audioChannel, demodFreq, decimation, mode, bw, bfo, streamid);
				channel = audioChannel + C3230::m_configData.numAudioChannels; // IQ channel starts with an offset to distinguish demod ch 0 from iq ch 0
				return true;
			}
		}
	}
	else if (mode == C3230::EDemodMode::WIDE_IQ)
	{
		if (m_Widebandparam.client == nullptr)
		{
			// This channel is unallocated
			m_Widebandparam.client = client;
			m_Widebandparam.streamid++;
			if (m_Widebandparam.streamid > 15) // FPGA can only support up to 4 bits so roll back to 0
				m_Widebandparam.streamid = 0;
			//printf("wide iq ch allocated\n");
			SetDemodParam(0, demodFreq, decimation, mode, bw, bfo, streamid);
			channel = C3230::m_configData.numAudioChannels * 2; // Wide IQ channel always set 1 above total iq+demod
			return true;
		}

	}
	else
	{
		for (unsigned char audioChannel = 0; audioChannel < m_param.size(); ++audioChannel)
		{
			if (m_param[audioChannel].client == nullptr)
			{
				// This channel is unallocated
				m_param[audioChannel].client = client;
				//printf("demod ch allocated\n");
				SetDemodParam(audioChannel, demodFreq, decimation, mode, bw, bfo, streamid);
				channel = audioChannel;
				return true;
			}
		}
	}

	// No free channel found
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Demod data handling thread
//
void CAudio::DemodThread(void)
{
	try
	{
		while(true)
		{
			try
			{
				auto msg = std::move(m_demodQueue.GetItem());
				switch (msg->type)
				{
				case SDemodInternalMsg::REALTIMEDATA:
					HandleRealTimeData(static_cast<const SDemodInternalMsg::SRealTimeDataMsg&>(*msg));
					break;

				default:
					TRACE(_T("CAudio::DemodThread retrieved unknown message %d\n"), msg->type);
					break;
				}
			}
			catch(DemodQueue::EProblem& problem)
			{
				switch(problem)
				{
				case DemodQueue::TIMEDOUT:
					// Ignore
					break;

				case DemodQueue::CLOSED:
					// Shutdown
					return;

				default:
					throw std::logic_error("Unexpected CAudio::DemodThread problem");
				}
			}
		}
	}
	catch(...)
	{
		CFailure::OnFail(std::current_exception());
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Enable the audio stream
//
void CAudio::Enable(bool enable)
{
	std::lock_guard<std::mutex> lock(m_critSect);

	if (m_enabled != enable)
	{
		TRACE("CAudio::Enable %d\n", enable);
		m_enabled = enable;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Free the audio on one channel
//
void CAudio::Free(C3230::EDemodMode mode, unsigned char audioChannel)
{
	//printf("BC- Free\n")
	// Lock should be held before calling
	// save following
	//TRACE("CAudio::Free[%u] IQ[%d]\n", audioChannel, isIQ);
	if (mode == C3230::EDemodMode::IQ)
	{
		printf("freeing iq chan %d mode= %d", audioChannel, (int) m_IQparam[audioChannel].params.mode);
		m_IQparam[audioChannel] = SAudioParam(m_IQparam[audioChannel].m_lastframe, m_IQparam[audioChannel].m_packetnumber, m_IQparam[audioChannel].m_framenumber, m_IQparam[audioChannel].streamid);		// Reset to default values (started = false)
		m_IQparam[audioChannel].params.mode = C3230::EDemodMode::IQ; // EDemoMode::Off will be used to turn off demod only
		m_digitizer->SetAudioParams(audioChannel, m_IQparam[audioChannel].params, m_IQparam[audioChannel].started && m_IQparam[audioChannel].enabled && m_enabled, 0, m_IQparam[audioChannel].streamid);
	}
	else if (mode == C3230::EDemodMode::WIDE_IQ)
	{
		//printf("freeing wideband chan %d mode= %d", audioChannel, (int) m_Widebandparam.params.mode);
		m_Widebandparam = SAudioParam(m_Widebandparam.m_lastframe, m_Widebandparam.m_packetnumber, m_Widebandparam.m_framenumber, m_Widebandparam.streamid);		// Reset to default values (started = false)
		m_Widebandparam.params.mode = C3230::EDemodMode::WIDE_IQ; // EDemoMode::Off will be used to turn off demod only
		m_digitizer->SetAudioParams(audioChannel, m_Widebandparam.params, m_Widebandparam.started && m_Widebandparam.enabled && m_enabled, 0, m_Widebandparam.streamid);
	}
	else
	{
		//printf("freeing demod chan %d mode= %d", audioChannel, (int) m_param[audioChannel].params.mode);
		m_param[audioChannel] = SAudioParam(m_param[audioChannel].m_lastframe, m_param[audioChannel].m_packetnumber, m_param[audioChannel].m_framenumber, m_param[audioChannel].streamid);		// Reset to default values (started = false)
		m_digitizer->SetAudioParams(audioChannel, m_param[audioChannel].params, m_param[audioChannel].started && m_param[audioChannel].enabled && m_enabled, 0, m_param[audioChannel].streamid);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Free all the audio for a client
//
void CAudio::Free(const CNetConnection<void>* client)
{
	std::lock_guard<std::mutex> lock(m_critSect);
	for (unsigned char audioChannel = 0; audioChannel < m_IQparam.size(); ++audioChannel)
	{
		if (m_IQparam[audioChannel].client == client)
		{
			Free(C3230::EDemodMode::IQ, audioChannel);
		}
	}

	if (m_Widebandparam.client == client)
		Free(C3230::EDemodMode::WIDE_IQ, 0l);

	for (unsigned char audioChannel = 0; audioChannel < m_param.size(); ++audioChannel)
	{
		if (m_param[audioChannel].client == client)
		{
			Free(C3230::EDemodMode::OFF, audioChannel);
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Free one audio channel for a client
//
bool CAudio::Free(const CNetConnection<void>* client, unsigned char audioChannel)
{
	std::lock_guard<std::mutex> lock(m_critSect);
	int widebandch = C3230::m_configData.numAudioChannels * 2;
	if (audioChannel >= C3230::m_configData.numAudioChannels && audioChannel < widebandch) // IQ channel ch0 had an offset
	{
		unsigned char iqch = audioChannel - C3230::m_configData.numAudioChannels;
		if (m_IQparam[iqch].client == client)
		{
			Free(C3230::EDemodMode::IQ, iqch);
			return true;
		}
	}
	else if (audioChannel == widebandch)
	{
		if (m_Widebandparam.client == client)
		{
			Free(C3230::EDemodMode::WIDE_IQ, 0);
			return true;
		}
	}
	else
	{
		if (m_param[audioChannel].client == client)
		{
			Free(C3230::EDemodMode::OFF, audioChannel);
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Handle realtime data from demod queue
//
void CAudio::HandleRealTimeData(const SDemodInternalMsg::SRealTimeDataMsg& realtimeDataMsg)
{
	try
	{
		CRealtimeNet& realtimeNet = dynamic_cast<CProcessing&>(*this).m_realtimeNet;
		if (realtimeNet.GetNumConnections() > 0)
		{
			// debug
			/*unsigned short numsamples =  static_cast<unsigned short> (std::min<unsigned short>(realtimeDataMsg.data.body.iqDataV4.numSamples, 1024));
			for (unsigned int i = 0; i < numsamples; i++)
			{
				fprintf(fp1, "%08lx\n", realtimeDataMsg.data.body.iqDataV4.SIq.audiosamples[i]);

			}*/
			//debug end

			realtimeNet.SendToAllClients(&realtimeDataMsg.data);
		}
	}
	catch(const std::bad_cast&)
	{
		// Shutting down or not fully constructed
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback for audio samples available when demodulating
//
//void CAudio::OnSamplesDemod(const std::vector<unsigned long>& , unsigned long )
//{
//	return;
//}


//////////////////////////////////////////////////////////////////////
//
// Callback for external demod
//
void CAudio::OnSamplesExt(const std::vector<unsigned long>& samples, unsigned long audioChannel, bool EOS)
{
//	std::lock_guard<std::mutex> lock(m_critSect);		// TODO: Not clear why holding a lock here is necessary
	if(!EOS)
		assert(samples.size() > 2);
//	auto sequence = static_cast<unsigned short>(samples[0] >> 16);
//	printf("CAudio::OnSamplesExt: seq:%u addr:0x%08lx count:%lu channel:%lu\n", sequence, samples[1], count, audioChannel);

	try
	{
		auto& net = dynamic_cast<CProcessing&>(*this).m_realtimeNet;
		if (net.GetNumConnections() > 0)
		{
			// send empty packet if EOS set
			auto header = &samples[0];
			auto sample = &samples[C3230Fdd::CAdmPacketSize::HEADER_SIZE];
			auto count = samples.size() - C3230Fdd::CAdmPacketSize::HEADER_SIZE;
			auto samplesize = count;
			unsigned short framecount = 0;
			unsigned long long sampleOffset = 0;
			if (EOS)
			{
				if (m_config->IsDDRSystem() && (net.GetNumIqClients(4) > 0) && EOS)
				{
					auto realtimeMsg = SDemodInternalMsg::MakeUnique<SDemodInternalMsg::SRealTimeDataMsg>();
					auto& msg = realtimeMsg->data;		// Note: this is the full SSmsRealtimeMsg (up to MAX_IQ samples)

					msg.hdr.msgType = SSmsRealtimeMsg::RT_IQ_DATA;
					msg.hdr.msgTypeVersion = 4;
					msg.hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SIqDataV4, SIq));
					msg.body.iqDataV4.freq = m_IQparam[audioChannel].demodFreq.GetRaw();
					Units::Frequency adjustedbw = Units::Frequency(m_digitizer->GetAudioOutputRate(audioChannel) * 25 / 32);
					msg.body.iqDataV4.actualBW = adjustedbw.GetRaw();
					msg.body.iqDataV4.rxAtten = 0;
					msg.body.iqDataV4.actualSampleRate = m_digitizer->GetAudioOutputRate(audioChannel);
					msg.body.iqDataV4.ddcChannel = audioChannel;
					msg.body.iqDataV4.numSamples = 0;
					//printf("EOS set\n");
					msg.body.iqDataV4.EOS = EOS?1:0;
					msg.body.iqDataV4.inputPort = dynamic_cast<CHwControl&>(*this).GetAntenna();
					msg.body.iqDataV4.dataType = SSmsRealtimeMsg::DataType::DataType_Int16c;
					msg.body.iqDataV4.seqNumber = m_IQparam[audioChannel].m_packetnumber;
					msg.body.iqDataV4.scaleFactor = 1;
					streamidmap stream_map = m_streamidmaplist[audioChannel];
					msg.body.iqDataV4.streamID = stream_map[m_IQparam[audioChannel].streamid];
					m_streamidmaplist[audioChannel].clear();
					Enqueue(realtimeMsg, 0);
				}
				return;

			}

			while (count > 0)
			{
				unsigned short numIq = static_cast<unsigned short>(std::min<unsigned int>(count, SSmsRealtimeMsg::SIqData::MAX_IQ));
				if (net.GetNumIqClients(1) > 0)
				{	// Version 1
					auto realtimeMsg = SDemodInternalMsg::MakeUnique<SDemodInternalMsg::SRealTimeDataMsg>();
					auto& msg = realtimeMsg->data;		// Note: this is the full SSmsRealtimeMsg (up to MAX_IQ samples)
					msg.hdr.msgType = SSmsRealtimeMsg::RT_IQ_DATA;
					msg.hdr.msgTypeVersion = 1;
					msg.hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SIqData, iq) + numIq * sizeof(SSmsRealtimeMsg::SIqData::SIq));
					msg.body.iqData.freq = 0;
					msg.body.iqData.bw = 0;
					msg.body.iqData.rxAtten = 0;
					msg.body.iqData.sampleRate = static_cast<unsigned long>(m_digitizer->GetAudioOutputRate(audioChannel));
					msg.body.iqData.dateTime = 0;
					msg.body.iqData.scale = 0;
					msg.body.iqData.rxChan = audioChannel;
					msg.body.iqData.numIq = numIq;
					memcpy(msg.body.iqData.iq, sample, numIq * sizeof(SSmsRealtimeMsg::SIqData::SIq));
					Enqueue(realtimeMsg, 0);		// Can drop messages here!
				}
				if (net.GetNumIqClients(2) > 0)
				{	// Version 2
					auto realtimeMsg = SDemodInternalMsg::MakeUnique<SDemodInternalMsg::SRealTimeDataMsg>();
					auto& msg = realtimeMsg->data;		// Note: this is the full SSmsRealtimeMsg (up to MAX_IQ samples)
					msg.hdr.msgType = SSmsRealtimeMsg::RT_IQ_DATA;
					msg.hdr.msgTypeVersion = 2;
					msg.hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SIqDataV2, iq) + numIq * sizeof(SSmsRealtimeMsg::SIqDataV2::SIq));
					msg.body.iqDataV2.freq = Units::Frequency(0).GetRaw();
					msg.body.iqDataV2.bw = Units::Frequency(0).GetRaw();
					msg.body.iqDataV2.rxAtten = 0;
					msg.body.iqDataV2.sampleRate = static_cast<unsigned long>(m_digitizer->GetAudioOutputRate(audioChannel));
					msg.body.iqDataV2.dateTime = 0;
					msg.body.iqDataV2.scale = 0;
					msg.body.iqDataV2.rxChan = audioChannel;
					msg.body.iqDataV2.numIq = numIq;
					memcpy(msg.body.iqDataV2.iq, sample, numIq * sizeof(SSmsRealtimeMsg::SIqDataV2::SIq));
					Enqueue(realtimeMsg, 0);		// Can drop messages here!
				}
				if (net.GetNumIqClients(3) > 0)
				{	// Version 3
					auto realtimeMsg = SDemodInternalMsg::MakeUnique<SDemodInternalMsg::SRealTimeDataMsg>();
					auto& msg = realtimeMsg->data;		// Note: this is the full SSmsRealtimeMsg (up to MAX_IQ samples)
					msg.hdr.msgType = SSmsRealtimeMsg::RT_IQ_DATA;
					msg.hdr.msgTypeVersion = 3;
					msg.hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SIqDataV3, iq) + numIq * sizeof(SSmsRealtimeMsg::SIqDataV3::SIq));
					msg.body.iqDataV3.freq = Units::Frequency(0).GetRaw();
					msg.body.iqDataV3.bw = Units::Frequency(0).GetRaw();
					msg.body.iqDataV3.rxAtten = 0;
					msg.body.iqDataV3.sampleRate = static_cast<unsigned long>(m_digitizer->GetAudioOutputRate(audioChannel));
					msg.body.iqDataV3.dateTime = 0;
					msg.body.iqDataV3.scale = 0;
					msg.body.iqDataV3.rxChan = audioChannel;
					msg.body.iqDataV3.numIq = numIq;
					memcpy(msg.body.iqDataV3.iq, sample, numIq * sizeof(SSmsRealtimeMsg::SIqDataV3::SIq));
					Enqueue(realtimeMsg, 0);		// Can drop messages here!
				}
				if (m_config->IsDDRSystem() && (net.GetNumIqClients(4) > 0))
				{
					if (m_streamidmaplist.size() > 0 && m_streamidmaplist[audioChannel].empty())
						break;
					// Version 4
					auto realtimeMsg = SDemodInternalMsg::MakeUnique<SDemodInternalMsg::SRealTimeDataMsg>();
					auto& msg = realtimeMsg->data;		// Note: this is the full SSmsRealtimeMsg (up to MAX_IQ samples)

					msg.hdr.msgType = SSmsRealtimeMsg::RT_IQ_DATA;
					msg.hdr.msgTypeVersion = 4;
					msg.hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SIqDataV4, SIq) + numIq * sizeof(SSmsRealtimeMsg::TciInt16c));
					//msg.hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SIqDataV4, SIq) + numIq * sizeof(unsigned long)); // debug only
					msg.body.iqDataV4.freq = m_IQparam[audioChannel].demodFreq.GetRaw();
					Units::Frequency adjustedbw = Units::Frequency(m_digitizer->GetAudioOutputRate(audioChannel) * 25 / 32);
					msg.body.iqDataV4.actualBW = adjustedbw.GetRaw();
					msg.body.iqDataV4.rxAtten = 0;
					msg.body.iqDataV4.actualSampleRate = m_digitizer->GetAudioOutputRate(audioChannel);
					msg.body.iqDataV4.ddcChannel = audioChannel;
					msg.body.iqDataV4.numSamples = numIq;
					msg.body.iqDataV4.EOS = EOS?1:0;
					msg.body.iqDataV4.inputPort = dynamic_cast<CHwControl&>(*this).GetAntenna();
					msg.body.iqDataV4.dataType = SSmsRealtimeMsg::DataType::DataType_Int16c;
					framecount =  (header[0] >> 16);
					// debug only simulate drop
					//if (m_IQparam[audioChannel].m_packetnumber == 50)
					//	framecount++;
					// debug end
					msg.body.iqDataV4.seqNumber = m_IQparam[audioChannel].m_packetnumber;

					auto framediff = framecount - m_IQparam[audioChannel].m_lastframe;
					if (framediff > 0)
					{
						m_IQparam[audioChannel].m_packetnumber += framediff;
					}
					else // frame may roll over back to 0
					{
						m_IQparam[audioChannel].m_packetnumber++;
					}

					msg.body.iqDataV4.sampleOffset = (m_IQparam[audioChannel].m_framenumber * samplesize) + sampleOffset;
					sampleOffset+=numIq;
					unsigned long sampleRate = C3230::ADC_CLOCK_RATE;
					unsigned long long processdelay = (unsigned long long ) (((unsigned long long ) header[4] << 32) / sampleRate);
					unsigned long long ppsfraction = (unsigned long long ) (((unsigned long long ) (header[3] & 0x1FFFFFFF) << 32) /  sampleRate);
					unsigned long long ppsseconds = (unsigned long long )  ((unsigned long long ) header[2] << 32);
					Units::Timestamp timestamp;
					timestamp.m_timestamp = ppsseconds + processdelay + ppsfraction;
					msg.body.iqDataV4.streamStartTime = timestamp.GetRaw();
					streamidmap stream_map = m_streamidmaplist[audioChannel];
					msg.body.iqDataV4.streamID = stream_map[(unsigned long) ((header[0] & 0x0000F000) >> 12)];
					Units::Timestamp msgStartTime;
					msgStartTime.m_timestamp = ppsseconds + ppsfraction + (msg.body.iqDataV4.sampleOffset << 32) /
						static_cast<unsigned long long>(msg.body.iqDataV4.actualSampleRate);
					msg.body.iqDataV4.scaleFactor = GetScaleFactor(msgStartTime);
					memcpy(msg.body.iqDataV4.SIq.samplesInt16, sample, numIq * sizeof(SSmsRealtimeMsg::TciInt16c));
					//memcpy(msg.body.iqDataV4.SIq.audiosamples, sample, numIq * sizeof(unsigned long)); // debug only
					Enqueue(realtimeMsg, 0);		// Can drop messages here!
					//TRACE("iq streaming data\n");
				}
				sample += numIq;
				count -= numIq;
			}
			auto framediff = framecount - m_IQparam[audioChannel].m_lastframe;
			if (framediff > 0)
			{
				m_IQparam[audioChannel].m_framenumber += framediff;
			}
			else // frame may roll over back to 0
			{
				m_IQparam[audioChannel].m_framenumber++;
			}
			m_IQparam[audioChannel].m_lastframe = framecount;
		}
	}
	catch(const std::bad_cast&)
	{
		// Shutting down or not fully constructed
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback for WidebandDDC
//
void CAudio::OnSamplesWidebandDDC(const std::vector<unsigned long>& samples, unsigned long audioChannel, bool EOS)
{
//	std::lock_guard<std::mutex> lock(m_critSect);		// TODO: Not clear why holding a lock here is necessary
	if(!EOS)
		assert(samples.size() > 2);
//	auto sequence = static_cast<unsigned short>(samples[0] >> 16);
//	printf("CAudio::OnSamplesExt: seq:%u addr:0x%08lx count:%lu channel:%lu\n", sequence, samples[1], count, audioChannel);

	try
	{
		auto& net = dynamic_cast<CProcessing&>(*this).m_realtimeNet;
		if (net.GetNumConnections() > 0)
		{
			// send empty packet if EOS set
			auto header = &samples[0];
			auto sample = &samples[C3230Fdd::CAdmPacketSize::HEADER_SIZE];
			auto count = samples.size() - C3230Fdd::CAdmPacketSize::HEADER_SIZE;
			auto samplesize = count;
			unsigned short framecount = 0;
			unsigned long long sampleOffset = 0;

			while (count > 0)
			{
				unsigned short numIq = static_cast<unsigned short>(std::min<unsigned int>(count, SSmsRealtimeMsg::SIqData::MAX_IQ));
				if (net.GetNumIqClients(1) > 0)
				{	// Version 1
					auto realtimeMsg = SDemodInternalMsg::MakeUnique<SDemodInternalMsg::SRealTimeDataMsg>();
					auto& msg = realtimeMsg->data;		// Note: this is the full SSmsRealtimeMsg (up to MAX_IQ samples)
					msg.hdr.msgType = SSmsRealtimeMsg::RT_IQ_DATA;
					msg.hdr.msgTypeVersion = 1;
					msg.hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SIqData, iq) + numIq * sizeof(SSmsRealtimeMsg::SIqData::SIq));
					msg.body.iqData.freq = 0;
					msg.body.iqData.bw = 0;
					msg.body.iqData.rxAtten = 0;
					msg.body.iqData.sampleRate = static_cast<unsigned long>(m_digitizer->GetAudioOutputRate(audioChannel));
					msg.body.iqData.dateTime = 0;
					msg.body.iqData.scale = 0;
					msg.body.iqData.rxChan = audioChannel;
					msg.body.iqData.numIq = numIq;
					memcpy(msg.body.iqData.iq, sample, numIq * sizeof(SSmsRealtimeMsg::SIqData::SIq));
					Enqueue(realtimeMsg, 0);		// Can drop messages here!
				}
				if (net.GetNumIqClients(2) > 0)
				{	// Version 2
					auto realtimeMsg = SDemodInternalMsg::MakeUnique<SDemodInternalMsg::SRealTimeDataMsg>();
					auto& msg = realtimeMsg->data;		// Note: this is the full SSmsRealtimeMsg (up to MAX_IQ samples)
					msg.hdr.msgType = SSmsRealtimeMsg::RT_IQ_DATA;
					msg.hdr.msgTypeVersion = 2;
					msg.hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SIqDataV2, iq) + numIq * sizeof(SSmsRealtimeMsg::SIqDataV2::SIq));
					msg.body.iqDataV2.freq = Units::Frequency(0).GetRaw();
					msg.body.iqDataV2.bw = Units::Frequency(0).GetRaw();
					msg.body.iqDataV2.rxAtten = 0;
					msg.body.iqDataV2.sampleRate = static_cast<unsigned long>(m_digitizer->GetAudioOutputRate(audioChannel));
					msg.body.iqDataV2.dateTime = 0;
					msg.body.iqDataV2.scale = 0;
					msg.body.iqDataV2.rxChan = audioChannel;
					msg.body.iqDataV2.numIq = numIq;
					memcpy(msg.body.iqDataV2.iq, sample, numIq * sizeof(SSmsRealtimeMsg::SIqDataV2::SIq));
					Enqueue(realtimeMsg, 0);		// Can drop messages here!
				}
				if (net.GetNumIqClients(3) > 0)
				{	// Version 3
					auto realtimeMsg = SDemodInternalMsg::MakeUnique<SDemodInternalMsg::SRealTimeDataMsg>();
					auto& msg = realtimeMsg->data;		// Note: this is the full SSmsRealtimeMsg (up to MAX_IQ samples)
					msg.hdr.msgType = SSmsRealtimeMsg::RT_IQ_DATA;
					msg.hdr.msgTypeVersion = 3;
					msg.hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SIqDataV3, iq) + numIq * sizeof(SSmsRealtimeMsg::SIqDataV3::SIq));
					msg.body.iqDataV3.freq = Units::Frequency(0).GetRaw();
					msg.body.iqDataV3.bw = Units::Frequency(0).GetRaw();
					msg.body.iqDataV3.rxAtten = 0;
					msg.body.iqDataV3.sampleRate = static_cast<unsigned long>(m_digitizer->GetAudioOutputRate(audioChannel));
					msg.body.iqDataV3.dateTime = 0;
					msg.body.iqDataV3.scale = 0;
					msg.body.iqDataV3.rxChan = audioChannel;
					msg.body.iqDataV3.numIq = numIq;
					memcpy(msg.body.iqDataV3.iq, sample, numIq * sizeof(SSmsRealtimeMsg::SIqDataV3::SIq));
					Enqueue(realtimeMsg, 0);		// Can drop messages here!
				}
				if (m_config->IsDDRSystem() && (net.GetNumIqClients(4) > 0))
				{
					//if (m_streamidmaplist.size() > 0  && m_streamidmaplist[m_streamidmaplist.size()-1].empty())
					//	break;
					// Version 4
					auto realtimeMsg = SDemodInternalMsg::MakeUnique<SDemodInternalMsg::SRealTimeDataMsg>();
					auto& msg = realtimeMsg->data;		// Note: this is the full SSmsRealtimeMsg (up to MAX_IQ samples)

					msg.hdr.msgType = SSmsRealtimeMsg::RT_IQ_DATA;
					msg.hdr.msgTypeVersion = 4;
					msg.hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SIqDataV4, SIq) + numIq * sizeof(SSmsRealtimeMsg::TciInt16c));
					//msg.hdr.bodySize = static_cast<unsigned long>(offsetof(SSmsRealtimeMsg::SIqDataV4, SIq) + numIq * sizeof(unsigned long)); // debug only
					msg.body.iqDataV4.freq = m_Widebandparam.demodFreq.GetRaw();
					Units::Frequency adjustedbw = Units::Frequency(m_digitizer->GetAudioOutputRate(audioChannel) * 25 / 32);
					msg.body.iqDataV4.actualBW = adjustedbw.GetRaw();
					msg.body.iqDataV4.rxAtten = 0;
					msg.body.iqDataV4.actualSampleRate = m_digitizer->GetAudioOutputRate(audioChannel);
					msg.body.iqDataV4.ddcChannel = audioChannel;
					msg.body.iqDataV4.numSamples = numIq;
					msg.body.iqDataV4.EOS = EOS?1:0;
					msg.body.iqDataV4.inputPort = dynamic_cast<CHwControl&>(*this).GetAntenna();
					msg.body.iqDataV4.dataType = SSmsRealtimeMsg::DataType::DataType_Int16c;
					framecount =  (header[0] >> 16);
					// debug only simulate drop
					//if (m_Widebandparam.m_packetnumber == 50)
					//	framecount++;
					// debug end
					msg.body.iqDataV4.seqNumber = m_Widebandparam.m_packetnumber;

					auto framediff = framecount - m_Widebandparam.m_lastframe;
					if (framediff > 0)
					{
						m_Widebandparam.m_packetnumber += framediff;
					}
					else // frame may roll over back to 0
					{
						m_Widebandparam.m_packetnumber++;
					}

					msg.body.iqDataV4.sampleOffset = (m_Widebandparam.m_framenumber * samplesize) + sampleOffset;
					sampleOffset+=numIq;
					unsigned long sampleRate = C3230::ADC_CLOCK_RATE;
					unsigned long long processdelay = (unsigned long long ) (((unsigned long long ) header[4] << 32) / sampleRate);
					unsigned long long ppsfraction = (unsigned long long ) (((unsigned long long ) (header[3] & 0x1FFFFFFF) << 32) /  sampleRate);
					unsigned long long ppsseconds = (unsigned long long )  ((unsigned long long ) header[2] << 32);
					Units::Timestamp timestamp;
					timestamp.m_timestamp = ppsseconds + processdelay + ppsfraction;
					msg.body.iqDataV4.streamStartTime = timestamp.GetRaw();
					streamidmap stream_map = m_streamidmaplist[m_streamidmaplist.size()-1];
					msg.body.iqDataV4.streamID = stream_map[(unsigned long) ((header[0] & 0x0000F000) >> 12)];
					Units::Timestamp msgStartTime;
					msgStartTime.m_timestamp = ppsseconds + ppsfraction + (msg.body.iqDataV4.sampleOffset << 32) /
						static_cast<unsigned long long>(msg.body.iqDataV4.actualSampleRate);
					msg.body.iqDataV4.scaleFactor = GetScaleFactor(msgStartTime);
					memcpy(msg.body.iqDataV4.SIq.samplesInt16, sample, numIq * sizeof(SSmsRealtimeMsg::TciInt16c));
					//memcpy(msg.body.iqDataV4.SIq.audiosamples, sample, numIq * sizeof(unsigned long)); // debug only
					Enqueue(realtimeMsg, 0);		// Can drop messages here!
					//TRACE("iq streaming data\n");
				}
				sample += numIq;
				count -= numIq;
			}
			auto framediff = framecount - m_Widebandparam.m_lastframe;
			if (framediff > 0)
			{
				m_Widebandparam.m_framenumber += framediff;
			}
			else // frame may roll over back to 0
			{
				m_Widebandparam.m_framenumber++;
			}
			m_Widebandparam.m_lastframe = framecount;
		}
	}
	catch(const std::bad_cast&)
	{
		// Shutting down or not fully constructed
	}

	return;
}

//////////////////////////////////////////////////////////////////////
//
// Set the demodulation parameters
//
bool CAudio::SetDemodParams(const CNetConnection<void>* client, unsigned char audioChannel, Units::Frequency demodFreq,
	const C3230::SDecimation& decimation, C3230::EDemodMode mode, Units::Frequency bw, Units::Frequency bfo, unsigned long streamid)
{
	std::lock_guard<std::mutex> lock(m_critSect);

	if (mode ==  C3230::EDemodMode::IQ || audioChannel >= C3230::m_configData.numAudioChannels)
	{
		unsigned iqch = audioChannel - C3230::m_configData.numAudioChannels;
		if (m_IQparam[iqch].client == client)
		{
			TRACE("CAudio::SetDemodParams[%u]\n", iqch);
			SetDemodParam(iqch, demodFreq, decimation, mode, bw, bfo, streamid);
			return true;
		}
	}
	else if (mode == C3230::EDemodMode::WIDE_IQ)
	{
		if (m_Widebandparam.client == client)
		{
			TRACE("CAudio::SetDemodParams[%u]\n", audioChannel);
			SetDemodParam(audioChannel, demodFreq, decimation, mode, bw, bfo, streamid);
			return true;
		}
	}
	else
	{
		if (m_param[audioChannel].client == client)
		{
			TRACE("CAudio::SetDemodParams[%u]\n", audioChannel);
			SetDemodParam(audioChannel, demodFreq, decimation, mode, bw, bfo, streamid);
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Start the audio channel
//
void CAudio::Start(C3230::EDemodMode mode, unsigned char audioChannel)
{
	// Lock should be held before calling
	if (mode == C3230::EDemodMode::IQ)
	{
		if (!m_IQparam[audioChannel].started)
		{
			TRACE("CAudio::Start[%u]\n", audioChannel);
			m_IQparam[audioChannel].started = true;
			m_IQparam[audioChannel].m_framenumber = 0;
			m_IQparam[audioChannel].m_packetnumber = 0;
			m_IQparam[audioChannel].m_lastframe = 0;
		}
	}
	else if (mode == C3230::EDemodMode::WIDE_IQ)
	{
		if (!m_Widebandparam.started)
		{
			TRACE("CAudio::Start[%u]\n", audioChannel);
			m_Widebandparam.started = true;
			m_Widebandparam.m_framenumber = 0;
			m_Widebandparam.m_packetnumber = 0;
			m_Widebandparam.m_lastframe = 0;
		}
	}
	else
	{
		if (!m_param[audioChannel].started)
		{
			TRACE("CAudio::Start[%u]\n", audioChannel);
			m_param[audioChannel].started = true;
			m_param[audioChannel].m_framenumber = 0;
			m_param[audioChannel].m_packetnumber = 0;
			m_param[audioChannel].m_lastframe = 0;
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Start all the audio channel threads.
//
void CAudio::Startup(void)
{
	m_demodThread = std::thread(&CAudio::DemodThread, this);

//	m_digitizer->RegisterNewSamplesCallback(OnSamplesDemod, this, 1);	// Audio
	m_digitizer->RegisterNewSamplesCallback(OnSamplesExt, this, 2);		// Realtime IQ
	m_digitizer->RegisterNewSamplesCallback(OnSamplesWidebandDDC, this, 3);		// Realtime Wideband IQ
	return;

}


//////////////////////////////////////////////////////////////////////
//
// Stop the audio on one channel
//
void CAudio::Stop(C3230::EDemodMode mode, unsigned char audioChannel)
{
	//printf("BC-stop\n");
	// Lock should be held before calling
	if (mode == C3230::EDemodMode::IQ)
	{
		if (m_IQparam[audioChannel].started)
		{
			TRACE("CAudio::Stop[%u]\n", audioChannel);
			m_IQparam[audioChannel].started = false;
			m_digitizer->SetAudioParams(audioChannel, m_IQparam[audioChannel].params, m_IQparam[audioChannel].started && m_IQparam[audioChannel].enabled && m_enabled, 0, m_IQparam[audioChannel].streamid);
		}

	}
	else if (mode == C3230::EDemodMode::WIDE_IQ)
	{
		if (m_Widebandparam.started)
		{
				TRACE("CAudio::Stop[%u]\n", audioChannel);
				m_Widebandparam.started = false;
				m_digitizer->SetAudioParams(audioChannel, m_Widebandparam.params, m_Widebandparam.started && m_Widebandparam.enabled && m_enabled, 0, m_Widebandparam.streamid);
		}
	}
	else
	{
		if (m_param[audioChannel].started)
		{
			TRACE("CAudio::Stop[%u]\n", audioChannel);
			m_param[audioChannel].started = false;
			m_digitizer->SetAudioParams(audioChannel, m_param[audioChannel].params, m_param[audioChannel].started && m_param[audioChannel].enabled && m_enabled, 0, m_param[audioChannel].streamid);
		}

	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Tune the digitizer demod channels
//
void CAudio::TuneDemods(Units::Frequency rfFreq, Units::Frequency finalIfFreq, bool invertRadio, bool invertSpectrumData, const Units::FreqPair& freqLimits)
{
	std::lock_guard<std::mutex> lock(m_critSect);
	m_finalIfFreq = finalIfFreq;
	m_rfFreq = rfFreq;
	m_invertRadio = invertRadio;
	m_freqLimits = freqLimits;
	bool skipTune = false;

	for (unsigned char audioChannel = 0; audioChannel < m_param.size(); ++audioChannel)
	{
		//printf("BC-tune demod ch %u\n", audioChannel);

		if (m_param[audioChannel].client != nullptr &&
			m_param[audioChannel].demodFreq >= freqLimits.first && m_param[audioChannel].demodFreq <= freqLimits.second)
		{
			auto demodFreq = m_param[audioChannel].demodFreq;
			auto& params = m_param[audioChannel].params;
			Units::Frequency procFreq = finalIfFreq + (invertRadio ? rfFreq - demodFreq : demodFreq - rfFreq);
			params.procFreq = procFreq;
			params.invertSpectrumData = invertSpectrumData;
			m_param[audioChannel].enabled = true;
		}
		else
		{
			if (!m_param[audioChannel].enabled && (m_IQparam[audioChannel].enabled || m_Widebandparam.enabled))
				skipTune = true;

			m_param[audioChannel].enabled = false;
		}
		if (!skipTune)
			m_digitizer->SetAudioParams(audioChannel, m_param[audioChannel].params, m_param[audioChannel].started && m_param[audioChannel].enabled && m_enabled, 0, m_param[audioChannel].streamid);
	}

	/*for (unsigned char audioChannel = 0; audioChannel < m_IQparam.size(); ++audioChannel)
	{
		if (m_IQparam[audioChannel].client != nullptr &&
				m_IQparam[audioChannel].demodFreq >= freqLimits.first && m_IQparam[audioChannel].demodFreq <= freqLimits.second)
		{
			auto demodFreq = m_IQparam[audioChannel].demodFreq;
			auto& params = m_IQparam[audioChannel].params;
			Units::Frequency procFreq = finalIfFreq + (invertRadio ? rfFreq - demodFreq : demodFreq - rfFreq);
			params.procFreq = procFreq;
			params.invertSpectrumData = invertSpectrumData;
			m_IQparam[audioChannel].enabled = true;
		}
		else
		{
			m_IQparam[audioChannel].enabled = false;
		}
		printf("BC-tune iq ch %u %d\n", audioChannel, m_enabled);
		m_digitizer->SetAudioParams(audioChannel, m_IQparam[audioChannel].params, m_IQparam[audioChannel].started && m_IQparam[audioChannel].enabled && m_enabled, 0, m_IQparam[audioChannel].streamid);
	}*/
	return;
}


#if 0
//////////////////////////////////////////////////////////////////////
//
// Update the audio tune frequency
//
void CAudio::TuneAudioChannel(unsigned char audioChannel, Units::Frequency procFreq, bool invertSpectrumData)
{
	auto& params = m_param[audioChannel].params;

	// Lock should be held before calling
	if (procFreq != params.procFreq || invertSpectrumData != params.invertSpectrumData)
	{
		params.procFreq = procFreq;
		params.invertSpectrumData = invertSpectrumData;
	}

	m_digitizer->SetAudioParams(audioChannel, m_param[audioChannel].params, m_param[audioChannel].started && m_enabled, 0);

	return;
}
#endif

//////////////////////////////////////////////////////////////////////
//
// Update the audio state
//
void CAudio::SetDemodParam(unsigned char audioChannel, Units::Frequency demodFreq, const C3230::SDecimation& decimation, C3230::EDemodMode mode,
	Units::Frequency bw, Units::Frequency bfo, unsigned long streamid)
{
	//printf("BC-tune SetDemodParam\n");
	bool isIQ = (mode == C3230::EDemodMode::IQ);
	C3230::SAudioParams *params = nullptr;
	if (mode == C3230::EDemodMode::IQ)
	{
		params = &(m_IQparam[audioChannel].params);
	}
	else if (mode == C3230::EDemodMode::WIDE_IQ)
	{
		params = &m_Widebandparam.params;
	}
	else
	{
		params = &(m_param[audioChannel].params);
	}

	if (mode != params->mode)
	{
		if (mode == C3230::EDemodMode::IQ || params->mode == C3230::EDemodMode::IQ ||
			mode == C3230::EDemodMode::OFF)
		{
			//Stop(isIQ, audioChannel);		// Changing to or from external (iq) "demod" or changing to OFF
		}

		params->mode = mode;
	}

	params->bw = bw;
	params->decimation = decimation;
	if (isIQ)
	{
		streamidmap stream_map = m_streamidmaplist[audioChannel];
		stream_map[m_IQparam[audioChannel].streamid] = streamid;
		m_streamidmaplist[audioChannel] = stream_map;
	}
	else if (mode == C3230::EDemodMode::WIDE_IQ)
	{
		streamidmap stream_map = m_streamidmaplist[m_streamidmaplist.size()-1];
		stream_map[m_Widebandparam.streamid] = streamid;
		m_streamidmaplist[m_streamidmaplist.size()-1] = stream_map;
	}

	if (bfo != params->bfo)
	{
		if (mode == C3230::EDemodMode::CW)
		{
			params->bfo = bfo;
		}
	}

	if (params->mode == C3230::EDemodMode::USB)
	{
		// frequency in the command is fCarrier + bw / 2. This demod requires the demodFreq to be re-centered.
		demodFreq -= params->bw / 2;
	}
	else if (params->mode == C3230::EDemodMode::LSB)
	{
		// frequency in the command is fCarrier - bw / 2. This demod requires the demodFreq to be re-centered.
		demodFreq += params->bw / 2;
	}

	if (isIQ)
	{
		if (demodFreq != m_IQparam[audioChannel].demodFreq)
		{
			m_IQparam[audioChannel].demodFreq = demodFreq;

			if (m_finalIfFreq > 0 &&
					m_IQparam[audioChannel].demodFreq >= m_freqLimits.first && m_IQparam[audioChannel].demodFreq <= m_freqLimits.second)
			{
				Units::Frequency procFreq = m_finalIfFreq +
					(m_invertRadio ? m_rfFreq - m_IQparam[audioChannel].demodFreq
										   : m_IQparam[audioChannel].demodFreq - m_rfFreq);
				params->procFreq = procFreq;
				m_IQparam[audioChannel].enabled = true;
			}
			else
			{
				m_IQparam[audioChannel].enabled = false;
			}
		}
		if (mode != C3230::EDemodMode::OFF)
		{
			Start(mode, audioChannel);
		}
		//m_IQparam[audioChannel].params.mode = C3230::EDemodMode::IQ;
		m_digitizer->SetAudioParams(audioChannel, m_IQparam[audioChannel].params, m_IQparam[audioChannel].started && m_IQparam[audioChannel].enabled && m_enabled, 0, m_IQparam[audioChannel].streamid);

	}
	else if (mode == C3230::EDemodMode::WIDE_IQ)
	{
		if (demodFreq != m_Widebandparam.demodFreq)
		{
			m_Widebandparam.demodFreq = demodFreq;

			if (m_finalIfFreq > 0 &&
					m_Widebandparam.demodFreq >= m_freqLimits.first && m_Widebandparam.demodFreq <= m_freqLimits.second)
			{
				Units::Frequency procFreq = m_finalIfFreq +
					(m_invertRadio ? m_rfFreq - m_Widebandparam.demodFreq
										   : m_Widebandparam.demodFreq - m_rfFreq);
				params->procFreq = procFreq;
				m_Widebandparam.enabled = true;
			}
			else
			{
				m_Widebandparam.enabled = false;
			}
		}
		if (mode != C3230::EDemodMode::OFF)
		{
			Start(mode, 0);
		}
		//m_Widebandparam.params.mode = C3230::EDemodMode::WideIQ;  // EDemoMode::Off changed to indicate Demod off only
		//printf("wide ddc--set digit audio params\n");
		m_digitizer->SetAudioParams(audioChannel, m_Widebandparam.params, m_Widebandparam.started && m_Widebandparam.enabled && m_enabled, 0, m_Widebandparam.streamid);
	}
	else
	{
		if (demodFreq != m_param[audioChannel].demodFreq)
		{
			m_param[audioChannel].demodFreq = demodFreq;

			if (m_finalIfFreq > 0 &&
				m_param[audioChannel].demodFreq >= m_freqLimits.first && m_param[audioChannel].demodFreq <= m_freqLimits.second)
			{
				Units::Frequency procFreq = m_finalIfFreq +
					(m_invertRadio ? m_rfFreq - m_param[audioChannel].demodFreq
										   : m_param[audioChannel].demodFreq - m_rfFreq);
				params->procFreq = procFreq;
				m_param[audioChannel].enabled = true;
			}
			else
			{
				m_param[audioChannel].enabled = false;
			}
		}
		if (mode != C3230::EDemodMode::OFF)
		{
			Start(mode, audioChannel);
		}
		//printf("demod--set digit audio params\n");
		m_digitizer->SetAudioParams(audioChannel, m_param[audioChannel].params, m_param[audioChannel].started && m_param[audioChannel].enabled && m_enabled, 0, m_param[audioChannel].streamid);
	}

	return;
}


/////////////////////Class CIqScaleFactor/////////////////////////////

//////////////////////////////////////////////////////////////////////
//
// Add a scale factor entry, purging old entries if needed
//
void CAudio::CIqScaleFactor::Add(Units::Timestamp scaleTime, float scaleFactor)
{
	std::lock_guard<std::mutex> lock(mtx);

	// First, remove any old entries from the beginning
	auto it = factors.begin();
	while (it != factors.end())
	{
		if (scaleTime - it->scaleTime < DEPTH)
		{
			break;
		}

		factors.pop_front();
		it = factors.begin();
	}

	// Then, add new entry to the back
	SIqScaleFactor f;
	f.scaleTime = scaleTime;
	f.scaleFactor = scaleFactor;
	factors.push_back(f);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get a scale factor entry, purging old entries if needed
//
float CAudio::CIqScaleFactor::Get(Units::Timestamp scaleTime)
{
	float scaleFactor = DEFAULT;

	std::lock_guard<std::mutex> lock(mtx);

	auto it = factors.begin();
	while (it != factors.end() && scaleTime >= it->scaleTime)
	{
		// This entry is a candidate since its timestamp is earlier than the one we're looking for
		scaleFactor = it->scaleFactor;

		// If we found one previously (at begin()), it needs to be removed
		if (it != factors.begin())
		{
			factors.pop_front();	// Makes this entry now the first entry
			it = factors.begin();	// Points to this entry again.
		}
		++it;
	}

	/*if (scaleFactor == DEFAULT)
	{
		printf("CIqScaleFactor Get %s %15.5e\n", scaleTime.Format(true).c_str(), scaleFactor);
		int i = 0;
		for (auto& it : factors)
		{
			printf("CIqScaleFactor Get: %d %s %15.5e\n", ++i, it.scaleTime.Format(false).c_str(), it.scaleFactor);
		}
	}*/
	return scaleFactor;
}


