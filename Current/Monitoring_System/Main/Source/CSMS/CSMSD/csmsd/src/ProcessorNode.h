/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "Bist.h"
#include "Config.h"
#include "EquipCtrlMsg.h"
#include "TaggedQueue.h"

class CDfCtrlNet;
class CVCPCtrlNet;
struct SDfCtrlMsg;

class CProcessorNode
{
public:
	// Types
	struct SDfDataQueue		// shared by all client instances
	{
		CTaggedQueueDf taggedDfDataQ;
		mutable std::mutex dfDataMutex;
		std::unique_ptr<NTaggedQueue::SDfData> dfData;	// Holds data as it arrives
	};


	typedef std::unique_ptr<SDfCtrlMsg> SDfCtrlMsgPtr;
	typedef std::unique_ptr<NTaggedQueue::SDfData> SDfDataPtr;

	// Functions
	CProcessorNode(void);
	~CProcessorNode(void);
	void AddSlaveDfData(const SDfCtrlMsg* msg, const char* peername);
	bool AreNodesBistDone(const std::vector<size_t>& sentIndex, std::vector<CBist::SBistResult>& allResults);
	bool AreNodesReady(void);
	void DoDfMaster(void);
	void DoDfSlave(void);
	unsigned long GetNodeId(void) { return m_processorNodes.nodeId; }
	bool IsNodeFaultDone(size_t nodeIndex, SDfCtrlMsg::SGetSlaveCsmsFaultResp& resp);
	bool IsSetAudioParamsDone(size_t nodeIndex, SDfCtrlMsg::SSetSlaveAudioParamsResp& resp);
	std::pair<bool, bool> IsProcessorDf(void) { return std::pair<bool, bool>( IsProcessorDfMaster(), IsProcessorDfSlave()); }
	bool IsProcessorDfMaster(void) { return (m_processorNodes.nodeId == 0 && !m_processorNodes.nodes.empty()); }
	bool IsProcessorMaster(void) { return m_processorNodes.nodeId == 0; }
	bool IsProcessorDfSlave(void) { return m_processorNodes.nodeId != 0; }
	void PingAllNodes(void);
	unsigned long SendToAllNodes(const SDfCtrlMsg& msg, std::vector<size_t>& sentIndex, bool tagged = false);
	int SendBistToAllNodes(std::vector<size_t>& sentIndex);
	int SendGetFaultToAllNodes(std::vector<size_t>& sentMask);
	int SendSetAudioParamsToAllNodes(std::vector<size_t>& sentMask, const C3230::SDecimation& decimations, const SEquipCtrlMsg::SAudioParamsCmd& audiocmd, const CNetConnection<void>* audiosource);
	int FreeSlaveAudioChannelToAllNodes(std::vector<size_t>& sentMask, unsigned long channelnum, bool freeall, const CNetConnection<void>* audiosource);
	bool Send(CDfCtrlNet* client, const SDfCtrlMsg& msg, const char* logStr = nullptr, bool logSuccess = true);
	bool SendToMaster(const SDfCtrlMsg& ctrlMsg, const char* logStr = nullptr, bool logSuccess = true);
	bool GetStartupDoneState(void);
	void SetStartupDone(void);
	bool WaitForAllResponses(const std::vector<size_t>& sentIndex, unsigned long tag, std::vector<SDfCtrlMsgPtr>& responses);
	bool WaitForAllResponses(const std::vector<size_t>& sentIndex, unsigned long tag, SDfDataPtr& responses);
	void ConnectMasterAsClient();
//	static void SetSimulatorForSlave(bool useSimulatorForSlave, bool useExternalClock) {
//		m_useSimulatorForSlave = useSimulatorForSlave;
//		m_useExternalClock = useExternalClock;
//	}
//	static bool UseExternalClock(void) { return m_useExternalClock; }
//	bool UseSimulator(void) { return m_useSimulatorForSlave && m_processorNodes.nodeId > 0; }
private:
	// Constants
	static const char* PORT;
	static const char* PORT2;

	// Data
	CSingleton<CConfig> m_config;
	SDfDataQueue m_dfDataQueue;		// shared by all client instances
	std::shared_ptr<CDfCtrlNet> m_dfServer;
	std::vector< std::shared_ptr<CDfCtrlNet> > m_nodeClients;
	CConfig::SProcessorNodes m_processorNodes;
	unsigned long m_tag;
	std::mutex m_tagLock;
	std::shared_ptr<CVCPCtrlNet> m_vcpServer;
//	static bool m_useSimulatorForSlave;
//	static bool m_useExternalClock;
};

