/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "3230Fdd.h"
#include "51432017.h"
#include "7234201102.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
//const C7234201102::BandSettings C7234201102::BAND_SETTINGS(BAND_SETTINGS_, &BAND_SETTINGS_[std::extent<decltype(BAND_SETTINGS_)>::value]);

const C7234201102::SCalSettings C7234201102::CAL_SETTINGS[] =
{
	{  281250000, -29.0, -35.0, 5.0,
		C3230Fdd::CSequencerAntennaControl::CALTONE_NOISESOURCE | C3230Fdd::CSequencerAntennaControl::CAL0 | C3230Fdd::CSequencerAntennaControl::CAL1 /*0x1c*/ },
	{ 2250000000, -36.0, -41.0, 8.0,
		C3230Fdd::CSequencerAntennaControl::CALTONE_NOISESOURCE | C3230Fdd::CSequencerAntennaControl::CAL1 /*0x18*/ },
	{ 4500000000, -35.0, -44.0, 10.0,
		C3230Fdd::CSequencerAntennaControl::CALTONE_NOISESOURCE | C3230Fdd::CSequencerAntennaControl::CAL0 /*0x14*/ }
};

const C7234201102::ESource C7234201102::GRAY_CODE9[2][9] =
{
	{ ANT_8, ANT_9, ANT_1, ANT_3, ANT_2, ANT_6, ANT_7, ANT_5, ANT_4 },
	{ ANT_8H, ANT_9H, ANT_1H, ANT_3H, ANT_2H, ANT_6H, ANT_7H, ANT_5H, ANT_4H }
//	// Reverse from normal:
//	{ ANT_4, ANT_5, ANT_7, ANT_6, ANT_2, ANT_3, ANT_1, ANT_9, ANT_8 },
//	{ ANT_4H, ANT_5H, ANT_7H, ANT_6H, ANT_2H, ANT_3H, ANT_1H, ANT_9H, ANT_8H }
};

const C7234201102::ESource C7234201102::GRAY_CODE5[5] =
{
//	ANT_5, ANT_1, ANT_2, ANT_4, ANT_3  <= This shows how the 4 elements antenna is connected to the switch input.
	ANT_9, ANT_1, ANT_3, ANT_7, ANT_5,
};

Units::Frequency C7234201102::m_crossOverFreq = 0;
//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C7234201102::C7234201102(void) :
	m_isPresent(false)
{
	auto mutableConfig = CSingleton<const CConfig>()->GetMutableConfig();
	auto antenna = mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna;	// df antenna is always [0]
	m_isPresent = (antenna && CAntenna::IsDfAntenna(antenna.get()));
	if (m_isPresent)
	{
		m_crossOverFreq = CAntenna::GetHighBandCrossoverFreq(antenna.get());
	}
	SetSwitch(0, RECEIVE, TERM, TERM, UHF);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C7234201102::~C7234201102(void)
{
	SetSwitch(0, RECEIVE, TERM, TERM, UHF);

	return;
}


inline unsigned int C7234201102::CalcSettleTime(const unsigned long currCtrlWord, const unsigned long newCtrlWord)
{
	if (newCtrlWord == currCtrlWord)
	{
		return 0;
	}
	else if ( (newCtrlWord & (C3230Fdd::CSequencerAntennaControl::CAL0 | C3230Fdd::CSequencerAntennaControl::CAL1)) != 0 &&
			  (newCtrlWord & C3230Fdd::CSequencerAntennaControl::CALTONE_NOISESOURCE) != 0 &&
			  ( (currCtrlWord & (C3230Fdd::CSequencerAntennaControl::CAL0 | C3230Fdd::CSequencerAntennaControl::CAL1)) == 0 ||
			    (currCtrlWord & C3230Fdd::CSequencerAntennaControl::CALTONE_NOISESOURCE) == 0) )
	{
		// Turning on cal source
		return 40000;
	}
	else if ( (newCtrlWord & (C3230Fdd::CSequencerAntennaControl::CAL0 | C3230Fdd::CSequencerAntennaControl::CAL1)) != 0 &&
			  (newCtrlWord & C3230Fdd::CSequencerAntennaControl::CALTONE_NOISESOURCE) == 0 &&
			  ( (currCtrlWord & (C3230Fdd::CSequencerAntennaControl::CAL0 | C3230Fdd::CSequencerAntennaControl::CAL1)) == 0 ||
			    (currCtrlWord & C3230Fdd::CSequencerAntennaControl::CALTONE_NOISESOURCE) != 0) )
	{
		// Turning on noise source
		return 10000;
	}
//	else if ( (newCtrlByte[1] & 0x08) != 0 && (currCtrlByte[1] & 0x08) == 0 )
//	{
//		// Turning on LO
//		return 15000;
//	}
//	else if ( ((newCtrlByte[1] ^ currCtrlByte[1]) & 0x07) != 0 && (newCtrlByte[1] & 0x08) != 0 )
//	{
//		// Change in LO
//		return 10000;
//	}
	else
	{
		// Default
		return 5;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the closest cal settings
//
float C7234201102::GetCalDbm(size_t index, size_t ch)
{
	return index < std::extent<decltype(CAL_SETTINGS)>::value ? (ch == 0 ? CAL_SETTINGS[index].refDbm : CAL_SETTINGS[index].sampleDbm) : 0;
}


//////////////////////////////////////////////////////////////////////
//
// Get the closest cal settings
//
Units::Frequency C7234201102::GetCalFreq(size_t index)
{
	return index < std::extent<decltype(CAL_SETTINGS)>::value ? CAL_SETTINGS[index].freq : 0;
}


//////////////////////////////////////////////////////////////////////
//
// Get the closest cal settings
//
float C7234201102::GetCalHalfRangeDbm(size_t index)
{
	return index < std::extent<decltype(CAL_SETTINGS)>::value ? CAL_SETTINGS[index].passRangeDbm : 10.0f;
}


//////////////////////////////////////////////////////////////////////
//
// Get the closest cal settings
//
const C7234201102::SCalSettings& C7234201102::GetCalSettings(Units::Frequency calFreq)
{
	size_t i;

	for (i = 0; i < std::extent<decltype(CAL_SETTINGS)>::value - 1; ++i)
	{
		if (calFreq.Hz<double>() < sqrt(CAL_SETTINGS[i].freq.Hz<double>() * CAL_SETTINGS[i + 1].freq.Hz<double>()))
		{
			return CAL_SETTINGS[i];
		}
	}

	return CAL_SETTINGS[i];
}


//////////////////////////////////////////////////////////////////////
//
// Get the control bytes
//
unsigned long C7234201102::GetControlWord(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf, EDfElemBandSel band)
{
	const SCalSettings& calSettings = GetCalSettings(freq);

	// Setting the block converter and cband_sw is currently independent of polarization.
	unsigned long controlWord = (freq <= Units::Frequency(CBAND_AMP_CROSSOVER_HZ) ? 0 : C3230Fdd::CSequencerAntennaControl::CBAND_VUHF_AMP);

	// TODO: This is trying to guess polarization.  Would be better to add additional argument to function for polarization.
	if (((sampleRf <= ANT_9H) && (sampleRf >= ANT_1H)) || refRf == REF_HORIZON)
	{
		// Horizontal polarization is not supported by SHF antenna array.
		band = UHF;
	}
	else
	{
		//Vertical polarization can be measured by either UHF or SHF antenna array.
		if (band == AUTO)
		{
			band = (freq <= m_crossOverFreq ? UHF : SHF);
		}

		if(band == SHF)
		{
			controlWord |= C3230Fdd::CSequencerAntennaControl::CBAND_VUHF_ELEMENT;
		}
	}
	switch(mode)
	{
		case RECEIVE:
		{
			//Default is to use vertical polarization.
			unsigned long pol = C3230Fdd::CSequencerAntennaControl::VERT_HORIZ;

			switch(refRf)
			{
				case TERM:
					ASSERT((sampleRf != BITE) && (sampleRf != NOISE));
					controlWord |= (C3230Fdd::CSequencerAntennaControl::CALTONE_NOISESOURCE | C3230Fdd::CSequencerAntennaControl::CAL_TO_REF);
					break;

				case REF_HORIZON:
					//Save the Horizontal polarization.
					pol = 0;
					// Fall through
				case REF:
					controlWord |= 0;	// C_REFOUT_2_REF_ANTENNA
					break;

				case BITE:
					controlWord |= (calSettings.control | C3230Fdd::CSequencerAntennaControl::CAL_TO_REF);
					break;

				case NOISE:
					controlWord |= (C3230Fdd::CSequencerAntennaControl::CAL_TO_REF | C3230Fdd::CSequencerAntennaControl::CAL0 | C3230Fdd::CSequencerAntennaControl::CAL1);
					break;

				default:
					ASSERT(FALSE);
					//Connect Reference to Terminated for unknown mode.
					controlWord |= (C3230Fdd::CSequencerAntennaControl::CALTONE_NOISESOURCE | C3230Fdd::CSequencerAntennaControl::CAL_TO_REF);
					break;
			}

			switch(sampleRf)
			{
				case ANT_1:
				case ANT_2:
				case ANT_3:
				case ANT_4:
				case ANT_5:
				case ANT_6:
				case ANT_7:
				case ANT_8:
				case ANT_9:
					//Assert if Ref polarization does not match sample.
					ASSERT (pol == C3230Fdd::CSequencerAntennaControl::VERT_HORIZ);
					controlWord |= pol;
					controlWord |= ((static_cast<unsigned char>(sampleRf) << C3230Fdd::CSequencerAntennaControl::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControl::DEFAULT_ANTENNA);
					break;

				case ANT_1H:
				case ANT_2H:
				case ANT_3H:
				case ANT_4H:
				case ANT_5H:
				case ANT_6H:
				case ANT_7H:
				case ANT_8H:
				case ANT_9H:
					//Assert if Ref polarization does not match sample.
					ASSERT (pol == 0);
					controlWord |= pol;
					controlWord |= ((static_cast<unsigned char>(sampleRf) << C3230Fdd::CSequencerAntennaControl::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControl::DEFAULT_ANTENNA);
					break;

				case TERM:
					controlWord |= pol;	// C_TERMINATED_SAMPLE_SELECTION = 0
					break;

				case NOISE:
					ASSERT(refRf != BITE);
					controlWord |= (pol | C3230Fdd::CSequencerAntennaControl::DEFAULT_ANTENNA | C3230Fdd::CSequencerAntennaControl::CAL0 | C3230Fdd::CSequencerAntennaControl::CAL1);
					break;

				case BITE:
					ASSERT(refRf != NOISE);
					controlWord |= (pol | C3230Fdd::CSequencerAntennaControl::DEFAULT_ANTENNA | calSettings.control);
					break;

				default:
					ASSERT(FALSE);
					//Undefine sample selection set it to Terminated.
					controlWord |= pol;	// C_TERMINATED_SAMPLE_SELECTION = 0
					break;
			}
			break;
		}

		case TX_BITE:
		{
			//Default is to use vertical polarization.
			unsigned long pol = C3230Fdd::CSequencerAntennaControl::VERT_HORIZ;
			switch(refRf)
			{
				case TERM:
					controlWord |= (calSettings.control | C3230Fdd::CSequencerAntennaControl::CAL_TO_ANT);
					break;

				case REF_HORIZON:
					pol = 0; // C_HORIZONTAL_VAL
					//fall through.
				case REF:
					ASSERT(FALSE);
					controlWord |= (calSettings.control | C3230Fdd::CSequencerAntennaControl::CAL_TO_ANT);
					break;

				case BITE:
					controlWord |= (calSettings.control | C3230Fdd::CSequencerAntennaControl::CAL_TO_ANT | C3230Fdd::CSequencerAntennaControl::CAL_TO_REF);
					break;

				case NOISE:
					ASSERT(FALSE);
					controlWord |= (calSettings.control | C3230Fdd::CSequencerAntennaControl::CAL_TO_ANT);
					break;

				default:
					ASSERT(FALSE);
					//Undefine Ref setting, set it to CAL tone.
					controlWord |= (calSettings.control | C3230Fdd::CSequencerAntennaControl::CAL_TO_ANT | C3230Fdd::CSequencerAntennaControl::CAL_TO_REF);
					break;
			}

			switch(sampleRf)
			{
				case ANT_1:
				case ANT_2:
				case ANT_3:
				case ANT_4:
				case ANT_5:
				case ANT_6:
				case ANT_7:
				case ANT_8:
				case ANT_9:
					controlWord |= pol;
					controlWord |= ((static_cast<unsigned char>(sampleRf) << C3230Fdd::CSequencerAntennaControl::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControl::DEFAULT_ANTENNA);
					break;

				case ANT_1H:
				case ANT_2H:
				case ANT_3H:
				case ANT_4H:
				case ANT_5H:
				case ANT_6H:
				case ANT_7H:
				case ANT_8H:
				case ANT_9H:
					controlWord |= pol;
					controlWord |= ((static_cast<unsigned char>(sampleRf) << C3230Fdd::CSequencerAntennaControl::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControl::DEFAULT_ANTENNA);
					break;

				case TERM:
					controlWord |= pol; // C_TERMINATED_SAMPLE_SELECTION = 0
					break;

				case NOISE:
					ASSERT(FALSE);
					controlWord |= pol; // C_TERMINATED_SAMPLE_SELECTION = 0
					break;

				case BITE:
					ASSERT(FALSE);
					controlWord |= pol; // C_TERMINATED_SAMPLE_SELECTION = 0
					break;

				default:
					ASSERT(FALSE);
					//Undefine sample selection set it to Terminated.
					controlWord |= pol; // C_TERMINATED_SAMPLE_SELECTION = 0
					break;
			}

			break;
		}
		default:
			ASSERT(FALSE);
			//Undefine reference Antenna mode, set sample and Ref to Terminated.
			controlWord |= (C3230Fdd::CSequencerAntennaControl::VERT_HORIZ | C3230Fdd::CSequencerAntennaControl::CAL_TO_REF | C3230Fdd::CSequencerAntennaControl::CALTONE_NOISESOURCE);
			break;
	}

	return controlWord;
}


//////////////////////////////////////////////////////////////////////
//
// Get the control word (modified current word with pattern[0]
//
unsigned long C7234201102::GetControlWord(const std::vector<unsigned long>& pattern)
{
	unsigned long controlWord;
	GetCurrentControlWord(controlWord);
	if (!pattern.empty())
	{
		controlWord &= ~C3230Fdd::CSequencerAntennaControl::DEFAULT_ANTENNA;	// Clear antenna bits
		controlWord |= ((pattern[0] << C3230Fdd::CSequencerAntennaControl::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControl::DEFAULT_ANTENNA);
	}
	return controlWord;
}


//////////////////////////////////////////////////////////////////////
//
// Set the switch
//
void C7234201102::SetSwitch(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf, EDfElemBandSel band)
{
	if (!m_isPresent)
	{
		return;
	}
	unsigned long controlWord = GetControlWord(freq, mode, refRf, sampleRf, band);
	unsigned long currCtrlWord;
	{
		std::lock_guard<std::mutex> lock(m_critSect);
		if (controlWord == m_controlWord)
			return;

		try
		{
			dynamic_cast<C51432017*>(this)->SetDataOut(controlWord);
		}
		catch(std::bad_cast&)
		{
			ASSERT(false);
		}
		currCtrlWord = m_controlWord;	// Save previous control word
		m_controlWord = controlWord;	// Set new control word
	}
	WaitForSettle(CalcSettleTime(currCtrlWord, controlWord));

	return;
}


void C7234201102::SetSwitch(const SVCPMsg::USwitch::SVushf& req)
{
	if (!m_isPresent)
	{
		return;
	}
	// Build control word
	unsigned long controlWord = 0;
	if (req.calToAnt) controlWord |= C3230Fdd::CSequencerAntennaControl::CAL_TO_ANT;
	if (req.calToRef) controlWord |= C3230Fdd::CSequencerAntennaControl::CAL_TO_REF;
	if (req.cal0) controlWord |= C3230Fdd::CSequencerAntennaControl::CAL0;
	if (req.cal1) controlWord |= C3230Fdd::CSequencerAntennaControl::CAL1;
	if (req.calToneNoiseSource) controlWord |= C3230Fdd::CSequencerAntennaControl::CALTONE_NOISESOURCE;
	if (req.cBandVuhfElement) controlWord |= C3230Fdd::CSequencerAntennaControl::CBAND_VUHF_ELEMENT;
	if (req.cBandVuhfAmp) controlWord |= C3230Fdd::CSequencerAntennaControl::CBAND_VUHF_AMP;
	if (req.vertHoriz) controlWord |= C3230Fdd::CSequencerAntennaControl::VERT_HORIZ;
	controlWord |= ((req.defaultAnt << C3230Fdd::CSequencerAntennaControl::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControl::DEFAULT_ANTENNA);

	unsigned long currCtrlWord;
	{
		std::lock_guard<std::mutex> lock(m_critSect);

		try
		{
			dynamic_cast<C51432017*>(this)->SetDataOut(controlWord);
		}
		catch(std::bad_cast&)
		{
			ASSERT(false);
		}
		currCtrlWord = m_controlWord;	// Save previous control word
		m_controlWord = controlWord;	// Set new control word
	}
	WaitForSettle(CalcSettleTime(currCtrlWord, controlWord));

	return;

}


//////////////////////////////////////////////////////////////////////
//
// Wait for switch to settle
//
void C7234201102::WaitForSettle(unsigned long usec)
{
	if (usec == 0) return;

	timespec ts, tr;
	ts.tv_sec = 0;
	ts.tv_nsec = 1000 * usec;	// approx 1 msec default
	nanosleep(&ts, &tr);
	return;
}


