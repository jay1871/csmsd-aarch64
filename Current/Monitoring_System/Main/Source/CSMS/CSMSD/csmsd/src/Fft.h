/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "Ne10Vec.h"
#include "PLddr3.h"

class CFft
{
public:
	// Functions
	static ne10_float32_t GetBwCorrection(ne10_float32_t db);
	static ne10_float32_t GetEnbw(unsigned int numSamples);
	static void GetPowerSpectrumWatts2(unsigned long numBins, unsigned long sampleSize, const uncached_ptr& data,
		float gainAdj, Ne10F32Vec& watts);
	static ne10_float32_t GetScalePower(unsigned int numSamples); // { CalculateWindow(numSamples); return m_scalePower; }
	static void GetVoltages2(unsigned long numBins, unsigned long sampleSize, const uncached_ptr& dataI,
		const uncached_ptr& dataQ, float gainAdj, Ne10F32cVec2& volts);
	static Ne10F32Vec WattsToDbm(const Ne10F32Vec& watts);
	static void TraceSpectrum(const char* label, size_t nbeg, size_t nend, const ne10_float32_t* s);
	template <typename V> static void TraceSpectrumRaw(const char* label, size_t nbeg, size_t nend, const V* s);
	static void TraceIq(const char* label, size_t nbeg, size_t nend, ne10_fft_cpx_float32_t* s);

private:
	// Functions
	static void CalculateWindow(unsigned int numSamples);

	// Data
	static Ne10F32cVec m_blackmanHarris;
	static ne10_float32_t m_enbw;
	static unsigned int m_numSamples;
	static unsigned int m_numWindow;
	static ne10_float32_t m_scalePower;
};


template<typename V> void CFft::TraceSpectrumRaw(const char* label, size_t nbeg, size_t nend, const V* s)
{
	for (size_t i = nbeg; i < nend; i += 8)
	{
		char temp[30];
		snprintf(temp, 30, "%s:%3u", label, i);
		std::string str(temp);
		for (size_t j = i; j < i + 8; ++j)
		{
			if (j == nend) break;
			snprintf(temp, 30, " %08lx", s[j]);
			str += temp;
		}
		printf("%s\n", str.c_str());
	}
	return;
}
