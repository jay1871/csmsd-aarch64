/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2015 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "Config.h"
#include "EquipControl.h"
#include "EquipCtrlMsg.h"
#include "Navigation.h"
#include "Ne10Vec.h"
#include "Singleton.h"
#include "Task.h"
#include "Units.h"

template<typename T> class CNetConnection;

class CSuspendableTask :
	// Inheritance
	public virtual CTask
{
public:
	// Types
	typedef std::shared_ptr<CSuspendableTask> SuspendableTask;

	virtual ~CSuspendableTask(void);
	bool CheckSuspend(void);
	bool Excluded(Units::Frequency freq, Units::Frequency bw, CConfig::EPolarization pol) const;
	float FieldStrength(float watts, Units::Frequency freq, float switchTemp, size_t band) const;
	float FieldStrengthFactor(Units::Frequency freq, size_t band) const;
	static SEquipCtrlMsg::SStateResp GetState(const SEquipCtrlMsg::STaskIdKey& taskIdKey);
	bool IsSuspended(void) const { return m_suspended; }	// TODO: Need to lock m_taskIdMutex?
	const Ne10F32Vec& GetFieldStrengthFactors(Units::Frequency freq, Units::Frequency bw, size_t numChannels, size_t step);
	const Ne10F32Vec& GetTerminatedData(size_t step, size_t block) const { return m_terminatedData[step][block]; }
	bool HasTerminatedData(size_t step, size_t block) const { return m_hasTerminatedData[step][block]; }
	bool IsTerminatedDataEmpty(size_t step, size_t block) const { return !m_hasTerminatedData[step][block] || m_terminatedData[step][block].size() == 0; }
	bool NoExcludes(void) const { return m_excludeBands.empty(); }
	void SetTerminatedData(_In_ const Ne10F32Vec& terminatedData, size_t step, size_t block) { m_terminatedData[step][block] = terminatedData; }
	static SuspendableTask Suspend(const SEquipCtrlMsg::STaskIdKey& taskIdKey, bool suspend);
	static std::vector<SEquipCtrlMsg::STaskIdKey> SuspendAll(bool suspend);
	static void Terminate(const SEquipCtrlMsg::STaskIdKey& taskIdKey);
	static void Terminate(const CNetConnection<void>* client);

protected:
	// Functions
	CSuspendableTask(void);
	CSuspendableTask(_In_ SRestartData& restartData);
	virtual void SaveRestartData(_Inout_ std::vector<unsigned char>& data) const;
	template<typename T, typename SMsgStruct, typename SDataStruct, const unsigned long MAX_DATA, typename DataMap>
		void SendReports(SEquipCtrlMsg::EOccupancyDfCtrl msgSubType, LPCTSTR msgTypeName, SMsgStruct& msgStruct,
		SDataStruct (&dataStruct)[MAX_DATA], DataMap& data,
		SDataStruct (T::*GetData)(typename DataMap::mapped_type& value, size_t chan), bool last);
	template<typename T, typename SMsgStruct, typename SDataStruct, const unsigned long MAX_DATA, typename DataVector>
		void SendReports(SEquipCtrlMsg::EOccupancyDfCtrl msgSubType, LPCTSTR msgTypeName, SMsgStruct& msgStruct,
		SDataStruct (&dataStruct)[MAX_DATA], DataVector& data,
		SDataStruct (T::*GetData)(typename DataVector::value_type& value, size_t chan), bool last);

	// Types
	struct SFsFactorKey
	{
		bool operator==(const SFsFactorKey& right) const
			{ return freq == right.freq && bw == right.bw && numChannels == right.numChannels && step == right.step; }

		Units::Frequency freq;
		Units::Frequency bw;
		size_t numChannels;
		size_t step;
	};
	struct SFsFactorHash : public std::unary_function<SFsFactorHash, size_t>
	{
		size_t operator()(const SFsFactorKey& key) const { return std::hash<long long>()(key.freq.GetRaw().internal); }
	};
	typedef std::unordered_map<SFsFactorKey, Ne10F32Vec, SFsFactorHash> FsFactorMap; // Map of fieldstrength factors for this task

	// Data
	std::vector<size_t> m_bandOffset;
	std::vector<size_t> m_bandSize;
	FsFactorMap m_fieldStrengthFactors;
	std::vector<std::vector<bool> > m_hasTerminatedData;
	CSingleton<const CNavigation> m_navigation;
	SEquipCtrlMsg m_report;
	bool m_suspend;
	bool m_suspended;
	std::vector<std::vector<Ne10F32Vec> > m_terminatedData;

private:
	// Constants
	static const unsigned int SUSPEND_TIMEOUT = 30000; // 30 seconds

	// Data
	static std::condition_variable m_suspendsChanged;
};


//////////////////////////////////////////////////////////////////////
//
// Report interim or final results
//
template<typename T, typename SMsgStruct, typename SDataStruct, const unsigned long MAX_DATA, typename DataMap>
void CSuspendableTask::SendReports(SEquipCtrlMsg::EOccupancyDfCtrl msgSubType, LPCTSTR msgTypeName, SMsgStruct& msgStruct, SDataStruct (&dataStruct)[MAX_DATA],
	DataMap& data, SDataStruct (T::*GetData)(typename DataMap::mapped_type& value, size_t chan), bool last)
{
	// dataStruct must be part of msgStruct
	ASSERT(static_cast<void*>(dataStruct) >= static_cast<void*>(&msgStruct) &&
		static_cast<void*>(dataStruct) < static_cast<void*>(&(&msgStruct)[1]));

	if (last)
	{
		msgStruct.occHdr.status = ErrorCodes::EErrorCode(msgStruct.occHdr.status + 10000);
	}

	m_report.hdr.msgSubType = msgSubType;
	m_report.hdr.bodySize = sizeof(SMsgStruct); // TODO support variable length messages
	msgStruct.occHdr.numTotalChannels = m_bandOffset.back() + m_bandSize.back();
	ASSERT(data.empty() || data.rbegin()->first.chan < msgStruct.occHdr.numTotalChannels);
	m_navigation->GetGpsResponse(msgStruct.occHdr.gpsResponse);
	msgStruct.occHdr.firstChannel = 0;
	msgStruct.occHdr.numChannels = MAX_DATA;
	memset(dataStruct, 0, sizeof(dataStruct));

	// Copy to message(s) and send
	for (typename DataMap::iterator entry = data.begin(); entry != data.end(); ++entry)
	{
		size_t index = m_bandOffset[entry->first.band] + entry->first.chan - msgStruct.occHdr.firstChannel;

		while (index >= MAX_DATA)
		{
			// Message full - send it
// TODO			m_report.hdr.bodySize = reinterpret_cast<BYTE*>(&dataStruct[msgStruct.occHdr.numChannels]) - reinterpret_cast<BYTE*>(&msgStruct);
			CEquipControl::Send(m_source, m_report, msgTypeName);
			msgStruct.occHdr.firstChannel += MAX_DATA;
			memset(dataStruct, 0, sizeof(dataStruct));
			index -= MAX_DATA;
		}

		dataStruct[index] = (dynamic_cast<T*>(this)->*GetData)(entry->second, m_bandOffset[entry->first.band] + entry->first.chan);
	}

	// Send last message(s)
	while (msgStruct.occHdr.firstChannel < msgStruct.occHdr.numTotalChannels)
	{
		msgStruct.occHdr.numChannels = std::min(MAX_DATA, msgStruct.occHdr.numTotalChannels - msgStruct.occHdr.firstChannel);
// TODO		m_report.hdr.bodySize = reinterpret_cast<BYTE*>(&dataStruct[msgStruct.occHdr.numChannels]) - reinterpret_cast<BYTE*>(&msgStruct);
		CEquipControl::Send(m_source, m_report, msgTypeName);
		msgStruct.occHdr.firstChannel += MAX_DATA;
		memset(dataStruct, 0, sizeof(dataStruct));
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Report interim or final results
//
template<typename T, typename SMsgStruct, typename SDataStruct, const unsigned long MAX_DATA, typename DataVector>
void CSuspendableTask::SendReports(SEquipCtrlMsg::EOccupancyDfCtrl msgSubType, LPCTSTR msgTypeName, SMsgStruct& msgStruct, SDataStruct (&dataStruct)[MAX_DATA],
	DataVector& data, SDataStruct (T::*GetData)(typename DataVector::value_type&, size_t chan), bool last)
{
	// dataStruct must be part of msgStruct
	ASSERT(static_cast<void*>(dataStruct) >= static_cast<void*>(&msgStruct) &&
		static_cast<void*>(dataStruct) < static_cast<void*>(&(&msgStruct)[1]));

	if (last)
	{
		msgStruct.occHdr.status = ErrorCodes::EErrorCode(msgStruct.occHdr.status + 10000);
	}

	m_report.hdr.msgSubType = msgSubType;
	m_report.hdr.bodySize = sizeof(SMsgStruct); // TODO support variable length messages
	m_navigation->GetGpsResponse(msgStruct.occHdr.gpsResponse);
	msgStruct.occHdr.firstChannel = 0;
	msgStruct.occHdr.numChannels = MAX_DATA;
	m_report.body.occResult.occHdr.numTotalChannels = static_cast<unsigned long>(data.size());

	// Copy to message(s) and send
	for (size_t chan = 0, index = 0; chan < data.size(); ++chan, ++index)
	{
		if (index >= MAX_DATA)
		{
			// Message full - send it
			CEquipControl::Send(m_source, m_report, msgTypeName);
			msgStruct.occHdr.firstChannel += MAX_DATA;
			index -= MAX_DATA;
		}

		dataStruct[index] = (dynamic_cast<T*>(this)->*GetData)(data[chan], chan);
	}

	// Send last message(s)
	while (msgStruct.occHdr.firstChannel < msgStruct.occHdr.numTotalChannels)
	{
		msgStruct.occHdr.numChannels = std::min(MAX_DATA, msgStruct.occHdr.numTotalChannels - msgStruct.occHdr.firstChannel);
		CEquipControl::Send(m_source, m_report, msgTypeName);
		msgStruct.occHdr.firstChannel += MAX_DATA;
		memset(dataStruct, 0, sizeof(dataStruct));
	}

	return;
}
