/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "EquipCtrlMsg.h"
#include "Ne10Vec.h"
#include "Task.h"

template <typename T> class CNetConnection;

class CPanTask :
	// Inheritance
	public virtual CTask
{
public:
	// Types
	typedef std::shared_ptr<CPanTask> PanTask;

	// Functions
	void AddPan(_In_ const Ne10F32Vec& spectr);
	void ClearSavedPanData(void) {m_upPanData.reset();}
	static PanTask Create(_In_opt_ const SEquipCtrlMsg* cmd, _In_opt_ CNetConnection<void>* source, /*EPriority priority,*/ SEquipCtrlMsg::EAnt ant);
	const SEquipCtrlMsg * GetResponse(void) const { return (reinterpret_cast<const SEquipCtrlMsg*>(&(m_respBuf[0]))); }
	Units::Frequency GetCenterFreq(void) const;
	bool PartialData(void) const { return(bool(m_upPanData));}
	void UpdateCompletionTime(DATE dateTime) { std::lock_guard<std::mutex> lock(m_taskIdMutex); m_completionTime = dateTime; }

	// Data
	Ne10F32Vec m_terminatedData;

protected:

private:
	// Types
	struct Deleter
	{
		void operator()(_In_ CPanTask* task) { delete task; }
	};

	struct SPanData
	{
		size_t numBins;
		Ne10F32Vec watts;
		double dateTime;	// using COleTime format

	};

	static const size_t C_DEF_NUM_BINS = 802;

	// Functions
	CPanTask(_In_opt_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source,
		/*EPriority priority = NON_QUEUED,*/ SEquipCtrlMsg::EAnt ant = SEquipCtrlMsg::ANT1);
	~CPanTask(void);

	// Data
	std::vector<unsigned char> m_respBuf;
	std::unique_ptr<SPanData> m_upPanData;
};

//////////////////////////////////////////////////////////////////////
///<summary>
/// Return the PAN center frequency.
///</summary>
///
/// <returns>
///@return: PAN center frequency.
/// </returns>
///
/// <remarks>
///@note: PAN center frequency may not be the same as tuned frequency
///	  when SHF Extension is used.
/// </remarks>
///
inline Units::Frequency CPanTask::GetCenterFreq(void) const
{
	auto pResp = reinterpret_cast<const SEquipCtrlMsg*>(&(m_respBuf[0]));
	return(Units::Frequency(pResp->body.getPanResp.freq));
}
