/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once
#include "DfCtrlMsg.h"
#include "EquipCtrlMsg.h"

class CMonitor
{
public:
	// Functions
	CMonitor(size_t numSlaves);
	~CMonitor(void);
	void AddSlaveResponse(size_t nodeIndex);
	void AddSlaveResponse(size_t nodeIndex, const SDfCtrlMsg::SGetSlaveCsmsFaultResp& slaveResp);
	bool Monitor(bool doSlow, SSmsMsg::EErrStatus& bistSummary, long maxDiffNsec, bool& has3230Timestamp);
	void GetFaultDetails(SEquipCtrlMsg::SGetCsmsFaultResp& faultStat) const
		{ std::lock_guard<std::mutex> lock(m_faultMutex); faultStat = m_faultCond; }
	void GetFaultDetails(SDfCtrlMsg::SGetSlaveCsmsFaultResp& faultStat) const
		{ std::lock_guard<std::mutex> lock(m_faultMutex); faultStat.cond = m_faultCond.cond; faultStat.env = m_faultCond.env; }
	void SetFaultDetails(const SEquipCtrlMsg::SGetCsmsFaultResp& faultStat)
		{ std::lock_guard<std::mutex> lock(m_faultMutex); m_faultCond = faultStat; }
private:
	// Types
	union UFaultDetails
	{
		struct /*SFaultDetails*/
		{
			SSmsMsg::EErrStatus bistCond;
			SSmsMsg::SGetCsmsFaultResp::SfItem temps3230[2];
			SSmsMsg::SGetCsmsFaultResp::SfItem voltages3230[21];
			SSmsMsg::SGetCsmsFaultResp::SulItem clocks3230[3];
			SSmsMsg::SGetCsmsFaultResp::SfItem temps2630[3];
			SSmsMsg::SGetCsmsFaultResp::SulItem ocxoLock;
		};
		SSmsMsg::SGetCsmsFaultResp::SFaultDetails faultDetailsMsg;
	};
	static_assert(sizeof(UFaultDetails) == sizeof(SSmsMsg::SGetCsmsFaultResp::SFaultDetails), "SFaultDetails size error");

	// Data
	SEquipCtrlMsg::SGetCsmsFaultResp m_faultCond;	//Has the environment fault conditions.
	mutable std::mutex m_faultMutex;
	SEquipCtrlMsg::SGetCsmsFaultResp::SFaultDetails m_faultDefault;
};
