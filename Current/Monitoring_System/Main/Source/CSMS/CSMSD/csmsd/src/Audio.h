/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "Config.h"
#include "Digitizer.h"
#include "PriorityQueue.h"
#include "SmsRealtimeMsg.h"
#include "Singleton.h"
#include "Units.h"

template <typename T> class CNetConnection;

class CAudio
{
public:
	// Types
	struct SAudioParam
	{
		SAudioParam(void):client(nullptr), demodFreq(0), params(), started(false), enabled(false), streamid(0){};
		SAudioParam(unsigned short lastframe, unsigned long packetnumber, unsigned long long framenumber, unsigned long strid) : client(nullptr), demodFreq(0), params(), started(false), enabled(false), streamid(0)
		{m_lastframe = lastframe; m_packetnumber = packetnumber; m_framenumber = framenumber; streamid = strid;}

		const CNetConnection<void>* client;
		Units::Frequency demodFreq;
		C3230::SAudioParams params;
		bool started;
		bool enabled;
		unsigned long long	   m_framenumber;
		unsigned short m_lastframe;
		unsigned long m_packetnumber;
		unsigned long streamid;
	};

	struct SDemodInternalMsg
	{
		enum EType
		{
			REALTIMEDATA
		};

		// Forward declarations
		struct SRealTimeDataMsg;

		// Destructor
		virtual ~SDemodInternalMsg(void) {}

		// Unique_ptr helpers
		template<typename T> static std::unique_ptr<T> MakeUnique(void) { return std::unique_ptr<T>(new T); }

		// Data
		EType type;

	private:
		// Constructor
		SDemodInternalMsg(EType type) : type(type) {}
	};
	typedef std::unique_ptr<SDemodInternalMsg> DemodMsg;

	// Message structures
	struct SDemodInternalMsg::SRealTimeDataMsg : public SDemodInternalMsg
	{
		SRealTimeDataMsg(void) : SDemodInternalMsg(REALTIMEDATA) {}
		SSmsRealtimeMsg data;
	};

	// Functions
	CAudio();
	virtual ~CAudio();
	void AddScaleFactor(Units::Timestamp scaleTime, float scaleFactor) { m_scaleFactor.Add(scaleTime, scaleFactor); }
	template <typename T> bool Enqueue(T& msg, unsigned int timeout = INFINITE);
	void Enable(bool enable);
	void TuneDemods(Units::Frequency rfFreq, Units::Frequency finalIfFreq, bool invertRadio, bool invertSpectrumData, const Units::FreqPair& freqLimits);
#ifdef CSMS_2016
	bool IQStreamRunning()
	{
		bool IQRunning = false;
		for (unsigned char audioChannel = 0; audioChannel < m_IQparam.size(); ++audioChannel)
		{
			if (m_IQparam[audioChannel].client != nullptr && m_IQparam[audioChannel].started && m_enabled)
			{
				IQRunning = true;
				break;
			}
		}
		return IQRunning;
	}
#else
	bool IQStreamRunning()
	{
		bool IQRunning = false;
		for (unsigned char audioChannel = 0; audioChannel < m_param.size(); ++audioChannel)
		{
			if (m_param[audioChannel].client != nullptr && m_param[audioChannel].started && m_enabled)
			{
				IQRunning = true;
				break;
			}
		}
		return IQRunning;
	}
#endif
protected:
	// Functions
	bool AllocateAudioChannel(const CNetConnection<void>* client, Units::Frequency demodFreq, const C3230::SDecimation& decimation, C3230::EDemodMode mode, Units::Frequency bw, Units::Frequency bfo,
		unsigned char& channel, unsigned long streamid);
	void Free(const CNetConnection<void>* client);
	bool Free(const CNetConnection<void>* client, unsigned char audioChannel);
	void GetAudioParams(std::vector<SAudioParam>& audioParams) const
		{ std::lock_guard<std::mutex> lock(m_critSect); audioParams = m_param; }
	bool SetDemodParams(const CNetConnection<void>* client, unsigned char audioChannel, Units::Frequency demodFreq, const C3230::SDecimation& decimation, C3230::EDemodMode mode, Units::Frequency bw, Units::Frequency bfo = 0, unsigned long streamid = 0);
	void Startup(void);

private:
	static const unsigned short MAX_CHANNELS = 8;

	// Constants
	static const unsigned int QUEUE_DEPTH = 384;		// 384 * 1024 samples @ 384Ksps = 1 second - may be too small

	// Types
	typedef CPriorityQueue<DemodMsg, QUEUE_DEPTH> DemodQueue;

	class CIqScaleFactor
	{
		public:
			static const Units::TimeSpan DEPTH;
			static const float DEFAULT;

			void Add(Units::Timestamp scaleTime, float scaleFactor);
			float Get(Units::Timestamp scaleTime);
		private:
			struct SIqScaleFactor
			{
				Units::Timestamp scaleTime;
				float scaleFactor;
			};

			std::deque<SIqScaleFactor> factors;
			std::mutex mtx;
	};

	// Functions
	void DemodThread(void);
#ifdef CSMS_2016
	void Free(C3230::EDemodMode mode, unsigned char audioChannel);
#else
	void Free(unsigned char audioChannel);
#endif
	float GetScaleFactor(Units::Timestamp scaleTime) { return m_scaleFactor.Get(scaleTime); }
	void HandleRealTimeData(const SDemodInternalMsg::SRealTimeDataMsg& realtimeDataMsg);
//	void OnSamplesDemod(const std::vector<unsigned long>& samples, unsigned long audioChannel);
	void OnSamplesExt(const std::vector<unsigned long>& samples, unsigned long audioChannel, bool EOS);
#ifdef CSMS_2016
	void OnSamplesWidebandDDC(const std::vector<unsigned long>& samples, unsigned long audioChannel, bool EOS);
#endif
//	static void OnSamplesDemod(const std::vector<unsigned long>& samples, unsigned long audioChannel, void* param)
//		{ static_cast<CAudio*>(param)->OnSamplesDemod(samples, audioChannel); }
	static void OnSamplesExt(const std::vector<unsigned long>& samples, unsigned long audioChannel, bool EOS, void* param)
		{ static_cast<CAudio*>(param)->OnSamplesExt(samples, audioChannel, EOS); }
#ifdef CSMS_2016
	static void OnSamplesWidebandDDC(const std::vector<unsigned long>& samples, unsigned long audioChannel, bool EOS, void* param)
		{ static_cast<CAudio*>(param)->OnSamplesWidebandDDC(samples, audioChannel, EOS); }
#endif
	void SetDemodParam(unsigned char audioChannel, Units::Frequency demodFreq, const C3230::SDecimation& decimation, C3230::EDemodMode mode,
		Units::Frequency bw, Units::Frequency bfo, unsigned long streamid);
#ifdef CSMS_2016
	void Start(C3230::EDemodMode mode, unsigned char audioChannel);
	void Stop(C3230::EDemodMode mode, unsigned char audioChannel);
#else
	void Start(unsigned char audioChannel);
	void Stop(unsigned char audioChannel);
#endif
//	void TuneAudioChannel(unsigned char audioChannel, Units::Frequency procFreq, bool invertSpectrumData);

	// Data
	CSingleton<const CConfig> m_config;
	mutable std::mutex m_critSect;
	DemodQueue m_demodQueue;
	std::thread m_demodThread;
	CSingleton<CDigitizer> m_digitizer;
	bool m_enabled;
	Units::Frequency m_finalIfFreq;
	Units::FreqPair m_freqLimits;
	std::vector<SAudioParam> m_param;
#ifdef CSMS_2016
	std::vector<SAudioParam> m_IQparam;
	SAudioParam m_Widebandparam;
#endif
	bool m_invertRadio;
	Units::Frequency m_rfFreq;
	CIqScaleFactor m_scaleFactor;
	typedef std::map<unsigned short, unsigned long> streamidmap;
	std::vector<streamidmap> m_streamidmaplist;
};

template <typename T> bool CAudio::Enqueue(T& msg, unsigned int timeout)
{
	try
	{
		m_demodQueue.AddItem(DemodMsg(std::move(msg)), 0, timeout);
	}
	catch(DemodQueue::EProblem& problem)
	{
		switch (problem)
		{
		case DemodQueue::CLOSED:
			// Exit
			return false;

		case DemodQueue::TIMEDOUT:
			// Drop message
			TRACE(_T("CAudio::Enqueue: Messages dropped"));
			return false;

		default:
			// Something bad
			throw std::logic_error("CAudio::Enqueue: Exception");
		}
	}
	return true;
}
