/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2015 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"

#include "ScanDfTask.h"

//Uncomment  DBG_SCANDF to trace ScanDF execution.
//#define DBG_SCANDF 1

#ifdef DBG_SCANDF
static size_t s_numAddBlockCalled = 0;
#endif

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CScanDfTask::CScanDfTask(_In_ const SEquipCtrlMsg* cmd, _In_opt_ CNetConnection<void>* source) try :
	CTask(cmd, source), // Virtual base
	CSuspendableTask(),
	m_conf(cmd->body.getScanDfCmd.confidence / 100.0f),
	m_snr(pow(10.0f, cmd->body.getScanDfCmd.scanDfThreshold / 10.0f)),
	m_data(),
	m_ionogram(),
	m_numAz(cmd->body.getScanDfCmd.numAzimuths),
	m_prevAz()
{
#ifdef DBG_SCANDF
	//m_conf = 0.5f;
#endif
	if (m_numAz < 1 || m_numAz > 360)
	{
		throw ErrorCodes::SCANDFTOOMANYCHANS;
	}

	for (size_t band = 0; band < m_taskParams.size(); ++band)
	{
		m_prevAz.push_back(std::vector<Ne10F32Vec>(m_taskParams[band].numBlocks));
	}

	m_report.hdr.msgSubType = SEquipCtrlMsg::SCANDF_DATA_RESULT;
	m_report.hdr.bodySize = sizeof(SEquipCtrlMsg::SScanDfVsChannelResp);
	m_report.body.scanDfVsChannelResp.occHdr.status = ErrorCodes::SUCCESS;
	m_report.body.scanDfVsChannelResp.occHdr.numTimeOfDays = 0;
	m_report.body.scanDfVsChannelResp.numAzimuths = int(m_numAz);
	m_report.body.scanDfVsChannelResp.numChannels = int(m_bandOffset.back() + m_bandSize.back());

	return;
}
catch(std::bad_alloc)
{
	throw ErrorCodes::SCANDFTOOMANYCHANS;
}


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CScanDfTask::CScanDfTask(_In_ SRestartData& restartData) :
	CTask(restartData), // CTask is a virtual base
	CSuspendableTask(restartData),
	m_prevAz()
{
	// Read from blob into member data
	ReadData(restartData, m_conf);
	ReadData(restartData, m_snr);
	ReadData(restartData, m_numAz);
	ReadData(restartData, m_data);
//	unsigned int checksum1 = m_checksum;
//	unsigned int checksum2;
//	ReadData(restartData, checksum2);
//
//	if(checksum1 != checksum2)
//	{
//		throw ErrorCodes::UNABLETOGETDATA;
//	}

	for (size_t band = 0; band < m_taskParams.size(); ++band)
	{
		m_prevAz.push_back(std::vector<Ne10F32Vec>(m_taskParams[band].numBlocks));
	}

	m_report.hdr.msgSubType = SEquipCtrlMsg::SCANDF_DATA_RESULT;
	m_report.hdr.bodySize = sizeof(SEquipCtrlMsg::SScanDfVsChannelResp);
	m_report.body.scanDfVsChannelResp.occHdr.status = ErrorCodes::SUCCESS;
	m_report.body.scanDfVsChannelResp.occHdr.numTimeOfDays = 0;
	m_report.body.scanDfVsChannelResp.numAzimuths = int(m_numAz);
	m_report.body.scanDfVsChannelResp.numChannels = int(m_bandOffset.back() + m_bandSize.back());

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CScanDfTask::~CScanDfTask(void)
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Add a block of data to accumulated results
//
void CScanDfTask::AddBlock(size_t band, size_t block, unsigned long offset,
	Units::Frequency firstChanFreq, const Ne10F32Vec& az, const Ne10F32Vec& el,
	const Ne10F32Vec& conf, const Ne10F32Vec& watts, float switchTemp)
{
	ASSERT(az.size() == el.size());
	ASSERT(conf.size() == el.size());
	ASSERT(watts.size() <= az.size());
	size_t chanOffset = block * size_t(GetProcBw(band) / m_taskParams[band].bw);
#ifdef DBG_SCANDF
	size_t azConfCnt = 0;
	float maxConf;
	float minConf;
	++s_numAddBlockCalled;
#endif

	for (size_t blockChan = offset; blockChan < watts.size(); ++blockChan)
	{
		if (conf[blockChan] >= m_conf)
		{
#ifdef DBG_SCANDF
			float heading = m_navigation->GetTrueHeading(m_taskParams[band].hf, Units::Frequency(0));
			float calcValue = ((az[blockChan] * Units::R2D + heading + 0.5f)
					/ Units::DEG_PER_CIRCLE);
			size_t azBin = size_t(m_numAz * calcValue) % m_numAz;
#else
			size_t azBin = size_t(m_numAz * ((az[blockChan] * Units::R2D + m_navigation->GetTrueHeading(m_taskParams[band].hf, Units::Frequency(0)) + 0.5f)
				/ Units::DEG_PER_CIRCLE)) % m_numAz;
#endif
			const SDataKey key = { azBin, band, chanOffset + blockChan };
			DataMap::mapped_type& entry = m_data[key];
			++entry.dfCount;
#ifdef DBG_SCANDF
			++azConfCnt;
			TRACE("CScanDfTask::AddBlock: offset = %lu, azBin = %u, band %u, chanOffset %u, m_numAz = %u, az[%u] =%f; heading =%f; calcValue = %f; R2D %f; azConfCnt %u, dfCount %u\n",
					offset, azBin, band, chanOffset, m_numAz, blockChan, az[blockChan], heading, calcValue, Units::R2D, azConfCnt, entry.dfCount);
#endif
			Units::Frequency freq = firstChanFreq + blockChan * m_taskParams[band].bw;

			if (el[blockChan] != Units::HALF_PI)
			{
				++entry.rangeCount;
				entry.rangeSum += m_ionogram->Range(el[blockChan], freq);
			}

			++entry.fldStrCount;
			entry.fldStrSum += FieldStrength(watts[blockChan], freq, switchTemp, band);
		}
#ifdef DBG_SCANDF
		if(blockChan == 0)
		{
			maxConf = conf[blockChan];
			minConf = conf[blockChan];
		}
		else
		{
			if(minConf > conf[blockChan])
			{
				minConf = conf[blockChan];
			}
			else if(maxConf > conf[blockChan])
			{
				maxConf = conf[blockChan];
			}
		}
#endif
	}

#ifdef DBG_SCANDF
	/*
		TRACE("CScanDfTask::AddBlock: offset = %lu, size = %u, m_conf = %f, conf[0] =%f; conf[max] =%f; minConf %f, maxConf %f, azConfCnt = %u\n",
				offset, watts.size(), m_conf, conf[0], conf[watts.size()-1], minConf, maxConf, azConfCnt);
	*/
#endif

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Create task from message and return shared pointer
//
CScanDfTask::ScanDfTask CScanDfTask::Create(_In_ const SEquipCtrlMsg* cmd, _In_opt_ CNetConnection<void>* source)
{
	ScanDfTask task(new CScanDfTask(cmd, source), Deleter());
	task->RegisterTask(true);

	return task;
}


//////////////////////////////////////////////////////////////////////
//
// Create task from restart data
//
void CScanDfTask::Create(_In_ SRestartData& restartData)
{
	RegisterTask(ScanDfTask(new CScanDfTask(restartData), Deleter()));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Helper function
//
size_t CScanDfTask::KeyDiff(_In_ const CScanDfTask::SDataKey& a, _In_ const CScanDfTask::SDataKey& b) const
{
	size_t diff = b.chan;

	if (a.band == size_t(-1) && a.chan == 0 && a.azBin == 0)
	{
		// Special case
		for (size_t band = 0; band < b.band; ++band)
		{
			diff += m_bandSize[band];
		}

#ifdef DBG_SCANDF
		TRACE("CScanDfTask::KeyDiff (a.band == size_t(-1) && a.chan == 0 && a.azBin == 0): diff %u, b.chan %u, b.band = %u, b.azBin = %u, m_bandSize[0] =%u; m_numAz =%u\n",
				diff, b.chan, b.band, b.azBin, m_bandSize[0], m_numAz);
#endif
		return diff * m_numAz + b.azBin + 1;
	}

	ASSERT(a.band < b.band || (a.band == b.band && (a.chan < b.chan || (a.chan == b.chan && a.azBin <= b.azBin))));

	for (size_t band = a.band; band < b.band; ++band)
	{
		diff += m_bandSize[band];
	}

#ifdef DBG_SCANDF
	if(diff > 50)
	{
		TRACE("CScanDfTask::KeyDiff: diff %u, b.band = %u, b.azBin = %u, a.chan %u, a.band %u, a.azBin %u, m_bandSize[0] =%u; m_numAz =%u\n",
					diff, b.band, b.azBin, a.chan, a.band,  a.azBin, m_bandSize[0], m_numAz);
		TRACE("CScanDfTask::KeyDiff: m_taskParams.size %u, m_taskParams[0]: numBlocks = %u, <%lf, %lf>, bw %lf\n",
				m_taskParams.size(), m_taskParams[0].numBlocks, m_taskParams[0].startFreq.Hz<double>(),
				m_taskParams[0].stopFreq.Hz<double>(), m_taskParams[0].bw.Hz<double>());

		if(m_taskParams.size() > 1)
		{
			TRACE("CScanDfTask::KeyDiff: m_taskParams[1]: numBlocks = %u, <%lf, %lf>, bw %lf\n",
					m_taskParams[1].numBlocks, m_taskParams[1].startFreq.Hz<double>(),
					m_taskParams[1].stopFreq.Hz<double>(), m_taskParams[1].bw.Hz<double>());
		}
	}
#endif
	return (diff - a.chan) * m_numAz + b.azBin - a.azBin;
}


//////////////////////////////////////////////////////////////////////
//
// Remember az data for next DF
//
void CScanDfTask::SaveCurrentAz(size_t band, size_t block, _In_ const Ne10F32Vec& az,
		_In_ const Ne10F32Vec& conf, unsigned long offset, unsigned long bandNumChan)
{
	ASSERT(az.size() == conf.size());
	Ne10F32Vec& prevAz = m_prevAz[band][block];
#ifdef DBG_SCANDF
	size_t azSavedCnt = 0;
	float maxConf;
	float minConf;
	/*
	if(400 < conf.size())
	{
		TRACE("CScanDfTask::SaveCurrentAz(%u,%u): conf.size = %u, m_conf = %f, conf[0] =%f; conf[400] =%f; conf[max] =%f, prevAz =%u\n",
				band, block, conf.size(), m_conf, conf[0], conf[400], conf[conf.size()-1], prevAz.size());
	}
	else
	{
		TRACE("CScanDfTask::SaveCurrentAz(%u,%u): conf.size = %u, m_conf = %f, conf[0] =%f; conf[max] =%f, prevAz =%u\n",
				band, block, conf.size(), m_conf, conf[0], conf[conf.size()-1], prevAz.size());
	}
	*/
#endif
	if (prevAz.size() == 0)
	{
		prevAz = az;
	}
	else
	{
		if(prevAz.size() < bandNumChan)
		{
			prevAz.resize(bandNumChan);
		}

		auto numChan = az.size();

		for (size_t chan = offset; chan < numChan; ++chan)
		{
			if (conf[chan] > m_conf)
			{
#ifdef DBG_SCANDF
				++azSavedCnt;
				TRACE("CScanDfTask::SaveCurrentAz: offset %lu, bandNumChan %lu, m_conf = %f, conf[%u] =%f; prevAz[]=%f; az[]=%f, azSavedCnt = %u\n",
						offset, bandNumChan, m_conf, chan, conf[chan], prevAz[chan], az[chan], azSavedCnt);
#endif
				prevAz[chan] = az[chan];
			}

#ifdef DBG_SCANDF
			if(chan == 0)
			{
				maxConf = conf[chan];
				minConf = conf[chan];
			}
			else
			{
				if(minConf > conf[chan])
				{
					minConf = conf[chan];
				}
				else if(maxConf < conf[chan])
				{
					maxConf = conf[chan];
				}
			}
#endif
		}

#ifdef DBG_SCANDF
		/*
		TRACE("CScanDfTask::SaveCurrentAz: offset %lu, bandNumChan %lu, size = %u, m_conf = %f, conf[0] =%f; conf[max] =%f; minConf %f, maxConf %f, azSavedCnt = %u\n",
				offset, bandNumChan, prevAz.size(), m_conf, conf[0], conf[prevAz.size()-1], minConf, maxConf, azSavedCnt);
		*/
#endif
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Save data for restart
//
void CScanDfTask::SaveRestartData(_Inout_ std::vector<unsigned char>& data) const
{
	// Base classes
	CTask::SaveRestartData(data);
	CSuspendableTask::SaveRestartData(data);

	// Member data
	SaveData(data, m_conf);
	SaveData(data, m_snr);
	SaveData(data, m_numAz);
	SaveData(data, m_data);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Report interim or final results
//
void CScanDfTask::SendReports(bool last)
{
	if (last)
	{
		m_report.body.scanDfVsChannelResp.occHdr.status = ErrorCodes::EErrorCode(m_report.body.scanDfVsChannelResp.occHdr.status + 10000);
	}

	const SDataKey FIRST_KEY = { 0, size_t(-1), 0 };
	const SDataKey LAST_KEY = { 0, m_taskParams.size(), 0 };

	// Count compressed entries for headers
	m_report.body.scanDfVsChannelResp.occHdr.numTotalChannels = static_cast<unsigned long>(m_data.size());
	SDataKey prevKey = FIRST_KEY;

	for (DataMap::const_iterator entry = m_data.begin(); entry != m_data.end(); ++entry)
	{
		// Insert zero count
		if (KeyDiff(prevKey, entry->first) > 1)
		{
			++m_report.body.scanDfVsChannelResp.occHdr.numTotalChannels;
		}

		prevKey = entry->first;
	}

	if (KeyDiff(prevKey, LAST_KEY) > 1)
	{
		++m_report.body.scanDfVsChannelResp.occHdr.numTotalChannels;
	}

	m_report.body.scanDfVsChannelResp.occHdr.firstChannel = 0;
	m_navigation->GetGpsResponse(m_report.body.scanDfVsChannelResp.occHdr.gpsResponse);
	unsigned long& count = m_report.body.scanDfVsChannelResp.occHdr.numChannels;
	count = 0;
	size_t zeroCount;
	prevKey = FIRST_KEY;
	size_t totalChans = 0;
#ifdef DBG_SCANDF
	const size_t C_ARRAY_CNT = 64;
	static unsigned long l_chanWtDf[C_ARRAY_CNT];
	static int l_dfCnt[C_ARRAY_CNT];
	size_t index = 0;
	size_t totalNonZeroDf = 0;
	size_t numZeroCnt = 0;
	int lastZeroCntVal = 0;

	for(size_t i =0; i < C_ARRAY_CNT; ++i)
	{
		l_chanWtDf[i] = l_dfCnt[i] = 0;
	}
#endif
	// Copy to message, compressing using RLE of zeros
	for (DataMap::const_iterator entry = m_data.begin(); entry != m_data.end(); ++entry)
	{
#ifdef DBG_SCANDF
		//Zero the entry to start with.
		m_report.body.scanDfVsChannelResp.scanDfData[count] = -int(zeroCount);
		m_report.body.scanDfVsChannelResp.aveRange[count] = -int(zeroCount);
		m_report.body.scanDfVsChannelResp.aveFldStr[count] = -int(zeroCount);
#endif
		if ((zeroCount = KeyDiff(prevKey, entry->first) - 1) > 0)
		{
			// Insert zero count
			m_report.body.scanDfVsChannelResp.scanDfData[count] = -int(zeroCount);
			m_report.body.scanDfVsChannelResp.aveRange[count] = -int(zeroCount);
			m_report.body.scanDfVsChannelResp.aveFldStr[count++] = -int(zeroCount);
			totalChans += zeroCount;
#ifdef DBG_SCANDF
			++numZeroCnt;
			lastZeroCntVal = -int(zeroCount);
#endif
		}

		if (count == SEquipCtrlMsg::MAX_OCCCHANNELS)
		{
#ifdef DBG_SCANDF
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			TRACE("CScanDfTask::SendReports 1000 Max (sec = %ld nsec =%ld): numZeroCnt %u, lastZeroCntVal %u, DfCnt %u\n",
					ts.tv_sec, ts.tv_nsec, numZeroCnt, lastZeroCntVal, totalNonZeroDf);
			TRACE("1: l_chanWtDf: %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu\n",
					l_chanWtDf[0], l_chanWtDf[1], l_chanWtDf[2], l_chanWtDf[3], l_chanWtDf[4], l_chanWtDf[5], l_chanWtDf[6], l_chanWtDf[7],
					l_chanWtDf[8], l_chanWtDf[9], l_chanWtDf[10], l_chanWtDf[11], l_chanWtDf[12], l_chanWtDf[13], l_chanWtDf[14], l_chanWtDf[15]);
			TRACE("1a: l_chanWtDf: %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu\n",
							l_chanWtDf[16], l_chanWtDf[17], l_chanWtDf[18], l_chanWtDf[19], l_chanWtDf[20], l_chanWtDf[21], l_chanWtDf[22], l_chanWtDf[23],
							l_chanWtDf[24], l_chanWtDf[25], l_chanWtDf[26], l_chanWtDf[27], l_chanWtDf[28], l_chanWtDf[29], l_chanWtDf[30], l_chanWtDf[31]);
			TRACE("1b: l_chanWtDf: %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu\n",
							l_chanWtDf[32], l_chanWtDf[33], l_chanWtDf[34], l_chanWtDf[35], l_chanWtDf[36], l_chanWtDf[37], l_chanWtDf[38], l_chanWtDf[39],
							l_chanWtDf[40], l_chanWtDf[41], l_chanWtDf[42], l_chanWtDf[43], l_chanWtDf[44], l_chanWtDf[45], l_chanWtDf[46], l_chanWtDf[47]);
			TRACE("1c: l_chanWtDf: %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu\n",
							l_chanWtDf[48], l_chanWtDf[49], l_chanWtDf[50], l_chanWtDf[51], l_chanWtDf[52], l_chanWtDf[53], l_chanWtDf[54], l_chanWtDf[55],
							l_chanWtDf[56], l_chanWtDf[57], l_chanWtDf[58], l_chanWtDf[59], l_chanWtDf[60], l_chanWtDf[61], l_chanWtDf[62], l_chanWtDf[63]);
			TRACE("1: l_dfCnt: %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
					l_dfCnt[0], l_dfCnt[1], l_dfCnt[2], l_dfCnt[3], l_dfCnt[4], l_dfCnt[5], l_dfCnt[6], l_dfCnt[7],
					l_dfCnt[8], l_dfCnt[9], l_dfCnt[10], l_dfCnt[11], l_dfCnt[12], l_dfCnt[13], l_dfCnt[14], l_dfCnt[15]);
			TRACE("1a: l_dfCnt: %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
							l_dfCnt[16], l_dfCnt[17], l_dfCnt[18], l_dfCnt[19], l_dfCnt[20], l_dfCnt[21], l_dfCnt[22], l_dfCnt[23],
							l_dfCnt[24], l_dfCnt[25], l_dfCnt[26], l_dfCnt[27], l_dfCnt[28], l_dfCnt[29], l_dfCnt[30], l_dfCnt[31]);
			TRACE("1b: l_dfCnt: %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
							l_dfCnt[32], l_dfCnt[33], l_dfCnt[34], l_dfCnt[35], l_dfCnt[36], l_dfCnt[37], l_dfCnt[38], l_dfCnt[39],
							l_dfCnt[40], l_dfCnt[41], l_dfCnt[42], l_dfCnt[43], l_dfCnt[44], l_dfCnt[45], l_dfCnt[46], l_dfCnt[47]);
			TRACE("1c: l_dfCnt: %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
							l_dfCnt[48], l_dfCnt[49], l_dfCnt[50], l_dfCnt[51], l_dfCnt[52], l_dfCnt[53], l_dfCnt[54], l_dfCnt[55],
							l_dfCnt[56], l_dfCnt[57], l_dfCnt[58], l_dfCnt[59], l_dfCnt[60], l_dfCnt[61], l_dfCnt[62], l_dfCnt[63]);
			numZeroCnt = lastZeroCntVal = totalNonZeroDf = index = 0;
#endif
			// Message full - send it
			CEquipControl::Send(m_source, m_report, _T("SCANDF_DATA_RESULT"));
			m_report.body.scanDfVsChannelResp.occHdr.firstChannel += SEquipCtrlMsg::MAX_OCCCHANNELS;
			count = 0;
		}

		m_report.body.scanDfVsChannelResp.scanDfData[count] = entry->second.dfCount;
		m_report.body.scanDfVsChannelResp.aveRange[count] =
			(entry->second.rangeCount > 0 ? int(entry->second.rangeSum / entry->second.rangeCount) : -1);
		m_report.body.scanDfVsChannelResp.aveFldStr[count++] =
			(entry->second.fldStrCount > 0 ? int(10 * log10(entry->second.fldStrSum / entry->second.fldStrCount)) : -1);
		++totalChans;
#ifdef DBG_SCANDF
		if(entry->second.dfCount != 0)
		{
			++totalNonZeroDf;
			if( index < C_ARRAY_CNT)
			{
				l_chanWtDf[index] = count;
				l_dfCnt[index++] = entry->second.dfCount;
			}
		}
#endif
		if (count == SEquipCtrlMsg::MAX_OCCCHANNELS)
		{
#ifdef DBG_SCANDF
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			TRACE("CScanDfTask::SendReports 1000 Max (sec = %ld nsec =%ld): numZeroCnt %u, lastZeroCntVal %u, DfCnt %u\n",
					ts.tv_sec, ts.tv_nsec, numZeroCnt, lastZeroCntVal, totalNonZeroDf);
			TRACE("2: l_chanWtDf: %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu\n",
					l_chanWtDf[0], l_chanWtDf[1], l_chanWtDf[2], l_chanWtDf[3], l_chanWtDf[4], l_chanWtDf[5], l_chanWtDf[6], l_chanWtDf[7],
					l_chanWtDf[8], l_chanWtDf[9], l_chanWtDf[10], l_chanWtDf[11], l_chanWtDf[12], l_chanWtDf[13], l_chanWtDf[14], l_chanWtDf[15]);
			TRACE("2a: l_chanWtDf: %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu\n",
							l_chanWtDf[16], l_chanWtDf[17], l_chanWtDf[18], l_chanWtDf[19], l_chanWtDf[20], l_chanWtDf[21], l_chanWtDf[22], l_chanWtDf[23],
							l_chanWtDf[24], l_chanWtDf[25], l_chanWtDf[26], l_chanWtDf[27], l_chanWtDf[28], l_chanWtDf[29], l_chanWtDf[30], l_chanWtDf[31]);
			TRACE("2b: l_chanWtDf: %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu\n",
							l_chanWtDf[32], l_chanWtDf[33], l_chanWtDf[34], l_chanWtDf[35], l_chanWtDf[36], l_chanWtDf[37], l_chanWtDf[38], l_chanWtDf[39],
							l_chanWtDf[40], l_chanWtDf[41], l_chanWtDf[42], l_chanWtDf[43], l_chanWtDf[44], l_chanWtDf[45], l_chanWtDf[46], l_chanWtDf[47]);
			TRACE("2c: l_chanWtDf: %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu\n",
							l_chanWtDf[48], l_chanWtDf[49], l_chanWtDf[50], l_chanWtDf[51], l_chanWtDf[52], l_chanWtDf[53], l_chanWtDf[54], l_chanWtDf[55],
							l_chanWtDf[56], l_chanWtDf[57], l_chanWtDf[58], l_chanWtDf[59], l_chanWtDf[60], l_chanWtDf[61], l_chanWtDf[62], l_chanWtDf[63]);
			TRACE("2: 1000 l_dfCnt: %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
					l_dfCnt[0], l_dfCnt[1], l_dfCnt[2], l_dfCnt[3], l_dfCnt[4], l_dfCnt[5], l_dfCnt[6], l_dfCnt[7],
					l_dfCnt[8], l_dfCnt[9], l_dfCnt[10], l_dfCnt[11], l_dfCnt[12], l_dfCnt[13], l_dfCnt[14], l_dfCnt[15]);
			TRACE("2a: l_dfCnt: %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
							l_dfCnt[16], l_dfCnt[17], l_dfCnt[18], l_dfCnt[19], l_dfCnt[20], l_dfCnt[21], l_dfCnt[22], l_dfCnt[23],
							l_dfCnt[24], l_dfCnt[25], l_dfCnt[26], l_dfCnt[27], l_dfCnt[28], l_dfCnt[29], l_dfCnt[30], l_dfCnt[31]);
			TRACE("2b: l_dfCnt: %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
							l_dfCnt[32], l_dfCnt[33], l_dfCnt[34], l_dfCnt[35], l_dfCnt[36], l_dfCnt[37], l_dfCnt[38], l_dfCnt[39],
							l_dfCnt[40], l_dfCnt[41], l_dfCnt[42], l_dfCnt[43], l_dfCnt[44], l_dfCnt[45], l_dfCnt[46], l_dfCnt[47]);
			TRACE("2c: l_dfCnt: %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
							l_dfCnt[48], l_dfCnt[49], l_dfCnt[50], l_dfCnt[51], l_dfCnt[52], l_dfCnt[53], l_dfCnt[54], l_dfCnt[55],
							l_dfCnt[56], l_dfCnt[57], l_dfCnt[58], l_dfCnt[59], l_dfCnt[60], l_dfCnt[61], l_dfCnt[62], l_dfCnt[63]);
			numZeroCnt = lastZeroCntVal = totalNonZeroDf = index = 0;
#endif
			// Message full - send it
			CEquipControl::Send(m_source, m_report, _T("SCANDF_DATA_RESULT"));
			m_report.body.scanDfVsChannelResp.occHdr.firstChannel += SEquipCtrlMsg::MAX_OCCCHANNELS;
			count = 0;
		}

		prevKey = entry->first;
	}

	// Insert zero count at end if needed
	if ((zeroCount = KeyDiff(prevKey, LAST_KEY) - 1) > 0)
	{
		m_report.body.scanDfVsChannelResp.scanDfData[count] = -int(zeroCount);
		m_report.body.scanDfVsChannelResp.aveRange[count] = -int(zeroCount);
		m_report.body.scanDfVsChannelResp.aveFldStr[count++] = -int(zeroCount);
		totalChans += zeroCount;
#ifdef DBG_SCANDF
		TRACE("CScanDfTask::SendReports zero count at end: totalChans %u, zeroCount %u, count %lu, scanTotChan %lu\n",
				totalChans, zeroCount, count, m_report.body.scanDfVsChannelResp.occHdr.numTotalChannels);
#endif
	}

	ASSERT(totalChans == m_report.body.scanDfVsChannelResp.numChannels * m_numAz);
	ASSERT(count % SEquipCtrlMsg::MAX_OCCCHANNELS ==
		m_report.body.scanDfVsChannelResp.occHdr.numTotalChannels % SEquipCtrlMsg::MAX_OCCCHANNELS);

#ifdef DBG_SCANDF
	timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	TRACE("CScanDfTask::SendReports end (sec = %ld nsec =%ld): numZeroCnt %u, lastZeroCntVal %u, DfCnt %u, numAddBlockCalled %u\n",
					ts.tv_sec, ts.tv_nsec, numZeroCnt, lastZeroCntVal, totalNonZeroDf, s_numAddBlockCalled);
	TRACE("3: l_chanWtDf: %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu\n",
					l_chanWtDf[0], l_chanWtDf[1], l_chanWtDf[2], l_chanWtDf[3], l_chanWtDf[4], l_chanWtDf[5], l_chanWtDf[6], l_chanWtDf[7],
					l_chanWtDf[8], l_chanWtDf[9], l_chanWtDf[10], l_chanWtDf[11], l_chanWtDf[12], l_chanWtDf[13], l_chanWtDf[14], l_chanWtDf[15]);
	TRACE("3a: l_chanWtDf: %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu\n",
					l_chanWtDf[16], l_chanWtDf[17], l_chanWtDf[18], l_chanWtDf[19], l_chanWtDf[20], l_chanWtDf[21], l_chanWtDf[22], l_chanWtDf[23],
					l_chanWtDf[24], l_chanWtDf[25], l_chanWtDf[26], l_chanWtDf[27], l_chanWtDf[28], l_chanWtDf[29], l_chanWtDf[30], l_chanWtDf[31]);
	TRACE("3b: l_chanWtDf: %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu\n",
					l_chanWtDf[32], l_chanWtDf[33], l_chanWtDf[34], l_chanWtDf[35], l_chanWtDf[36], l_chanWtDf[37], l_chanWtDf[38], l_chanWtDf[39],
					l_chanWtDf[40], l_chanWtDf[41], l_chanWtDf[42], l_chanWtDf[43], l_chanWtDf[44], l_chanWtDf[45], l_chanWtDf[46], l_chanWtDf[47]);
	TRACE("3c: l_chanWtDf: %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu\n",
					l_chanWtDf[48], l_chanWtDf[49], l_chanWtDf[50], l_chanWtDf[51], l_chanWtDf[52], l_chanWtDf[53], l_chanWtDf[54], l_chanWtDf[55],
					l_chanWtDf[56], l_chanWtDf[57], l_chanWtDf[58], l_chanWtDf[59], l_chanWtDf[60], l_chanWtDf[61], l_chanWtDf[62], l_chanWtDf[63]);
	TRACE("3: 1000 l_dfCnt: %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
					l_dfCnt[0], l_dfCnt[1], l_dfCnt[2], l_dfCnt[3], l_dfCnt[4], l_dfCnt[5], l_dfCnt[6], l_dfCnt[7],
					l_dfCnt[8], l_dfCnt[9], l_dfCnt[10], l_dfCnt[11], l_dfCnt[12], l_dfCnt[13], l_dfCnt[14], l_dfCnt[15]);
	TRACE("3a: l_dfCnt: %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
					l_dfCnt[16], l_dfCnt[17], l_dfCnt[18], l_dfCnt[19], l_dfCnt[20], l_dfCnt[21], l_dfCnt[22], l_dfCnt[23],
					l_dfCnt[24], l_dfCnt[25], l_dfCnt[26], l_dfCnt[27], l_dfCnt[28], l_dfCnt[29], l_dfCnt[30], l_dfCnt[31]);
	TRACE("3b: l_dfCnt: %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
					l_dfCnt[32], l_dfCnt[33], l_dfCnt[34], l_dfCnt[35], l_dfCnt[36], l_dfCnt[37], l_dfCnt[38], l_dfCnt[39],
					l_dfCnt[40], l_dfCnt[41], l_dfCnt[42], l_dfCnt[43], l_dfCnt[44], l_dfCnt[45], l_dfCnt[46], l_dfCnt[47]);
	TRACE("3c: l_dfCnt: %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
					l_dfCnt[48], l_dfCnt[49], l_dfCnt[50], l_dfCnt[51], l_dfCnt[52], l_dfCnt[53], l_dfCnt[54], l_dfCnt[55],
					l_dfCnt[56], l_dfCnt[57], l_dfCnt[58], l_dfCnt[59], l_dfCnt[60], l_dfCnt[61], l_dfCnt[62], l_dfCnt[63]);
#endif
	// Send last message
	CEquipControl::Send(m_source, m_report, _T("SCANDF_DATA_RESULT"));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate a command
//
void CScanDfTask::Validate(_In_ const SEquipCtrlMsg::SGetScanDfCmd& cmd)
{
	size_t bodySize = offsetof(SEquipCtrlMsg::SGetScanDfCmd, band[cmd.numBands]);
	std::shared_ptr<SEquipCtrlMsg> msg(new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg);

	msg->hdr.msgType = SEquipCtrlMsg::OCCUPANCYDF_CTRL;
	msg->hdr.msgSubType = SEquipCtrlMsg::VALIDATE_OCCUPANCY_SCANDF;
	msg->hdr.bodySize = static_cast<unsigned long>(bodySize);
	memcpy(&msg->body.getScanDfCmd, &cmd, bodySize);
	CScanDfTask(msg.get(), nullptr); // Will throw on error

	return;
}

