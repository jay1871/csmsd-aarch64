/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/
#include "stdafx.h"
#include "Config.h"
#include "Failure.h"
#include "Log.h"
#include "Singleton.h"
#include "TciGps.h"
#include "Utility.h"

//bool TciGps::GetDATE(_Out_ DATE& date)
//{
//	auto ts = Utility::CurrentTimeAsTimespec();
//	date = (ts.tv_sec + ts.tv_nsec * 1.e-9 + 2209161600.) / 86400.;
//	return true;
//}


// Command line: gpsd -n -G /dev/ttyPS1

/////////////////////////////////////////////////////////////////////
//
// Constructor
//
CTciGps::CTciGps(void) :
	m_config(),
	m_connected(false),
	m_ntpPpsSync(false),
	m_ntpQuery(m_config->IsProcessorDfSlave(), m_config->MasterIP()),
	m_shutdown(false)
{
	// Initialize the internal data structure
	m_gpsdataShared.satellites_visible = 0;
	m_gpsdataShared.satellites_used = 0;
	m_gpsdataShared.fix.mode = MODE_NOT_SEEN;
	m_gpsdataShared.fix.time = std::numeric_limits<double>::quiet_NaN();
	m_gpsdataShared.fix.latitude = std::numeric_limits<double>::quiet_NaN();
	m_gpsdataShared.fix.longitude = std::numeric_limits<double>::quiet_NaN();
	m_gpsdataShared.fix.altitude = std::numeric_limits<double>::quiet_NaN();
	m_gpsdataShared.fix.track = std::numeric_limits<double>::quiet_NaN();
	m_gpsdataShared.fix.speed = std::numeric_limits<double>::quiet_NaN();
	m_gpsdataShared.online = 0;

	// Copy config params to local
	auto& misc = m_config->GetMutableConfig().miscConfig;
	m_gpsAvailable = (m_config->IsProcessorDfSlave() ? CConfig::EGpsAvailable::NOT_AVAILABLE : misc.gpsAvailable);
	m_fixedLatitude = misc.fixedLatitude;
	m_fixedLongitude = misc.fixedLongitude;
	m_fixedAltitude = misc.fixedAltitude;

	printf("m_gpsAvailable set to %d %d %d\n", (int) m_gpsAvailable, m_config->IsProcessorDfSlave(), (int) misc.gpsAvailable);

	// Start polling thread
	m_thread = std::thread(&CTciGps::Thread, this);

	// Wait (up to 5 seconds) to get the first valid gps data
	int waitSecs = 5;
	while (GetNumSatellites() == 0)
	{
		sleep(1);
		if (--waitSecs == 0) break;
	}
	TRACE("TciGps ctor waitSecs = %d\n", waitSecs);

	// Wait (up to 2 minutes) to get the first valid gps data
	double lat, lon, alt;
	for (size_t i = 0; i < 120; ++i)
	{
		if (GetPosition(lat, lon, alt))
			break;
		if ((i % 10) == 0)
		{
			TRACE("gpsd does not have location: %u\n", i);
		}
		sleep(1);
		if (Utility::IsQuitting())
		{
			CLog::Log(CLog::INFORMATION, "TciGps received QUIT signal");
			return;
		}
	}
	TRACE("TciGps ctor location = %f %f %f\n", lat, lon, alt);

	// Wait (up to 2 minutes) to get the PPS sync from ntpd
	// Check ntp status
	for (size_t i = 0; i < 120; ++i)
	{
		if (m_ntpPpsSync)	// Comes from thread
			break;

		if ((i % 10) == 0)
			CLog::Log(CLog::INFORMATION, "ntpd not syncd to %s: offset %.2f ms: %u", m_ntpQuery.GetNtpRef().c_str(), m_offsetms, i);
		sleep(1);
		if (Utility::IsQuitting())
		{
			CLog::Log(CLog::INFORMATION, "TciGps received QUIT signal");
			return;
		}
	}
//	m_ntpPpsSync = m_ntpQuery.GetStatus();
	if (m_ntpPpsSync)
		CLog::Log(CLog::INFORMATION, "ntpd syncd to %s", m_ntpQuery.GetNtpRef().c_str());
	else
		CLog::Log(CLog::WARNINGS, "ntpd did not sync to %s", m_ntpQuery.GetNtpRef().c_str());
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CTciGps::~CTciGps(void)
{
	TRACE("CTciGps dtor 1\n");
	{
		std::lock_guard<std::mutex> lock(m_shutdownMutex);
		m_shutdown = true;
	}

	m_shutdownCond.notify_all();
	if(m_thread.joinable())
	{
		m_thread.join();
	}

	if (m_connected)
	{
		gps_close(&m_gpsdataRaw);
	}
	TRACE("CTciGps dtor 2\n");

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Gps get time
//
bool CTciGps::GetTime(double& date) const
{
	{
		CSharedLockGuard lock(m_rwLock);
		if (m_connected && isnan(m_gpsdataShared.fix.time) == 0)
		{
//			TRACE("Using gps time\n");
			date = m_gpsdataShared.fix.time / 86400.0 + 25569.0;
			return true;
		}
	}
	// Fall back to linux time
//	TciGps::GetDATE(date);
	date = Utility::CurrentTimeAsDATE();
	return false;
}


/////////////////////////////////////////////////////////////////////
//
// Gps get time as formatted string
//
bool CTciGps::GetGpsTime(std::string& str) const
{
	timestamp_t time = 0;

	bool fromGps = CSharedLockGuard::CallLocked(m_rwLock, [&]
	{
		if (m_connected && isnan(m_gpsdataShared.fix.time) == 0)
		{
			time = m_gpsdataShared.fix.time;
			return true;
		}
		return false;
	});

	if (!fromGps)
	{
		// Fall back to linux time
		str = Utility::CurrentTimeAsString(Utility::USEC);
		return false;
	}
	timespec ts;
	ts.tv_sec = static_cast<time_t>(time);
	ts.tv_nsec = (time - ts.tv_sec) * 1.e9;
	str = Utility::TimespecAsString(ts, Utility::USEC);
	return true;
}


/////////////////////////////////////////////////////////////////////
//
// Gps get fix mode
//
int CTciGps::GetMode(void) const
{
	{
		CSharedLockGuard lock(m_rwLock);
		if (m_connected)
		{
			return m_gpsdataShared.fix.mode;
		}
	}

	return 0;
}


/////////////////////////////////////////////////////////////////////
//
// Gps get number of satellites
//
int CTciGps::GetNumSatellites(void) const
{
	{
		CSharedLockGuard lock(m_rwLock);
		if (m_connected)
		{
			return m_gpsdataShared.satellites_used;
		}
	}

	return 0;
}


/////////////////////////////////////////////////////////////////////
//
// Gps get location
//
bool CTciGps::GetPosition(double& lat, double& lon, double& alt) const
{
	if (m_gpsAvailable == CConfig::EGpsAvailable::NOT_AVAILABLE)
	{
		lat = m_fixedLatitude;
		lon = m_fixedLongitude;
		alt = m_fixedAltitude;
		return true;
	}
	else if (m_gpsAvailable == CConfig::EGpsAvailable::INTERNAL)
	{
		CSharedLockGuard lock(m_rwLock);
		if (m_connected && m_gpsdataShared.fix.mode >= MODE_2D &&
			isnan(m_gpsdataShared.fix.latitude) == 0 && isnan(m_gpsdataShared.fix.longitude) == 0)
		{
//			TRACE("Using gps lat/lon\n");
			lat = m_gpsdataShared.fix.latitude;
			lon = m_gpsdataShared.fix.longitude;
			if (m_gpsdataShared.fix.mode >= MODE_3D && isnan(m_gpsdataShared.fix.altitude) == 0)
			{
//				TRACE("Using gps alt\n");
				alt = m_gpsdataShared.fix.altitude;
			}
			else
			{
				alt = 0;
			}
			return true;
		}
	}

	// Fall back
	alt = 0.0;
	lat = 90.0;
	lon = 0.0;
	return false;
}


/////////////////////////////////////////////////////////////////////
//
// Gps get speed (m/s)
//
bool CTciGps::GetSpeed(double& speed)
{
	CSharedLockGuard lock(m_rwLock);
	if (m_connected && m_gpsdataShared.fix.mode >= MODE_2D &&
		isnan(m_gpsdataShared.fix.speed) == 0)
	{
//		TRACE("gps speed = %f\n", m_gpsdataShared.fix.speed);
		speed = m_gpsdataShared.fix.speed;
		return true;
	}

	// Fall back
	speed = 0.0;
	return false;
}


/////////////////////////////////////////////////////////////////////
//
// Gps get track (deg)
//
bool CTciGps::GetTrack(double& track)
{
	CSharedLockGuard lock(m_rwLock);
	if (m_connected && m_gpsdataShared.fix.mode >= MODE_2D &&
		isnan(m_gpsdataShared.fix.track) == 0)
	{
//		TRACE("gps track = %f\n", m_gpsdataShared.fix.track);
		track = m_gpsdataShared.fix.track;
		return true;
	}

	// Fall back
	track = 0.0;
	return false;
}


/////////////////////////////////////////////////////////////////////
//
// Gps test if connected
//
bool CTciGps::IsConnected(void) const
{
	CSharedLockGuard lock(m_rwLock);
	return m_connected;
}


/////////////////////////////////////////////////////////////////////
//
// Gps polling thread function
//
void CTciGps::Thread(void)
{
	try
	{
		// Poll until shutdown
		auto timeout = std::chrono::seconds{0};
		auto Polling = [this, &timeout]
		{
			std::unique_lock<std::mutex> lock(m_shutdownMutex);
			return !m_shutdownCond.wait_for(lock, timeout, [this] { return m_shutdown; });
		};

		while(Polling())
		{

			if (Utility::IsQuitting())
			{
				m_shutdown = true;
				CLog::Log(CLog::INFORMATION, "TciGps Thread received QUIT signal");
				break;
			}

			timeout = std::chrono::seconds{1};
			m_ntpPpsSync = m_ntpQuery.GetStatus(m_offsetms);

			if (!m_connected)
			{
				if (m_gpsAvailable == CConfig::EGpsAvailable::INTERNAL)
				{
					if (gps_open(GPSD_SHARED_MEMORY, NULL, &m_gpsdataRaw) == 0)
					{
						m_connected = true;
						TRACE("Connected to gpsd\n");
						gps_read(&m_gpsdataRaw);
					}
					else
					{
						TRACE("gps_open failed %d\n", errno);
					}
				}
			}
			if (m_connected)
			{
				if (GpsWait())
				{
					// Some data is available
					auto readRv = gps_read(&m_gpsdataRaw);
					if (readRv > 0)
					{
						TraceGps(m_gpsdataRaw);
						CLockGuard lock(m_rwLock);
						// Only copy valid data, leave rest unchanged.
						if (isnan(m_gpsdataRaw.fix.time) == 0)
						{
							m_gpsdataShared.fix.time = m_gpsdataRaw.fix.time;
						}
						m_gpsdataShared.fix.mode = m_gpsdataRaw.fix.mode;
						if (m_gpsdataRaw.fix.mode >= MODE_2D)
						{
							if (isnan(m_gpsdataRaw.fix.latitude) == 0 && isnan(m_gpsdataRaw.fix.longitude) == 0)
							{
								m_gpsdataShared.fix.latitude = m_gpsdataRaw.fix.latitude;
								m_gpsdataShared.fix.longitude = m_gpsdataRaw.fix.longitude;
							}
							if (isnan(m_gpsdataRaw.fix.track) == 0)
							{
								m_gpsdataShared.fix.track = m_gpsdataRaw.fix.track;
							}
							if (isnan(m_gpsdataRaw.fix.speed) == 0)
							{
								m_gpsdataShared.fix.speed = m_gpsdataRaw.fix.speed;
							}
							if (m_gpsdataRaw.fix.mode >= MODE_3D && isnan(m_gpsdataRaw.fix.altitude) == 0)
							{
								m_gpsdataShared.fix.altitude = m_gpsdataRaw.fix.altitude;
							}
						}
						m_gpsdataShared.online = m_gpsdataRaw.online;
						m_gpsdataShared.satellites_used = m_gpsdataRaw.satellites_used;
						m_gpsdataShared.satellites_visible = m_gpsdataRaw.satellites_visible;
						TraceGps(m_gpsdataShared);

					}
					else
					{
						if (readRv == -1)
						{
							m_connected = false; // gps_open didn't get shared memory segment?
							gps_close(&m_gpsdataRaw);
							sleep(10);
						}
//						TRACE("gps_read failed %d %d\n", readRv, errno);
////					m_connected = false;
////					gps_close(&m_gpsdataRaw);
					}
				}
				else
				{
					TRACE("gps_waiting failed %d\n", errno);
//					m_connected = false;
//					gps_close(&m_gpsdataRaw);
				}
			}
		}

		printf("CTciGps::Thread exiting\n");
	}
	catch(...)
	{
		CFailure::OnFail(std::current_exception());
	}

	return;

}

bool CTciGps::GpsWait(void)
{
	for (size_t i = 0; i < 10; ++i)
	{
		if (gps_waiting(&m_gpsdataRaw, 100))	// 100 usec "spin"
		{
			return true;
		}
		usleep(50000);	// 50 msec sleep
	}
	return false;
//	return gps_waiting(&m_gpsdataRaw, 500000);
}

