/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2017 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "VCPMsg.h"
#include "NetConnection.h"
#include "Singleton.h"
#include "VCPCtrl.h"

class CVCPCtrlNet : public CNetConnection<CVCPCtrlNet>, public CVCPCtrl
{
public:
	// Functions
	CVCPCtrlNet(LPCTSTR service); // Server
	CVCPCtrlNet(SOCKET socket, _In_ CVCPCtrlNet*); // Dynamic connection
	~CVCPCtrlNet(void) {  TRACE("CVCPCtrlNet dtor1 %d\n", GetMode()); Shutdown(); TRACE("CVCPCtrlNet dtor2 %d\n", GetMode()); }
	static bool Send(_In_ CNetConnection<CVCPCtrlNet>* connection, _In_ const SVCPMsg* msg);
	virtual bool Send(_In_bytecount_(size) const void* msg, size_t size) { return CNetConnection<CVCPCtrlNet>::Send(msg, size); }
	void SendError(_In_ const SVCPMsg::SHdr& hdr, unsigned long code);
	static bool IsVCPControlClient(void)
	{
		std::lock_guard<std::mutex> lock(m_vcpCritSect);
		return (m_vcpControlClient != nullptr);
	}

private:
	// Functions
	inline virtual size_t BodySize(_In_ const void* hdr) const;
	static void DownConvert(_Inout_ const SVCPMsg*& msg);
	virtual size_t HeaderSize(void) const { return sizeof(SVCPMsg::SHdr); }
	virtual void OnClose(int errorCode); // Callback
	virtual void OnConnect(void); // Callback
	virtual void OnMessage(_In_bytecount_(size) const void* msg, size_t size); // Callback
	void OnVCPCtrl(_In_ const SVCPMsg* msg, _In_ CVCPCtrlNet* source);
	static void Send(_In_ CNetConnection<void>* client, _In_ const SVCPMsg& msg, _In_ LPCTSTR log);
	static void UpConvert(const SVCPMsg*& msg);
	static void ValidateVCPControlClient(const CVCPCtrlNet* source)
	{
		std::lock_guard<std::mutex> lock(m_vcpCritSect);
		if (m_vcpControlClient == nullptr) throw ErrorCodes::CMDINVALIDSTDMODE;	// No contol client.
		if (m_vcpControlClient != source) throw ErrorCodes::CMDINVALIDVCPMODE;	// This client is not the controlling one.
	}

	// Data
	SSmsMsg::VersionMap m_clientVersionMap;
//	CSingleton<CVCPCtrl> m_vcpControl;
	static SSmsMsg::VersionMap m_versionMap;
	static bool VERBOSE;
	static std::mutex m_vcpCritSect;
	static CVCPCtrlNet* m_vcpControlClient;

};


#if 0
//////////////////////////////////////////////////////////////////////
//
// Send a message, downconverting the version if necessary
//
bool CVCPCtrlNet::Send(_In_ CNetConnection<CVCPCtrlNet>* connection, _In_ const SVCPMsg* msg)
{
	// Check response version
	SSmsMsg::SVersionKey key = { msg->hdr.msgType, msg->hdr.msgSubType };
	SSmsMsg::VersionMap::const_iterator entry = m_versionMap.find(key);
	bool converted = false;
	unsigned char myRespVersion = (entry != m_versionMap.end() ? entry->second.respVersion : 0);

	if(myRespVersion > msg->hdr.respVersion)
	{
		// Downconvert to requested version (making a local copy)
		DownConvert(msg);
		converted = true;
	}
	else if(myRespVersion < msg->hdr.respVersion)
	{
		// Make a local copy and set response version to my version
		size_t size = offsetof(T, body) + msg->hdr.bodySize;
		SVCPMsg* newMsg = new(size) T;
		memcpy(newMsg, msg, size);
		newMsg->hdr.respVersion = myRespVersion;
		msg = newMsg;
		converted = true;
	}

	bool status = CNetConnection<CVCPCtrlNet>::Send(connection, msg, offsetof(SVCPMsg, body) + msg->hdr.bodySize);

	if(converted)
	{
		// Delete the local copy
		delete msg;
	}

	return status;
}
#endif


