/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "AvdTask.h"
#include "MeasurementTask.h"
#include "Metrics.h"
#include "OccupancyTask.h"
#include "PersistentData.h"
#include "ScanDfTask.h"
#include "Utility.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CMetrics::CMetrics(void) :
//	m_config(),
	m_connBufLen(0),
	m_database(),
	m_equipControl(),
	m_interactiveClient(nullptr),
	m_interactiveClientAnt(SMetricsMsg::ANT1),
	m_interactiveClientKey(0),
	m_pConnRespMsg(nullptr),
	m_priorityMode(false)
{
//	AllocAntNameChangeRespMsg();
//	srand(GetTickCount());
	// TODO: If this works, put into Utility as GetTickCount(void)
	auto x = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
	srand(x);
//	m_destroyed.ResetEvent();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CMetrics::~CMetrics(void)
{
	if (m_pConnRespMsg != nullptr)
	{
		ASSERT(m_connBufLen > 0);
		free(m_pConnRespMsg);
	}

//	if (m_pAntNameRespMsg != nullptr)
//	{
//		free(m_pAntNameRespMsg);
//	}

//	m_destroyed.SetEvent();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Delete a measurement
//
void CMetrics::DeleteResults(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	SMetricsMsg resp;
	resp.hdr = msg.hdr;
	std::string logMsg;

	switch(msg.hdr.msgSubType)
	{
	case SMetricsMsg::AVD_DELETE_REQUEST:
		resp.hdr.msgSubType = SMetricsMsg::AVD_DELETE_RESPONSE;
		logMsg = _T("AVD_DELETE_RESPONSE");
		break;

	case SMetricsMsg::DELETE_REQUEST:
		resp.hdr.msgSubType = SMetricsMsg::DELETE_RESPONSE;
		logMsg = _T("DELETE_RESPONSE");
		break;

	case SMetricsMsg::OCCUP_DELETE_REQUEST:
		resp.hdr.msgSubType = SMetricsMsg::OCCUP_DELETE_RESPONSE;
		logMsg = _T("OCCUP_DELETE_RESPONSE");
		break;

	case SMetricsMsg::SCANDF_DELETE_REQUEST:
		resp.hdr.msgSubType = SMetricsMsg::SCANDF_DELETE_RESPONSE;
		logMsg = _T("SCANDF_DELETE_RESPONSE");
		break;

	default:
		THROW_LOGIC_ERROR();
	}

	resp.hdr.bodySize = sizeof(SMetricsMsg::SMeasureCtrlMsgGenericResponse);
	resp.body.measureCtrlMsgGenericResponse.clientRequest = msg.body.measureCtrlMsgGenericRequest;
	resp.body.measureCtrlMsgGenericResponse.statusOfRequest =
		m_database->DeleteTask(msg.body.measureCtrlMsgGenericRequest.measureId);
	resp.body.measureCtrlMsgGenericResponse.stationId = GetStationId(client);
	logMsg += " status = ";
	logMsg += std::to_string(resp.body.measureCtrlMsgGenericResponse.statusOfRequest);
	client->SendImmediateResp(resp, logMsg.c_str());

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Forward an AUTOVIOLATE_CTRL, OCCUPANCY_CTRL or OCCUPANCYDF_CTRL to CEquipControl
//
void CMetrics::ForwardAvdOccupancyDfCtrl(_In_ const SMetricsMsg& msg, unsigned long measureId)
{
	SEquipCtrlMsg equipCtrlMsg;
	equipCtrlMsg.hdr = msg.hdr;
	equipCtrlMsg.hdr.destAddr = measureId;

	switch(msg.hdr.msgSubType)
	{
	case SMetricsMsg::AVD_RESUME_REQUEST:
	case SMetricsMsg::OCCUP_RESUME_REQUEST:
	case SMetricsMsg::SCANDF_RESUME_REQUEST:
		{
			SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;
			DATE startTime;
			DATE stopTime;
			DATE updateTime;
			equipCtrlMsg.hdr.msgSubType = SEquipCtrlMsg::RESUME_OCCUPANCY;
			equipCtrlMsg.hdr.bodySize = sizeof(SEquipCtrlMsg::STaskIdKey);
			m_database->RetrieveSchedule(measureId, equipCtrlMsg.body.taskIdKey.taskId, equipCtrlMsg.body.taskIdKey.key,
				state, startTime, stopTime, updateTime, nullptr);
		}

		break;

	case SMetricsMsg::AVD_SUSPEND_REQUEST:
	case SMetricsMsg::OCCUP_SUSPEND_REQUEST:
	case SMetricsMsg::SCANDF_SUSPEND_REQUEST:
		{
			SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;
			DATE startTime;
			DATE stopTime;
			DATE updateTime;
			equipCtrlMsg.hdr.msgSubType = SEquipCtrlMsg::SUSPEND_OCCUPANCY;
			equipCtrlMsg.hdr.bodySize = sizeof(SEquipCtrlMsg::STaskIdKey);
			m_database->RetrieveSchedule(measureId, equipCtrlMsg.body.taskIdKey.taskId, equipCtrlMsg.body.taskIdKey.key,
				state, startTime, stopTime, updateTime, nullptr);
		}

		break;

	case SMetricsMsg::AVD_TERMINATE_REQUEST:
	case SMetricsMsg::OCCUP_TERMINATE_REQUEST:
	case SMetricsMsg::SCANDF_TERMINATE_REQUEST:
		{
			SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;
			DATE startTime;
			DATE stopTime;
			DATE updateTime;
			equipCtrlMsg.hdr.msgType = SEquipCtrlMsg::EQUIPMENT_CTRL;
			equipCtrlMsg.hdr.msgSubType = SEquipCtrlMsg::TASK_TERMINATE;
			equipCtrlMsg.hdr.bodySize = sizeof(SEquipCtrlMsg::STaskIdKey);
			m_database->RetrieveSchedule(measureId, equipCtrlMsg.body.taskIdKey.taskId, equipCtrlMsg.body.taskIdKey.key,
				state, startTime, stopTime, updateTime, nullptr);
		}

		break;

	case SMetricsMsg::SCHED_AVD_REQUEST:
		{
			equipCtrlMsg.hdr.msgSubType = SEquipCtrlMsg::GET_AUTOVIOLATE;
			size_t bodySize = offsetof(SEquipCtrlMsg::SGetAutoViolateCmd, band[msg.body.schedMsgAutoViolateRequest.clientMeasure.numBands]);

			if(bodySize > sizeof(SEquipCtrlMsg::UBody))
			{
				throw ErrorCodes::AVDTOOMANYBANDS;
			}

			equipCtrlMsg.hdr.bodySize = static_cast<unsigned long>(bodySize);
			memcpy(&equipCtrlMsg.body.getAutoViolateCmd, &msg.body.schedMsgAutoViolateRequest.clientMeasure, bodySize);
		}

		break;

	case SMetricsMsg::SCHED_OCCUP_REQUEST:
		{
			equipCtrlMsg.hdr.msgSubType = SEquipCtrlMsg::GET_OCCUPANCY;
			size_t bodySize = offsetof(SEquipCtrlMsg::SGetOccupancyCmd, band[msg.body.schedMsgOccupancyRequest.clientMeasure.numBands]);

			if(bodySize > sizeof(SEquipCtrlMsg::UBody))
			{
				throw ErrorCodes::OCCUPANCYTOOMANYBANDS;
			}

			equipCtrlMsg.hdr.bodySize = static_cast<unsigned long>(bodySize);
			memcpy(&equipCtrlMsg.body.getOccupancyCmd, &msg.body.schedMsgOccupancyRequest.clientMeasure, bodySize);
		}

		break;

	case SMetricsMsg::SCHED_SCANDF_REQUEST:
		{
			equipCtrlMsg.hdr.msgSubType = SEquipCtrlMsg::GET_OCCUPANCY_SCANDF;
			size_t bodySize = offsetof(SEquipCtrlMsg::SGetScanDfCmd, band[msg.body.schedMsgScanDfRequest.clientMeasure.numBands]);

			if(bodySize > sizeof(SEquipCtrlMsg::UBody))
			{
				throw ErrorCodes::SCANDFTOOMANYBANDS;
			}

			equipCtrlMsg.hdr.bodySize = static_cast<unsigned long>(bodySize);
			equipCtrlMsg.body.getScanDfCmd = msg.body.schedMsgScanDfRequest.clientMeasure;
			memcpy(&equipCtrlMsg.body.getScanDfCmd, &msg.body.schedMsgScanDfRequest.clientMeasure, bodySize);
		}

		break;

	default:
		THROW_LOGIC_ERROR();
	}

	try
	{
		// Send to Equip Control using nullptr client so results go to database
		switch(equipCtrlMsg.hdr.msgType)
		{
		case SEquipCtrlMsg::AUTOVIOLATE_CTRL:
			m_equipControl->OnAvdCtrl(&equipCtrlMsg, nullptr);
			break;

		case SEquipCtrlMsg::EQUIPMENT_CTRL:
			m_equipControl->OnEquipmentCtrl(&equipCtrlMsg, nullptr);
			break;

		case SEquipCtrlMsg::OCCUPANCY_CTRL:
			m_equipControl->OnOccupancyCtrl(&equipCtrlMsg, nullptr);
			break;

		case SEquipCtrlMsg::OCCUPANCYDF_CTRL:
			m_equipControl->OnOccupancyDfCtrl(&equipCtrlMsg, nullptr);
			break;

		default:
			THROW_LOGIC_ERROR();
		}
	}
	catch(ErrorCodes::EErrorCode error)
	{
		switch(error)
		{
		case ErrorCodes::INVALIDTASKID:
			// Eat the error
			break;

		default:
			throw;
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Forward a MEASUREMENT_CTRL to CEquipControl
//
void CMetrics::ForwardMeasureCtrl(_In_ const SMetricsMsg& msg, _In_opt_ CMetricsNet* client, unsigned long measureId)
{
	SEquipCtrlMsg equipCtrlMsg;
	equipCtrlMsg.hdr = msg.hdr;
	equipCtrlMsg.hdr.destAddr = measureId;
//	CTask::EPriority priority = CTask::NON_QUEUED;

	switch(msg.hdr.msgSubType)
	{
	case SMetricsMsg::BAND_REQUEST_NETTED:
		{
			CLockGuard lock(m_critSect);

			ClientDataMap::iterator clientData = m_clientDataMap.find(client);

			if(clientData == m_clientDataMap.end())
			{
				// Client disconnected
				return;
			}

			clientData->second.isNetted = true;
			equipCtrlMsg.hdr.msgType = SEquipCtrlMsg::METRICS_CTRL;
			equipCtrlMsg.hdr.msgSubType = SEquipCtrlMsg::GET_BAND_REQUEST;
			equipCtrlMsg.hdr.bodySize = 0;
		}
		break;

	case SMetricsMsg::BAND_REQUEST:
	    {
	    	CLockGuard lock(m_critSect);

		    ClientDataMap::iterator clientData = m_clientDataMap.find(client);

		    if (clientData != m_clientDataMap.end())
		    {
				clientData->second.client = msg.body.bandReq.clientInfo;
		    }
			else
			{
				// Client disconnected
				return;
			}

            equipCtrlMsg.hdr.msgType = SEquipCtrlMsg::METRICS_CTRL;
		    equipCtrlMsg.hdr.msgSubType = SEquipCtrlMsg::GET_BAND_REQUEST;
		    equipCtrlMsg.hdr.bodySize = 0;
	    }

		break;

	case SMetricsMsg::HEADING_REQUEST:
		{
			//SMetricsMsg::HEADING_REQUEST version 1 is translated to
			// SEquipCtrlMsg::GET_HEADING version 0.
			if( msg.hdr.cmdVersion == 1 )
			{
				equipCtrlMsg.hdr.cmdVersion = 0;
				equipCtrlMsg.hdr.respVersion = 0;
			}

			equipCtrlMsg.hdr.msgType = SEquipCtrlMsg::METRICS_CTRL;
			equipCtrlMsg.hdr.msgSubType = SEquipCtrlMsg::GET_HEADING;
			equipCtrlMsg.hdr.bodySize = 0;
		}

		break;

	case SMetricsMsg::DWELL_REQUEST:
		equipCtrlMsg.hdr.msgType = SEquipCtrlMsg::METRICS_CTRL;
		equipCtrlMsg.hdr.msgSubType = SEquipCtrlMsg::GET_DWELL;
		equipCtrlMsg.hdr.bodySize = sizeof(SEquipCtrlMsg::SGetDwellCmd);
		equipCtrlMsg.body.getDwellCmd = msg.body.measureCtrlDwellRequest.dwellRequest;
		break;

	case SMetricsMsg::SCHED_REQUEST:
		equipCtrlMsg.hdr.msgType = SEquipCtrlMsg::METRICS_CTRL;
		equipCtrlMsg.hdr.msgSubType = SEquipCtrlMsg::GET_MEAS;
		equipCtrlMsg.hdr.bodySize = sizeof(SEquipCtrlMsg::SGetMeasCmd);
		equipCtrlMsg.body.getMeasCmd = msg.body.schedMsgRequest.clientMeasure;

		if (equipCtrlMsg.body.getMeasCmd.iqCmd.outputType != SSmsMsg::NONE && equipCtrlMsg.body.getMeasCmd.iqCmd.tdoa)
		{
			equipCtrlMsg.body.getMeasCmd.iqCmd.startTime = msg.body.schedMsgRequest.clientOptions.startMeasureDateTime;
//			priority = CTask::SYSTEM;
		}
		else
		{
			equipCtrlMsg.body.getMeasCmd.iqCmd.startTime = 0.0;
//			priority = (msg.body.schedMsgRequest.clientOptions.calRequest == SMetricsMsg::SSchedMsgOptions::CONVENIENT ? CTask::BACKGROUND : CTask::IMMEDIATE);
		}

		client = nullptr; // Causes results to be sent to database rather than client
		break;

	case SMetricsMsg::SERVER_LOCATION_REQUEST:
		equipCtrlMsg.hdr.msgType = SEquipCtrlMsg::METRICS_CTRL;
		equipCtrlMsg.hdr.msgSubType = SEquipCtrlMsg::GET_GPS;
		equipCtrlMsg.hdr.bodySize = 0;
		break;

	default:
		THROW_LOGIC_ERROR();
	}

	m_equipControl->OnMetricsCtrl(&equipCtrlMsg, /*priority,*/ client);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Forward a received STATUS_CTRL message to CEquipControl
//
void CMetrics::ForwardStatusCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	switch (msg.hdr.msgSubType)
	{
	case SMetricsMsg::GET_FAULT_REQUEST:
	    {
		    SEquipCtrlMsg equipCtrlMsg;
		    equipCtrlMsg.hdr = msg.hdr;
		    m_equipControl->OnStatusCtrl(&equipCtrlMsg, client);
	    }

		break;

	default:
		THROW_LOGIC_ERROR();
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Forward a received message to CEquipControl
//
void CMetrics::ForwardToEquipControl(_In_ const SMetricsMsg& msg, _In_opt_ CMetricsNet* client, unsigned long measureId)
{
	switch(msg.hdr.msgType)
	{
	case SMetricsMsg::ANT_CTRL:
		{
			SEquipCtrlMsg equipCtrlMsg;
			equipCtrlMsg.hdr = msg.hdr;

			switch(msg.hdr.msgSubType)
			{
			case SEquipCtrlMsg::GET_ANT:
				break;

			case SEquipCtrlMsg::SET_ANT:
				equipCtrlMsg.body.antSetCtrlCmd = msg.body.antSetCtrlCmd;
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			m_equipControl->OnAntCtrl(&equipCtrlMsg, client);
		}

		break;

#if 0
		case SMetricsMsg::AUDIO_SWITCH:
		{
			SEquipCtrlMsg equipCtrlMsg;
			equipCtrlMsg.hdr = msg.hdr;

			switch(msg.hdr.msgSubType)
			{
			case SEquipCtrlMsg::SET_SWITCH:
				equipCtrlMsg.hdr.bodySize = sizeof(SEquipCtrlMsg::SSetAudioSwitchCmd);
				equipCtrlMsg.body.setAudioSwitchCmd = msg.body.setAudioSwitchMsg.setSwitch;
				break;

			case SEquipCtrlMsg::SET_PHONE_HOOK:
				equipCtrlMsg.hdr.bodySize = sizeof(SEquipCtrlMsg::SSetPhoneHookCmd);
				equipCtrlMsg.body.setPhoneHookCmd = msg.body.setPhoneHookMsg.setPhone;
				break;

			case SEquipCtrlMsg::SET_AUTOANSWER_MODE:
				equipCtrlMsg.hdr.bodySize = sizeof(SEquipCtrlMsg::SSetAutoAnswerCmd);
				equipCtrlMsg.body.setAutoAnswerCmd = msg.body.setAutoAnswerMsg.setAutoAns;
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			m_equipControl->OnAudioSwitch(&equipCtrlMsg, client);
		}

		break;
#endif

	case SMetricsMsg::AUTOVIOLATE_CTRL:
	case SMetricsMsg::OCCUPANCY_CTRL:
	case SMetricsMsg::OCCUPANCYDF_CTRL:
		ForwardAvdOccupancyDfCtrl(msg, measureId);
		break;

	case SMetricsMsg::BIST_CTRL:
		{
			SEquipCtrlMsg equipCtrlMsg;
			equipCtrlMsg.hdr = msg.hdr;

			switch(msg.hdr.msgSubType)
			{
			case SMetricsMsg::MEASUREMENT_REQUEST:
				equipCtrlMsg.hdr.msgSubType = SEquipCtrlMsg::GET_BIST_RESULT;
				equipCtrlMsg.hdr.bodySize = 0;
				break;

			case SMetricsMsg::GET_BIST:
			case SMetricsMsg::GET_DIAGNOSTICS:
			{
				// First do a passband calibration if this is the df master
				bool doPbCal = false;
				try
				{
					doPbCal = CWeakSingleton<CProcessorNode>()->IsProcessorDfMaster();
				}
				catch(CWeakSingleton<CProcessorNode>::NoInstance&) {}
				if (doPbCal)
				{
					equipCtrlMsg.hdr.msgType = SEquipCtrlMsg::DF_CTRL;
					equipCtrlMsg.hdr.msgSubType = SEquipCtrlMsg::INITIALIZE_DF;
					equipCtrlMsg.hdr.cmdVersion = 2;
					equipCtrlMsg.hdr.respVersion = 2;
					equipCtrlMsg.hdr.bodySize = sizeof(SEquipCtrlMsg::SInitializeDf);
					equipCtrlMsg.body.initializeDf.freq = Units::Frequency(0).GetRaw();
					equipCtrlMsg.body.initializeDf.rxBw = Units::Frequency(0).GetRaw();
					equipCtrlMsg.body.initializeDf.numIntegrations = 0;
					equipCtrlMsg.body.initializeDf.hf = false;
					m_equipControl->OnDfCtrl(&equipCtrlMsg, nullptr);
				}
			}
				// Now do the BIST / diagnostics
				equipCtrlMsg.hdr = msg.hdr;	// restore the header
				equipCtrlMsg.hdr.bodySize = 0;
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			m_equipControl->OnBistCtrl(&equipCtrlMsg, client);
		}

		break;

	case SMetricsMsg::DEMOD_CTRL:
		{
			SEquipCtrlMsg equipCtrlMsg;
			equipCtrlMsg.hdr = msg.hdr;

			switch(msg.hdr.msgSubType)
			{
			case SEquipCtrlMsg::SET_RCVR:
				equipCtrlMsg.hdr.bodySize = sizeof(SEquipCtrlMsg::SRcvrCtrlCmd);
				equipCtrlMsg.body.rcvrCtrlCmd = msg.body.recvrCntlReqMsg.recvCmd;
				break;

			case SEquipCtrlMsg::SET_PAN_PARA:
				equipCtrlMsg.hdr.bodySize = sizeof(SEquipCtrlMsg::SPanParaCmd);
				equipCtrlMsg.body.panParaCmd = msg.body.panParaReqMsg.panCmd;
				break;

			case SEquipCtrlMsg::SET_AUDIO_PARAMS:
				equipCtrlMsg.hdr.bodySize = sizeof(SEquipCtrlMsg::SAudioParamsCmd);
				equipCtrlMsg.body.audioParamsCmd = msg.body.audioParamsCmd;
				break;

			case SEquipCtrlMsg::FREE_AUDIO_CHANNEL:
				equipCtrlMsg.hdr.bodySize = sizeof(SEquipCtrlMsg::SFreeAudioChannelCmd);
				equipCtrlMsg.body.freeAudioChannelCmd = msg.body.freeAudioChannelCmd;
				break;

			default:
				THROW_LOGIC_ERROR();
			}
			m_equipControl->OnDemodCtrl(&equipCtrlMsg, client);
		}

		break;

	case SMetricsMsg::MEASURE_CTRL:
		ForwardMeasureCtrl(msg, client, measureId);
		break;

	case SMetricsMsg::MEASUREMENT_PAN_REQ:
		{
//			__analysis_assert(client != nullptr);
			SEquipCtrlMsg equipCtrlMsg;
			equipCtrlMsg.hdr = msg.hdr;
			equipCtrlMsg.hdr.msgType = SEquipCtrlMsg::PAN_DISP_CTRL;
			equipCtrlMsg.hdr.bodySize = sizeof(SEquipCtrlMsg::SGetPanCmd);
			equipCtrlMsg.body.getPanCmd = msg.body.panMeasurementReqMsg.panCmd;
			m_equipControl->OnPanDispCtrl(&equipCtrlMsg, client);
		}

		break;

#if 0
	case SMetricsMsg::SOUNDER_CTRL:
	{
		__analysis_assert(client != nullptr);
		SEquipCtrlMsg equipCtrlMsg;
		equipCtrlMsg.hdr = msg.hdr;

		switch (msg.hdr.msgSubType)
		{
			case SMetricsMsg::SOUNDER_SWEEP_SETUP:
				ASSERT(equipCtrlMsg.hdr.bodySize == sizeof(equipCtrlMsg.body.sounderSweepParamsSetCmd));
				equipCtrlMsg.body.sounderSweepParamsSetCmd.startFreq = msg.body.sounderSweepParamsSetCmd.startFreq;
				equipCtrlMsg.body.sounderSweepParamsSetCmd.endFreq = msg.body.sounderSweepParamsSetCmd.endFreq;
				equipCtrlMsg.body.sounderSweepParamsSetCmd.sweepRate = msg.body.sounderSweepParamsSetCmd.sweepRate;
				equipCtrlMsg.body.sounderSweepParamsSetCmd.interval = msg.body.sounderSweepParamsSetCmd.interval;
				equipCtrlMsg.body.sounderSweepParamsSetCmd.powerLevel = msg.body.sounderSweepParamsSetCmd.powerLevel;
				equipCtrlMsg.body.sounderSweepParamsSetCmd.blankerOn = msg.body.sounderSweepParamsSetCmd.blankerOn;
				break;

			case SMetricsMsg::SOUNDER_BLANKER_FREQ:
				ASSERT(equipCtrlMsg.hdr.bodySize == sizeof(equipCtrlMsg.body.sounderBlankerFreqParamsSetCmd));
				for (int i = 0; i < 96; ++i)
					equipCtrlMsg.body.sounderBlankerFreqParamsSetCmd.freqs[i] = msg.body.sounderBlankerFreqParamsSetCmd.freqs[i];
				break;

			case SMetricsMsg::SOUNDER_START_STOP:
				ASSERT(equipCtrlMsg.hdr.bodySize == sizeof(equipCtrlMsg.body.sounderSweepOnOffCtrlCmd));
				equipCtrlMsg.body.sounderSweepOnOffCtrlCmd.Run = msg.body.sounderSweepOnOffCtrlCmd.Run;
				break;

			case SMetricsMsg::SOUNDER_TEST:
				ASSERT(equipCtrlMsg.hdr.bodySize == sizeof(equipCtrlMsg.body.sounderSelfTestCmd));
				equipCtrlMsg.body.sounderSelfTestCmd.Start = msg.body.sounderSelfTestCmd.Start;
				break;

			case SMetricsMsg::SOUNDER_STATUS_REQ:
				//Empty body
				break;

			case SMetricsMsg::SOUNDER_IONOREQUEST:
                // Empty body
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			m_equipControl->OnSounderCtrl(&equipCtrlMsg, client);
		}

		break;
#endif
	default:
		THROW_LOGIC_ERROR();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the client ID for a client
//
unsigned long CMetrics::GetClientId(_In_ const CMetricsNet* client) const
{
	CSharedLockGuard lock(m_critSect);
	ClientDataMap::const_iterator clientData = m_clientDataMap.find(client);

	if(clientData != m_clientDataMap.end())
	{
		return clientData->second.client.clientId;
	}
	else
	{
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the station ID for a client
//
unsigned long CMetrics::GetStationId(_In_ const CMetricsNet* client) const
{
	if(client == nullptr)
	{
		return 0;
	}

	auto address = client->GetLocalAddress();

	if(address.ss_family == AF_INET)
	{
		return ntohl(reinterpret_cast<SOCKADDR_IN*>(&address)->sin_addr.s_addr);
	}
	else if(address.ss_family == AF_INET6)
	{
		// Use least significant 4 bytes of IPV6 address
		union { unsigned long ulVal; unsigned char ucVal[4]; } u;
		memcpy(u.ucVal, reinterpret_cast<SOCKADDR_IN6*>(&address)->sin6_addr.s6_addr + 12, 4);
		return u.ulVal;
//		return *reinterpret_cast<unsigned long*>(&reinterpret_cast<SOCKADDR_IN6*>(&address)->sin6_addr.__in6_u.__u6_addr8[12]);
	}
	else
	{
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the station name associated with a client connection
//
void CMetrics::GetStationName(_In_ const CMetricsNet* client, char(&name)[STATIONNAME_LEN]) const
{
	CSharedLockGuard lock(m_critSect);
	ClientDataMap::const_iterator clientData = m_clientDataMap.find(client);

	if (clientData != m_clientDataMap.end())
	{
		strcpy_s(name, clientData->second.stationName);
	}
	else
	{
		name[0] = '\0';
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Is a client netted?
//
bool CMetrics::IsNetted(_In_ const CMetricsNet* client) const
{
	CSharedLockGuard lock(m_critSect);
	ClientDataMap::const_iterator clientData = m_clientDataMap.find(client);

	if (clientData != m_clientDataMap.end())
	{
		return clientData->second.isNetted;
	}
	else
	{
		return false;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Handle an ANT_CTRL message
//
void CMetrics::OnAntCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	if(client == m_interactiveClient)
	{
		switch(msg.hdr.msgSubType)
		{
		case SMetricsMsg::GET_ANT:
			ForwardToEquipControl(msg, client);
			break;

		case SMetricsMsg::SET_ANT:
			m_interactiveClientAnt = msg.body.antSetCtrlCmd.antenna;
			ForwardToEquipControl(msg, client);
			break;

		default:
			CLog::Log(CLog::INFORMATION, _T("Got invalid ANT_CTRL subtype %lu"), msg.hdr.msgSubType);
			throw ErrorCodes::INVALIDSUBTYPE;
		}
	}
	else
	{
		if(auto peer = client->GetPeerAddress())
		{
			CLog::Log(CLog::INFORMATION, _T("Got ANT_CTRL from %s"), peer->ai_canonname);
		}
		throw ErrorCodes::ERROR_NO_CONNECTION;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle an AUTOVIOLATE_CTRL message
//
void CMetrics::OnAvdCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	auto peer = (client != nullptr ? client->GetPeerAddress() : nullptr);

	switch(msg.hdr.msgSubType)
	{
	case SMetricsMsg::AVD_DELETE_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got AVD_DELETE_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		DeleteResults(msg, client);
		break;

	case SMetricsMsg::AVD_MEASUREMENT_REQUEST:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got AVD_MEASUREMENT_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
			}
			SMetricsMsg resp;
			resp.hdr = msg.hdr;
			unsigned long stationId = GetStationId(client);
			m_database->SendResults(client, resp, msg.body.measureCtrlMsgGenericRequest, SEquipCtrlMsg::OCC_FREQUENCY_RESULT,
				SMetricsMsg::AVD_FREQ_MEASURE, stationId, resp.body.measureFreqResultsResponse, "AVD_FREQ_MEASURE");
			bool sent = m_database->SendResults(client, resp, msg.body.measureCtrlMsgGenericRequest, SEquipCtrlMsg::OCC_CHANNEL_RESULT,
				SMetricsMsg::AVD_CHANNEL_MEASURE, stationId, resp.body.measureChanResultsResponse, "AVD_CHANNEL_MEASURE");
			sent = m_database->SendResults(client, resp, msg.body.measureCtrlMsgGenericRequest, SEquipCtrlMsg::AUTOVIOLATE_BWMEAS_RESULT,
				SMetricsMsg::AVD_BWMEAS_MEASURE, stationId, resp.body.measureAvdResultsResponse, "AVD_BWMEAS_MEASURE") || sent;
			sent = m_database->SendResults(client, resp, msg.body.measureCtrlMsgGenericRequest, SEquipCtrlMsg::AUTOVIOLATE_FREQMEAS_RESULT,
				SMetricsMsg::AVD_FREQMEAS_MEASURE, stationId, resp.body.measureAvdResultsResponse, "AVD_FREQMEAS_MEASURE") || sent;

			if (!sent)
			{
				resp.hdr.msgSubType = SMetricsMsg::REQUEST_FAIL_NOT_FOUND;
				resp.hdr.bodySize = sizeof(SMetricsMsg::SMeasureErrorResponse);
				resp.body.measureErrorResponse.clientRequest = msg.body.measureCtrlMsgGenericRequest;
				resp.body.measureErrorResponse.equipMeasurement = 0;
				resp.body.measureErrorResponse.stationId = GetStationId(client);
				resp.body.measureErrorResponse.statusOfRequest = ErrorCodes::TASK_COMPLETED;
				client->SendImmediateResp(resp, "REQUEST_FAIL_NOT_FOUND");
			}
		}

		break;

	case SMetricsMsg::AVD_RESUME_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got AVD_RESUME_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		ForwardToEquipControl(msg, client, msg.body.measureCtrlMsgGenericRequest.measureId);
		break;

	case SMetricsMsg::AVD_STATE_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got AVD_STATE_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		RequestState(msg, client);
		break;

	case SMetricsMsg::AVD_SUSPEND_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got AVD_SUSPEND_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		ForwardToEquipControl(msg, client, msg.body.measureCtrlMsgGenericRequest.measureId);
		break;

	case SMetricsMsg::AVD_TERMINATE_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got AVD_TERMINATE_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		ForwardToEquipControl(msg, client, msg.body.measureCtrlMsgGenericRequest.measureId);
		break;

	case SMetricsMsg::SCHED_AVD_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got SCHED_AVD_REQUEST from %s"), peer->ai_canonname);
		}

		assert(msg.hdr.cmdVersion != 0);
//		if (msg.hdr.cmdVersion == 0)
//		{
//			// Patch up antenna
//			msg.body.schedMsgAutoViolateRequest.clientMeasure.ant = m_interactiveClientAnt;
//		}

		ScheduleAvd(msg, client);
		break;

	default:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("invalid AUTOVIOLATE_CTRL subtype %u from %s"), msg.hdr.msgSubType, peer->ai_canonname);
		}
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;

}


//////////////////////////////////////////////////////////////////////
//
// Handle a BIST_CTRL message
//
void CMetrics::OnBistCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	switch(msg.hdr.msgSubType)
	{
	case SMetricsMsg::MEASUREMENT_REQUEST:
		{
			// Remember client ID and forward to EquipControl for last BIST result
			CLockGuard lock(m_critSect);

			ClientDataMap::iterator clientData = m_clientDataMap.find(client);

			if(clientData == m_clientDataMap.end())
			{
				// Client disconnected
				return;
			}

			if (clientData->second.client.clientId != 0)
			{
#ifdef NDEBUG
#else
				if (clientData->second.client.clientId != msg.body.measureCtrlMsgGenericRequest.clientId)
				{
					printf("MEASUREMENT_REQUEST clientId mismatch %lu %lu\n", clientData->second.client.clientId, msg.body.measureCtrlMsgGenericRequest.clientId);
//					ASSERT(clientData->second.client.clientId == msg.body.measureCtrlMsgGenericRequest.clientId);
				}
#endif
			}
			else
			{
				clientData->second.client.clientId = msg.body.measureCtrlMsgGenericRequest.clientId;
			}

			lock.Unlock();
			ForwardToEquipControl(msg, client);
		}

		break;

	case SMetricsMsg::GET_BIST:
	case SMetricsMsg::GET_DIAGNOSTICS:
		{
			// Treat all requests as immediate
			DATE now = Utility::CurrentTimeAsDATE();
//			TciGps::GetDATE(now);
			SMetricsMsg resp;
			resp.hdr = msg.hdr;
			resp.hdr.msgSubType = SMetricsMsg::BIST_SCHED_RESPONSE;
			resp.hdr.bodySize = sizeof(SMetricsMsg::SSchedMsgResponse);
			resp.body.schedMsgResponse.clientTaskId = msg.body.schedMsgOptions.clientTaskId;
			resp.body.schedMsgResponse.measureId = 0;
			resp.body.schedMsgResponse.state = SMetricsMsg::SSchedMsgResponse::RESULTS_COMPLETE; // Tell client not to request results; they'll just be sent
			resp.body.schedMsgResponse.stationId = GetStationId(client);
			resp.body.schedMsgResponse.statusOfRequest = ErrorCodes::TASK_COMPLETED;
			resp.body.schedMsgResponse.startMeasure = now;
			resp.body.schedMsgResponse.endMeasure = now;

			// Send schedule response
			std::string logMsg = "BIST_SCHED_RESPONSE measureId=" + std::to_string(resp.body.schedMsgResponse.measureId);
			client->SendImmediateResp(resp, logMsg.c_str());

			// Do it
			ForwardToEquipControl(msg, client);
		}

		break;

	default:
		CLog::Log(CLog::INFORMATION, _T("Got invalid BIST_CTRL subtype %lu"), msg.hdr.msgSubType);
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Client disconnected
//
void CMetrics::OnClientClose(_In_ const CMetricsNet* client)
{
	CLockGuard lock(m_critSect);
	m_clientDataMap.erase(client);

	if(client == m_interactiveClient)
	{
//		m_equipControl->RemovePanEntry(client);
		m_interactiveClient = nullptr;
	}
	m_equipControl->OnClientClose(client);
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Client connected
//
void CMetrics::OnClientConnect(_In_ const CMetricsNet* client, _In_ const ADDRINFOT* pPeer)
{
	CLockGuard lock(m_critSect);
	SMetricClientData clientData;

	client->SaveClientInfo(clientData.client, pPeer);
	m_clientDataMap.insert(std::make_pair(client, clientData));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle a DEMOD_CTRL message
//
void CMetrics::OnDemodCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	// Audio control commands do not require being the interactive client.
	if (msg.hdr.msgSubType == SMetricsMsg::GET_AUDIO_PARAMS)
	{
		auto peer = (client != nullptr ? client->GetPeerAddress() : nullptr);
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got GET_AUDIO_PARAMS from %s"), peer->ai_canonname);
		}

		SMetricsMsg resp;
		resp.hdr = msg.hdr;
		resp.hdr.msgSubType = SMetricsMsg::GET_AUDIO_PARAMS_RESP;
		resp.hdr.bodySize = sizeof(SMetricsMsg::SGetAudioParamsResp);

		std::vector<CAudio::SAudioParam> vParams;
		m_equipControl->GetAudioParams(vParams);

		resp.body.getAudioParamsResp.numChannels = 0;
		for (size_t i = 0; i < vParams.size(); ++i)
		{
			if (vParams[i].client != nullptr)
			{
				auto& p = resp.body.getAudioParamsResp.params[resp.body.getAudioParamsResp.numChannels];
				p.channel = i;
				{
					CSharedLockGuard lock(m_critSect);
					ClientDataMap::const_iterator clientData = m_clientDataMap.find(client);
					p.clientInfo = (clientData == m_clientDataMap.end() ? SMetricClientData().client : clientData->second.client);
				}
				p.freq = vParams[i].demodFreq.GetRaw();
				p.bandwidth = vParams[i].params.bw.GetRaw();
				CConfig::ConvertDemod(vParams[i].params.mode, p.detMode);
				p.bfo = vParams[i].params.bfo.GetRaw();
				p.started = vParams[i].started;
				++resp.body.getAudioParamsResp.numChannels;
				if (resp.body.getAudioParamsResp.numChannels == SMetricsMsg::SGetAudioParamsResp::MAX_AUDIO_CHANNELS)
					break;
			}
		}
		client->SendImmediateResp(resp, "GET_AUDIO_PARAMS_RESP");
		return;
	}
	if (msg.hdr.msgSubType == SMetricsMsg::SET_AUDIO_PARAMS || msg.hdr.msgSubType == SMetricsMsg::FREE_AUDIO_CHANNEL)
	{
		ForwardToEquipControl(msg, client);
		return;
	}

	if (client == m_interactiveClient)
	{
		if(msg.body.recvrCntlReqMsg.messageKey == m_interactiveClientKey)
		{
			ForwardToEquipControl(msg, client);
		}
		else
		{
			SendError(client, msg.hdr, ErrorCodes::ERROR_INVALID_KEY);
		}
	}
	else
	{
		if (auto peer = client->GetPeerAddress())
		{
			CLog::Log(CLog::INFORMATION, _T("Got DEMOD_CTRL from %s"), peer->ai_canonname);
		}
		throw ErrorCodes::ERROR_NO_CONNECTION;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle a MEASURE_CTRL message
//
void CMetrics::OnMeasureCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	auto peer = (client != nullptr ? client->GetPeerAddress() : nullptr);

	switch(msg.hdr.msgSubType)
	{
	case SMetricsMsg::BAND_REQUEST:
	case SMetricsMsg::HEADING_REQUEST:
//	case SMetricsMsg::PAN_CAPABILITIES_REQUEST:
	case SMetricsMsg::DWELL_REQUEST:
		ForwardToEquipControl(msg, client);
		break;

	case SMetricsMsg::BAND_REQUEST_NETTED:
	case SMetricsMsg::SERVER_LOCATION_REQUEST:
		{
			// Remember station name
			CLockGuard lock(m_critSect);
			ClientDataMap::iterator clientData = m_clientDataMap.find(client);

			if(clientData == m_clientDataMap.end())
			{
				// Client disconnected
				return;
			}

//			strcpy_s(clientData->second.stationName, msg.body.measureCtrlMsgStationName.stationName);
			strncpy(clientData->second.stationName, msg.body.measureCtrlMsgStationName.stationName, sizeof(clientData->second.stationName));
			lock.Unlock();
			ForwardToEquipControl(msg, client);
		}

		break;

		case SMetricsMsg::DELETE_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got DELETE_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		DeleteResults(msg, client);
		break;

	case SMetricsMsg::MEASUREMENT_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got MEASUREMENT_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		RetrieveMeasurement(msg, client);
		break;

	case SMetricsMsg::MEASUREMENT_IQDATA_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got MEASUREMENT_IQDATA_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgIqDataRequest.measureId, peer->ai_canonname);
		}
		RetrieveIqData(msg, client);
		break;

	case SMetricsMsg::SCHED_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got SCHED_REQUEST from %s"), peer->ai_canonname);
		}

		assert(msg.hdr.cmdVersion != 0);
//		if (msg.hdr.cmdVersion == 0)
//		{
//			// Patch up antenna for old clients
//			msg.body.schedMsgRequest.clientMeasure.ant = m_interactiveClientAnt;
//		}

		ScheduleMeasurement(msg, client);
		break;

	case SMetricsMsg::SERVER_WORKLOAD_REQUEST_MSG:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got SERVER_WORKLOAD_REQUEST_MSG from %s"), peer->ai_canonname);
			}
			SMetricsMsg resp;
			resp.hdr = msg.hdr;
			resp.hdr.msgSubType = SMetricsMsg::SERVER_WORKLOAD_RESPONSE_MSG;
			resp.hdr.bodySize = sizeof(SMetricsMsg::SServerWorkloadResponse);
			m_database->GetWorkload(msg.body.serverWorkloadRequest, resp.body.serverWorkloadResponse);
			resp.body.serverWorkloadResponse.inPriorityMode = m_priorityMode;
			client->SendImmediateResp(resp, "SERVER_WORKLOAD_RESPONSE_MSG");
		}

		break;

	default:
		CLog::Log(CLog::INFORMATION, _T("Got invalid MEASURE_CTRL subtype %lu"), msg.hdr.msgSubType);
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle a MEASUREMENT_GREETING_REQ message
//
void CMetrics::OnMeasurementGreetingReq(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	auto peer = client->GetPeerAddress();
	LPCTSTR peerName(peer == nullptr ? _T("Unknown") : peer->ai_canonname);

	if(msg.hdr.msgSubType != 0)
	{
		CLog::Log(CLog::INFORMATION, _T("Got invalid MEASUREMENT_GREETING_REQ subtype %lu from %s"), msg.hdr.msgSubType, peerName);
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	CLog::Log(CLog::INFORMATION, _T("Got MEASUREMENT_GREETING_REQ from %s"), peerName);
//	CLockGuard lock(m_critSect);

	if(m_interactiveClient == nullptr || client == m_interactiveClient)
	{
		// New or same interactive client
		m_interactiveClient = client;
		m_interactiveClientAnt = SMetricsMsg::ANT1;
		m_interactiveClientData = msg.body.greetingMeasurementReqMsg.echoClient;
		m_interactiveClientKey = rand() * (static_cast<unsigned long>(RAND_MAX) + 1) + rand();;
		SMetricsMsg resp;
		resp.hdr = msg.hdr;
		resp.hdr.msgType = SMetricsMsg::MEASUREMENT_GREETING_RES;
		resp.hdr.bodySize = sizeof(SMetricsMsg::SGreetingMeasurementRespMsg);
		resp.body.greetingMeasurementRespMsg.messageKey = m_interactiveClientKey;
		std::string logMsg = "MEASUREMENT_GREETING_RES";
		client->SendImmediateResp(resp, logMsg.c_str());
	}
	else
	{
		// Interactive mode in use - reject
		SendError(client, msg.hdr, ErrorCodes::ERROR_REJECTED_CONNECT);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle a MEASUREMENT_PAN_REQ message
//
void CMetrics::OnMeasurementPanReq(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	if(client == m_interactiveClient)
	{
		if(msg.body.panMeasurementReqMsg.messageKey == m_interactiveClientKey)
		{
			ForwardToEquipControl(msg, client);
		}
		else
		{
			SendError(client, msg.hdr, ErrorCodes::ERROR_INVALID_KEY);
		}
	}
	else
	{
		auto peer = client->GetPeerAddress();
		LPCTSTR peerName(peer == nullptr ? _T("Unknown") : peer->ai_canonname);
		CLog::Log(CLog::INFORMATION, _T("Got MEASUREMENT_PAN_REQ from %s"), peerName);
		throw ErrorCodes::ERROR_NO_CONNECTION;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle an OCCUPANCY_CTRL message
//
void CMetrics::OnOccupancyCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	auto peer = (client != nullptr ? client->GetPeerAddress() : nullptr);

	switch(msg.hdr.msgSubType)
	{
	case SMetricsMsg::OCCUP_DELETE_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got OCCUP_DELETE_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		DeleteResults(msg, client);
		break;

	case SMetricsMsg::OCCUP_MEASUREMENT_REQUEST:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got OCCUP_MEASUREMENT_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
			}
			SMetricsMsg resp;
			resp.hdr = msg.hdr;
			unsigned long stationId = GetStationId(client);
			m_database->SendResults(client, resp, msg.body.measureCtrlMsgGenericRequest, SEquipCtrlMsg::OCC_FREQUENCY_RESULT,
				SMetricsMsg::OCC_FREQ_MEASURE, stationId, resp.body.measureFreqResultsResponse, "OCC_FREQ_MEASURE");
			bool sent = m_database->SendResults(client, resp, msg.body.measureCtrlMsgGenericRequest, SEquipCtrlMsg::OCC_CHANNEL_RESULT,
				SMetricsMsg::OCC_CHANNEL_MEASURE, stationId, resp.body.measureChanResultsResponse, "OCC_CHANNEL_MEASURE");
			sent = m_database->SendResults(client, resp, msg.body.measureCtrlMsgGenericRequest, SEquipCtrlMsg::OCC_TIMEOFDAY_RESULT,
				SMetricsMsg::OCC_TIMEOFDAY_MEASURE, stationId, resp.body.measureChanResultsResponse, "OCC_TIMEOFDAY_MEASURE") || sent;
			sent = m_database->SendResults(client, resp, msg.body.measureCtrlMsgGenericRequest, SEquipCtrlMsg::OCC_EFLD_TIMEOFDAY_RESULT,
				SMetricsMsg::OCC_EFLD_TIMEOFDAY_MEASURE, stationId, resp.body.measureChanResultsResponse, "OCC_EFLD_TIMEOFDAY_MEASURE") || sent;
			sent = m_database->SendResults(client, resp, msg.body.measureCtrlMsgGenericRequest, SEquipCtrlMsg::EFLD_CHANNEL_RESULT,
				SMetricsMsg::EFLD_CHANNEL_MEASURE, stationId, resp.body.measureChanResultsResponse, "EFLD_CHANNEL_MEASURE") || sent;
			sent = m_database->SendResults(client, resp, msg.body.measureCtrlMsgGenericRequest, SEquipCtrlMsg::MSGLEN_CHANNEL_RESULT,
				SMetricsMsg::MSGLEN_CHANNEL_MEASURE, stationId, resp.body.measureChanResultsResponse, "MSGLEN_CHANNEL_MEASURE") || sent;
			sent = m_database->SendResults(client, resp, msg.body.measureCtrlMsgGenericRequest, SEquipCtrlMsg::MSGLEN_DIST_RESPONSE,
				SMetricsMsg::MSGLEN_DIST_MEASURE, stationId, resp.body.measureMsgLenDistResultsResponse, "MSGLEN_DIST_MEASURE") || sent;
			sent = m_database->SendResults(client, resp, msg.body.measureCtrlMsgGenericRequest, SEquipCtrlMsg::OCC_SPECGRAM_RESULT,
				SMetricsMsg::OCC_SPECGRAM_MEASURE, stationId, resp.body.measureChanResultsResponse, "OCC_SPECGRAM_MEASURE") || sent;

			if(!sent)
			{
				resp.hdr.msgSubType = SMetricsMsg::REQUEST_FAIL_NOT_FOUND;
				resp.hdr.bodySize = sizeof(SMetricsMsg::SMeasureErrorResponse);
				resp.body.measureErrorResponse.clientRequest = msg.body.measureCtrlMsgGenericRequest;
				resp.body.measureErrorResponse.equipMeasurement = 0;
				resp.body.measureErrorResponse.stationId = GetStationId(client);
				resp.body.measureErrorResponse.statusOfRequest = ErrorCodes::TASK_COMPLETED;
				client->SendImmediateResp(resp, "REQUEST_FAIL_NOT_FOUND");
			}
		}

		break;

	case SMetricsMsg::OCCUP_RESUME_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got OCCUP_RESUME_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		ForwardToEquipControl(msg, client, msg.body.measureCtrlMsgGenericRequest.measureId);
		break;

	case SMetricsMsg::OCCUP_STATE_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got OCCUP_STATE_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		RequestState(msg, client);
		break;

	case SMetricsMsg::OCCUP_SUSPEND_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got OCCUP_SUSPEND_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		ForwardToEquipControl(msg, client, msg.body.measureCtrlMsgGenericRequest.measureId);
		break;

	case SMetricsMsg::OCCUP_TERMINATE_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got OCCUP_TERMINATE_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		ForwardToEquipControl(msg, client, msg.body.measureCtrlMsgGenericRequest.measureId);
		break;

	case SMetricsMsg::SCHED_OCCUP_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got SCHED_OCCUP_REQUEST from %s"), peer->ai_canonname);
		}

		assert(msg.hdr.cmdVersion != 0);
//		if(msg.hdr.cmdVersion == 0)
//		{
//			// Patch up antenna
//			msg.body.schedMsgOccupancyRequest.clientMeasure.ant = m_interactiveClientAnt;
//		}

		ScheduleOccupancy(msg, client);
		break;

	default:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("invalid OCCUPANCY_CTRL subtype %u from %s"), msg.hdr.msgSubType, peer->ai_canonname);
		}
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle an OCCUPANCYDF_CTRL message
//
void CMetrics::OnOccupancyDfCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	auto peer = (client != nullptr ? client->GetPeerAddress() : nullptr);

	switch(msg.hdr.msgSubType)
	{
	case SMetricsMsg::SCANDF_DELETE_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got SCANDF_DELETE_REQUEST measId = %u from s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		DeleteResults(msg, client);
		break;

	case SMetricsMsg::SCANDF_MEASUREMENT_REQUEST:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got SCANDF_MEASUREMENT_REQUEST measId = %u from s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
			}
			SMetricsMsg resp;
			resp.hdr = msg.hdr;
			unsigned long stationId = GetStationId(client);
			m_database->SendResults(client, resp, msg.body.measureCtrlMsgGenericRequest, SEquipCtrlMsg::OCC_FREQUENCY_RESULT,
				SMetricsMsg::SCANDF_FREQ_MEASURE, stationId, resp.body.measureFreqResultsResponse, "SCANDF_FREQ_MEASURE");

			if(!m_database->SendResults(client, resp, msg.body.measureCtrlMsgGenericRequest, SEquipCtrlMsg::SCANDF_DATA_RESULT,
				SMetricsMsg::SCANDF_DATA_MEASURE, stationId, resp.body.measureScanDfDataResultsResponse, "SCANDF_DATA_MEASURE"))
			{
				resp.hdr.msgSubType = SMetricsMsg::REQUEST_FAIL_NOT_FOUND;
				resp.hdr.bodySize = sizeof(SMetricsMsg::SMeasureErrorResponse);
				resp.body.measureErrorResponse.clientRequest = msg.body.measureCtrlMsgGenericRequest;
				resp.body.measureErrorResponse.equipMeasurement = 0;
				resp.body.measureErrorResponse.stationId = GetStationId(client);
				resp.body.measureErrorResponse.statusOfRequest = ErrorCodes::TASK_COMPLETED;
				client->SendImmediateResp(resp, "REQUEST_FAIL_NOT_FOUND");
			}
		}

		break;

	case SMetricsMsg::SCANDF_RESUME_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got SCANDF_RESUME_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		ForwardToEquipControl(msg, client, msg.body.measureCtrlMsgGenericRequest.measureId);
		break;

	case SMetricsMsg::SCANDF_STATE_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got SCANDF_STATE_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		RequestState(msg, client);
		break;

	case SMetricsMsg::SCANDF_SUSPEND_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got SCANDF_SUSPEND_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		ForwardToEquipControl(msg, client, msg.body.measureCtrlMsgGenericRequest.measureId);
		break;

	case SMetricsMsg::SCANDF_TERMINATE_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got SCANDF_TERMINATE_REQUEST measId = %u from %s"), msg.body.measureCtrlMsgGenericRequest.measureId, peer->ai_canonname);
		}
		ForwardToEquipControl(msg, client, msg.body.measureCtrlMsgGenericRequest.measureId);
		break;

	case SMetricsMsg::SCHED_SCANDF_REQUEST:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got SCHED_SCANDF_REQUEST from %s"), peer->ai_canonname);
		}
		ScheduleOccupancyDf(msg, client);
		break;

	default:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("invalid OCCUPANCYDF_CTRL subtype %u from %s"), msg.hdr.msgSubType, peer->ai_canonname);
		}
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle a SYSTEM_STATE_CTRL message
//
void CMetrics::OnSystemStateCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	SMetricsMsg resp;
	resp.hdr = msg.hdr;
	resp.hdr.msgSubType = SMetricsMsg::SET_SYSTEM_STATE_RESPONSE;
	resp.hdr.bodySize = sizeof(SMetricsMsg::SSystemStateSetCtrlResp);

	switch(msg.hdr.msgSubType)
	{
	case SMetricsMsg::SET_SYSTEM_STATE:
		if(auto peer = client->GetPeerAddress())
		{
			CLog::Log(CLog::INFORMATION, _T("Got SET_SYSTEM_STATE %u from %s"), msg.body.systemStateSetCtrlCmd.state, peer->ai_canonname);
		}
		switch(msg.body.systemStateSetCtrlCmd.state)
		{
		case SMetricsMsg::NORMAL:
#if 0
			if(m_priorityMode)
			{
				if(m_suspendedTasks.size() > 0)
				{
					m_equipControl->Resume(m_suspendedTasks);
				}

				m_priorityMode = false;
				resp.body.systemStateSetCtrlResp.status = 1;

				// Update schedule
				SEquipCtrlMsg ecMsg;
				ecMsg.hdr.msgType = SEquipCtrlMsg::METRICS_CTRL;
				ecMsg.hdr.msgSubType = SEquipCtrlMsg::CAL_SCHED;
				ecMsg.hdr.bodySize = 0;
				m_database->AddMsg(ecMsg);
			}
			else
#endif
			{
				// Not suspended
				resp.body.systemStateSetCtrlResp.status = 0;
			}

			resp.body.systemStateSetCtrlResp.state = SMetricsMsg::NORMAL;
			break;

		case SMetricsMsg::SUSPENDED:
#if 0
			if(!m_priorityMode)
			{
				m_priorityMode = true;
				m_suspendedTasks = CSuspendableTask::SuspendAll(true);
#endif
				resp.body.systemStateSetCtrlResp.status = 1;
#if 0
			}
			else
			{
				// Already suspended
				resp.body.systemStateSetCtrlResp.status = 0;
			}
#endif
			resp.body.systemStateSetCtrlResp.state = SMetricsMsg::SUSPENDED;
			break;

		default:
			throw ErrorCodes::REQUEST_UNKNOWN;
		}

		break;

	default:
		CLog::Log(CLog::INFORMATION, _T("Got invalid SYSTEM_STATE_CTRL subtype %lu"), msg.hdr.msgSubType);
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	client->SendImmediateResp(resp, "SET_SYSTEM_STATE_RESPONSE");

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Request task state
//
void CMetrics::RequestState(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	SMetricsMsg resp;
	resp.hdr = msg.hdr;
	std::string log;

	switch(msg.hdr.msgType)
	{
	case SMetricsMsg::AUTOVIOLATE_CTRL:
		resp.hdr.msgSubType = SMetricsMsg::AVD_STATE_RESPONSE;
		log = _T("AVD_STATE_RESPONSE state=");
		break;

	case SMetricsMsg::OCCUPANCY_CTRL:
		resp.hdr.msgSubType = SMetricsMsg::OCCUP_STATE_RESPONSE;
		log = _T("OCCUP_STATE_RESPONSE state=");
		break;

	case SMetricsMsg::OCCUPANCYDF_CTRL:
		resp.hdr.msgSubType = SMetricsMsg::SCANDF_STATE_RESPONSE;
		log = _T("SCANDF_STATE_RESPONSE state=");
		break;

	default:
		THROW_LOGIC_ERROR();
	}

	resp.hdr.bodySize = sizeof(SMetricsMsg::SMeasureCtrlMsgStateResponse);
	resp.body.measureCtrlMsgStateResponse.measureId = msg.body.measureCtrlMsgGenericRequest.measureId;
	unsigned long taskId;
	unsigned long key;
	DATE startTime;
	resp.body.measureCtrlMsgStateResponse.statusOfRequest =
		m_database->RetrieveSchedule(msg.body.measureCtrlMsgGenericRequest.measureId, taskId, key,
		resp.body.measureCtrlMsgStateResponse.state, startTime, resp.body.measureCtrlMsgStateResponse.completionTime,
		resp.body.measureCtrlMsgStateResponse.lastUpdate, nullptr);
	resp.body.measureCtrlMsgStateResponse.stationId = GetStationId(client);
	std::string logMsg = log + std::to_string(resp.body.measureCtrlMsgStateResponse.state);
	client->SendImmediateResp(resp, logMsg.c_str());

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Request IQ data
//
void CMetrics::RetrieveIqData(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	SMetricsMsg resp;
	resp.hdr = msg.hdr;
	unsigned long stationId = GetStationId(client);
	bool sent = m_database->SendIqDataResults(client, resp, msg.body.measureCtrlMsgIqDataRequest,
		stationId, resp.body.measureCtrlMsgResultsIqDataResponse);

	if (!sent)
	{
		resp.hdr.msgSubType = SMetricsMsg::REQUEST_FAIL_NOT_FOUND;
		resp.hdr.bodySize = sizeof(SMetricsMsg::SMeasureErrorResponse);
		resp.body.measureErrorResponse.clientRequest = msg.body.measureCtrlMsgGenericRequest;
		resp.body.measureErrorResponse.equipMeasurement = 0;
		resp.body.measureErrorResponse.stationId = GetStationId(client);
		resp.body.measureErrorResponse.statusOfRequest = ErrorCodes::TASK_COMPLETED;
		client->SendImmediateResp(resp, "REQUEST_FAIL_NOT_FOUND");
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Request a measurement
//
void CMetrics::RetrieveMeasurement(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	SMetricsMsg resp;
	resp.hdr = msg.hdr;
	resp.hdr.msgSubType = SMetricsMsg::MEASUREMENT_RESPONSE;
	resp.hdr.bodySize = sizeof(SMetricsMsg::SMeasureCtrlMsgResultsResponse);
	resp.body.measureCtrlMsgResultsResponse.clientRequest = msg.body.measureCtrlMsgGenericRequest;
	resp.body.measureCtrlMsgResultsResponse.statusOfRequest =
		m_database->RetrieveMeasurement(msg.body.measureCtrlMsgGenericRequest.measureId,
		resp.body.measureCtrlMsgResultsResponse.equipMeasurement);
	resp.body.measureCtrlMsgResultsResponse.stationId = GetStationId(client);
	std::string logMsg("MEASUREMENT_RESPONSE status = ");
	logMsg += std::to_string(resp.body.measureCtrlMsgResultsResponse.statusOfRequest);
	client->SendImmediateResp(resp, logMsg.c_str());

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Schedule an AVD task
//
void CMetrics::ScheduleAvd(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	const SMetricsMsg::SSchedMsgAutoViolateRequest& cmd = msg.body.schedMsgAutoViolateRequest;
	DATE now = Utility::CurrentTimeAsDATE();
//	TciGps::GetDATE(now);
	DATE startTime = cmd.clientOptions.startMeasureDateTime;
	DATE stopTime = startTime + cmd.clientMeasure.measurementTime / double(Units::SECONDS_PER_DAY);;
	SMetricsMsg resp;
	resp.hdr = msg.hdr;
	resp.hdr.msgSubType = SMetricsMsg::SCHED_AVD_RESPONSE;
	resp.hdr.bodySize = sizeof(SMetricsMsg::SSchedMsgResponse);
	resp.body.schedMsgResponse.clientTaskId = cmd.clientOptions.clientTaskId;
	resp.body.schedMsgResponse.state = SMetricsMsg::SSchedMsgResponse::SCHED_FINI;
	resp.body.schedMsgResponse.stationId = GetStationId(client);

	try
	{
		if (startTime <= now)
		{
			// Do it now
			startTime = now;
			stopTime = startTime + cmd.clientMeasure.measurementTime / double(Units::SECONDS_PER_DAY);;
			resp.body.schedMsgResponse.statusOfRequest = m_database->SaveSchedule(resp.body.schedMsgResponse.measureId, 0, 0,
				SMetricsMsg::SMeasureCtrlMsgStateResponse::UNKNOWN, startTime, stopTime, now, msg);
			ForwardToEquipControl(msg, client, resp.body.schedMsgResponse.measureId);
			CLog::Log(CLog::INFORMATION, "Immediate AVD %u started", resp.body.schedMsgResponse.measureId);
		}
		else
		{
			// Validate
			CAvdTask::Validate(cmd.clientMeasure);

			// Save in database
			resp.body.schedMsgResponse.statusOfRequest = m_database->SaveSchedule(resp.body.schedMsgResponse.measureId, 0, 0,
				SMetricsMsg::SMeasureCtrlMsgStateResponse::IDLE, startTime, stopTime, now, msg);
		}

		resp.body.schedMsgResponse.startMeasure = startTime;
		resp.body.schedMsgResponse.endMeasure = stopTime;
	}
	catch(ErrorCodes::EErrorCode error)
	{
		m_database->DeleteTask(resp.body.schedMsgResponse.measureId);
		resp.body.schedMsgResponse.measureId = 0;
		resp.body.schedMsgResponse.startMeasure = SMetricsMsg::MAX_DATE;
		resp.body.schedMsgResponse.endMeasure = SMetricsMsg::MAX_DATE;
		resp.body.schedMsgResponse.state = SMetricsMsg::SSchedMsgResponse::INVALID_REQUEST;
		resp.body.schedMsgResponse.statusOfRequest = error;
	}

	std::string logMsg("SCHED_AVD_RESPONSE measureId=");
	logMsg += std::to_string(resp.body.schedMsgResponse.measureId) + " status=" + std::to_string(resp.body.schedMsgResponse.statusOfRequest);
	client->SendImmediateResp(resp, logMsg.c_str());

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Schedule a measurement
//
void CMetrics::ScheduleMeasurement(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	const SMetricsMsg::SSchedMsgRequest& cmd = msg.body.schedMsgRequest;
	DATE now = Utility::CurrentTimeAsDATE();
//	TciGps::GetDATE(now);

	SMetricsMsg resp;
	resp.hdr = msg.hdr;
	resp.hdr.msgSubType = SMetricsMsg::SCHED_RESPONSE;
	resp.hdr.bodySize = sizeof(SMetricsMsg::SSchedMsgResponse);
	resp.body.schedMsgResponse.clientTaskId = cmd.clientOptions.clientTaskId;
	resp.body.schedMsgResponse.state = SMetricsMsg::SSchedMsgResponse::SCHED_FINI;
	resp.body.schedMsgResponse.stationId = GetStationId(client);

	try
	{
		// Figure out real duration
		SEquipCtrlMsg validate = CMeasurementTask::Validate(cmd.clientMeasure);
		double duration = validate.body.validateMeasurementResp.totalTime / double(Units::MILLISECONDS_PER_DAY);

		if (cmd.clientOptions.calRequest == SMetricsMsg::SSchedMsgOptions::IMMEDIATE)
		{
			// Do it now
			DATE startTime = now;
			DATE stopTime = startTime + duration;
			resp.body.schedMsgResponse.statusOfRequest = m_database->SaveSchedule(resp.body.schedMsgResponse.measureId, 0, 0,
				SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING, startTime, stopTime, now, msg);
			ForwardToEquipControl(msg, client, resp.body.schedMsgResponse.measureId);

			// Send schedule response
			resp.body.schedMsgResponse.startMeasure = startTime;
			resp.body.schedMsgResponse.endMeasure = stopTime;
			client->SendImmediateResp(resp,
				(std::string("SCHED_RESPONSE measureId=") + std::to_string(resp.body.schedMsgResponse.measureId)).c_str());
			CLog::Log(CLog::INFORMATION, "Metrics task %u started", resp.body.schedMsgResponse.measureId);
		}
		else
		{
			if (cmd.clientMeasure.rcvrAtten != SMetricsMsg::SGetMeasCmd::AGC)
			{
				// MGC only valid in immediate mode
				throw ErrorCodes::INVALIDRFATTEN;
			}

			// Save in database
			resp.body.schedMsgResponse.statusOfRequest = ErrorCodes::TASK_COMPLETED;

			for(unsigned int repeat = 0;
				resp.body.schedMsgResponse.statusOfRequest == ErrorCodes::TASK_COMPLETED && repeat < cmd.clientOptions.measureQuantity;
				++repeat)
			{
				DATE startTime = cmd.clientOptions.startMeasureDateTime +
					repeat * cmd.clientOptions.measureQuantityDelta / double(Units::MILLISECONDS_PER_DAY);
				DATE stopTime = startTime + duration;
				resp.body.schedMsgResponse.startMeasure = startTime;
				resp.body.schedMsgResponse.endMeasure = stopTime;

				if (cmd.clientMeasure.iqCmd.outputType != SMetricsMsg::NONE && cmd.clientMeasure.iqCmd.tdoa)
				{
					// Start TDOA measurement early so it can preempt other running tasks
					startTime -= TDOA_EARLY_START_INTERVAL;
				}

				resp.body.schedMsgResponse.statusOfRequest = m_database->SaveSchedule(resp.body.schedMsgResponse.measureId, 0, 0,
					SMetricsMsg::SMeasureCtrlMsgStateResponse::IDLE, startTime, stopTime, now, msg);

				// Send schedule response
				client->SendImmediateResp(resp,
					(std::string("SCHED_RESPONSE measureId=") + std::to_string(resp.body.schedMsgResponse.measureId)).c_str());
			}
		}
	}
	catch(ErrorCodes::EErrorCode error)
	{
		// Send error response
		resp.body.schedMsgResponse.measureId = 0;
		resp.body.schedMsgResponse.startMeasure = SMetricsMsg::MAX_DATE;
		resp.body.schedMsgResponse.endMeasure = SMetricsMsg::MAX_DATE;
		resp.body.schedMsgResponse.state = SMetricsMsg::SSchedMsgResponse::INVALID_REQUEST;
		resp.body.schedMsgResponse.statusOfRequest = error;
		client->SendImmediateResp(resp, (std::string("SCHED_RESPONSE status=") + std::to_string(error)).c_str());
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Schedule an occupancy task
//
void CMetrics::ScheduleOccupancy(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	const SMetricsMsg::SSchedMsgOccupancyRequest& cmd = msg.body.schedMsgOccupancyRequest;
	DATE now = Utility::CurrentTimeAsDATE();
//	TciGps::GetDATE(now);
	DATE startTime = cmd.clientOptions.startMeasureDateTime;
	DATE stopTime = SMetricsMsg::MAX_DATE;
	SMetricsMsg resp;
	resp.hdr = msg.hdr;
	resp.hdr.msgSubType = SMetricsMsg::SCHED_OCCUP_RESPONSE;
	resp.hdr.bodySize = sizeof(SMetricsMsg::SSchedMsgResponse);
	resp.body.schedMsgResponse.clientTaskId = cmd.clientOptions.clientTaskId;
	resp.body.schedMsgResponse.state = SMetricsMsg::SSchedMsgResponse::SCHED_FINI;
	resp.body.schedMsgResponse.stationId = GetStationId(client);

	try
	{
		if (startTime <= now)
		{
			// Do it now
			startTime = now;

			if (cmd.clientMeasure.durationMethod == SMetricsMsg::TIME)
			{
				stopTime = startTime + cmd.clientMeasure.measurementTime / double(Units::SECONDS_PER_DAY);
			}

			resp.body.schedMsgResponse.statusOfRequest = m_database->SaveSchedule(resp.body.schedMsgResponse.measureId, 0, 0,
				SMetricsMsg::SMeasureCtrlMsgStateResponse::UNKNOWN, startTime, stopTime, now, msg);
			ForwardToEquipControl(msg, client, resp.body.schedMsgResponse.measureId);
			CLog::Log(CLog::INFORMATION, "Immediate occupancy %u started", resp.body.schedMsgResponse.measureId);
		}
		else
		{
			// Validate
			COccupancyTask::Validate(cmd.clientMeasure);

			// Save in database
			if (cmd.clientMeasure.durationMethod == SMetricsMsg::TIME)
			{
				stopTime = startTime + cmd.clientMeasure.measurementTime / double(Units::SECONDS_PER_DAY);
			}

			resp.body.schedMsgResponse.statusOfRequest = m_database->SaveSchedule(resp.body.schedMsgResponse.measureId, 0, 0,
				SMetricsMsg::SMeasureCtrlMsgStateResponse::IDLE, startTime, stopTime, now, msg);
		}

		resp.body.schedMsgResponse.startMeasure = startTime;
		resp.body.schedMsgResponse.endMeasure = stopTime;
	}
	catch(ErrorCodes::EErrorCode error)
	{
		m_database->DeleteTask(resp.body.schedMsgResponse.measureId);
		resp.body.schedMsgResponse.measureId = 0;
		resp.body.schedMsgResponse.startMeasure = SMetricsMsg::MAX_DATE;
		resp.body.schedMsgResponse.endMeasure = SMetricsMsg::MAX_DATE;
		resp.body.schedMsgResponse.state = SMetricsMsg::SSchedMsgResponse::INVALID_REQUEST;
		resp.body.schedMsgResponse.statusOfRequest = error;
	}

	std::string logMsg("SCHED_OCCUP_RESPONSE measureId=");
	logMsg += std::to_string(resp.body.schedMsgResponse.measureId) + " status=" + std::to_string(resp.body.schedMsgResponse.statusOfRequest);
	client->SendImmediateResp(resp, logMsg.c_str());

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Schedule a scan DF task
//
void CMetrics::ScheduleOccupancyDf(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	const SMetricsMsg::SSchedMsgScanDfRequest& cmd = msg.body.schedMsgScanDfRequest;
	DATE now = Utility::CurrentTimeAsDATE();
	DATE startTime = cmd.clientOptions.startMeasureDateTime;
	DATE stopTime = startTime + cmd.clientMeasure.measurementTime / double(Units::SECONDS_PER_DAY);
	SMetricsMsg resp;
	resp.hdr = msg.hdr;
	resp.hdr.msgSubType = SMetricsMsg::SCHED_SCANDF_RESPONSE;
	resp.hdr.bodySize = sizeof(SMetricsMsg::SSchedMsgScanDfResponse);
	resp.body.schedMsgScanDfResponse.schedResponse.clientTaskId = cmd.clientOptions.clientTaskId;
	resp.body.schedMsgScanDfResponse.schedResponse.state = SMetricsMsg::SSchedMsgResponse::SCHED_FINI;
	resp.body.schedMsgScanDfResponse.schedResponse.stationId = GetStationId(client);
	resp.body.schedMsgScanDfResponse.clientMeasure = msg.body.schedMsgScanDfRequest.clientMeasure;
	resp.body.schedMsgScanDfResponse.clientId = msg.body.schedMsgScanDfRequest.clientOptions.clientId;
	memcpy(resp.body.schedMsgScanDfResponse.clientName, msg.body.schedMsgScanDfRequest.clientOptions.clientName,
		sizeof(resp.body.schedMsgScanDfResponse.clientName));
	memcpy(resp.body.schedMsgScanDfResponse.ipAddr, msg.body.schedMsgScanDfRequest.clientOptions.ipAddr,
		sizeof(resp.body.schedMsgScanDfResponse.ipAddr));

	try
	{
		if(startTime <= now)
		{
			// Do it now
			startTime = now;
			stopTime = startTime + cmd.clientMeasure.measurementTime / double(Units::SECONDS_PER_DAY);
			resp.body.schedMsgScanDfResponse.schedResponse.statusOfRequest = m_database->SaveSchedule(
				resp.body.schedMsgScanDfResponse.schedResponse.measureId, 0, 0, SMetricsMsg::SMeasureCtrlMsgStateResponse::UNKNOWN,
				startTime, stopTime, now, msg);
			ForwardToEquipControl(msg, client, resp.body.schedMsgScanDfResponse.schedResponse.measureId);
			CLog::Log(CLog::INFORMATION, "Immediate DF Scan %u started", resp.body.schedMsgScanDfResponse.schedResponse.measureId);
		}
		else
		{
			// Validate
			CScanDfTask::Validate(cmd.clientMeasure);

			// Save in database
			resp.body.schedMsgScanDfResponse.schedResponse.statusOfRequest = m_database->SaveSchedule(
				resp.body.schedMsgScanDfResponse.schedResponse.measureId, 0, 0, SMetricsMsg::SMeasureCtrlMsgStateResponse::IDLE,
				startTime, stopTime, now, msg);
		}

		resp.body.schedMsgScanDfResponse.schedResponse.startMeasure = startTime;
		resp.body.schedMsgScanDfResponse.schedResponse.endMeasure = stopTime;
	}
	catch(ErrorCodes::EErrorCode error)
	{
		m_database->DeleteTask(resp.body.schedMsgScanDfResponse.schedResponse.measureId);
		resp.body.schedMsgScanDfResponse.schedResponse.measureId = 0;
		resp.body.schedMsgScanDfResponse.schedResponse.startMeasure = SMetricsMsg::MAX_DATE;
		resp.body.schedMsgScanDfResponse.schedResponse.endMeasure = SMetricsMsg::MAX_DATE;
		resp.body.schedMsgScanDfResponse.schedResponse.state = SMetricsMsg::SSchedMsgResponse::INVALID_REQUEST;
		resp.body.schedMsgScanDfResponse.schedResponse.statusOfRequest = error;
	}

	std::string logMsg("SCHED_SCANDF_RESPONSE measureId=");
	logMsg += std::to_string(resp.body.schedMsgScanDfResponse.schedResponse.measureId) + " status=" + std::to_string(resp.body.schedMsgScanDfResponse.schedResponse.statusOfRequest);
	client->SendImmediateResp(resp, logMsg.c_str());

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send an error message
//
void CMetrics::SendError(_In_ CMetricsNet* client, _In_ const SMetricsMsg::SHdr& hdr, ErrorCodes::EErrorCode error) const
{
	SMetricsMsg msg;
	msg.hdr = hdr;
	msg.hdr.msgType = SMetricsMsg::MEASUREMENT_ERROR_REPORTA;
	msg.hdr.msgSubType = 0;
	msg.hdr.bodySize = sizeof(SMetricsMsg::SMeasurementErrorReportA);
	msg.body.measurementErrorReportA.code = error;
	msg.body.measurementErrorReportA.clientData = m_interactiveClientData;
	client->SendImmediateResp(msg);

	if(CLog::GetLogLevel() == CLog::VERBOSE)
	{
		CLog::Log(CLog::INFORMATION, "Metrics: sent MEASUREMENT_ERROR_REPORTA %ld", error);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Construct and send SMetricsMsg::SAntNameChangeResp message
//
void CMetrics::SendAntNameChangeResp(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	SMetricsMsg resp;
	resp.hdr = msg.hdr;
	resp.hdr.msgSubType = SMetricsMsg::METRIC_ANT_NAME_CHANGE_RESP;
	resp.hdr.bodySize = offsetof(SMetricsMsg::SAntNameChangeResp, ant2Name);

	resp.body.antNameChangeResp.numNames = 0;

	// Send the constructed response.
	client->SendImmediateResp(resp, "METRIC_ANT_NAME_CHANGE_RESP");

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Construct and send SMetricsMsg::METRIC_CONN_INFO_RESP message
//
void CMetrics::SendStatusCtrlConnResp(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client)
{
	CLockGuard lock(m_critSect);
//	size_t numClient = m_clientDataMap.size();
//	size_t msgSize = offsetof(SMetricsMsg, body.connResp.clientInfo[0]) +
//		sizeof(SMetricsMsg::SClientInfo) * numClient;
	auto bodySize = offsetof(SMetricsMsg::SGetConnInfoResp, clientInfo) + m_clientDataMap.size() * sizeof(SMetricsMsg::SClientInfo);
	size_t msgSize = offsetof(SMetricsMsg, body) + bodySize;

	if (msgSize > m_connBufLen)
	{
		ASSERT(!(m_connBufLen == 0 && m_pConnRespMsg != nullptr));

		if (m_pConnRespMsg != nullptr)
		{
			free(m_pConnRespMsg);
		}

		TRACE("SendStatusCtrlConnResp malloc msgSize = %u\n", msgSize);
		m_pConnRespMsg = (SMetricsMsg *)malloc(msgSize);

		if (m_pConnRespMsg == nullptr)
		{
        	ASSERT(false);
			throw std::bad_alloc();
		}
		else
		{
			m_connBufLen = msgSize;

			//Setup msg hdr.
			m_pConnRespMsg->hdr = msg.hdr;
			m_pConnRespMsg->hdr.msgSubType = SMetricsMsg::METRIC_CONN_INFO_RESP;
		}
	}

	if (m_connBufLen >= msgSize && m_pConnRespMsg != nullptr)
	{
		size_t i = 0;

		// Fill-in SClientInfo.
		for (ClientDataMap::const_iterator clientData = m_clientDataMap.begin();
			clientData != m_clientDataMap.end(); ++clientData)
		{
			bool matched = false;

			if (i > 0)
			{
				for (size_t j = 0; j < i && !matched; ++j)
				{
					size_t count = (clientData->second.client.ipType == SMetricsMsg::IP_V6 ? 16 : 4);
					if (memcmp(m_pConnRespMsg->body.connResp.clientInfo[j].ipAddr, clientData->second.client.ipAddr, count) == 0)
					{
						matched = true;
					}
				}
			}
			if (!matched)
			{
				m_pConnRespMsg->body.connResp.clientInfo[i++] = clientData->second.client;
			}
		}
		m_pConnRespMsg->body.connResp.numClients = i;

        //Unlock since all of SClientInfo have been copied.
		lock.Unlock();

//		m_pConnRespMsg->hdr.bodySize = sizeof(SMetricsMsg::SGetConnInfoResp) +
//                                     sizeof(SMetricsMsg::SClientInfo) * (i - 1);
		m_pConnRespMsg->hdr.bodySize = offsetof(SMetricsMsg::SGetConnInfoResp, clientInfo) + i * sizeof(SMetricsMsg::SClientInfo);

		// Send the constructed response.
		std::string logMsg("METRIC_CONN_INFO_RESP number of Clients = ");
		logMsg += std::to_string(i);
		client->SendImmediateResp(*m_pConnRespMsg, logMsg.c_str());
	}
	else
	{
		ASSERT(false);
		THROW_LOGIC_ERROR();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Translate an EquipControl message to the equivalent Metrics message and send it
//
bool CMetrics::TranslateAndSend(_In_ CMetricsNet* client, _In_ const SEquipCtrlMsg& msg) const
{
	std::string log;
	SMetricsMsg metricsMsg;
	metricsMsg.hdr = msg.hdr;

	switch(msg.hdr.msgType)
	{
	case SEquipCtrlMsg::ANT_CTRL:
		switch(msg.hdr.msgSubType)
		{
		case SEquipCtrlMsg::GET_ANT_RESPONSE:
			metricsMsg.body.antGetSetCtrlResp = msg.body.antGetSetCtrlResp;
			log = _T("GET_ANT_RESPONSE");
			break;

		case SEquipCtrlMsg::SET_ANT_RESPONSE:
			metricsMsg.body.antGetSetCtrlResp = msg.body.antGetSetCtrlResp;
			log = _T("SET_ANT_RESPONSE");
			break;

		default:
			return false;
		}

		break;

	case SEquipCtrlMsg::AUTOVIOLATE_CTRL:
	case SEquipCtrlMsg::OCCUPANCY_CTRL:
	case SEquipCtrlMsg::OCCUPANCYDF_CTRL:
		switch(msg.hdr.msgSubType)
		{
		case SEquipCtrlMsg::OCCUPANCY_SOLICIT_STATE_RESP:
		case SEquipCtrlMsg::OCCUPANCY_STATE_RESP:
			{
				SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;

				switch(msg.body.stateResp.state)
				{
				case SEquipCtrlMsg::SStateResp::IDLE:
					state = SMetricsMsg::SMeasureCtrlMsgStateResponse::IDLE;
					break;

				case SEquipCtrlMsg::SStateResp::RUNNING:
					state = SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING;
					break;

				case SEquipCtrlMsg::SStateResp::SUSPENDED:
					state = SMetricsMsg::SMeasureCtrlMsgStateResponse::SUSPENDED;
					break;

				default:
					THROW_LOGIC_ERROR();
				}

				DATE now = Utility::CurrentTimeAsDATE();
//				TciGps::GetDATE(now);

				switch(msg.hdr.msgType)
				{
				case SEquipCtrlMsg::AUTOVIOLATE_CTRL:
					metricsMsg.hdr.msgSubType = SMetricsMsg::AVD_STATE_RESPONSE;
					log = _T("AVD_STATE_RESPONSE");
					break;

				case SEquipCtrlMsg::OCCUPANCY_CTRL:
					metricsMsg.hdr.msgSubType = SMetricsMsg::OCCUP_STATE_RESPONSE;
					log = _T("OCCUP_STATE_RESPONSE");
					break;

				case SEquipCtrlMsg::OCCUPANCYDF_CTRL:
					metricsMsg.hdr.msgSubType = SMetricsMsg::SCANDF_STATE_RESPONSE;
					log = _T("SCANDF_STATE_RESPONSE");
					break;

				default:
					THROW_LOGIC_ERROR();
				}

				metricsMsg.hdr.bodySize = sizeof(SMetricsMsg::SMeasureCtrlMsgStateResponse);
				metricsMsg.body.measureCtrlMsgStateResponse.measureId = msg.hdr.destAddr;
				metricsMsg.body.measureCtrlMsgStateResponse.state = state;
				metricsMsg.body.measureCtrlMsgStateResponse.completionTime = msg.body.stateResp.completionTime;
				metricsMsg.body.measureCtrlMsgStateResponse.lastUpdate = now;
				metricsMsg.body.measureCtrlMsgStateResponse.statusOfRequest = ErrorCodes::TASK_COMPLETED;
				metricsMsg.body.measureCtrlMsgStateResponse.stationId = GetStationId(client);

				// Also send to database
				m_database->AddMsg(msg);
			}

			break;

		default:
			return false;
		}
		break;

	case SEquipCtrlMsg::BIST_CTRL:
		switch(msg.hdr.msgSubType)
		{
		case SEquipCtrlMsg::GET_BIST_RESPONSE:
		case SEquipCtrlMsg::GET_BIST_RESULT_RESPONSE:
		case SEquipCtrlMsg::GET_DIAGNOSTICS_RESPONSE:
			// Send the new style responses
			metricsMsg.hdr.msgSubType = SMetricsMsg::BIST_RESULT_RESPONSE;
			metricsMsg.hdr.bodySize = offsetof(SMetricsMsg::SMeasureCtrlMsgBistResponse, bistResponse.text) +
				msg.body.getBistResp.textLen * sizeof(char16_t);
			metricsMsg.body.measureCtrlMsgBistResponse.statusOfRequest = ErrorCodes::TASK_COMPLETED;
			metricsMsg.body.measureCtrlMsgBistResponse.clientId = GetClientId(client);
			metricsMsg.body.measureCtrlMsgBistResponse.stationId = GetStationId(client);
			ASSERT(metricsMsg.hdr.bodySize <= sizeof(metricsMsg.body)); // Need to implement variable-size
			memcpy(&metricsMsg.body.measureCtrlMsgBistResponse.bistResponse, &msg.body.getBistResp,
				offsetof(SMetricsMsg::SGetBistResp, text[msg.body.getBistResp.textLen]));
			// Only log the last message
			if (msg.body.getBistResp.last)
			{
				log = _T("BIST_RESULT_RESPONSE (last)");
			}
			break;

		default:
			return false;
		}

		break;

	case SEquipCtrlMsg::DEMOD_CTRL:
		switch(msg.hdr.msgSubType)
		{
		case SEquipCtrlMsg::SET_RCVR_RESP:
			metricsMsg.body.genericResp = msg.body.genericResp;
			log = _T("SET_RCVR_RESP");
			break;

		case SEquipCtrlMsg::SET_PAN_PARA_RESP:
			metricsMsg.body.genericResp = msg.body.genericResp;
			log = _T("SET_PAN_PARA_RESP");
			break;

		case SEquipCtrlMsg::SET_AUDIO_PARAMS_RESP:
			metricsMsg.body.audioParamsResp = msg.body.audioParamsResp;
			log = _T("SET_AUDIO_PARAMS_RESP");
			break;

		case SEquipCtrlMsg::FREE_AUDIO_CHANNEL_RESP:
			metricsMsg.body.genericResp = msg.body.genericResp;
			log = _T("FREE_AUDIO_CHANNEL_RESP");
			break;

		default:
			return false;
		}
		break;

	case SEquipCtrlMsg::METRICS_CTRL:
		metricsMsg.hdr.msgType = SMetricsMsg::MEASURE_CTRL;

		switch(msg.hdr.msgSubType)
		{
		case SEquipCtrlMsg::GET_BAND_RESPONSE:
			if (IsNetted(client))
			{
				metricsMsg.hdr.msgSubType = SMetricsMsg::BAND_RESPONSE_NETTED;
				metricsMsg.hdr.bodySize = sizeof(SMetricsMsg::SGetBandRespNetted);
				metricsMsg.body.getBandRespNetted.bandResp = msg.body.getBandResp;
				GetStationName(client, metricsMsg.body.getBandRespNetted.stationName);
				log = _T("BAND_RESPONSE_NETTED");
			}
			else
			{
				metricsMsg.hdr.msgSubType = SMetricsMsg::BAND_RESPONSE;
				metricsMsg.body.getBandResp = msg.body.getBandResp;
//				log = _T("BAND_RESPONSE");
			}

			break;

		case SEquipCtrlMsg::GET_HEADING_RESPONSE:
			//SEquipCtrlMsg::GET_HEADING_RESPONSE version 0 is translated to
			// SMetricsMsg::HEADING_RESPONSE version 1.
			if (msg.hdr.respVersion == 0)
			{
				metricsMsg.hdr.cmdVersion = 1;
				metricsMsg.hdr.respVersion = 1;
			}

			metricsMsg.hdr.msgSubType = SMetricsMsg::HEADING_RESPONSE;
			metricsMsg.body.getHeadingResp = msg.body.getHeadingResp;
			log = _T("HEADING_RESPONSE");

			break;

		case SEquipCtrlMsg::GET_DWELL_RESPONSE:
			metricsMsg.hdr.msgSubType = SMetricsMsg::DWELL_RESPONSE;
			metricsMsg.hdr.bodySize = sizeof(SMetricsMsg::SMeasureCtrlDwellResponse);
			metricsMsg.body.measureCtrlDwellResponse.dwellResponse = msg.body.getDwellResp;
			metricsMsg.body.measureCtrlDwellResponse.clientId = GetClientId(client);
			metricsMsg.body.measureCtrlDwellResponse.stationId = GetStationId(client);
			log = _T("DWELL_RESPONSE");
			break;

		case SEquipCtrlMsg::GET_GPS_RESPONSE:
			metricsMsg.hdr.msgSubType = SMetricsMsg::SERVER_LOCATION_RESPONSE;
			metricsMsg.hdr.bodySize = sizeof(SMetricsMsg::SServerGpsResponse);

			if(msg.body.getGpsResp.gpsResponse.status.noGps)
			{
				metricsMsg.body.serverGpsResponse.status = ErrorCodes::HARDWARENOTPRESENT;
			}
			else if(msg.body.getGpsResp.gpsResponse.status.antenna > 0)
			{
				metricsMsg.body.serverGpsResponse.status = ErrorCodes::HARDWAREDOWN;
			}
			else if(msg.body.getGpsResp.gpsResponse.status.satLock || msg.body.getGpsResp.gpsResponse.status.accuracy > 2 ||
				msg.body.getGpsResp.gpsResponse.status.tracking)
			{
				metricsMsg.body.serverGpsResponse.status = ErrorCodes::UNCALIBRATED;
			}
			else
			{
				metricsMsg.body.serverGpsResponse.status = ErrorCodes::SUCCESS;
			}

			metricsMsg.body.serverGpsResponse.dateTime = msg.body.getGpsResp.gpsResponse.dateTime;
			metricsMsg.body.serverGpsResponse.latitude = msg.body.getGpsResp.gpsResponse.latitude;
			metricsMsg.body.serverGpsResponse.longitude = msg.body.getGpsResp.gpsResponse.longitude;
//			strcpy_s(metricsMsg.body.serverGpsResponse.stationName, m_config->GetServerName().c_str());
			strncpy(metricsMsg.body.serverGpsResponse.stationName, m_config->GetServerName().c_str(), sizeof(metricsMsg.body.serverGpsResponse.stationName));
//			log = _T("SERVER_LOCATION_RESPONSE");	// Too many
			break;

//		case SEquipCtrlMsg::GET_PAN_CAPABILITIES_RESPONSE:
//			metricsMsg.hdr.msgSubType = SMetricsMsg::PAN_CAPABILITIES_RESPONSE;
//			metricsMsg.body.panCapabilitiesResponse = msg.body.panCapabilitiesResponse;
//			log = _T("PAN_CAPABILITIES_RESPONSE");
//			break;

		default:
			return false;
		}

		break;

	case SEquipCtrlMsg::PAN_DISP_CTRL:
		switch(msg.hdr.msgSubType)
		{
		case SEquipCtrlMsg::GET_PAN_RESPONSE:
			memcpy(&metricsMsg.body.getPanResp, &msg.body.getPanResp,
				offsetof(SEquipCtrlMsg::SGetPanResp, binData[msg.body.getPanResp.numBins]));
//			log = _T("GET_PAN_RESPONSE");
			break;

		default:
			return false;
		}

		break;

	case SEquipCtrlMsg::STATUS_CTRL:
		switch(msg.hdr.msgSubType)
		{
		case SEquipCtrlMsg::GET_FAULT_RESP:
			metricsMsg.body.getFaultResp = msg.body.getFaultResp;
//			log = _T("Metrics' GET_FAULT_RESP");
			break;

		case SEquipCtrlMsg::GET_CSMS_FAULT_RESP:
			metricsMsg.body.getCsmsFaultResp = msg.body.getCsmsFaultResp;
//			log = _T("Metrics GET_CSMS_FAULT_RESP");
			break;

		default:
			return false;
		}

		break;

	case SEquipCtrlMsg::SOUNDER_CTRL:

		metricsMsg.hdr.sourceAddr = msg.hdr.sourceAddr;
		metricsMsg.hdr.destAddr = msg.hdr.destAddr;
		metricsMsg.hdr.msgType = msg.hdr.msgType;
		metricsMsg.hdr.cmdVersion = msg.hdr.cmdVersion;
		metricsMsg.hdr.respVersion = msg.hdr.cmdVersion;
		metricsMsg.hdr.msgSubType = msg.hdr.msgSubType;
		metricsMsg.hdr.bodySize = msg.hdr.bodySize;

		switch (msg.hdr.msgSubType)
		{
		case SMetricsMsg::SOUNDER_SETUP_RESPONSE:
		case SMetricsMsg::SOUNDER_BLANKER_FREQ_RESPONSE:
		case SMetricsMsg::SOUNDER_START_STOP_RESPONSE:
		case SMetricsMsg::SOUNDER_TEST_RESPONSE:
			metricsMsg.body.genericResp.status = msg.body.genericResp.status;
			break;

		case SMetricsMsg::SOUNDER_STATUS_RESPONSE:
			memcpy(&metricsMsg.body.sounderStatusResp, &msg.body.sounderStatusResp, sizeof(SSmsMsg::SGetSounderStatusResp));
			break;

		case SMetricsMsg::SOUNDER_IONO_RESPONSE:
			break;

		default:
			return false;
		}
     break;

	}

	// Forward the translated message
	bool sent = CMetricsNet::Send(client, &metricsMsg, offsetof(SMetricsMsg, body) + metricsMsg.hdr.bodySize);

	if(sent && !log.empty() && CLog::GetLogLevel() == CLog::VERBOSE)
	{
		// Verbose logging
		CLog::Log(CLog::INFORMATION, _T("Sent %s"), log.c_str());
	}

	return sent;
}

