/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *E
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "Config.h"
#include "EquipCtrlMsg.h"
#include "Units.h"
#include "VCPMsg.h"

class C7234201102
{
public:
	// Constants
//	static const unsigned long ANT_CROSSOVER_FREQ_HZ = 2500000000;
	static const unsigned long long CBAND_AMP_CROSSOVER_HZ = 3000000000ULL;
	static const size_t NUM_ANTS = 9;

	enum ESource // Some values match hardware - don't change!
	//ANT_1H to ANT_9 value matches with lower 6-bit Control Value.
	{
		TERM	= 0,
		REF		= -1,
		BITE	= -2,
		NOISE	= -3,
		REF_HORIZON = -4,
		ANT_1H	= 0x01,
		ANT_2H	= 0x02,
		ANT_3H	= 0x03,
		ANT_4H	= 0x04,
		ANT_5H	= 0x05,
		ANT_6H	= 0x06,
		ANT_7H	= 0x07,
		ANT_8H	= 0x08,
		ANT_9H	= 0x09,
		ANT_NOISE = 0x0F,
		ANT_1	= 0x11,
		ANT_2	= 0x12,
		ANT_3	= 0x13,
		ANT_4	= 0x14,
		ANT_5	= 0x15,
		ANT_6	= 0x16,
		ANT_7	= 0x17,
		ANT_8	= 0x18,
		ANT_9	= 0x19,
	};
	enum EDfElemBandSel { AUTO, UHF, SHF };
	enum ERefMode { RECEIVE, TX_BITE };

#if 0
	//============= Define lower 6-bit control constants (AC10 - AC15) ====
	//Cal tone & noise sample selection is 0x0f.
	static const unsigned char C_CAL_OR_NOISE_SAMPLE_SELECTION = 0x0f;
	static const unsigned char B_VERTICAL_POL = 0x10;
	static const unsigned char C_HORIZONTAL_VAL = 0;

	//Terminated sample selection is 0x00.
	static const unsigned char C_TERMINATED_SAMPLE_SELECTION = 0x00;
	//=============												===========

	//============= Define middle 6-bit control constants (AC20 - AC25) ===
	static const unsigned char B_SHF = 0x10;
	static const unsigned char C_NO_CONVERTER = 0;

	//============= Define upper 6-bit control constants (AC30 - AC35) ====
	static const unsigned char B_CAL_2_REF = 0x08;
	static const unsigned char B_CAL_2_ANTENNA = 0x10;

	static const unsigned char C_REFOUT_2_REF_ANTENNA = 0x00;
	static const unsigned char C_NOISE_ON = 0x03;
	static const unsigned char C_REFOUT_2_THERMAL_NOISE = 0x0B;

	//Terminated Reference selection is 0x0C where CAL Tone is turned-off
	// and Reference output is connected to the terminated CAL Tone off path.
	static const unsigned char C_TERMINATED_REF_SELECTION = 0x0C;
	//=============												===========
#endif

	static const ESource GRAY_CODE9[2][9];		// 0 => vertical, 1 => horizontal
	static const ESource GRAY_CODE5[5];

	// Functions
	static float GetCalDbm(size_t index, size_t ch);
	static Units::Frequency GetCalFreq(size_t index);
	static float GetCalHalfRangeDbm(size_t index);
	static unsigned long GetControlWord(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf, EDfElemBandSel band = AUTO);
	unsigned long GetControlWord(const std::vector<unsigned long>& pattern);
	void GetCurrentControlWord(unsigned long& controlWord) const
		{ std::lock_guard<std::mutex> lock(m_critSect); controlWord = m_controlWord; return; }
	static size_t Index9(ESource source);
	static size_t Index5(ESource source);
	bool IsPresent(void) const { return m_isPresent; }
	void SetSwitch(const SVCPMsg::USwitch::SVushf& req);
	void SetSwitch(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf, EDfElemBandSel band = AUTO);
	static ESource Source(SEquipCtrlMsg::EAnt ant);

protected:
	// Types
	struct SCalSettings
	{
		Units::Frequency freq;
		float refDbm;
		float sampleDbm;
		float passRangeDbm;		//Half range where the measured value should lay.
		unsigned long control;
	};

	struct SBandSettings
	{
		Units::Frequency low20dbFreq;
		Units::Frequency low1dbFreq;
		Units::Frequency high1dbFreq;
		Units::Frequency high20dbFreq;
		unsigned short data;
	};
	typedef std::vector<SBandSettings> BandSettings;

	// Constants
	static const BandSettings BAND_SETTINGS;
	static const SBandSettings BAND_SETTINGS_[];
	static const SCalSettings CAL_SETTINGS[];

	// Functions
	C7234201102(void); // Can only be constructed by a derived class
	virtual ~C7234201102(void);

private:

	// Functions
	static unsigned int CalcSettleTime(const unsigned long currCtrlWord, const unsigned long newCtrlWord);
	static const SCalSettings& GetCalSettings(Units::Frequency calFreq);
	void WaitForSettle(unsigned long usec);

	// Data
	mutable std::mutex m_critSect;
	volatile unsigned long m_controlWord;
	static Units::Frequency m_crossOverFreq;
	bool m_isPresent;

};


//////////////////////////////////////////////////////////////////////
//
// Conversion function for ESource
//
inline C7234201102::ESource C7234201102::Source(SEquipCtrlMsg::EAnt ant)
{
	if (CConfig::IsTermAnt(ant))
		return C7234201102::TERM;

	switch(ant)
	{
	case SEquipCtrlMsg::ANT1:
		return C7234201102::REF;

	case SEquipCtrlMsg::ANT1H:
		return C7234201102::REF_HORIZON;

	case SEquipCtrlMsg::ANT2:
	case SEquipCtrlMsg::ANT3:
	case SEquipCtrlMsg::SMPL_RF2:
	case SEquipCtrlMsg::SMPL_RF3:
		return C7234201102::TERM;

	case SEquipCtrlMsg::DF_ANT_1V:
		return C7234201102::ANT_1;

	case SEquipCtrlMsg::DF_ANT_2V:
		return C7234201102::ANT_2;

	case SEquipCtrlMsg::DF_ANT_3V:
		return C7234201102::ANT_3;

	case SEquipCtrlMsg::DF_ANT_4V:
		return C7234201102::ANT_4;

	case SEquipCtrlMsg::DF_ANT_5V:
		return C7234201102::ANT_5;

	case SEquipCtrlMsg::DF_ANT_6V:
		return C7234201102::ANT_6;

	case SEquipCtrlMsg::DF_ANT_7V:
		return C7234201102::ANT_7;

	case SEquipCtrlMsg::DF_ANT_8V:
		return C7234201102::ANT_8;

	case SEquipCtrlMsg::DF_ANT_9V:
		return C7234201102::ANT_9;

	case SEquipCtrlMsg::DF_ANT_1H:
		return C7234201102::ANT_1H;

	case SEquipCtrlMsg::DF_ANT_2H:
		return C7234201102::ANT_2H;

	case SEquipCtrlMsg::DF_ANT_3H:
		return C7234201102::ANT_3H;

	case SEquipCtrlMsg::DF_ANT_4H:
		return C7234201102::ANT_4H;

	case SEquipCtrlMsg::DF_ANT_5H:
		return C7234201102::ANT_5H;

	case SEquipCtrlMsg::DF_ANT_6H:
		return C7234201102::ANT_6H;

	case SEquipCtrlMsg::DF_ANT_7H:
		return C7234201102::ANT_7H;

	case SEquipCtrlMsg::DF_ANT_8H:
		return C7234201102::ANT_8H;

	case SEquipCtrlMsg::DF_ANT_9H:
		return C7234201102::ANT_9H;

	default:
		ASSERT(FALSE);
		return C7234201102::TERM;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Conversion function for ESource to get DF antenna index for 5 element graycode
//
inline size_t C7234201102::Index5(ESource source)
{
	switch(source)
	{
	case ANT_1: return 0;
	case ANT_3: return 1;
	case ANT_5: return 2;
	case ANT_7: return 3;
	case ANT_9: return 4;

	case TERM:
		return 0;

	default:
		ASSERT(FALSE);
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Conversion function for ESource to get DF antenna index for 9 element graycode
//
inline size_t C7234201102::Index9(ESource source)
{
	switch(source)
	{
	case ANT_1:
	case ANT_2:
	case ANT_3:
	case ANT_4:
	case ANT_5:
	case ANT_6:
	case ANT_7:
	case ANT_8:
	case ANT_9:
		return source - static_cast<size_t>(ANT_1);

	case ANT_1H:
	case ANT_2H:
	case ANT_3H:
	case ANT_4H:
	case ANT_5H:
	case ANT_6H:
	case ANT_7H:
	case ANT_8H:
	case ANT_9H:
		return source - static_cast<size_t>(ANT_1H);

	case TERM:
		return 0;

	default:
		ASSERT(FALSE);
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// ++ operator for ESource
//
inline C7234201102::ESource& operator++(C7234201102::ESource& s)
{
	switch(s)
	{
	case C7234201102::ANT_1:
	case C7234201102::ANT_2:
	case C7234201102::ANT_3:
	case C7234201102::ANT_4:
	case C7234201102::ANT_5:
	case C7234201102::ANT_6:
	case C7234201102::ANT_7:
	case C7234201102::ANT_8:
	case C7234201102::ANT_1H:
	case C7234201102::ANT_2H:
	case C7234201102::ANT_3H:
	case C7234201102::ANT_4H:
	case C7234201102::ANT_5H:
	case C7234201102::ANT_6H:
	case C7234201102::ANT_7H:
	case C7234201102::ANT_8H:
		s = static_cast<C7234201102::ESource>(s + 1);
		break;

	case C7234201102::ANT_9:
		s = C7234201102::ANT_1;
		break;

	case C7234201102::ANT_9H:
		s = C7234201102::ANT_1H;
		break;

	default:
		break;
	}

	return s;
}

// Use selectany to put this here rather than in the .cpp so the size of the array can be determined by users of the class
//__attribute__((__weak__))
//const C7234201102::SBandSettings C7234201102::BAND_SETTINGS_[] =
//{ // low 20 dB, low 1 dB, high 1 dB, high 20 dB, data
//	{   20000000,   20000000, 2700000000, 2700000000, 0x00 },
//	{ 2300000000, 2700000000, 3200000000, 3700000000, 0x09 },
//	{ 2800000000, 3200000000, 4000000000, 4500000000, 0x0a },
//	{ 3300000000, 4000000000, 4800000000, 5300000000, 0x0b },
//	{ 3900000000, 4800000000, 5600000000, 6200000000, 0x0c },
//	{ 4900000000, 5600000000, 6400000000, 7000000000, 0x0d },
//	{ 5500000000, 6400000000, 7200000000, 8100000000, 0x0e },
//	{ 6000000000, 7200000000, 8000000000, 8800000000, 0x0f }
//};


