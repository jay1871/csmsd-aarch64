/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once
#include <algorithm>
#include <assert.h>
#include <condition_variable>
#include <memory>
#include <mutex>

template<class T, size_t DEPTH, size_t STRATA> class CTaskScheduler
{
	// NOTE: This class cannot be used with std::unique_ptr
public:
	static const unsigned int INFINITE = static_cast<unsigned int>(-1);

	// Exceptions
	enum EProblem { TIMEDOUT, CLOSED };


	// Entries are always added at the position indicated by count
	// They are retrieved via next (if count > 0) which is incremented by 1 after
	//  each retrieval, with a wrap at count.
	// When entries are deleted, next may have to change so that it continues to point
	//  to the same entry after deletion.
	// There are STRATA normal stratum. If stratum is == STRATA, then it refers to the system (priority) group
	CTaskScheduler(void) :
		m_closed(false),
		m_count(0),
		m_countRunnable(0),
		m_nextStratum(0),
		m_stratum()
	{
	}

	virtual ~CTaskScheduler(void)
	{
		assert(!IsRunnable());

		if (!IsRunnable() && !IsEmpty())
		{
			RemoveNonRunnableItems();
		}

		assert(IsEmpty());
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Add an item to the scheduler.
	//
	void AddItem(const T& entry, int stratum, bool isRunnable, unsigned int timeout = INFINITE)
	{
		if ((unsigned int) stratum > STRATA)
		{
			throw std::logic_error("stratum must be <= template parameter STRATA");
		}
		auto& s = m_stratum[stratum];

		std::unique_lock<std::mutex> lock(m_mutex);

		while (!m_closed && (s.count == DEPTH))
		{
			if (timeout == INFINITE)
			{
				s.addable.wait(lock);
			}
			else if (s.addable.wait_for(lock, std::chrono::milliseconds(timeout)) == std::cv_status::timeout)
			{
				throw TIMEDOUT;
			}
		}

		if (m_closed)
		{
			throw CLOSED;
		}

		size_t insert = s.count++;
		++m_count;

		s.entries[insert] = entry;
		s.runnables[insert] = isRunnable;
		if (isRunnable)
		{
			++s.countRunnable;
			++m_countRunnable;
		}

		if (m_countRunnable == 1)
		{
			m_getable.notify_all();
		}

		return;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Find and change the stratum of the first item for which the supplied functor returns true.
	//
	template<typename U> bool ChangeStratum(U matches, int stratum, bool isRunnable, unsigned int timeout = INFINITE)
	{
		if (stratum > STRATA)
		{
			throw std::logic_error("stratum must be <= template parameter STRATA");
		}

		std::unique_lock<std::mutex> lock(m_mutex);

		// First, find and remove the item
		bool found = false;
		T item;
		for (size_t k = 0; k <= STRATA; ++k)
		{
			auto& s = m_stratum[k];

			size_t i;
			for (i = 0; i < s.count && !matches(s.entries[i]); ++i);

			if (i != s.count)
			{
				// i is the index of the entry to be removed in this stratum.
				item = s.entries[i];
				if (s.runnables[i])
				{
					// Update runnable counts
					--s.countRunnable;
					--m_countRunnable;
				}
				if (i == s.count - 1)
				{
					s.next = 0;
				}
				else
				{
					if (s.next > i) --s.next;

					// Need to move [i+1] -> [i] ... [count - 1] -> [count - 2]
					for(; i < s.count - 1; ++i)
					{
						s.entries[i] = s.entries[i + 1];
						s.runnables[i] = s.runnables[i + 1];
					}
				}

				s.entries[s.count - 1] = T();	// Clear last entry
				s.runnables[s.count - 1] = false;

				--s.count;
				--m_count;

				if (s.count == DEPTH - 1)
				{
					s.addable.notify_all();
				}
				found = true;
			}
		}
		if (!found)
			return false;	// not found anywhere

		// Second, add the item into the new stratum
		auto& s = m_stratum[stratum];

		while (!m_closed && (s.count == DEPTH))
		{
			if (timeout == INFINITE)
			{
				s.addable.wait(lock);
			}
			else if (s.addable.wait_for(lock, std::chrono::milliseconds(timeout)) == std::cv_status::timeout)
			{
				throw TIMEDOUT;
			}
		}

		if (m_closed)
		{
			throw CLOSED;
		}

		size_t insert = s.count++;
		++m_count;

		s.entries[insert] = item;
		s.runnables[insert] = isRunnable;
		if (isRunnable)
		{
			++s.countRunnable;
			++m_countRunnable;
		}

		if (m_countRunnable == 1)
		{
			m_getable.notify_all();
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Close the scheduler
	//
	void Close(void)
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		m_closed = true;

		if (m_countRunnable == 0)
		{
			m_getable.notify_all();
		}

		for (size_t i = 0; i <= STRATA; ++i)
		{
			m_stratum[i].addable.notify_all();
		}
		return;

	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Get the total number of items in the scheduler
	//
	size_t Count(void) const
	{
		size_t count = 0;
		std::unique_lock<std::mutex> lock(m_mutex);
		for (size_t i = 0; i <= STRATA; ++i)
		{
			count += m_stratum[i].count;
		}
		return count;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Get the number of items in the scheduler
	//
	size_t Count(int stratum) const
	{
		auto& s = m_stratum[stratum];

		std::unique_lock<std::mutex> lock(m_mutex);

		return s.count;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Get the number of runnable items in the scheduler
	//
	size_t CountRunnable(int stratum) const
	{
		auto& s = m_stratum[stratum];

		std::unique_lock<std::mutex> lock(m_mutex);

		return s.countRunnable;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Get the depth of the scheduler
	//
	size_t Depth(void) const
	{
		return DEPTH;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Call the supplied functor for each item in the task scheduler. Stop if it returns false;
	//
	template<typename U> void ForEachItem(U callback) const
	{
		std::unique_lock<std::mutex> lock(m_mutex);

		for (size_t k = 0; k <= STRATA; ++k)
		{
			auto& s = m_stratum[k];

			size_t i;
			for (i = 0; i < s.count; ++i)
			{
				if (!callback(s.entries[i]))
					return;
			}

		}
		return;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Get the first available item from the scheduler with optional timeout
	// Leave it in the schedule table
	//
	T GetItem(unsigned int timeout = INFINITE)
	{
		std::unique_lock<std::mutex> lock(m_mutex);

		while (m_countRunnable == 0  && !m_closed)
		{
			if (timeout == INFINITE)
			{
				m_getable.wait(lock);
			}
			else if (m_getable.wait_for(lock, std::chrono::milliseconds(timeout)) == std::cv_status::timeout)
			{
				throw TIMEDOUT;
			}
		}

		if (m_closed && m_countRunnable == 0)
		{
			throw CLOSED;
		}

		// Since m_countRunnable > 0, there must be at least one runnable item, else logic_error!

		T item;
		// Check system stratum first
		if (m_stratum[STRATA].countRunnable > 0)	// This stratum has runnable items
		{
			auto& s = m_stratum[STRATA];
			for (size_t i = 0; i < s.count; ++i)
			{
				if (s.runnables[s.next])
				{
					// Found the item to return, return it without updating for next time
					item = s.entries[s.next];
					return item;
				}
				else
				{
					++s.next %= s.count;
				}
			}
			throw std::logic_error("logic error 3 in GetItem");
		}
		for (size_t k = 0; k < STRATA; ++k)
		{
			auto& s = m_stratum[m_nextStratum];
			if (s.countRunnable == 0)	// This stratum has no runnable items
			{
				++m_nextStratum %= STRATA;
			}
			else
			{
				for (size_t i = 0; i < s.count; ++i)
				{
					if (s.runnables[s.next])
					{
						// Found the item to return, update for next time before returning it
						item = s.entries[s.next];
						++s.next %= s.count;
						++m_nextStratum %= STRATA;
						return item;
					}
					else
					{
						++s.next %= s.count;
					}
				}
				throw std::logic_error("logic error 1 in GetItem");
			}
		}
		throw std::logic_error("logic error 2 in GetItem");
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Is the scheduler closed?
	//
	bool IsClosed(void) const
	{
		std::unique_lock<std::mutex> lock(m_mutex);

		return m_closed;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Is the scheduler empty?
	//
	bool IsEmpty(void) const
	{
		std::unique_lock<std::mutex> lock(m_mutex);

		return m_count == 0;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Are any tasks runnable?
	//
	bool IsRunnable() const
	{
		std::unique_lock<std::mutex> lock(m_mutex);

		return m_countRunnable > 0;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Find and modify the first item for which the supplied functor returns true.
	// Returns false if no such item
	//
	template<typename U> bool ModifyItem(U modifies)
	{
		std::unique_lock<std::mutex> lock(m_mutex);

		for (size_t k = 0; k <= STRATA; ++k)
		{
			auto& s = m_stratum[k];

			size_t i;
			for(i = 0; i < s.count && !modifies(s.entries[i]); ++i);

			if (i != s.count)	// modify was successful
			{
				return true;
			}
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Remove an item
	//
	template<typename U> bool RemoveItem(U matches)
	{
		std::unique_lock<std::mutex> lock(m_mutex);

		for (size_t k = 0; k <= STRATA; ++k)
		{
			auto& s = m_stratum[k];

			size_t i;
			for (i = 0; i < s.count && !matches(s.entries[i]); ++i);

			if (i != s.count)
			{
				// i is the index of the entry to be removed in this stratum.
				if (s.runnables[i])
				{
					// Update runnable counts
					--s.countRunnable;
					--m_countRunnable;
				}
				if (i == s.count - 1)
				{
					s.next = 0;
				}
				else
				{
					if (s.next > i) --s.next;

					// Need to move [i+1] -> [i] ... [count - 1] -> [count - 2]
					for(; i < s.count - 1; ++i)
					{
						s.entries[i] = s.entries[i + 1];
						s.runnables[i] = s.runnables[i + 1];
					}
				}

				s.entries[s.count - 1] = T();	// Clear last entry
				s.runnables[s.count - 1] = false;

				--s.count;
				--m_count;

				if (s.count == DEPTH - 1)
				{
					s.addable.notify_all();
				}

				return true;
			}
		}
		return false;	// not find anywhere
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Remove all non-runnable items
	//
	void RemoveNonRunnableItems(void)
	{
		std::unique_lock<std::mutex> lock(m_mutex);

		for (size_t k = 0; k <= STRATA; ++k)
		{
			auto& s = m_stratum[k];

			size_t removed = 0;;
			for (size_t i = 0, j = 0; i < s.count; ++i)	// i is the source item, j is the dest item
			{
				if (s.runnables[i])
				{
					// This item is runnable. Leave in by advancing dest item and copying item
					if (j != i)
					{
						s.entries[j] = s.entries[i];
						s.runnables[j] = s.runnables[i];
					}
					++j;
				}
				else
				{
					// This item is not runnable. Don't advance dest item and update relevant parameters
					if (s.next > i) --s.next;
					++removed;
				}
			}
			if (removed > 0)
			{
				for (size_t n = s.count - removed; n < s.count; ++n)
				{
					s.entries[n] = T();	// Clear entry
					s.runnables[n] = false;
				}
				s.count -= removed;
				m_count -= removed;
				if (s.next >= s.count) s.next = 0;

				if (s.count == DEPTH - removed)
				{
					s.addable.notify_all();
				}
			}
		}
		return;
	}


	////////////////////////////////////////////////////////////////////////////////
	//
	// Find and replace the first item for which the supplied functor returns true.
	// NOTE: cannot change stratum of existing entry, but can change runnable state
	// If no such item, add it
	//
	template<typename U> bool ReplaceItem(U matches, const T& entry, int stratum, bool isRunnable, unsigned int timeout = INFINITE)
	{
		std::unique_lock<std::mutex> lock(m_mutex);

		for (size_t k = 0; k <= STRATA; ++k)
		{
			auto& s = m_stratum[k];

			size_t i;
			for (i = 0; i < s.count && !matches(s.entries[i]); ++i);

			if (i != s.count)	// match was found
			{
				s.entries[i] = entry;
				if (s.runnables[i] != isRunnable)	// Changing state
				{
					if (isRunnable)
					{
						// was not runnable, change to runnable
						++s.countRunnable;
						++m_countRunnable;
						if (m_countRunnable == 1)
						{
							m_getable.notify_all();
						}
					}
					else
					{
						// was runnable, change to not runnable
						--s.countRunnable;
						--m_countRunnable;
					}
					s.runnables[i] = isRunnable;
				}
				return true;
			}
		}
		// No match was found
		if ((unsigned int) stratum > STRATA)
		{
			throw std::logic_error("stratum must be <= template parameter STRATA");
		}
		auto& s = m_stratum[stratum];

		while (!m_closed && (s.count == DEPTH))
		{
			if (timeout == INFINITE)
			{
				s.addable.wait(lock);
			}
			else if (s.addable.wait_for(lock, std::chrono::milliseconds(timeout)) == std::cv_status::timeout)
			{
				throw TIMEDOUT;
			}
		}

		if (m_closed)
		{
			throw CLOSED;
		}

		size_t insert = s.count++;
		++m_count;

		s.entries[insert] = entry;
		s.runnables[insert] = isRunnable;
		if (isRunnable)
		{
			++s.countRunnable;
			++m_countRunnable;
		}

		if (m_countRunnable == 1)
		{
			m_getable.notify_all();
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Find and retrieve the first item for which the supplied functor returns true.
	// If no such item, return false
	//
	template<typename U> bool RetrieveItem(U matches, T& item)
	{
		std::unique_lock<std::mutex> lock(m_mutex);

		for (size_t k = 0; k <= STRATA; ++k)
		{
			auto& s = m_stratum[k];

			size_t i;
			for(i = 0; i < s.count && !matches(s.entries[i]); ++i);

			if (i != s.count)	// match was found
			{
				item = s.entries[i];
				return true;
			}
		}
		// No match was found
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Set the runnable status for the first item for which the supplied functor returns true.
	// Returns false if no such item
	//
	template<typename U> bool SetRunnable(U matches, bool isRunnable)
	{
		std::unique_lock<std::mutex> lock(m_mutex);

		for (size_t k = 0; k <= STRATA; ++k)
		{
			auto& s = m_stratum[k];

			size_t i;
			for (i = 0; i < s.count && !matches(s.entries[i]); ++i);

			if (i != s.count)
			{
				if (s.runnables[i] != isRunnable)	// Changing state
				{
					if (isRunnable)
					{
						// was not runnable, change to runnable
						++s.countRunnable;
						++m_countRunnable;
						if (m_countRunnable == 1)
						{
							m_getable.notify_all();
						}
					}
					else
					{
						// was runnable, change to not runnable
						--s.countRunnable;
						--m_countRunnable;
					}
					s.runnables[i] = isRunnable;
				}
				return true;
			}
		}
		return false;
	}

private:
	//Types
	struct SStratum
	{
		SStratum(void) : count(0), countRunnable(0), next(0)
		{

		}
		std::condition_variable addable;
		size_t count;
		size_t countRunnable;
		size_t next;
		std::array<T, DEPTH> entries;
		std::array<bool, DEPTH> runnables;
	};

	// Functions

	// Data
	bool m_closed;
	size_t m_count;
	size_t m_countRunnable;
	size_t m_nextStratum;
	std::array<SStratum, STRATA + 1> m_stratum;		// m_stratum[STRATA] is the system (priority) group
	std::condition_variable m_getable;
	mutable std::mutex m_mutex;

};
