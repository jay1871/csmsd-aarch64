/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include <list>
#include "2630.h"
#include "Config.h"
#include "Ne10Vec.h"
#include "ShfExt.h"
#include "Units.h"

class CRadioEquip :
	// Inheritance
	public C2630
{
	// Friends
	friend class CSingleton<CRadioEquip>;

public:
	// Types
	struct SGainTable
	{
		S2630Msg::ECalibrationType type;
		size_t subType;
		double startValue;
		double stepValue;
		double lowFreq;
		size_t ifIndex;
		size_t attIndexes[size_t(C2630::EGainMode::NUM_GAINMODES)];
		struct
		{
			double startValue;
			double stepValue;
		} low;
		ErrorCodes::EErrorCode status;
	};

	// Functions
	unsigned long CalcRxFreqMHz(bool hf, Units::Frequency frequency) const { return C2630::CalcRxFreqMHz(hf, frequency); }
	unsigned char CalcSampleGain(Units::Frequency rfFreq, bool isHorizon, bool hf) const;
	bool DoBistCal(size_t fpi, bool narrow, unsigned long& calFreq, float& rxInputDbm, bool& direct, float&expectedDbmAt3230);
	void GetAtten(unsigned char& atten);
	void GetAtten(unsigned char& atten, C2630::EGainMode& gainMode) { return C2630::GetAtten(atten, gainMode); }
	void GetCurrentAttenLimits(unsigned char& min, unsigned char& max) const;
	bool GetCalStatus(std::vector<SGainTable>& status) const;
	float GetCorrection(Units::Frequency frequency, Units::Frequency radioFreq, size_t rfGainTableIdx,
		long ifGainTableIdx, bool hf, unsigned char atten) const;
	float GetCorrection(Units::Frequency frequency, Units::Frequency radioFreq, size_t rfGainTableIdx,
		long ifGainTableIdx, unsigned char atten, EGainMode gainMode) const;
	void GetCorrection(Units::Frequency rfFreq, Units::Frequency radioFreq, Units::Frequency rxHwBw, size_t rfGainTableIdx,
		long ifGainTableIdx, Units::Frequency binSize, bool hf, unsigned char atten, Ne10F32Vec& adjPower) const;
	void GetCorrection(Units::Frequency rfFreq, Units::Frequency radioFreq, Units::Frequency rxHwBw, size_t rfGainTableIdx,
		long ifGainTableIdx, Units::Frequency binSize, unsigned char atten, EGainMode gainMode, Ne10F32Vec& adjPower) const;
	EGainMode GetGainMode(void) const { return C2630::GetGainMode(); }
	unsigned char GetMaxMaxAtten(EGainMode gainMode) const;
	float GetRadioTemp(void) const { std::lock_guard<std::mutex> lock(m_tempsMutex); return m_radioTemp; }
	Units::Frequency GetRxBandwidth(bool hf, bool narrow) const;
	Units::Frequency GetRxBandwidth(const CConfig::SMutableConfig& mutableConfig, bool hf, bool narrow) const;
	float GetRxGainRatio(size_t& rfGainTableIdx, long& ifGainTableIdx, float& gainAdjustmentTemp) const;
	Units::Frequency GetMaxHfFreq(void) const;
	bool IsHfFpi(size_t fpi) const { return fpi < m_hfBands.size(); }
	bool IsShfSpectrumInverted(_In_ Units::Frequency freq) {return(m_shfExt->IsSpectrumInverted(freq));}
	size_t NumFpis(void) const { return m_hfBands.size() + m_vushfBands.size(); }
	unsigned char RoundAtten(unsigned char atten) const;
	bool RunBist(S2630Msg::SBistResults& bistResults);
	void SetAtten(unsigned char refAtten, Units::Frequency freq, bool hf);
	void SetAttenForSpecifiedOutput(float rxInputDbm, float wantedDbmAt3230, EGainMode gainMode, float& expectedDbmAt3230);
	bool Tune(Units::Frequency rfFreq, Units::Frequency rxBw, Units::Frequency procBw, bool hf, C2630::EBandSelect bandSelect,
		Units::Frequency& finalIfFreq, bool& inverted, Units::FreqPair& freqLimits, Units::Frequency& radioFreq, size_t& band, bool& direct,
		const SEquipCtrlMsg::EAnt ant);
	bool TuneShfFreq(_In_ Units::Frequency rfFreq) {return(m_shfExt->Tune(rfFreq));}
	void UpdateConfig(void);
	void UpdateRadioTemp(const std::vector<float>& temps)
	{
		std::lock_guard<std::mutex> lock(m_tempsMutex);
		m_radioTemp = m_radioTemp * (1 - TEMP_TIME_CONSTANT) + temps[S2630Msg::CALIB_TEMP_INDEX] * TEMP_TIME_CONSTANT;
		return;
	}
	void Wait4Shf(void) {return(m_shfExt->Wait());}

protected:
	// Functions
//	void SetRfInput(SEquipCtrlMsg::EAnt ant);
	CRadioEquip();
	virtual ~CRadioEquip();

private:
	// Constants
	static const size_t NUM_CACHED_CALS = 128;
	static constexpr const float TEMP_TIME_CONSTANT = .05f;		// 63% rise in 20 samples

	// Types
	struct SCachedCorrection
	{
		Units::Frequency freq;
		Units::Frequency rxHwBw;
		Units::Frequency binSize;
		Units::Frequency radioFreq;
		size_t rfGainTableIdx;
		unsigned char atten;
		Ne10F32Vec cachedAdjustPower;
	};

	typedef std::list<size_t> MruList;

	mutable SCachedCorrection m_cachedCorrections[NUM_CACHED_CALS];
	CSingleton<CConfig> m_config;
	const CConfig::SMutableConfig& m_mutableConfig; // Must be after m_config
	mutable MruList m_mruList;
	volatile float m_radioTemp;
	CSingleton<CShfExt> m_shfExt;
	mutable std::mutex m_tempsMutex;
	S2630Msg::SBistResults m_bistResults; // save for later retrieval
};

