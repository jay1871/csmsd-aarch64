/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "Log.h"
#include "RealtimeNet.h"

//////////////////////////////////////////////////////////////////////
//
// Check if any version "version" real-time IQ clients
//
unsigned long CRealtimeNet::GetNumIqClients(unsigned long version)
{
	ASSERT(GetMode() == SERVER);
	SNumClients s;
	s.version = version;
	s.numIqClients = 0;
	ForEachConnection(&CRealtimeNet::UpdateNumIqClients, &s);

//	TRACE("GetNumIqClients %lu => %lu\n", s.version, s.numIqClients);
	return s.numIqClients;
}

unsigned long CRealtimeNet::GetNumSpectrumClients(unsigned long version)
{
	ASSERT(GetMode() == SERVER);
	SNumClients s;
	s.version = version;
	s.numSpectrumClients = 0;
	ForEachConnection(&CRealtimeNet::UpdateNumSpectrumClients, &s);

//	TRACE("GetNumSpectrumClients %lu => %lu\n", s.version, s.numSpectrumClients);
	return s.numSpectrumClients;
}

unsigned long CRealtimeNet::GetNumDfClients(unsigned long version)
{
	ASSERT(GetMode() == SERVER);
	SNumClients s;
	s.version = version;
	s.numDfClients = 0;
	ForEachConnection(&CRealtimeNet::UpdateNumDfClients, &s);

//	TRACE("GetNumDfClients %lu => %lu\n", s.version, s.numDfClients);
	return s.numDfClients;
}

void CRealtimeNet::UpdateNumIqClients(_In_ void* arg)
{
	auto s = static_cast<SNumClients*>(arg);
	VersionMap::const_iterator it = m_versionMap.find(SSmsRealtimeMsg::RT_IQ_DATA);
	if (it != m_versionMap.end() && it->second == s->version)
	{
		++s->numIqClients;
	}
	return;
}

void CRealtimeNet::UpdateNumSpectrumClients(_In_ void* arg)
{
	auto s = static_cast<SNumClients*>(arg);
	VersionMap::const_iterator it = m_versionMap.find(SSmsRealtimeMsg::RT_SPECTRUM);
	if (it != m_versionMap.end() && it->second == s->version)
	{
		++s->numSpectrumClients;
	}
	return;
}

void CRealtimeNet::UpdateNumDfClients(_In_ void* arg)
{
	auto s = static_cast<SNumClients*>(arg);
	VersionMap::const_iterator it = m_versionMap.find(SSmsRealtimeMsg::RT_DF_DATA);
	if (it != m_versionMap.end() && it->second == s->version)
	{
		++s->numDfClients;
	}
	return;
}

//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CRealtimeNet::OnClose(int errorCode)
{
	if (auto peer = GetPeerAddress())
	{
		CLog::Log(CLog::INFORMATION, "%s disconnected from Realtime port, code = %d", peer->ai_canonname, errorCode);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CRealtimeNet::OnConnect(void)
{
	if (auto peer = GetPeerAddress())
	{
		CLog::Log(CLog::INFORMATION, "%s connected to Realtime port", peer->ai_canonname);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CRealtimeNet::OnMessage(_In_bytecount_(size) const void* msg, size_t size)
{
	ASSERT(size == offsetof(SSmsRealtimeMsg, body));
	if(size != offsetof(SSmsRealtimeMsg, body))
	{
		auto peer = GetPeerAddress();
		LPCTSTR peerName(peer == nullptr ? _T("Unknown") : peer->ai_canonname);
		// Corrupt message
		CLog::Log(CLog::ERRORS, "Realtime: corrupt message from %s", peerName);
		Close();
		return;
	}

	const SSmsRealtimeMsg* rtMsg = static_cast<const SSmsRealtimeMsg*>(msg);

	// Validate
	switch(rtMsg->hdr.msgType)
	{
	case SSmsRealtimeMsg::RT_SPECTRUM_START:
		m_versionMap[rtMsg->hdr.msgType] = std::min(2ul, rtMsg->hdr.msgTypeVersion);
		break;

	case SSmsRealtimeMsg::RT_SPECTRUM_STOP:
		m_versionMap[rtMsg->hdr.msgType] = std::min(1ul, rtMsg->hdr.msgTypeVersion);
		break;

	case SSmsRealtimeMsg::RT_SPECTRUM:
		m_versionMap[rtMsg->hdr.msgType] = std::min(3ul, rtMsg->hdr.msgTypeVersion);
		break;

	case SSmsRealtimeMsg::RT_DF_START:
		m_versionMap[rtMsg->hdr.msgType] = std::min(2ul, rtMsg->hdr.msgTypeVersion);
		break;

	case SSmsRealtimeMsg::RT_DF_STOP:
		m_versionMap[rtMsg->hdr.msgType] = std::min(1ul, rtMsg->hdr.msgTypeVersion);
		break;

	case SSmsRealtimeMsg::RT_DF_DATA:
		m_versionMap[rtMsg->hdr.msgType] = std::min(3ul, rtMsg->hdr.msgTypeVersion);
		break;

	case SSmsRealtimeMsg::RT_IQ_DATA:
		m_versionMap[rtMsg->hdr.msgType] = std::min(4ul, rtMsg->hdr.msgTypeVersion);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send a message to all connected clients
//
void CRealtimeNet::SendToAllClients(_In_ const SSmsRealtimeMsg* msg)
{
	ASSERT(GetMode() == SERVER);
//	TRACE("SendToAllClients %lu %lu %lu\n", msg->hdr.msgType, msg->hdr.msgTypeVersion, msg->hdr.bodySize);
	ForEachConnection(&CRealtimeNet::SendMsg, const_cast<SSmsRealtimeMsg*>(msg));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send a message to a client
//
void CRealtimeNet::SendMsg(_In_ void* arg)
{
	ASSERT(GetMode() == DYNAMIC);
	const SSmsRealtimeMsg* msg = static_cast<const SSmsRealtimeMsg*>(arg);

	// Remember active DF V1 tasks
	if (msg->hdr.msgType == SSmsRealtimeMsg::RT_DF_START)
	{
		unsigned long dfDataVersion = 1;
		VersionMap::const_iterator version = m_versionMap.find(SSmsRealtimeMsg::RT_DF_DATA);

		if (version != m_versionMap.end())
		{
			dfDataVersion = version->second;
		}

		if (dfDataVersion == 1)
		{
			m_activeDfV1Set.insert(msg->body.start.taskId);
		}
	}
	else if (msg->hdr.msgType == SSmsRealtimeMsg::RT_DF_STOP)
	{
		m_activeDfV1Set.erase(msg->body.stop.taskId);
	}

	unsigned long versionRequested = (msg->hdr.msgType == SSmsRealtimeMsg::RT_IQ_DATA ? 0 : 1);
	VersionMap::const_iterator version = m_versionMap.find(msg->hdr.msgType);

	if (version != m_versionMap.end())
	{
		versionRequested = version->second;
	}

	// Don't send any data if the version doesn't match
	// Don't send spectrum data during DF if the DF_DATA version is 1 (which includes spectrum data)
	if (versionRequested == msg->hdr.msgTypeVersion &&
		(msg->hdr.msgType != SSmsRealtimeMsg::RT_SPECTRUM || m_activeDfV1Set.find(msg->body.spectrum.taskId) == m_activeDfV1Set.end()))
	{
		Send(msg, offsetof(SSmsRealtimeMsg, body) + msg->hdr.bodySize);
	}

	return;
}

