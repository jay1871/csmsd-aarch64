/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include <vector>
#include "Ne10Vec.h"

#include "StatsNoHist.h"

class CStats
{
public:
	CStats(void);
	~CStats(void);
	void AddValue(double value);
	void Clear(void);
	unsigned int GetCount(void) const { return m_plainStats.GetCount(); }
	double GetHistBinSize(void) { return m_hist.size() > 0 ? (m_histHiValue - m_histLoValue) / m_hist.size() : 0; }
	size_t GetHistNumBins(void) { return m_hist.size(); }
	double GetMax(void) const { return m_plainStats.GetMax(); }
	double GetMean(void ) const { return m_plainStats.GetMean(); }
	double GetMin(void) const { return m_plainStats.GetMin(); }
	template<size_t N> void GetProbDist(unsigned char (&dist)[N]) const;
	double GetRms(void) const { return m_plainStats.GetRms(); }
	double GetStdDev(void) const { return m_plainStats.GetStdDev(); }
	double GetVar(void) const { return m_plainStats.GetVar(); }
	static void LeastSquaresFit(const Ne10F32Vec& data, ne10_float32_t& slope, ne10_float32_t& intercept);
	static ne10_float32_t Percentile(const Ne10F32Vec& data, float percentile);
	void SetHist(double loValue, double hiValue, unsigned int numBins);

private:
	// Data
	std::vector<unsigned int> m_hist;
	double m_histHiValue;
	double m_histLoValue;
	CStatsNoHist m_plainStats;
};


//////////////////////////////////////////////////////////////////////
//
// Get probability distribution
//
template<size_t N> void CStats::GetProbDist(unsigned char (&dist)[N]) const
{
	ASSERT(m_hist.size() <= N);

	if (m_hist.size() <= N)
	{
		std::vector<float> temp(m_hist.size());
		temp[0] = float(m_hist[0]);

		for (size_t bin = 1; bin < m_hist.size(); ++bin)
		{
			temp[bin] = temp[bin - 1] + m_hist[bin];
		}

		if (temp.back() > 0)
		{
			for (size_t bin = 0; bin < m_hist.size(); ++bin)
			{
				dist[bin] = static_cast<unsigned char>(100 * (1 - temp[bin] / temp.back()));
			}
		}
		else
		{
			memset(&dist[0], 100, m_hist.size());
		}
	}

	return;
}
