/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "2630.h"
#include "DfCtrlNet.h"
#include "Log.h"
#include "NetConnection.h"
#include "ProcessorNode.h"
#include "VCPCtrlNet.h"

const char* CProcessorNode::PORT = "3304";
const char* CProcessorNode::PORT2 = "3305";

//bool CProcessorNode::m_useSimulatorForSlave = false;
//bool CProcessorNode::m_useExternalClock = false;

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CProcessorNode::CProcessorNode(void) :
	m_config(),
	m_processorNodes(m_config->GetProcessorNodes()),
	m_tag(0)
{

}


//////////////////////////////////////////////////////////////////////
//
// Denstructor
//
CProcessorNode::~CProcessorNode(void)
{
	// Delete clients
	m_nodeClients.clear();

	// Delete server
	m_dfServer.reset();

	// Close and flush the tagged df queue
	m_dfDataQueue.taggedDfDataQ.Close();
	size_t c2 = m_dfDataQueue.taggedDfDataQ.Flush();
	if (c2 > 0)
	{
		TRACE("CProcessorNode flushed %u items from tagged df queue\n", c2);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Check if df master and all nodes have data
//
void CProcessorNode::AddSlaveDfData(const SDfCtrlMsg* msg, const char* /*peername*/)
{
	auto& r = msg->body.slaveGetSamplesResp;

	std::lock_guard<std::mutex> lock(m_dfDataQueue.dfDataMutex);
	if (!m_dfDataQueue.dfData)
	{
//		TRACE("Initializing m_dfDataQueue.dfData\n");
//		CLog::Log(CLog::INFORMATION, "rcvd initial DF_GET_SAMPLES_RESP tag=%lu from %s", msg->hdr.sourceAddr, peername);	// Verbose
		m_dfDataQueue.dfData.reset(new NTaggedQueue::SDfData);	// allocate a new structure, shared by all nodes
		m_dfDataQueue.dfData->tag = msg->hdr.sourceAddr;
		m_dfDataQueue.dfData->status = NTaggedQueue::SDfData::EStatus(r.status);
		if ((r.dataTypes & SDfCtrlMsg::SlaveDataTypes::IQ) != 0)
		{
			m_dfDataQueue.dfData->iq.resize(r.totalAnts);
		}
		if ((r.dataTypes & SDfCtrlMsg::SlaveDataTypes::PSD) != 0)
		{
			m_dfDataQueue.dfData->watts.resize(r.totalAnts);
		}
		if ((r.dataTypes & SDfCtrlMsg::SlaveDataTypes::FFT) != 0)
		{
			m_dfDataQueue.dfData->volts.resize(r.totalAnts);
		}
		for (unsigned char i = 0; i < r.totalAnts; ++i)
			m_dfDataQueue.dfData->remainingAnts.insert(i);
	}

	if (m_dfDataQueue.dfData->tag != msg->hdr.sourceAddr)
	{
		TRACE("SLAVE_GET_SAMPLES_RESP tag mismatch %lu %lu\n", m_dfDataQueue.dfData->tag, msg->hdr.sourceAddr);
	}
	if (r.status == SDfCtrlMsg::FAILURE && m_dfDataQueue.dfData->status == NTaggedQueue::SDfData::EStatus::SUCCESS)
	{
		TRACE("SLAVE_GET_SAMPLES_RESP changing status to FAILURE\n");
		m_dfDataQueue.dfData->status = NTaggedQueue::SDfData::EStatus::FAILURE;
	}

	auto src = r.data;
	if ((r.dataTypes & SDfCtrlMsg::SlaveDataTypes::IQ) != 0 && r.antNum < m_dfDataQueue.dfData->iq.size())
	{
		m_dfDataQueue.dfData->iq[r.antNum].assign(src, &src[r.numIq]);
		src += r.numIq;
	}
	if ((r.dataTypes & SDfCtrlMsg::SlaveDataTypes::PSD) != 0 && r.antNum < m_dfDataQueue.dfData->watts.size())
	{
		m_dfDataQueue.dfData->watts[r.antNum].assign(reinterpret_cast<const float*>(src), r.numPoints);
		src += r.numPoints;
	}
	if ((r.dataTypes & SDfCtrlMsg::SlaveDataTypes::FFT) != 0 && r.antNum < m_dfDataQueue.dfData->volts.size())
	{
		m_dfDataQueue.dfData->volts[r.antNum].assign(reinterpret_cast<const float*>(src), r.numPoints);
	}
	m_dfDataQueue.dfData->remainingAnts.erase(r.antNum);

	if (m_dfDataQueue.dfData->remainingAnts.empty())
	{
//		CLog::Log(CLog::INFORMATION, "rcvd last DF_GET_SAMPLES_RESP tag=%lu from %s", msg->hdr.sourceAddr, peername);	// Verbose
		if (!m_dfDataQueue.taggedDfDataQ.Enqueue(m_dfDataQueue.dfData))	// Move the dfdata to the queue
		{
			m_dfDataQueue.dfData.reset();
		}
//		TRACE("after enqueue, m_dfDataQueue.dfData contains %p\n", m_dfDataQueue.dfData.get());
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Check if df master and all nodes have bist results
//
bool CProcessorNode::AreNodesBistDone(const std::vector<size_t>& sentIndex, std::vector<CBist::SBistResult>& allResults)
{
	allResults.clear();
	if (!IsProcessorDfMaster())
		return false;

	for (size_t j = 0; j < sentIndex.size(); ++j)
	{
		auto i = sentIndex[j];
		if (i < m_nodeClients.size())
		{
			auto node = m_nodeClients[i];
			if (!node || !node->IsBistDone())
			{
				return false;
			}
			node->AddBistResults(allResults);
		}
	}
	return true;
}

//////////////////////////////////////////////////////////////////////
//
// Check if df master and all nodes are ready and on external clock
//
bool CProcessorNode::AreNodesReady(void)
{
	if (!IsProcessorDfMaster())
		return false;

	if (m_nodeClients.empty())
		return false;

	for (size_t i = 0; i < m_nodeClients.size(); ++i)
	{
		auto node = m_nodeClients[i];
		if (!node || !node->UsingExternalClock())
		{
			return false;
		}
	}
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Do Df Master startup if configured to be
//
void CProcessorNode::DoDfMaster(void)
{
	if (m_processorNodes.nodeId != 0 || m_processorNodes.nodes.empty())
	{
#ifdef CSMS_2016
		if ( (C2630::GetrxProcessorStatic() < 41 && C2630::GetFirmwareVersionStatic() > 0x1060015) ||  (C2630::GetrxProcessorStatic() >= 41 && C2630::GetFirmwareVersionStatic() > 0x02030000))
		{
			// set to non master state on radio if it's set to df master
			if (C2630::ReadMasterStateStatic())
			{
				if (C2630::SetFlashLockStatic(0)) // unlock flash status
				{
					if (!C2630::SetMasterStateStatic(0))
						printf("CProcessorNode:: failed to set radio to non-df master state\n");
				}
				C2630::SetFlashLockStatic(1); // always lock it to prevent corrupting flash
			}
			else
				printf("radio not at df master state\n");
		}
		printf("CProcesorNode: this csmsd is not the df master\n");
#endif
		return;
	}

#ifdef CSMS_2016
	if ( (C2630::GetrxProcessorStatic() < 41 && C2630::GetFirmwareVersionStatic() > 0x1060015) ||  (C2630::GetrxProcessorStatic() >= 41 && C2630::GetFirmwareVersionStatic() > 0x02030000))
	{
		// set to master state on radio if it's not
		if (!C2630::ReadMasterStateStatic())
		{
			if (C2630::SetFlashLockStatic(0)) // unlock flash status
			{
				if (!C2630::SetMasterStateStatic(1))
					printf("CProcessorNode:: failed to set radio to df master state\n");
			}
			C2630::SetFlashLockStatic(1); // always lock it to prevent corrupting flash
		}
		else
			printf("radio at df master state\n");
	}
#endif

	// Make sure that our external clock is set to 100 MHz
	C2630::EExternalRef setRef;
	if (!C2630::SetExternalReferenceStatic(C2630::EExternalRef::EXT100MHZ, setRef))
	{
		printf("CProcessorNode:: failed to set external ref to 100 MHz\n");
	}

	// Create network server
	printf(">>>Creating dfctrl server for node %lu\n", m_processorNodes.nodeId);
	static const size_t maxTries = 10;
	for (size_t tries = 0; tries < maxTries; ++tries)
	{
		try
		{
			//m_dfServer = std::make_shared<CDfCtrlNet>(this, m_processorNodes.nodeId, PORT2);
			m_dfServer = std::make_shared<CDfCtrlNet>(this, m_processorNodes.nodeId, PORT);
			break;
		}
		catch (CDfCtrlNet::CNetError& e)
		{
			CLog::Log(CLog::ERRORS, "Unable to create Df Control server(%u) for node %lu - retrying\n", tries, m_processorNodes.nodeId);
			sleep(15);
		}
	}
	if (!m_dfServer)
	{
		throw std::runtime_error("Unable to create Df control server for node");
	}

	// Create network client connections to the slaves
	// Note that m_nodeClients[i] will be empty shared ptr if client ctor fails
	m_nodeClients.resize(m_processorNodes.nodes.size());

	// below can trigger a bist before "valid" master client connection if slave does not reboot following ext clock switch now
	/*CDfCtrlNet::(false);
	for (size_t i = 0; i < m_processorNodes.nodes.size(); ++i)
	{
		printf(">>>Creating connection to node %lu @ %s\n", m_processorNodes.nodes[i].id, m_processorNodes.nodes[i].ipAddress.c_str());
		try
		{
			m_nodeClients[i] = std::make_shared<CDfCtrlNet>(this, m_processorNodes.nodes[i], m_processorNodes.nodeId, PORT);
		}
		catch (CDfCtrlNet::CNetError& e)
		{
			CLog::Log(CLog::ERRORS, "Unable to create client to connect with node %lu @ %s\n",
				m_processorNodes.nodes[i].id, m_processorNodes.nodes[i].ipAddress.c_str());
		}
	}*/


	return;
}


//////////////////////////////////////////////////////////////////////
//
// Become Df Slave startup if configured to be
//
void CProcessorNode::DoDfSlave(void)
{
	if (m_processorNodes.nodeId == 0)
	{
		printf("CProcesorNode: this csmsd is not a df slave\n");
		return;
	}


#ifdef CSMS_2016
	if ( (C2630::GetrxProcessorStatic() < 41 && C2630::GetFirmwareVersionStatic() > 0x1060015) ||  (C2630::GetrxProcessorStatic() >= 41 && C2630::GetFirmwareVersionStatic() > 0x02030000))
	{
		// set to non master state on radio if it's set to df master
		if (C2630::ReadMasterStateStatic())
		{
			if (C2630::SetFlashLockStatic(0)) // unlock flash status
			{
				if (!C2630::SetMasterStateStatic(0))
					printf("CProcessorNode:: failed to set radio to non-df master state\n");
			}
			C2630::SetFlashLockStatic(1); // always lock it to prevent corrupting flash
		}
		else
			printf("radio not at df master state\n");
	}
#endif
	// Create network server
	printf(">>>Creating dfctrl server for node %lu\n", m_processorNodes.nodeId);
	static const size_t maxTries = 5;
	// Instantiate the VCP control server
	for (size_t tries = 0; tries < maxTries; ++tries)
	{
		try
		{
			m_vcpServer = std::make_shared<CVCPCtrlNet>("3303");
			break;
		}
		catch(CVCPCtrlNet::CNetError& e)
		{
			CLog::Log(CLog::ERRORS, "Unable to create VCP server(%u) for node %lu - retrying\n", tries, m_processorNodes.nodeId);
			sleep(15);
		}
	}
	if (!m_vcpServer)
	{
		throw std::runtime_error("Unable to create VCP server for node");
	}

	// Instantiate the DF control server
	for (size_t tries = 0; tries < maxTries; ++tries)
	{
		try
		{
			m_dfServer = std::make_shared<CDfCtrlNet>(this, m_processorNodes.nodeId, PORT);
			break;
		}
		catch (CDfCtrlNet::CNetError& e)
		{
			CLog::Log(CLog::ERRORS, "Unable to create Df Control server(%u) for node %lu - retrying\n", tries, m_processorNodes.nodeId);
			sleep(15);
		}
	}
	if (!m_dfServer)
	{
		throw std::runtime_error("Unable to create Df control server for node");
	}


	// Create network client connectio to the master
	m_nodeClients.resize(1);
	CConfig::SProcessorNodes::SNode masterNode;
	masterNode.id = 0;
	masterNode.ipAddress = m_processorNodes.masterIP;
	printf(">>>Creating connection to node %lu @ %s\n", masterNode.id, masterNode.ipAddress.c_str());
	try
	{
		//m_nodeClients[0] = std::make_shared<CDfCtrlNet>(this, masterNode, m_processorNodes.nodeId, PORT2);
		m_nodeClients[0] = std::make_shared<CDfCtrlNet>(this, masterNode, m_processorNodes.nodeId, PORT);
	}
	catch (CDfCtrlNet::CNetError& e)
	{
		CLog::Log(CLog::ERRORS, "Unable to create client to connect with node %lu @ %s\n",
			masterNode.id, masterNode.ipAddress.c_str());
	}


	return;
}


//////////////////////////////////////////////////////////////////////
//
// Check if df master and nodes has fault response
//
bool CProcessorNode::IsNodeFaultDone(size_t nodeIndex, SDfCtrlMsg::SGetSlaveCsmsFaultResp& resp)
{
	if (!IsProcessorDfMaster())
		return false;

	if (nodeIndex >= m_nodeClients.size())
		return false;

	auto node = m_nodeClients[nodeIndex];
	if (!node)
		return false;

	return node->GetFaultResp(resp);
}

//////////////////////////////////////////////////////////////////////
//
// Check if df master and nodes has set audio params response
//
bool CProcessorNode::IsSetAudioParamsDone(size_t nodeIndex, SDfCtrlMsg::SSetSlaveAudioParamsResp& resp)
{
	if (!IsProcessorDfMaster())
		return false;

	if (nodeIndex >= m_nodeClients.size())
		return false;

	auto node = m_nodeClients[nodeIndex];
	if (!node)
		return false;

	return node->GetSlaveAudioParamsResp(resp);
}

void CProcessorNode::ConnectMasterAsClient()
{
	// Create network client connections to the slaves if not already created
	// Note that m_nodeClients[i] will be empty shared ptr if client ctor fails

	if (m_processorNodes.nodeId == 0)
	{
		for (size_t i = 0; i < m_processorNodes.nodes.size(); ++i)
		{
			CLog::Log(CLog::INFORMATION, "Establishing master connection with client after server detect slave client connecting\n");
			try
			{
				m_nodeClients[i].reset();
				m_nodeClients[i] = std::make_shared<CDfCtrlNet>(this, m_processorNodes.nodes[i], m_processorNodes.nodeId, PORT);
			}
			catch (CDfCtrlNet::CNetError& e)
			{
				CLog::Log(CLog::ERRORS, "Unable to create client to connect with node %lu @ %s\n",
					m_processorNodes.nodes[i].id, m_processorNodes.nodes[i].ipAddress.c_str());
			}
		}
	}

}

//////////////////////////////////////////////////////////////////////
//
// Ping all nodes (no response)
//
void CProcessorNode::PingAllNodes(void)
{
	for (size_t i = 0; i < m_processorNodes.nodes.size(); ++i)
	{		// Ping a the remote node
		auto node = m_nodeClients[i];
		if (node) node->Ping();
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send a message to one node, translate if necessary
//
bool CProcessorNode::Send(CDfCtrlNet* client, const SDfCtrlMsg& ctrlMsg, const char* logStr, bool logSuccess)
{
	bool sent = CDfCtrlNet::Send(client, ctrlMsg);
	if (logStr != nullptr)
	{
		if (!sent)
		{
			CLog::Log(CLog::INFORMATION, "send failed %s", logStr);
		}
		else if (logSuccess)
		{
			CLog::Log(CLog::INFORMATION, "sent %s", logStr);

		}
	}
	return sent;
}


//////////////////////////////////////////////////////////////////////
//
// Send a message to all slave nodes
//
unsigned long CProcessorNode::SendToAllNodes(const SDfCtrlMsg& msg, std::vector<size_t>& sentIndex, bool tagged)
{
	sentIndex.clear();
	if (tagged)
	{
		std::lock_guard<std::mutex> lock(m_tagLock);
		++m_tag;
		if (m_tag == 0) m_tag = 1;
		const_cast<SDfCtrlMsg&>(msg).hdr.sourceAddr = m_tag;
	}
	for (size_t i = 0; i < m_processorNodes.nodes.size(); ++i)
	{
		// Send a message to the remote node
		auto node = m_nodeClients[i];

#ifdef CSMS_DEBUG
		bool connected = node && node->IsConnected();
		bool sendRetVal = false;

		if(connected)
		{
			sendRetVal = node->Send(&msg, offsetof(SDfCtrlMsg, body) + msg.hdr.bodySize);
		}

		if (sendRetVal)
		{
			sentIndex.push_back(i);
		}
		else
		{
			TRACE("CProcessorNode::SendToAllNodes No index node %d, connected %d, send %d\n ",
					(node ? 1 :0), (connected ? 1:0), (sendRetVal ? 1:0));
		}
#else
		if (node && node->IsConnected() && node->Send(&msg, offsetof(SDfCtrlMsg, body) + msg.hdr.bodySize))
		{
			sentIndex.push_back(i);
		}
#endif
	}
	return msg.hdr.sourceAddr;
}


//////////////////////////////////////////////////////////////////////
//
// Send a message to all slave nodes
//
int CProcessorNode::SendBistToAllNodes(std::vector<size_t>& sentIndex)
{
	sentIndex.clear();
	int numSent = 0;
	for (size_t i = 0; i < m_processorNodes.nodes.size(); ++i)
	{
		// Send a do_bist to the remote node
		auto node = m_nodeClients[i];
		if (node && node->SendDoBist())
		{
			sentIndex.push_back(i);
			++numSent;
		}
	}
	return numSent;
}

int CProcessorNode::SendGetFaultToAllNodes(std::vector<size_t>& sentMask)
{
	sentMask.resize(m_processorNodes.nodes.size());
	int numSent = 0;
	for (size_t i = 0; i < m_processorNodes.nodes.size(); ++i)
	{
		// Send a get_csms_fault to the remote node
		auto node = m_nodeClients[i];
		if (node && node->SendGetFault())
		{
			sentMask[i] = 1;
			++numSent;
		}
		else
		{
			sentMask[i] = 0;
		}
	}
	return numSent;
}

int CProcessorNode::SendSetAudioParamsToAllNodes(std::vector<size_t>& sentMask, const C3230::SDecimation& decimations, const SEquipCtrlMsg::SAudioParamsCmd& audiocmd, const CNetConnection<void>* audiosource)
{
	sentMask.resize(m_processorNodes.nodes.size());
	int numSent = 0;
	for (size_t i = 0; i < m_processorNodes.nodes.size(); ++i)
	{
		// Send a get_csms_fault to the remote node
		auto node = m_nodeClients[i];
		if (node && node->SendSetAudioParams(decimations, audiocmd, audiosource))
		{
			sentMask[i] = 1;
			++numSent;
		}
		else
		{
			sentMask[i] = 0;
		}
	}
	return numSent;
}

int CProcessorNode::FreeSlaveAudioChannelToAllNodes(std::vector<size_t>& sentMask, unsigned long channelnum, bool freeall, const CNetConnection<void>* audiosource)
{
	sentMask.resize(m_processorNodes.nodes.size());
	int numSent = 0;
	for (size_t i = 0; i < m_processorNodes.nodes.size(); ++i)
	{
		// Send a get_csms_fault to the remote node
		auto node = m_nodeClients[i];
		if (node && node->SendFreeSlaveAudioChannel(channelnum, freeall, audiosource))
		{
			sentMask[i] = 1;
			++numSent;
		}
		else
		{
			sentMask[i] = 0;
		}
	}
	return numSent;
}


//static int nloop = 0;
//////////////////////////////////////////////////////////////////////
//
// Set a message to the master node
//
bool CProcessorNode::SendToMaster(const SDfCtrlMsg& ctrlMsg, const char* logStr, bool logSuccess)
{
	if (m_processorNodes.nodeId == 0)
	{
		printf("CProcesorNode::SendToMaster: this csmsd is not a df slave\n");
		return false;
	}
	if (m_nodeClients.empty())
	{
		printf("CProcesorNode::SendToMaster: no client available\n");
		return false;
	}
	auto node = m_nodeClients[0];
	if (!node || !node->IsConnected())
	{
		printf("CProcesorNode::SendToMaster: no connection to master\n");
		return false;
	}
//#if CSMS_TIMING == 1
//	auto startTime = std::chrono::steady_clock::now();
//#endif
	bool sent = node->Send(&ctrlMsg, offsetof(SDfCtrlMsg, body) + ctrlMsg.hdr.bodySize);
//#if CSMS_TIMING == 1
//	auto endTime = std::chrono::steady_clock::now();
//	auto diffTime = endTime - startTime;
//	auto micro = std::chrono::duration<double, std::micro>(diffTime).count();
//	if ((nloop++ % 10) == 0)
//		printf("send to master took %f microseconds\n", micro);
//#endif
	if (logStr != nullptr)
	{
		auto peer = node->GetPeerAddress();
		LPCTSTR peerName(peer == nullptr ? _T("Unknown") : peer->ai_canonname);

		if (!sent)
		{
			CLog::Log(CLog::INFORMATION, "send failed %s to %s", logStr, peerName);
		}
		else if (logSuccess)
		{
			CLog::Log(CLog::INFORMATION, "sent %s to %s", logStr, peerName);
		}
	}
	return sent;
}

//////////////////////////////////////////////////////////////////////
//
// Get the startup done status dfctrlnet
//
bool CProcessorNode::GetStartupDoneState(void)
{
	return CDfCtrlNet::GetStartupDoneState();
}


//////////////////////////////////////////////////////////////////////
//
// Set the startup done status dfctrlnet
//
void CProcessorNode::SetStartupDone(void)
{
	CDfCtrlNet::SetStartupDone();
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for all nodes to respond to request with tag
//
bool CProcessorNode::WaitForAllResponses(const std::vector<size_t>& sentIndex, unsigned long tag,
	std::vector<SDfCtrlMsgPtr>& responses)
{
	if (!IsProcessorDfMaster())
		return false;

	bool rv = true;
	responses.resize(sentIndex.size());
	for (size_t j = 0; j < sentIndex.size(); ++j)
	{
		auto i = sentIndex[j];
		if (i < m_nodeClients.size())
		{
			auto node = m_nodeClients[i];
			if (!node)
			{
				rv = false;
			}
			else
			{
				if (!node->DequeueTaggedResponse(tag, responses[j]))
				{
					rv = false;
				}
			}
		}
	}
	return rv;
}

bool CProcessorNode::WaitForAllResponses(const std::vector<size_t>& sentIndex, unsigned long tag,
	SDfDataPtr& responses)
{
	if (!IsProcessorDfMaster() || sentIndex.empty())
		return false;

	return m_dfDataQueue.taggedDfDataQ.Dequeue(tag, responses, 7000);
}

