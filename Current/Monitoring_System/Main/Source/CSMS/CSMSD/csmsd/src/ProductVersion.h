/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

namespace VersionInfo
{
	static constexpr const char* Comments = "Controls the TCI Compact Spectrum Monitoring System";
	static constexpr const char* FileDescription = "TCI Compact Spectrum Monitoring System Daemon";
#ifdef CSMS_2016
	static constexpr const char* FileVersion = "691.2.3.1";
#else
	static constexpr const char* FileVersion = "691.1.12.0";
#endif
	static constexpr const char* LegalCopyright = "Copyright (C) 2015-2018 TCI International, Inc.";
	static constexpr const char* OriginalFilename = "csmsd.elf";
	static constexpr const char* ProductName = "CSMS";
#ifdef CSMS_DEBUG
 #ifdef CSMS_2016
	static constexpr const char* ProductVersion = "T0691-MM-02CX";
 #else
	static constexpr const char* ProductVersion = "T0691-MM-01LX";
 #endif
#else
 #ifdef CSMS_2016
	static constexpr const char* ProductVersion = "T0691-MM-02C";
#else
	static constexpr const char* ProductVersion = "T0691-MM-01L";
#endif
#endif
}
