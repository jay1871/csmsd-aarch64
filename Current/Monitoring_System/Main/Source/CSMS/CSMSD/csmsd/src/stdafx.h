/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#ifndef STDAFX_H_
#define STDAFX_H_

#include <algorithm>
#include <arpa/inet.h>
#include <atomic>
#include <cassert>
#include <cctype>
#include <cerrno>
#include <chrono>
#include <cmath>
#include <condition_variable>
#include <cstdarg>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <cwchar>
#include <exception>
#include <fcntl.h>
#include <float.h>
#include <fstream>
#include <functional>
#include <ifaddrs.h>
#include <initializer_list>
#include <iomanip>
#include <iostream>
#include <limits.h>
#include <map>
#include <memory>
#include <mutex>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <pthread.h>
#include <queue>
#include <set>
#include <signal.h>
#include <sstream>
#include <string>
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/ioctl.h>
#include <sys/reboot.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#include <system_error>
#include <thread>
#include <time.h>
#include <type_traits>
#include <unistd.h>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include "MSCompat.h" // For shared components

namespace TCIPaths
{
	const char* const dir = "/media/tci/csms/";
	const char* const configDir = "/media/tci/csms/etc/";
	const char* const dataDir = "/media/tci/csms/data/";
	const char* const logDir = "/media/tci/csms/log/";
}

#endif
