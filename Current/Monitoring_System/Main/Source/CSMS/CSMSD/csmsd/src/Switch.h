/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2017 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

class CSwitch
{
public:
	// Types
	enum class ESwitchType : uint32_t { NO_SWITCH, SW_7234_REF, SW_7234_MON, SW_7236_REF, SW_7236_MON };

	// Types
	typedef std::map<Units::Frequency, float> FreqDepParams;

	// Functions
	virtual ~CSwitch(void) {}

	static ESwitchType StrToEnum(const std::string& str)
	{
#define SW_STR_TO_ENUM(X) if (str == #X) return ESwitchType::X
		SW_STR_TO_ENUM(SW_7234_REF);
		SW_STR_TO_ENUM(SW_7234_MON);
		SW_STR_TO_ENUM(SW_7236_REF);
		SW_STR_TO_ENUM(SW_7236_MON);
		return ESwitchType::NO_SWITCH;
#undef SW_STR_TO_ENUM
	}
	static std::string EnumToStr(const ESwitchType& e)
	{
#define SW_ENUM_TO_STR(X) if (e == ESwitchType::X) return #X
		SW_ENUM_TO_STR(SW_7234_REF);
		SW_ENUM_TO_STR(SW_7234_MON);
		SW_ENUM_TO_STR(SW_7236_REF);
		SW_ENUM_TO_STR(SW_7236_MON);
		return "None";
#undef SW_ENUM_TO_STR
	}
	static CSwitch* Create(const ESwitchType& e);
	static FreqDepParams& GetSwitchGains(CSwitch* p, bool low) { return (p ? p->GetSwitchGains(low) : m_emptyGains); }
	static const std::string& GetName(const CSwitch* p) { return (p ? p->m_name : m_none); }

protected:
	// Functions
	CSwitch(void)
	{

	}

	// Data
	std::string m_name;

private:
	// Constants

	// Types

	// Functions
	FreqDepParams& GetSwitchGains(bool low)
	{
		return (low ? m_switchLfGains : m_switchGains);
	}


	// Data
	static const std::string m_none;
	static FreqDepParams m_emptyGains;
	FreqDepParams m_switchGains;
	FreqDepParams m_switchLfGains;
};

__attribute__((weak)) CSwitch::FreqDepParams CSwitch::m_emptyGains;
const __attribute__((weak)) std::string CSwitch::m_none = "None";

class C7234_ref_sw : public CSwitch
{
public:
	C7234_ref_sw(void) : CSwitch()
	{
		m_name = "7234_REF";
	}
	~C7234_ref_sw(void) {};
};

class C7234_mon_sw : public CSwitch
{
public:
	C7234_mon_sw(void) : CSwitch()
	{
		m_name = "7234_MON";
	}
	~C7234_mon_sw(void) {};
};

class C7236_ref_sw : public CSwitch
{
public:
	C7236_ref_sw(void) : CSwitch()
	{
		m_name = "7236_REF";
	}
	~C7236_ref_sw(void) {};
};

class C7236_mon_sw : public CSwitch
{
public:
	C7236_mon_sw(void) : CSwitch()
	{
		m_name = "7236_MON";
	}
	~C7236_mon_sw(void) {};
};


__attribute__((weak)) CSwitch* CSwitch::Create(const ESwitchType& e)
{
	switch (e)
	{
	case ESwitchType::SW_7234_REF: return new C7234_ref_sw;
	case ESwitchType::SW_7234_MON: return new C7234_mon_sw;
	case ESwitchType::SW_7236_REF: return new C7236_ref_sw;
	case ESwitchType::SW_7236_MON: return new C7236_mon_sw;
	default: return nullptr;
	}
}

