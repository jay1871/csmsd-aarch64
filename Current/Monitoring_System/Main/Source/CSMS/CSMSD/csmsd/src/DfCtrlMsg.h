/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "ErrorCodes.h"
#include "SmsMsg.h"
#include "3230.h"
#include <cassert>
#include <cstddef>
#include "MSCompat.h"
#include "NetConnection.h"

struct SDfCtrlMsg
:
	SSmsMsg
{
	// Message subtypes for message type INTERNAL_DF_CTRL
	enum EInternalDfCtrl : unsigned long
	{
		SET_EXTERNAL_CLOCK		= 1,
		SET_EXTERNAL_CLOCK_RESP	= 2,
		DO_BIST					= 3,
		DO_BIST_RESP			= 4,
		GET_CSMS_FAULT			= 11,
		GET_CSMS_FAULT_RESP		= 12,
		CCDF_COLLECT			= 15,
		CCDF_COLLECT_RESP		= 16,
		PING					= 17,
		SLAVE_TUNE				= 18,
		SLAVE_TUNE_RESP			= 19,
		SLAVE_SETUP_COLLECT		= 20,
		SLAVE_SETUP_COLLECT_READY = 21,
		SLAVE_SETUP_COLLECT_RESP = 22,
		SLAVE_GET_SAMPLES		= 23,
		SLAVE_GET_SAMPLES_RESP	= 24,
		DO_INITIAL_AGC			= 25,
		DO_INITIAL_AGC_RESP		= 26,
		SET_SLAVE_AUDIO_PARMS	= 27,
		SET_SLAVE_AUDIO_PARMS_RESP	= 28,
		FREE_SLAVE_AUDIO		= 29,
		FREE_SLAVE_AUDIO_RESP	= 30
	};

	enum EStatus : unsigned long { SUCCESS, FAILURE };
	enum EGainMode : unsigned char { NORMAL, RURAL, URBAN, CONGESTED };
	enum SlaveDataTypes : unsigned char { IQ = 0x01, PSD = 0x02, FFT = 0x04 };
	static int GetDataSize(SlaveDataTypes types, int numIq, int numBins)
	{
		int size = 0;
		if ((types & SDfCtrlMsg::SlaveDataTypes::IQ) != 0) size += numIq;
		if ((types & SDfCtrlMsg::SlaveDataTypes::PSD) != 0) size += numBins;
		if ((types & SDfCtrlMsg::SlaveDataTypes::FFT) != 0) size += 2 * numBins;
		return size;
	}

	// Message version data
	static const SVersionData VERSION_DATA[];

	// store slave ip address
	static char slaveipaddress[16];


	// Message structures
	// SET_EXTERNAL_CLOCK_RESP
	struct SSetExternalClockResp
	{
		static const int MAX_VERLEN = 32;
		enum EClockStatus : unsigned long { UNKNOWN, SUCCESS, UNABLE, ALREADY };
		EClockStatus status;
		char softwarever[MAX_VERLEN];
	};

	// DO_BIST
	struct SDoBist
	{
		enum EBistType : unsigned long { LAST_RESULT, LAST_DIAGNOSTICS, PERFORM_BIST, PERFORM_DIAGNOSTICS };
		EBistType bistType;
	};

	// DO_BIST_RESP
	struct SDoBistResp
	{
		static const int MAX_TESTLEN = 32;
		static const int MAX_TEXTLEN = 256;
		SGetBistResp::EResult result;
		unsigned short testLen;
		unsigned short textLen;
		bool last; // Last message of complete BIST result
		bool send;	// flag to force send result to client
		char16_t test[MAX_TESTLEN];	// Fixed-length test string
		char16_t text[MAX_TEXTLEN];	// Variable length text string
	};

	struct SDecimations
	{
		bool operator==(const SDecimations& other) const
		{
			return (zifCICDecimation == other.zifCICDecimation && zifFIRDecimation == other.zifFIRDecimation &&
				upResample == other.upResample && downResample == other.downResample &&
				ddcCICDecimation == other.ddcCICDecimation && ddcFIRDecimation == other.ddcFIRDecimation);
		}
		unsigned long zifCICDecimation;
		unsigned long zifFIRDecimation;
		unsigned long upResample;
		unsigned long downResample;
		unsigned long ddcCICDecimation;
		unsigned long ddcFIRDecimation;
	};

	// SLAVE_TUNE
	struct SSlaveTune
	{
		bool operator==(const SSlaveTune& other) const
		{
			if (tuneDigitizer != other.tuneDigitizer)
				return false;

			bool dig = true;
			if (tuneDigitizer)
			{
				dig = decimations == other.decimations && procOffset == other.procOffset;
			}
			return (rfFreq == other.rfFreq && rxBw == other.rxBw && procBw == other.procBw && hf == other.hf &&
				bandSelect == other.bandSelect && ant == other.ant && dig);
		}
		Units::Frequency::Raw rfFreq;
		Units::Frequency::Raw rxBw;
		Units::Frequency::Raw procBw;
		bool hf;
		unsigned char bandSelect;
		SSmsMsg::EAnt ant;
		bool tuneDigitizer;
		SDecimations decimations;
		Units::Frequency::Raw procOffset;
	};

	// SLAVE_TUNE_RESP
	struct SSlaveTuneResp
	{
		EStatus status;
		Units::Frequency::Raw finalIfFreq;
		Units::Frequency::Raw freqLimits[2];
		Units::Frequency::Raw radioFreq;
		Units::Frequency::Raw procFreq;
		unsigned long band;
		bool inverted;
		bool direct;
	};

	// SLAVE_SETUP_COLLECT
	struct SSlaveSetupCollect
	{
		static const unsigned char MAX_ANTS = 15;

		Units::Frequency::Raw midFrequency;	// midpoint of spectrum data to be used	[offset: 0]
		unsigned long count;	// No. of sample to collect                         [offset: 8]
		unsigned long adcCount;	// No. of a/d samples for threshold counters       	[offset:12]
		unsigned long delay;	// Delay before collecting (clocks)					[offset:16]
		unsigned long fftDelay;	// Delay to make fft_done interrupt constant time	[offset:20]
		unsigned long numDfBins;	//												[offset:24]
		SlaveDataTypes dataTypes;	//												[offset:28]
		bool termRadio;	//															[offset:29]
		unsigned char atten;	//													[offset:30]
		EGainMode gainMode;	//														[offset:31]
		//Number of antennas to collect from: PbCal has value of 0, but there is 1 pattern.
		unsigned char numAnts;	//													[offset:32]
		unsigned char pattern[MAX_ANTS];	//										[offset:33]
		//																			[offset:48]
		bool enableAudio;
	};

	// SLAVE_SETUP_COLLECT_READY
	struct SSlaveSetupCollectReady
	{
		EStatus status;
	};

	// SLAVE_SETUP_COLLECT_RESP
	struct SSlaveSetupCollectResp
	{
		static const size_t NUM_CCDF = 4;
		EStatus status;
		unsigned long key;	// key to taskMap
		unsigned int counts[NUM_CCDF];		// CCDF counts
		unsigned short thresh[NUM_CCDF];	// CCDF thresholds

	};

	// SLAVE_GET_SAMPLES
	struct SSlaveGetSamples
	{
		enum EAction : unsigned long { COLLECT, FLUSH };
		EAction action;
		unsigned long key;	// key to taskMap
	};

	// SLAVE_GET_SAMPLES_RESP
	struct SSlaveGetSamplesResp	// This is a variable length message
	{
		EStatus status;
		unsigned char antNum;		// Antenna number 0 - numAnts-1
		unsigned char totalAnts;	// numAnts
		SlaveDataTypes dataTypes;			// bit mask
		unsigned long numIq;		// Number of data points (iq)
		unsigned long numPoints;	// Number of data points (psd, fft)
		unsigned long data[1];		// This will contain up to numIq + 3 * numPoints items as shown:
		// unsigned long iq[numIq];
		// float psd[numPoints];
		// float fftReal[numPoints];
		// float fftImag[numPoints];
	};

	// GET_CSMS_FAULT
	// No body

	// GET_CSMS_FAULT_RESP
	struct SGetSlaveCsmsFaultRespV1
	{
		EErrStatus	cond;	//Overall system fault condition.
							//Valid value: PASS_STAT, FAIL_STAT, WARNING_STAT.
		SGetCsmsFaultRespV1::SFaultDetails env;	//Specific Environmental fault condition.
	};
	typedef SGetSlaveCsmsFaultRespV1 SGetSlaveCsmsFaultResp;

	// CCDF_COLLECT
	struct SCcdfCollect
	{
		enum EPowerType : unsigned char { TONEDBM, NOISEDBM };

		EPowerType powerType;
		unsigned char atten;
		EGainMode gainMode;
	};

	// CCDF_COLLECT_RESP
	struct SCcdfCollectResp
	{
		EStatus status;
		float powerDbm;
		float gainDb;
	};

	// DO_INITIAL_AGC
	struct SDoInitialAgc
	{
		Units::Frequency::Raw rfFreq;
		EGainMode gainMode;
	};

	// DO_INITIAL_AGC_RESP
	struct SDoInitialAgcResp
	{
		EStatus status;
		unsigned char initAtten;
		signed char attenAdj;
	};

	struct SSetSlaveAudioParams
	{
		SAudioParamsCmd audiocmd;
		SDecimations decimations;
		bool freeallchannels;
		bool freechannels;
		const CNetConnection<void>* audiosource;
	};

	struct SSetSlaveAudioParamsResp
	{
		SAudioParamsResp audioparamsresp;
	};

	// PING
	// No body, no response

	// Message format
#pragma pack(push, 4)
	SHdr hdr;

	union UBody
	{
		SGetMsgVersionsData getMsgVersionsData; // Variable length
		SSetExternalClockResp setExternalClockResp;
		SDoBist doBist;
		SDoBistResp doBistResp;
		SGetSlaveCsmsFaultResp getSlaveCsmsFaultResp;
		SCcdfCollect ccdfCollect;
		SCcdfCollectResp ccdfCollectResp;
		SSlaveTune slaveTune;
		SSlaveTuneResp slaveTuneResp;
		SSlaveSetupCollect slaveSetupCollect;
		SSlaveSetupCollectReady slaveSetupCollectReady;
		SSlaveSetupCollectResp slaveSetupCollectResp;
		SSlaveGetSamples slaveGetSamples;
		SSlaveGetSamplesResp slaveGetSamplesResp;
		SDoInitialAgc doInitialAgc;
		SDoInitialAgcResp doInitialAgcResp;
		SSetSlaveAudioParams setSlaveAudioParam;
		SSetSlaveAudioParamsResp setSlaveAudioParamResp;
	} body;
#pragma pack(pop)
};

// Message versions (__attribute__((__weak__)) lets this go in the .h file)
__attribute__((__weak__))
const SDfCtrlMsg::SVersionData SDfCtrlMsg::VERSION_DATA[] =
{
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::SET_EXTERNAL_CLOCK }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::SET_EXTERNAL_CLOCK_RESP }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::DO_BIST }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::DO_BIST_RESP }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::GET_CSMS_FAULT }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::GET_CSMS_FAULT_RESP }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::CCDF_COLLECT }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::CCDF_COLLECT_RESP }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::PING }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::SLAVE_TUNE }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::SLAVE_TUNE_RESP }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::SLAVE_SETUP_COLLECT }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::SLAVE_SETUP_COLLECT_READY }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::SLAVE_SETUP_COLLECT_RESP }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::SLAVE_GET_SAMPLES }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::SLAVE_GET_SAMPLES_RESP }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::DO_INITIAL_AGC }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::DO_INITIAL_AGC_RESP }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::SET_SLAVE_AUDIO_PARMS }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::SET_SLAVE_AUDIO_PARMS_RESP }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::FREE_SLAVE_AUDIO }, { 1, 1 } },
	{ { SDfCtrlMsg::INTERNAL_DF_CTRL, SDfCtrlMsg::FREE_SLAVE_AUDIO_RESP }, { 1, 1 } }
};

#ifdef _MSC_VER
#pragma warning(pop)
#endif
