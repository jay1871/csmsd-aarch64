/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "51432017.h"
#include "Agc.h"
#include "Bist.h"
#include "CsmsLicense.h"
#include "Digitizer.h"
#include "EquipCtrlMsg.h"
#include "Monitor.h"
#include "Navigation.h"
#include "PbCal.h"
#include "ProcessorNode.h"
#include "RadioEquip.h"
#include "RfSelSwitch.h"
#include "Singleton.h"
#include "TaskScheduler.h"
#include "Task.h"
#include "Units.h"
#include "Watchdog.h"

class CAvdTask;
class CMeasurementTask;
class CPanTask;
class CSuspendableTask;
class CNetConnection<void>;

class CHwControl :
	// Inheritance
	protected CBist
{
public:
	// Constants
	static constexpr unsigned short THRESHOLDS_3230[4] = { 128, 512, 2048, 8192 };

	// Types
	typedef std::unique_ptr<SDfCtrlMsg> DfCtrlMsgPtr;
	typedef CProcessorNode::SDfDataPtr MsgMapValue;

	// Functions
	bool AddTask(const CTask::Task& task, unsigned int timeout = INFINITE, bool isRunnable = true, SEquipCtrlMsg::EMsgType flush = SEquipCtrlMsg::NO_MSGTYPE);
	static unsigned long CalcDdrCount(unsigned long sampleSize, const CConfig::STaskParams& taskParams);
	void DeleteTask(const SEquipCtrlMsg::STaskIdKey& taskIdKey);
	CTask::Task GetTask(_In_ const SEquipCtrlMsg::STaskIdKey& taskIdKey);
	bool GetTask(CNetConnection<void>* source, SEquipCtrlMsg::EMsgType msgType, CTask::Task& existingTask);
	void ReplaceTask(const CTask::Task& task, unsigned int timeout, bool isRunnable);
	void SetRunnable(_In_ const SEquipCtrlMsg::STaskIdKey& taskIdKey, bool isRunnable, bool doAudio = true);
	SEquipCtrlMsg::EAnt GetAntenna() {return m_AntGetPan;};

	// Data
	static bool m_debug;
	static bool m_initDFdone;
#ifdef CSMS_DEBUG
	static size_t s_addAvd2ProcCnt;
	static bool s_bwOfInterest;
	static bool s_traceAvd;
	static bool s_tracePan;
	static bool s_traceSlave;
	static size_t s_withTermCnt;
	static bool s_withTermOn;

	static void EnablePanTrace(void);
#endif

protected:
	// Constants
	static const unsigned int ADD_TASK_TIMEOUT = 5000; // ms
	static const unsigned int QUEUE_DEPTH = 32;
	static const unsigned int NUM_STRATA = static_cast<const unsigned int>(CTask::ETaskGroup::NUM_TASKGROUP);
	static const unsigned int ANT_SWITCH_DELAY = 50000;// new = 80 us to slow down old value = 50000 (20us); // debug only

	// Types
	typedef CTaskScheduler<CTask::Task, QUEUE_DEPTH, NUM_STRATA> TaskSched;
	// Functions
	CHwControl(void);
	virtual ~CHwControl(void); // Make polymorphic
	void DoStartupTasks(void);
	void GetFaultDetails(_Out_ SEquipCtrlMsg::SGetCsmsFaultResp & faultStat) const;
	void Stop(void);

private:
	// Constants
	static constexpr std::chrono::seconds MONITOR_POLLING_INTERVAL = std::chrono::seconds{1};
	static constexpr std::chrono::milliseconds DEFAULT_AGC_DECAY_TIME = std::chrono::milliseconds{1000};
	static const unsigned int IDLE_TIMEOUT = 1000; // ms
	static const size_t MAX_COLLECTION_TRIES = 5;
	static constexpr std::chrono::seconds PAN_TIMEOUT_POLLING_INTERVAL = std::chrono::seconds{1};

	enum class ESequencerType { DF, SINGLE_FAST, SINGLE_SLOW, NOISE };
	enum class EFaultLEDState {OFF, ON, SLOW_BLINK, FAST_BLINK};

	// Types
	typedef std::shared_ptr<CAvdTask> AvdTask;
	typedef std::shared_ptr<CPanTask> PanTask;
	typedef std::shared_ptr<CMeasurementTask> MeasurementTask;
	typedef std::shared_ptr<CSuspendableTask> SuspendableTask;

	struct SAgcTaskParams
	{
		Units::Frequency freq;
		Units::Frequency rxHwBw;
		CRadioEquip::EBandSelect band;
		bool hf;
		SEquipCtrlMsg::EAnt eAnt;
		bool pbCalDirect;
		std::chrono::milliseconds agcDecayTime;
	};

	// Functions
	void AgcTimeoutThread(void);
	void AddTaskToProcessingQueue(const CTask::Task& task);
	bool CanEnableAudio(void) const;
	void CollectData(const CTask::Task& task, CRadioEquip::EBandSelect band, std::chrono::milliseconds agcDecayTime = DEFAULT_AGC_DECAY_TIME);
	bool CollectSlaveSamples(const CTask::Task& task, unsigned long sampleCount, unsigned long count, Units::Frequency rfFreq,
		CRadioEquip::EBandSelect band, SEquipCtrlMsg::EAnt input, bool doSamples,
		double collectionTimeSec, unsigned long numDfBins, bool termRadio, const Units::Frequency& midFrequency,
		const std::vector<unsigned long>& pattern, unsigned int delay, unsigned long& slaveTaskKey, bool enableAudio = false);
	bool CollectSamples(unsigned long sampleCount, unsigned long count, Units::Frequency rfFreq, Units::Frequency rxBw,
		CRadioEquip::EBandSelect band, SEquipCtrlMsg::EAnt input, bool withIq, bool withPsd, bool withFft, bool doSamples, double collectionTimeSec);
	unsigned int CollectSlaveData(const CTask::Task& task, unsigned long numBins, CRadioEquip::EBandSelect band,
		std::vector<size_t>& sentIndex, unsigned long tag, std::chrono::milliseconds agcDecayTime = DEFAULT_AGC_DECAY_TIME, bool enableAudio = false);
	void ControlThread(void);
	bool CreateSequencerPattern(ESequencerType type, SEquipCtrlMsg::EAnt ant, bool hf, Units::Frequency rxHwBw,
		std::vector<unsigned long>& pattern, unsigned long& currentControlWord, unsigned int& delay) const;
#ifdef CSMS_DEBUG
	void DisableAvdTrace(void);
	void EnableAvdTrace(void);
#endif
	bool DoAgc(Units::Frequency freq, Units::Frequency rxBw, CRadioEquip::EBandSelect band, bool hf, SEquipCtrlMsg::EAnt eAnt,
		bool doSlave, bool direct, unsigned char& slaveAtten, std::chrono::milliseconds agcDecayTime = DEFAULT_AGC_DECAY_TIME);
	void DoAvd(_In_ const AvdTask& task);
	void DoBist(_In_ const CTask::Task& task);
	void DoMeasure(_In_ const MeasurementTask& task);
	void DoOccupancyOrScanDf(_In_ const SuspendableTask& task);
	void DoPan(_In_ const PanTask& task);
	void DoPbCal(_In_ const CTask::Task& task);
	void DoSpurCal(bool needSlave, const CTask::Task& task, unsigned int numAnts, unsigned long numBins, bool enableAudio = false);
	bool GetAgcSlave(unsigned char& atten, Units::Frequency freq, Units::Frequency rxBw, CRadioEquip::EBandSelect band,
		bool hf, SEquipCtrlMsg::EAnt eAnt, std::chrono::milliseconds agcDecayTime = DEFAULT_AGC_DECAY_TIME);
	static unsigned long InitialAgcSlaves(Units::Frequency rfFreq, CRadioEquip::EGainMode gainMode, std::vector<size_t>& sentIndex);
	void MonitorThread(void);
	void FaultLEDThread(void);
	void PanTimeoutThread(void);
	static unsigned long RequestCollectedData(bool collect, unsigned long key, std::vector<size_t>& sentIndex);
	bool Reset3230Time(bool logError, bool startup);
	unsigned long ReserveBufferSpace(CTask::SBlockState& blockState, unsigned int numAnts, unsigned long ddrCount, bool spurCal) const;
	void SetAntSwitches(const CTask::Task& task, Units::Frequency freq);
	CRadioEquip::EBandSelect SetHardware(const CTask::Task& task, CRadioEquip::EBandSelect bandSelect, bool isDf = false,
		unsigned long* tag = nullptr, std::vector<size_t>* sentIndex = nullptr);
	static unsigned long SetupCollectSlaves(unsigned long count, unsigned long adcCount, unsigned long delay,
		unsigned long numDfBins, SDfCtrlMsg::SlaveDataTypes dataTypes, bool termRadio, unsigned char slaveAtten,
		CRadioEquip::EGainMode gainMode, const Units::Frequency& midFrequency,
		const std::vector<unsigned long>& pattern, std::vector<size_t>& sentIndex, size_t numAnts, bool enableAudio);
	static unsigned long TuneDfSlaves(Units::Frequency rfFreq, Units::Frequency rxHwBw, Units::Frequency procBw, bool hf,
		CRadioEquip::EBandSelect band, SEquipCtrlMsg::EAnt ant, const C3230::SDecimation& decimations, Units::Frequency procOffset, std::vector<size_t>& sentIndex);
	bool WaitForSlaves(const std::vector<size_t>& sentIndex, unsigned long tag);
	bool WaitForSlaves(const std::vector<size_t>& sentIndex, unsigned long tag, std::vector<CProcessorNode::SDfCtrlMsgPtr>& responses);

	// Data
	CSingleton<CRfSelSwitch> m_antSwitch;
	CSingleton<CNavigation> m_navigation;		// Keep navigation before digitizer
	std::thread m_agcTimeoutThread;
	CSingleton<CConfig> m_config;
	std::thread m_controlThread;
	CSingleton<CDigitizer> m_digitizer;				// Keep navigation before digitizer
	CSingleton<CCsmsLicense> m_license;
	CMonitor m_monitor;
	std::thread m_monitorThread;
	std::thread m_FaultLedThread;
	const CConfig::SMutableConfig& m_mutableConfig; // Must be after m_config
	std::thread m_panTimeoutThread;
	CSingleton<CPbCal> m_pbCal;
	CSingleton<CRadioEquip> m_radioEquip;
	TaskSched m_sched;
	bool m_shutdown;
	std::condition_variable m_shutdownCond;
	std::condition_variable m_stopfaultledthread;
	mutable std::mutex m_shutdownMutex;
	int m_startupComplete;
	int m_startupEpoll;
	CSingleton<C51432017> m_switch;		// should be after m_digitizer
	CSingleton<CWatchdog> m_watchdog;
	CSingleton<CAgc> m_cAgc;			// keep after digitizer and radioequip
	SEquipCtrlMsg::EAnt m_AntGetPan;
	unsigned long m_timeoutcounter;
	EFaultLEDState m_faultLedSate;
	bool m_exitFaultLedThread;
};
