/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"
#include <termios.h>

#include "Failure.h"
#include "Log.h"
#include "Navigation.h"
#include "TciGps.h"
#include "Utility.h"

const float CNavigation::MAX_CORRECTION = 75.0; // deg
const float CNavigation::MIN_HEADING_CHANGE = 2.0; // deg
const float CNavigation::SMOOTHING_FACTOR = 0.9f;
const double CNavigation::SPEED_THRESHOLD = 5; // m/s

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CNavigation::CNavigation(void) :
	CTciGps(),
	m_alt(0),
	m_config(),
	m_mutableConfig(m_config->GetMutableConfig()),
	m_compassHeading(0),
	m_declination(0),
	m_fixNoise(true),
	m_goodLocation(false),
	m_logged(false),
	m_mastDown(false),
	m_mastDownCorrectionsFromCompass(),
	m_mastDownCorrectionsFromGps(),
	m_mastFile(),
	m_orientusVersionSet(false),
	m_status(ErrorCodes::NOFLUXGATE),
	m_stop(false)
{
	memset(&m_gpsResponse, 0, sizeof(m_gpsResponse));
	m_gpsResponse.status.noGps = 1;

	auto declination = GetDeclination(true);

	m_hfTrueHeading = (m_mutableConfig.hfConfig.antCable.isMag
		? FixUp(m_mutableConfig.hfConfig.antCable.orientation + declination)
		: m_mutableConfig.hfConfig.antCable.orientation);
	printf("hfTrueHeading:%f + %f = %f\n", m_mutableConfig.hfConfig.antCable.orientation, declination, m_hfTrueHeading);

	m_ushfTrueHeading[0] = (m_mutableConfig.vushfConfig.antCable[0].isMag
		? FixUp(m_mutableConfig.vushfConfig.antCable[0].orientation + declination)
		: m_mutableConfig.vushfConfig.antCable[0].orientation);
	printf("ushfTrueHeading[0]:%f + %f = %f\n", m_mutableConfig.vushfConfig.antCable[0].orientation, declination, m_ushfTrueHeading[0]);

	m_ushfTrueHeading[1] = (m_mutableConfig.vushfConfig.antCable[1].isMag
		? FixUp(m_mutableConfig.vushfConfig.antCable[1].orientation + declination)
		: m_mutableConfig.vushfConfig.antCable[1].orientation);
	printf("ushfTrueHeading[1]:%f + %f = %f\n", m_mutableConfig.vushfConfig.antCable[1].orientation, declination, m_ushfTrueHeading[1]);

	//	If mobile antenna, get mast down corrections from file. If not, delete the file.
	m_mastFile = std::string(TCIPaths::dataDir) + "mastdown.dat"; //	"/media/tci/csms/data/mastdown.dat";
	if ((m_config->HasMobileAntenna(true) || m_config->HasMobileAntenna(false)) && !m_config->IsProcessorDfSlave())
	{
		// Read mast down corrections from data file
		bool fileError = false;
		std::ifstream file(m_mastFile.c_str(), std::ios::in |  std::ios::binary);
		if (!file)
		{
			fileError = true;
		}
		else
		{
			for (size_t bin = 0; bin < NUM_CORR_BINS; ++bin)
			{
				MastDownCorrections::value_type value(float(bin * Units::DEG_PER_CIRCLE / NUM_CORR_BINS), 0.0f);

				std::string line;
				file.read(reinterpret_cast<char*>(&value.second), sizeof(value.second));
				if (file.gcount() != sizeof(value.second))
				{
					fileError = true;
					break;
				}

				if (isnan(value.second) == 0)
				{
					m_mastDownCorrectionsFromGps.insert(value);
					m_mastDownCorrectionsFromCompass.insert(MastDownCorrections::value_type(FixUp(value.first + value.second), value.second));
				}
			}
		}
		if (fileError)
		{
			// Error, null correction
			m_mastDownCorrectionsFromCompass.clear();
			m_mastDownCorrectionsFromGps.clear();
		}

		Init();
		Start();
	}
	else
	{
		// No compass - remove correction file
		remove(m_mastFile.c_str());
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CNavigation::~CNavigation(void)
{
	Stop();
	Close();

	if (m_config->HasMobileAntenna(true) || m_config->HasMobileAntenna(false))
	{
		SaveCorrections();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Thread function to read compass and update heading and corrections
//
void CNavigation::CompassThread(void)
{
	float lastGpsHeading = std::numeric_limits<float>::quiet_NaN();
	bool updated = false;
	printf("CompassThread starting\n");

	try
	{
		// Poll until shutdown
		auto Polling = [this]
		{
			std::unique_lock<std::mutex> lock(m_stopMutex);
			return !m_stopCond.wait_for(lock, std::chrono::milliseconds(1000), [this] { return m_stop; });
		};

		while(Polling())
		{
			if (m_status != ErrorCodes::SUCCESS && m_mutableConfig.miscConfig.compassType != CConfig::ECompassType::NOT_PRESENT &&
				(m_config->HasMobileAntenna(true) || m_config->HasMobileAntenna(false)))
			{
				// Try to initialize it in case it came to life
//				TRACE("CompassThread: trying to initialize\n");
				Init();
			}

			try
			{
				switch(m_mutableConfig.miscConfig.compassType)
				{
				case CConfig::ECompassType::C100:
					ReadC100();
					HandleMastStatusCorrections(lastGpsHeading, updated);
					break;

				case CConfig::ECompassType::ORIENTUS:
					ReadNmea183();
					HandleMastStatusCorrections(lastGpsHeading, updated);
					break;

				case CConfig::ECompassType::NMEA:
					ReadNmea183();
					break;

				case CConfig::ECompassType::NOT_PRESENT:
					break;

				default:
					TRACE("Unknown compass type\n");
					break;
				}
			}
			catch(CSerialException& e)
			{
				std::lock_guard<std::mutex> lock(m_critSect);
				m_status = ErrorCodes::FLUXGATEREADERROR;
				m_mastDown = false;
			}
			catch(EException e)
			{
				if (e != ABORTED)
				{
					std::lock_guard<std::mutex> lock(m_critSect);
					m_status = ErrorCodes::FLUXGATEREADERROR;
					m_mastDown = false;
				}
			}
		}

		printf("CNavigation::CompassThread exiting\n");
	}
	catch(...)
	{
		CFailure::OnFail(std::current_exception());
	}
	return;

}


//////////////////////////////////////////////////////////////////////
//
// Adjust heading to be in the range [0, 360)
//
float CNavigation::FixUp(float heading)
{
	if (heading < 0)
	{
		heading += Units::DEG_PER_CIRCLE;
	}
	else if(heading >= Units::DEG_PER_CIRCLE)
	{
		heading -= Units::DEG_PER_CIRCLE;
	}

//	ASSERT(heading >= 0 && heading < Units::DEG_PER_CIRCLE);

	return heading;
}


//////////////////////////////////////////////////////////////////////
//
// Get the calibration status
//
CNavigation::SCalStatus CNavigation::GetCalStatus(void)
{
	SCalStatus status = { 0xffff,  0xffff,  0xffff };

	auto& misc = m_mutableConfig.miscConfig;
	switch(misc.compassType)
	{
	case CConfig::ECompassType::C100:
		Stop();

		try
		{
			SendCommand("?cs2");

			if (sscanf(ReadResponse(200).c_str(), "?cs2 %hu,%hu,%hu", &status.noise, &status.environment, &status.count) == 3)
			{
				if (m_fixNoise)
				{
					// The noise score is returned wrong
					++status.noise;
				}

				m_status = ErrorCodes::SUCCESS;
			}
			else
			{
				m_status = ErrorCodes::FLUXGATEDATAERROR;
			}
		}
		catch(EException)
		{
			m_status = ErrorCodes::FLUXGATEREADERROR;
		}

		Start();
		break;

	case CConfig::ECompassType::NMEA: // Not supported by NMEA
	case CConfig::ECompassType::ORIENTUS: // Not supported by ORIENTUS
	case CConfig::ECompassType::NOT_PRESENT:
		break;

	default:
		ASSERT(false);
		break;
	}

	return status;
}


//////////////////////////////////////////////////////////////////////
//
// Get the declination, recalculating if necessary
//
float CNavigation::GetDeclination(bool forceCalc) const
{
	if (!m_goodLocation || forceCalc)
	{
		double lat;
		double lon;
		double alt;
		m_goodLocation = GetPosition(lat, lon, alt);
		auto ts = Utility::CurrentTimeAsTimespec();

		if (m_goodLocation || forceCalc)
		{
			m_declination = m_config->CalcDeclination(float(lat), float(lon), float(alt), ts);
		}
	}

	return m_declination;
}


//////////////////////////////////////////////////////////////////////
//
// Get GPS response
//
ErrorCodes::EErrorCode CNavigation::GetGpsResponse(_Out_ SEquipCtrlMsg::SGpsResponse& gpsResponse) const
{
	ErrorCodes::EErrorCode error = ErrorCodes::SUCCESS;

	if (m_mutableConfig.miscConfig.compassType == CConfig::ECompassType::NMEA)
	{
		// Use external GPS data
		std::lock_guard<std::mutex> lock(m_critSect);
		gpsResponse = m_gpsResponse;

		if (gpsResponse.status.noGps == 1)
		{
			error = ErrorCodes::HARDWARENOTPRESENT;
		}
		else if (gpsResponse.status.antenna != 0)
		{
			error = ErrorCodes::HARDWAREDOWN;
		}
		else if (gpsResponse.status.accuracy == 3 || gpsResponse.status.timErr1 == 1 || gpsResponse.status.timErr2 == 1)
		{
			error = ErrorCodes::UNCALIBRATED;
		}
	}
	else
	{
		// Use my GPS
		memset(&gpsResponse.status, 0, sizeof(gpsResponse.status));

		if (!CTciGps::IsConnected())
		{
			gpsResponse.status.noGps = 1;
			error = ErrorCodes::HARDWARENOTPRESENT;
		}
		unsigned long numSat = 	CTciGps::GetNumSatellites();
		if (numSat == 0)
		{
			gpsResponse.status.satLock = 1;
			gpsResponse.status.accuracy = 3;
			gpsResponse.status.tracking = 1;
			error = ErrorCodes::UNCALIBRATED;
		}
		if (CTciGps::GetMode() >= MODE_2D)
		{
			gpsResponse.status.accuracy = 2;
		}

		if (gpsResponse.status.noGps == 0)
		{
			gpsResponse.status.mode = 2;
			gpsResponse.status.numSats = (numSat <= 7 ? numSat : 7);
		}

		double altitude;
		CTciGps::GetPosition(gpsResponse.latitude, gpsResponse.longitude, altitude);
	}

	CTciGps::GetTime(gpsResponse.dateTime);

	return error;
}


//////////////////////////////////////////////////////////////////////
//
// Get the magnetic heading
//
float CNavigation::GetMagHeading(bool hf, Units::Frequency freq) const
{
	std::lock_guard<std::mutex> lock(m_critSect);

	float trueHeading;
	if (hf)
	{
		trueHeading = m_hfTrueHeading;
	}
	else
	{
		if (m_mutableConfig.vushfConfig.antPolicy == CConfig::EAntPolicy::AUTO_FREQ)
		{
			if (freq <= m_mutableConfig.vushfConfig.antCable[0].freqHigh)
			{
				trueHeading = m_ushfTrueHeading[0];
			}
			else
			{
				trueHeading = m_ushfTrueHeading[1];
			}
		}
		else if (m_mutableConfig.vushfConfig.antPolicy == CConfig::EAntPolicy::CLIENT_NAME)
		{
			// For now, going to assume that client_name policy returns heading from antenna connected to RF1
//			if (ant == SEquipCtrlMsg::ANT2 || ant == SEquipCtrlMsg::SMPL_RF2)	// AUX1
//			{
//				trueHeading = m_ushfTrueHeading[1];
//			}
//			else
//			{
				trueHeading = m_ushfTrueHeading[0];
//			}
		}
		else
		{
			trueHeading = m_ushfTrueHeading[0];	// default
		}
	}
	return FixUp(trueHeading - GetDeclination());
}



//////////////////////////////////////////////////////////////////////
//
// Get the mast down corrections
//
CNavigation::MastDownCorrections CNavigation::GetMastDownCorrections(void) const
{
	std::lock_guard<std::mutex> lock(m_critSect);

	return m_mastDownCorrectionsFromGps;
}


//////////////////////////////////////////////////////////////////////
//
// Get the position
//
bool CNavigation::GetPosition(double& lat, double& lon, double& alt) const
{
	if (m_mutableConfig.miscConfig.compassType == CConfig::ECompassType::NMEA)
	{
		std::lock_guard<std::mutex> lock(m_critSect);
		lat = m_gpsResponse.latitude;
		lon = m_gpsResponse.longitude;
		alt = m_alt;

		return m_gpsResponse.status.noGps == 0 && m_gpsResponse.status.antenna == 0 && m_gpsResponse.status.accuracy < 3 &&
			m_gpsResponse.status.timErr1 == 0 && m_gpsResponse.status.timErr2 == 0;
	}
	else
	{
		return CTciGps::GetPosition(lat, lon, alt);
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the raw (uncorrected for mast-down) heading
//
float CNavigation::GetRawMagHeading(bool hf, Units::Frequency freq) const
{
	return m_config->HasMobileAntenna(hf) ? m_compassHeading : GetMagHeading(hf, freq);
}


//////////////////////////////////////////////////////////////////////
//
// Get the true heading
//
float CNavigation::GetTrueHeading(bool hf, Units::Frequency freq) const
{
	std::lock_guard<std::mutex> lock(m_critSect);
	if (hf)
		return m_hfTrueHeading;

	if (freq <= m_mutableConfig.vushfConfig.antCable[0].freqHigh)
		return m_ushfTrueHeading[0];

	return m_ushfTrueHeading[1];
}


//////////////////////////////////////////////////////////////////////
//
// Handle mast up/down, magnetic to true, etc
//
void CNavigation::HandleMastStatusCorrections(float& lastGpsHeading, bool& updated)
{
	// Mastdown status was updated by CHwControl::MonitorTHread
	std::lock_guard<std::mutex> lock(m_critSect);

//	// Read DSR / RLSD for mast down status
//	DWORD modemStatus;
//	GetModemStatus(modemStatus);
//	CSingleLock lock(&m_critSect, TRUE);
//	m_mastDown = (modemStatus & m_mutableConfig.miscConfig.compassMastDownState) != 0;

	// Read GPS velocity
	double speed;
	double track;
	if (CTciGps::GetSpeed(speed) && CTciGps::GetTrack(track) && speed > SPEED_THRESHOLD)
	{
		// Good GPS and driving "fast enough"
//		float gpsHeading = FixUp(float(atan2(east, north) * Units::R2D));
		float gpsHeading = FixUp(float(track));

		if (m_mastDown && m_status == ErrorCodes::SUCCESS && isnan(lastGpsHeading) == 0)
		{
			float change = fabsf(gpsHeading - lastGpsHeading);

			if (change > Units::DEG_PER_CIRCLE / 2)
			{
				change = Units::DEG_PER_CIRCLE - change;
			}

			if (change < MIN_HEADING_CHANGE)
			{
				// Update mast down calibration
				float correction = gpsHeading - (m_compassHeading + GetDeclination());

				while(correction > Units::DEG_PER_CIRCLE / 2)
				{
					correction -= Units::DEG_PER_CIRCLE;
				}

				while(correction <= -int(Units::DEG_PER_CIRCLE / 2))
				{
					correction += Units::DEG_PER_CIRCLE;
				}

				if (fabsf(correction) < MAX_CORRECTION)
				{
					TRACE("Mast down correction %.0f @ %.0f°\n", correction, gpsHeading);

					// Exponential average
					float rounded = FixUp(float(size_t((gpsHeading + Units::DEG_PER_CIRCLE / NUM_CORR_BINS / 2) /
						Units::DEG_PER_CIRCLE * NUM_CORR_BINS) * Units::DEG_PER_CIRCLE / NUM_CORR_BINS));
					MastDownCorrections::iterator bin = m_mastDownCorrectionsFromGps.find(rounded);

					if (bin == m_mastDownCorrectionsFromGps.end())
					{
						m_mastDownCorrectionsFromGps.insert(MastDownCorrections::value_type(rounded, correction));
					}
					else
					{
						bin->second = SMOOTHING_FACTOR * bin->second + (1 - SMOOTHING_FACTOR) * correction;
					}

					updated = true;
				}
			}
		}

		// Use GPS heading
		if (m_config->HasMobileAntenna(true))
		{
			m_hfTrueHeading = gpsHeading;
		}

		if (m_config->HasMobileAntenna(false))
		{
			m_ushfTrueHeading[0] = gpsHeading;
		}

		lastGpsHeading = gpsHeading;
	}
	else if (m_mastDown)
	{
		// Use fluxgate with mast down correction
		if (updated)
		{
			// Build reverse map
			m_mastDownCorrectionsFromCompass.clear();

			for (MastDownCorrections::const_iterator bin = m_mastDownCorrectionsFromGps.begin(); bin != m_mastDownCorrectionsFromGps.end(); ++bin)
			{
				m_mastDownCorrectionsFromCompass.insert(MastDownCorrections::value_type(FixUp(bin->first + bin->second), bin->second));
			}

			// Save corrections
			SaveCorrections();
			updated = false;
		}

		if (m_status == ErrorCodes::SUCCESS)
		{
			// Interpolate
			float trueFluxgateHeading = FixUp(m_compassHeading + GetDeclination());
			MastDownCorrections::const_iterator upper = m_mastDownCorrectionsFromCompass.lower_bound(trueFluxgateHeading);
			float trueHeading = trueFluxgateHeading;

			if (upper == m_mastDownCorrectionsFromCompass.end())
			{
				MastDownCorrections::const_reverse_iterator lower = m_mastDownCorrectionsFromCompass.rbegin();

				if (lower != m_mastDownCorrectionsFromCompass.rend())
				{
					upper = m_mastDownCorrectionsFromCompass.begin();
					trueHeading = FixUp(trueFluxgateHeading + lower->second + (trueFluxgateHeading - lower->first) * (upper->second - lower->second) /
						(upper->first + Units::DEG_PER_CIRCLE - lower->first));
				}
			}
			else if (upper == m_mastDownCorrectionsFromCompass.begin())
			{
				MastDownCorrections::const_reverse_iterator lower = m_mastDownCorrectionsFromCompass.rbegin();
				trueHeading = FixUp(trueFluxgateHeading + upper->second + (trueFluxgateHeading - upper->first) * (upper->second - lower->second) /
					(upper->first + Units::DEG_PER_CIRCLE - lower->first));
			}
			else
			{
				MastDownCorrections::const_iterator lower = upper;
				--lower;
				trueHeading = FixUp(trueFluxgateHeading + lower->second + (trueFluxgateHeading - lower->first) * (upper->second - lower->second) /
					(upper->first - lower->first));
			}

			if (m_config->HasMobileAntenna(true))
			{
				m_hfTrueHeading = trueHeading;
			}

			if (m_config->HasMobileAntenna(false))
			{
				m_ushfTrueHeading[0] = trueHeading;
			}
		}

		lastGpsHeading = std::numeric_limits<float>::quiet_NaN();
	}
	else if(m_status == ErrorCodes::SUCCESS)
	{
		// Use fluxgate uncorrected
		if (m_config->HasMobileAntenna(true))
		{
			m_hfTrueHeading = FixUp(m_compassHeading + GetDeclination());
		}

		if (m_config->HasMobileAntenna(false))
		{
			m_ushfTrueHeading[0] = FixUp(m_compassHeading + GetDeclination());
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Try to initialize the compass
//
void CNavigation::Init(void) try
{
	Close();
	auto& misc = m_mutableConfig.miscConfig;
	if (misc.compassType == CConfig::ECompassType::C100)
	{
		try
		{
			Open(misc.compassPort, misc.compassBaud);
			SendCommand("\b\b\b\b\b\b\b\b\b\b\b\bh");
		}
		catch(EException)
		{
			Close();
			return;
		}
		std::lock_guard<std::mutex> lock(m_critSect);
		m_status = ErrorCodes::SUCCESS;
	}
	else if (misc.compassType == CConfig::ECompassType::ORIENTUS || misc.compassType == CConfig::ECompassType::NMEA)
	{
		try
		{
			Open(misc.compassPort, misc.compassBaud);

			// Listen
			ReadResponse(2000, '\n');

			// Get another line (first may be partial) and check if it's valid NMEA
			std::string talker;
			std::string sentence;
			std::vector<std::string> data;
			ParseNmea183(ReadResponse(2000, '\n'), talker, sentence, data);

			if (talker == "HC")
			{
				// Might be the C100 in factory default mode
				throw OTHER;
			}

			// Succeeded
			std::lock_guard<std::mutex> lock(m_critSect);
			m_version = _T("External NMEA 0183");
			m_status = ErrorCodes::SUCCESS;
			m_logged = false;

			// External compass - remove correction file

			remove(m_mastFile.c_str());
			return;
		}
		catch(EException)
		{
			Close();
			return;
		}
	}
	else
	{
		return;
	}
	std::unique_lock<std::mutex> lock(m_critSect);
	if (misc.compassType == CConfig::ECompassType::C100)
	{
		// Program it
		lock.unlock();
		SendCommand("?w");
		lock.lock();
		m_version = ReadResponse(200).substr(4);
		char firmware;

		if (sscanf(m_version.c_str(), "%*d,%c", &firmware) == 1 && firmware >= 'D')
		{
			// Firmware bug fixed
			m_fixNoise = false;
		}

		lock.unlock();
		SendCommand("=r,60");
		SendCommand("=t,0");
		SendCommand("=i,d");
		SendCommand("=zt,0");
		SendCommand("=v,m");
		SendCommand("=vi,0");
		SendCommand("=dt0,3");
		SendCommand("=d0,0");
		m_logged = false;
	}

	return;
}
catch(CSerialException& e)
{
	if (!m_logged)
	{
		CLog::Log(CLog::ERRORS, "Failed to initialize fluxgate compass %s", e.what());
		m_logged = true;
	}

	std::lock_guard<std::mutex> lock(m_critSect);
	m_status = ErrorCodes::HARDWAREDOWN;

	return;
}
catch(EException)
{
	Close();
	std::lock_guard<std::mutex> lock(m_critSect);
	m_status = ErrorCodes::FLUXGATEREADERROR;

	return;
}

//////////////////////////////////////////////////////////////////////
//
// Parse an NMEA sentence
//
void CNavigation::ParseNmea183(const std::string& line, std::string& talker, std::string& sentence, std::vector<std::string>& data)
{
	if (line.length() < 6 || line[0] != '$' || !isalpha(static_cast<unsigned char>(line[1])) || !isalpha(static_cast<unsigned char>(line[2])) ||
		!isalpha(static_cast<unsigned char>(line[3])) || !isalpha(static_cast<unsigned char>(line[4])) || !isalpha(static_cast<unsigned char>(line[5])))
	{
		throw OTHER;
	}

	talker = line.substr(1, 2); // line.Mid(1, 2);
	sentence = line.substr(3, 3); // line.Mid(3, 3);
	data.clear();

	if (line.length() > 6)
	{
		if (line[6] == ',')
		{
			int comma = 6;

			for (size_t next = line.find(',', comma + 1); next != std::string::npos; comma = next, next = line.find(',', comma + 1))
			{
				data.push_back(line.substr(comma + 1, next - comma - 1));
			}

			data.push_back(line.substr(comma + 1));
		}
		else
		{
			throw OTHER;
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Read a heading from the C100
//
void CNavigation::ReadC100(void)
{
	SendCommand("d0");
	std::string talker;
	std::string sentence;
	std::vector<std::string> data;
	ParseNmea183(ReadResponse(2000, '\n'), talker, sentence, data);

	if (talker != std::string("HC") || sentence !=std::string("HDM") || data.size() != 2 || data[1] != std::string(1, 'M'))
	{
		throw OTHER;
	}

	m_compassHeading = float(atof(data[0].c_str()));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Read characters from the compass
//
char CNavigation::ReadChar(unsigned int timeout)
{
	char result = ' ';
	auto readStat = Read(reinterpret_cast<unsigned char&>(result), timeout);

	switch (readStat)
	{
	case CSerial::BYTE_READ:
		//Do nothing the byte char is read.
		break;

	case CSerial::TIMEOUT_ERR:
		throw TIMEOUT;
		break;

	case CSerial::LINE_ERR:
		throw LINE_ERROR;
		break;

	default:
		//should not happen.
		ASSERT(false);
		throw OTHER;
	}

	return result;
}


//////////////////////////////////////////////////////////////////////
//
// Read and process NMEA 183 strings from the external navigation device (eg FOG or Hemisphere LV101)
//
void CNavigation::ReadNmea183(void)
{
	std::string talker;
	std::string sentence;
	std::vector<std::string> data;
	ParseNmea183(ReadResponse(2000, '\n'), talker, sentence, data);

	if (talker[0] != 'P' && sentence == "GGA")
	{
		if (data.size() != 14)
		{
			throw OTHER;
		}

		std::lock_guard<std::mutex> lock(m_critSect);
		m_gpsResponse.status.noGps = 0;

		if (data[5] != "0")
		{
			m_gpsResponse.status.accuracy = 0;
			int deg;
			float min;

			if (sscanf(data[1].c_str(), "%2d%f", &deg, &min) == 2)
			{
				m_gpsResponse.latitude = deg + min / 60;

				if (data[2] == "S")
				{
					m_gpsResponse.latitude = -m_gpsResponse.latitude;
				}
			}
			else
			{
				m_gpsResponse.status.accuracy = 3;
			}

			if (sscanf(data[3].c_str(), "%3d%f", &deg, &min) == 2)
			{
				m_gpsResponse.longitude = deg + min / 60;

				if (data[4] == "W")
				{
					m_gpsResponse.longitude = -m_gpsResponse.longitude;
				}
			}
			else
			{
				m_gpsResponse.status.accuracy = 3;
			}

			unsigned long numSats;

			if (sscanf(data[6].c_str(), "%lu", &numSats) == 1)
			{
				m_gpsResponse.status.numSats = (numSats > 7 ? 7 : numSats);
			}

			float alt;

			if (sscanf(data[8].c_str(), "%f", &alt) == 1)
			{
				m_alt = alt;
			}
		}
		else
		{
			m_gpsResponse.status.accuracy = 3;
		}
	}
	else if (sentence == "HDT")
	{
		if (talker == "GP" || talker == "HE")
		{
			if (m_mutableConfig.miscConfig.compassType == CConfig::ECompassType::ORIENTUS && talker == "GP")
			{
				if (!m_orientusVersionSet && data.size() == 2 && data[1] == "T")
				{
					m_version = _T("Orientus NMEA 0183 @ 38400 baud");
					m_orientusVersionSet = true;
				}

				// Upload location for Orientus
				double lat;
				double lon;
				double alt;
				char nS = 'N';
				char eW = 'E';
				int degLat;
				double minLat;
				int degLon;
				double minLon;
				CTciGps::GetPosition(lat, lon, alt);

				if (lat < 0)
				{
					nS = 'S';
					lat *= -1;
				}

				degLat = int(lat);
				minLat = (lat - degLat) * 60;

				if (lon < 0)
				{
					eW = 'W';
					lon *= -1;
				}

				degLon = int(lon);
				minLon = (lon - degLon) * 60;
				auto ts = Utility::CurrentTimeAsTimespec();
				tm tmBuffer;
				char buffer[10];
				if (strftime(buffer, 10, "%H%M%S", gmtime_r(&ts.tv_sec, &tmBuffer)) == 0)
				{
					throw OTHER;
				}
				char temp[100];
				auto nchar = snprintf(temp, 100, "$GPGLL,%02d%.4f,%c,%d%.4f,%c,%s,A,",
					degLat, minLat, nS, degLon, minLon, eW, buffer);
//				gll.Format("$GPGLL,%02d%.4f,%c,%d%.4f,%c,%s,A,", degLat, minLat, nS, degLon, minLon, eW, LPCSTR(CT2A(COleDateTime(now).Format(_T("%H%M%S")))));
				std::string gll(temp, nchar);
				char checksum = 0;

				for (size_t i = 1; i < gll.length(); ++i)
				{
					checksum ^= gll[i];
				}

				char checksumStr[8];
				snprintf(checksumStr, 8, "*%02X\r\n", checksum);
//				checksumStr.Format("*%02X\r\n", checksum);
				gll += checksumStr;
				if (Write(gll, gll.length()) != int(gll.length()))
				{
					throw TIMEOUT;
				}
			}

			if (data.size() != 2 || data[1] != "T")
			{
				throw OTHER;
			}

			if (!data[0].empty())
			{
				std::lock_guard<std::mutex> lock(m_critSect);
				m_ushfTrueHeading[0] = std::stof(data[0]);
				m_compassHeading = FixUp(m_ushfTrueHeading[0] - GetDeclination());

				if (m_config->HasMobileAntenna(true))
				{
					m_hfTrueHeading = m_ushfTrueHeading[0];
				}

				m_status = ErrorCodes::SUCCESS;
			}
			else
			{
				m_status = ErrorCodes::FLUXGATEDATAERROR;
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Read a response string from the compass and check checksum
//
std::string CNavigation::ReadResponse(unsigned int timeout, char end)
{
	if (!IsOpen())
	{
		throw OTHER;
	}

	std::string response;
	response += ReadChar(timeout);

	while (response[response.length() - 1] != end)
	{
		response += ReadChar(100);
	}

	Utility::Trim(response);

	if (response.length() > 3 && response[response.length() - 3] == '*')
	{
		// Has a checksum
		const char* string = response.c_str();
		unsigned char checksum1 = static_cast<unsigned char>(strtoul(&string[response.length() - 2], nullptr, 16));
		unsigned char checksum2 = 0;

		for (int i = 0; i < int(response.length()) - 3; ++i)
		{
			if (string[i] != '$')
			{
				checksum2 ^= string[i];
			}
		}

		if (checksum1 != checksum2)
		{
			// Bad checksum
			throw CHECKSUM;
		}
		else
		{
			// Trim the checksum
			response.resize(response.length() - 3);
		}
	}

	return response;
}


//////////////////////////////////////////////////////////////////////
//
// Save mast down corrections
//
void CNavigation::SaveCorrections(void)
{
	static const float FNAN = std::numeric_limits<float>::quiet_NaN();
	std::ofstream file(m_mastFile.c_str(),std::ios::out |  std::ios::binary);
	if (!file)
	{
		return;
	}

	size_t lastBinIdx = 0;

	for (MastDownCorrections::const_iterator bin = m_mastDownCorrectionsFromGps.begin(); bin != m_mastDownCorrectionsFromGps.end(); ++bin)
	{
		size_t binIdx = size_t(bin->first * NUM_CORR_BINS / Units::DEG_PER_CIRCLE + 0.5f);

		for (++lastBinIdx; lastBinIdx < binIdx; ++lastBinIdx)
		{
			file.write(reinterpret_cast<const char*>(&FNAN), sizeof(FNAN));
		}

		file.write(reinterpret_cast<const char*>(&bin->second), sizeof(bin->second));
	}

	for(; lastBinIdx < NUM_CORR_BINS; ++lastBinIdx)
	{
		file.write(reinterpret_cast<const char*>(&FNAN), sizeof(FNAN));
	}

	TRACE(_T("Mast-down corrections written to file\n"));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send a command to the compass and check the result
//
void CNavigation::SendCommand(const std::string& cmd)
{
	if (!IsOpen())
	{
		throw OTHER;
	}

//	TRACE("SendCommand: ............%s\n", cmd.c_str());
	auto n = Write(cmd + '\r', cmd.length() + 1);
	if (n != int(cmd.length()) + 1)
	{
		throw TIMEOUT;
	}

	std::string result(ReadResponse(200));
//	TRACE("received %s\n", result.c_str());

	if (result == ">")
	{
		return;
	}
	else if (result == "!")
	{
		std::string error = ReadResponse(200);
		TRACE("Fluxgate error: %s\n", error.c_str());

		if (error == "Not Implemented")
		{
			throw NOT_IMPLEMENTED;
		}
		else if (error == "Bad Checksum")
		{
			throw CHECKSUM;
		}
		else
		{
			throw OTHER;
		}
	}
	else
	{
		throw OTHER;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Set the mast down status
//
void CNavigation::SetMastDownStatus(bool mastDown)
{
	if (m_mutableConfig.miscConfig.mastDownInverted) mastDown = !mastDown;

	std::lock_guard<std::mutex> lock(m_critSect);
	m_mastDown = mastDown;
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Start the heading messages and thread
//
void CNavigation::Start(void)
{
	// Start the Thread
	{
		std::lock_guard<std::mutex> lock(m_stopMutex);
		m_stop = false;
	}
	m_thread = std::thread(&CNavigation::CompassThread, this);
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Stop the heading messages and thread
//
void CNavigation::Stop(void)
{
	// Stop the tread
	{
		std::lock_guard<std::mutex> lock(m_stopMutex);
		m_stop = true;
	}

	m_stopCond.notify_all();
	if (m_thread.joinable())
	{
		m_thread.join();
	}
	return;
}

//////////////////////////////////////////////////////////////////////
//
// Serial Port Class
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//
// Close the serial port
//
void CSerial::Close(void)
{
	if (IsOpen())
	{
		close(m_fd);
		m_fd = -1;
		m_timeout10 = 0;	// tenths of second
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Open the serial port
//
void CSerial::Open(const char* port, unsigned int baud)
{
	int baudRate;
	switch(baud)
	{
	case 50: baudRate = B50; break;
	case 75: baudRate = B75; break;
	case 110: baudRate = B110; break;
	case 134: baudRate = B134; break;
	case 150: baudRate = B150; break;
	case 200: baudRate = B200; break;
	case 300: baudRate = B300; break;
	case 600: baudRate = B600; break;
	case 1200: baudRate = B1200; break;
	case 1800: baudRate = B1800; break;
	case 2400: baudRate = B2400; break;
	case 4800: baudRate = B4800; break;
	case 9600: baudRate = B9600; break;
	case 19200: baudRate = B19200; break;
	case 38400: baudRate = B38400; break;
	case 57600: baudRate = B57600; break;
	case 115200: baudRate = B115200; break;
	case 230400: baudRate = B230400; break;
	default:
		CLog::Log(CLog::ERRORS, "Invalid baud rate for compass port: %d", baud);
		return;
	}

	Close();	// In case we are already open

	// Open the port
	m_fd = open(port, O_RDWR | O_NOCTTY);
	if (m_fd < 0)
	{
		throw(CSerialException());
	}
	// Set the port attributes - baud rate, parity, etc.


	termios attr;
	memset(&attr, 0, sizeof(attr));
	attr.c_iflag = IGNPAR;
	attr.c_oflag = 0;
	attr.c_cflag = baudRate | CS8 | CLOCAL | CREAD;
	attr.c_lflag = 0;	// non-canonical, no echo
	m_timeout10 = 0;	// tenths of second
	attr.c_cc[VTIME]    = m_timeout10;		// tenths of second - needs to be changed in read call
	attr.c_cc[VMIN]     = 0;		// Read with timeout

	tcflush(m_fd, TCIFLUSH);
	if (tcsetattr(m_fd, TCSANOW, &attr) < 0)
	{
		Close();
		throw(CSerialException());
	}

	return;
}

//////////////////////////////////////////////////////////////////////
//
// Read 1 byte from the serial port
//
CSerial::EReturnVals CSerial::Read(unsigned char& result, unsigned int timeoutMsec)
{
	if (!IsOpen())
	{
		result = ' ';
		return CSerial::EReturnVals::LINE_ERR;
	}

	unsigned int timeout10 = timeoutMsec / 100;	// tenths of a second
	if (timeout10 == 0) timeout10 = 1;
	if (timeout10 != m_timeout10)
	{
		// Change timeout value
		termios attr;
		tcgetattr(m_fd, &attr);				// get current settings
		attr.c_cc[VTIME] = timeout10;		// change timeout value
		tcsetattr(m_fd, TCSANOW, &attr);	// set the new value now.
		m_timeout10 = timeout10;
	}

	auto numRead = read(m_fd, &result, 1);
	if (numRead == -1)
	{
		TRACE("CSerial read error %s\n", strerror(errno));
		result = ' ';
		return CSerial::EReturnVals::LINE_ERR;
	}
	if (numRead == 0)
	{
		result = ' ';
		return CSerial::EReturnVals::TIMEOUT_ERR;
	}
	return CSerial::EReturnVals::BYTE_READ;
}


//////////////////////////////////////////////////////////////////////
//
// Write to the serial port
//
int CSerial::Write(const std::string& buf, size_t count)
{
//	auto c = buf.c_str();
//	for (size_t i = 0; i < count; ++i)
//	{
//		TRACE("Write char[%u] = %02x\n", i, c[i]);
//	}
	if (!IsOpen())
	{
		return 0;
	}
	auto numWrite = write(m_fd, buf.c_str(), count);
	if (numWrite == -1)
	{
		TRACE("CSerial write error %s\n", strerror(errno));
		return 0;
	}
	return numWrite;
}
