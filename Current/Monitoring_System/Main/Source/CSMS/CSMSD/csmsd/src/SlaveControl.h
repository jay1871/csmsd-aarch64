/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "Agc.h"
#include "Bist.h"
#include "Config.h"
#include "CsmsLicense.h"
#include "DfCtrlMsg.h"
#include "Digitizer.h"
#include "Monitor.h"
#include "Navigation.h"
#include "PriorityQueue.h"
#include "ProcessorNode.h"
#include "RadioEquip.h"
#include "Audio.h"
#include "Singleton.h"

class CDfCtrlNet;

class CSlaveControl :
	// Inheritance
	protected CBist,
	public CAudio
{
	// Friends
	friend class CSingleton<CSlaveControl>;

public:
	// Functions
	void OnInternalDfCtrl(const SDfCtrlMsg* msg, CDfCtrlNet* source);

private:
	// Constants
	static constexpr std::chrono::seconds MONITOR_POLLING_INTERVAL = std::chrono::seconds{1};
	static const unsigned int QUEUE_DEPTH = 256;

	// Types
	struct SDfCollectTask
	{
		CDfCtrlNet* source;
		SDfCtrlMsg::EStatus status;
		SDfCtrlMsg::SHdr msgHdr;
		SDfCtrlMsg::SSlaveSetupCollect msgBody;
		C2630::S2630State radioState;
		unsigned long sampleCount;
		size_t rfGainTableIdx;
		long ifGainTableIdx;
		float gainAdj;
		float gainAdjTemp;
		float rxGain;
	};
	typedef std::unique_ptr<SDfCollectTask> Task;
	typedef CPriorityQueue<Task, QUEUE_DEPTH> TaskQueue;
	struct SKeyTask
	{
		unsigned long first;
		Task second;
	};

	typedef std::vector<SKeyTask> TaskMap;

	// Functions
	CSlaveControl(void);
	virtual ~CSlaveControl(void);
	void DoCcdfCollect(const SDfCtrlMsg* msg, CDfCtrlNet* source);
	void DoGetCsmsFault(const SDfCtrlMsg* msg, CDfCtrlNet* source);
	void DoSetSlaveAudioParams(const SDfCtrlMsg* msg, CDfCtrlNet* source);
	void DoFreeSlaveAudioChannel(const SDfCtrlMsg* msg, CDfCtrlNet* source);
	void DoInitialAgc(const SDfCtrlMsg* msg, CDfCtrlNet* source);
	void DoPing(const SDfCtrlMsg* msg, CDfCtrlNet* source);
	void DoSlaveGetSamples(const SDfCtrlMsg* msg, CDfCtrlNet* source);
	void DoSlaveTune(const SDfCtrlMsg* msg, CDfCtrlNet* source);
	void DoSlaveSetupCollect(const SDfCtrlMsg* msg, CDfCtrlNet* source);
	void GetCorrections(const SDfCollectTask& task, int numBins, float& gainAdj, Ne10F32Vec& adjPower) const;
	void MonitorThread(void);
	bool Reset3230Time(bool logError, bool startup);
	void Stop(void);
	void ProcessSlaveGetSamples(const SDfCollectTask& task);
#if NO_SLAVE_CLIENT == 1
	void ProcessSlaveGetSamples(const SDfCollectTask& task, CDfCtrlNet* source);
#endif
	void Thread(void);

	// Data
	CSingleton<CConfig> m_config;
	CMonitor m_monitor;
	std::thread m_monitorThread;
	CSingleton<const CNavigation> m_navigation;		// Keep navigation before digitizer
	CSingleton<CDigitizer> m_digitizer;
	CSingleton<CCsmsLicense> m_license;
	CSingleton<CProcessorNode> m_processorNode;
	CSingleton<CRadioEquip> m_radioEquip;
	bool m_shutdown;
	std::condition_variable m_shutdownCond;
	mutable std::mutex m_shutdownMutex;
	TaskQueue m_queue;
	TaskMap m_taskMap;
	std::mutex m_taskMapMtx;
	std::thread m_thread;
	CSingleton<CWatchdog> m_watchdog;
	CSingleton<CAgc> m_cAgc;
	bool m_CanEnableAudio;
	unsigned long m_busyCounter;
};

