/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "Ne10Vec.h"
#include "Singleton.h"
#include "SuspendableTask.h"
#include "Units.h"

struct SEquipCtrlMsg;
template<typename T> class CNetConnection;

class CScanDfTask :
	// Inheritance
	public CSuspendableTask
{
public:
	// Types
	typedef std::shared_ptr<CScanDfTask> ScanDfTask;

	// Functions
	void AddBlock(size_t band, size_t block, unsigned long offset,
		Units::Frequency firstChanFreq, _In_ const Ne10F32Vec& az, _In_ const Ne10F32Vec& el,
		_In_ const Ne10F32Vec& conf, _In_ const Ne10F32Vec& watts, float switchTemp);
	static ScanDfTask Create(_In_ const SEquipCtrlMsg* cmd, _In_opt_ CNetConnection<void>* source);
	static void Create(_In_ SRestartData& restartData);
	const Ne10F32Vec& GetPrevAz(size_t band, size_t block) const { return m_prevAz[band][block]; }
	void SaveCurrentAz(size_t band, size_t block, _In_ const Ne10F32Vec& az,
			_In_ const Ne10F32Vec& conf, unsigned long offset, unsigned long bandNumChan);
	void SendReports(bool last);
	static void Validate(_In_ const SEquipCtrlMsg::SGetScanDfCmd& cmd);

	// Data
	float m_conf;
	float m_snr; // As a ratio (not dB)

private:
	// Types
	struct Deleter
	{
		void operator()(_In_ CScanDfTask* task) { delete task; }
	};

	struct SDataKey
	{
		bool operator<(const SDataKey& b) const
			{ return band < b.band || (band == b.band && chan < b.chan) || (band == b.band && chan == b.chan && azBin < b.azBin); }

		size_t azBin;
		size_t band;
		size_t chan;
	};

	struct SDataValue
	{
		SDataValue(void) : dfCount(0), fldStrCount(0), fldStrSum(0), rangeCount(0), rangeSum(0) {}

		int dfCount;
		size_t fldStrCount;
		float fldStrSum;
		size_t rangeCount;
		float rangeSum;
	};

	typedef std::map<SDataKey, SDataValue> DataMap;

	// Functions
	CScanDfTask(_In_ const SEquipCtrlMsg* cmd, _In_opt_ CNetConnection<void>* source);
	CScanDfTask(_In_ SRestartData& restartData);
	virtual ~CScanDfTask(void);
	size_t KeyDiff(_In_ const CScanDfTask::SDataKey& a, _In_ const CScanDfTask::SDataKey& b) const;
	virtual unsigned long RestartDataType(void) const { return SEquipCtrlMsg::SCANDF_RESTART_DATA; }
	virtual void SaveRestartData(_Inout_ std::vector<unsigned char>& data) const;

	// Data
	DataMap m_data;
	CSingleton<const CIonogram> m_ionogram;
	size_t m_numAz;
	std::vector<std::vector<Ne10F32Vec> > m_prevAz;
};
