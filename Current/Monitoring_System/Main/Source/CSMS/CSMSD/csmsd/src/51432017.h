/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "7234201102.h"
#include "72362001.h"
#include "Digitizer.h"
#include "Singleton.h"

class C51432017 :
	// Inheritance
	public C7234201102,
	public C72362001
{
	friend class CSingleton<C51432017>;

public:
	// Constants
	enum class EPort { VUSHF = 0, HF = 2, NUM_PORT_TYPE };

	// Functions
	bool IsPresent(void) const { return m_present; }
	void SetDataOut(unsigned long controlWord);
	unsigned char GetRemoteAntType(EPort port) const { return m_remoteAntType[size_t(port)]; }
	bool GetRemoteWhipFault(void) const;

protected:

	// Functions
	C51432017(void);
	virtual ~C51432017(void);

private:
	// Constants

	// Data
	CSingleton<CDigitizer> m_digitizer;
	unsigned char m_remoteAntType[size_t(EPort::NUM_PORT_TYPE)];
	bool m_present;
};


