/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2014-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include <set>
#include "2630.h"
#include "3230.h"
#include "Antenna.h"
#include "EquipCtrlMsg.h"
#include "Ne10Vec.h"
#include "Singleton.h"
#include "Switch.h"
#include "Units.h"

//Forward defintion
//class CAntenna;
class XMLProcessor;

class CConfig
{
	friend class CSingleton<CConfig>;
	friend class CDf;

public:
	// Constants
	static const Units::Frequency C_20_MHZ;	//20 MHz in Units::Frequency.
	static const Units::Frequency C_40_MHZ;	//40 MHz in Units::Frequency.
#ifdef CSMS_2016
	//2016.4 built image version number that csmsd will works with.
	static const unsigned long MIN_CSMS_PRODUCT_VERSION = 0x02010016;
	static const unsigned long MIN_CSMS_FASTSEQ_VERSION = 0x02040013;
#else
	//2014.4 built image version number that csmsd will works with.
	static const unsigned long MIN_CSMS_PRODUCT_VERSION = 0x01050006;
#endif
	static const unsigned int OCCUPANCY_DWELL_TIME_MS = 100;

	// Types
	enum class ECableType : uint32_t { LMR_240 = 0, LMR_400 = 1, FOAM_3_8 = 2, FOAM_1_2 = 3, FOAM_7_8 = 4 };
	enum class EMode : uint32_t { DEMOD, PAN, DF, MEASURE, FAST_SCAN_DF, SCAN_DF, OCCUPANCY, AVD, IQDATA, PB_CAL, 	// These must match ProcParams!
		BIST, INIT, NONE };
	enum class EPolarization : uint32_t { VERT, HORIZ, BOTH };
	enum class EAlgo : uint32_t { NO_ALGO = 0, PASSBAND_EQUAL = 1, NO_RADIO_GAIN = 2 };
	enum class ECompassType : uint32_t { NOT_PRESENT, C100, ORIENTUS, NMEA };
	enum class ESystemType : uint32_t {TDOASYSTEM, DFSYSTEM, DDRSYSTEM, TDOA_MULTIPLE_ANT};
	enum class EGpsAvailable : uint32_t { NOT_AVAILABLE, INTERNAL };
	enum class EAntPolicy : uint32_t { AUTO_FREQ, CLIENT_NAME };

	///@note: C_NUM_CONNECTED_VUHF_RF is the constant used for array size,
	///		  therefore it should be the last port number + 1.
	///3: RF1, RF2 and 3rd one to hold Slave RF2 antenna info.
	enum ERfPort {C_RF1 = 0, C_RF2 = 1, C_SLAVE_RF2 = 2, C_NUM_CONNECTED_VUHF_RF = 3};

	//@note: The constant values once defined cannot be changed.
	enum class EShfExt : uint32_t
	{
		NO_SHF_EXT = 0,
		N9010A_544_B25 = 1,	//Agilent EXA N9010A 44GHz, 25 MHz (B25 option).
		N9010A_544_B40 = 2,	//Agilent EXA N9010A 44GHz, 40 MHz (B40 option).
		N9010A_526_B40 = 3,	//Agilent EXA N9010A 26.5GHz, 40 MHz (B40 option).
		N9010A_532_B40 = 4	//Keysight EXA N9010A 32GHz, 40 MHz (B40 option).
	};

	enum class EShfExtStatus : uint32_t
	{
		SHF_EXT_NOT_SPECIFIED = 0,	//User set SHF Ext. as None in config.
		ANALYZER_CONNECTED = 1,		//Connected to compatible Signal Analyzer.
		INCOMPATIBLE_ANALYZER = 2,	//Incompatible Signal Analyzer found.
		ANALYZER_NOT_FOUND = 3,		//Signal Analyzer not responding.
		ANALYZER_WRONG_MODE = 4,	//Signal Analyzer not in the right mode.
	};


	// Types
	struct SAvdMeasParams
	{
		unsigned long bwDwellTime;
		float bwBetaParam;
		float bwYParam;
		float bwX1Param;
		float bwX2Param;
		unsigned long bwRepeatCount;
		SEquipCtrlMsg::EAveMethod bwAveMethod;
		SEquipCtrlMsg::SGetFreqCmd::EFreqMethod freqFreqMethod;
		unsigned long freqDwellTime;
		unsigned long freqRepeatCount;
		SEquipCtrlMsg::EAveMethod freqAveMethod;
		float autorateMu;
		float autorateK;
		float autorateGoal;
		float autorateMaxTime;
	};

	struct SMutableConfig
	{
		struct SAntennaAndCable
		{
			std::shared_ptr<CAntenna> antenna;
			CAntenna::EAntennaType antType;
			std::string antName;
			Units::Frequency freqHigh;
			ECableType cableType;
			float cableLength;
			float fixedLoss;
			bool isMag;
			float orientation;
			bool lightningProt;
			std::shared_ptr<CSwitch> sw;
			CSwitch::ESwitchType switchType;
		};
		struct SHfConfig
		{
			// Constants
			enum EHfRxType : uint32_t { NO_HFRX = 0, HF_2630 };
			// Functions
			static EHfRxType StrToEnum(const std::string& str)
			{
				if (str == "HF_2630") return HF_2630;
				return NO_HFRX;
			}
			// Data
			SAntennaAndCable antCable;
			EHfRxType rxType;
			EAntPolicy antPolicy;
			////////////// DF /////////////
			static const size_t NUM_COORDS = 9;
			struct SCoords { float x; float y; float z; };
			SCoords antCoords[NUM_COORDS];
			float defaultIonoHeight;
		};

		struct SShfExt
		{
			// Functions
		  	static EShfExt StrToEnum(const std::string& str)
		  	{
		  		if (str == "N9010A_544") return EShfExt::N9010A_544_B25;
		  		else if (str == "N9010A_526") return EShfExt::N9010A_526_B40;
		  		else if (str == "N9010A_532") return EShfExt::N9010A_532_B40;
		  		return EShfExt::NO_SHF_EXT;
		  	}

		  	// Data
		  	EShfExt	shfExtType;
			ECableType shfExtCableType;
			float shfExtCableLength;
			std::string shfExtIP;
		};

		struct SVushfConfig
		{
			// Constants
			enum EVuhfRxType : uint32_t { NO_VUHFRX = 0, NARROW_2630, WIDE_2630 };
			// Functions
			static EVuhfRxType StrToEnum(const std::string& str)
			{
				if (str == "NARROW_2630") return NARROW_2630;
				if (str == "WIDE_2630") return WIDE_2630;
				return NO_VUHFRX;
			}
			// Data
			// There are two RF input connectors + 1 holder for Slave RF2 input.
			SAntennaAndCable antCable[C_NUM_CONNECTED_VUHF_RF];
			EVuhfRxType rxType;
			EAntPolicy antPolicy;
			SShfExt shfExt;
		};

		struct SMiscConfig
		{
			DATE databasePurgeAge;
			size_t langIdx;
			char partNum[13];	// nnnn-nnnn-nn
			char rev[3];		// A or AB
			char serialNumber[8];	// nnnn-nnnnnn
			char customer[41];
			EAlgo algoType;
			bool enhancedDynRng;
			bool disableWatchdog;
			unsigned int compassBaud;
			char compassPort[16];
			ECompassType compassType;
			EGpsAvailable gpsAvailable;
			ESystemType systemtype;
			double fixedLatitude;
			double fixedLongitude;
			double fixedAltitude;
			int allowShutdown;		// 0 = no shutdown, 1 = shutdown, 2 = reboot
			bool mastDownInverted;
		};


		// Functions
		static CConfig::ECableType CableStrToEnum(const std::string& str)
		{
			if (str == "LMR_240") return ECableType::LMR_240;
			if (str == "LMR_400") return ECableType::LMR_400;
			if (str == "FOAM_3_8") return ECableType::FOAM_3_8;
			if (str == "FOAM_1_2") return ECableType::FOAM_1_2;
			if (str == "FOAM_7_8") return ECableType::FOAM_7_8;
			return ECableType::LMR_240;
		}
		static std::string CableEnumToStr(const CConfig::ECableType& e)
		{
			if (e == ECableType::LMR_240) return "LMR-240";
			if (e == ECableType::LMR_400) return "LMR-400";
			if (e == ECableType::FOAM_3_8) return "3/8\" FOAM";
			if (e == ECableType::FOAM_1_2) return "1/2\" FOAM";
			if (e == ECableType::FOAM_7_8) return "7/8\" FOAM";
			return "Unk";
		}
		unsigned int version;
		SMiscConfig miscConfig;
		SHfConfig hfConfig;
		SVushfConfig vushfConfig;
	};

	struct SProcessorNodes
	{
		struct SNode
		{
			SNode(void) : id(0) {}
			SNode(unsigned long i, const std::string& s) : id(i), ipAddress(s) {}
			unsigned long id;
			std::string ipAddress;
		};
		unsigned long nodeId;
		std::string masterIP;
		std::vector<SNode> nodes;
	};

	struct SDemodBws
	{
		std::set<Units::Frequency> bandwidths;
	};

	struct SProcParams
	{
		Units::Frequency GetBinSize() const
		{
			unsigned long up;
			unsigned long down;
			decimations.GetUpDown(up, down);
			return (sampleRate * up) / (sampleSize * down);
		}
		unsigned long GetNumBins(Units::Frequency bw) const
		{
			unsigned long up;
			unsigned long down;
			decimations.GetUpDown(up, down);
			return static_cast<unsigned long>((bw * sampleSize * down) / (sampleRate * up));
		}

		unsigned long blockSize;
		C3230::SDecimation decimations;
		Units::Frequency rxProcBw;
		Units::Frequency sampleRate;
		unsigned long sampleSize;
		unsigned long iqCic;
	};

	struct STaskParams
	{
		bool operator<(const STaskParams& b) const
		{
			// Comparison operator for <map> support
			if (hf != b.hf)
			{
				return hf;
			}
			else if (mode != b.mode)
			{
				return mode < b.mode;
			}
			else if (rxHwBw != b.rxHwBw)
			{
				return rxHwBw < b.rxHwBw;
			}
			else if (bw != b.bw)
			{
				return bw < b.bw;
			}
			else
			{
				return stopFreq - startFreq < b.stopFreq - b.startFreq;
			}
		}

		bool operator==(const STaskParams& b) const
		{
			// Note <= for frequency width !
			return hf == b.hf && mode == b.mode && bw == b.bw && rxHwBw == b.rxHwBw &&
				stopFreq - startFreq <= b.stopFreq - b.startFreq;
		}

		Units::Frequency bw;
		Units::Frequency rxHwBw;
		Units::Frequency startFreq;
		Units::Frequency stopFreq;
		unsigned int clientBand;
		unsigned int numBlocks;
		EPolarization pol;
		EMode mode;
		bool hf;
		bool withIq;
		bool withPsd;
		bool withFft;

	};

	struct SSysInfo
	{
		std::string sysname;
		std::string release;
		std::string version;
		std::string machine;
		std::string product;
		std::string fwversion;
	};

	struct SEeprom
	{
		char macAddress[6];
	};

	struct STdoaValues
	{
		unsigned long csms;
		unsigned long sms;
	};

	struct STdoaClocks	// used for function parameter
	{
		STdoaValues bwDepValue;
		unsigned long extraDdcSamples;
		unsigned long extraDirectClocks;
	};

	////////////////////////////// DF //////////////////////////////////
	/////////////////////////// DF (end) ///////////////////////////////

	// Functions
	int AllowShutdown(void) const { return m_mutableConfig.miscConfig.allowShutdown; }
	bool AntennaCanSSL(void) const;
	float CalcDeclination(float lat, float lon, float alt, timespec ts) const;
	static bool ConvertDemod(SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode detMode, C3230::EDemodMode& mode);
	static bool ConvertDemod(C3230::EDemodMode mode, SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode& detMode);
	const SMutableConfig::SAntennaAndCable& GetThisSysAntCable(unsigned char rfInput) const;
	float GetAntennaFactor(bool hf, SEquipCtrlMsg::EAnt ant, Units::Frequency freq, bool isHorizon) const;
	Units::Frequency GetAux1FreqHigh(void) const;
	Units::Frequency GetAux1FreqLow(void) const;
	static size_t GetAntFromSubBlock(size_t subBlock, SEquipCtrlMsg::EAnt eAnt, bool hf);
	const SAvdMeasParams& GetAvdMeasParams(void) const { return m_avdMeasParams; }
	void GetBandwidths(bool hf, EMode mode, Units::Frequency narrowRxBw, Units::Frequency wideRxBw,
		_Out_ SEquipCtrlMsg::SGetBandResp::SBandwidthList& bandwidthList, bool force = false) const;
	float GetDwellTime(_In_ const SProcParams& procParams) const;
	void GetDwellTimes(bool hf, Units::Frequency narrowRxBw, Units::Frequency wideRxBw, unsigned char numAnts,
		_Out_ SEquipCtrlMsg::SGetBandResp::SDwellTimeList& dwellTimeList, float (*GetEstimatedLatency)(Units::Frequency sampleRate, C3230::SDecimation decimations)) const;
	const SEeprom& GetEeprom(void) const { return m_eeprom; }
	const std::string GetExeName(void) const { return m_exeName; }
	const std::string GetExeTime(void) const { return m_exeTime; }
	C2630::EGainMode GetGainMode(Units::Frequency freq, bool hf) const;
	Units::Frequency GetHfDfVLowFreq(void) const { return m_hfDfVLowFreqUnits; }
	void GetHfDfLimits(SSmsMsg::SGetBandResp& resp) const;
	void GetHfMetricLimits(SSmsMsg::SGetBandResp& resp) const;
	static void GetIpAddresses(std::string& ipAddress, std::string& macAddress);
	size_t GetLangIdx(void) const { return m_mutableConfig.miscConfig.langIdx; }
	double GetLatency(Units::Frequency bw, const C3230::SDecimation& decimations) const;
	float GetLightningProtectGain(bool hf, SEquipCtrlMsg::EAnt ant, Units::Frequency freq) const;
	const Units::Frequency& GetMaxTuneFreq(void) const { return m_maxFreq; }
	bool GetMediaReadWrite(void) const { return m_mediaReadWrite; }
	const SMutableConfig& GetMutableConfig(void) const { return m_mutableConfig; }
	size_t GetNumSlaves(void) const { return m_processorNodes.nodes.size(); }
	static EPolarization GetAntPolarization(SEquipCtrlMsg::EAnt ant);
	void GetNetworkInfo(std::string& ipAddress, std::string& macAddress) const;
//	unsigned long GetNodeId(void) const { return m_processorNodes.nodeId; }
	const SProcessorNodes & GetProcessorNodes(void) const { return m_processorNodes; }
	SProcParams GetProcParams(const STaskParams& taskParams, bool bestBw = false) const;
	static SEquipCtrlMsg::EAnt GetRefAnt(SEquipCtrlMsg::EAnt ant);
	bool GetRestrictedFreqPair(size_t index, Units::FreqPair& freqPair) const;
	float GetRfCableLoss(bool hf, SEquipCtrlMsg::EAnt ant, Units::Frequency freq) const;
	signed char GetSampleGainOffset(Units::Frequency freq, bool isHorizon, bool isHf) const;
	const std::string GetSerialNumber(void) const { return m_mutableConfig.miscConfig.serialNumber; }
	const std::string GetServerName(void) const { return m_serverName; }
	float GetShfExtCableLength(void) const { return(m_mutableConfig.vushfConfig.shfExt.shfExtCableLength); }
	ECableType GetShfExtCableType(void) const { return(m_mutableConfig.vushfConfig.shfExt.shfExtCableType); }
	float GetShfExtensionGain(Units::Frequency freq) const;
	float GetShfExtIfCableLoss(ERfPort rfIndex) const;
	EShfExt GetShfExtSetting(void) const { return(m_mutableConfig.vushfConfig.shfExt.shfExtType);}
	static Units::Frequency GetShfExtIfFreq(void) { return(s_shfIfFreq); }
	static LPCTSTR GetShfExtIpAddr(void) { return(s_shfExtIPAddr.c_str()); }
	static LPCTSTR GetShfExtIpPort(void) { return(s_shfExtIPPort); }
	Units::Frequency GetSmplRF2FreqHigh(void) const;
	Units::Frequency GetSmplRF2FreqLow(void) const;
	float GetSwitchGain(bool hf, SEquipCtrlMsg::EAnt ant, Units::Frequency freq) const;
	const SSysInfo GetSysInfo(void) const { return m_sysinfo; }
//	Units::TimeSpan GetTdoaAdjustment(Units::Frequency bw, const C3230::SDecimation& decimations, bool direct) const;
	void GetTdoaAdjustment(Units::Frequency bw, const C3230::SDecimation& decimations, bool direct,
		Units::TimeSpan& adjTdoa, Units::TimeSpan& adjStart) const;
	void GetVushfDfLimits(SSmsMsg::SGetBandResp& resp) const;
	void GetVushfMetricLimits(SSmsMsg::SGetBandResp& resp) const;
	bool HasDualAux1(void) const { return(m_dualAux1); }
	bool HasGainEqualization(void) const { return (m_mutableConfig.miscConfig.algoType == EAlgo::PASSBAND_EQUAL); }
	bool HasEnhancedDynamicRange(void) const { return m_mutableConfig.miscConfig.enhancedDynRng; }
	bool HasHfAnt(void) const {return(m_mutableConfig.hfConfig.antCable.antType != CAntenna::EAntennaType::NO_ANTENNA); }
	bool HasMobileAntenna(bool hf) const;
	bool HasNoRadioGain(void) const	{ return (m_mutableConfig.miscConfig.algoType == EAlgo::NO_RADIO_GAIN); }
	bool HasRestrictedFreq(void) const { return !m_restrictedFreq.empty(); }
	bool HasShfExt(void) const;
	bool InRestrictedFreq(Units::Frequency freq) const;
	bool InRestrictedFreqBw(Units::Frequency freq, Units::Frequency bw) const;
	bool InRestrictedFreqRange(Units::Frequency lowFreq, Units::Frequency highFreq) const;
	bool IsDfProcessor(void) const { return (m_mutableConfig.miscConfig.systemtype == ESystemType::DFSYSTEM);}
	bool IsFreqAboveVuhfMin(SEquipCtrlMsg::EAnt ant, Units::Frequency freq) const;
	bool IsFreqInRange(Units::Frequency freq, bool isHorizontal, bool isDf, _In_ SEquipCtrlMsg::EAnt ant);
	bool IsInHfFreqRange(Units::Frequency freq) const {return(freq>= m_hfMetricVLowFreqUnits && freq <= m_hfMetricVHighFreqUnits);}
	static bool IsInShfExtFreqRange(Units::Frequency freq);
	bool IsProcessorMaster(void) const{ return m_processorNodes.nodeId == 0; }
	bool IsProcessorDfMaster(void) const{ return (m_processorNodes.nodeId == 0 && !m_processorNodes.nodes.empty()); }
	bool IsProcessorDfSlave(void) const { return m_processorNodes.nodeId != 0; }
	bool IsProcWithAntSelectionSwitch(void) const { return (m_mutableConfig.miscConfig.systemtype == ESystemType::TDOA_MULTIPLE_ANT);}
	bool IsShfExtUsed(_In_ SEquipCtrlMsg::EAnt & ant, _In_ Units::Frequency freq) const;
	bool IsSlaveAntenna(SEquipCtrlMsg::EAnt ant, bool hf) const;
	static bool IsTermAnt(SEquipCtrlMsg::EAnt ant);
	bool IsValidAntenna(SEquipCtrlMsg::EAnt ant, bool hf, bool noHorizChk = false) const;
	bool IsValidDfElement(SEquipCtrlMsg::EAnt ant, bool hf) const;
	bool IsVushfAntSpecified(SEquipCtrlMsg::EAnt ant) const;
	bool IsVushfPortAntOk(ERfPort rfPort) const {return(m_antOk[rfPort]);}
	std::string MasterIP(void) const { return m_processorNodes.masterIP; }
	void SaveMutableConfig(const SMutableConfig& mutableConfig);
//	void SaveNewHardwareSetting(const SMutableConfig& mutableConfig);
//	void SetHfDfVLowFreq(Units::Frequency f) { m_hfDfVLowFreqUnits = f; }
	void SetAntennaSpecificParameters(void);
	void SetAbsoluteFreqs(Units::Frequency low, Units::Frequency high);
	void SetHfMetricFreqs(Units::Frequency low, Units::Frequency high);
	void SetNewAntType(ERfPort rfPort, SEquipCtrlMsg::EAnt ant);
	static void SetShfExtIfFreq(Units::Frequency ifFreq) { s_shfIfFreq = ifFreq; }
	void SetShfExtLicense(bool license) {m_shfExtLicenseValid = license; }
	void SetVushfMetricFreqs(Units::Frequency low, Units::Frequency high);
	bool TranslateVerify5143MultAntValue(SEquipCtrlMsg::EAnt ant, SEquipCtrlMsg::EAnt & modAnt, bool & forcedHf, bool & forcedVuhf) const;
	void UpdateAntennaParams(void);
	bool ValidateDemod(bool hf, SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode detMode, Units::Frequency demodBw) const;
	static void SetConfigFilename(const char* fnm) { s_configFileName = std::string("/") + fnm; };
	bool IsDDRSystem() const { return m_ddrtype; };
	bool AllowFastSeqMode() const { return m_fastseqenabled; };
private:
	// Constants
	static constexpr const char* EEPROM_FILENAME = "/sys/bus/i2c/devices/3-0050/eeprom";
	static const unsigned int CURRENT_NVRAM_VERSION = 1;
	static const Units::Frequency LPA_HIGH_FREQ;
	static const Units::Frequency LPA_LOW_FREQ;
	static const Units::Frequency HORN_HIGH_FREQ;
	static const Units::Frequency HORIZONTAL_647_MAX_FREQ;
	static const Units::Frequency HORIZONTAL_647_MIN_FREQ;
	static const Units::Frequency N9010A_526_MAX_FREQ;
	static const Units::Frequency N9010A_532_MAX_FREQ;
	static const Units::Frequency N9010A_544_MAX_FREQ;
	static const Units::Frequency VERTICAL_647_MAX_FREQ;
	static const Units::Frequency VERTICAL_647_MIN_FREQ;

	enum ELoadStatus
	{
		C_OK = 0,
		C_FILE_ERR = 1,
		C_FREQ_ERR = 2,
	};

	// Types
	typedef std::map<STaskParams, SProcParams> ProcParamsMap;
	typedef std::map<Units::Frequency, float> FreqDepParams;
	typedef std::vector<Units::FreqPair> FreqPairVec;

	struct SCableCoeffs
	{
		float k1;
		float k2;
		float velocityPercent;
	};

	struct SGainMode
	{
		SGainMode(void) : freqHigh(0), gainMode(C2630::EGainMode::NORMAL) {}
		SGainMode(Units::Frequency f, C2630::EGainMode m) : freqHigh(f), gainMode(m) {}
		static C2630::EGainMode StrToEnum(const std::string& str)
		{
			if (str == "NORMAL") return C2630::EGainMode::NORMAL;
			if (str == "RURAL") return C2630::EGainMode::RURAL;
			if (str == "URBAN") return C2630::EGainMode::URBAN;
			if (str == "CONGESTED") return C2630::EGainMode::CONGESTED;
			return C2630::EGainMode::NORMAL;
		}

		Units::Frequency freqHigh;
		C2630::EGainMode gainMode;
	};

	//SSmsMsgAnt2AntType has the mapping of Scorpio controlled antenna value
	// to the csms antenna type definition.
	struct SSmsMsgAnt2AntType
	{
		SEquipCtrlMsg::EAnt	ant;
		CAntenna::EAntennaType antType;
		std::shared_ptr<CAntenna> antenna;
	};

	// Functions
	CConfig(void);
	float CalcCableLoss(ECableType type, Units::Frequency freq, float length) const;

	static EAntPolicy AntPolicyStrToEnum(const std::string& str)
	{
		if (str == "AUTO_FREQ") return EAntPolicy::AUTO_FREQ;
		if (str == "CLIENT_NAME") return EAntPolicy::CLIENT_NAME;
		return EAntPolicy::AUTO_FREQ;
	}

	static ECompassType CompassStrToEnum(const std::string& str)
	{
		if (str == "C100") return ECompassType::C100;
		if (str == "ORIENTUS") return ECompassType::ORIENTUS;
		if (str == "NMEA") return ECompassType::NMEA;
		return ECompassType::NOT_PRESENT;
	}

	bool AntValid4RfPort(ERfPort rfPort, CAntenna::EAntennaType antType) const;
	static void DefaultShfExt(SMutableConfig::SShfExt & shfExt);
	Units::Frequency GetAnalyzerMaxFreq(EShfExt analyzer) const;
	bool GetAntCable(bool hf, SEquipCtrlMsg::EAnt ant, Units::Frequency freq, SMutableConfig::SAntennaAndCable& antCable) const;
	size_t GetAntType(SEquipCtrlMsg::EAnt ant, CAntenna::EAntennaType & antType) const;
	std::string GetConfigString(const std::string& keyWanted);
	static float GetFreqDepValue(const FreqDepParams& table, Units::Frequency freq);
	void LoadAntFactors(CAntenna* pAnt);
	void LoadAvdParams(void);
	SCableCoeffs LoadCableCoeffs(_In_ const std::string& section);
	void LoadAntennaParams(void);
	void LoadDfParams(void);
	bool LoadFreqDepValues(const std::string& filename, FreqDepParams& table) const;
	bool LoadFreqDepParams(FreqDepParams& table, const std::string& fileName, const std::string& section, std::string& filename, bool notify = true);
	bool LoadFreqPairParams(FreqPairVec& freqVec, const std::string& fileName, const std::string& section, bool notify = true);
	ELoadStatus LoadFreqPairValues(const std::string& filename, FreqPairVec& freqVec) const;
	void LoadGainModes(std::vector<SGainMode>& table, const std::string& section);
	void LoadGainModes();
	void LoadMutableConfig(void);
	void LoadMutableConfigShfExtVushf(XMLProcessor & xmlParse);
	void LoadProcessorNodes(void);
	void LoadRestrictedFreqParams(void);
	void LoadSystemType(void);
	void LoadProcParams(void);
	void LoadTdoaParams(void);
	static void ReadEeprom(SEeprom& eeprom);
	static void ResetLowFreq(const char* str, Units::Frequency& current, Units::Frequency value);
	static void ResetHighFreq(const char* str, Units::Frequency& current, Units::Frequency value);
	void SetAntFreqLimits(SEquipCtrlMsg::EAnt ant);
	void SetSlaveNodeFixedConfig(void);
	void SetSlaveRf2Para(void);

	////////////////////////////// DF //////////////////////////////////
	const CAntenna::SSymmetryDfParams* GetSymmetryDfParams(bool horiz, bool shf) const;
	const CAntenna::SWatsonWattParams* GetWatsonWattParams(void) const { return CAntenna::GetWatsonWattParams(m_mutableConfig.hfConfig.antCable.antenna.get()); } //{ return m_watsonWatt; }
	const CAntenna::SWatsonWattParams* GetWatsonWatt620Params(void) const { return CAntenna::GetWatsonWattParams(m_mutableConfig.hfConfig.antCable.antenna.get()); }//{ return m_watsonWatt620; }
	const CAntenna::SWfaParams* GetWfaParams(void) const { return CAntenna::GetWfaParams(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna.get()); }
	const CAntenna::WfaConjPattern* GetWfaConjPattern(Units::Frequency freq) const
		{ return CAntenna::GetWfaConjPattern(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna.get(), freq); }
	bool GetDualPolFourierPattern(const CAntenna::SWfaParams2& wfaParams2, unsigned long numAnts,
		Units::Frequency freq, bool interpolate, CAntenna::DualPolPattern& outPat) const;
	/////////////////////////// DF (end) ///////////////////////////////

	// Constant
	static const SSmsMsgAnt2AntType	SMSMSG_ANT_MAP[];
	static const size_t SMSMSG_ANT_MAP_CNT;

	// Data
	static std::string s_configFileName;

	 //Flag indicating incompatible antenna specified for the corresponding RF port.
	bool m_antOk[C_NUM_CONNECTED_VUHF_RF]; //true: Ok, false: Incompatible antenna.
	bool m_dualAux1;
	std::string  m_errAntStr[C_NUM_CONNECTED_VUHF_RF];
	SEeprom m_eeprom;
	std::string m_exeName; // Executable name
	std::string m_exeTime; // Executable mtime
	std::string m_filePath; // Path for all data files
	std::string m_ipAddress;
	std::string m_macAddress;
	bool m_mediaReadWrite;
	SMutableConfig m_mutableConfig;
	ProcParamsMap m_procParams;
	FreqPairVec m_restrictedFreq;
	std::string m_serverName;

	SAvdMeasParams m_avdMeasParams;
	std::map<ECableType, SCableCoeffs> m_cableCoeffs;
	unsigned long m_ddcExtraSamples;
	unsigned long m_directExtraClocks;
	STdoaValues m_tdoaStart;
	Units::Frequency m_hfMetricVLowFreqUnits; //Has the lowest vertical polarization metrics frequency supported.
	Units::Frequency m_hfMetricVHighFreqUnits;
	Units::Frequency m_hfDfVLowFreqUnits; //Has the lowest vertical polarization HF DF frequency supported.
	Units::Frequency m_hfDfVHighFreqUnits;
	static Units::Frequency s_shfIfFreq;
	FreqDepParams m_lightningProtectGain;
	std::vector<SGainMode> m_hfGainModes;
	Units::Frequency m_rf1VushfHLowFreq;
	Units::Frequency m_rf1VushfHHighFreq;
	Units::Frequency m_rf1VushfVLowFreq;
	Units::Frequency m_rf1VushfVHighFreq;
	FreqDepParams m_shfExtensionGain;
	bool m_shfExtLicenseValid;
	bool m_sysHasHorAnt;
	std::vector<SGainMode> m_vushfGainModes;
	Units::Frequency m_vushfMetricHLowFreq;
	Units::Frequency m_vushfMetricHHighFreq;
	Units::Frequency m_vushfMetricVLowFreq;
	Units::Frequency m_vushfMetricVHighFreq;
	Units::Frequency m_vushfDfHLowFreq;
	Units::Frequency m_vushfDfHHighFreq;
	Units::Frequency m_vushfDfVLowFreq;
	Units::Frequency m_vushfDfVHighFreq;
	Units::Frequency m_minFreq;		// Absolute min frequency, independent of antenna
	Units::Frequency m_maxFreq;		// Absolute max frequency, independent of antenna
	std::vector<SDemodBws> m_vushfDemodBws;
	std::vector<SDemodBws> m_hfDemodBws;
	SSysInfo m_sysinfo;
	SProcessorNodes m_processorNodes;
	bool m_ddrtype;
	std::map<Units::Frequency, STdoaValues> m_smsTdoaMap;
	bool m_fastseqenabled;

	static Units::FreqPair s_shfExtensionHorizRfFrequencyLimits;
	static Units::FreqPair s_shfExtensionVertRfFrequencyLimits;
	static std::string s_shfExtIPAddr;
	static LPCTSTR s_shfExtIPPort;

	////////////////////////////// DF //////////////////////////////////
	static const size_t maxDualPolCache = 10000;		// Large enough to hold 40 MHz / 6.25 KHz = 6400
	typedef std::map<Units::Frequency, CAntenna::DualPolPattern> MDualPolCache;
	mutable MDualPolCache dualPolCache;
	mutable std::deque<MDualPolCache::iterator> dualPolCacheList;
	/////////////////////////// DF (end) ///////////////////////////////

};

//////////////////////////////////////////////////////////////////////
/// <summary>
///Get Aux1 (RF2) upper limit frequency value.
/// </summary>
///
///@retval Has Aux1 upper limit frequency value.
///
inline Units::Frequency CConfig::GetAux1FreqHigh(void) const
{
	auto ant = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antenna.get();
	return(CAntenna::GetVHighFreq(ant));
}

//////////////////////////////////////////////////////////////////////
/// <summary>
///Get Aux1 (RF2) lower limit frequency value.
/// </summary>
///
///@retval Has Aux1 lower limit frequency value.
///
inline Units::Frequency CConfig::GetAux1FreqLow(void) const
{
	auto ant = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antenna.get();
	return(CAntenna::GetVLowFreq(ant));
}

//////////////////////////////////////////////////////////////////////
/// <summary>
///Get Slave RF2 upper limit frequency value.
/// </summary>
///
///@retval Has Slave RF2 upper limit frequency value.
///
inline Units::Frequency CConfig::GetSmplRF2FreqHigh(void) const
{
	auto ant = m_mutableConfig.vushfConfig.antCable[ERfPort::C_SLAVE_RF2].antenna.get();
	ASSERT(ant != m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antenna.get());
#ifdef CSMS_DEBUG
	ASSERT(CAntenna::GetVHighFreq(ant) == m_mutableConfig.vushfConfig.antCable[ERfPort::C_SLAVE_RF2].freqHigh );
#endif
	return(CAntenna::GetVHighFreq(ant));
}

//////////////////////////////////////////////////////////////////////
/// <summary>
///Get Slave RF2 lower limit frequency value.
/// </summary>
///
///@retval Has Slave RF2 lower limit frequency value.
///
inline Units::Frequency CConfig::GetSmplRF2FreqLow(void) const
{
	auto ant = m_mutableConfig.vushfConfig.antCable[ERfPort::C_SLAVE_RF2].antenna.get();
	return(CAntenna::GetVLowFreq(ant));
}

//////////////////////////////////////////////////////////////////////
/// <summary>
///Get the antenna and cable parameters that is connected to the RF port of
/// this csmsd system.
/// </summary>
///
/// <param name="rfInput">
///@param[in] RF input number; 0 => RF1; 1=>RF2; 2=>HF.
/// </param>
///
///@retval SAntennaAndCable of the specified port.
///
inline const CConfig::SMutableConfig::SAntennaAndCable &
		CConfig::GetThisSysAntCable(unsigned char rfInput) const
{
	return (static_cast<ERfPort>(rfInput) <= ERfPort:: C_RF2 ?
			m_mutableConfig.vushfConfig.antCable[rfInput] : m_mutableConfig.hfConfig.antCable);
}

//////////////////////////////////////////////////////////////////////
/// <summary>
///Indicate whether SHF Extension is supported.
/// </summary>
///
///@retval true: SHF Extension is supported.
/// 	    false: SHF Extension not supported.
///
inline bool CConfig::HasShfExt(void) const
{
	return(m_shfExtLicenseValid &&
	    (GetShfExtSetting() != EShfExt::NO_SHF_EXT));
}

//////////////////////////////////////////////////////////////////////
/// <summary>
///Verify whether the frequency specified is above the minimum of V/U/SHF
/// connected to csms RF1 input.
/// </summary>
///
/// <param name="ant">
///@param[in] Ant input that has polarization.
///	      Valid value are: ANT1 or ANT1H.
/// </param>
///
/// <param name="freq">
///@param[in] Frequency to be checked.
/// </param>
///
/// <returns>
///@return: true: Frequency is above VUHF minimum.
///	    false: Frequency is below or invalid ant value.
/// </returns>
///
inline bool CConfig::IsFreqAboveVuhfMin(SEquipCtrlMsg::EAnt ant, Units::Frequency freq) const
{
	ASSERT(ant == SEquipCtrlMsg::EAnt::ANT1 || ant == SEquipCtrlMsg::EAnt::ANT1H);
	bool isAbove = false;

	if(ant == SEquipCtrlMsg::EAnt::ANT1 && freq >= m_rf1VushfVLowFreq)
	{
		isAbove = true;
	}
	else if(ant == SEquipCtrlMsg::EAnt::ANT1H && freq >= m_rf1VushfHLowFreq)
	{
		isAbove = true;
	}

	return(isAbove);

}

//////////////////////////////////////////////////////////////////////
/// <summary>
///Verify whether the frequency specified is within the SHF Extension range
/// </summary>
///
/// <param name="freq">
///@param[in] "freq"  Frequency to be checked.
/// </param>
///
/// <returns>
///@retval true: Frequency is within SHF Extension range.
/// 	    false: Frequency is out of range.
/// </returns>
///
/// <remarks>
///@note: This static method does not differentiate vertical or horizontal
///		   polarization, as long as the frequency specified falls within the
///		   range of either vertical or horizontal polarization, a true will be
///		   returned.
/// </remarks>
///
inline bool CConfig::IsInShfExtFreqRange(Units::Frequency freq)
{
	bool inRange = false;

	if( (s_shfExtensionVertRfFrequencyLimits.second > 0) &&
		(freq >= s_shfExtensionVertRfFrequencyLimits.first &&
		 freq <= s_shfExtensionVertRfFrequencyLimits.second) )
	{
		inRange = true;
	}
	else if( !inRange && (s_shfExtensionHorizRfFrequencyLimits.second > 0) &&
			(freq >= s_shfExtensionHorizRfFrequencyLimits.first &&
			 freq <= s_shfExtensionHorizRfFrequencyLimits.second) )
	{
		inRange = true;
	}

	return( inRange );
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Check whether V/U/SHF antenna is specified.
///</summary>
///
/// <param name="ant">
///@param[in] Antenna value to be checked.
/// </param>
///
/// <returns>
///@retval true: V/U/SHF antenna is specified for system with multiple antenna
///				 at RF1 input.
/// 	   false: Not V/U/SHF antenna or not a multiple RF1 antenna input system.
/// </returns>
///
inline bool CConfig::IsVushfAntSpecified(SEquipCtrlMsg::EAnt ant) const
{
	bool isVushf = false;

	if(IsProcWithAntSelectionSwitch() &&
			(ant >= SEquipCtrlMsg::EAnt::ANT1_LPA && ant <= SEquipCtrlMsg::EAnt::ANT1_647H_MON))
	{
		isVushf = true;
	}

	return(isVushf);
}
