/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2014-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "Config.h"
#include "Singleton.h"
#include "Units.h"

class CWatchdog
{
	friend class CSingleton<CWatchdog>;

public:
	// Types
	// TODO: This class may not be used in CSMS.
	class CWatchdogGuard
	{
	public:
		CWatchdogGuard(CWatchdog& watchdog, const std::string& name) : pingSource(watchdog.RegisterPingSource(name)), watchdog(watchdog) {}
		~CWatchdogGuard(void) { watchdog.UnregisterPingSource(pingSource); }

	private:
		int pingSource;
		CWatchdog& watchdog;
	};

	struct SStatus
	{
		bool watchdogEnabled;
	};

	// Functions
	SStatus GetStatus(void) const { std::lock_guard<std::mutex> lock(m_mutex); return m_status; }
	bool IsWatchUsed(void) const { return(m_useWatchdog); }
	void Ping(int source);
	int RegisterPingSource(const std::string& name);
	void UnregisterPingSource(int source);

private:
	// Constants
	static constexpr const char* watchdogDevice = "/dev/watchdog0";
	static constexpr const DATE FAIL_INTERVAL = 20.0 / Units::SECONDS_PER_DAY;
	static constexpr const size_t MAX_WATCHDOG_REPORTS = 10;
	static constexpr const unsigned int THREAD_TIMEOUT = 10000; // ms

	// Types
	struct SPingSourceValue
	{
		bool pinged;
		bool preTriggered;
		std::string source;
	};

	typedef std::map<int, SPingSourceValue> PingSourceMap;

	// Functions
	CWatchdog(void);
	~CWatchdog(void);
	static bool IsDebuggerPresent(void);
	void Stop(void);
	void TimeoutThread(void);

	// Data
	CSingleton<const CConfig> m_config;
	int m_fileDesc;
	volatile DATE m_lastOkTime;
	mutable std::mutex m_mutex;
	int m_nextHandle;
	PingSourceMap m_pingSourceMap;
	SStatus m_status;
	bool m_stopThread;
	std::condition_variable m_stopThreadCond;
	mutable std::mutex m_stopThreadMutex;
	std::thread m_thread;
	bool m_useWatchdog;
	std::string m_watchdogLog;
};
