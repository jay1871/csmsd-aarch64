/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "ErrorCodes.h"
#include "Ne10Vec.h"
#include "VCPMsg.h"
#include "Config.h"
#include "Singleton.h"
class CVCPCtrl
{
public:
	// Functions

protected:
	// Constants

	// Types

	// Functions
	CVCPCtrl(void);
	virtual ~CVCPCtrl(void);

	ErrorCodes::EErrorCode GetRegValues(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);
	ErrorCodes::EErrorCode GetReceiverData(_In_ const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);
	ErrorCodes::EErrorCode GetRxAtten(_In_ const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);
	ErrorCodes::EErrorCode GetRxCalgen(_In_ const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);
	ErrorCodes::EErrorCode GetRxCapabilities(_In_ const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);
	ErrorCodes::EErrorCode GetRxMoc(_In_ const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);
	ErrorCodes::EErrorCode GetRxOcxo(_In_ const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);
	ErrorCodes::EErrorCode GetRxTemps(_In_ const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);
	ErrorCodes::EErrorCode RxTune(_In_ const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);
	ErrorCodes::EErrorCode SetRxAtten(_In_ const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);
	ErrorCodes::EErrorCode SetRxCalgen(_In_ const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);
	ErrorCodes::EErrorCode GetSpectrumData(_In_ const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);
	ErrorCodes::EErrorCode GetDigiTemps(_In_ const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);
	ErrorCodes::EErrorCode GetDigiVolts(_In_ const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);
	ErrorCodes::EErrorCode GetAntennaSwitch(_In_ const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);
	ErrorCodes::EErrorCode SetAntennaSwitch(_In_ const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp);

private:
	// Types
	struct SVcpSpectrum	// used for spectrum averaging
	{
		SVcpSpectrum(void) : haveGetSpectrumData(false), haveRxTune(false) {}
		bool operator==(const SVCPMsg::SRxTune& s) const
		{
			return (haveRxTune &&
				rxTune.freq == s.freq && rxTune.fpi == s.fpi && rxTune.bw == s.bw && rxTune.rfInput == s.rfInput);
		}
		bool operator!=(const SVCPMsg::SRxTune& s) const
		{
			return !(*this == s);
		}
		bool operator==(const SVCPMsg::SGetSpectrumData& s) const
		{
			// Not quite a standard == operator!
			return (haveGetSpectrumData &&
				getSpectrumData.average == s.average && getSpectrumData.gainEqualization == s.gainEqualization);
		}
		bool operator!=(const SVCPMsg::SGetSpectrumData& s) const
		{
			return !(*this == s);
		}

		bool haveGetSpectrumData;
		bool haveRxTune;
		Ne10F32Vec spect;
		SVCPMsg::SGetSpectrumData getSpectrumData;
		SVCPMsg::SRxTune rxTune;
	};

	// Data
	SVcpSpectrum m_vcpSpectrum;		// Only one client can be doing spectrum at a time
	CSingleton<const CConfig> m_config;
	unsigned char rxbwused;

};

