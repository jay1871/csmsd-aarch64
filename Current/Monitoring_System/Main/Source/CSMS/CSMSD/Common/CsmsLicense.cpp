/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include <string>
#include <fstream>
#include <sstream>
#include "CsmsLicense.h"
#include "Sha256.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CCsmsLicense::CCsmsLicense() :
	m_appSection(false),
	m_has80MHz(false),
	m_hasHf(false),
	m_hasPrecision(false),
	m_hasShfExt(false),
	m_initialized(false),
	m_valid(false),
	m_vushfMHz(0)
{
	// Get the path to the license file
	std::string filePath(TCIPaths::configDir);	//	"/media/tci/csms/etc";

	std::ifstream file((filePath + "CsmsLicense.lic").c_str());

	if (!file)
	{
		std::string status("Failed to open CsmsLicense.lic: ");

#ifdef __GNUG__
		auto errbuf = strerror(errno);
#else
		char errbuf[256];
		if (strerror_s(errbuf, errno) == 0)
#endif
		{
			status += errbuf;
		}
		m_status.push_back(status);
		return;
	}

	// Validate the license file
	if (!SecurityCheck(file))
	{
		return;
	}
	file.close();

	// Check that file contains the [CsmsServer] key
	if (!m_appSection)
	{
		std::string status("Unable to read section [CsmsServer] from license file");
		m_status.push_back(status);
		return;
	}
	m_initialized = true;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CCsmsLicense::~CCsmsLicense()
{
	return;
}

//////////////////////////////////////////////////////////////////////
//
// Check for licenses
//
bool CCsmsLicense::Check(void) const
{
	return m_valid;
}

//////////////////////////////////////////////////////////////////////
//
// Check machine id
//
bool CCsmsLicense::CheckMachineId(const SMachineId& machineId)
{
	char machineIdBuf[40];
#ifdef __GNUG__
	if (snprintf(machineIdBuf, 40, "%06lx-%08lx:%08lx-%08lx", machineId.dna[0], machineId.dna[1], machineId.max10Id[0], machineId.max10Id[1]) <= 0)
#else
	if (sprintf_s(machineIdBuf, "%06lx-%08lx:%08lx-%08lx", machineId.dna[0], machineId.dna[1], machineId.max10Id[0], machineId.max10Id[1]) <= 0)
#endif
	{
		std::string status("License failed to encode machine id");
		m_status.push_back(status);
		return false;
	}
	std::string machineIdStr(machineIdBuf);

	std::string status("Machine Id: ");
	status += machineIdStr;
	m_status.push_back(status);

	if (!m_initialized)
	{
		std::string status("License failed to initialize. Request new license.");
		m_status.push_back(status);
		return false;
	}

	if (m_valid)
		return true;

	if (machineIdStr != m_machineId)
	{
		std::string status("Invalid MachineId in license. Request new license.");
		m_status.push_back(status);
		return false;
	}
	m_valid = true;
	return true;
}

//////////////////////////////////////////////////////////////////////
//
// Hash a string
//
void CCsmsLicense::HashToString(const std::string& secData, std::string& hashString)
{
	std::vector<Sha256::BYTE> b(secData.begin(), secData.end());
	Sha256::BYTE buf[SHA256_BLOCK_SIZE];

	Sha256::SHA256_CTX ctx;
	Sha256::sha256_init(&ctx);
	Sha256::sha256_update(&ctx, &b[0], secData.length());
	Sha256::sha256_final(&ctx, buf);

	char strbuf[2 * SHA256_BLOCK_SIZE + 1];
	for (size_t i = 0; i < SHA256_BLOCK_SIZE; ++i)
	{
#ifdef __GNUG__
		if (snprintf(&strbuf[2 * i], 3, "%02x", buf[i]) <= 0)
#else
		if (sprintf_s(&strbuf[2 * i], 3, "%02x", buf[i]) <= 0)
#endif
		{
			strbuf[2 * i] = '*';
			strbuf[2 * i + 1] = '*';
		}
	}
	hashString.assign(strbuf);

	return;
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Init pre-read all available licenses.
///</summary>
///
void CCsmsLicense::Init(void)
{
      m_has80MHz = Read80MHzLicense();
      m_hasHf = ReadHFLicense();
      m_hasPrecision = ReadPrecisionLicense();
      m_hasShfExt = ReadShfExtLicense();
      m_vushfMHz = ReadVUHFLicense();
      return;
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Return whether 80 MHz bandwidth License is valid.
///</summary>
///
///@retval true: This unit has 80 MHz bandwidth License.
/// 	    false: Has no 80 MHz bandwidth License.
///
bool CCsmsLicense::Read80MHzLicense(void) const
{
	if (!m_valid) return false;

	auto it = m_keyVals.find("Has80MHzRadio");
	if (it == m_keyVals.end() || it->second.empty() || it->second == "0")
		return false;

	return true;
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Return whether HF License is valid.
///</summary>
///
///@retval true: This unit has HF License.
/// 	    false: Has no HF License.
///
bool CCsmsLicense::ReadHFLicense(void) const
{
	if (!m_valid) return false;

	auto it = m_keyVals.find("HasHF");
	if (it == m_keyVals.end() || it->second.empty() || it->second == "0")
		return false;

	return true;
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Return whether Precision Timestamp License is valid.
///</summary>
///
///@retval true: This unit has Precision Timestamp License.
/// 	    false: Has no Precision Timestamp License.
///
bool CCsmsLicense::ReadPrecisionLicense(void) const
{
	if (!m_valid) return false;

	auto it = m_keyVals.find("HasPrecisionTimestamp");
	if (it == m_keyVals.end() || it->second.empty() || it->second == "0")
		return false;

	return true;
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Return whether SHF Extension License is valid.
///</summary>
///
///@retval true: This unit has SHF Extension License.
/// 	    false: Has no SHF Extension License.
///
bool CCsmsLicense::ReadShfExtLicense(void) const
{
	if (!m_valid) return false;

	auto it = m_keyVals.find("HasSHFExt");
	if (it == m_keyVals.end() || it->second.empty() || it->second == "0")
		return false;

	return true;
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Return the maximum VUHF supported license.
///</summary>
///
///@retval Has the maximum VUHF frequency in MHz.
/// 	   0: used default maximum VUHF frequency.
///
unsigned long CCsmsLicense::ReadVUHFLicense(void) const
{
	if (!m_valid) return false;

	auto it = m_keyVals.find("HasVUHF");
	if (it == m_keyVals.end() || it->second.empty())
		return 0;

	try
	{
		auto mhz = std::stoul(it->second);
		return mhz;
	}
	catch(std::invalid_argument&)
	{
	}
	return 0;
}


//////////////////////////////////////////////////////////////////////
//
// Validate the integrity of the license file
//
bool CCsmsLicense::SecurityCheck(std::ifstream& file)
{
	std::string line;

	// Read contents of license file up to security code
	std::string secCode;
	std::string secData;
	bool secSection = false;

	while(!std::getline(file, line).eof())
	{
		line.erase(line.find_last_not_of(" \t\n\r") + 1);
		if (line.find("SecurityCode", 0, 12) == 0)
		{
			secCode = line.substr(13);
		}
		else if (line.find("SignDate", 0, 8) == std::string::npos)
		{
			secData += line;
			if (line.find("[CsmsServer]", 0, 12) == 0)
			{
				m_appSection = true;
			}
			else if (line.find("[Security]", 0, 10) == 0)
			{
				secSection = true;
			}
			else if (secSection && line.find("MachineId", 0, 9) == 0)
			{
				m_machineId = line.substr(10);
			}
			else if (m_appSection && !secSection)
			{
				std::istringstream lineStream(line);
				std::string key;
				std::string val;
				std::getline(lineStream, key, '=');
				key.erase(key.find_last_not_of(" \t\n\r") + 1);
				std::getline(lineStream, val);
				if (!key.empty())
				{
					m_keyVals[key] = val;
//					m_status.push_back(key + " = " + val);
				}
			}
		}
		else
		{
			m_status.push_back(line);
		}
	}

	if (secCode.empty())
	{
		std::string status("Unable to parse SecurityCode from license file");
		m_status.push_back(status);
		return false;
	}

	// Append my private key to file data
	secData += MEMERR;

	// Hash the file+private_key
	std::string stringbuf;
	HashToString(secData, stringbuf);

	// Check the hash value against file's security code
	if (secCode != stringbuf)
	{
		std::string status("Invalid License SecurityCode");
		m_status.push_back(status);
		return false;
	}

	return true;
}

