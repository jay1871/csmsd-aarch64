/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2017 TCI International, Inc. All rights reserved              *
**************************************************************************/
#include "stdafx.h"
#include <fstream>
#include "PatternDatFile.h"

void CPatternDatFile::Load(LPCSTR patternFile, int numAnts, WfaConjPatterns& m_wfaConjPatterns)
{
	std::ifstream file(patternFile, std::ios::in | std::ios::binary);

	if (!file)
	{
		TRACE("File %s not found!", patternFile);
		throw std::runtime_error("patternFIle is missing");
	}

	struct SRecord1
	{
		int numPols;
		int zSym;
		int numEls;
		int numFileAz;
		float azStart;
		float azInc;
		double freqMhz;
		int nfc;
	};
	struct SPolar
	{
		float mag;		// scaled
		float phase;	// radians
	};

	bool evenSymmetry = false;

	while (!file.eof())
	{
		// Read parameters
		SRecord1 record1;
		file.read(reinterpret_cast<char*>(&record1), sizeof(record1));
		if (file.eof()) break;

		if (file.gcount() != sizeof(record1))
		{
			throw std::runtime_error("Invalid data in patternFile header record (size)");
		}

		ASSERT(record1.azStart == 0.0f); // TODO - fix this

		int numAz = int(360.0 / record1.azInc + 0.5);
		ASSERT(record1.numFileAz == numAz / 2 + 1 || record1.numFileAz == numAz || record1.numFileAz == numAz + 1);

		if (m_wfaConjPatterns.empty())
		{
			if (record1.numFileAz != numAz && record1.numFileAz != numAz + 1)
			{
				evenSymmetry = true;
			}
		}
		else
		{
			if ((evenSymmetry && record1.numFileAz == int(360.0 / record1.azInc + 0.5)) ||
				(!evenSymmetry && record1.numFileAz < int(360.0 / record1.azInc + 0.5)))
			{
				TRACE(_T("AntParms not consistent\n"));
				throw std::runtime_error("Invalid data in patternFile header record (inconsistent)");
			}
		}

		// Create an entry in the map with the vectors all sized appropriately
		WfaConjPattern pat(numAz);
		for (int i = 0; i < numAz; ++i)
		{
			pat[i].resize(numAnts);
		}
		WfaConjPattern& conjPattern = (m_wfaConjPatterns[Units::Frequency(record1.freqMhz * 1e6 + 0.5)] = pat);

		// Read in voltage vectors
		std::vector<SPolar> record2(record1.numFileAz);	// in wira (cartesian) coordinates

		file.read(reinterpret_cast<char*>(&record2[0]), record1.numFileAz * sizeof(SPolar));

		if (file.gcount() != int(record1.numFileAz * sizeof(SPolar)))
		{
			throw std::runtime_error("Invalid data in patternFile header record (voltage size)");
		}

		for (int wiraAz = 0; wiraAz < record1.numFileAz; ++wiraAz)
		{
			int az = (wiraAz == 0 ? 0 : numAz - wiraAz);	// convert to compass coords
			if (wiraAz >= numAz)
				continue;

			conjPattern[az][0].r = record2[wiraAz].mag * cos(record2[wiraAz].phase);
			conjPattern[az][0].i = -record2[wiraAz].mag * sin(record2[wiraAz].phase);

			// Expand symmetries
			if (evenSymmetry && az != 0 && az != numAz / 2)
			{
				conjPattern[numAz - az][0] = conjPattern[az][0];
			}

			for (int ant = 1; ant < numAnts; ant++)
			{
				conjPattern[(az + ant * numAz / numAnts) % numAz][ant] = conjPattern[az][0];

				if (evenSymmetry && az != 0 && az != numAz / 2)
				{
					conjPattern[(numAz - az + ant * numAz / numAnts) % numAz][ant] = conjPattern[az][0];
				}
			}
		}

		for (int az = 0; az < numAz; ++az)
		{
			auto& pSrc = conjPattern[az];
			float norm;
			double temp = 0;
			for (size_t i = 0; i < pSrc.size(); ++i)
			{
				temp += (pSrc[i].r * pSrc[i].r + pSrc[i].i * pSrc[i].i);
			}
			norm = static_cast<float>(sqrt(temp));
			for (size_t i = 0; i < pSrc.size(); ++i)
			{
				pSrc[i].r /= norm;
				pSrc[i].i /= norm;
			}
		}
	}
	return;
}

