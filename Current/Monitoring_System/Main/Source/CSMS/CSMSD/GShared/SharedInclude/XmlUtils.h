#pragma once


#include <MsXml.h>
#include <tchar.h>
#include <vector>
#include <map>


#pragma comment(lib, "User32.lib")			// for ::MessageBox (maybe do not need this if stop using ::MesasgeBox)
#pragma comment(lib, "Ole32.lib")			// for CoInitialize, CoUninitialize
#pragma comment(lib, "OleAut32.lib")		// for SysAllocString
#pragma comment(lib, "Shell32.lib")			// for SHCreateDirectoryEx
#pragma comment(lib, "Shlwapi.lib")			// for PathFileExists


namespace TciXmlUtils
{
	#if defined(UNICODE) || defined(_UNICODE)
		#define tchar		wchar_t
		#define tstring		std::wstring
		#define tout		std::wcout
		#define tin			std::wcin
		#define tostream	std::wostream
		#define tofstream	std::wofstream
		#define tifstream	std::wifstream
		#define tfstream	std::wfstream
	#else
		#define tchar		char
		#define tstring		std::string
		#define tout		std::cout
		#define tin			std::cin
		#define tostream	std::ostream
		#define tofstream	std::ofstream
		#define tifstream	std::ifstream
		#define tfstream	std::fstream
	#endif


	enum EInputFileType : unsigned short
	{
		// These are the type of input XML files that we currently support
		INPUTFILETYPE_UNKNOWN		= 0, 
		INPUTFILETYPE_GSP_REQUEST	= 1, 
		INPUTFILETYPE_ORDER_REQUEST	= 2
	};

	enum ESiteType : unsigned short
	{
		SITETYPE_UNKNOWN	= 0, 
		SITETYPE_FIXED		= 1, 
		SITETYPE_MOBILE		= 2
	};

	enum EMeasurementStatus : unsigned short
	{
		MEAS_SUCCESS = 0, 
		MEAS_WARNING = 1, 
		MEAS_ERROR   = 2
	};

	enum EModulationType : unsigned short
	{
		MODULATION_AM = 0, 
		MODULATION_FM = 1, 
		MODULATION_PM = 2
	};

	static EInputFileType DetermineInputFile(TCHAR* inputFilename)
	{
		EInputFileType inputFileType = INPUTFILETYPE_UNKNOWN;

		if (towupper(inputFilename[0]) == 'G' && towupper(inputFilename[1]) == 'S' && towupper(inputFilename[2]) == 'P' && inputFilename[3] == '-')
		{
			inputFileType = INPUTFILETYPE_GSP_REQUEST;			// GSP (Get System Parameters) Request
		}
		else if (towupper(inputFilename[0]) == 'O' && towupper(inputFilename[1]) == 'R' && inputFilename[2] == '-')
		{
			inputFileType = INPUTFILETYPE_ORDER_REQUEST;		// OR(der) Request
		}

		return inputFileType;
	}

	static tstring Format(const tchar* frmt, ...)
	{          
		if (frmt != NULL)
		{
			va_list marker;
 
			// Initialize variable arguments
			va_start(marker, frmt);
 
			// Get formatted string length adding one for the NULL
			size_t len = _vsctprintf(frmt, marker) + 1;
               
			// Create a char vector to hold the formatted string.
			std::vector<tchar> buffer(len, _T('\0'));
			int written = _vsntprintf_s(&buffer[0], buffer.size(), len, frmt, marker);     
               
			// Reset variable arguments
			va_end(marker); 
 
			return &buffer[0];
		}
 
		return _T("");
	}


#if defined(UNICODE) || defined(_UNICODE)

	static /*std::string*/ tstring BstrToStdString(BSTR bstr)
	{
		/*std::string*/ tstring str;
		str = TciXmlUtils::Format(_T("%s"), bstr);
		return str;
	}

#else

	// Convert a BSTR to a std::string. 
	static /*std::string&*/ tstring& BstrToStdString(const BSTR bstr, /*std::string&*/ tstring& dst, int cp = CP_UTF8)
	{
		if (!bstr)
		{
			// Define NULL functionality. I just clear the target.
			dst.clear();
			return dst;
		}

		// Request content length in single-chars through a terminating
		//  nullchar in the BSTR. note: BSTR's support embedded nullchars,
		//  so this will only convert through the first nullchar.
		int res = WideCharToMultiByte(cp, 0, bstr, -1, NULL, 0, NULL, NULL);
		if (res > 0)
		{
			dst.resize(res);
			WideCharToMultiByte(cp, 0, bstr, -1, &dst[0], res, NULL, NULL);
		}
		else
		{    // No content. Clear target
			dst.clear();
		}
		return dst;
	}

	// Conversion with temp.
	static tstring BstrToStdString(BSTR bstr, int cp = CP_UTF8)
	{
		tstring str;
		BstrToStdString(bstr, str, cp);
		return str;
	}

	static BSTR BstrFromTchar(const TCHAR* tcharString)	// tcharString must be null-terminated, and no more than 512 chars (including null)
	{
		WCHAR buffer[512];
		ZeroMemory(buffer, sizeof(buffer));
		MultiByteToWideChar(CP_ACP, MB_ERR_INVALID_CHARS, tcharString, strlen(tcharString), buffer, sizeof(buffer)-1);
		return SysAllocString(buffer);	// todo when to call SysFreeString?
	}

#endif	// defined(UNICODE) || defined(_UNICODE)



	class CTciXmlResp
	{
	public:
		CTciXmlResp::CTciXmlResp(const TCHAR* inputFilename, const TCHAR* outputFilename);
		virtual CTciXmlResp::~CTciXmlResp();


		virtual void	WriteResponse() = 0;	// derived class must override this to create it's approrpriate XML Response file

		virtual tstring GetOutputFile() { return _outputXmlFilename; }


		// Mutators
		void	SiteType(TciXmlUtils::ESiteType siteType)			{ _siteType = siteType; }
        void	SiteLocation(double siteLat, double siteLong);
		void	SiteHeight(signed long siteHeight)					{ _siteHeight = siteHeight; }
        void    OrderSenderDefault(const TCHAR* orderSender)        { _tcsncpy_s(_orderSenderDefault, _countof(_orderSenderDefault), orderSender, _TRUNCATE); }
        void    OrderSenderPC(const TCHAR* orderSenderPc)           { _tcsncpy_s(_orderSenderPc, _countof(_orderSenderPc), orderSenderPc, _TRUNCATE); }
        void    OrderAddressee(const TCHAR* orderAddressee)         { _tcsncpy_s(_orderAddressee, _countof(_orderAddressee), orderAddressee, _TRUNCATE); }
        

		/////////////////////////////////////////////////////////////////////////////////////////////
		void	MeasurementStatus(EMeasurementStatus measStatus, const TCHAR* measStatusMsg=NULL);
		void	MeasurementError(const TCHAR* measError)			{ _tcsncpy_s(_measError, _countof(_measError), measError, _TRUNCATE); }
		/////////////////////////////////////////////////////////////////////////////////////////////

		// Accessors
		TciXmlUtils::ESiteType SiteType()		{ return _siteType; }
		double	SiteLong()						{ return _siteLong; }
		double	SiteLat()						{ return _siteLat; }
		signed long SiteHeight()				{ return _siteHeight; }
		/////////////////////////////////////////////////////////////////////////////////////////////
		EMeasurementStatus MeasurementStatus()	{ return _measStatus; }
		TCHAR*	MeasurementError()				{ return _measError; }
		/////////////////////////////////////////////////////////////////////////////////////////////


	protected:
		// Reading information about the order, from the given (input/request) XML file
		virtual bool readOrderInfo(TCHAR* inputFilename) = 0;

		bool	loadDOMRaw(TCHAR* inputXmlFilename);
		bool	getFieldValue(TCHAR* pFieldToRead, TCHAR* pFieldValue, short sizeFieldValue, TCHAR* pParentOfField=NULL);
		bool	readSection(const TCHAR* pFieldToRead, tstring& sectionToFill, TCHAR* pParentOfField=NULL);


		// Writing to the (output/results) XML file
		void	writeSection(const TCHAR* pSection, tstring& sectionContents, short level);
		void	writeSectionContents(tstring& sectionContents, short level);
		void	writeTag(const TCHAR* tagName, tstring& tagValue, short level);
		void	writeTag(const TCHAR* tagName, TCHAR* tagValue, short level);
		void	writeComment(const TCHAR* comment, short level);
		void	writeTabs(short level);
		void	writeEol();
		bool	writeOutput(const TCHAR* strToBeWritten);

		// ORDER_DEF section
		void	writeOrderType(TCHAR* tagValue, TCHAR* tagValueDefault, short level);
		void	writeOrderName(TCHAR* tagValue, TCHAR* tagValueDefault, short level);
		void	writeOrderSender(TCHAR* tagValue, TCHAR* tagValueDefault, short level);
		void	writeOrderSenderPc(TCHAR* tagValue, TCHAR* tagValueDefault, short level);
		void	writeOrderCreator(TCHAR* tagValue, TCHAR* tagValueDefault, short level);
		void	writeOrderAddressee(TCHAR* tagValue, TCHAR* tagValueDefault, short level);
		void	writeOrderVer(TCHAR* tagValue, TCHAR* tagValueDefault, short level);

		// ACT_DEF section
		void	writeActDef(TCHAR* orderId, short level);
		void	writeAcdUserString(TCHAR* tagValue, short level);
		void	writeAcdErr(short level);
		void	writeAcdErrCode(TCHAR* tagValue, short level);
		void	writeAcdErrMessage(short level);


	protected:
		tstring _inputXmlFilename;
		tstring _outputXmlFilename;

		// Interface to XML DOM (document) - In-memory copy of XML file
		IXMLDOMDocument *_pXMLDom;

		// For general-purpose (formatting strings to be written to XML files)
		static const size_t	_bufSize = 512;
		TCHAR		_tcharStr[_bufSize];

		bool		_validXmlInputFile;

		// TCI Site information (used in both the GSP and ORder response XML files)
		TciXmlUtils::ESiteType	_siteType;	// Unknown, Fixed Site, Mobile Site
		double		_siteLong;
		double		_siteLat;
		signed long	_siteHeight;	// Height above/below sea-level (in meters)


		// Read from the input (request) GSP- or OR- XML file some field values, to be 
		//	echoed back in the output (response) GSP or OR- XML file
		static const short	_sizeField = 32+1;
		TCHAR		_orderType[_sizeField];
		TCHAR		_orderName[_sizeField];
        TCHAR		_orderSender[_sizeField];
        TCHAR		_orderSenderDefault[_sizeField];
		TCHAR		_orderSenderPc[_sizeField];
		TCHAR		_orderCreator[_sizeField];
		TCHAR		_orderAddressee[_sizeField];
		TCHAR		_orderVersion[_sizeField];


		// Used only to hold onto the orderId string (used in several places when creating response XML files)
		static const short	_sizeOrderId = 64;
		TCHAR		_orderId[_sizeOrderId];

		// Used for ORder response
		EMeasurementStatus	_measStatus;		// will eventually be 'S' (success), 'W' (warning) or 'E' (error). For now can only be S or E (do we want warning??)
		TCHAR				_measError[256+1];	// if there is a measurement error, this string indicates why (will be written to a result XML file in the <ACD_DEF> section's <ACD_ERR_MESS> field)


		// Workstation's time zone information (used when writing formatted date/time to result XML file)
		bool	_timeZonePlus;		// true: time zone is UTC + some time, false: time zone is UTC - some time
		LONG	_timeZoneHours;		// Number of whole hours difference from UTC
		LONG	_timeZoneMinutes;	// Number of minutes difference from UTC (after subtracting the number of hours)


	private:
		HRESULT createAndInitDOM(IXMLDOMDocument **ppDoc);

		#if defined(UNICODE) || defined(_UNICODE)
			HRESULT variantFromString(PCWSTR wszValue, VARIANT &Variant);
		#else
			HRESULT variantFromString(const TCHAR* wszValue, VARIANT &Variant);
		#endif

#ifndef SAFE_RELEASE
		#define SAFE_RELEASE(p)     if ((p)) { (p)->Release(); (p) = NULL; }
#endif

		// For writing back to the result XML file (GSP or OR response)
		HANDLE _hOutputFile;
	};



	class CTciXmlGspResp : public CTciXmlResp
	{
	public:
		CTciXmlGspResp(TCHAR* inputFilename, const TCHAR* outputFilename=NULL);
		~CTciXmlGspResp(void);


		// Top/High level GSP Response File
		virtual void WriteResponse();	// derived class must override this to create it's approrpriate XML Response file (in this case, create the GSP response XML file)


		// Mutators
		void	SiteMinFreq(unsigned __int64 minFreq)	{ _siteMinFreq = minFreq; }
		void	SiteMaxFreq(unsigned __int64 maxFreq)	{ _siteMaxFreq = maxFreq; }
		void	StationName(const TCHAR* stationName)	{ _tcsncpy_s(_stationName, _countof(_stationName), stationName, _TRUNCATE); }
		void	StoreMeasurementBw(tstring measBw)		{ _measBandwidths.push_back(measBw); } // measurement bandwidth string includes the value and the units (e.g., "200 kHz")
        void    StationCapabilities(SMetricsMsg::SGetBandResp& capabilities);
        void    StationLastConnectDateTime(SYSTEMTIME lastConnectDateTime) { _lastConnectDateTime = lastConnectDateTime; }

		// Accessors
		unsigned __int64 SiteMinFreq()		{ return _siteMinFreq; }
		unsigned __int64 SiteMaxFreq()		{ return _siteMaxFreq; }
		TCHAR*	StationName()				{ return _stationName; }


	protected:
		// Reading from the GSP (input/request) XML file
		virtual bool readOrderInfo(TCHAR* inputFilename);


		// Writing to the GSP (output/results) XML file
		void	writeGspResponse();
		void	writeOrderDef(TCHAR* orderId, short level);

		// MONSYS_STRUCTURE section
		void	writeMonSysStructure(short level);
		void	writeMssStType(short level);
		void	writeMssLong4Parts(short level);
		void	writeMssLong4Parts(double siteLong, short level);
		void	writeMssLat4Parts(short level);
		void	writeMssLat4Parts(double siteLat, short level);
		void	writeMssLong(short level);
		void	writeMssLat(short level);
		void	writeMssPaths(short level);
        void	writeMssDateTime(short level);

		// MSS_PATHS section
		void	writeMpDev(short level);
		void	writeMpName(short level);
		void	writeMpFrL(short level);
		void	writeMpFrU(short level);

		// MP_DEV section
		void	writeD_IfSpan(TCHAR* tagValue, short level);
		void	writeD_DfTime(TCHAR* tagValue, short level);
		void	writeD_Preamp(TCHAR* tagValue, short level);
		void	writeD_Azimuth(TCHAR* tagValue, short level);
		void	writeD_Height(TCHAR* tagValue, short level);
		void	writeD_Elevation(TCHAR* tagValue, short level);
		void	writeD_Polarization(TCHAR* tagValue, short level);

        CString formatBandwidthToString(double bandwidthHz);

	protected:
		// Fields specific for the GSP response XML file (will be formatted into text before writing to XML file)
		// Use mutators/accessors for access to these
		unsigned __int64 _siteMinFreq;	// Site's mimimum frequency that can be tasked
		unsigned __int64 _siteMaxFreq;	// Site's maximum frequency that can be tasked
		TCHAR		_stationName[30+1];	// when writing back to order response xml file, limited to 30 characters

        SMetricsMsg::SGetBandResp	_serverCapabilities;	// Server capability info from the server.
        SYSTEMTIME _lastConnectDateTime;

		std::vector<tstring> _measBandwidths;	// Store a list of measurement bandwidths supported by a (TCI) server
		typedef std::vector<tstring>::iterator MEASBW_ITER;
	};



	// CTciXmlOrderResp is a class designed to read and process an OR(der) XML file.
	//	This file OR(der) can be generated by the SMS4DC application (using the Argus menus).
	//	This file defines an order, asking for measurement(s) from an (SMS) server to 
	//	be run. 
	//
	// This class will read an input OR(der) file, and hold onto information necessary to 
	//	run measurements (to a TCI SMS server). These information are accessed through 
	//	various accessor functions.
	//
	// This class can also create an output OR(der) file (is that what it's called?), which 
	//	contains results of measurments requested in the input OR(der) file. TO BE DONE!!
	// Should we use MSXML to create an XML tree in memory (DOM), and then just write, or 
	// should we just write to the file using our own code.
	class CTciXmlOrderResp : public CTciXmlResp
	{
	public:
		CTciXmlOrderResp(TCHAR* inputFilename, const TCHAR* outputFilename=NULL);
		~CTciXmlOrderResp(void);

		// Top/High level Order (measurement results) Response File
		virtual void WriteResponse();	// derived class must override this to create it's approrpriate XML Response file (in this case, create the Order response XML file)


		// The client calls these functions to store measurement data
		void	StoreMeasurementDatum_Fs(	unsigned __int64 measFreqHz, SYSTEMTIME measTime, double measDataFs, TCHAR* measDataFsUnit);
		void	StoreMeasurementDatum_Df(	unsigned __int64 measFreqHz, SYSTEMTIME measTime, double measDataDfBearing, short measDataDfQuality);
		void	StoreMeasurementDatum_FrOff(unsigned __int64 measFreqHz, SYSTEMTIME measTime, double measDataFreqOffHz);
		void	StoreMeasurementDatum_Bw(	unsigned __int64 measFreqHz, SYSTEMTIME measTime, double measDataBwKhz);
		void	StoreMeasurementDatum_Mod(	unsigned __int64 measFreqHz, SYSTEMTIME measTime, unsigned long measDataMod, TciXmlUtils::EModulationType modulationType);


		// Is the OR(der) request from SMS4DC for a S(ingle) frequency, a frequency L(ist), or frequency R(ange).
		struct SFrequencyMode
		{
			SFrequencyMode()
				: mode(UNKNOWN)
			{
				memset(&strMode, 0, sizeof(strMode));
				memset(&freqs, 0, sizeof(freqs));
			}

			enum EFrequencyMode : unsigned short
			{
				UNKNOWN = 0, 
				SINGLE	= 1, 	// Perform measurements(s) on a single frequency ('FFM')
				RANGE	= 2, 	// Perform measurements(s) on a range of frequencies F1-F2, Step ('Scan')
				LIST	= 3		// Perform measurements(s) on a list of frequencies ('Frequency List Scan') - maybe don't support this for now?
			};

			TCHAR strMode[2];	// Taken directly from input OR(der) XML file from SMS4DC (<FREQ_PAR_MODE> single TCHAR, null-terminated). Probably don't need to maintain this.
			EFrequencyMode mode;// Internal representation of frequency mode
			union
			{
				// Depending on frequency mode, use various members of this union
				struct SFrequencySingle
				{
					unsigned __int64	freqSingleHz;
				};

				struct SFrequencyRange
				{
					unsigned __int64	freqLowerHz;
					unsigned __int64	freqUpperHz;
					unsigned long		stepSizeHz;
				};

				struct SFrequencyList
				{
					// Read/process up to 100 frequencies from the input OR(der) XML file from SMS4DC - maybe don't support this for now?
					unsigned short numFreq;
					unsigned __int64 listOfFreqsHz[100];
				};

				SFrequencySingle freqSingle;	// If a single frequency
				SFrequencyRange  freqRange;		// If a frequency range
				SFrequencyList   freqList;		// If a frequency list - maybe don't support this for now?
			} freqs;
		};

		struct SMeasurementTypes
		{
			SMeasurementTypes()
				: runFreqMeas(false), runModAmMeas(false), runModFmMeas(false), runModPmMeas(false), 
				  runBwMeas(false), runFsMeas(false), runDfMeas(false)
			{
			}

			bool	runFreqMeas;	// MEAS_DATA_TYPE contains "FO[OO]" or "FR[OO]"	(frequency offset or frequency measurement)
			bool	runModAmMeas;	// MEAS_DATA_TYPE contains "AM[OO]"				(modulation measurement, AM)
			bool	runModFmMeas;	// MEAS_DATA_TYPE contains "FM[OO]"				(modulation measurement, FM)
			bool	runModPmMeas;	// MEAS_DATA_TYPE contains "PM[OO]"				(modulation measurement, PM)
			bool	runBwMeas;		// MEAS_DATA_TYPE contains "BW[OO]"				(bandwidth measurement)
			bool	runFsMeas;		// MEAS_DATA_TYPE contains "LV[OO]" ???			(field strength measurement)
			bool	runDfMeas;		// MEAS_DATA_TYPE contains "BE[OO]"				(DF measurement)
		};


		// Mutators
		//
		// Frequency Mode and associated ////////////////////////////////////////////////////////////
		void	FrequencyMode(SFrequencyMode::EFrequencyMode mode)	{ _freqMode.mode = mode; }
		//	If SINGLE frequency mode:
		void	SingleFrequencyHz(const TCHAR* strSingleFreqHz)	    { _freqMode.freqs.freqSingle.freqSingleHz = _tstoi64(strSingleFreqHz); }
		//	If frequency RANGE mode:
		void	LowerFrequencyHz(const TCHAR* strLowerFreqHz)		{ _freqMode.freqs.freqRange.freqLowerHz =   _tstoi64(strLowerFreqHz); }
		void	UpperFrequencyHz(const TCHAR* strUpperFreqHz)		{ _freqMode.freqs.freqRange.freqUpperHz =   _tstoi64(strUpperFreqHz); }
		void	StepSizeHz(const TCHAR* strStepSizeHz)				{ _freqMode.freqs.freqRange.stepSizeHz =    _ttol(strStepSizeHz); }
		void	StepSizeHz(const unsigned long ulStepSizeHz)		{ _freqMode.freqs.freqRange.stepSizeHz =    ulStepSizeHz; }
		//	If frequency LIST mode:
		//		done inside of TciXmlUtils::CTciXmlOrderResp::readFreqMode()

		/////////////////////////////////////////////////////////////////////////////////////////////
		void	FreqMeasSelected(bool runFreqMeas)					{ _measTypes.runFreqMeas  = runFreqMeas;   }
		void	ModAmMeasSelected(bool runModAmMeas)				{ _measTypes.runModAmMeas = runModAmMeas;  }
		void	ModFmMeasSelected(bool runModFmMeas)				{ _measTypes.runModFmMeas = runModFmMeas;  }
		void	ModPmMeasSelected(bool runModPmMeas)				{ _measTypes.runModPmMeas = runModPmMeas;  }
		void	BwMeasSelected(bool runBwMeas)						{ _measTypes.runBwMeas    = runBwMeas;     }
		void	FsMeasSelected(bool runFsMeas)						{ _measTypes.runFsMeas    = runFsMeas;     }
		void	DfMeasSelected(bool runDfMeas)						{ _measTypes.runDfMeas    = runDfMeas;     }

		/////////////////////////////////////////////////////////////////////////////////////////////
		void	MeasBandwidthHz(const TCHAR* strBandwidthHz)		{ _measBandwidthHz = _ttol(strBandwidthHz); }
		void	MeasBandwidthHz(const unsigned long ulBandwidthHz)	{ _measBandwidthHz = ulBandwidthHz; }
		/////////////////////////////////////////////////////////////////////////////////////////////


		// Accessors
		//
		// Frequency Mode and associated ////////////////////////////////////////////////////////////
		//	Set the frequency mode itself
		SFrequencyMode::EFrequencyMode FrequencyMode()	{ return _freqMode.mode; }
		//	If SINGLE frequency mode:
		unsigned __int64	SingleFrequencyHz()			{ return _freqMode.freqs.freqSingle.freqSingleHz; }
		//	If frequency RANGE mode:
		unsigned __int64	LowerFrequencyHz()			{ return _freqMode.freqs.freqRange.freqLowerHz; }
		unsigned __int64	UpperFrequencyHz()			{ return _freqMode.freqs.freqRange.freqUpperHz; }
		unsigned long		StepSizeHz()				{ return _freqMode.freqs.freqRange.stepSizeHz; }
		//	If frequency LIST mode
		bool				GetFirstFreq(unsigned __int64& freq);
		bool				GetNextFreq(unsigned __int64& freq);
		
		/////////////////////////////////////////////////////////////////////////////////////////////
		bool	FreqMeasSelected()						{ return _measTypes.runFreqMeas;   }
		bool	ModAmMeasSelected()						{ return _measTypes.runModAmMeas;  }
		bool	ModFmMeasSelected()						{ return _measTypes.runModFmMeas;  }
		bool	ModPmMeasSelected()						{ return _measTypes.runModPmMeas;  }
		bool	ModMeasSelected()						{ return ModAmMeasSelected() || ModFmMeasSelected() || ModPmMeasSelected(); }
		bool	BwMeasSelected()						{ return _measTypes.runBwMeas;     }
		bool	FsMeasSelected()						{ return _measTypes.runFsMeas;     }
		bool	DfMeasSelected()						{ return _measTypes.runDfMeas;     }
		/////////////////////////////////////////////////////////////////////////////////////////////
		unsigned long MeasBandwidthHz()					{ return _measBandwidthHz; }
		/////////////////////////////////////////////////////////////////////////////////////////////


	protected:
		// Reading from the OR (input/request) XML file
		// Many (all?) of these read() functions read/store related XML values and store them into member variables (Accessors are later used to gain access to this data)
		virtual bool readOrderInfo(TCHAR* inputFilename);
		bool	readFreqMode();
		bool	readMeasDataTypes();
		bool	readMeasBandwidth();


		// Writing to the ORder (output/results) XML file
		void	writeOrderResponse(/*TCHAR* orderId*/);
		void	writeOrderDef(TCHAR* orderId, short level);
		void	writeSubOrderDef(TCHAR* orderId, short level);

		// SUB_ORDER_DEF section
		void	writeSubOrderName(TCHAR* tagValue, TCHAR* tagValueDefault, short level);
		void	writeSubOrderTask(TCHAR* tagValue, TCHAR* tagValueDefault, short level);
		void	writeSubOrderPrio(TCHAR* tagValue, TCHAR* tagValueDefault, short level);
		void	writeResultType(TCHAR* tagValue, TCHAR* tagValueDefault, short level);

		// FREQ_PARAM, and other sections, that are inside of the SUB_ORDER_DEF section
		void	writeFreqParam(short level);	// just write back the FREQ_PARAM section from the input OR(der) XML file
		void	writeMdtParamSection(short level);
		void	writeAntSetSection(short level);
		void	writeTimeParamSection(short level);
		void	writeTimeParamListSection(short level);
		void	writeMeasStatParamSection(short level);
		void	writeMeasLocParamSection(short level);
		void	writeMeasPrepParamSection(short level);
		void	writeMeasParamSection(short level);
		void	writeRiParamSection(short level);
		void	writeRiResultSection(short level);

		// MEAS_DATA_HDR section (includes MEAS_DATA sections for all measurement data)
		void	writeMeasDataHdr(short level);
		void	writeMeasDataHdrSiteType(short level);
		void	writeHeasDataHdrSiteLong(short level);
		void	writeMeasDataHdrSiteLat(short level);
		void	writeMeasDataHdrHeight(short level);

		void	writeMeasData(unsigned __int64 measFreqHz, SYSTEMTIME measTime,		            // measurement frequency and time
							  bool bHasFs, double measDataFs, TCHAR* measDataFsUnit,	        // Field strength result and unit (always "D" ?) (if Field Strength measurement requested)
							  bool bHasDf, double measDataDfBearing, short measDataDfQuality,	// DF bearing and quality (if DF measurement requested)
							  bool bHasFr, double measDataFreqOffHz, TCHAR* measDataFreqOffUnit,// Frequency Offset result and unit (always "H" ?) (if Frequency Offset measurement requested)
							  bool bHasBw, double measDataBw, TCHAR* measDataBwUnit,			// Bandwidth result and unit (always "H" ?) (if Bandwidth measurement requested)
							  bool bHasMod, unsigned long measDataMod, TciXmlUtils::EModulationType modulationType,	// Modulation result, type (AM, FM, PM) - unit will be determined base on type (if Modulation measurement requested)
							  short level);


	protected:
		// to store measurement data collected from the (Scorpio) client... (put these as protected!!). Put StoreMeasurementDatum_Bw and other StoreMeasurementDatum functions as public (these will be called by the client)
		void addDatumFs(unsigned __int64 measFreqHz, SYSTEMTIME measTime, double measDataFs)
		{
			SMeasurementDatum datum;
			datum = _measurementData[measFreqHz];
			
			datum.bHasFs = true;
			datum.measTime = measTime;
			datum.measDataFs = measDataFs;

			_measurementData[measFreqHz] = datum;
		}

		void addDatumDf(unsigned __int64 measFreqHz, SYSTEMTIME measTime, double measDataDfBearing, short measDataDfQuality)
		{
			SMeasurementDatum datum;
			datum = _measurementData[measFreqHz];

			datum.bHasDf = true;
			datum.measTime = measTime;
			datum.measDataDfBearing = measDataDfBearing;
			datum.measDataDfQuality = measDataDfQuality;

			_measurementData[measFreqHz] = datum;
		}

		void addDatumFrOff(unsigned __int64 measFreqHz, SYSTEMTIME measTime, double measDataFreqOffHz)
		{
			SMeasurementDatum datum;
			datum = _measurementData[measFreqHz];

			datum.bHasFr = true;
			datum.measTime = measTime;
			datum.measDataFreqOffHz = measDataFreqOffHz;

			_measurementData[measFreqHz] = datum;
		}

		void addDatumBw(unsigned __int64 measFreqHz, SYSTEMTIME measTime, double measDataBwKhz)
		{
			SMeasurementDatum datum;
			datum = _measurementData[measFreqHz];

			datum.bHasBw = true;
			datum.measTime = measTime;
			datum.measDataBwKhz = measDataBwKhz;

			_measurementData[measFreqHz] = datum;
		}

		void addDatumMod(unsigned __int64 measFreqHz, SYSTEMTIME measTime, unsigned long measDataMod, EModulationType modulationType)
		{
			SMeasurementDatum datum;
			datum = _measurementData[measFreqHz];

			datum.bHasMod = true;
			datum.measTime = measTime;
			datum.measDataMod = measDataMod;
			datum.measModType = modulationType;

			_measurementData[measFreqHz] = datum;
		}


		// put Order Response (XML File) specific fields here
		TCHAR	_subOrderName[_sizeField];
		TCHAR	_subOrderTask[_sizeField];
		TCHAR	_subOrderPrio[_sizeField];
		TCHAR	_resultType[_sizeField];

		TCHAR	_MSP_SIG_PATH[_sizeField];		// will be read from the ORder request file's <MSP_SIG_PATH> field (inside <MEAS_STAT_PARAM> section). This originally comes from the GSP response (client will set this to be the TCI's server name)


		// Read and matinain the (up to four) measurements requested to be run; we support {Freq, Mod, BW, FS, DF}
		//why keep this around? TCHAR		_measDataTypes[_sizeField];	// these 2-letter abbreviations (can have one or two appended 'O's) determine the type of measurement(s) to be run by the SMS server
		SMeasurementTypes _measTypes;

		// Read and maintain the frequency mode (single frequency, frequency range, frequency list-maybe) specified in the input OR(der) request XML file, and the frequency info
		SFrequencyMode _freqMode;

		// Measurement bandwidth (If DF_BANDWIDTH is not defined, then FREQ_PAR_BWIDTH is also used as DF bandwidth)
		unsigned long _measBandwidthHz;


		// map of collected measurement results (Scorpio Client will get these and add to this map via some mutator functions)
		struct SMeasurementDatum
		{
			SMeasurementDatum() 
				: bHasFs(false), bHasDf(false), bHasFr(false), bHasBw(false), bHasMod(false)
			{
			}

			bool	bHasFs;		// true: includes Field Strength result (measDataFs)
			bool	bHasDf;		// true: includes Direction results (measDataDfBearing, measDataDfQuality)
			bool	bHasFr;		// true: includes Frequency Offset result (measDataFreqOffHz)
			bool	bHasBw;		// true: includes Bandwidth result (measDataBw)
			bool	bHasMod;
			
			SYSTEMTIME measTime;
			double	measDataFs;			// Value of Field Strength measurement result (question: do we want to store the fs unit here too? it's a /*std::wstring*/ tstring/TCHAR*, and will be the same for all fs measurements! think about storing it elsewhere and just once). Hard code to "D" (for dBuV/m) ?
			double	measDataDfBearing;	// DF Azimuth
			short	measDataDfQuality;	// DF Quality (DF confidence??)
			double	measDataFreqOffHz;	// Frequency (offset) value. (question: do we want to store the freq offset unit here too? it's a /*std::wstring*/ tstring/TCHAR*, and will be the same for all fs measurements! think about storing it elsewhere and just once) hard-code to "H" (for Hz) ?
			double	measDataBwKhz;		// Bandwidth (question: do we want to store the bw unit here too? it's a /*std::wstring*/ tstring/TCHAR*, and will be the same for all fs measurements! think about storing it elsewhere and just once). hard-code to "K" (for kHz) ?
			unsigned long measDataMod;	// Modulation value
			EModulationType measModType;// Type of modulation (AM, FM or PM). Note that SMS4DC only shows one (1) modulation result, even though there are places for all three in the result XML file
		};
		typedef std::map<unsigned __int64, SMeasurementDatum/*double*/> MEASUREMENTDATA;
		MEASUREMENTDATA _measurementData;	//snm-todo. keep the key as and uint64 (freq), but the value must be a new structure, that has all types of meas result fields (for fs, df, bw, fr, mod). 
					// create mutator functions to add/update into this map different meas results. Maybe one function to add (freq, meastime), and others to update with meas results?


		// Maintain (some) entire sections of the input OR(der) XML file, just to be written back to the output OR(der) XML file (is that right?)
		//BSTR _bstrFreqParamSection;
		tstring _inputFreqParamSection;
		tstring _inputMdtParamSection;
		tstring _inputAntSetSection;
		tstring _inputTimeParamSection;
		tstring _inputTimeParamListSection;
		tstring _inputMeasStatParamSection;
		tstring _inputMeasLocParamSection;
		tstring _inputMeasPrepParamSection;
		tstring _inputMeasParamSection;
		tstring _inputRiParamSection;
		tstring _inputRiResultSection;


		private:
			unsigned long index;
	};

	class CLatLong
	{
	public:
		CLatLong(void)
		{
		}

		virtual ~CLatLong(void)
		{
		}

		void Lat2dms(double latd, short& deglat, short& minlat, short& seclat, TCHAR& nslat)
		{
			tstring strCardinalDirChar;

			if (latd >= 0.0)
			{
				deg2dms(latd, deglat, minlat, seclat);
				strCardinalDirChar = _T("N");
			}
			else
			{
				deg2dms(-latd, deglat, minlat, seclat);
				strCardinalDirChar = _T("S");
			}

			nslat = strCardinalDirChar[0];
		}

		void Lon2dms(double lond, short& deglon, short& minlon, short& seclon, TCHAR& ewlon)
		{
			tstring strCardinalDirChar;

			if (lond >= 0.0)
			{
				deg2dms(lond, deglon, minlon, seclon);
				strCardinalDirChar = _T("E");
			}
			else
			{
				deg2dms(-lond, deglon, minlon, seclon);
				strCardinalDirChar = _T("W");
			}

			ewlon = strCardinalDirChar[0];
		}

	protected:
		void deg2dms(double degrees, short& deg, short& min, short& sec)
		{                          
			double temp;
			deg = static_cast<short>(degrees);
			temp = 60.0 * (degrees - static_cast<double>(deg));
			min = static_cast<short>(temp);
			temp = 60.0 * (temp - static_cast<double>(min));
			sec = static_cast<short>(temp);
		}
	};
};

