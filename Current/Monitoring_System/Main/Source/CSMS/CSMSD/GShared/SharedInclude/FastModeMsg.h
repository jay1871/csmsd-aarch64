/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#pragma once

#include "ErrorCodes.h"
#include "SmsMsg.h"
#include "Units.h"

struct SFastModeMsg
:
	SSmsMsg
{
	// Message subtypes for message type FAST_MODE_CTRL
	enum EFastModeCtrl
	{
		GOTO_STANDARD_MODE			= 1,
		GOTO_FAST_MODE				= 2,
		START_RESUME				= 3,
		STOP_PAUSE					= 4,
		SET_FAST_MODE_PARAMS		= 5,
		CLEAR_HISTORY				= 6,
		GOTO_STANDARD_MODE_RESPONSE	= 65537,
		GOTO_FAST_MODE_RESPONSE		= 65538,
		START_RESUME_RESPONSE		= 65539,
		STOP_PAUSE_RESPONSE			= 65540,
		SET_FAST_MODE_PARAMS_RESPONSE = 65541,
		CLEAR_HISTORY_RESPONSE		= 65542,
		SIGNAL_UP_REPORT			= 65543,
		SIGNAL_DOWN_REPORT			= 65544,
		DF_REPORT					= 65545,
		SPECTRUM_OUTPUT				= 65546,
		ACTIVITY_OUTPUT				= 65547,
		FAST_MODE_STATUS			= 524289
	};


	// Message version data
	static const SVersionData VERSION_DATA[];


	// Message structures
	static const unsigned int MAX_BANDS = 25;
	static const unsigned int MAX_NUM_AZIM_ENTRIES = 10;

	struct SSetFastModeParamsV0 // SET_FAST_MODE_PARAMS V0
	{
		struct SFastModeParams
		{
			unsigned long detectionThreshold;	// dB above noise floor
			unsigned long dwell;				// number of "looks" for each report (cannot be 0)
			unsigned long attackTime;			// up signal (ms)
			unsigned long decayTime;			// down signal (ms)
			unsigned long upCount;				// up signal
			unsigned long downCount;			// down signal
			unsigned long combineAdjacentChans;	// discrete channels if 0
			unsigned long spectrumOutput;		// 0 = no output; 1 = spectrum; 2 = activity; 3 = both
			unsigned long dfOnlyOnFirst;		// DF on every sweep if 0
			unsigned long rxBw;					// the receiver bandwidth
		};

		struct SScanList
		{
			struct SBandList
			{
				unsigned long	startFreq;			// band start frequency in Hz
				unsigned long	stopFreq;			// band stop frequency in Hz
				unsigned long	chanBw;				// channel bandwidth in Hz
				bool			include;			// exclude if FALSE
			};
	
			unsigned long	numBands;			// number of frequency bands
			SBandList		bandList[MAX_BANDS];
		}; 
	
		SFastModeParams	param;
		SScanList		scanList;
	};

	struct SSetFastModeParamsV2; // Forward declaration

	struct SSetFastModeParamsV1 // SET_FAST_MODE_PARAMS V1 (variable length)
	{
		operator SSetFastModeParamsV0(void) const // Convert to V0
		{
			SSetFastModeParamsV0 cmd;
			cmd.param.detectionThreshold = param.detectionThreshold;
			cmd.param.dwell = param.dwell;
			cmd.param.attackTime = param.attackTime;
			cmd.param.decayTime = param.decayTime;
			cmd.param.upCount = param.upCount;
			cmd.param.downCount = param.downCount;
			cmd.param.combineAdjacentChans = param.combineAdjacentChans;
			cmd.param.spectrumOutput = param.spectrumOutput;
			cmd.param.dfOnlyOnFirst = param.dfOnlyOnFirst;
			cmd.param.rxBw = param.rxBw;
			cmd.scanList.numBands = (scanList.numBands <= MAX_BANDS ? scanList.numBands : MAX_BANDS);

			for(size_t band = 0; band < cmd.scanList.numBands; ++band)
			{
				cmd.scanList.bandList[band].startFreq = scanList.bandList[band].startFreq;
				cmd.scanList.bandList[band].stopFreq = scanList.bandList[band].stopFreq;
				cmd.scanList.bandList[band].chanBw = scanList.bandList[band].chanBw;
				cmd.scanList.bandList[band].include = scanList.bandList[band].include;
			}

			return cmd;
		}

		SSetFastModeParamsV1& operator =(SSetFastModeParamsV0 v0) // Convert from V0
		{
			param.detectionThreshold = v0.param.detectionThreshold;
			param.dwell = v0.param.dwell;
			param.attackTime = v0.param.attackTime;
			param.decayTime = v0.param.decayTime;
			param.upCount = v0.param.upCount;
			param.downCount = v0.param.downCount;
			param.combineAdjacentChans = v0.param.combineAdjacentChans;
			param.spectrumOutput = v0.param.spectrumOutput;
			param.dfOnlyOnFirst = v0.param.dfOnlyOnFirst;
			param.rxBw = v0.param.rxBw;
			scanList.numBands = (v0.scanList.numBands <= MAX_BANDS ? v0.scanList.numBands : 0);

			for(size_t i = 0; i < scanList.numBands; ++i)
			{
				scanList.bandList[i].startFreq = v0.scanList.bandList[i].startFreq;
				scanList.bandList[i].stopFreq = v0.scanList.bandList[i].stopFreq;
				scanList.bandList[i].chanBw = v0.scanList.bandList[i].chanBw;
				scanList.bandList[i].include = v0.scanList.bandList[i].include;
				scanList.bandList[i].horizPol = false;
			}

			return *this;
		}

		SSetFastModeParamsV1& operator =(const SSetFastModeParamsV2& v2); // Convert from V2

		struct SFastModeParams
		{
			unsigned long detectionThreshold;	// dB above noise floor
			unsigned long dwell;				// number of "looks" for each report (cannot be 0)
			unsigned long attackTime;			// up signal (ms)
			unsigned long decayTime;			// down signal (ms)
			unsigned long upCount;				// up signal
			unsigned long downCount;			// down signal
			unsigned long combineAdjacentChans;	// discrete channels if 0
			unsigned long spectrumOutput;		// 0 = no output; 1 = spectrum; 2 = activity; 3 = both
			unsigned long dfOnlyOnFirst;		// DF on every sweep if 0
			unsigned long rxBw;					// the receiver bandwidth
		};

		struct SScanList
		{
			struct SBandList
			{
				unsigned long	startFreq;	// band start frequency in Hz
				unsigned long	stopFreq;	// band stop frequency in Hz
				unsigned long	chanBw;		// channel bandwidth in Hz
				bool			include;	// exclude if FALSE
				bool			horizPol;	// use horizontally polarized antenna if available
			};
	
			unsigned long	numBands;		// number of frequency bands
			SBandList		bandList[1];	// variable length (numBands)
		}; 
	
		SFastModeParams	param;
		SScanList		scanList;
	};

	struct SSetFastModeParamsV3; // Forward declaration

	struct SSetFastModeParamsV2 // SET_FAST_MODE_PARAMS V2 (variable length)
	{
		SSetFastModeParamsV2& operator =(const SSetFastModeParamsV1& v1) // Convert from V1
		{
			param.detectionThreshold = v1.param.detectionThreshold;
			param.dwell = v1.param.dwell;
			param.attackTime = v1.param.attackTime;
			param.decayTime = v1.param.decayTime;
			param.upCount = v1.param.upCount;
			param.downCount = v1.param.downCount;
			param.combineAdjacentChans = v1.param.combineAdjacentChans;
			param.spectrumOutput = v1.param.spectrumOutput;
			param.numDfs = v1.param.dfOnlyOnFirst;
			param.rxBw = v1.param.rxBw;
			memmove(scanList.bandList, v1.scanList.bandList, v1.scanList.numBands * sizeof(scanList.bandList[0]));
			scanList.numBands = v1.scanList.numBands;
			param.numAzimEntries = 0;
			param.loRange = 0;
			param.hiRange = 0xffffffff;
			param.loBandwidth = 0;
			param.hiBandwidth = 0xffffffff;
			param.loSigStrength = 0;
			param.hiSigStrength = 255;
			param.loDfConf = 0;

			return *this;
		}

		inline SSetFastModeParamsV2& operator =(const SSetFastModeParamsV3& v3); // Convert from V3

		struct SFastModeParams
		{
			unsigned long	detectionThreshold;	// dB above noise floor
			unsigned long	dwell;				// number of "looks" for each report (cannot be 0)
			unsigned long	attackTime;			// up signal (ms)
			unsigned long	decayTime;			// down signal (ms)
			unsigned long	upCount;			// up signal
			unsigned long	downCount;			// down signal
			unsigned long	combineAdjacentChans;	// discrete channels if 0
			unsigned long	spectrumOutput;		// 0 = no output; 1 = spectrum; 2 = activity; 3 = both
			unsigned long	numDfs;				// DF on every sweep if 0, otherwise do this number of DFs on each new signal
			unsigned long	rxBw;				// the receiver bandwidth
			unsigned char	numAzimEntries;
			unsigned short	startAzim[MAX_NUM_AZIM_ENTRIES];	// deg (0 to 359)
			unsigned short	stopAzim[MAX_NUM_AZIM_ENTRIES];		// deg (0 to 359)
			unsigned long	loRange;		// km
			unsigned long	hiRange;		// km
			unsigned long	loBandwidth;	// Hz
			unsigned long	hiBandwidth;	// Hz
			unsigned char	loSigStrength;	// dBm + 200
			unsigned char	hiSigStrength;	// dBm + 200
			unsigned char	loDfConf;		// % (0 to 100)
		};

		struct SScanList
		{
			struct SBandList
			{
				unsigned long	startFreq;	// band start frequency in Hz
				unsigned long	stopFreq;	// band stop frequency in Hz
				unsigned long	chanBw;		// channel bandwidth in Hz
				bool			include;	// exclude if FALSE
				bool			horizPol;	// use horizontally polarized antenna if available
			};
	
			unsigned long	numBands;		// number of frequency bands
			SBandList		bandList[1];	// variable length (numBands)
		}; 
	
		SFastModeParams	param;
		SScanList		scanList;
	};

	struct SSetFastModeParamsV3 // SET_FAST_MODE_PARAMS V3 (variable length)
	{
		SSetFastModeParamsV3& operator =(const SSetFastModeParamsV2& v2) // Convert from V2
		{
			const SSetFastModeParamsV2* src = &v2;
			size_t sizeofV2 = offsetof(SFastModeMsg, body.setFastModeParamsV2.scanList.bandList[v2.scanList.numBands]);

			if(static_cast<void*>(this) == static_cast<const void*>(&v2))
			{
				// Make a copy
				if((src = static_cast<const SSetFastModeParamsV2*>(malloc(sizeofV2))) == nullptr)
				{
					AfxThrowMemoryException();
				}

				memcpy(const_cast<SSetFastModeParamsV2*>(src), &v2, sizeofV2);
			}

			params.rxBw = Units::Frequency(src->param.rxBw).GetRaw();
			params.loBandwidth = Units::Frequency(src->param.loBandwidth).GetRaw();
			params.hiBandwidth = Units::Frequency(src->param.hiBandwidth).GetRaw();
			params.loRange = unsigned short(src->param.loRange);
			params.hiRange = unsigned short(src->param.hiRange);
			params.attackTime = unsigned short(src->param.attackTime);
			params.decayTime = unsigned short(src->param.decayTime);
			params.upCount = unsigned short(src->param.upCount);
			params.downCount = unsigned short(src->param.downCount);
			params.detectionThreshold = unsigned char(src->param.detectionThreshold);
			params.dwell = unsigned char(src->param.dwell);
			params.combineAdjacentChans = (src->param.combineAdjacentChans != 0);
			params.spectrumOutput = unsigned char(src->param.spectrumOutput);
			params.numDfs = unsigned char(src->param.numDfs);
			params.loSigStrength = src->param.loSigStrength;
			params.hiSigStrength = src->param.hiSigStrength;
			params.loDfConf = src->param.loDfConf;
			params.numAzimEntries = src->param.numAzimEntries;

			for(unsigned char entry = 0; entry < params.numAzimEntries; ++entry)
			{
				params.startAzim[entry] = src->param.startAzim[entry];
				params.stopAzim[entry] = src->param.stopAzim[entry];
			}

			scanList.numBands = src->scanList.numBands;

			for(unsigned int band = 0; band < scanList.numBands; ++band)
			{
				scanList.bandList[band].startFreq = Units::Frequency(src->scanList.bandList[band].startFreq).GetRaw();
				scanList.bandList[band].stopFreq = Units::Frequency(src->scanList.bandList[band].stopFreq).GetRaw();
				scanList.bandList[band].chanBw = Units::Frequency(src->scanList.bandList[band].chanBw).GetRaw();
				scanList.bandList[band].include = src->scanList.bandList[band].include;
				scanList.bandList[band].horizPol = src->scanList.bandList[band].horizPol;
			}

			if(src != &v2)
			{
				free(const_cast<SSetFastModeParamsV2*>(src));
			}

			return *this;
		}

		struct SFastModeParams
		{
			Units::Frequency::Raw	rxBw;				// The receiver bandwidth
			Units::Frequency::Raw	loBandwidth;
			Units::Frequency::Raw	hiBandwidth;
			unsigned short			loRange;			// km
			unsigned short			hiRange;			// km
			unsigned short			attackTime;			// Up signal (ms)
			unsigned short			decayTime;			// Down signal (ms)
			unsigned short			upCount;			// Up signal
			unsigned short			downCount;			// Down signal
			unsigned char			detectionThreshold;	// dB above noise floor
			unsigned char			dwell;				// Number of "looks" for each report (cannot be 0)
			bool					combineAdjacentChans;	// Discrete channels if false
			unsigned char			spectrumOutput;		// 0 = no output; 1 = spectrum; 2 = activity; 3 = both
			unsigned char			numDfs;				// DF on every sweep if 0, otherwise do this number of DFs on each new signal
			unsigned char			loSigStrength;		// dBm + 200
			unsigned char			hiSigStrength;		// dBm + 200
			unsigned char			loDfConf;			// % (0 to 100)
			unsigned char			numAzimEntries;
			unsigned short			startAzim[MAX_NUM_AZIM_ENTRIES];	// deg (0 to 359)
			unsigned short			stopAzim[MAX_NUM_AZIM_ENTRIES];		// deg (0 to 359)
		};

		struct SScanList
		{
			struct SBandList
			{
				Units::Frequency::Raw	startFreq;	// Band start frequency
				Units::Frequency::Raw	stopFreq;	// Band stop frequency
				Units::Frequency::Raw	chanBw;		// Channel bandwidth
				bool					include;	// Exclude if false
				bool					horizPol;	// Use horizontally polarized antenna if available
			};
	
			unsigned long	numBands;		// number of frequency bands
			SBandList		bandList[1];	// variable length (numBands)
		}; 
	
		SFastModeParams	params;
		SScanList		scanList;
	};

	typedef SSetFastModeParamsV3 SSetFastModeParams;
	
	struct SSetFastModeParamsRespV0 // SET_FAST_MODE_PARAMS_RESPONSE V0
	{
#if _MSC_VER >= 1400
		static const unsigned int MAX_TUNE = 250;
#else
		enum { MAX_TUNE = 250 };
#endif

		struct SBandLinks
		{
			unsigned long	numRcvrBlocks;		// number of receiver blocks in client band
			unsigned long	rcvrBlockNum[MAX_TUNE];	// receiver block number
		};
		
		struct SFreqLinks
		{
			unsigned long	numRcvrBlocks;				// number of receiver blocks in all client bands
			unsigned long	startFrequency[MAX_TUNE];	// block start freq in Hertz 
		};
	
		ErrorCodes::EErrorCode	status;					// status
		unsigned long			missionId;				// mission ID
		SSetFastModeParamsV0	clientParams;			// original client tasking parameters
		SBandLinks				bandLinks[MAX_BANDS];	// links between client bands & receiver blocks
		SFreqLinks				freqLinks;				// links between rx blocks & rx tune freqs
	};

	struct SSetFastModeParamsRespV2; // Forward declaration

	struct SSetFastModeParamsRespV1 // SET_FAST_MODE_PARAMS_RESPONSE V1 (variable length)
	{
		operator SSetFastModeParamsRespV0(void) const // Convert to V0
		{
			SSetFastModeParamsRespV0 resp;
			resp.status = status;
			resp.missionId = missionId;
			resp.clientParams = clientParams;
			resp.freqLinks.numRcvrBlocks = (numRcvrBlocks <= SSetFastModeParamsRespV0::MAX_TUNE ? numRcvrBlocks : SSetFastModeParamsRespV0::MAX_TUNE);
			unsigned long band = 0;
			unsigned long bandStart = 0;
			unsigned long block;

			for(block = 0; block < resp.freqLinks.numRcvrBlocks; ++block)
			{
				if(links[block].band != band)
				{
					if(links[block].band >= MAX_BANDS)
					{
						break;
					}

					resp.bandLinks[band].numRcvrBlocks = block - bandStart;
					band = links[block].band;
					bandStart = block;
				}

				resp.bandLinks[band].rcvrBlockNum[block - bandStart] = block;
				resp.freqLinks.startFrequency[block] = links[block].startFrequency;
			}

			resp.bandLinks[band].numRcvrBlocks = block - bandStart;

			return resp;
		}

		SSetFastModeParamsRespV1& operator =(SSetFastModeParamsRespV0 v0) // Convert from V0
		{
			status = v0.status;
			missionId = v0.missionId;
			clientParams.param.detectionThreshold = v0.clientParams.param.detectionThreshold;
			clientParams.param.dwell = v0.clientParams.param.dwell;
			clientParams.param.attackTime = v0.clientParams.param.attackTime;
			clientParams.param.decayTime = v0.clientParams.param.decayTime;
			clientParams.param.upCount = v0.clientParams.param.upCount;
			clientParams.param.downCount = v0.clientParams.param.downCount;
			clientParams.param.combineAdjacentChans = v0.clientParams.param.combineAdjacentChans;
			clientParams.param.spectrumOutput = v0.clientParams.param.spectrumOutput;
			clientParams.param.dfOnlyOnFirst = v0.clientParams.param.dfOnlyOnFirst;
			clientParams.param.rxBw = v0.clientParams.param.rxBw;
			clientParams.scanList.numBands = 1; // Only copy first band
			clientParams.scanList.bandList[0].startFreq = v0.clientParams.scanList.bandList[0].startFreq;
			clientParams.scanList.bandList[0].stopFreq = v0.clientParams.scanList.bandList[0].stopFreq;
			clientParams.scanList.bandList[0].chanBw = v0.clientParams.scanList.bandList[0].chanBw;
			clientParams.scanList.bandList[0].include = v0.clientParams.scanList.bandList[0].include;
			clientParams.scanList.bandList[0].horizPol = false;
			numRcvrBlocks = (v0.freqLinks.numRcvrBlocks <= SSetFastModeParamsRespV0::MAX_TUNE ? v0.freqLinks.numRcvrBlocks : 0);
			unsigned long band = 0;
			unsigned long bandBlock = 0;

			for(size_t block = 0; block < numRcvrBlocks; ++block)
			{
				if(bandBlock++ == v0.bandLinks[band].numRcvrBlocks)
				{
					++band;
					bandBlock = 0;
				}

				links[block].band = band;
				links[block].startFrequency = v0.freqLinks.startFrequency[block];
			}

			return *this;
		}

		SSetFastModeParamsRespV1& operator =(const SSetFastModeParamsRespV2& v2); // Convert from V2

		struct SLinks
		{
			unsigned long band;				// Band for block
			unsigned long startFrequency;	// Block start freq in Hertz
		};

		ErrorCodes::EErrorCode	status;			// Status
		unsigned long			missionId;		// Mission ID
		SSetFastModeParamsV1	clientParams;	// Original client tasking parameters (variable length, but here we only return the 
												//  first band so it's actually fixed length)
		unsigned long			numRcvrBlocks;	// Number of receiver blocks in all client bands
		SLinks					links[1];		// Variable length (numRcvrBlocks)
	};

	struct SSetFastModeParamsRespV3; // Forward declaration

	struct SSetFastModeParamsRespV2 // SET_FAST_MODE_PARAMS_RESPONSE V2 (variable length)
	{
		SSetFastModeParamsRespV2& operator =(const SSetFastModeParamsRespV1& v1) // Convert from V1
		{
			status = v1.status;
			missionId = v1.missionId;
			numRcvrBlocks = v1.numRcvrBlocks;
			memmove(links, v1.links, v1.numRcvrBlocks * sizeof(links[0]));

			return *this;
		}

		inline SSetFastModeParamsRespV2& operator =(const SSetFastModeParamsRespV3& v3); // Convert from V3

		struct SLinks
		{
			unsigned long band;				// Band for block
			unsigned long startFrequency;	// Block start freq in Hertz
		};

		ErrorCodes::EErrorCode	status;			// Status
		unsigned long			missionId;		// Mission ID
		unsigned long			numRcvrBlocks;	// Number of receiver blocks in all client bands
		SLinks					links[1];		// Variable length (numRcvrBlocks)
	};

	struct SSetFastModeParamsRespV3 // SET_FAST_MODE_PARAMS_RESPONSE V3 (variable length)
	{
		SSetFastModeParamsRespV3& operator =(const SSetFastModeParamsRespV2& v2) // Convert from V2
		{
			// Copy backwards to allow in-place
#pragma warning(suppress : 6293) // Uses overflow to terminate loop
			for(unsigned long block = v2.numRcvrBlocks - 1; block < v2.numRcvrBlocks; --block)
			{
				links[block].startFrequency = Units::Frequency(v2.links[block].startFrequency).GetRaw();
				links[block].band = v2.links[block].band;
			}

			numRcvrBlocks = v2.numRcvrBlocks;
			missionId = v2.missionId;
			status = v2.status;

			return *this;
		}

		struct SLinks
		{
			unsigned long band;						// Band for block
			Units::Frequency::Raw startFrequency;	// Block start freq
		};

		ErrorCodes::EErrorCode	status;			// Status
		unsigned long			missionId;		// Mission ID
		unsigned long			numRcvrBlocks;	// Number of receiver blocks in all client bands
		SLinks					links[1];		// Variable length (numRcvrBlocks)
	};

	typedef SSetFastModeParamsRespV3 SSetFastModeParamsResp;

	struct SWbActivityOutput // ACTIVITY_OUTPUT
	{
		double dateTime;			// using DATE format
		unsigned long missionId;	// mission ID
		unsigned long rcvrBlockNum;	// starts at 0
		unsigned long numBins;		// number of bins in spectrum data
		unsigned char activityData[1]; // Variable length
	}; 

	struct SWbSignalDownReportV1; // Forward declaration

	struct SWbSignalDownReportV0 // SIGNAL_DOWN_REPORT V0
	{
		inline operator SWbSignalDownReportV1(void) const; // Convert to V1

		double	dateTime;			// using COleTime format
		unsigned long missionId;	// mission ID
		unsigned long rcvrBlockNum;	// starts at 0
		unsigned long frequency;	// signal center frequency in Hz
		unsigned long bandwidth;	// signal bandwidth in Hz
	}; 

	struct SWbSignalDownReportV1 // SIGNAL_DOWN_REPORT V1
	{
		operator SWbSignalDownReportV0(void) const // Convert to V0
		{
			SWbSignalDownReportV0 v0;
			v0.dateTime = abs(dateTime);
			v0.missionId = missionId;
			v0.rcvrBlockNum = rcvrBlockNum;
			v0.frequency = unsigned long(frequency + 0.5);
			v0.bandwidth = unsigned long(bandwidth + 0.5);

			return v0;
		}

		double	dateTime;			// using COleTime format
		unsigned long missionId;	// mission ID
		unsigned long rcvrBlockNum;	// starts at 0
		double frequency;			// signal center frequency in Hz
		double bandwidth;			// signal bandwidth in Hz
	};

	typedef SWbSignalDownReportV1 SWbSignalDownReport;
	struct SWbSignalUpDfReportV1; // Forward declaration

	struct SWbSignalUpDfReportV0 // SIGNAL_UP_REPORT / DF_REPORT V0
	{
		inline operator SWbSignalUpDfReportV1(void) const; // Convert to V1

		double dateTime;			// using COleTime format
		double latitude;			// degrees (+ North, - South)
		double longitude;			// degrees (+ East, - West)
		unsigned long missionId;	// mission ID
		unsigned long rcvrBlockNum;	// starts at 0
		unsigned long frequency;	// signal center frequency in Hz
		unsigned long bandwidth;	// signal bandwidth in Hz
		unsigned long azimuth;		// DF azimuth in 1/100s degrees (0 to 35999)
		unsigned long confidence;	// confidence (0 to 100)
		unsigned long heading;		// degrees * 100 (0 to 35999)
		long power;					// power in dBm * 10
	};

	struct SWbSignalUpDfReportV2; // Forward declaration

	struct SWbSignalUpDfReportV1 // SIGNAL_UP_REPORT / DF_REPORT V1
	{
		inline operator SWbSignalUpDfReportV2(void) const; // Convert to V2

		operator SWbSignalUpDfReportV0(void) const // Convert to V0
		{
			SWbSignalUpDfReportV0 v0;
			v0.dateTime = abs(dateTime);
			v0.latitude = latitude;
			v0.longitude = longitude;
			v0.missionId = missionId;
			v0.rcvrBlockNum = rcvrBlockNum;
			v0.frequency = unsigned long(frequency + 0.5);
			v0.bandwidth = unsigned long(bandwidth + 0.5);
			v0.azimuth = (100 * unsigned long(azimuth + 0.005f)) % 36000;
			v0.confidence = unsigned long(100 * confidence);
			v0.heading = (100 * unsigned long(heading + 0.005f)) % 36000;
			v0.power = long(10 * power);

			return v0;
		}

		double dateTime;			// using COleTime format
		double latitude;			// degrees (+ North, - South)
		double longitude;			// degrees (+ East, - West)
		unsigned long missionId;	// mission ID
		unsigned long rcvrBlockNum;	// starts at 0
		double frequency;			// signal center frequency in Hz
		double bandwidth;			// signal bandwidth in Hz
		float azimuth;				// DF azimuth in degrees
		float el;					// DF elevation in degrees
		float range;				// DF range in km
		float confidence;			// confidence (0 to 1.0)
		float heading;				// degrees
		float power;				// power in dBm
	};

	struct SWbSignalUpDfReportV3; // Forward declaration

	struct SWbSignalUpDfReportV2 // SIGNAL_UP_REPORT / DF_REPORT V2
	{
		inline operator SWbSignalUpDfReportV3(void) const; // Convert to V3

		operator SWbSignalUpDfReportV1(void) const // Convert to V1
		{
			SWbSignalUpDfReportV1 v1;
			v1.dateTime = abs(dateTime);
			v1.latitude = latitude;
			v1.longitude = longitude;
			v1.missionId = missionId;
			v1.rcvrBlockNum = rcvrBlockNum;
			v1.frequency = frequency;
			v1.bandwidth = bandwidth;
			v1.azimuth = azimuth;
			v1.el = el;
			v1.range = range;
			v1.confidence = (badHeading ? 0 : confidence);
			v1.heading = heading;
			v1.power = power;

			return v1;
		}

		double dateTime;			// using COleTime format
		double latitude;			// degrees (+ North, - South)
		double longitude;			// degrees (+ East, - West)
		unsigned long missionId;	// mission ID
		unsigned long rcvrBlockNum;	// starts at 0
		double frequency;			// signal center frequency in Hz
		float bandwidth;			// signal bandwidth in Hz
		float azimuth;				// DF azimuth in degrees (relative to heading)
		float el;					// DF elevation in degrees
		float range;				// DF range in km
		float confidence;			// confidence (0 to 1.0)
		float heading;				// degrees
		float power;				// power in dBm
		bool badHeading;			// heading is bad
	};

	struct SWbSignalUpDfReportV3 // SIGNAL_UP_REPORT / DF_REPORT V3
	{
		operator SWbSignalUpDfReportV2(void) const // Convert to V2
		{
			SWbSignalUpDfReportV2 v2;
			v2.dateTime = abs(dateTime);
			v2.latitude = latitude;
			v2.longitude = longitude;
			v2.missionId = missionId;
			v2.rcvrBlockNum = rcvrBlockNum;
			v2.frequency = frequency;
			v2.bandwidth = bandwidth;
			v2.azimuth = azimuth;
			v2.el = el;
			v2.range = range;
			v2.confidence = (badHeading ? 0 : confidence);
			v2.heading = heading;
			v2.power = power;
			v2.badHeading = badHeading;

			return v2;
		}

		double dateTime;			// using COleTime format
		double latitude;			// degrees (+ North, - South)
		double longitude;			// degrees (+ East, - West)
		unsigned long missionId;	// mission ID
		unsigned long rcvrBlockNum;	// starts at 0
		double frequency;			// signal center frequency in Hz
		float bandwidth;			// signal bandwidth in Hz
		float azimuth;				// DF azimuth in degrees (relative to heading)
		float el;					// DF elevation in degrees
		float range;				// DF range in km
		float confidence;			// confidence (0 to 1.0)
		float heading;				// degrees
		float power;				// power in dBm
		bool badHeading;			// heading is bad
		bool badSiteLocation;		// site location is bad
	};

	typedef SWbSignalUpDfReportV3 SWbSignalUpDfReport;

	struct SWbSpectrumOutput // SPECTRUM_OUTPUT
	{
		double dateTime;			// using DATE format
		unsigned long missionId;	// mission ID
		unsigned long rcvrBlockNum;	// starts at 0
		unsigned long numBins;		// number of bins in spectrum data
		unsigned char spectrumData[1]; // Variable length
	};


	// Message format
#pragma pack(push, 4)
	SHdr hdr;

	union UBody
	{
		SGenericResp genericResp;
		SGetBistResp getBistResp;
		SGetBistRespOld getBistRespOld;
		SGetGpsResp getGpsResp;
		SGetHeadingResp getHeadingResp;
		SGetMsgVersionsData getMsgVersionsData; // Variable length
		SInitializeDf initializeDf;
		SInitializeDfV1 initializeDfV1; // V0 has no body
		SInitializeDfV2 initializeDfV2;
		SInitializeDfResp initializeDfResp; // Variable length
		SInitializeDfRespV0 initializeDfRespV0;
		SInitializeDfRespV1 initializeDfRespV1; // Variable length
		SInitializeDfRespV2 initializeDfRespV2; // Variable length
		SInitializeDfRespV3 initializeDfRespV3; // Variable length
		SInitializeDfRespV4 initializeDfRespV4; // Variable length
		SLoadIonogram loadIonogram;
		SLoadIonogramResp loadIonogramResp;
		SSetFastModeParams setFastModeParams; // Variable length
		SSetFastModeParamsV0 setFastModeParamsV0;
		SSetFastModeParamsV1 setFastModeParamsV1; // Variable length
		SSetFastModeParamsV2 setFastModeParamsV2; // Variable length
		SSetFastModeParamsV3 setFastModeParamsV3; // Variable length
		SSetFastModeParamsResp setFastModeParamsResp; // Variable length
		SSetFastModeParamsRespV0 setFastModeParamsRespV0;
		SSetFastModeParamsRespV1 setFastModeParamsRespV1; // Variable length
		SSetFastModeParamsRespV2 setFastModeParamsRespV2; // Variable length
		SSetFastModeParamsRespV3 setFastModeParamsRespV3; // Variable length
		SWbActivityOutput wbActivityOutput; // Variable length
		SWbSignalDownReport wbSignalDownReport;
		SWbSignalDownReportV0 wbSignalDownReportV0;
		SWbSignalDownReportV1 wbSignalDownReportV1;
		SWbSignalUpDfReport wbSignalUpDfReport;
		SWbSignalUpDfReportV0 wbSignalUpDfReportV0;
		SWbSignalUpDfReportV1 wbSignalUpDfReportV1;
		SWbSignalUpDfReportV2 wbSignalUpDfReportV2;
		SWbSignalUpDfReportV3 wbSignalUpDfReportV3;
		SWbSpectrumOutput wbSpectrumOutput; // Variable length
	} body;
#pragma pack(pop)
};

// Conversion operators
inline SFastModeMsg::SSetFastModeParamsV1& SFastModeMsg::SSetFastModeParamsV1::operator =
	(const SFastModeMsg::SSetFastModeParamsV2& v2) // Convert from V2
{
	param.detectionThreshold = v2.param.detectionThreshold;
	param.dwell = v2.param.dwell;
	param.attackTime = v2.param.attackTime;
	param.decayTime = v2.param.decayTime;
	param.upCount = v2.param.upCount;
	param.downCount = v2.param.downCount;
	param.combineAdjacentChans = v2.param.combineAdjacentChans;
	param.spectrumOutput = v2.param.spectrumOutput;
	param.dfOnlyOnFirst = v2.param.numDfs;
	param.rxBw = v2.param.rxBw;
	scanList.numBands = v2.scanList.numBands;

	for(size_t i = 0; i < scanList.numBands; ++i)
	{
		scanList.bandList[i].startFreq = v2.scanList.bandList[i].startFreq;
		scanList.bandList[i].stopFreq = v2.scanList.bandList[i].stopFreq;
		scanList.bandList[i].chanBw = v2.scanList.bandList[i].chanBw;
		scanList.bandList[i].include = v2.scanList.bandList[i].include;
		scanList.bandList[i].horizPol = v2.scanList.bandList[i].horizPol;
	}

	return *this;
}

inline SFastModeMsg::SSetFastModeParamsV2& SFastModeMsg::SSetFastModeParamsV2::operator =
	(const SFastModeMsg::SSetFastModeParamsV3& v3) // Convert from V3
{
	const SSetFastModeParamsV3* src = &v3;
	size_t sizeofV3 = offsetof(SFastModeMsg, body.setFastModeParamsV3.scanList.bandList[v3.scanList.numBands]);

	if(static_cast<void*>(this) == static_cast<const void*>(&v3))
	{
		// Make a copy
		if((src = static_cast<const SSetFastModeParamsV3*>(malloc(sizeofV3))) == nullptr)
		{
			AfxThrowMemoryException();
		}

		memcpy(const_cast<SSetFastModeParamsV3*>(src), &v3, sizeofV3);
	}

	param.detectionThreshold = src->params.detectionThreshold;
	param.dwell = src->params.dwell;
	param.attackTime = src->params.attackTime;
	param.decayTime = src->params.decayTime;
	param.upCount = src->params.upCount;
	param.downCount = src->params.downCount;
	param.combineAdjacentChans = (src->params.combineAdjacentChans ? 1 : 0);
	param.spectrumOutput = src->params.spectrumOutput;
	param.numDfs = src->params.numDfs;
	param.rxBw = Units::Frequency(src->params.rxBw).Hz<unsigned long>();
	param.numAzimEntries = src->params.numAzimEntries;

	for(unsigned char entry = 0; entry < param.numAzimEntries; ++entry)
	{
		param.startAzim[entry] = src->params.startAzim[entry];
		param.stopAzim[entry] = src->params.stopAzim[entry];
	}

	param.loRange = src->params.loRange;
	param.hiRange = src->params.hiRange;
	param.loBandwidth = Units::Frequency(src->params.loBandwidth).Hz<unsigned long>();
	param.hiBandwidth = Units::Frequency(src->params.hiBandwidth).Hz<unsigned long>();
	param.loSigStrength = src->params.loSigStrength;
	param.hiSigStrength = src->params.hiSigStrength;
	param.loDfConf = src->params.loDfConf;
	scanList.numBands = src->scanList.numBands;

	for(size_t i = 0; i < scanList.numBands; ++i)
	{
		scanList.bandList[i].startFreq = Units::Frequency(src->scanList.bandList[i].startFreq).Hz<unsigned long>(true);
		scanList.bandList[i].stopFreq = Units::Frequency(src->scanList.bandList[i].stopFreq).Hz<unsigned long>(true);
		scanList.bandList[i].chanBw = Units::Frequency(src->scanList.bandList[i].chanBw).Hz<unsigned long>();
		scanList.bandList[i].include = src->scanList.bandList[i].include;
		scanList.bandList[i].horizPol = src->scanList.bandList[i].horizPol;
	}

	if(src != &v3)
	{
		free(const_cast<SSetFastModeParamsV3*>(src));
	}

	return *this;
}

inline SFastModeMsg::SSetFastModeParamsRespV1& SFastModeMsg::SSetFastModeParamsRespV1::operator =
	(const SFastModeMsg::SSetFastModeParamsRespV2& v2) // Convert from V2
{
	status = v2.status;
	missionId = v2.missionId;
	memmove(links, v2.links, v2.numRcvrBlocks * sizeof(links[0]));
	numRcvrBlocks = v2.numRcvrBlocks;
	clientParams.param.detectionThreshold = 12;
	clientParams.param.dwell = 1;
	clientParams.param.attackTime = 5;
	clientParams.param.decayTime = 5;
	clientParams.param.upCount = 1;
	clientParams.param.downCount = 1;
	clientParams.param.combineAdjacentChans = false;
	clientParams.param.spectrumOutput = false;
	clientParams.param.dfOnlyOnFirst = 0;
	clientParams.param.rxBw = 0;
	clientParams.scanList.numBands = 0;

	return *this;
}

inline SFastModeMsg::SSetFastModeParamsRespV2& SFastModeMsg::SSetFastModeParamsRespV2::operator =
	(const SFastModeMsg::SSetFastModeParamsRespV3& v3) // Convert from V3
{
	status = v3.status;
	missionId = v3.missionId;
	numRcvrBlocks = v3.numRcvrBlocks;

	for(unsigned long block = 0; block < numRcvrBlocks; ++block)
	{
		links[block].band = v3.links[block].band;
		links[block].startFrequency = Units::Frequency(v3.links[block].startFrequency).Hz<unsigned long>(true);
	}

	return *this;
}

inline SFastModeMsg::SWbSignalDownReportV0::operator SFastModeMsg::SWbSignalDownReportV1(void) const // Convert to V1
{
	SWbSignalDownReportV1 v1;
	v1.dateTime = dateTime;
	v1.missionId = missionId;
	v1.rcvrBlockNum = rcvrBlockNum;
	v1.frequency = frequency;
	v1.bandwidth = bandwidth;

	return v1;
}

inline SFastModeMsg::SWbSignalUpDfReportV0::operator SFastModeMsg::SWbSignalUpDfReportV1(void) const // Convert to V1
{
	SWbSignalUpDfReportV1 v1;
	v1.dateTime = dateTime;
	v1.latitude = latitude;
	v1.longitude = longitude;
	v1.missionId = missionId;
	v1.rcvrBlockNum = rcvrBlockNum;
	v1.frequency = frequency;
	v1.bandwidth = bandwidth;
	v1.azimuth = azimuth / 100.0f;
	v1.el = 90;
	v1.range = 0;
	v1.confidence = confidence / 100.0f;
	v1.heading = heading / 100.0f;
	v1.power = power / 10.0f;

	return v1;
}

inline SFastModeMsg::SWbSignalUpDfReportV1::operator SFastModeMsg::SWbSignalUpDfReportV2(void) const // Convert to V2
{
	SWbSignalUpDfReportV2 v2;
	v2.dateTime = dateTime;
	v2.latitude = latitude;
	v2.longitude = longitude;
	v2.missionId = missionId;
	v2.rcvrBlockNum = rcvrBlockNum;
	v2.frequency = frequency;
	v2.bandwidth = float(bandwidth);
	v2.azimuth = azimuth;
	v2.el = el;
	v2.range = range;
	v2.confidence = confidence;
	v2.heading = heading;
	v2.power = power;
	v2.badHeading = false;

	return v2;
}

inline SFastModeMsg::SWbSignalUpDfReportV2::operator SFastModeMsg::SWbSignalUpDfReportV3(void) const // Convert to V3
{
	SWbSignalUpDfReportV3 v3;
	v3.dateTime = dateTime;
	v3.latitude = latitude;
	v3.longitude = longitude;
	v3.missionId = missionId;
	v3.rcvrBlockNum = rcvrBlockNum;
	v3.frequency = frequency;
	v3.bandwidth = float(bandwidth);
	v3.azimuth = azimuth;
	v3.el = el;
	v3.range = range;
	v3.confidence = confidence;
	v3.heading = heading;
	v3.power = power;
	v3.badHeading = badHeading;
	v3.badSiteLocation = false;

	return v3;
}


// Message versions (selectany lets this go in the .h file)
const __declspec(selectany) SFastModeMsg::SVersionData SFastModeMsg::VERSION_DATA[] =
{
	{ { SFastModeMsg::DF_CTRL, SFastModeMsg::INITIALIZE_DF }, { 2, 4 } },
	{ { SFastModeMsg::DF_CTRL, SFastModeMsg::INITIALIZE_DF_RESPONSE }, { 2, 4 } },
	{ { SFastModeMsg::FAST_MODE_CTRL, SFastModeMsg::DF_REPORT }, { 0, 3 } },
	{ { SFastModeMsg::FAST_MODE_CTRL, SFastModeMsg::SET_FAST_MODE_PARAMS }, { 3, 3 } },
	{ { SFastModeMsg::FAST_MODE_CTRL, SFastModeMsg::SET_FAST_MODE_PARAMS_RESPONSE }, { 3, 3 } },
	{ { SFastModeMsg::FAST_MODE_CTRL, SFastModeMsg::SIGNAL_DOWN_REPORT }, { 0, 1 } },
	{ { SFastModeMsg::FAST_MODE_CTRL, SFastModeMsg::SIGNAL_UP_REPORT }, { 0, 3 } },
	{ { SFastModeMsg::FAST_MODE_CTRL, SFastModeMsg::START_RESUME }, { 0, 3 } }
};
