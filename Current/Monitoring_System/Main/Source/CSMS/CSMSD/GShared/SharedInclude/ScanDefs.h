/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2005 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/
#ifndef SCANDEFS_DEF
#define SCANDEFS_DEF

typedef struct
{
	unsigned long band;
	unsigned long lowFrequency;
	unsigned long highFrequency;
	unsigned long channelBandwidth;
	unsigned long numChannels;
	unsigned long rxBandwidth;
	unsigned long sampleRate;
	unsigned long decimation;
	unsigned long ddcFirBW;
	unsigned long sampleSize;
	unsigned long processBandwidth;
	unsigned long numBins;
	unsigned long invertSpectrum;
	unsigned long binsPerChannel;
	unsigned long numChannelsPerDwell;
} scanDFBandInclude;

typedef struct
{
	unsigned long lowFrequency;
	unsigned long highFrequency;
	unsigned long channelBandwidth;
} scanDFBandExclude;


#endif
