/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013-2014 TCI International, Inc.   All rights reserved       *
**************************************************************************/

#pragma once

#include <map>
#include <vector>
#include "IppVec.h"
#include "Units.h"

class CTDOATimeDelay
{
protected:
	// constants
	static const unsigned long MAX_SAMPLE_SIZE = 524288ul;

	// data structures
	struct SResult
	{
		enum EStatusCode
		{
			SUCCESS =					0,
			START_TIME_DIFF_TOO_LARGE =	1,
			OUT_OF_MEMORY =				2,
			THREAD_TIMEOUT =			3,
			DELAY_TIME_TOO_LARGE =		4
		} status;
		double delayTime; // seconds (sensor B wrt sensor A)
		float correlationPeak; // 0.0 to 1.0
		float correlationMin; // 0.0 to 1.0
	};

	struct SResultV1
	{
		operator SResult(void) const		// Convert from V1 to V0
		{
			SResult v0;
			v0.status = status;
			v0.delayTime = delayTime;
			v0.correlationPeak = correlationPeak;
			v0.correlationMin = correlationMin;
			return v0;
		}

		SResult::EStatusCode status;
		double delayTime; // seconds (sensor B wrt sensor A)
		float correlationPeak; // 0.0 to 1.0
		float correlationMin; // 0.0 to 1.0
		unsigned long sensorGroupId;
		bool useGroupInFix;
	};

	struct SSensorIdPair
	{
		bool operator<(const SSensorIdPair& b) const { return (this->sensorIdA < b.sensorIdA ||
				!(b.sensorIdA < this->sensorIdA) && this->sensorIdB < b.sensorIdB); }
		unsigned long sensorIdA;
		unsigned long sensorIdB;
	};

	enum EIqType { IQ_16SC, IQ_32SC, IQ_32FC, IQ_64FC };
	struct STDOATimeDelayInput
	{
		// Types
		struct STDOAData
		{
			// ctor
			STDOAData(void) : iqData(nullptr), latitude(90.0), longitude(0.0), sensorId(0), sensorVelocity(0.0) {}

			union
			{
				Ipp16scVec* iqData;		// size s/b power of 2 (though class will truncate). Not used in ComputeDataOverlap()
				Ipp32scVec* iqData32sc; // size s/b power of 2 (though class will truncate). Not used in ComputeDataOverlap()
				Ipp32fcVec* iqData32fc; // size s/b power of 2 (though class will truncate). Not used in ComputeDataOverlap()
				Ipp64fcVec* iqData64fc; // size s/b power of 2 (though class will truncate). Not used in ComputeDataOverlap()
			};
			Units::Timestamp firstSampleTime;
			double latitude;
			double longitude;
			unsigned long sensorId;  // should be unique ID to make output meaningfull
			double sensorVelocity;  // meters / sec
		};

		// Functions
		STDOATimeDelayInput(void) : iqType(IQ_16SC), maxVelocityTarget(0), numSamples(0) {}
		STDOATimeDelayInput(_In_ EIqType iq) : iqType(iq), maxVelocityTarget(0), numSamples(0) {}

		// Data
		Units::Frequency frequency; // RF measurement frequency
		double maxVelocityTarget; // meters / sec
		unsigned int numSamples; // s/b power of 2 (though class will truncate)
		Units::Frequency sampleRate;
		EIqType iqType;		// all STDOAData::iqData must be the same type
		std::vector<STDOAData> sensorData; // all STDOAData::iqData vector sizes must equal numSamples
	};

	template <typename T> struct STDOATimeDelayInputV2
	{
		// Types
		template <typename T> struct STDOADataV2
		{
			// ctor
			STDOADataV2(void) : latitude(90.0), longitude(0.0), sensorId(0), sensorVelocity(0.0), sensorGroupId(1), useGroupInFix(false) {}
			STDOADataV2(_In_ EIqType iqType, _In_ const STDOATimeDelayInput::STDOAData& v0)	// Convert from V0 to V2
			{
				CopyData(iqType, v0);
				firstSampleTime = v0.firstSampleTime;
				latitude = v0.latitude;
				longitude = v0.longitude;
				sensorId = v0.sensorId;
				sensorVelocity = v0.sensorVelocity;
				sensorGroupId = 1;
				useGroupInFix = false;
				return;
			}

			void CopyData(_In_ EIqType iqType, _In_ const STDOATimeDelayInput::STDOAData& v0);

			// Data
			IppCmplxVec<T> iqData;
			Units::Timestamp firstSampleTime;
			double latitude;
			double longitude;
			unsigned long sensorId;  // should be unique ID to make output meaningfull
			double sensorVelocity;  // meters / sec
			unsigned long sensorGroupId;
			bool useGroupInFix;
		};

		//Functions
		STDOATimeDelayInputV2(void) : maxVelocityTarget(0), numSamples(0) {}
		explicit STDOATimeDelayInputV2(_In_ const STDOATimeDelayInput& v0)	// Convert from V0 to V2
		{
			frequency = v0.frequency;
			maxVelocityTarget = v0.maxVelocityTarget;
			numSamples = v0.numSamples;
			sampleRate = v0.sampleRate;
			for (size_t i = 0; i < v0.sensorData.size(); ++i)
			{
				sensorData.emplace_back(v0.iqType, v0.sensorData[i]);
			}
			return;
		}

		// Data
		Units::Frequency frequency; // RF measurement frequency
		double maxVelocityTarget; // meters / sec
		unsigned int numSamples; // s/b power of 2 (though class will truncate)
		Units::Frequency sampleRate;
		std::vector<STDOADataV2<T>> sensorData; // all STDOAData::iqData vector sizes must equal numSamples
	};

	typedef std::map<unsigned long, bool> TDOADataOverlapOutput; // key is sensorId; element = false denotes that data from this sensor
																 //   doesn't overlap with any other sensor (i.e., no reason to retrieve)
	typedef std::map<SSensorIdPair, SResult> TDOATimeDelayOutput;
	typedef std::map<SSensorIdPair, SResultV1> TDOATimeDelayOutputV1;

	// functions:
	CTDOATimeDelay(void);
	~CTDOATimeDelay(void);
	static bool ComputeDataOverlap(_In_ const STDOATimeDelayInput& input, _Out_ TDOADataOverlapOutput& output);
	bool ComputeTimeDelay(_In_ const STDOATimeDelayInput& input, _Out_ TDOATimeDelayOutput& output);
	template <typename T> static bool ComputeDataOverlap(_In_ const STDOATimeDelayInputV2<T>& input, _Out_ TDOADataOverlapOutput& output);
	template <typename T> bool ComputeTimeDelay(_In_ const STDOATimeDelayInputV2<T>& input, _Out_ TDOATimeDelayOutputV1& output);

private:
	// data structures
	struct SProcessTaskResponse
	{
		unsigned int size;
		unsigned int maxDopplerOffsetBins;
		int lowLag;
		bool coarse;
		size_t firstIdx1;
		size_t firstIdx2;
		size_t lastIdx1;
		bool mallocError[4];
		Ipp64f max[4];
		size_t maxIdx1[4];
		size_t maxIdx2[4];
	};

	struct SThreadStruct
	{
		CTDOATimeDelay* ptr;
		size_t index;
	};

	// functions
	static unsigned int CalcMaxDopplerBinOffset(_In_ const Units::Frequency frequency, _In_ const Units::Frequency sampleRate,
			_In_ const double velocitySensor1, _In_ const double velocitySensor2, _In_ const double maxVelocityTarget,
			_In_ const unsigned int numSamples);
	static size_t CalcNumCorrelationBins(_In_ const double latitude1, _In_ const double longitude1, _In_ const double latitude2,
			_In_ const double longitude2, _In_ const Units::Frequency sampleRate);
	double ComputeMagnitude(_In_ const Ipp64fcVec& signal, _In_ const int len);
	bool ConfigureFFTBuffers(_In_ const unsigned int numSamples);
	int CrossCorrelation(_In_ unsigned int maxDopplerOffsetBins, _In_ const int lowLag, _In_ const Ipp64fcVec& refCSignal,
			_In_ const Ipp64fcVec& delayCSignal, _Out_ double& xcorrMaxIndex, _Inout_ Ipp64fVec& xcorr,
			_Out_ int& maxXCorrIndex, _Out_ double& maxXCorr, _Out_ double& minXCorr, _In_ const int sampleLength);
	_Success_(return) static bool ParabolicFit(_In_ const Ipp64fVec X, _In_ const Ipp64fVec Y, _Out_ double& vertexX, _Out_ double& vertexY);
	void PartitionedDopplerSearch(_In_ const size_t index);
	static UINT Thread0(_In_ void* arg);
	static UINT Thread1(_In_ void* arg);
	static UINT Thread2(_In_ void* arg);

	// data
	Ipp64fcVec m_delaySignal;
	bool m_FFTAllocationOK;
	size_t m_numCorr;
	unsigned int m_numSamples;
	Ipp8u* m_pFFTBuffer[4];
	Ipp8u* m_pFFTBufferC[4];
	IppsFFTSpec_C_64fc* m_pFFTSpec[4];
	IppsFFTSpec_C_64fc* m_pFFTSpecC[4];
	SProcessTaskResponse m_processTaskResponse;
	Ipp64fcVec m_refSignal[4];
	std::vector<Ipp64fcVec> m_signal;
	CWinThread* m_thread[3];
};


//////////////////////////////////////////////////////////////////////
//
// Verifies IQ data overlap amongst all sensors
//
template <typename T> static bool CTDOATimeDelay::ComputeDataOverlap(_In_ const STDOATimeDelayInputV2<T>& input, _Out_ TDOADataOverlapOutput& output)
// return:
// true = success
// false = failure (no output possible -> output map is empty)
{
	// clear output map
	output.clear();

	// must have at least two sensors
	if (input.sensorData.size() < 2)
	{
		return false;
	}

	// truncate sample size to a power of 2
	unsigned int newNumSamples = 1;
	unsigned int numSamples = input.numSamples;
	while (numSamples > 1)
	{
		newNumSamples *= 2;
		numSamples /= 2;
	}

	// sequence through pairs computing data overlap
	for (size_t i = 0; i < input.sensorData.size(); i++)
	{
		bool outputValue = false;
		for (size_t j = 0; j < input.sensorData.size(); j++)
		{
			if (i != j && input.sensorData[i].sensorGroupId == input.sensorData[j].sensorGroupId)
			{
				size_t numCorr = CalcNumCorrelationBins(input.sensorData[i].latitude, input.sensorData[i].longitude, input.sensorData[j].latitude,
					input.sensorData[j].longitude, input.sampleRate);
				if (numCorr <= newNumSamples)
				{
					double timeDiff;
					int lowLag;
					if (input.sensorData[i].firstSampleTime > input.sensorData[j].firstSampleTime)
					{
						timeDiff = Units::TimeSpan(input.sensorData[i].firstSampleTime - input.sensorData[j].firstSampleTime).NanoSeconds<double>() / 1000000000.0;  // timeA - timeB in seconds (will be positive)
						lowLag = int(input.sampleRate.Hz<double>() * timeDiff + 0.5) - int(numCorr) / 2;
					}
					else
					{
						timeDiff = -Units::TimeSpan(input.sensorData[j].firstSampleTime - input.sensorData[i].firstSampleTime).NanoSeconds<double>() / 1000000000.0;  // timeA - timeB in seconds (will be negative)
						lowLag = int(input.sampleRate.Hz<double>() * timeDiff - 0.5) - int(numCorr) / 2;
					}
					unsigned int maxDopplerOffsetBins = CalcMaxDopplerBinOffset(input.frequency, input.sampleRate,
						input.sensorData[i].sensorVelocity, input.sensorData[j].sensorVelocity, input.maxVelocityTarget, newNumSamples);

					if (((maxDopplerOffsetBins == 0) && (unsigned int(abs(2 * lowLag + int(numCorr))) <= newNumSamples - numCorr)) ||
						((maxDopplerOffsetBins != 0) && (unsigned int(abs(2 * lowLag + int(numCorr))) <= std::min(newNumSamples, 65536u) - numCorr)))
					{
						outputValue = true;
						break;
					}
				}
			}
		}
		output[input.sensorData[i].sensorId] = outputValue;
	}
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Overall TDOA time delay calculation
//
template <typename T> bool CTDOATimeDelay::ComputeTimeDelay(_In_ const STDOATimeDelayInputV2<T>& input, _Out_ TDOATimeDelayOutputV1& output)
// return:
// true = success
// false = failure (no output possible -> output map is empty)
{
	// clear output map
	output.clear();

	// must have at least two sensors
	if (input.sensorData.size() < 2)
	{
		return false;
	}

	// verify all sample sizes match input.numSamples
	for (size_t i = 0; i < input.sensorData.size(); i++)
	{
		if (input.numSamples != input.sensorData[i].iqData.size())
		{
			return false;
		}
	}

	// truncate sample size to a power of 2
	unsigned int newNumSamples = 1;
	unsigned int numSamples = input.numSamples;
	while (numSamples > 1)
	{
		newNumSamples *= 2;
		numSamples /= 2;
	}

	// initialize fft processing
	if (!ConfigureFFTBuffers(newNumSamples)) // m_numSamples gets updated in this function
	{
		return false;
	}

	// convert data to Ipp64fcVec and compute magnitudes and maxMagnitude
	double maxMagnitude = 0.0;
	std::vector<double> magnitude;
	m_signal.resize(input.sensorData.size());
	for (size_t i = 0; i < input.sensorData.size(); i++)
	{
//		m_signal[i].resize(m_numSamples);
		m_signal[i] = input.sensorData[i].iqData;	// Copies or converts
		magnitude.push_back(ComputeMagnitude(m_signal[i], m_numSamples));
		if (magnitude[i] > maxMagnitude)
		{
			maxMagnitude = magnitude[i];
		}
	}

	// Scale all signals to strongest signal
	for (size_t i = 0; i < input.sensorData.size(); i++)
	{
		if (maxMagnitude != magnitude[i])
		{
			Ipp64fc factor;
			factor.re = maxMagnitude / magnitude[i];
			factor.im = 0.0;
			ippsMulC_64fc_I(factor, &m_signal[i][0], m_numSamples);
		}
	}

	// sequence through pairs doing cross corrrelation
	for (size_t i = 0; i < input.sensorData.size() - 1; i++)
	{
		for (size_t j = i + 1; j < input.sensorData.size(); j++)
		{
			if (input.sensorData[i].sensorGroupId == input.sensorData[j].sensorGroupId)
			{
				m_numCorr = CalcNumCorrelationBins(input.sensorData[i].latitude, input.sensorData[i].longitude, input.sensorData[j].latitude,
					input.sensorData[j].longitude, input.sampleRate);
				// maximum of m_numSamples
				SResultV1 result;
				if (m_numCorr <= m_numSamples)
				{
					double timeDiff;
					int lowLag;
					if (input.sensorData[i].firstSampleTime > input.sensorData[j].firstSampleTime)
					{
						timeDiff = Units::TimeSpan(input.sensorData[i].firstSampleTime - input.sensorData[j].firstSampleTime).NanoSeconds<double>() / 1000000000.0;  // timeA - timeB in seconds (will be positive)
						lowLag = int(input.sampleRate.Hz<double>() * timeDiff + 0.5) - int(m_numCorr) / 2;
					}
					else
					{
						timeDiff = -Units::TimeSpan(input.sensorData[j].firstSampleTime - input.sensorData[i].firstSampleTime).NanoSeconds<double>() / 1000000000.0;  // timeA - timeB in seconds (will be negative)
						lowLag = int(input.sampleRate.Hz<double>() * timeDiff - 0.5) - int(m_numCorr) / 2;
					}
					result.delayTime = 0.0;
					result.correlationPeak = 0.0;
					result.correlationMin = 0.0;
					unsigned int maxDopplerOffsetBins = CalcMaxDopplerBinOffset(input.frequency, input.sampleRate,
						input.sensorData[i].sensorVelocity, input.sensorData[j].sensorVelocity, input.maxVelocityTarget, m_numSamples);
					if (((maxDopplerOffsetBins == 0) && (unsigned int(abs(2 * lowLag + int(m_numCorr))) <= m_numSamples - m_numCorr)) ||
						((maxDopplerOffsetBins != 0) && (unsigned int(abs(2 * lowLag + int(m_numCorr))) <= std::min(m_numSamples, 65536u) - m_numCorr)))
					{
						double xcorrMaxIndex;
						int maxXCorrIndex;
						double maxXCorr;
						double minXCorr;
						Ipp64fVec xcorr(m_numCorr);

						int corrStatus = CrossCorrelation(maxDopplerOffsetBins, lowLag, m_signal[i], m_signal[j], xcorrMaxIndex, xcorr, maxXCorrIndex, maxXCorr, minXCorr, m_numSamples);
						if (corrStatus == 1)
						{
							result.status = CTDOATimeDelay::SResult::OUT_OF_MEMORY;
						}
						else if (corrStatus == 2)
						{
							result.status = CTDOATimeDelay::SResult::THREAD_TIMEOUT;
						}
						else if (corrStatus == 3)
						{
							result.status = CTDOATimeDelay::SResult::DELAY_TIME_TOO_LARGE;
						}
						else
						{
							result.status = CTDOATimeDelay::SResult::SUCCESS;
							result.delayTime = timeDiff - xcorrMaxIndex / input.sampleRate.Hz<double>();
							result.correlationPeak = float(xcorr[maxXCorrIndex]);
							result.correlationMin = float(minXCorr);
						}
					}
					else
					{
						result.status = CTDOATimeDelay::SResult::START_TIME_DIFF_TOO_LARGE;
					}
				}
				else
				{
					result.status = CTDOATimeDelay::SResult::START_TIME_DIFF_TOO_LARGE;
				}
				result.sensorGroupId = input.sensorData[i].sensorGroupId;
				result.useGroupInFix = input.sensorData[i].useGroupInFix;

				SSensorIdPair idPair;
				idPair.sensorIdA = input.sensorData[i].sensorId;
				idPair.sensorIdB = input.sensorData[j].sensorId;
				output[idPair] = result;
			}
		}
	}
	return true;
}