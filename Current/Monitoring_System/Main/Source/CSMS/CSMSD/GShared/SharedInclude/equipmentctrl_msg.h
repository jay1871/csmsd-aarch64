/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2005 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/
// message subtypes for message type EQUIPMENT_CTRL
#define TASK_TERMINATE			1L
#define GET_TASK_STATUS			2L
#define TASK_STATUS				65538L

// status values for equipTaskStatusResp
#define EQUIP_TASK_STARTED		1L
#define EQUIP_TASK_COMPLETED	2L
#define EQUIP_TASK_TERMINATED	3L
#define EQUIP_TASK_RESTARTED	4L
#define EQUIP_TASK_ACTIVE		5L
typedef struct
{
	unsigned long taskID;			// unique ID assigned by equipcontrol
	unsigned long key;				// unique key needed for other commands
	unsigned long status;
	double dateTime;
} equipTaskStatusResp;

typedef struct
{
	unsigned long taskID;			// equipcontrol ID for task (from start msg)
	unsigned long key;				// equipcontrol key for task (from start msg)
} taskTerminateCmd;

typedef struct
{
	unsigned long taskID;
	unsigned long key;
} taskStatusCmd;
