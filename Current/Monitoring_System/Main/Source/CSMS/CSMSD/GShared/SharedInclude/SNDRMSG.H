/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2005 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/
/*
	THIS FILE IS TO BE USED WITH AN INTERFACE TO THE 9046 SOUNDER SYSTEM

*/

#ifndef SOUNDER9046_HOST_MSG_DEF
#define SOUNDER9046_HOST_MSG_DEF

#include "sndrcomm.h"	// common sounder message stuff

const int MAX_BLANKER_FREQS = 96;

typedef struct
{
	long startFreq;
	long endFreq;
	long interval;
	int  sweepRate;
	char powerLevel;
	char blankerOn;
} SOUNDER_SETUP;

typedef struct
{
	long freqs[MAX_BLANKER_FREQS];
} BLANKER_LIST;

typedef struct
{
	long testFreq;
	short int onOffFlag;
	short int powerLevel;
} SOUNDER_BITE;


typedef struct
{
	short int sec;
	short int min;
	short int hour;
	short int yday;  // year day (0-365) Jan 1=0
} TIMESTAMP;


typedef struct 
{
	TIMESTAMP tmStamp;
	FINISHED_IONOGRAM finishedIonogram;
} IONOGRAM_DATA_MSG;	// To requestor


typedef struct
{
	long tmSec;		// time in seconds since 1/1/1970
	long daySec;	// seconds in the day
	long sec;
	long min;
	long int hour;
	long yday;	// year day
} ION_TM_STAMP;


typedef struct
{
	ION_TM_STAMP ionTmStamp;
	FINISHED_IONOGRAM finishedIon;
} ION_FILE_DATA;	// Ionogram file storage format




#endif	// SOUNDER9046_HOST_MSG_DEF
