/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2005 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#ifndef BITESTAT_H
#define BITESTAT_H

// DO NOT REMOVE OR CHANGE EXISTING FIELDS
//   Only Add To Them

typedef enum
{
   BiteOK                   = 0x00000000,
   BiteProblem              = 0x00000001
} BITESTATUS;

// DSP "error" codes
typedef enum
{
   DSPOpenError             = 0x00000001,
   SharedSRAMMemTooSmall    = 0x00000002,
   SharedSRAMMemTooLarge    = 0x00000004,
   NodeANearSRAMMemTooSmall = 0x00000008,
   NodeANearSRAMMemTooLarge = 0x00000010,
   NodeBNearSRAMMemTooSmall = 0x00000020,
   NodeBNearSRAMMemTooLarge = 0x00000040,
   NodeAIntrTestError       = 0x00000080,
   NodeBIntrTestError       = 0x00000100,
   NodeBNotResponding       = 0x00000200,
   NodeBOpenError           = 0x00000400,
   CV8AlreadyOpen           = 0x00001000,
   CV8OpenFailed            = 0x00002000,
   CV8NotOpen               = 0x00004000,
   CV8ResetFailed           = 0x00008000,
   CV8GetSRAMHandleFailed   = 0x00010000,
   CV8SRAMNotShared         = 0x00020000,
   CV8SetHostMBXFailed      = 0x00040000,
   CV8SetDSPMBXFailed       = 0x00080000,
   CV8ReadHostMBXFailed     = 0x00100000,
   CV8ReadDSPMBXFailed      = 0x00200000,
   CV8WriteFailed           = 0x00400000,
   CV8ReadFailed            = 0x00800000,
   CV8LoadFailed            = 0x01000000,
   CV8DSPSendInitFailed		= 0x02000000,
   CV8NotTested             = 0x80000000
} BITECV8ERR;

// A256 "error" codes
typedef enum
{
   wrongOMDType             = 0x00000001,
   wrongIMDType             = 0x00000002,
   synthTuningError         = 0x00000004,
   synthControlError        = 0x00000008,
   initAccessBoardError     = 0x00000010,
   undeterminedError        = 0x00000020,
   memVMEWriteError         = 0x00000040,
   memVMEReadError          = 0x00000080,
   memVMECompareError       = 0x00000100,
   memTooLarge              = 0x00000200,
   memTooSmall              = 0x00000400,
   LRS32Error               = 0x00000800,
   OMDC40PortErr            = 0x00001000,
   synthNotLocked           = 0x00002000,
   LRS12SnglSampleError     = 0x00004000,
   LRS12SnglThresholdError  = 0x00008000,
   LRS12SnglDataError       = 0x00010000,
   LRS12FastSampleError     = 0x00020000,
   LRS12FastThresholdError  = 0x00040000,
   LRS12FastDataError       = 0x00080000,
   CV8B5NotConnected        = 0x00100000,
   A256NotTested            = 0x80000000
} BITEA256ERR;

// Clock Synthesizer "error" codes
typedef enum
{
   clockSynthVMEReadError   = 0x00000001,
   clockSynthVMEWriteError  = 0x00000002,
   clockSynthLockError      = 0x00000004,
   clockSynthCtrlVoltError  = 0x00000008,
   clockSynthCtrlIntWarning = 0x00000010,
   clockSynthGPSValidError  = 0x00000020,
   clockSynthNotTested      = 0x80000000
} BITECLOCKSYNTHERR;

// HFDFInterfaceCard "error" codes
typedef enum
{
   dataSampleError            = 0x00000001,
   HFDFInterfaceCardNotTested = 0x80000000
} BITEHFDFINTERFACECARDERR;

// UHFDFMux "error" codes
typedef enum
{
   IFNotQuiet               = 0x00000001,
   IFChan1Error             = 0x00000002,
   IFChan2Error             = 0x00000004,
   IFChanSwappedError       = 0x00000008,
   RFChan1NormAttenError    = 0x00000010,
   RFChan2NormAttenError    = 0x00000020,
   RFChanSwappedError       = 0x00000040,
   RFChan1LowAttenError     = 0x00000080,
   RFChan2LowAttenError     = 0x00000100,
   RFChan1HighAttenError    = 0x00000200,
   RFChan2HighAttenError    = 0x00000400,
   UHFDFMuxNotTested        = 0x80000000
} BITEUHFDFMUXERR;

// HFDFSwitch "error" codes
typedef enum
{
   swSampleError            = 0x00000001,
   swBiteError              = 0x00000002,
   swSwappedChannelError    = 0x00000004,
   swNoiseError             = 0x00000008,
   HFDFSwitchNotTested      = 0x80000000
} BITEHFDFSWITCHERR;

// UHFDFSwitch "error" codes
typedef enum
{
   uhfSwBiteError           = 0x00000001,
   uhfSwSwappedChannelError = 0x00000002,
   uhfSwNoiseError          = 0x00000004,
   uhfSwLoFrqBiteError      = 0x00000008,
   uhfSwHiFrqBiteError      = 0x00000010,
   UHFDFSwitchNotTested     = 0x80000000
} BITEUHFDFSWITCHERR;

// VHFDFSwitch "error" codes
typedef enum
{
   vhfSwNotchBiteError      = 0x00000001,
   vhfSwSwappedChannelError = 0x00000002,
   vhfSwNoNotchBiteError    = 0x00000004,
   VHFDFSwitchNotTested     = 0x80000000
} BITEVHFDFSWITCHERR;

// VHFReceiver "error" codes
typedef enum
{
   // APCOM Receiver Errors
   BusyError                = 0x00000001,
   ExtRefNotDetected        = 0x00000002,
   ExtRefOutOfLock          = 0x00000004,
   CalibrationFailure       = 0x00000008,
   POSTError                = 0x00000010,
   LO2CourseLoopOutOfLock   = 0x00000020,
   LO2TrackingLoopOutOfLock = 0x00000040,
   LO4LoopOutOfLock         = 0x00000080,
   // TCI Receiver Errors
   VHFRcvrChan1RMSError     = 0x00000100,
   VHFRcvrChan2RMSError     = 0x00000200,
   VHFRcvrChanSwappedError  = 0x00000400,
   VHFRcvrChan1NarrowPassbandError = 0x00000800,
   VHFRcvrChan2NarrowPassbandError = 0x00001000,
   VHFRcvrChan1WidePassbandError   = 0x00002000,
   VHFRcvrChan2WidePassbandError   = 0x00004000,
   LO1OutOfLock             = 0x00008000,
   LO2OutOfLock             = 0x00010000,
   LO3OutOfLock             = 0x00020000,
   ExRefOutOfLock           = 0x00040000,
   ExRefNotDetected         = 0x00080000,
   LO2VoltageTuneError      = 0x00100000,
   HighTempWarning          = 0x00200000,  
   VHFReceiverNotTested     = 0x80000000
} BITEVHFRECEIVERERR;

// HFChassis "error" codes
typedef enum
{
   synth1Error              = 0x00000001,
   ref1Error                = 0x00000002,
   power1Error              = 0x00000004,
   temp1Error               = 0x00000008,
   synth2Error              = 0x00000010,
   ref2Error                = 0x00000020,
   power2Error              = 0x00000040,
   temp2Error               = 0x00000080,
   synth3Error              = 0x00000100,
   HFChassisNotTested       = 0x80000000
} BITEHFCHASSISERR;


// HFDFReceiver "error" codes
typedef enum
{
   sampleError              = 0x00000001,
   rmsError                 = 0x00000002,
   swappedChannelError      = 0x00000004,
   passbandError            = 0x00000008,
   HFDFReceiverNotTested    = 0x80000000
} BITEHFDFRECEIVERERR;

// HFReceiver "error" codes
typedef enum
{
   HFrmsError               = 0x00000001,
   HFPassbandError          = 0x00000002,
   HFReceiverNotTested      = 0x80000000
} BITEHFRECEIVERERR;

// HFAntenna "error" codes
typedef enum
{
   antennaFault             = 0x00000001,
   HFAntennaNotTested       = 0x80000000
} BITEHFANTENNAERR;

// VHFAntenna "error" codes
typedef enum
{
   vhfAntBiteError          = 0x00000001,
   VHFAntennaNotTested      = 0x80000000
} BITEVHFANTENNAERR;

// UHFAntenna "error" codes
typedef enum
{
   uhfAntBiteError          = 0x00000001,
   UHFAntennaNotTested      = 0x80000000
} BITEUHFANTENNAERR;

// AudioCard "error" codes
typedef enum
{
   VMEBusError              = 0x00000001,
   CV8B2NotConnected        = 0x00000002,
   CV8B0NotConnected        = 0x00000004,
   AudioCardNotTested       = 0x80000000
} BITEAUDIOCARDERR;

// FluxGate "error" codes
typedef enum
{
   NeverCalibrated          = 0x00000001,
   LowNoiseScore            = 0x00000002,
   LowMagneticEnviron       = 0x00000004,
   NoFluxGate               = 0x01000000,
   FluxGateNotTested		= 0x80000000
} BITEFLUXGATECARDERR;

// GPS "error" codes
typedef enum
{
	NoGPS					= 0x01000000,
	GPSNotTested			= 0x80000000
} GPSERR;

#endif
