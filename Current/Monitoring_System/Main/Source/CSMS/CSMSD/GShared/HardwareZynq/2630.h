/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once
#include "2630Msg.h"
#include "SharedMemory.h"
#include "Units.h"
#include "Log.h"

class C2630
{
public:
	// Constants
	enum class EBandSelect { BAND1, BAND2, BAND3, OPTIMUM };
	enum class EGainMode { NORMAL = 0, RURAL = 1, URBAN = 2, CONGESTED = 3, NUM_GAINMODES };
	enum class EClockState { INTERNAL, EXTERNAL };
	enum class EExternalRef { EXT100MHZ, EXT10MHZ };
	enum class EInternalRef { INTERNAL, EXT10MHZ, EXT1PPS, EXT100MHZ };

	// Types
//	typedef S2630Msg::SRxCapabilities SConfigData;
	struct SConfigData : S2630Msg::SRxCapabilities
	{
		// Functions
		SConfigData(void) {}
		SConfigData(const S2630Msg::SRxCapabilities& other) : SRxCapabilities(other)
		{
			TRACE("SConfigData...\n");
			for (size_t i = 0; i < size_t(EGainMode::NUM_GAINMODES); ++i)
			{
				attenCalTableIndexLimits[i] = 0;
			}
			maxAtten = 0;
		}

		// Data
		unsigned long attenCalTableIndexLimits[size_t(EGainMode::NUM_GAINMODES)];
		unsigned char maxAtten;
	};

	// TODO: Move next two structs to some other shared file
	struct SDateStamp
	{
		SDateStamp(unsigned long dt) :
			year(1000 * ((dt >> 28) & 0x0f) + 100 * ((dt >> 24) & 0x0f) + 10 * ((dt >> 20) & 0x0f) + ((dt >> 16) & 0x0f)),
			month(10 * ((dt >> 12) & 0x0f) + ((dt >> 8) & 0x0f)),
			date(10 * ((dt >> 4) & 0x0f) + (dt & 0x0f)) {}
		unsigned long year;
		unsigned long month;
		unsigned long date;
	};

	struct SRevision
	{
		SRevision(unsigned long rev) : majorVersion((rev >> 24) & 0xff), revision((rev >> 16) & 0xff), subrevision(rev & 0xffff) {}
		unsigned char majorVersion;
		unsigned char revision;
		unsigned short subrevision;
	};

	struct SVersionInfo
	{
		SVersionInfo(void) : fwRevision(0), fwDatestamp(0), fpgaRevision(0) {}
		SRevision fwRevision;
		SDateStamp fwDatestamp;
		SRevision fpgaRevision;
	};

	struct S2630State
	{
		S2630Msg::SRxCapabilities rxCapabilities;
		unsigned long flashVersion;
		unsigned long ifreq;
		unsigned long if1;
		unsigned long if2;
		unsigned long lo1;
		unsigned long lo2;
		unsigned char ant;
		unsigned char atten;
		unsigned char bw;
		unsigned char freqIndex;
		EGainMode gainMode;
		unsigned long psf1;
		unsigned long psf2;
		bool lna;
		unsigned char sense;
		unsigned char presel;
		float temperatures[3];
		unsigned char ocxoLoopStatus;
		unsigned long ocxoDacCode;
		unsigned long ocxoInterval;
		double frequency;
		SVersionInfo versionInfo;
		bool calgenEnable;
		unsigned long calFreqMHz;
		bool inverted;
		bool direct;
	};

#ifdef CSMS_DEBUG
	static bool s_printResult;
	static bool s_printTuneInfo;
#endif
	// Functions
	bool GetDirect(void) { CSharedMemory::CLockGuard lock(m_shMem->sharedMutex); return m_state->direct; }
	long GetFlashVersion(void) const { CSharedMemory::CLockGuard lock(m_shMem->sharedMutex); return m_state->flashVersion; }
	bool GetFreqLimits(Units::FreqPair& freqLimits);
	void GetMax10UniqueId(unsigned long(&id)[2]) const { id[0] = m_configData.max10UniqueIdLow; id[1] = m_configData.max10UniqueIdHigh; }
	static bool GetRxCapabilitiesStatic(const S2630Msg::SRxCapabilities** rxcapabilities);
	static bool GetMax10UniqueIdStatic(unsigned long(&id)[2]);
	static unsigned long GetrxProcessorStatic();
	static unsigned long GetFirmwareVersionStatic();
	unsigned char GetOcxoLoopStatus(void) const { CSharedMemory::CLockGuard lock(m_shMem->sharedMutex); return m_state->ocxoLoopStatus;}
	void GetState(S2630State& state) const { CSharedMemory::CLockGuard lock(m_shMem->sharedMutex); state = *m_state; }
	std::vector<std::array<float, 2> > GetTempLimits(void) const;
	const SVersionInfo& GetVersionInfo(void)
	{
		CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
		return m_state->versionInfo;
	}
	bool IsWideband(void) const { return m_configData.tuneBwMHz > m_configData.narrowBwMHz; }
	bool ReadAttenuation(unsigned char& atten, EGainMode& gainMode, bool& lna);
	bool ReadCalTone(bool& setOn);
	bool ReadCalToneMHz(unsigned long& setFreqMHz);
	bool ReadClockState(EClockState& clockState);
	static bool ReadOrSetClockStateStatic(bool doSet, EClockState newState, EClockState& clockState);
	bool ReadExternalReference(EExternalRef& extRef);
	bool ReadInternalReference(EInternalRef& intRef);
	bool ReadMOC(unsigned long& freq, unsigned char& presel, unsigned char& bw, unsigned char& ant);
	bool ReadOcxo(unsigned char& loopStatus, unsigned long& code, unsigned long& interval);
//	bool ReadRxTemperature(signed char& temperature);
	bool ReadRxTemperatures(std::vector<float>& temperatures);
	unsigned char GetAntenna(void) const { CSharedMemory::CLockGuard lock(m_shMem->sharedMutex); return m_state->ant; }
	bool SetAntenna(unsigned char ant, unsigned char& setAntenna);
	bool SetAttenuation(EGainMode gainMode, unsigned char attenuation, unsigned char& setAttenuation, EGainMode& setGainMode, bool& setLNA);
	bool SetCalTone(bool on, bool& setOn);
	bool SetCalToneMHz(unsigned long freqMHz, unsigned long& setFreqMHz);
	bool SetClockState(EClockState state, EClockState& setState);
	bool SetExternalReference(EExternalRef ref, EExternalRef& setRef);
	static bool SetExternalReferenceStatic(EExternalRef ref, EExternalRef& setRef);
	static bool ReadMasterStateStatic();
	static bool SetMasterStateStatic(unsigned char mState);
	static bool SetFlashLockStatic(unsigned char mLockState);
	bool SetInternalReference(EInternalRef ref, EInternalRef& setRef);
	void WaitForSettle(unsigned long usec = 1000);
	bool ReadRxSettings(S2630Msg::SRxSettings& rxSettings);
	bool ReadRxCapabilities(S2630Msg::SRxCapabilities& rxCapabilities);
	bool TuneRaw(const Units::Frequency& frequency, unsigned char rxBw, bool hf, bool autoFpi, unsigned char fpi,
		S2630Msg::EAntennaType rxInput, bool& inverted, bool& direct, Units::Frequency& radioFreq, Units::Frequency& ifFreq);

	void GetfreqLowHighFromTable(std::vector<unsigned long>& pffreqlow, std::vector<unsigned long>& pffreqhigh){
		for (unsigned int i = 0; i< m_hfBands.size(); i++)
		{
			pffreqlow.push_back( (unsigned long ) (m_hfBands[i].f1.Hz<double>() / 1000000));
			pffreqhigh.push_back( (unsigned long ) (m_hfBands[i].f2.Hz<double>() / 1000000));
		}
		for (unsigned int i = 0; i< m_vushfBands.size(); i++)
		{
			pffreqlow.push_back( (unsigned long ) (m_vushfBands[i].f1.Hz<double>() / 1000000));
			pffreqhigh.push_back( (unsigned long ) (m_vushfBands[i].f2.Hz<double>() / 1000000));
		}

	}

	// Data
	SConfigData m_configData;
//	static bool m_configDataIsLoaded;

protected:
	// Types
	template <typename T> struct SBaseCalTable
	{
		T startValue;		// freq (Hz) or atten
		T stepValue;		// freq (Hz) or atten
		std::vector<float> data;
	};

	typedef SBaseCalTable<unsigned long> SAttenTable;
	typedef SBaseCalTable<float> SCalGenTable;
	typedef SBaseCalTable<float> SIfGainTable;

	struct SRfGainTable : SBaseCalTable<float>
	{
		size_t ifIndex;			// 0 => no if table
		unsigned char attIndexes[size_t(EGainMode::NUM_GAINMODES)];
		double lowFreq;			// Hz; 0 => no dataLow, else use dataLow for freq < lowFreq
		SBaseCalTable<float> lowTable;
	};

	struct STempGainTable
	{
		struct SAbData
		{
			float belowData;
			float aboveData;
		};
		float nominalTemp;
		std::vector<SAbData> data;	// slope in db/degree C, indexed by FPI
	};

	struct SFreqBand
	{
		Units::Frequency f1;
		Units::Frequency f2;
		Units::Frequency psf1;
		Units::Frequency psf2;
		Units::Frequency ftop;
		unsigned long ftopMHz;
		bool direct;
		bool inverted;
		unsigned char ifIndex;
		unsigned char attIndexes[size_t(EGainMode::NUM_GAINMODES)];
	};

	// Functions
	C2630(std::pair<bool, bool> dfType);
	virtual ~C2630(void);
//	unsigned char MinAtten(Units::Frequency /*freq*/) const { return 0; /*MinAtten(GetGainMode(freq));*/ }
	unsigned char CurrentMaxAtten(void) const { return CurrentMaxAtten(m_state->gainMode); }
	unsigned char CurrentMaxAtten(EGainMode gainMode) const;
	unsigned long CalcRxFreqMHz(bool hf, Units::Frequency frequency) const;
	unsigned char GetAtten(void) const { CSharedMemory::CLockGuard lock(m_shMem->sharedMutex); return m_state->atten; }
	void GetAtten(unsigned char& atten, EGainMode& gainMode) const
		{ CSharedMemory::CLockGuard lock(m_shMem->sharedMutex); atten = m_state->atten; gainMode = m_state->gainMode; return; }
	EGainMode GetGainMode(void) const { CSharedMemory::CLockGuard lock(m_shMem->sharedMutex); return m_state->gainMode; }
	float GetGainRatio(float temp, size_t& rfGainTableIdx, long& ifGainTableIdx, float& gainAdjustmentTemp) const;
	bool HasHf(void) const { return m_configData.hfBwMHz > 0; }
	bool HasVushf(void) const { return m_configData.narrowBwMHz > 0 || m_configData.tuneBwMHz > 0; }
	static float InterpolateGain(double frequency, double startFreq, double stepFreq, const std::vector<float>& data, bool extend);
	bool RunBist(S2630Msg::SBistResults& bistResults);
	bool SetBandWidth(unsigned char bandWidth, unsigned char& setBandwidth);
	bool Tune(Units::Frequency frequency, Units::Frequency rxBw, Units::Frequency procBw, bool hf, S2630Msg::EAntennaType rxInput,
		EBandSelect bandSelect, bool& inverted, bool& direct, size_t& band, Units::Frequency& radioFreq,
		Units::Frequency& f1, Units::Frequency& f2, Units::Frequency& retVal);

	// Data
	std::vector<SAttenTable> m_attenTables[size_t(EGainMode::NUM_GAINMODES)];
	std::vector<SIfGainTable> m_ifGainTable;
	std::vector<SRfGainTable> m_rfGainTable;
	std::vector<SCalGenTable> m_calGenTable;		// TODO: may switch to a map
	std::vector<STempGainTable> m_tempGainTable;
	float m_nominalTemp;
	std::vector<SFreqBand> m_vushfBands;
	std::vector<SFreqBand> m_hfBands;

private:
	// Constants
	static const unsigned long IOCTL_GetRxData = 0x801;
	static constexpr const char* SHMEM_NAME = "Global.TCI.ShMem2630";
	static constexpr const float m_tempLimits[][2] =
	{
		{0.0f, 60.0f}, {0.0f, 60.0f}, {0.0f, 60.0f}
	};
	static const unsigned long DELAY_TO_DIRECT_USEC = 5000;	// 5 msec

	// Types
	struct TCISPI_RXDATA
	{
	  unsigned long transferSize;
	  unsigned char spiData[4096];
	};

	// Functions
	int BandIndex(unsigned long fMHz) const;
	static unsigned long ByteSwap(const unsigned long data)
	{
		return((data&0xFF)<<24)+((data&0xFF00)<<8)+((data&0xFF0000)>>8)+((data&0xFF000000)>>24);
	}
	unsigned long Cmd(S2630Msg::ECommand command);
	static void Delay(unsigned long usec);
	bool DoRxReset(void);
	unsigned long GetImmediate(S2630Msg::ECommand command, unsigned char& value);
	unsigned long GetImmediate(S2630Msg::ECommand command, unsigned char& value0, unsigned char& value1);
	template <typename T> unsigned long GetShort(S2630Msg::ECommand command, T& value);
	template <typename T> unsigned long GetShort(S2630Msg::ECommand command, T& value, unsigned char& extra);
	template <typename T> unsigned long GetShort(S2630Msg::ECommand command, unsigned char setExtra, T& value, unsigned char& extra);
	template <typename T> unsigned long GetLong(S2630Msg::ECommand command, T& value);
	template <typename T> unsigned long GetLong(S2630Msg::ECommand command, T& value, bool varLen, unsigned short bistver = 0);
	template <typename T> unsigned long GetLong(S2630Msg::ECommand command, unsigned long param, T& value, bool varLen);
	void LoadCalibrationTables(void);
	bool ReadAntenna(unsigned char& ant);
	bool ReadBandWidth(unsigned char& bandWidth);
	bool ReadCalTable(S2630Msg::ECalibrationType calType, unsigned char subType, unsigned short index, S2630Msg::SCalTable& calTable);
	bool ReadFirmwareDate(unsigned long& fwDate);
	bool ReadFirmwareVersion(unsigned long& fwVersion);
	bool ReadFpgaVersion(unsigned long& fpgaVersion);
	bool ReadFrequency(unsigned long& frequency);
	bool ReadLoFrequencies(unsigned long& lo1, unsigned long& lo2);
	bool ReadOcxoDacCode(unsigned long& code);
	bool ReadOcxoInterval(unsigned long& interval);
	bool ReadOcxoLoopStatus(unsigned char& loopStatus);
	bool ReadPreselector(unsigned char& preselector);
	bool ReadRxTable(S2630Msg::SFreqPlanTable2& freqPlanTable, unsigned long& numFreqPlanRows);
	int HighestContainedBand(Units::Frequency f) const;
	template <typename T> unsigned long SetShort(S2630Msg::ECommand command, const T& setValue, T& getValue);
	template <typename T> unsigned long SetShort(S2630Msg::ECommand command, const T& setValue, unsigned char setExtra,
		T& getValue, unsigned char& getExtra);
	unsigned long SetImmediate(S2630Msg::ECommand command, unsigned char setValue, unsigned char& getValue);
	unsigned long SetImmediate(S2630Msg::ECommand command, unsigned char setValue0, unsigned char setValue1, unsigned char& getValue0, unsigned char& getValue1);
	bool SetFrequency(unsigned long ifreq, unsigned long& setIfreq);
	bool SetMOC(unsigned long freq, unsigned char presel, unsigned char bw, unsigned char ant,
		unsigned long& setFreq, unsigned char& setPresel, unsigned char& setBw, unsigned char& setAnt);
	bool SetPreselector(unsigned char freqPlanIndex, unsigned char& setIndex);
	size_t WriteRead(const void* sendMsg, size_t sendSize, void* recvMsg, size_t recvSize, bool varLen = false);
#ifdef TEST2630API
	void testApi(void);
#endif

	// Data
	mutable std::mutex m_2630Mutex;
	static int m_driverDesc;
	static int m_driverDesc1;
	bool m_master;
	TCISPI_RXDATA m_rxData;
	CSharedMemory m_sharedMemory;
	CSharedMemory::SSharedMemory* m_shMem;
//	bool m_sim;
//	bool m_useExternal;
	S2630State* m_state;
	unsigned long fwVersion;

};


template <typename T>
unsigned long C2630::GetShort(S2630Msg::ECommand command, unsigned char setExtra, T& value, unsigned char& extra)
{
	for (size_t itry = 0; itry < 2; ++itry)
	{
		S2630Msg::UHeader sendHdr;
		sendHdr.shortHdr.msgType = S2630Msg::SHORTMSG;
		sendHdr.shortHdr.command = command;
		sendHdr.shortHdr.numPackets = 0;
		sendHdr.shortHdr.body = setExtra;

		S2630Msg recvMsg;
		size_t numBytesRead = WriteRead(&sendHdr, sizeof(S2630Msg::UHeader), &recvMsg, sizeof(S2630Msg::UHeader) + sizeof(T));

		if (numBytesRead >= sizeof(S2630Msg::UHeader))
		{
			if (recvMsg.hdr.shortHdr.msgType == S2630Msg::SHORTMSG && recvMsg.hdr.shortHdr.command == command)
			{
				if (recvMsg.hdr.shortHdr.numPackets * 4 == sizeof(T))
				{
					memcpy(&value, recvMsg.body, sizeof(T));
					extra = recvMsg.hdr.shortHdr.body;
					return sizeof(T);
				}
			}
		}
	}
	return 0;
}

template <typename T>
unsigned long C2630::GetShort(S2630Msg::ECommand command, T& value, unsigned char& extra)
{
	return GetShort(command, 0, value, extra);
}

template <typename T>
unsigned long C2630::GetShort(S2630Msg::ECommand command, T& value)
{
	unsigned char extra;
	return GetShort(command, 0, value, extra);
}

template <typename T>
unsigned long C2630::GetLong(S2630Msg::ECommand command, T& value)
{
	S2630Msg::UHeader sendHdr;

	sendHdr.longHdr.msgType = S2630Msg::LONGMSG;
	sendHdr.longHdr.command = command;
	sendHdr.longHdr.numPackets = 0;

	S2630Msg recvMsg;
	size_t numBytesRead = WriteRead(&sendHdr, sizeof(S2630Msg::UHeader), &recvMsg, sizeof(S2630Msg::UHeader) + sizeof(T));

	if (numBytesRead >= sizeof(S2630Msg::UHeader))
	{
		if (recvMsg.hdr.longHdr.msgType == S2630Msg::LONGMSG && recvMsg.hdr.longHdr.command == command)
		{
			if (recvMsg.hdr.longHdr.numPackets * 4 == sizeof(T))
			{
				memcpy(&value, recvMsg.body, sizeof(T));
				return sizeof(T);
			}
		}
	}
	return 0;
}

template <typename T>
unsigned long C2630::GetLong(S2630Msg::ECommand command, T& value, bool varLen, unsigned short bistVer)
{
	S2630Msg::UHeader sendHdr;

	sendHdr.longHdr.msgType = S2630Msg::LONGMSG;
	sendHdr.longHdr.command = command;
	sendHdr.longHdr.numPackets = bistVer; // this is only non-zero if bist version request needs to be 1 or newer

	S2630Msg recvMsg;
	size_t numBytesRead = WriteRead(&sendHdr, sizeof(S2630Msg::UHeader), &recvMsg, sizeof(S2630Msg::UHeader) + sizeof(T), varLen);

	if (numBytesRead >= sizeof(S2630Msg::UHeader))
	{
		if (recvMsg.hdr.longHdr.msgType == S2630Msg::LONGMSG && recvMsg.hdr.longHdr.command == command)
		{
			if (recvMsg.hdr.longHdr.numPackets * 4 <= sizeof(T))
			{
				memcpy(&value, recvMsg.body, recvMsg.hdr.longHdr.numPackets * 4);
				return recvMsg.hdr.longHdr.numPackets * 4;
			}
		}
	}
	return 0;
}

template <typename T>
unsigned long C2630::GetLong(S2630Msg::ECommand command, unsigned long param, T& value, bool varLen)
{
	S2630Msg sendMsg;

	sendMsg.hdr.longHdr.msgType = S2630Msg::LONGMSG;
	sendMsg.hdr.longHdr.command = command;
	sendMsg.hdr.longHdr.numPackets = 1;
	memcpy(sendMsg.body, &param, sizeof(unsigned long));

	S2630Msg recvMsg;
	size_t numBytesRead = WriteRead(&sendMsg, sizeof(unsigned long) + sizeof(S2630Msg::UHeader),
		&recvMsg, sizeof(S2630Msg::UHeader) + sizeof(T), varLen);

	if (numBytesRead >= sizeof(S2630Msg::UHeader))
	{
		if (recvMsg.hdr.longHdr.msgType == S2630Msg::LONGMSG && recvMsg.hdr.longHdr.command == command)
		{
			if (recvMsg.hdr.longHdr.numPackets * 4 <= sizeof(T))
			{
				memcpy(&value, recvMsg.body, recvMsg.hdr.longHdr.numPackets * 4);
				return recvMsg.hdr.longHdr.numPackets * 4;
			}
		}
	}
	CLog::Log(CLog::ERRORS, "GetLong error received command = %d, numByteRead = %u ssize of header %u numPackets%u  size of T= %u  msg type=%d\n", int(recvMsg.hdr.longHdr.command), numBytesRead, sizeof(S2630Msg::UHeader), recvMsg.hdr.longHdr.numPackets, sizeof(T), int(recvMsg.hdr.longHdr.msgType) );
	return 0;
}

template <typename T>
unsigned long C2630::SetShort(S2630Msg::ECommand command, const T& setValue, T& getValue)
{
	unsigned char setExtra = 0;
	unsigned char getExtra;
	return SetShort(command, setValue, setExtra, getValue, getExtra);
}

template <typename T>
unsigned long C2630::SetShort(S2630Msg::ECommand command, const T& setValue, unsigned char setExtra,
	T& getValue, unsigned char& getExtra)
{
	S2630Msg sendMsg;

	sendMsg.hdr.shortHdr.msgType = S2630Msg::SHORTMSG;
	sendMsg.hdr.shortHdr.command = command;
	sendMsg.hdr.shortHdr.numPackets = sizeof(T) / 4;
	sendMsg.hdr.shortHdr.body = setExtra;
	memcpy(sendMsg.body, &setValue, sizeof(T));

	S2630Msg recvMsg;
	size_t numBytesRead = WriteRead(&sendMsg, sizeof(T) + sizeof(S2630Msg::UHeader), &recvMsg, sizeof(T) + sizeof(S2630Msg::UHeader));

	if (numBytesRead >= sizeof(S2630Msg::UHeader))
	{
		if (recvMsg.hdr.shortHdr.msgType == S2630Msg::SHORTMSG && recvMsg.hdr.shortHdr.command == command)
		{
			if (recvMsg.hdr.shortHdr.numPackets * 4 == sizeof(T))
			{
				memcpy(&getValue, recvMsg.body, sizeof(T));
				getExtra = recvMsg.hdr.shortHdr.body;
				return sizeof(T);
			}
		}
	}
	printf("C2630::SetShort failed %u %u %u %u %u\n",
		numBytesRead, recvMsg.hdr.shortHdr.msgType, recvMsg.hdr.shortHdr.command, command, recvMsg.hdr.shortHdr.numPackets);
	return 0;
}

