/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

namespace C3230Fdd
{
	static constexpr const unsigned long MIN_CSMSDIGI_VERSION = 0x01060004;
#ifdef CSMS_2016
	static constexpr const unsigned long MIN_3230_VERSION = 0x00000222; // was 188
#else
	static constexpr const unsigned long MIN_3230_VERSION = 0x00000188;
#endif
	// Register definitions
	enum class EReg : unsigned char
	{
		GLOBAL_CONTROL = 0x00,
		GLOBAL_STATUS = 0x01,
		GLOBAL_DEVICEID = 0x02,
		GLOBAL_BOARDREVISION = 0x03,
		GLOBAL_DATESTAMP = 0x04,
		GLOBAL_FIRMWAREIMAGE = 0x05,
		GLOBAL_FIRMWAREREVISION = 0x06,
		GLOBAL_SCRATCH = 0x07,
		GLOBAL_INTERRUPTS = 0x08,
		GLOBAL_INTERRUPTMASK = 0x09,
		GLOBAL_HMC1035_MEASCLKFREQUENCY = 0x0A,
		GLOBAL_SI5338_200MEASCLKFREQUENCY = 0x0B,
		GLOBAL_DEBUG_REGISTER = 0x0B,
		GLOBAL_SI5338_100MEASCLKFREQUENCY = 0x0C,
		GLOBAL_SI5338_150MEASCLKFREQUENCY = 0x0D,
		GLOBAL_DEVICEDNA_LOWER = 0x0E,
		GLOBAL_DEVICEDNA_UPPER = 0x0F,
		AGC_CONTROL = 0x20,
		AGC_STATUS = 0x21,
		AGC_SLICE0 = 0x22,
		AGC_SLICE1 = 0x23,
		AGC_CCDF = 0x24,
		AGC_WINDOWSIZE = 0x25,
		AGC_INTERRUPT_CNT = 0x26,
		SIGNAL_CONTROL = 0x30,
		SIGNAL_STATUS = 0x31,
		SIGNAL_SCALE = 0x32,
		SIGNAL_NCOFREQUENCY = 0x35,
		SIGNAL_CICDECIMATION = 0x37,
		SIGNAL_CICGAINCOMPENSATION = 0x38,
		FORMAT_CONTROL = 0x40,
		FORMAT_STATUS = 0x41,
		FORMAT_PACKETSIZE = 0x42,
		FORMAT_RESERVED = 0x43,
		FORMAT_SUBBLOCKSIZE = 0x44,
		FORMAT_PACKETCOUNT = 0x45,
		FORMAT_LATENCYCNTR = 0x4F,
		SEQUENCER_CONTROL = 0x50,
		SEQUENCER_STATUS = 0x51,
		SEQUENCER_DATA = 0x52,
		SEQUENCER_ANTENNA_CONTROL = 0x53,
		SEQUENCER_CURRENT_BLOCK_SIZE = 0x54,
		SEQUENCER_CURRENT_ANTENNA = 0x55,
		SEQUENCER_ANTENNA_STATUS = 0x56,
		FFT_CONTROL = 0x60,
		FFT_STATUS = 0x61,
		FFT_SIZE = 0x62,
		FFT_STATUS1 = 0x63,
		FFT_SEQUENCER_DATA = 0x64,
		FFT_PACKETCOUNT = 0x65,
		FFT_I_PACKETCOUNT = 0x66,
		FFT_Q_PACKETCOUNT = 0x67,
		DDC_TOTALDDCS = 0x80,
		DDC_STATUS = 0x81,
		DDC_CHANNELSELECT = 0x82,
		DDC_CHANNELNCOFREQUENCY = 0x83,
		DDC_CICDECIMATION = 0x84,
		DDC_CICGAINCOMPENSATION = 0x85,
		TIMESTAMP_CONTROL = 0x90,
		TIMESTAMP_STATUS = 0x91,
		TIMESTAMP_1PPSWRTIME = 0x92,		// Write seconds to FPGA
		TIMESTAMP_CLKFREQUENCY = 0x93,		// Write frequency to FPGA (one time)
		TIMESTAMP_INTEGERSECOND = 0x95,		// Write to FPGA for internal trigger
		TIMESTAMP_FRACTIONALSECOND = 0x96,	// Write to FPGA for internal trigger
		TIMESTAMP_1PPSCONTTIME = 0x97,		// Read anytime from FPGA, seconds only
		TIMESTAMP_1PPSCURRENTTIMEINTEGER = 0x98,	// Read time on request
		TIMESTAMP_1PPSCURRENTTIMEFRACTIONAL = 0x99,	// Read time on request
		TIMESTAMP_1PPSBLOCKSTARTTIMEINTEGER = 0x9A,		// FPGA updates on block start (read by processor)
		TIMESTAMP_1PPSBLOCKSTARTTIMEFRACTIONAL = 0x9B,	// FPGA updates on block start (read by processor)
		TIMESTAMP_MEASUREDCLKFREQMETHOD1 = 0x9C,	// Read from FPGA
		TIMESTAMP_MEASUREDCLKFREQMETHOD2 = 0x9D,	// Read from FPGA
		TIMESTAMP_PPSDRIFT = 0x9E,	// Read from FPGA, reset using CONTROL
		TIMESTAMP_PPSDRIFT2 = 0x9F,	// Read from FPGA, reset using CONTROL
		ADM_CONTROL = 0xC0,
		ADM_READPARAMETER = 0xC1,
		ADM_CHANNELSELECT = 0xC2,
		ADM_PLDDR3ADDRESS = 0xC3,
		ADM_PACKETSIZE = 0xC4,
		ADM_IQMONITORFIFOOUT = 0xC5,
		ADM_DEMODMONITORFIFOOUT = 0xC6,
		ADM_MONITORFIFOSTATUS = 0xC7,
		ADM_STATUS = 0xC8,
		ADM_BUFFERSIZE = 0xCF,
		DEMOD_CONTROL = 0xD0,
		DEMOD_READPARAMETER = 0xD1,
		DEMOD_CHANNELSELECT = 0xD2,
		DEMOD_PARAMS = 0xD3,
		DEMOD_BFOPHASEINCR = 0xD4,
		BIST_CONTROL = 0xF0,
		BIST_STATUS = 0xF1,
		BIST_TIMERLOADREGISTER = 0xF2,
		BIST_GPIOREGISTER = 0xF3
	};
	enum class EDMAReg : unsigned char
	{
		CDMA_IP_CONTROL = 0x00,
		CDMA_IP_DDRSRCADDRESS = 0x06,
		CDMA_IP_DMADESTADDRESS = 0x08,
		CDMA_IP_TOTALBYTES = 0x0A // total bytes to be transferred
	};
	struct CAdmControl
	{
		static constexpr unsigned long ADM_DM_FIFO_FLUSH = 0x00000001;
		static constexpr unsigned long ADM_INPUT_FIFO_FLUSH = 0x00000002;
		static constexpr unsigned long ADM_S2MM_SOFT_SHUTDOWN = 0x00000004;
	};
	struct CAdmChannelSelect
	{
		static constexpr unsigned long CHANNEL_SELECT_MASK = 0x00000007;
		static constexpr unsigned long READ_SELECT_MASK = 	0x00007000;
		static constexpr unsigned long READ_SELECT_PLDDR3STARTADDR = 	0x00000000;
		static constexpr unsigned long READ_SELECT_PACKETSIZE = 0x00001000;
	};
	struct CAdmPacketSize
	{
		static constexpr unsigned long PACKET_SIZE_MASK = 0x0000ffff;
		static constexpr unsigned long AUDIO_BYPASS = 0x00010000;
		static constexpr unsigned long RESERVED = 0x000E0000;
		static constexpr unsigned long STREAM_ID = 0x00F00000;

		static constexpr const unsigned long AUDIO_MIN_PACKET = 64;	// samples
		static constexpr const unsigned long AUDIO_MAX_PACKET = 16384;	// samples
		static constexpr const double AUDIO_RATE = 16000.;	// Hz
		static constexpr const unsigned long HEADER_SIZE = 32;	// samples
	};
	struct CAdmStatus
	{
		static constexpr unsigned long ADM_DM_FIFO_EMPTY = 		0x00000001;
		static constexpr unsigned long ADM_DM_FIFO_FULL = 		0x00000002;
		static constexpr unsigned long ADM_DM_FIFO_LOSTDATA = 	0x00000004;
		static constexpr unsigned long ADM_DM_FIFO_SENDING = 	0x00000008;
		static constexpr unsigned long ADM_DM_SOURCE_READY = 	0x00000010;
		static constexpr unsigned long ADM_DM_DEST_READY =	 	0x00000020;
		static constexpr unsigned long ADM_DM_END_OF_PACKET = 	0x00000040;
		static constexpr unsigned long ADM_DM_INTERNAL_ERROR = 	0x00000100;
		static constexpr unsigned long ADM_DM_DECODE_ERROR = 	0x00000200;
		static constexpr unsigned long ADM_DM_SLAVE_ERROR = 	0x00000400;
		static constexpr unsigned long ADM_DM_TRANSFER_OK = 	0x00000800;
		static constexpr unsigned long ADM_DM_ERROR = 			0x00001000;
		static constexpr unsigned long ADM_S2MM_SOFT_SHUTDOWN_COMPLETE = 0x00002000;
		static constexpr unsigned long ADM_DM_PACKET_COUNTER = 	0x001F0000;
	};
	struct CAgcControl
	{
		static constexpr unsigned long CCDF_FIFO_FLUSH = 0x00000001;
		static constexpr unsigned long AGC_BLOCK_START = 0x00000004;
	};
	struct CAgcStatus
	{
		static constexpr unsigned long CCDF_FIFO_CNT = 		0x000001ff;		// Note: actual number in FIFO is 2 more than this
		static constexpr unsigned long AGC_RESERVEDA =		0x0000e000;
		static constexpr unsigned long CCDF_FIFO_EMPTY =	0x00001000;
		static constexpr unsigned long CCDF_FIFO_FULL =		0x00002000;
//		static constexpr unsigned long AGC_DONE	=			0x00004000;		// Not working since build 154
		static constexpr unsigned long AGC_RESERVEDB =		0xe0008000;
	};
	struct CBistControl
	{
		static constexpr unsigned long ZYNQ_INTERNAL_TEMP = 0x00000000;
		static constexpr unsigned long ZYNQ_INTERNAL_VCCINT = 0x00000001;
		static constexpr unsigned long ZYNQ_INTERNAL_VCCAUX = 0x00000002;
		static constexpr unsigned long ZYNQ_INTERNAL_VCCBRAM = 0x00000003;
		static constexpr unsigned long ZYNQ_INTERNAL_VCCPINT = 0x00000004;
		static constexpr unsigned long ZYNQ_INTERNAL_VCCPAUX = 0x00000005;
		static constexpr unsigned long ZYNQ_INTERNAL_VCCODDR = 0x00000006;
		static constexpr unsigned long BOARD_5p3V_INPUT 	= 0x00000010;
		static constexpr unsigned long DIGITAL_3p3V 		= 0x00000011;
		static constexpr unsigned long DIGITAL_2p5V 		= 0x00000012;
		static constexpr unsigned long MGT_1p0V 			= 0x00000013;
		static constexpr unsigned long MGT_1p2V 			= 0x00000014;
		static constexpr unsigned long MGT_1p8V 			= 0x00000015;
		static constexpr unsigned long PS_DDR3_VTT 			= 0x00000016;
		static constexpr unsigned long PL_DDR3_VTT 			= 0x00000017;
		static constexpr unsigned long BOARD_TEMP 			= 0x00000018;
		static constexpr unsigned long BOARD_TOTAL_CURR_5p3V = 0x00000019;
		static constexpr unsigned long ADC_3p0V_ANALOG		 = 0x0000001a;
		static constexpr unsigned long ADC_1p8V_ANALOG		 = 0x0000001b;
		static constexpr unsigned long ADC_1p2V_ANALOG		 = 0x0000001c;
		static constexpr unsigned long CLOCK_3p3_ANALOG		 = 0x0000001d;
		static constexpr unsigned long CLOCK_3p3_DIGITAL	 = 0x0000001e;
		static constexpr unsigned long TEMP_REF_2p5V		 = 0x0000001f;
		static constexpr unsigned long SENSOR_REG_ADDR_MASK = 0x0000001f;
		static constexpr unsigned long TIMER_ENABLE =		0x00000100;
		static constexpr unsigned long TIMER_RUNS_ONCE =	0x00000200;
		static constexpr unsigned long ZYNQ_TCK =			0x00001000;
		static constexpr unsigned long ZYNQ_TDI =			0x00002000;
		static constexpr unsigned long ZYNQ_TMS =			0x00004000;
		static constexpr unsigned long RESERVED =			0x00078000;
		static constexpr unsigned long READ_CURRENT_TIME =	0x00080000;
	};
	struct CBistStatus
	{
		static constexpr unsigned long ADC_CODE = 		0x00000fff;
		static constexpr unsigned long ADC_SCALE = 		0x00001000;	// Not actually part of interface
		static constexpr unsigned long OVER_TEMP_ALARM = 0x00001000;
		static constexpr unsigned long USER_TEMP_ALARM = 0x00002000;
		static constexpr unsigned long VCCINT_ALARM = 	0x00004000;
		static constexpr unsigned long VCCAUX_ALARM = 	0x00008000;
		static constexpr unsigned long VCCPINT_ALARM = 	0x00010000;
		static constexpr unsigned long ZYNQ_TDO = 		0x00100000;
	};
	struct CBistGPIORegister
	{
		static constexpr unsigned long RESERVED = 		0x0000001F;
		static constexpr unsigned long LED2_P = 		0x00000020;
		static constexpr unsigned long WATCHDOG_CLEAR =	0x00000040;
	};
	struct CDdcChannelSelect
	{
		static constexpr unsigned long CHANNEL_SELECT_MASK = 0x0000000f;
		static constexpr unsigned long READ_SELECT_MASK = 	0x00000300;
		static constexpr unsigned long READ_SELECT_FREQUENCY = 	0x00000000;
		static constexpr unsigned long READ_SELECT_DECIMATION = 0x00000100;
		static constexpr unsigned long READ_SELECT_GAINCOMP = 	0x00000200;
	};
	struct CDdcDecimation
	{
		static constexpr unsigned long MIN_DATA_DDC_CICDECIMATION = 1;
		static constexpr unsigned long MAX_DATA_DDC_CICDECIMATION = 16384;
		static constexpr unsigned long MIN_AUDIO_DDC_CICDECIMATION = 60;
		static constexpr unsigned long MAX_AUDIO_DDC_CICDECIMATION = 16384;
	};
	struct CDdcGainCompensation
	{
		static constexpr unsigned long CIC_GAIN_MASK = 0x00000fff;
		static constexpr unsigned long CIC_SHIFT_MASK = 0x000ff000;
		static constexpr unsigned long CHANNEL_ENABLE = 0x00100000;
		static constexpr unsigned long IQ_STREAMING_ON = 	0x200000;
		static constexpr unsigned long MIXER_NEG_EVEN_REAL= 0x01000000;
		static constexpr unsigned long MIXER_NEG_EVEN_IMAG= 0x02000000;
		static constexpr unsigned long MIXER_NEG_ODD_REAL =	0x04000000;
		static constexpr unsigned long MIXER_NEG_ODD_IMAG =	0x08000000;

	};
	struct CDemodControl
	{
		static constexpr unsigned long DEMOD_CHAN_0_ON_OFF = 0x00000001;
		static constexpr unsigned long DEMOD_CHAN_1_ON_OFF = 0x00000002;
		static constexpr unsigned long DEMOD_CHAN_2_ON_OFF = 0x00000004;
		static constexpr unsigned long DEMOD_CHAN_3_ON_OFF = 0x00000008;
		static constexpr unsigned long DEMOD_CHAN_4_ON_OFF = 0x00000010;
		static constexpr unsigned long DEMOD_CHAN_MASK = 0x0000001F;
		static constexpr const unsigned long DEMOD_CHAN_ON_OFF[] = { 0x01, 0x02, 0x04, 0x08, 0x10 };
	};
	struct CDemodChannelSelect
	{
		static constexpr unsigned long CHANNEL_SELECT_MASK = 0x00000007;
		static constexpr unsigned long READ_SELECT_MASK = 	0x00007000;
		static constexpr unsigned long READ_SELECT_DEMOD_PARAMS = 	0x00000000;
		static constexpr unsigned long READ_SELECT_BFO_PHASE_INCR = 0x00001000;
	};
	struct CDemodParams
	{
		static constexpr unsigned long DEMOD_TYPE_MASK = 0x00000007;
		static constexpr unsigned long DEMOD_TYPE_FM = 0x00000000;
		static constexpr unsigned long DEMOD_TYPE_AM = 0x00000001;
		static constexpr unsigned long DEMOD_TYPE_USB = 0x00000002;
		static constexpr unsigned long DEMOD_TYPE_LSB = 0x00000003;
		static constexpr unsigned long DEMOD_TYPE_CW = 0x00000004;
		static constexpr unsigned long DEMOD_BANDWIDTH_MASK = 0x000000f0;
		static constexpr const unsigned long DEMOD_BANDWIDTHS[][2] =
		{
			{  300, 0x00}, { 1000, 0x10}, { 3000, 0x20}, { 6000, 0x30}, {  6250, 0x40}, {  9000, 0x50}, { 10000, 0x60}, { 12500, 0x70},
			{20000, 0x80}, {25000, 0x90}, {30000, 0xa0}, {50000, 0xb0}, {100000, 0xc0}, {150000, 0xd0}, {200000, 0xe0}, {300000, 0xf0}
		};
		static constexpr unsigned long DEMOD_ALC_DISABLE = 0x00001000;
		static constexpr unsigned long DEMOD_LOCAL_AUDIO = 0x00002000;
		static constexpr unsigned long DEMOD_CHANNEL_RESET = 0x00004000;
	};
	struct CDemodBfoPhaseIncr
	{
		static constexpr unsigned long DEMOD_BFO_MASK = 0x000fffff;
	};
	struct CFftControl
	{
		static constexpr unsigned long FFT_FIFO_FLUSH = 	0x00000002;
		static constexpr unsigned long ENABLE_PSD_OUTPUT = 	0x00000004;
		static constexpr unsigned long FFT_IP_RESET = 		0x00000008;
		static constexpr unsigned long FFT_SOURCE_SELECT =	0x00000030;
		static constexpr unsigned long FFT_INPUT_FRONTEND =	0x00000000;
		static constexpr unsigned long FFT_INPUT_DDCM = 	0x00000010;
		static constexpr unsigned long FFT_INPUT_DDR3 = 	0x00000020; // for now use for df mode only
		static constexpr unsigned long FFT_S2MM_DM_RST =	0x00000100;
		static constexpr unsigned long FFT_S2MM_SOFT_SHUTDOWN =	0x00000200;
		static constexpr unsigned long ENABLE_FFT_OUTPUT =	0x00000400;		// If this is set, 40.2 and 40.10 should be 0
		static constexpr unsigned long FFT_DELAY_CONTROL =	0x0000F000;		// delay = blocksize * (n + 1) clocks
		static constexpr unsigned long FFT_DELAY_SHIFT = 12;	// no. of bits to shift left for fft delay
	};
	struct CFftStatus
	{
		static constexpr unsigned long FFT_DM_FIFO_EMPTY = 		0x00000001;
		static constexpr unsigned long FFT_DM_FIFO_FULL = 		0x00000002;
		static constexpr unsigned long FFT_DM_FIFO_LOSTDATA = 	0x00000004;
		static constexpr unsigned long FFT_DM_FIFO_SENDING = 	0x00000008;
		static constexpr unsigned long FFT_DM_SOURCE_READY = 	0x00000010;
		static constexpr unsigned long FFT_DM_DEST_READY =	 	0x00000020;
		static constexpr unsigned long FFT_DM_END_OF_PACKET = 	0x00000040;
		static constexpr unsigned long FFT_DM_END_OF_STREAM = 	0x00000080;
		static constexpr unsigned long FFT_DM_INTERNAL_ERROR = 	0x00000100;
		static constexpr unsigned long FFT_DM_DECODE_ERROR = 	0x00000200;
		static constexpr unsigned long FFT_DM_SLAVE_ERROR = 	0x00000400;
		static constexpr unsigned long FFT_DM_TRANSFER_OK = 	0x00000800;
		static constexpr unsigned long FFT_INPUT_FIFO_EMPTY = 	0x00001000;
		static constexpr unsigned long FFT_INPUT_FIFO_FULL = 	0x00002000;
		static constexpr unsigned long FFT_INPUT_FIFO_DATALOST = 0x00004000;
		static constexpr unsigned long FFT_INPUT_FIFO_SENDING = 0x00008000;
		static constexpr unsigned long FFT_INPUT_SOURCE_READY = 0x00010000;
		static constexpr unsigned long FFT_IP_DEST_READY = 		0x00020000;
		static constexpr unsigned long FFT_S2MM_DATAMOVER_ERROR = 0x00040000;
		static constexpr unsigned long FFT_S2MM_SOFT_SHUTDOWN_COMPLETE = 0x00080000;
		static constexpr unsigned long FFT_READY = FFT_DM_FIFO_EMPTY | FFT_DM_DEST_READY | FFT_INPUT_FIFO_EMPTY | FFT_IP_DEST_READY;
	};
	struct CFftStatus1
	{
		static constexpr unsigned long FFT_I_DM_FIFO_EMPTY = 		0x00000001;
		static constexpr unsigned long FFT_I_DM_FIFO_FULL = 		0x00000002;
		static constexpr unsigned long FFT_I_DM_FIFO_LOSTDATA = 	0x00000004;
		static constexpr unsigned long FFT_I_DM_FIFO_SENDING = 		0x00000008;
		static constexpr unsigned long FFT_I_DM_SOURCE_READY = 		0x00000010;
		static constexpr unsigned long FFT_I_DM_DEST_READY =	 	0x00000020;
		static constexpr unsigned long FFT_I_DM_INTERNAL_ERROR = 	0x00000100;
		static constexpr unsigned long FFT_I_DM_DECODE_ERROR = 		0x00000200;
		static constexpr unsigned long FFT_I_DM_SLAVE_ERROR = 		0x00000400;
		static constexpr unsigned long FFT_I_DM_TRANSFER_OK = 		0x00000800;
		static constexpr unsigned long FFT_I_S2MM_DATAMOVER_ERROR = 0x00001000;
		static constexpr unsigned long FFT_I_S2MM_SOFT_SHUTDOWN_COMPLETE = 0x00002000;
		static constexpr unsigned long FFT_Q_DM_FIFO_EMPTY = 		0x00010000;
		static constexpr unsigned long FFT_Q_DM_FIFO_FULL = 		0x00020000;
		static constexpr unsigned long FFT_Q_DM_FIFO_LOSTDATA = 	0x00040000;
		static constexpr unsigned long FFT_Q_DM_FIFO_SENDING = 		0x00080000;
		static constexpr unsigned long FFT_Q_DM_SOURCE_READY = 		0x00100000;
		static constexpr unsigned long FFT_Q_DM_DEST_READY =	 	0x00200000;
		static constexpr unsigned long FFT_Q_DM_INTERNAL_ERROR = 	0x01000000;
		static constexpr unsigned long FFT_Q_DM_DECODE_ERROR = 		0x02000000;
		static constexpr unsigned long FFT_Q_DM_SLAVE_ERROR = 		0x04000000;
		static constexpr unsigned long FFT_Q_DM_TRANSFER_OK = 		0x08000000;
		static constexpr unsigned long FFT_Q_S2MM_DATAMOVER_ERROR = 0x10000000;
		static constexpr unsigned long FFT_Q_S2MM_SOFT_SHUTDOWN_COMPLETE = 0x20000000;
		static constexpr unsigned long FFT_I_READY = FFT_I_DM_FIFO_EMPTY | FFT_I_DM_DEST_READY;
		static constexpr unsigned long FFT_Q_READY = FFT_Q_DM_FIFO_EMPTY | FFT_Q_DM_DEST_READY;
		static constexpr unsigned long FFT_IQ_READY = FFT_I_READY | FFT_Q_READY;
		static constexpr unsigned long FFT_IQ_DM_TRANSFER_OK = FFT_I_DM_TRANSFER_OK | FFT_Q_DM_TRANSFER_OK;
	};
	struct CFormatControl
	{
		static constexpr unsigned long FORMAT_INPUT_FRONTEND =	0x00000000;
		static constexpr unsigned long FORMAT_INPUT_DDCM = 	0x00001000;
		static constexpr unsigned long STREAMING_MODE = 	0x00000001;
		static constexpr unsigned long FORMAT_FIFO_FLUSH = 	0x00000002;
		static constexpr unsigned long ENABLE_FRONTEND_IQ_SAMPLES = 0x00000004;
		static constexpr unsigned long X2_GAIN =			0x00000008;
		static constexpr unsigned long MIXER_NEG_EVEN_REAL= 0x00000010;
		static constexpr unsigned long MIXER_NEG_EVEN_IMAG= 0x00000020;
		static constexpr unsigned long MIXER_NEG_ODD_REAL =	0x00000040;
		static constexpr unsigned long MIXER_NEG_ODD_IMAG =	0x00000080;
		static constexpr unsigned long S2MM_DM_RST =		0x00000100;
		static constexpr unsigned long S2MM_SOFT_SHUTDOWN = 0x00000200;
		static constexpr unsigned long ENABLE_DDCM_IQ_SAMPLES = 0x00000400;
		static constexpr unsigned long FORMAT_SOURCE_SELECT	  = 0x00003000;
	};
	struct CFormatStatus
	{
		static constexpr unsigned long FORMAT_FIFO_EMPTY = 	0x00000001;
		static constexpr unsigned long FORMAT_FIFO_FULL = 	0x00000002;
		static constexpr unsigned long FORMAT_FIFO_LOSTDATA = 0x00000004;
		static constexpr unsigned long SENDING_DATA =		0x00000008;
		static constexpr unsigned long SOURCE_READY	= 		0x00000010;
		static constexpr unsigned long DESTINATION_READY = 	0x00000020;
		static constexpr unsigned long END_OF_PACKET =		0x00000040;
		static constexpr unsigned long END_OF_STREAM =		0x00000080;
		static constexpr unsigned long INTERNAL_ERROR =		0x00000100;
		static constexpr unsigned long DECODE_ERROR =		0x00000200;
		static constexpr unsigned long SLAVE_ERROR =		0x00000400;
		static constexpr unsigned long TRANSFER_OK =		0x00000800;
		static constexpr unsigned long S2MM_DATAMOVER_ERROR = 0x00001000;
		static constexpr unsigned long S2MM_SOFT_SHUTDOWN_COMPLETE = 0x00002000;
		static constexpr unsigned long FORMAT_READY = FORMAT_FIFO_EMPTY | DESTINATION_READY;
	};
	struct CGlobalControl
	{
		static constexpr unsigned long SW_RESET = 					0x00000001;
		static constexpr unsigned long MMCM_PLL_RESET = 			0x00000002;
		static constexpr unsigned long QPLL_RESET = 				0x00000004;
		static constexpr unsigned long INTERRUPT_SELECT =			0x00000008;
		static constexpr unsigned long ADC_GTX_RESET = 				0x00000010;
		static constexpr unsigned long JESD204B_IP_RESET= 			0x00000020;
		static constexpr unsigned long ADC_CALIBRATION_START =		0x00000040;
		static constexpr unsigned long HMC1035_REGULATOR_ENABLE =	0x00000080;
		static constexpr unsigned long SI5338_CLOCK_MEAS_ENABLE =	0x00000100;
		static constexpr unsigned long LMK04828_RESET = 			0x00000200;
		static constexpr unsigned long LMK04828_SYNC_REQUEST = 		0x00000400;
		static constexpr unsigned long NIOSIICPU_RESET =			0x00000800;
		static constexpr unsigned long OCXO_LOOP_INHIBIT = 			0x00001000;
		static constexpr unsigned long USB_12MHZ_CLK_ENABLE = 		0x00002000;
		static constexpr unsigned long ONEPPS_PORT_DIRECTION = 		0x00004000;
		static constexpr unsigned long HW_WATCHDOG_ENABLE = 		0x00008000;
		static constexpr unsigned long PLDDR3_RESET = 				0x00010000;
	};
	struct CGlobalStatus
	{
		static constexpr unsigned long MMCM_PLL_LOCK = 		0x00000001;
		static constexpr unsigned long QPLL_PLL_LOCK = 		0x00000002;
		static constexpr unsigned long ADC_GTX_RESET_DONE = 0x00000004;
		static constexpr unsigned long MHZ100_PLL_LOCK = 	0x00000008;
		static constexpr unsigned long PL_DDR3_MMCM_LOCK = 	0x00000010;
		static constexpr unsigned long PL_DDR3_INIT_CAL_DONE = 0x00000020;
		static constexpr unsigned long PL_DDR3_SATA_DONE = 0x00000300;
		static constexpr unsigned long GLOBAL_STATUS_READY =
			MMCM_PLL_LOCK | QPLL_PLL_LOCK | ADC_GTX_RESET_DONE | MHZ100_PLL_LOCK | PL_DDR3_MMCM_LOCK | PL_DDR3_INIT_CAL_DONE; //| PL_DDR3_SATA_DONE;
	};
	struct CGlobalInterrupts
	{
		static constexpr unsigned long AGC_DONE	= 				0x00000001;
		static constexpr unsigned long AGC_FIFO_FULL =			0x00000002;
		static constexpr unsigned long IQ_DONE =				0x00000004;
		static constexpr unsigned long IQ_SUBBLOCK_DONE =		0x00000008;
		static constexpr unsigned long IQ_DATAMOVER_ERROR =		0x00000010;
		static constexpr unsigned long FORMAT_FIFO_DATA_LOST =	0x00000020;
		static constexpr unsigned long FFT_DONE =				0x00000040;
		static constexpr unsigned long FFT_DATAMOVER_ERROR =	0x00000080;
		static constexpr unsigned long FFT_OUT_FIFO_DATA_LOST =	0x00000100;
		static constexpr unsigned long FFT_IN_FIFO_DATA_LOST =	0x00000200;
		static constexpr unsigned long SEQUENCER_DONE =			0x00000400;
		static constexpr unsigned long SEQUENCER_BLOCK_DONE =	0x00000800;
		static constexpr unsigned long USER_SHUTDOWN =			0x00004000;
		static constexpr unsigned long RF_BOARD_PLL_LOCK =		0x00008000;
		static constexpr unsigned long BIST_TIMER =				0x00010000;
		static constexpr unsigned long OVER_TEMPERATURE =		0x00020000;
		static constexpr unsigned long ONE_PPS =				0x00040000;
		static constexpr unsigned long ADM_IQ =					0x00080000;
		static constexpr unsigned long ADM_DEMOD =				0x00100000;
	};
	struct CSequencerControl
	{
		static constexpr unsigned long SEQUENCER_BLOCK_START = 	0x00000001;
		static constexpr unsigned long SEQUENCER_FIFO_FLUSH = 	0x00000002;
		static constexpr unsigned long ANTENNA_WORD_WRITE =	 	0x00000004;
		static constexpr unsigned long DF_SLAVE = 				0x00000008;
		static constexpr unsigned long DF_MODE = 				0x00000010;
	};
	struct CSequencerStatus
	{
		static constexpr unsigned long SEQUENCER_FIFO_COUNT = 	0x000000ff;
		static constexpr unsigned long SEQUENCER_BLOCK_COUNT = 	0x0000ff00;
		static constexpr unsigned long SEQUENCER_FIFO_FULL = 	0x00010000;
		static constexpr unsigned long SEQUENCER_FIFO_EMPTY = 	0x00020000;
		static constexpr unsigned long SEQUENCER_DONE = 		0x00040000;
		static constexpr unsigned long SEQUENCER_BLOCK_DONE = 	0x00080000;
		static constexpr unsigned long SEQUENCER_ERROR = 		0x00100000;
	};
	struct CSequencerAntennaControl
	{
		static constexpr unsigned long CAL_TO_ANT =		 		0x00000001;
		static constexpr unsigned long CAL_TO_REF =		 		0x00000002;
		static constexpr unsigned long CAL0 =		 			0x00000004;
		static constexpr unsigned long CAL1 =		 			0x00000008;
		static constexpr unsigned long CALTONE_NOISESOURCE =	0x00000010;
		static constexpr unsigned long CBAND_VUHF_ELEMENT =		0x00000020;
		static constexpr unsigned long CBAND_VUHF_AMP =		 	0x00000040;
		static constexpr unsigned long VERT_HORIZ =		 		0x00000080;
		static constexpr unsigned long HF_ANT_SELECT =		 	0x00000100;
		static constexpr unsigned long LED0_N =		 			0x00000100;
		static constexpr unsigned long RESERVED =		 		0x00003E00;
		static constexpr unsigned long DEFAULT_ANTENNA =		0x0003C000;
		static constexpr unsigned long ANT_SHIFT = 14;	// number of bits to shift antenna selection
	};
	struct CSequencerAntennaControlHF
	{
		static constexpr unsigned long CAL_TO_ANT =		 		0x00000001;
		static constexpr unsigned long CAL_TO_REF =		 		0x00000002;
		static constexpr unsigned long CAL_EN =		 			0x00000004;
		static constexpr unsigned long NOISE_GEN =	 			0x00000008;
		static constexpr unsigned long NOISE_SW =				0x00000010;
		static constexpr unsigned long REF_SOURCE =				0x00000020;
		static constexpr unsigned long LB_BYPASS =			 	0x00000040;
		static constexpr unsigned long REF_TERM =		 		0x00000080;
		static constexpr unsigned long HF_ANT_SELECT =		 	0x00000100;
		static constexpr unsigned long RESERVED =		 		0x00003E00;
		static constexpr unsigned long CHAN_SELECT =			0x0003C000;
		static constexpr unsigned long ANT_SHIFT = 14;	// number of bits to shift antenna selection
	};
	struct CSequencerAntennaStatus
	{
		// Most bits require CSequencerControl::ANTENNA_WORD_READ = 1
		static constexpr unsigned long UNUSED1 =		 		0x00000003;
		static constexpr unsigned long K11242_D5 =		 		0x00000004;
		static constexpr unsigned long K11242_D6 =		 		0x00000008;
		static constexpr unsigned long K11242_D2 =		 		0x00000010;
		static constexpr unsigned long UNUSED2 =				0x000001E0;
		static constexpr unsigned long WHIP_ANT_FAULT =		 	0x00000200;	// always readable
		static constexpr unsigned long MAST_UP_DOWN =		 	0x00000400;	// always readable
		static constexpr unsigned long RESERVED =		 		0x00003800;
		static constexpr unsigned long K11242_D0 =		 		0x00004000;
		static constexpr unsigned long K11242_D1 =		 		0x00008000;
		static constexpr unsigned long K11242_D3 =		 		0x00010000;
		static constexpr unsigned long K11242_D4 =		 		0x00020000;
	};
	struct CSignalControl
	{
		static constexpr unsigned long ADC_CHANNEL1_SELECT = 	0x00000000;
		static constexpr unsigned long ADC_CHANNEL2_SELECT = 	0x00000001;
		static constexpr unsigned long TEST_SIGNAL = 			0x00000002;
		static constexpr unsigned long TEST_TONE_SIGNAL_1 = 	0x00000000;
		static constexpr unsigned long TEST_TONE_SIGNAL_7 =		0x00000004;
		static constexpr unsigned long FRONTEND_RESET =			0x00000008;
		static constexpr unsigned long OUTPUT_NORESAMPLER = 	0x00000000;
		static constexpr unsigned long OUTPUT_RESAMPLER56 = 	0x00000040;
		static constexpr unsigned long OUTPUT_RESAMPLER89 = 	0x00000080;
		static constexpr unsigned long OUTPUT_RESAMPLER_MASK =	0x000000c0;
	};
	struct CTimestampControl
	{
		static constexpr unsigned long PPS_OUT_CONTROL =			0x00000001;
		static constexpr unsigned long PPS_TIME_REFERENCE =			0x00000002;
		static constexpr unsigned long MODULE_RESET = 				0x00000004;
//		static constexpr unsigned long READ_CURRENT_TIME =			0x00000008;
		static constexpr unsigned long RESET_DRIFT = 				0x00000010;
		static constexpr unsigned long INTERNAL_SEQUENCE_TRIG =		0x00000020;
		static constexpr unsigned long PPS_OUT_ENABLE =				0x00000040;
		static constexpr unsigned long TEST_MODE =					0x00008000;
	};
	struct CDMARegControl
	{
		static constexpr unsigned long DMA_RESET_CONTROL = 		0x00010004;
		static constexpr unsigned long DMA_ENABLE_INTERRUPT =	0x00011000;

	};
	struct CTimestampStatus
	{
		static constexpr unsigned long PPS_ERROR2 =			0x000000FF;
		static constexpr unsigned long PPS_ERROR =			0x0000FF00;
		static constexpr unsigned long PPS_TRACK_MODE = 	0x08000000;
		static constexpr unsigned long CURRENT_TIME_VALID = 0x10000000;
		static constexpr unsigned long INPUT_PPS_LOSS = 	0x20000000;
		static constexpr unsigned long PPS_LOCK1 =			0x40000000;
		static constexpr unsigned long PPS_LOCK2 =			0x80000000;
		static constexpr unsigned long TIMESTAMP_STARTUP_STATUS_MASK = PPS_TRACK_MODE | INPUT_PPS_LOSS | PPS_LOCK1;
		static constexpr unsigned long TIMESTAMP_STARTUP_STATUS_READY = PPS_TRACK_MODE | PPS_LOCK1;
		static constexpr unsigned long TIMESTAMP_STATUS_MASK = INPUT_PPS_LOSS | PPS_LOCK1;
		static constexpr unsigned long TIMESTAMP_STATUS_READY = PPS_LOCK1;
	};
	static constexpr const unsigned long m_bistVoltages[] =
	{
		CBistControl::ZYNQ_INTERNAL_VCCINT,
		CBistControl::ZYNQ_INTERNAL_VCCAUX,
		CBistControl::ZYNQ_INTERNAL_VCCBRAM,
		CBistControl::ZYNQ_INTERNAL_VCCPINT,
		CBistControl::ZYNQ_INTERNAL_VCCPAUX,
		CBistControl::ZYNQ_INTERNAL_VCCODDR,
		CBistControl::BOARD_5p3V_INPUT,
		CBistControl::DIGITAL_3p3V,
		CBistControl::DIGITAL_2p5V,
		CBistControl::MGT_1p0V,
		CBistControl::MGT_1p2V,
		CBistControl::MGT_1p8V,
		CBistControl::PS_DDR3_VTT,
		CBistControl::PL_DDR3_VTT,
		CBistControl::BOARD_TOTAL_CURR_5p3V,
		CBistControl::ADC_3p0V_ANALOG,
		CBistControl::ADC_1p8V_ANALOG,
		CBistControl::ADC_1p2V_ANALOG,
		CBistControl::CLOCK_3p3_ANALOG,
		CBistControl::CLOCK_3p3_DIGITAL,
		CBistControl::TEMP_REF_2p5V
	};

	static constexpr const float m_bistLimits[][2] =
	{
		{-40.f, 100.f}, {0.97f, 1.03f}, {1.71f, 1.89f}, {0.97f, 1.03f}, {0.95f, 1.05f}, {1.71f, 1.89f}, {1.2825f, 1.4175f}, {0, 0}, // 00-07
		{0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, // 08-0F
		{4.95f, 5.565f}, {3.2f, 3.6f}, {2.4f, 2.6f}, {0.97f, 1.08f}, {1.17f, 1.23f}, {1.75f, 1.85f}, {0.6412f, 0.7087f}, {0.6412f, 0.7087f}, // 10-17
		{0.0f, 60.0f}, {0.0f, 4.0f}, {2.8f, 3.2f}, {1.7f, 1.9f}, {1.1f, 1.3f}, {3.2f, 3.6f}, {3.2f, 3.6f}, {2.4f, 2.6f} // 18-1f
	};
	static constexpr const float m_bistScaleFactors[] =
	{
		0, 3.0f, 3.0f, 3.0f, 3.0f, 3.0f, 3.0f, 0, // 00-07
		0, 0, 0, 0, 0, 0, 0, 0, // 08-0f
		(6.01f / 1.02f), (4.14f / 1.13f), (3.36f / 1.21f), 1.0f, (4.01f / 3.01f), 2.0f, 1.0f, 1.0f,	// 10-17
		0, (1.0f / 35.84f) * (1.0f / .0044f), 3.0f, 2.0f, (4.01f / 3.01f), (4.14f / 1.13f), (4.14f / 1.13f), (3.36f / 1.21f) // 18-1f
	};
	// Sequencer data
	struct SSequencerData
	{
		unsigned long antennaControl;	// 18 bits
		unsigned long rfDelay;
		unsigned long blockSize;
		unsigned long iqPLDDR3Address;
		unsigned long psdPLDDR3Address;
		unsigned long fftIPLDDR3Address;
		unsigned long fftQPLDDR3Address;
	};
};

