/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"
#include <sys/mman.h>

#include "Log.h"
#include "SBuffer.h"

#ifdef CSMS_DEBUG
#include "Processing.h"

size_t CSBufferBase::s_onSampleCnt = 0;
size_t CSBufferBase::s_sampleFlushCnt = 0;
size_t CSBufferBase::s_sampleFndCnt = 0;
size_t CSBufferBase::s_sampleWaitCnt = 0;
bool CSBufferBase::s_sampleTimeout = false;
bool CSBufferBase::s_traceAvd = false;
#endif

////////////////////  This class uses the PL DDR3 memory for the buffer //////////////////

//////////////////////////////////////////////////////////////////////
//
// Assign values for memory buffer and size
//
void CSBufferPLddr3::Assign(const CPLddr3& addr, size_t size, off_t base)
{
	m_buffer = addr;
	m_bufSize = size / m_buffer.ItemSize();
	m_basePhysicalAddress = base;
	printf("Assign %u bytes %lu items at %p, physical %lu\n", size, m_bufSize, addr.Get(), base);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Fill memory with a known pattern
//
void CSBufferPLddr3::FillMemory(unsigned long sampleCount, unsigned long count)
{
	if (!m_buffer)
	{
		return;
	}
	if (sampleCount + count > m_bufSize)
	{
		printf("Error in FillMemory %lu %lu\n", sampleCount, count);
		return;
	}
	auto addr = m_buffer + sampleCount;
	addr.FillBlock(m_buffer, m_buffer + m_bufSize, count);
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Find samples in DDR buffer or wait until they arrive
//
bool CSBufferPLddr3::FindSamples(unsigned long sampleCount, unsigned long count, std::vector<unsigned long>& data, unsigned long timeout)
{
	if (!m_buffer)
	{
		return false; // nullptr;
	}
	if (!CSBufferBase::FindSamples(sampleCount, count, timeout))
	{
		return false;
	}
	auto addr = m_buffer + sampleCount;
	addr.GetBlock(m_buffer, m_buffer + m_bufSize, count, data);
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Map a block of memory
//
bool CSBufferPLddr3::MapMemory(int driverDesc, size_t totalSize, size_t size, CPLddr3& virtualAddress)
{
	m_usedPhysicalMemory += size;
	if (m_usedPhysicalMemory > totalSize)
	{
		CLog::Log(CLog::ERRORS, "CSBufferPLddr3: ddr3 memory overflow");
		return false;
	}
	void* ret;
	if ((ret = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, driverDesc, m_nextPhysicalAddress)) == MAP_FAILED)
	{
		CLog::Log(CLog::ERRORS, "CSBufferPLddr3: Failed to map ddr3: %s (%d)", std::strerror(errno), errno);
		return false;
	}
	SMappedMemory m;
	m.addr = static_cast<volatile unsigned long*>(ret);
	m.size = size;
	m_mappedMemory.push_back(m);	// save for munmap
	virtualAddress.Assign(static_cast<volatile unsigned long*>(ret));
	CLog::Log(CLog::INFORMATION, "CSBufferPLddr3: User space ddr3 address: %p size %u maps to %08lx", virtualAddress.Get(), size, m_nextPhysicalAddress);
	m_nextPhysicalAddress += size;
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Mark (reserve) space in the buffer for samples to be stored
//
bool CSBufferPLddr3::MarkSamplesInUse(unsigned long sampleCount, unsigned long count)
{
	if (!m_buffer)
	{
		printf("PL DDR3 buffer not defined\n");		// log
		return false;
	}
	return CSBufferBase::MarkSamplesInUse(sampleCount, count);
}


//////////////////////////////////////////////////////////////////////
//
// Get physical address from offset (sampleCount)
//
unsigned long CSBufferPLddr3::PhysicalAddress(unsigned long sampleCount)
{
	return m_basePhysicalAddress + sampleCount * m_buffer.ItemSize();
}

//////////////////////////////////////////////////////////////////////
//
// Get physical address from offset (sampleCount)
//
unsigned long CSBufferPLddr3::PhysicalAddress(unsigned long offsetbase, unsigned long sampleCount)
{
	return (m_basePhysicalAddress + offsetbase) + sampleCount * m_buffer.ItemSize();
}


//////////////////////////////////////////////////////////////////////
//
// Read memory
//
void CSBufferPLddr3::ReadMemory(unsigned long sampleCount, unsigned long count, std::vector<unsigned long>& data)
{
	if (!m_buffer)
	{
		return;
	}
	if (sampleCount + count > m_bufSize)
	{
		printf("Error in FillMemory %lu %lu\n", sampleCount, count);
		return;
	}
	auto addr = m_buffer + sampleCount;
	addr.GetBlock(m_buffer, m_buffer + m_bufSize, count, data);
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Check if enough space in buffer for new samples or wait until it is
//
bool CSBufferPLddr3::WaitForBufferSpace(unsigned long count, unsigned long& sampleCount) const
{
	if (!m_buffer)
	{
		printf("PL DDR3 buffer not defined\n");		// log
		return false;
	}
	return CSBufferBase::WaitForBufferSpace(count, sampleCount);
}

////////////////////  This class uses the PS DMA memory for the buffer //////////////////

//////////////////////////////////////////////////////////////////////
//
// Assign values for memory buffer and size
//
//void CSBufferPSdma::Assign(volatile unsigned long* addr, size_t size, off_t base)
void CSBufferPSdma::Assign(const uncached_ptr& addr, size_t size, off_t base)
{
	m_buffer = addr;
	m_bufSize = size / sizeof(unsigned long);
	m_basePhysicalAddress = base;
	printf("Assign %u bytes %lu items at %p, physical %lu\n", size, m_bufSize, addr.get(), base);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Find samples in DMA buffer or wait until they arrive
//
//bool CSBufferPSdma::FindSamples(unsigned long sampleCount, unsigned long count, unsigned long*& data, unsigned long timeout)
bool CSBufferPSdma::FindSamples(unsigned long sampleCount, unsigned long count, uncached_ptr& data, unsigned long timeout)
{
	if (!m_buffer)
	{
		return false; // nullptr;
	}
	if (!CSBufferBase::FindSamples(sampleCount, count, timeout))
	{
		return false;
	}
	data = m_buffer + sampleCount;

	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Map a block of memory
//
//bool CSBufferPSdma::MapMemory(int driverDesc, size_t totalSize, size_t size, volatile unsigned long*& virtualAddress)
bool CSBufferPSdma::MapMemory(int driverDesc, size_t totalSize, size_t size, uncached_ptr& virtualAddress)
{
	m_usedPhysicalMemory += size;
	if (m_usedPhysicalMemory > totalSize)
	{
		CLog::Log(CLog::ERRORS, "CSBufferPSdma: dma memory overflow");
		return false;
	}
	void* ret;
	if ((ret = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, driverDesc, m_nextPhysicalAddress)) == MAP_FAILED)
	{
		CLog::Log(CLog::ERRORS, "CSBufferPSdma: Failed to map dma: %s (%d)", std::strerror(errno), errno);
		return false;
	}
	SMappedMemory m;
	m.addr = static_cast<volatile unsigned long*>(ret);
	m.size = size;
	m_mappedMemory.push_back(m);	// save for munmap
	virtualAddress.set(static_cast<unsigned long*>(ret));
	CLog::Log(CLog::INFORMATION, "CSBufferPSdma: User space dma address: %p size %u maps to %08lx", virtualAddress.get(), size, m_nextPhysicalAddress);
	m_nextPhysicalAddress += size;
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Mark (reserve) space in the buffer for samples to be stored
//
bool CSBufferPSdma::MarkSamplesInUse(unsigned long sampleCount, unsigned long count)
{
	if (!m_buffer)
	{
		printf("PS DMA buffer not defined\n");		// log
		return false;
	}
	return CSBufferBase::MarkSamplesInUse(sampleCount, count);
}


//////////////////////////////////////////////////////////////////////
//
// Get physical address from offset (sampleCount)
//
unsigned long CSBufferPSdma::PhysicalAddress(unsigned long sampleCount)
{
	return m_basePhysicalAddress + sampleCount * sizeof(unsigned long);
}


//////////////////////////////////////////////////////////////////////
//
// Check if enough space in buffer for new samples or wait until it is
//
bool CSBufferPSdma::WaitForBufferSpace(unsigned long count, unsigned long& sampleCount) const
{
	if (!m_buffer)
	{
		printf("PS DMA buffer not defined\n");		// log
		return false;
	}
	return CSBufferBase::WaitForBufferSpace(count, sampleCount);
}


/////////////////////////    Base class //////////////////////////////

//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CSBufferBase::~CSBufferBase(void)
{
	for (size_t i = 0; i < m_mappedMemory.size(); ++i)
	{
		if (m_mappedMemory[i].addr && m_mappedMemory[i].size != 0)
		{
			TRACE("CSBufferBase unmapping %p %u\n", m_mappedMemory[i].addr, m_mappedMemory[i].size);
			munmap(const_cast<unsigned long*>(m_mappedMemory[i].addr), m_mappedMemory[i].size);
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Make block of sample data available and notify waiters
//
void CSBufferBase::DoneWithSamples(unsigned long sampleCount)
{
	std::lock_guard<std::mutex> lock(m_dataCritSect);

	auto availableData = m_available.begin();
	if (availableData == m_available.end())
	{
		printf("CSBufferBase::DoneWithSamples error %lu\n", sampleCount);
		return;
	}
	if (sampleCount != availableData->offset)
	{
		printf("CSBufferBase::DoneWithSamples error match %lu %lu\n", sampleCount, availableData->offset);
		return;
	}
	m_moreBufferSpace.notify_all();
	m_available.erase(availableData);
//	ShowBuffer("Done");

#ifdef CSMS_DEBUG
	if(s_traceAvd)
	{
		TRACE("CSBufferBase::DoneWithSamples: sampleCount %lu\n", sampleCount);
	}
#endif

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Find samples in DDR buffer or wait until they arrive
//
bool CSBufferBase::FindSamples(unsigned long sampleCount, unsigned long count, unsigned long timeout)
{
#ifdef CSMS_DEBUG
	s_sampleTimeout = false;
#endif
	if (sampleCount + count > m_bufSize)
	{
		printf("CSBufferBase::FindSamples Error: %lu %lu\n", sampleCount, count);	// assert or throw
		return false; // nullptr;
	}

	std::unique_lock<std::mutex> lock(m_dataCritSect);
	if (m_reserved.empty() && m_available.empty())
	{
		TRACE("CSBufferBase::FindSamples Error: Nothing reserved or available\n");
		return false;	// There is not going to be any data to find.
	}
	// Loop until we find samples or timeout
	while (true)
	{
		auto availableData = m_available.begin();
		if (availableData == m_available.end())
		{
#ifdef CSMS_DEBUG
#ifdef CSMS_2016
			++s_sampleWaitCnt;
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);

			TRACE("CSBufferBase::FindSamples sec = %ld nsec =%ld; wait(%u) for data %lu %lu, onSample %u, sampleFnd %u, 3220sampleFnd %u\n",
					ts.tv_sec, ts.tv_nsec, s_sampleWaitCnt, sampleCount, count, s_onSampleCnt, s_sampleFndCnt,  C3230::s_psdSampleCnt);
			//Output interrupt info.
			C3230::IntrOutTrace();
#endif
#endif

			if (m_newSamples.wait_for(lock, std::chrono::milliseconds(timeout)) == std::cv_status::timeout)
			{
#ifdef CSMS_DEBUG
#ifdef CSMS_2016
				C3230::ScratchRegsOutTrace();
				TRACE("CSBufferBase::FindSamples Error: timed out waiting for %lu samples from %lu, found %u, OnSample %u\n",
						count, sampleCount, s_sampleFndCnt, s_onSampleCnt);
#endif
				s_sampleTimeout = true;
#else
				printf("CSBufferBase::FindSamples Error: timed out waiting for %lu samples from %lu\n", count, sampleCount);
#endif
				return false; // nullptr;
			}
		}
		else
		{
			if (sampleCount == availableData->offset && count == availableData->count)
			{
#ifdef CSMS_DEBUG
				++s_sampleFndCnt;

				if(s_traceAvd)
				{
					timespec ts;
					clock_gettime(CLOCK_MONOTONIC, &ts);
					TRACE("FindSamples(%u, OnSample %u) sec = %ld nsec =%ld; found %lu %lu, %lu %lu\n",
							s_sampleFndCnt, s_onSampleCnt, ts.tv_sec, ts.tv_nsec, sampleCount, availableData->offset, count, availableData->count);
				}
#endif
				return true;
			}
			// else discard this block of data and try the next one
			// debug output
			if (availableData->offset + availableData->count != sampleCount || count != 2 * availableData->count)
			{
				printf("CSBufferBase::FindSamples Error: discarding %lu != %lu or %lu != %lu\n", sampleCount, availableData->offset, count, availableData->count);
			}
			m_available.erase(availableData);
			m_moreBufferSpace.notify_all();
//			ShowBuffer("Find");
		}
	}
	return false; // nullptr;		// Quiets code analysis, although unreachable
}


//////////////////////////////////////////////////////////////////////
//
// Flush samples previously reserved my MarkSamplesInUse
//
bool CSBufferBase::FlushSamples(unsigned long sampleCount, unsigned long count)
{
	std::lock_guard<std::mutex> lock(m_dataCritSect);

	// First see if the samples are still in m_reserved
	bool found = !m_reserved.empty() && sampleCount == m_reserved.back().offset && count == m_reserved.back().count;

#ifdef CSMS_DEBUG
	++s_sampleFlushCnt;

	if(s_traceAvd)
	{
		TRACE("CSBufferBase::FlushSamples found %d, sampleCount %lu, count %lu, numFlushCalls = %u\n",
				(found ? 1 :0), sampleCount, count, s_sampleFlushCnt);
	}
#endif

#ifdef CSMS_DEBUG
	//////////////////////////////
	/// @note: It is possible in Slave to have !found1
	if (!found)
	{
		if (m_reserved.empty())
		{
			TRACE("CSBufferBase::FlushSamples Error: found no reserved space, FlushCnt = %u\n",
					s_sampleFlushCnt);
		}
		else
		{
			if (sampleCount != m_reserved.back().offset || count != m_reserved.back().count)
			{
				TRACE("CSBufferBase::FlushSamples Error: No reserved match (%lu,%lu)!= (%lu,%lu), numFlushCalls = %u\n",
						sampleCount, count, m_reserved.back().offset, m_reserved.back().count, s_sampleFlushCnt);
			}
		}
	}
#endif
	////////////////////////////////

	if (found)
	{
		m_reserved.pop_back();	// Remove the item just added by MarkSamplesInUse
	}
	else
	{
		// Not found in m_reserved try m_available (OnSamples may have done a push_back)
		found = !m_available.empty() && sampleCount == m_available.back().offset && count == m_available.back().count;

#ifdef CSMS_DEBUG
		if (!found)
		{
			if (m_available.empty())
			{
				TRACE("CSBufferBase::FlushSamples Error: found no available space\n");
			}
			else
			{
				if (sampleCount != m_available.back().offset || count != m_available.back().count)
				{
					TRACE("CSBufferBase::FlushSamples Error: available match error (%lu,%lu)!= (%lu,%lu)\n", sampleCount, count, m_available.back().offset, m_available.back().count);
				}
			}
		}
#endif
		if (found)
		{
			m_available.pop_back();	// Remove the item just added by MarkSamplesInUse and pushed by OnSamples
		}
	}
//	ShowBuffer("FlushSamples");
	return(found);
}


//////////////////////////////////////////////////////////////////////
//
// Get nextPhysicalAddress
//
off_t CSBufferBase::GetNextPhysicalAddress(void) const
{
	return m_nextPhysicalAddress;
}


//////////////////////////////////////////////////////////////////////
//
// Initialization
//
void CSBufferBase::Init(off_t addr)
{
	m_nextPhysicalAddress = addr;
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Mark (reserve) space in the buffer for samples to be stored
//
bool CSBufferBase::MarkSamplesInUse(unsigned long sampleCount, unsigned long count)
{
	if (count > m_bufSize)
	{
		printf("CSBufferBase::MarkSamplesInUse Error: buffer too small\n");		// log
		return false;
	}

#ifdef CSMS_DEBUG
	if(s_traceAvd)
	{
		timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		TRACE("CSBufferBase::MarkSamplesInUse sec = %ld nsec =%ld; sampleCount %lu, count %lu, reserved = %u, m_bufSize %lu\n",
				ts.tv_sec, ts.tv_nsec, sampleCount, count, m_reserved.size(), m_bufSize);
#ifdef CSMS_2016
		if(C3230::ScratchRegsValid())
		{
			C3230::ScratchRegsOutTrace();
		}
#endif
	}
#endif

	std::lock_guard<std::mutex> lock(m_dataCritSect);
	SInUse inUse;
	inUse.offset = sampleCount;
	inUse.count = count;
	m_reserved.push_back(inUse);
//	ShowBuffer("Mark");
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Update available data list on end-of-data interrupt
//
void CSBufferBase::OnSamples(void)
{
#ifdef CSMS_DEBUG
	++s_onSampleCnt;

	if(s_traceAvd)
	{
		TRACE("CSBufferBase::OnSamples called, cnt = %u\n", s_onSampleCnt);
	}
#endif

	std::lock_guard<std::mutex> lock(m_dataCritSect);
	if (m_reserved.empty())
	{
		printf("CSBufferBase::OnSamples Error: found no reserved space\n");
		return;
	}
//	if (notify)
	m_newSamples.notify_all();

	m_available.push_back(m_reserved.front());
	m_reserved.pop_front();
//	ShowBuffer("DataRcvd");
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset the buffer info to initial state
//
void CSBufferBase::Reset(void)
{
#ifdef CSMS_DEBUG
	if(s_traceAvd)
	{
		TRACE("CSBufferBase::Reset called\n");
	}
#endif

	std::lock_guard<std::mutex> lock(m_dataCritSect);
	m_reserved.clear();
	m_available.clear();
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Check if enough space in buffer for new samples or wait until it is
//
bool CSBufferBase::WaitForBufferSpace(unsigned long count, unsigned long& sampleCount) const
{
#ifdef CSMS_DEBUG
	if(s_traceAvd)
	{
		TRACE("CSBufferBase::WaitForBufferSpace count %lu, sampleCount %lu\n",
				count, sampleCount);
	}
#endif

	if (count > m_bufSize)
	{
		printf("CSBufferBase::WaitForBufferSpace Error: buffer too small\n");		// log
		return false;
	}

	std::unique_lock<std::mutex> lock(m_dataCritSect);

	size_t totalSize;
	while ((totalSize = m_reserved.size() + m_available.size()) > 0)
	{
		if (totalSize < MAX_BLOCKS_IN_BUFFER)
		{
			auto firstOffset = (m_available.size() > 0 ? m_available.front().offset : m_reserved.front().offset);
			auto nextOffset = (m_reserved.size() > 0 ? m_reserved.back().offset + m_reserved.back().count : m_available.back().offset + m_available.back().count);
			if (nextOffset > firstOffset)
			{
				if (nextOffset < m_bufSize && m_bufSize - nextOffset >= count)
				{
					sampleCount = nextOffset;
					return true;
				}
				if (firstOffset >= count)
				{
					sampleCount = 0;
#ifdef CSMS_DEBUG
					if(s_traceAvd)
					{
						timespec ts;
						clock_gettime(CLOCK_MONOTONIC, &ts);
						TRACE("CSBufferBase::WaitForBufferSpace sec = %ld nsec =%ld; firstOffset, nextOffset, count %lu %lu %lu\n",
								ts.tv_sec, ts.tv_nsec, firstOffset, nextOffset, count);
					}
#endif
					return true;
				}
			}
			else
			{
				if (firstOffset - nextOffset >= count)
				{
					sampleCount = nextOffset;
#ifdef CSMS_DEBUG
				if(s_traceAvd)
				{
					TRACE("CSBufferBase::WaitForBufferSpace(2) firstOffset, nextOffset, count %lu %lu %lu\n", firstOffset, nextOffset, count);

				}
#endif
					return true;
				}
			}
		}
		else
		{
#ifdef CSMS_DEBUG
				if(s_traceAvd)
				{
					TRACE("CSBufferBase::WaitForBufferSpace totalSize %u\n", totalSize);
				}
#endif
		}

#ifdef CSMS_DEBUG
		if(s_traceAvd)
		{
			TRACE("Waiting for buffer space\n");
		}
#endif
		// Need to wait
		m_moreBufferSpace.wait(lock);
//		TRACE("Done waiting for buffer space\n");
	}

	sampleCount = 0;
	return true;
}


void CSBufferBase::ShowBuffer(const char* str) const
{
	printf("%s:available/reserved = { ", str);
	for (auto& x : m_available)
	{
		printf("(%lu %lu) ", x.offset, x.count);
	}
	printf("} ");
	printf("{ ");
	for (auto& x : m_reserved)
	{
		printf("(%lu %lu) ", x.offset, x.count);
	}
	printf("}\n");
	return;
}
