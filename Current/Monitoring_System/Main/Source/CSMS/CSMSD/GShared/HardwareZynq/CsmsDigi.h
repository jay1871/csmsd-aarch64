/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015-2018 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

// 3230 User Registers.
//
typedef struct
{
	unsigned long Regs;
	size_t size;
} CsmsDigiRegs;

// 3230 Digitizer DMA memory.
//
typedef struct
{
	void* Mem;
	size_t size;
} CsmsDigiDMAMemory;

// 3230 Version and Date.
//
typedef struct
{
  unsigned long version;
  unsigned long date;
} CsmsDigiGetDriverVersion;

// 3230 Driver Instance and Interrupt Mask
//
typedef struct
{
  unsigned long driverInstance;
  unsigned long interruptCount;
  unsigned long interruptMask;
  unsigned long itemCount;
  unsigned long lostInterruptCount;
  unsigned long processedInterruptCount;
  unsigned long queriedInstance;
  unsigned long totalInstances;
} CsmsDigiGetDriverInstanceInfo;

// 3230 program Forward DMA(s).
//
typedef struct
{
	size_t channel; // Forward DMA channel to use.
	size_t size;    // Size in bytes.
	unsigned long count;    // 0 = Streaming Mode, > 0 = Number of blocks for Block mode.
} CsmsDigiFwdDMA;

typedef struct
{
	size_t channel; // Forward DMA channel to use.
	size_t count;    // Number of streaming packets before generating an interrupt.
                 // Must be >= 1. At present, nly valid for forward DMA(s).
} CsmsDigiDMAStreamPacketCount;

// 3230 CSMS interrupt sources
//
#ifndef CSMS_2016
	typedef enum
	{
		CsmsDigi_NONE = 0x00000000,
		CsmsDigi_DMA0 = 0x00000001,
		CsmsDigi_DMA1 = 0x00000002,
		CsmsDigi_DMA2 = 0x00000004,
		CsmsDigi_DMA3 = 0x00000008,
		CsmsDigi_DMA4 = 0x00000010,
		CsmsDigi_DMA5 = 0x00000020,
		CsmsDigi_DMA6 = 0x00000040,
		CsmsDigi_DMA7 = 0x00000080,
		CsmsDigi_agcDone  = 0x00000100,
		CsmsDigi_agcFifoFull = 0x00000200,
		CsmsDigi_iqDone = 0x00000400,
		CsmsDigi_iqBlockDone = 0x00000800,
		CsmsDigi_iqDataMoverErr = 0x00001000,
		CsmsDigi_formatFifoDataErr = 0x00002000,
		CsmsDigi_fftDone = 0x00004000,
		CsmsDigi_fftDataMoverErr = 0x00008000,
		CsmsDigi_fftOutputFifoDataErr = 0x00010000,
		CsmsDigi_fftInputFifoDataErr = 0x00020000,
	CsmsDigi_sequencerDone = 0x00040000,
	CsmsDigi_sequencerBlockDone = 0x00080000,
	CsmsDigi_fftIDone = 0x00100000,
	CsmsDigi_fftQDone = 0x00200000,
		CsmsDigi_shutDown = 0x00400000,
		CsmsDigi_rfPllLock = 0x00800000,
		CsmsDigi_bistTimer = 0x01000000,
		CsmsDigi_overTemp = 0x02000000,
		CsmsDigi_onePps = 0x04000000,
		CsmsDigi_ADM = 0x08000000,
		CsmsDigi_ADMD = 0x10000000,
		CsmsDigi_shutDown2 = 0x20000000,
		CsmsDigi_CDMA_DONE = 0x40000000,
		CsmsDigi_WB_DDC = 0x80000000
	} ECsmsDigiInterruptSource;
#else 
	typedef enum
	{
		CsmsDigi_NONE = 0x00000000,
		CsmsDigi_agcDone = 0x00000001,
		CsmsDigi_agcFifoFull = 0x00000002,
		CsmsDigi_iqDone = 0x000000004,
		CsmsDigi_iqBlockDone = 0x000000008,
		CsmsDigi_iqDataMoverErr = 0x00000010,
		CsmsDigi_formatFifoDataErr = 0x00000020,
		CsmsDigi_fftDone = 0x00000040,
		CsmsDigi_fftDataMoverErr = 0x00000080,
		CsmsDigi_fftOutputFifoDataErr = 0x00000100,
		CsmsDigi_fftInputFifoDataErr = 0x00000200,
		CsmsDigi_sequencerDone = 0x00000400,
		CsmsDigi_sequencerBlockDone = 0x00000800,
		CsmsDigi_fftIDone = 0x00001000,
		CsmsDigi_fftQDone = 0x00002000,
		CsmsDigi_shutDown = 0x00004000,
		CsmsDigi_rfPllLock = 0x00008000,
		CsmsDigi_bistTimer = 0x00010000,
		CsmsDigi_overTemp = 0x00020000,
		CsmsDigi_onePps = 0x00040000,
		CsmsDigi_ADM = 0x00080000,
		CsmsDigi_ADMD = 0x00100000,
		CsmsDigi_shutDown2 = 0x00200000,
		CsmsDigi_CDMA_DONE = 0x00400000,
		CsmsDigi_WB_DDC = 0x00800000,
		CsmsDigi_fftSeqDone = 0x01000000,
		CsmsDigi_fftBlkDone = 0x04000000
	} ECsmsDigiInterruptSource;
#endif

// 3230 Interrupt request structure.
//
typedef struct
{
	unsigned long offset;  // Offset in memory where data exists.
	unsigned long size;  // Size in bytes of available data.
	unsigned long channel;

	ECsmsDigiInterruptSource source;  // Source of interrupt(s).
} CsmsDigi_INTERRUPT;


// IOCTLs
// Acquire a lock on the Regs
//	Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_CSMSDIGI_LOCK_REGS 0x801

// Release a lock on the Regs  = 0x802
//	Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_CSMSDIGI_UNLOCK_REGS 0x803

// Get Regs.
//	Input buffer:  NULL
//	Output buffer: CsmsDigiRegs*
//
#define IOCTL_CSMSDIGI_GET_REGS 0x804

// Wait for interrupt
//	Input buffer:  const unsigned long* (mask of ECsmsDigiInterruptSource)
//	Output buffer: ECsmsDigiInterruptSource*
//
#define IOCTL_CSMSDIGI_WAIT_FOR_INTERRUPT 0x805

// Flush Interrupt Queue
// Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_CSMSDIGI_FLUSH_INTERRUPT_QUEUE 0x806

// Reset Digitizer
// Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_CSMSDIGI_RESET_DIGITIZER 0x807


//	Input buffer:  const unsigned long* (mask of ECsmsDigiInterruptSource
//	Output buffer: NULL
//
#define IOCTL_CSMSDIGI_ENABLE_INTERRUPTS 0x808

// Get DMA Memory
//	Input buffer:  ULONG channel number 0 - 7.
//	Output buffer: CsmsDigiDMAMemory*
//
#define IOCTL_CSMSDIGI_GET_DMA_MEMORY 0x809

// Get Driver Version
// Input buffer:  CsmsDigiGetDriverVersion*
//	Output buffer: CsmsDigiGetDriverVersion
//
#define IOCTL_CSMSDIGI_GET_DRIVER_VERSION 0x80a

// Get Driver Instance Info
// Input buffer:  CsmsDigiGetDriverInstanceInfo*
//	Output buffer: CsmsDigiGetDriverInstanceInfo
//
#define IOCTL_CSMSDIGI_GET_DRIVER_INSTANCE_INFO 0x80b

// Set wait for interrupts timeout.
// Input unsigned long* timeout
//	Output buffer: NULL
//
#define IOCTL_CSMSDIGI_SET_WAIT_TIMEOUT 0x80d

// Block/Stream Data transfer via Forward DMA
// Input buffer:  CsmsDigiFwdDMA*
//	Output buffer: NULL
//
#define IOCTL_CSMSDIGI_PROG_FWD_DMA 0x810

// Acquire a lock on the CDMA Regs
//	Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_CSMSDIGI_LOCK_CDMA_REGS 0x8011

// Release a lock on the CDMA Regs
//	Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_CSMSDIGI_UNLOCK_CDMA_REGS 0x812

// Get CDMA Regs.
//	Input buffer:  NULL
//	Output buffer: CsmsDigiRegs*
//
#define IOCTL_CSMSDIGI_GET_CDMA_REGS 0x813


// Acquire a lock on the Scratch Regs
//	Input buffer:  NULL
//	Output buffer: NULL
//
#define 	 IOCTL_CSMSDIGI_LOCK_SCRATCH_REGS 0x902

// Release a lock on the Scratch Regs
//	Input buffer:  NULL
//	Output buffer: NULL
//
#define 	 IOCTL_CSMSDIGI_UNLOCK_SCRATCH_REGS 0x903

// Get Scratch Regs 
//	Input buffer:  NULL
//	Output buffer: CsmsDigiRegs*
//
#define 	 IOCTL_CSMSDIGI_GET_SCRATCH_REGS 0x904
