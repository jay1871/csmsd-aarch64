/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once
#include <deque>
#include <thread>

#include "3230Fdd.h"
#include "RWLock.h"
#include "Units.h"
#include "SharedMemory.h"
#include "CsmsDigi.h"
#include "PLddr3.h"
#include "RWLock.h"
#include "SBuffer.h"

inline ECsmsDigiInterruptSource operator~ (ECsmsDigiInterruptSource a) { return (ECsmsDigiInterruptSource)~(int)a; }
inline ECsmsDigiInterruptSource operator| (ECsmsDigiInterruptSource a, ECsmsDigiInterruptSource b)
	{ return ECsmsDigiInterruptSource((int)a | (int)b); }
inline ECsmsDigiInterruptSource& operator|= (ECsmsDigiInterruptSource& a, ECsmsDigiInterruptSource b)
	{ return (ECsmsDigiInterruptSource&)((int&)a |= (int)b); }
inline ECsmsDigiInterruptSource operator& (ECsmsDigiInterruptSource a, ECsmsDigiInterruptSource b)
	{ return ECsmsDigiInterruptSource((int)a & (int)b); }
inline ECsmsDigiInterruptSource& operator&= (ECsmsDigiInterruptSource& a, ECsmsDigiInterruptSource b)
	{ return (ECsmsDigiInterruptSource&)((int&)a &= (int)b); }

class C3230
{
	// Friends
	friend class CHwControl;		// Note: Only needs access to protected/public members
	friend class CProcessing;
	friend class CEquipControl;
	friend class CVCPCtrl;
	friend class CSlaveControl;
	friend class CAgc;

public:
	// Constants
	static const int ADC_FULL_SCALE_COUNT = 32768;
	static constexpr float ADC_FULL_SCALE_DBM = 9.0f;
	static constexpr float ADC_FULL_SCALE_WATTS = pow(10.0f, (ADC_FULL_SCALE_DBM - 30.0f) / 10.0f);
	static const unsigned int SAMPLES_TIMEOUT = 2000; // ms
	static const unsigned long ADC_CLOCK_RATE = 368640000; // Hz
	static constexpr const int DDC_EXTRA_SAMPLES_REQUIRED = 44;

	// Types
	enum class EDemodMode { OFF, FM, AM, CW, USB, LSB, IQ, NUM_MODES };
	enum class EDfMode { NODF, DFMASTER, DFSLAVE };

	typedef void (*NewSamplesCallback)(const std::vector<unsigned long>& samples, unsigned long channel, bool EOS, void* param);

	struct SDecimation
	{
		static const unsigned long MAX_ZIF_CIC_DECIMATION = 32;

		SDecimation(void) : zifCICDecimation(0), zifFIRDecimation(0), upResample(0), downResample(0), ddcCICDecimation(0), ddcFIRDecimation(0) {}
		SDecimation(unsigned long zc, unsigned long zf, unsigned long up, unsigned long dn, unsigned long dc, unsigned long df) :
			zifCICDecimation(zc), zifFIRDecimation(zf), upResample(up), downResample(dn), ddcCICDecimation(dc), ddcFIRDecimation(df) {}
		bool operator==(const SDecimation& b) const
		{
			return (zifCICDecimation == b.zifCICDecimation && zifFIRDecimation == b.zifFIRDecimation && upResample == b.upResample &&
				downResample == b.downResample && ddcCICDecimation == b.ddcCICDecimation && ddcFIRDecimation == b.ddcFIRDecimation);
		}
		bool operator!=(const SDecimation& b) const
		{
			return !(*this == b);
		}
		void GetUpDown(unsigned long& up, unsigned long& down) const
		{
			down = zifCICDecimation * zifFIRDecimation * downResample * ddcCICDecimation * ddcFIRDecimation;
			up = upResample;
			return;
		}
		void GetUpDownFront(unsigned long& up, unsigned long& down) const
		{
			down = zifCICDecimation * zifFIRDecimation * downResample;
			up = upResample;
			return;
		}
		double GetTotal(void) const
		{
			unsigned long up;
			unsigned long down;
			GetUpDown(up, down);
			return (up > 0 ? static_cast<double>(down) / up : 0);
		}
		double GetTotalFront(void) const
		{
			unsigned long up;
			unsigned long down;
			GetUpDownFront(up, down);
			return (up > 0 ? static_cast<double>(down) / up : 0);
		}

		unsigned long zifCICDecimation;
		unsigned long zifFIRDecimation;
		unsigned long upResample;
		unsigned long downResample;
		unsigned long ddcCICDecimation;
		unsigned long ddcFIRDecimation;
	};

	struct SConfigData
	{
		SConfigData(void) : isLoaded(false), sampleRate(0), numAudioChannels(0) {}

		bool isLoaded;
		unsigned long sampleRate;
		unsigned long numAudioChannels;
	};

	struct SDateStamp
	{
		SDateStamp(unsigned long dt) :
			year(1000 * ((dt >> 28) & 0x0f) + 100 * ((dt >> 24) & 0x0f) + 10 * ((dt >> 20) & 0x0f) + ((dt >> 16) & 0x0f)),
			month(10 * ((dt >> 12) & 0x0f) + ((dt >> 8) & 0x0f)),
			date(10 * ((dt >> 4) & 0x0f) + (dt & 0x0f)) {}
		unsigned long year;
		unsigned long month;
		unsigned long date;
	};

	struct SRevision
	{
		SRevision(unsigned long rev) : majorVersion((rev >> 24) & 0xff), revision((rev >> 16) & 0xff), subrevision(rev & 0xffff) {}
		unsigned char majorVersion;
		unsigned char revision;
		unsigned short subrevision;
	};
	struct SDeviceId
	{
		SDeviceId(unsigned long id) : revision((id >> 28) & 0x0f), family((id >> 21) & 0x7f), subFamily((id >> 17) & 0x0f),
			device((id >> 12) & 0x1f), manufacturerId((id >> 1) & 0x07ff) {}
		unsigned char revision;
		unsigned char family;
		unsigned char subFamily;
		unsigned char device;
		unsigned short manufacturerId;
	};

	struct SVersionInfo
	{
		enum class EFwType : unsigned char { UNKNOWN, GOLDEN, MICRO_SMS };
		SVersionInfo(void) : driverRevision(0), driverDatestamp(0),
			boardRevision(0), fwDatestamp(0), deviceId(0), fwRevision(0), type(EFwType::UNKNOWN) {}
		SRevision driverRevision;
		SDateStamp driverDatestamp;
		unsigned short boardRevision;
		SDateStamp fwDatestamp;
		SDeviceId deviceId;
		SRevision fwRevision;
		EFwType type;
		unsigned char totalDDCs;
		unsigned char audioDDCs;
	};

	struct SAudioParams
	{
		SAudioParams(void) : bfo(0), bw(0), decimation(1,1,1,1,240,4), invertSpectrumData(false),
			mode(C3230::EDemodMode::OFF), procFreq(0) {}

		Units::Frequency bfo;
		Units::Frequency bw;
		SDecimation decimation;
		bool invertSpectrumData;
		C3230::EDemodMode mode;
		Units::Frequency procFreq;
	};

	// Functions
	static unsigned long AdjustAudioDdcDecimation(const Units::Frequency& bw, const SDecimation& decimation);
	bool AdjustInversion(_In_ bool direct, _In_ bool inverted);
	static bool BlinkLedStatic(unsigned char seconds);
	static float CalcNoiseDbm(unsigned int adcCount, const unsigned int (&counts)[4], const unsigned short (&thresh)[4]);
	static float CalcToneDbm(unsigned int adcCount, const unsigned int (&counts)[4], const unsigned short (&thresh)[4]);
	bool CheckTimestamp(bool logErrors, bool startup, unsigned long (&values)[5], bool (&errors)[5]);
	float GetAdcFullScaleWatts(void) const { return ADC_FULL_SCALE_WATTS; }
	unsigned long GetAntennaControl(void) const;
	void GetAntennaStatus(bool& mastDown, bool& whipFault) const;
	unsigned long GetAntennaStatus(unsigned char& antennaType) const;
	double GetAudioOutputRate(unsigned char ch) const { return (ch < m_audioMem.size() ? m_audioMem[ch].outputRate : 0); }
	std::vector<unsigned long> GetClockFrequencies(void) const;
	unsigned long GetCurrentAntenna(void) const;
	bool GetCurrentTime(Units::Timestamp& timestamp) const;
	void GetDeviceDna(unsigned long(&dna)[2]) const { dna[0] = m_deviceDna[0]; dna[1] = m_deviceDna[1]; }
	static bool GetDeviceDnaStatic(unsigned long(&dna)[2]);
	float GetGainRatio(bool direct) const;
	unsigned short GetGlobalStatus(void) const;
	static float GetEstimatedLatency(Units::Frequency sampleRate, const SDecimation& decimations);
	std::vector<unsigned char> GetMemoryChecks(void) const;
	std::vector<float> GetTemperatures(void) const;
	std::vector<std::array<float, 2> > GetTempLimits(void) const;
	unsigned long GetTimestampStatus(void) const;
	const SVersionInfo& GetVersionInfo(void) { return m_versionInfo; }
	std::vector<std::array<float, 2> > GetVoltageLimits(void) const;
	std::vector<float> GetVoltages(void) const;
	bool HasTimestampCapability(void) const { return m_hasTimestamp && m_setSeconds; }
	bool IsMaster(void) const { return m_master; }
	static bool MapRegisters(int driverDesc, volatile unsigned long*& regsmap);
	void MeasureNoiseDbm(bool direct, float& power);
	void MeasureToneDbm(bool direct, float& power);
	unsigned long Peek(C3230Fdd::EReg reg) const { return PeekUser(reg); }
	void Poke(C3230Fdd::EReg reg, unsigned long data, unsigned long mask = 0xfffffffful) { if (m_master) PokeUser(reg, data, mask); }
	void RegisterNewSamplesCallback(NewSamplesCallback callback, void* param, unsigned char ch);
	void ResetFrequency(void);
	void SetAntennaControl(unsigned long controlWord);
	bool SetAudioParams(unsigned char audioChannel, const SAudioParams& decimation, bool enable, unsigned char gainShift, unsigned long streamid);
	bool SetDDCNCO(unsigned char ddcChannel, Units::Frequency freq);
	bool SetDecimation(const SDecimation& params, unsigned char gainShift = 0);
	void SetLed(bool on);
	void SetFaultStatusLed(bool on);
	void ToggleFaultStatusLed(void);
	static bool SetLedStatic(bool on);
	void SetThresholdCounters(const unsigned short (&thresh)[4]);
	void ShowDigitizerSettings(void);
	void ToggleLed(void);
	void Tune(Units::Frequency freq, bool invertSpectrumData, bool direct, bool shift);
	void UnregisterNewSamplesCallback(unsigned char ch);
	void SetCSMSRegister(unsigned long regnum, unsigned long data, unsigned long mask = 0);
	unsigned long GetCSMSRegister(unsigned long regnum) const;
	void SetADCChannel(bool direct);
	// Data
	static SConfigData m_configData;
	std::atomic_bool m_setSeconds;

protected:
	// Types

	// Functions
	C3230(bool dfSlave, int allowShutdown);
	virtual ~C3230(void);
	bool CheckMemory(void);
	bool CheckTimestamp(void);
	void CollectCounts(bool direct, unsigned int& adcCount, unsigned int (&counts)[4], unsigned short (&thresh)[4]);
	void CollectSamples(unsigned long sampleCount, unsigned long count, unsigned long adcCount, unsigned long delay,
		bool withIq, bool withPsd, bool withFft, bool doSamples, double collectionTimeSec, unsigned long subBlockCount,
		Units::Timestamp startTime, EDfMode dfMode, const std::vector<unsigned long>& pattern);
	bool FifoHasOverrun(void);
	int GetAdcFullScaleCounts(void) const { return ADC_FULL_SCALE_COUNT; }
	unsigned long GetLatencyCounter(void) const;
	double GetDecimation(void) const;
	void GetSampleCount(unsigned int& count) const;
	_Success_(return) bool GetThresholdCounters(unsigned int(&counts)[4], unsigned short(&thresh)[4]);
	bool GetTimeStamp(Units::Timestamp& timestamp) const;
	bool Is3230Ready(void) const { return (m_driverDesc != -1); }
	bool Reset3230(bool dfSlave);
	void ResetDrift(void);
	void ResetFifos(void);
	void ResetThresholdCounters(void);
	void ResetTimestamp(void);
	bool SetSeconds(Units::Timestamp timestamp);
	void SetDbg(bool dbg) { m_dbg = dbg; }

private:
	// Constants
	static constexpr const char* SHMEM_NAME = "Global.TCI.ShMem3230";
	static constexpr const double AUDIO_INTERRUPT_RATE = 0.020;	// seconds
	static constexpr const size_t NUM_INTERRUPT_THREADS = 3;	// data, audio, realtime iq
	static constexpr const unsigned long THREAD_TIMEOUT = 1000;	// 1000 msec

	static constexpr const char* csmsDigiDevice = "/dev/CsmsDigi";

	// Types
	class CRegLock
	{
	public:
		CRegLock(int driverDesc);
		~CRegLock(void);

	private:
		int m_driverDesc;
	};

	struct SDDCParams
	{
		unsigned long rate;
		Units::Frequency freq;
		bool invertSpectrumData;
		bool enable;
	};

	struct SDemodParams
	{
		SDemodParams(void) : bfo(0), bw(0), decimation(0,0,0,0,0,0), mode(C3230::EDemodMode::OFF), enable(false) {}

		Units::Frequency bfo;
		Units::Frequency bw;
		SDecimation decimation;
		C3230::EDemodMode mode;
		bool enable;
	};

	struct SAdmParams
	{
		SAdmParams(void) : outputRate(0), mode(C3230::EDemodMode::OFF) {}

		double outputRate;
		C3230::EDemodMode mode;
		unsigned long streamid;
	};

	template <typename T> struct SAtomic
	{
		SAtomic(void) { data = 0L;}
		SAtomic(const SAtomic<T>& other) { data = T(other.data); }
		std::atomic<T> data;
	};

	struct SAudioMemory
	{
		CPLddr3 ddr3;				// virtual address
		CPLddr3 ddr3End;			// virtual address
		SAtomic<unsigned short> expectedSequence;
		off_t physicalAddress;						// byte address
		double outputRate;							// Hz
		size_t size;								// bytes
		SAtomic<bool> m_EOS;
	};

	// Functions
	static unsigned long CalcFftDelay(unsigned long count, bool withPsd, bool withFft);
	unsigned long CalcFreqWord(Units::Frequency freq, Units::Frequency sampleRate) const;
	void GetThresholds(unsigned short(&thresh)[4]) const;
	static void GetWaitMask(unsigned long long v, ECsmsDigiInterruptSource& s, unsigned long& count)
		{ s = ECsmsDigiInterruptSource(v & 0xffffffff); count = (v >> 32) & 0xffffffff; return; }
	static float InvErf(float x);
	unsigned long PeekUser(C3230Fdd::EReg reg) const;
	unsigned long PeekDMA(C3230Fdd::EDMAReg reg) const;
	virtual void PingWatchdog(void) {} // Override if watchdog is needed
	void PokeDMA(C3230Fdd::EDMAReg reg, unsigned long data, unsigned long mask = 0xfffffffful);
	void PokeUser(C3230Fdd::EReg reg, unsigned long data, unsigned long mask = 0xfffffffful);
	bool StartSampling(unsigned long sampleCount, unsigned long count, unsigned long adcCount, unsigned long delay, unsigned long fftDelay,
		bool withIq, bool withPsd, bool withFft, unsigned long subBlockCount, Units::Timestamp startTime, EDfMode dfMode, const std::vector<unsigned long>& pattern);
	bool SetADM(unsigned char audioChannel, double outputRate, C3230::EDemodMode mode, unsigned long streamid);
	bool SetDDC(unsigned char ddcChannel, unsigned long rate, Units::Frequency freq, bool invertSpectrumData, bool enable, unsigned char gainShift);
	bool SetDemod(unsigned char audioChannel, const SAudioParams& params, bool enable);
	bool SetTimeStamp(const Units::Timestamp& timestamp);
	static unsigned long long SetWaitMask(ECsmsDigiInterruptSource s, unsigned long count) { unsigned long long v = count; return (v << 32) + s; }
	void StartThreads(void);
	void StopThreads(void);
	void Thread(ECsmsDigiInterruptSource sourceMask, unsigned long idx);
	bool WaitForSequencer(bool withIq, bool withPsd, bool withFft, bool doSamples, double collectionTimeSec, Units::Timestamp startTime);

	// Data
	int m_allowShutdown;
	std::vector<SAdmParams> m_adm;
	std::vector<SAudioMemory> m_audioMem;
	NewSamplesCallback m_callbacks[NUM_INTERRUPT_THREADS];
	void* m_callbackParams[NUM_INTERRUPT_THREADS];
	mutable std::mutex m_callbackCritSect[NUM_INTERRUPT_THREADS];
	std::vector<SDDCParams> m_ddc;
	SDecimation m_decimation;
	std::vector<SDemodParams> m_demod;
	unsigned long m_deviceDna[2];
	bool m_direct;
	volatile unsigned long* m_DMAregs;
	size_t m_DMAregSize;
	int m_driverDesc;
	Units::Frequency m_freq;
	std::atomic_bool m_hasTimestamp;
	bool m_invertSpectrumData;
	bool m_master;
	int m_memDesc;
	std::vector<unsigned char> m_memoryCheck;
	volatile unsigned long* m_regs;
	size_t m_regSize;
	Units::Frequency m_sampleRate;
	CSharedMemory m_sharedMemory;
	bool m_shift;
	CSBufferPLddr3 m_sbuffer;
	CSBufferPSdma m_sdma;
	bool m_shutdown;
	mutable std::mutex m_shutdownMutex;
	std::thread m_thread[NUM_INTERRUPT_THREADS];
	SVersionInfo m_versionInfo;
	std::atomic_ullong m_waitMaskAndCount[NUM_INTERRUPT_THREADS];
	bool m_waitDone;
	std::condition_variable m_waitDoneCond;
	mutable std::mutex m_waitDoneMutex;
	mutable std::mutex m_wdMutex;
	bool m_dbg;
};

