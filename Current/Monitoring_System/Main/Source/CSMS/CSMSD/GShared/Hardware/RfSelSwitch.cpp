/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved			  *
**************************************************************************/

#include "StdAfx.h"

#include "RfSelSwitch.h"
#include "Utility.h"

//Static definitions.
const std::string CRfSelSwitch::C_CONNECTED_STR("Connected");
const std::string CRfSelSwitch::C_NOT_CONNECTED_STR("Not Connected");
const std::string CRfSelSwitch::C_READ_ERR_STR("Read Error");
const std::string CRfSelSwitch::C_SET_ERR_STR("Set Error");
const std::string CRfSelSwitch::C_UNKNOWN_STR("Status Unknown");
//Switch input to antenna for TCI5143_MULTIPLE_ANT system.
const CRfSelSwitch::SSwitchAntInput CRfSelSwitch::TCI5143_ANT_MAP[] =
{
    {EMiniCircuitInput::C_INPUT1, SSmsMsg::ANT1_LPA},
    {EMiniCircuitInput::C_INPUT2, SSmsMsg::ANT1_647V_MON},
    {EMiniCircuitInput::C_INPUT3, SSmsMsg::ANT1_647H_MON},
    {EMiniCircuitInput::C_INPUT4, SSmsMsg::INVALID_ANT},
};

//////////////////////////////////////////////////////////////////////
///<summary>
/// Default ctor.
///</summary>
///
CRfSelSwitch::CRfSelSwitch(void) :
	m_ant(SSmsMsg::INVALID_ANT),
	m_currAntPort(EMiniCircuitInput::C_INVALID_INPUT),
	m_numAntPort(0),
	m_pSwitch2Ant(nullptr),
	m_switchState(ESwitchState::C_NOT_CONNECTED),
	m_switchType(EANTSetup::C_NO_SWITCH)
{}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Default ctor.
///</summary>
///
CRfSelSwitch::~CRfSelSwitch(void)
{
	if(m_switchState == ESwitchState::C_CONNECTED)
	{
#ifdef CSMS_2016
		Utility::RFSwitchDisconnect();
#endif
		m_switchState = ESwitchState::C_NOT_CONNECTED;
	}
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Return the antenna value that is connected to the switch input port.
///</summary>
///
/// <param name="inPortNum">
///@param[in] Has the RF switch input port number.
/// </param>
///
/// <returns>
///@return: Antenna that is connected to the input port.
///	    INVALID_ANT for unknown inPortNum.
/// </returns>
///
SEquipCtrlMsg::EAnt CRfSelSwitch::GetConnectedAnt(EMiniCircuitInput inPortNum) const
{
	auto ant = SSmsMsg::INVALID_ANT;
	auto pSwitchMap = m_pSwitch2Ant;

	for(size_t i =0; i < m_numAntPort; ++i, ++pSwitchMap)
	{
		  if(pSwitchMap->switchIn == inPortNum)
		  {
			    ant = pSwitchMap->ant;
			    break;	//Exit loop.
		  }
	}

	return(ant);
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Get the switch port number that has the specified antenna.
///</summary>
///
/// <param name="ant">
///@param[in] antenna value to check.
/// </param>
///
/// <param name="portNum">
///@param[out] Successful return this has the switch input port number that has
///		specified antenna connected to it.
/// </param>
///
/// <returns>
///@return: true: Switch port/input found.
///	    false: None of the switch port/input connected to the specified antenna.
/// </returns>
///
bool CRfSelSwitch::GetSwitchPort(SEquipCtrlMsg::EAnt ant, unsigned char & portNum) const
{
	auto portFound = false;
	auto pSwitchMap = m_pSwitch2Ant;

	for(size_t i =0; i < m_numAntPort; ++i, ++pSwitchMap)
	{
		if(pSwitchMap->ant == ant)
		{
			portNum = static_cast<unsigned char>(pSwitchMap->switchIn);
			portFound = true;
			break;	//Exit loop.
		}
	}

	return(portFound);
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Initialize connection to RF switch and reads its input setting.
///</summary>
///
/// <param name="sysType">
///@param[in] Has the System type to allow different mapping of antenna
/// RF switch input.
/// Currently, only 1 mapping, C_TCI5143 is allowed.
/// </param>
///
/// <returns>
///@return: true: Init is successful & switch setting read.
///	    false: Init error or unable to read switch setting.
/// </returns>
///
bool CRfSelSwitch::Init(EANTSetup sysType)
{
	bool initOk = false;
	m_switchType = sysType;

	switch(sysType)
	{
		case  EANTSetup::C_NO_SWITCH:
		{
			m_switchState = ESwitchState::C_NOT_CONNECTED;
			m_numAntPort = 0;
			m_pSwitch2Ant = nullptr;
			initOk = true;
			break;
		}
		case  EANTSetup::C_TCI5143:
		{
			m_pSwitch2Ant = TCI5143_ANT_MAP;
			m_numAntPort = sizeof(TCI5143_ANT_MAP)/sizeof(TCI5143_ANT_MAP[0]);
#ifdef CSMS_2016
			if (Utility::RFSwitchInit())
			{
				  unsigned char activeInput;

				  if (Utility::GetRFSwitchPort(&activeInput))
				  {
					    //Must return valid input id.
					    ASSERT(activeInput >= C_INPUT1 && activeInput <= C_INPUT4);
						initOk = true;

					    if(GetConnectedAnt(static_cast<EMiniCircuitInput>(activeInput))
					    		== SSmsMsg::INVALID_ANT)
					    {
						    //Set the switch to connect to default antenna.
						    if(SetAnt(C_5143_DEFAULT_ANT, true))
						    {
						    	SEquipCtrlMsg::EAnt ant;
							    if(!ReadSwitchAntVal(ant))
							    {
								    initOk = false;
							    }
						    }
						    else
						    {
							    initOk = false;
						    }
					    }
					    else
					    {
						    m_ant = GetConnectedAnt(static_cast<EMiniCircuitInput>(activeInput));
						    m_currAntPort = static_cast<EMiniCircuitInput>(activeInput);
						    m_switchState = ESwitchState::C_CONNECTED;
					    }
				  }
				  else
				  {
					    m_switchState = ESwitchState::C_READ_ERR;
					    TRACE("CRfSelSwitch::Init: Utility::GetRFSwitchPort returns error\n");
				  }
			}
#else
			//m_switchState = ESwitchState::C_NOT_CONNECTED;
			m_switchState = ESwitchState::C_CONNECTED;
			m_ant = SSmsMsg::ANT1_LPA;
			TRACE("CRfSelSwitch::Init: called with EANTSetup::C_TCI5143, TCI5143_ANT_MAP used and LPA is set\n");
			initOk = true;
#endif
			break;
		}
	}

	return(initOk);
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Verify whether the RF switch is set (connected) to the specified antenna.
///</summary>
///
/// <param name="antVal">
///@param[in] Has the antenna value to check.
/// </param>
///
/// <param name="failReason">
///@param[out] If the return value is false, this has the reason for the failure.
/// </param>
///
/// <returns>
///@return: true: RF switch is set (connected) to the specified antenna.
///	    false: RF switch is not switched to the specified antenna.
/// </returns>
///
bool CRfSelSwitch::IsAntConnected(SEquipCtrlMsg::EAnt antVal, ESwitchState & failReason)  const
{
	auto connected = false;
	failReason = m_switchState;

	if(m_switchState == ESwitchState::C_CONNECTED)
	{
		if(antVal == m_ant)
		{
			connected = true;
		}
		else
		{
			failReason = ESwitchState::C_ANT_VAL_NOT_MATCHED;
		}
	}
	else
	{
		TRACE("CRfSelSwitch::IsAntConnected: RF switch not connected to any input, m_switchState = %u\n",
				unsigned(m_switchState));
	}

	return(connected);
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Read the antenna switch setting and the antenna that is connect to it.
///</summary>
///
/// <param name="ant">
///@param[out] Has the antenna currently connected to the active switch port.
/// </param>
///
/// <returns>
///@return: true: ant has valid value.
///	    false: Error in reading active switch port.
/// </returns>
///
bool CRfSelSwitch::ReadSwitchAntVal(SEquipCtrlMsg::EAnt & ant)
{
	auto antValid = false;
	ant = m_ant;

	if(m_switchState == ESwitchState::C_CONNECTED)
	{
		antValid = true;
	}
	else if(m_switchState == ESwitchState::C_SET_NOT_VERIFIED)
	{
		unsigned char  activeInput;

#ifdef CSMS_2016
		if (Utility::GetRFSwitchPort(&activeInput))
	    	{
	    		if(m_currAntPort != static_cast<EMiniCircuitInput>(activeInput))
	    		{
	    			m_currAntPort = static_cast<EMiniCircuitInput>(activeInput);
	    			ant = m_ant = GetConnectedAnt(m_currAntPort);
	    		}
	    		antValid = true;
	    		m_switchState = ESwitchState::C_CONNECTED;
	    	}
		else
		{
			m_switchState = ESwitchState::C_READ_ERR;
			TRACE("CRfSelSwitch::ReadSwitchAntVal: Utility::GetRFSwitchPort returns error\n");
		}
#else
    		antValid = true;
    		m_switchState = ESwitchState::C_CONNECTED;
#endif
	}

	return(antValid);
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Set the RF switch to connect to the specified antenna.
///</summary>
///
/// <param name="ant">
///@param[in] Has the antenna that the switch should be connected to.
/// </param>
///
/// <param name="dontInit">
///@param[in] true: Don't call Init as this method is called by it.
///			  false: If switch is not connected, call Init.
/// </param>
///
/// <returns>
///@return: true: RF switch is set to switch the specified antenna.
///	    false: Unable to switch the specified antenna.
/// </returns>
///
bool CRfSelSwitch::SetAnt(SEquipCtrlMsg::EAnt ant, bool dontInit)
{
	auto setOk = false;

	if(m_switchType != EANTSetup::C_NO_SWITCH)
	{
		setOk = true;

		if(!dontInit && (m_switchState == ESwitchState::C_NOT_CONNECTED))
		{
			setOk = Init(m_switchType);
		}

		if(setOk)
		{
			if(Need2VerifySetAnt())
			{
				SEquipCtrlMsg::EAnt ant;
				if(!ReadSwitchAntVal(ant))
				{
					setOk = false;
					TRACE("Unable to read RF Switch value %u\n", ant);
					ASSERT(false);
				}
			}

			if(setOk)
			{
				if(ant != m_ant)
				{
					unsigned char portNum;

					if(GetSwitchPort(ant, portNum))
					{
			#ifdef CSMS_2016
						if (Utility::SetRFSwitchPort(&portNum))
						{
							m_switchState = ESwitchState::C_SET_NOT_VERIFIED;
							m_ant = ant;
							m_currAntPort = static_cast<EMiniCircuitInput>(portNum);
						}
						else
						{
							setOk = false;
							m_switchState = ESwitchState::C_SET_ERR;
							TRACE("CRfSelSwitch::SetAnt: Utility::SetRFSwitchPort returns error\n");
						}
			#else
						m_switchState = ESwitchState::C_SET_NOT_VERIFIED;
						m_ant = ant;
						TRACE("CRfSelSwitch::SetAnt: ant %u is saved and state at C_SET_NOT_VERIFIED\n", ant);
			#endif
					}
					else
					{
						//Specified antenna is not connected to any of the RF
						// switch input port, return false.
						setOk = false;
						TRACE("CRfSelSwitch::SetAnt: ant %u is not connected to RF switch\n", ant);
					}
				}
			}
		}
	}

	return(setOk);
}
