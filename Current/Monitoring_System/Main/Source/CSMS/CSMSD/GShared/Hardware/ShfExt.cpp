/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013-2014 TCI International, Inc. All rights reserved		  *
**************************************************************************/

#include "StdAfx.h"

#include "ShfExt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
//
// Static data
//
#ifdef _MSC_VER
CString CShfExt::s_emptyStr;		//Empty string definition.
#else
std::string CShfExt::s_emptyStr;	//Empty string definition.
#endif
CEShfExt  CShfExt::s_shfExt = CEShfExt::NO_SHF_EXT;
static size_t tunedCalledCnt = 0;
static size_t tunedWaitCnt = 0;

//@note: In multi-threading, the access and modification of s_pN9010AAnalyzer
//       should be protected by CriticalSection/lock.
//		 For efficiency, the CriticalSection is not implemented because
//		 the analyzer pointer is only set at ctor or during program startup
//		 and never change after creation.
std::unique_ptr<CN9010A544> CShfExt::s_pN9010AAnalyzer;

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CShfExt::CShfExt(void)
:
	m_IFOutputEnabled(false),
	m_lastTuneFreq(0)
{
	UpdateAnalyzer(CSingleton<CConfig>()->GetShfExtSetting());
	return;
}

//////////////////////////////////////////////////////////////////////
/// <summary>
/// This static method is called when there is changed of Analyzer type.
/// </summary>
///
/// <param name="shfExt">
/// @param[in] Has the new Signal Analyzer type.
/// </param>
///
/// <remarks>
/// @note: 1. It is assumed that this method is either called at ctor or
///			  at the program initialization, therefore mutex/lock is not used
///			  to protect the change to s_shfExt & s_pN9010AAnalyzer.
/// </remarks>
inline void CShfExt::ChangeAnalyzer(_In_ CConfig::EShfExt shfExt)
{
	switch(shfExt)
	{
	case CEShfExt::NO_SHF_EXT:
		s_pN9010AAnalyzer.reset();
		break;

	case CEShfExt::N9010A_544_B25:
		s_pN9010AAnalyzer = std::unique_ptr<CN9010A544>(
				new CN9010A544(CEShfExt::N9010A_544_B25));
		break;

	case CEShfExt::N9010A_544_B40:
		s_pN9010AAnalyzer = std::unique_ptr<CN9010A544>(
				new CN9010A544(CEShfExt::N9010A_544_B40));
		break;

	case CEShfExt::N9010A_526_B40:
		s_pN9010AAnalyzer = std::unique_ptr<CN9010A544>(
			new CN9010A544(CEShfExt::N9010A_526_B40));
		break;
	
	case CEShfExt::N9010A_532_B40:
		s_pN9010AAnalyzer = std::unique_ptr<CN9010A544>(
			new CN9010A544(CEShfExt::N9010A_532_B40));
		break;

	default:
		ASSERT(FALSE);
		shfExt = CEShfExt::NO_SHF_EXT;
		s_pN9010AAnalyzer.reset();
		break;
	}

	s_shfExt = shfExt;
	return;
}

//////////////////////////////////////////////////////////////////////
//
// Turns IF output off
//
bool CShfExt::DisableIfOutput(void)
{
	if(!m_IFOutputEnabled)
	{
		// already off
		return true;
	}

	bool status = false;

	if( s_shfExt != CEShfExt::NO_SHF_EXT )
	{
		// couldn't disable IF output (:OUTP:AUX OFF didn't work)
		// don't want to set 60 dB attenuation (:SENS:POW:RF:ATT 60) -- uses mechanical relays!
		// so this function tunes to lowest frequency above DC
		unsigned long long offFreq = std::max(s_pN9010AAnalyzer->GetMinFreq(),
					s_pN9010AAnalyzer->GetIfBw()/ 2 + s_pN9010AAnalyzer->GetIfBw()/ 10);
		status = s_pN9010AAnalyzer->Tune(offFreq);

		if(status)
		{
			m_IFOutputEnabled = false;
		}

	}
	else
	{
		ASSERT(FALSE);
	}

	return status;
}


//////////////////////////////////////////////////////////////////////
//
// Turns IF output on
//
bool CShfExt::EnableIfOutput(void)
{
	if(m_IFOutputEnabled)
	{
		// already on
		return true;
	}

	bool status = false;

	if( s_shfExt != CEShfExt::NO_SHF_EXT )
	{
		// couldn't disable IF output (:OUTP:AUX OFF didn't work)
		// don't want to set 0 dB attenuation (:SENS:POW:RF:ATT 0) -- uses mechanical relays!
		// so this function tunes to last commanded frequency
		status = s_pN9010AAnalyzer->Tune(m_lastTuneFreq);

		if(status)
		{
			m_IFOutputEnabled = true;
		}

	}
	else
	{
		ASSERT(FALSE);
	}

	return status;
}


//////////////////////////////////////////////////////////////////////
//
// Returns the IF bandwidth
//
Units::Frequency CShfExt::GetIfBandwidth(void)
{
	Units::Frequency frequency(0);

	if( s_shfExt != CEShfExt::NO_SHF_EXT )
	{
		frequency = Units::Frequency(s_pN9010AAnalyzer->GetIfBw());
	}
	else
	{
		ASSERT(FALSE);
	}

	return frequency;
}


//////////////////////////////////////////////////////////////////////
//
// Returns the IF center frequency
//
Units::Frequency CShfExt::GetIfFrequency(void)
{
	Units::Frequency frequency(0);

	if( s_shfExt != CEShfExt::NO_SHF_EXT )
	{
		frequency = Units::Frequency(s_pN9010AAnalyzer->GetIfCenterFreq());
	}
	else
	{
		ASSERT(FALSE);
	}

	return frequency;
}

//////////////////////////////////////////////////////////////////////
//
// Returns the RF frequency limits
//   @note: Careful: this is hardware limits, not necessarily processing limits
//
Units::FreqPair CShfExt::GetRfFrequencyLimits(void)
{
	Units::FreqPair freqPair(0, 0);

	if( s_shfExt != CEShfExt::NO_SHF_EXT )
	{
		freqPair = Units::FreqPair(s_pN9010AAnalyzer->GetMinFreq(), s_pN9010AAnalyzer->GetMaxFreq());
	}
	else
	{
		ASSERT(FALSE);
	}

	return freqPair;
}

//////////////////////////////////////////////////////////////////////
/// <summary>
/// This static method is called to obtain maximum RF frequency supported
///  by the SHF Extension.
/// </summary>
/// <returns>
/// Maximum RF frequency supported.
/// </returns>
Units::Frequency CShfExt::GetRfMaxFreq(void)
{
	Units::Frequency frequency(0);

	if( s_shfExt != CEShfExt::NO_SHF_EXT )
	{
		frequency = Units::Frequency(s_pN9010AAnalyzer->GetMaxFreq());
	}
	else
	{
		ASSERT(FALSE);
	}

	return frequency;
}

//////////////////////////////////////////////////////////////////////
//
// Is spectrum inverted
//  note that this is just spectrum analyzer (not whole system while using SHF extension)
//  (2612 & 2614 down-converters are inverted over whole frequency range)
//
bool CShfExt::IsSpectrumInverted(_In_ Units::Frequency freq) const
{

	bool invertedSpectrum = false;

	if( s_shfExt != CEShfExt::NO_SHF_EXT )
	{
		//Call the analyzer class to get its spectrum nature.
		invertedSpectrum = s_pN9010AAnalyzer->IsSpectrumInverted(freq);
	}
	else
	{
		ASSERT(FALSE);
	}

	return invertedSpectrum;
}


//////////////////////////////////////////////////////////////////////
//
// Tune the RF frequency and enable IF output
//
bool CShfExt::Tune(Units::Frequency freq)
{
	bool status = false;

	if( !CConfig::IsInShfExtFreqRange(freq) )
	{
		//Always return true if the frequency specified is not in
		// SHF extension range.
		//@note: It is possible tune is called with SHF Extension IF frequency.
		status = true;
	}
	else if(m_IFOutputEnabled && m_lastTuneFreq == freq)
	{
		// already tuned and enabled
		status = true;
	}
	else if( s_shfExt != CEShfExt::NO_SHF_EXT )
	{
		status = s_pN9010AAnalyzer->Tune(freq);
		if(status)
		{
			m_IFOutputEnabled = true;
			m_lastTuneFreq = freq;
			++tunedCalledCnt;
		}
	}
	else
	{
		ASSERT(FALSE);
	}

	return status;
}

//////////////////////////////////////////////////////////////////////
/// <summary>
/// This static method is called to update Analyzer type.
/// </summary>
/// <param name="shfExt">
/// Has the new Signal Analyzer type.
/// </param>
/// <remarks>
/// @note: 1. It is assumed that this method is either called at ctor or
///			  at the program initialization, therefore mutex/lock is not used
///			  to protect the change to s_shfExt & s_pN9010AAnalyzer.
/// </remarks>
inline void CShfExt::UpdateAnalyzer(_In_ CConfig::EShfExt shfExt)
{
	if( s_shfExt != shfExt )
	{
		ChangeAnalyzer(shfExt);
	}

	return;
}

//////////////////////////////////////////////////////////////////////
//
// Wait for hardware to settle after tune command
//
void CShfExt::Wait(void)
{
	if( s_shfExt != CEShfExt::NO_SHF_EXT &&
	    (tunedCalledCnt > tunedWaitCnt || (tunedCalledCnt == (tunedWaitCnt+1))))
	{
		s_pN9010AAnalyzer->Wait();
		++tunedWaitCnt;
	}

	return;

}


