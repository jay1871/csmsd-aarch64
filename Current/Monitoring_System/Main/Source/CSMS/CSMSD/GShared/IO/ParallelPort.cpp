/*
Module : PARALLEL.CPP
Purpose: Implementation for an MFC class to encapsulate parallel ports
Created: PJN / 28-12-1999
History: PJN / 06-03-2000 Code now throws an exception if no Win32 parallel ports are available
         PJN / 04-04-2000 Code handles the case where it is run on NT / Windows 2000 by throwing
                          an exception                 
         PJN / 19-07-2000 1. Now works on NT / Windows 2000 thanks to DriverLINX Port I/O Driver
                          2. Made all typedefs and enums local to the appropiate class rather
                          than polluting the global namespace
         PJN / 12-04-2003 1. Updated copyright details. 
                          2. Updated documentation to refer to new DriverLinx web site.
                          3. Code now issues compile time message if conio or afxpriv are not in 
                          your PCH.
         PJN / 11-06-2003 1. Reviewed all places where exceptions are throw and the details which
                          are logged in TRACE statements.
         PJN / 09-07-2006 1, Updated copyright details. 
                          2. Renamed AfxThrowParallelException to ThrowParallelException and made it
                          a member of CParallelPort.
                          3. CParallelPort is no longer derived from CObject as it was not really required
                          4. Optimized CParallelPortSettings constructor code
                          5. Optimized CParallelException constructor code
                          6. Removed CParallelException destructor
                          7. Optimized CParallelPort constructor code
                          8. Renamed CParallelException class to CParallelPortException
                          9. Removed CParallelPortFileTransfer destructor as it was not required.
                          10. Code now uses newer C++ style casts instead of C style casts. 
                          11. Updated the documentation to use the same style as the web site
                          12. Updated code to clean compile on VC 2005   
         PJN / 24-12-2007 1. Updated copyright details
                          2. CParallelPortException::GetErrorMessage now uses the FORMAT_MESSAGE_IGNORE_INSERTS flag. 
                          For more information please see Raymond Chen's blog at 
                          http://blogs.msdn.com/oldnewthing/archive/2007/11/28/6564257.aspx. Thanks to Alexey 
                          Kuznetsov for reporting this issue.
                          3. Updated the sample app to clean compile on VC 2005
         PJN / 30-12-2007 1. Updated the code and sample app to clean compile on VC 2008
                          2. CParallelPortException::GetErrorMessage now uses Checked::tcsncpy_s if compiled using VC 2005 or 
                          later.

Copyright (c) 1999 - 2007 by PJ Naughter (Web: www.naughter.com, Email: pjna@naughter.com)

All rights reserved.

Copyright / Usage Details:

You are allowed to include the source code in any product (commercial, shareware, freeware or otherwise) 
when your product is released in binary form. You are allowed to modify the source code in any way you want 
except you cannot modify the copyright details at the top of each module. If you want to distribute source 
code with your application, then you are only allowed to distribute versions released by the author. This is 
to maintain a single distribution point for the source code. 

*/


/////////////////////////////////  Includes  //////////////////////////////////

#include "stdafx.h"

#include <conio.h>
//#include <afxpriv.h>

#include "ParallelPort.h"



///////////////////////////////// Defines / Statics ///////////////////////////

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

int CParallelPort::sm_nRefCount = 0;
BOOL CParallelPort::sm_bRunningOnNT = FALSE;
HINSTANCE CParallelPort::sm_hDLINX = NULL;
CParallelPort::LPDLPORTREADPORTUCHAR CParallelPort::sm_lpfnDlPortReadUchar = NULL;
CParallelPort::LPDLPORTWRITEPORTUCHAR CParallelPort::sm_lpfnDlPortWriteUchar = NULL;
CArray<CParallelPortSettings, CParallelPortSettings&> CParallelPort::sm_Ports;


//////////////////////////////// Implementation ///////////////////////////////

BOOL CParallelPortException::GetErrorMessage(_Out_ LPTSTR pstrError, UINT nMaxError, _In_opt_ PUINT pnHelpContext)
{
  //Validate our parameters
	ASSERT(pstrError != NULL && AfxIsValidString(pstrError, nMaxError));

	if (pnHelpContext != NULL)
		*pnHelpContext = 0;
		
  //What will be the return value from this function (assume the worst)
  BOOL bSuccess = FALSE;

	LPTSTR lpBuffer;
	DWORD dwReturn = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			                           NULL,  m_dwError, MAKELANGID(LANG_NEUTRAL, SUBLANG_SYS_DEFAULT),
			                           reinterpret_cast<LPTSTR>(&lpBuffer), 0, NULL);

	if (dwReturn == FALSE)
		*pstrError = _T('\0');
	else
	{
	  bSuccess = TRUE;
  #if (_MSC_VER >= 1400)
    Checked::tcsncpy_s(pstrError, nMaxError, lpBuffer, _TRUNCATE);
  #else
		lstrcpyn(pstrError, lpBuffer, nMaxError);
  #endif		
		LocalFree(lpBuffer);
	}

	return bSuccess;
}

CString CParallelPortException::GetErrorMessage()
{
  CString rVal;
  LPTSTR pstrError = rVal.GetBuffer(4096);
  GetErrorMessage(pstrError, 4096, NULL);
  rVal.ReleaseBuffer();
  return rVal;
}

CParallelPortException::CParallelPortException(DWORD dwError) : m_dwError(dwError)
{
}

IMPLEMENT_DYNAMIC(CParallelPortException, CException)

#ifdef _DEBUG
void CParallelPortException::Dump(_In_ CDumpContext& dc) const
{
	CObject::Dump(dc);

	dc << "m_dwError = " << m_dwError;
}
#endif


CParallelPortSettings::CParallelPortSettings() : m_nBaseAddress(0),
                                                 m_Type(ParallelTypeUndefined),
                                                 m_ECPMode(ECPModeUndefined)
{
}

CParallelPortSettings::CParallelPortSettings(_In_ const CParallelPortSettings& state)
{
  *this = state;
}

_Ret_ CParallelPortSettings& CParallelPortSettings::operator=(_In_ const CParallelPortSettings& state)
{
  m_nBaseAddress = state.m_nBaseAddress;
  m_Type         = state.m_Type;   
  m_ECPMode      = state.m_ECPMode;

  return *this;
}


CParallelPort::CParallelPort() : m_hPort(INVALID_HANDLE_VALUE),
                                 m_nPortIndex(-1),
                                 m_nBaseAddress(0),
                                 m_dwTimeout(1000)  //Default timeout is 1 second
{

  //Test for presence of parallel ports at the standard addresses of 0x3BC, 0x378 and 0x278
  if (sm_nRefCount == 0)
  {
    ++sm_nRefCount;

    //Initialize the DriverLINX driver if on NT / Windows 2000
    sm_bRunningOnNT = RunningOnNT();
    if (sm_bRunningOnNT)
    {
      if (!InitializeDriverLINX())
      {
        TRACE(_T("CParallelPort::CParallelPort, Running on the NT kernel (i.e. Windows 2000, Windows XP, Windows 2003, Vista or Windows 2008) and the DriverLINX PORTIO driver is not installed\n"));
        ThrowParallelException(ERROR_CALL_NOT_IMPLEMENTED);
      }
    }

    //Open LPT1 - LPT3 as a precaution against other processes trying to write 
    //to the port while we are detecting them
    HANDLE hPort1 = CreateFile(_T("\\\\.\\LPT1"), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
    HANDLE hPort2 = CreateFile(_T("\\\\.\\LPT2"), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
    HANDLE hPort3 = CreateFile(_T("\\\\.\\LPT3"), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

    //Must have at least one port available in Win32 to continue to attempt
    //detection of the ports
    int nWin32Ports = 0;
    if (hPort1 != INVALID_HANDLE_VALUE)
      ++nWin32Ports;
    if (hPort2 != INVALID_HANDLE_VALUE)
      ++nWin32Ports;
    if (hPort3 != INVALID_HANDLE_VALUE)
      ++nWin32Ports;
    if (nWin32Ports == 0)
    {
      TRACE(_T("CParallelPort::CParallelPort, No parallel ports are available to Win32 SDK\n"));
      ThrowParallelException(ERROR_DEV_NOT_EXIST);
    }

    //Try to detect the details of the 3 standard ports
    CParallelPortSettings settings;
    if (GetPort(0x3BC, settings))
      sm_Ports.Add(settings);

    if (sm_Ports.GetSize() < nWin32Ports) 
      if (GetPort(0x378, settings))
        sm_Ports.Add(settings);

    if (sm_Ports.GetSize() < nWin32Ports) 
      if (GetPort(0x278, settings))
        sm_Ports.Add(settings);

    if (sm_Ports.GetSize() == 0)
      TRACE(_T("CParallelPort::CParallelPort, Could not detect any parallel ports on this machine\n"));

    //Don't forget to close the 3 SDK handles we had open
    CloseHandle(hPort3);
    CloseHandle(hPort2);
    CloseHandle(hPort1);
  }
}

CParallelPort::~CParallelPort()
{
  //decrement the reference count and 
  //free the DriverLINX pointers if necessary
  --sm_nRefCount;
  if (sm_nRefCount == 0)
    DeInitializeDriverLINX();

  Close();
}

void CParallelPort::ThrowParallelException(DWORD dwError)
{
	if (dwError == 0)
		dwError = ::GetLastError();

	CParallelPortException* pException = new CParallelPortException(dwError);

	TRACE(_T("Warning: throwing CParallelPortException for error %d\n"), dwError);
	THROW(pException);
}

void CParallelPort::Open(int nPort)
{
  //Call Close just in case we already have the port open
  Close();

  m_nPortIndex = nPort - 1;
  if (m_nPortIndex < sm_Ports.GetSize())
  {
    //Cache the base address of the port for performance reasons
    m_nBaseAddress = sm_Ports.ElementAt(m_nPortIndex).m_nBaseAddress;

    //Call CreateFile to open up the parallel port. This prevents other apps 
    //causing problems when we do the Port IO directly.
    CString sPort;
    sPort.Format(_T("\\\\.\\LPT%d"), nPort);
    m_hPort = CreateFile(sPort, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
    if (m_hPort == INVALID_HANDLE_VALUE)
    {
      DWORD dwLastError = GetLastError();
      TRACE(_T("CParallelPort::Open, Failed to open parallel port, LPT%d, Error:%d\n"), nPort, dwLastError);
      Close();
      ThrowParallelException(dwLastError);
    }
  }
  else
  {
    TRACE(_T("CParallelPort::Open, Could not find the parallel port, LPT%d\n"), nPort);
    Close();
    ThrowParallelException(ERROR_FILE_NOT_FOUND);
  }
}

BOOL CParallelPort::IsOpen() const
{
  return (m_hPort != INVALID_HANDLE_VALUE);
}

void CParallelPort::Close()
{
  if (IsOpen())
  {
    BOOL bSuccess = CloseHandle(m_hPort);
    m_hPort = INVALID_HANDLE_VALUE;
    m_nPortIndex = -1;
    m_nBaseAddress = 0;
    if (!bSuccess)
      TRACE(_T("CParallelPort::Close, Failed to close the parallel port, GetLastError:%d\n"), GetLastError());
  }
}

void CParallelPort::SetECPMode(CParallelPortSettings::ECPPortMode mode)
{
  ASSERT(IsOpen()); //Port must be open 
  CParallelPortSettings& settings = sm_Ports.ElementAt(m_nPortIndex);
  ASSERT(settings.m_Type == CParallelPortSettings::ParallelTypeECP); //Must be an ECP port
  ASSERT(mode != CParallelPortSettings::ECPModeUndefined);

  unsigned short nEcrAddress = static_cast<unsigned short>(m_nBaseAddress + 0x402);

  //Read the ECR & clear bits 5, 6 & 7
  int nEcrData = _inp(nEcrAddress) & 0x1F;
  
  //Write the selected value to bits 5, 6 & 7
  switch (mode)
  {
    case CParallelPortSettings::ECPModeSPP:            
    {
      nEcrData |= (0 << 5); 
      break;
    }
    case CParallelPortSettings::ECPModePS2:            
    {
      nEcrData |= (1 << 5); 
      break;
    }
    case CParallelPortSettings::ECPModeFastCentronics: 
    {
      nEcrData |= (2 << 5); 
      break;
    }
    case CParallelPortSettings::ECPModeECP:            
    {
      nEcrData |= (3 << 5); 
      break;
    }
    case CParallelPortSettings::ECPModeEPP:
    {
      nEcrData |= (4 << 5); 
      break;
    }
    case CParallelPortSettings::ECPModeTest:           
    {
      nEcrData |= (6 << 5); 
      break;
    }
    case CParallelPortSettings::ECPModeConfiguration:  
    {
      nEcrData |= (7 << 5); 
      break;
    }
    default: 
    {
      ASSERT(FALSE);                           
      break;
    }
  }
  _outp(nEcrAddress, nEcrData);

  //Update the value in our cached array
  settings.m_ECPMode = mode;
}

CParallelPortSettings::ECPPortMode CParallelPort::GetECPMode()
{
  ASSERT(IsOpen()); //Port must be open 
  CParallelPortSettings& settings = sm_Ports.ElementAt(m_nPortIndex);
  ASSERT(settings.m_Type == CParallelPortSettings::ParallelTypeECP); //Must be an ECP port

  CParallelPortSettings::ECPPortMode t(ReadECPMode(settings.m_nBaseAddress));
  ASSERT(t == settings.m_ECPMode);

  return settings.m_ECPMode;
}

CParallelPortSettings::ECPPortMode CParallelPort::ReadECPMode(unsigned short nBaseAddress)
{
  CParallelPortSettings::ECPPortMode mode = CParallelPortSettings::ECPModeUndefined;
  int nEcrData = _inp(static_cast<unsigned short>(nBaseAddress + 0x402));
  nEcrData = (nEcrData & 0xE0) >> 5;
  switch (nEcrData)
  { 
    case 0: 
    {
      mode = CParallelPortSettings::ECPModeSPP;            
      break;
    }
    case 1: 
    {
      mode = CParallelPortSettings::ECPModePS2;            
      break;
    }
    case 2: 
    {
      mode = CParallelPortSettings::ECPModeFastCentronics; 
      break;
    }
    case 3: 
    {
      mode = CParallelPortSettings::ECPModeECP;            
      break;
    }
    case 4: 
    {
      mode = CParallelPortSettings::ECPModeEPP;            
      break;
    }
    case 6: 
    {
      mode = CParallelPortSettings::ECPModeTest;           
      break;
    }
    case 7: 
    {
      mode = CParallelPortSettings::ECPModeConfiguration;  
      break;
    }
    default: 
    { 
      break;
    }
  }
  return mode;
}

BOOL CParallelPort::GetPort(unsigned short nBaseAddress, _Out_ CParallelPortSettings& settings)
{
  BOOL bSuccess = FALSE;

  //First try to detect an ECP port
  if (GetECPPort(nBaseAddress))
  {
    settings.m_nBaseAddress = nBaseAddress;
    settings.m_Type = CParallelPortSettings::ParallelTypeECP;
    settings.m_ECPMode = ReadECPMode(nBaseAddress);
    bSuccess = TRUE;
  }
  else
  {
    //If its not an ECP, look for an EPP.
    //If the baseaddress is 3BCh, skip the EPP test.
    //EPPs aren't allowed at 3BCh due to possible conflicts
    //with video memory
    BOOL bFoundEPP = FALSE;

    if (nBaseAddress != 0x3BC)
    {
      bFoundEPP = GetEPPPort(nBaseAddress);
      if (bFoundEPP)
      {
        settings.m_nBaseAddress = nBaseAddress;
        settings.m_Type = CParallelPortSettings::ParallelTypeEPP;
        settings.m_ECPMode = CParallelPortSettings::ECPModeUndefined;
        bSuccess = TRUE;
      }
    }

    if (!bFoundEPP)
    {
      //If its not an EPP, look for an SPP.
      if (GetSPPPort(nBaseAddress))
      {
        //Test for a PS/2 port only if the SPP exists.
        //because if the port doesn't exist, it will 
        //pass the PS/2 test!
        if (GetPS2Port(nBaseAddress))
          settings.m_Type = CParallelPortSettings::ParallelTypePS2;
        else
          settings.m_Type = CParallelPortSettings::ParallelTypeSPP;

        settings.m_nBaseAddress = nBaseAddress;
        settings.m_ECPMode = CParallelPortSettings::ECPModeUndefined;
        bSuccess = TRUE;
      }
    }
  }

  if (!bSuccess)
  {
    settings.m_nBaseAddress = 0;
    settings.m_Type = CParallelPortSettings::ParallelTypeUndefined;
    settings.m_ECPMode = CParallelPortSettings::ECPModeUndefined;
  }  

  return bSuccess;
}

BOOL CParallelPort::GetECPPort(unsigned short nBaseAddress)
{
  //If the ECP is idle and the FIFO empty,
  //in the ECP's Ecp (at base address+402h),
  //bit 1 (Fifo full)=0, and bit 0 (Fifo empty)=1.
  //The first test is to see if these bits differ from the
  //corresponding bits in the control port (at base address+2).
  //If so a further test is to write 34h to the Ecr, 
  //then read it back. Bit 1 is read/write and bit 0 is read-only.
  //If the value read is 35h, the port is an ECP.
  BOOL bSuccess = FALSE;
 
  unsigned short nEcrAddress = static_cast<unsigned short>(nBaseAddress + 0x402);
  int nEcrData = _inp(nEcrAddress);
  
  //Read bits 0 and 1 and control port bit 1
  int nEcrBit0 = nEcrData & 0x1;
  int nEcrBit1 = (nEcrData & 0x2) >> 1;
  int nControlBit1 = (ReadControl(nBaseAddress) & 0x2) >> 1;

  if (nEcrBit0 == 1 && nEcrBit1 == 0)
  {
    //Compare control bit 1 to ECR bit 1
    //Toggle the control bit if necessary
    //to be sure the two registers are different.
    if (nControlBit1 == 0)
    {
      WriteControl(nBaseAddress, 0xF);
      nControlBit1 = (ReadControl(nBaseAddress) & 0x2) >> 1;
    }

    if (nEcrBit1 != nControlBit1)
    {
      int nOriginalEcrData = nEcrData;
      _outp(nEcrAddress, 0x34);
      if (_inp(nEcrAddress) == 0x35)
        bSuccess = TRUE;

      //Restore the ECR to its original value
      _outp(nEcrAddress, nOriginalEcrData);
    }
  }

  return bSuccess;
}

BOOL CParallelPort::GetEPPPort(unsigned short nBaseAddress)
{
  //Write to an EPP register, then read it back.
  //If the read matches the write, it's probably an EPP
  BOOL bSuccess = FALSE;

  //Use nEppAddressPort for testing, SPP's, ECP's and PS/2 ports
  //do not have this register
  unsigned short nEppAddressPort = static_cast<unsigned short>(nBaseAddress + 3);
  _outp(nEppAddressPort, 0x55);

  //Clear the timeout bit after each EPP operation
  int nTimeoutBit = GetEPPTimeoutBit(nBaseAddress);
  int nRead = _inp(nEppAddressPort);
  nTimeoutBit = GetEPPTimeoutBit(nBaseAddress);
  if (nRead == 0x55)
  {
    _outp(nEppAddressPort, 0xAA);
    nTimeoutBit = GetEPPTimeoutBit(nBaseAddress);
    nRead = _inp(nEppAddressPort);
    nTimeoutBit = GetEPPTimeoutBit(nBaseAddress);
    if (nRead == 0xAA)
      bSuccess = TRUE;
  }  

  return bSuccess;
}

int CParallelPort::GetEPPTimeoutBit(unsigned short nBaseAddress)
{
  //Reads and clears the EPP timeout bit (Status port bit 0)
  //Should be done after each EPP operation.
  //The method for clearing the bit varies, so try 3 ways
  //1. Write 1 to status port bit 0
  //2. Write 0 to status port bit 1
  //3. Read the status port again
  int nReturn = (ReadStatus(nBaseAddress) & 0x1);
  unsigned short nStatusPortAddress = static_cast<unsigned short>(nBaseAddress + 1);
  _outp(nStatusPortAddress, 1);
  _outp(nStatusPortAddress, 0);
  nReturn = (ReadStatus(nBaseAddress) & 0x1);
  return nReturn;
}

BOOL CParallelPort::GetPS2Port(unsigned short nBaseAddress)
{
  //First try to tristate (disable) the data outputs by
  //setting bit 5 of the control port. Then write 2 values
  //to the data port and read each one back. If the values
  //match, the data outputs are not disabled and the port
  //is not bidirectional. If the values don't match, the data 
  //outputs are disabled and the port is didirectional
  BOOL bSuccess = FALSE;

  //Set control port bit 5
  WriteControl(nBaseAddress, 0x2F);

  //Write the first byte and read it back
  WriteData(nBaseAddress, 0x55);

  //If it doesn't match, the port is bidirectional
  if (ReadData(nBaseAddress) == 0x55)
  {
    WriteData(nBaseAddress, 0xAA);

    //If it doesn't match, the port is bidirectional
    if (ReadData(nBaseAddress) != 0xAA)
      bSuccess = TRUE;
  }
  else
    bSuccess = TRUE;

  //Reset control port bit 5
  WriteControl(nBaseAddress, 0xF);

  return bSuccess;
}

BOOL CParallelPort::GetSPPPort(unsigned short nBaseAddress)
{
  //Write two bytes and read them back. If the
  //reads matches the writes, the port exists
  BOOL bSuccess = FALSE;
  
  //Be sure that control port bit 5 = 0 (Data outputs enabled)
  WriteControl(nBaseAddress, 0xF);

  //Perform the first write  
  WriteData(nBaseAddress, 0x55);
  if (ReadData(nBaseAddress) == 0x55)
  {
    WriteData(nBaseAddress, 0xAA);
    bSuccess = (ReadData(nBaseAddress) == 0xAA);
  }

  return bSuccess;
}

void CParallelPort::WriteControl(unsigned short nBaseAddress, int nData)
{
  //The control port is at offset nBaseAddress + 2.
  //Bits 0, 1 & 3 need to be inverted  
  _outp(static_cast<unsigned short>(nBaseAddress+2), nData ^ 0xB);
}

int CParallelPort::ReadControl(unsigned short nBaseAddress)
{
  //The control port is at offset nBaseAddress + 2.
  //Bits 0, 1 & 3 need to be inverted  
  return (_inp(static_cast<unsigned short>(nBaseAddress+2)) ^ 0xB);
}

void CParallelPort::WriteData(unsigned short nBaseAddress, int nData)
{
  //The data port is at offset nBaseAddress.
  _outp(nBaseAddress, nData);
}

int CParallelPort::ReadData(unsigned short nBaseAddress)
{
  //The data port is at offset nBaseAddress.
  return _inp(nBaseAddress);
}

int CParallelPort::ReadStatus(unsigned short nBaseAddress)
{
  //The status port is at offset nBaseAddress + 1.
  //Bit 7 need to be inverted  
  return (_inp(static_cast<unsigned short>(nBaseAddress+1)) ^ 0x80);
}

void CParallelPort::WriteControl(int nData)
{
  ASSERT(IsOpen()); //Port must be open
  WriteControl(m_nBaseAddress, nData);  
}

int CParallelPort::ReadControl()
{
  ASSERT(IsOpen()); //Port must be open
  return ReadControl(m_nBaseAddress);  
}

void CParallelPort::WriteData(int nData)
{
  ASSERT(IsOpen()); //Port must be open
  WriteData(m_nBaseAddress, nData);  
}

int CParallelPort::ReadData()
{
  ASSERT(IsOpen()); //Port must be open
  return ReadData(m_nBaseAddress);  
}

int CParallelPort::ReadStatus()
{
  ASSERT(IsOpen()); //Port must be open
  return ReadStatus(m_nBaseAddress);  
}

BOOL CParallelPort::ReadByteUsingNibbleMode(_Out_ BYTE& byData)
{
  ASSERT(IsOpen()); //Port must be open

  //Read a byte of data at the status port, in 2 nibbles

  //When S6 = 0, set D3 to 0
  int S6;
  DWORD dwStartTicks = GetTickCount();
  do
  {
    S6 = (ReadStatus(m_nBaseAddress) & 0x40) >> 6;

    //Check the timeout has not elapsed
    if ((GetTickCount() - dwStartTicks) > m_dwTimeout)
      return FALSE;
  }
  while (S6 == 1);
  WriteData(m_nBaseAddress, 0);

  //When the peripheral responds by setting S6=1, Set D3=1
  //nLowNibble then holds 4 bits of data
  int nLowNibble;
  do
  {
    nLowNibble = ReadStatus(m_nBaseAddress);
    S6 = (nLowNibble & 0x40) >> 6;

    //Check the timeout has not elapsed
    if ((GetTickCount() - dwStartTicks) > m_dwTimeout)
      return FALSE;
  }
  while (S6 == 0);
  WriteData(m_nBaseAddress, 8);

  //When S6 = 0, set D3 to 0
  do
  {
    S6 = (ReadStatus(m_nBaseAddress) & 0x40) >> 6;

    //Check the timeout has not elapsed
    if ((GetTickCount() - dwStartTicks) > m_dwTimeout)
      return FALSE;
  }
  while (S6 == 1);
  WriteData(m_nBaseAddress, 0);

  //When the peripheral responds by setting S6=1, Set D3=1
  //nHighNibble then holds 4 bits of data
  int nHighNibble;
  do
  {
    nHighNibble = ReadStatus(m_nBaseAddress);
    S6 = (nHighNibble & 0x40) >> 6;

    //Check the timeout has not elapsed
    if ((GetTickCount() - dwStartTicks) > m_dwTimeout)
      return FALSE;
  }
  while (S6 == 0);
  WriteData(m_nBaseAddress, 8);

  //Recombine the two nibbles back into the byte
  byData = static_cast<BYTE>(((nLowNibble & 0x8) >> 3) +
                             ((nLowNibble & 0x10) >> 3) +
                             ((nLowNibble & 0x20) >> 3) +
                             ((nLowNibble & 0x80) >> 4) +
                             ((nHighNibble & 0x8) << 1) +
                             ((nHighNibble & 0x10) << 1) +
                             ((nHighNibble & 0x20) << 1) +
                             ((nHighNibble & 0x80))); 

  return TRUE;
}

BOOL CParallelPort::WriteByteUsingNibbleMode(BYTE byData)
{
  ASSERT(IsOpen()); //Port must be open

  //Write a byte to the data port, in 2 nibbles.
  //The remote system reads the data at its status port.
  //The data bits are D0, D1, D2 and D4.
  //D3 is the strobe
  int nLowNibble = byData & 0x07;
  nLowNibble |= (byData & 0x08) << 1;
  int nHighNibble = (byData & 0x70) >> 4;
  nHighNibble |= (byData & 0x80) >> 3;

  //When S6=1 (not busy), write the low nibble and set D3=0.
  int S6;
  DWORD dwStartTicks = GetTickCount();  
  do
  {
    S6 = (ReadStatus(m_nBaseAddress) & 0x40) >> 6;

    //Check the timeout has not elapsed
    if ((GetTickCount() - dwStartTicks) > m_dwTimeout)
      return FALSE;
  }
  while (S6 == 0);
  WriteData(m_nBaseAddress, nLowNibble);

  //When the peripheral responds by setting S6=0, Set D3=1.
  do
  {
    S6 = (ReadStatus(m_nBaseAddress) & 0x40) >> 6;

    //Check the timeout has not elapsed
    if ((GetTickCount() - dwStartTicks) > m_dwTimeout)
      return FALSE;
  }
  while (S6 == 1);
  WriteData(m_nBaseAddress, nLowNibble | 0x8);

  //When S6=1, write the high nibble and set D3=0
  do
  {
    S6 = (ReadStatus(m_nBaseAddress) & 0x40) >> 6;

    //Check the timeout has not elapsed
    if ((GetTickCount() - dwStartTicks) > m_dwTimeout)
      return FALSE;
  }
  while (S6 == 0);
  WriteData(m_nBaseAddress, nHighNibble);

  //When the peripheral responds by setting S6=0, Set D3=1.
  do
  {
    S6 = (ReadStatus(m_nBaseAddress) & 0x40) >> 6;

    //Check the timeout has not elapsed
    if ((GetTickCount() - dwStartTicks) > m_dwTimeout)
      return FALSE;
  }
  while (S6 == 1);
  WriteData(m_nBaseAddress, nHighNibble | 0x8);

  return TRUE;
}

BOOL CParallelPort::ReadUsingNibbleMode(_Out_bytecap_(dwBytes) void* lpBuf, DWORD dwBytes)
{
  //Validate the parameters
  ASSERT(lpBuf);

  //Read each byte into lpBuf  
  BYTE* lpByteBuf = static_cast<BYTE*>(lpBuf);
  for (DWORD i=0; i<dwBytes; i++)
  {
#pragma warning(suppress : 6385) // Spurious warning
    if (!ReadByteUsingNibbleMode(lpByteBuf[i]))
      return FALSE;
  }
  return TRUE;
}

BOOL CParallelPort::WriteUsingNibbleMode(_In_bytecount_(dwBytes) const void* lpBuf, DWORD dwBytes)
{
  //Validate the parameters
  ASSERT(lpBuf);

  //Write each byte from lpBuf  
  const BYTE* lpByteBuf = static_cast<const BYTE*>(lpBuf);
  for (DWORD i=0; i<dwBytes; i++)
  {
    if (!WriteByteUsingNibbleMode(lpByteBuf[i]))
      return FALSE;
  }
  return TRUE;
}

int CParallelPort::_inp(unsigned short port)
{
  if (sm_bRunningOnNT)
  {
    ASSERT(sm_lpfnDlPortReadUchar);
    return sm_lpfnDlPortReadUchar(port);
  }
  else
  {
  #ifdef  _M_IX86
    return ::_inp(port);
  #else
    return 0; //To keep the compiler happy when we compile for X64
  #endif
  }
}

int CParallelPort::_outp(unsigned short port, int databyte)
{
  if (sm_bRunningOnNT)
  {
    ASSERT(sm_lpfnDlPortWriteUchar);
    sm_lpfnDlPortWriteUchar(port, static_cast<UCHAR>(databyte));
    return databyte;
  }
  else
  {
  #ifdef  _M_IX86
    return ::_outp(port, databyte);
  #else
    return 0; //To keep the compiler happy when we compile for X64
  #endif
  }
}

BOOL CParallelPort::InitializeDriverLINX()
{
  //Load up the DriverLINX dll and get the function pointers we are interested in
  ASSERT(sm_hDLINX == NULL);
  BOOL bSuccess = FALSE;
  sm_hDLINX = LoadLibrary(_T("DLPORTIO.DLL"));
  if (sm_hDLINX)
  {
    //Get the function pointers
    sm_lpfnDlPortReadUchar = reinterpret_cast<LPDLPORTREADPORTUCHAR>(GetProcAddress(sm_hDLINX, "DlPortReadPortUchar"));
    sm_lpfnDlPortWriteUchar = reinterpret_cast<LPDLPORTWRITEPORTUCHAR>(GetProcAddress(sm_hDLINX, "DlPortWritePortUchar"));

    //If any of the functions are not installed then fail the load
    if (sm_lpfnDlPortReadUchar == NULL || sm_lpfnDlPortWriteUchar == NULL)
    {
      TRACE(_T("CParallelPort::InitializeDriverLINX, Failed to get one of the function pointer`s in the DriverLINX dll\n"));
      DeInitializeDriverLINX();
    }
    else
      bSuccess = TRUE;
  }
  else
    TRACE(_T("CParallelPort::InitializeDriverLINX, Could not find the load the DriverLINX dll, please ensure DriverLINX driver is installed\n"));

  return bSuccess;
}

void CParallelPort::DeInitializeDriverLINX()
{
  if (sm_hDLINX)
  {
    //Unload the dll and reset the function pointers to NULL    
    FreeLibrary(sm_hDLINX);
    sm_hDLINX = NULL;
    sm_lpfnDlPortReadUchar = NULL;
    sm_lpfnDlPortWriteUchar = NULL;
  }
}

BOOL CParallelPort::PortPresent(int nPort)
{
  BOOL bSuccess = FALSE;

  //Try to open the port 
  CString sPort;
  sPort.Format(_T("\\\\.\\LPT%d"), nPort);
  HANDLE hPort = CreateFile(sPort, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
  if (hPort != INVALID_HANDLE_VALUE)
  {
    bSuccess = TRUE;

    //Close the port, now that we don't need it anymore
    CloseHandle(hPort);
  }
  else
  {
    //Determine if the port exists based on the error code.
    DWORD dwError = GetLastError();

    //Check to see if the error was because some other app had the port open or a general failure
    if (dwError == ERROR_ACCESS_DENIED || dwError == ERROR_GEN_FAILURE)
      bSuccess = TRUE;
  }

  return bSuccess;
}

BOOL CParallelPort::RunningOnNT()
{
  OSVERSIONINFO osvi;
  memset(&osvi, 0, sizeof(OSVERSIONINFO));
  osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
  return (GetVersionEx(&osvi) && osvi.dwPlatformId == VER_PLATFORM_WIN32_NT);
}

CParallelPortSettings CParallelPort::GetSettings() const
{
  ASSERT(IsOpen()); //Port must be open 
  return sm_Ports.GetAt(m_nPortIndex);
}



////////// The file transfer class

CParallelPortFileTransfer::CParallelPortFileTransfer(_In_ CParallelPort* pPort) : m_pPort(NULL)
{
  ASSERT(pPort != NULL);
  ASSERT(pPort->IsOpen());

  //Ensure the Port is set to SPP mode if it is an ECP port
  CParallelPortSettings settings = pPort->GetSettings();
  if (settings.m_Type == CParallelPortSettings::ParallelTypeECP)
    pPort->SetECPMode(CParallelPortSettings::ECPModeSPP);

  //Initialize D3 (strobe) to 1
  pPort->WriteData(0x8);

  //Wait for the opposite end to set D3=1 (not busy)
  int S6;
  DWORD dwStartTicks = GetTickCount();
  do
  {
    S6 = (pPort->ReadStatus() & 0x40) >> 6;

    //Check the timeout has not elapsed
    if ((GetTickCount() - dwStartTicks) > pPort->GetTimeout())
    {
      TRACE(_T("CParallelPortFileTransfer::CParallelPortFileTransfer, Could not setup file transfer, other end busy\n"));
      CParallelPort::ThrowParallelException(ERROR_TIMEOUT);
    }
  }
  while (S6 == 0);

  //Store away the port pointer for use in other class methods
  m_pPort = pPort;
}

void CParallelPortFileTransfer::SendFile(_In_ const CString& sLocalFile)
{
  //Validate our parameters
  ASSERT(m_pPort);
  __analysis_assume(m_pPort);

  //First try to open the file to send
  CFile file;
  CFileException ex;
  if (file.Open(sLocalFile, CFile::modeRead | CFile::shareDenyWrite, &ex))
  {
    //first send the filename (excluding the path) (size if _MAX_PATH)
    TCHAR pszPath[_MAX_PATH];
    TCHAR pszFname[_MAX_FNAME];
    TCHAR pszExt[_MAX_EXT];
  #if (_MSC_VER >= 1400)
    _tsplitpath_s(sLocalFile, NULL, 0, NULL, 0, pszFname, sizeof(pszFname)/sizeof(TCHAR), pszExt, sizeof(pszExt)/sizeof(TCHAR));
    _tmakepath_s(pszPath, sizeof(pszPath)/sizeof(TCHAR), NULL, NULL, pszFname, pszExt);
  #else
    _tsplitpath(sLocalFile, NULL, NULL, pszFname, pszExt);
    _tmakepath(pszPath, NULL, NULL, pszFname, pszExt);
  #endif

    //Now need to convert the filename to ASCII so that a UNICODE
    //client can take to an ASCII server
    char pszAsciiPath[_MAX_PATH];
    CT2A pszTemp(pszPath);
    memset(pszAsciiPath, 0, _MAX_PATH);
  #if (_MSC_VER >= 1400)
    strcpy_s(pszAsciiPath, sizeof(pszAsciiPath), pszTemp);
  #else
    strcpy(pszAsciiPath, pszTemp);
  #endif

    //Do the actual send of the filename
    if (!m_pPort->WriteUsingNibbleMode(pszAsciiPath, _MAX_PATH))
    {
      TRACE(_T("CParallelPortFileTransfer::SendFile, Failed to send the filename\n"));
      CParallelPort::ThrowParallelException(ERROR_TIMEOUT);
    }

    try
    {
      //Then send the size of the file (as 4 bytes).
      DWORD dwLength = static_cast<DWORD>(file.GetLength()); //Note we only support transfering files ups to 4GB in size
      if (!m_pPort->WriteUsingNibbleMode(&dwLength, sizeof(DWORD)))
      {
        TRACE(_T("CParallelPortFileTransfer::SendFile, Failed to send the file length\n"));
        CParallelPort::ThrowParallelException(ERROR_TIMEOUT);
      }

      //Then the actual file contents ifself (4k at a time)
      BYTE byData[4096];
      int nBytesRead;
      do
      {
        nBytesRead = file.Read(byData, 4096);
        if (!m_pPort->WriteUsingNibbleMode(byData, nBytesRead))
        {
          TRACE(_T("CParallelPortFileTransfer::SendFile, Failed to send the file contents\n"));
          CParallelPort::ThrowParallelException(ERROR_TIMEOUT);
        }
      }
      while (nBytesRead);
    }
    catch(CFileException* pEx)
    {
      DWORD dwLastError = pEx->m_lOsError;
      TRACE(_T("CParallelPortFileTransfer::SendFile, An error occurred reading from the file to send, Error:%d\n"), dwLastError);
      delete pEx;
      CParallelPort::ThrowParallelException(dwLastError);
    }
  }
  else
  {
    TRACE(_T("CParallelPortFileTransfer::SendFile, Could not open the file to send, %s\n"), sLocalFile);
    CParallelPort::ThrowParallelException(ex.m_lOsError);
  }
}

void CParallelPortFileTransfer::ReceiveFile(_In_ const CString& sLocalFile)
{
	USES_CONVERSION;

  //First try to open the file to send
  CFile file;
  CFileException ex;
  if (file.Open(sLocalFile, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &ex))
  {
    //Receive the filename
    char pszAsciiPath[_MAX_PATH];
    if (!m_pPort->ReadUsingNibbleMode(pszAsciiPath, _MAX_PATH))
    {
      TRACE(_T("CParallelPortFileTransfer::ReceiveFile, Failed to receive the filename\n"));

      //Delete the local file
      file.Close();
      DeleteFile(sLocalFile);

      //Throw the exception
      CParallelPort::ThrowParallelException(ERROR_TIMEOUT);
    }

    try
    {
      //Receive the size of the file (as 4 bytes).
      DWORD dwLength;
      if (!m_pPort->ReadUsingNibbleMode(&dwLength, sizeof(DWORD)))
      {
        TRACE(_T("CParallelPortFileTransfer::ReceiveFile, Failed to receive the file length\n"));
  
        //Delete the local file
        file.Close();
        DeleteFile(sLocalFile);

        //Throw the exception
        CParallelPort::ThrowParallelException(ERROR_TIMEOUT);
      }

      //Then the actual file contents ifself (4k at a time)
      BYTE byData[4096];
      DWORD dwBytesLeft = dwLength;
      do
      {
        DWORD dwBytesToRead;
        if (dwBytesLeft > 4096)
          dwBytesToRead = 4096;
        else
          dwBytesToRead = dwBytesLeft;
          
        if (!m_pPort->ReadUsingNibbleMode(byData, dwBytesToRead))
        {
          TRACE(_T("CParallelPortFileTransfer::ReceiveFile, Failed to receive the file contents\n"));

          //Delete the local file
          file.Close();
          DeleteFile(sLocalFile);

          //Throw the exception
          CParallelPort::ThrowParallelException(ERROR_TIMEOUT);
        }

        file.Write(byData, dwBytesToRead);
        dwBytesLeft -= dwBytesToRead;
      }
      while (dwBytesLeft);
    }
    catch(CFileException* pEx)
    {
      DWORD dwLastError = pEx->m_lOsError;
      TRACE(_T("CParallelPortFileTransfer::ReceiveFile, An error occurred writing the file which was being received, Error:%d\n"), dwLastError);
      delete pEx;
      CParallelPort::ThrowParallelException(dwLastError);
    }
  }
  else
  {
    TRACE(_T("CParallelPortFileTransfer::ReceiveFile, Could not open the file to receive into, %s, Error:%d\n"), sLocalFile, ex.m_lOsError);
    CParallelPort::ThrowParallelException(ex.m_lOsError);
  }
}

