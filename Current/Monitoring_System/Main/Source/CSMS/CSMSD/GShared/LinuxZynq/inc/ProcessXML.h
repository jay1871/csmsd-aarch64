/*
 * ProcessXML.h
 *
 *  Created on: Sep 16, 2015
 *      Author: rwerpf
 */
#pragma once
#include <map>
#include <vector>

// Forward declarations
namespace rapidxml {
    template<class Ch> class xml_node;
    template<class Ch> class xml_attribute;
    template<class Ch> class xml_document;
}
using namespace rapidxml;
class XMLProcessor {
public:
	typedef std::vector<std::string> VectStr;
	typedef std::map<std::string, VectStr> XMLMap;

	XMLProcessor();
	virtual ~XMLProcessor();
	void HandleNodes(const char *elename, const char *value);
	void SaveXML(std::string filename);
	void ConstructXMLHeader();
	int ReadLoadXML(std::string filename);
	void PrintNodes();
	void GetNode(std::string nodename, std::string &value);
	VectStr GetXMLVectorValue(std::string elementname);
	static std::string attribnodename;

private:
	void AppendNode();
	void AppendNode(const char *elename, const char * value);
	void AppendAttribute(const char *elename, const char *value);
	void VisitAllNodes(const xml_node<char>* node, int indent);
	xml_node<char>* root;
	xml_node<char>* child;
	xml_node<char>* currParent, * Attribute_Node;
	xml_document<char> *doc;
	std::string root_name;
	int attribute_count;
	char attributename[20];
	std::string nestname;
	XMLMap XMLKeyPair;
};
