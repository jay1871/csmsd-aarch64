/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved			  *
**************************************************************************/
#pragma once

/// <summary>
/// This class provides RAII encapsulation of the boolean flag, where if
///  the flag is set by this object, it will be cleared when the object is destroyed.
/// The flag is usually used as condition variable in a routine that can be
///  re-entrant in the same thread where CriticalSection cannot be used for
///  protection.
/// </summary>
/// <remarks>
/// @note: 1. The setting and clearing of the flag is not protected by
///           CriticalSection, therefore this is strictly used by re-entrant
///           routine(s) running in the same thread.
///        2. It only clears the flag at dtor that is set by this object. Flag
///           set outside the object will not be cleared by dtor.
/// </remarks>
class CRaiiFlag
{
public:
	CRaiiFlag(bool & flag) : m_flag(flag) { m_lclFlag = false; return; }
	CRaiiFlag(bool & flag, bool flagVal) : m_flag(flag) { m_lclFlag = m_flag = flagVal; return; }
	~CRaiiFlag();
	bool IsSet(void) const { return m_flag; }
	void ModFlag(bool flagVal) { m_lclFlag = m_flag = flagVal; return; }

private:
	// Data
	bool&  m_flag;
	bool   m_lclFlag;

	//Disallow default constructor.
    CRaiiFlag();
	//Disallow default copy-constructor.
	CRaiiFlag(const CRaiiFlag & origFlag);
	//Disallow default assignment.
	CRaiiFlag& operator=(const CRaiiFlag & srcFlag);
};

//////////////////////////////////////////////////////////////////////
/// <summary>
/// Default dtor.
/// </summary>
inline CRaiiFlag::~CRaiiFlag()
{
    //Only clear the flag if it is set by this object.
	if( m_lclFlag )
	{
		m_flag = false;
	}

	return;
}