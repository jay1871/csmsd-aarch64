/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2010-2018 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once
#ifdef _MSC_VER
	#include <fdi.h>
#else
#define MTYPE_DISKFILE  (0x01)
#define MTYPE_STDIOFH   (0x02)
#define MTYPE_FILEDESC  (0x03)
#define MTYPE_MEMORY    (0x04)

struct m_filename {
  unsigned char type;   /* one of MTYPE_DISKFILE, STDIOFH, FILEDESC or MEMORY */
  const char *filename; /* the user-friendly printable filename (may be NULL) */
  union {
    const char *diskfile; /* char *filename      for MTYPE_DISKFILE */
    FILE *stdiofh;        /* FILE *existing_fh   for MTYPE_STDIOFH */
    int filedesc;         /* int file_descriptor for MTYPE_FILEDESC */
    struct {
      unsigned char *data;
      size_t length;
    } memory;
  } x;
};

struct m_file {
  struct m_filename *file; /* pointer back to the m_filename data */
  union {
    FILE *fh;        /* only used in DISKFILE, STDIOFH and FILEDESC types */
    size_t position; /* only used in MEMORY types */
  } x;
};

	#define CB_MAX_CHUNK            32768U
#endif
class CCabFile
{
public:
	// Functions
	CCabFile(_In_ LPCTSTR cabPath);
	~CCabFile(void);
#ifdef _MSC_VER
	CString GetFileName(void) const { return m_filename; }
	_Success_(return) bool GetLine(_Out_ CString& line);
#else
	std::string GetFileName(void) const { return m_filename; }
	_Success_(return) bool GetLine(_Out_ std::string& line);
#endif
	_Success_(return) bool GetOctalShort(_Out_ short& num);
	bool OpenNextFile(void);

private:
	// Functions
	static int Close(int fd);
#ifdef _MSC_VER
	static INT_PTR Notify_(FDINOTIFICATIONTYPE fdiType, _In_ FDINOTIFICATION* fdiNotification)
		{ return static_cast<CCabFile*>(fdiNotification->pv)->Notify(fdiType, fdiNotification); }
	INT_PTR Notify(FDINOTIFICATIONTYPE fdiType, _In_ FDINOTIFICATION* fdiNotification);
	static INT_PTR Open(_In_ LPSTR filename, int oflag, int pmode);
	static UINT Read(INT_PTR fd, _Out_bytecap_(count) void* buffer, unsigned int count);
	static UINT Thread(_In_ void* arg) { static_cast<CCabFile*>(arg)->Thread(); return 0; }
	static UINT Write(INT_PTR fd, _In_bytecount_(count) void* buffer, unsigned int count)
		{ return reinterpret_cast<CCabFile*>(fd)->Write(static_cast<char*>(buffer), count); }
	UINT Write(_In_bytecount_(count) const void* buffer, unsigned int count);
#endif
	void Thread(void);
	// Data
	char* m_bufEnd;
	char* m_buffer;
#ifdef _MSC_VER
	char m_cab[_MAX_PATH];
	ERF m_erf;
	CString m_filename;
	HFDI m_hFdi;
	CEvent m_needData;
	CEvent m_newData;
	char m_path[_MAX_PATH];
	CWinThread* m_thread;
#else
	std::string m_filename;
	char m_path[NAME_MAX+1];
	  struct mscab_decompressor *cabd;
	  struct mscabd_cabinet *cab;
	  struct mscabd_file *file;
	  const char * output;
	  std::thread m_thread;

	  void ExtractCabToMemBuffer();

	  /* ------------------------------------------------------------------------ */
	  /* mspack_system implementation */

	  static void *m_alloc(struct mspack_system *self, size_t bytes) {
	    return malloc(bytes);
	  }

	  static void m_free(void *buffer) {
	    free(buffer);
	  }

	  static void m_copy(void *src, void *dest, size_t bytes) {
	    memcpy(dest, src, bytes);
	  }

	  /* A message printer that prints to stderr */
	  static void m_msg(struct m_file *file, const char *format, ...) {
	    va_list ap;
	    if (file && file->file && file->file->filename) {
	      fprintf(stderr, "%s: ", file->file->filename);
	    }
	    va_start(ap, format);
	    vfprintf(stderr, format, ap);
	    va_end(ap);
	    fputc((int) '\n', stderr);
	    fflush(stderr);
	  }


	  static struct m_file *m_open_mem(struct mspack_system *self,
	  				 struct m_filename *fn, int mode)
	  {
	    struct m_file *fh;

	    /* validate arguments of the filename */
	    if (!fn->x.memory.data)   return NULL;
	    if (!fn->x.memory.length) return NULL;

	    if ((fh = (struct m_file *) m_alloc(self, sizeof(struct m_file)))) {
	      fh->x.position = (mode == MSPACK_SYS_OPEN_APPEND) ?
	        fn->x.memory.length : 0;
	      fh->file = fn;
	    }
	    return fh;
	  }

	  static struct m_file *m_open_file(struct mspack_system *self,
	  				  struct m_filename *fn, int mode)
	  {
	    struct m_file *fh;
	    const char *fmode;
	    int fd;

	    switch (mode) {
	    case MSPACK_SYS_OPEN_READ:   fmode = "rb";  break;
	    case MSPACK_SYS_OPEN_WRITE:  fmode = "wb";  break;
	    case MSPACK_SYS_OPEN_UPDATE: fmode = "r+b"; break;
	    case MSPACK_SYS_OPEN_APPEND: fmode = "ab";  break;
	    default: return NULL;
	    }

	    /* validate the arguments in the provided filename */
	    switch (fn->type) {
	    case MTYPE_DISKFILE: if (!fn->x.diskfile)    return NULL; break;
	    case MTYPE_STDIOFH:  if (!fn->x.stdiofh)     return NULL; break;
	    case MTYPE_FILEDESC: if (fn->x.filedesc < 0) return NULL; break;
	    }

	    /* allocate memory for the file handle */
	    if (!(fh = (struct m_file *) m_alloc(self, sizeof(struct m_file)))) return NULL;

	    /* open or duplicate the filehandle */
	    switch (fn->type) {
	    case MTYPE_DISKFILE:
	      fh->x.fh = fopen(fn->x.diskfile, fmode);
	      break;

	    case MTYPE_STDIOFH:
	      fd = fileno(fn->x.stdiofh);
	      fh->x.fh = (fd >= 0) ? fdopen(fd, fmode) : NULL;
	      break;

	    case MTYPE_FILEDESC:
	      fh->x.fh = fdopen(fn->x.filedesc, fmode);
	      break;
	    }

	    /* validate the new stdio filehandle */
	    if (fh->x.fh) {
	      fh->file = fn;
	    }
	    else {
	      free(fh);
	      fh = NULL;
	    }

	    return fh;
	  }

	  static struct m_file *m_open(struct mspack_system *self,
	  			     struct m_filename *fn, int mode)
	  {
	    if (!self || !fn) return NULL;

	    switch (fn->type) {
	    case MTYPE_DISKFILE:
	    case MTYPE_STDIOFH:
	    case MTYPE_FILEDESC:
	      return m_open_file(self, fn, mode);

	    case MTYPE_MEMORY:
	      return m_open_mem(self, fn, mode);
	    }
	    return NULL;
	  }

	  static void m_close(struct m_file *fh) {
	    if (!fh || !fh->file) return;
	    if (fh->file->type != MTYPE_MEMORY) fclose(fh->x.fh);
	    m_free(fh);
	  }


	  static int m_read(struct m_file *fh, void *buffer, int bytes) {
	    if (!fh || !fh->file || !buffer || bytes < 0) return -1;

	    if (fh->file->type == MTYPE_MEMORY) {
	      int count = fh->file->x.memory.length - fh->x.position;
	      if (count > bytes) count = bytes;
	      if (count > 0) {
	        m_copy(&fh->file->x.memory.data[fh->x.position], buffer, (size_t) count);
	      }
	      fh->x.position += count;
	      return count;
	    }
	    else {
	      size_t count = fread(buffer, 1, (size_t) bytes, fh->x.fh);
	      if (!ferror(fh->x.fh)) return (int) count;
	    }
	    return -1;
	  }


	  static int m_write(struct m_file *fh, void *buffer, int bytes) {
	    if (!fh || !fh->file || !buffer || bytes < 0) return -1;

	    if (fh->file->type == MTYPE_MEMORY) {
	      int count = fh->file->x.memory.length - fh->x.position;
	      if (count > bytes) count = bytes;
	      if (count > 0) {
	        m_copy(buffer, &fh->file->x.memory.data[fh->x.position], (size_t) count);
	      }
	      fh->x.position += count;
	      return count;
	    }
	    else {
	      size_t count = fwrite(buffer, 1, (size_t) bytes, fh->x.fh);
	      if (!ferror(fh->x.fh)) return (int) count;
	    }
	    return -1;
	  }


	  static int m_seek(struct m_file *fh, off_t offset, int mode) {
	    if (!fh || !fh->file) return 1;

	    if (fh->file->type == MTYPE_MEMORY) {
	      switch (mode) {
	      case MSPACK_SYS_SEEK_START:
	        break;
	      case MSPACK_SYS_SEEK_CUR:
	        offset += (off_t) fh->x.position;
	        break;
	      case MSPACK_SYS_SEEK_END:
	        offset += (off_t) fh->file->x.memory.length;
	        break;
	      default:
	        return 1;
	      }

	      if (offset < 0) return 1;
	      if (offset > (off_t) fh->file->x.memory.length) return 1;
	      fh->x.position = (size_t) offset;
	      return 0;
	    }

	    /* file IO based method */
	    switch (mode) {
	    case MSPACK_SYS_SEEK_START: mode = SEEK_SET; break;
	    case MSPACK_SYS_SEEK_CUR:   mode = SEEK_CUR; break;
	    case MSPACK_SYS_SEEK_END:   mode = SEEK_END; break;
	    default: return 1;
	    }
	  #if HAVE_FSEEKO
	    return fseeko(fh->x.fh, offset, mode);
	  #else
	    return fseek(fh->x.fh, offset, mode);
	  #endif
	  }


	  static off_t m_tell(struct m_file *fh) {
	    if (!fh || !fh->file) return -1;
	    if (fh->file->type == MTYPE_MEMORY) {
	      return (off_t) fh->x.position;
	    }
	  #if HAVE_FSEEKO
	    return (off_t) ftello(fh->x.fh);
	  #else
	    return (off_t) ftell(fh->x.fh);
	  #endif
	  }

	  struct mspack_system multi_system = {
	    (struct mspack_file * (*)(struct mspack_system *, const char *, int)) &m_open,
	    (void (*)(struct mspack_file *)) &m_close,
	    (int (*)(struct mspack_file *, void *, int)) &m_read,
	    (int (*)(struct mspack_file *, void *, int)) &m_write,
	    (int (*)(struct mspack_file *, off_t, int)) &m_seek,
	    (off_t (*)(struct mspack_file *)) &m_tell,
	    (void (*)(struct mspack_file *, const char *, ...))  &m_msg,
	    &m_alloc,
	    &m_free,
	    &m_copy,
	    NULL
	  };

	  const char *create_filename_from_memory(void *data, size_t length) {
	    struct m_filename *fn;

	    if (!data) return NULL; /* data pointer must not be NULL */
	    if (length == 0) return NULL; /* length must not be zero */

	    if ((fn = (struct m_filename *) malloc(sizeof(struct m_filename)))) {
	      fn->type = MTYPE_MEMORY;
	      fn->filename = NULL; /* pretty-printable filename */
	      fn->x.memory.data   = (unsigned char *) data;
	      fn->x.memory.length = length;
	    }
	    return (const char *) fn;
	  }

	  const char *create_filename(const char *filename) {
	    struct m_filename *fn;

	    if (!filename) return NULL; /* filename must not be null */

	    if ((fn = (struct m_filename *) malloc(sizeof(struct m_filename)))) {
	      fn->type = MTYPE_DISKFILE;
	      fn->filename = filename; /* pretty-printable filename */
	      fn->x.diskfile = filename;
	    }
	    return (const char *) fn;
	  }

#endif
	volatile bool m_eof;
	volatile bool m_shutdown;
};
