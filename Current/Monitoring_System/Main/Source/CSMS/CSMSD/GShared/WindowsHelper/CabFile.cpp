/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2010-2018 TCI International, Inc. All rights reserved         *
**************************************************************************/
#include "StdAfx.h"

#include <fcntl.h>
#include <sys/stat.h>

#ifdef _MSC_VER
#include <fdi.h>
#include <io.h>
#else
#include "mspack.h"
#endif
#include "CabFile.h"


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
#ifdef _MSC_VER
CCabFile::CCabFile(_In_ LPCTSTR cabPath)
:
	m_bufEnd(nullptr),
	m_buffer(nullptr),
	m_eof(false),
	m_hFdi(nullptr),
	m_needData(TRUE, FALSE), // Signalled, automatic reset
	m_newData(FALSE, FALSE), // Not signalled, automatic reset
	m_shutdown(false),
	m_thread(nullptr)
{
	// Create buffer
	if((m_bufEnd = m_buffer = static_cast<char*>(malloc(CB_MAX_CHUNK))) == nullptr)
	{
		CFileException::ThrowOsError(ERROR_OUTOFMEMORY, cabPath);
	}

	*m_bufEnd = '\0';

	// Create FDI context
	if((m_hFdi = FDICreate(reinterpret_cast<PFNALLOC>(malloc), free, Open, reinterpret_cast<PFNREAD>(_read), Write,
		Close, _lseek, cpuUNKNOWN, &m_erf)) == nullptr)
	{
		free(m_buffer);
		CFileException::ThrowOsError(m_erf.erfType, cabPath);
	}

	// Check CAB file validity
	int fd;

	if(_tsopen_s(&fd, cabPath, _O_RDONLY, _SH_DENYWR, _S_IREAD) != 0)
	{
		FDIDestroy(m_hFdi);
		free(m_buffer);
		CFileException::ThrowErrno(errno, cabPath);
	}

	FDICABINETINFO fdiCabinetInfo;

	if(!FDIIsCabinet(m_hFdi, fd, &fdiCabinetInfo) || fdiCabinetInfo.cFiles == 0 || fdiCabinetInfo.iCabinet > 0 ||
		fdiCabinetInfo.hasnext || fdiCabinetInfo.hasprev)
	{
		_close(fd);
		FDIDestroy(m_hFdi);
		free(m_buffer);
		CFileException::ThrowOsError(ERROR_INVALID_DATA, cabPath);
	}

	_close(fd);

	// Parse out path and name
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char filename[_MAX_FNAME];
	char ext[_MAX_EXT];
	_splitpath_s(CT2A(cabPath), drive, dir, filename, ext);
	_makepath_s(m_cab, nullptr, nullptr, filename, ext);
	_makepath_s(m_path, drive, dir, nullptr, nullptr);

	// Start a thread
	m_thread = AfxBeginThread(Thread, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, nullptr);
	m_thread->m_bAutoDelete = FALSE;
	m_thread->ResumeThread();

	// Wait for data
	WaitForSingleObject(m_newData.m_hObject, INFINITE);

	return;
}

#else // linux version

CCabFile::CCabFile(_In_ LPCTSTR cabPath)
:
	m_bufEnd(nullptr),
	m_buffer(nullptr),
	m_eof(false),
	m_shutdown(false)
{
	int test;
	const char *inputcab;
	MSPACK_SYS_SELFTEST(test);
	if (test != MSPACK_ERR_OK)
	{
		char errMsg[40];
		sprintf(errMsg, "MSPack Error = %d", test);
		throw std::runtime_error(errMsg);
	}
	// Create buffer
	if((m_bufEnd = m_buffer = static_cast<char*>(malloc(CB_MAX_CHUNK))) == nullptr)
	{
		throw std::runtime_error("Out of Memory");
	}

	if ((cabd = mspack_create_cab_decompressor(&multi_system))) // to extract to memory buffer
	{
		inputcab = create_filename(cabPath);
		if ((cab = cabd->open(cabd, inputcab)))
		{
			// Do extraction
			file = cab->files;
			m_filename = file->filename;

			//ExtractCabToMemBuffer();

		}
		else
			throw std::runtime_error("error opening cab file");
	}
	else
	{
		free(m_buffer);
		throw std::runtime_error("error creating cab decompressor");
	}

	// Start a thread
    m_thread = std::thread(&CCabFile::Thread, this);

	// Wait for data
    if(m_thread.joinable())
    	m_thread.join();

	return;
}
#endif



//////////////////////////////////////////////////////////////////////
//
// Destructor
//
#ifdef _MSC_VER
CCabFile::~CCabFile(void)
{
	// Abort extraction and stop thread
	if(m_thread != nullptr)
	{
		m_shutdown = true;
		m_needData.SetEvent();
		WaitForSingleObject(m_thread->m_hThread, INFINITE);
		delete m_thread;
	}

	// Destroy context
	if(m_hFdi != nullptr)
	{
		FDIDestroy(m_hFdi);
	}

	// Free buffer
	if(m_buffer != nullptr)
	{
		free(m_buffer);
	}

	return;
}
#else
CCabFile::~CCabFile(void)
{
	// Abort extraction and stop thread
	/*if(m_thread.joinable())
	{
		m_thread.join();

	}*/
	m_shutdown = true;

	// Free buffer
	if(m_buffer != nullptr)
	{
		free(m_buffer);
	}

	if (cabd)
	{
		if (cab)
			cabd->close(cabd, cab);
		mspack_destroy_cab_decompressor(cabd);
	}


	return;
}
#endif

#ifdef _MSC_VER
//////////////////////////////////////////////////////////////////////
//
// FDI Callback
//
int CCabFile::Close(int fd)
{
	if(fd > 2048) // 2048 is the max file descriptor (not defined in any public include file)
	{
		// A file in the cabinet
		return 0;
	}
	else
	{
		// A regular file (the cabinet itself)
		return _close(fd);
	}
}
#endif


//////////////////////////////////////////////////////////////////////
//
// Read a line from the current file
//
#ifdef _MSC_VER
_Success_(return) bool CCabFile::GetLine(_Out_ CString& line)
{
	if(m_eof)
	{
		// End of file reached previously
		return false;
	}

	// Look for newline
	char* eol = nullptr;

	while(!m_eof && (eol = strchr(m_buffer, '\n')) == nullptr)
	{
		// Get more data
		m_needData.SetEvent();
		WaitForSingleObject(m_newData.m_hObject, INFINITE);
	}

	if(eol != nullptr)
	{
		 // Found newline
		*eol = '\0';

		if(eol > m_buffer && *(eol - 1) == '\r')
		{
			// Erase carriage return
			*(eol - 1) = '\0';
		}
	}
	else if(m_bufEnd == m_buffer)
	{
		// Empty - return eof
		return false;
	}
	else
	{
		// Remainder up to eof not terminated by newline
		eol = m_bufEnd;
	}

	// Got a line
	line = m_buffer;

	// Move remainder to front of buffer
	memmove(m_buffer, eol + 1, m_bufEnd - eol);
	m_bufEnd -= eol - m_buffer + 1 ;

	return true;
}
#else
_Success_(return) bool CCabFile::GetLine(_Out_ std::string& line)
{
	if(m_eof)
	{
		// End of file reached previously
		return false;
	}

	// Look for newline
	char* eol = nullptr;

	eol = strchr(m_buffer, '\n');

	if(eol != nullptr)
	{
		 // Found newline
		*eol = '\0';

		if(eol > m_buffer && *(eol - 1) == '\r')
		{
			// Erase carriage return
			*(eol - 1) = '\0';
		}
	}
	else if(m_bufEnd == m_buffer)
	{
		// Empty - return eof
		m_eof = true;
		return false;
	}
	else
	{
		// Remainder up to eof not terminated by newline
		eol = m_bufEnd;
	}

	// Got a line
	line = m_buffer;

	// Move remainder to front of buffer
	memmove(m_buffer, eol + 1, m_bufEnd - eol);
	m_bufEnd -= eol - m_buffer + 1 ;


	return true;
}
#endif


//////////////////////////////////////////////////////////////////////
//
// Read a short formatted as octal from the current file
//
#ifdef _MSC_VER
_Success_(return) bool CCabFile::GetOctalShort(_Out_ short& num)
{
	if(m_eof)
	{
		// End of file reached previously
		return false;
	}

	// Try to extract octal short
	int items = 0;
	int chars = 0;

	while(!m_eof && ((items = sscanf_s(m_buffer, " %ho%n", &num, &chars)) == EOF || m_buffer + chars == m_bufEnd))
	{
		// Get more data
		m_needData.SetEvent();
		WaitForSingleObject(m_newData.m_hObject, INFINITE);
	}

	if(m_eof)
	{
		// Reached eof without finding anything - clear buffer and return eof
		m_bufEnd = m_buffer;
		*m_bufEnd = '\0';
		return false;
	}

	if(items != 1)
	{
		// Bad data - leave data in buffer so GetLine can retrieve it but return eof
		return false;
	}

	// Move remainder to front of buffer
	memmove(m_buffer, &m_buffer[chars], m_bufEnd - &m_buffer[chars] + 1);
	m_bufEnd -= chars;

	return true;
}
#else
_Success_(return) bool CCabFile::GetOctalShort(_Out_ short& num)
{
	if(m_eof)
	{
		// End of file reached previously
		return false;
	}

	// Try to extract octal short
	int items = 0;
	int chars = 0;

	if(((items = sscanf(m_buffer, " %ho%n", &num, &chars)) == EOF || m_buffer + chars == m_bufEnd))
	{
		m_eof = true;
	}


	if(m_eof)
	{
		// Reached eof without finding anything - clear buffer and return eof
		m_bufEnd = m_buffer;
		*m_bufEnd = '\0';
		return false;
	}

	if(items != 1)
	{
		// Bad data - leave data in buffer so GetLine can retrieve it but return eof
		m_eof = true;
		return false;
	}

	// Move remainder to front of buffer
	memmove(m_buffer, &m_buffer[chars], m_bufEnd - &m_buffer[chars] + 1);
	m_bufEnd -= chars;

	return true;
}
#endif

//////////////////////////////////////////////////////////////////////
//
// FDI Callback
//
#ifdef _MSC_VER

INT_PTR CCabFile::Notify(FDINOTIFICATIONTYPE fdiType, _In_ FDINOTIFICATION* fdiNotification)
{
	switch(fdiType)
	{
	case fdintCABINET_INFO:
		// Do nothing
		return 0;

	case fdintCLOSE_FILE_INFO:
		WaitForSingleObject(m_needData.m_hObject, INFINITE);
		m_eof = true;
		m_newData.SetEvent();

		return m_shutdown ? FALSE : TRUE;

	case fdintCOPY_FILE:
		WaitForSingleObject(m_needData.m_hObject, INFINITE);

		// Remember the file name
		m_filename = fdiNotification->psz1;
		m_eof = false;
		m_newData.SetEvent();

		// Use the this pointer as the file descriptor
		return m_shutdown ? -1 : reinterpret_cast<INT_PTR>(this);

	case fdintENUMERATE:
		// Do nothing
		return 0;

	case fdintNEXT_CABINET:
		 // Not implemented
		return -1;

	case fdintPARTIAL_FILE:
		 // Not implemented
		return -1;

	default:
		// Ignore
		return 0;
	}
}
#endif


//////////////////////////////////////////////////////////////////////
//
// FDI Callback
//
#ifdef _MSC_VER
INT_PTR CCabFile::Open(_In_ LPSTR filename, int oflag, int pmode)
{
	int fd;
	_sopen_s(&fd, filename, oflag, _SH_DENYWR, pmode);

	return fd;
}
#endif


//////////////////////////////////////////////////////////////////////
//
// Open the next file in the cabinet
//
#ifdef _MSC_VER
bool CCabFile::OpenNextFile(void)
{
	// Eat anything remaining in the current file
	CString discard;
	while(GetLine(discard));

	// Signal to continue past fdintCOPY_FILE or exit thread
	m_needData.SetEvent();
	WaitForSingleObject(m_newData.m_hObject, INFINITE);

	return !m_shutdown;
}
#else

bool CCabFile::OpenNextFile(void)
{
	file = file->next;
	if (file)
	{
		m_filename = file->filename;
		free((char*)output);
		//ExtractCabToMemBuffer();
	    m_thread = std::thread(&CCabFile::Thread, this);
	    if(m_thread.joinable())
	    	m_thread.join();
	}
	else
		m_shutdown = true;

	if (m_shutdown)
		printf("last file in cab extracted\n");
	return !m_shutdown;
}

void CCabFile::ExtractCabToMemBuffer()
{
	output = create_filename_from_memory(m_buffer, file->length);
	if (cabd->extract(cabd, file, output))
	{
		int lasterr = cabd->last_error(cabd);
		switch (lasterr) {
			case MSPACK_ERR_OPEN:
			case MSPACK_ERR_READ:
			case MSPACK_ERR_WRITE:
			case MSPACK_ERR_SEEK:
				printf("open read write or seek error\n");
				break;
			case MSPACK_ERR_NOMEMORY:
				printf ("out of memory");
				break;
			case MSPACK_ERR_SIGNATURE:
				printf( "bad CAB signature");
				break;
			case MSPACK_ERR_DATAFORMAT:
				printf("error in CAB data format");
				break;
			case MSPACK_ERR_CHECKSUM:
				printf("checksum error");
				break;
			case MSPACK_ERR_DECRUNCH:
				printf( "decompression error");
				break;
		  default:
			printf("unknown error =%d\n", lasterr);
			break;
		  }
		char errMsg[40];
		sprintf(errMsg, "error %d extracting cab file =%s", lasterr, m_filename.c_str());
		throw std::runtime_error(errMsg);

	}
	else
	{
		if(m_buffer)
		{
			m_eof = false;
			m_bufEnd = m_buffer;
			m_bufEnd += file->length;
			*m_bufEnd = '\0';
		}

	}
}
#endif


#ifdef _MSC_VER
//////////////////////////////////////////////////////////////////////
//
// Thread
//
void CCabFile::Thread(void)
{
	// Do extraction
	if(FDICopy(m_hFdi, m_cab, m_path, 0, Notify_, nullptr, this))
	{
		// Wait to exit
		WaitForSingleObject(m_needData.m_hObject, INFINITE);
	}

	m_shutdown = true;
	m_filename = _T("");
	m_newData.SetEvent();

	return;
}
#else
//////////////////////////////////////////////////////////////////////
//
// Thread
//
void CCabFile::Thread(void)
{
	// Do extraction

	ExtractCabToMemBuffer();

	return;
}
#endif

//////////////////////////////////////////////////////////////////////
//
// FDI Callback
//
#ifdef _MSC_VER
UINT CCabFile::Write(_In_bytecount_(count) const void* buffer, unsigned int count)
{
	// Wait until data requested
	WaitForSingleObject(m_needData.m_hObject, INFINITE);

	if(m_shutdown)
	{
		// Aborting
		return UINT(-1);
	}

	// Check buffer size
	size_t spaceNeeded = m_bufEnd - m_buffer + count + 1;

	if(spaceNeeded > _msize(m_buffer))
	{
		// Resize buffer
		if(char* newBuffer = static_cast<char*>(realloc(m_buffer, spaceNeeded)))
		{
			if(newBuffer != m_buffer)
			{
				m_bufEnd += newBuffer - m_buffer;
				m_buffer = newBuffer;
			}
		}
		else
		{
			errno = ENOMEM;
			return UINT(-1);
		}
	}

	// Add new data to end of buffer
	memcpy(m_bufEnd, buffer, count);
	m_bufEnd += count;
	*m_bufEnd = '\0';

	// Signal
	m_newData.SetEvent();

	return count;
}
#endif
