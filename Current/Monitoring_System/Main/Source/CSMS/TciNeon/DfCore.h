/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2017 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "TciNeon.h"

class CDfCore
{
public:
protected:
	CDfCore(size_t numAnts);
	virtual ~CDfCore(void) {}
	float CalcSymConf(float azim, const TciNeon::Ne10F32cVec& volts, bool phaseAdjust, bool horiz) const;
	void InitSymConf(float confFact, float confNorm, const TciNeon::Ne10F32Vec& measAmp);
private:
	static TciNeon::Ne10F32cVec m_antVec;
	float m_confFact;
	float m_confNorm;
	TciNeon::Ne10F32Vec m_measAmp;
	size_t m_numAnts;
};
