/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2017 TCI International, Inc. All rights reserved              *
**************************************************************************/
#include <assert.h>
#include "DfCore.h"
#include "vdtMath.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
TciNeon::Ne10F32cVec CDfCore::m_antVec;

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CDfCore::CDfCore(size_t numAnts) :
	m_confFact(0),
	m_confNorm(0)
{
	// Adjust static data if necessary
	if (m_numAnts != numAnts)
	{
		m_numAnts = numAnts;
		assert(m_numAnts > 1);
		m_antVec.resize(int(m_numAnts));
		float phase = 0;
		ne10sTone_Direct(m_antVec, 1, 1.0f / m_numAnts, phase);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Calculate symmetry confidence
//
float CDfCore::CalcSymConf(float azim, const TciNeon::Ne10F32cVec& volts, bool phaseAdjust, bool horiz) const
{
	static const float PI = 4 * atan(1.0f);

	float cosAzim;
	float sinAzim;
	vdt::fast_sincosf(azim, sinAzim, cosAzim);
	double tempr = 0;
	double tempi = 0;

	float phase;
	float sinp;
	float cosp;
	for (size_t i = 0; i < volts.size(); i++)
	{
		phase = -(cosAzim * m_antVec[i].r + sinAzim * m_antVec[i].i) * m_confFact;;
		if (phaseAdjust && horiz && phase < 0)
		{
			// Adjust expected phase of back elements by 180�
			phase += PI;
		}
		vdt::fast_sincosf(phase, sinp, cosp);
		tempr += (volts[i].r * cosp - volts[i].i * sinp) * m_measAmp[i];
		tempi += (volts[i].r * sinp + volts[i].i * cosp) * m_measAmp[i];
	}

	return static_cast<float>(tempr * tempr + tempi * tempi) * m_confNorm;
}


//////////////////////////////////////////////////////////////////////
//
// Initialize azimuth-independent features for confidence calculation.
//
void CDfCore::InitSymConf(float confFact, float confNorm, const TciNeon::Ne10F32Vec& measAmp)
{
	m_confFact = confFact;
	m_confNorm = confNorm;
	m_measAmp = measAmp;

	return;
}

