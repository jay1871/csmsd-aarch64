/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2017 TCI International, Inc. All rights reserved              *
**************************************************************************/
#pragma once
#include <vector>
#include <map>
#include "IppVec.h"
#include "Units.h"

namespace CCabToCsms
{
	typedef std::map<Units::Frequency, float> FreqDepParams;
	typedef std::vector<Ipp32fcVec> WfaConjPattern; // Complex conjugate of pattern voltages[az][ant]
	typedef std::map<Units::Frequency, WfaConjPattern> WfaConjPatterns; // Lookup by frequency

	void Convert(LPCTSTR filePath);
	void Verify(LPCTSTR filePath);
	void LoadPatternCabFile(LPCTSTR patternFile, int numAnts, WfaConjPatterns& s_wfaConjPatterns);
};

