#cmake file for csmsd
message(STATUS "Loading Projects and files from ProjectsAndFiles.cmake")

# set various projects dir
SET(TOP_DIR 		${PROJECT_SOURCE_DIR})
SET(CSMSD_DIR		${TOP_DIR}/CSMSD)
SET(GSHARED_DIR 	${CSMSD_DIR}/GShared)
SET(G_CONTAINERS_DIR	${GSHARED_DIR}/Containers)
SET(G_HWZYNQ_DIR	${GSHARED_DIR}/HardwareZynq)
SET(G_LINUXZYNQ_DIR	${GSHARED_DIR}/LinuxZynq)
SET(G_LINUXZYNQ_DIR_LIB ${GSHARED_DIR}/LinuxZynq/lib)
SET(G_SHAREDINC_DIR	${GSHARED_DIR}/SharedInclude)
SET(G_HW_DIR		${GSHARED_DIR}/Hardware)
SET(G_IO_DIR		${GSHARED_DIR}/IO)
SET(G_PRIMITIVES_DIR	${GSHARED_DIR}/Primitives)
SET(G_WINHELPER_DIR	${GSHARED_DIR}/WindowsHelper)
SET(CSMSD_COMMON_DIR    ${CSMSD_DIR}/Common)
SET(CSMSD_SRC_DIR	${CSMSD_DIR}/csmsd/src)
SET(CSMSD_DATA_DIR	${CSMSD_DIR}/csmsd/Data)
SET(TCINEON_DIR 	${TOP_DIR}/TciNeon)

message(STATUS "Base dir is: ${TOP_DIR}")
message(STATUS "CSMSD source dir is: ${CSMSD_SRC_DIR}")
message(STATUS "TCI Neon dir is: ${TCINEON_DIR}")

# list of source code now.

#for TCI NEON library
file(GLOB TCINEONLIB_SRC
${TCINEON_DIR}/inc/*.h
${TCINEON_DIR}/DfCore.cpp
${TCINEON_DIR}/DfCore.h
${TCINEON_DIR}/TciNeon.cpp
${TCINEON_DIR}/TciNeon.h
)

#for GShared subdirs
file(GLOB G_SHAREDINC_SRC
${G_SHAREDINC_SRC}/*.h
)

file(GLOB G_LINUXZYNQ_SRC
${G_LINUXZYNQ_DIR}/inc/*.h
)

file(GLOB G_COMMON_SRC
${CSMSD_COMMON_DIR}*.h
${CSMSD_COMMON_DIR}/CsmsLicense.cpp
${CSMSD_COMMON_DIR}/PatternDatFile.cpp
${CSMSD_COMMON_DIR}/Sha256.cpp
)

file(GLOB G_HW_SRC
${G_HW_DIR}/*.h
${G_HW_DIR}/N9010A544B25.cpp
${G_HW_DIR}/RfSelSwitch.cpp
${G_HW_DIR}/ShfExt.cpp
${G_HW_DIR}/ShfExtNet.cpp
)

file(GLOB G_IO_SRC
${G_IO_DIR}/*.h
#${G_IO_DIR}/ParallelPort.cpp
#${G_IO_DIR}/serialport.cpp
)

file(GLOB G_CONTAINERS_SRC
${G_CONTAINERS_DIR}/PriorityQueue.h
)

file(GLOB G_HWZYNQ_SRC
${G_HWZYNQ_DIR}/*.h
${G_HWZYNQ_DIR}/2630.cpp
${G_HWZYNQ_DIR}/SBuffer.cpp
${G_HWZYNQ_DIR}/3230.cpp
)

file(GLOB G_PRIMITIVES_SRC
${G_PRIMITIVES_DIR}/*.h
)

file(GLOB G_WINHELPER_SRC
${G_WINHELPER_DIR}/*.h
${G_WINHELPER_DIR}/CabFile.cpp
)

# for csmd src code
file(GLOB CSMSD_SRC
${CSMSD_SRC_DIR}/*.h
${CSMSD_SRC_DIR}/51432017.cpp
${CSMSD_SRC_DIR}/DfCtrlNet.cpp	  
${CSMSD_SRC_DIR}/MeasurementTask.cpp  
${CSMSD_SRC_DIR}/Processing.cpp
${CSMSD_SRC_DIR}/Stats.cpp
${CSMSD_SRC_DIR}/7234201102.cpp
${CSMSD_SRC_DIR}/Digitizer.cpp 
${CSMSD_SRC_DIR}/Metrics.cpp    
${CSMSD_SRC_DIR}/ProcessorNode.cpp  
${CSMSD_SRC_DIR}/StatsNoHist.cpp
${CSMSD_SRC_DIR}/72362001.cpp	
${CSMSD_SRC_DIR}/EquipControl.cpp  
${CSMSD_SRC_DIR}/MetricsNet.cpp
${CSMSD_SRC_DIR}/RadioEquip.cpp
${CSMSD_SRC_DIR}/SuspendableTask.cpp
${CSMSD_SRC_DIR}/Agc.cpp
${CSMSD_SRC_DIR}/EquipCtrlNet.cpp
${CSMSD_SRC_DIR}/Monitor.cpp
${CSMSD_SRC_DIR}/RealtimeNet.cpp
${CSMSD_SRC_DIR}/Task.cpp
${CSMSD_SRC_DIR}/Antenna.cpp
${CSMSD_SRC_DIR}/Failure.cpp
${CSMSD_SRC_DIR}/Navigation.cpp
${CSMSD_SRC_DIR}/ResultsTbl.cpp
${CSMSD_SRC_DIR}/TciGps.cpp
${CSMSD_SRC_DIR}/Audio.cpp
${CSMSD_SRC_DIR}/Fft.cpp
${CSMSD_SRC_DIR}/NoiseEstimate.cpp
${CSMSD_SRC_DIR}/ScanDfTask.cpp
${CSMSD_SRC_DIR}/Utility.cpp
${CSMSD_SRC_DIR}/AvdTask.cpp
${CSMSD_SRC_DIR}/HwControl.cpp
${CSMSD_SRC_DIR}/OccupancyTask.cpp
${CSMSD_SRC_DIR}/ScheduleTbl.cpp
${CSMSD_SRC_DIR}/VCPCtrl.cpp
${CSMSD_SRC_DIR}/Bist.cpp
${CSMSD_SRC_DIR}/Ionogram.cpp
${CSMSD_SRC_DIR}/PanTask.cpp
${CSMSD_SRC_DIR}/SharedMemory.cpp
${CSMSD_SRC_DIR}/VCPCtrlNet.cpp
${CSMSD_SRC_DIR}/Config.cpp
${CSMSD_SRC_DIR}/Log.cpp
${CSMSD_SRC_DIR}/PbCal.cpp
${CSMSD_SRC_DIR}/SlaveControl.cpp
${CSMSD_SRC_DIR}/Watchdog.cpp
${CSMSD_SRC_DIR}/Df.cpp
${CSMSD_SRC_DIR}/main.cpp
${CSMSD_SRC_DIR}/PersistentData.cpp
${CSMSD_SRC_DIR}/SqliteDb.cpp
${CSMSD_SRC_DIR}/stdafx.cpp
)

# define source code groups - TODO for IDE
source_group(TCINEONLib 	FILES ${TCINEONLIB_SRC}	)

# merge source groups that build csmsd into one variable so easier to work with.
set(CSMSD_ALL_SRC
${G_SHAREDINC_SRC}
${G_LINUXZYNQ_SRC}
${G_COMMON_SRC}
${G_HW_SRC}
${G_IO_SRC}
${G_CONTAINERS_SRC}
${G_HWZYNQ_SRC}
${G_PRIMITIVES_SRC}
${G_WINHELPER_SRC}
${CSMSD_SRC}
)

# define library built
# note this will drop it where cmakelist.txt is under bin.
# include search path for tci neon

# we need to change the naming of the output eventually TODO
add_library(tcineonlib ${TCINEONLIB_SRC})

# set required system libraries now
set(MY_LINK_LIBS 
   pthread 
   rt
)

INCLUDE_DIRECTORIES(
${TCINEON_DIR}
${TCINEON_DIR}/inc
${G_CONTAINERS_DIR}
${G_HWZYNQ_DIR}
${G_IO_DIR}
${G_LINUXZYNQ_DIR}/inc
${G_PRIMITIVES_DIR}
${G_SHAREDINC_DIR}
${CSMSD_COMMON_DIR}
${CSMSD_SRC_DIR}
${G_HW_DIR}
${G_WINHELPER_DIR}
)

# now build executable csmsd
add_executable(csmsd ${CSMSD_ALL_SRC})

# add necessary libs that csmsd needs
target_link_libraries(csmsd ${MY_LINK_LIBS})
target_link_libraries(csmsd tcineonlib)  # we need to make sure it depends on this.
target_link_libraries(csmsd libSqlite.a)
target_link_libraries(csmsd libgps.so)
target_link_libraries(csmsd libCSMSXML.a)
target_link_libraries(csmsd libhidapi-libusb.a)
target_link_libraries(csmsd libusb-1.0.a)
target_link_libraries(csmsd libmspack.a)
target_link_libraries(csmsd -L${G_LINUXZYNQ_DIR_LIB})

# define executable(s) built - TODO - add Debug/Release directories.
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
set(LIBRARY_OUTPUT_PATH    ${PROJECT_SOURCE_DIR}/bin)
