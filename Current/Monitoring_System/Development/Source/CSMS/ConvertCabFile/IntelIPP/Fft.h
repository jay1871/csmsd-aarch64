/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include <algorithm>
#include <math.h>
#pragma warning(push)
#pragma warning(disable : 6011) // Warnings from ppl.h
#include <ppl.h>
#pragma warning(pop)

#include "IppVec.h"
#include "Units.h"

#pragma intrinsic(cos, log)

//////////////////////////////////////////////////////////////////////
//
// Template declaration
//
// T is the frequency domain data type, U is the time domain data type, S is the time domain scale factor (eg 23 for 24-bit data in a 32-bit word),
// I is an instance identifier so multiple concurrent FFTs of different sizes can cache their parameters
template<typename T = Ipp32f, typename U = Ipp16s, int S = 15, unsigned int = 1> class CFft
{
public:
	// Functions
	CFft(unsigned int numSamples, _In_count_(numSamples) const typename Ipp<U>::IppCmplx* samples, IppHintAlgorithm hint = ippAlgHintNone);
	~CFft(void);
	static T GetBwCorrection(T db);
	static T GetEnbw(void) { return m_enbw; }
	void GetPowerSpectrumDbm(_Out_ IppVec<T>& dbm, unsigned long numBins, float gainAdj) const;
	void GetPowerSpectrumWatts(_Out_ IppVec<T>& watts, unsigned long numBins, float gainAdj) const;
	_Ret_ const IppCmplxVec<T>& GetSamples(void) const { return m_samples; }
	void GetVoltages(_Out_ IppCmplxVec<T>& volts, unsigned long numBins, float gainAdj) const;
	static IppVec<T> WattsToDbm(_In_ const IppVec<T>& watts);

private:
	// Types
	struct SDftSpec_C // Wraps the DFT specification pointer
	{
	public:
		SDftSpec_C(void) : dftSpec(nullptr) {}
		~SDftSpec_C(void) { if(dftSpec != nullptr) ippsDFTFree_C(dftSpec); }
		SDftSpec_C& operator=(nullptr_t) { dftSpec = nullptr; return *this; }
		operator typename Ipp<T>::IppsDFTSpec_C*() { return dftSpec; }
		typename Ipp<T>::IppsDFTSpec_C** operator &() { return &dftSpec; }

	private:
		typename Ipp<T>::IppsDFTSpec_C* dftSpec;
	};

	struct SFftSpec_C // Wraps the FFT specification pointer
	{
	public:
		SFftSpec_C(void) : fftSpec(nullptr) {}
		~SFftSpec_C(void) { if(fftSpec != nullptr) ippsFFTFree_C(fftSpec); }
		SFftSpec_C& operator=(nullptr_t) { fftSpec = nullptr; return *this; }
		operator typename Ipp<T>::IppsFFTSpec_C*() { return fftSpec; }
		typename Ipp<T>::IppsFFTSpec_C** operator &() { return &fftSpec; }

	private:
		typename Ipp<T>::IppsFFTSpec_C* fftSpec;
	};

	// Functions
	CFft(void); // Not allowed

	// Data
	static IppCmplxVec<T> m_blackmanHarris;
	static concurrency::combinable<Ipp8uVec> m_buffer;
	static CCriticalSection m_critSect;
	static SDftSpec_C m_dftSpec;
	static T m_enbw;
	static int m_fftBufSize;
	static SFftSpec_C m_fftSpec;
	static IppHintAlgorithm m_hint;
	static unsigned int m_numSamples;
	static unsigned int m_useCount;
	IppCmplxVec<T> m_volts;
};


//////////////////////////////////////////////////////////////////////
//
// Static data
//
template<typename T, typename U, int S, unsigned int I> IppCmplxVec<T> CFft<T, U, S, I>::m_blackmanHarris;
template<typename T, typename U, int S, unsigned int I>  concurrency::combinable<Ipp8uVec> CFft<T, U, S, I>::m_buffer;
template<typename T, typename U, int S, unsigned int I> CCriticalSection CFft<T, U, S, I>::m_critSect;
template<typename T, typename U, int S, unsigned int I> typename CFft<T, U, S, I>::SDftSpec_C CFft<T, U, S, I>::m_dftSpec;
template<typename T, typename U, int S, unsigned int I> T CFft<T, U, S, I>::m_enbw = 0;
template<typename T, typename U, int S, unsigned int I> int CFft<T, U, S, I>::m_fftBufSize = 0;
template<typename T, typename U, int S, unsigned int I> typename CFft<T, U, S, I>::SFftSpec_C CFft<T, U, S, I>::m_fftSpec;
template<typename T, typename U, int S, unsigned int I> IppHintAlgorithm CFft<T, U, S, I>::m_hint = ippAlgHintNone;
template<typename T, typename U, int S, unsigned int I> unsigned int CFft<T, U, S, I>::m_numSamples = 0;
template<typename T, typename U, int S, unsigned int I> unsigned int CFft<T, U, S, I>::m_useCount = 0;


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
// The Pentek sequencer can only schedule up to 65535 samples, so that number is used for a 64k FFT and zero-padded
//
template<typename T, typename U, int S, unsigned int I> CFft<T, U, S, I>::CFft(unsigned int numSamples, _In_count_(numSamples) const typename Ipp<U>::IppCmplx* samples, IppHintAlgorithm hint)
:
	m_volts(numSamples == 65535 ? 65536 : numSamples)
{
	{
		CSingleLock lock(&m_critSect, TRUE);
		++m_useCount;

		if(m_volts.size() != m_numSamples || hint != m_hint)
		{
			// Shouldn't change FFT size if there's another active instance
			ASSERT(m_useCount == 1);
			m_hint = hint;

			// Initialize FFT and associated static data
			DWORD fftOrder = 0;

			if(numSamples == 65535)
			{
				fftOrder = 16;
				m_numSamples = 65536;
			}
			else
			{
				if(!_BitScanForward(&fftOrder, numSamples))
				{
					fftOrder = 0;
				}

				m_numSamples = numSamples;

				if(1u << fftOrder != numSamples)
				{
					// Use DFT
					fftOrder = 0;
				}
			}

			if(m_dftSpec != nullptr)
			{
				ippsDFTFree_C(m_dftSpec);
				m_dftSpec = nullptr;
			}

			if(m_fftSpec != nullptr)
			{
				ippsFFTFree_C(m_fftSpec);
				m_fftSpec = nullptr;
			}

			if(fftOrder == 0)
			{
				ippsDFTInitAlloc_C(&m_dftSpec, m_numSamples, IPP_FFT_DIV_FWD_BY_N, hint);
				ippsDFTGetBufSize_C(m_dftSpec, &m_fftBufSize);
			}
			else
			{
				ippsFFTInitAlloc_C(&m_fftSpec, fftOrder, IPP_FFT_DIV_FWD_BY_N, hint);
				ippsFFTGetBufSize_C(m_fftSpec, &m_fftBufSize);
			}

			// Blackman-Harris window
			m_blackmanHarris.assign(m_numSamples, IppCmplxVec<T>::ZERO);
			const T ARG = Units::TWO_PI / (m_numSamples - 1);
			T sum = 0;
			T sumSq = 0;

			for(size_t i = 0; i < m_numSamples / 2u; ++i)
			{
				const T COS = cos(ARG * i);
				m_blackmanHarris[i].re = m_blackmanHarris[m_numSamples - 1 - i].re =
					0.35875f - 0.48829f * COS + 0.14128f * (2 * COS * COS - 1) - 0.01168f * (4 * COS * COS * COS - 3 * COS);
				sum += m_blackmanHarris[i].re;
				sumSq += m_blackmanHarris[i].re * m_blackmanHarris[i].re;
			}

			// Calculate equivalent noise bandwidth
			m_enbw = m_numSamples * sumSq / (2 * sum * sum);

			// Adjust window for unity gain
			m_blackmanHarris /= 2 * sum / m_numSamples;
		}

		// Resize buffer
		m_buffer.local().resize(m_fftBufSize);
	}

	// Convert samples to complex float format scaling to -1.0 to +1.0
	// Note: Is and Qs are reversed, but we've done an extra spectrum inversion to compensate
	IppCmplxVec<T> samples2(m_numSamples);
	ippsConvert(reinterpret_cast<const U*>(samples), reinterpret_cast<T*>(&samples2[0]), 2 * numSamples, S);

	if(numSamples == 65535)
	{
		// Pad last sample
		samples2[65535] = IppCmplxVec<T>::ZERO;
	}

	// Multiply by window
	samples2 *= m_blackmanHarris;

	// Perform FFT
	if(m_dftSpec != nullptr)
	{
		ippsDFTFwd_CToC(samples2, m_volts, m_dftSpec, m_buffer.local());
	}
	else
	{
		ippsFFTFwd_CToC(samples2, m_volts, m_fftSpec, m_buffer.local());
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
template<typename T, typename U, int S, unsigned int I> CFft<T, U, S, I>::~CFft(void)
{
	CSingleLock lock(&m_critSect, TRUE);
	--m_useCount;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get bandwidth correction for window function (in bins)
//
template<typename T, typename U, int S, unsigned int I> T CFft<T, U, S, I>::GetBwCorrection(T)
{
	return 3; // TODO do this better
}


//////////////////////////////////////////////////////////////////////
//
// Get power spectrum
//
template<typename T, typename U, int S, unsigned int I> void CFft<T, U, S, I>::GetPowerSpectrumDbm(_Out_ IppVec<T>& dbm, unsigned long numBins, float gainAdj) const
{
	GetPowerSpectrumWatts(dbm, numBins, gainAdj);
	ippsLog10(dbm);
	dbm *= 10;
	dbm += 30;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get power spectrum
//
template<typename T, typename U, int S, unsigned int I> void CFft<T, U, S, I>::GetPowerSpectrumWatts(_Out_ IppVec<T>& watts, unsigned long numBins, float gainAdj) const
{
	ASSERT(numBins <= m_numSamples);
	watts.resize(numBins);
	ippsPowerSpectr(&m_volts[m_numSamples / 2 - numBins / 2], watts);
	watts *= gainAdj;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get voltage vectors
//
template<typename T, typename U, int S, unsigned int I> void CFft<T, U, S, I>::GetVoltages(_Out_ IppCmplxVec<T>& volts, unsigned long numBins, float gainAdj) const
{
	ASSERT(numBins <= m_numSamples);
	volts.resize(numBins);
	ippsCopy(&m_volts[m_numSamples / 2 - numBins / 2], &volts[0], numBins);
	volts *= sqrt(gainAdj);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Convert Watts to dBm
//
template<typename T, typename U, int S, unsigned int I> IppVec<T> CFft<T, U, S, I>::WattsToDbm(_In_ const IppVec<T>& watts)
{
	IppVec<T> dbm;
	ippsLog10(watts, dbm);
	dbm *= 10;
	dbm += 30;

	return dbm;
}

#pragma function(cos, log)
