/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2014 TCI International, Inc.   All rights reserved       *
**************************************************************************/

#pragma once

#include <algorithm>
#include <exception>
#include <ipp.h>

#include "PoolAllocator.h"

// Functions from Ipp 6 missing from Ipp 7 - also link with ippsrmerged.lib
extern "C"
{
#ifdef _M_X64
	IppStatus m7_ippsNthMaxElement_32f(const Ipp32f* pSrc, int len, int N, Ipp32f* pRes);
	IppStatus m7_ippsNthMaxElement_64f(const Ipp64f* pSrc, int len, int N, Ipp64f* pRes);
	struct IppsResamplingPolyphase_16s;
	IppStatus m7_ippsResamplePolyphaseFree_16s(IppsResamplingPolyphase_16s* pSpec);
	IppStatus m7_ippsResamplePolyphaseInitAlloc_16s(IppsResamplingPolyphase_16s** pSpec, Ipp32f window,
		int nStep, Ipp32f rollf, Ipp32f alpha, IppHintAlgorithm hint);
	IppStatus m7_ippsResamplePolyphase_16s(const IppsResamplingPolyphase_16s* pSpec, const Ipp16s* pSrc, int len,
		Ipp16s* pDst, Ipp64f factor, Ipp32f norm, Ipp64f* pTime, int* pOutlen);
#else
	IppStatus __STDCALL w7_ippsNthMaxElement_32f(const Ipp32f* pSrc, int len, int N, Ipp32f* pRes);
	IppStatus __STDCALL w7_ippsNthMaxElement_64f(const Ipp64f* pSrc, int len, int N, Ipp64f* pRes);
	struct IppsResamplingPolyphase_16s;
	IppStatus __STDCALL w7_ippsResamplePolyphaseFree_16s(IppsResamplingPolyphase_16s* pSpec);
	IppStatus __STDCALL w7_ippsResamplePolyphaseInitAlloc_16s(IppsResamplingPolyphase_16s** pSpec, Ipp32f window,
		int nStep, Ipp32f rollf, Ipp32f alpha, IppHintAlgorithm hint);
	IppStatus __STDCALL w7_ippsResamplePolyphase_16s(const IppsResamplingPolyphase_16s* pSpec, const Ipp16s* pSrc, int len,
		Ipp16s* pDst, Ipp64f factor, Ipp32f norm, Ipp64f* pTime, int* pOutlen);
#endif
}

// Generic template - specialized further down
template<typename T> struct Ipp
{
private:
	friend IppStatus ippsDiv_I(_In_count_(len) const Ipp16s*, _Inout_count_(len) Ipp16sc*, size_t len);
	friend IppStatus ippsDiv_I(_In_count_(len) const Ipp16sc*, _Inout_count_(len) Ipp16sc*, size_t len);
	friend IppStatus ippsDiv_I(_In_count_(len) const Ipp32f*, _Inout_count_(len) Ipp32fc*, size_t len);
	friend IppStatus ippsDiv_I(_In_count_(len) const Ipp64f*, _Inout_count_(len) Ipp64fc*, size_t len);
	friend IppStatus ippsMul_I(_In_count_(len) const Ipp16s*, _Inout_count_(len) Ipp16sc*, size_t len);
	friend IppStatus ippsMul_I(_In_count_(len) const Ipp32s*, _Inout_count_(len) Ipp32sc*, size_t len);
	friend IppStatus ippsMul_I(_In_count_(len) const Ipp32f*, _Inout_count_(len) Ipp32fc*, size_t len);
	friend IppStatus ippsMul_I(_In_count_(len) const Ipp64f*, _Inout_count_(len) Ipp64fc*, size_t len);
	template<typename U> friend class IppVec;
	template<typename U> friend class IppCmplxVec;
	static const size_t IPP_ALIGN = 32;
	static const size_t SMALL_VECTOR = 16;
};

// Overloaded definitions for C++ - not complete; add more if needed
inline IppStatus ippsAdd_I(_In_count_(len) const Ipp16s* pSrc, _Inout_count_(len) Ipp16s* pSrcDst, size_t len) { return ippsAdd_16s_ISfs(pSrc, pSrcDst, int(len), 0); }
inline IppStatus ippsAdd_I(_In_count_(len) const Ipp16sc* pSrc, _Inout_count_(len) Ipp16sc* pSrcDst, size_t len) { return ippsAdd_16sc_ISfs(pSrc, pSrcDst, int(len), 0); }
inline IppStatus ippsAdd_I(_In_count_(len) const Ipp32s* pSrc, _Inout_count_(len) Ipp32s* pSrcDst, size_t len) { return ippsAdd_32s_ISfs(pSrc, pSrcDst, int(len), 0); }
inline IppStatus ippsAdd_I(_In_count_(len) const Ipp32sc* pSrc, _Inout_count_(len) Ipp32sc* pSrcDst, size_t len) { return ippsAdd_32sc_ISfs(pSrc, pSrcDst, int(len), 0); }
inline IppStatus ippsAdd_I(_In_count_(len) const Ipp32f* pSrc, _Inout_count_(len) Ipp32f* pSrcDst, size_t len) { return ippsAdd_32f_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsAdd_I(_In_count_(len) const Ipp32fc* pSrc, _Inout_count_(len) Ipp32fc* pSrcDst, size_t len) { return ippsAdd_32fc_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsAdd_I(_In_count_(len) const Ipp64f* pSrc, _Inout_count_(len) Ipp64f* pSrcDst, size_t len) { return ippsAdd_64f_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsAdd_I(_In_count_(len) const Ipp64fc* pSrc, _Inout_count_(len) Ipp64fc* pSrcDst, size_t len) { return ippsAdd_64fc_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsAddC_I(Ipp16s val, _Inout_count_(len) Ipp16s* pSrcDst, size_t len) { return ippsAddC_16s_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsAddC_I(Ipp16sc val, _Inout_count_(len) Ipp16sc* pSrcDst, size_t len) { return ippsAddC_16sc_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsAddC_I(Ipp32s val, _Inout_count_(len) Ipp32s* pSrcDst, size_t len) { return ippsAddC_32s_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsAddC_I(Ipp32sc val, _Inout_count_(len) Ipp32sc* pSrcDst, size_t len) { return ippsAddC_32sc_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsAddC_I(Ipp32f val, _Inout_count_(len) Ipp32f* pSrcDst, size_t len) { return ippsAddC_32f_I(val, pSrcDst, int(len)); }
inline IppStatus ippsAddC_I(Ipp32fc val, _Inout_count_(len) Ipp32fc* pSrcDst, size_t len) { return ippsAddC_32fc_I(val, pSrcDst, int(len)); }
inline IppStatus ippsAddC_I(Ipp64f val, _Inout_count_(len) Ipp64f* pSrcDst, size_t len) { return ippsAddC_64f_I(val, pSrcDst, int(len)); }
inline IppStatus ippsAddC_I(Ipp64fc val, _Inout_count_(len) Ipp64fc* pSrcDst, size_t len) { return ippsAddC_64fc_I(val, pSrcDst, int(len)); }
inline void ippsConvert(_In_count_(len) const Ipp8u* pSrc, _Out_cap_(len) Ipp32f* pDst, size_t len) { VERIFY(ippsConvert_8u32f(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp16s* pSrc, _Out_cap_(len) Ipp32f* pDst, size_t len, int scaleFactor = 0)
	{ VERIFY(ippsConvert_16s32f_Sfs(pSrc, pDst, int(len), scaleFactor) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp16sc* pSrc, _Out_cap_(len) Ipp32fc* pDst, size_t len, int scaleFactor = 0)
	{ VERIFY(ippsConvert_16s32f_Sfs(reinterpret_cast<const Ipp16s*>(pSrc), reinterpret_cast<Ipp32f*>(pDst), int(2 * len), scaleFactor) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp16sc* pSrc, _Out_cap_(len) Ipp64fc* pDst, size_t len, int scaleFactor = 0)
	{ VERIFY(ippsConvert_16s64f_Sfs(reinterpret_cast<const Ipp16s*>(pSrc), reinterpret_cast<Ipp64f*>(pDst), int(2 * len), scaleFactor) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp16s* pSrc, _Out_cap_(len) Ipp32s* pDst, size_t len) { VERIFY(ippsConvert_16s32s(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp16sc* pSrc, _Out_cap_(len) Ipp32sc* pDst, size_t len)
	{ VERIFY(ippsConvert_16s32s(reinterpret_cast<const Ipp16s*>(pSrc), reinterpret_cast<Ipp32s*>(pDst), int(2 * len)) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp16s* pSrc, _Out_cap_(len) Ipp64f* pDst, size_t len, int scaleFactor = 0)
	{ VERIFY(ippsConvert_16s64f_Sfs(pSrc, pDst, int(len), scaleFactor) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp32s* pSrc, _Out_cap_(len) Ipp32f* pDst, size_t len, int scaleFactor = 0)
	{ VERIFY(ippsConvert_32s32f_Sfs(pSrc, pDst, int(len), scaleFactor) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp32sc* pSrc, _Out_cap_(len) Ipp32fc* pDst, size_t len, int scaleFactor = 0)
	{ VERIFY(ippsConvert_32s32f_Sfs(reinterpret_cast<const Ipp32s*>(pSrc), reinterpret_cast<Ipp32f*>(pDst), int(2 * len), scaleFactor) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp32sc* pSrc, _Out_cap_(len) Ipp64fc* pDst, size_t len, int scaleFactor = 0)
	{ VERIFY(ippsConvert_32s64f_Sfs(reinterpret_cast<const Ipp32s*>(pSrc), reinterpret_cast<Ipp64f*>(pDst), int(2 * len), scaleFactor) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp32s* pSrc, _Out_cap_(len) Ipp64f* pDst, size_t len, int scaleFactor = 0)
	{ VERIFY(ippsConvert_32s64f_Sfs(pSrc, pDst, int(len), scaleFactor) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp32f* pSrc, _Out_cap_(len) Ipp8u* pDst, size_t len, IppRoundMode rndMode = ippRndNear, int scaleFactor = 0)
	{ VERIFY(ippsConvert_32f8u_Sfs(pSrc, pDst, int(len), rndMode, scaleFactor) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp32f* pSrc, _Out_cap_(len) Ipp32s* pDst, size_t len, IppRoundMode rndMode = ippRndNear, int scaleFactor = 0)
	{ VERIFY(ippsConvert_32f32s_Sfs(pSrc, pDst, int(len), rndMode, scaleFactor) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp32f* pSrc, _Out_cap_(len) long* pDst, size_t len, IppRoundMode rndMode = ippRndNear, int scaleFactor = 0)
	{ VERIFY(ippsConvert_32f32s_Sfs(pSrc, reinterpret_cast<Ipp32s*>(pDst), int(len), rndMode, scaleFactor) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp32fc* pSrc, _Out_cap_(len) Ipp32sc* pDst, size_t len, IppRoundMode rndMode = ippRndNear, int scaleFactor = 0)
	{ VERIFY(ippsConvert_32f32s_Sfs(reinterpret_cast<const Ipp32f*>(pSrc), reinterpret_cast<Ipp32s*>(pDst), int(2 * len), rndMode, scaleFactor) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp32f* pSrc, _Out_cap_(len) Ipp16s* pDst, size_t len, IppRoundMode rndMode = ippRndNear, int scaleFactor = 0)
	{ VERIFY(ippsConvert_32f16s_Sfs(pSrc, pDst, int(len), rndMode, scaleFactor) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp32fc* pSrc, _Out_cap_(len) Ipp16sc* pDst, size_t len, IppRoundMode rndMode = ippRndNear, int scaleFactor = 0)
	{ VERIFY(ippsConvert_32f16s_Sfs(reinterpret_cast<const Ipp32f*>(pSrc), reinterpret_cast<Ipp16s*>(pDst), int(2 * len), rndMode, scaleFactor) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp32f* pSrc, _Out_cap_(len) Ipp64f* pDst, size_t len) { VERIFY(ippsConvert_32f64f(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp32fc* pSrc, _Out_cap_(len) Ipp64fc* pDst, size_t len)
	{ VERIFY(ippsConvert_32f64f(reinterpret_cast<const Ipp32f*>(pSrc), reinterpret_cast<Ipp64f*>(pDst), int(2 * len)) == ippStsNoErr); return; }
inline void ippsConvert(_In_count_(len) const Ipp64f* pSrc, _Out_cap_(len) Ipp32f* pDst, size_t len) { VERIFY(ippsConvert_64f32f(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsCopy(_In_count_(len) const Ipp8u* pSrc, _Out_cap_(len) Ipp8u* pDst, size_t len) { VERIFY(ippsCopy_8u(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsCopy(_In_count_(len) const Ipp16s* pSrc, _Out_cap_(len) Ipp16s* pDst, size_t len) { VERIFY(ippsCopy_16s(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsCopy(_In_count_(len) const Ipp16sc* pSrc, _Out_cap_(len) Ipp16sc* pDst, size_t len) { VERIFY(ippsCopy_16sc(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsCopy(_In_count_(len) const Ipp32s* pSrc, _Out_cap_(len) Ipp32s* pDst, size_t len) { VERIFY(ippsCopy_32s(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsCopy(_In_count_(len) const Ipp32sc* pSrc, _Out_cap_(len) Ipp32sc* pDst, size_t len) { VERIFY(ippsCopy_32sc(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsCopy(_In_count_(len) const Ipp64s* pSrc, _Out_cap_(len) Ipp64s* pDst, size_t len) { VERIFY(ippsCopy_64s(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsCopy(_In_count_(len) const Ipp64sc* pSrc, _Out_cap_(len) Ipp64sc* pDst, size_t len) { VERIFY(ippsCopy_64sc(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsCopy(_In_count_(len) const Ipp32f* pSrc, _Out_cap_(len) Ipp32f* pDst, size_t len) { VERIFY(ippsCopy_32f(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsCopy(_In_count_(len) const Ipp32fc* pSrc, _Out_cap_(len) Ipp32fc* pDst, size_t len) { VERIFY(ippsCopy_32fc(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsCopy(_In_count_(len) const Ipp64f* pSrc, _Out_cap_(len) Ipp64f* pDst, size_t len) { VERIFY(ippsCopy_64f(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsCopy(_In_count_(len) const Ipp64fc* pSrc, _Out_cap_(len) Ipp64fc* pDst, size_t len) { VERIFY(ippsCopy_64fc(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsDFTFree_C(_In_ IppsDFTSpec_C_32fc* pDFTSpec) { VERIFY(ippsDFTFree_C_32fc(pDFTSpec) == ippStsNoErr); return; }
inline void ippsDFTFree_C(_In_ IppsDFTSpec_C_64fc* pDFTSpec) { VERIFY(ippsDFTFree_C_64fc(pDFTSpec) == ippStsNoErr); return; }
inline void ippsDFTFree_R(_In_ IppsDFTSpec_R_32f* pDFTSpec) { VERIFY(ippsDFTFree_R_32f(pDFTSpec) == ippStsNoErr); return; }
inline void ippsDFTFree_R(_In_ IppsDFTSpec_R_64f* pDFTSpec) { VERIFY(ippsDFTFree_R_64f(pDFTSpec) == ippStsNoErr); return; }
inline void ippsDFTGetBufSize_C(_In_ const IppsDFTSpec_C_32fc* pDFTSpec, _Out_ int* pSize) { VERIFY(ippsDFTGetBufSize_C_32fc(pDFTSpec, pSize) == ippStsNoErr); return; }
inline void ippsDFTGetBufSize_C(_In_ const IppsDFTSpec_C_64fc* pDFTSpec, _Out_ int* pSize) { VERIFY(ippsDFTGetBufSize_C_64fc(pDFTSpec, pSize) == ippStsNoErr); return; }
inline void ippsDFTGetBufSize_R(_In_ const IppsDFTSpec_R_32f* pDFTSpec, _Out_ int* pSize) { VERIFY(ippsDFTGetBufSize_R_32f(pDFTSpec, pSize) == ippStsNoErr); return; }
inline void ippsDFTGetBufSize_R(_In_ const IppsDFTSpec_R_64f* pDFTSpec, _Out_ int* pSize) { VERIFY(ippsDFTGetBufSize_R_64f(pDFTSpec, pSize) == ippStsNoErr); return; }
inline void ippsDFTInitAlloc_C(_Deref_out_ IppsDFTSpec_C_32fc** pDFTSpec, int order, int flag, IppHintAlgorithm hint)
	{ VERIFY(ippsDFTInitAlloc_C_32fc(pDFTSpec, order, flag, hint) == ippStsNoErr); return; }
inline void ippsDFTInitAlloc_C(_Deref_out_ IppsDFTSpec_C_64fc** pDFTSpec, int order, int flag, IppHintAlgorithm hint)
	{ VERIFY(ippsDFTInitAlloc_C_64fc(pDFTSpec, order, flag, hint) == ippStsNoErr); return; }
inline void ippsDFTInitAlloc_R(_Deref_out_ IppsDFTSpec_R_32f** pDFTSpec, int order, int flag, IppHintAlgorithm hint)
	{ VERIFY(ippsDFTInitAlloc_R_32f(pDFTSpec, order, flag, hint) == ippStsNoErr); return; }
inline void ippsDFTInitAlloc_R(_Deref_out_ IppsDFTSpec_R_64f** pDFTSpec, int order, int flag, IppHintAlgorithm hint)
	{ VERIFY(ippsDFTInitAlloc_R_64f(pDFTSpec, order, flag, hint) == ippStsNoErr); return; }
inline IppStatus ippsDiv_I(_In_count_(len) const Ipp16s* pSrc, _Inout_count_(len) Ipp16s* pSrcDst, size_t len) { return ippsDiv_16s_ISfs(pSrc, pSrcDst, int(len), 0); }
inline IppStatus ippsDiv_I(_In_count_(len) const Ipp16s* pSrc, _Inout_count_(len) Ipp16sc* pSrcDst, size_t len)
{
	auto temp = (len > Ipp<void>::SMALL_VECTOR ? ippsMalloc_16sc(int(len)) : CPoolAllocator<Ipp16sc, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Allocate(len * sizeof(Ipp16sc)));
	ippsRealToCplx_16s(pSrc, pSrc, temp, int(len));
	auto status = ippsDiv_16s_ISfs(reinterpret_cast<Ipp16s*>(temp), reinterpret_cast<Ipp16s*>(pSrcDst), int(2 * len), 0);
	len > Ipp<void>::SMALL_VECTOR ? ippsFree(temp) : CPoolAllocator<Ipp16sc, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Deallocate(temp, len * sizeof(Ipp16sc));
	return status;
}
inline IppStatus ippsDiv_I(_In_count_(len) const Ipp16sc* pSrc, _Inout_count_(len) Ipp16sc* pSrcDst, size_t len) { return ippsDiv_16sc_ISfs(pSrc, pSrcDst, int(len), 0); }
inline IppStatus ippsDiv_I(_In_count_(len) const Ipp32s* pSrc, _Inout_count_(len) Ipp32s* pSrcDst, size_t len) { return ippsDiv_32s_ISfs(pSrc, pSrcDst, int(len), 0); }
inline IppStatus ippsDiv_I(_In_count_(len) const Ipp32f* pSrc, _Inout_count_(len) Ipp32f* pSrcDst, size_t len) { return ippsDiv_32f_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsDiv_I(_In_count_(len) const Ipp32f* pSrc, _Inout_count_(len) Ipp32fc* pSrcDst, size_t len)
{
	auto temp = (len > Ipp<void>::SMALL_VECTOR ? ippsMalloc_32fc(int(len)) : CPoolAllocator<Ipp32fc, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Allocate(len * sizeof(Ipp32fc)));
	ippsRealToCplx_32f(pSrc, pSrc, temp, int(len));
	auto status = ippsDiv_32f_I(reinterpret_cast<Ipp32f*>(temp), reinterpret_cast<Ipp32f*>(pSrcDst), int(2 * len));
	len > Ipp<void>::SMALL_VECTOR ? ippsFree(temp) : CPoolAllocator<Ipp32fc, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Deallocate(temp, len * sizeof(Ipp32fc));
	return status;
}
inline IppStatus ippsDiv_I(_In_count_(len) const Ipp32fc* pSrc, _Inout_count_(len) Ipp32fc* pSrcDst, size_t len) { return ippsDiv_32fc_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsDiv_I(_In_count_(len) const Ipp64f* pSrc, _Inout_count_(len) Ipp64f* pSrcDst, size_t len) { return ippsDiv_64f_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsDiv_I(_In_count_(len) const Ipp64f* pSrc, _Inout_count_(len) Ipp64fc* pSrcDst, size_t len)
{
	auto temp = (len > Ipp<void>::SMALL_VECTOR ? ippsMalloc_64fc(int(len)) : CPoolAllocator<Ipp64fc, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Allocate(len * sizeof(Ipp64fc)));
	ippsRealToCplx_64f(pSrc, pSrc, temp, int(len));
	auto status = ippsDiv_64f_I(reinterpret_cast<Ipp64f*>(temp), reinterpret_cast<Ipp64f*>(pSrcDst), int(2 * len));
	len > Ipp<void>::SMALL_VECTOR ? ippsFree(temp) : CPoolAllocator<Ipp64fc, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Deallocate(temp, len * sizeof(Ipp64fc));
	return status;
}
inline IppStatus ippsDiv_I(_In_count_(len) const Ipp64fc* pSrc, _Inout_count_(len) Ipp64fc* pSrcDst, size_t len) { return ippsDiv_64fc_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsDivC_I(Ipp16s val, _Inout_count_(len) Ipp16s* pSrcDst, size_t len) { return ippsDivC_16s_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsDivC_I(Ipp16s val, _Inout_count_(len) Ipp16sc* pSrcDst, size_t len) { return ippsDivC_16s_ISfs(val, reinterpret_cast<Ipp16s*>(pSrcDst), int(2 * len), 0); }
inline IppStatus ippsDivC_I(Ipp16sc val, _Inout_count_(len) Ipp16sc* pSrcDst, size_t len) { return ippsDivC_16sc_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsDivC_I(Ipp64s val, _Inout_count_(len) Ipp64s* pSrcDst, size_t len) { return ippsDivC_64s_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsDivC_I(Ipp32f val, _Inout_count_(len) Ipp32f* pSrcDst, size_t len) { return ippsDivC_32f_I(val, pSrcDst, int(len)); }
inline IppStatus ippsDivC_I(Ipp32f val, _Inout_count_(len) Ipp32fc* pSrcDst, size_t len) { return ippsDivC_32f_I(val, reinterpret_cast<Ipp32f*>(pSrcDst), int(2 * len)); }
inline IppStatus ippsDivC_I(Ipp32fc val, _Inout_count_(len) Ipp32fc* pSrcDst, size_t len) { return ippsDivC_32fc_I(val, pSrcDst, int(len)); }
inline IppStatus ippsDivC_I(Ipp64f val, _Inout_count_(len) Ipp64f* pSrcDst, size_t len) { return ippsDivC_64f_I(val, pSrcDst, int(len)); }
inline IppStatus ippsDivC_I(Ipp64f val, _Inout_count_(len) Ipp64fc* pSrcDst, size_t len) { return ippsDivC_64f_I(val, reinterpret_cast<Ipp64f*>(pSrcDst), int(2 * len)); }
inline IppStatus ippsDivC_I(Ipp64fc val, _Inout_count_(len) Ipp64fc* pSrcDst, size_t len) { return ippsDivC_64fc_I(val, pSrcDst, int(len)); }
inline void ippsFFTFree_C(_In_ IppsFFTSpec_C_32fc* pFFTSpec) { VERIFY(ippsFFTFree_C_32fc(pFFTSpec) == ippStsNoErr); return; }
inline void ippsFFTFree_C(_In_ IppsFFTSpec_C_64fc* pFFTSpec) { VERIFY(ippsFFTFree_C_64fc(pFFTSpec) == ippStsNoErr); return; }
inline void ippsFFTFree_R(_In_ IppsFFTSpec_R_32f* pFFTSpec) { VERIFY(ippsFFTFree_R_32f(pFFTSpec) == ippStsNoErr); return; }
inline void ippsFFTFree_R(_In_ IppsFFTSpec_R_64f* pFFTSpec) { VERIFY(ippsFFTFree_R_64f(pFFTSpec) == ippStsNoErr); return; }
inline void ippsFFTGetBufSize_C(_In_ const IppsFFTSpec_C_32fc* pFFTSpec, _Out_ int* pSize) { VERIFY(ippsFFTGetBufSize_C_32fc(pFFTSpec, pSize) == ippStsNoErr); return; }
inline void ippsFFTGetBufSize_C(_In_ const IppsFFTSpec_C_64fc* pFFTSpec, _Out_ int* pSize) { VERIFY(ippsFFTGetBufSize_C_64fc(pFFTSpec, pSize) == ippStsNoErr); return; }
inline void ippsFFTGetBufSize_R(_In_ const IppsFFTSpec_R_32f* pFFTSpec, _Out_ int* pSize) { VERIFY(ippsFFTGetBufSize_R_32f(pFFTSpec, pSize) == ippStsNoErr); return; }
inline void ippsFFTGetBufSize_R(_In_ const IppsFFTSpec_R_64f* pFFTSpec, _Out_ int* pSize) { VERIFY(ippsFFTGetBufSize_R_64f(pFFTSpec, pSize) == ippStsNoErr); return; }
inline void ippsFFTInitAlloc_C(_Deref_out_ IppsFFTSpec_C_32fc** pFFTSpec, int order, int flag, IppHintAlgorithm hint)
	{ VERIFY(ippsFFTInitAlloc_C_32fc(pFFTSpec, order, flag, hint) == ippStsNoErr); return; }
inline void ippsFFTInitAlloc_C(_Deref_out_ IppsFFTSpec_C_64fc** pFFTSpec, int order, int flag, IppHintAlgorithm hint)
	{ VERIFY(ippsFFTInitAlloc_C_64fc(pFFTSpec, order, flag, hint) == ippStsNoErr); return; }
inline void ippsFFTInitAlloc_R(_Deref_out_ IppsFFTSpec_R_32f** pFFTSpec, int order, int flag, IppHintAlgorithm hint)
	{ VERIFY(ippsFFTInitAlloc_R_32f(pFFTSpec, order, flag, hint) == ippStsNoErr); return; }
inline void ippsFFTInitAlloc_R(_Deref_out_ IppsFFTSpec_R_64f** pFFTSpec, int order, int flag, IppHintAlgorithm hint)
	{ VERIFY(ippsFFTInitAlloc_R_64f(pFFTSpec, order, flag, hint) == ippStsNoErr); return; }
inline void ippsFIRFree(_In_ IppsFIRState32s_16s* state) { VERIFY(ippsFIRFree32s_16s(state) == ippStsNoErr); }
inline void ippsFIRFree(_In_ IppsFIRState_32s* state) { VERIFY(ippsFIRFree_32s(state) == ippStsNoErr); }
inline void ippsFIROne(Ipp16s src, _Out_ Ipp16s* dst, IppsFIRState32s_16s* state, int scale) { VERIFY(ippsFIROne32s_16s_Sfs(src, dst, state, scale) == ippStsNoErr); }
inline void ippsFIROne(Ipp32s src, _Out_ Ipp32s* dst, IppsFIRState_32s* state, int scale) { VERIFY(ippsFIROne_32s_Sfs(src, dst, state, scale) == ippStsNoErr); }
inline IppStatus ippsMax(_In_count_(len) const Ipp16s* pSrc, size_t len, _Out_ Ipp16s* pMax) { return ippsMax_16s(pSrc, int(len), pMax); }
inline IppStatus ippsMax(_In_count_(len) const Ipp32s* pSrc, size_t len, _Out_ Ipp32s* pMax) { return ippsMax_32s(pSrc, int(len), pMax); }
inline IppStatus ippsMax(_In_count_(len) const Ipp32f* pSrc, size_t len, _Out_ Ipp32f* pMax) { return ippsMax_32f(pSrc, int(len), pMax); }
inline IppStatus ippsMax(_In_count_(len) const Ipp64f* pSrc, size_t len, _Out_ Ipp64f* pMax) { return ippsMax_64f(pSrc, int(len), pMax); }
inline void ippsMean(_In_count_(len) const Ipp16s* pSrc, size_t len, _Out_ Ipp16s* pMean) { VERIFY(ippsMean_16s_Sfs(pSrc, int(len), pMean, 0) == ippStsNoErr); return; }
inline void ippsMean(_In_count_(len) const Ipp16sc* pSrc, size_t len, _Out_ Ipp16sc* pMean) { VERIFY(ippsMean_16sc_Sfs(pSrc, int(len), pMean, 0) == ippStsNoErr); return; }
inline void ippsMean(_In_count_(len) const Ipp32s* pSrc, size_t len, _Out_ Ipp32s* pMean) { VERIFY(ippsMean_32s_Sfs(pSrc, int(len), pMean, 0) == ippStsNoErr); return; }
inline void ippsMean(_In_count_(len) const Ipp32f* pSrc, size_t len, _Out_ Ipp32f* pMean) { VERIFY(ippsMean_32f(pSrc, int(len), pMean, ippAlgHintAccurate) == ippStsNoErr); return; }
inline void ippsMean(_In_count_(len) const Ipp32fc* pSrc, size_t len, _Out_ Ipp32fc* pMean) { VERIFY(ippsMean_32fc(pSrc, int(len), pMean, ippAlgHintAccurate) == ippStsNoErr); return; }
inline void ippsMean(_In_count_(len) const Ipp64f* pSrc, size_t len, _Out_ Ipp64f* pMean) { VERIFY(ippsMean_64f(pSrc, int(len), pMean) == ippStsNoErr); return; }
inline void ippsMean(_In_count_(len) const Ipp64fc* pSrc, size_t len, _Out_ Ipp64fc* pMean) { VERIFY(ippsMean_64fc(pSrc, int(len), pMean) == ippStsNoErr); return; }
inline IppStatus ippsMin(_In_count_(len) const Ipp16s* pSrc, size_t len, _Out_ Ipp16s* pMin) { return ippsMin_16s(pSrc, int(len), pMin); }
inline IppStatus ippsMin(_In_count_(len) const Ipp32s* pSrc, size_t len, _Out_ Ipp32s* pMin) { return ippsMin_32s(pSrc, int(len), pMin); }
inline IppStatus ippsMin(_In_count_(len) const Ipp32f* pSrc, size_t len, _Out_ Ipp32f* pMin) { return ippsMin_32f(pSrc, int(len), pMin); }
inline IppStatus ippsMin(_In_count_(len) const Ipp64f* pSrc, size_t len, _Out_ Ipp64f* pMin) { return ippsMin_64f(pSrc, int(len), pMin); }
inline void ippsMove(_In_count_(len) const Ipp8u* pSrc, _Out_cap_(len) Ipp8u* pDst, size_t len) { VERIFY(ippsMove_8u(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsMove(_In_count_(len) const Ipp16s* pSrc, _Out_cap_(len) Ipp16s* pDst, size_t len) { VERIFY(ippsMove_16s(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsMove(_In_count_(len) const Ipp16sc* pSrc, _Out_cap_(len) Ipp16sc* pDst, size_t len) { VERIFY(ippsMove_16sc(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsMove(_In_count_(len) const Ipp32s* pSrc, _Out_cap_(len) Ipp32s* pDst, size_t len) { VERIFY(ippsMove_32s(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsMove(_In_count_(len) const Ipp32sc* pSrc, _Out_cap_(len) Ipp32sc* pDst, size_t len) { VERIFY(ippsMove_32sc(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsMove(_In_count_(len) const Ipp64s* pSrc, _Out_cap_(len) Ipp64s* pDst, size_t len) { VERIFY(ippsMove_64s(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsMove(_In_count_(len) const Ipp64sc* pSrc, _Out_cap_(len) Ipp64sc* pDst, size_t len) { VERIFY(ippsMove_64sc(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsMove(_In_count_(len) const Ipp32f* pSrc, _Out_cap_(len) Ipp32f* pDst, size_t len) { VERIFY(ippsMove_32f(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsMove(_In_count_(len) const Ipp32fc* pSrc, _Out_cap_(len) Ipp32fc* pDst, size_t len) { VERIFY(ippsMove_32fc(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsMove(_In_count_(len) const Ipp64f* pSrc, _Out_cap_(len) Ipp64f* pDst, size_t len) { VERIFY(ippsMove_64f(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline void ippsMove(_In_count_(len) const Ipp64fc* pSrc, _Out_cap_(len) Ipp64fc* pDst, size_t len) { VERIFY(ippsMove_64fc(pSrc, pDst, int(len)) == ippStsNoErr); return; }
inline IppStatus ippsMul_I(_In_count_(len) const Ipp16s* pSrc, _Inout_count_(len) Ipp16s* pSrcDst, size_t len) { return ippsMul_16s_ISfs(pSrc, pSrcDst, int(len), 0); }
inline IppStatus ippsMul_I(_In_count_(len) const Ipp16s* pSrc, _Inout_count_(len) Ipp16sc* pSrcDst, size_t len)
{
	auto temp = (len > Ipp<void>::SMALL_VECTOR ? ippsMalloc_16sc(int(len)) : CPoolAllocator<Ipp16sc, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Allocate(len * sizeof(Ipp16sc)));
	ippsRealToCplx_16s(pSrc, pSrc, temp, int(len));
	auto status = ippsMul_16s_ISfs(reinterpret_cast<Ipp16s*>(temp), reinterpret_cast<Ipp16s*>(pSrcDst), int(2 * len), 0);
	len > Ipp<void>::SMALL_VECTOR ? ippsFree(temp) : CPoolAllocator<Ipp16sc, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Deallocate(temp, len * sizeof(Ipp16sc));
	return status;
}
inline IppStatus ippsMul_I(_In_count_(len) const Ipp16sc* pSrc, _Inout_count_(len) Ipp16sc* pSrcDst, size_t len) { return ippsMul_16sc_ISfs(pSrc, pSrcDst, int(len), 0); }
inline IppStatus ippsMul_I(_In_count_(len) const Ipp32s* pSrc, _Inout_count_(len) Ipp32s* pSrcDst, size_t len) { return ippsMul_32s_ISfs(pSrc, pSrcDst, int(len), 0); }
inline IppStatus ippsMul_I(_In_count_(len) const Ipp32s* pSrc, _Inout_count_(len) Ipp32sc* pSrcDst, size_t len)
{
	auto temp = (len > Ipp<void>::SMALL_VECTOR ? ippsMalloc_32sc(int(len)) : CPoolAllocator<Ipp32sc, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Allocate(len * sizeof(Ipp32sc)));
	ippsRealToCplx_32f(reinterpret_cast<const Ipp32f*>(pSrc), reinterpret_cast<const Ipp32f*>(pSrc), reinterpret_cast<Ipp32fc*>(temp), int(len));
	auto status = ippsMul_32s_ISfs(reinterpret_cast<Ipp32s*>(temp), reinterpret_cast<Ipp32s*>(pSrcDst), int(2 * len), 0);
	len > Ipp<void>::SMALL_VECTOR ? ippsFree(temp) : CPoolAllocator<Ipp32sc, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Deallocate(temp, len * sizeof(Ipp32sc));
	return status;
}
inline IppStatus ippsMul_I(_In_count_(len) const Ipp32sc* pSrc, _Inout_count_(len) Ipp32sc* pSrcDst, size_t len) { return ippsMul_32sc_ISfs(pSrc, pSrcDst, int(len), 0); }
inline IppStatus ippsMul_I(_In_count_(len) const Ipp32f* pSrc, _Inout_count_(len) Ipp32f* pSrcDst, size_t len) { return ippsMul_32f_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsMul_I(_In_count_(len) const Ipp32f* pSrc, _Inout_count_(len) Ipp32fc* pSrcDst, size_t len)
{
	auto temp = (len > Ipp<void>::SMALL_VECTOR ? ippsMalloc_32fc(int(len)) : CPoolAllocator<Ipp32fc, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Allocate(len * sizeof(Ipp32fc)));
	ippsRealToCplx_32f(pSrc, pSrc, temp, int(len));
	auto status = ippsMul_32f_I(reinterpret_cast<Ipp32f*>(temp), reinterpret_cast<Ipp32f*>(pSrcDst), int(2 * len));
	len > Ipp<void>::SMALL_VECTOR ? ippsFree(temp) : CPoolAllocator<Ipp32fc, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Deallocate(temp, len * sizeof(Ipp32fc));
	return status;
}
inline IppStatus ippsMul_I(_In_count_(len) const Ipp32fc* pSrc, _Inout_count_(len) Ipp32fc* pSrcDst, size_t len) { return ippsMul_32fc_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsMul_I(_In_count_(len) const Ipp64f* pSrc, _Inout_count_(len) Ipp64f* pSrcDst, size_t len) { return ippsMul_64f_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsMul_I(_In_count_(len) const Ipp64f* pSrc, _Inout_count_(len) Ipp64fc* pSrcDst, size_t len)
{
	auto temp = (len > Ipp<void>::SMALL_VECTOR ? ippsMalloc_64fc(int(len)) : CPoolAllocator<Ipp64fc, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Allocate(len * sizeof(Ipp64fc)));
	ippsRealToCplx_64f(pSrc, pSrc, temp, int(len));
	auto status = ippsMul_64f_I(reinterpret_cast<Ipp64f*>(temp), reinterpret_cast<Ipp64f*>(pSrcDst), int(2 * len));
	len > Ipp<void>::SMALL_VECTOR ? ippsFree(temp) : CPoolAllocator<Ipp64fc, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Deallocate(temp, len * sizeof(Ipp64fc));
	return status;
}
inline IppStatus ippsMul_I(_In_count_(len) const Ipp64fc* pSrc, _Inout_count_(len) Ipp64fc* pSrcDst, size_t len) { return ippsMul_64fc_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsMulC_I(Ipp16s val, _Inout_count_(len) Ipp16s* pSrcDst, size_t len) { return ippsMulC_16s_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsMulC_I(Ipp16s val, _Inout_count_(len) Ipp16sc* pSrcDst, size_t len) { return ippsMulC_16s_ISfs(val, reinterpret_cast<Ipp16s*>(pSrcDst), int(2 * len), 0); }
inline IppStatus ippsMulC_I(Ipp16sc val, _Inout_count_(len) Ipp16sc* pSrcDst, size_t len) { return ippsMulC_16sc_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsMulC_I(Ipp32s val, _Inout_count_(len) Ipp32s* pSrcDst, size_t len) { return ippsMulC_32s_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsMulC_I(Ipp32s val, _Inout_count_(len) Ipp32sc* pSrcDst, size_t len) { return ippsMulC_32s_ISfs(val, reinterpret_cast<Ipp32s*>(pSrcDst), int(2 * len), 0); }
inline IppStatus ippsMulC_I(Ipp32sc val, _Inout_count_(len) Ipp32sc* pSrcDst, size_t len) { return ippsMulC_32sc_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsMulC_I(Ipp64s val, _Inout_count_(len) Ipp64s* pSrcDst, size_t len) { return ippsMulC_64s_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsMulC_I(Ipp32f val, _Inout_count_(len) Ipp32f* pSrcDst, size_t len) { return ippsMulC_32f_I(val, pSrcDst, int(len)); }
inline IppStatus ippsMulC_I(Ipp32f val, _Inout_count_(len) Ipp32fc* pSrcDst, size_t len) { return ippsMulC_32f_I(val, reinterpret_cast<Ipp32f*>(pSrcDst), int(2 * len)); }
inline IppStatus ippsMulC_I(Ipp32fc val, _Inout_count_(len) Ipp32fc* pSrcDst, size_t len) { return ippsMulC_32fc_I(val, pSrcDst, int(len)); }
inline IppStatus ippsMulC_I(Ipp64f val, _Inout_count_(len) Ipp64f* pSrcDst, size_t len) { return ippsMulC_64f_I(val, pSrcDst, int(len)); }
inline IppStatus ippsMulC_I(Ipp64f val, _Inout_count_(len) Ipp64fc* pSrcDst, size_t len) { return ippsMulC_64f_I(val, reinterpret_cast<Ipp64f*>(pSrcDst), int(2 * len)); }
inline IppStatus ippsMulC_I(Ipp64fc val, _Inout_count_(len) Ipp64fc* pSrcDst, size_t len) { return ippsMulC_64fc_I(val, pSrcDst, int(len)); }
inline IppStatus ippsSet(Ipp8u val, _Out_cap_(len) Ipp8u* pDst, size_t len) { return ippsSet_8u(val, pDst, int(len)); }
inline IppStatus ippsSet(Ipp16s val, _Out_cap_(len) Ipp16s* pDst, size_t len) { return ippsSet_16s(val, pDst, int(len)); }
inline IppStatus ippsSet(Ipp16sc val, _Out_cap_(len) Ipp16sc* pDst, size_t len) { return ippsSet_16sc(val, pDst, int(len)); }
inline IppStatus ippsSet(Ipp32s val, _Out_cap_(len) Ipp32s* pDst, size_t len) { return ippsSet_32s(val, pDst, int(len)); }
inline IppStatus ippsSet(Ipp32sc val, _Out_cap_(len) Ipp32sc* pDst, size_t len) { return ippsSet_32sc(val, pDst, int(len)); }
inline IppStatus ippsSet(Ipp64s val, _Out_cap_(len) Ipp64s* pDst, size_t len) { return ippsSet_64s(val, pDst, int(len)); }
inline IppStatus ippsSet(Ipp64sc val, _Out_cap_(len) Ipp64sc* pDst, size_t len) { return ippsSet_64sc(val, pDst, int(len)); }
inline IppStatus ippsSet(Ipp32f val, _Out_cap_(len) Ipp32f* pDst, size_t len) { return ippsSet_32f(val, pDst, int(len)); }
inline IppStatus ippsSet(Ipp32fc val, _Out_cap_(len) Ipp32fc* pDst, size_t len) { return ippsSet_32fc(val, pDst, int(len)); }
inline IppStatus ippsSet(Ipp64f val, _Out_cap_(len) Ipp64f* pDst, size_t len) { return ippsSet_64f(val, pDst, int(len)); }
inline IppStatus ippsSet(Ipp64fc val, _Out_cap_(len) Ipp64fc* pDst, size_t len) { return ippsSet_64fc(val, pDst, int(len)); }
inline IppStatus ippsSortAscend_I(_Inout_count_(len) Ipp8u* pSrcDst, size_t len) { return ippsSortAscend_8u_I(pSrcDst, int(len)); }
inline IppStatus ippsSortAscend_I(_Inout_count_(len) Ipp16s* pSrcDst, size_t len) { return ippsSortAscend_16s_I(pSrcDst, int(len)); }
inline IppStatus ippsSortAscend_I(_Inout_count_(len) Ipp32s* pSrcDst, size_t len) { return ippsSortAscend_32s_I(pSrcDst, int(len)); }
inline IppStatus ippsSortAscend_I(_Inout_count_(len) Ipp32f* pSrcDst, size_t len) { return ippsSortAscend_32f_I(pSrcDst, int(len)); }
inline IppStatus ippsSortAscend_I(_Inout_count_(len) Ipp64f* pSrcDst, size_t len) { return ippsSortAscend_64f_I(pSrcDst, int(len)); }
inline void ippsStdDev(_In_count_(len) const Ipp16s* pSrc, size_t len, _Out_ Ipp16s* pStdDev) { VERIFY(ippsStdDev_16s_Sfs(pSrc, int(len), pStdDev, 0) == ippStsNoErr); return; }
inline void ippsStdDev(_In_count_(len) const Ipp32f* pSrc, size_t len, _Out_ Ipp32f* pStdDev) { VERIFY(ippsStdDev_32f(pSrc, int(len), pStdDev, ippAlgHintAccurate) == ippStsNoErr); return; }
inline void ippsStdDev(_In_count_(len) const Ipp64f* pSrc, size_t len, _Out_ Ipp64f* pStdDev) { VERIFY(ippsStdDev_64f(pSrc, int(len), pStdDev) == ippStsNoErr); return; }
inline IppStatus ippsSub_I(_In_count_(len) const Ipp16s* pSrc, _Inout_count_(len) Ipp16s* pSrcDst, size_t len) { return ippsSub_16s_ISfs(pSrc, pSrcDst, int(len), 0); }
inline IppStatus ippsSub_I(_In_count_(len) const Ipp16sc* pSrc, _Inout_count_(len) Ipp16sc* pSrcDst, size_t len) { return ippsSub_16sc_ISfs(pSrc, pSrcDst, int(len), 0); }
inline IppStatus ippsSub_I(_In_count_(len) const Ipp32s* pSrc, _Inout_count_(len) Ipp32s* pSrcDst, size_t len) { return ippsSub_32s_ISfs(pSrc, pSrcDst, int(len), 0); }
inline IppStatus ippsSub_I(_In_count_(len) const Ipp32sc* pSrc, _Inout_count_(len) Ipp32sc* pSrcDst, size_t len) { return ippsSub_32sc_ISfs(pSrc, pSrcDst, int(len), 0); }
inline IppStatus ippsSub_I(_In_count_(len) const Ipp32f* pSrc, _Inout_count_(len) Ipp32f* pSrcDst, size_t len) { return ippsSub_32f_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsSub_I(_In_count_(len) const Ipp32fc* pSrc, _Inout_count_(len) Ipp32fc* pSrcDst, size_t len) { return ippsSub_32fc_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsSub_I(_In_count_(len) const Ipp64f* pSrc, _Inout_count_(len) Ipp64f* pSrcDst, size_t len) { return ippsSub_64f_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsSub_I(_In_count_(len) const Ipp64fc* pSrc, _Inout_count_(len) Ipp64fc* pSrcDst, size_t len) { return ippsSub_64fc_I(pSrc, pSrcDst, int(len)); }
inline IppStatus ippsSubC_I(Ipp16s val, _Inout_count_(len) Ipp16s* pSrcDst, size_t len) { return ippsSubC_16s_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsSubC_I(Ipp16sc val, _Inout_count_(len) Ipp16sc* pSrcDst, size_t len) { return ippsSubC_16sc_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsSubC_I(Ipp32s val, _Inout_count_(len) Ipp32s* pSrcDst, size_t len) { return ippsSubC_32s_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsSubC_I(Ipp32sc val, _Inout_count_(len) Ipp32sc* pSrcDst, size_t len) { return ippsSubC_32sc_ISfs(val, pSrcDst, int(len), 0); }
inline IppStatus ippsSubC_I(Ipp32f val, _Inout_count_(len) Ipp32f* pSrcDst, size_t len) { return ippsSubC_32f_I(val, pSrcDst, int(len)); }
inline IppStatus ippsSubC_I(Ipp32fc val, _Inout_count_(len) Ipp32fc* pSrcDst, size_t len) { return ippsSubC_32fc_I(val, pSrcDst, int(len)); }
inline IppStatus ippsSubC_I(Ipp64f val, _Inout_count_(len) Ipp64f* pSrcDst, size_t len) { return ippsSubC_64f_I(val, pSrcDst, int(len)); }
inline IppStatus ippsSubC_I(Ipp64fc val, _Inout_count_(len) Ipp64fc* pSrcDst, size_t len) { return ippsSubC_64fc_I(val, pSrcDst, int(len)); }
inline IppStatus ippsSum(_In_count_(len) const Ipp16s* pSrc, size_t len, _Out_ Ipp16s* pSum) { return ippsSum_16s_Sfs(pSrc, int(len), pSum, 0); }
inline IppStatus ippsSum(_In_count_(len) const Ipp16sc* pSrc, size_t len, _Out_ Ipp16sc* pSum) { return ippsSum_16sc_Sfs(pSrc, int(len), pSum, 0); }
inline IppStatus ippsSum(_In_count_(len) const Ipp32s* pSrc, size_t len, _Out_ Ipp32s* pSum) { return ippsSum_32s_Sfs(pSrc, int(len), pSum, 0); }
inline IppStatus ippsSum(_In_count_(len) const Ipp32f* pSrc, size_t len, _Out_ Ipp32f* pSum) { return ippsSum_32f(pSrc, int(len), pSum, ippAlgHintAccurate); }
inline IppStatus ippsSum(_In_count_(len) const Ipp32fc* pSrc, size_t len, _Out_ Ipp32fc* pSum) { return ippsSum_32fc(pSrc, int(len), pSum, ippAlgHintAccurate); }
inline IppStatus ippsSum(_In_count_(len) const Ipp64f* pSrc, size_t len, _Out_ Ipp64f* pSum) { return ippsSum_64f(pSrc, int(len), pSum); }
inline IppStatus ippsSum(_In_count_(len) const Ipp64fc* pSrc, size_t len, _Out_ Ipp64fc* pSum) { return ippsSum_64fc(pSrc, int(len), pSum); }


////////////////////////////////////////////////////////////////////////////////
//
// Templates to encapsulate Intel IPP vectors and operations
//

// Specializations of generic functions and typedefs
template<> struct Ipp<Ipp8u>
{
	_Ret_notnull_ _Post_writable_size_(size) static Ipp8u* ippsMalloc(size_t size)
	{
		if(auto buffer = ippsMalloc_8u(int(size)))
		{
			return buffer;
		}

		ASSERT(FALSE);
		throw std::bad_alloc();
	}
};

template<> struct Ipp<Ipp16s>
{
	_Ret_notnull_ _Post_writable_size_(size) static Ipp16s* ippsMalloc(size_t size)
	{
		if(auto buffer = ippsMalloc_16s(int(size)))
		{
			return buffer;
		}

		ASSERT(FALSE);
		throw std::bad_alloc();
	}

	_Ret_notnull_ _Post_writable_size_(size) static Ipp16sc* ippsMallocCmplx(size_t size)
	{
		if(auto buffer = ippsMalloc_16sc(int(size)))
		{
			return buffer;
		}

		ASSERT(FALSE);
		throw std::bad_alloc();
	}

	typedef Ipp16sc IppCmplx;
	typedef IppsFIRState32s_16s IppsFIRState;
};

template<> struct Ipp<Ipp32s>
{
	_Ret_notnull_ _Post_writable_size_(size) static Ipp32s* ippsMalloc(size_t size)
	{
		if(auto buffer = ippsMalloc_32s(int(size)))
		{
			return buffer;
		}

		ASSERT(FALSE);
		throw std::bad_alloc();
	}

	_Ret_notnull_ _Post_writable_size_(size) static Ipp32sc* ippsMallocCmplx(size_t size)
	{
		if(auto buffer = ippsMalloc_32sc(int(size)))
		{
			return buffer;
		}

		ASSERT(FALSE);
		throw std::bad_alloc();
	}

	typedef Ipp32sc IppCmplx;
	typedef IppsFIRState_32s IppsFIRState;
};

template<> struct Ipp<Ipp64s>
{
	_Ret_notnull_ _Post_writable_size_(size) static Ipp64s* ippsMalloc(size_t size)
	{
		if(auto buffer = ippsMalloc_64s(int(size)))
		{
			return buffer;
		}

		ASSERT(FALSE);
		throw std::bad_alloc();
	}

	_Ret_notnull_ _Post_writable_size_(size) static Ipp64sc* ippsMallocCmplx(size_t size)
	{
		if(auto buffer = ippsMalloc_64sc(int(size)))
		{
			return buffer;
		}

		ASSERT(FALSE);
		throw std::bad_alloc();
	}

	typedef Ipp64sc IppCmplx;
};

template<> struct Ipp<Ipp32f>
{
	_Ret_notnull_ _Post_writable_size_(size) static Ipp32f* ippsMalloc(size_t size)
	{
		if(auto buffer = ippsMalloc_32f(int(size)))
		{
			return buffer;
		}

		ASSERT(FALSE);
		throw std::bad_alloc();
	}

	_Ret_notnull_ _Post_writable_size_(size) static Ipp32fc* ippsMallocCmplx(size_t size)
	{
		if(auto buffer = ippsMalloc_32fc(int(size)))
		{
			return buffer;
		}

		ASSERT(FALSE);
		throw std::bad_alloc();
	}

	typedef Ipp32fc IppCmplx;
	typedef IppsDFTSpec_C_32fc IppsDFTSpec_C;
	typedef IppsDFTSpec_R_32f IppsDFTSpec_R;
	typedef IppsFFTSpec_C_32fc IppsFFTSpec_C;
	typedef IppsFFTSpec_R_32f IppsFFTSpec_R;
};

template<> struct Ipp<Ipp64f>
{
	_Ret_notnull_ _Post_writable_size_(size)static Ipp64f* ippsMalloc(size_t size)
	{
		if(auto buffer = ippsMalloc_64f(int(size)))
		{
			return buffer;
		}

		ASSERT(FALSE);
		throw std::bad_alloc();
	}

	_Ret_notnull_ _Post_writable_size_(size) static Ipp64fc* ippsMallocCmplx(size_t size)
	{
		if(auto buffer = ippsMalloc_64fc(int(size)))
		{
			return buffer;
		}

		ASSERT(FALSE);
		throw std::bad_alloc();
	}

	typedef Ipp64fc IppCmplx;
	typedef IppsDFTSpec_C_64fc IppsDFTSpec_C;
	typedef IppsDFTSpec_R_64f IppsDFTSpec_R;
	typedef IppsFFTSpec_C_64fc IppsFFTSpec_C;
	typedef IppsFFTSpec_R_64f IppsFFTSpec_R;
};

// Real vectors
template<typename T> class IppVec
{
	template<typename U> friend class IppVec;
	template<typename U> friend class IppCmplxVec;

	friend void swap(_Inout_ IppVec& a, _Inout_ IppVec& b)
	{
		using std::swap;
		swap(a.m_reserved, b.m_reserved); // Bogus intelliSense errors
		swap(a.m_size, b.m_size);
		swap(a.m_vector, b.m_vector);
	}
#if defined(_DEBUG)
	inline _When_(size != 0, _Ret_notnull_ _Post_writable_size_(size)) _When_(size == 0, _Ret_null_) static T* Allocate(size_t size, bool chkDeAlloc)
	{
		T* ptr = Allocate(size);
		CPoolAllocator<T, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::SetDeAllocVal(chkDeAlloc);
		return(ptr);
	}
#endif
	inline _When_(size != 0, _Ret_notnull_ _Post_writable_size_(size)) _When_(size == 0, _Ret_null_) static T* Allocate(size_t size)
	{ return size == 0 ? nullptr : size > Ipp<void>::SMALL_VECTOR ? Ipp<T>::ippsMalloc(size) :
		CPoolAllocator<T, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Allocate(Ipp<void>::SMALL_VECTOR * sizeof(T)); }

	inline static void Deallocate(_Pre_readable_size_(size) _Post_ptr_invalid_ T* vector, size_t size)
	{ if(size > Ipp<void>::SMALL_VECTOR) ippsFree(vector);
		else if(size) CPoolAllocator<T, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Deallocate(vector, Ipp<void>::SMALL_VECTOR * sizeof(T)); }

public:
	IppVec(void)
	: m_reserved(0), m_size(0), m_vector(nullptr)
	{}

	explicit IppVec(size_t size)
	: m_reserved(size ? std::max(size, Ipp<void>::SMALL_VECTOR) : 0), m_size(size), m_vector(Allocate(size))
	{
		ASSERT(m_size == 0 || m_vector);
	}

	IppVec(size_t size, T val)
	: m_reserved(size ? std::max(size, Ipp<void>::SMALL_VECTOR) : 0), m_size(size), m_vector(Allocate(size))
	{
		if(m_size)
		{
			ASSERT(m_vector);
#pragma warning(suppress : 6387) // m_vector cannot be nullptr
			ippsSet(val, m_vector, m_size);
		}
	}

	IppVec(_In_count_(size) const T* v, size_t size)
	: m_reserved(size ? std::max(size, Ipp<void>::SMALL_VECTOR) : 0), m_size(size), m_vector(Allocate(size))
	{
		if(m_size)
		{
			ASSERT(m_vector);
#pragma warning(suppress : 6387) // m_vector cannot be nullptr
			ippsCopy(v, m_vector, m_size);
		}
	}

	IppVec(_In_ const IppVec& v)
		: m_reserved(v.m_size ? std::max(v.m_size, Ipp<void>::SMALL_VECTOR) : 0), m_size(v.m_size), m_vector(Allocate(v.m_size))
	{
		if(m_size)
		{
			ASSERT(m_vector);
#pragma warning(suppress : 6387) // m_vector cannot be nullptr
			ippsCopy(v.m_vector, m_vector, m_size);
		}
	}

	IppVec(_Inout_ IppVec&& v)
	: m_reserved(v.m_reserved), m_size(v.m_size), m_vector(v.m_vector)
	{
		v.m_reserved = 0;
		v.m_size = 0;
		v.m_vector = nullptr;
	}

	template<typename U> IppVec(_In_ const IppVec<U>& v)
	: m_reserved(v.m_size ? std::max(v.m_size, Ipp<void>::SMALL_VECTOR) : 0), m_size(v.m_size), m_vector(Allocate(v.m_size))
	{
		if(m_size)
		{
			ASSERT(m_vector);
#pragma warning(suppress : 6387) // m_vector cannot be nullptr
			ippsConvert(&v[0], m_vector, m_size);
		}
	}

	~IppVec(void)
	{
		Deallocate(m_vector, m_reserved);
	}

	_Ret_ IppVec& operator=(_In_ const IppVec& v)
	{
		if(&v != this)
		{
			m_size = v.m_size;

			if(m_size > m_reserved)
			{
				Deallocate(m_vector, m_reserved);
				m_reserved = std::max(m_size, Ipp<void>::SMALL_VECTOR);
				m_vector = Allocate(m_reserved);
			}

			if(m_size != 0)
			{
#pragma warning(suppress : 6387) // m_vector cannot be nullptr
				ippsCopy(v.m_vector, m_vector, m_size);
			}
		}

		return *this;
	}

	_Ret_ IppVec& operator=(_Inout_ IppVec&& v)
	{
		if(&v != this)
		{
			Deallocate(m_vector, m_reserved);
			m_reserved = v.m_reserved;
			m_size = v.m_size;
			m_vector = v.m_vector;
			v.m_reserved = 0;
			v.m_size = 0;
			v.m_vector = nullptr;
		}

		return *this;
	}

	template<typename U> _Ret_ IppVec& operator=(_In_ const IppVec<U>& v)
	{
		if(v.size() != m_size)
		{
			m_size = v.size();

			if(m_size > m_reserved)
			{
				Deallocate(m_vector, m_reserved);
				m_reserved = std::max(m_size, Ipp<void>::SMALL_VECTOR);
				m_vector = Allocate(m_reserved);
			}
		}

		if(m_size != 0)
		{
#pragma warning(suppress : 6387) // m_vector cannot be nullptr
			ippsConvert(&v[0], m_vector, m_size);
		}

		return *this;
	}

	_Ret_ IppVec& operator=(T val)
	{
		if(m_size > 0)
		{
#pragma warning(suppress : 6387) // m_vector cannot be nullptr
			VERIFY(ippsSet(val, m_vector, m_size) == ippStsNoErr);
		}

		return *this;
	}

	_Ret_ T& operator[](size_t i)
	{
		ASSERT(i < m_size);
		return m_vector[i];
	}

	_Ret_ const T& operator[](size_t i) const
	{
		ASSERT(i < m_size);
		return m_vector[i];
	}

	_Ret_ IppVec& operator +=(_In_ const IppVec& v)
	{
		ASSERT(v.m_size == m_size);
		VERIFY(ippsAdd_I(v.m_vector, m_vector, m_size) == ippStsNoErr);
		return *this;
	}

	_Ret_ IppVec& operator +=(T val)
	{
		VERIFY(ippsAddC_I(val, m_vector, m_size) == ippStsNoErr);
		return *this;
	}

	_Ret_ IppVec& operator -=(_In_ const IppVec& v)
	{
		ASSERT(v.m_size == m_size);
		VERIFY(ippsSub_I(v.m_vector, m_vector, m_size) == ippStsNoErr);
		return *this;
	}

	_Ret_ IppVec& operator -=(T val)
	{
		VERIFY(ippsSubC_I(val, m_vector, m_size) == ippStsNoErr);
		return *this;
	}

	_Ret_ IppVec& operator *=(_In_ const IppVec& v)
	{
		ASSERT(v.m_size == m_size);
		VERIFY(ippsMul_I(v.m_vector, m_vector, m_size) == ippStsNoErr);
		return *this;
	}

	_Ret_ IppVec& operator *=(T val)
	{
		VERIFY(ippsMulC_I(val, m_vector, m_size) == ippStsNoErr);
		return *this;
	}

	_Ret_ IppVec& operator /=(_In_ const IppVec& v)
	{
		ASSERT(v.m_size == m_size);
#ifdef _DEBUG
		auto status =
#endif
			ippsDiv_I(v.m_vector, m_vector, m_size);
		ASSERT(status == ippStsNoErr || status == ippStsDivByZero || status == ippStsDivByZeroErr);
		return *this;
	}

	_Ret_ IppVec& operator /=(T val)
	{
#ifdef _DEBUG
		auto status =
#endif
			ippsDivC_I(val, m_vector, m_size);
		ASSERT(status == ippStsNoErr || status == ippStsDivByZero || status == ippStsDivByZeroErr);
		return *this;
	}

	_Ret_ IppVec& operator <<=(int rot)
	{
		if(m_size > 0)
		{
			rot %= m_size;

			if(rot > int(m_size / 2))
			{
				*this >>= -rot;
			}
			else if(rot != 0)
			{
				reserve(m_size + rot);
				ippsCopy(m_vector, &m_vector[m_size], rot);
				ippsMove(&m_vector[rot], m_vector, m_size);
			}
		}

		return *this;
	}

	_Ret_ IppVec& operator >>=(int rot)
	{
		if(m_size > 0)
		{
			rot %= m_size;

			if(rot > int(m_size / 2))
			{
				*this <<= -rot;
			}
			else if(rot != 0)
			{
				reserve(m_size + rot);
				ippsMove(m_vector, &m_vector[rot], m_size);
				ippsCopy(&m_vector[m_size], m_vector, rot);
			}
		}

		return *this;
	}

	_Ret_ IppVec& append(_In_ const IppVec& v)
	{
		if(v.m_size > 0)
		{
			auto oldSize = m_size;
			resize(m_size + v.m_size);
			ippsCopy(v.m_vector, &m_vector[oldSize], v.m_size);
		}

		return *this;
	}

	void assign(size_t size, T val)
	{
		if(size > m_reserved)
		{
			Deallocate(m_vector, m_reserved);
			m_reserved = std::max(size, Ipp<void>::SMALL_VECTOR);
			m_vector = Allocate(m_reserved);
		}
		
		m_size = size;

		if(m_size > 0)
		{
#pragma warning(suppress : 6387) // m_vector cannot be nullptr
			VERIFY(ippsSet(val, m_vector, m_size) == ippStsNoErr);
		}

		return;
	}

	size_t capacity(void) const
	{
		return m_reserve;
	}

	T maximum(void) const
	{
		T max;
		VERIFY(ippsMax(m_vector, m_size, &max) == ippStsNoErr);
		return max;
	}

	T mean(void) const
	{
		T mean;
		ippsMean(m_vector, m_size, &mean);
		return mean;
	}

	T minimum(void) const
	{
		T min;
		VERIFY(ippsMin(m_vector, m_size, &min) == ippStsNoErr);
		return min;
	}

	void push_back(T val)
	{
		if(m_reserved == 0)
		{
			reserve(Ipp<void>::SMALL_VECTOR);
		}
		else if(m_reserved == m_size)
		{
			reserve(3 * m_size / 2);
		}

		m_vector[m_size++] = val;
	}

	void reserve(size_t size)
	{
		if(size > m_reserved)
		{
			auto newVector = Allocate(size);

			if(m_size != 0)
			{
#pragma warning(suppress : 6387) // newVector cannot be nullptr
				ippsCopy(m_vector, newVector, m_size);
			}

			Deallocate(m_vector, m_reserved);
			m_reserved = std::max(size, Ipp<void>::SMALL_VECTOR);
			m_vector = newVector;
		}
	}

	void resize(size_t size)
	{
		if(size > m_reserved)
		{
			reserve(size);
		}

		m_size = size;
	}

	void reverse(void)
	{
		if (m_size > 1)
		{
			size_t lastIdx = m_size - 1;
			size_t loopCnt = m_size / 2;

			for (size_t idx = 0; idx < loopCnt; ++idx, --lastIdx)
			{
				T val = m_vector[idx];
				m_vector[idx] = m_vector[lastIdx];
				m_vector[lastIdx] = val;
			}
		}

		return;
	}

	size_t size(void) const
	{
		return m_size;
	}

	T stdDev(void) const
	{
		T stdDev;
		ippsStdDev(m_vector, m_size, &stdDev);
		return stdDev;
	}

	T sum(void) const
	{
		T sum;
		VERIFY(ippsSum(m_vector, m_size, &sum) == ippStsNoErr);
		return sum;
	}

private:
	size_t m_reserved;
	size_t m_size;
	T* m_vector;
};

// Shorthand
typedef IppVec<Ipp8u> Ipp8uVec;
typedef IppVec<Ipp16s> Ipp16sVec;
typedef IppVec<Ipp32s> Ipp32sVec;
typedef IppVec<Ipp64s> Ipp64sVec;
typedef IppVec<Ipp32f> Ipp32fVec;
typedef IppVec<Ipp64f> Ipp64fVec;

// Complex vectors
template<typename T> class IppCmplxVec
{
	template<typename U> friend class IppCmplxVec;

	friend void swap(_Inout_ IppCmplxVec& a, _Inout_ IppCmplxVec& b)
	{
		using std::swap;
		swap(a.m_reserved, b.m_reserved); // Bogus intelliSense errors
		swap(a.m_size, b.m_size);
		swap(a.m_vector, b.m_vector);
	}

public:
	static const typename Ipp<T>::IppCmplx ONE;
	static const typename Ipp<T>::IppCmplx ZERO;

	inline _When_(size != 0, _Ret_notnull_ _Post_writable_size_(size)) _When_(size == 0, _Ret_null_) static typename Ipp<T>::IppCmplx* Allocate(size_t size)
	{ return size == 0 ? nullptr : size > Ipp<void>::SMALL_VECTOR ? Ipp<T>::ippsMallocCmplx(size) :
		CPoolAllocator<Ipp<T>::IppCmplx, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Allocate(Ipp<void>::SMALL_VECTOR * sizeof(Ipp<T>::IppCmplx)); }

#if defined(_DEBUG)
	inline _When_(size != 0, _Ret_notnull_ _Post_writable_size_(size)) _When_(size == 0, _Ret_null_) static typename Ipp<T>::IppCmplx* Allocate(size_t size, bool chkDeAlloc)
	{
		return size == 0 ? nullptr : size > Ipp<void>::SMALL_VECTOR ? Ipp<T>::ippsMallocCmplx(size) :
			CPoolAllocator<Ipp<T>::IppCmplx, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Allocate(Ipp<void>::SMALL_VECTOR * sizeof(Ipp<T>::IppCmplx), chkDeAlloc);
	}
#endif

	inline static void Deallocate(_Pre_readable_size_(size) _Post_ptr_invalid_ typename Ipp<T>::IppCmplx* vector, size_t size)
	{ if(size > Ipp<void>::SMALL_VECTOR) ippsFree(vector);
		else if(size) CPoolAllocator<Ipp<T>::IppCmplx, Ipp<void>::SMALL_VECTOR, Ipp<void>::IPP_ALIGN>::Deallocate(vector, Ipp<void>::SMALL_VECTOR * sizeof(Ipp<T>::IppCmplx)); }

	IppCmplxVec(void)
	: m_reserved(0), m_size(0), m_vector(nullptr)
	{}

	explicit IppCmplxVec(size_t size)
	: m_reserved(size ? std::max(size, Ipp<void>::SMALL_VECTOR) : 0), m_size(size), m_vector(Allocate(size))
	{
		ASSERT(m_size == 0 || m_vector);
	}

#if defined(_DEBUG)
	explicit IppCmplxVec(size_t size, bool chkDeAlloc)
		: m_reserved(size ? std::max(size, Ipp<void>::SMALL_VECTOR) : 0), m_size(size), m_vector(Allocate(size, chkDeAlloc))
	{
		ASSERT(m_size == 0 || m_vector);
	}
#endif

	IppCmplxVec(size_t size, _In_ const typename Ipp<T>::IppCmplx& val)
	: m_reserved(size ? std::max(size, Ipp<void>::SMALL_VECTOR) : 0), m_size(size), m_vector(Allocate(size))
	{
		if(m_size)
		{
			ASSERT(m_vector);
#pragma warning(suppress : 6387) // m_vector cannot be nullptr
			ippsSet(val, m_vector, m_size);
		}
	}

	IppCmplxVec(_In_count_(size) const typename Ipp<T>::IppCmplx* v, size_t size)
	: m_reserved(size ? std::max(size, Ipp<void>::SMALL_VECTOR) : 0), m_size(size), m_vector(Allocate(size))
	{
		if(m_size)
		{
			ASSERT(m_vector);
#pragma warning(suppress : 6387) // m_vector cannot be nullptr
			ippsCopy(v, m_vector, m_size);
		}
	}

	IppCmplxVec(_In_ const IppCmplxVec& v)
	: m_reserved(v.m_size ? std::max(v.m_size, Ipp<void>::SMALL_VECTOR) : 0), m_size(v.m_size), m_vector(Allocate(v.m_size))
	{
		if(m_size)
		{
			ASSERT(m_vector);
#pragma warning(suppress : 6387) // m_vector cannot be nullptr
			ippsCopy(v.m_vector, m_vector, m_size);
		}
	}

	IppCmplxVec(_Inout_ IppCmplxVec&& v)
	: m_reserved(v.m_reserved), m_size(v.m_size), m_vector(v.m_vector)
	{
		v.m_reserved = 0;
		v.m_size = 0;
		v.m_vector = nullptr;
	}

	template<typename U> IppCmplxVec(_In_ const IppCmplxVec<U>& v)
	: m_reserved(v.m_size ? std::max(v.m_size, Ipp<void>::SMALL_VECTOR) : 0), m_size(v.m_size), m_vector(Allocate(v.m_size))
	{
		if(m_size)
		{
			ASSERT(m_vector);
#pragma warning(suppress : 6387) // m_vector cannot be nullptr
			ippsConvert(&v[0], m_vector, m_size);
		}
	}

	~IppCmplxVec(void)
	{
		Deallocate(m_vector, m_reserved);
	}

	_Ret_ IppCmplxVec& operator=(_In_ const IppCmplxVec& v)
	{
		if(&v != this)
		{
			m_size = v.m_size;

			if(m_size > m_reserved)
			{
				Deallocate(m_vector, m_reserved);
				m_reserved = std::max(m_size, Ipp<void>::SMALL_VECTOR);
				m_vector = Allocate(m_reserved);
			}

			if(m_size != 0)
			{
#pragma warning(suppress : 6387) // m_vector cannot be nullptr
				ippsCopy(v.m_vector, m_vector, m_size);
			}
		}

		return *this;
	}

	_Ret_ IppCmplxVec& operator=(_Inout_ IppCmplxVec&& v)
	{
		if(&v != this)
		{
			Deallocate(m_vector, m_reserved);
			m_reserved = v.m_reserved;
			m_size = v.m_size;
			m_vector = v.m_vector;
			v.m_reserved = 0;
			v.m_size = 0;
			v.m_vector = nullptr;
		}

		return *this;
	}

	template<typename U> _Ret_ IppCmplxVec& operator=(_In_ const IppCmplxVec<U>& v)
	{
		if(v.size() != m_size)
		{
			m_size = v.size();

			if(m_size > m_reserved)
			{
				Deallocate(m_vector, m_reserved);
				m_reserved = std::max(m_size, Ipp<void>::SMALL_VECTOR);
				m_vector = Allocate(m_reserved);
			}
		}

		if(m_size != 0)
		{
#pragma warning(suppress : 6387) // m_vector cannot be nullptr
			ippsConvert(&v[0], m_vector, m_size);
		}

		return *this;
	}

	_Ret_ IppCmplxVec& operator=(_In_ const typename Ipp<T>::IppCmplx& val)
	{
		if(m_size > 0)
		{
			VERIFY(ippsSet(val, m_vector, m_size) == ippStsNoErr);
		}

		return *this;
	}

	_Ret_ typename Ipp<T>::IppCmplx& operator[](size_t i)
	{
		ASSERT(i < m_size);
		return m_vector[i];
	}

	_Ret_ const typename Ipp<T>::IppCmplx& operator[](size_t i) const
	{
		ASSERT(i < m_size);
		return m_vector[i];
	}

	_Ret_ IppCmplxVec& operator +=(_In_ const IppCmplxVec& v)
	{
		ASSERT(v.m_size == m_size);
		VERIFY(ippsAdd_I(v.m_vector, m_vector, m_size) == ippStsNoErr);
		return *this;
	}

	_Ret_ IppCmplxVec& operator +=(_In_ const typename Ipp<T>::IppCmplx& val)
	{
		VERIFY(ippsAddC_I(val, m_vector, m_size) == ippStsNoErr);
		return *this;
	}

	_Ret_ IppCmplxVec& operator -=(_In_ const IppCmplxVec& v)
	{
		ASSERT(v.m_size == m_size);
		VERIFY(ippsSub_I(v.m_vector, m_vector, m_size) == ippStsNoErr);
		return *this;
	}

	_Ret_ IppCmplxVec& operator -=(_In_ const typename Ipp<T>::IppCmplx& val)
	{
		VERIFY(ippsSubC_I(val, m_vector, m_size) == ippStsNoErr);
		return *this;
	}

	_Ret_ IppCmplxVec& operator *=(_In_ const IppCmplxVec& v)
	{
		ASSERT(v.m_size == m_size);
		VERIFY(ippsMul_I(v.m_vector, m_vector, m_size) == ippStsNoErr);
		return *this;
	}

	_Ret_ IppCmplxVec& operator *=(_In_ const IppVec<T>& v)
	{
		ASSERT(v.m_size == m_size);
		VERIFY(ippsMul_I(v.m_vector, m_vector, m_size) == ippStsNoErr);
		return *this;
	}

	_Ret_ IppCmplxVec& operator *=(_In_ const typename Ipp<T>::IppCmplx& val)
	{
		VERIFY(ippsMulC_I(val, m_vector, m_size) == ippStsNoErr);
		return *this;
	}

	_Ret_ IppCmplxVec& operator *=(T val)
	{
		VERIFY(ippsMulC_I(val, m_vector, m_size) == ippStsNoErr);
		return *this;
	}

	_Ret_ IppCmplxVec& operator /=(_In_ const IppCmplxVec& v)
	{
		ASSERT(v.m_size == m_size);
#ifdef _DEBUG
		auto status =
#endif
			ippsDiv_I(v.m_vector, m_vector, m_size);
		ASSERT(status == ippStsNoErr || status == ippStsDivByZero || status == ippStsDivByZeroErr);
		return *this;
	}

	_Ret_ IppCmplxVec& operator /=(_In_ const IppVec<T>& v)
	{
		ASSERT(v.m_size == m_size);
#ifdef _DEBUG
		auto status =
#endif
			ippsDiv_I(v.m_vector, m_vector, m_size);
		ASSERT(status == ippStsNoErr || status == ippStsDivByZero || status == ippStsDivByZeroErr);
		return *this;
	}

	_Ret_ IppCmplxVec& operator /=(_In_ const typename Ipp<T>::IppCmplx& val)
	{
#ifdef _DEBUG
		auto status =
#endif
			ippsDivC_I(val, m_vector, m_size);
		ASSERT(status == ippStsNoErr || status == ippStsDivByZero || status == ippStsDivByZeroErr);
		return *this;
	}

	_Ret_ IppCmplxVec& operator /=(T val)
	{
#ifdef _DEBUG
		auto status =
#endif
			ippsDivC_I(val, m_vector, m_size);
		ASSERT(status == ippStsNoErr || status == ippStsDivByZero || status == ippStsDivByZeroErr);
		return *this;
	}

	_Ret_ IppCmplxVec& operator <<=(int rot)
	{
		if(m_size > 0)
		{
			rot %= int(m_size);

			if(rot < 0)
			{
				rot += m_size;
			}

			if(rot > int(m_size / 2))
			{
				*this >>= -rot;
			}
			else if(rot != 0)
			{
				reserve(m_size + rot);
				ippsCopy(m_vector, &m_vector[m_size], rot);
				ippsMove(&m_vector[rot], m_vector, m_size);
			}
		}

		return *this;
	}

	_Ret_ IppCmplxVec& operator >>=(int rot)
	{
		if(m_size > 0)
		{
			rot %= int(m_size);

			if(rot < 0)
			{
				rot += m_size;
			}

			if(rot > int(m_size / 2))
			{
				*this <<= -rot;
			}
			else if(rot != 0)
			{
				reserve(m_size + rot);
				ippsMove(m_vector, &m_vector[rot], m_size);
				ippsCopy(&m_vector[m_size], m_vector, rot);
			}
		}

		return *this;
	}

	_Ret_ IppCmplxVec& append(_In_ const IppCmplxVec& v)
	{
		if(v.m_size > 0)
		{
			auto oldSize = m_size;
			resize(m_size + v.m_size);
			ippsCopy(v.m_vector, &m_vector[oldSize], v.m_size);
		}

		return *this;
	}

	void assign(size_t size, _In_ const typename Ipp<T>::IppCmplx& val)
	{
		if(size > m_reserved)
		{
			Deallocate(m_vector, m_reserved);
			m_reserved = std::max(size, Ipp<void>::SMALL_VECTOR);
			m_vector = Allocate(m_reserved);
		}
		
		m_size = size;

		if(m_size > 0)
		{
#pragma warning(suppress : 6387) // m_vector cannot be nullptr
			VERIFY(ippsSet(val, m_vector, m_size) == ippStsNoErr);
		}

		return;
	}

	size_t capacity(void) const
	{
		return m_reserved;
	}

	typename Ipp<T>::IppCmplx mean(void) const
	{
		typename Ipp<T>::IppCmplx mean;
		ippsMean(m_vector, m_size, &mean);
		return mean;
	}

	void push_back(_In_ const typename Ipp<T>::IppCmplx& val)
	{
		if(m_reserved == 0)
		{
			reserve(Ipp<void>::SMALL_VECTOR);
		}
		else if(m_reserved == m_size)
		{
			reserve(3 * m_size / 2);
		}

		m_vector[m_size++] = val;
	}

	void reserve(size_t size)
	{
		if(size > m_reserved)
		{
			auto newVector = Allocate(size);

			if(m_size != 0)
			{
#pragma warning(suppress : 6387) // newVector cannot be nullptr
				ippsCopy(m_vector, newVector, m_size);
			}

			Deallocate(m_vector, m_reserved);
			m_reserved = std::max(size, Ipp<void>::SMALL_VECTOR);
			m_vector = newVector;
		}
	}

	void resize(size_t size)
	{
		if(size > m_reserved)
		{
			reserve(size);
		}

		m_size = size;
	}

	size_t size(void) const
	{
		return m_size;
	}

	typename Ipp<T>::IppCmplx sum(void) const
	{
		typename Ipp<T>::IppCmplx sum;
		VERIFY(ippsSum(m_vector, m_size, &sum) == ippStsNoErr);
		return sum;
	}

private:
	size_t m_size;
	size_t m_reserved;
	typename Ipp<T>::IppCmplx* m_vector;
};

// Operators for IPP complex types
inline Ipp16sc operator+(_In_ const Ipp16sc& a, _In_ const Ipp16sc& b)
{
	Ipp16sc add = { a.re + b.re, a.im + b.im };
	return add;
}

_Ret_ inline Ipp16sc& operator+=(_Inout_ Ipp16sc& a, _In_ const Ipp16sc& b)
{
	a.re += b.re;
	a.im += b.im;
	return a;
}

inline Ipp16sc operator-(_In_ const Ipp16sc& a, _In_ const Ipp16sc& b)
{
	Ipp16sc subtract = { a.re - b.re, a.im - b.im };
	return subtract;
}

_Ret_ inline Ipp16sc& operator-=(_Inout_ Ipp16sc& a, _In_ const Ipp16sc& b)
{
	a.re -= b.re;
	a.im -= b.im;
	return a;
}

inline Ipp16sc operator*(_In_ const Ipp16sc& a, _In_ const Ipp16sc& b)
{
	Ipp16sc mult = { a.re * b.re - a.im * b.im, a.re * b.im + a.im * b.re };
	return mult;
}

inline Ipp16sc operator*(_In_ const Ipp16sc& a, Ipp16s b)
{
	Ipp16sc mult = { a.re * b, a.im * b };
	return mult;
}

inline Ipp16sc MultByConj(_In_ const Ipp16sc& a, _In_ const Ipp16sc& b)
{
	Ipp16sc mult = { a.re * b.re + a.im * b.im, a.im * b.re - a.re * b.im };
	return mult;
}

inline Ipp16sc operator/(_In_ const Ipp16sc& a, _In_ const Ipp16sc& b)
{
	Ipp16sc div;
	Ipp16s quot = b.re * b.re + b.im * b.im;
	div.re = (a.re * b.re + a.im * b.im) / quot;
	div.im = (a.im * b.re - a.re * b.im) / quot;

	return div;
}

inline Ipp16sc operator/(_In_ const Ipp16sc& a, Ipp16s b)
{
	Ipp16sc div = { a.re / b, a.im / b };
	return div;
}

inline bool operator==(_In_ const Ipp16sc& a, _In_ const Ipp16sc& b)
{
	return a.re == b.re && a.im == b.im;
}

inline bool operator!=(_In_ const Ipp16sc& a, _In_ const Ipp16sc& b)
{
	return a.re != b.re || a.im != b.im;
}

inline Ipp16s MagSq(_In_ const Ipp16sc& a)
{
	return a.re * a.re + a.im * a.im;
}

inline Ipp32sc operator+(_In_ const Ipp32sc& a, _In_ const Ipp32sc& b)
{
	Ipp32sc add = { a.re + b.re, a.im + b.im };
	return add;
}

_Ret_ inline Ipp32sc& operator+=(_Inout_ Ipp32sc& a, _In_ const Ipp32sc& b)
{
	a.re += b.re;
	a.im += b.im;
	return a;
}

inline Ipp32sc operator-(_In_ const Ipp32sc& a, _In_ const Ipp32sc& b)
{
	Ipp32sc subtract = { a.re - b.re, a.im - b.im };
	return subtract;
}

_Ret_ inline Ipp32sc& operator-=(_Inout_ Ipp32sc& a, _In_ const Ipp32sc& b)
{
	a.re -= b.re;
	a.im -= b.im;
	return a;
}

inline Ipp32sc operator*(_In_ const Ipp32sc& a, _In_ const Ipp32sc& b)
{
	Ipp32sc mult = { a.re * b.re - a.im * b.im, a.re * b.im + a.im * b.re };
	return mult;
}

inline Ipp32sc operator*(_In_ const Ipp32sc& a, Ipp32s b)
{
	Ipp32sc mult = { a.re * b, a.im * b };
	return mult;
}

inline Ipp32sc MultByConj(_In_ const Ipp32sc& a, _In_ const Ipp32sc& b)
{
	Ipp32sc mult = { a.re * b.re + a.im * b.im, a.im * b.re - a.re * b.im };
	return mult;
}

inline Ipp32sc operator/(_In_ const Ipp32sc& a, _In_ const Ipp32sc& b)
{
	Ipp32sc div;
	auto quot = b.re * b.re + b.im * b.im;
	div.re = (a.re * b.re + a.im * b.im) / quot;
	div.im = (a.im * b.re - a.re * b.im) / quot;

	return div;
}

inline Ipp32sc operator/(_In_ const Ipp32sc& a, Ipp32s b)
{
	Ipp32sc div = { a.re / b, a.im / b };
	return div;
}

inline bool operator==(_In_ const Ipp32sc& a, _In_ const Ipp32sc& b)
{
	return a.re == b.re && a.im == b.im;
}

inline bool operator!=(_In_ const Ipp32sc& a, _In_ const Ipp32sc& b)
{
	return a.re != b.re || a.im != b.im;
}

inline Ipp32s MagSq(_In_ const Ipp32sc& a)
{
	return a.re * a.re + a.im * a.im;
}

inline Ipp64sc operator+(_In_ const Ipp64sc& a, _In_ const Ipp64sc& b)
{
	Ipp64sc add = { a.re + b.re, a.im + b.im };
	return add;
}

_Ret_ inline Ipp64sc& operator+=(_Inout_ Ipp64sc& a, _In_ const Ipp64sc& b)
{
	a.re += b.re;
	a.im += b.im;
	return a;
}

inline Ipp64sc operator-(_In_ const Ipp64sc& a, _In_ const Ipp64sc& b)
{
	Ipp64sc subtract = { a.re - b.re, a.im - b.im };
	return subtract;
}

_Ret_ inline Ipp64sc& operator-=(_Inout_ Ipp64sc& a, _In_ const Ipp64sc& b)
{
	a.re -= b.re;
	a.im -= b.im;
	return a;
}

inline Ipp64sc operator*(_In_ const Ipp64sc& a, _In_ const Ipp64sc& b)
{
	Ipp64sc mult = { a.re * b.re - a.im * b.im, a.re * b.im + a.im * b.re };
	return mult;
}

inline Ipp64sc operator*(_In_ const Ipp64sc& a, Ipp64s b)
{
	Ipp64sc mult = { a.re * b, a.im * b };
	return mult;
}

inline Ipp64sc MultByConj(_In_ const Ipp64sc& a, _In_ const Ipp64sc& b)
{
	Ipp64sc mult = { a.re * b.re + a.im * b.im, a.im * b.re - a.re * b.im };
	return mult;
}

inline Ipp64sc operator/(_In_ const Ipp64sc& a, _In_ const Ipp64sc& b)
{
	Ipp64sc div;
	auto quot = b.re * b.re + b.im * b.im;
	div.re = (a.re * b.re + a.im * b.im) / quot;
	div.im = (a.im * b.re - a.re * b.im) / quot;

	return div;
}

inline Ipp64sc operator/(_In_ const Ipp64sc& a, Ipp64s b)
{
	Ipp64sc div = { a.re / b, a.im / b };
	return div;
}

inline bool operator==(_In_ const Ipp64sc& a, _In_ const Ipp64sc& b)
{
	return a.re == b.re && a.im == b.im;
}

inline bool operator!=(_In_ const Ipp64sc& a, _In_ const Ipp64sc& b)
{
	return a.re != b.re || a.im != b.im;
}

inline Ipp64s MagSq(_In_ const Ipp64sc& a)
{
	return a.re * a.re + a.im * a.im;
}

inline Ipp32fc operator+(_In_ const Ipp32fc& a, _In_ const Ipp32fc& b)
{
	Ipp32fc add = { a.re + b.re, a.im + b.im };
	return add;
}

_Ret_ inline Ipp32fc& operator+=(_Inout_ Ipp32fc& a, _In_ const Ipp32fc& b)
{
	a.re += b.re;
	a.im += b.im;
	return a;
}

inline Ipp32fc operator-(_In_ const Ipp32fc& a, _In_ const Ipp32fc& b)
{
	Ipp32fc subtract = { a.re - b.re, a.im - b.im };
	return subtract;
}

_Ret_ inline Ipp32fc& operator-=(_Inout_ Ipp32fc& a, _In_ const Ipp32fc& b)
{
	a.re -= b.re;
	a.im -= b.im;
	return a;
}

inline Ipp32fc operator*(_In_ const Ipp32fc& a, _In_ const Ipp32fc& b)
{
	Ipp32fc mult = { a.re * b.re - a.im * b.im, a.re * b.im + a.im * b.re };
	return mult;
}

inline Ipp32fc operator*(_In_ const Ipp32fc& a, float b)
{
	Ipp32fc mult = { a.re * b, a.im * b };
	return mult;
}

inline Ipp32fc MultByConj(_In_ const Ipp32fc& a, _In_ const Ipp32fc& b)
{
	Ipp32fc mult = { a.re * b.re + a.im * b.im, a.im * b.re - a.re * b.im };
	return mult;
}

inline Ipp32fc operator/(_In_ const Ipp32fc& a, _In_ const Ipp32fc& b)
{
	Ipp32fc div;
	auto quot = b.re * b.re + b.im * b.im;
	div.re = (a.re * b.re + a.im * b.im) / quot;
	div.im = (a.im * b.re - a.re * b.im) / quot;

	return div;
}

inline Ipp32fc operator/(_In_ const Ipp32fc& a, float b)
{
	Ipp32fc div = { a.re / b, a.im / b };
	return div;
}

inline bool operator==(_In_ const Ipp32fc& a, _In_ const Ipp32fc& b)
{
	return a.re == b.re && a.im == b.im;
}

inline bool operator!=(_In_ const Ipp32fc& a, _In_ const Ipp32fc& b)
{
	return a.re != b.re || a.im != b.im;
}

inline Ipp32f Mag(_In_ const Ipp32fc& a)
{
	return sqrt(a.re * a.re + a.im * a.im);
}

inline Ipp32f MagSq(_In_ const Ipp32fc& a)
{
	return a.re * a.re + a.im * a.im;
}

inline Ipp32f Phase(_In_ const Ipp32fc& a)
{
	return atan2(a.im, a.re);
}

inline Ipp64fc operator+(_In_ const Ipp64fc& a, _In_ const Ipp64fc& b)
{
	Ipp64fc add = { a.re + b.re, a.im + b.im };
	return add;
}

 _Ret_ inline Ipp64fc& operator+=(_Inout_ Ipp64fc& a, _In_ const Ipp64fc& b)
{
	a.re += b.re;
	a.im += b.im;
	return a;
}

inline Ipp64fc operator-(_In_ const Ipp64fc& a, _In_ const Ipp64fc& b)
{
	Ipp64fc subtract = { a.re - b.re, a.im - b.im };
	return subtract;
}

 _Ret_ inline Ipp64fc& operator-=(_Inout_ Ipp64fc& a, _In_ const Ipp64fc& b)
{
	a.re -= b.re;
	a.im -= b.im;
	return a;
}

inline Ipp64fc operator*(_In_ const Ipp64fc& a, _In_ const Ipp64fc& b)
{
	Ipp64fc mult = { a.re * b.re - a.im * b.im, a.re * b.im + a.im * b.re };
	return mult;
}

inline Ipp64fc operator*(_In_ const Ipp64fc& a, double b)
{
	Ipp64fc mult = { a.re * b, a.im * b };
	return mult;
}

inline Ipp64fc MultByConj(_In_ const Ipp64fc& a, _In_ const Ipp64fc& b)
{
	Ipp64fc mult = { a.re * b.re + a.im * b.im, a.im * b.re - a.re * b.im };
	return mult;
}

inline Ipp64fc operator/(_In_ const Ipp64fc& a, _In_ const Ipp64fc& b)
{
	Ipp64fc div;
	auto quot = b.re * b.re + b.im * b.im;
	div.re = (a.re * b.re + a.im * b.im) / quot;
	div.im = (a.im * b.re - a.re * b.im) / quot;

	return div;
}

inline Ipp64fc operator/(_In_ const Ipp64fc& a, double b)
{
	Ipp64fc div = { a.re / b, a.im / b };
	return div;
}

inline bool operator==(_In_ const Ipp64fc& a, _In_ const Ipp64fc& b)
{
	return a.re == b.re && a.im == b.im;
}

inline bool operator!=(_In_ const Ipp64fc& a, _In_ const Ipp64fc& b)
{
	return a.re != b.re || a.im != b.im;
}

inline Ipp64f Mag(_In_ const Ipp64fc& a)
{
	return sqrt(a.re * a.re + a.im * a.im);
}

inline Ipp64f MagSq(_In_ const Ipp64fc& a)
{
	return a.re * a.re + a.im * a.im;
}

inline Ipp64f Phase(_In_ const Ipp64fc& a)
{
	return atan2(a.im, a.re);
}

// Static data
template<typename T> const typename Ipp<T>::IppCmplx IppCmplxVec<T>::ONE = { 1, 0 };
template<typename T> const typename Ipp<T>::IppCmplx IppCmplxVec<T>::ZERO = { 0, 0 };

// Shorthand
typedef IppCmplxVec<Ipp16s> Ipp16scVec;
typedef IppCmplxVec<Ipp32s> Ipp32scVec;
typedef IppCmplxVec<Ipp64s> Ipp64scVec;
typedef IppCmplxVec<Ipp32f> Ipp32fcVec;
typedef IppCmplxVec<Ipp64f> Ipp64fcVec;

// More overloaded definitions for C++ - not complete; add more if needed
inline void ippsAbs(_Inout_ Ipp16sVec& srcDst) { VERIFY(ippsAbs_16s_I(&srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsAbs(_Inout_ Ipp32sVec& srcDst) { VERIFY(ippsAbs_32s_I(&srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsAbs(_Inout_ Ipp32fVec& srcDst) { VERIFY(ippsAbs_32f_I(&srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsAbs(_Inout_ Ipp64fVec& srcDst) { VERIFY(ippsAbs_64f_I(&srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsAddProduct(_In_ const Ipp32fVec& src1, _In_ const Ipp32fVec& src2, _Inout_ Ipp32fVec srcDst)
	{ ASSERT(src1.size() == src2.size()); srcDst.resize(src1.size()); VERIFY(ippsAddProduct_32f(&src1[0], &src2[0], &srcDst[0], int(src1.size())) == ippStsNoErr); return; }
inline void ippsAddProduct(_In_ const Ipp32fcVec& src1, _In_ const Ipp32fcVec& src2, _Inout_ Ipp32fcVec srcDst)
	{ ASSERT(src1.size() == src2.size()); srcDst.resize(src1.size()); VERIFY(ippsAddProduct_32fc(&src1[0], &src2[0], &srcDst[0], int(src1.size())) == ippStsNoErr); return; }
inline void ippsCartToPolar(_In_ const Ipp32fcVec& src, _Out_ Ipp32fVec& dstMagn, _Out_ Ipp32fVec& dstPhase)
	{ dstMagn.resize(src.size()); dstPhase.resize(src.size()); VERIFY(ippsCartToPolar_32fc(&src[0], &dstMagn[0], &dstPhase[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsCartToPolar(_In_ const Ipp64fcVec& src, _Out_ Ipp64fVec& dstMagn, _Out_ Ipp64fVec& dstPhase)
	{ dstMagn.resize(src.size()); dstPhase.resize(src.size()); VERIFY(ippsCartToPolar_64fc(&src[0], &dstMagn[0], &dstPhase[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsConj(_Inout_ Ipp16scVec& srcDst) { VERIFY(ippsConj_16sc_I(&srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsConj(_Inout_ Ipp32fcVec& srcDst) { VERIFY(ippsConj_32fc_I(&srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsConj(_Inout_ Ipp64fcVec& srcDst) { VERIFY(ippsConj_64fc_I(&srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsCplxToReal(_In_ const Ipp16scVec& src, _Out_ Ipp16sVec& dstRe, _Out_ Ipp16sVec& dstIm)
	{ dstRe.resize(src.size()); dstIm.resize(src.size()); VERIFY(ippsCplxToReal_16sc(&src[0], &dstRe[0], &dstIm[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsCplxToReal(_In_ const Ipp32scVec& src, _Out_ Ipp32sVec& dstRe, _Out_ Ipp32sVec& dstIm)
	{ dstRe.resize(src.size()); dstIm.resize(src.size()); VERIFY(ippsCplxToReal_32fc(reinterpret_cast<const Ipp32fc*>(&src[0]), reinterpret_cast<Ipp32f*>(&dstRe[0]), reinterpret_cast<Ipp32f*>(&dstIm[0]), int(src.size())) == ippStsNoErr); return; }
inline void ippsCplxToReal(_In_ const Ipp32fcVec& src, _Out_ Ipp32fVec& dstRe, _Out_ Ipp32fVec& dstIm)
	{ dstRe.resize(src.size()); dstIm.resize(src.size()); VERIFY(ippsCplxToReal_32fc(&src[0], &dstRe[0], &dstIm[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsCplxToReal(_In_ const Ipp64fcVec& src, _Out_ Ipp64fVec& dstRe, _Out_ Ipp64fVec& dstIm)
	{ dstRe.resize(src.size()); dstIm.resize(src.size()); VERIFY(ippsCplxToReal_64fc(&src[0], &dstRe[0], &dstIm[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsCrossCorr(_In_ const Ipp32fVec& src1, _In_ const Ipp32fVec& src2, _Inout_ Ipp32fVec& dst, int lowLag)
	{ VERIFY(ippsCrossCorr_32f(&src1[0], int(src1.size()), &src2[0], int(src2.size()), &dst[0], int(dst.size()), lowLag) == ippStsNoErr); return; }
inline void ippsCrossCorr(_In_ const Ipp32fVec& src1, _In_ const Ipp32fVec& src2, _Out_ Ipp32f& dst)
	{ VERIFY(ippsCrossCorr_32f(&src1[0], int(src1.size()), &src2[0], int(src2.size()), &dst, 1, 0) == ippStsNoErr); return; }
inline void ippsCrossCorr(_In_ const Ipp32fcVec& src1, _In_ const Ipp32fcVec& src2, _Inout_ Ipp32fcVec& dst, int lowLag)
	{ VERIFY(ippsCrossCorr_32fc(&src1[0], int(src1.size()), &src2[0], int(src2.size()), &dst[0], int(dst.size()), lowLag) == ippStsNoErr); return; }
inline void ippsCrossCorr(_In_ const Ipp32fcVec& src1, _In_ const Ipp32fcVec& src2, _Out_ Ipp32fc& dst)
	{ VERIFY(ippsCrossCorr_32fc(&src1[0], int(src1.size()), &src2[0], int(src2.size()), &dst, 1, 0) == ippStsNoErr); return; }
inline void ippsCrossCorr(_In_ const Ipp64fVec& src1, _In_ const Ipp64fVec& src2, _Inout_ Ipp64fVec& dst, int lowLag)
	{ VERIFY(ippsCrossCorr_64f(&src1[0], int(src1.size()), &src2[0], int(src2.size()), &dst[0], int(dst.size()), lowLag) == ippStsNoErr); return; }
inline void ippsCrossCorr(_In_ const Ipp64fVec& src1, _In_ const Ipp64fVec& src2, _Out_ Ipp64f& dst)
	{ VERIFY(ippsCrossCorr_64f(&src1[0], int(src1.size()), &src2[0], int(src2.size()), &dst, 1, 0) == ippStsNoErr); return; }
inline void ippsCrossCorr(_In_ const Ipp64fcVec& src1, _In_ const Ipp64fcVec& src2, _Inout_ Ipp64fcVec& dst, int lowLag)
	{ VERIFY(ippsCrossCorr_64fc(&src1[0], int(src1.size()), &src2[0], int(src2.size()), &dst[0], int(dst.size()), lowLag) == ippStsNoErr); return; }
inline void ippsCrossCorr(_In_ const Ipp64fcVec& src1, _In_ const Ipp64fcVec& src2, _Out_ Ipp64fc& dst)
	{ VERIFY(ippsCrossCorr_64fc(&src1[0], int(src1.size()), &src2[0], int(src2.size()), &dst, 1, 0) == ippStsNoErr); return; }
inline void ippsDemodulateFM_CToR(_In_ const Ipp16sVec& srcRe, _In_ const Ipp16sVec& srcIm, _Out_ Ipp16sVec& dst, _Inout_ Ipp16sc& dlyPoint)
	{ ASSERT(srcRe.size() == srcIm.size()); dst.resize(srcRe.size()); VERIFY(ippsDemodulateFM_CToR_16s(&srcRe[0], &srcIm[0], &dst[0], int(srcRe.size()), &dlyPoint) == ippStsNoErr); return; }
inline void ippsDFTFwd_CToC(_In_ const Ipp32fcVec& src, _Out_ Ipp32fcVec& dst, _In_ const IppsDFTSpec_C_32fc* pDFTSpec, _In_ Ipp8uVec& buffer)
	{ VERIFY(ippsDFTFwd_CToC_32fc(&src[0], &dst[0], pDFTSpec, buffer.size() == 0 ? nullptr : &buffer[0]) == ippStsNoErr); return; };
inline void ippsDFTFwd_CToC(_In_ const Ipp64fcVec& src, _Out_ Ipp64fcVec& dst, _In_ const IppsDFTSpec_C_64fc* pDFTSpec, _In_ Ipp8uVec& buffer)
	{ VERIFY(ippsDFTFwd_CToC_64fc(&src[0], &dst[0], pDFTSpec, buffer.size() == 0 ? nullptr : &buffer[0]) == ippStsNoErr); return; };
inline void ippsDivCRev(Ipp32f val, _Inout_ Ipp32fVec& srcDst) { VERIFY(ippsDivCRev_32f_I(val, &srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsDotProd(_In_ const Ipp32fVec& src1, _In_ const Ipp32fVec& src2, _Out_ Ipp32f& dp)
	{ ASSERT(src1.size() == src2.size()); VERIFY(ippsDotProd_32f(&src1[0], &src2[0], int(src1.size()), &dp) == ippStsNoErr); return; }
inline void ippsDotProd(_In_ const Ipp32fVec& src1, _In_ const Ipp32fcVec& src2, _Out_ Ipp32fc& dp)
	{ ASSERT(src1.size() == src2.size()); VERIFY(ippsDotProd_32f32fc(&src1[0], &src2[0], int(src1.size()), &dp) == ippStsNoErr); return; }
inline void ippsDotProd(_In_ const Ipp32fcVec& src1, _In_ const Ipp32fcVec& src2, _Out_ Ipp32fc& dp)
	{ ASSERT(src1.size() == src2.size()); VERIFY(ippsDotProd_32fc(&src1[0], &src2[0], int(src1.size()), &dp) == ippStsNoErr); return; }
inline void ippsDotProd(_In_ const Ipp64fVec& src1, _In_ const Ipp64fVec& src2, _Out_ Ipp64f& dp)
	{ ASSERT(src1.size() == src2.size()); VERIFY(ippsDotProd_64f(&src1[0], &src2[0], int(src1.size()), &dp) == ippStsNoErr); return; }
inline void ippsDotProd(_In_ const Ipp64fVec& src1, _In_ const Ipp64fcVec& src2, _Out_ Ipp64fc& dp)
	{ ASSERT(src1.size() == src2.size()); VERIFY(ippsDotProd_64f64fc(&src1[0], &src2[0], int(src1.size()), &dp) == ippStsNoErr); return; }
inline void ippsDotProd(_In_ const Ipp64fcVec& src1, _In_ const Ipp64fcVec& src2, _Out_ Ipp64fc& dp)
	{ ASSERT(src1.size() == src2.size()); VERIFY(ippsDotProd_64fc(&src1[0], &src2[0], int(src1.size()), &dp) == ippStsNoErr); return; }
inline void ippsFFTFwd_CToC(_In_ const Ipp32fcVec& src, _Out_ Ipp32fcVec& dst, _In_ const IppsFFTSpec_C_32fc* pFFTSpec, _In_ Ipp8uVec& buffer)
	{ VERIFY(ippsFFTFwd_CToC_32fc(&src[0], &dst[0], pFFTSpec, buffer.size() == 0 ? nullptr : &buffer[0]) == ippStsNoErr); return; };
inline void ippsFFTFwd_CToC(_In_ const Ipp64fcVec& src, _Out_ Ipp64fcVec& dst, _In_ const IppsFFTSpec_C_64fc* pFFTSpec, _In_ Ipp8uVec& buffer)
	{ VERIFY(ippsFFTFwd_CToC_64fc(&src[0], &dst[0], pFFTSpec, buffer.size() == 0 ? nullptr : &buffer[0]) == ippStsNoErr); return; };
inline void ippsFFTFwd_CToC(_Inout_ Ipp32fcVec& srcDst, _In_ const IppsFFTSpec_C_32fc* pFFTSpec, _In_ Ipp8uVec& buffer)
	{ VERIFY(ippsFFTFwd_CToC_32fc_I(&srcDst[0], pFFTSpec, buffer.size() == 0 ? nullptr : &buffer[0]) == ippStsNoErr); return; };
inline void ippsFFTFwd_CToC(_Inout_ Ipp64fcVec& srcDst, _In_ const IppsFFTSpec_C_64fc* pFFTSpec, _In_ Ipp8uVec& buffer)
	{ VERIFY(ippsFFTFwd_CToC_64fc_I(&srcDst[0], pFFTSpec, buffer.size() == 0 ? nullptr : &buffer[0]) == ippStsNoErr); return; };
inline void ippsFIR(_In_ const Ipp16sVec& src, _Out_ Ipp16sVec& dst, IppsFIRState32s_16s* state, int scale)
	{ dst.resize(src.size()); VERIFY(ippsFIR32s_16s_Sfs(&src[0], &dst[0], int(src.size()), state, scale) == ippStsNoErr); }
inline void ippsFIR(_In_ const Ipp32sVec& src, _Out_ Ipp32sVec& dst, IppsFIRState_32s* state, int scale)
	{ dst.resize(src.size()); VERIFY(ippsFIR_32s_Sfs(&src[0], &dst[0], int(src.size()), state, scale) == ippStsNoErr); }
inline void ippsFIRInitAlloc(_Out_ IppsFIRState32s_16s*& state, _In_ const Ipp32sVec& taps)
#pragma warning(suppress : 6001) // Uninitialized memory pass to IPP by design
	{ VERIFY(ippsFIRInitAlloc32s_16s(&state, &taps[0], int(taps.size()), 0, nullptr) == ippStsNoErr); }
inline void ippsFIRInitAlloc(_Out_ IppsFIRState_32s*& state, _In_ const Ipp32sVec& taps)
#pragma warning(suppress : 6001) // Uninitialized memory pass to IPP by design
	{ VERIFY(ippsFIRInitAlloc_32s(&state, &taps[0], int(taps.size()), nullptr) == ippStsNoErr); }
inline void ippsImag(_In_ const Ipp16scVec& src, _Out_ Ipp16sVec& dstIm) { dstIm.resize(src.size()); VERIFY(ippsImag_16sc(&src[0], &dstIm[0], int(src.size())) == ippStsNoErr); return; };
inline void ippsImag(_In_ const Ipp32fcVec& src, _Out_ Ipp32fVec& dstIm) { dstIm.resize(src.size()); VERIFY(ippsImag_32fc(&src[0], &dstIm[0], int(src.size())) == ippStsNoErr); return; };
inline void ippsImag(_In_ const Ipp64fcVec& src, _Out_ Ipp64fVec& dstIm) { dstIm.resize(src.size()); VERIFY(ippsImag_64fc(&src[0], &dstIm[0], int(src.size())) == ippStsNoErr); return; };
inline void ippsLog10(_In_ const Ipp32fVec& src, _Out_ Ipp32fVec& dst)
{
	dst.resize(src.size());
#ifdef _DEBUG
	auto status =
#endif
		ippsLog10_32f_A21(&src[0], &dst[0], int(src.size()));
	ASSERT(status == ippStsNoErr || status == ippStsSingularity);
	return;
}
inline void ippsLog10(_In_ const Ipp64fVec& src, _Out_ Ipp64fVec& dst)
{
	dst.resize(src.size());
#ifdef _DEBUG
	auto status =
#endif
		ippsLog10_64f_A50(&src[0], &dst[0], int(src.size()));
	ASSERT(status == ippStsNoErr || status == ippStsSingularity);
	return;
}
inline void ippsLn(_In_ const Ipp32fVec& src, _Out_ Ipp32fVec& dst)
{
	dst.resize(src.size());
#ifdef _DEBUG
	auto status =
#endif
		ippsLn_32f(&src[0], &dst[0], int(src.size()));
	ASSERT(status == ippStsNoErr || status == ippStsLnZeroArg);
	return;
}
inline void ippsLn(_In_ const Ipp64fVec& src, _Out_ Ipp64fVec& dst)
{
	dst.resize(src.size());
#ifdef _DEBUG
	auto status =
#endif
		ippsLn_64f(&src[0], &dst[0], int(src.size()));
	ASSERT(status == ippStsNoErr || status == ippStsLnZeroArg);
	return;
}
inline void ippsLog10(_Inout_ Ipp32fVec& srcDst)
{
#ifdef _DEBUG
	auto status =
#endif
		ippsLog10_32f_A21(&srcDst[0], &srcDst[0], int(srcDst.size()));
	ASSERT(status == ippStsNoErr || status == ippStsSingularity);
	return;
}
inline void ippsLog10(_Inout_ Ipp64fVec& srcDst)
{
#ifdef _DEBUG
	auto status =
#endif
		ippsLog10_64f_A50(&srcDst[0], &srcDst[0], int(srcDst.size()));
	ASSERT(status == ippStsNoErr || status == ippStsSingularity);
	return;
}
inline void ippsLn(_Inout_ Ipp32fVec& srcDst)
{
#ifdef _DEBUG
	auto status =
#endif
		ippsLn_32f_I(&srcDst[0], int(srcDst.size()));
	ASSERT(status == ippStsNoErr || status == ippStsLnZeroArg);
	return;
}
inline void ippsLn(_Inout_ Ipp64fVec& srcDst)
{
#ifdef _DEBUG
	auto status =
#endif
		ippsLn_64f_I(&srcDst[0], int(srcDst.size()));
	ASSERT(status == ippStsNoErr || status == ippStsLnZeroArg);
	return;
}
inline void ippsMagnitude(_In_ const Ipp16scVec& src, _Out_ Ipp16sVec& dst, int scale) { dst.resize(src.size()); VERIFY(ippsMagnitude_16sc_Sfs(&src[0], &dst[0], int(src.size()), scale) == ippStsNoErr); return; }
inline void ippsMagnitude(_In_ const Ipp32fcVec& src, _Out_ Ipp32fVec& dst) { dst.resize(src.size()); VERIFY(ippsMagnitude_32fc(&src[0], &dst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsMagnitude(_In_ const Ipp64fcVec& src, _Out_ Ipp64fVec& dst) { dst.resize(src.size()); VERIFY(ippsMagnitude_64fc(&src[0], &dst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsMaxIndx(_In_ const Ipp32fVec& src, _Out_ Ipp32f& max, _Out_ int& indx) { VERIFY(ippsMaxIndx_32f(&src[0], int(src.size()), &max, &indx) == ippStsNoErr); return; }
inline void ippsMaxIndx(_In_ const Ipp64fVec& src, _Out_ Ipp64f& max, _Out_ int& indx) { VERIFY(ippsMaxIndx_64f(&src[0], int(src.size()), &max, &indx) == ippStsNoErr); return; }
inline void ippsMaxEvery(_In_ const Ipp32fVec& src, _Inout_ Ipp32fVec& srcDst) { ASSERT(src.size() == int(srcDst.size())); VERIFY(ippsMaxEvery_32f_I(&src[0], &srcDst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsMinEvery(_In_ const Ipp32fVec& src, _Inout_ Ipp32fVec& srcDst) { ASSERT(src.size() == int(srcDst.size())); VERIFY(ippsMinEvery_32f_I(&src[0], &srcDst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsNorm_L1(_In_ const Ipp32fVec& src, _Out_ Ipp32f& norm) { VERIFY(ippsNorm_L1_32f(&src[0], int(src.size()), &norm) == ippStsNoErr); return; }
inline void ippsNorm_L1(_In_ const Ipp32fcVec& src, _Out_ Ipp32f& norm) { Ipp64f temp; VERIFY(ippsNorm_L1_32fc64f(&src[0], int(src.size()), &temp) == ippStsNoErr); norm = Ipp32f(temp); return; }
inline void ippsNorm_L1(_In_ const Ipp64fVec& src, _Out_ Ipp64f& norm) { VERIFY(ippsNorm_L1_64f(&src[0], int(src.size()), &norm) == ippStsNoErr); return; }
inline void ippsNorm_L1(_In_ const Ipp64fcVec& src, _Out_ Ipp64f& norm) { VERIFY(ippsNorm_L1_64fc64f(&src[0], int(src.size()), &norm) == ippStsNoErr); return; }
inline void ippsNorm_L2(_In_ const Ipp16sVec& src, _Out_ Ipp32f& norm) { VERIFY(ippsNorm_L2_16s32f(&src[0], int(src.size()), &norm) == ippStsNoErr); return; }
inline void ippsNorm_L2(_In_ const Ipp32fVec& src, _Out_ Ipp32f& norm) { VERIFY(ippsNorm_L2_32f(&src[0], int(src.size()), &norm) == ippStsNoErr); return; }
inline void ippsNorm_L2(_In_ const Ipp32fcVec& src, _Out_ Ipp32f& norm) { Ipp64f temp; VERIFY(ippsNorm_L2_32fc64f(&src[0], int(src.size()), &temp) == ippStsNoErr); norm = Ipp32f(temp); return; }
inline void ippsNorm_L2(_In_ const Ipp64fVec& src, _Out_ Ipp64f& norm) { VERIFY(ippsNorm_L2_64f(&src[0], int(src.size()), &norm) == ippStsNoErr); return; }
inline void ippsNorm_L2(_In_ const Ipp64fcVec& src, _Out_ Ipp64f& norm) { VERIFY(ippsNorm_L2_64fc64f(&src[0], int(src.size()), &norm) == ippStsNoErr); return; }
#ifdef _M_X64
inline void ippsNthMaxElement(_In_ const Ipp32fVec& src, size_t N, _Out_ Ipp32f& res) { VERIFY(m7_ippsNthMaxElement_32f(&src[0], int(src.size()), int(N), &res) == ippStsNoErr); return; }
inline void ippsNthMaxElement(_In_ const Ipp64fVec& src, size_t N, _Out_ Ipp64f& res) { VERIFY(m7_ippsNthMaxElement_64f(&src[0], int(src.size()), int(N), &res) == ippStsNoErr); return; }
inline void ippsNthMaxElement(_In_count_(len) const Ipp32f* src, size_t len, size_t N, _Out_ Ipp32f& res) { VERIFY(m7_ippsNthMaxElement_32f(src, int(len), int(N), &res) == ippStsNoErr); return; }
inline void ippsNthMaxElement(_In_count_(len) const Ipp64f* src, size_t len, size_t N, _Out_ Ipp64f& res) { VERIFY(m7_ippsNthMaxElement_64f(src, int(len), int(N), &res) == ippStsNoErr); return; }
#else
inline void ippsNthMaxElement(_In_ const Ipp32fVec& src, size_t N, _Out_ Ipp32f& res) { VERIFY(w7_ippsNthMaxElement_32f(&src[0], int(src.size()), int(N), &res) == ippStsNoErr); return; }
inline void ippsNthMaxElement(_In_ const Ipp64fVec& src, size_t N, _Out_ Ipp64f& res) { VERIFY(w7_ippsNthMaxElement_64f(&src[0], int(src.size()), int(N), &res) == ippStsNoErr); return; }
inline void ippsNthMaxElement(_In_count_(len) const Ipp32f* src, size_t len, size_t N, _Out_ Ipp32f& res) { VERIFY(w7_ippsNthMaxElement_32f(src, int(len), int(N), &res) == ippStsNoErr); return; }
inline void ippsNthMaxElement(_In_count_(len) const Ipp64f* src, size_t len, size_t N, _Out_ Ipp64f& res) { VERIFY(w7_ippsNthMaxElement_64f(src, int(len), int(N), &res) == ippStsNoErr); return; }
#endif
inline void ippsPolarToCart(_In_ const Ipp16sVec& srcMagn, _In_ const Ipp16sVec& srcPhase, _Out_ Ipp16scVec& dst, int magScale, int phaseScale)
	{ ASSERT(srcMagn.size() == srcPhase.size()); dst.resize(int(srcMagn.size())); VERIFY(ippsPolarToCart_16sc_Sfs(&srcMagn[0], &srcPhase[0], &dst[0], int(srcMagn.size()), magScale, phaseScale) == ippStsNoErr); return; }
inline void ippsPolarToCart(_In_ const Ipp32fVec& srcMagn, _In_ const Ipp32fVec& srcPhase, _Out_ Ipp32fcVec& dst)
	{ ASSERT(srcMagn.size() == srcPhase.size()); dst.resize(int(srcMagn.size())); VERIFY(ippsPolarToCart_32fc(&srcMagn[0], &srcPhase[0], &dst[0], int(srcMagn.size())) == ippStsNoErr); return; }
inline void ippsPolarToCart(_In_ const Ipp64fVec& srcMagn, _In_ const Ipp64fVec& srcPhase, _Out_ Ipp64fcVec& dst)
	{ ASSERT(srcMagn.size() == srcPhase.size()); dst.resize(int(srcMagn.size())); VERIFY(ippsPolarToCart_64fc(&srcMagn[0], &srcPhase[0], &dst[0], int(srcMagn.size())) == ippStsNoErr); return; }
inline void ippsPhase(_In_ const Ipp32fcVec& src, _Out_ Ipp32fVec& dst) { dst.resize(src.size()); VERIFY(ippsPhase_32fc(&src[0], &dst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsPhase(_In_ const Ipp64fcVec& src, _Out_ Ipp64fVec& dst) { dst.resize(src.size()); VERIFY(ippsPhase_64fc(&src[0], &dst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsPowerSpectr(_In_ const Ipp32fcVec& src, _Out_ Ipp32fVec& dst) { dst.resize(src.size()); VERIFY(ippsPowerSpectr_32fc(&src[0], &dst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsPowerSpectr(_In_ const Ipp64fcVec& src,_Out_  Ipp64fVec& dst) { dst.resize(src.size()); VERIFY(ippsPowerSpectr_64fc(&src[0], &dst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsPowerSpectr(_In_ const Ipp32fc* pSrc, _Inout_ Ipp32fVec& dst) { VERIFY(ippsPowerSpectr_32fc(pSrc, &dst[0], int(dst.size())) == ippStsNoErr); return; }
inline void ippsPowerSpectr(_In_ const Ipp64fc* pSrc, _Inout_ Ipp64fVec& dst) { VERIFY(ippsPowerSpectr_64fc(pSrc, &dst[0], int(dst.size())) == ippStsNoErr); return; }
inline void ippsReal(_In_ const Ipp16scVec& src, _Out_ Ipp16sVec& dstRe) { dstRe.resize(src.size()); VERIFY(ippsReal_16sc(&src[0], &dstRe[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsReal(_In_ const Ipp32fcVec& src, _Out_ Ipp32fVec& dstRe) { dstRe.resize(src.size()); VERIFY(ippsReal_32fc(&src[0], &dstRe[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsReal(_In_ const Ipp64fcVec& src, _Out_ Ipp64fVec& dstRe) { dstRe.resize(src.size()); VERIFY(ippsReal_64fc(&src[0], &dstRe[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsRealToCplx(_In_ const Ipp16sVec& srcRe, Ipp16scVec& dst)
	{ dst.resize(srcRe.size()); VERIFY(ippsRealToCplx_16s(&srcRe[0], nullptr, &dst[0], int(srcRe.size())) == ippStsNoErr); return; }
inline void ippsRealToCplx(_In_ const Ipp16sVec& srcRe, _In_ const Ipp16sVec& srcIm, _Out_ Ipp16scVec& dst)
	{ ASSERT(srcRe.size() == srcIm.size()); dst.resize(srcRe.size()); VERIFY(ippsRealToCplx_16s(&srcRe[0], &srcIm[0], &dst[0], int(srcRe.size())) == ippStsNoErr); return; }
inline void ippsRealToCplx(_In_ const Ipp32sVec& srcRe, _In_ const Ipp32sVec& srcIm, _Out_ Ipp32scVec& dst)
	{ ASSERT(srcRe.size() == srcIm.size()); dst.resize(srcRe.size()); VERIFY(ippsRealToCplx_32f(reinterpret_cast<const Ipp32f*>(&srcRe[0]), reinterpret_cast<const Ipp32f*>(&srcIm[0]), reinterpret_cast<Ipp32fc*>(&dst[0]), int(srcRe.size())) == ippStsNoErr); return; }
inline void ippsRealToCplx(_In_ const Ipp32fVec& srcRe, Ipp32fcVec& dst)
	{ dst.resize(srcRe.size()); VERIFY(ippsRealToCplx_32f(&srcRe[0], nullptr, &dst[0], int(srcRe.size())) == ippStsNoErr); return; }
inline void ippsRealToCplx(_In_ const Ipp32fVec& srcRe, _In_ const Ipp32fVec& srcIm, _Out_ Ipp32fcVec& dst)
	{ ASSERT(srcRe.size() == srcIm.size()); dst.resize(srcRe.size()); VERIFY(ippsRealToCplx_32f(&srcRe[0], &srcIm[0], &dst[0], int(srcRe.size())) == ippStsNoErr); return; }
inline void ippsRealToCplx(_In_ const Ipp64fVec& srcRe, _Out_ Ipp64fcVec& dst)
	{ dst.resize(srcRe.size()); VERIFY(ippsRealToCplx_64f(&srcRe[0], nullptr, &dst[0], int(srcRe.size())) == ippStsNoErr); return; }
inline void ippsRealToCplx(_In_ const Ipp64fVec& srcRe, _In_ const Ipp64fVec& srcIm, _Out_ Ipp64fcVec& dst)
	{ ASSERT(srcRe.size() == srcIm.size()); dst.resize(srcRe.size()); VERIFY(ippsRealToCplx_64f(&srcRe[0], &srcIm[0], &dst[0], int(srcRe.size())) == ippStsNoErr); return; }
inline void ippsSampleDown(_In_ const Ipp16sVec& src, _Out_ Ipp16sVec& dst, size_t factor, _Inout_ size_t& phase)
{
	auto dstLen = int((src.size() + factor - 1 - phase) / factor);
	dst.resize(dstLen);

	if(dstLen == 0)
	{
		phase = (factor + phase - src.size() % factor) % factor;
	}
	else
	{
		auto p = int(phase);
		VERIFY(ippsSampleDown_16s(&src[0], int(src.size()), &dst[0], &dstLen, int(factor), &p) == ippStsNoErr);
		phase = p;
	}
}
inline void ippsSampleDown(_In_ const Ipp32sVec& src, _Out_ Ipp32sVec& dst, size_t factor, _Inout_ size_t& phase)
{
	auto dstLen = int((src.size() + factor - 1 - phase) / factor);
	dst.resize(dstLen);

	if(dstLen == 0)
	{
		phase = (factor + phase - src.size() % factor) % factor;
	}
	else
	{
		auto p = int(phase);
		VERIFY(ippsSampleDown_32f(reinterpret_cast<const Ipp32f*>(&src[0]), int(src.size()), reinterpret_cast<Ipp32f*>(&dst[0]), &dstLen, int(factor), &p) == ippStsNoErr);
		phase = p;
	}
}
inline void ippsSampleDown(_In_count_(srcLen) const Ipp16sc* src, size_t srcLen, _Out_cap_x_((srcLen + factor - 1 - phase) / factor) Ipp16sc* dst, _Out_ size_t& dstLen, size_t factor, _Inout_ size_t& phase)
{
	int dstLen_;
	auto p = int(phase);
	VERIFY(ippsSampleDown_16sc(src, int(srcLen), dst, &dstLen_, int(factor), &p) == ippStsNoErr);
	dstLen = dstLen_;
	phase = p;
}
inline void ippsSampleDown(_In_count_(srcLen) const Ipp32sc* src, size_t srcLen, _Out_cap_x_((srcLen + factor - 1 - phase) / factor) Ipp32sc* dst, _Out_ size_t& dstLen, size_t factor, _Inout_ size_t& phase)
{
	int dstLen_;
	auto p = int(phase);
	VERIFY(ippsSampleDown_32fc(reinterpret_cast<const Ipp32fc*>(src), int(srcLen), reinterpret_cast<Ipp32fc*>(dst), &dstLen_, int(factor), &p) == ippStsNoErr);
	dstLen = dstLen_;
	phase = p;
}
inline void ippsSampleDown(_In_ const Ipp32fVec& src, _Out_ Ipp32fVec& dst, size_t factor, _Inout_ size_t& phase)
{
	int dstLen = int((src.size() + factor - 1 - phase) / factor);
	dst.resize(dstLen);

	if(dstLen == 0)
	{
		phase = (factor + phase - src.size() % factor) % factor;
	}
	else
	{
		int p = int(phase);
		VERIFY(ippsSampleDown_32f(&src[0], int(src.size()), &dst[0], &dstLen, int(factor), &p) == ippStsNoErr);
		phase = p;
	}
}
inline void ippsSampleDown(_In_ const Ipp32fcVec& src, _Out_ Ipp32fcVec& dst, size_t factor, _Inout_ size_t& phase)
{
	auto dstLen = int((src.size() + factor - 1 - phase) / factor);
	dst.resize(dstLen);

	if(dstLen == 0)
	{
		phase = (factor + phase - src.size() % factor) % factor;
	}
	else
	{
		auto p = int(phase);
		VERIFY(ippsSampleDown_32fc(&src[0], int(src.size()), &dst[0], &dstLen, int(factor), &p) == ippStsNoErr);
		phase = p;
	}
}
inline void ippsSampleDown(_In_ const Ipp64fVec& src, _Out_ Ipp64fVec& dst, size_t factor, _Inout_ size_t& phase)
{
	auto dstLen = int((src.size() + factor - 1 - phase) / factor);
	dst.resize(dstLen);

	if(dstLen == 0)
	{
		phase = (factor + phase - src.size() % factor) % factor;
	}
	else
	{
		auto p = int(phase);
		VERIFY(ippsSampleDown_64f(&src[0], int(src.size()), &dst[0], &dstLen, int(factor), &p) == ippStsNoErr);
		phase = p;
	}
}
inline void ippsSampleDown(const Ipp64fcVec& src, _Out_ Ipp64fcVec& dst, size_t factor, _Inout_ size_t& phase)
{
	auto dstLen = int((src.size() + factor - 1 - phase) / factor);
	dst.resize(dstLen);

	if(dstLen == 0)
	{
		phase = (factor + phase - src.size() % factor) % factor;
	}
	else
	{
		auto p = int(phase);
		VERIFY(ippsSampleDown_64fc(&src[0], int(src.size()), &dst[0], &dstLen, int(factor), &p) == ippStsNoErr);
		phase = p;
	}
}
inline void ippsSqr(_Inout_ Ipp32fVec& srcDst) { VERIFY(ippsSqr_32f_I(&srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsSqr(_Inout_ Ipp32fcVec& srcDst) { VERIFY(ippsSqr_32fc_I(&srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsSqr(_Inout_ Ipp64fVec& srcDst) { VERIFY(ippsSqr_64f_I(&srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsSqr(_Inout_ Ipp64fcVec& srcDst) { VERIFY(ippsSqr_64fc_I(&srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsSqr(_In_ const Ipp32fVec& src, _Out_ Ipp32fVec& dst) { dst.resize(src.size()); VERIFY(ippsSqr_32f(&src[0], &dst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsSqr(_In_ const Ipp32fcVec& src, _Out_ Ipp32fcVec& dst) { dst.resize(src.size()); VERIFY(ippsSqr_32fc(&src[0], &dst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsSqr(_In_ const Ipp64fVec& src, _Out_ Ipp64fVec& dst) { dst.resize(src.size()); VERIFY(ippsSqr_64f(&src[0], &dst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsSqr(_In_ const Ipp64fcVec& src, _Out_ Ipp64fcVec& dst) { dst.resize(src.size()); VERIFY(ippsSqr_64fc(&src[0], &dst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsSqrt(_Inout_ Ipp32fVec& srcDst) { VERIFY(ippsSqrt_32f_I(&srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsSqrt(_Inout_ Ipp32fcVec& srcDst) { VERIFY(ippsSqrt_32fc_I(&srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsSqrt(_Inout_ Ipp64fVec& srcDst) { VERIFY(ippsSqrt_64f_I(&srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsSqrt(_Inout_ Ipp64fcVec& srcDst) { VERIFY(ippsSqrt_64fc_I(&srcDst[0], int(srcDst.size())) == ippStsNoErr); return; }
inline void ippsSqrt(_In_ const Ipp32fVec& src, _Out_ Ipp32fVec& dst) { dst.resize(src.size()); VERIFY(ippsSqrt_32f(&src[0], &dst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsSqrt(_In_ const Ipp32fcVec& src, _Out_ Ipp32fcVec& dst) { dst.resize(src.size()); VERIFY(ippsSqrt_32fc(&src[0], &dst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsSqrt(_In_ const Ipp64fVec& src, _Out_ Ipp64fVec& dst) { dst.resize(src.size()); VERIFY(ippsSqrt_64f(&src[0], &dst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsSqrt(_In_ const Ipp64fcVec& src, _Out_ Ipp64fcVec& dst) { dst.resize(src.size()); VERIFY(ippsSqrt_64fc(&src[0], &dst[0], int(src.size())) == ippStsNoErr); return; }
inline void ippsStdDev(_In_ const Ipp32fVec& src, _Out_ Ipp32f& stdDev) { VERIFY(ippsStdDev_32f(&src[0], int(src.size()), &stdDev, ippAlgHintAccurate) == ippStsNoErr); return; }
inline void ippsStdDev(_In_ const Ipp64fVec& src, _Out_ Ipp64f& stdDev) { VERIFY(ippsStdDev_64f(&src[0], int(src.size()), &stdDev) == ippStsNoErr); return; }
inline void ippsThreshold_GT(_In_ const Ipp16scVec& src, _Out_ Ipp16scVec& dst, Ipp16s level)
	{ dst.resize(src.size()); VERIFY(ippsThreshold_GT_16sc(&src[0], &dst[0], int(src.size()), level) == ippStsNoErr); return; }
inline void ippsThreshold_GT(_In_ const Ipp32fcVec& src, _Out_ Ipp32fcVec& dst, Ipp32f level)
	{ dst.resize(src.size()); VERIFY(ippsThreshold_GT_32fc(&src[0], &dst[0], int(src.size()), level) == ippStsNoErr); return; }
inline void ippsThreshold_GT(_In_ const Ipp64fcVec& src, _Out_ Ipp64fcVec& dst, Ipp64f level)
	{ dst.resize(src.size()); VERIFY(ippsThreshold_GT_64fc(&src[0], &dst[0], int(src.size()), level) == ippStsNoErr); return; }
inline void ippsThreshold_GT(_Inout_ Ipp32fVec& srcDst, Ipp32f level)
	{ VERIFY(ippsThreshold_GT_32f_I(&srcDst[0], int(srcDst.size()), level) == ippStsNoErr); return; }
inline void ippsThreshold_GT(_Inout_ Ipp32fcVec& srcDst, Ipp32f level)
	{ VERIFY(ippsThreshold_GT_32fc_I(&srcDst[0], int(srcDst.size()), level) == ippStsNoErr); return; }
inline void ippsThreshold_GTVal(_Inout_ Ipp32fcVec& srcDst, Ipp32f level, _In_ const Ipp32fc& value) { VERIFY(ippsThreshold_GTVal_32fc_I(&srcDst[0], int(srcDst.size()), level, value) == ippStsNoErr); return; }
inline void ippsThreshold_GTVal(_Inout_ Ipp64fcVec& srcDst, Ipp64f level, _In_ const Ipp64fc& value) { VERIFY(ippsThreshold_GTVal_64fc_I(&srcDst[0], int(srcDst.size()), level, value) == ippStsNoErr); return; }
inline void ippsThreshold_GTVal(_In_ const Ipp32fVec& src, _Out_ Ipp32fVec& dst, Ipp32f level, Ipp32f value)
	{ dst.resize(src.size()); VERIFY(ippsThreshold_GTVal_32f(&src[0], &dst[0], int(src.size()), level, value) == ippStsNoErr); return; }
inline void ippsThreshold_GTVal(_In_ const Ipp64fVec& src, _Out_ Ipp64fVec& dst, Ipp64f level, Ipp64f value)
	{ dst.resize(src.size()); VERIFY(ippsThreshold_GTVal_64f(&src[0], &dst[0], int(src.size()), level, value) == ippStsNoErr); return; }
inline void ippsThreshold_LT(_In_ const Ipp32fcVec& src, _Out_ Ipp32fcVec& dst, Ipp32f level)
	{ dst.resize(src.size()); VERIFY(ippsThreshold_LT_32fc(&src[0], &dst[0], int(src.size()), level) == ippStsNoErr); return; }
inline void ippsThreshold_LT(_In_ const Ipp64fcVec& src, _Out_ Ipp64fcVec& dst, Ipp64f level)
	{ dst.resize(src.size()); VERIFY(ippsThreshold_LT_64fc(&src[0], &dst[0], int(src.size()), level) == ippStsNoErr); return; }
inline void ippsThreshold_LT(_Inout_ Ipp32fVec& srcDst, Ipp32f level)
	{ VERIFY(ippsThreshold_LT_32f_I(&srcDst[0], int(srcDst.size()), level) == ippStsNoErr); return; }
inline void ippsThreshold_LT(_Inout_ Ipp32fcVec& srcDst, Ipp32f level)
	{ VERIFY(ippsThreshold_LT_32fc_I(&srcDst[0], int(srcDst.size()), level) == ippStsNoErr); return; }
inline void ippsThreshold_LT(_In_ const Ipp32fc* pSrc, _Inout_ Ipp32fcVec& dst, Ipp32f level)
	{ VERIFY(ippsThreshold_LT_32fc(pSrc, &dst[0], int(dst.size()), level) == ippStsNoErr); return; }
inline void ippsThreshold_LT(_In_ const Ipp64fc* pSrc, _Inout_ Ipp64fcVec& dst, Ipp64f level)
	{ VERIFY(ippsThreshold_LT_64fc(pSrc, &dst[0], int(dst.size()), level) == ippStsNoErr); return; }
inline void ippsThreshold_LTVal(_Inout_ Ipp32fVec& srcDst, Ipp32f level, _In_ const Ipp32f& value) { VERIFY(ippsThreshold_LTVal_32f_I(&srcDst[0], int(srcDst.size()), level, value) == ippStsNoErr); return; }
inline void ippsThreshold_LTVal(_Inout_ Ipp32fcVec& srcDst, Ipp32f level, _In_ const Ipp32fc& value) { VERIFY(ippsThreshold_LTVal_32fc_I(&srcDst[0], int(srcDst.size()), level, value) == ippStsNoErr); return; }
inline void ippsThreshold_LTVal(_Inout_ Ipp64fVec& srcDst, Ipp64f level, _In_ const Ipp64f& value) { VERIFY(ippsThreshold_LTVal_64f_I(&srcDst[0], int(srcDst.size()), level, value) == ippStsNoErr); return; }
inline void ippsThreshold_LTVal(_Inout_ Ipp64fcVec& srcDst, Ipp64f level, _In_ const Ipp64fc& value) { VERIFY(ippsThreshold_LTVal_64fc_I(&srcDst[0], int(srcDst.size()), level, value) == ippStsNoErr); return; }
inline void ippsThreshold_LTVal(_In_ const Ipp32fVec& src, _Out_ Ipp32fVec& dst, Ipp32f level, Ipp32f value)
	{ dst.resize(src.size()); VERIFY(ippsThreshold_LTVal_32f(&src[0], &dst[0], int(src.size()), level, value) == ippStsNoErr); return; }
inline void ippsThreshold_LTVal(_In_ const Ipp64fVec& src, _Out_ Ipp64fVec& dst, Ipp64f level, Ipp64f value)
	{ dst.resize(src.size()); VERIFY(ippsThreshold_LTVal_64f(&src[0], &dst[0], int(src.size()), level, value) == ippStsNoErr); return; }
inline void ippsThreshold_LTValGTVal(_In_ const Ipp32fVec& src, _Out_ Ipp32fVec& dst, Ipp32f levelLT, Ipp32f valueLT, Ipp32f levelGT, Ipp32f valueGT)
	{ dst.resize(src.size()); VERIFY(ippsThreshold_LTValGTVal_32f(&src[0], &dst[0], int(dst.size()), levelLT, valueLT, levelGT, valueGT) == ippStsNoErr); return; }
inline void ippsTone_Direct(_Inout_ Ipp16sVec& dst, Ipp16s magn, float rFreq, _Inout_ float& phase)
	{ VERIFY(ippsTone_Direct_16s(&dst[0], int(dst.size()), magn, rFreq, &phase, ippAlgHintAccurate) == ippStsNoErr); return; }
inline void ippsTone_Direct(_Inout_ Ipp16scVec& dst, Ipp16s magn, float rFreq, _Inout_ float& phase)
	{ VERIFY(ippsTone_Direct_16sc(&dst[0], int(dst.size()), magn, rFreq, &phase, ippAlgHintAccurate) == ippStsNoErr); return; }
inline void ippsTone_Direct(_Inout_ Ipp32fcVec& dst, float magn, float rFreq, _Inout_ float& phase)
	{ VERIFY(ippsTone_Direct_32fc(&dst[0], int(dst.size()), magn, rFreq, &phase, ippAlgHintAccurate) == ippStsNoErr); return; }
inline void ippsTone_Direct(_Inout_ Ipp64fcVec& dst, double magn, double rFreq, _Inout_ double& phase)
	{ VERIFY(ippsTone_Direct_64fc(&dst[0], int(dst.size()), magn, rFreq, &phase, ippAlgHintAccurate) == ippStsNoErr); return; }
inline void ippsVectorRamp(_Inout_ Ipp8uVec& dst, float offset, float slope) { VERIFY(ippsVectorRamp_8u(&dst[0], int(dst.size()), offset, slope) == ippStsNoErr); return; }
inline void ippsVectorRamp(_Inout_ Ipp32fVec& dst, float offset, float slope) { VERIFY(ippsVectorRamp_32f(&dst[0], int(dst.size()), offset, slope) == ippStsNoErr); return; }
inline void ippsVectorRamp(_Inout_ Ipp64fVec& dst, double offset, double slope) { VERIFY(ippsVectorRamp_64f(&dst[0], int(dst.size()), float(offset), float(slope)) == ippStsNoErr); return; }
