/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2017 TCI International, Inc. All rights reserved              *
**************************************************************************/
#include "stdafx.h"
#include <vector>
#include <fstream>
#include <string>
#include <map>
#include "CabToCsms.h"
#include "PatternDatFile.h"
#include "CabFile.h"
#include "IppVec.h"
#include "Units.h"

void CCabToCsms::Convert(LPCTSTR filePath)
{
	static const float TWO_PI = 8 * atan(1.0f);

	CString patternFile(filePath);
	patternFile += _T(".cab");
	CString outFile(filePath);
	outFile += _T(".dat");

	CCabFile cabFile(patternFile);
	_tprintf(_T("Loading patterns from file %s\n"), LPCTSTR(patternFile));

	
	std::ofstream file(LPCTSTR(outFile), std::ios::out | std::ios::binary);
	if (!file)
	{
		CFileException::ThrowErrno(errno, outFile);
	}
	_tprintf(_T("Storing patterns to file %s\n"), LPCTSTR(outFile));

	do
	{
		_tprintf(_T(" %s...\n"), cabFile.GetFileName());
		CString line;

		// Read parameters, skipping blank line(s)
		while (cabFile.GetLine(line) && line.IsEmpty());

		// Parse and store parameters
		struct SRecord1
		{
			int numPols;
			int zSym;
			int numEls;
			int numFileAz;
			float azStart;
			float azInc;
			double freqMhz;
			int nfc;
		};
		SRecord1 record1;
		float dummy;

		if (_stscanf_s(line, _T("%d %d %d %d %f %f %f %f %lf %d"),
			&record1.numPols, &record1.zSym, &record1.numEls, &record1.numFileAz, &dummy, &dummy,
			&record1.azStart, &record1.azInc, &record1.freqMhz, &record1.nfc) != 10 || record1.numPols != 1 || record1.numEls != 1)
		{
			CFileException::ThrowOsError(ERROR_INVALID_DATA, cabFile.GetFileName() + _T(" in ") + patternFile);
			__assume(false);
		}

		int numAz = int(360.0 / record1.azInc + 0.5);

		file.write(reinterpret_cast<char*>(&record1), sizeof(record1));
		_tprintf(_T("Write record1: %d %d %d %d %f %f %lf %d\n"),
			record1.numPols, record1.zSym, record1.numEls, record1.numFileAz, record1.azStart, record1.azInc, record1.freqMhz, record1.nfc);

		// Read in voltage vectors
		if (!cabFile.GetLine(line))
		{
			CFileException::ThrowOsError(ERROR_INVALID_DATA, cabFile.GetFileName() + _T(" in ") + patternFile);
			__assume(false);
		}

		float scale;
		if (_stscanf_s(line, _T("%f"), &scale) != 1)
		{
			CFileException::ThrowOsError(ERROR_INVALID_DATA, cabFile.GetFileName() + _T(" in ") + patternFile);
			__assume(false);
		}
		struct SPolar
		{
			float mag;
			float phase;
		};
		std::vector<SPolar> record2(record1.numFileAz);

		int numRecord2 = 0;
		for (int wiraAz = 0; wiraAz < record1.numFileAz; ++wiraAz)
		{
			short magOct;
			short phaseOct;
			if (!cabFile.GetOctalShort(magOct) || magOct < 0 || !cabFile.GetOctalShort(phaseOct))
			{
				CFileException::ThrowOsError(ERROR_INVALID_DATA, cabFile.GetFileName() + _T(" in ") + patternFile);
				__assume(false);
			}

			if (wiraAz >= numAz)
			{
				TRACE("wiraAz >= numAz\n");
				continue;
			}

			record2[wiraAz].mag = magOct * scale;
			record2[wiraAz].phase = float(phaseOct * TWO_PI / (SHRT_MAX + 1));
			++numRecord2;
		}
		file.write(reinterpret_cast<char*>(&record2[0]), record1.numFileAz * sizeof(SPolar));
		_tprintf(_T("Write record2: %d %d %d\n"), record1.numFileAz, numRecord2, record1.numFileAz * sizeof(SPolar));

		short temp;
		if (cabFile.GetOctalShort(temp))
		{
			CFileException::ThrowOsError(ERROR_INVALID_DATA, cabFile.GetFileName() + _T(" in ") + patternFile);
			__assume(false);
		}
	} while (cabFile.OpenNextFile());

	_tprintf(_T("Done\n"));
	file.close();
	return;
}

void CCabToCsms::Verify(LPCTSTR filePath)
{
	int numAnts = 5;

	std::string patternFile = LPCSTR(CStringA(filePath));
	patternFile += ".dat";
	_tprintf(_T("Loading patterns from file %hs\n"), patternFile.c_str());

	CPatternDatFile::WfaConjPatterns m_wfaConjPatterns;
	CPatternDatFile::Load(patternFile.c_str(), numAnts, m_wfaConjPatterns);
	_tprintf(_T("m_wfaConjPatterns size = %u\n"), m_wfaConjPatterns.size());

	CString cabFile = filePath;
	cabFile += ".cab";
	_tprintf(_T("Loading patterns from file %s\n"), LPCTSTR(cabFile));

	WfaConjPatterns s_wfaConjPatterns;
	LoadPatternCabFile(cabFile, numAnts, s_wfaConjPatterns);
	_tprintf(_T("s_wfaConjPatterns size = %u\n"), s_wfaConjPatterns.size());

	if (m_wfaConjPatterns.size() != s_wfaConjPatterns.size())
	{
		_tprintf(_T("ERROR: Patterns have different sizes!\n"));
		return;
	}
	auto it1 = m_wfaConjPatterns.begin();
	auto it2 = s_wfaConjPatterns.begin();
	for (size_t i = 0; i < m_wfaConjPatterns.size(); ++i, ++it1, ++it2)
	{
		if (it1 == m_wfaConjPatterns.end())
		{
			_tprintf(_T("ERROR: m_wfaConjPatterns ran out of data!\n"));
			return;
		}
		if (it2 == s_wfaConjPatterns.end())
		{
			_tprintf(_T("ERROR: s_wfaConjPatterns ran out of data!\n"));
			return;
		}
		if (it1->first != it2->first)
		{
			_tprintf(_T("ERROR: map key mismatch %f != %f\n"), it1->first.Hz<double>(), it2->first.Hz<double>());
			return;
		}
		if (it1->second.size() != it2->second.size())
		{
			_tprintf(_T("ERROR: map value @ %f size mismatch %u != %u\n"), it1->first.Hz<double>(), it1->second.size(), it2->second.size());
			return;
		}
		for (size_t j = 0; j < it1->second.size(); ++j)
		{
			if (it1->second[j].size() != it2->second[j].size())
			{
				_tprintf(_T("ERROR: vec @ %f,%u size mismatch %u != %u\n"), it1->first.Hz<double>(), j,
					it1->second[j].size(), it2->second[j].size());
				return;
			}
			for (size_t k = 0; k < it1->second[j].size(); ++k)
			{
				float err1 = fabs(it1->second[j][k].r - it2->second[j][k].re);
				float err2 = fabs(it1->second[j][k].i - it2->second[j][k].im);
				if (err1 > 1.e-6 * fabs(it2->second[j][k].re) || err2 > 1.e-6 * fabs(it2->second[j][k].im))
				{
					_tprintf(_T("ERROR: data @ %f,%u,%u data mismatch %.7f,%.7f != %.7f,%.7f\n"), it1->first.Hz<double>(), j, k,
						it1->second[j][k].r, it1->second[j][k].i, it2->second[j][k].re, it2->second[j][k].im);
					return;
				}
			}
		}
	}

	_tprintf(_T("Verify Successful\n"));

	return;
}

void CCabToCsms::LoadPatternCabFile(LPCTSTR patternFile, int numAnts, WfaConjPatterns& s_wfaConjPatterns)
{
	// Adapted from CConfigDf::LoadWfaDfParams

	bool evenSymmetry = false;

	CCabFile cabFile(patternFile);
	TRACE(_T("Loading patterns from file %s\n"), LPCTSTR(patternFile));

	do
	{
		TRACE(_T(" %s...\n"), cabFile.GetFileName());
		CString line;

		// Read parameters, skipping blank line(s)
		while (cabFile.GetLine(line) && line.IsEmpty());

		// Parse and store parameters
		int numPols;
		int zSym;
		int numEls;
		int numFileAz;
		float dummy;
		float azStart;
		float azInc;
		double freqMhz;
		int nfc;

		if (_stscanf_s(line, _T("%d %d %d %d %f %f %f %f %lf %d"),
			&numPols, &zSym, &numEls, &numFileAz, &dummy, &dummy,
			&azStart, &azInc, &freqMhz, &nfc) != 10 || numPols != 1 || numEls != 1)
		{
			CFileException::ThrowOsError(ERROR_INVALID_DATA, cabFile.GetFileName() + _T(" in ") + patternFile);
			__assume(false);
		}

		ASSERT(azStart == 0.0); // TODO - fix this
		int numAz = int(360.0 / azInc + 0.5);
		ASSERT(numFileAz == numAz / 2 + 1 || numFileAz == numAz || numFileAz == numAz + 1);

		if (s_wfaConjPatterns.empty())
		{
			if (numFileAz != numAz && numFileAz != numAz + 1)
			{
				evenSymmetry = true;
			}
		}
		else
		{
			if ((evenSymmetry && numFileAz == int(360.0 / azInc + 0.5)) || (!evenSymmetry && numFileAz < int(360.0 / azInc + 0.5)))
			{
				TRACE(_T("AntParms not consistent\n"));
				CFileException::ThrowOsError(ERROR_INVALID_DATA, cabFile.GetFileName() + _T(" in ") + patternFile);
				__assume(false);
			}
		}

		// Create an entry in the map with the vectors all sized appropriately
		WfaConjPattern& conjPattern = (s_wfaConjPatterns[Units::Frequency(freqMhz * 1e6 + 0.5)]
			= WfaConjPattern(numAz, Ipp32fcVec(numAnts)));

		// Read in voltage vectors
		if (!cabFile.GetLine(line))
		{
			CFileException::ThrowOsError(ERROR_INVALID_DATA, cabFile.GetFileName() + _T(" in ") + patternFile);
			__assume(false);
		}

		float scale;

		if (_stscanf_s(line, _T("%f"), &scale) != 1)
		{
			CFileException::ThrowOsError(ERROR_INVALID_DATA, cabFile.GetFileName() + _T(" in ") + patternFile);
			__assume(false);
		}

		for (int wiraAz = 0; wiraAz < numFileAz; ++wiraAz)
		{
			// WIRA patterns are in cartesian coordinates - convert to compass coordinates
			int az = (wiraAz == 0 ? 0 : numAz - wiraAz);
			short magOct;
			short phaseOct;

			if (!cabFile.GetOctalShort(magOct) || magOct < 0 || !cabFile.GetOctalShort(phaseOct))
			{
				CFileException::ThrowOsError(ERROR_INVALID_DATA, cabFile.GetFileName() + _T(" in ") + patternFile);
				__assume(false);
			}

			if (wiraAz >= numAz)
			{
				continue;
			}

			float mag = magOct * scale;
			float phase = float(phaseOct * Units::TWO_PI / (SHRT_MAX + 1));
			conjPattern[az][0].re = mag * cos(phase);
			conjPattern[az][0].im = -mag * sin(phase);

			// Expand symmetries
			if (evenSymmetry && az != 0 && az != numAz / 2)
			{
				conjPattern[numAz - az][0] = conjPattern[az][0];
			}

			for (int ant = 1; ant < numAnts; ant++)
			{
				conjPattern[(az + ant * numAz / numAnts) % numAz][ant] = conjPattern[az][0];

				if (evenSymmetry && az != 0 && az != numAz / 2)
				{
					conjPattern[(numAz - az + ant * numAz / numAnts) % numAz][ant] = conjPattern[az][0];
				}
			}
		}

		for (int az = 0; az < numAz; ++az)
		{
			Ipp32f norm;
			ippsNorm_L2(conjPattern[az], norm);
			conjPattern[az] /= norm;
		}

		short temp;

		if (cabFile.GetOctalShort(temp))
		{
			CFileException::ThrowOsError(ERROR_INVALID_DATA, cabFile.GetFileName() + _T(" in ") + patternFile);
			__assume(false);
		}
	} while (cabFile.OpenNextFile());

	TRACE(_T("Done\n"));

	return;
}
