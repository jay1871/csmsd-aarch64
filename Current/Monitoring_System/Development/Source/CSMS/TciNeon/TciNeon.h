/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <vector>
#include <functional>

namespace TciNeon
{
	typedef int8_t ne10_int8_t;
	typedef uint8_t ne10_uint8_t;
	typedef int16_t ne10_int16_t;
	typedef int32_t ne10_int32_t;
	typedef float ne10_float32_t;
	struct ne10_fft_cpx_int16_t
	{
		int16_t r;
		int16_t i;
	};
	struct ne10_fft_cpx_int32_t
	{
		int32_t r;
		int32_t i;
	};
	struct ne10_fft_cpx_float32_t
	{
		float r;
		float i;
	};

	ne10_fft_cpx_float32_t& operator+=(ne10_fft_cpx_float32_t& a, const ne10_fft_cpx_float32_t& b);
	ne10_fft_cpx_float32_t operator-(const ne10_fft_cpx_float32_t& a, const ne10_fft_cpx_float32_t& b);
	ne10_fft_cpx_float32_t operator*(const ne10_fft_cpx_float32_t& a, ne10_fft_cpx_float32_t b);
	ne10_fft_cpx_float32_t operator*(const ne10_fft_cpx_float32_t& a, float b);
	ne10_fft_cpx_float32_t operator/(const ne10_fft_cpx_float32_t& a, const ne10_fft_cpx_float32_t& b);
	ne10_fft_cpx_float32_t operator/(const ne10_fft_cpx_float32_t& a, float b);
	ne10_fft_cpx_float32_t MultByConj(const ne10_fft_cpx_float32_t& a, const ne10_fft_cpx_float32_t& b);

	// Overloaded definitions for C++ - add more if needed
	void ne10sAbs(ne10_float32_t* pSrcDst, int len);
	void ne10sAdd(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pSrcDst, int len);
	void ne10sAddC(ne10_float32_t val, ne10_float32_t* pSrcDst, int len);
	void ne10sAdd(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pSrcDst, int len);
	void ne10sAutoCorr(const ne10_fft_cpx_float32_t* __restrict__ pSrc, int srcLen,
		ne10_fft_cpx_float32_t* __restrict__ pDst, int dstLen, int lowLag);
	void ne10sCartToPolar(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDstMagn, ne10_float32_t* __restrict__ pDstPhase, int len);
	void ne10sConj(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pDst, int len);
	void ne10sConvert(const ne10_int16_t* __restrict__ pSrc,  ne10_float32_t* __restrict__ pDst, int len, int scaleFactor);
	void ne10sConvert(const ne10_float32_t* __restrict__ pSrc, ne10_int32_t* __restrict__ pDst, int len, ne10_int32_t low, ne10_int32_t high);
	void ne10sConvert(const ne10_int32_t* __restrict__ pSrc, ne10_int8_t* __restrict__ pDst, int len);
	void ne10sConvert(const ne10_float32_t* __restrict__ pSrc, ne10_uint8_t* __restrict__ pDst, int len, int scaleFactor);
	void ne10sConvert(const ne10_fft_cpx_int16_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pDst, int len, int scaleFactor);
	void ne10sCrossCorr(const ne10_fft_cpx_float32_t* __restrict__ pSrc1, int src1Len, const ne10_fft_cpx_float32_t* __restrict__ pSrc2, int src2Len,
		ne10_fft_cpx_float32_t* __restrict__ pDst, int dstLen, int lowLag);
	void ne10sCopy(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len);
	void ne10sCopy(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pDst, int len);
	void ne10sCopyUncached(const unsigned long* __restrict__ pSrc, unsigned long* __restrict__ pDst, int len);
	void ne10sCopyUncached(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len);
	void ne10sCopyUncached(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pDst, int len);
	void ne10sDeriv31(const ne10_fft_cpx_float32_t* __restrict__ pSrc2, ne10_fft_cpx_float32_t* __restrict__ pDp);
	void ne10sDiv(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pSrcDst, int len, ne10_float32_t val);
	void ne10sDiv(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pSrcDst, int len);
	void ne10sDiv(const ne10_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pSrcDst, int len);
	void ne10sDivC(ne10_float32_t val, ne10_float32_t* pSrcDst, int len);
	void ne10sDiv(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pSrcDst, int len);
	void ne10sDivC(ne10_float32_t val, ne10_fft_cpx_float32_t* pSrcDst, int len);
	void ne10sDotProd(const ne10_float32_t* __restrict__ pSrc1, const ne10_float32_t* __restrict__ pSrc2, int len, ne10_float32_t* __restrict__ pDp);
	void ne10sDotProd(const ne10_float32_t* __restrict__ pSrc1, const ne10_fft_cpx_float32_t* __restrict__ pSrc2, int len, ne10_fft_cpx_float32_t* __restrict__ pDp);
	void ne10sDotProd(const ne10_fft_cpx_float32_t* __restrict__ pSrc1, const ne10_fft_cpx_float32_t* __restrict__ pSrc2, int len, ne10_fft_cpx_float32_t* __restrict__ pDp);
	void ne10sImag(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDstIm, int len);
	void ne10sLn(ne10_float32_t* pSrcDst, int len);
	void ne10sLn(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len);
	void ne10sLog10(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len);
	void ne10sMagnitude(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len);
	void ne10sMagnitudeSqr(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len);
	void ne10sMaxEvery(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pSrcDst, int len);
	void ne10sMax(const ne10_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pMax);
	void ne10sMaxandMaxIndex(const ne10_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pMax, int& maxindex);
	void ne10sMean(const ne10_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pMean);
	void ne10sMin(const ne10_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pMin);
	void ne10sMove(const ne10_float32_t* pSrc, ne10_float32_t* pDst, int len);
	void ne10sMove(const ne10_fft_cpx_float32_t* pSrc, ne10_fft_cpx_float32_t* pDst, int len);
	void ne10sMul(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pSrcDst, int len);
	void ne10sMulC(ne10_float32_t val, ne10_float32_t* pSrcDst, int len);
	void ne10sMulC(ne10_float32_t val, const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len);
	void ne10sMul(ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pSrcDst, int len);
	void ne10sMulC(ne10_float32_t val, ne10_fft_cpx_float32_t* pSrcDst, int len);
	void ne10sNthMaxElement(const ne10_float32_t* __restrict__ pSrc, int len, int N, ne10_float32_t* __restrict__ pRes);
	void ne10sNorm_L2(const ne10_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pNorm);
	void ne10sNorm_L2(const ne10_fft_cpx_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pNorm);
	void ne10sPhase(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len);
	void ne10sPolarToCart(const ne10_float32_t* __restrict__ pSrcMagn, const ne10_float32_t* __restrict__ pSrcPhase, ne10_fft_cpx_float32_t* __restrict__ pDst, int len);
	void ne10sRealToCplx(const ne10_float32_t* __restrict__ pSrcRe, const ne10_float32_t* __restrict__ pSrcIm, ne10_fft_cpx_float32_t* __restrict__ pDst, int len);
	void ne10sRealToCplx(ne10_float32_t val, const ne10_float32_t* __restrict__ pSrcRe, const ne10_float32_t* __restrict__ pSrcIm, ne10_fft_cpx_float32_t* __restrict__ pDst, int len);
	void ne10sSet(ne10_float32_t val, ne10_float32_t* pDst, int len);
	void ne10sSet(ne10_fft_cpx_float32_t val, ne10_fft_cpx_float32_t* pDst, int len);
	void ne10sSqr(ne10_float32_t* pSrcDst, int len);
	void ne10sSqrt(ne10_float32_t* pSrcDst, int len);
	void ne10sStdDev(const ne10_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pStdDev);
	void ne10sSub(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pSrcDst, int len);
	void ne10sSubC(ne10_float32_t val, ne10_float32_t* pSrcDst, int len);
	void ne10sSum(const ne10_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pSum);
	void ne10sSum(const ne10_fft_cpx_float32_t* __restrict__ pSrc, int len, ne10_fft_cpx_float32_t* __restrict__ pSum);
	void ne10sThreshold_LT(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pDst, int len, ne10_float32_t level);
	void ne10sThreshold_LTVal(ne10_float32_t* pSrcDst, int len, ne10_float32_t level, ne10_float32_t value);
	void ne10sThreshold_LTVal(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len, ne10_float32_t level, ne10_float32_t value);
	void ne10sThreshold_LTVal(ne10_fft_cpx_float32_t* pSrcDst, int len, ne10_float32_t level, ne10_fft_cpx_float32_t value);
	void ne10sThreshold_GTVal(ne10_fft_cpx_float32_t* pSrcDst, int len, ne10_float32_t level, ne10_fft_cpx_float32_t value);
	void ne10sTone_Direct(ne10_fft_cpx_float32_t* __restrict__ pDst, int len, float magn, float rFreq, float* __restrict__ phase);
	void ne10sVectorRamp(ne10_float32_t* pDst, int len, float offset, float slope);
	bool ne10sVerifyArgs(const char* str, const unsigned long* a, const unsigned long* b, int len);
	bool ne10sVerifyArgs(const char* str, const ne10_float32_t* a, const ne10_float32_t* b, int len);
	bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, const ne10_fft_cpx_float32_t* b, int len);
	bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, const ne10_float32_t* b, const ne10_float32_t* c, int len);
	bool ne10sVerifyArgs(const char* str, const ne10_int16_t* a, const ne10_float32_t* b, int len);
	bool ne10sVerifyArgs(const char* str, const ne10_float32_t* a, const ne10_uint8_t* b, int len);
	bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, const ne10_fft_cpx_float32_t* b, int aLen, int bLen);
	bool ne10sVerifyArgs(const char* str, const ne10_float32_t* a, const ne10_float32_t* b, int aLen, int bLen);
	bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, const ne10_fft_cpx_float32_t* b,
		const ne10_fft_cpx_float32_t* c, int aLen, int bLen, int cLen);
	bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, const ne10_float32_t* b, int len);
	bool ne10sVerifyArgs(const char* str, const ne10_float32_t* a, const ne10_float32_t* b, int len, const ne10_float32_t* c);
	bool ne10sVerifyArgs(const char* str, const ne10_float32_t* a, const ne10_fft_cpx_float32_t* b, int len, const ne10_fft_cpx_float32_t* c);
	bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, const ne10_fft_cpx_float32_t* b, int len, const ne10_fft_cpx_float32_t* c);
	bool ne10sVerifyArgs(const char* str, const ne10_float32_t* a, int len, const ne10_float32_t* b);
	bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, int len, const ne10_float32_t* b);
	bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, int len, const ne10_fft_cpx_float32_t* b);

	// Real float vector
	class Ne10F32Vec
	{
		friend class Ne10F32cVec;
		friend class Ne10F32cVec2;
	public:
		static ne10_float32_t* ne10sMalloc(int size);
		static ne10_float32_t* Allocate(int size);
		static void Deallocate(ne10_float32_t* vector);

		Ne10F32Vec(void);
		explicit Ne10F32Vec(int size);
		Ne10F32Vec(int size, ne10_float32_t val);
		Ne10F32Vec(const ne10_float32_t* v, int size);
		Ne10F32Vec(const ne10_int16_t* v, int size);
		Ne10F32Vec(const Ne10F32Vec& v);
		~Ne10F32Vec(void);

		Ne10F32Vec& operator=(const Ne10F32Vec& v);
		Ne10F32Vec& operator=(Ne10F32Vec&& v);
		Ne10F32Vec& operator=(ne10_float32_t val);
		ne10_float32_t& operator[](int i);
		const ne10_float32_t& operator[](int i) const;
		Ne10F32Vec& operator+=(const Ne10F32Vec& v);
		Ne10F32Vec& operator+=(ne10_float32_t val);
		Ne10F32Vec& operator*=(const Ne10F32Vec& v);
		Ne10F32Vec& operator*=(ne10_float32_t val);
		Ne10F32Vec& operator-=(const Ne10F32Vec& v);
		Ne10F32Vec& operator-=(ne10_float32_t val);
		Ne10F32Vec& operator/=(const Ne10F32Vec& v);
		Ne10F32Vec& operator/=(ne10_float32_t val);
		Ne10F32Vec& operator<<=(int rot);
		Ne10F32Vec& operator>>=(int rot);
		void assign(int size, ne10_float32_t val);
		void assign(const ne10_float32_t* v, int size);
		ne10_float32_t* data(void);
		const ne10_float32_t* data(void) const;
		ne10_float32_t maximum(void) const;
		ne10_float32_t mean(void) const;
		ne10_float32_t minimum(void) const;
		void reserve(int size);
		void resize(int size);
		size_t size(void) const;
		ne10_float32_t sum(void) const;

	private:
		size_t m_reserved;
		size_t m_size;
		ne10_float32_t* m_vector;
	};

	// Complex float vector type 2
	class Ne10F32cVec2
	{
		friend class Ne10F32cVec;
	public:
//		static const ne10_fft_cpx_float32_t ONE;
//		static const ne10_fft_cpx_float32_t ZERO;

		static ne10_float32_t* ne10sMallocCmplx2(int size);
		inline static ne10_float32_t* Allocate(int size);
		inline static void Deallocate(ne10_float32_t* vector);

		// Constructors
		Ne10F32cVec2(void);
//		explicit Ne10F32cVec2(int size);
//		Ne10F32cVec2(int size, const ne10_fft_cpx_float32_t& val);
//		Ne10F32cVec2(const ne10_fft_cpx_float32_t* v, int size);
//		Ne10F32cVec2(const ne10_fft_cpx_int16_t* v, int size);
		Ne10F32cVec2(const Ne10F32cVec2& v);
//		Ne10F32cVec2(const ne10_float32_t* real, const ne10_float32_t* imag, int size);

		~Ne10F32cVec2(void);

//		Ne10F32cVec2& operator=(const Ne10F32cVec2& v);
		Ne10F32cVec2& operator=(Ne10F32cVec2&& v);
//		Ne10F32cVec2& operator=(const ne10_fft_cpx_float32_t& val);
//		ne10_fft_cpx_float32_t& operator[](int i);
//		Ne10F32cVec2& operator+=(const Ne10F32cVec2& v);
		Ne10F32cVec2& operator*=(const Ne10F32cVec2& v);
		Ne10F32cVec2& operator*=(ne10_float32_t val);
		Ne10F32cVec2& operator/=(const Ne10F32cVec2& v);
		Ne10F32cVec2& operator/=(const Ne10F32Vec& v);
//		Ne10F32cVec2& operator/=(ne10_float32_t val);
//		Ne10F32cVec2& operator<<=(int rot);
//		Ne10F32cVec2& operator>>=(int rot);
//		const ne10_fft_cpx_float32_t& operator[](int i) const;
//		void assign(int size, const ne10_fft_cpx_float32_t& val);
		void assign(const ne10_float32_t* v, int size);
//		ne10_fft_cpx_float32_t* data(void);
//		const ne10_fft_cpx_float32_t* data(void) const;
		ne10_float32_t* dataReal(void);
		ne10_float32_t* dataImag(void);
		const ne10_float32_t* dataReal(void) const;
		const ne10_float32_t* dataImag(void) const;
		const ne10_float32_t& real(int i) const;
		const ne10_float32_t& imag(int i) const;
		ne10_float32_t& real(int i);
		ne10_float32_t& imag(int i);
		size_t imgOffset(void) const { return(m_imgOffset); }
		void reserve(int size, bool preservedContent = false);
		void resize(int size);
		size_t size(void) const;
//		ne10_fft_cpx_float32_t sum(void) const;

	private:
		size_t m_imgOffset;
		size_t m_reserved;
		size_t m_size;
		ne10_float32_t* m_vector;	// real[m_size], imag[m_size]
	};

	// Complex float vector
	class Ne10F32cVec
	{
	public:
		static const ne10_fft_cpx_float32_t ONE;
		static const ne10_fft_cpx_float32_t ZERO;

		static ne10_fft_cpx_float32_t* ne10sMallocCmplx(int size);
		inline static ne10_fft_cpx_float32_t* Allocate(int size);
		inline static void Deallocate(ne10_fft_cpx_float32_t* vector);

		// Constructors
		Ne10F32cVec(void);
		explicit Ne10F32cVec(int size);
		Ne10F32cVec(int size, const ne10_fft_cpx_float32_t& val);
		Ne10F32cVec(const ne10_fft_cpx_float32_t* v, int size);
		Ne10F32cVec(const ne10_fft_cpx_int16_t* v, int size);
		Ne10F32cVec(const Ne10F32cVec& v);
		explicit Ne10F32cVec(const Ne10F32cVec2& v);
		Ne10F32cVec(const ne10_float32_t* real, const ne10_float32_t* imag, int size);

		~Ne10F32cVec(void);

		Ne10F32cVec& operator=(const Ne10F32cVec& v);
		Ne10F32cVec& operator=(Ne10F32cVec&& v);
		Ne10F32cVec& operator=(const ne10_fft_cpx_float32_t& val);
		ne10_fft_cpx_float32_t& operator[](int i);
		Ne10F32cVec& operator+=(const Ne10F32cVec& v);
		Ne10F32cVec& operator*=(const Ne10F32cVec& v);
		Ne10F32cVec& operator*=(ne10_float32_t val);
		Ne10F32cVec& operator/=(const Ne10F32cVec& v);
		Ne10F32cVec& operator/=(const Ne10F32Vec& v);
		Ne10F32cVec& operator/=(ne10_float32_t val);
		Ne10F32cVec& operator<<=(int rot);
		Ne10F32cVec& operator>>=(int rot);
		const ne10_fft_cpx_float32_t& operator[](int i) const;
		void assign(int size, const ne10_fft_cpx_float32_t& val);
		void assign(const ne10_fft_cpx_float32_t* v, int size);
		ne10_fft_cpx_float32_t* data(void);
		const ne10_fft_cpx_float32_t* data(void) const;
		void reserve(int size);
		void resize(int size);
		size_t size(void) const;
		ne10_fft_cpx_float32_t sum(void) const;

	private:
		size_t m_reserved;
		size_t m_size;
		ne10_fft_cpx_float32_t* m_vector;
	};

	// Static data
	__attribute__((__weak__)) const ne10_fft_cpx_float32_t Ne10F32cVec::ONE = { 1, 0 };
	__attribute__((__weak__)) const ne10_fft_cpx_float32_t Ne10F32cVec::ZERO = { 0, 0 };

	void ne10sAbs(Ne10F32Vec& srcDst);
	void ne10sAutoCorr(const Ne10F32cVec& __restrict__ src, Ne10F32cVec& __restrict__ dst, int lowLag);
	void ne10sCartToPolar(const Ne10F32cVec& __restrict__ src, Ne10F32Vec& __restrict__ dstMagn, Ne10F32Vec& __restrict__ dstPhase);
	void ne10sCrossCorr(const Ne10F32cVec& __restrict__ src1, const Ne10F32cVec& __restrict__ src2, Ne10F32cVec& __restrict__ dst, int lowLag);
	void ne10sCrossCorr(const Ne10F32cVec& __restrict__ src1, const Ne10F32cVec& __restrict__ src2, ne10_fft_cpx_float32_t& __restrict__ dst);
	void ne10sDotProd(const Ne10F32Vec& __restrict__ src1, const Ne10F32Vec& __restrict__ src2, ne10_float32_t& __restrict__ dp);
	void ne10sDotProd(const Ne10F32Vec& __restrict__ src1, const Ne10F32cVec& __restrict__ src2, ne10_fft_cpx_float32_t& __restrict__ dp);
	void ne10sDotProd(const Ne10F32cVec& __restrict__ src1, const Ne10F32cVec& __restrict__ src2, ne10_fft_cpx_float32_t& __restrict__ dp);
	//void ne10sFFTFwd_CToC(const Ne10F32cVec& src, Ne10F32cVec& dst, ne10_fft_cfg_float32_t cfg);
	void ne10sImag(const Ne10F32cVec& __restrict__ src, Ne10F32Vec& __restrict__ dstIm);
	void ne10sLn(Ne10F32Vec& srcDst);
	void ne10sLog10(const Ne10F32Vec& __restrict__ src, Ne10F32Vec& __restrict__ dst);
	void ne10sMagnitude(const Ne10F32cVec& __restrict__ src, Ne10F32Vec& __restrict__ dst);
	void ne10sMagnitude(const Ne10F32cVec2& __restrict__ src, Ne10F32Vec& __restrict__ dst);
	void ne10sMaxEvery(const Ne10F32Vec& __restrict__ src, Ne10F32Vec& __restrict__ srcDst);
	void ne10sNthMaxElement(const ne10_float32_t* __restrict__ pSrc, int len, int N, ne10_float32_t& __restrict__ res);
	void ne10sNorm_L2(const Ne10F32Vec& __restrict__ src, ne10_float32_t& __restrict__ norm);
	void ne10sNorm_L2(const Ne10F32cVec& __restrict__ src, ne10_float32_t& __restrict__ norm);
	void ne10sPhase(const Ne10F32cVec& __restrict__ src, Ne10F32Vec& __restrict__ dst);
	void ne10sPolarToCart(const Ne10F32Vec& __restrict__ srcMagn, const Ne10F32Vec& __restrict__ srcPhase, Ne10F32cVec& __restrict__ dst);
	void ne10sPowerSpectr(const ne10_fft_cpx_float32_t* __restrict__ pSrc, Ne10F32Vec& __restrict__ dst);
	void ne10sPowerSpectr(const Ne10F32cVec& __restrict__ src, Ne10F32Vec& __restrict__ dst);
	void ne10sSqr(Ne10F32Vec& srcDst);
	void ne10sSqrt(Ne10F32Vec& srcDst);
	void ne10sStdDev(const Ne10F32Vec& __restrict__ src, ne10_float32_t& __restrict__ stdDev);
	void ne10sThreshold_LT(const ne10_fft_cpx_float32_t* __restrict__ pSrc, Ne10F32cVec& __restrict__ dst, ne10_float32_t level);
	void ne10sThreshold_LTVal(Ne10F32Vec& srcDst, ne10_float32_t level, ne10_float32_t value);
	void ne10sThreshold_LTVal(const Ne10F32Vec& __restrict__ src, Ne10F32Vec& __restrict__ dst, ne10_float32_t level, ne10_float32_t value);
	void ne10sThreshold_LTVal(Ne10F32cVec& srcDst, ne10_float32_t level, ne10_fft_cpx_float32_t value);
	void ne10sThreshold_LTVal(Ne10F32cVec2& srcDst, ne10_float32_t level, ne10_fft_cpx_float32_t value);
	void ne10sThreshold_GTVal(Ne10F32cVec& srcDst, ne10_float32_t level, ne10_fft_cpx_float32_t value);
	void ne10sThreshold_GTVal(Ne10F32cVec2& srcDst, ne10_float32_t level, ne10_fft_cpx_float32_t value);
	void ne10sVectorRamp(Ne10F32Vec& dst, float offset, float slope);
	void ne10sTone_Direct(Ne10F32cVec& __restrict__ dst, float magn, float rFreq, float& __restrict__ phase);
};	// namespace TciNeon
