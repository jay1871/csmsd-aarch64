/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2015 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"

#include "SuspendableTask.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
std::condition_variable CSuspendableTask::m_suspendsChanged;
const unsigned int CSuspendableTask::SUSPEND_TIMEOUT;


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CSuspendableTask::CSuspendableTask(void) :
	CTask((assert(false), (SEquipCtrlMsg*)nullptr), nullptr), // Should never be constructed this way (CTask is a virtual base)
	m_bandOffset(),
	m_bandSize(),
	m_hasTerminatedData(m_taskParams.size()),
	m_navigation(),
	m_suspend(false),
	m_suspended(false),
	m_terminatedData(m_taskParams.size())
{
	for (size_t band = 0; band < m_taskParams.size(); ++band)
	{
		if (band == 0)
		{
			m_bandOffset.push_back(0);
		}
		else
		{
			m_bandOffset.push_back(m_bandOffset.back() + m_bandSize.back());
		}

		if (m_taskParams[band].numBlocks > 0)
		{
			// Include band
			m_bandSize.push_back(size_t((m_taskParams[band].stopFreq - m_taskParams[band].startFreq) / m_taskParams[band].bw));
		}
		else
		{
			// Exclude band
			m_bandSize.push_back(0);
		}
	}

	m_report.hdr = m_msgHdr;

	// Size vectors
	for (size_t band = 0; band < m_terminatedData.size(); ++band)
	{
		m_terminatedData[band].assign(m_taskParams[band].numBlocks, Ne10F32Vec());
	}
	for (size_t band = 0; band < m_hasTerminatedData.size(); ++band)
	{
		m_hasTerminatedData[band].assign(m_taskParams[band].numBlocks, false);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CSuspendableTask::CSuspendableTask(_In_ SRestartData& restartData)
:
	CTask((assert(FALSE), (SEquipCtrlMsg*)nullptr), nullptr), // Should never be constructed this way (CTask is a virtual base)
	m_hasTerminatedData(m_taskParams.size()),
	m_navigation(),
	m_suspend(true),
	m_suspended(true),
	m_terminatedData(m_taskParams.size())
{
	TRACE("m_taskParams size = %u\n", m_taskParams.size());

	// Read from ISequentialStream into member data
	ReadData(restartData, m_bandOffset);
	ReadData(restartData, m_bandSize);
	m_report.hdr = m_msgHdr;

	// Size vectors
	for (size_t band = 0; band < m_terminatedData.size(); ++band)
	{
		m_terminatedData[band].assign(m_taskParams[band].numBlocks, Ne10F32Vec());
	}
	for (size_t band = 0; band < m_hasTerminatedData.size(); ++band)
	{
		m_hasTerminatedData[band].assign(m_taskParams[band].numBlocks, false);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CSuspendableTask::~CSuspendableTask(void)
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Should the task be suspended?
//
bool CSuspendableTask::CheckSuspend(void)
{
	// Suspended?
	std::unique_lock<std::mutex> lock(m_taskIdMutex);

	if (m_suspend)
	{
		m_suspended = true;
		m_suspendsChanged.notify_all();
		return true;
	}
	else
	{
		return false;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Is a channel excluded?
//
bool CSuspendableTask::Excluded(Units::Frequency freq, Units::Frequency bw, CConfig::EPolarization pol) const
{
	if (m_excludeBands.empty())
	{
		return false;
	}
	else
	{
		CConfig::STaskParams taskParams;
		taskParams.startFreq = taskParams.stopFreq = freq;
		taskParams.bw = bw;
		taskParams.pol = pol;

		return m_excludeBands.find(taskParams) != m_excludeBands.end();
	}
}


//////////////////////////////////////////////////////////////////////
//
// Convert watts to (uncalibrated) field strength
//
float CSuspendableTask::FieldStrength(float watts, Units::Frequency freq, float /*switchTemp*/, size_t band) const
{
	return watts * pow(10.0f, FieldStrengthFactor(freq, band) / 10);
}

float CSuspendableTask::FieldStrengthFactor(Units::Frequency freq, size_t band) const
{
	float cableLoss = m_config->GetRfCableLoss(m_taskParams[band].hf, m_ant, freq);
	float lightningGain = m_config->GetLightningProtectGain(m_taskParams[band].hf, m_ant, freq);
	const CTask::SBlockState& blockState = GetBlockState();
	float switchGain = m_config->GetSwitchGain(m_taskParams[band].hf, m_ant, freq) +
			    blockState.gainAdjdB;
	float antFactor = m_config->GetAntennaFactor(m_taskParams[band].hf, m_ant, freq, m_taskParams[band].pol == CConfig::EPolarization::HORIZ);
	return (cableLoss - lightningGain - switchGain + antFactor + 30.f);
}


//////////////////////////////////////////////////////////////////////
//
// Get the field strength factors from the map, creating them if they don't exist
//
const Ne10F32Vec& CSuspendableTask::GetFieldStrengthFactors(Units::Frequency freq, Units::Frequency bw, size_t numChannels, size_t step)
{
	SFsFactorKey key = { freq, bw, numChannels, step };

	auto fs = m_fieldStrengthFactors.find(key);
	if (fs == m_fieldStrengthFactors.end())
	{
		// No factors yet, calculate them and store
		auto pr = m_fieldStrengthFactors.insert(FsFactorMap::value_type(key, Ne10F32Vec()));
		if (!pr.second)
		{
			throw std::logic_error("m_fieldStrengthFactors.insert failed\n");	// This can't happen
		}
		fs = pr.first;
		fs->second.resize(numChannels);
		for (unsigned int chan = 0; chan < numChannels; ++chan)
		{
			fs->second[chan] = FieldStrengthFactor(freq + chan * bw, step);
		}
	}
	return fs->second;
}


//////////////////////////////////////////////////////////////////////
//
// Get the task state
//
SEquipCtrlMsg::SStateResp CSuspendableTask::GetState(const SEquipCtrlMsg::STaskIdKey& taskIdKey)
{
	std::unique_lock<std::mutex> lock(m_taskIdMutex);
	TaskMap::const_iterator task = m_taskMap.find(taskIdKey.taskId);
	SEquipCtrlMsg::SStateResp stateResp;
	SuspendableTask suspendableTask;
	if (task != m_taskMap.end() && task->second->m_taskIdKey.key == taskIdKey.key &&
		bool(suspendableTask = std::dynamic_pointer_cast<CSuspendableTask>(task->second)))
	{
		stateResp.state = (suspendableTask->m_suspended ? SEquipCtrlMsg::SStateResp::SUSPENDED : SEquipCtrlMsg::SStateResp::RUNNING);
		stateResp.completionTime = suspendableTask->m_completionTime;
	}
	else
	{
		stateResp.state = SEquipCtrlMsg::SStateResp::IDLE;
		stateResp.completionTime = SEquipCtrlMsg::MAX_DATE;
	}

	return stateResp;
}


//////////////////////////////////////////////////////////////////////
//
// Save data for restart
//
void CSuspendableTask::SaveRestartData(_Inout_ std::vector<unsigned char>& data) const
{
	SaveData(data, m_bandOffset);
	SaveData(data, m_bandSize);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Suspend or release a task
//
CSuspendableTask::SuspendableTask CSuspendableTask::Suspend(const SEquipCtrlMsg::STaskIdKey& taskIdKey, bool suspend)
{
	std::unique_lock<std::mutex> lock(m_taskIdMutex);
	TaskMap::iterator task = m_taskMap.find(taskIdKey.taskId);
	SuspendableTask suspendableTask;

	if (task != m_taskMap.end() && task->second->m_taskIdKey.key == taskIdKey.key &&
		bool(suspendableTask = std::dynamic_pointer_cast<CSuspendableTask>(task->second)) && suspendableTask->m_suspend != suspend)
	{
		suspendableTask->m_suspend = suspend;
		TRACE(_T("%sing task %lu\n"), suspend ? _T("Suspend") : _T("Resum"), taskIdKey.taskId);

		if (suspend)
		{
			while (!suspendableTask->m_suspended)
			{
				if (m_suspendsChanged.wait_for(lock, std::chrono::milliseconds(SUSPEND_TIMEOUT)) == std::cv_status::timeout)
				{
					TRACE(_T("Timed out suspending task %lu\n"), taskIdKey.taskId);
					suspendableTask->m_suspended = true;
				}
			}
		}
		else
		{
			suspendableTask->m_suspended = false;
		}

		return suspendableTask;
	}
	else
	{
		return SuspendableTask();
	}
}


//////////////////////////////////////////////////////////////////////
//
// Suspend or release all tasks
//
std::vector<SEquipCtrlMsg::STaskIdKey> CSuspendableTask::SuspendAll(bool suspend)
{
	std::unique_lock<std::mutex> lock(m_taskIdMutex);
	std::vector<SEquipCtrlMsg::STaskIdKey> taskIds;

	for (TaskMap::iterator task = m_taskMap.begin(); task != m_taskMap.end(); ++task)
	{
		SuspendableTask suspendableTask(std::dynamic_pointer_cast<CSuspendableTask>(task->second));

		if (suspendableTask && suspendableTask->m_suspend != suspend)
		{
			suspendableTask->m_suspend = suspend;
			taskIds.push_back(suspendableTask->m_taskIdKey);
			TRACE(_T("%sing task %lu\n"), suspend ? _T("Suspend") : _T("Resum"), suspendableTask->m_taskIdKey.taskId);
		}
	}

	for (size_t i = 0; i < taskIds.size(); ++i)
	{
		if (suspend)
		{
			for (SuspendableTask suspendableTask(std::dynamic_pointer_cast<CSuspendableTask>(FindTask(taskIds[i])));
				suspendableTask && !suspendableTask->m_suspended;)
			{
				if (m_suspendsChanged.wait_for(lock, std::chrono::milliseconds(SUSPEND_TIMEOUT)) == std::cv_status::timeout)
				{
					TRACE(_T("Timed out suspending task %lu\n"), taskIds[i].taskId);
					suspendableTask->m_suspended = true;
				}
			}
		}
		else
		{
			if (SuspendableTask suspendableTask = std::dynamic_pointer_cast<CSuspendableTask>(FindTask(taskIds[i])))
			{
				suspendableTask->m_suspended = false;
			}
		}
	}

	return taskIds;
}


//////////////////////////////////////////////////////////////////////
//
// Terminate a task
//
void CSuspendableTask::Terminate(const SEquipCtrlMsg::STaskIdKey& taskIdKey)
{
	std::unique_lock<std::mutex> lock(m_taskIdMutex);
	TaskMap::iterator task = m_taskMap.find(taskIdKey.taskId);

	if (task != m_taskMap.end() && task->second->m_taskIdKey.key == taskIdKey.key)
	{
		task->second->m_completionTime = 0;
		TRACE(_T("Terminating task %lu\n"), taskIdKey.taskId);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Terminate all a client's tasks
//
void CSuspendableTask::Terminate(_In_ const CNetConnection<void>* client)
{
	std::unique_lock<std::mutex> lock(m_taskIdMutex);

	for (TaskMap::iterator task = m_taskMap.begin(); task != m_taskMap.end(); ++task)
	{
		if (task->second->m_source == client)
		{
			task->second->m_completionTime = 0;
			TRACE(_T("Terminating task %lu\n"), task->second->m_taskIdKey.taskId);
		}
	}

	return;
}
