/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "Config.h"
#include "ErrorCodes.h"
#include "MetricsMsg.h"
#include "MetricsNet.h"
#include "PriorityQueue.h"
#include "ResultsTbl.h"
#include "Singleton.h"
#include "ScheduleTbl.h"
#include "SqliteDb.h"

struct SEquipCtrlMsg;
//class CMetricsNet;

//class CMetrics;

class CPersistentData :
	// Inheritance
	protected CResultsTbl,
	protected CScheduleTbl
{
	// Friends
	friend class CSingleton<CPersistentData>;

public:
	// Types
	typedef std::shared_ptr<const SEquipCtrlMsg> QueueMsg;

	// Functions
	void AddMsg(_In_ const SEquipCtrlMsg& msg, int priority = 1, unsigned int timeout = INFINITE);
	void AddMsg(QueueMsg msg, int priority = 1, unsigned int timeout = INFINITE);
	ErrorCodes::EErrorCode DeleteTask(unsigned long measureId);
	void GetWorkload(const SMetricsMsg::SServerWorkloadRequest& cmd, SMetricsMsg::SServerWorkloadResponse& resp);
	ErrorCodes::EErrorCode RetrieveMeasurement(unsigned long measureId, SMetricsMsg::SGetMeasResp& measurement);
	ErrorCodes::EErrorCode RetrieveSchedule(unsigned long measureId, unsigned long& taskId, unsigned long& key,
		SMetricsMsg::SMeasureCtrlMsgStateResponse::EState& state,
		DATE& startTime, DATE& stopTime, DATE& updateTime, SMetricsMsg* msg);
	ErrorCodes::EErrorCode SaveSchedule(unsigned long& measureId, unsigned long taskId, unsigned long key,
		SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE startTime, DATE stopTime, DATE updateTime, const SMetricsMsg& msg);
	bool SendIqDataResults(CMetricsNet* client, SMetricsMsg& resp, const SMetricsMsg::SMeasureCtrlMsgIqDataRequest& request,
		unsigned long stationId, SMetricsMsg::SMeasureCtrlMsgResultsIqDataResponse& body);
	template<typename T> bool SendResults(CMetricsNet* client, SMetricsMsg& resp, const SMetricsMsg::SMeasureCtrlMsgGenericRequest& request,
		unsigned long msgSubTypeIn, unsigned long msgSubTypeOut, unsigned long stationId, T& body, const char* log);

private:
	// Constants
	static const unsigned int QUEUE_DEPTH = 64;
	static constexpr DATE RETRY_DELAY = 30.0 / Units::SECONDS_PER_DAY; // 30 seconds

	// Types
	typedef CPriorityQueue<QueueMsg, QUEUE_DEPTH> Queue;

	// Functions
	CPersistentData(void);
	virtual ~CPersistentData(void);
	ErrorCodes::EErrorCode DeleteResults(unsigned long measureId, unsigned long msgSubType = 0);
	static std::string GetTaskType(const SMetricsMsg::SHdr& msg);
	void OnAvdCtrl(const SEquipCtrlMsg* msg);
	void OnOccupancyCtrl(const SEquipCtrlMsg* msg);
	void OnOccupancyDfCtrl(const SEquipCtrlMsg* msg);
	void ProcessMsg(DWORD timeout);
	void RecreateTables(void);
	void RestartTasks(void);
	template<typename T> ErrorCodes::EErrorCode RetrieveResult(unsigned long measureId, unsigned long msgSubType, unsigned long offset, T& result);
	void RunScheduledTask(void);
	void SaveIqData(unsigned long measureId, _In_ const SMetricsMsg::SIqDataResp& iqData);
	ErrorCodes::EErrorCode SaveMeasurement(unsigned long measureId, _In_ const SMetricsMsg::SGetMeasResp& measurement);
	ErrorCodes::EErrorCode SaveRestartData(unsigned long measureId, unsigned long msgSubType, _In_bytecount_(size) const void* data, unsigned long size);
	template<typename T> ErrorCodes::EErrorCode SaveResult(unsigned long measureId, unsigned long msgSubType, _In_ const T& result);
	void ScheduleNextTaskAndPurge(void);
	void Thread(void);
	ErrorCodes::EErrorCode UpdateSchedule(unsigned long measureId, SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state,
		DATE updateTime);
	ErrorCodes::EErrorCode UpdateSchedule(unsigned long measureId, SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state,
		DATE stopTime, DATE updateTime);
	ErrorCodes::EErrorCode UpdateSchedule(unsigned long measureId, unsigned long taskId, unsigned long key,
		SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE stopTime, DATE updateTime);
	ErrorCodes::EErrorCode UpdateSchedule(unsigned long measureId, unsigned long taskId, unsigned long key,
		SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE updateTime);

	// Data
	CSingleton<CConfig> m_config;
	std::recursive_mutex m_critSect;
	Queue m_queue;
	unsigned long m_scheduledMeasureId;
	DATE m_scheduledTime;
	std::mutex m_sendResultsCritSect;
	std::thread m_thread;
};


//////////////////////////////////////////////////////////////////////
//
// Save a result in the database
//
template<typename T> ErrorCodes::EErrorCode CPersistentData::SaveResult(unsigned long measureId, unsigned long msgSubType, _In_ const T& result)
{
	assert(result.occHdr.firstChannel + result.occHdr.numChannels <= result.occHdr.numTotalChannels);

	if (result.occHdr.firstChannel == 0)
	{
		m_critSect.lock();
	}

	ErrorCodes::EErrorCode status = ErrorCodes::TASK_COMPLETED;

	switch (int hr = CResultsTbl::UpdateRow(measureId, msgSubType, result.occHdr.firstChannel, result))
	{
	case SQLITE_OK:
	case SQLITE_DONE:
		break;

	default:
		LogHr(hr);
		SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state = SMetricsMsg::SMeasureCtrlMsgStateResponse::FAILED;
		CScheduleTbl::UpdateRow(measureId, state);
		status = ErrorCodes::MEASURE_DATA_NOT_STORED;
		break;
	}

	if(result.occHdr.firstChannel + result.occHdr.numChannels >= result.occHdr.numTotalChannels)
	{
		m_critSect.unlock();
	}

	return status;
}


//////////////////////////////////////////////////////////////////////
//
// Send result messages
//
template<typename T> bool CPersistentData::SendResults(CMetricsNet* client, SMetricsMsg& resp,
	const SMetricsMsg::SMeasureCtrlMsgGenericRequest& request, unsigned long msgSubTypeIn, unsigned long msgSubTypeOut,
	unsigned long stationId, T& body, const char* log)
{
	bool sent = false;
	body.clientRequest = request;
	body.stationId = stationId;
	resp.hdr.msgSubType = msgSubTypeOut;
	resp.hdr.bodySize = sizeof(T); // TODO support variable length messages

	// Get the task state
	unsigned long taskId, key;
	SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state = SMetricsMsg::SMeasureCtrlMsgStateResponse::UNKNOWN;
	DATE startTime, stopTime, updateTime;
	body.statusOfRequest = RetrieveSchedule(request.measureId, taskId, key, state, startTime, stopTime, updateTime, nullptr);

	// Get and send all the records
	std::unique_lock<std::mutex> resultsLock(m_sendResultsCritSect);
	std::unique_lock<std::recursive_mutex> lock(m_critSect);

	for (int hr = CResultsTbl::GetRow(CResultsTbl::FIND, request.measureId, msgSubTypeIn, body.equipMeasurement);
		hr == SQLITE_ROW;
		hr = CResultsTbl::GetRow(CResultsTbl::NEXT, request.measureId, msgSubTypeIn, body.equipMeasurement))
	{
		body.statusOfRequest = ErrorCodes::TASK_COMPLETED;
		if (state != SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING)
		{
			// Release lock if the task is not running (and thus can't be updated between messages)
			lock.unlock();
		}

		CMetricsNet::Send(client, resp, log);
//		printf("CMetricsNet::Send results %u\n", body.statusOfRequest);

		if (state != SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING)
		{
			// Reacquire lock
			lock.lock();
		}

		sent = true;
	}

	return sent;
}


