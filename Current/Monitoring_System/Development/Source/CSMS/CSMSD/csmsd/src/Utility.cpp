/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include <uchar.h>
#include "Utility.h"

static bool QuitAll = false;
static bool CompatibleSoftware = true;
namespace Utility
{
	hid_device *handle;
	unsigned char PACKET[SEND_PACKET_LEN];
}

DATE Utility::CurrentTimeAsDATE(void)
{
	auto ts = Utility::CurrentTimeAsTimespec();
	return (ts.tv_sec + ts.tv_nsec * 1.e-9 + 2209161600.) / 86400.;
}

timespec Utility::CurrentTimeAsTimespec(void)
{
	auto tp = std::chrono::system_clock::now();

	timespec ts;
	ts.tv_sec = std::chrono::system_clock::to_time_t(tp);
	auto tp_sec = std::chrono::system_clock::from_time_t(ts.tv_sec);
	ts.tv_nsec = std::chrono::duration_cast<std::chrono::nanoseconds>(tp - tp_sec).count();
	return ts;
}

Units::Timestamp Utility::CurrentTimeAsTimestamp(void)
{
	return Units::Timestamp(CurrentTimeAsTimespec());
}

std::string Utility::CurrentTimeAsString(ETimeFormat etime)
{
	return TimespecAsString(CurrentTimeAsTimespec(), etime);
}

std::string Utility::TimespecAsString(const timespec& ts, ETimeFormat etime)
{
	tm tmBuffer;
	char buffer[26];
	size_t n = strftime(buffer, 26, "%m/%d/%Y %H:%M:%S", gmtime_r(&ts.tv_sec, &tmBuffer));
	std::string string(buffer, n);
	char text[12] = { 0 };
	switch(etime)
	{
	case MSEC:
//		snprintf(text, 12, ".%03ld", (ts.tv_nsec + 500000) / 1000000);		// Causes a problem when tv_nsec >= 999500000
		snprintf(text, 12, ".%03ld", ts.tv_nsec / 1000000);					// Truncate instead of round
		break;
	case USEC:
//		snprintf(text, 12, ".%06ld", (ts.tv_nsec + 500) / 1000);				// Causes a problem when tv_nsec >= 999999500
		snprintf(text, 12, ".%06ld", ts.tv_nsec / 1000);						// Truncate instead of round
		break;
	case NSEC:
		snprintf(text, 12, ".%09ld", ts.tv_nsec);
		break;
	case SEC:
	default:
		return string;
	}
	string += text;
	return string;
}

int64_t Utility::GetTickCount(void)
{
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
}

void Utility::ToU16String(const char* str, size_t maxSize, std::u16string& out)
{
	const char* src = str;
	for (size_t i = 0; i < maxSize; ++i)
	{
		char16_t tmp;
		auto rv = mbrtoc16(&tmp, src, 1, nullptr);
		if (rv == 0 || rv == (size_t)-1 || rv == (size_t)-2)
			break;	// done
		out.push_back(tmp);		// Don't copy trailing null

		if (rv > 0)
			src += rv;
		// rv must be -3, no input consumed or positive, consuming rv input char's
	}
	return;
}

void Utility::RTrim(std::string& s)
{
	auto pos = s.find_last_not_of(" \t\n\r");
	if (pos != std::string::npos) s.erase(pos + 1);
	else s.clear();
	return;
}

void Utility::LTrim(std::string& s)
{
	auto pos = s.find_first_not_of(" \t\n\r");
	if (pos != std::string::npos) s.erase(0, pos);
	else s.clear();
	return;
}

void Utility::Trim(std::string& s)
{
	LTrim(s);
	RTrim(s);
	return;
}

bool Utility::IsQuitting(void)
{
	sigset_t set;
	if (!QuitAll)
	{
		QuitAll = sigpending(&set) == 0 && sigismember(&set, SIGQUIT) == 1;
		if (QuitAll)
			printf("QuitAll changed to true\n");
		return QuitAll;
	}
	else
	{
		printf("QuitAll already true\n");
		return QuitAll;
	}
}

void Utility::SetQuitAll(void)
{
	QuitAll = true;
}

// check if master and slave software are compatible
void Utility::SetSoftwareCompatibleoff()
{
	CompatibleSoftware = false;
}

bool Utility::GetSoftwareCompatiblFlag()
{
	return CompatibleSoftware;
}

void Utility::Delay(unsigned long usec)
{
	if (usec == 0) return;

	timespec ts, tr;
	ts.tv_sec = 0;
	ts.tv_nsec = 1000 * usec;	// approx 1 msec default
	nanosleep(&ts, &tr);
	return;
}

void LinuxUtility::ShutdownSystem(unsigned long countSec, bool shutdown, void(*PreShutdown)())
{
	// Use the linux shutdown command to make sure that all the rc shutdown tasks are performed

	if (countSec > 300) countSec = 300;		// limit to 5 minutes

	sigset_t blockMask;
	sigemptyset(&blockMask);	// block SIGCHLD
	sigaddset(&blockMask, SIGCHLD);

	sigset_t origMask;
	sigprocmask(SIG_BLOCK, &blockMask, &origMask);

	TRACE("LinuxUtility::ShutdownSystem: forking\n");
	pid_t childPid;
	switch (childPid = fork())
	{
	case -1:	// fork failed
		TRACE("ShutdownSystem fork failed\n");
		break;

	case 0:
		// Child - execute shutdown/reboot
		sigprocmask(SIG_SETMASK, &origMask, nullptr);
		printf("Waiting...\n");
//		sleep(countSec);
		if (PreShutdown != nullptr)
			PreShutdown();
		printf("Starting shutdown\n");
		if (execl("/sbin/shutdown", "/sbin/shutdown", (shutdown ? "-h" : "-r"), "now", NULL) < 0)
		{
			printf("shutdown execution failed\n");
			exit(EXIT_FAILURE);
		}
		// This line will never be reached - execl replaces the executable image
		exit(EXIT_SUCCESS);

	default:
		// Parent
		break;
	}
	// This is the parent process which is not waiting for the shutdown to complete
	sigprocmask(SIG_SETMASK, &origMask, nullptr);	// Unblock SIGCHLD

	// Stop the service
	TRACE("LinuxUtility::ShutdownSystem: parent QUITting\n");
	kill(getpid(), SIGQUIT);	// Send QUIT signal

	return;
}

//ifdef CSMS_2016
#ifdef DISABLE_USB
bool Utility::RFSwitchInit(void)
{
	bool success = true;

	//struct hid_device_info *devs, *cur_dev;

	if (hid_init())
		return false;
	handle = hid_open(VENDOR_ID, PRODUCT_ID, NULL);
	if (!handle) {
		printf("unable to open device\n");
 		return false;
	}
	return success;
}

void Utility::RFSwitchDisconnect(void)
{
	hid_close(handle);

	/* Free static HIDAPI objects. */
	hid_exit();
}


bool Utility::SetRFSwitchPort(unsigned char *State)
{
	int ret;
	unsigned char PACKETreceive[SEND_PACKET_LEN];

	memset(PACKET,0x00,SEND_PACKET_LEN);
	PACKET[0]= int(*State);
#ifdef CSMS_DEBUG
	printf("State is being set to: %02hhx\n", int(*State));
#endif
 	ret = hid_write(handle, PACKET, SEND_PACKET_LEN);
	if (ret < 0) {
		fprintf(stderr, "hid_interrupt_write failed with return code %d\n", ret);
		return false;
	}

	ret = 0; //
	while (ret == 0) {
		ret = hid_read(handle, PACKETreceive, SEND_PACKET_LEN);;
		if (ret == 0)
			printf("waiting...\n");
		if (ret < 0)
			printf("Unable to read()\n");
		#ifdef WIN32
		Sleep(500);
		#else
		usleep(500*1000);
		#endif
	}
	return (ret > 0);
}

bool Utility::GetRFSwitchPort(unsigned char * State)
{
	int ret;
	int nwait = 0;
	unsigned char PACKETreceive[SEND_PACKET_LEN];
	*State = 0;
    memset(PACKET,0x00,SEND_PACKET_LEN);
	PACKET[0]=15; // Ruturn Input Port #
	ret = hid_write(handle, PACKET, SEND_PACKET_LEN);
	if (ret <0) {
		fprintf(stderr, "hid_interrupt_write failed with return code %d\n", ret);
		return false;
	}

	ret = 0;
	while (ret == 0 && nwait < 10) {
		ret = hid_read(handle, PACKETreceive, SEND_PACKET_LEN);
		if (ret == 0)
			nwait++;
		if (ret < 0)
			fprintf(stderr, "GetUSBPort-- Unable to read() %d\n", ret);
		#ifdef WIN32
		Sleep(500);
		#else
		usleep(500*1000);
		#endif
	}
	if (ret > 0) {
		//for (i = 0; i < ret; i++)
		//	printf("%02hhx ", PACKETreceive[i]);
		*State= PACKETreceive[1];
		//printf("state returned = %02hhx", *State);
		return true;
	}
	return false;
}
#endif
