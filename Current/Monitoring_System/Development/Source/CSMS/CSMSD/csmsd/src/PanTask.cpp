/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "Fft.h"
#include "PanTask.h"

CPanTask::CPanTask(_In_opt_ const SEquipCtrlMsg* cmd, _In_opt_ CNetConnection<void>* source, /*EPriority priority,*/ SEquipCtrlMsg::EAnt ant) :
	CTask(cmd, source, /*priority,*/ ant)
{
	m_respBuf.resize(offsetof(SEquipCtrlMsg::SGetPanResp, binData) + C_DEF_NUM_BINS * sizeof(SEquipCtrlMsg::SGetPanResp::binData));
	auto pResp = reinterpret_cast<SEquipCtrlMsg*>(&(m_respBuf[0]));

	// Initialize pan response
	pResp->hdr = m_msgHdr;
	pResp->hdr.msgSubType = SEquipCtrlMsg::GET_PAN_RESPONSE;
	pResp->body.getPanResp.status = ErrorCodes::SUCCESS;

	pResp->body.getPanResp.dateTime = 0;
	pResp->body.getPanResp.freq = cmd->body.getPanCmd.freq;
	pResp->body.getPanResp.binSize = Units::Frequency::Raw::ZERO;
	pResp->body.getPanResp.numBins = 0;
	pResp->body.getPanResp.rcvrAtten = 0;
	pResp->body.getPanResp.powerDbm = 0;

	return;
}

CPanTask::~CPanTask(void)
{
	return;
}

//////////////////////////////////////////////////////////////////////
//
// Add pan display result to task and send to client
//
void CPanTask::AddPan(_In_ const Ne10F32Vec& watts)
{
	auto multiPanData = (GetNumBlocks() > 1);
	size_t numBins = watts.size();
	auto updtResp = true;
	double dateTime;

	if(multiPanData)
	{
		if(!m_upPanData)
		{
			m_upPanData = std::unique_ptr<SPanData>(new SPanData);
			m_upPanData->numBins = numBins;

			//Reserved the expected bin size.
			m_upPanData->watts.reserve(2*numBins);
			m_upPanData->watts = watts;
			m_upPanData->dateTime = GetBlockState().sampleTime.GetDATE();
			updtResp = false;
		}
		else
		{
			numBins += m_upPanData->numBins;
			dateTime = m_upPanData->dateTime;
		}
	}
	else
	{
		dateTime = GetBlockState().sampleTime.GetDATE();
	}

	if(updtResp)
	{
		Ne10F32Vec dbm(CFft::WattsToDbm(watts));
		float gainAdjdB = 192.0f;

		if(m_config->IsShfExtUsed(m_ant, GetTuneFreq()))
		{
			///@note: It is assumed SHF Extension always connected to RF2 (Aux1) input.
			gainAdjdB -= (m_config->GetShfExtensionGain(GetTuneFreq()) -
				  m_config->GetShfExtIfCableLoss(CConfig::ERfPort::C_RF2));
		}

		dbm += gainAdjdB;

		// Build pan response
		auto& taskParams = GetTaskParams();
		auto& procParams = GetProcParams();
		size_t length = offsetof(SEquipCtrlMsg::SGetPanResp, binData) + numBins * sizeof(SEquipCtrlMsg::SGetPanResp::binData);

		if (length > m_respBuf.size())
		{
		    m_respBuf.resize(length);
		}


		auto pResp = reinterpret_cast<SEquipCtrlMsg*>(&(m_respBuf[0]));
		pResp->body.getPanResp.dateTime = dateTime;
		pResp->body.getPanResp.freq = taskParams.startFreq.GetRaw();
		pResp->body.getPanResp.binSize = procParams.GetBinSize().GetRaw();
		pResp->body.getPanResp.numBins = static_cast<unsigned short>(numBins);
		pResp->body.getPanResp.rcvrAtten = GetBlockState().rcvrAtten;
		pResp->hdr.bodySize =  offsetof(SEquipCtrlMsg::SGetPanResp, binData) + pResp->body.getPanResp.numBins;
		size_t secondPanBins;

		if(multiPanData)
		{
			Ne10F32Vec initialDbm(CFft::WattsToDbm(m_upPanData->watts));
			ne10sConvert(&initialDbm[0], &(pResp->body.getPanResp.binData[0]),
			             m_upPanData->numBins, 0);
			secondPanBins = numBins - m_upPanData->numBins;
			ne10sConvert(&dbm[0], &(pResp->body.getPanResp.binData[m_upPanData->numBins]),
			             secondPanBins, 0);
		}
		else
		{
			ne10sConvert(&dbm[0], &pResp->body.getPanResp.binData[0], numBins, 0);
		}

		// Get power in demod bandwidth
		size_t demodBins = size_t(numBins * m_powerBw / taskParams.bw);

		if (demodBins > numBins)
		{
			demodBins = numBins;
		}
		else if (demodBins == 0)
		{
			demodBins = 1;
		}

		if(multiPanData)
		{
			m_upPanData->watts.resize(numBins);
			ne10sCopy(&watts[0], &(m_upPanData->watts[m_upPanData->numBins]), secondPanBins);
			Ne10F32Vec power(&(m_upPanData->watts[(numBins - demodBins) / 2]), demodBins);
			pResp->body.getPanResp.powerDbm = static_cast<unsigned char>(floor(10.f * log10(power.sum() / CFft::GetEnbw(2*procParams.sampleSize)) + 222.5f)); // dBm + 192
			m_upPanData.reset();	//PAN data sent reset the allocated buffer.
		}
		else
		{
			Ne10F32Vec power(&watts[(numBins - demodBins) / 2], demodBins);
			pResp->body.getPanResp.powerDbm = static_cast<unsigned char>(floor(10.f * log10(power.sum() / CFft::GetEnbw(procParams.sampleSize)) + 222.5f)); // dBm + 192
		}

#ifdef CSMS_DEBUG
		const static size_t C_MAX_LCL_BIN =  512;
		static Units::Frequency l_prevFreq(0);
		static unsigned char l_binData[C_MAX_LCL_BIN];

		if(l_prevFreq != taskParams.startFreq)
		{
			l_prevFreq = taskParams.startFreq;
			size_t copySize = (numBins < C_MAX_LCL_BIN ? numBins : C_MAX_LCL_BIN);
			memcpy(l_binData, &(pResp->body.getPanResp.binData[0]), copySize);
			TRACE("CPanTask::AddPan: freq =%fMHz, numBins = %u demodBins = %u binSize = %fHz rcvrAtten = %u\n",
			      l_prevFreq.Hz<float>()/1000000, numBins, demodBins, procParams.GetBinSize().Hz<float>(),
			      unsigned(pResp->body.getPanResp.rcvrAtten));

		}
#endif


	}


	return;
}


//////////////////////////////////////////////////////////////////////
//
// Create task from message and return shared pointer
//
CPanTask::PanTask CPanTask::Create(_In_opt_ const SEquipCtrlMsg* cmd, _In_opt_ CNetConnection<void>* source, /*EPriority priority,*/ SEquipCtrlMsg::EAnt ant)
{
	PanTask task(new CPanTask(cmd, source, /*priority,*/ ant), Deleter());
	task->RegisterTask(false);

	return task;
}


