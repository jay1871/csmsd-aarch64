/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

class CStatsNoHist
{
public:
	//Functions
	CStatsNoHist(void);
	~CStatsNoHist(void);
	void AddValue(double value);
	void Clear(void);
	unsigned int GetCount(void) const { return m_count; }
	double GetMax(void) const { return m_max; }
	double GetMean(void ) const { return m_count > 0 ? m_sum / m_count : 0; }
	double GetMin(void) const { return m_min; }
	template<size_t N> void GetProbDist(unsigned char (&dist)[N]) const;
	double GetRms(void) const { return m_count > 0 ? sqrt(m_sumSq / m_count) : 0; }
	double GetStdDev(void) const { return m_count > 1 ? sqrt(GetVar()) : 0; }
	double GetVar(void) const { return m_count > 1 ? (m_count * m_sumSq - m_sum * m_sum) / (m_count * (m_count - 1)) : 0; }

private:
	// Data
	//@note: The data fields of this class must be of type intrinsic or class
	//       or struct that has only intrinsic fields, because this class is
	//       being byte saved to database.
	unsigned int m_count;
	double m_max;
	double m_min;
	double m_sum;
	double m_sumSq;
};
