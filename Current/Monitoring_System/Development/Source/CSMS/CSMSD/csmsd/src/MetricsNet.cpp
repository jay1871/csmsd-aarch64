/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/
#include "stdafx.h"
#include <malloc.h>
#include <uchar.h>
#include "Metrics.h"
#include "MetricsNet.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
SMetricsMsg::VersionMap CMetricsNet::m_compressionMap;
CRWLock CMetricsNet::m_critSect;
SMetricsMsg::VersionMap CMetricsNet::m_versionMap;


//////////////////////////////////////////////////////////////////////
//
// Constructor for server
//
CMetricsNet::CMetricsNet(LPCTSTR service) :
	CNetConnection<CMetricsNet>(service),
	m_metrics(),
	m_equipControl(),
	m_maxRecvBodySize(sizeof(SMetricsMsg)),
	m_recvMsg(nullptr),
	m_sendMsg(nullptr)
{
	// Build message version map
	for(unsigned int entry = 0; entry < std::extent<decltype(SMetricsMsg::VERSION_DATA)>::value; ++entry)
	{
		m_versionMap[SMetricsMsg::VERSION_DATA[entry].msgId] = SMetricsMsg::VERSION_DATA[entry].version;
	}

	// Build compression data map
	for(unsigned int entry = 0; entry < std::extent<decltype(SMetricsMsg::COMPRESSION_DATA)>::value; ++entry)
	{
		m_compressionMap[SMetricsMsg::COMPRESSION_DATA[entry].msgId] = SMetricsMsg::COMPRESSION_DATA[entry].version;
	}

	Startup();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Constructor - dynamic
//
CMetricsNet::CMetricsNet(SOCKET socket, _In_ CMetricsNet*) :
#ifdef CSMS_DEBUG
			CNetConnection<CMetricsNet>(socket),
#else
			CNetConnection<CMetricsNet>(socket, 30000, 10000),
#endif
	m_metrics(),
	m_equipControl(),
	m_maxRecvBodySize(sizeof(SMetricsMsg)),
	m_recvMsg(new SMetricsMsg),
	m_sendMsg(malloc(1024))
//	m_usesVersions(false)
{
	Startup(true);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CMetricsNet::~CMetricsNet(void)
{
	TRACE("CMetricsNet dtor entered %d\n", GetMode());
	Shutdown();
	CLockGuard lock(m_critSect);

	if(GetMode() == DYNAMIC)
	{
		free(m_sendMsg);
		m_sendMsg = nullptr;
		delete m_recvMsg;
	}
	TRACE("CMetricsNet dtor %d\n", GetMode());

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the body size. Protects against buffer overruns
//
size_t CMetricsNet::BodySize(_In_ const void* hdr) const
{
	return static_cast<const SMetricsMsg::SHdr*>(hdr)->bodySize;
}


//////////////////////////////////////////////////////////////////////
//
// Downconvert a message to the requested version
//
bool CMetricsNet::DownConvert(_Inout_ const SMetricsMsg*& msg)
{
	bool rv = true;
	SMetricsMsg* newMsg = new SMetricsMsg; // Note: fixed length messages only
	newMsg->hdr = msg->hdr;

	switch(msg->hdr.msgType)
	{
	case SMetricsMsg::ANT_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SMetricsMsg::GET_ANT_RESPONSE:
		case SMetricsMsg::SET_ANT_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
//				memcpy(&newMsg->body.antGetSetCtrlRespV0, &msg->body.antGetSetCtrlResp, msg->hdr.bodySize);
				newMsg->body.antGetSetCtrlRespV0 = msg->body.antGetSetCtrlResp;
				break;

			default:
				rv = false;
				break;
			}
			break;

		default:
			rv = false;
			break;
		}
		break;

	case SMetricsMsg::AUTOVIOLATE_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SMetricsMsg::AVD_BWMEAS_MEASURE:
		case SMetricsMsg::AVD_FREQMEAS_MEASURE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg->body.measureAvdResultsResponseV0 = msg->body.measureAvdResultsResponse;
				newMsg->hdr.bodySize = sizeof(SMetricsMsg::SMeasureAvdResultsResponseV0);
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::AVD_CHANNEL_MEASURE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg->body.measureChanResultsResponseV0 = msg->body.measureChanResultsResponse;
				newMsg->hdr.bodySize = sizeof(SMetricsMsg::SMeasureChanResultsResponseV0);
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::AVD_FREQ_MEASURE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg->body.measureFreqResultsResponseV0 = msg->body.measureFreqResultsResponse;
				newMsg->hdr.bodySize = sizeof(SMetricsMsg::SMeasureFreqResultsResponseV0);
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::AVD_DELETE_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.measureCtrlMsgGenericResponseV0 = msg->body.measureCtrlMsgGenericResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::AVD_STATE_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.measureCtrlMsgStateResponseV0 = msg->body.measureCtrlMsgStateResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::SCHED_AVD_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.schedMsgResponseV0 = msg->body.schedMsgResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::REQUEST_FAIL_NOT_FOUND:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.measureErrorResponseV0 = msg->body.measureErrorResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		default:
			rv = false;
			break;
		}

		break;

	case SMetricsMsg::BIST_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SMetricsMsg::BIST_RESULT_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.measureCtrlMsgBistResponseV0 = msg->body.measureCtrlMsgBistResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::BIST_SCHED_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.schedMsgResponseV0 = msg->body.schedMsgResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		default:
			rv = false;
			break;
		}
		break;

	case SMetricsMsg::DEMOD_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SMetricsMsg::SET_RCVR_RESP:
		case SMetricsMsg::SET_PAN_PARA_RESP:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.genericRespV0 = msg->body.genericResp;
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SEquipCtrlMsg::SET_AUDIO_PARAMS_RESP:
			switch(msg->hdr.respVersion)
			{
				case 1: // from v2 to v1
					newMsg = new SMetricsMsg;
					newMsg->hdr = msg->hdr;
					newMsg->body.audioParamsRespV1 = msg->body.audioParamsRespV2;
					newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SAudioParamsRespV1);
				break;
				default:
					THROW_LOGIC_ERROR();
			}
			break;
		case SMetricsMsg::FREE_AUDIO_CHANNEL_RESP:
		case SMetricsMsg::GET_AUDIO_PARAMS_RESP:
			switch(msg->hdr.respVersion)
			{
			case 0:
				//Should never be version 0 as it starts with version 1.
				rv = false;
				break;

			default:
				rv = false;
				break;
			}
			break;

		default:
			rv = false;
			break;
		}
		break;

	case SMetricsMsg::MEASURE_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SMetricsMsg::BAND_RESPONSE:
		{
			SMetricsMsg::SGetBandRespV5 v5 = msg->body.getBandResp;

			if (msg->hdr.respVersion == 5)
			{
				newMsg->body.getBandRespV5 = v5;
				newMsg->hdr.bodySize = sizeof(SMetricsMsg::SGetBandRespV5);
			}
			else
			{
				SMetricsMsg::SGetBandRespV4 v4 =v5;
				if (msg->hdr.respVersion == 4)
				{
					newMsg->body.getBandRespV4 = v4;
					newMsg->hdr.bodySize = sizeof(SMetricsMsg::SGetBandRespV4);
				}
				else
				{
					SMetricsMsg::SGetBandRespV3 V3 = v4;
					if (msg->hdr.respVersion == 3)
					{
						newMsg->body.getBandRespV3 = V3;
						newMsg->hdr.bodySize = sizeof(SMetricsMsg::SGetBandRespV3);
					}
					else
					{
						SMetricsMsg::SGetBandRespV2 V2 = V3;
						if (msg->hdr.respVersion == 2)
						{
							newMsg->body.getBandRespV2 = V2;
							newMsg->hdr.bodySize = sizeof(SMetricsMsg::SGetBandRespV2);
						}
						else
						{
							SMetricsMsg::SGetBandRespV1 V1 = V2;
							if (msg->hdr.respVersion == 1)
							{
								newMsg->body.getBandRespV1 = V1;
								newMsg->hdr.bodySize = sizeof(SMetricsMsg::SGetBandRespV1);
							}
							else if (msg->hdr.respVersion == 0)
							{
								newMsg->body.getBandRespV0 = V1;
								newMsg->hdr.bodySize = sizeof(SMetricsMsg::SGetBandRespV0);
							}
							else
							{
								rv = false;
								break;
							}
						}
					}
				}
			}

			break;
		}
		case SMetricsMsg::BAND_RESPONSE_NETTED:
		{
			SMetricsMsg::SGetBandRespNettedV5 v5 = msg->body.getBandRespNetted;

			if (msg->hdr.respVersion == 5)
			{
				newMsg->body.getBandRespNettedV5 = v5;
				newMsg->hdr.bodySize = sizeof(SMetricsMsg::SGetBandRespNettedV5);
			}
			else
			{
				SMetricsMsg::SGetBandRespNettedV4 v4 = v5;
				if (msg->hdr.respVersion == 4)
				{
					newMsg->body.getBandRespNettedV4 = v4;
					newMsg->hdr.bodySize = sizeof(SMetricsMsg::SGetBandRespNettedV4);
				}
				else
				{
					SMetricsMsg::SGetBandRespNettedV3 V3 = v4;
					if (msg->hdr.respVersion == 3)
					{
						newMsg->body.getBandRespNettedV3 = V3;
						newMsg->hdr.bodySize = sizeof(SMetricsMsg::SGetBandRespNettedV3);
					}
					else
					{
						SMetricsMsg::SGetBandRespNettedV2 V2 = V3;
						if (msg->hdr.respVersion == 2)
						{
							newMsg->body.getBandRespNettedV2 = V2;
							newMsg->hdr.bodySize = sizeof(SMetricsMsg::SGetBandRespNettedV2);
						}
						else
						{
							SMetricsMsg::SGetBandRespNettedV1 V1 = V2;
							if (msg->hdr.respVersion == 1)
							{
								newMsg->body.getBandRespNettedV1 = V1;
								newMsg->hdr.bodySize = sizeof(SMetricsMsg::SGetBandRespNettedV1);
							}
							else if (msg->hdr.respVersion == 0)
							{
								newMsg->body.getBandRespNettedV0 = V1;
								newMsg->hdr.bodySize = sizeof(SMetricsMsg::SGetBandRespNettedV0);
							}
							else
							{
								rv = false;
								break;
							}
						}
					}
				}
			}

			break;
		}

		case SMetricsMsg::DELETE_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.measureCtrlMsgGenericResponseV0 = msg->body.measureCtrlMsgGenericResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::MEASUREMENT_RESPONSE:
		{
			SMetricsMsg::SMeasureCtrlMsgResultsResponseV2 V2 = msg->body.measureCtrlMsgResultsResponse;
			if(msg->hdr.respVersion == 2)
			{
				newMsg->body.measureCtrlMsgResultsResponseV2 = V2;
				newMsg->hdr.bodySize = sizeof(SMetricsMsg::SMeasureCtrlMsgResultsResponseV2);
			}
			else
			{
				SMetricsMsg::SMeasureCtrlMsgResultsResponseV1 V1 = V2;
				if(msg->hdr.respVersion == 1)
				{
					newMsg->body.measureCtrlMsgResultsResponseV1 = V1;
					newMsg->hdr.bodySize = sizeof(SMetricsMsg::SMeasureCtrlMsgResultsResponseV1);
				}
				else if(msg->hdr.respVersion == 0)
				{
					newMsg->body.measureCtrlMsgResultsResponseV0 = V1;
					newMsg->hdr.bodySize = sizeof(SMetricsMsg::SMeasureCtrlMsgResultsResponseV0);
				}
				else
				{
					rv = false;
					break;
				}
			}

			break;
		}

		case SMetricsMsg::MEASUREMENT_IQDATA_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg->body.measureCtrlMsgResultsIqDataResponseV0 = msg->body.measureCtrlMsgResultsIqDataResponse;
				newMsg->hdr.bodySize = sizeof(SMetricsMsg::SMeasureCtrlMsgResultsIqDataResponseV0);
				break;

			default:
				rv = false;
				break;
			}

			break;

		case SMetricsMsg::SERVER_LOCATION_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.serverGpsResponseV0 = msg->body.serverGpsResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::SERVER_WORKLOAD_RESPONSE_MSG:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.serverWorkloadResponse = msg->body.serverWorkloadResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::HEADING_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Should never be version 0 PAN_CAPABILITES_RESPONSE as it starts with version 1.
				rv = false;
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::SCHED_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.schedMsgResponseV0 = msg->body.schedMsgResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::REQUEST_FAIL_NOT_FOUND:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.measureErrorResponseV0 = msg->body.measureErrorResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		default:
			rv = false;
			break;
		}

	case SMetricsMsg::MEASUREMENT_ERROR_REPORTA:
		switch(msg->hdr.respVersion)
		{
		case 0:
			// Should never be version 0 MEASUREMENT_ERROR_REPORTA as it starts with version 1.
			rv = false;
			break;
		default:
			rv = false;
			break;
		}

		break;

	case SMetricsMsg::MEASUREMENT_GREETING_RES:
		switch(msg->hdr.respVersion)
		{
		case 0:
			// Special case - the bodies are actually the same (so no conversion)
//			memcpy(&newMsg->body.greetingMeasurementRespMsgV0, &msg->body.greetingMeasurementRespMsg, msg->hdr.bodySize);
			newMsg->body.greetingMeasurementRespMsgV0 = msg->body.greetingMeasurementRespMsg;
			break;

		default:
			rv = false;
			break;
		}

		break;

	case SMetricsMsg::OCCUPANCY_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SMetricsMsg::OCC_FREQ_MEASURE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg->body.measureFreqResultsResponseV0 = msg->body.measureFreqResultsResponse;
				newMsg->hdr.bodySize = sizeof(SMetricsMsg::SMeasureFreqResultsResponseV0);
				break;

			default:
				rv = false;
				break;
			}

			break;

		case SMetricsMsg::OCC_CHANNEL_MEASURE:
		case SMetricsMsg::EFLD_CHANNEL_MEASURE:
		case SMetricsMsg::OCC_TIMEOFDAY_MEASURE:
		case SMetricsMsg::MSGLEN_CHANNEL_MEASURE:
		case SMetricsMsg::OCC_SPECGRAM_MEASURE:
		case SMetricsMsg::OCC_EFLD_TIMEOFDAY_MEASURE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg->body.measureChanResultsResponseV0 = msg->body.measureChanResultsResponse;
				newMsg->hdr.bodySize = sizeof(SMetricsMsg::SMeasureChanResultsResponseV0);
				break;

			default:
				rv = false;
				break;
			}

			break;

		case SMetricsMsg::MSGLEN_DIST_MEASURE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg->body.measureMsgLenDistResultsResponseV0 = msg->body.measureMsgLenDistResultsResponse;
				newMsg->hdr.bodySize = sizeof(SMetricsMsg::SMeasureMsgLenDistResultsResponseV0);
				break;

			default:
				rv = false;
				break;
			}

			break;

		case SMetricsMsg::SCHED_OCCUP_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.schedMsgResponseV0 = msg->body.schedMsgResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::OCCUP_STATE_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.measureCtrlMsgStateResponseV0 = msg->body.measureCtrlMsgStateResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::OCCUP_DELETE_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.measureCtrlMsgGenericResponseV0 = msg->body.measureCtrlMsgGenericResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::REQUEST_FAIL_NOT_FOUND:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.measureErrorResponseV0 = msg->body.measureErrorResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		default:
			rv = false;
			break;
		}

		break;

	case SMetricsMsg::OCCUPANCYDF_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SMetricsMsg::SCANDF_FREQ_MEASURE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg->body.measureFreqResultsResponseV0 = msg->body.measureFreqResultsResponse;
				newMsg->hdr.bodySize = sizeof(SMetricsMsg::SMeasureFreqResultsResponseV0);
				break;

			default:
				rv = false;
				break;
			}

			break;

		case SMetricsMsg::SCANDF_DATA_MEASURE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg->body.measureScanDfDataResultsResponseV0 = msg->body.measureScanDfDataResultsResponse;
				newMsg->hdr.bodySize = sizeof(SMetricsMsg::SMeasureScanDfDataResultsResponseV0);
				break;

			default:
				rv = false;
				break;
			}

			break;

		case SMetricsMsg::SCHED_SCANDF_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg->body.schedMsgScanDfResponseV0 = msg->body.schedMsgScanDfResponse;
				newMsg->hdr.bodySize = sizeof(SMetricsMsg::SSchedMsgScanDfResponseV0);
				break;

			default:
				rv = false;
				break;
			}

			break;

		case SMetricsMsg::SCANDF_STATE_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.measureCtrlMsgStateResponseV0 = msg->body.measureCtrlMsgStateResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::SCANDF_DELETE_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.measureCtrlMsgGenericResponseV0 = msg->body.measureCtrlMsgGenericResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

		case SMetricsMsg::REQUEST_FAIL_NOT_FOUND:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.measureErrorResponseV0 = msg->body.measureErrorResponse;
				break;

			default:
				rv = false;
				break;
			}
			break;

			default:
				rv = false;
				break;
		}

		break;

	case SMetricsMsg::PAN_DISP_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SMetricsMsg::GET_PAN_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg->body.getPanRespV0 = msg->body.getPanResp;
				newMsg->hdr.bodySize = sizeof(SMetricsMsg::SGetPanRespV0);
				break;

			default:
				rv = false;
				break;
			}

			break;

		default:
			rv = false;
			break;
		}

		break;

	case SMetricsMsg::STATUS_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SMetricsMsg::GET_MSG_VERSIONS_RESP:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
//				memcpy(&newMsg->body.getMsgVersionsDataV0, &msg->body.getMsgVersionsData, msg->hdr.bodySize);
				newMsg->body.getMsgVersionsDataV0 = msg->body.getMsgVersionsData;
				break;

			default:
				rv = false;
				break;
			}

			break;

		case SMetricsMsg::GET_FAULT_RESP:
			switch(msg->hdr.respVersion)
			{
			case 0:
				//Should never be version 0 GET_FAULT_RESP as it starts with version 1.
				rv = false;
				break;
			default:
				rv = false;
				break;
			}

			break;

		case SMetricsMsg::GET_CSMS_FAULT_RESP:
			switch(msg->hdr.respVersion)
			{
			case 0:
				//Should never be version 0 GET_FAULT_RESP as it starts with version 1.
				rv = false;
				break;
			case 1:	// from v2 to v1
				newMsg->body.getCsmsFaultRespV1 = msg->body.getCsmsFaultResp;
				newMsg->hdr.bodySize = sizeof(SMetricsMsg::SGetCsmsFaultRespV1);
				break;
			default:
				rv = false;
				break;
			}

			break;

		case SMetricsMsg::METRIC_ANT_NAME_CHANGE_RESP:
			switch(msg->hdr.respVersion)
			{
			case 0:
				//Should never be version 0 METRIC_ANT_NAME_CHANGE_RESP as it starts with version 1.
				rv = false;
				break;

			default:
				rv = false;
				break;
			}

			break;

		default:
			rv = false;
			break;
		}

		break;

	case SMetricsMsg::SYSTEM_STATE_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SMetricsMsg::SET_SYSTEM_STATE_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				// Special case - the bodies are actually the same (so no conversion)
				newMsg->body.systemStateSetCtrlRespV0 = msg->body.systemStateSetCtrlResp;
				break;

			default:
				rv = false;
				break;
			}

			break;

		default:
			rv = false;
			break;
		}

		break;

	default:
		rv = false;
		break;
	}

	msg = newMsg;

	return rv;
}


//////////////////////////////////////////////////////////////////////
//
// Get local IP adress for this connection
//
SOCKADDR_STORAGE CMetricsNet::GetLocalAddress(void) const
{
	SOCKADDR_STORAGE name;
	socklen_t namelen = sizeof(name);

	if(getsockname(GetSocket(), reinterpret_cast<sockaddr*>(&name), &namelen) ==  SOCKET_ERROR)
	{
		name.ss_family = AF_UNSPEC;
	}

	return name;
}


//////////////////////////////////////////////////////////////////////
//
// Helper
//
bool CMetricsNet::IsBodyCompressed(_In_ const SMetricsMsg& msg)
{
	const SMetricsMsg::SVersionKey key = { msg.hdr.msgType, msg.hdr.msgSubType };
	SMetricsMsg::CompressionMap::const_iterator value = m_compressionMap.find(key);

	if(value != m_compressionMap.end() && ((value->second.cmdVersion != 0xff && msg.hdr.cmdVersion >= value->second.cmdVersion) ||
		(value->second.respVersion != 0xff && msg.hdr.respVersion >= value->second.respVersion)))
	{
		return false;
	}
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CMetricsNet::OnClose(int errorCode)
{
	m_metrics->OnClientClose(this);

	if(auto peer = GetPeerAddress())
	{
		CLog::Log(CLog::INFORMATION, "%s disconnected from Metrics port, code = %d", peer->ai_canonname, errorCode);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CMetricsNet::OnConnect(void)
{
	auto peer = GetPeerAddress();
	m_metrics->OnClientConnect(this, peer);

	if (peer != nullptr)
	{
		CLog::Log(CLog::INFORMATION, "%s connected to Metrics port", peer->ai_canonname);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CMetricsNet::OnMessage(_In_bytecount_(size) const void* msg, size_t size)
{
	const SMetricsMsg::SHdr* hdr = &static_cast<const SMetricsMsg*>(msg)->hdr;
	ASSERT(size == offsetof(SMetricsMsg, body) + hdr->bodySize);
	auto peer = GetPeerAddress();
	LPCTSTR peerName(peer == nullptr ? _T("Unknown") : peer->ai_canonname);

	if(size != offsetof(SMetricsMsg, body) + hdr->bodySize)
	{
		// Corrupt message
		Close();
		return;
	}

	memcpy(&m_recvMsg->hdr, hdr, sizeof(SMetricsMsg::SHdr));

	// Check command version and report error
	SMetricsMsg::SVersionKey key = { hdr->msgType, hdr->msgSubType };
	SMetricsMsg::VersionMap::const_iterator entry = m_clientVersionMap.find(key);

	if(m_clientVersionMap.size() > 0 &&
		((entry == m_clientVersionMap.end() && (hdr->cmdVersion > 0 || hdr->respVersion > 0)) ||
		(entry != m_clientVersionMap.end() &&
		(entry->second.cmdVersion != hdr->cmdVersion || entry->second.respVersion != hdr->respVersion))))
	{
		// Invalid version (client mismatch with itself!)
		if(entry == m_clientVersionMap.end())
		{
			m_recvMsg->hdr.cmdVersion = 0;
			m_recvMsg->hdr.respVersion = 0;
		}
		else
		{
			m_recvMsg->hdr.cmdVersion = entry->second.cmdVersion;
			m_recvMsg->hdr.respVersion = entry->second.respVersion;
		}

		CLog::Log(CLog::ERRORS, "Metrics: bad version from %s: type=%u subtype=%lu cmdVer=%u respVer=%u", peerName,
				hdr->msgType, hdr->msgSubType, hdr->cmdVersion, hdr->respVersion);
		SendError(m_recvMsg->hdr, ErrorCodes::INVALIDVERSION);

		return;
	}

	entry = m_versionMap.find(key);

	if((entry == m_versionMap.end() && hdr->cmdVersion > 0) ||
		(entry != m_versionMap.end() && entry->second.cmdVersion < hdr->cmdVersion))
	{
		// Invalid version (client mismatch with me)
		if(entry == m_versionMap.end())
		{
			m_recvMsg->hdr.cmdVersion = 0;
			m_recvMsg->hdr.respVersion = 0;
		}
		else
		{
			m_recvMsg->hdr.cmdVersion = entry->second.cmdVersion;
			m_recvMsg->hdr.respVersion = entry->second.respVersion;
		}

		CLog::Log(CLog::ERRORS, "Metrics: bad version from %s: type=%u subtype=%lu cmdVer=%u", peerName,
				hdr->msgType, hdr->msgSubType, hdr->cmdVersion);
		SendError(m_recvMsg->hdr, ErrorCodes::INVALIDVERSION);

		return;
	}

	// Check for VCP control mode here
	if (m_equipControl->VCPControlClient() != nullptr)
	{
		SendError(m_recvMsg->hdr, ErrorCodes::CMDINVALIDVCPMODE);
	}

	bool errorReturn = false;

	try
	{
		if(hdr->bodySize != 0)
		{
			if(IsBodyCompressed(*m_recvMsg))
			{
				// Compressed messages are not supported
				if (m_recvMsg->hdr.msgType == SMetricsMsg::STATUS_CTRL && m_recvMsg->hdr.msgSubType == SMetricsMsg::GET_MSG_VERSIONS)
				{
					SMetricsMsg::SHdr msg(m_recvMsg->hdr);
					msg.msgSubType = SMetricsMsg::GET_MSG_VERSIONS_RESP;
					msg.bodySize = 0;

					Send(&msg, sizeof(msg));
//					CLog::Log(CLog::INFORMATION, "Metrics: sent GET_MSG_VERSIONS_RESP(0) to %s", peerName);
					return;
				}
				CLog::Log(CLog::ERRORS, "received unsupported compressed message (%u,%lu)", m_recvMsg->hdr.msgType, m_recvMsg->hdr.msgSubType);
				SendError(m_recvMsg->hdr, ErrorCodes::INVALIDVERSION);
				return;
			}
			// Copy body
			if(hdr->bodySize > m_maxRecvBodySize)
			{
				m_maxRecvBodySize = hdr->bodySize;
#ifdef _DEBUG
				SMetricsMsg::Realloc(m_recvMsg, offsetof(SMetricsMsg, body) + m_maxRecvBodySize, __FILE__, __LINE__);
#else
				SMetricsMsg::Realloc(m_recvMsg, offsetof(SMetricsMsg, body) + m_maxRecvBodySize);
#endif
			}

			memcpy(&m_recvMsg->body, &static_cast<const SMetricsMsg*>(msg)->body, hdr->bodySize);
		}

		if(entry != m_versionMap.end() && entry->second.cmdVersion > hdr->cmdVersion)
		{
			// Upconvert to latest version
			UpConvert(m_recvMsg);
		}

		// Pass to Metrics or EquipControl
		switch(m_recvMsg->hdr.msgType)
		{
		case SMetricsMsg::ANT_CTRL:
			m_metrics->OnAntCtrl(*m_recvMsg, this);
			break;

//		case SMetricsMsg::AUDIO_SWITCH:
//			m_metrics->OnAudioSwitch(*m_recvMsg, this);
//			break;

		case SMetricsMsg::AUTOVIOLATE_CTRL:
			m_metrics->OnAvdCtrl(*m_recvMsg, this);
			break;

		case SMetricsMsg::BIST_CTRL:
			m_metrics->OnBistCtrl(*m_recvMsg, this);
			break;

		case SMetricsMsg::DEMOD_CTRL:
			m_metrics->OnDemodCtrl(*m_recvMsg, this);
			break;

		case SMetricsMsg::MEASURE_CTRL:
			m_metrics->OnMeasureCtrl(*m_recvMsg, this);
			break;

		case SMetricsMsg::MEASUREMENT_GREETING_REQ:
			m_metrics->OnMeasurementGreetingReq(*m_recvMsg, this);
			break;

		case SMetricsMsg::MEASUREMENT_PAN_REQ:
			m_metrics->OnMeasurementPanReq(*m_recvMsg, this);
			break;

		case SMetricsMsg::OCCUPANCY_CTRL:
			m_metrics->OnOccupancyCtrl(*m_recvMsg, this);
			break;

		case SMetricsMsg::OCCUPANCYDF_CTRL:
			m_metrics->OnOccupancyDfCtrl(*m_recvMsg, this);
			break;

		case SMetricsMsg::STATUS_CTRL:
			OnStatusCtrl(*m_recvMsg);
			break;

		case SMetricsMsg::SYSTEM_STATE_CTRL:
			m_metrics->OnSystemStateCtrl(*m_recvMsg, this);
			break;

//		case SMetricsMsg::SOUNDER_CTRL:
//			m_metrics->OnSounderCtrl(*m_recvMsg, this);
//			break;

		default:
			CLog::Log(CLog::ERRORS, "Metrics: invalid message type from %s: type %u subtype %lu", peerName,
					hdr->msgType, hdr->msgSubType);
			throw ErrorCodes::INVALIDMSGTYPE;
		}
	}
	catch(ErrorCodes::EErrorCode& error)
	{
		SendError(m_recvMsg->hdr, error);
	}
	// Catch all the possible fatal exceptions in as much detail as possible
	catch(const std::exception& ex)
	{
		CLog::Log(CLog::ERRORS, "Metrics: service failed %s", ex.what());
		errorReturn = true;
	}
	catch(...)
	{
		CLog::Log(CLog::ERRORS, "Metrics: service failed An unexpected error occurred");
		errorReturn = true;
	}

	if(errorReturn)
	{
		throw std::runtime_error("Fatal error");
//		CService::Instance()->OnAbort(errorReturn);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle a STATUS_CTRL message
//
void CMetricsNet::OnStatusCtrl(_In_ const SMetricsMsg& msg)
{
	auto peer = GetPeerAddress();
	LPCTSTR peerName(peer == nullptr ? _T("Unknown") : peer->ai_canonname);

	switch(msg.hdr.msgSubType)
	{
	case SMetricsMsg::PING_SERVER:
		// Ignore
		break;

	case SMetricsMsg::GET_MSG_VERSIONS:
		{
//			CLog::Log(CLog::INFORMATION, "Metrics: received from %s: GET_MSG_VERSIONS", peerName);

			// Remember client versions
			for(size_t i = 0; msg.hdr.bodySize > 0 && i < msg.body.getMsgVersionsData.count; ++i)
			{
				SMetricsMsg::VersionMap::const_iterator entry = m_versionMap.find(msg.body.getMsgVersionsData.versionData[i].msgId);

				if(entry != m_versionMap.end())
				{
					SMetricsMsg::SVersionValue version;
					version.cmdVersion = std::min(msg.body.getMsgVersionsData.versionData[i].version.cmdVersion, entry->second.cmdVersion);
					version.respVersion = std::min(msg.body.getMsgVersionsData.versionData[i].version.respVersion, entry->second.respVersion);
					m_clientVersionMap[msg.body.getMsgVersionsData.versionData[i].msgId] = version;
				}
			}

			// Send my versions
			unsigned long count = m_versionMap.size();
			auto msgSize = offsetof(SMetricsMsg, body.getMsgVersionsData.versionData) + count * sizeof(SMetricsMsg::SVersionData);
			std::shared_ptr<SMetricsMsg> resp(new(msgSize) SMetricsMsg);
			resp->hdr = msg.hdr;
			resp->hdr.msgSubType = SMetricsMsg::GET_MSG_VERSIONS_RESP;
			resp->hdr.bodySize = offsetof(SMetricsMsg::SGetMsgVersionsData, versionData) + count * sizeof(SMetricsMsg::SVersionData);
			resp->body.getMsgVersionsData.count = count;
			size_t index = 0;

			for(SMetricsMsg::VersionMap::const_iterator entry = m_versionMap.begin(); entry != m_versionMap.end(); ++entry)
			{
				resp->body.getMsgVersionsData.versionData[index].msgId = entry->first;
				resp->body.getMsgVersionsData.versionData[index++].version = entry->second;
			}

			Send(resp.get(), offsetof(SMetricsMsg, body) + resp->hdr.bodySize);
//			CLog::Log(CLog::INFORMATION, "Metrics: sent GET_MSG_VERSIONS_RESP to %s", peerName);
		}
		break;

	case SMetricsMsg::GET_FAULT_REQUEST:
//		CLog::Log(CLog::INFORMATION, "Metrics: received from %s: GET_FAULT_REQUEST", peerName);
		m_metrics->ForwardStatusCtrl(msg, this);
		break;

	case SMetricsMsg::METRIC_CONN_INFO_GET:
		CLog::Log(CLog::INFORMATION, "Metrics: received from %s: METRIC_CONN_INFO_GET", peerName);
		m_metrics->SendStatusCtrlConnResp(msg, this);
		break;

	case SMetricsMsg::METRIC_ANT_NAME_CHANGE_REQUEST:
		CLog::Log(CLog::INFORMATION, "Metrics: received from %s: METRIC_ANT_NAME_CHANGE_REQUEST", peerName);
		m_metrics->SendAntNameChangeResp(msg, this);
		break;

	default:
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Save client info
//
_Success_(return == SAVED) CMetricsNet::ESaveClientInfoResult CMetricsNet::SaveClientInfo(
	_Out_ SMetricsMsg::SClientInfo & clientInfo, _In_ const ADDRINFOT * pPeer)
{
	ESaveClientInfoResult retcode = SAVED;

	//We only handle IPv4 for now.

	if(pPeer != nullptr)
	{
		const char* src = pPeer->ai_canonname;
		for (size_t i = 0; i < SMetricsMsg::MAX_STATIONNAME_LEN; ++i)
		{
			auto rv = mbrtoc16(&clientInfo.clientName[i], src, 1, nullptr);
			if (rv == 0 || rv == (size_t)-1 || rv == (size_t)-2)
				break;	// done

			if (rv > 0)
				src += rv;
			// rv must be -3, no input consumed
		}

		if(pPeer->ai_family == PF_INET)
		{
			//4 bytes unsigned transfer as unsigned long.
			clientInfo.ipType = SMetricsMsg::IP_V4;
			unsigned long* pIpAddr = reinterpret_cast<unsigned long*>(&(clientInfo.ipAddr[0]));
			*pIpAddr = reinterpret_cast<SOCKADDR_IN*>(pPeer->ai_addr)->sin_addr.s_addr;
//			*pIpAddr = reinterpret_cast<SOCKADDR_IN*>(pPeer->ai_addr)->sin_addr.S_un.S_addr;
		}
		else if(pPeer->ai_family == PF_INET6)
		{
			clientInfo.ipType = SMetricsMsg::IP_V6;
			memcpy_s(clientInfo.ipAddr, sizeof(clientInfo.ipAddr), &reinterpret_cast<SOCKADDR_IN6*>(pPeer->ai_addr)->sin6_addr, sizeof(reinterpret_cast<SOCKADDR_IN6*>(pPeer->ai_addr)->sin6_addr));
		}
		else
		{
//			ASSERT(false);
			TRACE("SaveClientInfo ai_family = %d\n", pPeer->ai_family);
			retcode = NOT_AF_INET;
		}
	}
	else
	{
		clientInfo.ipType = SMetricsMsg::IP_V4;
		clientInfo.clientName[0] = L'\0';
		clientInfo.ipAddr[0] = clientInfo.ipAddr[1] = clientInfo.ipAddr[2] =
			clientInfo.ipAddr[3] = 0;
		retcode = NULL_SOCK_PTR;
	}

	return(retcode);
}


//////////////////////////////////////////////////////////////////////
//
// Send a message
//
bool CMetricsNet::Send(_In_bytecount_(size) const void* msg, size_t /*size*/)
{
	UNREFERENCED_PARAMETER(size);

	const SMetricsMsg* mMsg = static_cast<const SMetricsMsg*>(msg);

	// Check response version
	SMetricsMsg::SVersionKey key = { mMsg->hdr.msgType, mMsg->hdr.msgSubType };
	SMetricsMsg::VersionMap::const_iterator entry = m_versionMap.find(key);
	bool converted = false;
	unsigned char myRespVersion = (entry != m_versionMap.end() ? entry->second.respVersion : 0);

	if(myRespVersion > mMsg->hdr.respVersion)
	{
		// Downconvert to requested version (making a local copy)
		if (!DownConvert(mMsg))
		{
			CLog::Log(CLog::ERRORS, "Metrics:Send downconvert failed (%u,%lu)", mMsg->hdr.msgType, mMsg->hdr.msgSubType);
			delete mMsg;
			return false;
		}
		converted = true;
	}

	SMetricsMsg::SHdr* sndHdr = static_cast<SMetricsMsg::SHdr*>(m_sendMsg);
	*sndHdr = mMsg->hdr;

	if(myRespVersion < mMsg->hdr.respVersion)
	{
		// Set response version to my version
		sndHdr->respVersion = myRespVersion;
	}

	if(sndHdr->bodySize != 0)
	{
		if(IsBodyCompressed(*mMsg))
		{
			// Compress the body
			CLog::Log(CLog::ERRORS, "Metrics:Send compressed messages unsupported (%u,%lu)", mMsg->hdr.msgType, mMsg->hdr.msgSubType);
//			ASSERT(FALSE);
			if(converted)
			{
				delete mMsg;
			}
			return false;
//			throw std::runtime_error("Compressed messages not supported");
		}
		// Copy the body
		if(malloc_usable_size(m_sendMsg) < offsetof(SMetricsMsg, body) + sndHdr->bodySize)
		{
			// Resize buffer
			void* biggerSendMsg = realloc(m_sendMsg, offsetof(SMetricsMsg, body) + sndHdr->bodySize);

			if(biggerSendMsg == nullptr)
			{
				ASSERT(FALSE);
				throw std::bad_alloc();
			}

			m_sendMsg = biggerSendMsg;
			sndHdr = static_cast<SMetricsMsg::SHdr*>(m_sendMsg);
		}
		memcpy(m_sendMsg, mMsg, offsetof(SMetricsMsg, body) + sndHdr->bodySize);
	}

	if(converted)
	{
		// Delete the local copy
		delete mMsg;
	}

	return CNetConnection<CMetricsNet>::Send(m_sendMsg, offsetof(SMetricsMsg, body) + sndHdr->bodySize);
}


//////////////////////////////////////////////////////////////////////
//
// Send a message
//
void CMetricsNet::Send(_In_ CMetricsNet* client, _In_ const SMetricsMsg& msg, _In_opt_ LPCTSTR log, ...)
{
	if (Send(client, &msg, offsetof(SMetricsMsg, body) + msg.hdr.bodySize) && log != nullptr && CLog::GetLogLevel() == CLog::VERBOSE)
	{
		// Verbose logging
		char str[256];
		va_list args;
		va_start(args, log);
		vsnprintf(str, 256, log, args);
		va_end(args);
		CLog::Log(CLog::INFORMATION, "Metrics: sent %s", str);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send an error message
//
void CMetricsNet::SendError(_In_ const SMetricsMsg::SHdr& hdr, unsigned long code)
{
	SMetricsMsg::SHdr msg(hdr);
	msg.msgType = SMetricsMsg::SMS_ERROR_REPORT;
	msg.msgSubType = code;
	msg.bodySize = 0;

	if(Send(&msg, sizeof(msg)) && CLog::GetLogLevel() == CLog::VERBOSE)
	{
		CLog::Log(CLog::INFORMATION, "Metrics: sent SMS_ERROR_REPORT %lu", code);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send immediate response to Metric Client.
//
void CMetricsNet::SendImmediateResp(_In_ const SMetricsMsg& msg, _In_opt_ LPCTSTR pLog)
{
	if (Send(&msg, offsetof(SMetricsMsg, body) + msg.hdr.bodySize) && pLog != nullptr && CLog::GetLogLevel() == CLog::VERBOSE)
	{
		CLog::Log(CLog::INFORMATION, "Metrics: sent %s", pLog);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Upconvert a message to latest version
//
void CMetricsNet::UpConvert(_Inout_ SMetricsMsg*& msg)
{
	unsigned long bodySize;

	switch(msg->hdr.msgType)
	{
	case SMetricsMsg::ANT_CTRL:
		switch(msg->hdr.cmdVersion)
		{
		case 0:
			// Body is the same, just originally compressed
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SMetricsMsg::AUTOVIOLATE_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SMetricsMsg::AVD_DELETE_REQUEST:
		case SMetricsMsg::AVD_MEASUREMENT_REQUEST:
		case SMetricsMsg::AVD_RESUME_REQUEST:
		case SMetricsMsg::AVD_STATE_REQUEST:
		case SMetricsMsg::AVD_SUSPEND_REQUEST:
		case SMetricsMsg::AVD_TERMINATE_REQUEST:
			switch (msg->hdr.cmdVersion)
			{
			case 0:
				// Message body is same, just compressed
				break;

			default:
				THROW_LOGIC_ERROR();
			}
			break;

		case SMetricsMsg::SCHED_AVD_REQUEST:
			switch(msg->hdr.cmdVersion)
			{
			case 0:
				{
					if(msg->hdr.bodySize != sizeof(msg->body.schedMsgAutoViolateRequestV0))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					bodySize = static_cast<unsigned long>(offsetof(SMetricsMsg::SSchedMsgAutoViolateRequest,
						clientMeasure.band[msg->body.schedMsgAutoViolateRequestV0.clientMeasure.numBands]));
					size_t	bodySize2 = offsetof(SMetricsMsg::SSchedMsgAutoViolateRequestV2,
						clientMeasure.band[msg->body.schedMsgAutoViolateRequestV0.clientMeasure.numBands]);
					size_t	maxSize = ( bodySize < bodySize2) ? bodySize2 : bodySize;

					if(maxSize > m_maxRecvBodySize)
					{
#ifdef _DEBUG
						SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + maxSize, __FILE__, __LINE__);
#else
						SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + maxSize);
#endif
						m_maxRecvBodySize = maxSize;
					}

					msg->body.schedMsgAutoViolateRequest = msg->body.schedMsgAutoViolateRequestV2 =
						SMetricsMsg::SSchedMsgAutoViolateRequestV1(msg->body.schedMsgAutoViolateRequestV0);
				}
				break;

			case 1:
				{
					if(msg->hdr.bodySize != sizeof(msg->body.schedMsgAutoViolateRequestV1))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					bodySize = static_cast<unsigned long>(offsetof(SMetricsMsg::SSchedMsgAutoViolateRequest,
						clientMeasure.band[msg->body.schedMsgAutoViolateRequestV1.clientMeasure.numBands]));
					size_t	bodySize2 = offsetof(SMetricsMsg::SSchedMsgAutoViolateRequestV2,
						clientMeasure.band[msg->body.schedMsgAutoViolateRequestV0.clientMeasure.numBands]);
					size_t	maxSize = ( bodySize < bodySize2) ? bodySize2 : bodySize;

					if(maxSize > m_maxRecvBodySize)
					{
#ifdef _DEBUG
						SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + maxSize, __FILE__, __LINE__);
#else
						SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + maxSize);
#endif
						m_maxRecvBodySize = maxSize;
					}

					msg->body.schedMsgAutoViolateRequest = msg->body.schedMsgAutoViolateRequestV2 =
								msg->body.schedMsgAutoViolateRequestV1;
				}
				break;

			case 2:
				if(msg->hdr.bodySize != static_cast<unsigned long>(offsetof(SMetricsMsg::SSchedMsgAutoViolateRequestV2,
					clientMeasure.band[msg->body.schedMsgAutoViolateRequestV2.clientMeasure.numBands])))
				{
					throw ErrorCodes::INVALIDVERSION;
				}

				bodySize = static_cast<unsigned long>(offsetof(SMetricsMsg::SSchedMsgAutoViolateRequest,
					clientMeasure.band[msg->body.schedMsgAutoViolateRequestV2.clientMeasure.numBands]));

				if(bodySize > m_maxRecvBodySize)
				{
#ifdef _DEBUG
					SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + bodySize, __FILE__, __LINE__);
#else
					SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + bodySize);
#endif
					m_maxRecvBodySize = bodySize;
				}

				msg->body.schedMsgAutoViolateRequest = msg->body.schedMsgAutoViolateRequestV2;
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			msg->hdr.bodySize = bodySize;
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SMetricsMsg::BIST_CTRL:
		switch(msg->hdr.cmdVersion)
		{
		case 0:
			// Body is the same, just originally compressed
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SMetricsMsg::DEMOD_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SMetricsMsg::SET_RCVR:
			switch(msg->hdr.cmdVersion)
			{
			case 0:
				if(msg->hdr.bodySize != sizeof(msg->body.recvrCntlReqMsgV0))
				{
					throw ErrorCodes::INVALIDVERSION;
				}

				msg->body.recvrCntlReqMsg = msg->body.recvrCntlReqMsgV0;
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			msg->hdr.bodySize = sizeof(SMetricsMsg::SRecvrCntlReqMsg);
			break;

			case SMetricsMsg::SET_AUDIO_PARAMS:
				switch (msg->hdr.cmdVersion)
				{
				case 1:
					if (msg->hdr.bodySize != sizeof(msg->body.audioParamsCmdV1))
					{
						throw ErrorCodes::INVALIDVERSION;
					}
					msg->body.audioParamsCmd = msg->body.audioParamsCmdV1;
					break;
				default:
					THROW_LOGIC_ERROR();
				}
				msg->hdr.bodySize = sizeof(SMetricsMsg::SAudioParamsCmd);
				break;
		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SMetricsMsg::MEASURE_CTRL:
		UpConvertMeasureCtrl(msg);
		break;

	case SMetricsMsg::MEASUREMENT_GREETING_REQ:
		switch(msg->hdr.cmdVersion)
		{
		case 0:
			// Body is the same, just originally compressed
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SMetricsMsg::MEASUREMENT_PAN_REQ:
		switch(msg->hdr.msgSubType)
		{
		case SMetricsMsg::GET_PAN:
		case SMetricsMsg::GET_PAN_AGC:
			switch(msg->hdr.cmdVersion)
			{
			case 0:
				if(msg->hdr.bodySize != sizeof(msg->body.panMeasurementReqMsgV0))
				{
					throw ErrorCodes::INVALIDVERSION;
				}

				msg->body.panMeasurementReqMsg = msg->body.panMeasurementReqMsgV1 = msg->body.panMeasurementReqMsgV0;
				break;

			case 1:
				if(msg->hdr.bodySize != sizeof(msg->body.panMeasurementReqMsgV1))
				{
					throw ErrorCodes::INVALIDVERSION;
				}

				msg->body.panMeasurementReqMsg = msg->body.panMeasurementReqMsgV1;
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			msg->hdr.bodySize = sizeof(SMetricsMsg::SPanMeasurementReqMsg);
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SMetricsMsg::OCCUPANCY_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SMetricsMsg::OCCUP_MEASUREMENT_REQUEST:
		case SMetricsMsg::OCCUP_STATE_REQUEST:
		case SMetricsMsg::OCCUP_DELETE_REQUEST:
		case SMetricsMsg::OCCUP_RESUME_REQUEST:
		case SMetricsMsg::OCCUP_SUSPEND_REQUEST:
		case SMetricsMsg::OCCUP_TERMINATE_REQUEST:
			switch (msg->hdr.cmdVersion)
			{
			case 0:
				// Message body is same, just compressed
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			break;

		case SMetricsMsg::SCHED_OCCUP_REQUEST:
			switch(msg->hdr.cmdVersion)
			{
			case 0:
				if(msg->hdr.bodySize != sizeof(msg->body.schedMsgOccupancyRequestV0))
				{
					throw ErrorCodes::INVALIDVERSION;
				}

				bodySize = static_cast<unsigned long>(offsetof(SMetricsMsg::SSchedMsgOccupancyRequest,
					clientMeasure.band[msg->body.schedMsgOccupancyRequestV0.clientMeasure.numBands]));

				if(bodySize > m_maxRecvBodySize)
				{
#ifdef _DEBUG
					SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + bodySize, __FILE__, __LINE__);
#else
					SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + bodySize);
#endif
					m_maxRecvBodySize = bodySize;
				}

				msg->body.schedMsgOccupancyRequest = msg->body.schedMsgOccupancyRequestV4 =
                                    msg->body.schedMsgOccupancyRequestV3 =
									msg->body.schedMsgOccupancyRequestV2 =
									SMetricsMsg::SSchedMsgOccupancyRequestV1(msg->body.schedMsgOccupancyRequestV0);
				break;

			case 1:
				if(msg->hdr.bodySize != sizeof(msg->body.schedMsgOccupancyRequestV1))
				{
					throw ErrorCodes::INVALIDVERSION;
				}

				bodySize = static_cast<unsigned long>(offsetof(SMetricsMsg::SSchedMsgOccupancyRequest,
					clientMeasure.band[msg->body.schedMsgOccupancyRequestV1.clientMeasure.numBands]));

				if(bodySize > m_maxRecvBodySize)
				{
#ifdef _DEBUG
					SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + bodySize, __FILE__, __LINE__);
#else
					SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + bodySize);
#endif
					m_maxRecvBodySize = bodySize;
				}

				msg->body.schedMsgOccupancyRequest = msg->body.schedMsgOccupancyRequestV4 =
                                    msg->body.schedMsgOccupancyRequestV3 =
									msg->body.schedMsgOccupancyRequestV2 =
									msg->body.schedMsgOccupancyRequestV1;
				break;

			case 2:
				if(msg->hdr.bodySize != offsetof(SMetricsMsg::SSchedMsgOccupancyRequestV2,
					clientMeasure.band[msg->body.schedMsgOccupancyRequestV2.clientMeasure.numBands]))
				{
					throw ErrorCodes::INVALIDVERSION;
				}

				bodySize = static_cast<unsigned long>(offsetof(SMetricsMsg::SSchedMsgOccupancyRequest,
					clientMeasure.band[msg->body.schedMsgOccupancyRequestV2.clientMeasure.numBands]));

				if(bodySize > m_maxRecvBodySize)
				{
#ifdef _DEBUG
					SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + bodySize, __FILE__, __LINE__);
#else
					SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + bodySize);
#endif
					m_maxRecvBodySize = bodySize;
				}

				msg->body.schedMsgOccupancyRequest = msg->body.schedMsgOccupancyRequestV4 =
                                        msg->body.schedMsgOccupancyRequestV3 =
										msg->body.schedMsgOccupancyRequestV2;
				break;

			case 3:
				if(msg->hdr.bodySize != offsetof(SMetricsMsg::SSchedMsgOccupancyRequestV3,
					clientMeasure.band[msg->body.schedMsgOccupancyRequestV3.clientMeasure.numBands]))
				{
					throw ErrorCodes::INVALIDVERSION;
				}

				bodySize = static_cast<unsigned long>(offsetof(SMetricsMsg::SSchedMsgOccupancyRequest,
					clientMeasure.band[msg->body.schedMsgOccupancyRequestV3.clientMeasure.numBands]));

				if(bodySize > m_maxRecvBodySize)
				{
#ifdef _DEBUG
					SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + bodySize, __FILE__, __LINE__);
#else
					SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + bodySize);
#endif
					m_maxRecvBodySize = bodySize;
				}

				msg->body.schedMsgOccupancyRequest = msg->body.schedMsgOccupancyRequestV4 =
                                        msg->body.schedMsgOccupancyRequestV3;
				break;

			case 4:
				if (msg->hdr.bodySize != offsetof(SMetricsMsg::SSchedMsgOccupancyRequestV4,
					clientMeasure.band[msg->body.schedMsgOccupancyRequestV4.clientMeasure.numBands]))
				{
					throw ErrorCodes::INVALIDVERSION;
				}

				bodySize = static_cast<unsigned long>(offsetof(SMetricsMsg::SSchedMsgOccupancyRequest,
					clientMeasure.band[msg->body.schedMsgOccupancyRequestV4.clientMeasure.numBands]));

				if (bodySize > m_maxRecvBodySize)
				{
#ifdef _DEBUG
					SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + bodySize, __FILE__, __LINE__);
#else
					SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + bodySize);
#endif
					m_maxRecvBodySize = bodySize;
				}

				msg->body.schedMsgOccupancyRequest = msg->body.schedMsgOccupancyRequestV4;
				break;

			default:
				THROW_LOGIC_ERROR();
			}	// end of switch(msg->hdr.cmdVersion)

			msg->hdr.bodySize = bodySize;
			break;

		default:
			THROW_LOGIC_ERROR();

		}	// end of switch(msg->hdr.msgSubType) for OCCUPANCY_CTRL

		break;

	case SMetricsMsg::OCCUPANCYDF_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SMetricsMsg::SCANDF_MEASUREMENT_REQUEST:
		case SMetricsMsg::SCANDF_STATE_REQUEST:
		case SMetricsMsg::SCANDF_DELETE_REQUEST:
		case SMetricsMsg::SCANDF_RESUME_REQUEST:
		case SMetricsMsg::SCANDF_SUSPEND_REQUEST:
		case SMetricsMsg::SCANDF_TERMINATE_REQUEST:
			switch(msg->hdr.cmdVersion)
			{
			case 0:
				// Message body is same, just compressed
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			break;

		case SMetricsMsg::SCHED_SCANDF_REQUEST:

			switch(msg->hdr.cmdVersion)
			{
			case 0:
				if(msg->hdr.bodySize != sizeof(msg->body.schedMsgScanDfRequestV0))
				{
					throw ErrorCodes::INVALIDVERSION;
				}

				bodySize = static_cast<unsigned long>(offsetof(SMetricsMsg::SSchedMsgScanDfRequest,
					clientMeasure.band[msg->body.schedMsgScanDfRequestV0.clientMeasure.numBands]));

				if(bodySize > m_maxRecvBodySize)
				{
#ifdef _DEBUG
					SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + bodySize, __FILE__, __LINE__);
#else
					SMetricsMsg::Realloc(msg, offsetof(SMetricsMsg, body) + bodySize);
#endif
					m_maxRecvBodySize = bodySize;
				}

				msg->body.schedMsgScanDfRequest = msg->body.schedMsgScanDfRequestV0;
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			msg->hdr.bodySize = bodySize;
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SMetricsMsg::STATUS_CTRL:
			switch(msg->hdr.cmdVersion)
			{
			case 0:
				// Body is the same, just originally compressed
				break;

			default:
				THROW_LOGIC_ERROR();
			}

		break;

	case SMetricsMsg::SYSTEM_STATE_CTRL:
			switch(msg->hdr.cmdVersion)
			{
			case 0:
				// Body is the same, just originally compressed
				break;

			default:
				THROW_LOGIC_ERROR();
			}

		break;

	default:
		THROW_LOGIC_ERROR();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Upconvert a MEASURE_CTRL message to latest version
//
void CMetricsNet::UpConvertMeasureCtrl(_Inout_ SMetricsMsg*& msg)
{
	switch (msg->hdr.msgSubType)
	{
	case SMetricsMsg::DELETE_REQUEST:
		switch (msg->hdr.cmdVersion)
		{
		case 0:
			// Message body is same, just compressed
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SMetricsMsg::DWELL_REQUEST:
		switch (msg->hdr.cmdVersion)
		{
		case 0:
			if (msg->hdr.bodySize != sizeof(msg->body.measureCtrlDwellRequestV0))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			msg->body.measureCtrlDwellRequest = msg->body.measureCtrlDwellRequestV0;
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		msg->hdr.bodySize = sizeof(SMetricsMsg::SMeasureCtrlDwellRequest);
		break;

	case SMetricsMsg::SCHED_REQUEST:
		switch (msg->hdr.cmdVersion)
		{
		case 0:
			if (msg->hdr.bodySize != sizeof(msg->body.schedMsgRequestV0))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			msg->body.schedMsgRequest = msg->body.schedMsgRequestV4 = msg->body.schedMsgRequestV3 = msg->body.schedMsgRequestV2 = msg->body.schedMsgRequestV1 = msg->body.schedMsgRequestV0;
			break;

		case 1:
			if (msg->hdr.bodySize != sizeof(msg->body.schedMsgRequestV1))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			msg->body.schedMsgRequest = msg->body.schedMsgRequestV4 = msg->body.schedMsgRequestV3 = msg->body.schedMsgRequestV2 = msg->body.schedMsgRequestV1;
			break;

		case 2:
			if (msg->hdr.bodySize != sizeof(msg->body.schedMsgRequestV2))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			msg->body.schedMsgRequest = msg->body.schedMsgRequestV4 = msg->body.schedMsgRequestV3 = msg->body.schedMsgRequestV2;
			break;

		case 3:
			if (msg->hdr.bodySize != sizeof(msg->body.schedMsgRequestV3))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			msg->body.schedMsgRequest = msg->body.schedMsgRequestV4 = msg->body.schedMsgRequestV3;
			break;

		case 4:
			if (msg->hdr.bodySize != sizeof(msg->body.schedMsgRequestV4))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			msg->body.schedMsgRequest = msg->body.schedMsgRequestV4;
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		msg->hdr.bodySize = sizeof(SMetricsMsg::SSchedMsgRequest);
		break;

	case SMetricsMsg::BAND_REQUEST:
		//Allocated buffer should be greater than SBandRequest.
		ASSERT(m_maxRecvBodySize >= sizeof(SMetricsMsg::SBandRequest));

		switch (msg->hdr.cmdVersion)
		{
		case 0:
		{
			msg->body.bandReq.clientInfo.clientId = m_metrics->GetClientId(this);
			auto peer = GetPeerAddress();

			if (SaveClientInfo(msg->body.bandReq.clientInfo, peer) == NOT_AF_INET)
			{
				THROW_LOGIC_ERROR();
			}

			msg->hdr.bodySize = sizeof(SMetricsMsg::SBandRequest);

		}
			break;

		case 1:
			if (msg->hdr.bodySize != sizeof(msg->body.bandReqV1))
			{
				throw ErrorCodes::INVALIDVERSION;
			}
			break;

		default:
			THROW_LOGIC_ERROR();
		}
		break;

	case SMetricsMsg::BAND_REQUEST_NETTED:
		switch (msg->hdr.cmdVersion)
		{
		case 0:
			// Message body is same, just compressed
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SMetricsMsg::MEASUREMENT_REQUEST:
		switch (msg->hdr.cmdVersion)
		{
		case 0:
			// Message body is same, just compressed
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

		case SMetricsMsg::SERVER_LOCATION_REQUEST:
		switch (msg->hdr.cmdVersion)
		{
		case 0:
			// Message body is same, just compressed
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SMetricsMsg::SERVER_WORKLOAD_REQUEST_MSG:
		switch (msg->hdr.cmdVersion)
		{
		case 0:
			// Message body is same, just compressed
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SMetricsMsg::HEADING_REQUEST:
		switch (msg->hdr.cmdVersion)
		{
		case 0:
			// Message body is same, just compressed
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	default:
		THROW_LOGIC_ERROR();
	}
}
