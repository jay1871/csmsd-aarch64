/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2014 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

//#include <exception>
//#include <mutex>
//#include <pthread.h>
//#include <signal.h>

#include "Failure.h"

std::exception_ptr CFailure::m_exception(nullptr);
pthread_t CFailure::m_main(pthread_self());
std::mutex CFailure::m_mutex;


//////////////////////////////////////////////////////////////////////
//
// Check for failure
//
void CFailure::CheckFail(void)
{
	std::lock_guard<std::mutex> lock(m_mutex);

	if(m_exception)
	{
		std::rethrow_exception(m_exception);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Record failure
//
void CFailure::OnFail(std::exception_ptr e)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	m_exception = e;
	pthread_kill(m_main, SIGQUIT);

	return;
}
