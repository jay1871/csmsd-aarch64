/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"

#include "Antenna.h"
#include "AvdTask.h"
#include "Digitizer.h"
#include "EquipControl.h"
#include "EquipCtrlMsg.h"
#include "Fft.h"
#include "MeasurementTask.h"
#include "NoiseEstimate.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CMeasurementTask::CMeasurementTask(_In_opt_ const SEquipCtrlMsg* cmd, _In_opt_ CNetConnection<void>* source/*, EPriority priority*/) :
	CTask(cmd, source/*, priority*/),
	m_altitude(0),
	m_amDepth(),
	m_amMinus(),
	m_amPlus(),
	m_betaFreqOffsetHi(),
	m_betaFreqOffsetLo(),
	m_bwDwellCount(0),
	m_cableLoss(0),
	m_calibrated(true),
	m_cmd(),
	m_conf(),
	m_digitizerBlockCount(0),
	m_dfDwellCount(0),
	m_dfNumBins(0),
	m_dwellAmMinus(),
	m_dwellAmPlus(),
	m_dwellFieldStrength(),
	m_dwellFmPeak(0),
	m_dwellFmRms(),
	m_dwellFreqOffset(),
	m_dwellMaxHoldPower(-DBL_MAX),
	m_dwellPmPeak(0),
	m_dwellPmRms(),
	m_fieldStrength(),
	m_fieldStrengthDwellCount(0),
	m_fmPeak(),
	m_fmRms(),
	m_freqDwellCount(0),
	m_freqOffset(),
	m_iqDataSamples(0),
	m_maxHoldPower(-DBL_MAX),
	m_modDwellCount(0),
	m_navigation(),
	m_pmPeak(),
	m_pmRms(),
	m_rxCorr(1),
	m_snr(0),
	m_x1FreqOffsetHi(),
	m_x1FreqOffsetLo(),
	m_x2FreqOffsetHi(),
	m_x2FreqOffsetLo()
{
	m_prevAz[0] = m_prevAz[1] = 0;

	if(cmd == nullptr)
	{
//		TRACE(_T("CMeasurementTask avd ctor @ %.6f MHz\n"), Units::Frequency(m_cmd.freq).Hz<double>() / 1e6);
		// AVD measurement task
		const CConfig::SAvdMeasParams& params = m_config->GetAvdMeasParams();
		m_cmd.ant = m_ant;
		m_cmd.bandwidth = m_taskParams[0].bw.GetRaw();
		m_cmd.bwCmd.dwellTime = params.bwDwellTime;
		m_cmd.bwCmd.betaParam = static_cast<unsigned long>(10 * params.bwBetaParam + 0.5f);
		m_cmd.bwCmd.yParam = static_cast<unsigned long>(10 * params.bwYParam + 0.5f);
		m_cmd.bwCmd.x1Param = static_cast<unsigned long>(10 * params.bwX1Param + 0.5f);
		m_cmd.bwCmd.x2Param = static_cast<unsigned long>(10 * params.bwX2Param + 0.5f);
		m_cmd.bwCmd.repeatCount = params.bwRepeatCount;
		m_cmd.bwCmd.aveMethod = params.bwAveMethod;
		m_cmd.bwCmd.outputType = SEquipCtrlMsg::SUMMARY;
		m_cmd.freqCmd.freqMethod = params.freqFreqMethod;
		m_cmd.freqCmd.dwellTime = params.freqDwellTime;
		m_cmd.freqCmd.repeatCount = params.freqRepeatCount;
		m_cmd.freqCmd.aveMethod = params.freqAveMethod;
		m_cmd.freqCmd.outputType = SEquipCtrlMsg::SUMMARY;
		m_cmd.modulationCmd.outputType = SEquipCtrlMsg::NONE;
		m_cmd.fieldStrengthCmd.outputType = SEquipCtrlMsg::NONE;
		m_cmd.dfCmd.outputType = SEquipCtrlMsg::SGetMeasCmd::SGetDfCmd::NONE;
	}
	else
	{
//		static const unsigned int MAX_MEAS_BLOCKS = 1000;

		// Metrics measurement task
		m_cmd = cmd->body.getMeasCmd;

		if(cmd->hdr.msgSubType == SEquipCtrlMsg::GET_MEAS ||
				cmd->hdr.msgSubType == SEquipCtrlMsg::VALIDATE_MEAS)
		{
			//Ctask ctor updates m_ant with translated ANT value that corresponds
			// to RF input and polarization in TCI5143_MULTIPLE_ANT system.
			m_cmd.ant = m_ant;
		}

		m_conf = m_cmd.dfCmd.confThreshold / 100.0f;

		if (m_taskParams.size() > 0)
		{
			// If IQData step is present, adjust decimation and possibly sampleSize
			if (m_taskParams[0].mode == CConfig::EMode::IQDATA)
			{
				if (m_cmd.iqCmd.tdoa)
				{
					if (m_config->IsSlaveAntenna(m_ant, m_taskParams[0].hf))
					{
						throw ErrorCodes::INVALIDANTENNAID;
					}
					if (!m_license->HasPrecisionTimestamp())
					{
						throw ErrorCodes::HARDWARENOTPRESENT;
					}
					if (!CSingleton<CDigitizer>()->HasTimestampCapability() && m_radioEquip->GetOcxoLoopStatus() != 0)
					{
						throw ErrorCodes::PRECISIONTIMEUNAVAILABLE;
					}
#if ALLOW_EQUIPCONTROL_TDOA != 1
					if (source != nullptr)
					{
						m_cmd.iqCmd.startTime = 0.0;
					}
#endif
//					m_procParams[0].sampleSize = m_cmd.iqCmd.numSamples;	// Already done!
				}
				m_iqDataSamples = m_cmd.iqCmd.numSamples;

				// Determine actual number of samples to collect
				if (m_procParams[0].sampleSize > m_procParams[0].blockSize)
				{
					m_digitizerBlockCount = m_procParams[0].sampleSize / m_procParams[0].blockSize;
					if (m_digitizerBlockCount * m_procParams[0].blockSize < m_procParams[0].sampleSize)
						++m_digitizerBlockCount;
					// Set sampleSize to be a multiple of the blockSize
					m_procParams[0].sampleSize = m_digitizerBlockCount * m_procParams[0].blockSize;
				}
			}

			// Find last measurement step (the actual measurement, not a calibration step)
			size_t step = 0;
			for (step = m_taskParams.size() - 1; step > 0 && m_taskParams[step].mode != CConfig::EMode::MEASURE; --step);

			if(m_taskParams[step].mode == CConfig::EMode::MEASURE)
			{
				float blockTime = 1000 * m_config->GetDwellTime(m_procParams[step]); // ms;

				if (m_cmd.fieldStrengthCmd.outputType != SEquipCtrlMsg::NONE)
				{
//					if (m_cmd.fieldStrengthCmd.dwellTime > MAX_MEAS_BLOCKS * blockTime)
//					{
//						throw ErrorCodes::INVALIDDWELLTIME;
//					}
					m_fieldStrengthDwellCount = static_cast<unsigned int>(ceil(m_cmd.fieldStrengthCmd.dwellTime / blockTime));
					m_taskParams[step].numBlocks = m_fieldStrengthDwellCount * m_cmd.fieldStrengthCmd.repeatCount;

					if (step == 0 || m_taskParams[step - 1].mode != CConfig::EMode::MEASURE)
					{
						// No calibration
						SEquipCtrlMsg::EAnt ant;
						if (m_taskParams[step].hf)
						{
							ant = SEquipCtrlMsg::ANT1;
						}
						else
						{
							ant = m_cmd.ant;
						}
						m_cableLoss = m_config->GetRfCableLoss(m_taskParams[step].hf, ant, m_cmd.freq);
						m_calibrated = false;
					}
				}

				if (m_cmd.bwCmd.outputType != SEquipCtrlMsg::NONE)
				{
//					if (m_cmd.bwCmd.dwellTime > MAX_MEAS_BLOCKS * blockTime)
//					{
//						throw ErrorCodes::INVALIDDWELLTIME;
//					}
					m_bwDwellCount = static_cast<unsigned int>(ceil(m_cmd.bwCmd.dwellTime / blockTime));
					m_taskParams[step].numBlocks = std::max(m_taskParams[step].numBlocks, m_bwDwellCount * static_cast<unsigned int>(m_cmd.bwCmd.repeatCount));
				}

				if (m_cmd.freqCmd.outputType != SEquipCtrlMsg::NONE)
				{
//					if (m_cmd.freqCmd.dwellTime > MAX_MEAS_BLOCKS * blockTime)
//					{
//						throw ErrorCodes::INVALIDDWELLTIME;
//					}
					m_freqDwellCount = static_cast<unsigned int>(ceil(m_cmd.freqCmd.dwellTime / blockTime));
					m_taskParams[step].numBlocks = std::max(m_taskParams[step].numBlocks, m_freqDwellCount * static_cast<unsigned int>(m_cmd.freqCmd.repeatCount));
				}

				if (m_cmd.modulationCmd.outputType != SEquipCtrlMsg::NONE)
				{
//					if (m_cmd.modulationCmd.dwellTime > MAX_MEAS_BLOCKS * blockTime)
//					{
//						throw ErrorCodes::INVALIDDWELLTIME;
//					}
					m_modDwellCount = static_cast<unsigned int>(ceil(m_cmd.modulationCmd.dwellTime / blockTime));
					m_taskParams[step].numBlocks = std::max(m_taskParams[step].numBlocks, m_modDwellCount * static_cast<unsigned int>(m_cmd.modulationCmd.repeatCount));

					if (m_cmd.modulationCmd.outputType == SEquipCtrlMsg::DETAILS)
					{
						m_amDepth.SetHist(0, 1.5, SEquipCtrlMsg::NUMHISTBINS_MAX);
						m_fmPeak.SetHist(0, Units::Frequency(m_cmd.bandwidth).Hz<double>(), SEquipCtrlMsg::NUMHISTBINS_MAX);
						m_pmPeak.SetHist(0, 8, SEquipCtrlMsg::NUMHISTBINS_MAX);
					}
				}
			}

			if (m_cmd.dfCmd.outputType != SEquipCtrlMsg::SGetMeasCmd::SGetDfCmd::NONE)
			{
				step = m_taskParams.size() - 1; // DF is always the last

				size_t numAnts;

				if (m_taskParams[step].hf)
				{
					numAnts = CAntenna::NumAnts(m_mutableConfig.hfConfig.antCable.antenna.get(), m_cmd.freq);
				}
				else
				{
					numAnts = CAntenna::NumAnts(m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get(), m_cmd.freq);
				}


				m_dfDwellCount = static_cast<unsigned int>(ceil(m_cmd.dfCmd.dwellTime / (1000 * numAnts *
					(m_config->GetLatency(m_taskParams[step].bw, m_procParams[step].decimations) +
					m_config->GetDwellTime(m_procParams[step])))));

				if (m_dfDwellCount > m_cmd.dfCmd.repeatCount)
				{
					m_dfDwellCount = m_cmd.dfCmd.repeatCount;
				}

				if (m_taskParams[step].pol == CConfig::EPolarization::BOTH)
				{
					// Double up for both polarizations
					m_dfDwellCount *= 2;
				}

				m_taskParams[step].numBlocks = m_dfDwellCount;
				m_dfNumBins = static_cast<unsigned long>(Units::Frequency(m_cmd.dfCmd.dfBandwidth) / m_procParams[step].GetBinSize() + 1);

				if (m_dfNumBins < 3 || m_dfNumBins > m_procParams[step].GetNumBins(m_taskParams[step].bw) + 1)
				{
					throw ErrorCodes::INVALIDDFBANDWIDTH;
				}
			}
		}

		// TODO: Isn't this only needed if Df is part of task?
		for (size_t pol = 0; pol < 2; ++pol)
		{
			m_numDfCuts[pol] = 0;

			for (unsigned int bin = 0; bin < Units::DEG_PER_CIRCLE / AZ_HIST_BINSIZE; ++bin)
			{
				m_azHist[pol][bin] = 0;
			}
		}

		// Initialize m_resp
		m_resp.hdr = m_msgHdr;
		m_resp.hdr.msgSubType = SEquipCtrlMsg::GET_MEAS_RESPONSE;
		m_resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SGetMeasResp);
		memset(&m_resp.body, 0, m_resp.hdr.bodySize);
		m_navigation->GetGpsResponse(m_resp.body.getMeasResp.gpsResponse);
		// following used to get altitude needed for iqdata response
		double latitude;
		double longitude;
		m_navigation->GetPosition(latitude, longitude, m_altitude);
		m_resp.body.getMeasResp.bwResponse.status = ErrorCodes::NOERROR_OUTPUT_NONE;
		m_resp.body.getMeasResp.bwResponse.numBins = 0;
		m_resp.body.getMeasResp.freqResponse.status = ErrorCodes::NOERROR_OUTPUT_NONE;
		m_resp.body.getMeasResp.modulationResponse.status = ErrorCodes::NOERROR_OUTPUT_NONE;
		m_resp.body.getMeasResp.modulationResponse.numAmHistBins = 0;
		m_resp.body.getMeasResp.modulationResponse.numFmHistBins = 0;
		m_resp.body.getMeasResp.modulationResponse.numPmHistBins = 0;
		m_resp.body.getMeasResp.fieldStrengthResponse.status = ErrorCodes::NOERROR_OUTPUT_NONE;
		m_resp.body.getMeasResp.dfResponse[0].dfResponse.status = ErrorCodes::NOERROR_OUTPUT_NONE;
		m_resp.body.getMeasResp.dfResponse[1].dfResponse.status = ErrorCodes::NOERROR_OUTPUT_NONE;
		m_resp.body.getMeasResp.iqResponse.status = ErrorCodes::NOERROR_OUTPUT_NONE;

		// Initialize m_iqResp
		m_iqResp.hdr = m_msgHdr;
		m_iqResp.hdr.msgType = SEquipCtrlMsg::METRICS_CTRL;
		m_iqResp.hdr.msgSubType = SEquipCtrlMsg::GET_MEAS_IQDATA_RESP;
		m_iqResp.hdr.respVersion = CEquipControl::GetResponseVersion(source, m_iqResp.hdr);
		m_iqResp.hdr.bodySize = sizeof(SEquipCtrlMsg::SIqDataResp);
		m_iqResp.body.iqDataResp.numIqData = 0;
		m_iqResp.body.iqDataResp.packetIndex = 0;
		memset(&m_iqResp.body, 0, m_iqResp.hdr.bodySize);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CMeasurementTask::CMeasurementTask(_In_ SRestartData& restartData) :
	CTask((assert(false), (SEquipCtrlMsg*)nullptr), nullptr) // Should never be constructed this way (CTask is a virtual base)
{
	// Read from blob into member data
	ReadData(restartData, m_cmd);
	ReadData(restartData, m_bwDwellCount);
	ReadData(restartData, m_freqDwellCount);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CMeasurementTask::~CMeasurementTask(void)
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Add a new block of data to the measurement
//
void CMeasurementTask::AddBlock(const Ne10F32cVec& samples, const Ne10F32Vec& watts, const CTask::Task& task)
{
	const CConfig::SProcParams& procParams = task->GetProcParams();
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CTask::SBlockState& blockState = task->GetBlockState();
	ne10_float32_t enbw = 1.0f;
	double binSize = procParams.GetBinSize().Hz<double>();

	auto edr = m_config->HasEnhancedDynamicRange();
	// NOTE: m_snr is only > 0 if this measurement was invoked from AVD task, in which case taskParams.withPsd is set to true.
	//       Otherwise, the rest of this logic is equivalent to taskParams.withPsd which "guarantees" that psdData is valid.
	//       So, it is safe to remove software FFT option below.
	if (m_snr > 0 ||
		task->m_currentBlock < m_bwDwellCount * m_cmd.bwCmd.repeatCount ||
		(task->m_currentBlock < m_freqDwellCount * m_cmd.freqCmd.repeatCount && (edr || m_cmd.freqCmd.freqMethod == SEquipCtrlMsg::SGetFreqCmd::FFT)) ||
		(task->m_currentBlock < m_fieldStrengthDwellCount * m_cmd.fieldStrengthCmd.repeatCount && edr))
	{
		// Need power spectrum
		if (watts.size() == 0)
		{
			ASSERT(false);
			return;
		}
		enbw = CFft::GetEnbw(procParams.sampleSize);
	}
	if (m_snr > 0)
	{
		if (watts.maximum() < CNoiseEstimate::Create(watts, watts.size(), 0)->GetNoise() * m_snr)
		{
			return;
		}
	}

	if (task->m_currentBlock < m_modDwellCount * m_cmd.modulationCmd.repeatCount || (!m_config->HasEnhancedDynamicRange() &&
		(task->m_currentBlock < m_fieldStrengthDwellCount * m_cmd.fieldStrengthCmd.repeatCount ||
		(task->m_currentBlock < m_freqDwellCount * m_cmd.freqCmd.repeatCount &&
			m_cmd.freqCmd.freqMethod == SEquipCtrlMsg::SGetFreqCmd::IFM))))
	{
		// Need complex samples
		if (samples.size() == 0)
		{
			TRACE("***********WARNING: Need IQ data for AddBlock but none available*************\n");
			return;
		}
	}

	// Bandwidth measurement
	if (task->m_currentBlock < m_bwDwellCount * m_cmd.bwCmd.repeatCount)
	{
		UpdateBw(watts, binSize, task->m_currentBlock);
	}

	struct SUnwrapped		// To avoid calculating this twice, once for freq(IFM) and once for PM mod.
	{
		Ne10F32Vec phase;
		float offset;
		float slope;
	} unwrapped;
	// Frequency measurement
	if (task->m_currentBlock < m_freqDwellCount * m_cmd.freqCmd.repeatCount)
	{
		// silently switches to FFT method if enhancedDynRng invoked
		if (!m_config->HasEnhancedDynamicRange() && m_cmd.freqCmd.freqMethod == SEquipCtrlMsg::SGetFreqCmd::IFM)
		{
			unsigned long up, down;
			procParams.decimations.GetUpDown(up, down);
			UnwrapPhase(samples, unwrapped.phase, unwrapped.offset, unwrapped.slope);
			UpdateFreq(unwrapped.slope, (procParams.sampleRate * up / down).Hz<double>(), task->m_currentBlock, task->m_currentStep);
		}
		else
		{
			UpdateFreq(watts, binSize, task->m_currentBlock, task->m_currentStep);
		}
	}

	// Modulation measurement
	if (task->m_currentBlock < m_modDwellCount * m_cmd.modulationCmd.repeatCount)
	{
		// not affected by enhancedDynRng (not correct, but we'll live with)
		unsigned long up, down;
		procParams.decimations.GetUpDown(up, down);
		if (unwrapped.phase.size() == 0)
		{
			UnwrapPhase(samples, unwrapped.phase, unwrapped.offset, unwrapped.slope);
		}
		UpdateMod(samples, unwrapped.phase, unwrapped.offset, unwrapped.slope, (procParams.sampleRate * up / down).Hz<double>(), taskParams.bw.Hz<double>(), task->m_currentBlock);
	}

	// Field strength measurement
	if (task->m_currentBlock < m_fieldStrengthDwellCount * m_cmd.fieldStrengthCmd.repeatCount)
	{
		if (m_config->HasEnhancedDynamicRange())
		{
			// used if enhancedDynRng invoked
			ne10_float32_t dmag = sqrt(watts.sum() / enbw);
			Ne10F32Vec mag(1, dmag);
			UpdateFieldStrength(mag, task);
		}
		else
		{
			UpdateFieldStrength(samples, blockState.gainAdj, task);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Add a DF result to the measurement
//
void CMeasurementTask::AddDf(float az, float el, float conf, DATE time, float refPowerDbm, float maxSamplePowerDbm,
	size_t maxSamplePowerIdx, bool horiz)
{
	size_t pol = (horiz ? 1 : 0);
	std::lock_guard<std::mutex> lock(m_critSect);

	if (m_dfData[pol].size() < m_cmd.dfCmd.repeatCount)
	{
		if (++m_numDfCuts[pol] <= SEquipCtrlMsg::DFCUTS_MAX)
		{
			m_resp.body.getMeasResp.dfResponse[pol].dfCutData[m_numDfCuts[pol] - 1].azim = az * Units::R2D;
			m_resp.body.getMeasResp.dfResponse[pol].dfCutData[m_numDfCuts[pol] - 1].elev = el * Units::R2D;
			m_resp.body.getMeasResp.dfResponse[pol].dfCutData[m_numDfCuts[pol] - 1].conf = conf;
			m_resp.body.getMeasResp.dfResponse[pol].dfCutData[m_numDfCuts[pol] - 1].range = m_ionogram->Range(el, m_cmd.freq);
			m_resp.body.getMeasResp.dfResponse[pol].dfCutData[m_numDfCuts[pol] - 1].ms = float(time - m_dfStartTime) * Units::MILLISECONDS_PER_DAY;
			m_resp.body.getMeasResp.dfResponse[pol].dfCutData[m_numDfCuts[pol] - 1].refPowerDbm = refPowerDbm;
			m_resp.body.getMeasResp.dfResponse[pol].dfCutData[m_numDfCuts[pol] - 1].maxSamplePowerDbm = maxSamplePowerDbm;
			m_resp.body.getMeasResp.dfResponse[pol].dfCutData[m_numDfCuts[pol] - 1].maxSamplePowerIdx = static_cast<unsigned char>(maxSamplePowerIdx + 1);
		}

		if (conf >= m_conf)
		{
			ne10_fft_cpx_float32_t azConfVec = { conf * cosf(az), conf * sinf(az) };
//			TRACE("AddDf: %f %f %f %f\n", az, conf, azConfVec.r, azConfVec.i);
			ne10_fft_cpx_float32_t elConfVec = { el == Units::HALF_PI ? 0 : conf * cosf(el), el == Units::HALF_PI ? 0 : conf * sinf(el) };
			m_dfData[pol].push_back(DfData::value_type(azConfVec, elConfVec, m_numDfCuts[pol] - 1));
			size_t bin = size_t(az * Units::R2D / AZ_HIST_BINSIZE + 0.5f) % std::extent<decltype(m_azHist), 1>::value;
			++m_azHist[pol][bin];
			m_prevAz[pol] = az;
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Add power and snr to response message
//
void CMeasurementTask::AddPower(float dbm, float snr, bool horiz)
{
	size_t pol = (horiz ? 1 : 0);
	m_resp.body.getMeasResp.dfResponse[pol].dfResponse.snr = snr; // From first cut
	m_resp.body.getMeasResp.dfResponse[pol].dfResponse.powerDbm = dbm; // From first cut

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Add voltage vectors to response message
//
void CMeasurementTask::AddVolts(_In_ const Ne10F32cVec& volts, float dbmRef, float dbmSample, size_t refAntNum, size_t dfAntNum,
	size_t count, bool horiz)
{
	ASSERT(count < SEquipCtrlMsg::DFVOLTS_MAX);
	size_t pol = (horiz ? 1 : 0);

	// Convert to polar
	for (size_t ant = 0; ant < volts.size(); ++ant)
	{
		m_resp.body.getMeasResp.dfResponse[pol].dfVoltageData[count].volts[ant][0] = sqrtf(volts[ant].r * volts[ant].r + volts[ant].i * volts[ant].i);
		m_resp.body.getMeasResp.dfResponse[pol].dfVoltageData[count].volts[ant][1] = atan2f(volts[ant].i, volts[ant].r) * Units::R2D;
	}

	m_resp.body.getMeasResp.dfResponse[pol].dfVoltageData[count].powerDbm[0] = dbmRef;
	m_resp.body.getMeasResp.dfResponse[pol].dfVoltageData[count].powerDbm[1] = dbmSample;
	m_resp.body.getMeasResp.dfResponse[pol].dfVoltageData[count].antNum[0] = static_cast<unsigned char>(refAntNum);
	m_resp.body.getMeasResp.dfResponse[pol].dfVoltageData[count].antNum[1] = static_cast<unsigned char>(dfAntNum + 1);
	m_resp.body.getMeasResp.dfResponse[pol].dfResponse.numAnts = static_cast<unsigned char>(volts.size());

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Calculate the bandwidth results
//
void CMeasurementTask::CalcBw(double freqOffsetLo, double freqOffsetHi, Units::Frequency freq, double binSize, double correction,
	_Out_ double& freqLo, _Out_ double& freqHi, _Out_ double& bw)
{
	if (freqOffsetHi - freqOffsetLo <= correction * binSize)
	{
		freqHi = freqLo = freq.Hz<double>() + (freqOffsetHi + freqOffsetLo) / 2;
		bw = 0;
	}
	else
	{
		freqLo = freq.Hz<double>() + freqOffsetLo + correction * binSize / 2;
		freqHi = freq.Hz<double>() + freqOffsetHi - correction * binSize / 2;
		bw = freqOffsetHi - freqOffsetLo - correction * binSize;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Calculate the DF summary results
//
void CMeasurementTask::CalcDfSummary(void)
{
	// Calculate summary DF results
	for (int pol = (m_taskParams.back().pol != CConfig::EPolarization::HORIZ ? 0 : 1);
		pol <= (m_taskParams.back().pol == CConfig::EPolarization::VERT ? 0 : 1); ++pol)
	{
		if (m_dfData[pol].size() > 0)
		{
			unsigned int maxBin = 0;
			unsigned int maxCount = 0;

			// Find peak in histogram
			for (unsigned int histBin = 0; histBin < Units::DEG_PER_CIRCLE / AZ_HIST_BINSIZE; ++histBin)
			{
				if(m_azHist[pol][histBin] > maxCount)
				{
					maxCount = m_azHist[pol][histBin];
					maxBin = histBin;
				}
			}

			ne10_fft_cpx_float32_t peakVec = { cosf(maxBin * AZ_HIST_BINSIZE * Units::D2R), sinf(maxBin * AZ_HIST_BINSIZE * Units::D2R) };

			// Filter out vectors away from peak
			for (DfData::iterator vec = m_dfData[pol].begin(); vec != m_dfData[pol].end();)
			{
				if (fabsf(atan2f(vec->azConfVec.i * peakVec.r - vec->azConfVec.r * peakVec.i,
					vec->azConfVec.r * peakVec.r + vec->azConfVec.i * peakVec.i)) > HALF_AZ_PEAK_WIDTH)
				{
					// Negate confidence of stored cut
					if (vec->cut < SEquipCtrlMsg::DFCUTS_MAX)
					{
						m_resp.body.getMeasResp.dfResponse[pol].dfCutData[vec->cut].conf *= -1;
					}

					vec = m_dfData[pol].erase(vec);
				}
				else
				{
					++vec;
				}
			}
		}

		// Summarize remaining vectors
		float az = 0;
		float el = Units::DEG_PER_CIRCLE / 4;
		float range = 0;
		float conf = 0;
		float stdDev = 0;

		if (m_dfData[pol].size() > 0)
		{
			ne10_fft_cpx_float32_t sumAz = Ne10F32cVec::ZERO;
			ne10_fft_cpx_float32_t sumEl = Ne10F32cVec::ZERO;
			float sumAzLen = 0;

			for (DfData::iterator vec = m_dfData[pol].begin(); vec != m_dfData[pol].end(); ++vec)
			{
				sumAz += vec->azConfVec;
				sumEl += vec->elConfVec;
				sumAzLen += sqrt(vec->azConfVec.r * vec->azConfVec.r + vec->azConfVec.i * vec->azConfVec.i);
			}

			az = atan2(sumAz.i, sumAz.r) * Units::R2D;

			if (az < 0)
			{
				az += Units::DEG_PER_CIRCLE;
			}

			el = (sumEl.r == 0 && sumEl.i == 0 ? Units::HALF_PI : atan2(sumEl.i, sumEl.r));
			range = m_ionogram->Range(el, m_cmd.freq);
			el *= Units::R2D;
			conf = sqrt(sumAz.r * sumAz.r + sumAz.i * sumAz.i);
			stdDev = (conf >= sumAzLen ? 0 : sqrt(-2 * log(conf / sumAzLen)) * Units::R2D); // Circular standard deviation (per Fisher, 1993)
			conf /= m_dfData[pol].size();
		}

		// Build DF response
		m_resp.body.getMeasResp.dfResponse[pol].dfResponse.fluxgateStatus = m_navigation->GetStatus();
		m_resp.body.getMeasResp.dfResponse[pol].dfResponse.status = (m_resp.body.getMeasResp.dfResponse[pol].dfResponse.fluxgateStatus != ErrorCodes::SUCCESS &&
			m_config->HasMobileAntenna(m_taskParams.back().hf) ? ErrorCodes::NOFLUXGATE : ErrorCodes::SUCCESS);
		m_resp.body.getMeasResp.dfResponse[pol].dfResponse.headingCorrection = m_navigation->GetTrueHeading(m_taskParams.back().hf, Units::Frequency(0));
		m_resp.body.getMeasResp.dfResponse[pol].dfResponse.numSumCuts = static_cast<unsigned long>(m_dfData[pol].size());
		m_resp.body.getMeasResp.dfResponse[pol].dfResponse.azim = az;
		m_resp.body.getMeasResp.dfResponse[pol].dfResponse.elev = el;
		m_resp.body.getMeasResp.dfResponse[pol].dfResponse.conf = conf;
		m_resp.body.getMeasResp.dfResponse[pol].dfResponse.range = range;
		m_resp.body.getMeasResp.dfResponse[pol].dfResponse.stdDev = stdDev;
		m_resp.body.getMeasResp.dfResponse[pol].dfResponse.numAllCuts = static_cast<unsigned long>(m_numDfCuts[pol]);
#ifdef CSMS_DEBUG
		TRACE("CMeasurementTask::CalcDfSummary: status %d, fluxgateStatus %d; headingCorrection %f, numSumCuts %lu, numDfCuts %u, azim %f, elev %f, conf %f, range %f, stdDev %f\n",
				int(m_resp.body.getMeasResp.dfResponse[pol].dfResponse.status),
				int(m_resp.body.getMeasResp.dfResponse[pol].dfResponse.fluxgateStatus),
				m_resp.body.getMeasResp.dfResponse[pol].dfResponse.headingCorrection,
				m_resp.body.getMeasResp.dfResponse[pol].dfResponse.numSumCuts,
				m_numDfCuts[pol],
				az, el, conf, range, stdDev);
#endif
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Amplitude calibration steps
//
void  CMeasurementTask::Calibrate(_In_ const CTask::Task& task, size_t calibrateStep, size_t /*numCalSteps*/, float watts)
{
	if (task->GetBlock() == 0)
	{
		m_power = watts;
	}
	else
	{
		m_power += watts;
	}
	if (task->AtStepEnd())
	{
		m_power = 10 * log10(m_power / task->GetNumBlocks()) + 30;

		switch (calibrateStep)
		{
		case 0: // Switch BITE
			// Remember for step 1
			m_cableLoss = m_power;
			break;

		case 1: // Synth cal @ BITE freq
			{
#if 0
				float adj = 0; // dB
				Units::Frequency biteFreq = m_taskParams[task->GetStep() - 1].startFreq; // Previous step
				float biteDbm = C72332004::BITE_DBM;

				Units::Frequency calFreq = task->GetTuneFreq();

				float measCableLoss = biteDbm - m_cableLoss + m_power - m_config->GetVuhfRxCalGenLevel(calFreq) - adj;
				float fixedLoss = m_mutableConfig.vushfConfig.fixedLoss - (m_mutableConfig.vushfConfig.lightningProt ?
					m_config->GetLightningProtectGain(biteFreq) : 0);
				Units::Frequency rxFreq = m_cmd.freq;
				m_cableLoss = fixedLoss + (measCableLoss - fixedLoss) * 1.05f *
					sqrt(rxFreq.Hz<float>() / biteFreq.Hz<float>());
				float nominalCableLoss = GetRfCableLoss(false, SEquipCtrlMsg::ANT1, m_cmd.freq);

				if (abs(m_cableLoss - nominalCableLoss) > CABLE_LOSS_FAIL_THRESHOLD)
				{
					CString freqString;
					freqString.Format(_T("%.6f"), rxFreq.Hz<double>() / 1e6);
					CString cableLossString;
					cableLossString.Format(_T("%.2f"), m_cableLoss);
					CString nominalCableLossString;
					nominalCableLossString.Format(_T("%.2f"), nominalCableLoss);
					CLog::Log(SMS_BAD_CABLE_LOSS, LOG_CATEGORY_HARDWARE, freqString, cableLossString, nominalCableLossString);
					m_cableLoss = nominalCableLoss;
					m_calibrated = false;
				}
#else
				m_cableLoss = 0;
				m_calibrated = false;
#endif
			}

			break;

		case 2: // Synth cal @ RF freq
			{
				Units::Frequency rxFreq = m_cmd.freq;
				CLog::Log(CLog::VERBOSE, "Cable loss @ %.6f MHz is %.2f dB (nominal %.2f dB), Rx correction is %.2f dB",
					rxFreq.Hz<double>() / 1e6, m_cableLoss, m_config->GetRfCableLoss(false, task->m_ant, rxFreq), 20 * log10(m_rxCorr));
			}

			break;

		default:
			THROW_LOGIC_ERROR();
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Create task from message and return shared pointer
//
CMeasurementTask::MeasurementTask CMeasurementTask::Create(_In_opt_ const SEquipCtrlMsg* cmd, _In_opt_ CNetConnection<void>* source/*, EPriority priority*/)
{
	MeasurementTask task(new CMeasurementTask(cmd, source/*, priority*/), Deleter());
	task->RegisterTask(true);

	return task;
}


//////////////////////////////////////////////////////////////////////
//
// Enough cuts?
//
bool CMeasurementTask::EnoughGoodCuts(void) const
{
	if (m_taskParams.back().mode != CConfig::EMode::DF)
	{
		// Not DF
		return false;
	}

	std::lock_guard<std::mutex> lock(m_critSect);
	CConfig::EPolarization pol = m_taskParams.back().pol;

	return (pol == CConfig::EPolarization::HORIZ || m_dfData[0].size() == m_cmd.dfCmd.repeatCount) &&
		(pol == CConfig::EPolarization::VERT || m_dfData[1].size() == m_cmd.dfCmd.repeatCount);
}


//////////////////////////////////////////////////////////////////////
//
// Obtain Difference Between Mean Sea Level (MSL) And Height Above Elipsoid (HAE)
//
double CMeasurementTask::GetGeoidValue(double latitude, double longitude)
{
	// The geoid table gives the separation distance of the WGS-84 spheroid from the geoid
	//   (MSL height) in units of meters.
	// Each row corresponds to the longitude (0, 10, 20, ..., 170),
	//   and each column to the latitude (90, 80, 70, ..., -90).
	// NOTE: This data is available in a file as well (see HybridGeoFix installation)
	double geoidTable[36][19] = {
	{ 13, 33, 51, 47, 47, 52, 36, 31, 22,  18, 12, 17, 22, 18, 25, 16, 16, -4,-30},
	{ 13, 34, 43, 41, 48, 48, 28, 26, 23,  12, 13, 23, 27, 26, 26, 19, 16, -1,-30},
	{ 13, 28, 29, 21, 42, 35, 29, 15,  2, -13, -2, 21, 34, 31, 34, 25, 17,  1,-30},
	{ 13, 23, 20, 18, 28, 40, 17,  6, -3,  -9,-14,  8, 29, 33, 39, 30, 21,  4,-30},
	{ 13, 17, 12, 14, 12, 33, 12,  1, -7, -28,-25, -9, 14, 39, 45, 35, 20,  4,-30},
	{ 13, 13,  5,  7,-10, -9,-20,-29,-36, -49,-32,-10, 15, 41, 45, 35, 26,  6,-30},
	{ 13,  9, -2, -3,-19,-28,-15,-44,-59, -62,-38,-11, 15, 30, 38, 33, 26,  5,-30},
	{ 13,  4,-10,-22,-33,-39,-40,-61,-90, -89,-60,-20,  7, 24, 39, 30, 22,  4,-30},
	{ 13,  4,-14,-29,-43,-48,-33,-67,-95,-102,-75,-40, -9, 13, 28, 27, 16,  2,-30},
	// -- 90 --
	{ 13,  1,-12,-32,-42,-59,-34,-59,-63, -63,-63,-47,-25, -2, 13, 10, 10, -6,-30},
	{ 13, -2,-10,-32,-43,-50,-34,-36,-24,  -9,-26,-45,-37,-20, -1, -2, -1,-15,-30},
	{ 13, -2,-14,-26,-29,-28,-28,-11, 12,  33,  0,-25,-39,-32,-15,-14,-16,-24,-30},
	{ 13,  0,-12,-15, -2,  3,  7, 21, 53,  58, 35,  5,-23,-33,-22,-23,-29,-33,-30},
	{ 13,  2, -6, -2, 17, 23, 29, 39, 60,  73, 52, 23,-14,-27,-22,-30,-36,-40,-30},
	{ 13,  3, -2, 13, 23, 37, 43, 49, 58,  74, 68, 45, 15,-14,-18,-33,-46,-48,-30},
	{ 13,  2,  3, 17, 22, 18, 20, 39, 46,  63, 76, 58, 33, -2,-15,-29,-55,-50,-30},
	{ 13,  1,  6, 19,  6, -1,  4, 22, 36,  50, 64, 57, 34,  5,-14,-35,-54,-53,-30},
	{ 13,  1,  4,  6,  2,-11, -6, 10, 26,  32, 52, 63, 45, 20,-10,-43,-59,-52,-30},
	// -- 180 --
	{ 13,  3,  2,  2, -8,-12, -7,  5, 13,  22, 36, 51, 46, 21,-15,-45,-61,-53,-30},
	{ 13,  1,  2,  9,  8,-10, -5, 10, 12,  16, 22, 27, 22,  6,-18,-43,-60,-54,-30},
	{ 13, -2,  1, 17,  8,-13, -8,  7, 11,  17, 11, 10,  5,  1,-18,-37,-61,-55,-30},
	{ 13, -3, -1, 10,  1,-20,-15, -7,  2,  13,  6,  0, -2, -7,-16,-32,-55,-52,-30},
	{ 13, -3, -3, 13,-11,-31,-28,-23,-11,   1, -1, -9, -8,-12,-17,-30,-49,-48,-30},
	{ 13, -3, -7,  1,-19,-34,-40,-39,-28, -12, -8,-11,-13,-12,-15,-26,-44,-42,-30},
	{ 13, -1,-14,-14,-16,-21,-42,-47,-38, -23,-10, -5,-10,-12,-10,-23,-38,-38,-30},
	{ 13,  3,-24,-30,-18,-16,-29,-34,-29, -20, -8, -2, -7,-10,-10,-22,-31,-38,-30},
	{ 13,  1,-27,-39,-22,-26,-22, -9,-10, -14,-11, -3, -4, -7, -8,-16,-25,-29,-30},
	// -- 270 --
	{ 13,  5,-25,-46,-35,-34,-26,-10,  3,  -3, -9, -1,  1, -1, -2,-10,-16,-26,-30},
	{ 13,  9,-19,-42,-40,-33,-32,-20,  1,  14,  1,  9,  9,  8,  6, -2, -6,-26,-30},
	{ 13, 11,  3,-21,-26,-35,-51,-45,-11,  10, 32, 35, 32, 23, 14, 10,  1,-24,-30},
	{ 13, 19, 24,  6,-12,-26,-40,-48,-41, -15,  4, 20, 16, 15, 13, 20,  4,-23,-30},
	{ 13, 27, 37, 29, 24,  2,-17,-32,-42, -27,-18, -5,  4, -2,  3, 20,  5,-21,-30},
	{ 13, 31, 47, 49, 45, 33, 17, -9,-16, -18,-13, -6, -8, -6,  3, 21,  4,-19,-30},
	{ 13, 34, 60, 65, 63, 59, 31, 17,  3,  3,  -9, -5,  4,  6, 10, 24,  2,-16,-30},
	{ 13, 33, 61, 60, 62, 52, 34, 25, 17,  12,  4,  0, 12, 21, 20, 22,  6,-12,-30},
	{ 13, 34, 58, 57, 59, 51, 44, 31, 33,  20, 14, 13, 15, 24, 27, 17, 12, -8,-30}};
	double lat, lat1, lat2;
	double lon, lon1, lon2;
	int row, col;
	double a0, a1, a2, a3;
	double n1, n2, n3, n4;
	double x, y;

	// get lat/lon values of a box around the actual lat/long
	lat = 90 - latitude; // values now increase when going south
	lat1 = 10.0 * ceil(lat / 10.0); // bottom of box
	lat2 = 10.0 * floor(lat / 10.0); // top of box
	lon = longitude;
	if (lon < 0.0)
	{
		lon = 360 + lon;
	};
	lon1 = 10.0 * floor(lon / 10.0); // left edge of box
	lon2 = 10.0 * ceil(lon / 10.0); // right edge of box

	// get geoid values in a box surrounding the actual lat/long
	row = int(lon1 / 10.0);
	col = int(lat1 / 10.0);
	n1 = geoidTable[row][col];
	row = int(lon2 / 10.0);
	col = int(lat1 / 10.0);
	n2 = geoidTable[row][col];
	row = int(lon2 / 10.0);
	col = int(lat2 / 10.0);
	n3 = geoidTable[row][col];
	row = int(lon1 / 10.0);
	col = int(lat2 / 10.0);
	n4 = geoidTable[row][col];

	// setup bi-linear interpolation coefficients
	a0 = n1;
	a1 = n2 - n1;
	a2 = n4 - n1;
	a3 = n1 + n3 - n2 - n4;
	x = (lon - lon1) / (lon2 - lon1);
	y = (lat - lat1) / (lat2 - lat1);

	//evaluate bi-linear equation
	return (a0 + a1*x + a2*y + a3*x*y);
}


//////////////////////////////////////////////////////////////////////
//
// Build a VALIDATE_MEAS_RESPONSE message
//
const SEquipCtrlMsg CMeasurementTask::GetValidateResponse(void) const
{
	SEquipCtrlMsg resp;
	resp.hdr = m_msgHdr;
	resp.hdr.msgSubType = SEquipCtrlMsg::VALIDATE_MEAS_RESPONSE;
	resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SValidateMeasurementResp);
	resp.body.validateMeasurementResp.status = ErrorCodes::SUCCESS;
	resp.body.validateMeasurementResp.bwDwellTime = 0;
	resp.body.validateMeasurementResp.freqDwellTime = 0;
	resp.body.validateMeasurementResp.modulationDwellTime = 0;
	resp.body.validateMeasurementResp.fieldStrengthDwellTime = 0;
	resp.body.validateMeasurementResp.dfDwellTime = 0;
	resp.body.validateMeasurementResp.totalTime = 0;

	// Locate first measurement step
	size_t step;
	unsigned long iqCmdTotalTime = 0ul;

	for (step = 0; step < m_taskParams.size(); ++step)
	{
		if (m_taskParams[step].mode == CConfig::EMode::IQDATA)
		{
			// first step is always iqCmd if commanded
			iqCmdTotalTime = static_cast<unsigned long>(0.5 + (1000.0 * m_procParams[step].decimations.GetTotal() * (double(m_procParams[step].sampleSize) /
				Units::Frequency(m_procParams[step].sampleRate).Hz<double>())));
		}
		else if (m_taskParams[step].mode == CConfig::EMode::MEASURE)
		{
			unsigned long dwellTime = std::max(1ul, static_cast<unsigned long>(1000 * m_config->GetDwellTime(m_procParams[step]))); // ms

			if (m_cmd.bwCmd.outputType != SEquipCtrlMsg::NONE)
			{
				resp.body.validateMeasurementResp.bwDwellTime = dwellTime;
				resp.body.validateMeasurementResp.totalTime =
					std::max(resp.body.validateMeasurementResp.totalTime, m_cmd.bwCmd.dwellTime * m_cmd.bwCmd.repeatCount);
			}

			if (m_cmd.freqCmd.outputType != SEquipCtrlMsg::NONE)
			{
				resp.body.validateMeasurementResp.freqDwellTime = dwellTime;
				resp.body.validateMeasurementResp.totalTime =
					std::max(resp.body.validateMeasurementResp.totalTime, m_cmd.freqCmd.dwellTime * m_cmd.freqCmd.repeatCount);
			}

			if (m_cmd.modulationCmd.outputType != SEquipCtrlMsg::NONE)
			{
				resp.body.validateMeasurementResp.modulationDwellTime = dwellTime;
				resp.body.validateMeasurementResp.totalTime =
					std::max(resp.body.validateMeasurementResp.totalTime, m_cmd.modulationCmd.dwellTime * m_cmd.modulationCmd.repeatCount);
			}

			if (m_cmd.fieldStrengthCmd.outputType != SEquipCtrlMsg::NONE)
			{
				resp.body.validateMeasurementResp.fieldStrengthDwellTime = dwellTime;
				resp.body.validateMeasurementResp.totalTime =
					std::max(resp.body.validateMeasurementResp.totalTime, m_cmd.fieldStrengthCmd.dwellTime * m_cmd.fieldStrengthCmd.repeatCount);
			}

			break;
		}
	}
	resp.body.validateMeasurementResp.totalTime += iqCmdTotalTime;

	// Look for DF step (always last)
	if (m_taskParams.size() > 0)
	{
		step = m_taskParams.size() - 1;

//		if (m_taskParams[step].mode == CConfig::DF)
//		{
//			resp.body.validateMeasurementResp.dfDwellTime = std::max(1ul, static_cast<unsigned long>(1000 * GetNumAnts(step, 0) *
//				(m_config->GetDwellTime(m_procParams[step]) + CSingleton<CDigitizer>()->GetLatency(m_procParams[step].sampleRate, m_procParams[step].decimations.GetTotal())))); // ms
//			resp.body.validateMeasurementResp.totalTime += m_cmd.dfCmd.dwellTime;
//		}
	}

	return resp;
}


//////////////////////////////////////////////////////////////////////
//
// Save data for restart
//
void CMeasurementTask::SaveRestartData(_Inout_ std::vector<unsigned char>& data) const
{
	SaveData(data, m_cmd);
	SaveData(data, m_bwDwellCount);
	SaveData(data, m_freqDwellCount);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Calculate the unwrapped phase and fit to a straight line
//
void CMeasurementTask::UnwrapPhase(const Ne10F32cVec& samples, Ne10F32Vec& unwrappedPhase, float& intercept, float& slope)
{
	size_t numSamples = samples.size();
	ne10sPhase(samples, unwrappedPhase);

	// Unwrap the phase (samples are in normal time order)
	for (size_t sample = 1; sample < numSamples; ++sample)
	{
		float unwrap = Units::TWO_PI * floorf((unwrappedPhase[sample] - unwrappedPhase[sample - 1] + Units::PI) / Units::TWO_PI);
		unwrappedPhase[sample] -= unwrap;
	}

	// Least-squares fit to phase slope
	CStats::LeastSquaresFit(unwrappedPhase, slope, intercept);
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update the bandwidth measurement
//
void CMeasurementTask::UpdateBw(const Ne10F32Vec& watts, double binSize, size_t count)
{
	size_t numBins = watts.size();

	// Peak hold spectrum over dwell
	if (m_dwellMaxHoldSpectrum.size() != numBins)
	{
		m_dwellMaxHoldSpectrum.resize(numBins);
		m_dwellMaxHoldSpectrum = -FLT_MAX;
		m_spectrum.resize(numBins);
		m_spectrum = 0;
	}

	// Spectrum
//	size_t nbeg = (watts.size() > 64 ? watts.size() / 2 - 32 : 0);
//	size_t nend = (watts.size() > 64 ? watts.size() / 2 + 31 : watts.size());
//	CFft<ne10_float32_t>::TraceSpectrum("UpdateBw watts", nbeg, nend, &watts[0]);
	m_spectrum += watts;
	ne10sMaxEvery(watts, m_dwellMaxHoldSpectrum);

	// Peak hold total power over dwell
	double power = watts.maximum();

	if (power > m_dwellMaxHoldPower)
	{
		m_dwellMaxHoldPower = power;

		if (m_dwellMaxHoldPower > m_maxHoldPower)
		{
			m_maxHoldPower = m_dwellMaxHoldPower;
		}
	}

	if (count % m_bwDwellCount == m_bwDwellCount - 1)
	{
		// Beta method
		Ne10F32Vec thresholded;
		ne10sThreshold_LTVal(m_dwellMaxHoldSpectrum, thresholded,
			CNoiseEstimate::Create(m_dwellMaxHoldSpectrum, numBins, 0)->GetNoise() * pow(10.0f, m_cmd.bwCmd.yParam / 100.0f), 0);
		double thresh;

		if ((power = thresholded.sum()) != 0)
		{
			thresh = (0.5 - m_cmd.bwCmd.betaParam / 2000.0) * power;
			power = 0;

			for (size_t loBin = 0; loBin < numBins; ++loBin)
			{
				power += thresholded[loBin];

				if (power > thresh)
				{
					m_betaFreqOffsetLo.AddValue(binSize * (loBin - numBins / 2.0 - (power - thresh) / thresholded[loBin]));
					break;
				}
			}

			power = 0;

			for (size_t hiBin = numBins - 1; hiBin < numBins; --hiBin)
			{
				power += thresholded[hiBin];

				if (power > thresh)
				{
					m_betaFreqOffsetHi.AddValue(binSize * (hiBin - numBins / 2.0 + (power - thresh) / thresholded[hiBin]));
					break;
				}
			}
		}

		// xdB method
		thresh = m_dwellMaxHoldPower / pow(10.0, m_cmd.bwCmd.x1Param / 100.0);

		for (size_t loBin = 0; loBin < numBins; ++loBin)
		{
			if (m_dwellMaxHoldSpectrum[loBin] > thresh)
			{
				m_x1FreqOffsetLo.AddValue(binSize * (loBin - numBins / 2.0 - (m_dwellMaxHoldSpectrum[loBin] - thresh) / m_dwellMaxHoldSpectrum[loBin]));
				break;
			}
		}

		for(size_t hiBin = numBins - 1; hiBin < numBins; --hiBin)
		{
			if(m_dwellMaxHoldSpectrum[hiBin] > thresh)
			{
				m_x1FreqOffsetHi.AddValue(binSize * (hiBin - numBins / 2.0 + (m_dwellMaxHoldSpectrum[hiBin] - thresh) / m_dwellMaxHoldSpectrum[hiBin]));
				break;
			}
		}

		thresh = m_dwellMaxHoldPower / pow(10.0, m_cmd.bwCmd.x2Param / 100.0);

		for(size_t loBin = 0; loBin < numBins; ++loBin)
		{
			if(m_dwellMaxHoldSpectrum[loBin] > thresh)
			{
				m_x2FreqOffsetLo.AddValue(binSize * (loBin - numBins / 2.0 - (m_dwellMaxHoldSpectrum[loBin] - thresh) / m_dwellMaxHoldSpectrum[loBin]));
				break;
			}
		}

		for (size_t hiBin = numBins - 1; hiBin < numBins; --hiBin)
		{
			if (m_dwellMaxHoldSpectrum[hiBin] > thresh)
			{
				m_x2FreqOffsetHi.AddValue(binSize * (hiBin - numBins / 2.0 + (m_dwellMaxHoldSpectrum[hiBin] - thresh) / m_dwellMaxHoldSpectrum[hiBin]));
				break;
			}
		}

		if (count == m_bwDwellCount * m_cmd.bwCmd.repeatCount - 1)
		{
			// All done
			m_resp.body.getMeasResp.bwResponse.status = ErrorCodes::SUCCESS;
			m_resp.body.getMeasResp.bwResponse.repeatCount = m_betaFreqOffsetHi.GetCount();
			m_resp.body.getMeasResp.bwResponse.freq = Units::Frequency(m_cmd.freq).GetRaw();
			m_resp.body.getMeasResp.bwResponse.binSize = Units::Frequency(binSize).GetRaw();

			switch(m_cmd.bwCmd.aveMethod)
			{
			case SEquipCtrlMsg::MAX_HOLD:
				CalcBw(m_betaFreqOffsetLo.GetMin(), m_betaFreqOffsetHi.GetMax(), Units::Frequency(m_cmd.freq), binSize, CFft::GetBwCorrection(3),
					m_resp.body.getMeasResp.bwResponse.betaFreqLow, m_resp.body.getMeasResp.bwResponse.betaFreqHigh,
					m_resp.body.getMeasResp.bwResponse.betaBw);
				m_resp.body.getMeasResp.bwResponse.betaStdDev = float(sqrt(m_betaFreqOffsetLo.GetVar() + m_betaFreqOffsetHi.GetVar()));
				CalcBw(m_x1FreqOffsetLo.GetMin(), m_x1FreqOffsetHi.GetMax(), m_cmd.freq, binSize, CFft::GetBwCorrection(m_cmd.bwCmd.x1Param / 10.0),
					m_resp.body.getMeasResp.bwResponse.x1FreqLow, m_resp.body.getMeasResp.bwResponse.x1FreqHigh,
					m_resp.body.getMeasResp.bwResponse.x1Bw);
				m_resp.body.getMeasResp.bwResponse.x1StdDev = float(sqrt(m_x1FreqOffsetLo.GetVar() + m_x1FreqOffsetHi.GetVar()));
				CalcBw(m_x2FreqOffsetLo.GetMin(), m_x2FreqOffsetHi.GetMax(), m_cmd.freq, binSize, CFft::GetBwCorrection(m_cmd.bwCmd.x2Param / 10.0),
					m_resp.body.getMeasResp.bwResponse.x2FreqLow, m_resp.body.getMeasResp.bwResponse.x2FreqHigh,
					m_resp.body.getMeasResp.bwResponse.x2Bw);
				m_resp.body.getMeasResp.bwResponse.x2StdDev = float(sqrt(m_x2FreqOffsetLo.GetVar() + m_x2FreqOffsetHi.GetVar()));
				break;

			case SEquipCtrlMsg::MEAN_AVE:
				CalcBw(m_betaFreqOffsetLo.GetMean(), m_betaFreqOffsetHi.GetMean(), m_cmd.freq, binSize, CFft::GetBwCorrection(3),
					m_resp.body.getMeasResp.bwResponse.betaFreqLow, m_resp.body.getMeasResp.bwResponse.betaFreqHigh,
					m_resp.body.getMeasResp.bwResponse.betaBw);
				m_resp.body.getMeasResp.bwResponse.betaStdDev = float(sqrt(m_betaFreqOffsetLo.GetVar() + m_betaFreqOffsetHi.GetVar()));
				CalcBw(m_x1FreqOffsetLo.GetMean(), m_x1FreqOffsetHi.GetMean(), m_cmd.freq, binSize, CFft::GetBwCorrection(m_cmd.bwCmd.x1Param / 10.0),
					m_resp.body.getMeasResp.bwResponse.x1FreqLow, m_resp.body.getMeasResp.bwResponse.x1FreqHigh,
					m_resp.body.getMeasResp.bwResponse.x1Bw);
				m_resp.body.getMeasResp.bwResponse.x1StdDev = float(sqrt(m_x1FreqOffsetLo.GetVar() + m_x1FreqOffsetHi.GetVar()));
				CalcBw(m_x2FreqOffsetLo.GetMean(), m_x2FreqOffsetHi.GetMean(), m_cmd.freq, binSize, CFft::GetBwCorrection(m_cmd.bwCmd.x2Param / 10.0),
					m_resp.body.getMeasResp.bwResponse.x2FreqLow, m_resp.body.getMeasResp.bwResponse.x2FreqHigh,
					m_resp.body.getMeasResp.bwResponse.x2Bw);
				m_resp.body.getMeasResp.bwResponse.x2StdDev = float(sqrt(m_x2FreqOffsetLo.GetVar() + m_x2FreqOffsetHi.GetVar()));
				break;

			case SEquipCtrlMsg::RMS_AVE:
				CalcBw(m_betaFreqOffsetLo.GetRms(), m_betaFreqOffsetHi.GetRms(), m_cmd.freq, binSize, CFft::GetBwCorrection(3),
					m_resp.body.getMeasResp.bwResponse.betaFreqLow, m_resp.body.getMeasResp.bwResponse.betaFreqHigh,
					m_resp.body.getMeasResp.bwResponse.betaBw);
				m_resp.body.getMeasResp.bwResponse.betaStdDev = float(sqrt(m_betaFreqOffsetLo.GetVar() + m_betaFreqOffsetHi.GetVar()));
				CalcBw(m_x1FreqOffsetLo.GetRms(), m_x1FreqOffsetHi.GetRms(), m_cmd.freq, binSize, CFft::GetBwCorrection(m_cmd.bwCmd.x1Param / 10.0),
					m_resp.body.getMeasResp.bwResponse.x1FreqLow, m_resp.body.getMeasResp.bwResponse.x1FreqHigh,
					m_resp.body.getMeasResp.bwResponse.x1Bw);
				m_resp.body.getMeasResp.bwResponse.x1StdDev = float(sqrt(m_x1FreqOffsetLo.GetVar() + m_x1FreqOffsetHi.GetVar()));
				CalcBw(m_x2FreqOffsetLo.GetRms(), m_x2FreqOffsetHi.GetRms(), m_cmd.freq, binSize, CFft::GetBwCorrection(m_cmd.bwCmd.x2Param / 10.0),
					m_resp.body.getMeasResp.bwResponse.x2FreqLow, m_resp.body.getMeasResp.bwResponse.x2FreqHigh,
					m_resp.body.getMeasResp.bwResponse.x2Bw);
				m_resp.body.getMeasResp.bwResponse.x2StdDev = float(sqrt(m_x2FreqOffsetLo.GetVar() + m_x2FreqOffsetHi.GetVar()));
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			if (m_cmd.bwCmd.outputType == SEquipCtrlMsg::DETAILS)
			{
				m_spectrum /= float(count + 1);
				m_resp.body.getMeasResp.bwResponse.numBins = (numBins < SEquipCtrlMsg::BWBINS_MAX ? numBins : SEquipCtrlMsg::BWBINS_MAX);
				Ne10F32Vec dbm(CFft::WattsToDbm(m_spectrum));

//				size_t nbeg = (m_spectrum.size() > 64 ? m_spectrum.size() / 2 - 32 : 0);
//				size_t nend = (m_spectrum.size() > 64 ? m_spectrum.size() / 2 + 31 : m_spectrum.size());
//				CFft<ne10_float32_t>::TraceSpectrum("UpdateBw m_spectrum", nbeg, nend, &m_spectrum[0]);

				dbm -= float(10.0 * log10(m_maxHoldPower)) + 30 - SEquipCtrlMsg::BWBIN_OFFSET;
				ne10sConvert(&dbm[0], m_resp.body.getMeasResp.bwBinData, m_resp.body.getMeasResp.bwResponse.numBins, 0);
			}

			if (IsAvd())
			{
				if (m_betaFreqOffsetLo.GetCount() > 0)
				{
					dynamic_cast<CAvdTask*>(this)->UpdateBw(Units::Frequency(m_resp.body.getMeasResp.bwResponse.betaBw).Hz<double>());
				}

				m_betaFreqOffsetLo.Clear();
				m_betaFreqOffsetHi.Clear();
			}
		}
		m_dwellMaxHoldPower = -DBL_MAX;
		m_dwellMaxHoldSpectrum = -FLT_MAX;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update the field strength measurement
//
void CMeasurementTask::UpdateFieldStrength(_In_ const Ne10F32cVec& samples, float gainAdj, _In_ const CTask::Task& task)
{
	Ne10F32Vec mag;		// was Ipp64f
	ne10sMagnitude(samples, mag);
	mag *= sqrt(gainAdj); // gainAdj is a power ratio

	UpdateFieldStrength(mag, task);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update the field strength measurement
//
void CMeasurementTask::UpdateFieldStrength(const Ne10F32Vec& magnitude, const CTask::Task& task)
{
	Ne10F32Vec mag = magnitude;

	switch (m_cmd.fieldStrengthCmd.fieldMethod)
	{
	case SEquipCtrlMsg::LINAVERAGE:
		m_dwellFieldStrength.AddValue(mag.mean() * m_rxCorr);
		break;

	case SEquipCtrlMsg::LOGAVERAGE:
		mag *= m_rxCorr;
		ne10sLn(mag);
		m_dwellFieldStrength.AddValue(mag.mean());
		break;

	case SEquipCtrlMsg::RMS:
		mag *= m_rxCorr;
		ne10sSqr(mag);
		m_dwellFieldStrength.AddValue(mag.mean());
		break;

	case SEquipCtrlMsg::PEAK:
		m_dwellFieldStrength.AddValue(mag.maximum() * m_rxCorr);
		break;

	default:
		THROW_LOGIC_ERROR();
	}

	if (task->m_currentBlock % m_fieldStrengthDwellCount == m_fieldStrengthDwellCount - 1)
	{
		// Last in dwell
		switch (m_cmd.fieldStrengthCmd.fieldMethod)
		{
		case SEquipCtrlMsg::LINAVERAGE:
			m_fieldStrength.AddValue(m_dwellFieldStrength.GetMean());
			break;

		case SEquipCtrlMsg::LOGAVERAGE:
			m_fieldStrength.AddValue(m_dwellFieldStrength.GetMean());
			break;

		case SEquipCtrlMsg::RMS:
			m_fieldStrength.AddValue(sqrt(m_dwellFieldStrength.GetMean()));
			break;

		case SEquipCtrlMsg::PEAK:
			m_fieldStrength.AddValue(m_dwellFieldStrength.GetMax());
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		m_dwellFieldStrength.Clear();

		if (task->m_currentBlock == m_fieldStrengthDwellCount * m_cmd.fieldStrengthCmd.repeatCount - 1)
		{
			// All done
			m_resp.body.getMeasResp.fieldStrengthResponse.status = (m_calibrated ? ErrorCodes::SUCCESS : ErrorCodes::UNCALIBRATED);
			m_resp.body.getMeasResp.fieldStrengthResponse.repeatCount = m_fieldStrength.GetCount();
			m_resp.body.getMeasResp.fieldStrengthResponse.fieldMethod = m_cmd.fieldStrengthCmd.fieldMethod;
			double fieldStrength;

			switch(m_cmd.fieldStrengthCmd.aveMethod)
			{
			case SEquipCtrlMsg::MAX_HOLD:
				fieldStrength = m_fieldStrength.GetMax();
				break;

			case SEquipCtrlMsg::MEAN_AVE:
				fieldStrength = m_fieldStrength.GetMean();
				break;

			case SEquipCtrlMsg::RMS_AVE:
				fieldStrength = m_fieldStrength.GetRms();
				// handle negative for RMS
				//if (mag.mean() < 0)
				//	fieldStrength = 0 - fieldStrength;
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			// Convert to dBm
			double dbm;
			double stdDev;

			if (m_cmd.fieldStrengthCmd.fieldMethod == SEquipCtrlMsg::LOGAVERAGE)
			{
				dbm = 20 * fieldStrength / log(10.0) + 30;
				stdDev = 20 * m_fieldStrength.GetStdDev() / log(10.0); // dB
			}
			else
			{
				dbm = 20 * log10(fieldStrength) + 30;
				stdDev = 20 * log10(exp(1.0)) * m_fieldStrength.GetStdDev() / fieldStrength; // dB
			}

			float lightningGain = m_config->GetLightningProtectGain(task->GetTaskParams().hf, task->m_ant, m_cmd.freq);
			float switchGain = m_config->GetSwitchGain(task->GetTaskParams().hf, task->m_ant, m_cmd.freq) +
					    task->BlockState().gainAdjdB;

			float antFactor = m_config->GetAntennaFactor(task->GetTaskParams().hf, task->m_ant, m_cmd.freq, task->GetTaskParams().pol == CConfig::EPolarization::HORIZ);
			m_resp.body.getMeasResp.fieldStrengthResponse.rxPowerDbm = float(dbm);
			m_resp.body.getMeasResp.fieldStrengthResponse.powerDbm = float(dbm + m_cableLoss - lightningGain - switchGain);
			m_resp.body.getMeasResp.fieldStrengthResponse.eField = float(dbm + m_cableLoss - lightningGain - switchGain + antFactor);
			m_resp.body.getMeasResp.fieldStrengthResponse.pFlux = m_resp.body.getMeasResp.fieldStrengthResponse.eField - 25.8f;
			m_resp.body.getMeasResp.fieldStrengthResponse.stdDev = float(stdDev);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update the frequency measurement
//
void CMeasurementTask::UpdateFreq(float slope, double sampleRate, size_t count, size_t step)
{
	// IFM Method
	m_dwellFreqOffset.AddValue(slope * sampleRate / Units::TWO_PI);

	if (count % m_freqDwellCount == m_freqDwellCount - 1)
	{
		UpdateFreqDwell(count, step);
	}

	return;
}

void CMeasurementTask::UpdateFreq(const Ne10F32Vec& watts, double binSize, size_t count, size_t step)
{
	// FFT Method
	// Calculate power-weighted average frequency thresholded to remove noise

	Ne10F32Vec thresholded;
	const float SNR = 100.f; // Ratio (20 dB)
	ne10sThreshold_LTVal(watts, thresholded, CNoiseEstimate::Create(watts, watts.size(), 0)->GetNoise() * SNR, 0);

	size_t numBins = watts.size();
	Ne10F32Vec binIdx(numBins);
	ne10sVectorRamp(binIdx, -float(numBins / 2), 1);

	ne10_float32_t dotProd;
	ne10sDotProd(thresholded, binIdx, dotProd);

	if (ne10_float32_t sum = thresholded.sum())
	{
		m_dwellFreqOffset.AddValue(dotProd / sum * binSize);
	}

	if (count % m_freqDwellCount == m_freqDwellCount - 1)
	{
		UpdateFreqDwell(count, step);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update the frequency measurement (common code)
//
void CMeasurementTask::UpdateFreqDwell(size_t count, size_t step)
{
	// Last in dwell
	if (m_dwellFreqOffset.GetCount() > 0)
	{
		m_freqOffset.AddValue(m_dwellFreqOffset.GetMean());
	}

	m_dwellFreqOffset.Clear();

	if (count == m_freqDwellCount * m_cmd.freqCmd.repeatCount - 1)
	{
		// All done
		m_resp.body.getMeasResp.freqResponse.status = ErrorCodes::SUCCESS;
		m_resp.body.getMeasResp.freqResponse.repeatCount = m_freqOffset.GetCount();
		double freqOffset;

		switch(m_cmd.freqCmd.aveMethod)
		{
		case SEquipCtrlMsg::MAX_HOLD:
			if (m_freqOffset.GetMean() < 0)
			{
				freqOffset = m_freqOffset.GetMin();
			}
			else
			{
				freqOffset = m_freqOffset.GetMax();
			}

			break;

		case SEquipCtrlMsg::MEAN_AVE:
			freqOffset = m_freqOffset.GetMean();
			break;

		case SEquipCtrlMsg::RMS_AVE:
			freqOffset = m_freqOffset.GetRms();
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		m_resp.body.getMeasResp.freqResponse.freqOffset = float(freqOffset);
		m_resp.body.getMeasResp.freqResponse.freq = Units::Frequency(m_cmd.freq).Hz<double>() + freqOffset;
		m_resp.body.getMeasResp.freqResponse.stdDev = float(m_freqOffset.GetStdDev());

		if (IsAvd())
		{
			if (m_freqOffset.GetCount() > 0)
			{
				dynamic_cast<CAvdTask*>(this)->UpdateFreq(m_taskParams[step].startFreq.Hz<double>() + freqOffset);
			}

			m_freqOffset.Clear();
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update the IQ data measurement
//
void CMeasurementTask::UpdateIqData(const uncached_ptr samples, const std::vector<unsigned long>& slaveSamples, const unsigned int numSamples, const unsigned int packetIndex,
	const CTask::Task& task, bool lastPacket, int slaveSampleIndex)
{
	// if samples == nullptr and slaveSamples empty, then there was error - just clear iqResponse part of m_resp and return
	if (!samples && slaveSamples.empty())
	{
		memset(&(m_resp.body.getMeasResp.iqResponse), 0, sizeof(SEquipCtrlMsg::SIqResponse));
		m_resp.body.getMeasResp.iqResponse.status = ErrorCodes::UNABLETOGETDATA;
		return;
	}
	// Create and send a message with a block of iq data
	if (m_iqDataSamples > 0)
	{
		unsigned int samplesToSend = std::min(numSamples, m_iqDataSamples);
		m_iqResp.body.iqDataResp.numIqData = static_cast<unsigned short>(samplesToSend);
		m_iqResp.body.iqDataResp.packetIndex = packetIndex;

		if (samplesToSend < SEquipCtrlMsg::SIqDataResp::IQDATA_SIZE)
		{
			memset(&m_iqResp.body.iqDataResp.iqData[samplesToSend], 0, (SEquipCtrlMsg::SIqDataResp::IQDATA_SIZE - samplesToSend) * sizeof(SEquipCtrlMsg::SIqDataResp::SIq));
		}

		auto dst = reinterpret_cast<unsigned long*>(m_iqResp.body.iqDataResp.iqData);
		const unsigned long* src;
		if (samples)
		{
			src = const_cast<const unsigned long*>(samples.get());
			ne10sCopyUncached(src, dst, samplesToSend);
		}
		else if (!slaveSamples.empty())
		{
			src = &slaveSamples[slaveSampleIndex];
			memcpy(dst, src, samplesToSend * sizeof(unsigned long));
		}
		CEquipControl::Send(task->m_source, m_iqResp); // too many too fast to log
		m_iqDataSamples -= samplesToSend;
	}

	if (lastPacket)
	{
		const CConfig::SProcParams& procParams = task->GetProcParams();

		memset(&(m_resp.body.getMeasResp.iqResponse), 0, sizeof(SEquipCtrlMsg::SIqResponse));
		if (!m_cmd.iqCmd.tdoa || task->m_blockState.sampleTime.IsValid())
		{
			m_resp.body.getMeasResp.iqResponse.status = ErrorCodes::SUCCESS;
		}
		else
		{
			// don't have accurate timestamp
			m_resp.body.getMeasResp.iqResponse.status = ErrorCodes::UNABLETOGETDATA;
		}
		if (m_license->HasPrecisionTimestamp())
		{
			m_resp.body.getMeasResp.iqResponse.firstSampleTime = task->m_blockState.sampleTime.GetRaw();
		}
		m_resp.body.getMeasResp.iqResponse.latitude = m_resp.body.getMeasResp.gpsResponse.latitude;
		m_resp.body.getMeasResp.iqResponse.longitude = m_resp.body.getMeasResp.gpsResponse.longitude;
		m_resp.body.getMeasResp.iqResponse.altitude = float(m_altitude) + float(GetGeoidValue(m_resp.body.getMeasResp.gpsResponse.latitude,
			m_resp.body.getMeasResp.gpsResponse.longitude));  // MSL-to HAE conversion
		m_resp.body.getMeasResp.iqResponse.freq = m_cmd.freq;
		unsigned long up, down;
		procParams.decimations.GetUpDown(up, down);
		m_resp.body.getMeasResp.iqResponse.sampleRate = (procParams.sampleRate * up / down).GetRaw();
		m_resp.body.getMeasResp.iqResponse.numSamplesTotal = m_cmd.iqCmd.numSamples;
		m_resp.body.getMeasResp.iqResponse.numDataPackets = m_cmd.iqCmd.numSamples / SEquipCtrlMsg::SIqDataResp::IQDATA_SIZE;

		if((m_cmd.iqCmd.numSamples % SEquipCtrlMsg::SIqDataResp::IQDATA_SIZE) != 0)
		{
			m_resp.body.getMeasResp.iqResponse.numDataPackets++;
		}
	}


}


//////////////////////////////////////////////////////////////////////
//
// Update the modulation measurement
//
void CMeasurementTask::UpdateMod(_In_ const Ne10F32cVec& samples, const Ne10F32Vec& unwrappedPhase, float phaseOffset, float phaseSlope, double sampleRate, double bw, size_t count)
{
	// AM - use 2nd and 98th percentiles instead of mmin and max
	Ne10F32Vec mag;		// was Ipp64
	ne10sMagnitude(samples, mag);
	double eCarrier = mag.mean();
	m_dwellAmPlus.AddValue((CStats::Percentile(mag, 98) - eCarrier) / eCarrier);
	m_dwellAmMinus.AddValue((eCarrier - CStats::Percentile(mag, 2)) / eCarrier);

	// FM
	static size_t ncoefs = 31;		// Filter used in ne10sDeriv31

	size_t numSamples = samples.size();

	// Differentiate the complex signal into deriv
	size_t numFiltered = numSamples - ncoefs + 1; // Number of usable samples post-differentiator
	Ne10F32cVec deriv(numFiltered);
	for (size_t n = 0; n < numFiltered; ++n)
	{
		ne10sDeriv31(&samples[n], &deriv[n]);
	}

	// Divide derivative by offset signal (thresholding zeros)
	ne10sDiv(&samples[(ncoefs - 1) / 2], &deriv[0], numFiltered, 1.e-6);

    // deriv now has FM signal f(t) in imag part
	Ne10F32Vec fm;		// was Ipp64fVec
	ne10sImag(deriv, fm);

	// Remove constant slope/offset as a tuning error using least-squares best fit line
	float slope;		// was double
	float offset;		// was double
	CStats::LeastSquaresFit(fm, slope, offset);
	Ne10F32Vec bestFit(numFiltered);		// was Ipp64fVec
	ne10sVectorRamp(bestFit, offset, slope);
	fm -= bestFit;

	// Find max(abs(f(t)) and rms(f(t))
	float rms;		// was double
	ne10sNorm_L2(fm, rms);
	rms /= sqrt(float(numFiltered));
	ne10sAbs(fm);
	double fmPeak = CStats::Percentile(fm, 98.5f); // Actually 98.5th percentile

	// Normalize data and accumulate results
	if ((fmPeak *= sampleRate) > bw)
	{
		fmPeak = bw;
	}

	if (fmPeak > m_dwellFmPeak)
	{
		m_dwellFmPeak = fmPeak;
	}

	if ((rms *= sampleRate) > bw)
	{
		rms = bw;
	}

	m_dwellFmRms.AddValue(rms);

	// PM
	bestFit.resize(numSamples);
	ne10sVectorRamp(bestFit, phaseOffset, phaseSlope);
	auto phase = unwrappedPhase;	// make a local copy
	phase -= bestFit;

	// Find max(abs(ph(t)) and rms(ph(t))
	ne10sNorm_L2(phase, rms);
	rms /= sqrt(float(numSamples));
	ne10sAbs(phase);
	double pmPeak = CStats::Percentile(phase, 98); // Actually 98.0th percentile

	// Accumulate results
	if (pmPeak > m_dwellPmPeak)
	{
		m_dwellPmPeak = pmPeak;
	}

	m_dwellPmRms.AddValue(rms);

	// Check for end of dwell
	if (count % m_modDwellCount == m_modDwellCount - 1)
	{
		// AM
		m_amPlus.AddValue(m_dwellAmPlus.GetMax());
		m_amMinus.AddValue(m_dwellAmMinus.GetMax());
		m_amDepth.AddValue((m_dwellAmPlus.GetMax() + m_dwellAmMinus.GetMax()) / 2);

		// FM
		m_fmPeak.AddValue(m_dwellFmPeak);
		m_fmRms.AddValue(m_dwellFmRms.GetRms());

		// PM
		m_pmPeak.AddValue(m_dwellPmPeak);
		m_pmRms.AddValue(m_dwellPmRms.GetRms());

		// All done?
		if (count == m_modDwellCount * m_cmd.modulationCmd.repeatCount - 1)
		{
			// Fill in results
			m_resp.body.getMeasResp.modulationResponse.status = ErrorCodes::SUCCESS;
			m_resp.body.getMeasResp.modulationResponse.repeatCount = m_amDepth.GetCount();

			switch(m_cmd.modulationCmd.aveMethod)
			{
			case SEquipCtrlMsg::MAX_HOLD:
				m_resp.body.getMeasResp.modulationResponse.amModPlus = float(m_amPlus.GetMax());
				m_resp.body.getMeasResp.modulationResponse.amModMinus = float(m_amMinus.GetMax());
				m_resp.body.getMeasResp.modulationResponse.amModDepth = float(m_amDepth.GetMax());
				m_resp.body.getMeasResp.modulationResponse.fmPeakFreq = float(m_fmPeak.GetMax());
				m_resp.body.getMeasResp.modulationResponse.fmRmsFreq = float(m_fmRms.GetMax());
				m_resp.body.getMeasResp.modulationResponse.pmRmsPhase = float(m_pmRms.GetMax());
				m_resp.body.getMeasResp.modulationResponse.pmPeakPhase = float(m_pmPeak.GetMax());
				break;

			case SEquipCtrlMsg::MEAN_AVE:
				m_resp.body.getMeasResp.modulationResponse.amModPlus = float(m_amPlus.GetMean());
				m_resp.body.getMeasResp.modulationResponse.amModMinus = float(m_amMinus.GetMean());
				m_resp.body.getMeasResp.modulationResponse.amModDepth = float(m_amDepth.GetMean());
				m_resp.body.getMeasResp.modulationResponse.fmPeakFreq = float(m_fmPeak.GetMean());
				m_resp.body.getMeasResp.modulationResponse.fmRmsFreq = float(m_fmRms.GetMean());
				m_resp.body.getMeasResp.modulationResponse.pmRmsPhase = float(m_pmRms.GetMean());
				m_resp.body.getMeasResp.modulationResponse.pmPeakPhase = float(m_pmPeak.GetMean());
				break;

			case SEquipCtrlMsg::RMS_AVE:
				m_resp.body.getMeasResp.modulationResponse.amModPlus = float(m_amPlus.GetRms());
				m_resp.body.getMeasResp.modulationResponse.amModMinus = float(m_amMinus.GetRms());
				m_resp.body.getMeasResp.modulationResponse.amModDepth = float(m_amDepth.GetRms());
				m_resp.body.getMeasResp.modulationResponse.fmPeakFreq = float(m_fmPeak.GetRms());
				m_resp.body.getMeasResp.modulationResponse.fmRmsFreq = float(m_fmRms.GetRms());
				m_resp.body.getMeasResp.modulationResponse.pmRmsPhase = float(m_pmRms.GetRms());
				m_resp.body.getMeasResp.modulationResponse.pmPeakPhase = float(m_pmPeak.GetRms());
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			if (m_cmd.modulationCmd.outputType == SEquipCtrlMsg::DETAILS)
			{
				m_resp.body.getMeasResp.modulationResponse.numAmHistBins = static_cast<unsigned char>(m_amDepth.GetHistNumBins());
				m_resp.body.getMeasResp.modulationResponse.amHistBinsize = float(m_amDepth.GetHistBinSize());
				assert(m_resp.body.getMeasResp.modulationResponse.numAmHistBins <= SEquipCtrlMsg::NUMHISTBINS_MAX);
				m_amDepth.GetProbDist(m_resp.body.getMeasResp.amProbDist);
				m_resp.body.getMeasResp.modulationResponse.numFmHistBins = static_cast<unsigned char>(m_fmPeak.GetHistNumBins());
				m_resp.body.getMeasResp.modulationResponse.fmHistBinsize = float(m_fmPeak.GetHistBinSize());
				assert(m_resp.body.getMeasResp.modulationResponse.numFmHistBins <= SEquipCtrlMsg::NUMHISTBINS_MAX);
				m_fmPeak.GetProbDist(m_resp.body.getMeasResp.fmProbDist);
				m_resp.body.getMeasResp.modulationResponse.numPmHistBins = static_cast<unsigned char>(m_pmPeak.GetHistNumBins());
				m_resp.body.getMeasResp.modulationResponse.pmHistBinsize = float(m_pmPeak.GetHistBinSize());
				assert(m_resp.body.getMeasResp.modulationResponse.numPmHistBins <= SEquipCtrlMsg::NUMHISTBINS_MAX);
				m_pmPeak.GetProbDist(m_resp.body.getMeasResp.pmProbDist);
			}
		}

		// Reset AM for next dwell
		m_dwellAmMinus.Clear();
		m_dwellAmPlus.Clear();

		// Reset FM for next dwell
		m_dwellFmPeak = 0;
		m_dwellFmRms.Clear();

		// Reset PM for next dwell
		m_dwellPmPeak = 0;
		m_dwellPmRms.Clear();
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate a command
//
SEquipCtrlMsg CMeasurementTask::Validate(_In_ const SEquipCtrlMsg::SGetMeasCmd& cmd)
{
	SEquipCtrlMsg msg;
	msg.hdr.msgType = SEquipCtrlMsg::METRICS_CTRL;
	msg.hdr.msgSubType = SEquipCtrlMsg::VALIDATE_MEAS;
	msg.hdr.bodySize = sizeof(SEquipCtrlMsg::SGetMeasCmd);
	msg.body.getMeasCmd = cmd;
	msg = CMeasurementTask(&msg, nullptr/*, CTask::NON_QUEUED*/).GetValidateResponse();

	return msg;
}
