/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/
#include "stdafx.h"

#include "Digitizer.h"
#include "Monitor.h"
#include "Navigation.h"
#include "RadioEquip.h"
#include "Singleton.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CMonitor::CMonitor(size_t numSlaves)
{
	//Initial environmental fault conditions.
	m_faultCond.cond = SSmsMsg::FAIL_STAT;	//Indicate fail until proven otherwise.

	// Create default faultDetails
	UFaultDetails& faultDetails = reinterpret_cast<UFaultDetails&>(m_faultDefault);
	faultDetails.bistCond = SSmsMsg::BIST_STAT;

	for (size_t i = 0; i < std::extent<decltype(faultDetails.voltages3230)>::value; ++i)
	{
		faultDetails.voltages3230[i].fval = 0;
		faultDetails.voltages3230[i].cond = SSmsMsg::NO_VAL_STAT;
	}

	for (size_t i = 0; i < std::extent<decltype(faultDetails.temps3230)>::value; ++i)
	{
		faultDetails.temps3230[i].fval = 0;
		faultDetails.temps3230[i].cond = SSmsMsg::NO_VAL_STAT;
	}

	for (size_t i = 0; i < std::extent<decltype(faultDetails.clocks3230)>::value; ++i)
	{
		faultDetails.clocks3230[i].ulval = 0;
		faultDetails.clocks3230[i].cond = SSmsMsg::NO_VAL_STAT;
	}

	for (size_t i = 0; i < std::extent<decltype(faultDetails.temps2630)>::value; ++i)
	{
		faultDetails.temps2630[i].fval = 0;
		faultDetails.temps2630[i].cond = SSmsMsg::NO_VAL_STAT;
	}

	faultDetails.ocxoLock.ulval = 0;
	faultDetails.ocxoLock.cond = SSmsMsg::NO_VAL_STAT;

	// Initialize response with defaults
	m_faultCond.env = m_faultDefault;
	m_faultCond.numSlaves = numSlaves;
	TRACE("CMonitor setting numSlaves to %u\n", numSlaves);

	for (size_t i = 0; i < numSlaves; ++i)
	{
		m_faultCond.slaves[i] = m_faultDefault;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CMonitor::~CMonitor(void)
{

}


//////////////////////////////////////////////////////////////////////
//
// Add "no response" to overall responses
//
void CMonitor::AddSlaveResponse(size_t nodeIndex)
{
	if (nodeIndex >= SSmsMsg::SGetCsmsFaultResp::MAX_SLAVES)
		return;

	std::lock_guard<std::mutex> lock(m_faultMutex);
	m_faultCond.slaves[nodeIndex] = m_faultDefault;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Add slave response to overall responses
//
void CMonitor::AddSlaveResponse(size_t nodeIndex, const SDfCtrlMsg::SGetSlaveCsmsFaultResp& slaveResp)
{
	if (nodeIndex >= SSmsMsg::SGetCsmsFaultResp::MAX_SLAVES)
		return;

	std::lock_guard<std::mutex> lock(m_faultMutex);
	m_faultCond.slaves[nodeIndex] = slaveResp.env;

	// Update overall status
	if (slaveResp.cond == SSmsMsg::FAIL_STAT)
	{
		TRACE("AddSlaveResponse setting fault status to FAIL\n");
		m_faultCond.cond = SSmsMsg::FAIL_STAT;
	}
	else if (slaveResp.cond == SSmsMsg::WARNING_STAT && m_faultCond.cond == SSmsMsg::PASS_STAT)
	{
		TRACE("AddSlaveResponse setting fault status to WARNING\n");
		m_faultCond.cond = SSmsMsg::WARNING_STAT;
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Monitor all of the hardware
//
bool CMonitor::Monitor(bool doSlow, SSmsMsg::EErrStatus& bistSummary, long maxDiffNsec, bool& has3230Timestamp)
{
	try
	{
		CWeakSingleton<CNavigation> navigation;
		CWeakSingleton<CDigitizer> digitizer;
		CWeakSingleton<CRadioEquip> radioEquip;

		// Get the current fault status
		SEquipCtrlMsg::SGetCsmsFaultResp faultStat;
		UFaultDetails& faultDetails = reinterpret_cast<UFaultDetails&>(faultStat.env);		// alias

		GetFaultDetails(faultStat);
		auto previousFaultCond = faultStat.cond;

		faultStat.cond = SSmsMsg::PASS_STAT;		// Initialize overall status

		// Get last Bist status
		faultStat.env.bistCond = bistSummary;
		if (faultStat.env.bistCond == SSmsMsg::WARNING_STAT)
		{
			faultStat.cond = SSmsMsg::WARNING_STAT;
		}
		else if (faultStat.env.bistCond == SSmsMsg::FAIL_STAT)
		{
			faultStat.cond = SSmsMsg::FAIL_STAT;
		}

		// Get and check temperatures
		auto temps = digitizer->GetTemperatures();
		auto tempLimits = digitizer->GetTempLimits();
		assert(temps.size() == tempLimits.size());

		for (unsigned int i = 0; i < temps.size(); ++i)
		{
			if (i < std::extent<decltype(faultDetails.temps3230)>::value)
			{
				faultDetails.temps3230[i].fval = temps[i];
				faultDetails.temps3230[i].cond = SSmsMsg::PASS_STAT;
				if (temps[i] < tempLimits[i][0] || temps[i] > tempLimits[i][1])
				{
					faultDetails.temps3230[i].cond = SSmsMsg::WARNING_STAT;

					if (faultStat.cond == SSmsMsg::PASS_STAT)
						faultStat.cond = SSmsMsg::WARNING_STAT;
				}
			}
		}

		// Get and check radio temperatures
		std::vector<float> rxtemps;
		if (radioEquip->ReadRxTemperatures(rxtemps))
		{
			std::vector<std::array<float, 2> > tempLimits = radioEquip->GetTempLimits();
			assert(rxtemps.size() == tempLimits.size());

			for (unsigned int i = 0; i < rxtemps.size(); ++i)
			{
				if (i < std::extent<decltype(faultDetails.temps2630)>::value)
				{
					faultDetails.temps2630[i].fval = rxtemps[i];
					faultDetails.temps2630[i].cond = SSmsMsg::PASS_STAT;
					if (rxtemps[i] < tempLimits[i][0] || rxtemps[i] > tempLimits[i][1])
					{
						faultDetails.temps2630[i].cond = SSmsMsg::WARNING_STAT;

						if (faultStat.cond == SSmsMsg::PASS_STAT)
							faultStat.cond = SSmsMsg::WARNING_STAT;
					}
				}
			}
			// Set radio temperature used for gain calculations
			radioEquip->UpdateRadioTemp(rxtemps);
		}
		else
		{
			for (size_t i = 0; i < std::extent<decltype(faultDetails.temps2630)>::value; ++i)
			{
				faultDetails.temps2630[i].fval = 0;
				faultDetails.temps2630[i].cond = SSmsMsg::NO_VAL_STAT;
				if (faultStat.cond == SSmsMsg::PASS_STAT)
					faultStat.cond = SSmsMsg::WARNING_STAT;
			}
		}

		if (doSlow)
		{
			// Get and check voltages
			auto voltages = digitizer->GetVoltages();
			auto voltageLimits = digitizer->GetVoltageLimits();
			assert(voltages.size() == voltageLimits.size());

			for (unsigned int i = 0; i < voltages.size(); ++i)
			{
				if (i < std::extent<decltype(faultDetails.voltages3230)>::value)
				{
					faultDetails.voltages3230[i].fval = voltages[i];
					faultDetails.voltages3230[i].cond = SSmsMsg::PASS_STAT;
					if (voltages[i] < voltageLimits[i][0] || voltages[i] > voltageLimits[i][1])
					{
						faultDetails.voltages3230[i].cond = SSmsMsg::WARNING_STAT;
					}
				}
			}

			// Get and check radio ocxo
			unsigned char loopStatus = 0;
			unsigned long code = 0;
			unsigned long interval = 0;
			if (radioEquip->ReadOcxo(loopStatus, code, interval))
			{
				faultDetails.ocxoLock.ulval = loopStatus;
				faultDetails.ocxoLock.cond = SSmsMsg::PASS_STAT;
				if (loopStatus == 0)
				{
					faultDetails.ocxoLock.cond = SSmsMsg::WARNING_STAT;
				}
			}
		}

		for (size_t i = 0; i < std::extent<decltype(faultDetails.voltages3230)>::value; ++i)
		{
			if (faultDetails.voltages3230[i].cond == SSmsMsg::WARNING_STAT)
			{
				if (faultStat.cond == SSmsMsg::PASS_STAT)
					faultStat.cond = SSmsMsg::WARNING_STAT;
				break;
			}
		}

		if (faultDetails.ocxoLock.cond == SSmsMsg::WARNING_STAT)
		{
			if (faultStat.cond == SSmsMsg::PASS_STAT)
				faultStat.cond = SSmsMsg::WARNING_STAT;
		}

		// Get and check digitizer clocks
		unsigned long vals[5];
		bool errs[5];
		has3230Timestamp = digitizer->CheckTimestamp(true, false, vals, errs);

		faultStat.env.clock368MHz.ulval = vals[0];
		faultStat.env.clock100MHz.ulval =  vals[1];
		faultStat.env.timeStatus.ulval =  vals[2];

		if (!has3230Timestamp || !digitizer->m_setSeconds)
		{
			if (faultStat.cond == SSmsMsg::PASS_STAT)
				faultStat.cond = SSmsMsg::WARNING_STAT;
		}
		faultStat.env.clock368MHz.cond = (errs[0] ? SSmsMsg::WARNING_STAT : SSmsMsg::PASS_STAT);
		faultStat.env.clock100MHz.cond = (errs[1] ? SSmsMsg::WARNING_STAT : SSmsMsg::PASS_STAT);
		faultStat.env.timeStatus.cond = (errs[2] ? SSmsMsg::WARNING_STAT : SSmsMsg::PASS_STAT);

		if (previousFaultCond != faultStat.cond)
		{
			std::string s1, s2;
			if (previousFaultCond == SSmsMsg::PASS_STAT) s1 = "PASS";
			else if (previousFaultCond == SSmsMsg::WARNING_STAT) s1 = "WARNING";
			else if (previousFaultCond == SSmsMsg::FAIL_STAT) s1 = "FAIL";
			if (faultStat.cond == SSmsMsg::PASS_STAT) s2 = "PASS";
			else if (faultStat.cond == SSmsMsg::WARNING_STAT) s2 = "WARNING";
			else if (faultStat.cond == SSmsMsg::FAIL_STAT) s2 = "FAIL";
			printf("System fault status changed from %s to %s\n", s1.c_str(), s2.c_str());
		}

		if (doSlow)
		{
			// Check the digitizer time only if seconds has been set at least once
			if (digitizer->m_setSeconds)
			{
				Units::Timestamp currentTime;
				if (digitizer->GetCurrentTime(currentTime))
				{
					auto linuxTime = Utility::CurrentTimeAsTimestamp();
					auto timeDiff = (currentTime - linuxTime).NanoSeconds<long>();
					if (abs(timeDiff) > maxDiffNsec)
					{
						TRACE("3230 time = %s, linuxTime = %s, diff = %ld\n", currentTime.Format(true).c_str(), linuxTime.Format(true).c_str(), timeDiff);
					}
				}
				else
				{
					TRACE("3230 time not available\n");
				}
			}
		}

		// Read the mast down and 7031 whip fault status
		bool mastDown;
		bool whipFault;
		digitizer->GetAntennaStatus(mastDown, whipFault);
		navigation->SetMastDownStatus(mastDown);

		// Save the new fault status
		SetFaultDetails(faultStat);

	}
	catch(CWeakSingleton<CNavigation>::NoInstance&)
	{
		has3230Timestamp = false;
		return false;
	}
	catch(CWeakSingleton<CDigitizer>::NoInstance&)
	{
		has3230Timestamp = false;
		return false;
	}
	catch(CWeakSingleton<CRadioEquip>::NoInstance&)
	{
		has3230Timestamp = false;
		return false;
	}
	return true;
}

