/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "3230Fdd.h"
#include "51432017.h"
#include "72362001.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
Units::Frequency C72362001::m_crossOverFreq = 0;
const C72362001::SCalSettings C72362001::CAL_SETTINGS = { 12345678.f, -30.f, -30.f };

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C72362001::C72362001(void) :
	m_isPresent(false)
{
	auto mutableConfig = CSingleton<const CConfig>()->GetMutableConfig();
	auto antenna = mutableConfig.hfConfig.antCable.antenna;
	m_isPresent = (antenna && CAntenna::IsDfAntenna(antenna.get()));
	if (m_isPresent)
	{
		m_crossOverFreq = CAntenna::GetHighBandCrossoverFreq(antenna.get());
	}

	SetSwitch(0, RECEIVE, TERM, TERM);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C72362001::~C72362001(void)
{
	SetSwitch(0, RECEIVE, TERM, TERM);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the cal settings
//
float C72362001::GetCalDbm(size_t ch)
{
	return (ch == 0 ? CAL_SETTINGS.refDbm : CAL_SETTINGS.sampleDbm);
}


//////////////////////////////////////////////////////////////////////
//
// Get the cal settings
//
Units::Frequency C72362001::GetCalFreq(void)
{
	return CAL_SETTINGS.freq;
}


//////////////////////////////////////////////////////////////////////
//
// Get the control word
//
unsigned long C72362001::GetControlWord(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf)
{
	// NOTE: This version of the 7236 only has one cal tone.

	// Set address bits and default state (mode = RECEIVE, ref = REF, sample = TERM)
	unsigned long controlWord = C3230Fdd::CSequencerAntennaControlHF::HF_ANT_SELECT;	// select HF antenna

	switch(mode)
	{
	case RECEIVE:
		switch(sampleRf)
		{
		case ANT_1:
		case ANT_2:
		case ANT_3:
		case ANT_4:
		case ANT_5:
		case ANT_6:
		case ANT_7:
		case ANT_8:
		case ANT_9:
		case ANT_REF:
			ASSERT(refRf != BITE && refRf != NOISE);
			controlWord |= ((static_cast<unsigned char>(sampleRf) << C3230Fdd::CSequencerAntennaControlHF::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControlHF::CHAN_SELECT);
			break;

		case TERM:
			controlWord |= ((static_cast<unsigned char>(0x0b) << C3230Fdd::CSequencerAntennaControlHF::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControlHF::CHAN_SELECT);
			break;

		case NOISE:
			ASSERT(refRf != BITE);
			controlWord |= ((static_cast<unsigned char>(0x0f) << C3230Fdd::CSequencerAntennaControlHF::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControlHF::CHAN_SELECT) |
				C3230Fdd::CSequencerAntennaControlHF::NOISE_GEN | C3230Fdd::CSequencerAntennaControlHF::NOISE_SW;
			break;

		case BITE:
			ASSERT(refRf != NOISE);
			controlWord |= ((static_cast<unsigned char>(0x0f) << C3230Fdd::CSequencerAntennaControlHF::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControlHF::CHAN_SELECT) |
				C3230Fdd::CSequencerAntennaControlHF::CAL_EN;
			break;

		default:
			ASSERT(FALSE);
			controlWord |= ((static_cast<unsigned char>(0x0f) << C3230Fdd::CSequencerAntennaControlHF::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControlHF::CHAN_SELECT) |
				C3230Fdd::CSequencerAntennaControlHF::REF_TERM;
			break;
		}

		switch(refRf)
		{
		case TERM:
			controlWord |= C3230Fdd::CSequencerAntennaControlHF::REF_TERM;
			break;

		case MONITOR:
			if (freq < m_crossOverFreq)
			{
				controlWord |= C3230Fdd::CSequencerAntennaControlHF::LB_BYPASS;
			}
			else
			{
				controlWord |= (C3230Fdd::CSequencerAntennaControlHF::REF_TERM | C3230Fdd::CSequencerAntennaControlHF::REF_SOURCE);
			}

			break;

		case REF:
			break;

		case BITE:
			controlWord |= (C3230Fdd::CSequencerAntennaControlHF::CAL_TO_REF | C3230Fdd::CSequencerAntennaControlHF::REF_TERM |
				C3230Fdd::CSequencerAntennaControlHF::CAL_EN);
			break;

		case NOISE:
			controlWord |= (C3230Fdd::CSequencerAntennaControlHF::CAL_TO_REF | C3230Fdd::CSequencerAntennaControlHF::REF_TERM |
				C3230Fdd::CSequencerAntennaControlHF::NOISE_GEN | C3230Fdd::CSequencerAntennaControlHF::NOISE_SW);
			break;

		default:
			ASSERT(FALSE);
			controlWord |= (C3230Fdd::CSequencerAntennaControlHF::REF_TERM);
			break;
		}

		break;

	case TX_BITE:
		{
			controlWord |= (C3230Fdd::CSequencerAntennaControlHF::CAL_TO_ANT |C3230Fdd::CSequencerAntennaControlHF::CAL_EN);

			switch(sampleRf)
			{
			case TERM:
				controlWord |= ((static_cast<unsigned char>(0x0b) << C3230Fdd::CSequencerAntennaControlHF::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControlHF::CHAN_SELECT);
				break;

			case ANT_1:
			case ANT_2:
			case ANT_3:
			case ANT_4:
			case ANT_5:
			case ANT_6:
			case ANT_7:
			case ANT_8:
			case ANT_9:
			case ANT_REF:
				controlWord |= ((static_cast<unsigned char>(sampleRf) << C3230Fdd::CSequencerAntennaControlHF::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControlHF::CHAN_SELECT);
				break;

			case NOISE:
				controlWord |= ((static_cast<unsigned char>(0x0f) << C3230Fdd::CSequencerAntennaControlHF::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControlHF::CHAN_SELECT) |
						C3230Fdd::CSequencerAntennaControlHF::NOISE_GEN | C3230Fdd::CSequencerAntennaControlHF::NOISE_SW;
				break;

			default:
				ASSERT(FALSE);
				controlWord |= ((static_cast<unsigned char>(0x0b) << C3230Fdd::CSequencerAntennaControlHF::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControlHF::CHAN_SELECT);
				break;
			}

			switch(refRf)
			{
			case MONITOR:
				controlWord |= C3230Fdd::CSequencerAntennaControlHF::REF_SOURCE;
				break;

			case BITE:
				// Get this by leakage through the switches
				break;

			case NOISE:
				controlWord |= C3230Fdd::CSequencerAntennaControlHF::CAL_TO_REF | C3230Fdd::CSequencerAntennaControlHF::NOISE_GEN |
					C3230Fdd::CSequencerAntennaControlHF::NOISE_SW;
				break;

			default:
				ASSERT(FALSE);
				break;
			}
		}

		break;

	default:
		ASSERT(FALSE);
		controlWord |=  C3230Fdd::CSequencerAntennaControlHF::REF_TERM;
		break;
	}

	return controlWord;
}


//////////////////////////////////////////////////////////////////////
//
// Get the control word (modified current word with pattern[0]
//
unsigned long C72362001::GetControlWord(const std::vector<unsigned long>& pattern)
{
	unsigned long controlWord;
	GetCurrentControlWord(controlWord);
	if (!pattern.empty())
	{
		controlWord &= ~C3230Fdd::CSequencerAntennaControlHF::CHAN_SELECT;	// Clear antenna bits
		controlWord |= ((pattern[0] << C3230Fdd::CSequencerAntennaControlHF::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControlHF::CHAN_SELECT);
	}
	return controlWord;
}


//////////////////////////////////////////////////////////////////////
//
// Get the settle time for a switch change
//
inline unsigned int C72362001::CalcSettleTime(const unsigned long currCtrlWord, const unsigned long newCtrlWord)
{
	if (newCtrlWord == currCtrlWord)
	{
		return 0;
	}
	else if ((newCtrlWord & C3230Fdd::CSequencerAntennaControlHF::CAL_EN) != 0 &&
			((newCtrlWord ^ currCtrlWord) & C3230Fdd::CSequencerAntennaControlHF::CAL_EN) != 0)
	{
		// Turning on cal
		return 50000;
	}
	else if ((newCtrlWord & C3230Fdd::CSequencerAntennaControlHF::NOISE_GEN) != 0 &&
			((newCtrlWord ^ currCtrlWord) & C3230Fdd::CSequencerAntennaControlHF::NOISE_GEN) != 0)
	{
		// Turning on noise
		return 5000;
	}
	else
	{
		return 5;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Set the switch
//
void C72362001::SetSwitch(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf)
{
	if (!m_isPresent)
	{
		return;
	}
	unsigned long controlWord = GetControlWord(freq, mode, refRf, sampleRf);
	unsigned long currCtrlWord;
	{
		std::lock_guard<std::mutex> lock(m_critSect);
		if (controlWord == m_controlWord)
			return;

		try
		{
			dynamic_cast<C51432017*>(this)->SetDataOut(controlWord);
		}
		catch(std::bad_cast&)
		{
			ASSERT(false);
		}
		currCtrlWord = m_controlWord;		// Save previous control word
		m_controlWord = controlWord;		// Set new control word
	}

	WaitForSettle(CalcSettleTime(currCtrlWord, controlWord));

	return;
}

void C72362001::SetSwitch(const SVCPMsg::USwitch::SHf& req)
{
	if (!m_isPresent)
	{
		return;
	}
	// Build control word
	unsigned long controlWord = C3230Fdd::CSequencerAntennaControlHF::HF_ANT_SELECT;
	if (req.calToAnt) controlWord |= C3230Fdd::CSequencerAntennaControlHF::CAL_TO_ANT;
	if (req.calToRef) controlWord |= C3230Fdd::CSequencerAntennaControlHF::CAL_TO_REF;
	if (req.calEnable) controlWord |= C3230Fdd::CSequencerAntennaControlHF::CAL_EN;
	if (req.noiseGen) controlWord |= C3230Fdd::CSequencerAntennaControlHF::NOISE_GEN;
	if (req.noiseSwitch) controlWord |= C3230Fdd::CSequencerAntennaControlHF::NOISE_SW;
	if (req.refSource) controlWord |= C3230Fdd::CSequencerAntennaControlHF::REF_SOURCE;
	if (req.lbBypass) controlWord |= C3230Fdd::CSequencerAntennaControlHF::LB_BYPASS;
	if (req.refTerm) controlWord |= C3230Fdd::CSequencerAntennaControlHF::REF_TERM;
	controlWord |= ((req.chanSelect << C3230Fdd::CSequencerAntennaControlHF::ANT_SHIFT) & C3230Fdd::CSequencerAntennaControlHF::CHAN_SELECT);

	unsigned long currCtrlWord;
	{
		std::lock_guard<std::mutex> lock(m_critSect);

		try
		{
			dynamic_cast<C51432017*>(this)->SetDataOut(controlWord);
		}
		catch(std::bad_cast&)
		{
			ASSERT(false);
		}
		currCtrlWord = m_controlWord;	// Save previous control word
		m_controlWord = controlWord;	// Set new control word
	}
	WaitForSettle(CalcSettleTime(currCtrlWord, controlWord));

	return;

}


//////////////////////////////////////////////////////////////////////
//
// Wait for switch to settle
//
void C72362001::WaitForSettle(unsigned long usec)
{
	if (usec == 0) return;

	timespec ts, tr;
	ts.tv_sec = 0;
	ts.tv_nsec = 1000 * usec;	// approx 1 msec default
	nanosleep(&ts, &tr);
	return;
}



