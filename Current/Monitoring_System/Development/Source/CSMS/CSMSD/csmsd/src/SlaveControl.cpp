/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/
#include "stdafx.h"

#include "DfCtrlNet.h"
#include "Failure.h"
#include "HwControl.h"
#include "Fft.h"
#include "Log.h"
#include "ProcessorNode.h"
#include "SlaveControl.h"
#include "Task.h"
#include "VCPCtrlNet.h"

// This class is used in the slave processors to handle the messages received by
// the DFCtrlNet server and dynamic connection instances
//


//////////////////////////////////////////////////////////////////////
//
// Static data
//
constexpr std::chrono::seconds CSlaveControl::MONITOR_POLLING_INTERVAL;
#ifdef CSMS_DEBUG
size_t s_flushCnt = 0;
#endif

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CSlaveControl::CSlaveControl(void) :
	CBist(),
	CAudio(),
	m_config(),
	m_monitor(0),
	m_navigation(),
	m_digitizer(),
	m_processorNode(),
	m_radioEquip(),
	m_shutdown(false),
	m_queue(),
	m_taskMap(),
	m_watchdog(),
	m_CanEnableAudio(true)
{
	if (m_processorNode->IsProcessorMaster())
	{
		// This must be a slave node
		CLog::Log(CLog::ERRORS, "CSlaveControl class in a master node!");
		throw std::runtime_error("CSlaveControl class in a master node!");
	}

	if (!m_digitizer->IsMaster())
	{
		throw std::runtime_error("This digitizer is not master");
	}
	m_busyCounter = 0;
#ifdef CSMS_DEBUG
	//CHwControl::EnablePanTrace();
#endif
	// Check license machine id
	CCsmsLicense::SMachineId machineId;
	m_digitizer->GetDeviceDna(machineId.dna);
	m_radioEquip->GetMax10UniqueId(machineId.max10Id);
	m_license->CheckMachineId(machineId);

	// Update config data based on license, etc.
	m_radioEquip->UpdateConfig();
	m_config->UpdateAntennaParams();

	// Check all hardware is ready
	m_digitizer->ResetFrequency();		// Not really necessary

	// Wait for 10 seconds
	for (size_t i = 0; i < 10; ++i)
	{
		auto clocks = m_digitizer->GetClockFrequencies();
		TRACE("368640000 clock[%u] = %lu\n", i, clocks[0]);
		sleep(1);
		if (Utility::IsQuitting())
		{
			CLog::Log(CLog::INFORMATION, "SlaveControl received QUIT signal");
			Stop();
			return;
		}
	}

	// Wait for timestamp to be ready
	m_digitizer->ResetFrequency();
	for (size_t i = 0; i < 120; ++i)
	{
		if (Reset3230Time(false, true))		// No error logging.
			break;

		if ((i % 10) == 0)
			CLog::Log(CLog::INFORMATION, "3230 time not available: %u", i);

		sleep(1);
		if (Utility::IsQuitting())
		{
			CLog::Log(CLog::INFORMATION, "SlaveControl received QUIT signal");
			Stop();
			return;
		}
	}
	unsigned long vals[5];
	bool errs[5];
	if (m_digitizer->CheckTimestamp(true, true, vals, errs) && m_digitizer->m_setSeconds)
		CLog::Log(CLog::INFORMATION, "3230 time is available");
	else
		CLog::Log(CLog::WARNINGS, "3230 time is not available");

	// Monitor thread will set the time when the digital board is ready

	// Setup hardware
	m_digitizer->SetThresholdCounters(CHwControl::THRESHOLDS_3230);
	m_digitizer->EnableWatchdog(true);

	// Start the threads
	m_monitorThread = std::thread(&CSlaveControl::MonitorThread, this);
	m_thread = std::thread(&CSlaveControl::Thread, this);
	CAudio::Startup();
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CSlaveControl::~CSlaveControl(void)
{
	Stop();

	std::lock_guard<std::mutex> lock(m_taskMapMtx);
	printf("~CSlaveControl: TaskMap still has %u entries\n", m_taskMap.size());
	m_taskMap.clear();
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Process CCDF_SETUP_COLLECT request
//
void CSlaveControl::DoCcdfCollect(const SDfCtrlMsg* msg, CDfCtrlNet* source)
{
	//1. Master sends RX_TUNE and wait for response.
	//2. Master sets the antenna switch - settle.
	//3. Master Sends CCDF_SETUP_COLLECT which sets attenuator and collects CCDF, calculates power and expectedDbm.

	auto& req = msg->body.ccdfCollect;

	// First, Set the radio attenuator and gain mode

	C2630::EGainMode gainMode;
	if (req.gainMode == SDfCtrlMsg::NORMAL) gainMode = C2630::EGainMode::NORMAL;
	else if (req.gainMode == SDfCtrlMsg::RURAL) gainMode = C2630::EGainMode::RURAL;
	else if (req.gainMode == SDfCtrlMsg::URBAN) gainMode = C2630::EGainMode::URBAN;
	else if (req.gainMode == SDfCtrlMsg::CONGESTED) gainMode = C2630::EGainMode::CONGESTED;
	else gainMode = C2630::EGainMode::NORMAL;

	unsigned char setAtten;
	C2630::EGainMode setGainMode;
	bool setLna;
	bool ok = m_radioEquip->SetAttenuation(gainMode, req.atten, setAtten, setGainMode, setLna);

	C2630::S2630State radioState;
	m_radioEquip->GetState(radioState);

	size_t rfGainTableIdx;
	long ifGainTableIdx;
	float gainAdjTemp;
	auto rxGain = m_radioEquip->GetRxGainRatio(rfGainTableIdx, ifGainTableIdx, gainAdjTemp);
	auto gainDb = 10.f * log10(rxGain * m_digitizer->GetGainRatio(radioState.direct));

	// Collect the counts / measure power
	float power = -std::numeric_limits<float>::infinity();
	if (req.powerType == SDfCtrlMsg::SCcdfCollect::EPowerType::TONEDBM)
	{
		m_digitizer->MeasureToneDbm(radioState.direct, power);
	}
	else if (req.powerType == SDfCtrlMsg::SCcdfCollect::EPowerType::NOISEDBM)
	{
		m_digitizer->MeasureNoiseDbm(radioState.direct, power);
	}
	else
	{
		ok = false;
	}
	// Send the response

	SDfCtrlMsg resp;
	resp.hdr = msg->hdr;
	resp.hdr.msgSubType = SDfCtrlMsg::CCDF_COLLECT_RESP;
	resp.hdr.bodySize = sizeof(SDfCtrlMsg::SCcdfCollectResp);

	auto& r = resp.body.ccdfCollectResp;
	r.status = (ok ? SDfCtrlMsg::SUCCESS : SDfCtrlMsg::FAILURE);
	r.powerDbm = power;
	r.gainDb = gainDb;		// Add to rxInputDbm to get expectedDbm

	m_processorNode->Send(source, resp/*, "CCDF_COLLECT_RESP"*/);	// Suppress log

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Process DO_INITIAL_AGC request
//
void CSlaveControl::DoInitialAgc(const SDfCtrlMsg* msg, CDfCtrlNet* source)
{
	auto& req = msg->body.doInitialAgc;

	C2630::EGainMode gainMode;
	if (req.gainMode == SDfCtrlMsg::NORMAL) gainMode = C2630::EGainMode::NORMAL;
	else if (req.gainMode == SDfCtrlMsg::RURAL) gainMode = C2630::EGainMode::RURAL;
	else if (req.gainMode == SDfCtrlMsg::URBAN) gainMode = C2630::EGainMode::URBAN;
	else if (req.gainMode == SDfCtrlMsg::CONGESTED) gainMode = C2630::EGainMode::CONGESTED;
	else gainMode = C2630::EGainMode::NORMAL;

	C2630::S2630State radioState;
	m_radioEquip->GetState(radioState);

	// Create response message
	SDfCtrlMsg resp;
	resp.hdr = msg->hdr;
	resp.hdr.msgSubType = SDfCtrlMsg::DO_INITIAL_AGC_RESP;
	resp.hdr.bodySize = sizeof(SDfCtrlMsg::SDoInitialAgcResp);

	auto& r = resp.body.doInitialAgcResp;
	TRACE("DoInitialAgc: %u %d\n", r.initAtten, r.attenAdj);
	r.status = (m_cAgc->InitialAgc(gainMode, radioState.direct, r.initAtten, r.attenAdj) ? SDfCtrlMsg::SUCCESS : SDfCtrlMsg::FAILURE);
#ifdef CSMS_DEBUG
	timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	TRACE("CSlaveControl::DoInitialAgc(DO_INITIAL_AGC_RESP): sec = %ld nsec =%ld; status = %d\n",
						ts.tv_sec, ts.tv_nsec, int(r.status));
#endif
	// Send the response
	m_processorNode->Send(source, resp/*, "DO_INITIAL_AGC_RESP"*/);	// Suppress log

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Process SLAVE_GET_SAMPLES request
//
#if NO_SLAVE_CLIENT == 1
void CSlaveControl::DoSlaveGetSamples(const SDfCtrlMsg* msg, CDfCtrlNet* source) // debug only
#else
void CSlaveControl::DoSlaveGetSamples(const SDfCtrlMsg* msg, CDfCtrlNet* /*source*/)
#endif
{
	auto& req = msg->body.slaveGetSamples;
	Task task;
	{
		//CLog::Log(CLog::INFORMATION, _T("slave DoSlaveGetSamples")); //debug only
		std::lock_guard<std::mutex> lock(m_taskMapMtx);
		for (auto it = m_taskMap.begin(); it != m_taskMap.end(); )
		{
			if (it->first == req.key)
			{
				task = std::move(it->second);
				m_taskMap.erase(it);
#ifdef CSMS_DEBUG
				timespec ts;
				clock_gettime(CLOCK_MONOTONIC, &ts);
				TRACE("DoSlaveGetSamples: got task %lu from taskMap and erased it, sec = %ld nsec =%ld\n",
						req.key, ts.tv_sec, ts.tv_nsec);
#endif
				break;
			}
#ifdef CSMS_DEBUG
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			TRACE("DoSlaveGetSamples erasing entry with key = %lu, looking for %lu, sec = %ld nsec =%ld\n",
					it->first, req.key, ts.tv_sec, ts.tv_nsec);
#endif
			auto& b = it->second->msgBody;
			size_t numAnts = (b.numAnts != 0 ? b.numAnts : 1);
			auto antCount = SDfCtrlMsg::GetDataSize(b.dataTypes, b.count, b.count);
			m_digitizer->m_sdma.FlushSamples(it->second->sampleCount, antCount * numAnts);	// Flush the samples for this task
			it = m_taskMap.erase(it);
		}
	}
	if (task)
	{
		if (req.action == SDfCtrlMsg::SSlaveGetSamples::COLLECT)
		{
#if NO_SLAVE_CLIENT == 1
			// Replace the header
			task->msgHdr = msg->hdr;
			ProcessSlaveGetSamples(*task, source); // returns to master not as client
#else
			// Replace the header
			task->msgHdr = msg->hdr;
			TRACE("Adding task %lu to taskQueue\n", req.key);
			m_queue.AddItem(std::move(task), 0, INFINITE);
#endif
		}
		else
		{
			auto& b = task->msgBody;
			size_t numAnts = (b.numAnts != 0 ? b.numAnts : 1);
			auto antCount = SDfCtrlMsg::GetDataSize(b.dataTypes, b.count, b.count);
#ifdef CSMS_DEBUG
			++s_flushCnt;
			TRACE("task %lu flushed cnt = %u\n", req.key, s_flushCnt);
#endif
			// Flush the samples for this task
			auto flushOk = m_digitizer->m_sdma.FlushSamples(task->sampleCount, antCount * numAnts);
			if(!flushOk)
			{
				uncached_ptr found;
				auto foundData = m_digitizer->m_sdma.FindSamples(task->sampleCount, antCount * numAnts, found);
				if(foundData)
				{
					m_digitizer->m_sdma.DoneWithSamples(task->sampleCount);
				}

			}
		}
	}
	else
	{
		// Couldn't find task
//		printf("DoSlaveGetSamples cannot find entry with key = %lu\n", req.key);
		SDfCtrlMsg errorResp;
		auto dfResp = &errorResp;
		dfResp->hdr = msg->hdr;
		dfResp->hdr.msgSubType = SDfCtrlMsg::SLAVE_GET_SAMPLES_RESP;
		dfResp->hdr.bodySize = offsetof(SDfCtrlMsg::SSlaveGetSamplesResp, data);
		auto& samplesResp = dfResp->body.slaveGetSamplesResp;
		samplesResp.status = SDfCtrlMsg::FAILURE;
		samplesResp.totalAnts = 0;
		samplesResp.numIq = 0;
		samplesResp.numPoints = 0;
		samplesResp.antNum = 0;
		m_processorNode->SendToMaster(*dfResp, "SLAVE_GET_SAMPLES_RESP");
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Process SLAVE_TUNE request
//
void CSlaveControl::DoSlaveTune(const SDfCtrlMsg* msg, CDfCtrlNet* source)
{
	// Set the radio
	Units::Frequency finalIfFreq;
	bool inverted = false;
	bool direct = false;
	Units::FreqPair freqLimits;
	Units::Frequency radioFreq;
	size_t band = 0;
	Units::Frequency procFreq;
	bool invertRadio = false;

	auto& t = msg->body.slaveTune;
	SDfCtrlMsg::EStatus status = SDfCtrlMsg::SUCCESS;
//	printf("tune: @ %lld rf:%f band:%d\n",
//		std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count(),
//		Units::Frequency(t.rfFreq).Hz<double>(), t.bandSelect);

	if (!m_radioEquip->Tune(Units::Frequency(t.rfFreq), Units::Frequency(t.rxBw), Units::Frequency(t.procBw), t.hf,
		CRadioEquip::EBandSelect(t.bandSelect), finalIfFreq, inverted, freqLimits, radioFreq, band, direct, t.ant))
	{
		TRACE("CSlaveControl::DoSlaveTune RadioEquip Tune failed\n");
		status = SDfCtrlMsg::FAILURE;
	}
	else
	{
		invertRadio = inverted;

//		printf("tune: @ %lld rf:%f band:%d f1-f2:%f-%f fin:%f radio:%f\n",
//			std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count(),
//			Units::Frequency(t.rfFreq).Hz<double>(), t.bandSelect, freqLimits.first.Hz<double>(), freqLimits.second.Hz<double>(),
//			finalIfFreq.Hz<double>(), radioFreq.Hz<double>());
		if (t.tuneDigitizer)
		{
			auto& d = t.decimations;
			m_digitizer->SetDecimation(
				C3230::SDecimation(d.zifCICDecimation, d.zifFIRDecimation, d.upResample, d.downResample, d.ddcCICDecimation, d.ddcFIRDecimation), 0);

			inverted = m_digitizer->AdjustInversion(direct, inverted);
			procFreq = (inverted ? finalIfFreq - Units::Frequency(t.procOffset) : finalIfFreq + Units::Frequency(t.procOffset));

			m_digitizer->Tune(procFreq, inverted, direct, false);
//			printf("proc:%f\n", procFreq.Hz<double>());
		}
	}

	SDfCtrlMsg resp;
	resp.hdr = msg->hdr;
	resp.hdr.msgSubType = SDfCtrlMsg::SLAVE_TUNE_RESP;
	resp.hdr.bodySize = sizeof(SDfCtrlMsg::SSlaveTuneResp);

	auto& r = resp.body.slaveTuneResp;
	r.status = status;
	r.finalIfFreq = finalIfFreq.GetRaw();
	r.inverted = inverted;
	r.direct = direct;
	r.freqLimits[0] = freqLimits.first.GetRaw();
	r.freqLimits[1] = freqLimits.second.GetRaw();
	r.radioFreq = radioFreq.GetRaw();
	r.band = band;
	r.procFreq = procFreq.GetRaw();

	CAudio::Enable(m_CanEnableAudio);
	//printf("slave tune audio enable = %d", m_CanEnableAudio);
	dynamic_cast<CAudio&>(*this).TuneDemods(t.rfFreq, finalIfFreq, invertRadio, inverted, freqLimits);

	m_processorNode->Send(source, resp); // , "SLAVE_TUNE_RESP");
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Process SLAVE_SETUP_COLLECT request
//
void CSlaveControl::DoSlaveSetupCollect(const SDfCtrlMsg* msg, CDfCtrlNet* source)
{
	auto& req = msg->body.slaveSetupCollect;

	if (req.count == 0)
	{
		throw ErrorCodes::INVALIDSAMPLESIZE;
	}

	// First, Set the radio attenuator and gain mode
//	printf("coll: @ %lld at:%u dl:%lu fft:%lu g:%d cnt:%lu adc:%lu\n",
//		std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count(),
//		req.atten, req.delay, req.fftDelay, req.gainMode, req.count, req.adcCount);

	C2630::EGainMode gainMode;
	if (req.gainMode == SDfCtrlMsg::NORMAL) gainMode = C2630::EGainMode::NORMAL;
	else if (req.gainMode == SDfCtrlMsg::RURAL) gainMode = C2630::EGainMode::RURAL;
	else if (req.gainMode == SDfCtrlMsg::URBAN) gainMode = C2630::EGainMode::URBAN;
	else if (req.gainMode == SDfCtrlMsg::CONGESTED) gainMode = C2630::EGainMode::CONGESTED;
	else gainMode = C2630::EGainMode::NORMAL;

	unsigned char setAtten;
	C2630::EGainMode setGainMode;
	bool setLna;
	bool ok = m_radioEquip->SetAttenuation(gainMode, req.atten, setAtten, setGainMode, setLna);

	// Then set up the digitizer

	C2630::S2630State radioState;
	m_radioEquip->GetState(radioState);
	unsigned long count = req.count;

	//  CHwControl
	unsigned long sampleCount;
	size_t numAnts = (req.numAnts != 0 ? req.numAnts : 1);

	auto antCount = SDfCtrlMsg::GetDataSize(req.dataTypes, req.count, req.count);
	if (!m_digitizer->m_sdma.WaitForBufferSpace(antCount * numAnts, sampleCount) ||
		!m_digitizer->m_sdma.MarkSamplesInUse(sampleCount, antCount * numAnts))
	{
		throw ErrorCodes::HARDWAREDOWN;		// Perhaps there's a better one
	}

	// save the current radio antenna setting
	unsigned char currentAntenna = radioState.ant;

	std::vector<unsigned long> pattern(numAnts);
	if (req.termRadio)
	{
		auto currentAnt = m_digitizer->GetCurrentAntenna();
		// Note that numAnts should be 1 for this case.
		for (size_t i = 0; i < numAnts; ++i)
		{
			pattern[i] = currentAnt;
		}

		// Terminate the radio
		unsigned char setAntenna;
		m_radioEquip->SetAntenna(S2630Msg::TERMINATE, setAntenna);
	}
	else
	{
		for (size_t i = 0; i < numAnts; ++i)
		{
			pattern[i] = req.pattern[i];
		}
	}

	m_CanEnableAudio = req.enableAudio;
	CAudio::Enable(m_CanEnableAudio);
	//printf("DoSlaveSetupCollect audio enable = %d", m_CanEnableAudio);
	m_digitizer->SetLed(true);
	bool withIq = (req.dataTypes & SDfCtrlMsg::SlaveDataTypes::IQ) != 0; // false;
	bool withPsd = (req.dataTypes & SDfCtrlMsg::SlaveDataTypes::PSD) != 0; // true;
	bool withFft = (req.dataTypes & SDfCtrlMsg::SlaveDataTypes::FFT) != 0; // true;
	C3230::EDfMode dfMode = (req.numAnts == 1 ? C3230::EDfMode::NODF : C3230::EDfMode::DFSLAVE);


	m_digitizer->StartSampling(sampleCount, count, req.adcCount, req.delay, req.fftDelay, withIq, withPsd, withFft,
		0, Units::Timestamp(), dfMode, pattern);

	// Digitizer is armed and waiting for pulse from master, send ready msg

	SDfCtrlMsg ready;
	ready.hdr = msg->hdr;
	ready.hdr.msgSubType = SDfCtrlMsg::SLAVE_SETUP_COLLECT_READY;
	ready.hdr.bodySize = sizeof(SDfCtrlMsg::SSlaveSetupCollectReady);
	ready.body.slaveSetupCollectReady.status = (ok ? SDfCtrlMsg::SUCCESS : SDfCtrlMsg::FAILURE);
	m_processorNode->Send(source, ready/*, "SLAVE_SETUP_COLLECT_READY"*/);	// Suppress log

//	printf("start: @ %lld at:%u dl:%lu fft:%lu g:%d cnt:%lu adc:%lu\n",
//		std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count(),
//		req.atten, req.delay, req.fftDelay, req.gainMode, req.count, req.adcCount);

	// Continue now waiting for trigger
	auto temp = count * m_digitizer->GetDecimation() + 0.5;
	auto collectionTimeSec = temp / CDigitizer::ADC_CLOCK_RATE;	// sec
	collectionTimeSec *= (static_cast<double>(count + CDigitizer::DDC_EXTRA_SAMPLES_REQUIRED) / count);	// Adjust for extra samples (only really needed when ddc is used)
	collectionTimeSec *= numAnts;
	collectionTimeSec += 5.;	// Add 5 seconds to more than cover the block start delay from now

	SDfCtrlMsg::EStatus status = SDfCtrlMsg::SUCCESS;
	if (!m_digitizer->WaitForSequencer(withIq, withPsd, withFft, true, collectionTimeSec, Units::Timestamp()))
	{
		status = SDfCtrlMsg::FAILURE;
	}
//	printf("done: @ %lld at:%u dl:%lu fft:%lu g:%d cnt:%lu adc:%lu\n",
//		std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count(),
//		req.atten, req.delay, req.fftDelay, req.gainMode, req.count, req.adcCount);
	m_digitizer->SetLed(false);

	// Read the AGC counters to empty the FIFO and send to master
	unsigned int counts[4];
	unsigned short thresh[4];
	for (size_t i = 0; i < numAnts; ++i)
	{
		m_digitizer->GetThresholdCounters(counts, thresh);
	}

	size_t rfGainTableIdx;
	long ifGainTableIdx;
	float gainAdjTemp;
	float rxGain = m_radioEquip->GetRxGainRatio(rfGainTableIdx, ifGainTableIdx, gainAdjTemp);
	float gainAdj = m_digitizer->GetAdcFullScaleWatts() / (rxGain * m_digitizer->GetGainRatio(radioState.direct));

	if (req.termRadio)
	{
		// Restore the original antenna
		unsigned char setAntenna;
		m_radioEquip->SetAntenna(currentAntenna, setAntenna);
	}

	// Create a task to pass the data to a separate Processing thread and store in task map
	SKeyTask keyTask;
	keyTask.first = msg->hdr.sourceAddr;
	keyTask.second.reset(new SDfCollectTask);
	keyTask.second->source = source;
	keyTask.second->status = status;
	keyTask.second->msgHdr = msg->hdr;
	keyTask.second->msgBody = msg->body.slaveSetupCollect;
	keyTask.second->radioState = radioState;
	keyTask.second->sampleCount = sampleCount;
	keyTask.second->rfGainTableIdx = rfGainTableIdx;
	keyTask.second->ifGainTableIdx = ifGainTableIdx;
	keyTask.second->gainAdj = gainAdj;
	keyTask.second->gainAdjTemp = gainAdjTemp;
	keyTask.second->rxGain = rxGain;

	// Add task to map with unique key = tag. Send response with ccdf data to master
	{
		std::lock_guard<std::mutex> lock(m_taskMapMtx);
		m_taskMap.push_back(std::move(keyTask));
//		printf("Added task %lu to taskMap\n", keyTask.first);
	}
	// task has been moved to taskMap and is now empty unique_ptr

	// Send response to master with key to taskMap and ccdf info for agc
	SDfCtrlMsg resp;
	resp.hdr = msg->hdr;
	resp.hdr.msgSubType = SDfCtrlMsg::SLAVE_SETUP_COLLECT_RESP;
	resp.hdr.bodySize = sizeof(SDfCtrlMsg::SSlaveSetupCollectResp);
	auto& r = resp.body.slaveSetupCollectResp;
	r.status = status;
	r.key = msg->hdr.sourceAddr;
	for (size_t i = 0; i < SDfCtrlMsg::SSlaveSetupCollectResp::NUM_CCDF; ++i)
	{
		r.counts[i] = counts[i];
		r.thresh[i] = thresh[i];
	}
	m_processorNode->Send(source, resp /*, "SLAVE_SETUP_COLLECT_RESP"*/);	// Suppress log

	// Don't queue for processing until requested by master
	// Master will either request processing by tag or request flush of samples.
	// In either case, data will be deleted from map, or if processing/flush request has higher tag. Also, at dtor.

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Process GET_CSMS_FAULT request
//
void CSlaveControl::DoGetCsmsFault(const SDfCtrlMsg* msg, CDfCtrlNet* source)
{
	SDfCtrlMsg resp;
	resp.hdr = msg->hdr;
	resp.hdr.msgSubType = SDfCtrlMsg::GET_CSMS_FAULT_RESP;
	resp.hdr.bodySize = sizeof(SDfCtrlMsg::SGetSlaveCsmsFaultResp);

	m_monitor.GetFaultDetails(resp.body.getSlaveCsmsFaultResp);
	m_processorNode->Send(source, resp /*, "GET_CSMS_FAULT_RESP"*/);	// too verbose
	return;
}

//////////////////////////////////////////////////////////////////////
//
// Process SET_SLAVE_AUDIO_PARAMS request
//
void CSlaveControl::DoSetSlaveAudioParams(const SDfCtrlMsg* msg, CDfCtrlNet* source)
{
	SDfCtrlMsg resp;
	resp.hdr = msg->hdr;
	resp.hdr.msgSubType = SDfCtrlMsg::SET_SLAVE_AUDIO_PARMS_RESP;
	resp.hdr.bodySize = sizeof(SDfCtrlMsg::SSetSlaveAudioParamsResp);
	const CNetConnection<void>* audiosource = msg->body.setSlaveAudioParam.audiosource;
	const SEquipCtrlMsg::SAudioParamsCmd& cmd = msg->body.setSlaveAudioParam.audiocmd;
	C3230::EDemodMode mode;
	CConfig::ConvertDemod(cmd.detMode, mode);
	auto& d = msg->body.setSlaveAudioParam.decimations;
	unsigned char audioChan = 0xff;
	C3230::SDecimation demodDecimation(d.zifCICDecimation, d.zifFIRDecimation, d.upResample, d.downResample, d.ddcCICDecimation, d.ddcFIRDecimation);

	if (cmd.anyChannel)
	{
		printf("audiocmd freq = %f", Units::Frequency(cmd.freq).Hz<double>());
		// Allocate a channel and add to vector, if possible

		if (!CAudio::AllocateAudioChannel(audiosource, Units::Frequency(cmd.freq), demodDecimation, mode,
			Units::Frequency(cmd.bandwidth), Units::Frequency(cmd.bfo), audioChan, cmd.streamID))
		{
			throw ErrorCodes::NOFREEAUDIOCHANNEL;
			// debug only -- free and reallocate for now
			/*if (!CAudio::Free(source, 0))
			{
							throw ErrorCodes::AUDIOCHANNELNOTFOUND;
			}

			if (!CAudio::AllocateAudioChannel(source, Units::Frequency(cmd.freq), demodDecimation, mode,
				Units::Frequency(cmd.bandwidth), Units::Frequency(cmd.bfo), audioChan, cmd.streamID))
			{
				throw ErrorCodes::NOFREEAUDIOCHANNEL;
			}*/

		}
	}
	else
	{
		// Either update an existing channel or fail
		if (!CAudio::SetDemodParams(audiosource, cmd.channel, Units::Frequency(cmd.freq), demodDecimation, mode, Units::Frequency(cmd.bandwidth),
			Units::Frequency(cmd.bfo), cmd.streamID))
		{
			throw ErrorCodes::AUDIOCHANNELNOTFOUND;
		}
		audioChan = cmd.channel;
	}
	CAudio::Enable(m_CanEnableAudio);
	//printf("DoSetSlaveAudioParams audio enable = %d", m_CanEnableAudio);

	resp.body.setSlaveAudioParamResp.audioparamsresp.status = ErrorCodes::SUCCESS;
	resp.body.setSlaveAudioParamResp.audioparamsresp.channel = audioChan;
	m_processorNode->Send(source, resp /*, "SET_SLAVE_AUDIO_PARMS_RESP"*/);	// too verbose
	return;
}

void CSlaveControl::DoFreeSlaveAudioChannel(const SDfCtrlMsg* msg, CDfCtrlNet* source)
{
	SDfCtrlMsg resp;
	resp.hdr = msg->hdr;
	resp.hdr.msgSubType = SDfCtrlMsg::SET_SLAVE_AUDIO_PARMS_RESP;
	resp.hdr.bodySize = sizeof(SDfCtrlMsg::SSetSlaveAudioParamsResp);
	const CNetConnection<void>* audiosource = msg->body.setSlaveAudioParam.audiosource;
	const SEquipCtrlMsg::SAudioParamsCmd& cmd = msg->body.setSlaveAudioParam.audiocmd;
	if(msg->body.setSlaveAudioParam.freechannels)
	{
		if (cmd.anyChannel)
		{
			// Free audio channels associated with this client
			CAudio::Free(audiosource);
		}
		else
		{
			if (!CAudio::Free(audiosource, cmd.channel))
			{
				throw ErrorCodes::AUDIOCHANNELNOTFOUND;
			}
			else
			{
				resp.body.setSlaveAudioParamResp.audioparamsresp.status = ErrorCodes::SUCCESS;
				resp.body.setSlaveAudioParamResp.audioparamsresp.channel = cmd.channel;
				m_processorNode->Send(source, resp /*, "SET_SLAVE_AUDIO_PARMS_RESP"*/);	// too verbose

			}
		}
	}

}


//////////////////////////////////////////////////////////////////////
//
// Process PING
//
void CSlaveControl::DoPing(const SDfCtrlMsg* /*msg*/, CDfCtrlNet* /*source*/)
{
	// Just toggle the LED
	m_digitizer->ToggleLed();
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get gain corrections
//
void CSlaveControl::GetCorrections(const SDfCollectTask& task, int numBins, float& gainAdj, Ne10F32Vec& adjPower) const
{
	auto& req = task.msgBody;

	unsigned long up;
	unsigned long down;
	m_digitizer->m_decimation.GetUpDown(up, down);
	auto binSize = (Units::Frequency(CDigitizer::ADC_CLOCK_RATE) * up) / (req.count * down);

	gainAdj = task.gainAdj;
	if (m_config->HasGainEqualization())
	{
		// Remove radio portion of gain, leaving temp correction in
		gainAdj *= (task.rxGain / task.gainAdjTemp);
		adjPower.resize(numBins);
		m_radioEquip->GetCorrection(Units::Frequency(task.radioState.frequency), Units::Frequency(task.radioState.ifreq * 1000000ULL),
			Units::Frequency(task.radioState.bw * 1000000ULL), task.rfGainTableIdx,
			task.ifGainTableIdx, binSize, task.radioState.atten, task.radioState.gainMode, adjPower);
//		ne10sSqrt(adjPower, rootPower);
	}
	else
	{
		Units::Frequency midFrequency(req.midFrequency);
		if (midFrequency != 0)
		{
			auto adj = m_radioEquip->GetCorrection(midFrequency, Units::Frequency(task.radioState.ifreq * 1000000ULL),
				task.rfGainTableIdx, task.ifGainTableIdx,task. radioState.atten, task.radioState.gainMode);
			float midCorrection = task.rxGain / (task.gainAdjTemp * adj);
			if (fabsf(midCorrection - 1.0f) > 1.e-6)
			{
				gainAdj *= midCorrection;
			}
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Monitor Thread
//
void CSlaveControl::MonitorThread(void)
{
	unsigned int busyCounter = 0;
	unsigned int idleCounter = 0;
	// Setup watchdog
	int monitor = m_watchdog->RegisterPingSource(_T("slave monitor thread"));

	try
	{
		bool had3230Timestamp = Reset3230Time(true, false);
		bool hadNtpTime = m_navigation->IsNtpSyncd();
		if (had3230Timestamp && m_digitizer->m_setSeconds)
		{
			printf("MonitorThread: Resetting time and setting seconds\n");
		}

		const unsigned long slowUpdate = 10;
		unsigned long slowCounter = 0;

		// Poll until shutdown
		auto Polling = [this]
		{
			std::unique_lock<std::mutex> lock(m_shutdownMutex);
			return !m_shutdownCond.wait_for(lock, MONITOR_POLLING_INTERVAL, [this] { return m_shutdown; });
		};

		while(Polling())
		{
			// check if slave loop been idle too long (idle counter reaches 5)
			if (busyCounter == m_busyCounter)
				idleCounter++;

			if (idleCounter >=5)
			{
				idleCounter = 0;
				m_CanEnableAudio = true;
				CAudio::Enable(m_CanEnableAudio); // reenable audio if slave get sample task time out
				//printf("idle timeout audio enable = %d", m_CanEnableAudio);
			}


			// Ping the watchdog
			m_watchdog->Ping(monitor);

			// Monitor everything
			// Note: This has to be the only thread that updates fault details!
			auto bistSummary = GetFinalSummary();	// Get BIST summary

			bool has3230Timestamp;
			if (!m_monitor.Monitor(slowCounter == 0, bistSummary, 1000000000, has3230Timestamp))
			{
				TRACE("Monitor failed\n");
			}
			++slowCounter;
			if (slowCounter == slowUpdate) slowCounter = 0;

			// If timestamp status changes from bad to good, reset and set seconds again.
			bool hasNtpTime = m_navigation->IsNtpSyncd();
			bool changed3230 = !had3230Timestamp && has3230Timestamp;
			bool changedNtp = !hadNtpTime && hasNtpTime;
			if (changed3230 || changedNtp)
			{
				printf("Resetting time and setting seconds %s %s\n", (changed3230 ? "3230" : "    "), (changedNtp ? "ntp" : "   "));
				Reset3230Time(true, false);
			}
			had3230Timestamp = has3230Timestamp;
			hadNtpTime = hasNtpTime;
			if (busyCounter != m_busyCounter)
			{
				idleCounter = 0;
				busyCounter = m_busyCounter;
			}
		}

		printf("CSlaveControl::MonitorThread exiting\n");
	}
	catch(...)
	{
		CFailure::OnFail(std::current_exception());
	}

	m_watchdog->UnregisterPingSource(monitor);

	return;
}

//////////////////////////////////////////////////////////////////////
//
// Handle internal df control messages to slave
//
void CSlaveControl::OnInternalDfCtrl(const SDfCtrlMsg* msg, CDfCtrlNet* source)
{
	auto peer = (source != nullptr ? source->GetPeerAddress() : nullptr);
	auto peername = (peer != nullptr ? peer->ai_canonname : "unknown");
	SDfCtrlMsg resp;

	switch(msg->hdr.msgSubType)
	{
	case SDfCtrlMsg::DO_BIST:
		CLog::Log(CLog::INFORMATION, "rcvd DO_BIST from %s", peername);
		if (msg->hdr.bodySize != sizeof(SDfCtrlMsg::SDoBist))
		{
			throw ErrorCodes::INVALIDVERSION;
		}
		if (CVCPCtrlNet::IsVCPControlClient())
		{
			throw ErrorCodes::CMDINVALIDVCPMODE;
		}
		{
			SEquipCtrlMsg ecMsg;
			// Need dfctrlmsg versions of DO_BIST_RESP here
			ecMsg.hdr.cmdVersion = msg->hdr.cmdVersion;
			ecMsg.hdr.respVersion = msg->hdr.respVersion;
			ecMsg.hdr.bodySize = 0;
			ecMsg.hdr.msgType = SEquipCtrlMsg::BIST_CTRL;
			ecMsg.hdr.msgSubType = SEquipCtrlMsg::GET_DIAGNOSTICS;
			ecMsg.hdr.bodySize = 0;
			CTask::Task task(new CTask(&ecMsg, source));
			CBist::DoBist(task);
		}
		break;

	case SDfCtrlMsg::SLAVE_TUNE:
//		CLog::Log(CLog::INFORMATION, "rcvd SLAVE_TUNE from %s", peername);	// Too many messages when scanning
		if (msg->hdr.bodySize != sizeof(SDfCtrlMsg::SSlaveTune))
		{
			throw ErrorCodes::INVALIDVERSION;
		}
		if (CVCPCtrlNet::IsVCPControlClient())
		{
			throw ErrorCodes::CMDINVALIDVCPMODE;
		}
		DoSlaveTune(msg, source);
		break;

	case SDfCtrlMsg::SLAVE_SETUP_COLLECT:
//		CLog::Log(CLog::INFORMATION, "rcvd SLAVE_SETUP_COLLECT from %s", peername);	// Too many messages
		if (msg->hdr.bodySize != sizeof(SDfCtrlMsg::SSlaveSetupCollect))
		{
			throw ErrorCodes::INVALIDVERSION;
		}
		if (CVCPCtrlNet::IsVCPControlClient())
		{
			throw ErrorCodes::CMDINVALIDVCPMODE;
		}
		DoSlaveSetupCollect(msg, source);
		break;

	case SDfCtrlMsg::SLAVE_GET_SAMPLES:
		m_busyCounter++;

//		CLog::Log(CLog::INFORMATION, "rcvd SLAVE_GET_SAMPLES from %s", peername);	// Too many messages
		if (msg->hdr.bodySize != sizeof(SDfCtrlMsg::SSlaveGetSamples))
		{
			throw ErrorCodes::INVALIDVERSION;
		}
		if (CVCPCtrlNet::IsVCPControlClient())
		{
			throw ErrorCodes::CMDINVALIDVCPMODE;
		}
		DoSlaveGetSamples(msg, source);
		break;

	case SDfCtrlMsg::GET_CSMS_FAULT:
//		CLog::Log(CLog::INFORMATION, "rcvd GET_CSMS_FAULT from %s", peername);	// too verbose
		if (msg->hdr.bodySize != 0)
		{
			throw ErrorCodes::INVALIDVERSION;
		}
		DoGetCsmsFault(msg, source);
		break;

	case SDfCtrlMsg::SET_SLAVE_AUDIO_PARMS:
//		CLog::Log(CLog::INFORMATION, "rcvd GET_CSMS_FAULT from %s", peername);	// too verbose
		if (msg->hdr.bodySize != sizeof(SDfCtrlMsg::SSetSlaveAudioParams))
		{
			throw ErrorCodes::INVALIDVERSION;
		}
		if (msg->body.setSlaveAudioParam.freechannels)
		{
			DoFreeSlaveAudioChannel(msg, source);
		}
		else
		{
			DoSetSlaveAudioParams(msg, source);
		}
		break;

	case SDfCtrlMsg::CCDF_COLLECT:
//		CLog::Log(CLog::INFORMATION, "rcvd CCDF_COLLECT from %s", peername);	// Too many messages
		if (msg->hdr.bodySize != sizeof(SDfCtrlMsg::SCcdfCollect))
		{
			throw ErrorCodes::INVALIDVERSION;
		}
		if (CVCPCtrlNet::IsVCPControlClient())
		{
			throw ErrorCodes::CMDINVALIDVCPMODE;
		}
		DoCcdfCollect(msg, source);
		break;

	case SDfCtrlMsg::PING:
//		CLog::Log(CLog::INFORMATION, "rcvd PING from %s", peername);
		if (msg->hdr.bodySize != 0)
		{
			throw ErrorCodes::INVALIDVERSION;
		}
		DoPing(msg, source);
		break;

	case SDfCtrlMsg::DO_INITIAL_AGC:
//		CLog::Log(CLog::INFORMATION, "rcvd DO_INITIAL_AGC from %s", peername);
		if (msg->hdr.bodySize != sizeof(SDfCtrlMsg::SDoInitialAgc))
		{
			throw ErrorCodes::INVALIDVERSION;
		}
		DoInitialAgc(msg, source);
		break;


	default:
		CLog::Log(CLog::INFORMATION, _T("rcvd unknown INTERNAL_DF_CTRL subType %d from %s"), msg->hdr.msgSubType, peername);
		throw ErrorCodes::INVALIDSUBTYPE;
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Process the data collected in DoSlaveSetupCollect
//
void CSlaveControl::ProcessSlaveGetSamples(const SDfCollectTask& task)
{
	auto& req = task.msgBody;
	size_t numAnts = (req.numAnts != 0 ? req.numAnts : 1);

	Ne10F32cVec2 sampleVolts;
	Ne10F32Vec sampleWatts;

	auto antCount = SDfCtrlMsg::GetDataSize(req.dataTypes, req.count, req.count);
	uncached_ptr found;
	auto foundData = m_digitizer->m_sdma.FindSamples(task.sampleCount, antCount * numAnts, found);

	bool withIq = (req.dataTypes & SDfCtrlMsg::SlaveDataTypes::IQ) != 0; // false;
	bool withPsd = (req.dataTypes & SDfCtrlMsg::SlaveDataTypes::PSD) != 0; // true;
	bool withFft = (req.dataTypes & SDfCtrlMsg::SlaveDataTypes::FFT) != 0; // true;

	size_t bodySize = offsetof(SDfCtrlMsg::SSlaveGetSamplesResp, data) +
		SDfCtrlMsg::GetDataSize(req.dataTypes, req.count, req.numDfBins) * sizeof(unsigned long);
	size_t msgSize = offsetof(SDfCtrlMsg, body) + bodySize;

	SDfCtrlMsg::EStatus status = task.status;
	std::unique_ptr<SDfCtrlMsg> dfResp;
	if (status == SDfCtrlMsg::SUCCESS)
	{
		try
		{
			SDfCtrlMsg* p = new(msgSize) SDfCtrlMsg;
			dfResp.reset(p);
		}
		catch(std::bad_alloc& e)
		{
			status = SDfCtrlMsg::FAILURE;
		}
	}
	if (status == SDfCtrlMsg::SUCCESS && dfResp)	// redundant check
	{
		dfResp->hdr = task.msgHdr;
		dfResp->hdr.msgSubType = SDfCtrlMsg::SLAVE_GET_SAMPLES_RESP;
		dfResp->hdr.bodySize = bodySize;

		auto& samplesResp = dfResp->body.slaveGetSamplesResp;
		samplesResp.status = status;
		samplesResp.totalAnts = numAnts;
		samplesResp.numIq = (withIq ? req.count : 0);
		samplesResp.numPoints = (withPsd || withFft ? req.numDfBins : 0);
		samplesResp.dataTypes = req.dataTypes;

		if (foundData && found)
		{
			// Terminated data (for edr) is not being done in slave (only changes psd) - skip that logic

			// Get gain equalization if requested - to power spectrum only - independent of subblock
			Ne10F32Vec adjPower;
//			Ne10F32Vec rootPower;
			float gainAdj;
			GetCorrections(task, req.numDfBins, gainAdj, adjPower);

			auto foundAddr = found;	// address for first data block
#if 0
			printf("\nfreq = %f gainAdj = %15.5e\n", task.radioState.frequency, gainAdj);
#endif
			for (size_t subBlock = 0; subBlock < numAnts; ++subBlock)
			{
				// Find antenna number from the subblock
				SEquipCtrlMsg::EAnt eAnt;
				if (numAnts == 3) eAnt = SEquipCtrlMsg::DF_ANT_3V;
				else if (numAnts == 5) eAnt = SEquipCtrlMsg::DF_ANT_5V;
				else if (numAnts == 9) eAnt = SEquipCtrlMsg::DF_ANT_9V;	// polarization doesn't matter here
				else eAnt = SEquipCtrlMsg::DF_ANT_3V;	// safe default for now
				size_t ant = CConfig::GetAntFromSubBlock(subBlock, eAnt, task.radioState.ant == S2630Msg::HFANT);

				// Get the iq data, psd and voltages based on dataTypes
				uncached_ptr iqData;
				uncached_ptr psdData;
				uncached_ptr fftIData;
				uncached_ptr fftQData;

				if (withIq)
				{
					iqData = foundAddr;
					foundAddr += req.count;
				}
				if (withPsd)
				{
					psdData = foundAddr;
					foundAddr += req.count;
				}
				if (withFft)
				{
					fftIData = foundAddr;
					foundAddr += req.count;
					fftQData = foundAddr;
					foundAddr += req.count;
				}
				if (fftIData && fftQData)
				{
					CFft::GetVoltages2(req.numDfBins, req.count, fftIData, fftQData, task.gainAdj, sampleVolts);
				}
				if (psdData)
				{
					CFft::GetPowerSpectrumWatts2(req.numDfBins, req.count, psdData, gainAdj, sampleWatts);
				}

				// Do gain equalization if requested - to power spectrum only ??
				if (m_config->HasGainEqualization())
				{
					sampleWatts /= adjPower;
										// No voltage adjustments for now
	//				sampleVolts /= rootPower;
				}

				/*for (size_t i = 0; i < sampleWatts.size(); i++) //debug only
				{
					if (sampleWatts[i] > 1.e-11)
						printf("large sample found at index %i value = %.3e\n", i, sampleWatts[i]);
					if (sampleWatts[i] < 1.e-23)
						printf("small sample found at index %i value = %.3e\n", i, sampleWatts[i]);
				}*/  // debug only

#if 0
				size_t i1 = (req.numDfBins > 7 ? (req.numDfBins / 2) - 2 : 1);
				size_t i2 = std::min(i1 + 5, size_t(req.numDfBins - 1));
				printf("%u-%u smp:", i1, i2);
				for (size_t i = i1; i < i2; ++i)
				{
					ne10_fft_cpx_float32_t smp = { sampleVolts.real(i), sampleVolts.imag(i) };
					ne10_float32_t  mag;
					ne10_float32_t phas;
					ne10sCartToPolar(&smp, &mag, &phas, 1);
					printf(" %.9f %.9f %.9f %.9f\n", sampleVolts.real(i), sampleVolts.imag(i), mag, phas * Units::R2D);
				}
#endif
				auto dest = &samplesResp.data[0];
				if (withIq)
				{
					ne10sCopyUncached(const_cast<const unsigned long*>(iqData.get()), dest, req.count);
					dest += req.count;
				}
				if (withPsd)
				{
					memcpy(dest, &sampleWatts[0], req.numDfBins * sizeof(float));
					dest += req.numDfBins;
				}
				if (withFft)
				{
					memcpy(dest, sampleVolts.dataReal(), 2 * req.numDfBins * sizeof(float));
				}
				samplesResp.antNum = ant;
//				printf("SendToMaster: tag = %lu\n", dfResp->hdr.sourceAddr);
				//auto now = Utility::CurrentTimeAsTimestamp(); // debug only
				m_processorNode->SendToMaster(*dfResp, "SLAVE_GET_SAMPLES_RESP", false);	// log errors
				//auto timeDiff = (Utility::CurrentTimeAsTimestamp() - now).NanoSeconds<long>(); // debug only
				//printf("%s  slave sent SLAVE_GET_SAMPLES_RESP %ld\n", Utility::CurrentTimeAsTimestamp().Format(true).c_str(), timeDiff);//debug only
			}
		}
		else
		{
			printf("sample data not found\n");
			dfResp->hdr.bodySize = offsetof(SDfCtrlMsg::SSlaveGetSamplesResp, data);	// reset body size
			samplesResp.status = SDfCtrlMsg::FAILURE;
			samplesResp.numIq = 0;	// no data
			samplesResp.numPoints = 0;	// nodata
			for (size_t ant = 0; ant < numAnts; ++ant)
			{
				samplesResp.antNum = ant;
//				printf("SendToMaster: tag = %lu\n", dfResp->hdr.sourceAddr);
				m_processorNode->SendToMaster(*dfResp, "SLAVE_GET_SAMPLES_RESP");
			}
		}
	}
	else
	{
		if (task.status == SDfCtrlMsg::FAILURE)
		{
			printf("data collection failed\n");
		}
		else
		{
			printf("unable to allocate space for df response message\n");
		}
		SDfCtrlMsg errorResp;
		auto dfResp = &errorResp;
		dfResp->hdr = task.msgHdr;
		dfResp->hdr.msgSubType = SDfCtrlMsg::SLAVE_GET_SAMPLES_RESP;
		dfResp->hdr.bodySize = offsetof(SDfCtrlMsg::SSlaveGetSamplesResp, data);
		auto& samplesResp = dfResp->body.slaveGetSamplesResp;
		samplesResp.status = status;
		samplesResp.totalAnts = numAnts;
		samplesResp.numIq = 0;
		samplesResp.numPoints = 0;
		for (size_t ant = 0; ant < numAnts; ++ant)
		{
			samplesResp.antNum = ant;
//			printf("SendToMaster: tag = %lu\n", dfResp->hdr.sourceAddr);
			m_processorNode->SendToMaster(*dfResp, "SLAVE_GET_SAMPLES_RESP");
		}
	}
	m_digitizer->m_sdma.DoneWithSamples(task.sampleCount);

	return;
}

#if NO_SLAVE_CLIENT == 1
void CSlaveControl::ProcessSlaveGetSamples(const SDfCollectTask& task, CDfCtrlNet* source)
{
	auto& req = task.msgBody;
	size_t numAnts = (req.numAnts != 0 ? req.numAnts : 1);

	Ne10F32cVec2 sampleVolts;
	Ne10F32Vec sampleWatts;


	auto antCount = SDfCtrlMsg::GetDataSize(req.dataTypes, req.count, req.count);
	uncached_ptr found;
	auto foundData = m_digitizer->m_sdma.FindSamples(task.sampleCount, antCount * numAnts, found);
	//CLog::Log(CLog::INFORMATION, _T("slave processing found data = %d-- offset = %lu--- count = %lu"), foundData, task.sampleCount, antCount * req.numAnts); //debug only

	bool withIq = (req.dataTypes & SDfCtrlMsg::SlaveDataTypes::IQ) != 0; // false;
	bool withPsd = (req.dataTypes & SDfCtrlMsg::SlaveDataTypes::PSD) != 0; // true;
	bool withFft = (req.dataTypes & SDfCtrlMsg::SlaveDataTypes::FFT) != 0; // true;

	size_t bodySize = offsetof(SDfCtrlMsg::SSlaveGetSamplesResp, data) +
		SDfCtrlMsg::GetDataSize(req.dataTypes, req.count, req.numDfBins) * sizeof(unsigned long);
	size_t msgSize = offsetof(SDfCtrlMsg, body) + bodySize;

	SDfCtrlMsg::EStatus status = task.status;
	std::unique_ptr<SDfCtrlMsg> dfResp;
	if (status == SDfCtrlMsg::SUCCESS)
	{
		try
		{
			SDfCtrlMsg* p = new(msgSize) SDfCtrlMsg;
			dfResp.reset(p);
		}
		catch(std::bad_alloc& e)
		{
			status = SDfCtrlMsg::FAILURE;
		}
	}
	if (status == SDfCtrlMsg::SUCCESS && dfResp)	// redundant check
	{
		dfResp->hdr = task.msgHdr;
		dfResp->hdr.msgSubType = SDfCtrlMsg::SLAVE_GET_SAMPLES_RESP;
		dfResp->hdr.bodySize = bodySize;

		auto& samplesResp = dfResp->body.slaveGetSamplesResp;
		samplesResp.status = status;
		samplesResp.totalAnts = numAnts;
		samplesResp.numIq = (withIq ? req.count : 0);
		samplesResp.numPoints = (withPsd || withFft ? req.numDfBins : 0);
		samplesResp.dataTypes = req.dataTypes;

		if (foundData && found)
		{
			// Terminated data (for edr) is not being done in slave (only changes psd) - skip that logic

			// Get gain equalization if requested - to power spectrum only - independent of subblock
			Ne10F32Vec adjPower;
//			Ne10F32Vec rootPower;
			float gainAdj;
			GetCorrections(task, req.numDfBins, gainAdj, adjPower);

			auto foundAddr = found;	// address for first data block
#if 0
			printf("\nfreq = %f gainAdj = %15.5e\n", task.radioState.frequency, gainAdj);
#endif
			for (size_t subBlock = 0; subBlock < numAnts; ++subBlock)
			{
				// Find antenna number from the subblock
				SEquipCtrlMsg::EAnt eAnt;
				if (numAnts == 3) eAnt = SEquipCtrlMsg::DF_ANT_3V;
				else if (numAnts == 5) eAnt = SEquipCtrlMsg::DF_ANT_5V;
				else if (numAnts == 9) eAnt = SEquipCtrlMsg::DF_ANT_9V;	// polarization doesn't matter here
				else eAnt = SEquipCtrlMsg::DF_ANT_3V;	// safe default for now
				size_t ant = CConfig::GetAntFromSubBlock(subBlock, eAnt, task.radioState.ant == S2630Msg::HFANT);

				// Get the iq data, psd and voltages based on dataTypes
				uncached_ptr iqData;
				uncached_ptr psdData;
				uncached_ptr fftIData;
				uncached_ptr fftQData;

				if (withIq)
				{
					iqData = foundAddr;
					foundAddr += req.count;
				}
				if (withPsd)
				{
					psdData = foundAddr;
					foundAddr += req.count;
				}
				if (withFft)
				{
					fftIData = foundAddr;
					foundAddr += req.count;
					fftQData = foundAddr;
					foundAddr += req.count;
				}
				if (fftIData && fftQData)
				{
					CFft::GetVoltages2(req.numDfBins, req.count, fftIData, fftQData, task.gainAdj, sampleVolts);
				}
				if (psdData)
				{
					CFft::GetPowerSpectrumWatts2(req.numDfBins, req.count, psdData, gainAdj, sampleWatts);
				}

				// Do gain equalization if requested - to power spectrum only ??
				if (m_config->HasGainEqualization())
				{
					sampleWatts /= adjPower;
					// No voltage adjustments for now
	//				sampleVolts /= rootPower;
				}

#if 0
				size_t i1 = (req.numDfBins > 7 ? (req.numDfBins / 2) - 2 : 1);
				size_t i2 = std::min(i1 + 5, size_t(req.numDfBins - 1));
				printf("%u-%u smp:", i1, i2);
				for (size_t i = i1; i < i2; ++i)
				{
					ne10_fft_cpx_float32_t smp = { sampleVolts.real(i), sampleVolts.imag(i) };
					ne10_float32_t  mag;
					ne10_float32_t phas;
					ne10sCartToPolar(&smp, &mag, &phas, 1);
					printf(" %.9f %.9f %.9f %.9f\n", sampleVolts.real(i), sampleVolts.imag(i), mag, phas * Units::R2D);
				}
#endif
				auto dest = &samplesResp.data[0];
				if (withIq)
				{
					ne10sCopyUncached(const_cast<const unsigned long*>(iqData.get()), dest, req.count);
					dest += req.count;
				}
				if (withPsd)
				{
					memcpy(dest, &sampleWatts[0], req.numDfBins * sizeof(float));
					dest += req.numDfBins;
				}
				if (withFft)
				{
					memcpy(dest, sampleVolts.dataReal(), 2 * req.numDfBins * sizeof(float));
				}
				samplesResp.antNum = ant;
//				printf("SendToMaster: tag = %lu\n", dfResp->hdr.sourceAddr);
				//auto now = Utility::CurrentTimeAsTimestamp(); // debug only
				//m_processorNode->SendToMaster(*dfResp, "SLAVE_GET_SAMPLES_RESP", false);	// log errors
				m_processorNode->Send(source, *dfResp, "SLAVE_GET_SAMPLES_RESP", false); //debug only
				//auto timeDiff = (Utility::CurrentTimeAsTimestamp() - now).NanoSeconds<long>(); // debug only
				//printf("%s  slave sent SLAVE_GET_SAMPLES_RESP %ld\n", Utility::CurrentTimeAsTimestamp().Format(true).c_str(), timeDiff);//debug only
			}
		}
		else
		{
			printf("sample data not found\n");
			dfResp->hdr.bodySize = offsetof(SDfCtrlMsg::SSlaveGetSamplesResp, data);	// reset body size
			samplesResp.status = SDfCtrlMsg::FAILURE;
			samplesResp.numIq = 0;	// no data
			samplesResp.numPoints = 0;	// nodata
			for (size_t ant = 0; ant < numAnts; ++ant)
			{
				samplesResp.antNum = ant;
//				printf("SendToMaster: tag = %lu\n", dfResp->hdr.sourceAddr);
				m_processorNode->SendToMaster(*dfResp, "SLAVE_GET_SAMPLES_RESP");
			}
		}
	}
	else
	{
		if (task.status == SDfCtrlMsg::FAILURE)
		{
			printf("data collection failed\n");
		}
		else
		{
			printf("unable to allocate space for df response message\n");
		}
		SDfCtrlMsg errorResp;
		auto dfResp = &errorResp;
		dfResp->hdr = task.msgHdr;
		dfResp->hdr.msgSubType = SDfCtrlMsg::SLAVE_GET_SAMPLES_RESP;
		dfResp->hdr.bodySize = offsetof(SDfCtrlMsg::SSlaveGetSamplesResp, data);
		auto& samplesResp = dfResp->body.slaveGetSamplesResp;
		samplesResp.status = status;
		samplesResp.totalAnts = numAnts;
		samplesResp.numIq = 0;
		samplesResp.numPoints = 0;
		for (size_t ant = 0; ant < numAnts; ++ant)
		{
			samplesResp.antNum = ant;
//			printf("SendToMaster: tag = %lu\n", dfResp->hdr.sourceAddr);
			m_processorNode->SendToMaster(*dfResp, "SLAVE_GET_SAMPLES_RESP");
		}
	}
	m_digitizer->m_sdma.DoneWithSamples(task.sampleCount);

	return;
}
#endif // NO_SLAVE_CLIENT


//////////////////////////////////////////////////////////////////////
//
// Reset the 3230 time from ntp time (same function in CHwControl - share?)
//
bool CSlaveControl::Reset3230Time(bool logError, bool startup)
{
	bool hasNtpTime = m_navigation->IsNtpSyncd();
	unsigned long vals[5];
	bool errs[5];
	bool hasTimestamp = m_digitizer->CheckTimestamp(logError, startup, vals, errs);
	if (hasTimestamp && hasNtpTime)
	{
		auto stamp = Utility::CurrentTimeAsTimestamp();
		auto fraction = static_cast<unsigned long>(stamp.GetNTP() & 0xffffffff);
		size_t iSetSecs = 0;
		while (fraction < 0x60000000 || fraction > 0xa0000000)
		{
			usleep(10000);
			stamp = Utility::CurrentTimeAsTimestamp();
			fraction = static_cast<unsigned long>(stamp.GetNTP() & 0xffffffff);
			++iSetSecs;
		}
		m_digitizer->SetSeconds(stamp);

		Units::Timestamp debugStamp;
		m_digitizer->GetCurrentTime(debugStamp);
		TRACE("CHwControl: Set seconds[%u] to %s, get %s\n", iSetSecs, stamp.Format(true).c_str(), debugStamp.Format(true).c_str());
	}
	else if (logError)
	{
		CLog::Log(CLog::ERRORS, "CHwControl: CheckTimestamp error: not enabling 3230 timestamp");
	}
	m_digitizer->ResetDrift();

	return hasTimestamp;
}


//////////////////////////////////////////////////////////////////////
//
// Stop threads
//
void CSlaveControl::Stop(void)
{
	{
		// Do this before notifying the other threads
		std::lock_guard<std::mutex> lock(m_shutdownMutex);
		m_shutdown = true;
	}

	m_shutdownCond.notify_all();
	if (m_monitorThread.joinable())
	{
		m_monitorThread.join();
	}

	if (m_thread.joinable())
	{
		m_queue.Close();
		m_thread.join();
	}

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Processing thread
//
void CSlaveControl::Thread(void)
{
	int pingSource = m_watchdog->RegisterPingSource(_T("slave processing thread"));

	try
	{
		while(true)
		{
			m_watchdog->Ping(pingSource);

			try
			{
				static const unsigned int TIMEOUT = 1000;
				auto task(m_queue.GetItem(TIMEOUT));

				switch(task->msgHdr.msgSubType)
				{
				case SDfCtrlMsg::SLAVE_GET_SAMPLES:
#if NO_SLAVE_CLIENT == 0
					ProcessSlaveGetSamples(*task);
#endif
					break;

				default:
					throw std::logic_error("CSlaveControl::Thread: Invalid task type");
				}
			}
			catch(TaskQueue::EProblem& problem)
			{
				switch(problem)
				{
				case TaskQueue::TIMEDOUT:
					// Need to ping watchdog
					break;

				case TaskQueue::CLOSED:
					// Shutdown
					m_watchdog->UnregisterPingSource(pingSource);
					printf("CSlaveControl::Thread exiting\n");
					return;

				default:
					throw std::logic_error("Unexpected CSlaveControl::TaskQueue problem");
				}
			}
		}
	}
	catch(...)
	{
		CFailure::OnFail(std::current_exception());
	}

	m_watchdog->UnregisterPingSource(pingSource);
	return;
}
