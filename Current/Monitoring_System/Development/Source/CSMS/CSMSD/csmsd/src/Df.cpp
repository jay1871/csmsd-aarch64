/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"
#include "Config.h"
//#define CDF_SIMULATION
#include "Df.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
Ne10F32cVec CDf::m_antVec;
Ne10F32cVec CDf::m_antVec2;
Ne10F32cVec CDf::m_antVec3;
Ne10F32Vec CDf::m_az;
Ne10F32Vec CDf::m_conf;
Ne10F32Vec CDf::m_el;
std::vector<Ne10F32cVec> CDf::m_volts;
size_t CDf::m_numAnts = 0;
size_t CDf::m_numChans = 0;

#ifdef CSMS_DEBUG
bool CDf::s_traceWfa = false;
#endif

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CDf::CDf(_In_ const Ne10F32cVec& volts, Units::Frequency freq, CAntenna::EDfAlgorithm algorithm, ne10_float32_t prevAz) :
	CDfCore(volts.size()),
	m_binsPerChan(1),
	m_chanSize(0),
	m_config(),
	m_horiz(false),
	m_mutableConfig(m_config->GetMutableConfig()),
	m_navigation(),
	m_startFreq(freq),
	m_symmetryDfParams(nullptr),
	m_wfaConjPattern(nullptr)
{
	// Adjust static data if necessary
	if (m_numAnts != volts.size())
	{
		m_numAnts = int(volts.size());
		ASSERT(m_numAnts > 1);
		m_antVec.resize(int(m_numAnts));
		float phase = 0;
		ne10sTone_Direct(m_antVec, 1, 1.0f / m_numAnts, phase);
	}

	if (m_numChans != 1)
	{
		// Resize internal storage
		m_numChans = 1;
		m_az.resize(m_numChans);
		m_conf.resize(m_numChans);
		m_el.resize(m_numChans);
		m_volts.resize(m_numChans);
	}

	// Initialize the combined result
	m_volts[0] = volts;
	m_az[0] = prevAz;
	m_el[0] = Units::HALF_PI;
	m_conf[0] = 0;

#ifdef CSMS_DEBUG
	TRACE("CDf::CDf 1: m_volts[%u], m_volts[0][0].r %f, m_volts[0][0].i %f, m_volts[][1].r %f, m_volts[][1].i %f, m_volts[][2].r %f, m_volts[][2].i %f,m_volts[][3].r %f, m_volts[][3].i %f,m_volts[][4].r %f, m_volts[][4].i %f\n",
			m_volts.size(), m_volts[0][0].r, m_volts[0][0].i,
			m_volts[0][1].r, m_volts[0][1].i, m_volts[0][2].r, m_volts[0][2].i,
			m_volts[0][3].r, m_volts[0][3].i, m_volts[0][4].r, m_volts[0][4].i);
#endif
	// Do the DF
	DoDf(algorithm);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CDf::CDf(const std::vector<Ne10F32cVec2>& refVolts, const std::vector<Ne10F32cVec2>& sampleVolts,
	const std::vector<size_t>& validBins, Units::Frequency startFreq,
	Units::Frequency chanSize, unsigned int binsPerChan, CAntenna::EDfAlgorithm algorithm, const Ne10F32Vec& prevAz) :
	CDfCore(refVolts.size()),
	m_binsPerChan(binsPerChan),
	m_chanSize(chanSize),
	m_config(),
	m_horiz(false),
	m_mutableConfig(m_config->GetMutableConfig()),
	m_navigation(),
	m_startFreq(startFreq),
	m_symmetryDfParams(nullptr),
	m_wfaConjPattern(nullptr)
{
	ASSERT(refVolts.size() == sampleVolts.size() && refVolts[0].size() == sampleVolts[0].size());

	// Adjust static data if necessary
	if (m_numAnts != refVolts.size())
	{
		m_numAnts = refVolts.size();
		m_antVec.resize(m_numAnts);
		float phase = 0;
		ne10sTone_Direct(m_antVec, 1, 1.0f / m_numAnts, phase);
	}

	if (m_numChans != refVolts[0].size() / binsPerChan)
	{
		// Resize internal storage
		m_numChans = refVolts[0].size() / binsPerChan;
		m_az.resize(m_numChans);
		m_conf.resize(m_numChans);
		m_el.resize(m_numChans);
	}

	// Initialize the combined result
	m_volts.resize(m_numChans);
	size_t chan;

	for (chan = 0; chan < m_numChans; ++ chan)
	{
		m_volts[chan].resize(m_numAnts);
		m_volts[chan] = Ne10F32cVec::ZERO;
	}

	m_az = 0;
	m_el = Units::HALF_PI;
	m_conf = -1; // Don't measure

	// Transpose input data and combine bins
	if (validBins.empty())
	{
		// Nothing to do
		return;
	}

	chan = 0;
	Ne10F32cVec denom(m_numAnts, Ne10F32cVec::ZERO);

	for (size_t i = 0; i < validBins.size(); ++i)
	{
		size_t validBin = validBins[i];

		if (validBin / binsPerChan != chan)
		{
			// New combined bin
			if (denom[0].r != 0)
			{
				m_volts[chan] /= denom;
				m_az[chan] = (chan < prevAz.size() ? prevAz[chan] : 0);
				m_conf[chan] = 0;
			}

			chan = validBin / binsPerChan;
			denom = Ne10F32cVec::ZERO;
		}

		// Accumulate numerator and denominator
		for (size_t ant = 0; ant < m_numAnts; ++ant)
		{
			ne10_fft_cpx_float32_t temp = {
				sampleVolts[ant].real(validBin) * refVolts[ant].real(validBin) + sampleVolts[ant].imag(validBin) * refVolts[ant].imag(validBin),
				sampleVolts[ant].imag(validBin) * refVolts[ant].real(validBin) - sampleVolts[ant].real(validBin) * refVolts[ant].imag(validBin) };
			m_volts[chan][ant] += temp;
			denom[ant].r += refVolts[ant].real(validBin) * refVolts[ant].real(validBin) + refVolts[ant].imag(validBin) * refVolts[ant].imag(validBin);
		}
	}

	if (denom[0].r != 0)
	{
		m_volts[chan] /= denom;
		m_conf[chan] = 0;
	}

#ifdef CSMS_DEBUG
	TRACE("CDf::CDf Scan: m_volts[%u], chan %u; m_volts[chan][0].r %f, m_volts[chan][0].i %f, m_volts[][1].r %f, m_volts[][1].i %f, m_volts[][2].r %f, m_volts[][2].i %f,m_volts[][3].r %f, m_volts[][3].i %f,m_volts[][4].r %f, m_volts[][4].i %f\n",
			m_volts.size(), chan, m_volts[chan][0].r, m_volts[chan][0].i,
			m_volts[chan][1].r, m_volts[chan][1].i, m_volts[chan][2].r, m_volts[chan][2].i,
			m_volts[chan][3].r, m_volts[chan][3].i, m_volts[chan][4].r, m_volts[chan][4].i);
#endif
	// Do the DF
	DoDf(algorithm);

	return;

}


//////////////////////////////////////////////////////////////////////
//
// Perform DF calculation.
//
inline void CDf::DoDf(CAntenna::EDfAlgorithm algorithm)
{
	switch(algorithm)
	{
	case CAntenna::INTERFEROMETRY:
#ifdef CDF_SIMULATION
		SimulateDfInterferometry();
#endif
		CalcDfInterferometry();
		break;

	case CAntenna::WATSON_WATT:
	{
		const CAntenna::SWatsonWattParams* params = m_config->GetWatsonWattParams();
#ifdef CDF_SIMULATION
		SimulateDfWatsonWatt(params);
#endif
		CalcDfWatsonWatt(params);
	}
		break;

	case CAntenna::WATSON_WATT620:
	{
		const CAntenna::SWatsonWattParams* params = m_config->GetWatsonWatt620Params();
#ifdef CDF_SIMULATION
		SimulateDfWatsonWatt(params);
#endif
		CalcDfWatsonWatt(params);
	}
	break;

	case CAntenna::WFA649LOOPS_NOTUSED:
		// This algorithm is not used.
		TRACE("WFA649LOOPS is no longer used\n");
		break;

	case CAntenna::WFA:
		CalcDfWfa();
		break;

	case CAntenna::SYMMETRY:
		CalcDfSymmetry(false, false);
		break;

	case CAntenna::SYMMETRY_HORIZ:
		CalcDfSymmetry(true, false);
		break;

	case CAntenna::SYMMETRY_SHF:
		CalcDfSymmetry(false, true);
		break;

	default:
		THROW_LOGIC_ERROR();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Do symmetry DF using circular interferometry algorithm
//
void CDf::CalcDfCircularInterferometry(void)
{
	if (m_numAnts == 0)
		return;

	Ne10F32Vec phase(m_numAnts);

	for (unsigned int chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			ne10sPhase(m_volts[chan], phase);

			// Compute unwrapped voltage phase in radians for each element
			for (size_t i = 1; i < phase.size(); ++i)
			{
				while (phase[i] - phase[i - 1] < -Units::PI)
				{
					phase[i] += Units::TWO_PI;
				}
				while (phase[i] - phase[i - 1] > Units::PI)
				{
					phase[i] -= Units::TWO_PI;
				}
			}

			// Calculate azimuth and confidence
			ne10_fft_cpx_float32_t dot;
			ne10sDotProd(phase, m_antVec, dot);
			m_az[chan] = atan2f(dot.i, dot.r);

			auto sumph = phase.sum();
			ne10_float32_t sumphsq;
			ne10sDotProd(phase, phase, sumphsq);
			m_conf[chan] = (2 * (dot.r * dot.r + dot.i * dot.i) + sumph * sumph) / (m_numAnts * sumphsq);
		}
	}

	if (m_symmetryDfParams->arrayOffset != 0)
	{
		m_az += Units::D2R * m_symmetryDfParams->arrayOffset;
	}

	// Normalize azimuths
	Normalize();

	return;
}



//////////////////////////////////////////////////////////////////////
//
// Do DF using interferometry algorithm
//
void CDf::CalcDfInterferometry(void)
{
	size_t numAnts = ((m_numAnts & 1) != 0 ? m_numAnts - 1 : m_numAnts);	// if odd, first one is reference

	for (unsigned int chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			static const int NITERS = 5;
			size_t i;
			size_t iants;
			double wl_inv;
			std::vector<double> rx(numAnts);
			std::vector<double> ry(numAnts);
			std::vector<double> rz(numAnts);
			std::vector<double> phi(numAnts);
			double s11 = 0.0;
			double s12 = 0.0;
			double s13 = 0.0;
			double s22 = 0.0;
			double s23 = 0.0;
			double s33 = 0.0;
			double t0 = 0.0;
			double t1 = 0.0;
			double t2 = 0.0;
			double s1 = 0.0;
			double s2 = 0.0;
			double s3 = 0.0;
			double a11, a12, a13, a22, a23;
			double b1, b2;
			double sum1, sum2;
			double denom = 0;
			double tmp;
			double k1 = 0.0;
			double k2 = 0.0;
			double k3 = 0.0;
			double iphi;
			double phmean, xmean, ymean, zmean;
			double vsq;
			double delph;
			double ampl;
			double phi0;

			// Compute conversion factor
			wl_inv = (m_startFreq + chan * m_chanSize + m_chanSize / 2).Hz<double>() * Units::TWO_PI / LIGHTSPEED; // = 2PI/wavelength

			// If this has been collected as odd number of voltages, then the first one in the reference followed by the df elements
			if ((m_volts[chan].size() & 1) != 0)
			{
				// Normalize by the reference voltage before calculating
				auto temp = m_volts[chan][0];
				for (size_t i = 1; i < m_volts[chan].size(); ++i)
				{
					m_volts[chan][i - 1] = m_volts[chan][i] / temp;
				}
				m_volts[chan].resize(numAnts);
			}

			// Compute voltage phase in radians for each element
			for (i = 0; i < numAnts; ++i)
			{
				phi[i] = atan2f(m_volts[chan][i].i, m_volts[chan][i].r);
			}

			// Compute element coordinates in wavelengths &
			//  the voltage phases; both relative to the reference
			//  element
			for (i = 0; i < numAnts; ++i)
			{
				rx[i] = wl_inv * (m_mutableConfig.hfConfig.antCoords[i + 1].x - m_mutableConfig.hfConfig.antCoords[0].x);
				ry[i] = wl_inv * (m_mutableConfig.hfConfig.antCoords[i + 1].y - m_mutableConfig.hfConfig.antCoords[0].y);
				rz[i] = wl_inv * (m_mutableConfig.hfConfig.antCoords[i + 1].z - m_mutableConfig.hfConfig.antCoords[0].z);
			}

			// Sequence through pairs of antenna elements
			for (iants = 3; iants <= numAnts + 1; iants += 2)
			{
				for (i = iants - 3; i < iants - 1; ++i)
				{
					// Compute ideal phase
					iphi = k1 * rx[i] + k2 * ry[i] + k3 * rz[i];

					// Unwrap phase
					while(iphi - phi[i] > Units::PI)
					{
						phi[i] += Units::TWO_PI;
					}

					while(phi[i] - iphi >= Units::PI)
					{
						phi[i] -= Units::TWO_PI;
					}

					ASSERT(fabs(iphi - phi[i]) <= Units::PI);

					// Accumulate coefficients
					s11 += rx[i] * rx[i];
					s12 += rx[i] * ry[i];
					s13 += rx[i] * rz[i];
					s22 += ry[i] * ry[i];
					s23 += ry[i] * rz[i];
					s33 += rz[i] * rz[i];

					t1 += rx[i] * phi[i];
					t2 += ry[i] * phi[i];

					s1 += rx[i];
					s2 += ry[i];
					s3 += rz[i];

					t0 += phi[i];
				}

				tmp = 1.0 / iants;
				a11 = s11 - s1 * s1 * tmp;
				a12 = s12 - s1 * s2 * tmp;
				a13 = s13 - s1 * s3 * tmp;
				a22 = s22 - s2 * s2 * tmp;
				a23 = s23 - s2 * s3 * tmp;

				b1 = t1 - s1 * t0 * tmp;
				b2 = t2 - s2 * t0 * tmp;

				// Compute the signal wave vector
				denom = a11 * a22 - a12 * a12;

				if (denom == 0.0)
				{
					m_conf[chan] = 0.0;
					m_az[chan] = 0.0;
					m_el[chan] = Units::HALF_PI;
					break;
				}

				double lastk3;

				for (i = 0; i < NITERS; ++i)
				{
					lastk3 = k3;
					sum1 = b1 - a13 * k3;
					sum2 = b2 - a23 * k3;
					k1 = (sum1 * a22 - sum2 * a12) / denom;
					k2 = (sum2 * a11 - sum1 * a12) / denom;
					k3 = 1.0 - k1 * k1 - k2 * k2;

					if (k3 <= 0.0)
					{
						tmp = 1.0 / sqrt(k1 * k1 + k2 * k2);
						k1 *= tmp;
						k2 *= tmp;
						k3 = 0.0;
						break;
					}
					else
					{
						k3 = sqrt(k3);

						if (a13 == 0 || a23 == 0 || fabs(lastk3 - k3) < 0.001f)
						{
							break;
						}
					}
				}
			}

			if (denom == 0.0)
			{
				return;
			}

			// Compute azimuth
			double az1 = atan2(k2, k1);

			// Convert from ccw rotation wrt x to cw rotation wrt y
			az1 = Units::HALF_PI - az1;

			while(az1 < 0.0)
			{
				az1 += Units::TWO_PI;
			}

			while(az1 >= Units::TWO_PI)
			{
				az1 -= Units::TWO_PI;
			}

			m_az[chan] = float(az1);

			// Compute elevation
			m_el[chan] = float(atan2(k3, sqrt(k1 * k1 + k2 * k2)));

			// Compute confidence
			phmean = t0 / numAnts;
			xmean = s1 / numAnts;
			ymean = s2 / numAnts;
			zmean = s3 / numAnts;

			phi0 = phmean - k1 * xmean - k2 * ymean - k3 * zmean;

			s1 = 0.0;
			s2 = 0.0;
			vsq = 0.0;

			for(i = 0; i < numAnts; ++i)
			{
				ampl = sqrt(m_volts[chan][i].r * m_volts[chan][i].r + m_volts[chan][i].i * m_volts[chan][i].i);
				delph = phi[i] - k1 * rx[i] - k2 * ry[i] - k3 * rz[i] - phi0;
				s1 += ampl * cos(delph);
				s2 += ampl * sin(delph);
				vsq += ampl * ampl;
			}

			m_conf[chan] = float((s1 * s1 + s2 * s2) / (vsq * numAnts));
		}
	}

	Normalize();

	return;
}



//////////////////////////////////////////////////////////////////////
//
// Do DF using symmetry algorithm
//
void CDf::CalcDfSymmetry(bool horiz, bool shf)
{
	m_horiz = horiz;
	m_symmetryDfParams = m_config->GetSymmetryDfParams(horiz, shf);
	if (m_symmetryDfParams == nullptr)
	{
		TRACE("No symmetry Df params available\n");
		return;
	}
	if (m_symmetryDfParams->methods.size() == 0)
	{
		TRACE("No symmetry methods defined\n");
		return;
	}

	// Coarse calculation
	size_t methodIdx = 0;
	if (m_symmetryDfParams->methods.size() > 1)
	{
		for (methodIdx = 0; methodIdx < m_symmetryDfParams->methods.size() - 1 && m_startFreq > m_symmetryDfParams->methods[methodIdx].freq; ++methodIdx);
	}
	const CAntenna::SSymmetryDfParams::SMethod& method = m_symmetryDfParams->methods[methodIdx];

	switch(method.method)
	{
	case CAntenna::SSymmetryDfParams::SMethod::MODES_FINE: // Modes + fine
	case CAntenna::SSymmetryDfParams::SMethod::MODES_ONLY: // Modes only
		CalcSymModes();
		break;

	case CAntenna::SSymmetryDfParams::SMethod::MODES_INV_FINE: // Inverted Modes + fine
	case CAntenna::SSymmetryDfParams::SMethod::MODES_INV_ONLY: // Inverted Modes
		CalcSymModes(true);
		break;

	case CAntenna::SSymmetryDfParams::SMethod::GRID_FINE: // Grid search + fine
		CalcSymGrid(method.gridWidth, method.uniqueConf, method.phaseAdjust);
		break;

	case CAntenna::SSymmetryDfParams::SMethod::AMP_ONLY: // Amplitude only
	case CAntenna::SSymmetryDfParams::SMethod::AMP_FINE: // Amplitude + fine
		CalcSymAmpl();
		break;

	case CAntenna::SSymmetryDfParams::SMethod::AMP_PAIRS_FINE: // Amplitude pairs + fine
		CalcSymAmplPairs();
		break;

	case CAntenna::SSymmetryDfParams::SMethod::AMP_TRIPLET_FINE:
		CalcSymAmplTriplet();
		break;

	case CAntenna::SSymmetryDfParams::SMethod::AMP_DOUBLE_FINE:
	case CAntenna::SSymmetryDfParams::SMethod::AMP_DOUBLE_ONLY:
		CalcSymAmplDouble(method.phaseAdjust);
		break;

	case CAntenna::SSymmetryDfParams::SMethod::MODES23_FINE:		// Modes 2-3 + fine
		CalcSymModes23();
		break;

	case CAntenna::SSymmetryDfParams::SMethod::AMP_DOUBLE2_FINE:
		CalcSymAmplDouble2();
		break;

	case CAntenna::SSymmetryDfParams::SMethod::MODESCF_FINE:
		CalcSymModesCf(false, method.phaseAdjust);
		break;

	case CAntenna::SSymmetryDfParams::SMethod::MODESCF_INV_FINE:
		CalcSymModesCf(true, method.phaseAdjust);
		break;

	case CAntenna::SSymmetryDfParams::SMethod::WFA:
		CalcSymWfa(method.gridWidth, method.uniqueConf, false);
		break;

	case CAntenna::SSymmetryDfParams::SMethod::WFA_MODES_INV:
		CalcSymWfa(method.gridWidth, method.uniqueConf, true);
		break;

	case CAntenna::SSymmetryDfParams::SMethod::CIRCULAR_INTERFEROMETRY:
		CalcDfCircularInterferometry();
		break;

	default:
		ASSERT(FALSE);
		return;
	}

	if (method.method == CAntenna::SSymmetryDfParams::SMethod::WFA || method.method == CAntenna::SSymmetryDfParams::SMethod::WFA_MODES_INV ||
		method.method == CAntenna::SSymmetryDfParams::SMethod::CIRCULAR_INTERFEROMETRY)
	{
		return;
	}

	CalcSymOffsetCorrection();

	if (method.method == CAntenna::SSymmetryDfParams::SMethod::MODES_ONLY ||
		method.method == CAntenna::SSymmetryDfParams::SMethod::AMP_ONLY ||
		method.method == CAntenna::SSymmetryDfParams::SMethod::MODES_INV_ONLY ||
		method.method == CAntenna::SSymmetryDfParams::SMethod::AMP_DOUBLE_ONLY)
	{
		for (size_t chan = 0; chan < m_numChans; ++chan)
		{
			if(m_conf[chan] != -1)
			{
				InitSymConf(chan);
				m_conf[chan] = CalcSymConf(m_az[chan], m_volts[chan], method.phaseAdjust, m_horiz);

				// Apply the "post-calculation" azimuth correction AFTER the conf factor is calculated
				m_az[chan] -= CalcPostAzimCorrection(chan);
			}
		}

		if (m_symmetryDfParams->arrayOffset != 0)
		{
			m_az += Units::D2R * m_symmetryDfParams->arrayOffset;
		}

		Normalize();

		return;
	}

	// Fine calculation
	static const float TOL = 0.05f * Units::D2R;
	static const float DELTA_MAX = 5 * Units::D2R;

	for (size_t chan = 0; chan < m_numChans; ++chan)
	{
		if(m_conf[chan] != -1)
		{
			int leftGoodEl = 0;

			if(method.mainLobeWidth < 360)
			{
				// Do backlobe blanking
				leftGoodEl = int(m_numAnts * (m_az[chan] / Units::TWO_PI - method.mainLobeWidth / 2.0f / Units::DEG_PER_CIRCLE) + m_numAnts + 1) % m_numAnts;
				int rightGoodEl = int(m_numAnts * (m_az[chan] / Units::TWO_PI + method.mainLobeWidth / 2.0f / Units::DEG_PER_CIRCLE) + m_numAnts) % m_numAnts;
				m_volts[chan] <<= leftGoodEl;
				m_volts[chan].resize((rightGoodEl - leftGoodEl + m_numAnts + 1) % m_numAnts);
				m_az[chan] -= leftGoodEl * Units::TWO_PI / m_numAnts;
			}

			InitSymConf(chan);

			// Grid search spiralling out from coarse result
			float c;
			float cMax = 0;
			float azim1 = 0;

			for(int i = 0; i <= method.gridWidth / 2; i = (i < 0 ? -i : -i - 1))
			{
				float azim2 = m_az[chan] + i * Units::D2R;
				c = CalcSymConf(azim2, m_volts[chan], method.phaseAdjust, m_horiz);
				if(c > cMax)
				{
					cMax = c;
					azim1 = azim2;

					// Terminate search if confidence is "good enough"
					if(cMax > method.uniqueConf)
					{
						break;
					}
				}
			}

			if((c = cMax) != 0)
			{
				// Newton's Method
				unsigned int iter = 0;
				for(iter = 0; iter < 10; ++iter)
				{
					float c1 = CalcSymConf(azim1 - TOL, m_volts[chan], method.phaseAdjust, m_horiz);
					float c2 = CalcSymConf(azim1 + TOL, m_volts[chan], method.phaseAdjust, m_horiz);
					float dc = (c2 - c1) / 2;
					float ddc = c2 - 2 * c + c1;
					float delta;

					if(ddc >= 0 && ddc <= 4 * FLT_EPSILON)
					{
						delta = TOL;
					}
					else if(ddc > 0)
					{
						delta = DELTA_MAX;
					}
					else
					{
						delta = fabsf(TOL * dc / ddc);

						if(delta > DELTA_MAX)
						{
							delta = DELTA_MAX;
						}
					}

					if(dc < 0)
					{
						delta = -delta;
					}

					azim1 += delta;
					c = CalcSymConf(azim1, m_volts[chan], method.phaseAdjust, m_horiz);

					if(fabsf(delta) < TOL)
					{
						break;
					}
				}

				m_az[chan] = azim1 + leftGoodEl * Units::TWO_PI / m_numAnts;
				m_conf[chan] = c;
			}
			else
			{
				m_az[chan] = 0;
				m_conf[chan] = 0;
			}
		}
		m_az[chan] -= CalcPostAzimCorrection(chan);
	}

	if (m_symmetryDfParams->arrayOffset != 0)
	{
		m_az += Units::D2R * m_symmetryDfParams->arrayOffset;
	}

	Normalize();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Do DF using Watson Watt algorithm
//
void CDf::CalcDfWatsonWatt(const CAntenna::SWatsonWattParams* params)
{
	if (params == nullptr)
		return;

	for (unsigned int chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			// Find the known phase difference of ref wrt loops at this freq
			Units::Frequency freq = m_startFreq + chan * m_chanSize + m_chanSize / 2;
			float sinRef;
			float cosRef;
			if (GetWWRefPhase(freq, params, sinRef, cosRef))
			{
				// If this has been collected as 3 voltages, then the first one in the reference followed by the loops
				if (m_volts[chan].size() == 3)
				{
					// Normalize by the reference voltage before calculating
					auto temp = m_volts[chan][0];
					m_volts[chan][0] = m_volts[chan][1] / temp;
					m_volts[chan][1] = m_volts[chan][2] / temp;
					m_volts[chan].resize(2);
				}
				// Do the calculation
				float ampl0 = sqrtf(m_volts[chan][0].r * m_volts[chan][0].r + m_volts[chan][0].i * m_volts[chan][0].i);
				float ampl1 = sqrtf(m_volts[chan][1].r * m_volts[chan][1].r + m_volts[chan][1].i * m_volts[chan][1].i);
				float az0;
				float az1;

				// Pick two most likely quadrants
				if(ampl0 < ampl1)
				{
					if (m_volts[chan][1].r * cosRef + m_volts[chan][1].i * sinRef < 0)
					{
						ampl1 = -ampl1;
					}

					az0 = atan2f(ampl0, ampl1);
					az1 = atan2f(-ampl0, ampl1);
				}
				else
				{
					if (m_volts[chan][0].r * cosRef + m_volts[chan][0].i * sinRef < 0)
					{
						ampl0 = -ampl0;
					}

					az0 = atan2f(ampl0, ampl1);
					az1 = atan2f(ampl0, -ampl1);
				}

				// Pick best confidence
				float conf0 = CalcWWConf(chan, az0);
				float conf1 = CalcWWConf(chan, az1);

				if (conf0 > conf1)
				{
					m_az[chan] = az0;
					m_conf[chan] = conf0;
				}
				else
				{
					m_az[chan] = az1;
					m_conf[chan] = conf1;
				}

				// Adjust to vehicle reference
				m_az[chan] += Units::HALF_PI / 2;
			}
		}
	}

	Normalize();

	return;
}


////////////////////////////////////////////////////////////////////////////////
//
// Do DF using WFA algorithm
//
void CDf::CalcDfWfa(void)
{
	const CAntenna::SWfaParams* wfaParams = m_config->GetWfaParams();
	if (wfaParams == nullptr)
	{
		TRACE("No WFA params available\n");
		return;
	}
	for (unsigned int chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1 &&
			(m_wfaConjPattern = m_config->GetWfaConjPattern(m_startFreq + chan * m_chanSize + m_chanSize / 2)) != nullptr)
		{
			// Normalize the input voltages
			ne10_float32_t norm;
			ne10sNorm_L2(m_volts[chan], norm);
			m_volts[chan] /= norm;

			// Perform coarse pattern search
			int azMax;
			CalcWfaCoarseSearch(chan, *wfaParams, azMax);

			// Perform fine pattern search
			CalcWfaFineSearch2d(chan, azMax);
		}
	}

	// Normalize azimuths
	Normalize();

	return;
}


////////////////////////////////////////////////////////////////////////////////
//
// Calculate the voltage terms for the dual pol fourier alg
//
void CDf::CalcDualPolVoltageTerms(Ne10F32cVec volts, const CAntenna::DualPolPattern& dualPol, SDualPolVoltageTerms& v) const
{
	// Normalize the input voltages
	ne10_float32_t norm;
	ne10sNorm_L2(volts, norm);
	volts /= norm;

	long numFineSteps = static_cast<long>(m_fineTwid.size());
	long numStepsPerAnt = numFineSteps / m_numAnts;
	ASSERT(numStepsPerAnt * static_cast<long>(m_numAnts) == numFineSteps);

	// Calculate the voltage modes
	v.same.resize(2 * dualPol.maxMode + 1);
	v.other.resize(2 * dualPol.maxMode + 1);
	v.modes.resize(2 * dualPol.maxMode + 1);
	for (long i = 0; i < static_cast<long>(2 * dualPol.maxMode + 1); ++i)
	{
		v.modes[i] = Ne10F32cVec::ZERO;
		long m = i - static_cast<long>(dualPol.maxMode);
		for (size_t n = 0; n < volts.size(); ++n)
		{
			// Note: m is signed so we add enough to make it positive and such that x % numFineSteps = 0
			long twidIdx = ((m + m_numAnts) * n * numStepsPerAnt) % numFineSteps;
			v.modes[i] += MultByConj(volts[n], m_fineTwid[twidIdx]);
		}
		v.same[i] = v.modes[i] * dualPol.coefSame[i];
		v.other[i] = v.modes[i] * dualPol.coefOther[i];
	}
	return;
}


////////////////////////////////////////////////////////////////////////////////
//
// Calculate conf for the dual pol fourier alg
//
float CDf::CalcDualPolWfaConf(const ne10_fft_cpx_float32_t& bSame, const ne10_fft_cpx_float32_t bOther, const CAntenna::DualPolPattern& dualPol,
	float* ampSame, float* ampOther)
{
	float conf = 0;

	if (dualPol.normOther < 1.e-4 * dualPol.normSame)	// Must solve as single pol same
	{
		conf = (bSame.r * bSame.r + bSame.i * bSame.i);
		if (ampSame) *ampSame = 1;
		if (ampOther) *ampOther = 0;
	}
	else if (dualPol.normSame < 1.e-4 * dualPol.normOther)
	{
		conf = (bOther.r * bOther.r + bOther.i * bOther.i);
		if (ampSame) *ampSame = 0;
		if (ampOther) *ampOther = 1;
	}
	else
	{
		float temp = 1 - (dualPol.otherSame.r * dualPol.otherSame.r + dualPol.otherSame.i * dualPol.otherSame.i);
		if (temp < 1.e-4)	// Correlated patterns - solve as single pol using either pol pattern (choose same)
		{
			conf = (bSame.r * bSame.r + bSame.i * bSame.i);
			if (ampSame) *ampSame = 1;
			if (ampOther) *ampOther = 0;
		}
		else
		{
			ne10_fft_cpx_float32_t aSame = (bSame - bOther * dualPol.otherSame) / temp;
			ne10_fft_cpx_float32_t aOther = (bOther - MultByConj(bSame, dualPol.otherSame)) / temp;
			conf = (bSame.r * aSame.r + bSame.i * aSame.i + bOther.r * aOther.r + bOther.i * aOther.i);

			// If wanted, return the normalized A-vector:
			if (ampSame)
				*ampSame = (dualPol.normSame > 0 ? sqrtf(aSame.r * aSame.r + aSame.i * aSame.i) / dualPol.normSame : 0);
			if (ampOther)
				*ampOther = (dualPol.normOther > 0 ? sqrtf(aOther.r * aOther.r + aOther.i * aOther.i) / dualPol.normOther : 0);
		}
	}
	return conf;
}


float CDf::CalcDualPolWfaConf(float azim, const SDualPolVoltageTerms& vModes, const CAntenna::DualPolPattern& dualPol,
	float* ampSame, float* ampOther)
{
	// Arbitrary azimuth (radians)
	ne10_fft_cpx_float32_t bSame = Ne10F32cVec::ZERO;
	ne10_fft_cpx_float32_t bOther = Ne10F32cVec::ZERO;
	for (long i = 0; i < static_cast<long>(2 *  dualPol.maxMode + 1); ++i)
	{
		long m = i - static_cast<long>(dualPol.maxMode);
		ne10_fft_cpx_float32_t z = { cosf(m * azim), sinf(m * azim) };
		bSame += vModes.same[i] * z;
		bOther += vModes.other[i] * z;
	}
	return CalcDualPolWfaConf(bSame, bOther, dualPol, ampSame, ampOther);
}


//////////////////////////////////////////////////////////////////////
//
// Get the special modes azimuth correction
//
float  CDf::CalcModeAzimCorr(Units::Frequency freq) const
{
	if (m_symmetryDfParams->modesazimMap.empty())
	{
		return 0;
	}

	return CConfig::GetFreqDepValue(m_symmetryDfParams->modesazimMap, freq) * Units::D2R;
}


//////////////////////////////////////////////////////////////////////
//
// Initialize correction to be applied to azimuth AFTER fine calculation (and coarse if not doing fine)
//
float CDf::CalcPostAzimCorrection(size_t chan) const
{
	if (m_symmetryDfParams->postcorrMap.empty())
		return 0;

	Units::Frequency freq = m_startFreq + chan * m_chanSize + m_chanSize / 2;
	return CConfig::GetFreqDepValue(m_symmetryDfParams->postcorrMap, freq) * Units::D2R;
}


//////////////////////////////////////////////////////////////////////
//
// Do coarse DF using symmetry algorithm
//
void CDf::CalcSymAmpl(void)
{
	static Ne10F32Vec watts;
	ne10_fft_cpx_float32_t sum;

	for (unsigned int chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			ne10sPowerSpectr(m_volts[chan], watts);
			ne10sDotProd(watts, m_antVec, sum);
			m_az[chan] = atan2f(sum.i, sum.r);
		}
	}

	return;
}

//////////////////////////////////////////////////////////////////////
//
// Do coarse DF using amplitude symmetry based on 2*theta
//
void CDf::CalcSymAmplDouble(bool phaseAdjust)
{
	InitTwoPhase();

	static Ne10F32Vec watts;
	ne10_fft_cpx_float32_t sum;

	for (unsigned int chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			InitSymConf(chan);
			ne10sPowerSpectr(m_volts[chan], watts);
			ne10sDotProd(watts, m_antVec2, sum);
			float az = 0.5f * atan2f(sum.i, sum.r);

			// Check for "flip" by using confidence
			float cf1 = CalcSymConf(az, m_volts[chan], phaseAdjust, m_horiz);
			float cf2 = CalcSymConf(az + Units::PI, m_volts[chan], phaseAdjust, m_horiz);
			m_az[chan] = (cf1 > cf2 ? az : az + Units::PI);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Do coarse DF using amplitude symmetry based on 2*theta and 1*theta
//
void CDf::CalcSymAmplDouble2(void)
{
	InitTwoPhase();

	static Ne10F32Vec watts;
	ne10_fft_cpx_float32_t sum;

	for (unsigned int chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			InitSymConf(chan);
			ne10sPowerSpectr(m_volts[chan], watts);		// Might prefer ippsMagnitude instead of power here
			ne10sDotProd(watts, m_antVec2, sum);
			float az = 0.5f * atan2f(sum.i, sum.r);
			if (az < 0)
				az += Units::PI;

			// Check for "flip" by using SymAmpl

			ne10sDotProd(watts, m_antVec, sum);			// Might prefer ippsMagnitude instead of power here, too
			float az1 = atan2f(sum.i, sum.r);
			if (az1 < 0)
				az1 += Units::TWO_PI;

			float diff = Units::PI - fabsf(fabsf(az - az1) - Units::PI);
			if (diff > Units::HALF_PI)
				az += Units::PI;

			// Sanity check

			diff = Units::PI - fabsf(fabsf(az - az1) - Units::PI);
			m_az[chan] = (diff < Units::PI / 10.f ? az : az1);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Do coarse DF using strongest pairs algorithm
//
void CDf::CalcSymAmplPairs(void)
{
	static Ne10F32Vec watts;

	for (unsigned int chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			ne10sPowerSpectr(m_volts[chan], watts);

			// Find the adjacent pair with the largest power
			size_t maxIdx = watts.size() - 1;  //  ex. 8
			size_t maxIdx1 = 0;
			float maxwatts = watts[maxIdx] + watts[maxIdx1];	// e.g., [8] + [0]

			for (size_t i = maxIdx; i > 0; --i)	//	8,7,6,5,4,3,2,1
			{
				float pair = watts[i - 1] + watts[i];	//	7+8,...0+1

				if (pair > maxwatts)
				{
					maxwatts = pair;
					maxIdx = i - 1;	// Lowest antenna of pair
					maxIdx1 = i;
				}
			}

			float watts1 = watts[maxIdx];
			float watts2 = watts[maxIdx1];

			m_az[chan] = m_symmetryDfParams->pairFactor * (watts2 - watts1) / (watts2 + watts1) +
				Units::TWO_PI / m_numAnts * (maxIdx + 0.5f);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Do coarse DF using strongest element with triplet algorithm
//
void CDf::CalcSymAmplTriplet(void)
{
	static Ne10F32Vec watts;

	for (unsigned int chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			ne10sPowerSpectr(m_volts[chan], watts);

			// Find the antenna with the largest power
			size_t maxIdx = 0;
			float maxwatts = 0;

			for (size_t i = 0; i < watts.size(); ++i)
			{
				if (watts[i] > maxwatts)
				{
					maxwatts = watts[i];
					maxIdx = i;
				}
			}

			size_t maxIdx1 = (maxIdx == watts.size() - 1 ? 0 : maxIdx + 1);
			size_t maxIdx2 = (maxIdx == 0 ? watts.size() - 1 : maxIdx - 1);
			float denom = watts[maxIdx2] + watts[maxIdx1] - 2 * watts[maxIdx];
			float delta = 0;

			if(denom != 0)
			{
				static const float TRIPLET_FACTOR = 0.49f;
				delta = TRIPLET_FACTOR * (watts[maxIdx2] - watts[maxIdx1]) / denom;

				if (delta > Units::PI / m_numAnts)
				{
					delta = Units::PI / m_numAnts;
				}
				else if(delta < -Units::PI / m_numAnts)
				{
					delta = -Units::PI / m_numAnts;
				}
			}

			m_az[chan] = Units::TWO_PI / m_numAnts * maxIdx + delta;
		}
	}

	return;
}


#if 0
//////////////////////////////////////////////////////////////////////
//
// Calculate symmetry confidence
//
float CDf::CalcSymConfOrig(float azim, const Ne10F32cVec& volts, bool phaseAdjust) const
{
	float cosAzim = cosf(azim);
	float sinAzim = sinf(azim);
	static Ne10F32Vec phase;
	phase.resize(volts.size());

	for (size_t i = 0; i < phase.size(); i++)
	{
		phase[i] = cosAzim * m_antVec[i].r + sinAzim * m_antVec[i].i;
	}

	if (phaseAdjust && m_horiz)
	{
		for (size_t i = 0; i < phase.size(); i++)
		{
			if (phase[i] < 0)
			{
				// Adjust expected phase of back elements by 180°
				phase[i] += Units::PI / m_confFact;
			}
		}
	}

	phase *= -m_confFact;
	static Ne10F32cVec expected;
	ne10sPolarToCart(m_measAmp, phase, expected);
	ne10_fft_cpx_float32_t dotProd;
	ne10sDotProd(volts, expected, dotProd);

	return (dotProd.r * dotProd.r + dotProd.i * dotProd.i) * m_confNorm;
}
#endif

//////////////////////////////////////////////////////////////////////
//
// Do coarse DF using grid algorithm
//
void CDf::CalcSymGrid(unsigned char width, float uniqueConf, bool phaseAdjust)
{
	ASSERT(Units::DEG_PER_CIRCLE % width == 0);

	for (size_t chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			InitSymConf(chan);

			// Find peak
			for (unsigned int i = 0; i < Units::DEG_PER_CIRCLE / width; ++i)
			{
				float azim = i * width * Units::D2R;
				float conf = CalcSymConf(azim, m_volts[chan], phaseAdjust, m_horiz);

				if (conf > m_conf[chan])
				{
					m_conf[chan] = conf;
					m_az[chan] = azim;

					if (conf > uniqueConf)
					{
						// Found "good enough" peak
						break;
					}
				}
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Do coarse DF using modes algorithm
//
void CDf::CalcSymModes(bool inverted)
{
	for (size_t chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			static Ne10F32cVec modes(3);
			static Ne10F32Vec modesAmp(3);
			static Ne10F32Vec modesPhase(3);

			// Calculate specified modes
			modes[0] = m_volts[chan].sum(); // mode 0
			ne10sDotProd(m_volts[chan], m_antVec, modes[1]); // mode 1
			ne10sCrossCorr(m_antVec, m_volts[chan], modes[2]); // mode -1
			ne10sCartToPolar(modes, modesAmp, modesPhase);

			// Compute specified azimuths
			Units::Frequency freq = m_startFreq + chan * m_chanSize + m_chanSize / 2;
			float corrfac = CalcSymModesCorr(freq);
			float corr0 = (inverted ? modesPhase[0] - corrfac : modesPhase[0] + corrfac);
			float azim0 = modesPhase[1] - corr0;
			float azim1 = corr0 - modesPhase[2];

			// Adjust for special cases (e.g., 647D horiz)
			azim0 -= CalcModeAzimCorr(freq);

			if (azim0 < 0)
			{
				azim0 += Units::TWO_PI;
			}
			else if (azim0 >= Units::TWO_PI)
			{
				azim0 -= Units::TWO_PI;
			}

			if (azim1 < 0)
			{
				azim1 += Units::TWO_PI;
			}
			else if (azim1 >= Units::TWO_PI)
			{
				azim1 -= Units::TWO_PI;
			}

			m_az[chan] = (azim0 + azim1) / 2;

			if (azim1 - azim0 > Units::PI)
			{
				m_az[chan] -= Units::PI;
			}
			else if (azim1 - azim0 < -Units::PI)
			{
				m_az[chan] += Units::PI;
			}

			// Adjust the coarse value for the post-calculation correction to be applied after this returns
			// Note: this should only make a correction if there was also a correction from CalcModeAzimCorr above
			m_az[chan] += CalcPostAzimCorrection(chan);

			// Calculate confidence factor
			float temp = modesAmp[1] + modesAmp[2];
			m_conf[chan] = 0.5f * temp * temp / (modesAmp[1] * modesAmp[1] + modesAmp[2] * modesAmp[2]);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Do coarse DF using modes 2-3 algorithm
//
void CDf::CalcSymModes23(void)
{
	InitTwoPhase();
	InitThreePhase();

	for (size_t chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			static Ne10F32cVec modes(4);		// 2, -2, 3, -3
			static Ne10F32Vec modesAmp(4);
			static Ne10F32Vec modesPhase(4);

			// Calculate all specified modes
			ne10sDotProd(m_volts[chan], m_antVec2, modes[0]); // mode 2
			ne10sCrossCorr(m_antVec2, m_volts[chan], modes[1]); // mode -2
			ne10sDotProd(m_volts[chan], m_antVec3, modes[2]); // mode 3
			ne10sCrossCorr(m_antVec3, m_volts[chan], modes[3]); // mode -3
			ne10sCartToPolar(modes, modesAmp, modesPhase);

			// Compute specified azimuths
			float azim0 = modesPhase[2] - modesPhase[0] - Units::HALF_PI;
			float azim1 = modesPhase[1] - modesPhase[3] + Units::HALF_PI;

			if (azim0 < 0)
			{
				azim0 += Units::TWO_PI;
			}
			else if (azim0 >= Units::TWO_PI)
			{
				azim0 -= Units::TWO_PI;
			}

			if (azim1 < 0)
			{
				azim1 += Units::TWO_PI;
			}
			else if (azim1 >= Units::TWO_PI)
			{
				azim1 -= Units::TWO_PI;
			}

			m_az[chan] = (azim0 + azim1) / 2;

			if (azim1 - azim0 > Units::PI)
			{
				m_az[chan] -= Units::PI;
			}
			else if (azim1 - azim0 < -Units::PI)
			{
				m_az[chan] += Units::PI;
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Do coarse DF using modes algorithm with cf to resolve ambiguity
//
void CDf::CalcSymModesCf(bool inverted, bool phaseAdjust)
{
	for (size_t chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			static Ne10F32cVec modes(3);
			static Ne10F32Vec modesAmp(3);
			static Ne10F32Vec modesPhase(3);

			// Calculate specified modes
			modes[0] = m_volts[chan].sum(); // mode 0
			ne10sDotProd(m_volts[chan], m_antVec, modes[1]); // mode 1
			ne10sCrossCorr(m_antVec, m_volts[chan], modes[2]); // mode -1
			ne10sCartToPolar(modes, modesAmp, modesPhase);

			// Compute specified azimuths
			Units::Frequency freq = m_startFreq + chan * m_chanSize + m_chanSize / 2;
			float corrfac = CalcSymModesCorr(freq);
			float corr0 = (inverted ? modesPhase[0] - corrfac : modesPhase[0] + corrfac);
			float azim0 = modesPhase[1] - corr0;
			float azim1 = corr0 - modesPhase[2];

			if (azim0 < 0)
			{
				azim0 += Units::TWO_PI;
			}
			else if (azim0 >= Units::TWO_PI)
			{
				azim0 -= Units::TWO_PI;
			}

			if (azim1 < 0)
			{
				azim1 += Units::TWO_PI;
			}
			else if (azim1 >= Units::TWO_PI)
			{
				azim1 -= Units::TWO_PI;
			}

			float az = (azim0 + azim1) / 2;
			InitSymConf(chan);
			float cf1 = CalcSymConf(az, m_volts[chan], phaseAdjust, m_horiz);
			float cf2 = CalcSymConf(az + Units::PI, m_volts[chan], phaseAdjust, m_horiz);
			if (cf1 > cf2)
			{
				m_az[chan] = az;
				m_conf[chan] = cf1;
			}
			else
			{
				if (az < Units::PI)
					m_az[chan] = az + Units::PI;
				else
					m_az[chan] = az - Units::PI;
				m_conf[chan] = cf2;
			}
		}
	}

	return;
}

//////////////////////////////////////////////////////////////////////
//
// Get the mode0 correction
//
float CDf::CalcSymModesCorr(Units::Frequency freq) const
{
	if (m_symmetryDfParams->modesfactorMap.empty())
	{
		return Units::HALF_PI;
	}

	return CConfig::GetFreqDepValue(m_symmetryDfParams->modesfactorMap, freq) * Units::D2R;
}


//////////////////////////////////////////////////////////////////////
//
// Apply empirical offset correction for off-centered antenna patterns
//
void CDf::CalcSymOffsetCorrection(void)
{
	if (m_symmetryDfParams->offsetMap.empty())
	{
		return;
	}

	for (size_t chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			//  Note:  convert to radians in config
			Units::Frequency freq = m_startFreq + chan * m_chanSize + m_chanSize / 2;
			float offsetDeg = CConfig::GetFreqDepValue(m_symmetryDfParams->offsetMap, freq);
			m_az[chan] -= offsetDeg * Units::D2R;
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the symmetry radius
//
float CDf::CalcSymRadius(Units::Frequency freq)
{
	return CConfig::GetFreqDepValue(m_symmetryDfParams->radiusMap, freq);
}


////////////////////////////////////////////////////////////////////////////////
//
// Do DF using Dual Polarization WFA algorithm with Fourier pattern coefficients
//
void CDf::CalcSymWfa(unsigned char width, float uniqueConf, bool invModes)
{
	if (m_symmetryDfParams->wfaParams2.wfaType != CAntenna::SWfaParams2::DUAL_POL_FOURIER) return;		// Only supports dual-polarization fourier patterns

	unsigned long numFineSteps = (Units::DEG_PER_CIRCLE / (width * m_numAnts)) * m_numAnts * width;	// 360 for 9 antennas, 330 for 11, etc.
	InitDualPolWfaTone(numFineSteps);

	for (unsigned int chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			// Initialize frequency-dependent but azimuth-independent factors
			// Get the pattern data for this channel (conjugate cartesian scaled so that pattern vectors are unit vectors)
			Units::Frequency freq(m_startFreq + chan * m_chanSize + m_chanSize / 2);
			CAntenna::DualPolPattern dualPol;
			if (!m_config->GetDualPolFourierPattern(m_symmetryDfParams->wfaParams2, m_numAnts, freq, true, dualPol))
				continue;

			SDualPolVoltageTerms vModes;
			CalcDualPolVoltageTerms(m_volts[chan], dualPol, vModes);

			m_conf[chan] = 0;
			m_az[chan] = 0;

			// Coarse pattern search
			long maxAzIdx = 0;
			long kMax = numFineSteps / width;		// 72 for 9 element, 66 for 11 element, etc.
			float fineStep = static_cast<float>(Units::DEG_PER_CIRCLE / numFineSteps);	// 1.0 for 9 element, 1.09 for 11 element, etc.

			long azIndex = 0;
			long kLim = kMax;
			// Use the modes algorithm if required
			if (invModes)
			{
				float corrfac = CalcSymModesCorr(freq);
				float corr0 = atan2f(vModes.modes[2].i, vModes.modes[2].r) - corrfac;
				float azim0 = atan2f(vModes.modes[1].i, vModes.modes[1].r) - corr0 - CalcModeAzimCorr(freq);
				float azim1 = corr0 - atan2f(vModes.modes[3].i, vModes.modes[3].r);
				if (azim0 < 0) azim0 += Units::TWO_PI;
				else if (azim0 >= Units::TWO_PI) azim0 -= Units::TWO_PI;
				if (azim1 < 0) azim1 += Units::TWO_PI;
				else if (azim1 >= Units::TWO_PI) azim1 -= Units::TWO_PI;
				float az = (azim0 + azim1) / 2;
				if (azim1 - azim0 > Units::PI) az -= Units::PI;
				else if (azim1 - azim0 < -Units::PI) az += Units::PI;
				// Set up limits to search a semi-circle centered on az
				azIndex = static_cast<long>(az / Units::TWO_PI * numFineSteps);
				azIndex = (azIndex + 3 * numFineSteps / 4) % numFineSteps;
				kLim = kMax / 2;
			}
			for (long k = 0; k < kLim; ++k)
			{
				float conf = CalcDualPolWfaConf(azIndex* Units::D2R, vModes, dualPol);
				if (conf > m_conf[chan])
				{
					m_conf[chan] = conf;
					maxAzIdx = azIndex;
					if (conf > uniqueConf)
					{
						break;	// Found "good enough" peak
					}
				}
				azIndex = (azIndex + width) % numFineSteps;
			}
			m_az[chan] = maxAzIdx * fineStep * Units::D2R;

			// Perform fine pattern search
			// First - spiralling grid search in 1 degree steps from coarse value
			float c;
			float cMax = 0;
			long maxIndex = 0;
			for (int k = 0; k <= width; k = (k < 0 ? -k : -k - 1))
			{
				long idx = (k + maxAzIdx + numFineSteps) % numFineSteps;	// make sure arg is positive
				c = CalcDualPolWfaConf(idx* Units::D2R, vModes, dualPol);
				if (c > cMax)
				{
					cMax = c;
					maxIndex = idx;
					if (cMax > uniqueConf)
					{
						break;	// Found "good enough" peak
					}
				}
			}
			// Second - Use Newton's method to find peak
			unsigned iter = 0;

			if ( (c = cMax) != 0)	// Have a result from spiral search
			{
				float azim1 = maxIndex * fineStep * Units::D2R;
				DualPolWfaFineIteration(azim1, c, vModes, dualPol, iter);
				m_az[chan] = azim1;
				m_conf[chan] = c;
			}
			else	// No result from spiral search
			{
				m_conf[chan] = 0;
				m_az[chan] = 0;
			}
		}
	}

	if (m_symmetryDfParams->arrayOffset != 0)
	{
		m_az += Units::D2R * m_symmetryDfParams->arrayOffset;
	}
	// Normalize azimuths
	Normalize();

	return;
}


////////////////////////////////////////////////////////////////////////////////
//
// WFA coarse search
//
void CDf::CalcWfaCoarseSearch(unsigned int chan, _In_ const CAntenna::SWfaParams& wfaParams, int& azMax)
{
	int numAz = m_wfaConjPattern->size();
	int azStart = int(m_az[chan] * numAz / Units::TWO_PI + 0.5f);
	float trialCf;
	azMax = 0;

#ifdef CSMS_DEBUG
	int i = 0;
	int oldAzStart = azStart;
	for(; i <= numAz / 2; i = (i < 0 ? -i : -i - wfaParams.coarseAzStep))
#else
	for(int i = 0; i <= numAz / 2; i = (i < 0 ? -i : -i - wfaParams.coarseAzStep))
#endif
	{
		if ((trialCf = CalcWfaConf(chan, azStart + i)) > m_conf[chan])
		{
			m_conf[chan] = trialCf;
			azMax = azStart + i;

			if(trialCf > wfaParams.uniqueConf)
			{
				break;
			}
		}
	}

#ifdef CSMS_DEBUG
	if(s_traceWfa)
	{
		TRACE("CDf::CalcWfaCoarseSearch: chan = %u, i = %d, oldAzStart %d, azStart %d, azMax %d, trialCf %f, m_conf[chan] = %f, wfaParams.uniqueConf %f\n",
				chan, i, oldAzStart, azStart, azMax, trialCf, m_conf[chan],
				wfaParams.uniqueConf);
	}
#endif
	return;
}


////////////////////////////////////////////////////////////////////////////////
//
// WFA calculate confidence
//
float CDf::CalcWfaConf(unsigned int chan, int azIdx) const
{
	azIdx %= int(m_wfaConjPattern->size());

	if (azIdx < 0)
	{
		azIdx += m_wfaConjPattern->size();
	}

	ne10_fft_cpx_float32_t gamma;
	ne10sDotProd(m_volts[chan], (*m_wfaConjPattern)[azIdx], gamma);
	return gamma.r * gamma.r + gamma.i * gamma.i;
}


////////////////////////////////////////////////////////////////////////////////
//
// WFA fine search in 2D
//
void CDf::CalcWfaFineSearch2d(unsigned int chan, int azIdx) const
{
	float conf[3];
#ifdef CSMS_DEBUG
	int initIdx = azIdx;
#endif
	// Find the confidence values for the first set of three
	for (int i = 0; i < 3; ++i)
	{
		conf[i] = CalcWfaConf(chan, azIdx - 1 + i);
	}

	while(true)
	{
		if (conf[2] > conf[1] && conf[2] >= conf[0])
		{
			// Largest conf is last of three
			conf[0] = conf[1];
			conf[1] = conf[2];
			conf[2] = CalcWfaConf(chan, azIdx + 2);
			++azIdx;
		}
		else if (conf[0] > conf[1] && conf[0] >= conf[2])
		{
			// Largest conf is first of three
			conf[2] = conf[1];
			conf[1] = conf[0];
			conf[0] = CalcWfaConf(chan, azIdx - 2);
			--azIdx;
		}
		else
		{
			// Largest conf is center
			break;
		}
	}

	// Interpolate the final three points
	float maxX;
	CalcWfaMax1d(conf, maxX, m_conf[chan]);

	if (m_conf[chan] > 1.0)
	{
		m_conf[chan] = 1.0;
	}

	// Compute final azimuth
	m_az[chan] = (azIdx + maxX) * Units::TWO_PI / int(m_wfaConjPattern->size());
#ifdef CSMS_DEBUG
	if(s_traceWfa)
	{
		TRACE("CDf::CalcWfaFineSearch2d: chan = %u, conf[0] %f, conf[1] %f, conf[2] %f, azIdx %d, initIdx %d, m_az[chan] = %f, m_conf[0] =%f, m_conf[chan] =%f\n",
				chan, conf[0], conf[1], conf[2], azIdx, initIdx, m_az[chan], m_conf[0], m_conf[chan]);
	}
#endif

	return;
}


////////////////////////////////////////////////////////////////////////////////
//
// WFA find max using 1D parabolic interpolation
//
void CDf::CalcWfaMax1d(const float fx[3], float& maxX, float& maxFx)
{
	float denom = 2.0f * fx[1] - fx[0] - fx[2];
	maxX = 0.5f * (fx[2] - fx[0]) / denom;

	if(maxX < -1.0)
	{
		maxX = -1.0;
		maxFx = fx[0];
	}
	else if(maxX > 1.0)
	{
		maxX = 1.0;
		maxFx = fx[2];
	}
	else
	{
		maxFx = fx[1] + 0.5f * maxX * maxX * denom;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Calculate Watson-Watt confidence
//
float CDf::CalcWWConf(size_t chan, float az)
{
	float sinAzim = sinf(az);
	float cosAzim = cosf(az);
	float temp1 = m_volts[chan][1].r * cosAzim + m_volts[chan][0].r * sinAzim;
	float temp2 = m_volts[chan][1].i * cosAzim + m_volts[chan][0].i * sinAzim;

	return (temp1 * temp1 + temp2 * temp2) /
		(m_volts[chan][0].r * m_volts[chan][0].r + m_volts[chan][0].i * m_volts[chan][0].i +
		m_volts[chan][1].r * m_volts[chan][1].r + m_volts[chan][1].i * m_volts[chan][1].i);
}


//////////////////////////////////////////////////////////////////////
//
// Dual pol fine interation
//
void CDf::DualPolWfaFineIteration(float& azim1, float& c, const SDualPolVoltageTerms& vModes, const CAntenna::DualPolPattern& dualPol,
	unsigned int& iter, float* ampSame, float* ampOther)
{
	static const float TOL = 0.05f * Units::D2R;
	static const float DELTA_MAX = 5 * Units::D2R;
	for (iter = 0; iter < 10; ++iter)
	{
		float c1 = CalcDualPolWfaConf(azim1 - TOL, vModes, dualPol);
		float c2 = CalcDualPolWfaConf(azim1 + TOL, vModes, dualPol);
		float dc = (c2 - c1) / 2;
		float ddc = c2 - 2 * c + c1;
		float delta;

		if (ddc >= 0 && ddc <= 4 * FLT_EPSILON)
		{
			delta = TOL;
		}
		else if (ddc > 0)
		{
			delta = DELTA_MAX;
		}
		else
		{
			delta = fabsf(TOL * dc / ddc);

			if (delta > DELTA_MAX)
			{
				delta = DELTA_MAX;
			}
		}

		if (dc < 0)
		{
			delta = -delta;
		}

		azim1 += delta;
		c = CalcDualPolWfaConf(azim1, vModes, dualPol, ampSame, ampOther);

		if (fabsf(delta) < TOL)
		{
			break;
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get azimuth in 1/100th degree units with invalid flag
//
unsigned short CDf::GetCorrectedAz(unsigned int chan, bool hf, float confThresh) const
{
	if (m_conf[chan] >= confThresh)
	{
		unsigned int az = static_cast<unsigned int>((m_az[chan] * Units::R2D + m_navigation->GetTrueHeading(hf, 0)) * 100 + 0.5f);

		if (az >= Units::DEG_PER_CIRCLE * 100)
		{
			az -= Units::DEG_PER_CIRCLE * 100;
		}

		return static_cast<unsigned short>(az);
	}
	else
	{
		return 0xffff;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the reference phase for the Watson-Watt df
//
bool CDf::GetWWRefPhase(Units::Frequency freq, const CAntenna::SWatsonWattParams* params, float& sinRef, float& cosRef) const
{
	if (params == nullptr) return false;

	// Find the known phase difference of ref wrt loops at this freq
	size_t idx;
	for (idx = 0; idx < params->phase.size() - 1 && freq > params->phaseFreq[idx + 1]; ++idx );
	unsigned long lowFreq = params->phaseFreq[idx];
	unsigned long highFreq;
	short phaseLowFreq = params->phase[idx];
	short phaseHighFreq;

	if (idx == params->phase.size() - 1)
	{
		//Last entry makes high = low.
		highFreq = lowFreq;
		phaseHighFreq = phaseLowFreq;
	}
	else
	{
		highFreq = params->phaseFreq[idx + 1];
		phaseHighFreq = params->phase[idx + 1];
	}

	// Adjust for phases which wrap
	if ((phaseLowFreq - phaseHighFreq) > 180)
	{
		phaseHighFreq += 360;
	}
	else if ((phaseLowFreq - phaseHighFreq) < -180)
	{
		phaseLowFreq += 360;
	}

	// Interpolate the phase difference
	float refPhase = Units::D2R *
		(phaseLowFreq + (phaseHighFreq - phaseLowFreq) * (freq.Hz<float>() - lowFreq) / (highFreq - lowFreq));
	sinRef = sinf(refPhase);
	cosRef = cosf(refPhase);
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Initialize twiddle factors for dual-pol fourier coeff coarse calc
//
void CDf::InitDualPolWfaTone(_In_ unsigned long numFineSteps)
{
	if (numFineSteps != m_fineTwid.size())
	{
		m_fineTwid.resize(numFineSteps);

		float phase = 0;
		ne10sTone_Direct(m_fineTwid, 1, 1.0f / numFineSteps, phase);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Initialize azimuth-independent features for confidence calculation.
//
void CDf::InitSymConf(size_t chan)
{
	Units::Frequency freq = m_startFreq + chan * m_chanSize + m_chanSize / 2;
	m_confFact = Units::TWO_PI * CalcSymRadius(freq) * freq.Hz<float>() / LIGHTSPEED;
	ne10sPowerSpectr(m_volts[chan], m_measAmp);
	float sumX = m_measAmp.sum();
	m_confNorm = 1 / (sumX * sumX);
	ne10sSqrt(m_measAmp);
	CDfCore::InitSymConf(m_confFact, m_confNorm, m_measAmp);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Initialize two-theta phase vector and cache
//
void CDf::InitTwoPhase(void)
{
	if (m_numAnts != m_antVec2.size())
	{
		m_antVec2.resize(int(m_numAnts));

		if (m_numAnts == 2)
		{
			m_antVec2[0].r = 1;
			m_antVec2[0].i = 0;
			m_antVec2[1].r = 1;
			m_antVec2[1].i = 0;
		}
		else
		{
			float phase = 0;
			ne10sTone_Direct(m_antVec2, 1, 2.0f / m_numAnts, phase);
		}
	}
	return;
}

//////////////////////////////////////////////////////////////////////
//
// Initialize three-theta phase vector and cache
//
void CDf::InitThreePhase(void)
{
	if (m_numAnts != m_antVec3.size())
	{
		m_antVec3.resize(int(m_numAnts));

		ASSERT(m_numAnts >= 3);		// Should this throw something?

		if (m_numAnts == 3)
		{
			// Antennas must be at 0, 120, 240 and 3*theta % 360 = 0
			m_antVec3[0].r = 1;
			m_antVec3[0].i = 0;
			m_antVec3[1].r = 1;
			m_antVec3[1].i = 0;
			m_antVec3[2].r = 1;
			m_antVec3[2].i = 0;
		}
		else
		{
			float phase = 0;
			ne10sTone_Direct(m_antVec3, 1, 3.0f / m_numAnts, phase);
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Normalize azimuth
//
void CDf::Normalize(void)
{
	// Normalize
	for (unsigned int chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			while (m_az[chan] < 0)
			{
				m_az[chan] += Units::TWO_PI;
			}

			while (m_az[chan] >= Units::TWO_PI)
			{
				m_az[chan] -= Units::TWO_PI;
			}
		}
	}

	return;
}


#ifdef CDF_SIMULATION
//////////////////////////////////////////////////////////////////////
//
// Simulation - should be removed
//
void CDf::SimulateDfInterferometry(void)
{
	if ((m_numAnts & 1) == 0) return;	// don't do even cases for now

	size_t numAnts = ((m_numAnts & 1) != 0 ? m_numAnts - 1 : m_numAnts);	// if odd, first one is reference
	// numAnts is always even
	// choose an azimuth (20 degrees for now) and elevation (15 degrees for now)
	float az = 20.f * Units::D2R;
	float el = 15.f * Units::D2R;
	az = Units::HALF_PI - az;	// Correct for cartesian vs geographic coords

	float k1 = cosf(az) * cosf(el);
	float k2 = sinf(az) * cosf(el);
	float k3 = sinf(el);
	for (unsigned int chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			auto wl_inv = (m_startFreq + chan * m_chanSize + m_chanSize / 2).Hz<float>() * Units::TWO_PI / LIGHTSPEED;
			m_volts[chan][0] = { rand() / float(RAND_MAX), rand() / float(RAND_MAX) };
			for (size_t i = 0; i < numAnts; ++i)
			{
				float ph = wl_inv * (
					k1 * (m_mutableConfig.hfConfig.antCoords[i + 1].x - m_mutableConfig.hfConfig.antCoords[0].x) +
					k2 * (m_mutableConfig.hfConfig.antCoords[i + 1].y - m_mutableConfig.hfConfig.antCoords[0].y) +
					k3 * (m_mutableConfig.hfConfig.antCoords[i + 1].z - m_mutableConfig.hfConfig.antCoords[0].z));
				ne10_fft_cpx_float32_t expphase = { cosf(ph), sinf(ph) };
				m_volts[chan][i + 1] = m_volts[chan][0] * expphase;
			}
		}
	}
	return;
}

void CDf::SimulateDfWatsonWatt(const CAntenna::SWatsonWattParams* params)
{
	if (m_numAnts != 3) return;	// don't do standard case
	if (params == nullptr) return;

	// choose an azimuth (20 degrees for now)
	float az = 20.f * Units::D2R;
	az -= Units::HALF_PI / 2;	// Correct for vehicle reference
	float sinAz = sinf(az);
	float cosAz = cosf(az);
	for (unsigned int chan = 0; chan < m_numChans; ++chan)
	{
		if (m_conf[chan] != -1)
		{
			Units::Frequency freq = m_startFreq + chan * m_chanSize + m_chanSize / 2;
			float sinRef;
			float cosRef;
			if (GetWWRefPhase(freq, params, sinRef, cosRef))
			{
				m_volts[chan][0] = { rand() / float(RAND_MAX), rand() / float(RAND_MAX) };
				float a = rand() / float(RAND_MAX);
				ne10_fft_cpx_float32_t expRef = { cosRef, sinRef };
				ne10_fft_cpx_float32_t ref1 = expRef * (sinAz * a);
				ne10_fft_cpx_float32_t ref2 = expRef * (cosAz * a);
				m_volts[chan][1] = m_volts[chan][0] * ref1;
				m_volts[chan][2] = m_volts[chan][0] * ref2;
			}
		}
	}
	return;
}
#endif
#undef CDF_SIMULATION

