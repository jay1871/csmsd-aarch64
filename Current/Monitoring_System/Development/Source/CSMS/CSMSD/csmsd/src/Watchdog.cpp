/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2014-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"

#include <linux/watchdog.h>
#include "Failure.h"
#include "Log.h"
#include "Utility.h"
#include "Watchdog.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CWatchdog::CWatchdog() :
	m_fileDesc(-1),
	m_lastOkTime(0),
	m_nextHandle(0),
	m_pingSourceMap(),
	m_stopThread(false),
	m_useWatchdog(false),
	m_watchdogLog(std::string(TCIPaths::logDir) + "Watchdog")	// "/media/tci/csms/log/Watchdog")
{
	m_status.watchdogEnabled = false;

	// Check the hardware watchdog and disable it
	m_fileDesc = open(watchdogDevice, O_RDWR);
	if (m_fileDesc == -1)
	{
		CLog::Log(CLog::ERRORS, "open watchdogDevice failed: %s - not using watchdog", strerror(errno));
	}
	else
	{
		// Get last boot status
		int bootstatus;
		if (ioctl(m_fileDesc, WDIOC_GETBOOTSTATUS, &bootstatus) == 0)
		{
			CLog::Log(CLog::INFORMATION, "Last boot caused by %s", (bootstatus != 0 ? "Watchdog" : "Power-On Reset"));
		}
		else
		{
			CLog::Log(CLog::INFORMATION, "Cannot read watchdog status");
		}

		// Get some timeout info and set the new value
		int timeout;
		if (ioctl(m_fileDesc, WDIOC_GETTIMEOUT, &timeout) == 0)
		{
			CLog::Log(CLog::INFORMATION, "Watchdog timeout (initial): %u", timeout);
		}
		else
		{
			CLog::Log(CLog::INFORMATION, "Cannot get watchdog timeout");
		}
		timeout = 35;
		if (ioctl(m_fileDesc, WDIOC_SETTIMEOUT, &timeout) != 0)
		{
			CLog::Log(CLog::INFORMATION, "Cannot set watchdog timeout");
		}
		if (ioctl(m_fileDesc, WDIOC_GETTIMEOUT, &timeout) == 0)
		{
			CLog::Log(CLog::INFORMATION, "Watchdog timeout (final): %u", timeout);
		}
		else
		{
			CLog::Log(CLog::INFORMATION, "Cannot get watchdog timeout");
		}
		// Disable watchdog now
		int flags = WDIOS_DISABLECARD;
		ioctl(m_fileDesc, WDIOC_SETOPTIONS, &flags);

		write(m_fileDesc, "V", 1);
		close(m_fileDesc);
		m_fileDesc = -1;
		printf("watchdog has been disabled\n");

		if (!m_config->GetMutableConfig().miscConfig.disableWatchdog)
		{
			m_useWatchdog = !IsDebuggerPresent();	// For both debug and release builds
		}

		if (m_useWatchdog)
		{
			CLog::Log(CLog::INFORMATION, "Using watchdog");
		}
	}

	// Check logfile for previous failure
	std::ifstream file(m_watchdogLog.c_str());
	if (file)
	{
		std::string line;
		size_t count = 0;

		while(!std::getline(file, line).eof())
		{
			CLog::Log(CLog::ERRORS, "Rebooted from watchdog reset caused by %s", line.c_str());
			++count;
		}

		if (count >= MAX_WATCHDOG_REPORTS)
		{
			CLog::Log(CLog::ERRORS, "Too many watchdog reboots - disabling watchdog");;
			m_useWatchdog = false;
		}

		file.close();
		remove(m_watchdogLog.c_str());
	}

	return;
}

//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CWatchdog::~CWatchdog()
{
	TRACE("CWatchdog dtor 1\n");
	if (!m_pingSourceMap.empty())
	{
		Stop();
	}

	TRACE("CWatchdog dtor 2\n");
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Check if running under the debugger
//
bool CWatchdog::IsDebuggerPresent(void)
{
    auto fd = open("/proc/self/status", O_RDONLY);
    if (fd == -1)
        return false;

    char buf[1024];
	auto numRead = read(fd, buf, sizeof(buf));
    close(fd);

	if (numRead <= 0)
		return false;

	buf[numRead] = 0;
	auto tracerPid = strstr(buf, "TracerPid:");
	if (tracerPid == nullptr)
		return false;

	auto isDebugger = atoi(tracerPid + 10);
	return isDebugger != 0;
}


//////////////////////////////////////////////////////////////////////
//
// Ping the watchdog to keep the system alive
//
void CWatchdog::Ping(int source)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	m_pingSourceMap[source].pinged = true;
	DATE now = Utility::CurrentTimeAsDATE();
//	TciGps::GetDATE(now);

	for (PingSourceMap::const_iterator pingSource = m_pingSourceMap.begin(); pingSource != m_pingSourceMap.end(); ++pingSource)
	{
		if (!pingSource->second.pinged)
		{
			// Not all sources have pinged
			return;
		}
	}

	if (m_useWatchdog)
	{
		// Ping the hardware watchdog
		assert(m_fileDesc != -1);
		if (m_fileDesc != -1)
		{
			if (ioctl(m_fileDesc, WDIOC_KEEPALIVE, nullptr) != 0)
			{
				printf("WDIOC_KEEPALIVE failed\n");
			}
		}
	}

	// Remember time
	m_lastOkTime = now;

	// Reset for next round
	bool preTriggered = false;

	for (PingSourceMap::iterator pingSource = m_pingSourceMap.begin(); pingSource != m_pingSourceMap.end(); ++pingSource)
	{
		pingSource->second.pinged = false;

		if (pingSource->second.preTriggered)
		{
			preTriggered = true;
			pingSource->second.preTriggered = false;
		}
	}

	if (preTriggered)
	{
		CLog::Log(CLog::INFORMATION, "The pending watchdog trigger has been reset");

		if (m_useWatchdog)
		{
			// Delete the log file
			remove(m_watchdogLog.c_str());
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Returns a handle for a ping source and makes it active
//
int CWatchdog::RegisterPingSource(const std::string& name)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	int handle = ++m_nextHandle;
	SPingSourceValue value = { false, false, name };
	m_pingSourceMap[handle] = value;

	if (m_pingSourceMap.size() == 1)
	{
//		TciGps::GetDATE(const_cast<DATE&>(m_lastOkTime));
		m_lastOkTime = Utility::CurrentTimeAsDATE();

		// Start the timeout thread
		m_thread = std::thread(&CWatchdog::TimeoutThread, this);

		if (m_useWatchdog)
		{
			if (m_fileDesc == -1)
			{
				m_fileDesc = open(watchdogDevice, O_RDWR);
				if (m_fileDesc == -1)
				{
					CLog::Log(CLog::ERRORS, "open watchdogDevice failed: %s", strerror(errno));	// Shouldn't be possible here.
				}
				else
				{
					CLog::Log(CLog::INFORMATION, "Watchdog opened");
				}
			}
			if (m_fileDesc != -1)
			{
				int flags = WDIOS_ENABLECARD;
				if (ioctl(m_fileDesc, WDIOC_SETOPTIONS, &flags) == 0)
				{
					m_status.watchdogEnabled = true;
				}
				else
				{
					CLog::Log(CLog::ERRORS, "Unable to enable watchdog");
				}
			}
		}
	}

	return handle;
}


//////////////////////////////////////////////////////////////////////
//
// Stop the watchdog and timeout thread
//
void CWatchdog::Stop(void)
{
	if (m_useWatchdog)
	{
		m_status.watchdogEnabled = false;

		if (m_fileDesc != -1)
		{
			int flags = WDIOS_DISABLECARD;
			ioctl(m_fileDesc, WDIOC_SETOPTIONS, &flags);

			write(m_fileDesc, "V", 1);
			close(m_fileDesc);
			CLog::Log(CLog::INFORMATION, "Watchdog closed");
		}
	}

	{
		std::lock_guard<std::mutex> lock(m_stopThreadMutex);
		m_stopThread = true;
	}

	m_stopThreadCond.notify_all();
	if (m_thread.joinable())
	{
		m_thread.join();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Writes failure information to NVRAM and log when pings stop before the hardware watchdog triggers
//
void CWatchdog::TimeoutThread(void)
{
	try
	{
		// Run until stopped
		auto timeout = std::chrono::milliseconds{0};

		auto Polling = [this, &timeout]
		{
			std::unique_lock<std::mutex> lock(m_stopThreadMutex);
			return !m_stopThreadCond.wait_for(lock, timeout, [this] { return m_stopThread; });
		};

		while(Polling())
		{
			DATE now = Utility::CurrentTimeAsDATE();
//			TciGps::GetDATE(now);

			std::lock_guard<std::mutex> lock(m_mutex);

			if (now - m_lastOkTime >= FAIL_INTERVAL)
			{
				timeout = std::chrono::milliseconds(static_cast<long long int>(FAIL_INTERVAL * Units::MILLISECONDS_PER_DAY / 2));

				for (PingSourceMap::iterator pingSource = m_pingSourceMap.begin(); pingSource != m_pingSourceMap.end(); ++pingSource)
				{
					if (!pingSource->second.pinged && !pingSource->second.preTriggered)
					{
						if (m_useWatchdog)
						{
							// Record the failure in log file
							std::ofstream file(m_watchdogLog.c_str(), std::ios::app);
							if (file)
							{
								std::string log = pingSource->second.source + " at " + Utility::CurrentTimeAsString() + "\n";
								file.write(log.c_str(), log.length());
							}
						}

						// Log failure
						CLog::Log(CLog::ERRORS, "The watchdog has been triggered by %s", pingSource->second.source.c_str());
						pingSource->second.preTriggered = true;
					}
				}
			}
			else
			{
				timeout = std::chrono::milliseconds(static_cast<long long int>((m_lastOkTime + FAIL_INTERVAL - now) * Units::MILLISECONDS_PER_DAY));
			}
		}
		printf("CWatchdog::TimeoutThread exiting\n");
	}
	catch(...)
	{
		CFailure::OnFail(std::current_exception());
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Clears a ping source
//
void CWatchdog::UnregisterPingSource(int source)
{
	Ping(source);	// Also takes lock on m_mutex

	std::lock_guard<std::mutex> lock(m_mutex);
	m_pingSourceMap.erase(source);

	if (m_pingSourceMap.empty())
	{
		Stop();
	}

	return;
}



