/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2015 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"

#include "AvdTask.h"
#include "Utility.h"


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CAvdTask::CAvdTask(_In_ const SEquipCtrlMsg* cmd, _In_opt_ CNetConnection<void>* source) try :
	CTask(cmd, source /*, source == nullptr ? BACKGROUND : IMMEDIATE*/), // Virtual base
	CMeasurementTask(nullptr, nullptr/*, source == nullptr ? BACKGROUND : IMMEDIATE*/),
	CSuspendableTask(),
	m_count(0),
	m_data(),
	m_firstAvdBand(0),
	m_measurement(m_data.end()),
	m_noise(),
	m_numLooks(0),
	m_prevNumLooks(0),
	m_rate(cmd->body.getAutoViolateCmd.measurementRate),
	m_resetDwell(true),
	m_snr(pow(10.0f, cmd->body.getAutoViolateCmd.avdThreshold / 10.0f)),
	m_watts()
{
	CMeasurementTask::m_snr = m_snr;
	m_report.body.avdMeasureResult.occHdr.numTimeOfDays = 0;

	return;
}
catch(std::bad_alloc)
{
	throw ErrorCodes::AVDTOOMANYCHANS;
}


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CAvdTask::CAvdTask(_In_ SRestartData& restartData) :
	CTask(restartData), // CTask is a virtual base
	CMeasurementTask(restartData),
	CSuspendableTask(restartData),
	m_count(0),
	m_noise(),
	m_resetDwell(true),
	m_watts()
{
	// Read from blob into member data
	ReadData(restartData, m_firstAvdBand);
	ReadData(restartData, m_numLooks);
	ReadData(restartData, m_prevNumLooks);
	ReadData(restartData, m_rate);
	ReadData(restartData, m_snr);
	ReadData(restartData, m_data);
//	unsigned int checksum1 = m_checksum;
//	unsigned int checksum2;
//	ReadData(restartData, checksum2);
//
//	if(checksum1 != checksum2)
//	{
//		throw ErrorCodes::UNABLETOGETDATA;
//	}

	m_report.body.avdMeasureResult.occHdr.numTimeOfDays = 0;

	if (m_firstAvdBand == 1)
	{
		// Remove measurement step
		m_taskParams.erase(m_taskParams.begin());
		m_procParams.erase(m_procParams.begin());
		m_firstAvdBand = 0;
		--m_firstStep;
		--m_lastStep;
	}

	m_measurement = m_data.end();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CAvdTask::~CAvdTask(void)
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Abandon the current dwell so task can yield immediately
//
void CAvdTask::AbandonDwell(void)
{
	// Base classes
	CTask::AbandonDwell();
	CMeasurementTask::AbandonDwell();

	// Member data
	m_resetDwell = true;
}


//////////////////////////////////////////////////////////////////////
//
// Add a block of data
//
void CAvdTask::AddBlock(_In_ const Ne10F32Vec& watts, bool partialBlock)
{
	if (m_resetDwell)
	{
		m_watts.resize(watts.size());
		m_watts = 0;
		m_count = 0;
		m_resetDwell = false;
	}
	else
	{
		ASSERT(m_watts.size() == watts.size());
	}

	m_watts += watts;

	if (!partialBlock)
	{
		++m_count;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Create and register task from message and return shared pointer
//
CAvdTask::AvdTask CAvdTask::Create(_In_ const SEquipCtrlMsg* cmd, _In_opt_ CNetConnection<void>* source)
{
	AvdTask task(new CAvdTask(cmd, source), Deleter());
	task->RegisterTask(true);

	return task;
}


//////////////////////////////////////////////////////////////////////
//
// Create task from restart data
//
void CAvdTask::Create(_In_ SRestartData& restartData)
{
	RegisterTask(AvdTask(new CAvdTask(restartData), Deleter()));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback to fill in message data structure
//
SEquipCtrlMsg::SAvdMeasureResult::SMeasureData CAvdTask::GetBwData(_Inout_ SDataValue& value, size_t)
{
	SEquipCtrlMsg::SAvdMeasureResult::SMeasureData data;
	data.result = value.bw.GetMean();
	data.stdDev = float(value.bw.GetStdDev());

	return data;
}


//////////////////////////////////////////////////////////////////////
//
// Callback to fill in message data structure
//
SEquipCtrlMsg::SOccResult::SResultData CAvdTask::GetChannelOccData(_Inout_ SDataValue& value, size_t)
{
	if (m_numLooks > m_prevNumLooks)
	{
		float maxOcc = float((value.numAbove - value.prevNumAbove)) / (m_numLooks - m_prevNumLooks);
		ASSERT(maxOcc <= 1);

		if (maxOcc > value.maxOcc)
		{
			value.maxOcc = maxOcc;
		}
	}

	value.prevNumAbove = value.numAbove;
	SEquipCtrlMsg::SOccResult::SResultData data;
	ASSERT(value.numAbove <= m_numLooks);
	data.avg = (m_numLooks ? static_cast<signed char>(100 * double(value.numAbove) / m_numLooks + 0.5) : 0);
	data.max = static_cast<signed char>(100 * value.maxOcc + 0.5f);

	return data;
}


//////////////////////////////////////////////////////////////////////
//
// Get power at end of dwell and convert bins to channels
//
void CAvdTask::GetChanWatts(unsigned long binsPerChan, unsigned long numChan, Ne10F32Vec& wattsRT, float enbw)
{
	if (m_count > 0)
	{
		m_watts /= float(m_count);

		// Update noise estimate from m_watt before resize
		m_noise = CNoiseEstimate::Create(m_watts, numChan, 0, CNoiseEstimate::DEFAULT_SNR, binsPerChan, enbw);

		for (unsigned int chan = 0; chan < numChan; ++chan)
		{
			// Sum bin in channel - was max before 2/16/17
			ne10sSum(&m_watts[chan * binsPerChan + 1], binsPerChan - 2, &wattsRT[chan]); // Exclude guard bins
			// Sum bins in channel
//			ne10sSum(&m_watts[chan * binsPerChan + 1], binsPerChan - 2, &m_watts[chan]); // Exclude guard bins
		}
		m_watts = wattsRT;
		m_watts.resize(numChan);
		m_watts /= enbw;
		wattsRT /= enbw;	//SS 05/04/17
	}
	else
	{
		m_watts.resize(numChan);
		m_watts = 0;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback to fill in message data structure
//
SEquipCtrlMsg::SAvdMeasureResult::SMeasureData CAvdTask::GetFreqData(_Inout_ SDataValue& value, size_t)
{
	SEquipCtrlMsg::SAvdMeasureResult::SMeasureData data;
	data.result = value.freq.GetMean();
	data.stdDev = float(value.freq.GetStdDev());

	return data;
}


//////////////////////////////////////////////////////////////////////
//
// Save data for restart
//
void CAvdTask::SaveRestartData(_Inout_ std::vector<unsigned char>& data) const
{
	// Base classes
	CTask::SaveRestartData(data);
	CMeasurementTask::SaveRestartData(data);
	CSuspendableTask::SaveRestartData(data);

	// Member data
	SaveData(data, m_firstAvdBand);
	SaveData(data, m_numLooks);
	SaveData(data, m_prevNumLooks);
	SaveData(data, m_rate);
	SaveData(data, m_snr);
	SaveData(data, m_data);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Report interim or final results
//
void CAvdTask::SendReports(bool last)
{
	m_report.body.occResult.occHdr.status = ErrorCodes::SUCCESS;
	CSuspendableTask::SendReports(SEquipCtrlMsg::OCC_CHANNEL_RESULT, _T("OCC_CHANNEL_RESULT"),
		m_report.body.occResult, m_report.body.occResult.resultData, m_data, &CAvdTask::GetChannelOccData, last);
	m_report.body.avdMeasureResult.occHdr.status = ErrorCodes::SUCCESS;
	CSuspendableTask::SendReports(SEquipCtrlMsg::AUTOVIOLATE_BWMEAS_RESULT, _T("AUTOVIOLATE_BWMEAS_RESULT"),
		m_report.body.avdMeasureResult, m_report.body.avdMeasureResult.measData, m_data, &CAvdTask::GetBwData, last);
	m_report.body.avdMeasureResult.occHdr.status = ErrorCodes::SUCCESS;
	CSuspendableTask::SendReports(SEquipCtrlMsg::AUTOVIOLATE_FREQMEAS_RESULT, _T("AUTOVIOLATE_FREQMEAS_RESULT"),
		m_report.body.avdMeasureResult, m_report.body.avdMeasureResult.measData, m_data, &CAvdTask::GetFreqData, last);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update signal state
//
void CAvdTask::UpdateBlock(size_t band, size_t block, Units::Frequency firstChanFreq)
{
	if (band == m_firstAvdBand && block == 0)
	{
		++m_numLooks;
	}

	size_t chanOffset = size_t(block * GetProcBw(band) / m_taskParams[band].bw);

	for (size_t blockChan = 0; blockChan < m_watts.size(); ++blockChan)
	{
		if (m_watts[blockChan] > m_noise->GetNoise(blockChan) * m_snr &&
			!Excluded(firstChanFreq + blockChan * m_taskParams[band].bw, m_taskParams[band].bw, m_taskParams[band].pol))
		{
//			TRACE("UP: %u %u %.6g %.6g %.6g\n", blockChan, chanOffset, m_watts[blockChan], m_noise->GetNoise(blockChan), m_snr);
			// Signal is up
			SDataKey key = { band - m_firstAvdBand, chanOffset + blockChan};
			++m_data[key].numAbove;
			m_data[key].up = true;
		}
	}

	// Reset for next dwell
	m_resetDwell = true;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update the bandwidth result
//
void CAvdTask::UpdateBw(double bw)
{
	m_measurement->second.bw.AddValue(bw);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update the frequency result
//
void CAvdTask::UpdateFreq(double freq)
{
	m_measurement->second.freq.AddValue(freq);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update the task list
//
void CAvdTask::UpdateTask(void)
{
	// Find next measurement to do
	DATE now = Utility::CurrentTimeAsDATE();
//	TciGps::GetDATE(now);
	DataMap::iterator meas = m_data.end();
	DataMap::iterator nextMeasurement = m_data.end();

	if (m_data.size() > 0)
	{
		if (m_measurement == m_data.end())
		{
			m_measurement = m_data.begin();
		}

		meas = m_measurement;

		do
		{
			// Next signal
			if (++meas == m_data.end())
			{
				meas = m_data.begin();
			}

			// Is it active?
			if (meas->second.up)
			{
				if (meas->second.lastTime == 0)
				{
					// New signal or not AUTO mode
//					TRACE("Set nextMeasurement chan=%u, lastTime == 0\n", meas->first.chan);
					nextMeasurement = meas;
					break;
				}
				else if (nextMeasurement == m_data.end() && now - meas->second.lastTime >= meas->second.threshTime / Units::SECONDS_PER_DAY)
				{
					// Known signal
//					TRACE("Set nextMeasurement chan=%u, now=%f lastTime=%f thresh=%f\n",
//						meas->first.chan, now, meas->second.lastTime, meas->second.threshTime / Units::SECONDS_PER_DAY);
					nextMeasurement = meas;
				}
			}
		}
		while (meas != m_measurement);

		if (nextMeasurement != m_data.end())
		{
			m_measurement = nextMeasurement;

			if (m_rate == SEquipCtrlMsg::AUTO)
			{
				// Update autoParams for this channel, unless first time
				if (m_measurement->second.lastTime > 0)
				{
					// Not first time
					const CConfig::SAvdMeasParams& params = m_config->GetAvdMeasParams();
					m_measurement->second.deltaTime = params.autorateMu * m_measurement->second.deltaTime +
						(1 - params.autorateMu) * (now - m_measurement->second.lastTime) * Units::SECONDS_PER_DAY;
					m_measurement->second.threshTime += params.autorateK * (params.autorateGoal - m_measurement->second.deltaTime);

					if (m_measurement->second.threshTime > params.autorateMaxTime)
					{
						m_measurement->second.threshTime = params.autorateMaxTime;
					}
					else if (m_measurement->second.threshTime < 0)
					{
						m_measurement->second.threshTime = 0;
					}
				}

				m_measurement->second.lastTime = now;
			}

			// Create params for a measurement step
			const CConfig::STaskParams& avdTaskParams = m_taskParams[m_measurement->first.band + m_firstAvdBand];
			Units::Frequency freq = avdTaskParams.startFreq + m_measurement->first.chan * avdTaskParams.bw + avdTaskParams.bw / 2;
			TRACE(_T("AVD Measurement @ %.6f MHz delta = %.0f thresh = %.0f\n"), freq.Hz<double>() / 1e6,
				m_measurement->second.deltaTime, m_measurement->second.threshTime);
			CConfig::STaskParams taskParams;
			taskParams.bw = avdTaskParams.bw;
			taskParams.pol = avdTaskParams.pol;
			taskParams.hf = ValidateFrequency(freq, taskParams.bw, taskParams.pol != CConfig::EPolarization::VERT, false, m_ant);
			taskParams.mode = CConfig::EMode::MEASURE;
			Units::Frequency rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, true);

			if (rxHwBw < avdTaskParams.bw)
			{
				rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, false);
			}

			taskParams.rxHwBw = rxHwBw;
			taskParams.startFreq = taskParams.stopFreq = freq;
			CConfig::SProcParams procParams = m_config->GetProcParams(taskParams);
			float blockTime = 1000 * m_config->GetDwellTime(procParams); // ms
			m_bwDwellCount = static_cast<unsigned int>(ceil(m_cmd.bwCmd.dwellTime / blockTime));
			m_freqDwellCount = static_cast<unsigned int>(ceil(m_cmd.freqCmd.dwellTime / blockTime));
			taskParams.numBlocks = std::max(m_bwDwellCount * static_cast<unsigned int>(m_cmd.bwCmd.repeatCount),
				m_freqDwellCount * static_cast<unsigned int>(m_cmd.freqCmd.repeatCount));

			taskParams.withIq = (m_cmd.freqCmd.freqMethod == SEquipCtrlMsg::SGetFreqCmd::EFreqMethod::IFM);
			taskParams.withPsd = true;
			taskParams.withFft = false;
			if (m_firstAvdBand == 1)
			{
				// Replace measurement step
				m_taskParams[0] = taskParams;
				m_procParams[0] = procParams;
			}
			else
			{
				// Insert measurement step
				m_taskParams.insert(m_taskParams.begin(), taskParams);
				m_procParams.insert(m_procParams.begin(), procParams);
				m_firstAvdBand = 1;
				++m_firstStep;
				++m_lastStep;
			}
		}
		else if (m_firstAvdBand == 1)
		{
			// Remove measurement step
			m_taskParams.erase(m_taskParams.begin());
			m_procParams.erase(m_procParams.begin());
			m_firstAvdBand = 0;
			--m_firstStep;
			--m_lastStep;
		}

	}

	// Clean up flags
	for (DataMap::iterator data = m_data.begin(); data != m_data.end(); ++data)
	{
		data->second.up = false;
	}

	m_currentStep = 0;
	m_currentBlock = 0;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate a command
//
void CAvdTask::Validate(_In_ const SEquipCtrlMsg::SGetAutoViolateCmd& cmd)
{
	size_t bodySize = offsetof(SEquipCtrlMsg::SGetAutoViolateCmd, band[cmd.numBands]);
	std::shared_ptr<SEquipCtrlMsg> msg(new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg);

	msg->hdr.msgType = SEquipCtrlMsg::AUTOVIOLATE_CTRL;
	msg->hdr.msgSubType = SEquipCtrlMsg::VALIDATE_AUTOVIOLATE;
	msg->hdr.bodySize = static_cast<unsigned long>(bodySize);
	memcpy(&msg->body.getAutoViolateCmd, &cmd, bodySize);
	CAvdTask(msg.get(), nullptr); // Will throw on error

	return;
}

