/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2014-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"

#include "Bist.h"
#include "DfCtrlMsg.h"
#include "DfCtrlNet.h"
#include "EquipControl.h"
#include "EquipCtrlMsg.h"
#include "PersistentData.h"
#include "ProductVersion.h"
#include "Task.h"
#include "Utility.h"
#include "Watchdog.h"

using namespace BistString;

//////////////////////////////////////////////////////////////////////
//
// Statics
//
//constexpr const char* const* CBist::IDS_3230_TEMPS[];
//constexpr const char* const* CBist::IDS_3230_VOLTAGES[];
SSmsMsg::SGetBistResp::EResult CBist:: m_OverallResult;
//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CBist::CBist(void) :
	m_bistSummary(SEquipCtrlMsg::PASS_STAT),
	m_config(),
	m_digitizer(),
	m_mutableConfig(m_config->GetMutableConfig()),
	m_numSlaves(0),
	m_pbCal(),
	m_radioEquip(),
	m_switch(),
	m_watchdog()
{
	// Size m_bistResults for all nodes plus df, if any
	size_t resultSize = 1;
	if (m_config->IsProcessorDfMaster())
	{
		// df master
		resultSize = 3;	// master + othernodes + df, 0 = this box, 1 = nodes, 3 = df
	}
	m_bistResults.resize(resultSize);
	auto& bistResults = m_bistResults[0];	// This box
	bistResults.resize(2);		// this box

	bistResults[0].result = SSmsMsg::SGetBistResp::PASS;
	bistResults[0].test = std::u16string(u"Built-In Self Test");
	bistResults[0].text = std::u16string(u"Diagnostics never run");
	bistResults[0].last = false;

	bistResults[1].result = SSmsMsg::SGetBistResp::PASS;
	bistResults[1].test = std::u16string(u"Built-In Self Test");
	bistResults[1].text = std::u16string(u"");
	bistResults[1].last = true;
	m_senddetail = true;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CBist::~CBist(void)
{
	TRACE("CBist dtor\n");
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test
//
void CBist::DoBist(_In_ const CTask::Task& task)
{
	CLockGuard lock(m_critSect);
	for (size_t i = 0; i < m_bistResults.size(); ++i)
	{
		m_bistResults[i].clear();
	}
	SSmsMsg::SGetBistResp::EResult result = SSmsMsg::SGetBistResp::PASS;
	//SSmsMsg::SGetBistResp::EResult result = SSmsMsg::SGetBistResp::FAIL; // debug only
	m_OverallResult = result;
	m_bistSummary = SEquipCtrlMsg::BIST_STAT;

	//	Initiate BIST on the remote nodes, if any
	std::vector<size_t> bistSentIndex;
	if (m_config->IsProcessorDfMaster())
	{
		// This is a df system and this is the master
		CSingleton<CProcessorNode> processorNode;
		// Send a message to the remote nodes
		m_numSlaves = processorNode->SendBistToAllNodes(bistSentIndex);
		CLog::Log(CLog::INFORMATION, "sent DO_BIST to %d nodes", m_numSlaves);
	}

	// Check system info
	UpdateOverallResult(DoBistSystem(task), result);

	// Check Processor nodes
	UpdateOverallResult(DoBistProcessorNodes(task), result);

	// Check license and restricted frequencies
	UpdateOverallResult(DoBistLicense(task), result);
	UpdateOverallResult(DoBistRestrictedFreq(task), result);

	if (m_config->IsProcessorMaster())	// only in master
	{
		if (!m_config->IsDDRSystem())  // database only instantiated by metrics class
		{
			// Check database
			UpdateOverallResult(DoBistDatabase(task), result);
		}
	}
	// Check digital board
	UpdateOverallResult(DoBist3230(task), result);

	// Check Watchdog
	UpdateOverallResult(DoBistWatchdog(task), result);

	if (Utility::IsQuitting())
	{
		return;
	}

	// Check RF board
	UpdateOverallResult(DoBist2630(task), result);

	// skip slave reporting of antenna info
	if (m_config->IsProcessorMaster())
	{
		// Check antennas (RF inputs)
		UpdateOverallResult(DoBistRfInputs(task), result);
	}

	if (Utility::IsQuitting())
	{
		return;
	}

	//Check PB Cal
	UpdateOverallResult(DoBistPbCal(task), result);

	if (Utility::IsQuitting())
	{
		return;
	}

	// Check Ntp
	UpdateOverallResult(DoBistNtp(task), result);

	if (m_config->IsProcessorMaster())	// only in master
	{
		// Check Gps
		UpdateOverallResult(DoBistGps(task), result);

		// Check compass
		UpdateOverallResult(DoBistCompass(task), result);

		if (Utility::IsQuitting())
		{
			return;
		}

		// Append the slave node BIST results
		if (m_config->IsProcessorDfMaster())
		{
			// This is a df system and this is the master
			CSingleton<CProcessorNode> masterNode;
			std::vector<SBistResult> allResults;
			const CConfig::SProcessorNodes & processorNodes = m_config->GetProcessorNodes();
			std::vector<size_t> bistAllIndex(processorNodes.nodes.size());
			for (size_t i = 0; i < processorNodes.nodes.size(); ++i)
			{
				bistAllIndex[i] = i;
			}
			for (size_t i = 0; i < 10; ++i)
			{
				if (masterNode->AreNodesBistDone(bistAllIndex, allResults))
				{
					printf("All nodes Bist done %u\n", allResults.size());
					break;
				}
				sleep(5);
			}
			for (size_t i = 0; i < allResults.size(); ++i)
			{
				SBistResult bistResult;
				bistResult.result = allResults[i].result;
				bistResult.test = allResults[i].test;
				bistResult.text = allResults[i].text;
				bistResult.last = allResults[i].last;
				bistResult.send = allResults[i].send;
				std::string logStr;
				auto& bistResults = m_bistResults[1];	// Other nodes
				bistResults.push_back(bistResult);
				RecordBistResult(task, false, bistResult.send, bistResult, logStr);
				if (bistResult.last)
				{
					UpdateOverallResult(allResults[i].result, result);
				}
			}

			// Run DF tests
			if (Utility::IsQuitting())
			{
				return;
			}

			UpdateOverallResult(DoBistDfSwitch(task), result);

			if (m_switch->C51432017::IsPresent() && m_switch->C7234201102::IsPresent())
			{
				UpdateOverallResult(DoBist72342011(task), result);
			}
			if (m_switch->C51432017::IsPresent() && m_switch->C72362001::IsPresent())
			{
				UpdateOverallResult(DoBist72362001(task), result);
			}

			if (CAntenna::IsDfAntenna(m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get()))
			{
				UpdateOverallResult(DoBistUshfAntenna(task), result);
			}
			if (CAntenna::IsDfAntenna(m_mutableConfig.hfConfig.antCable.antenna.get()))
			{
				UpdateOverallResult(DoBistHfAntenna(task), result);
			}

			UpdateOverallResult(DoBistDfPbCal(task), result);
		}

		if (m_config->HasShfExt())
		{
			UpdateOverallResult(DoBistShfExt(task), result);
		}
	}

	// Overall result
	RecordBistResult(task, true, true, IDS_BIST, result);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the 2630 Rf Board
//
SSmsMsg::SGetBistResp::EResult CBist::DoBist2630(_In_ const CTask::Task& task)
{
	m_senddetail = false;
	auto lang = m_config->GetLangIdx();

	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;

	auto result = SSmsMsg::SGetBistResp::WARNING;

	// Run 2630 internal bist
	S2630Msg::SBistResults bist2630;
	if (!m_radioEquip->RunBist(bist2630))
	{
		UpdateOverallResult(result, passed);
		RecordBistResult(task, false, true, IDS_RADIO, result, IDS_BIST_2630_INTERNAL_BIST_FAILED);
	}
	else
	{
		RecordBistResult(task, false, true, IDS_RADIO, SSmsMsg::SGetBistResp::PASS, IDS_BIST_FW_VERSION,
			"2630", Format(bist2630.firmwareVersion).c_str(), Format(bist2630.firmwareDate).c_str(), Format(bist2630.fpgaVersion).c_str());
		RecordBistResult(task, false, true, IDS_RADIO, SSmsMsg::SGetBistResp::PASS, IDS_BIST_2630_TABLE_VERSIONS,
			Format(bist2630.attenTableNormalVersion).c_str(), Format(bist2630.attenTableRuralVersion).c_str(),
			Format(bist2630.attenTableUrbanVersion).c_str(), Format(bist2630.attenTableCongestedVersion).c_str(),
			Format(bist2630.fpiTable10IbwVersion).c_str(), Format(bist2630.fpiTable80IbwVersion).c_str(),
			Format(bist2630.psTableVersion).c_str(), Format(bist2630.lo1TableVersion).c_str(),
			Format(bist2630.lo2TableVersion).c_str(), Format(bist2630.calGenTableVersion).c_str(),
			Format(bist2630.bistRssiTableVersion).c_str());
		RecordBistResult(task, false, true, IDS_RADIO, SSmsMsg::SGetBistResp::PASS, IDS_BIST_2630_CALTABLE_VERSIONS,
			Format(bist2630.rfCalTableNoLnaVersion).c_str(), Format(bist2630.rfCalTableLnaVersion).c_str(),
			Format(bist2630.ifCalTableVersion).c_str(), Format(bist2630.attenCalTableNormalVersion).c_str(),
			Format(bist2630.attenCalTableRuralVersion).c_str(), Format(bist2630.attenCalTableUrbanVersion).c_str(),
			Format(bist2630.attenCalTableCongestedVersion).c_str(), Format(bist2630.calGenDataTableVersion).c_str(),
			Format(bist2630.tempCompTableNoLnaVersion).c_str(), Format(bist2630.tempCompTableLnaVersion).c_str(),
			Format(bist2630.vtfTableVersion).c_str());

		std::string voltStr;
		result = SSmsMsg::SGetBistResp::PASS;
		for (unsigned int i = 0; i < std::extent<decltype(bist2630.voltages)>::value; ++i)
		{
			if (bist2630.voltages[i] < -100.f || bist2630.voltages[i] > 100.f)
			{
				result = SSmsMsg::SGetBistResp::WARNING;
				UpdateOverallResult(SSmsMsg::SGetBistResp::WARNING, passed);
			}
			voltStr += Format(bist2630.voltages[i], 2) + "V ";
		}
		RecordBistResult(task, false, true, IDS_RADIO, result, IDS_BIST_2630_VOLTS, voltStr.c_str());

		// Bandwidth test
		result = (bist2630.bandWidthTest == 3 ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::FAIL);
		RecordBistResult(task, false, true, IDS_RADIO, result, IDS_BIST_2630_BWTEST,
			"10", ((bist2630.bandWidthTest & 1) != 0 ? IDS_YES[lang] : IDS_NO[lang]),
			"80", ((bist2630.bandWidthTest & 2) != 0 ? IDS_YES[lang] : IDS_NO[lang]));

        if (result == SSmsMsg::SGetBistResp::FAIL)
        {
			UpdateOverallResult(result, passed);
			RecordBistResult(task, false, true, IDS_RADIO, result, IDS_BIST_2630_BWTEST_DETAILS,
    			Format(bist2630.bandWidthTestFrequency, 0).c_str(),
    			Format(bist2630.bandWidthTestSignalLevel, 2).c_str(),
    			Format(bist2630.bandWidthTestBelowThreshold, 2).c_str());
        }

		// Calgen test
        result = (bist2630.calGenTest != 0 ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::FAIL);
 		UpdateOverallResult(result, passed);
		RecordBistResult(task, false, true, IDS_RADIO, result, IDS_BIST_2630_CALGENTEST,
			((bist2630.calGenTest & 1) != 0 ? IDS_IS[lang] : IDS_ISNOT[lang]), Format(bist2630.VUSHFCALSSI, 2).c_str(),
			((bist2630.calGenTest & 2) != 0 ? IDS_IS[lang] : IDS_ISNOT[lang]), Format(bist2630.HFCALSSI, 2).c_str());

		// FPI Scan test
		for (size_t i = 0; i < bist2630.maxFpi; ++i)
		{
			auto& test = bist2630.scanTest[i];

			std::string rfInput;
			switch (test.selectedRfInput)
			{
			case S2630Msg::TERMINATE: rfInput = "TERMINATE"; break;
			case S2630Msg::VUSHFANT1: rfInput = "VUSHFANT1"; break;
			case S2630Msg::VUSHFANT2: rfInput = "VUSHFANT2"; break;
			case S2630Msg::HFANT: rfInput = "HFANT"; break;
			case S2630Msg::VUSHFCAL: rfInput = "VUSHFCAL"; break;
			case S2630Msg::HFCAL: rfInput = "HFCAL"; break;
			default: rfInput = "INVALID"; break;
			}

			std::string mode;
			switch (test.modeUsed)
			{
			case S2630Msg::NORMAL: mode = "normal"; break;
			case S2630Msg::RURAL: mode = "rural"; break;
			case S2630Msg::URBAN: mode = "urban"; break;
			case S2630Msg::CONGESTED: mode = "congested"; break;
			default: mode = "unknown"; break;
			}

			std::string rssi;
			for (size_t k = 1; k < 8; ++k)
			{
				if (((test.rssiPath >> k) & 1) != 0)
					rssi += std::to_string(k) + " ";
			}

	        result = (((test.testStatus & S2630Msg::ESignalPresentTest) != 0) ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::FAIL);
	 		UpdateOverallResult(result, passed);
			RecordBistResult(task, false, m_senddetail, IDS_RADIO, result, IDS_BIST_2630_SCANTEST, (i + 1),
				test.signalTestFrequency, test.bandWidthUsed, rfInput.c_str(),
				Format(test.inputSignalLevel, 2).c_str(), Format(test.outputSignalLevel, 2).c_str(),
				(((test.testStatus & S2630Msg::ESignalPresentTest) != 0) ? IDS_IS[lang] : IDS_ISNOT[lang]),
				Format(test.signalAboveThresholdValue ,2).c_str(), test.attenuationUsed, mode.c_str(), rssi.c_str(),
				(((test.testStatus & S2630Msg::ELo1LockTest) != 0) ? IDS_YES[lang] : IDS_NO[lang]),
				(((test.testStatus & S2630Msg::ELo2LockTest) != 0) ? IDS_YES[lang] : IDS_NO[lang]),
				(((test.testStatus & S2630Msg::ECalLockTest) != 0) ? IDS_YES[lang] : IDS_NO[lang]));

			/*
			if (bist2630.bistExtend1.bistVeresion > 0)
			{
			 * for (int k=0; k < 7; k++)
			{
				RecordBistResult(task, false, m_senddetail, IDS_RADIO, result, IDS_BIST_2630_RSSI, Format(bist2630.bistExtend1.rssiValues[i][k], 1).c_str());
			}
			}*/

		}

		// check for new bist structure with Version 1 if radio support it
		if (bist2630.bistExtend1.bistVeresion > 0)
		{
			char dactableversion[9];
			snprintf(dactableversion, 9, "%08lx", bist2630.bistExtend1.dacTableVersion);
			RecordBistResult(task, false, m_senddetail, IDS_RADIO, result, IDS_BIST_2630_DACTABLEVERSION, dactableversion);
			RecordBistResult(task, false, m_senddetail, IDS_RADIO, result, IDS_BIST_2630_TEMP_SENSOR1, Format(bist2630.bistExtend1.tempSensor1, 1).c_str());
			RecordBistResult(task, false, m_senddetail, IDS_RADIO, result, IDS_BIST_2630_TEMP_SENSOR2, Format(bist2630.bistExtend1.tempSensor2, 1).c_str());

			std::string statusText = "passed";
			if (bist2630.bistExtend1.tempSensorTest != 1)
			{
				statusText = "failed";
				result = SSmsMsg::SGetBistResp::FAIL;
				UpdateOverallResult(result, passed);
			}
			RecordBistResult(task, false, m_senddetail, IDS_RADIO, result, IDS_BIST_2630_TEMP_SENSOR_TEST, statusText.c_str());
			result = SSmsMsg::SGetBistResp::PASS;
			switch(bist2630.bistExtend1.inputReference)
			{
			case 0:
				statusText = "Intenal 100 MHz Disciplined by GPS";
				break;
			case 1:
				statusText = "Internal 100 MHz not Disciplined, 10 MHz not Available";
				break;
			case 2:
				statusText = "Internal 100 MHz not Disciplined, 1PPS not Available";
				break;
			case 3:
				statusText = "Internal 100 MHz Disciplined by GPS, 10 MHz Available";
				break;
			case 4:
				statusText = "External 10 MHz";
				break;
			case 5:
				statusText = "Internal 100 MHz not Disciplined, 10 MHz Available";
				break;
			case 6:
				statusText = "Internal 100 MHz Disciplined by GPS, External 1PPS Available";
				break;
			case 7:
				statusText = "Internal 100 MHz not Disciplined, External 1PPS Available";
				break;
			case 8:
				statusText = "External 1PPS";
				break;
			case 9:
				statusText = "External 100 MH";
				break;
			default:
				break;
			}
			RecordBistResult(task, false, m_senddetail, IDS_RADIO, result, IDS_BIST_2630_INPUT_CLOCK, statusText.c_str());
			switch(bist2630.bistExtend1.outputReference)
			{
			case 0:
				statusText = "100 MHz";
				break;
			case 1:
				statusText = "10 MHz";
				break;
			case 2:
				statusText = "One PPS";
				break;
			default:
				break;
			}
			RecordBistResult(task, false, m_senddetail, IDS_RADIO, result, IDS_BIST_2630_OUTPUT_CLOCK, statusText.c_str());
		}

		// QSPI test
		std::string qspi;
		result = SSmsMsg::SGetBistResp::FAIL;
		switch (bist2630.qspiFlashStatus)
		{
		case S2630Msg::CORRUPT: qspi = "Corrupt"; break;
		case S2630Msg::EMPTY: qspi = "Empty"; result = SSmsMsg::SGetBistResp::WARNING; break;
		case S2630Msg::FULL: qspi = "Full"; result = SSmsMsg::SGetBistResp::WARNING; break;
		case S2630Msg::OK: qspi = "Ok"; result = SSmsMsg::SGetBistResp::PASS; break;
		case S2630Msg::RECOVERED: qspi = "Recovered"; result = SSmsMsg::SGetBistResp::WARNING; break;
		default: qspi = "Unknown"; break;
		}
  		UpdateOverallResult(result, passed);

 		RecordBistResult(task, false, true, IDS_RADIO, result, IDS_BIST_2630_QSPITEST,
 			bist2630.qspiFlashSize, bist2630.qspiAvailableFlashPages, qspi.c_str(),
 			(bist2630.qspiFlashLockStatus != 0 ? IDS_YES[lang] : IDS_NO[lang]));
	}

	auto v = m_radioEquip->GetVersionInfo();
	char fwVersion[12];
	char dStamp[12];
	char fpgaVersion[12];
	snprintf(fwVersion, 12, "%02u.%02u(%04hx)", v.fwRevision.majorVersion, v.fwRevision.revision, v.fwRevision.subrevision);
	snprintf(dStamp, 12, "%04lu-%02lu-%02lu", v.fwDatestamp.year, v.fwDatestamp.month, v.fwDatestamp.date);
	snprintf(fpgaVersion, 12, "%02u.%02u(%04hx)", v.fpgaRevision.majorVersion, v.fpgaRevision.revision, v.fpgaRevision.subrevision);

	RecordBistResult(task, false, true, IDS_RADIO, SSmsMsg::SGetBistResp::PASS, IDS_BIST_FW_VERSION,
		"2630", fwVersion, dStamp, fpgaVersion);

	unsigned long id[2];
	m_radioEquip->GetMax10UniqueId(id);
	char radioId[18];
	snprintf(radioId, 18, "%08lx-%08lx", id[0], id[1]);
		RecordBistResult(task, false, true, IDS_RADIO, SSmsMsg::SGetBistResp::PASS, IDS_BIST_MAX10_ID, radioId);

	// Temperatures
	result = SSmsMsg::SGetBistResp::WARNING;

	std::vector<float> temperatures;
	if (m_radioEquip->ReadRxTemperatures(temperatures))
	{
		std::vector<std::array<float, 2> > tempLimits = m_radioEquip->GetTempLimits();
		assert(temperatures.size() == tempLimits.size());

		//for (unsigned int i = 0; i < temperatures.size(); ++i)
		for (unsigned int i = 1; i < temperatures.size(); ++i) // skip max 10 internal temp check until sensor works
		{
			result = SSmsMsg::SGetBistResp::PASS;
			if (temperatures[i] < tempLimits[i][0] || temperatures[i] > tempLimits[i][1])
			{
				result = SSmsMsg::SGetBistResp::WARNING;
				UpdateOverallResult(result, passed);
			}
			RecordBistResult(task, false, false, IDS_RADIO, result, IDS_BIST_2630_TEMP, IDS_2630_TEMPS[i][lang], Format(temperatures[i], 1).c_str(),
				Format(tempLimits[i][0], 1).c_str(), Format(tempLimits[i][1], 1).c_str());
		}
	}
	else
	{
		result = SSmsMsg::SGetBistResp::WARNING;
		UpdateOverallResult(result, passed);
		RecordBistResult(task, false, false, IDS_RADIO, result, IDS_BIST_2630_TEMP_FAILED);
	}
//	UpdateOverallResult(result, passed);
//	RecordBistResult(task, false, false, IDS_RADIO, result, IDS_BIST_2630_TEMP, Format(temp, 1).c_str(),
//		Format(tempLimit[0], 1).c_str(), Format(tempLimit[1], 1).c_str());

	// Ocxo
	unsigned char loopStatus = 0;
	unsigned long code = 0;
	unsigned long interval = 0;
	result = SSmsMsg::SGetBistResp::WARNING;
	if (m_radioEquip->ReadOcxo(loopStatus, code, interval))
	{
		result = SSmsMsg::SGetBistResp::PASS;
	}
	char codeStr[12];
	char intervalStr[12];
	snprintf(codeStr, 12, "%08lu", code);
	snprintf(intervalStr, 12, "%08lu", interval);

	UpdateOverallResult(result, passed);
	RecordBistResult(task, false, false, IDS_RADIO, result, IDS_BIST_2630_OCXO, (loopStatus == 1 ? "locked" : "not locked"),
		codeStr, intervalStr);

	// Signal power measurements

	// Turn on calgen
	bool setOn;
	m_radioEquip->SetCalTone(true, setOn);

	for (size_t j = 0; j < m_radioEquip->NumFpis(); ++j)	// Loop over FPIs
	{
		size_t numBws = (m_radioEquip->IsHfFpi(j) || !m_radioEquip->IsWideband() ? 1 : 2);
		for (size_t i = 0; i < numBws; ++i)	// Loop over bandwidths
		{
			// Set up radio
			unsigned long calFreqMHz;
			float rxInputDbm;
			bool direct;
			float expectedDbmAt3230;
			if (!m_radioEquip->DoBistCal(j, (i == 0), calFreqMHz, rxInputDbm, direct, expectedDbmAt3230))
				continue;

			auto expectedDbm = expectedDbmAt3230 + 10.f * log10(m_digitizer->GetGainRatio(direct));	// This is what is expected at the A/D

			float errorDbm;
			for (unsigned int retry = 0; retry < (RETRY + 1); ++retry)
			{
				m_digitizer->MeasureToneDbm(direct, errorDbm);
				errorDbm -= expectedDbm;
				if (fabsf(errorDbm) > 8)
				{
					result = SSmsMsg::SGetBistResp::FAIL;
				}
				else if (fabsf(errorDbm) > 6)
				{
					result = SSmsMsg::SGetBistResp::WARNING;
				}
				else
				{
					result = SSmsMsg::SGetBistResp::PASS;
				}
				if (result == SSmsMsg::SGetBistResp::PASS || retry == RETRY)
				{
					RecordBistResult(task, false, false, IDS_RADIO, result, IDS_BIST_CAL_TONE, Format(errorDbm, 1).c_str(), Format(rxInputDbm, 1).c_str(),
						calFreqMHz, (i == 0 ? IDS_NARROW[lang] : IDS_WIDE[lang]), j, (retry == 0 ? IDS_EMPTY[lang] : IDS_AFTER_RETRY[lang]));
					break;
				}
				if (retry < RETRY)
				{
					usleep(1000);
				}
			}
			UpdateOverallResult(result, passed);
		}
	}

	// Attenuation measurements
	// TODO:


	// Turn off calgen
	m_radioEquip->SetCalTone(false, setOn);

	// Overall
	RecordBistResult(task, false, true, IDS_RADIO, passed);

	return passed;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the 3230 Digital Board
//
SSmsMsg::SGetBistResp::EResult CBist::DoBist3230(_In_ const CTask::Task& task)
{
	auto lang = m_config->GetLangIdx();

	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;

	auto v = m_digitizer->GetVersionInfo();

	// Driver version
	{
		char drvVersion[12];
		char drvStamp[12];
		snprintf(drvVersion, 12, "%02u.%02u(%04hx)", v.driverRevision.majorVersion, v.driverRevision.revision, v.driverRevision.subrevision);
		snprintf(drvStamp, 12, "%04lu-%02lu-%02lu", v.driverDatestamp.year, v.driverDatestamp.month, v.driverDatestamp.date);
			RecordBistResult(task, false, true, IDS_DIGITIZER, SSmsMsg::SGetBistResp::PASS, IDS_BIST_DRV_VERSION,
				drvVersion, drvStamp);
	}
	// Board and firmware versions
	{
		char deviceId[18];
		char boardRev[12];
		char gStatus[12];
		snprintf(deviceId, 18, "%02x-%02x-%02x-%02x-%04x",
			v.deviceId.revision, v.deviceId.family, v.deviceId.subFamily, v.deviceId.device, v.deviceId.manufacturerId);
//		snprintf(deviceId, 12, "%08lx", v.deviceId);
		snprintf(boardRev, 12, "%hu", v.boardRevision);
		snprintf(gStatus, 12, "%04hx", m_digitizer->GetGlobalStatus());
		RecordBistResult(task, false, true, IDS_DIGITIZER, SSmsMsg::SGetBistResp::PASS, IDS_BIST_BOARD_VERSION,
			deviceId, boardRev, gStatus);

		unsigned long dna[2];
		m_digitizer->GetDeviceDna(dna);
		char deviceDna[16];
		snprintf(deviceDna, 16, "%06lx-%08lx", dna[0], dna[1]);
		RecordBistResult(task, false, true, IDS_DIGITIZER, SSmsMsg::SGetBistResp::PASS, IDS_BIST_BOARD_DEVICEDNA, deviceDna);

		std::string typeStr;
		char fwVersion[12];
		char dStamp[12];
		char totalDDCs[12];
		if (v.type == C3230::SVersionInfo::EFwType::GOLDEN)
			typeStr = "Golden";
		else if (v.type == C3230::SVersionInfo::EFwType::MICRO_SMS)
			typeStr = "MicroSMS";
		else
			typeStr = "Unknown";
		snprintf(fwVersion, 12, "%02u.%02u(%04hx)", v.fwRevision.majorVersion, v.fwRevision.revision, v.fwRevision.subrevision);
		snprintf(dStamp, 12, "%04lu-%02lu-%02lu", v.fwDatestamp.year, v.fwDatestamp.month, v.fwDatestamp.date);
		snprintf(totalDDCs, 12, "%3u DDCs", v.totalDDCs);
			RecordBistResult(task, false, true, IDS_DIGITIZER, SSmsMsg::SGetBistResp::PASS, IDS_BIST_FW_VERSION,
			typeStr.c_str(), fwVersion, dStamp, totalDDCs);
	}
	// Clocks and status
	{
		auto clocks = m_digitizer->GetClockFrequencies();
		auto ReadyorNot = IDS_READY[lang];
		std::vector<std::string> clockStr;
		for (size_t i = 0; i < clocks.size(); ++i)
		{
			clockStr.push_back(std::to_string(clocks[i]));
		}
		clockStr.resize(4);		// Should be no-op
		auto result = SSmsMsg::SGetBistResp::PASS;
		if (clocks.size() < 4 ||
			clocks[0] < C3230::ADC_CLOCK_RATE - 10 || clocks[0] > C3230::ADC_CLOCK_RATE + 10 ||
			clocks[2] < 100000000ul - 3 || clocks[2] > 100000000ul + 3)
		{
			ReadyorNot = IDS_NOTREADY[lang];
			//result = SSmsMsg::SGetBistResp::WARNING;
			//UpdateOverallResult(SSmsMsg::SGetBistResp::WARNING, passed);
		}
		char tsStatus[12];
		snprintf(tsStatus, 12, "%08lx", m_digitizer->GetTimestampStatus());
		RecordBistResult(task, false, false, IDS_DIGITIZER, result, IDS_3230_CLOCK, ReadyorNot, clockStr[0].c_str(),
			clockStr[2].c_str(), tsStatus);
	}

	// Voltages
	auto voltages = m_digitizer->GetVoltages();
	auto voltageLimits = m_digitizer->GetVoltageLimits();
	assert(voltages.size() == voltageLimits.size());

	for (unsigned int i = 0; i < voltages.size(); ++i)
	{
		auto result = SSmsMsg::SGetBistResp::PASS;
		if (voltages[i] < voltageLimits[i][0] || voltages[i] > voltageLimits[i][1])
		{
			result = SSmsMsg::SGetBistResp::WARNING;
			UpdateOverallResult(SSmsMsg::SGetBistResp::WARNING, passed);
		}
		RecordBistResult(task, false, false, IDS_DIGITIZER, result, IDS_BIST_3230_VOLTS, IDS_3230_VOLTAGES[i][lang], Format(voltages[i], 2).c_str(),
			Format(voltageLimits[i][0], 2).c_str(), Format(voltageLimits[i][1], 2).c_str());
	}

	// Temperatures
	std::vector<float> temps = m_digitizer->GetTemperatures();
	std::vector<std::array<float, 2> > tempLimits = m_digitizer->GetTempLimits();
	assert(temps.size() == tempLimits.size());

	for (unsigned int i = 0; i < temps.size(); ++i)
	{
		auto result = SSmsMsg::SGetBistResp::PASS;
		if (temps[i] < tempLimits[i][0] || temps[i] > tempLimits[i][1])
		{
			result = SSmsMsg::SGetBistResp::WARNING;
			UpdateOverallResult(SSmsMsg::SGetBistResp::WARNING, passed);
		}
		RecordBistResult(task, false, false, IDS_DIGITIZER, result, IDS_BIST_3230_TEMP, IDS_3230_TEMPS[i][lang], Format(temps[i], 1).c_str(),
			Format(tempLimits[i][0], 1).c_str(), Format(tempLimits[i][1], 1).c_str());
	}

	// Report on digitizer memory check
	std::vector<unsigned char> memChecks = m_digitizer->GetMemoryChecks();
	bool memCheckOk = true;
	std::string memCheckStr;
	for (unsigned int i = 0; i < memChecks.size(); ++i)
	{
		memCheckStr += std::to_string(memChecks[i]);
		if (memChecks[i] == 0)
		{
			memCheckOk = false;
			break;
		}
	}
	if (!memCheckOk)
		UpdateOverallResult(SSmsMsg::SGetBistResp::WARNING, passed);

	RecordBistResult(task, false, false, IDS_DIGITIZER, (memCheckOk ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::WARNING),
		IDS_BIST_3230_MEMCHECK, memCheckStr.c_str());

	bool bootModeCheckOk = true;
	std::string bootmode = "booting from flash";
	if (!m_digitizer->CheckifBootfromFlash())
	{
		bootModeCheckOk = false;
		bootmode = "not booting from flash!";
		UpdateOverallResult(SSmsMsg::SGetBistResp::WARNING, passed);
	}

	RecordBistResult(task, false, false, IDS_DIGITIZER, (bootModeCheckOk ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::WARNING),
			IDS_BIST_BOOT_MODE, bootmode.c_str());

	// Overall
	RecordBistResult(task, false, true, IDS_DIGITIZER, passed);

	return passed;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the 7234201101 (U/SHF Switch)
//
SSmsMsg::SGetBistResp::EResult CBist::DoBist72342011(_In_ const CTask::Task& task)
{
	auto lang = m_config->GetLangIdx();
	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;
	auto result = SSmsMsg::SGetBistResp::PASS;
	bool senddetail = false;

	// BIST Cal Source / low frequency path / individual channels to detect swapped condition
	// set so -30 dBm BITE source produces -10 dBm IF output
	float power[2];

	auto testGainMode = C2630::EGainMode::RURAL;
	auto slaveGainMode = SDfCtrlMsg::EGainMode::RURAL;

	CBistSlave bistSlave;

	// Tone power tests
	for (size_t i = 0; C7234201102::GetCalFreq(i) != 0; ++i)
	{
		Units::Frequency biteFreq = C7234201102::GetCalFreq(i);
		Units::Frequency rxFreq = biteFreq;
		float adjustedDbm = -m_config->GetRfCableLoss(false, SEquipCtrlMsg::ANT1, rxFreq);
		float realNominal = C7234201102::GetCalDbm(i, 0);
		float nominalRef = realNominal + adjustedDbm;
		float lowest = nominalRef - C7234201102::GetCalHalfRangeDbm(i);
		float highest = nominalRef + C7234201102::GetCalHalfRangeDbm(i);
		float nominalSample = C7234201102::GetCalDbm(i, 1) + adjustedDbm;

		// Tune the radio
		Units::Frequency finalIfFreq;
		bool inverted;
		Units::FreqPair freqLimits;
		Units::Frequency radioFreq;
		size_t band;
		bool direct;
		if (!m_radioEquip->Tune(biteFreq, m_radioEquip->GetRxBandwidth(false, true), Units::Frequency(), false, CRadioEquip::EBandSelect::OPTIMUM,
			finalIfFreq, inverted, freqLimits, radioFreq, band, direct, SEquipCtrlMsg::EAnt::ANT1))
		{
			break;
		}

		RecordDfBistResult(task, false, senddetail, IDS_UHF_SWITCH, SSmsMsg::SGetBistResp::PASS, IDS_BIST_NOMINAL_ADJUSTMENT,
			Format(adjustedDbm, 1).c_str());
		RecordDfBistResult(task, false, senddetail, IDS_UHF_SWITCH, SSmsMsg::SGetBistResp::PASS, IDS_BIST_NOMINAL_MIN_MAX,
			Format(realNominal, 1).c_str(), Format(lowest, 1).c_str(), Format(highest, 1).c_str());

		// Tune the slave rx
		std::vector<size_t> sentIndex;
		bistSlave.Init(biteFreq, m_radioEquip->GetRxBandwidth(false, true), Units::Frequency(), false, CRadioEquip::EBandSelect::OPTIMUM);

		// Set the attenuator and get the expected level at the 3230 and at the A/D
		float expectedDbmAt3230;
		m_radioEquip->SetAttenForSpecifiedOutput(nominalRef, -10.f, testGainMode, expectedDbmAt3230);
		auto expectedDbm = expectedDbmAt3230 + 10.f * log10(m_digitizer->GetGainRatio(direct));

		// Set the antenna switch and wait for settling
		m_switch->C7234201102::SetSwitch(biteFreq, C7234201102::RECEIVE, C7234201102::BITE, C7234201102::TERM);

		// Wait for slave to be tuned and request ToneDbm
		unsigned char atten;
		m_radioEquip->GetAtten(atten);
		bistSlave.Request(SDfCtrlMsg::SCcdfCollect::TONEDBM, atten, slaveGainMode);

		m_digitizer->MeasureToneDbm(direct, power[0]);	// can only measure ref here

		std::vector<float> slavePower;
		bistSlave.GetResults(slavePower);

		float gain = expectedDbm - nominalRef;
		power[0] -= gain;
		// TODO: Handle multiple slave case eventually
		power[1] = (!slavePower.empty() ? slavePower[0] : -std::numeric_limits<float>::infinity());

		if (power[0] < lowest || power[0] > highest)
		{
			result = SSmsMsg::SGetBistResp::FAIL;
			passed = SSmsMsg::SGetBistResp::FAIL;
		}
		else
		{
			result = SSmsMsg::SGetBistResp::PASS;
		}

		RecordDfBistResult(task, false, false, IDS_UHF_SWITCH, result, IDS_BIST_SWITCH_CAL_TONE, IDS_REFERENCE[lang],
			Format(biteFreq.Hz<double>() / 1e6, 3).c_str(), Format(power[0], 1).c_str(), Format(nominalRef, 1).c_str());

		float oppositeMaxPower = nominalSample - 10.0f;
		if (biteFreq < 3000000000 && power[1] >= oppositeMaxPower) // Leakage causes problems at 4500 MHz
		{
			result = SSmsMsg::SGetBistResp::FAIL;
			passed = SSmsMsg::SGetBistResp::FAIL;
			RecordDfBistResult(task, false, false, IDS_UHF_SWITCH, result, IDS_BIST_SWITCH_CAL_TONE, IDS_OPPOSITE[lang],
				Format(biteFreq.Hz<double>() / 1e6, 3).c_str(), Format(power[1], 1).c_str(), Format(oppositeMaxPower, 1).c_str());
		}
		else
		{
			result = SSmsMsg::SGetBistResp::PASS;
			if (power[1] < oppositeMaxPower)
			{
				RecordDfBistResult(task, false, false, IDS_UHF_SWITCH, result, IDS_BIST_SWITCH_CAL_TONE, IDS_OPPOSITE[lang],
					Format(biteFreq.Hz<double>() / 1e6, 3).c_str(), Format(power[1], 1).c_str(), Format(oppositeMaxPower, 1).c_str());
			}
			else
			{
				RecordDfBistResult(task, false, false, IDS_UHF_SWITCH, result, IDS_BIST_SWITCH_CAL_TONE, IDS_OPPOSITE[lang],
					Format(biteFreq.Hz<double>() / 1e6, 3).c_str(), Format(power[1], 1).c_str(), IDS_HIGH[lang]);
			}
		}

		m_switch->C7234201102::SetSwitch(biteFreq, C7234201102::RECEIVE, C7234201102::BITE, C7234201102::BITE);

		m_radioEquip->GetAtten(atten);
		bistSlave.RequestQuick(SDfCtrlMsg::SCcdfCollect::TONEDBM, atten, slaveGainMode);

		m_digitizer->MeasureToneDbm(direct, power[0]);

		bistSlave.GetResults(slavePower);

		power[0] -= gain;
		// TODO: Handle multiple slave case eventually
		power[1] = (!slavePower.empty() ? slavePower[0] : -std::numeric_limits<float>::infinity());

		if (power[0] < lowest || power[0] > highest)
		{
			result = SSmsMsg::SGetBistResp::FAIL;
			passed = SSmsMsg::SGetBistResp::FAIL;
		}
		else
		{
			result = SSmsMsg::SGetBistResp::PASS;
		}

		RecordDfBistResult(task, false, false, IDS_UHF_SWITCH, result, IDS_BIST_SWITCH_CAL_TONE, IDS_REFERENCE[lang],
			Format(biteFreq.Hz<double>() / 1e6, 3).c_str(), Format(power[0], 1).c_str(), Format(nominalRef, 1).c_str());

		if ((power[1] < nominalSample - C7234201102::GetCalHalfRangeDbm(i) ||
			power[1] > nominalSample + C7234201102::GetCalHalfRangeDbm(i)))
		{
			result = SSmsMsg::SGetBistResp::FAIL;
			passed = SSmsMsg::SGetBistResp::FAIL;
		}
		else
		{
			result = SSmsMsg::SGetBistResp::PASS;
		}

		RecordDfBistResult(task, false, false, IDS_UHF_SWITCH, result, IDS_BIST_SWITCH_CAL_TONE, IDS_SAMPLE[lang],
			Format(biteFreq.Hz<double>() / 1e6, 3).c_str(), Format(power[1], 1).c_str(), Format(nominalSample, 1).c_str());
	}

	// Noise Source / low frequency path / both channels
	// set so noise source produces -10 dBm IF output
	static const struct
	{
		unsigned long long freq;
		signed char nominalRef;
		signed char nominalSample;
	} NOISE_PARAMS[] =
	{
		{  250000000, -48, -56 }, // Nominal includes SHF converter gain
		{  850000000, -48, -56 },
		{ 1360000000, -48, -56 },
		{ 1890000000, -50, -56 },
		{ 2530000000, -51, -56 },
		{ 3123456789, -52, -56 },
		{ 3980000000, -54, -58 },
		{ 5610000000, -57, -67 },
		{ 6023456789, -58, -70 },
		{ 6890000000, -63, -71 }
	};

	for (size_t band = 0; band < std::extent<decltype(NOISE_PARAMS)>::value; ++band)
	{
		Units::Frequency rxFreq = NOISE_PARAMS[band].freq;

		//2614: Add 7.0dB for 10MHz 2630 as its bandwidth 5x 2 MHz 2612. (TODO: redo table?)
		float nominalRef = NOISE_PARAMS[band].nominalRef + 7.0f - m_config->GetRfCableLoss(false, SEquipCtrlMsg::ANT1, rxFreq);
		float nominalSample = NOISE_PARAMS[band].nominalSample +7.0f - m_config->GetRfCableLoss(false, SEquipCtrlMsg::ANT1, rxFreq);

		// Tune the radio
		Units::Frequency finalIfFreq;
		bool inverted;
		Units::FreqPair freqLimits;
		Units::Frequency radioFreq;
		size_t radioBand;
		bool direct;
		if (!m_radioEquip->Tune(rxFreq, m_radioEquip->GetRxBandwidth(false, true), Units::Frequency(), false, CRadioEquip::EBandSelect::OPTIMUM,
			finalIfFreq, inverted, freqLimits, radioFreq, radioBand, direct, SEquipCtrlMsg::EAnt::ANT1))
		{
			break;
		}

		// Tune the slave rx
		std::vector<size_t> sentIndex;
		bistSlave.Init(rxFreq, m_radioEquip->GetRxBandwidth(false, true), Units::Frequency(), false, CRadioEquip::EBandSelect::OPTIMUM);

		// Set the attenuator and get the expected level at the 3230 and at the A/D
		float expectedDbmAt3230;
		m_radioEquip->SetAttenForSpecifiedOutput(nominalRef, -10.f, testGainMode, expectedDbmAt3230);
		auto expectedDbm = expectedDbmAt3230 + 10.f * log10(m_digitizer->GetGainRatio(direct));
		float gain = expectedDbm - nominalRef;

		// Set the antenna switch and wait for settling
		m_switch->C7234201102::SetSwitch(rxFreq, C7234201102::RECEIVE, C7234201102::NOISE, C7234201102::NOISE);

		// Wait for slave to be tuned and request ToneDbm
		unsigned char atten;
		m_radioEquip->GetAtten(atten);
		bistSlave.Request(SDfCtrlMsg::SCcdfCollect::NOISEDBM, atten, slaveGainMode);

		m_digitizer->MeasureNoiseDbm(direct, power[0]);

		std::vector<float> slavePower;
		bistSlave.GetResults(slavePower);

		power[0] -= gain;
		// TODO: Handle multiple slave case eventually
		power[1] = (!slavePower.empty() ? slavePower[0] : -std::numeric_limits<float>::infinity());

		if (power[0] < nominalRef - 10 || power[0] > nominalRef + 10)
		{
			result = SSmsMsg::SGetBistResp::FAIL;
			passed = SSmsMsg::SGetBistResp::FAIL;
		}
		else
		{
			result = SSmsMsg::SGetBistResp::PASS;
		}

		RecordDfBistResult(task, false, false, IDS_UHF_SWITCH, result, IDS_BIST_ANT_SWITCH_NOISE, IDS_REFERENCE[lang],
			Format(power[0], 1).c_str(), Format(nominalRef, 1).c_str());

		if (power[1] < nominalSample - 10 || power[1] > nominalSample + 10)
		{
			result = SSmsMsg::SGetBistResp::FAIL;
			passed = SSmsMsg::SGetBistResp::FAIL;
		}
		else
		{
			result = SSmsMsg::SGetBistResp::PASS;
		}

		RecordDfBistResult(task, false, false, IDS_UHF_SWITCH, result, IDS_BIST_ANT_SWITCH_NOISE, IDS_SAMPLE[lang],
			Format(power[1], 1).c_str(), Format(nominalSample, 1).c_str());
	}

	// Overall
	RecordDfBistResult(task, false, true, IDS_UHF_SWITCH, passed);

	return passed;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the 72362001 (HF Switch)
//
SSmsMsg::SGetBistResp::EResult CBist::DoBist72362001(_In_ const CTask::Task& task)
{
	auto lang = m_config->GetLangIdx();
	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;
	auto result = SSmsMsg::SGetBistResp::PASS;
	bool senddetail = false;

	// BIST Cal Source / low frequency path / individual channels to detect swapped condition
	// set so -30 dBm BIST source produces -10 dBm IF output
	auto testGainMode = C2630::EGainMode::RURAL;
	auto slaveGainMode = SDfCtrlMsg::EGainMode::RURAL;
	CBistSlave bistSlave;

	float power[2];

	// Note: This version of the 7236 has only one cal frequency
	Units::Frequency biteFreq = C72362001::GetCalFreq();
	float adjustedDbm = -m_config->GetRfCableLoss(true, SEquipCtrlMsg::ANT1, biteFreq);
	float realNominal = C72362001::GetCalDbm(0);
	float nominalRef = realNominal + adjustedDbm;
	float lowest = nominalRef - 10;
	float highest = nominalRef + 10;

	// Tune the radio
	Units::Frequency finalIfFreq;
	bool inverted;
	Units::FreqPair freqLimits;
	Units::Frequency radioFreq;
	size_t band;
	bool direct;
	if (m_radioEquip->Tune(biteFreq, m_radioEquip->GetRxBandwidth(true, true), Units::Frequency(), true, CRadioEquip::EBandSelect::OPTIMUM,
		finalIfFreq, inverted, freqLimits, radioFreq, band, direct, SEquipCtrlMsg::EAnt::ANT1))
	{
		RecordDfBistResult(task, false, senddetail, IDS_HF_SWITCH, SSmsMsg::SGetBistResp::PASS, IDS_BIST_NOMINAL_ADJUSTMENT,
			Format(adjustedDbm, 1).c_str());
		RecordDfBistResult(task, false, senddetail, IDS_HF_SWITCH, SSmsMsg::SGetBistResp::PASS, IDS_BIST_NOMINAL_MIN_MAX,
			Format(realNominal, 1).c_str(), Format(lowest, 1).c_str(), Format(highest, 1).c_str());

		// Tune the slave rx
		std::vector<size_t> sentIndex;
		bistSlave.Init(biteFreq, m_radioEquip->GetRxBandwidth(true, true), Units::Frequency(), true, CRadioEquip::EBandSelect::OPTIMUM);

		// Set the attenuator and get the expected level at the 3230 and at the A/D
		float expectedDbmAt3230;
		m_radioEquip->SetAttenForSpecifiedOutput(nominalRef, -10.f, testGainMode, expectedDbmAt3230);
		auto expectedDbm = expectedDbmAt3230 + 10.f * log10(m_digitizer->GetGainRatio(direct));

		// Set the antenna switch and wait for settling
		m_switch->C72362001::SetSwitch(biteFreq, C72362001::RECEIVE, C72362001::BITE, C72362001::TERM);

		// Wait for slave to be tuned and request ToneDbm
		unsigned char atten;
		m_radioEquip->GetAtten(atten);
		bistSlave.Request(SDfCtrlMsg::SCcdfCollect::TONEDBM, atten, slaveGainMode);

		m_digitizer->MeasureToneDbm(direct, power[0]);	// reference channel

		std::vector<float> slavePower;
		bistSlave.GetResults(slavePower);

		float gain = expectedDbm - nominalRef;
		power[0] -= gain;
		// TODO: Handle multiple slave case eventually
		power[1] = (!slavePower.empty() ? slavePower[0] : -std::numeric_limits<float>::infinity());

		if (power[0] < lowest || power[0] > highest)
		{
			// Reset the cal generator and try again
			float firstPower = power[0];
			m_switch->C72362001::SetSwitch(biteFreq, C72362001::RECEIVE, C72362001::TERM, C72362001::TERM);
			Utility::Delay(50000);
			m_switch->C72362001::SetSwitch(biteFreq, C72362001::RECEIVE, C72362001::BITE, C72362001::TERM);

			bistSlave.RequestQuick(SDfCtrlMsg::SCcdfCollect::TONEDBM, atten, slaveGainMode);
			m_digitizer->MeasureToneDbm(direct, power[0]);
			bistSlave.GetResults(slavePower);
			power[0] -= gain;
			power[1] = (!slavePower.empty() ? slavePower[0] : -std::numeric_limits<float>::infinity());

			if (power[0] < lowest || power[0] > highest)
			{
				// Failed twice so FAIL
				result = SSmsMsg::SGetBistResp::FAIL;
				passed = SSmsMsg::SGetBistResp::FAIL;
			}
			else
			{
				// OK second time so WARNING
				power[0] = firstPower;
				result = SSmsMsg::SGetBistResp::WARNING;
				UpdateOverallResult(SSmsMsg::SGetBistResp::WARNING, passed);
			}
		}

		RecordDfBistResult(task, false, false, IDS_HF_SWITCH, result, IDS_BIST_SWITCH_CAL_TONE, IDS_REFERENCE[lang],
			Format(biteFreq.Hz<double>() / 1e6, 3).c_str(), Format(power[0], 1).c_str(), Format(nominalRef, 1).c_str());

		float oppositeMaxPower = lowest;

		if (power[1] >= oppositeMaxPower)
		{
			result = SSmsMsg::SGetBistResp::FAIL;
			passed = SSmsMsg::SGetBistResp::FAIL;
		}
		else
		{
			result = SSmsMsg::SGetBistResp::PASS;
		}

		RecordDfBistResult(task, false, false, IDS_HF_SWITCH, result, IDS_BIST_SWITCH_CAL_TONE, IDS_OPPOSITE[lang],
			Format(biteFreq.Hz<double>() / 1e6, 3).c_str(), Format(power[1], 1).c_str(), Format(oppositeMaxPower, 1).c_str());

		m_switch->C72362001::SetSwitch(biteFreq, C72362001::RECEIVE, C72362001::TERM, C72362001::BITE);

		bistSlave.RequestQuick(SDfCtrlMsg::SCcdfCollect::TONEDBM, atten, slaveGainMode);
		m_digitizer->MeasureToneDbm(direct, power[0]);
		bistSlave.GetResults(slavePower);
		power[0] -= gain;
		power[1] = (!slavePower.empty() ? slavePower[0] : -std::numeric_limits<float>::infinity());

		if (power[1] < lowest || power[1] > highest)
		{
			result = SSmsMsg::SGetBistResp::FAIL;
			passed = SSmsMsg::SGetBistResp::FAIL;
		}
		else
		{
			result = SSmsMsg::SGetBistResp::PASS;
		}

		RecordDfBistResult(task, false, false, IDS_HF_SWITCH, result, IDS_BIST_SWITCH_CAL_TONE, IDS_SAMPLE[lang],
			Format(biteFreq.Hz<double>() / 1e6, 3).c_str(), Format(power[1], 1).c_str(), Format(nominalRef, 1).c_str());

		if (power[0] >= oppositeMaxPower)
		{
			result = SSmsMsg::SGetBistResp::FAIL;
			passed = SSmsMsg::SGetBistResp::FAIL;
		}
		else
		{
			result = SSmsMsg::SGetBistResp::PASS;
		}

		RecordDfBistResult(task, false, false, IDS_HF_SWITCH, result, IDS_BIST_SWITCH_CAL_TONE, IDS_OPPOSITE[lang],
			Format(biteFreq.Hz<double>() / 1e6, 3).c_str(), Format(power[0], 1).c_str(), Format(oppositeMaxPower, 1).c_str());
	}

	// Noise Source / low frequency path / both channels
	// set so -40 dBm (8084 2 MHz BW) or -30 dBm (2620 / 2621 preselector) noise source produces -10 dBm IF output
	nominalRef = -30.f - m_config->GetRfCableLoss(true, SEquipCtrlMsg::ANT1, 12345678);

	if (m_radioEquip->Tune(12345678, m_radioEquip->GetRxBandwidth(false, true), Units::Frequency(), true, CRadioEquip::EBandSelect::OPTIMUM,
		finalIfFreq, inverted, freqLimits, radioFreq, band, direct, SEquipCtrlMsg::EAnt::ANT1))
	{
		bistSlave.Init(12345678, m_radioEquip->GetRxBandwidth(false, true), Units::Frequency(), true, CRadioEquip::EBandSelect::OPTIMUM);

		// Set the attenuator and get the expected level at the 3230 and at the A/D
		float expectedDbmAt3230;
		m_radioEquip->SetAttenForSpecifiedOutput(nominalRef, -10.f, testGainMode, expectedDbmAt3230);
		auto expectedDbm = expectedDbmAt3230 + 10.f * log10(m_digitizer->GetGainRatio(direct));

		m_switch->C72362001::SetSwitch(12345678, C72362001::RECEIVE, C72362001::NOISE, C72362001::NOISE);

		// Wait for slave to be tuned and request ToneDbm
		unsigned char atten;
		m_radioEquip->GetAtten(atten);
		bistSlave.Request(SDfCtrlMsg::SCcdfCollect::NOISEDBM, atten, slaveGainMode);

		m_digitizer->MeasureNoiseDbm(direct, power[0]);

		std::vector<float> slavePower;
		bistSlave.GetResults(slavePower);

		auto gain = expectedDbm - nominalRef;
		power[0] -= gain;
		power[1] = (!slavePower.empty() ? slavePower[0] : -std::numeric_limits<float>::infinity());


		if (power[0] < nominalRef - 10 || power[0] > nominalRef + 10)
		{
			result = SSmsMsg::SGetBistResp::FAIL;
			passed = SSmsMsg::SGetBistResp::FAIL;
		}
		else
		{
			result = SSmsMsg::SGetBistResp::PASS;
		}

		RecordDfBistResult(task, false, false, IDS_HF_SWITCH, result, IDS_BIST_ANT_SWITCH_NOISE, IDS_REFERENCE[lang],
			Format(power[0], 1).c_str(), Format(nominalRef, 1).c_str());

		if (power[1] < nominalRef - 10 || power[1] > nominalRef + 10)
		{
			result = SSmsMsg::SGetBistResp::FAIL;
			passed = SSmsMsg::SGetBistResp::FAIL;
		}
		else
		{
			result = SSmsMsg::SGetBistResp::PASS;
		}

		RecordDfBistResult(task, false, false, IDS_HF_SWITCH, result, IDS_BIST_ANT_SWITCH_NOISE, IDS_SAMPLE[lang],
			Format(power[1], 1).c_str(), Format(nominalRef, 1).c_str());
	}

	RecordDfBistResult(task, false, true, IDS_HF_SWITCH, passed);

	return passed;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the database
//
SSmsMsg::SGetBistResp::EResult CBist::DoBistCompass(_In_ const CTask::Task& task)
{
	auto lang = m_config->GetLangIdx();
	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;
//	SSmsMsg::SGetBistResp::EResult result = SSmsMsg::SGetBistResp::PASS;

	// Mobile antenna implies compass
	if (m_config->HasMobileAntenna(true) || m_config->HasMobileAntenna(false))
	{
		if (!m_navigation->IsCompassPresent())
		{
			RecordBistResult(task, false, true, IDS_COMPASS, SSmsMsg::SGetBistResp::FAIL, IDS_BIST_NOT_DETECTED);
			passed = SSmsMsg::SGetBistResp::FAIL;
		}
		else
		{
			// Version information
			RecordBistResult(task, false, true, IDS_COMPASS, SSmsMsg::SGetBistResp::PASS, IDS_BIST_VERSION,
				m_navigation->GetCompassVersion().c_str());

			if (m_mutableConfig.miscConfig.compassType != CConfig::ECompassType::NMEA)
			{
				// Headings
				float heading = m_navigation->GetRawMagHeading(m_config->HasMobileAntenna(true), 0.);
				float declination = m_navigation->GetDeclination();
				RecordBistResult(task, false, true, IDS_COMPASS, SSmsMsg::SGetBistResp::PASS, IDS_BIST_COMPASS_HEADING,
					Format(heading, 1).c_str(), Format(declination, 1).c_str());
			}

			if (m_mutableConfig.miscConfig.compassType == CConfig::ECompassType::C100)
			{
				// Calibration status
				CNavigation::SCalStatus status = m_navigation->GetCalStatus();

				if (status.count == 0)
				{
					RecordBistResult(task, false, true, IDS_COMPASS, SSmsMsg::SGetBistResp::FAIL, IDS_BIST_COMPASS_CALIBRATION,
						status.count, status.noise, status.environment);
					passed = SSmsMsg::SGetBistResp::FAIL;
				}
				else if (status.noise < 8 || status.environment < 5)
				{
					RecordBistResult(task, false, true, IDS_COMPASS,SSmsMsg::SGetBistResp::WARNING, IDS_BIST_COMPASS_CALIBRATION,
						status.count, status.noise, status.environment);
					UpdateOverallResult(SSmsMsg::SGetBistResp::WARNING, passed);
				}
				else
				{
					RecordBistResult(task, false, true, IDS_COMPASS, SSmsMsg::SGetBistResp::PASS, IDS_BIST_COMPASS_CALIBRATION,
						status.count, status.noise, status.environment);
				}
			}

			if (m_mutableConfig.miscConfig.compassType != CConfig::ECompassType::NMEA)
			{
				// Mast status
				RecordBistResult(task, false, true, IDS_COMPASS, SSmsMsg::SGetBistResp::PASS, IDS_BIST_COMPASS_MAST,
					(m_navigation->MastDown() ? IDS_DOWN[lang] : IDS_UP[lang]));

				// Mast down correction
				SSmsMsg::SGetBistResp::EResult result = SSmsMsg::SGetBistResp::PASS;
				CNavigation::MastDownCorrections corrections = m_navigation->GetMastDownCorrections();
				std::string text;

				if (corrections.empty())
				{
					text = IDS_NOTPRESENT[lang];

					if (m_navigation->MastDown())
					{
						result = SSmsMsg::SGetBistResp::WARNING;
						UpdateOverallResult(SSmsMsg::SGetBistResp::WARNING, passed);
					}
				}
				else
				{
					float previous = corrections.begin()->first;

					// Want corrections at least every 60° if mast is down
					static const float MIN_CORRECTION_GAP = 60;

					if (m_navigation->MastDown() && previous + Units::DEG_PER_CIRCLE - corrections.rbegin()->first > MIN_CORRECTION_GAP)
					{
						result = SSmsMsg::SGetBistResp::WARNING;
						UpdateOverallResult(SSmsMsg::SGetBistResp::WARNING, passed);
					}

					for (CNavigation::MastDownCorrections::const_iterator bin = corrections.begin(); bin != corrections.end(); ++bin)
					{
						if (bin != corrections.begin())
						{
							if (m_navigation->MastDown() && bin->first - previous > MIN_CORRECTION_GAP)
							{
								result = SSmsMsg::SGetBistResp::WARNING;
								UpdateOverallResult(SSmsMsg::SGetBistResp::WARNING, passed);
							}

							previous = bin->first;
							text += ", ";
						}

						text += Format(bin->second, 0)+ '@' + Format(bin->first, 0);
					}
				}

				RecordBistResult(task, false, true, IDS_COMPASS, result, IDS_BIST_COMPASS_CORRECTIONS, text.c_str());
			}
		}

		// Overall
		RecordBistResult(task, false, true, IDS_COMPASS, passed);
	}
	return passed;

}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the database
//
SSmsMsg::SGetBistResp::EResult CBist::DoBistDatabase(_In_ const CTask::Task& task)
{
	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;

	try
	{
		CWeakSingleton<CPersistentData> database;
		SMetricsMsg::SServerWorkloadRequest cmd;
		cmd.clientOptions.clientId = 0;
		cmd.clientOptions.startMeasureDateTime = 0;
		cmd.clientOptions.endMeasureDateTime = SMetricsMsg::MAX_DATE;
		cmd.workloadRequest.reqMetricsMeasurements = true;
		cmd.workloadRequest.reqAvdMeasurements = true;
		cmd.workloadRequest.reqDfScanMeasurements = true;
		cmd.workloadRequest.reqSpecOccMeasurements = true;
		SMetricsMsg::SServerWorkloadResponse resp;
		database->GetWorkload(cmd, resp);

		if (resp.statusOfRequest != ErrorCodes::TASK_COMPLETED)
		{
			passed = SSmsMsg::SGetBistResp::FAIL;
			RecordBistResult(task, false, false, IDS_DATABASE, passed, IDS_BIST_NOT_DETECTED);
		}
		else
		{
			UpdateOverallResult(ListDatabaseTasks(task, resp.metricsWorkload), passed);
			UpdateOverallResult(ListDatabaseTasks(task, resp.avdWorkload), passed);
			UpdateOverallResult(ListDatabaseTasks(task, resp.specOccWorkload), passed);
			UpdateOverallResult(ListDatabaseTasks(task, resp.dfScanWorkload), passed);
		}
	}
	catch(CWeakSingleton<CPersistentData>::NoInstance&)
	{
		passed = SSmsMsg::SGetBistResp::FAIL;
		RecordBistResult(task, false, false, IDS_DATABASE, passed, IDS_BIST_NOT_DETECTED);
	}

	// Overall
	RecordBistResult(task, false, true, IDS_DATABASE, passed);

	return passed;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the Df switch
//
SSmsMsg::SGetBistResp::EResult CBist::DoBistDfSwitch(_In_ const CTask::Task& task)
{
//	auto lang = m_config->GetLangIdx();
	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;

	if (!m_switch->C51432017::IsPresent())
	{
		passed = SSmsMsg::SGetBistResp::FAIL;
		RecordDfBistResult(task, false, true, IDS_DF, passed, IDS_DF_SWITCH_NOT_DETECTED);
	}
	else
	{
		if (CAntenna::IsDfAntenna(m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get()))
		{
			//auto remoteAntType = m_switch->C51432017::GetRemoteAntType(C51432017::EPort::VUSHF);
			// do not report wrong hardware type if no bits read
			/*if (remoteAntType && static_cast<unsigned char>(m_mutableConfig.vushfConfig.antCable[0].antType) != remoteAntType)
			{
				passed = (remoteAntType != 0 ? SSmsMsg::SGetBistResp::FAIL : SSmsMsg::SGetBistResp::PASS);
				RecordDfBistResult(task, false, true, IDS_DF, passed, IDS_BIST_WRONG_HARDWARE,
					m_mutableConfig.vushfConfig.antCable[0].antName.c_str());
			}
			else
			{
				RecordDfBistResult(task, false, true, IDS_DF, SSmsMsg::SGetBistResp::PASS, IDS_BIST_SWITCH_HARDWARE,
					m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antName.c_str());
			}*/
			{
				RecordDfBistResult(task, false, true, IDS_DF, SSmsMsg::SGetBistResp::PASS, IDS_BIST_SWITCH_CONFIG_HARDWARE,
						m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antName.c_str());
			}
		}
		if (CAntenna::IsDfAntenna(m_mutableConfig.hfConfig.antCable.antenna.get()))
		{
			//auto remoteAntType = m_switch->C51432017::GetRemoteAntType(C51432017::EPort::HF);
			// do not report wrong hardware type if no bits read or antenna hardcoded to 255
			/*if (remoteAntType && (remoteAntType != 255) && static_cast<unsigned char>(m_mutableConfig.hfConfig.antCable.antType) != remoteAntType)
			{
				passed = (remoteAntType != 255 ? SSmsMsg::SGetBistResp::FAIL : SSmsMsg::SGetBistResp::PASS);
				RecordDfBistResult(task, false, true, IDS_DF, passed, IDS_BIST_WRONG_HARDWARE,
					m_mutableConfig.hfConfig.antCable.antName.c_str());
			}
			else
			{
				RecordDfBistResult(task, false, true, IDS_DF, SSmsMsg::SGetBistResp::PASS, IDS_BIST_SWITCH_HARDWARE,
					m_mutableConfig.hfConfig.antCable.antName.c_str());
			}*/
			{
				RecordDfBistResult(task, false, true, IDS_DF, SSmsMsg::SGetBistResp::PASS, IDS_BIST_SWITCH_CONFIG_HARDWARE,
					m_mutableConfig.hfConfig.antCable.antName.c_str());
			}
			auto remoteWhipFault = m_switch->C51432017::GetRemoteWhipFault();
			if (remoteWhipFault)
			{
				passed = SSmsMsg::SGetBistResp::FAIL;
				RecordDfBistResult(task, false, true, IDS_DF, SSmsMsg::SGetBistResp::FAIL, IDS_BIST_NOHFWHIP);
			}
			else
			{
				RecordDfBistResult(task, false, true, IDS_DF, SSmsMsg::SGetBistResp::PASS, IDS_BIST_HFWHIP);
			}
		}
	}

	// Overall
	RecordBistResult(task, false, true, IDS_DF, passed);

	return passed;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the Df - passband amp/phase cal
//
SSmsMsg::SGetBistResp::EResult CBist::DoBistDfPbCal(_In_ const CTask::Task& task)
{
//	auto lang = m_config->GetLangIdx();

	std::vector<SEquipCtrlMsg::SInitializeDfResp::SPbCalStatus> status;
	SSmsMsg::SGetBistResp::EResult passed = (m_pbCal->GetStatus(status) ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::FAIL);

	for (size_t i = 0; i < status.size(); ++i)
	{
		SSmsMsg::SGetBistResp::EResult freqpassed;
		if (Units::Frequency(status[i].freq).Hz<double>() < 6890000001 && Units::Frequency(status[i].freq).Hz<double>() > 6889999999) // always pass 6890 Mhz for now
			freqpassed = SSmsMsg::SGetBistResp::PASS;
		else
			freqpassed = status[i].status == ErrorCodes::SUCCESS ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::FAIL;

		RecordDfBistResult(task, false, false, IDS_DF_PBCAL,
			freqpassed,
			IDS_BIST_PBCAL, Format(Units::Frequency(status[i].freq).Hz<double>() / 1e6, 2).c_str(),
			Format(Units::Frequency(status[i].rxBw).Hz<double>() / 1e6, 1).c_str(),
			status[i].band, Format(status[i].avgRelAmp, 2).c_str(), Format(status[i].stdDevRelAmp, 2).c_str(),
			Format(status[i].avgRelDelayNsec, 1).c_str(), Format(status[i].avgRelPhase, 0).c_str(), Format(status[i].stdDevRelPhase, 0).c_str(),
			status[i].numIntegrations);
	}

	// Overall
	RecordDfBistResult(task, false, true, IDS_DF_PBCAL, passed);

	return passed;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the GPS
//
SSmsMsg::SGetBistResp::EResult CBist::DoBistGps(_In_ const CTask::Task& task)
{
	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;
	SSmsMsg::SGetBistResp::EResult result = SSmsMsg::SGetBistResp::PASS;

	std::string statusText = "OK";
	BYTE nErrorCode = 0;
	if (!m_navigation->GpsIsConnected())
	{
		nErrorCode |= 0x0001;
		statusText = "No GPS Connection";
		result = SSmsMsg::SGetBistResp::FAIL;
		passed = SSmsMsg::SGetBistResp::FAIL;
	}
//	TciGps::EStatus status = TciGps::GetStatus();
//
//	if (status != TciGps::OK)
//	{
//		if (status == TciGps::PDOP_HIGH)
//		{
//			result = SSmsMsg::SGetBistResp::WARNING;
//			UpdateOverallResult(SSmsMsg::SGetBistResp::WARNING, passed);
//		}
//		else
//		{
//			result = SSmsMsg::SGetBistResp::FAIL;
//			passed = SSmsMsg::SGetBistResp::FAIL;
//		}
//	}

	int numSatellites = m_navigation->GetNumSatellites();
	if (numSatellites == 0)
	{
		nErrorCode |= 0x0002;
		statusText = "No Satellites";
		result = SSmsMsg::SGetBistResp::FAIL;
		passed = SSmsMsg::SGetBistResp::FAIL;
	}
	double latitude;
	double longitude;
	double altitude;
	if (!m_navigation->GetPosition(latitude, longitude, altitude))
	{
		nErrorCode |= 0x0004;
		statusText = "Read position Failure";
		result = SSmsMsg::SGetBistResp::FAIL;
		passed = SSmsMsg::SGetBistResp::FAIL;
	}

	std::string timeText = "GPS";
	std::string date;
	if (!m_navigation->GetGpsTime(date))
	{
		nErrorCode |= 0x0008;
		statusText = "Read time Failure";
		timeText = "RTC";
		result = SSmsMsg::SGetBistResp::FAIL;
		passed = SSmsMsg::SGetBistResp::FAIL;
	}

	RecordBistResult(task, false, false, IDS_GPS, result, IDS_BIST_GPS, statusText.c_str(), numSatellites,
		Format(latitude, 6).c_str(), Format(longitude, 6).c_str(), Format(altitude, 1).c_str(), timeText.c_str(), date.c_str(), Format(nErrorCode).c_str());

	// Overall
	RecordBistResult(task, false, true, IDS_GPS, passed);

	return passed;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the HF DF antenna
//
SSmsMsg::SGetBistResp::EResult CBist::DoBistHfAntenna(_In_ const CTask::Task& task)
{
	auto lang = m_config->GetLangIdx();
	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;

	auto testGainMode = C2630::EGainMode::RURAL;
	auto slaveGainMode = SDfCtrlMsg::EGainMode::RURAL;

	CBistSlave bistSlave;
	// Transmit BITE tone test

	//TODO: Would like to move all of thie antenna-dependend data to CAntenna.
	struct TestHfParams
	{
		size_t index;
		unsigned long freq;
//		unsigned char atten;
		float nominal;
		float low;
	};
	static const TestHfParams TEST_PARAMS[][C72362001::NUM_ANTS + 1] =
	{
		{
			// 632
			{ 0, 27181000, -60, -70 },
			{ 1, 13590000, -20, -30 },
			{ 2, 13590000, -20, -30 },
			{ 3, 13590000, -30, -40 },
			{ 4, 13590000, -30, -40 },
			{ 5, 13590000, -40, -50 },
			{ 6, 13590000, -40, -50 },
			{ 7, 13590000, -55, -65 },
			{ 8, 13590000, -55, -65 }
		},
		{
			// 7235
			{ 0, 27181000,  -40, -50 },
			{ 1, 27181000,  -40, -50 },
			{ 2, 27181000,  -40, -50 },
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 0 }
		}
	};

	size_t type = (m_mutableConfig.hfConfig.antCable.antType == CAntenna::EAntennaType::HF_632 ? 0 : 1);
	C72362001::ESource elem = C72362001::TERM;
	float power[2];
	auto result = SSmsMsg::SGetBistResp::PASS;

	for (size_t i = 0; i < C72362001::NUM_ANTS + 1; ++i, elem == C72362001::TERM ? (elem = C72362001::ANT_1) : ++elem)
	{
		if (TEST_PARAMS[type][i].freq != 0)
		{
			Units::Frequency finalIfFreq;
			bool inverted;
			Units::FreqPair freqLimits;
			Units::Frequency radioFreq;
			size_t band;
			bool direct;

			// Tune the radio
			if (!m_radioEquip->Tune(TEST_PARAMS[type][i].freq, m_radioEquip->GetRxBandwidth(true, true), Units::Frequency(), true, CRadioEquip::EBandSelect::OPTIMUM,
				finalIfFreq, inverted, freqLimits, radioFreq, band, direct, SEquipCtrlMsg::EAnt::ANT1))
			{
				continue;
			}

			// Tune the slave rx
			std::vector<size_t> sentIndex;
			bistSlave.Init(TEST_PARAMS[type][i].freq, m_radioEquip->GetRxBandwidth(true, true), Units::Frequency(), true, CRadioEquip::EBandSelect::OPTIMUM);

			// Set the attenuator and get the expected level at the 3230 and at the A/D
			float expectedDbmAt3230;
			m_radioEquip->SetAttenForSpecifiedOutput(TEST_PARAMS[type][i].nominal, -10.f, testGainMode, expectedDbmAt3230);
			auto expectedDbm = expectedDbmAt3230 + 10.f * log10(m_digitizer->GetGainRatio(direct));
			float gain = expectedDbm - TEST_PARAMS[type][i].nominal;

			// Set the antenna swith and wait for settling
			m_switch->C72362001::SetSwitch(TEST_PARAMS[type][i].freq, C72362001::TX_BITE, C72362001::MONITOR, elem);

			unsigned char atten;
			m_radioEquip->GetAtten(atten);
//			if (i == 0)
//			{
				// Wait for slave to be tuned and request ToneDbm
				bistSlave.Request(SDfCtrlMsg::SCcdfCollect::TONEDBM, atten, slaveGainMode);
//			}
//			else
//			{
//				// Just request ToneDbm
//				bistSlave.RequestQuick(SDfCtrlMsg::SCcdfCollect::TONEDBM, atten, slaveGainMode);
//			}

			m_digitizer->MeasureToneDbm(direct, power[0]);
			std::vector<float> slavePower;
			bistSlave.GetResults(slavePower);

			power[0] -= gain;
			// TODO: Handle multiple slave case eventually
			power[1] = (!slavePower.empty() ? slavePower[0] : -std::numeric_limits<float>::infinity());

			result = SSmsMsg::SGetBistResp::PASS;
			float nominal = TEST_PARAMS[type][i].nominal - m_config->GetRfCableLoss(true, SEquipCtrlMsg::INVALID_ANT, TEST_PARAMS[type][i].freq);
			float low = TEST_PARAMS[type][i].low - m_config->GetRfCableLoss(true, SEquipCtrlMsg::INVALID_ANT, TEST_PARAMS[type][i].freq);

			// Don't fail on outer elements as the test signal is unreliable
			if (TEST_PARAMS[type][i].index < 7 && power[i == 0 ? 0 : 1] < low)
			{
				UpdateOverallResult(SSmsMsg::SGetBistResp::WARNING, passed);
				result = SSmsMsg::SGetBistResp::WARNING;
			}

			RecordDfBistResult(task, false, false, IDS_HF_ANTENNA, result, IDS_BIST_ANTPOL, (i == 0 ? IDS_MONITOR[lang] : IDS_SAMPLE[lang]),
				i + 1, IDS_VERTICAL[lang], Format(power[i == 0 ? 0 : 1], 1).c_str(), Format(nominal, 1).c_str());
		}
	}


	RecordDfBistResult(task, false, true, IDS_HF_ANTENNA, passed);

	return passed;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the License
//
SSmsMsg::SGetBistResp::EResult CBist::DoBistLicense(_In_ const CTask::Task& task)
{
	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;

	std::vector<std::string> status;
	auto license = CSingleton<CCsmsLicense>();
	auto licOk = license->GetStatus(status);
	for (size_t i = 0; i < status.size(); ++i)
	{
		RecordBistResult(task, false, true, IDS_LICENSE,
			(licOk ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::FAIL), IDS_1S, status[i].c_str());
	}
	if (licOk)
	{
		if (license->HasPrecisionTimestamp())
			RecordBistResult(task, false, true, IDS_LICENSE, SSmsMsg::SGetBistResp::PASS, IDS_BIST_PRECISIONTIME_LICENSE);
		if (license->Has80MHzRadio())
			RecordBistResult(task, false, true, IDS_LICENSE, SSmsMsg::SGetBistResp::PASS, IDS_BIST_80MHZ_LICENSE);
		if (license->HasHF())
			RecordBistResult(task, false, true, IDS_LICENSE, SSmsMsg::SGetBistResp::PASS, IDS_BIST_HF_LICENSE);
		auto val = license->HasVUHF();
		if (val > 0)
			RecordBistResult(task, false, true, IDS_LICENSE, SSmsMsg::SGetBistResp::PASS, IDS_BIST_VUSHF_LICENSE, val);
		if (license->HasShfExt())
			RecordBistResult(task, false, true, IDS_LICENSE, SSmsMsg::SGetBistResp::PASS, IDS_BIST_SHF_EXT_LICENSE);
	}
	else
	{
		passed = SSmsMsg::SGetBistResp::FAIL;
	}

	// Overall
	RecordBistResult(task, false, true, IDS_LICENSE, passed);

	return passed;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on NTP
//
SSmsMsg::SGetBistResp::EResult CBist::DoBistNtp(_In_ const CTask::Task& task)
{
	auto lang = m_config->GetLangIdx();
	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;
	SSmsMsg::SGetBistResp::EResult result = SSmsMsg::SGetBistResp::PASS;
	auto isOrNot = IDS_IS[lang];

	//std::string ntpRef = m_navigation->GetNtpRef();
	if (!m_navigation->IsNtpSyncd())
	{
		//result = SSmsMsg::SGetBistResp::WARNING;
		//passed = SSmsMsg::SGetBistResp::WARNING;
		isOrNot = IDS_ISNOT[lang];
	}
	RecordBistResult(task, false, true, IDS_NTP, result, IDS_BIST_NTP, isOrNot, m_navigation->GetOffset());

	// Overall
	RecordBistResult(task, false, true, IDS_NTP, passed);

	return passed;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the Passband Calibration
//
SSmsMsg::SGetBistResp::EResult CBist::DoBistPbCal(_In_ const CTask::Task& task)
{
	std::vector<CRadioEquip::SGainTable> status;
	SSmsMsg::SGetBistResp::EResult passed = (m_radioEquip->GetCalStatus(status) ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::FAIL);

	size_t irf = 0;
	size_t iif = 0;
	size_t iatt[4] = { 0, 0, 0, 0};
	size_t igen = 0;
	size_t itemp = 0;
	bool skippassbandcalreport = true;
	if (passed == SSmsMsg::SGetBistResp::FAIL)
		skippassbandcalreport = false; // don't skip if there are errors

	for (size_t i = 0; i < status.size(); ++i)
	{
		if (skippassbandcalreport)
			break;
		if (status[i].type == S2630Msg::RFCALDATA)
		{
			if (status[i].lowFreq == 0)
			{
				RecordBistResult(task, false, false, IDS_PBCAL, status[i].status == ErrorCodes::SUCCESS ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::FAIL,
					IDS_BIST_RFCAL, irf, Format(status[i].startValue / 1e6, 2).c_str(), Format(status[i].stepValue / 1e6, 1).c_str(),
					status[i].ifIndex, status[i].attIndexes[0], status[i].attIndexes[1], status[i].attIndexes[2], status[i].attIndexes[3]);
			}
			else
			{
				RecordBistResult(task, false, false, IDS_PBCAL, status[i].status == ErrorCodes::SUCCESS ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::FAIL,
					IDS_BIST_RFCAL1, irf, Format(status[i].startValue / 1e3, 2).c_str(), Format(status[i].stepValue / 1e3, 1).c_str(),
					Format(status[i].lowFreq / 1e3, 2).c_str(), Format(status[i].low.startValue / 1e3, 2).c_str(), Format(status[i].low.stepValue / 1e3, 1).c_str(),
					status[i].ifIndex, status[i].attIndexes[0], status[i].attIndexes[1], status[i].attIndexes[2], status[i].attIndexes[3]);
			}
			++irf;
		}
		else if (status[i].type == S2630Msg::IFCALDATA)
		{
			RecordBistResult(task, false, false, IDS_PBCAL, status[i].status == ErrorCodes::SUCCESS ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::FAIL,
				IDS_BIST_IFCAL, iif, Format(status[i].startValue / 1e6, 2).c_str(), Format(status[i].stepValue / 1e6, 1).c_str());
			++iif;
		}
		else if (status[i].type == S2630Msg::ATTCALDATA)
		{
			RecordBistResult(task, false, false, IDS_PBCAL, status[i].status == ErrorCodes::SUCCESS ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::FAIL,
				IDS_BIST_ATTCAL, status[i].subType, iatt[status[i].subType], Format(status[i].startValue, 0).c_str(), Format(status[i].stepValue, 0).c_str());
			++iatt[status[i].subType];
		}
		else if (status[i].type == S2630Msg::CALGENDATA)
		{
			RecordBistResult(task, false, false, IDS_PBCAL, status[i].status == ErrorCodes::SUCCESS ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::FAIL,
				IDS_BIST_CALGENCAL, igen, Format(status[i].startValue / 1e6, 2).c_str(), Format(status[i].stepValue / 1e6, 1).c_str());
			++igen;
		}
		else if (status[i].type == S2630Msg::TEMP_COMP)
		{
			RecordBistResult(task, false, false, IDS_PBCAL, status[i].status == ErrorCodes::SUCCESS ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::FAIL,
				IDS_BIST_TEMPCAL, itemp, Format(status[i].startValue, 2).c_str());
			++itemp;
		}
	}

	// Overall
	RecordBistResult(task, false, true, IDS_PBCAL, passed);

	return passed;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the Processor nodes
//
SSmsMsg::SGetBistResp::EResult CBist::DoBistProcessorNodes(_In_ const CTask::Task& task)
{
	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;

	if (m_config->IsProcessorMaster())
	{
		// This is a master node
		if (!m_config->IsProcessorDfMaster())
		{
			if(m_config->HasDualAux1())
			{
				//Dual Aux1 antenna can only be specified in DF system.
				passed = SSmsMsg::SGetBistResp::FAIL;
				RecordBistResult(task, false, true, IDS_PROCESSORNODES, passed, IDS_BIST_RF2_DUAL_BAND_WRONG_SYS);
			}

			if(m_config->IsProcWithAntSelectionSwitch())
			{
				// This is a single box system that has multiple antennas at its RF1 input.
				RecordBistResult(task, false, true, IDS_PROCESSORNODES, SSmsMsg::SGetBistResp::PASS, IDS_BIST_SINGLE_MULT_ANT);
				bool isOk = false;
				auto statusString = m_antSwitch->GetStatusStr(isOk);
				auto result = (isOk ? SSmsMsg::SGetBistResp::PASS : SSmsMsg::SGetBistResp::FAIL);
				UpdateOverallResult(result, passed);
				RecordBistResult(task, false, true, IDS_PROCESSORNODES, result, IDS_BIST_RFSWITCH_STATUS, statusString.c_str());
			}
			else
			{
				// This is a single box system
				RecordBistResult(task, false, true, IDS_PROCESSORNODES, SSmsMsg::SGetBistResp::PASS, IDS_BIST_SINGLEMASTER);
			}
		}
		else
		{
			const CConfig::SProcessorNodes & processorNodes = m_config->GetProcessorNodes();
			// This is a df system and this is the master
			std::string s;
			for (size_t i = 0; i < processorNodes.nodes.size(); ++i)
			{
				s += std::to_string(processorNodes.nodes[i].id);
				s += ":";
				s += processorNodes.nodes[i].ipAddress;
				s += "; ";
			}
			RecordBistResult(task, false, true, IDS_PROCESSORNODES, SSmsMsg::SGetBistResp::PASS, IDS_BIST_DFMASTER, s.c_str());

			if(m_numSlaves <= 0)
			{
				passed = SSmsMsg::SGetBistResp::FAIL;
				RecordBistResult(task, false, true, IDS_PROCESSORNODES,
						passed, IDS_BIST_NO_DFSLAVE);
			}

			if(m_config->HasDualAux1())
			{
				RecordBistResult(task, false, true, IDS_PROCESSORNODES,
						SSmsMsg::SGetBistResp::PASS, IDS_BIST_RF2_DUAL_BAND_SPECS,
						m_config->GetAux1FreqLow().Hz<unsigned long long>(),
						m_config->GetAux1FreqHigh().Hz<unsigned long long>(),
						m_config->GetSmplRF2FreqLow().Hz<unsigned long long>(),
						m_config->GetSmplRF2FreqHigh().Hz<unsigned long long>());
				auto antenna = m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_SLAVE_RF2].antenna;
				auto pAnt = (antenna ? antenna.get() : nullptr);

				if(!CAntenna::IsAntFactorLoaded(pAnt))
				{
					passed = SSmsMsg::SGetBistResp::FAIL;
					RecordBistResult(task, false, true, IDS_PROCESSORNODES,
							passed, IDS_BIST_ANT_FACTORS_NOTFOUND,
							unsigned(CConfig::ERfPort::C_SLAVE_RF2 +1), CAntenna::GetAntFactorFilename(pAnt).c_str());
				}
			}
		}
	}
	else
	{
		const CConfig::SProcessorNodes & processorNodes = m_config->GetProcessorNodes();
		// This is a slave node in a df system
		RecordBistResult(task, false, true, IDS_PROCESSORNODES, SSmsMsg::SGetBistResp::PASS, IDS_BIST_DFSLAVE, processorNodes.nodeId);
	}
	// Overall

	RecordBistResult(task, false, true, IDS_PROCESSORNODES, passed);

	return passed;

}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the restricted frequency list
//
SSmsMsg::SGetBistResp::EResult CBist::DoBistRestrictedFreq(_In_ const CTask::Task& task)
{
	if (!m_config->HasRestrictedFreq())
	{
		RecordBistResult(task, false, true, IDS_RESTRICTED_FREQ, SSmsMsg::SGetBistResp::PASS, IDS_BIST_NO_RESTRICTED_FREQ);
	}
	else
	{
		// Loop over the restricted frequency list
		size_t idx = 0;
		Units::FreqPair freqPair;
		while (m_config->GetRestrictedFreqPair(idx++, freqPair))
		{
			RecordBistResult(task, false, true, IDS_RESTRICTED_FREQ, SSmsMsg::SGetBistResp::PASS, IDS_BIST_RESTRICTED_FREQ,
				freqPair.first.Hz<unsigned long long>(), freqPair.second.Hz<unsigned long long>());
		}
	}
	// Overall
	RecordBistResult(task, false, true, IDS_RESTRICTED_FREQ, SSmsMsg::SGetBistResp::PASS);

	return SSmsMsg::SGetBistResp::PASS;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the Radio Rf Inputs (antennas)
//
SSmsMsg::SGetBistResp::EResult CBist::DoBistRfInputs(_In_ const CTask::Task& task)
{
	auto passed = SSmsMsg::SGetBistResp::PASS;

	// TODO: If available, check the actual antenna type with the config value

	// Loop over the 3 rf inputs
	for (size_t iant = 0; iant < 3; ++iant)
	{
		// Check configured cable length
		auto result = SSmsMsg::SGetBistResp::PASS;
		auto ant = m_config->GetThisSysAntCable(iant);

		if(static_cast<CConfig::ERfPort>(iant) <= CConfig::ERfPort::C_RF2)
		{
			if(!m_config->IsVushfPortAntOk(static_cast<CConfig::ERfPort>(iant)))
			{
				//Antenna specified for the RF port is not compatible.
				passed = result = SSmsMsg::SGetBistResp::FAIL;
				RecordBistResult(task, false, true, IDS_ANTENNA, result, IDS_BIST_RF_ANT_ERR,
					ant.antName.c_str(),  iant + 1);
			}

			auto antenna = m_mutableConfig.vushfConfig.antCable[iant].antenna;
			auto pAnt = (antenna ? antenna.get() : nullptr);
			if((pAnt != nullptr) && !CAntenna::IsAntFactorLoaded(pAnt))
			{
				passed = SSmsMsg::SGetBistResp::FAIL;
				RecordBistResult(task, false, true, IDS_ANTENNA,
						passed, IDS_BIST_ANT_FACTORS_NOTFOUND,
						iant+1, CAntenna::GetAntFactorFilename(pAnt).c_str());
			}

		}

		if (ant.cableLength == 0)
		{
			result = SSmsMsg::SGetBistResp::WARNING;
			UpdateOverallResult(result, passed);
		}
		if (iant == 2)
		{
			auto license = CSingleton<CCsmsLicense>();
			if (license->HasHF())
				RecordBistResult(task, false, true, IDS_ANTENNA, result, IDS_BIST_ANTENNA, iant + 1,
					ant.antName.c_str(), CConfig::SMutableConfig::CableEnumToStr(ant.cableType).c_str(),
					Format(ant.cableLength, 1).c_str());
			else
				RecordBistResult(task, false, true, IDS_ANTENNA, result, IDS_BIST_RF3ANTENNA_NOHF, iant + 1);
		}
		else
		{
			RecordBistResult(task, false, true, IDS_ANTENNA, result, IDS_BIST_ANTENNA, iant + 1,
				ant.antName.c_str(), CConfig::SMutableConfig::CableEnumToStr(ant.cableType).c_str(),
				Format(ant.cableLength, 1).c_str());
		}
	}

	// Overall
	RecordBistResult(task, false, true, IDS_ANTENNA, passed);

	return passed;
}

//////////////////////////////////////////////////////////////////////
/// Perform SHFExtension BIST by querying for various status.
///
///@param[in] task: Task associated with the BIST.
///
///@returns
/// SGetBistResp::PASS: No error in SHFExtension BIST.
/// SGetBistResp::WARN: Error detected in SHFExtension BIST.
///
SSmsMsg::SGetBistResp::EResult CBist::DoBistShfExt(_In_ const CTask::Task & task)
{
	auto lang = m_config->GetLangIdx();
	auto shfExtStatus = CShfExt::GetShfExtStatus();
	auto & shfExtId = CShfExt::GetId();
	auto result = OutputShfExtStatus(task, IDS_BLK_CONVERTER, shfExtStatus,
	                                 shfExtId, SSmsMsg::SGetBistResp::PASS);
	auto passed = result;

	if (shfExtStatus == CConfig::EShfExtStatus::ANALYZER_CONNECTED ||
		shfExtStatus == CConfig::EShfExtStatus::INCOMPATIBLE_ANALYZER ||
		shfExtStatus == CConfig::EShfExtStatus::ANALYZER_WRONG_MODE)
	{
		auto & optStr = CShfExt::GetOptStr();
		result = SSmsMsg::SGetBistResp::PASS;
		RecordBistResult(task, false, true, IDS_BLK_CONVERTER, result, IDS_STATUS, IDS_OPTION[lang], optStr.c_str());
	}

	RecordBistResult(task, false, true, IDS_BLK_CONVERTER, passed);
	return(passed);
}

//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the system (versions, etc.)
//
SSmsMsg::SGetBistResp::EResult CBist::DoBistSystem(_In_ const CTask::Task& task)
{
	auto lang = m_config->GetLangIdx();
	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;

	RecordBistResult(task, false, true, IDS_BIST, SSmsMsg::SGetBistResp::PASS, IDS_BIST_ID, m_config->GetServerName().c_str(),
		m_mutableConfig.miscConfig.partNum, m_mutableConfig.miscConfig.rev, m_mutableConfig.miscConfig.serialNumber, Utility::CurrentTimeAsString().c_str());
	RecordBistResult(task, false, true, IDS_BIST, SSmsMsg::SGetBistResp::PASS, IDS_BIST_CUSTOMER, m_mutableConfig.miscConfig.customer);
	if (Utility::GetSoftwareCompatiblFlag())
	{
		RecordBistResult(task, false, true, IDS_BIST, SSmsMsg::SGetBistResp::PASS, IDS_BIST_SW_VERSION, VersionInfo::ProductName,
				VersionInfo::ProductVersion, VersionInfo::FileVersion);
	}
	else
	{
		passed = SSmsMsg::SGetBistResp::FAIL;
		RecordBistResult(task, false, true, IDS_BIST, SSmsMsg::SGetBistResp::FAIL, IDS_BIST_SW_VERSIONMISMATCH, VersionInfo::ProductName,
				VersionInfo::ProductVersion, VersionInfo::FileVersion);
	}
	RecordBistResult(task, false, true, IDS_BIST, SSmsMsg::SGetBistResp::PASS, IDS_BIST_EXEFILE, m_config->GetExeName().c_str(),
			m_config->GetExeTime().c_str());
	SSmsMsg::SGetBistResp::EResult FailResult;
	if (m_config->IsDDRSystem())
		FailResult = SSmsMsg::SGetBistResp::WARNING;
	else
		FailResult = SSmsMsg::SGetBistResp::FAIL;
	RecordBistResult(task, false, true, IDS_BIST, (m_config->GetMediaReadWrite() ? SSmsMsg::SGetBistResp::PASS : FailResult),
		IDS_BIST_MEDIARW, (m_config->GetMediaReadWrite() ? IDS_YES[lang] : IDS_NO[lang]));

	std::string ntpVersion = m_navigation->GetNtpVersion();
	RecordBistResult(task, false, true, IDS_BIST, SSmsMsg::SGetBistResp::PASS, IDS_BIST_NTPVERSION, ntpVersion.c_str());

	CConfig::SSysInfo sysinfo = m_config->GetSysInfo();
	RecordBistResult(task, false, true, IDS_BIST, SSmsMsg::SGetBistResp::PASS, IDS_BIST_SYSINFO, sysinfo.sysname.c_str(),
		sysinfo.release.c_str(), sysinfo.version.c_str(), sysinfo.machine.c_str());
	RecordBistResult(task, false, true, IDS_BIST, SSmsMsg::SGetBistResp::PASS, IDS_BIST_SYSINFO2, sysinfo.product.c_str(),
		sysinfo.fwversion.c_str());
	RecordBistResult(task, false, true, IDS_BIST, SSmsMsg::SGetBistResp::PASS, IDS_BIST_GAINEQUALIZATION,
		(m_config->HasGainEqualization() ? IDS_YES[lang] : IDS_NO[lang]));
	RecordBistResult(task, false, true, IDS_BIST, SSmsMsg::SGetBistResp::PASS, IDS_BIST_ENHANCEDDYNAMICRANGE,
		(m_config->HasEnhancedDynamicRange() ? IDS_YES[lang] : IDS_NO[lang]));

	std::string ipAddress;
	std::string macAddress;
	m_config->GetNetworkInfo(ipAddress, macAddress);
	RecordBistResult(task, false, true, IDS_BIST, SSmsMsg::SGetBistResp::PASS, IDS_BIST_NETWORK, ipAddress.c_str(), macAddress.c_str());

	auto eeprom = m_config->GetEeprom();
	char macBuf[18];
	snprintf(macBuf, 18, "%02x:%02x:%02x:%02x:%02x:%02x",
		eeprom.macAddress[5], eeprom.macAddress[4], eeprom.macAddress[3], eeprom.macAddress[2], eeprom.macAddress[1],eeprom.macAddress[0]);
	RecordBistResult(task, false, true, IDS_BIST, SSmsMsg::SGetBistResp::PASS, IDS_BIST_EEPROM, macBuf);

	return passed;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the VUSHF DF antenna
//
SSmsMsg::SGetBistResp::EResult CBist::DoBistUshfAntenna(_In_ const CTask::Task& task)
{
	auto lang = m_config->GetLangIdx();
	SSmsMsg::SGetBistResp::EResult overall = SSmsMsg::SGetBistResp::PASS;
	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;

	auto testGainMode = C2630::EGainMode::RURAL;
	auto slaveGainMode = SDfCtrlMsg::EGainMode::RURAL;
	m_senddetail = false;

	CBistSlave bistSlave;

	// Transmit BITE tone test
    //@note: Parameters structure used for all 647 & 645-8.

	// TODO: Would like to move all of this antenna-dependent data to CAntenna.
	struct TestUshfParams
	{
		const char* const* bandId;
		C7234201102::EDfElemBandSel band;
		size_t calFreqIndex;
		float nominal;
		bool isHorizon;
	};

	static const TestUshfParams TEST641_PARAMS[] =
	{
		{ IDS_UHF_ANTENNA, C7234201102::UHF, 0, -34.0f, false }
	};

	static const TestUshfParams TEST643_3_PARAMS[] =
	{
//		{ IDS_UHF_ANTENNA, C7234201102::UHF, 0, -40.0f, false },
//		{ IDS_UHF_ANTENNA, C7234201102::UHF, 0, -52.0f, true }
		{ IDS_UHF_ANTENNA, C7234201102::UHF, 0, -37.0f, false },
		{ IDS_UHF_ANTENNA, C7234201102::UHF, 0, -43.0f, true }
	};

	static const TestUshfParams TEST645_8_PARAMS[] =
	{
		{ IDS_UHF_ANTENNA, C7234201102::UHF, 0, -40.0f, false },
		{ IDS_SHF_ANTENNA, C7234201102::SHF, 1, -50.0f, false }
	};

	///@note: The nominal value is 5 dB lower than SMS to fit with the
	///		  empirical test result when 647 is connected to csms. In addition
	///		  this value is also checked against 1500 BIST result of 1 of 647M in SMS,
	///		  if SMS were to use this value, it would not fail its BIST either.
	static const TestUshfParams TEST647_PARAMS[] =
	{
		{ IDS_UHF_ANTENNA, C7234201102::UHF, 0, -35.0f, false },
		{ IDS_SHF_ANTENNA, C7234201102::SHF, 1, -55.0f, false }
	};

	static const TestUshfParams TEST647D_PARAMS[] =
	{
		{ IDS_UHF_ANTENNA, C7234201102::UHF, 0, -37.0f, false },
		//647-D only supports up to UHF for Horizontal polarization,
		// therefore SHF testing is performed on Vertical polarization only.
		{ IDS_UHF_ANTENNA, C7234201102::UHF, 0, -43.0f, true },
		{ IDS_SHF_ANTENNA, C7234201102::SHF, 1, -56.0f, false }
	};

	static const TestUshfParams TEST649_8_PARAMS[] =
	{
		//UHF of 649-8(type 2) antenna consists of 5 elements.
		{ IDS_UHF_ANTENNA, C7234201102::UHF, 0, -52.0f, false },
		{ IDS_SHF_ANTENNA, C7234201102::SHF, 1, -55.0f, false }
	};

	SSmsMsg::SGetBistResp::EResult uhfPassed = SSmsMsg::SGetBistResp::PASS;
	SSmsMsg::SGetBistResp::EResult shfPassed = SSmsMsg::SGetBistResp::PASS;

	auto antenna = m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna;
	auto pAnt = (antenna ? antenna.get() : nullptr);

	//Test only vertical unless it is a 647-D Antenna.
	size_t testCount = 0;		// TODO: CAntenna::GetTestCount(pAnt);
	TestUshfParams const* pParams = nullptr;

	if (m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antType == CAntenna::EAntennaType::USHF_647D ||
		m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antType == CAntenna::EAntennaType::USHF_647D_COMPASS)
	{
		testCount = std::extent<decltype(TEST647D_PARAMS)>::value;
		pParams = &TEST647D_PARAMS[0];
	}
	else if (m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antType == CAntenna::EAntennaType::UHF_645_8 ||
			m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antType == CAntenna::EAntennaType::UHF_645_8_COMPASS)
	{
		testCount =  std::extent<decltype(TEST645_8_PARAMS)>::value;
		pParams = &TEST645_8_PARAMS[0];
	}
	else if (m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antType == CAntenna::EAntennaType::USHF_649_8 ||
			m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antType == CAntenna::EAntennaType::USHF_649_8_COMPASS)
	{
		testCount = std::extent<decltype(TEST649_8_PARAMS)>::value;
		pParams = &TEST649_8_PARAMS[CConfig::ERfPort::C_RF1];
	}
	else if (m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antType == CAntenna::EAntennaType::USHF_647 ||
			m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antType == CAntenna::EAntennaType::USHF_647_COMPASS)
	{
		testCount =  std::extent<decltype(TEST647_PARAMS)>::value;
		pParams = &TEST647_PARAMS[0];
	}
	else if (m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antType == CAntenna::EAntennaType::UHF_641 ||
			m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antType == CAntenna::EAntennaType::UHF_641_COMPASS)
	{
		testCount =  std::extent<decltype(TEST641_PARAMS)>::value;
		pParams = &TEST641_PARAMS[0];
	}
	else if (m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antType == CAntenna::EAntennaType::UHF_643_3 ||
			m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antType == CAntenna::EAntennaType::UHF_643_3_COMPASS)
	{
		testCount =  std::extent<decltype(TEST643_3_PARAMS)>::value;
		pParams = &TEST643_3_PARAMS[0];
	}

	for (size_t i = 0; i < testCount; ++i, ++pParams)
	{
		// TODO: CAntenna::GetTestParams(i, calFreqIndex, nominal, isHorizon)
		auto rxFreq = m_switch->C7234201102::GetCalFreq(pParams->calFreqIndex);
		float adjustedDbm = -m_config->GetRfCableLoss(false, SEquipCtrlMsg::ANT1, rxFreq);
		float realNominal = pParams->nominal;
		float nominal = realNominal + adjustedDbm;
		bool chkBiteTone = true;
		float lowerRange, upperRange;
		lowerRange = upperRange = 10.0f;
		auto skipElement = CAntenna::HasLowbandSkippedElement(pAnt);
		if((pParams->band == C7234201102::UHF) && skipElement)
		{
			lowerRange = upperRange = 13.0f;
			chkBiteTone = false;
		}

		float low = nominal - lowerRange;
		float high = nominal + upperRange;

		// Tune the radio
		Units::Frequency finalIfFreq;
		bool inverted;
		Units::FreqPair freqLimits;
		Units::Frequency radioFreq;
		size_t band;
		bool direct;
		if (!m_radioEquip->Tune(rxFreq, m_radioEquip->GetRxBandwidth(false, true), Units::Frequency(), false, CRadioEquip::EBandSelect::OPTIMUM,
			finalIfFreq, inverted, freqLimits, radioFreq, band, direct, SEquipCtrlMsg::EAnt::ANT1))
		{
			break;
		}

		RecordDfBistResult(task, false, m_senddetail, pParams->bandId, SSmsMsg::SGetBistResp::PASS, IDS_BIST_NOMINAL_ADJUSTMENT,
			Format(adjustedDbm, 1).c_str());
		RecordDfBistResult(task, false, m_senddetail, pParams->bandId, SSmsMsg::SGetBistResp::PASS, IDS_BIST_NOMINAL_MIN_MAX,
			Format(realNominal, 1).c_str(), Format(low, 1).c_str(), Format(high, 1).c_str());

		// Tune the slave rx
		std::vector<size_t> sentIndex;
		bistSlave.Init(rxFreq, m_radioEquip->GetRxBandwidth(false, true), Units::Frequency(), false, CRadioEquip::EBandSelect::OPTIMUM);

		// Set the attenuator and get the expected level at the 3230 and at the A/D
		float expectedDbmAt3230;
		m_radioEquip->SetAttenForSpecifiedOutput(nominal, -10.f, testGainMode, expectedDbmAt3230);
		auto expectedDbm = expectedDbmAt3230 + 10.f * log10(m_digitizer->GetGainRatio(direct));
		float gain = expectedDbm - nominal;
		passed = SSmsMsg::SGetBistResp::PASS;

		// Set the antenna swith and wait for settling
		m_switch->C7234201102::SetSwitch(rxFreq, C7234201102::TX_BITE, C7234201102::TERM, C7234201102::TERM, pParams->band);

		float maxPower = float(-HUGE_VAL);
		float minPower = float(HUGE_VAL);
		SSmsMsg::SGetBistResp::EResult result;

		C7234201102::ESource elem;
		bool isHorizon = pParams->isHorizon;

		if (isHorizon)
		{
			elem = C7234201102::ANT_1H;
		}
		else
		{
			elem = C7234201102::ANT_1;
		}

		size_t antId = 1;

		auto numAnts = CAntenna::NumAnts(pAnt, rxFreq);
		for (size_t j = 0; j < numAnts; ++j, ++elem)
		{
			m_switch->C7234201102::SetSwitch(rxFreq, C7234201102::TX_BITE, C7234201102::TERM, elem, pParams->band);

			unsigned char atten;
			m_radioEquip->GetAtten(atten);
			if (j == 0)
			{
				// Wait for slave to be tuned and request ToneDbm
				bistSlave.Request(SDfCtrlMsg::SCcdfCollect::TONEDBM, atten, slaveGainMode);
			}
			else
			{
				// Just request ToneDbm
				bistSlave.RequestQuick(SDfCtrlMsg::SCcdfCollect::TONEDBM, atten, slaveGainMode);
			}

			float power[2];
			m_digitizer->MeasureToneDbm(direct, power[0]);

			std::vector<float> slavePower;
			bistSlave.GetResults(slavePower);

			power[0] -= gain;
			// TODO: Handle multiple slave case eventually
			power[1] = (!slavePower.empty() ? slavePower[0] : -std::numeric_limits<float>::infinity());

			if (power[1] > maxPower)
			{
				maxPower = power[1];
			}

			if (power[1] < minPower)
			{
				minPower = power[1];
			}

			if (power[1] < low || power[1] > high)
			{
				result = SSmsMsg::SGetBistResp::FAIL;
				passed = SSmsMsg::SGetBistResp::FAIL;
			}
			else
			{
				result = SSmsMsg::SGetBistResp::PASS;
			}

			RecordDfBistResult(task, false, false, pParams->bandId, result, IDS_BIST_ANTPOL, IDS_SAMPLE[lang],
				antId, (isHorizon ? IDS_HORIZONTAL[lang] : IDS_VERTICAL[lang]), Format(power[1], 1).c_str(), Format(nominal, 1).c_str());

			// Do not test the opposite Antenna if we are in SHF band or
			// 5-element UHF 649-8(Type 2) antenna.
			if (pParams->band != C7234201102::SHF && !skipElement)
			{
				float oppositeMaxPower = low;

				if (power[0] >= oppositeMaxPower)
				{
					result = SSmsMsg::SGetBistResp::FAIL;
					passed = SSmsMsg::SGetBistResp::FAIL;
				}
				else
				{
					result = SSmsMsg::SGetBistResp::PASS;
				}

				RecordDfBistResult(task, false, false, pParams->bandId, result, IDS_BIST_ANTPOL, IDS_OPPOSITE[lang],
					antId, (isHorizon ? IDS_HORIZONTAL[lang] : IDS_VERTICAL[lang]), Format(power[0], 1).c_str(), Format(oppositeMaxPower, 1).c_str());
			}

			++antId;

			//Skip the next switch input if low band (UHF) has 5 elements that is mapped to 9 elements switch.
			if(skipElement && (pParams->band == C7234201102::UHF))
			{
				 ++elem;
			}
		}

		result = SSmsMsg::SGetBistResp::PASS;

		if( chkBiteTone )
		{
			if (maxPower - minPower > 10)
			{
				result = SSmsMsg::SGetBistResp::FAIL;
				passed = SSmsMsg::SGetBistResp::FAIL;
			}
			else if (maxPower - minPower > 6)
			{
				result = SSmsMsg::SGetBistResp::WARNING;
				UpdateOverallResult(SSmsMsg::SGetBistResp::WARNING, passed);
			}
			else
			{
				result = SSmsMsg::SGetBistResp::PASS;
			}

			RecordDfBistResult(task, false, false, pParams->bandId, result, IDS_BIST_ANTVARPOL,
				(isHorizon ? IDS_HORIZONTAL[lang] : IDS_VERTICAL[lang]), Format(maxPower - minPower, 1).c_str());
		}

		if (pParams->band == C7234201102::UHF)
		{
			//Only update if it fails.
			if (passed == SSmsMsg::SGetBistResp::FAIL)
				uhfPassed = passed;
		}
		else
		{
			//Only update if it fails.
			if (passed == SSmsMsg::SGetBistResp::FAIL)
				shfPassed = passed;
		}
		passed = SSmsMsg::SGetBistResp::PASS;
	}

	//Record BIST summary.
	RecordDfBistResult(task, false, true, IDS_UHF_ANTENNA, uhfPassed);
	UpdateOverallResult(uhfPassed, overall);
	RecordDfBistResult(task, false, true, IDS_SHF_ANTENNA, shfPassed);
	UpdateOverallResult(shfPassed, overall);

	m_switch->C7234201102::SetSwitch(0, C7234201102::RECEIVE, C7234201102::TERM, C7234201102::TERM);

	return overall;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test on the Watchdog
//
SSmsMsg::SGetBistResp::EResult CBist::DoBistWatchdog(_In_ const CTask::Task& task)
{
	auto lang = m_config->GetLangIdx();
	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;
	SSmsMsg::SGetBistResp::EResult result = SSmsMsg::SGetBistResp::PASS;
	CWatchdog::SStatus status = m_watchdog->GetStatus();

	if (m_watchdog->IsWatchUsed() && !status.watchdogEnabled)
	{
		result = SSmsMsg::SGetBistResp::FAIL;
		passed = SSmsMsg::SGetBistResp::FAIL;
	}

	RecordBistResult(task, false, true, IDS_WATCHDOG, result, IDS_BIST_WATCHDOG_STATUS, (status.watchdogEnabled ? IDS_IS[lang] : IDS_ISNOT[lang]));
	RecordBistResult(task, false, true, IDS_WATCHDOG, passed);

	return result;
}


//////////////////////////////////////////////////////////////////////
//
// Helper
//
std::string CBist::Format(DATE date) const
{
	return Units::Timestamp(date).Format(false);
}


//////////////////////////////////////////////////////////////////////
//
// Helper
//
std::string CBist::Format(unsigned long x) const
{
	char str[12];
	snprintf(str, 12, "%08lx", x);
	std::string text(str);
	return text;
}

/////////////////////////////////////////////////////////////////////
//
// Helper
//
std::string CBist::Format(BYTE x) const
{
	char str[12];
	snprintf(str, 12, "%02x", x);
	std::string text(str);
	return text;
}

//////////////////////////////////////////////////////////////////////
//
// Helper
//
std::string CBist::Format(double x, unsigned int places) const
{
	std::string text;

	switch (std::fpclassify(x))
	{
	case FP_INFINITE:
		text = (std::signbit(x) ? "(undetected)" : "(overload)");
		break;

	case FP_NAN:
		text = "(unknown)";
		break;

	default:
		text = static_cast<std::ostringstream&>(std::ostringstream() << std::fixed << std::setprecision(places) << x).str();
		break;
	}

	return text;
}


//////////////////////////////////////////////////////////////////////
//
// Get the last results
//
const CBist::BistResults CBist::GetBistResults(void)
{
	CSharedLockGuard lock(m_critSect);
	if (m_bistResults.size() == 1)
	{
		return m_bistResults[0];
	}

	// Make sure we have results from the slaves, if available
	std::vector<size_t> bistAllIndex(m_config->GetProcessorNodes().nodes.size());
	for (size_t i = 0; i < bistAllIndex.size(); ++i)
	{
		bistAllIndex[i] = i;
	}
	std::vector<SBistResult> allResults;
	if (CSingleton<CProcessorNode>()->AreNodesBistDone(bistAllIndex, allResults))
	{
		TRACE("GetBistResults: from slaves\n");
//		auto& bistResults = m_bistResults[1];	// Other nodes
		m_bistResults[1] = allResults;
//		for (size_t i = 0; i < allResults.size(); ++i)
//		{
//			SBistResult bistResult;
//			bistResult = allResults[i];
//			bistResult.result = allResults[i].result;
//			bistResult.test = allResults[i].test;
//			bistResult.text = allResults[i].text;
//			bistResult.last = allResults[i].last;
//			bistResult.send = allResults[i].send;
//			bistResults.push_back(allResults[i]);
//		}
	}

	// put last entry (Built-In Self Test as last item in order for Scorpio to use as root node?)
	if (m_bistResults.size() > 1)
	{

		if (m_bistResults[0][m_bistResults[0].size()-1].test.compare(std::u16string(u"Built-In Self Test[0]")) == 0)
		{
			m_bistResults[m_bistResults.size()-1].push_back(m_bistResults[0][m_bistResults[0].size()-1]);
			m_bistResults[0].pop_back();
		}
	}

	// combine all results
	auto res = m_bistResults[0];

	for (size_t i = 1; i < m_bistResults.size(); ++i)
	{
		res.insert(res.end(), m_bistResults[i].begin(), m_bistResults[i].end());
	}
	return res;
}


//////////////////////////////////////////////////////////////////////
//
// List database task status
//
SSmsMsg::SGetBistResp::EResult CBist::ListDatabaseTasks(_In_ const CTask::Task& task, _In_ const SMetricsMsg::SServerWorkloadResponse::SMeasWorkload& workload)
{
	std::string UNKNOWN("(unknown)");

	SSmsMsg::SGetBistResp::EResult passed = SSmsMsg::SGetBistResp::PASS;

	std::string type;
	switch(workload.measType)
	{
	case SMetricsMsg::SServerWorkloadResponse::SMeasWorkload::DFSCAN:
		type = "Scan DF task";		// put these in string table
		break;
	case SMetricsMsg::SServerWorkloadResponse::SMeasWorkload::OCCUPANCY:
		type = "Occupancy task";
		break;
	case SMetricsMsg::SServerWorkloadResponse::SMeasWorkload::AVD:
		type = "AVD task";
		break;
	case SMetricsMsg::SServerWorkloadResponse::SMeasWorkload::METRICS:
		type = "Metrics task";
		break;

	default:
		THROW_LOGIC_ERROR();
	}

	for(size_t meas = 0; meas < workload.numMeasurements; ++meas)
	{
		SSmsMsg::SGetBistResp::EResult result = SSmsMsg::SGetBistResp::PASS;

		std::string state;
		switch(workload.info[meas].state)
		{
		default:
		case SMetricsMsg::SMeasureCtrlMsgStateResponse::UNKNOWN:
			state = "UNKNOWN";
			break;
		case SMetricsMsg::SMeasureCtrlMsgStateResponse::FAILED:
			result = SSmsMsg::SGetBistResp::WARNING;
			state = "FAILED";
			break;
		case SMetricsMsg::SMeasureCtrlMsgStateResponse::TERMINATED:
			state = "terminated";
			break;
		case SMetricsMsg::SMeasureCtrlMsgStateResponse::COMPLETED:
			state = "completed";
			break;
		case SMetricsMsg::SMeasureCtrlMsgStateResponse::SUSPENDED:
			state = "suspended";
			break;
		case SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING:
			state = "running";
			break;
		case SMetricsMsg::SMeasureCtrlMsgStateResponse::RESUMED:
			state = "resumed";
			break;
		case SMetricsMsg::SMeasureCtrlMsgStateResponse::IDLE:
			state = "scheduled";
			break;
		}

		RecordBistResult(task, false, false, IDS_DATABASE, result, IDS_BIST_DATABASE_TASK, type.c_str(),
			workload.info[meas].clientName, Format(workload.info[meas].startTime).c_str(),
			Format(workload.info[meas].endTime).c_str(), state.c_str());
		UpdateOverallResult(result, passed);
	}

	return passed;
}


/////////////////////////////////////////////////////////////////////
//
// Set an individual BIST result
//
void CBist::RecordBistResult(_In_ const CTask::Task& task, bool last, bool send, const char* const testName[], SSmsMsg::SGetBistResp::EResult result,
	const char* const textStr[], ...)
{
	if(textStr != nullptr)
	{
		va_list args;
		va_start(args, textStr);
		RecordBistResult(0, task, last, send, testName, result, textStr, args);
		va_end(args);
	}
	else
	{
		RecordBistResult(0, task, last, send, testName, result);
	}
}

void CBist::RecordDfBistResult(_In_ const CTask::Task& task, bool last, bool send, const char* const testName[], SSmsMsg::SGetBistResp::EResult result,
	const char* const textStr[], ...)
{
	if(textStr != nullptr)
	{
		va_list args;
		va_start(args, textStr);
		RecordBistResult(2, task, last, send, testName, result, textStr, args);
		va_end(args);
	}
	else
	{
		RecordBistResult(2, task, last, send, testName, result);
	}
}

void CBist::RecordBistResult(size_t resIndex, _In_ const CTask::Task& task, bool last, bool send, const char* const testName[], SSmsMsg::SGetBistResp::EResult result,
	const char* const textStr[], va_list args)
{
	// Build bistResult
	auto lang = m_config->GetLangIdx();
	if (lang > NUM_LANGS) lang = 0;

	// Fill out result
	SBistResult bistResult;
	bistResult.result = result;

	Utility::ToU16String(testName[lang], 256, bistResult.test);

	// Append node id to test name
	char nodeChar[6];
	snprintf(nodeChar, 6, "[%lu]", (m_config->GetProcessorNodes()).nodeId);
	std::u16string nodeStr;
	Utility::ToU16String(nodeChar, 6, nodeStr);
	bistResult.test += nodeStr;

	char textName[256];
	if(textStr != nullptr)
	{
//		va_list args;
//		va_start(args, textStr);
		vsnprintf(textName, 256, textStr[lang], args);
//		va_end(args);
		Utility::ToU16String(textName, 256, bistResult.text);
	}

	auto logStr = std::string(testName[0]);		// Always English
	if(textStr != nullptr)
	{
		logStr += ": ";
		logStr += textName;
	}

	bistResult.last = last;
	bistResult.send = send;

	// Save last result
	if (resIndex < m_bistResults.size())
	{
		auto& bistResults = m_bistResults[resIndex];	// This box or DF
		bistResults.push_back(bistResult);
	}
	RecordBistResult(task, last, send, bistResult, logStr);
	return;
}

void CBist::RecordBistResult(_In_ const CTask::Task& task, bool last, bool send, _In_ const SBistResult& bistResult, _In_ const std::string& logStr)
{
	if(task->m_msgHdr.msgSubType == SEquipCtrlMsg::GET_DIAGNOSTICS || bistResult.result != SSmsMsg::SGetBistResp::PASS || send || last)
	{
		// skip passband cal containing too many entries
		/*if (!send && !last)
		{
			if (bistResult.test.find(u"Passband Calibration") != std::u16string::npos)
			{
				return;
			}
		}*/

		// Send to client
		SendBistResult(task, bistResult, last);

		// Log
		if (!logStr.empty())
		{
			switch(bistResult.result)
			{
			case SEquipCtrlMsg::SGetBistResp::PASS:
				CLog::Log(CLog::INFORMATION, logStr.c_str());
				break;

			case SEquipCtrlMsg::SGetBistResp::FAIL:
				CLog::Log(CLog::ERRORS, logStr.c_str());
				break;

			case SEquipCtrlMsg::SGetBistResp::WARNING:
				CLog::Log(CLog::WARNINGS, logStr.c_str());
				break;

			default:
				break;
			}
		}
	}

	if (last)
	{
		switch(bistResult.result)
		{
		case SEquipCtrlMsg::SGetBistResp::PASS:
			m_bistSummary = SSmsMsg::PASS_STAT;
			break;

		case SEquipCtrlMsg::SGetBistResp::WARNING:
			m_bistSummary = SSmsMsg::WARNING_STAT;
			break;

		case SEquipCtrlMsg::SGetBistResp::FAIL:
			m_bistSummary = SSmsMsg::FAIL_STAT;
			break;

		default:
			ASSERT( false );
		}

//		SaveDiagnosticsFile();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
/// Output SHF Extension status.
///
///@param[in] task: Task associated with the SHF Extension BIST.
///
///@param[in] testName: BIST category name.
///
///@param[in] shfExtStat: Has the status of SHF Extension.
///
///@param[in] idStr: Has the SHF Extension id String.
/// @note: If the string is empty, SHF will be output.
///
///@param[in] passed:
/// SGetBistResp::PASS: No error detected with SHF Extension version.
/// SGetBistResp::FAIL: Error detected with SHF Extension version.
///
///@returns
/// SGetBistResp::PASS: SHF Extension is connected.
/// SGetBistResp::FAIL: Error in connecting to SHF Extension.
///
SSmsMsg::SGetBistResp::EResult CBist::OutputShfExtStatus(_In_ const CTask::Task& task, const char* const testName[], CConfig::EShfExtStatus shfExtStat,
	                                                 _In_ const std::string & idStr,
	                                  		_In_ SSmsMsg::SGetBistResp::EResult passed)
{
	auto lang = m_config->GetLangIdx();

	//Should only be PASS or FAIL
	ASSERT(passed != SSmsMsg::SGetBistResp::WARNING);
	const char * pStatusStr(IDS_UNKNOWN[lang]);
	auto result = SSmsMsg::SGetBistResp::FAIL;


	switch (shfExtStat)
	{
	case CConfig::EShfExtStatus::ANALYZER_CONNECTED:
		result = SSmsMsg::SGetBistResp::PASS;
		pStatusStr = IDS_ENABLE[lang];
		break;

	case CConfig::EShfExtStatus::INCOMPATIBLE_ANALYZER:
		pStatusStr = IDS_SHFEXT_ANALYZER_ERR[lang];
		break;

	case CConfig::EShfExtStatus::ANALYZER_NOT_FOUND:
		pStatusStr = IDS_SHF_EXT_COMM_ERR[lang];
		break;

	case CConfig::EShfExtStatus::ANALYZER_WRONG_MODE:
		pStatusStr = IDS_SHFEXT_ANALYZER_MODE_ERR[lang];
		break;

	default:
		ASSERT(false);
		break;
	}

	if( idStr.empty() && passed == SSmsMsg::SGetBistResp::PASS )
	{
		RecordBistResult(task, false, true, testName, result, IDS_STATUS,
		                 IDS_SHF[lang], pStatusStr);
	}
	else
	{
		auto localResult = SSmsMsg::SGetBistResp::PASS;

		if( result == SSmsMsg::SGetBistResp::FAIL && passed == SSmsMsg::SGetBistResp::PASS )
		{
			result = localResult = SSmsMsg::SGetBistResp::WARNING;
		}
		else if( passed == SSmsMsg::SGetBistResp::FAIL )
		{
			localResult = SSmsMsg::SGetBistResp::FAIL;
		}

		RecordBistResult(task, false, true, testName, localResult, IDS_STATUS,
				pStatusStr, idStr.c_str());
	}

	return(result);
}

/////////////////////////////////////////////////////////////////////
//
// Send a BIST result message
//
void CBist::SendBistResult(_In_ const CTask::Task& task, _In_ const SBistResult& bistResult, bool last) const
{
	if(task->m_source != nullptr)
	{
		// Send response
		size_t textLen = bistResult.test.size() + bistResult.text.size() + 2; // Includes tab and null
		std::shared_ptr<SEquipCtrlMsg> resp(new(offsetof(SEquipCtrlMsg, body.getBistResp.text) + textLen * sizeof(char16_t)) SEquipCtrlMsg);
		resp->hdr = task->m_msgHdr;

		switch(task->m_msgHdr.msgSubType)
		{
		case SEquipCtrlMsg::GET_BIST:
			resp->hdr.msgSubType = SEquipCtrlMsg::GET_BIST_RESPONSE;
			break;

		case SEquipCtrlMsg::GET_DIAGNOSTICS:
			resp->hdr.msgSubType = SEquipCtrlMsg::GET_DIAGNOSTICS_RESPONSE;
			break;

		case SEquipCtrlMsg::GET_BIST_RESULT:
			resp->hdr.msgSubType = SEquipCtrlMsg::GET_BIST_RESULT_RESPONSE;
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		resp->hdr.bodySize = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SGetBistResp, text) + textLen * sizeof(char16_t));
		resp->body.getBistResp.result = bistResult.result;
		resp->body.getBistResp.textLen = textLen;
		resp->body.getBistResp.last = last;
//		swprintf_s(resp->body.getBistResp.text, textLen, L"%s\t%s", bistResult.test.c_str(), bistResult.text.c_str());
		std::u16string temp(bistResult.test);
		temp += u"\t";
		temp += bistResult.text;
//		printf("%u %u %u\n", bistResult.test.size(), bistResult.text.size(), temp.size());
		//wprintf(L"diag test %s   text %s   last %d", bistResult.test.c_str(), bistResult.text.c_str(), last);
		//std::wcout << "diag test " <<  bistResult.test.c_str() << " text " << bistResult.text.c_str() << "last " << (wchar_t) last << std::endl;
		//CLog::Log(CLog::INFORMATION, _T("test %s text %s  last %d"), bistResult.test.c_str(), bistResult.text.c_str(), last);

		memcpy(resp->body.getBistResp.text, temp.c_str(), (temp.size() + 1) * sizeof(char16_t));
		// Only log the last message
		if (m_config->IsProcessorMaster())
		{
			CEquipControl::Send(task->m_source, *resp, (last ? _T("GET_(BIST/DIAGNOSTICS/BIST_RESULT)_RESPONSE(last)") : nullptr));
		}
		else
		{
			// This is a slave. Send result to master.
			SDfCtrlMsg ctrlMsg;
			ctrlMsg.hdr = task->m_msgHdr;
			ctrlMsg.hdr.msgType = SDfCtrlMsg::INTERNAL_DF_CTRL;
			ctrlMsg.hdr.msgSubType = SDfCtrlMsg::DO_BIST_RESP;
			ctrlMsg.hdr.sourceAddr = 0;
			ctrlMsg.hdr.destAddr = 0;
			ctrlMsg.hdr.bodySize = sizeof(SDfCtrlMsg::SDoBistResp);
			ctrlMsg.body.doBistResp.result = bistResult.result;
			ctrlMsg.body.doBistResp.last = last;
			ctrlMsg.body.doBistResp.send = bistResult.send;
			ctrlMsg.body.doBistResp.testLen = bistResult.test.size();
			ctrlMsg.body.doBistResp.textLen = bistResult.text.size();
			if (ctrlMsg.body.doBistResp.testLen > SDfCtrlMsg::SDoBistResp::MAX_TESTLEN) ctrlMsg.body.doBistResp.testLen = SDfCtrlMsg::SDoBistResp::MAX_TESTLEN;
			if (ctrlMsg.body.doBistResp.textLen > SDfCtrlMsg::SDoBistResp::MAX_TEXTLEN) ctrlMsg.body.doBistResp.textLen = SDfCtrlMsg::SDoBistResp::MAX_TEXTLEN;
			bistResult.test.copy(ctrlMsg.body.doBistResp.test, ctrlMsg.body.doBistResp.testLen);
			bistResult.text.copy(ctrlMsg.body.doBistResp.text, ctrlMsg.body.doBistResp.textLen);
			try
			{
				CWeakSingleton<CProcessorNode>()->Send(static_cast<CDfCtrlNet*>(task->m_source), ctrlMsg, "DO_BIST_RESP");
			}
			catch(CWeakSingleton<CProcessorNode>::NoInstance&)
			{
			}
		}
	}

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Helper
//
void CBist::UpdateOverallResult(SSmsMsg::SGetBistResp::EResult result, _Inout_ SSmsMsg::SGetBistResp::EResult& overall)
{
	if (result == SSmsMsg::SGetBistResp::FAIL)
	{
		overall = SSmsMsg::SGetBistResp::FAIL;
		m_OverallResult = overall;
	}
	else if (result == SSmsMsg::SGetBistResp::WARNING && overall == SSmsMsg::SGetBistResp::PASS)
	{
		overall = SSmsMsg::SGetBistResp::WARNING;
	}

	return;
}


/////////////////////////////////////////////////////////////////////
//
// class CBistSlave
//
void CBist::CBistSlave::Init(Units::Frequency rfFreq, Units::Frequency rxHwBw, Units::Frequency procBw, bool hf, CRadioEquip::EBandSelect band)
{
	// Initialize variables
	m_powerOk = false;
	m_sentIndex.clear();
	m_sentIndex2.clear();
	m_tag = 0;
	m_tag2 = 0;
	m_tuneOk = false;

	// Tune the slave rx
	m_tag = TuneSlaveRx(rfFreq, rxHwBw, procBw, hf, band, m_sentIndex);
	m_tuneOk = (m_tag != 0 && !m_sentIndex.empty());
	if (!m_tuneOk)
	{
		TRACE("CBist::TuneSlaveRx returned tag=%lu sent=%u\n", m_tag, m_sentIndex.size());
	}
	return;
}

void CBist::CBistSlave::Request(SDfCtrlMsg::SCcdfCollect::EPowerType powerType, unsigned char atten, SDfCtrlMsg::EGainMode gainMode)
{
	// Wait for slave to be tuned and request ToneDbm

	m_powerOk = false;
	m_tag2 = 0;
	m_sentIndex2.clear();
	if (m_tuneOk)
	{
		std::vector<CProcessorNode::SDfCtrlMsgPtr> tuneResponses;
		if (!WaitForSlaves(m_sentIndex, m_tag, tuneResponses))
		{
			TRACE("CBist::WaitForSlaves failed\n");
		}
		else
		{
			m_tag2 = MeasureSlavePower(powerType, atten, gainMode, m_sentIndex2);
			m_powerOk = (m_tag2 != 0 && !m_sentIndex2.empty());
		}
	}
	return;
}

void CBist::CBistSlave::RequestQuick(SDfCtrlMsg::SCcdfCollect::EPowerType powerType, unsigned char atten, SDfCtrlMsg::EGainMode gainMode)
{
	// Assume slave is tuned and request ToneDbm

	m_powerOk = false;
	m_tag2 = 0;
	m_sentIndex2.clear();
	if (m_tuneOk)
	{
		m_tag2 = MeasureSlavePower(powerType, atten, gainMode, m_sentIndex2);
		m_powerOk = (m_tag2 != 0 && !m_sentIndex2.empty());
	}
	return;
}

void CBist::CBistSlave::GetResults(std::vector<float>& power)
{
	power.clear();

	if (m_powerOk)
	{
		std::vector<CProcessorNode::SDfCtrlMsgPtr> powerResponses;
		if (!WaitForSlaves(m_sentIndex2, m_tag2, powerResponses))
		{
			TRACE("CBist::WaitForSlaves failed\n");
		}
		else if (powerResponses.empty())
		{
			TRACE("CBist::WaitForSlaves powerResponses empty\n");
		}
		else
		{
			// TODO: handle more than one slave correctly
			power.resize(powerResponses.size(), -std::numeric_limits<float>::infinity());
			for (size_t i = 0; i < powerResponses.size(); ++i)
			{
				if (powerResponses[i]->body.ccdfCollectResp.status != SDfCtrlMsg::SUCCESS)
				{
					TRACE("powerResponses[%u] status fail\n", i);
				}
				else
				{
					power[i] = powerResponses[i]->body.ccdfCollectResp.powerDbm;
					power[i] -= powerResponses[i]->body.ccdfCollectResp.gainDb;
				}
			}
		}
	}
	return;
}

unsigned long CBist::CBistSlave::MeasureSlavePower(SDfCtrlMsg::SCcdfCollect::EPowerType powerType, unsigned char atten, SDfCtrlMsg::EGainMode gainMode,
	std::vector<size_t>& sentIndex)
{
	SDfCtrlMsg msg;
	msg.hdr.msgType = SDfCtrlMsg::INTERNAL_DF_CTRL;
	msg.hdr.msgSubType = SDfCtrlMsg::CCDF_COLLECT;
	msg.hdr.sourceAddr = 0;
	msg.hdr.destAddr = 0;
	msg.hdr.bodySize = sizeof(SDfCtrlMsg::SCcdfCollect);

	msg.body.ccdfCollect.powerType = powerType;
	msg.body.ccdfCollect.atten = atten;
	msg.body.ccdfCollect.gainMode = gainMode;

	unsigned long tag = 0;
	try
	{
		CWeakSingleton<CProcessorNode> procNode;
		tag = procNode->SendToAllNodes(msg, sentIndex, true);		// Send tagged message
	}
	catch(CWeakSingleton<CProcessorNode>::NoInstance&)
	{
		TRACE("CBist::MeasureSlavePower Unable to access processorNode\n");
	}
	return tag;
}

unsigned long CBist::CBistSlave::TuneSlaveRx(Units::Frequency rfFreq, Units::Frequency rxHwBw, Units::Frequency procBw, bool hf,
	CRadioEquip::EBandSelect band, std::vector<size_t>& sentIndex)
{
	SDfCtrlMsg msg;
	msg.hdr.msgType = SDfCtrlMsg::INTERNAL_DF_CTRL;
	msg.hdr.msgSubType = SDfCtrlMsg::SLAVE_TUNE;
	msg.hdr.sourceAddr = 0;
	msg.hdr.destAddr = 0;
	msg.hdr.bodySize = sizeof(SDfCtrlMsg::SSlaveTune);

	memset(&msg.body.slaveTune, 0, sizeof(SDfCtrlMsg::SSlaveTune));
	msg.body.slaveTune.rfFreq = rfFreq.GetRaw();
	msg.body.slaveTune.rxBw = rxHwBw.GetRaw();
	msg.body.slaveTune.procBw = procBw.GetRaw();
	msg.body.slaveTune.hf = hf;
	msg.body.slaveTune.bandSelect = static_cast<unsigned char>(band);
	msg.body.slaveTune.ant = SEquipCtrlMsg::EAnt::ANT1;
	msg.body.slaveTune.tuneDigitizer = false;

	unsigned long tag = 0;
	try
	{
		CWeakSingleton<CProcessorNode> procNode;
		tag = procNode->SendToAllNodes(msg, sentIndex, true);		// Send tagged message
	}
	catch(CWeakSingleton<CProcessorNode>::NoInstance&)
	{
		TRACE("CBist::TuneSlaveRx Unable to access processorNode\n");
	}
	return tag;
}

bool CBist::CBistSlave::WaitForSlaves(const std::vector<size_t> sentIndex, unsigned long tag, std::vector<SDfCtrlMsgPtr>& responses)
{
	bool failed = false;
	responses.clear();
	try
	{
		CWeakSingleton<CProcessorNode> procNode;
		if (!procNode->WaitForAllResponses(sentIndex, tag, responses))
		{
			failed = true;
		}
	}
	catch(CWeakSingleton<CProcessorNode>::NoInstance&)
	{
		TRACE("CBist::WaitForSlaves Unable to access processorNode\n");
	}
	// Check that all responses are successes, otherwise fail
	for (size_t i = 0; i < responses.size(); ++i)
	{
		auto& r = responses[i];
		if (!r)
		{
			failed = true;
		}
		else
		{
			switch(r->hdr.msgType)
			{
			case SDfCtrlMsg::SMS_ERROR_REPORT:
				failed = true;
				TRACE("CBist::WaitForSlaves received SMS_ERROR_REPORT\n");
				break;

			case SDfCtrlMsg::INTERNAL_DF_CTRL:
				switch(r->hdr.msgSubType)
				{
//				case SDfCtrlMsg::RX_TUNE_RESP:
//					if (r->body.rxTuneResp.status == SDfCtrlMsg::SRxTuneResp::FAILURE)
//					{
//						failed = true;
//						TRACE("CBist::WaitForSlaves received RX_TUNE_RESP with status FAILURE\n");
//					}
//					break;

				case SDfCtrlMsg::SLAVE_TUNE_RESP:
					if (r->body.slaveTuneResp.status == SDfCtrlMsg::FAILURE)
					{
						failed = true;
						TRACE("CBist::WaitForSlaves received SLAVE_TUNE_RESP with status FAILURE\n");
					}
					break;

				case SDfCtrlMsg::CCDF_COLLECT_RESP:
					if (r->body.ccdfCollectResp.status == SDfCtrlMsg::FAILURE)
					{
						failed = true;
						TRACE("CBist::WaitForSlaves received CCDF_COLLECT_RESP with status FAILURE\n");
					}
					break;

				default:
					failed = true;
					TRACE("CBist::WaitForSlaves received unknown INTERNAL_DF_CTRL msgSubType %lu\n", r->hdr.msgSubType);
					break;
				}
				break;

			default:
				failed = true;
				TRACE("CBist::WaitForSlaves received unknown msgType %hu\n", r->hdr.msgType);
				break;
			}
		}
	}
	return !failed;
}

