/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "NetConnection.h"
#include "SmsRealtimeMsg.h"

class CRealtimeNet : public CNetConnection<CRealtimeNet>
{
public:
	// Functions
	CRealtimeNet(LPCTSTR service) : CNetConnection<CRealtimeNet>(service) { Startup(); } // Server
	CRealtimeNet(SOCKET socket, _In_ CRealtimeNet*) :
#ifdef _DEBUG
		CNetConnection<CRealtimeNet>(socket) { Startup(); } // Dynamic connection to server
#else
		CNetConnection<CRealtimeNet>(socket, 30000, 5000) { Startup(); } // Dynamic connection to server
#endif
	~CRealtimeNet(void) { Shutdown(); }
	unsigned long GetNumIqClients(unsigned long version);
	unsigned long GetNumSpectrumClients(unsigned long version);
	unsigned long GetNumDfClients(unsigned long version);
	void SendToAllClients(_In_ const SSmsRealtimeMsg* msg);

private:
	// Types
	typedef std::set<unsigned long> TaskSet;
	typedef std::map<unsigned long, unsigned long> VersionMap;

	struct SNumClients
	{
		unsigned long version;
		unsigned long numIqClients;
		unsigned long numSpectrumClients;
		unsigned long numDfClients;
	};

	// Functions
	virtual size_t BodySize(_In_ const void* hdr) const { return static_cast<const SSmsRealtimeMsg*>(hdr)->hdr.bodySize; }
	virtual size_t HeaderSize(void) const { return offsetof(SSmsRealtimeMsg, body); }
	virtual void OnClose(int errorCode); // Callback
	virtual void OnConnect(void); // Callback
	virtual void OnMessage(_In_bytecount_(size) const void* msg, size_t size);
	void SendMsg(_In_ void* arg);
	void UpdateNumIqClients(_In_ void* arg);
	void UpdateNumSpectrumClients(_In_ void* arg);
	void UpdateNumDfClients(_In_ void* arg);

	// Data
	TaskSet m_activeDfV1Set;
	VersionMap m_versionMap;

};
