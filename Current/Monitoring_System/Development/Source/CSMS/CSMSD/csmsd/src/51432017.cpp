/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "51432017.h"
#include "3230Fdd.h"
#include "Config.h"
#include "Log.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C51432017::C51432017() :
	C7234201102(),
	C72362001(),
	m_digitizer()
{
	// Read the antenna status register
	unsigned char antennaType;
	// auto status =
	m_digitizer->GetAntennaStatus(antennaType);
	printf("C51432017: antennaType = %x\n", antennaType);
//	if (antennaType != x)
	{
		m_present = true;
	}
	for (size_t i = 0; i < size_t(EPort::NUM_PORT_TYPE); ++i)
	{
		m_remoteAntType[i] = 0;
	}

	if (m_present)
	{
		// Set the antenna types
		m_remoteAntType[size_t(EPort::VUSHF)] = antennaType;
		auto mutableConfig = CSingleton<const CConfig>()->GetMutableConfig();

		//Set the HF type to the user configured antenna type until it can be
		// read reliably.
		//m_remoteAntType[size_t(EPort::HF)] = 255;
		m_remoteAntType[size_t(EPort::HF)] = static_cast<unsigned char>(mutableConfig.hfConfig.antCable.antType);


		// Read and verify the antenna type
		if (antennaType && static_cast<unsigned char>(mutableConfig.hfConfig.antCable.antType) != m_remoteAntType[size_t(EPort::HF)])
		{
			CLog::Log(CLog::ERRORS, "HF antenna type error: %u %u %s", m_remoteAntType[size_t(EPort::HF)],
				mutableConfig.hfConfig.antCable.antType, mutableConfig.hfConfig.antCable.antName.c_str());
		}

		// do not report error since HF is hardcoded to 255 instead of actual switch reading
		/*if (static_cast<unsigned char>(mutableConfig.vushfConfig.antCable[0].antType) != m_remoteAntType[size_t(EPort::VUSHF)])
		{
			CLog::Log(CLog::ERRORS, "VUSHF antenna type error: %u %u %s", m_remoteAntType[size_t(EPort::VUSHF)],
					mutableConfig.vushfConfig.antCable[0].antType, mutableConfig.vushfConfig.antCable[0].antName.c_str());
		}*/
	}
}

C51432017::~C51432017()
{

}


//////////////////////////////////////////////////////////////////////
//
// Get the antenna whip fault status
//
bool C51432017::GetRemoteWhipFault(void) const
{
	bool mastDown;
	bool whipFault;
	m_digitizer->GetAntennaStatus(mastDown, whipFault);
	return whipFault;
}


//////////////////////////////////////////////////////////////////////
//
// Set the antenna control word
//
void C51432017::SetDataOut(unsigned long controlWord)
{
	m_digitizer->SetAntennaControl(controlWord);
	return;
}


