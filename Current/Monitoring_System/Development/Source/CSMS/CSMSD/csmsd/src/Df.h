/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "Antenna.h"
#include "Config.h"
#include "DfCore.h"
#include "Navigation.h"
#include "Ne10Vec.h"
#include "Singleton.h"
#include "Units.h"

class CDf : public CDfCore
{
public:
	// Types
//	enum EDfAlgorithm { NONE, INTERFEROMETRY, WATSON_WATT, WATSON_WATT620, WFA, SYMMETRY, SYMMETRY_HORIZ, SYMMETRY_SHF, WFA649LOOPS };

	// Functions
	CDf(const Ne10F32cVec& volts, Units::Frequency freq, CAntenna::EDfAlgorithm algorithm, ne10_float32_t prevAz);
	CDf(const std::vector<Ne10F32cVec2>& refVolts, const std::vector<Ne10F32cVec2>& sampleVolts,
		const std::vector<size_t>& validBins, Units::Frequency startFreq,
		Units::Frequency chanSize, unsigned int binsPerChan, CAntenna::EDfAlgorithm algorithm, const Ne10F32Vec& prevAz);
	unsigned short GetCorrectedAz(unsigned int chan, bool hf, float confThresh) const;
	const Ne10F32Vec& GetEl(void) const { return m_el; }
	const Ne10F32Vec& GetUncorrectedAz(void) const { return m_az; }
	const Ne10F32Vec& GetConf(void) const { return m_conf; }

#ifdef CSMS_DEBUG
	static bool s_traceWfa;
#endif

private:
	// Constants
	static constexpr const float LIGHTSPEED = 299792458.0f;

	// Types
	struct SDualPolVoltageTerms
	{
		Ne10F32cVec modes;
		Ne10F32cVec same;
		Ne10F32cVec other;
	};

	// Functions
	CDf(void); // Not allowed
	CDf(const CDf&); // Not allowed
	CDf& operator=(const CDf&); // Not allowed
	void CalcDfCircularInterferometry(void);
	void CalcDfInterferometry(void);
	void CalcDfSymmetry(bool horiz, bool shf);
	void CalcDfWatsonWatt(const CAntenna::SWatsonWattParams* params);
	void CalcDfWfa(void);
	void CalcDualPolVoltageTerms(Ne10F32cVec volts, const CAntenna::DualPolPattern& dualPol, SDualPolVoltageTerms& v) const;
	static float CalcDualPolWfaConf(float azim, const SDualPolVoltageTerms& vModes, const CAntenna::DualPolPattern& dualPol,
		float* ampSame = nullptr, float* ampOther = nullptr);
	static float CalcDualPolWfaConf(const ne10_fft_cpx_float32_t& bSame, const ne10_fft_cpx_float32_t bOther, const CAntenna::DualPolPattern& dualPol,
		float* ampSame = nullptr, float* ampOther = nullptr);
	float CalcModeAzimCorr(Units::Frequency freq) const;
	float CalcPostAzimCorrection(size_t chan) const;
	void CalcSymAmpl(void);
	void CalcSymAmplDouble(bool phaseAdjust);
	void CalcSymAmplDouble2(void);
	void CalcSymAmplPairs(void);
	void CalcSymAmplTriplet(void);
//	float CalcSymConfOrig(float azim, const Ne10F32cVec& volts, bool phaseAdjust) const;
	void CalcSymGrid(unsigned char width, float uniqueConf, bool phaseAdjust);
	void CalcSymModes(bool inverted = false);
	void CalcSymModes23(void);
	void CalcSymModesCf(bool inverted, bool phaseAdjust);
	float CalcSymModesCorr(Units::Frequency freq) const;
	void CalcSymOffsetCorrection(void);
	float CalcSymRadius(Units::Frequency freq);
	void CalcSymWfa(unsigned char width, float uniqueConf, bool invModes);
	void CalcWfaCoarseSearch(unsigned int chan, const CAntenna::SWfaParams& wfaParams, int& azMax);
	float CalcWfaConf(unsigned int chan, int azIdx) const;
	void CalcWfaFineSearch2d(unsigned int chan, int azIdx) const;
	static void CalcWfaMax1d(const float fx[3], float& maxX, _Out_ float& maxFx);
	float CalcWWConf(size_t chan, float az);
	void DoDf(CAntenna::EDfAlgorithm algorithm);
	static void DualPolWfaFineIteration(float& azim1, float& c, const SDualPolVoltageTerms& vModes, const CAntenna::DualPolPattern& dualPol,
		unsigned int& iter, float* ampSame = nullptr, float* ampOther = nullptr);
	bool GetWWRefPhase(Units::Frequency freq, const CAntenna::SWatsonWattParams* params, float& sinRef, float& cosRef) const;
	void InitDualPolWfaTone(unsigned long numFineSteps);
	void InitSymConf(size_t chan);
	void InitTwoPhase(void);
	void InitThreePhase(void);
#ifdef CDF_SIMULATION
	void SimulateDfInterferometry(void);
	void SimulateDfWatsonWatt(const CAntenna::SWatsonWattParams* params);
#endif
	void Normalize(void);

	// Data
	static Ne10F32cVec m_antVec;
	static Ne10F32cVec m_antVec2;
	static Ne10F32cVec m_antVec3;
	static Ne10F32Vec m_az;
	unsigned int m_binsPerChan;
	Units::Frequency m_chanSize;
	static Ne10F32Vec m_conf;
	float m_confFact;
	float m_confNorm;
	CSingleton<const CConfig> m_config;
	static Ne10F32Vec m_el;
	Ne10F32cVec m_fineTwid;
	bool m_horiz;
	Ne10F32Vec m_measAmp;
	const CConfig::SMutableConfig& m_mutableConfig;	// must be after m_config
	CSingleton<const CNavigation> m_navigation;
	static size_t m_numAnts;
	static size_t m_numChans;
	Units::Frequency m_startFreq;
	const CAntenna::SSymmetryDfParams* m_symmetryDfParams;
	static std::vector<Ne10F32cVec> m_volts; // Vector of vector of ants
	const CAntenna::WfaConjPattern* m_wfaConjPattern;
};

