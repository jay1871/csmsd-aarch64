/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "RWLock.h"

class CLog
{
public:
	// Types
	enum ELogLevel { VERBOSE = 0, INFORMATION = 1,
		WARNINGS = 2, ERRORS = 3 };

	// Functions
	virtual ~CLog(void);
	static ELogLevel GetLogLevel(void) { return m_logLevel; }
	static void Log(ELogLevel level, const char* format, ...);
	static void SetLogLevel(ELogLevel logLevel) { m_logLevel = logLevel; };

private:
	// Types
	CLog(void);
	static CLog& Instance(void);

	// Data
	static CRWLock m_critSect;
	static ELogLevel m_logLevel;
};


