/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"

#include "Antenna.h"
#include "AvdTask.h"
#include "Config.h"
#include "CsmsLicense.h"
#include "Digitizer.h"
#include "EquipControl.h"
#include "EquipCtrlNet.h"
#include "Fft.h"
#include "MeasurementTask.h"
#include "Metrics.h"
#include "OccupancyTask.h"
#include "PanTask.h"
#include "ScanDfTask.h"
#include "SuspendableTask.h"
#include "Utility.h"
#include "VCPMsg.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CEquipControl::CEquipControl(void) :
	CAudio(),
	CHwControl(),
	CProcessing(),
	m_dfNodesReady(false),
	m_navigation(),
	m_config(),
	m_digitizer(),
	m_vcpControlClient(nullptr),
	m_mutableConfig(m_config->GetMutableConfig()),
	m_panParamMap(),
	m_processorNode(),
	m_shutdown(false),
	m_suspendedStandardModeTasks()
{
	if (!m_digitizer->IsMaster())
	{
		throw std::runtime_error("This digitizer is not master");
	}

	m_digitizer->EnableWatchdog(true);

	// If this is the DF master, wait until all of the nodes have come up on external clock (up to 180 seconds)
	if (m_processorNode->IsProcessorDfMaster())
	{
		for (size_t i = 0; i < 180; ++i)
		{
			if (m_processorNode->AreNodesReady())		// No error logging.
				break;

			if ((i % 10) == 0)
				printf("DF nodes are not ready: %u\n", i);
			sleep(1);
			if (Utility::IsQuitting())
			{
				CLog::Log(CLog::INFORMATION, "EquipControl received QUIT signal");
				return;
			}
		}
		if (m_processorNode->AreNodesReady())
		{
			CLog::Log(CLog::INFORMATION, "All DF nodes are ready");
			m_dfNodesReady = true;
		}
		else
		{
			CLog::Log(CLog::WARNINGS, "DF nodes are not ready");
		}
	}
	else if (m_config->IsProcWithAntSelectionSwitch())
	{
		if(m_antSwitch->Init(CRfSelSwitch::EANTSetup::C_TCI5143))
		{
			SEquipCtrlMsg::EAnt ant;

			if(m_antSwitch->ReadSwitchAntVal(ant))
			{
			    m_config->SetNewAntType(CConfig::ERfPort::C_RF1, ant);
			}
		}
	}

	CHwControl::DoStartupTasks();
	m_processorNode->SetStartupDone();

	CAudio::Startup();

//	m_digitizer->SetDbg(true);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CEquipControl::~CEquipControl(void)
{
	CHwControl::Stop();
	CProcessing::Stop();

	// Clean up running tasks
	CTask::CleanUp();
	TRACE("CEquipControl dtor\n");
}

//////////////////////////////////////////////////////////////////////
///<summary>
///Check antenna value of Client request, and returns the appropriate antenna
/// that corresponds to the csms RF input.
///This method also configure the RF switch to connect to the requested antenna
/// value.
///</summary>
///
/// <param name="ant">
///@param[in] Has the requested antenna value.
/// </param>
///
/// <param name="hf">
///@param[out] true: HF antenna is requested.
///	       false: V/U/SHF antenna is requested..
/// </param>
///
/// <returns>
///@return: ANT1 for various V/USHF antenna connected to RF1 or HF antenna.
///	    ANT1H for Horizontal polarized antenna.
///	    ANT2 for SHF Extension Horn antenna..
/// </returns>
///
/// <remarks>
/// @note: 1. This method will throw when error is found.
///	   2. The code assumes caller has checked for TCI5143_MULTIPLE_ANT system.
/// </remarks>
///
inline SEquipCtrlMsg::EAnt CEquipControl::CheckMultAntValue(SEquipCtrlMsg::EAnt ant, bool & hf) const
{
	ASSERT(m_config->IsProcWithAntSelectionSwitch());
	SEquipCtrlMsg::EAnt retAnt = SEquipCtrlMsg::EAnt::ANT1;
	hf = false;

	if(ant == SEquipCtrlMsg::EAnt::ANT1_HF)
	{
		hf = true;
	}
	else if(ant >= SEquipCtrlMsg::EAnt::ANT1_LPA && ant <= SEquipCtrlMsg::EAnt::ANT1_647H_MON)
	{
		CRfSelSwitch::ESwitchState failReason;
		if(!m_antSwitch->IsAntConnected(ant, failReason))
		{
			//The switch is not set to the specified antenna, change
			// it so the antenna is connected.
			if(!m_antSwitch->SetAnt(ant))
			{
				throw ErrorCodes::HARDWAREDOWN;
			}
			else
			{
				m_config->SetNewAntType(CConfig::ERfPort::C_RF1, ant);
			}
		}

		if(ant == SEquipCtrlMsg::EAnt::ANT1_647H_MON)
		{
			retAnt = SEquipCtrlMsg::EAnt::ANT1H;
		}
	}
	else if(ant == SEquipCtrlMsg::EAnt::ANT2)
	{
		retAnt = ant;
	}
	else if(ant != SEquipCtrlMsg::EAnt::ANT1)
	{
		//ANT1 to work with old Scorpio, that will use whatever antenna is
		// connected to the present input.
		throw ErrorCodes::INVALIDANTENNAID;
	}

	return(retAnt);
}

//////////////////////////////////////////////////////////////////////
//
// Get audio parameters from CAudio
//
void CEquipControl::GetAudioParams(std::vector<CAudio::SAudioParam>& params)
{
	CAudio::GetAudioParams(params);
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get response version for a message from appropriate network connection
//
unsigned char CEquipControl::GetResponseVersion(_In_ const CNetConnection<void>* client, _In_ const SEquipCtrlMsg::SHdr& hdr)
{
	return CEquipCtrlNet::GetResponseVersion(static_cast<const CEquipCtrlNet*>(client), hdr);
}

//////////////////////////////////////////////////////////////////////
///<summary>
///Returns the configured csms system type.
///</summary>
///
/// <returns>
///@return: Has the system type as configured by user.
///	    Currently, returns TCI5143 or TCI5143_MULTIPLE_ANT.
/// </returns>
///
inline SEquipCtrlMsg::EMetricHw CEquipControl::GetSystemType(void) const
{
	SEquipCtrlMsg::EMetricHw sysType = SEquipCtrlMsg::TCI5143;
	if(m_config->IsProcWithAntSelectionSwitch())
  	{
	    sysType = SEquipCtrlMsg::TCI5143_MULTIPLE_ANT;
  	}
	else if(m_config->HasDualAux1())
	{
		sysType = SEquipCtrlMsg::TCI5143_DUAL_AUX1;
	}

	return(sysType);

}

//////////////////////////////////////////////////////////////////////
//
// Handle client disconnection
//
void CEquipControl::OnClientClose(_In_ const CNetConnection<void>* client)
{
	{
		std::lock_guard<std::mutex> lock(m_vcpCritSect);
		if (client == m_vcpControlClient)
		{
			// Delete VCP task if there was one (there is none)

			m_vcpControlClient = nullptr;

			// Release standard mode background tasks
			for (unsigned int i = 0; i < m_suspendedStandardModeTasks.size(); ++i)
			{
				if (CSuspendableTask::SuspendableTask task = CSuspendableTask::Suspend(m_suspendedStandardModeTasks[i], false))
				{
					SendRealtimeStart(task);
					CHwControl::AddTask(task, ADD_TASK_TIMEOUT);
				}
			}
		}
		else
		{
			CSuspendableTask::Terminate(client);
		}
	}
	// Remove client's pan entry
	RemovePanEntry(client);

	if (m_processorNode->IsProcessorDfMaster())
	{
		if (m_pantoSlave)
		{

				// Send a message to the remote nodes
				std::vector<size_t> audioParamsSentMask;	// 0 => not sent, 1 => sent. size = total number of nodes
				//auto numSent =
				m_processorNode->FreeSlaveAudioChannelToAllNodes(audioParamsSentMask, 0, true, client);
				bool gotResp = false;
				SDfCtrlMsg::SSetSlaveAudioParamsResp slaveResp;
				// If anything was sent, wait for the responses now
				for (size_t node = 0; node < audioParamsSentMask.size(); ++node)
				{
					if (audioParamsSentMask[node] == 1)
					{
						for (size_t i = 0; i < 10; ++i)
						{
							if (m_processorNode->IsSetAudioParamsDone(node, slaveResp))
							{
								gotResp = true;
								break;
							}
							usleep(10000);	// 10 msec
						}
					}
				}
				if (gotResp)
				{
					// indicate success releasing audios from client?
				}
			printf("freeing slave audio\n");
			return;
		} // if m_pantoSlave
	}

	// Free audio channels associated with this client
	printf("freeing master audio\n");
	CAudio::Free(client);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle ANT_CTRL message
//
void CEquipControl::OnAntCtrl(_In_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source)
{
	auto peer = (source != nullptr ? source->GetPeerAddress() : nullptr);

	{
		std::lock_guard<std::mutex> lock(m_vcpCritSect);
		if (m_vcpControlClient != nullptr)
		{
			throw ErrorCodes::CMDINVALIDVCPMODE;
		}
	}

	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_ANT:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_ANT from %s"), peer->ai_canonname);
			}
			CTask(msg, source); // Just to validate

			// Respond
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::GET_ANT_RESPONSE;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SAntGetSetCtrlResp);
			resp.body.antGetSetCtrlResp.status = ErrorCodes::SUCCESS;

			// Get pan parameters
			{
				std::unique_lock<std::mutex> lock(m_panParamMutex);
				PanParamMap::const_iterator pan = m_panParamMap.find(source);

				if (pan == m_panParamMap.end())
				{
					resp.body.antGetSetCtrlResp.antenna = SEquipCtrlMsg::INVALID_ANT;
				}
				else
				{
					resp.body.antGetSetCtrlResp.antenna = pan->second.m_ant;
				}
			}

			Send(source, resp, _T("GET_ANT_RESPONSE"));
		}

		break;

	case SEquipCtrlMsg::SET_ANT:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got SET_ANT from %s"), peer->ai_canonname);
			}
			CTask(msg, source); // Just to validate

			// See if the panParam is already in map
			{
				std::unique_lock<std::mutex> lock(m_panParamMutex);
				PanParamMap::iterator pan = m_panParamMap.find(source);
				if (pan == m_panParamMap.end())
				{
					// Add entry
					SPanParam param(6000, 1000, msg->body.antSetCtrlCmd.antenna);
					pan = m_panParamMap.insert(PanParamMap::value_type(source, param)).first;
				}
				else
				{
					auto& param = pan->second;
					if (param.m_ant != msg->body.antSetCtrlCmd.antenna)
					{
						// Update entry
						param.m_ant = msg->body.antSetCtrlCmd.antenna;
					}
				}
			}

			// Respond
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::SET_ANT_RESPONSE;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SAntGetSetCtrlResp);
			resp.body.antGetSetCtrlResp.status = ErrorCodes::SUCCESS;
			resp.body.antGetSetCtrlResp.antenna = msg->body.antSetCtrlCmd.antenna;
			Send(source, resp, _T("SET_ANT_RESPONSE"));
		}

		break;

	default:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got invalid ANT_CTRL subtype %u from %s"), msg->hdr.msgSubType, peer->ai_canonname);
		}
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle AUTOVIOLATE_CTRL message
//
void CEquipControl::OnAvdCtrl(_In_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source)
{
	auto peer = (source != nullptr ? source->GetPeerAddress() : nullptr);

	{
		std::lock_guard<std::mutex> lock(m_vcpCritSect);
		if (m_vcpControlClient != nullptr)
		{
			throw ErrorCodes::CMDINVALIDVCPMODE;
		}
	}

	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_AUTOVIOLATE:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_AUTOVIOLATE from %s"), peer->ai_canonname);
			}

			if (m_config->IsProcWithAntSelectionSwitch())
			{
				SEquipCtrlMsg::EAnt ant = msg->body.getAutoViolateCmd.ant;
				bool hf;

				//Validate Scorpio antenna selection
				CheckMultAntValue(ant, hf);
			}


			CAvdTask::AvdTask task(CAvdTask::Create(msg, source));

			// Put on queue
			try
			{
				CHwControl::AddTask(task, ADD_TASK_TIMEOUT, false);		// Add as not-runnable
			}
			catch(CHwControl::TaskSched::EProblem)
			{
				CLog::Log(CLog::ERRORS, _T("Unable to add avd task- timing out"));
				throw ErrorCodes::HARDWAREBUSY;
			}

			// Send task status
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::TASK_STATUS;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SEquipTaskStatusResp);
//			TciGps::GetDATE(resp.body.equipTaskStatusResp.dateTime);
			resp.body.equipTaskStatusResp.dateTime = Utility::CurrentTimeAsDATE();
			resp.body.equipTaskStatusResp.key = task->m_taskIdKey.key;
			resp.body.equipTaskStatusResp.status = SEquipCtrlMsg::SEquipTaskStatusResp::STARTED;
			resp.body.equipTaskStatusResp.taskId = task->m_taskIdKey.taskId;
			Send(source, resp, _T("TASK_STATUS"));

			// Send task state
			resp.hdr.msgSubType = SEquipCtrlMsg::OCCUPANCY_STATE_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SStateResp);
			resp.body.stateResp = CSuspendableTask::GetState(task->m_taskIdKey);
			Send(source, resp, _T("OCCUPANCY_STATE_RESP"));

			// Send frequency vs bands
			std::shared_ptr<SEquipCtrlMsg> occMsg(
				new(offsetof(SEquipCtrlMsg, body.getOccupancyCmd.band[msg->body.getAutoViolateCmd.numBands])) SEquipCtrlMsg);
			occMsg->body.getOccupancyCmd = msg->body.getAutoViolateCmd;
			SendFrequencyVsChannelResp(source, resp, occMsg->body.getOccupancyCmd, task->GetAllTaskParams());

			// Send realtime DF start message
			SendRealtimeStart(task);

			// Allow the task to run
			CHwControl::SetRunnable(task->m_taskIdKey, true);
		}

		break;

	case SEquipCtrlMsg::GET_TASK_STATE:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_TASK_STATE from %s"), peer->ai_canonname);
			}
			CTask(msg, source); // Just to validate the message

			// Respond with status
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::OCCUPANCY_SOLICIT_STATE_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SStateResp);
			resp.body.stateResp = CSuspendableTask::GetState(msg->body.taskIdKey);
			Send(source, resp, _T("OCCUPANCY_SOLICIT_STATE_RESP"));
		}

		break;

	case SEquipCtrlMsg::RESUME_OCCUPANCY:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got RESUME_OCCUPANCY from %s"), peer->ai_canonname);
			}
			CTask(msg, source); // Just to validate the message

			if (CSuspendableTask::SuspendableTask task = CSuspendableTask::Suspend(msg->body.taskIdKey, false))
			{
				// Put on queue if not there (as non-runnable)
				try
				{
					CHwControl::ReplaceTask(task, ADD_TASK_TIMEOUT, false);
				}
				catch(CHwControl::TaskSched::EProblem)
				{
					throw ErrorCodes::HARDWAREBUSY;
				}

				// Respond with status
				SEquipCtrlMsg resp;
				resp.hdr = msg->hdr;
				resp.hdr.msgSubType = SEquipCtrlMsg::OCCUPANCY_SOLICIT_STATE_RESP;
				resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SStateResp);
				resp.body.stateResp = CSuspendableTask::GetState(msg->body.taskIdKey);
				Send(source, resp, _T("OCCUPANCY_SOLICIT_STATE_RESP"));

				// Send realtime DF start message
				SendRealtimeStart(task);

				// Allow the task to run
				CHwControl::SetRunnable(task->m_taskIdKey, true);
				TRACE("Setting task runnable\n");
			}
		}

		break;

	case SEquipCtrlMsg::SUSPEND_OCCUPANCY:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got SUSPEND_OCCUPANCY from %s"), peer->ai_canonname);
		}
		CTask(msg, source); // Just to validate the message

		if (CSuspendableTask::Suspend(msg->body.taskIdKey, true) != nullptr)
		{
			// Respond with status
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::OCCUPANCY_SOLICIT_STATE_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SStateResp);
			resp.body.stateResp = CSuspendableTask::GetState(msg->body.taskIdKey);
			Send(source, resp, _T("OCCUPANCY_SOLICIT_STATE_RESP"));
		}

		break;

	case SEquipCtrlMsg::VALIDATE_AUTOVIOLATE:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got VALIDATE_AUTOVIOLATE from %s"), peer->ai_canonname);
			}

			// Respond with status unless from CMetrics
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::VALIDATE_OCC_RESPONSE;
			resp.body.validateOccupancyResp.status = ErrorCodes::SUCCESS;

			//Create local copy of SGetAutoViolateCmd with a single band.
			SEquipCtrlMsg::SGetAutoViolateCmd avdCmd( msg->body.getAutoViolateCmd);
			if (avdCmd.numBands > 0 )
			{
				avdCmd.numBands = 1;
			}
			resp.body.validateOccupancyResp.occCmd = avdCmd;

			//Set numBands to be 1 and make the size of response msg to be fixed.
			resp.hdr.bodySize = sizeof(resp.body.validateOccupancyResp);

			try
			{
				CAvdTask::Validate(msg->body.getAutoViolateCmd);
			}
			catch(ErrorCodes::EErrorCode error)
			{
				if (source == nullptr)
				{
					// Rethrow for CMetrics
					throw;
				}

				resp.body.validateOccupancyResp.status = error;
			}

			if (source != nullptr)
			{
				Send(source, resp, _T("VALIDATE_OCC_RESPONSE"));
			}
		}

		break;

	default:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got invalid AUTOVIOLATE_CTRL subtype %u from %s"), msg->hdr.msgSubType, peer->ai_canonname);
		}
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle BIST_CTRL message
//
void CEquipControl::OnBistCtrl(_In_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source)
{
	auto peer = (source != nullptr ? source->GetPeerAddress() : nullptr);

	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_BIST:
	case SEquipCtrlMsg::GET_DIAGNOSTICS:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_BIST/GET_DIAGNOSTICS from %s"), peer->ai_canonname);
			}

			CTask::Task task(new CTask(msg, source));

			try
			{
				CHwControl::AddTask(task, ADD_TASK_TIMEOUT);
			}
			catch(CHwControl::TaskSched::EProblem)
			{
				throw ErrorCodes::HARDWAREBUSY;
			}
		}

		break;

	case SEquipCtrlMsg::GET_BIST_RESULT:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_BIST_RESULT from %s"), peer->ai_canonname);
			}
			CTask::Task task(new CTask(msg, source));
			const CBist::BistResults bistResults = GetBistResults();

			if(bistResults.empty())
			{
				// BIST not run yet
				SEquipCtrlMsg resp;
				resp.hdr = msg->hdr;
				resp.hdr.msgSubType = SEquipCtrlMsg::NOBIST_RESPONSE;
				resp.hdr.respVersion = 0;
				resp.hdr.bodySize = 0;
				Send(source, resp, _T("NOBIST_RESPONSE"));
			}
			else
			{
				for(size_t test = 0; test < bistResults.size(); ++test)
				{
					SendBistResult(task, bistResults[test], test == bistResults.size() - 1 ? true : false);
				}
			}
		}

		break;

	default:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got invalid BIST_CTRL subtype %u from %s"), msg->hdr.msgSubType, peer->ai_canonname);
		}
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle DEMOD_CTRL message
//
void CEquipControl::OnDemodCtrl(_In_ const SEquipCtrlMsg* msg, _In_ CNetConnection<void>* source)
{
	auto peer = (source != nullptr ? source->GetPeerAddress() : nullptr);

	{
		std::lock_guard<std::mutex> lock(m_vcpCritSect);
		if (m_vcpControlClient != nullptr)
		{
			throw ErrorCodes::CMDINVALIDVCPMODE;
		}
	}

	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::AGC_ONOFF:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got AGC_ONOFF from %s"), peer->ai_canonname);
			}
#if 0
			CTask(msg, source); // Just to validate the message
			SetAgc(msg->body.agcOnOffCmd.onOff == SEquipCtrlMsg::SAgcOnOffCmd::ON);
#endif
			// Respond
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::AGC_ONOFF_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SGenericResp);
			resp.body.genericResp.status = ErrorCodes::SUCCESS;
			Send(source, resp, _T("AGC_ONOFF_RESP"));
		}

		break;

	case SEquipCtrlMsg::SET_LAN_AUDIO:
		break; // TODO

	case SEquipCtrlMsg::SET_RCVR:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got SET_RCVR from %s"), peer->ai_canonname);
			}
			const SEquipCtrlMsg::SRcvrCtrlCmd& cmd = msg->body.rcvrCtrlCmd;

			// See if the panParam is already in map
			{
				std::unique_lock<std::mutex> lock(m_panParamMutex);
				PanParamMap::iterator pan = m_panParamMap.find(source);
				if (pan == m_panParamMap.end())
				{
					CTask task(msg, source); // Just to validate the message

					// Add entry
					SPanParam param(cmd.bandwidth, cmd.agcTime, SEquipCtrlMsg::ANT1);
					pan = m_panParamMap.insert(PanParamMap::value_type(source, param)).first;
				}
				else
				{
					auto& param = pan->second;
					// Update entry
					CTask task(msg, source, param.m_ant); // Validate
					param.m_agcTime = std::chrono::milliseconds(cmd.agcTime);
//					break;
				}
			}

			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::SET_RCVR_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SGenericResp);
			resp.body.genericResp.status = ErrorCodes::SUCCESS;
			Send(source, resp, _T("SET_RCVR_RESP"));
		}

		break;

	case SEquipCtrlMsg::SET_RCVR_MAN:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got SET_RCVR_MAN from %s"), peer->ai_canonname);
			}
#if 0
			CTask task(msg, source); // Just to validate the message
			SetMgcAtten(msg->body.rcvrManCmd.freq, task.GetTaskParams().rxBw, CDownConverter::OPTIMUM, SEquipCtrlMsg::ANT1,
				unsigned char(msg->body.rcvrManCmd.atten));
			SetMgcAtten(msg->body.rcvrManCmd.freq, task.GetTaskParams().rxBw, CDownConverter::OPTIMUM, SEquipCtrlMsg::ANT2,
				unsigned char(msg->body.rcvrManCmd.atten));
			SetMgcAtten(msg->body.rcvrManCmd.freq, task.GetTaskParams().rxBw, CDownConverter::OPTIMUM, SEquipCtrlMsg::ANT3,
				unsigned char(msg->body.rcvrManCmd.atten));

			// Remember settings
			m_testAtten = unsigned char(msg->body.rcvrManCmd.atten);
			m_testFreq = msg->body.rcvrManCmd.freq;
#endif
			// Respond
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::SET_RCVR_MAN_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SGenericResp);
			resp.body.genericResp.status = ErrorCodes::SUCCESS;
			Send(source, resp, _T("SET_RCVR_MAN_RESP"));
		}

		break;

	case SEquipCtrlMsg::SET_PAN_PARA:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got SET_PAN_PARA from %s"), peer->ai_canonname);
			}

			auto cmdAnt = msg->body.panParaCmd.antenna;
			auto ant = cmdAnt;

			if (m_config->IsProcWithAntSelectionSwitch())
			{
				//Validate Scorpio antenna selection
				ant = ValidateMultAnt(cmdAnt, msg->body.panParaCmd.rcvr.freq);
			}

			CTask task(msg, source, ant); // Validate
			const SEquipCtrlMsg::SRcvrCtrlCmd& cmd = msg->body.panParaCmd.rcvr;

			// See if the panParam is already in map
			std::unique_lock<std::mutex> lock(m_panParamMutex);
			PanParamMap::iterator pan = m_panParamMap.find(source);
			if (pan == m_panParamMap.end())
			{
				// Add entry
				SPanParam param(cmd.bandwidth, cmd.agcTime, cmdAnt);
				pan = m_panParamMap.insert(PanParamMap::value_type(source, param)).first;
			}
			else
			{
				auto& param = pan->second;
				param.m_powerBw = Units::Frequency(cmd.bandwidth);
				param.m_agcTime = std::chrono::milliseconds(cmd.agcTime);
				param.m_ant = cmdAnt;
			}
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::SET_PAN_PARA_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SGenericResp);
			resp.body.genericResp.status = ErrorCodes::SUCCESS;
			Send(source, resp, _T("SET_PAN_PARA_RESP"));
		}

		break;

	case SEquipCtrlMsg::SET_AUDIO_PARAMS:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got SET_AUDIO_PARAMS from %s"), peer->ai_canonname);
			}

			CTask task(msg, source); // Validate
			const SEquipCtrlMsg::SAudioParamsCmd& cmd = msg->body.audioParamsCmd;
			C3230::EDemodMode mode;
			CConfig::ConvertDemod(cmd.detMode, mode);
			auto demodDecimation = task.GetProcParams().decimations;
#ifdef CSMS_2016
			if (mode == C3230::EDemodMode::IQ || mode == C3230::EDemodMode::WIDE_IQ)
#else
			if (mode == C3230::EDemodMode::IQ)
#endif
			{
				demodDecimation.ddcCICDecimation = task.GetProcParams().iqCic;
			}
			unsigned char audioChan = 0xff;

			if (m_processorNode->IsProcessorDfMaster())
			{
				if (m_pantoSlave)
				{
						// Send a message to the remote nodes
						std::vector<size_t> audioParamsSentMask;	// 0 => not sent, 1 => sent. size = total number of nodes
						//auto numSent =
						m_processorNode->SendSetAudioParamsToAllNodes(audioParamsSentMask, demodDecimation, cmd, source);
						bool gotResp = false;
						SDfCtrlMsg::SSetSlaveAudioParamsResp slaveResp;
						// If anything was sent, wait for the responses now
						for (size_t node = 0; node < audioParamsSentMask.size(); ++node)
						{
							if (audioParamsSentMask[node] == 1)
							{
								for (size_t i = 0; i < 10; ++i)
								{
									if (m_processorNode->IsSetAudioParamsDone(node, slaveResp))
									{
										gotResp = true;
										break;
									}
									usleep(10000);	// 10 msec
								}
							}
						}
						if (gotResp)
						{
							SEquipCtrlMsg resp;
							audioChan = slaveResp.audioparamsresp.channel;
							resp.hdr = msg->hdr;
							resp.hdr.msgSubType = SEquipCtrlMsg::SET_AUDIO_PARAMS_RESP;
							resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SAudioParamsResp);
							resp.body.audioParamsResp.status = slaveResp.audioparamsresp.status;
							resp.body.audioParamsResp.channel = slaveResp.audioparamsresp.channel;
							// return slave ip address
							std::string slaveip = SDfCtrlMsg::slaveipaddress;
							if (slaveip.find('.') != std::string::npos)
							{
								strcpy(resp.body.audioParamsResp.ipAddress, slaveip.c_str());
								printf("read slave audio ip address = %s\n", resp.body.audioParamsResp.ipAddress);
							}
							else
							{
								// use .xml internal address

								const CConfig::SProcessorNodes & processorNodes = m_config->GetProcessorNodes();
								strcpy(resp.body.audioParamsResp.ipAddress, processorNodes.nodes[0].ipAddress.c_str());

								printf("xml slave audio ip address = %s\n", resp.body.audioParamsResp.ipAddress);
							}
							printf("slave ip audio ip address=%s\n", resp.body.audioParamsResp.ipAddress);
							Send(source, resp, _T("SET_AUDIO_PARAMS_RESP"));
						}
						else
						{
							if (cmd.anyChannel)
							{
								throw ErrorCodes::NOFREEAUDIOCHANNEL;
							}
							else
							{
								throw ErrorCodes::AUDIOCHANNELNOTFOUND;
							}

						}



					return;
				} // if m_pantoSlave

			}


			// If anyChannel=t in cmd, allocate a channel
			// If anyChannel=f in cmd, update matching entry, else error
			if (cmd.anyChannel)
			{
				// Allocate a channel and add to vector, if possible
				if (!CAudio::AllocateAudioChannel(source, Units::Frequency(cmd.freq), demodDecimation, mode,
					Units::Frequency(cmd.bandwidth), Units::Frequency(cmd.bfo), audioChan, cmd.streamID))
				{
					throw ErrorCodes::NOFREEAUDIOCHANNEL;
				}
			}
			else
			{
				// Either update an existing channel or fail
				if (!CAudio::SetDemodParams(source, cmd.channel, Units::Frequency(cmd.freq), demodDecimation, mode, Units::Frequency(cmd.bandwidth),
					Units::Frequency(cmd.bfo), cmd.streamID))
				{
					throw ErrorCodes::AUDIOCHANNELNOTFOUND;
				}
				audioChan = cmd.channel;
			}
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::SET_AUDIO_PARAMS_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SAudioParamsResp);
			resp.body.audioParamsResp.status = ErrorCodes::SUCCESS;
			resp.body.audioParamsResp.channel = audioChan;
			// copy master ip address to resp
			std::string ipAddress;
			std::string macAddress;
			m_config->GetNetworkInfo(ipAddress, macAddress);
			if (ipAddress.find(",")!= std::string::npos)
			{
				int commapos = ipAddress.find(",");
				strcpy(resp.body.audioParamsResp.ipAddress, ipAddress.substr(0, commapos).c_str());
				resp.body.audioParamsResp.ipAddress[commapos]='\0'; // 12 is the characters in bist before ip addr
				printf("master audio ip address = %s\n", resp.body.audioParamsResp.ipAddress);
			}
			else
			{
				if (!ipAddress.empty())
				{
					 // external primary
					 strcpy(resp.body.audioParamsResp.ipAddress, ipAddress.c_str());
					 resp.body.audioParamsResp.ipAddress[ipAddress.length()+1] = '\0';
					printf("read single master audio ip address = %s\n", resp.body.audioParamsResp.ipAddress);
				}
				else // use internal fall back from xml
				{
					const CConfig::SProcessorNodes & processorNodes = m_config->GetProcessorNodes();
					strcpy(resp.body.audioParamsResp.ipAddress, processorNodes.masterIP.c_str());
					resp.body.audioParamsResp.ipAddress[processorNodes.masterIP.length()+1] = '\0';
					printf("xml master audio ip address = %s\n", resp.body.audioParamsResp.ipAddress);
				}
			}

			Send(source, resp, _T("SET_AUDIO_PARAMS_RESP"));
		}

		break;

	case SEquipCtrlMsg::FREE_AUDIO_CHANNEL:
		{
			CTask task(msg, source); // Validate
			const SEquipCtrlMsg::SFreeAudioChannelCmd& cmd = msg->body.freeAudioChannelCmd;
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got FREE_AUDIO_CHANNEL %ufrom %s"), cmd.channel, peer->ai_canonname);
			}

			if (m_processorNode->IsProcessorDfMaster())
			{
				if (m_pantoSlave)
				{
						// Send a message to the remote nodes
						std::vector<size_t> audioParamsSentMask;	// 0 => not sent, 1 => sent. size = total number of nodes
						//auto numSent =
						m_processorNode->FreeSlaveAudioChannelToAllNodes(audioParamsSentMask, cmd.channel, false, source);
						bool gotResp = false;
						SDfCtrlMsg::SSetSlaveAudioParamsResp slaveResp;
						// If anything was sent, wait for the responses now
						for (size_t node = 0; node < audioParamsSentMask.size(); ++node)
						{
							if (audioParamsSentMask[node] == 1)
							{
								for (size_t i = 0; i < 10; ++i)
								{
									if (m_processorNode->IsSetAudioParamsDone(node, slaveResp))
									{
										gotResp = true;
										break;
									}
									usleep(10000);	// 10 msec
								}
							}
						}
						if (gotResp)
						{
							SEquipCtrlMsg resp;
							resp.hdr = msg->hdr;
							resp.hdr.msgSubType = SEquipCtrlMsg::FREE_AUDIO_CHANNEL_RESP;
							resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SGenericResp);
							resp.body.genericResp.status = ErrorCodes::SUCCESS;
							Send(source, resp, _T("FREE_AUDIO_CHANNEL_RESP"));
						}
						else
						{
							throw ErrorCodes::AUDIOCHANNELNOTFOUND;
						}

					return;
				} // if m_pantoSlave
			}
			if (!CAudio::Free(source, cmd.channel))
			{
				throw ErrorCodes::AUDIOCHANNELNOTFOUND;
			}

			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::FREE_AUDIO_CHANNEL_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SGenericResp);
			resp.body.genericResp.status = ErrorCodes::SUCCESS;
			Send(source, resp, _T("FREE_AUDIO_CHANNEL_RESP"));
		}

		break;

	default:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got invalid DEMOD_CTRL subtype %u from %s"), msg->hdr.msgSubType, peer->ai_canonname);
		}
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle DF_CTRL message
//
void CEquipControl::OnDfCtrl(_In_ const SEquipCtrlMsg* msg, _In_ CNetConnection<void>* source)
{
	auto peer = (source != nullptr ? source->GetPeerAddress() : nullptr);

	{
		std::lock_guard<std::mutex> lock(m_vcpCritSect);
		if (m_vcpControlClient != nullptr)
		{
			throw ErrorCodes::CMDINVALIDVCPMODE;
		}
	}

	try
	{
		switch(msg->hdr.msgSubType)
		{
#if 0
		case SEquipCtrlMsg::GET_IONOGRAM:
			{
				CTask(msg, source); // Just to validate the message
				LogReceive(msg, source, _T("GET_IONOGRAM"));

				// Respond
				SEquipCtrlMsg resp;
				resp.hdr = msg->hdr;
				resp.hdr.msgSubType = SEquipCtrlMsg::GET_IONOGRAM_RESPONSE;
				resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SGetIonogramResp);
				resp.body.getIonogramResp.taskId = msg->body.getIonogram.taskId;
				resp.body.getIonogramResp.status = ErrorCodes::SUCCESS;
				m_ionogram->GetIonogram(resp.body.getIonogramResp.ionoData);
				Send(source, resp, _T("GET_IONOGRAM_RESPONSE"));
			}

			break;
#endif
		case SEquipCtrlMsg::INITIALIZE_DF:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got INITIALIZE_DF from %s"), peer->ai_canonname);
			}
			{
				auto task = CTask::Task(new CTask(msg, source));
				task->RegisterTask(true);
				CHwControl::AddTask(task, ADD_TASK_TIMEOUT);
			}
			break;

#if 0
			case SEquipCtrlMsg::LOAD_IONOGRAM:
			{
				CTask(msg, source); // Just to validate the message
				LogReceive(msg, source, _T("LOAD_IONOGRAM"));
				m_ionogram->LoadIonogram(msg->body.loadIonogram.ionoData);

				// Respond
				SEquipCtrlMsg resp;
				resp.hdr = msg->hdr;
				resp.hdr.msgSubType = SEquipCtrlMsg::LOAD_IONOGRAM_RESPONSE;
				resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SLoadIonogramResp);
				resp.body.loadIonogramResp.taskId = msg->body.loadIonogram.taskId;
				resp.body.loadIonogramResp.status = ErrorCodes::SUCCESS;
				Send(source, resp, _T("LOAD_IONOGRAM_RESPONSE"));
			}

			break;
#endif
		default:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("invalid DF_CTRL subtype %u from %s"), msg->hdr.msgSubType, peer->ai_canonname);
			}
			throw ErrorCodes::INVALIDSUBTYPE;
		}
	}
	catch(CHwControl::TaskSched::EProblem)
	{
		throw ErrorCodes::HARDWAREBUSY;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle EQUIPMENT_CTRL message
//
void CEquipControl::OnEquipmentCtrl(_In_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source)
{
	auto peer = (source != nullptr ? source->GetPeerAddress() : nullptr);

	{
		std::lock_guard<std::mutex> lock(m_vcpCritSect);
		if (m_vcpControlClient != nullptr)
		{
			throw ErrorCodes::CMDINVALIDVCPMODE;
		}
	}

	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::TASK_TERMINATE:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got TASK_TERMINATE from %s"), peer->ai_canonname);
			}
			CTask(msg, source); // Just to validate the message
			const SEquipCtrlMsg::STaskIdKey& cmd = msg->body.taskIdKey;
			CSuspendableTask::Terminate(cmd);
			if (CSuspendableTask::SuspendableTask task = CSuspendableTask::Suspend(msg->body.taskIdKey, false))
			{
				SendRealtimeStart(task);
				CHwControl::AddTask(task, ADD_TASK_TIMEOUT);
			}

			// Make sure task is running
			CHwControl::SetRunnable(cmd, true);
		}

		break;

		case SEquipCtrlMsg::GET_TASK_STATUS:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_TASK_STATUS from %s"), peer->ai_canonname);
			}
			CTask(msg, source); // Just to validate the message

			// Respond with status
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			CTask::Task task(CTask::FindTask(msg->body.taskIdKey));

			switch(task->GetTaskParams().mode)
			{

			case CConfig::EMode::AVD:
				resp.hdr.msgType = SEquipCtrlMsg::AUTOVIOLATE_CTRL;
				break;

			case CConfig::EMode::OCCUPANCY:
				resp.hdr.msgType = SEquipCtrlMsg::OCCUPANCY_CTRL;
				break;

			case CConfig::EMode::SCAN_DF:
			case CConfig::EMode::FAST_SCAN_DF:
				resp.hdr.msgType = SEquipCtrlMsg::OCCUPANCYDF_CTRL;
				break;

			default:
				throw ErrorCodes::INVALIDTASKID;
			}

			resp.hdr.msgSubType = SEquipCtrlMsg::TASK_STATUS;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SEquipTaskStatusResp);
//			TciGps::GetDATE(resp.body.equipTaskStatusResp.dateTime);
			resp.body.equipTaskStatusResp.dateTime = Utility::CurrentTimeAsDATE();
			resp.body.equipTaskStatusResp.key = msg->body.taskIdKey.key;
			resp.body.equipTaskStatusResp.status = SEquipCtrlMsg::SEquipTaskStatusResp::ACTIVE;
			resp.body.equipTaskStatusResp.taskId = msg->body.taskIdKey.taskId;
			Send(source, resp, _T("TASK_STATUS"));
		}

		break;

	default:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got invalid EQUIPMENT_CTRL subtype %u from %s"), msg->hdr.msgSubType, peer->ai_canonname);
		}
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle METRICS_CTRL message
//
void CEquipControl::OnMetricsCtrl(_In_ const SEquipCtrlMsg* msg, /*CTask::EPriority priority,*/ _In_opt_ CNetConnection<void>* source)
{
	auto peer = (source != nullptr ? source->GetPeerAddress() : nullptr);
	if (m_config->IsDDRSystem())
	{
		throw ErrorCodes::CMDINVALIDDDRMODE;
	}
	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_BAND_REQUEST:
		{
//			if (peer != nullptr)
//			{
//				CLog::Log(CLog::INFORMATION, _T("Got GET_BAND_REQUEST from %s"), peer->ai_canonname);
//			}

			CTask(msg, source); // Just to validate the message

			// Respond with status
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::GET_BAND_RESPONSE;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SGetBandResp);
			resp.body.getBandResp.status = ErrorCodes::SUCCESS;
			Units::Frequency narrowHfRxBw = 0;
			Units::Frequency wideHfRxBw = 0;

			CSingleton<CRadioEquip> radioEquip;
			if(m_mutableConfig.hfConfig.rxType != CConfig::SMutableConfig::SHfConfig::NO_HFRX)
			{
				resp.body.getBandResp.hfMetricHardwarePresent = GetSystemType();
				m_config->GetHfMetricLimits(resp.body.getBandResp);
				narrowHfRxBw = Units::Frequency(radioEquip->C2630::m_configData.hfBwMHz * 1000000.0);
				wideHfRxBw = Units::Frequency(radioEquip->C2630::m_configData.hfBwMHz * 1000000.0);
			}
			else
			{
				resp.body.getBandResp.hfMetricHardwarePresent = SEquipCtrlMsg::NO_METRICS;
				resp.body.getBandResp.hfMetricLowFreq = Units::Frequency(0).GetRaw();
				resp.body.getBandResp.hfMetricHighFreq = Units::Frequency(0).GetRaw();
			}

			if (CAntenna::IsDfAntenna(m_mutableConfig.hfConfig.antCable.antenna.get()) && m_processorNode->IsProcessorDfMaster())
			{
				resp.body.getBandResp.hfDfHardwarePresent = SEquipCtrlMsg::SINGLE_POL;
				m_config->GetHfDfLimits(resp.body.getBandResp);
			}
			else
			{
				resp.body.getBandResp.hfDfHardwarePresent = SEquipCtrlMsg::NO_DF;
				resp.body.getBandResp.hfDfLowFreq = Units::Frequency(0).GetRaw();
				resp.body.getBandResp.hfDfHighFreq = Units::Frequency(0).GetRaw();
			}

			Units::Frequency narrowVushfRxBw = Units::Frequency(radioEquip->C2630::m_configData.narrowBwMHz * 1000000UL);
			Units::Frequency wideVushfRxBw(narrowVushfRxBw);

			if(m_mutableConfig.vushfConfig.rxType != CConfig::SMutableConfig::SVushfConfig::NO_VUHFRX)
			{
				resp.body.getBandResp.vushfMetricHardwarePresent = GetSystemType();
			}
			else
			{
				resp.body.getBandResp.vushfMetricHardwarePresent = SEquipCtrlMsg::NO_METRICS;
			}
			m_config->GetVushfMetricLimits(resp.body.getBandResp);

			if (m_mutableConfig.vushfConfig.rxType == CConfig::SMutableConfig::SVushfConfig::WIDE_2630)
			{
				wideVushfRxBw = Units::Frequency(radioEquip->C2630::m_configData.tuneBwMHz * 1000000UL);
			}

			if (CAntenna::IsDfAntenna(m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get()) &&
			    m_processorNode->IsProcessorDfMaster())
			{
				if (CAntenna::HasHorizontalPolarization(m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get()))
				{
					resp.body.getBandResp.vushfDfHardwarePresent = SEquipCtrlMsg::DUAL_POL;

				}
				else
					resp.body.getBandResp.vushfDfHardwarePresent = SEquipCtrlMsg::SINGLE_POL;
				m_config->GetVushfDfLimits(resp.body.getBandResp);
			}
			else
			{
				resp.body.getBandResp.vushfDfHardwarePresent = SEquipCtrlMsg::NO_DF;
				resp.body.getBandResp.vushfDfHLowFreq = Units::Frequency(0).GetRaw();
				resp.body.getBandResp.vushfDfHHighFreq = Units::Frequency(0).GetRaw();
				resp.body.getBandResp.vushfDfVLowFreq = Units::Frequency(0).GetRaw();
				resp.body.getBandResp.vushfDfVHighFreq = Units::Frequency(0).GetRaw();
			}

			if (m_config->HasMobileAntenna(false) || m_config->HasMobileAntenna(true))
			{
				resp.body.getBandResp.siteInfo = SSmsMsg::MOBILE_SITE;
			}
			else
			{
				resp.body.getBandResp.siteInfo = SSmsMsg::FIXED_SITE;
			}

			if (m_license->HasPrecisionTimestamp())
			{
				resp.body.getBandResp.iqDataCapability = SEquipCtrlMsg::TDOA_PRESENT;
			}
			else
			{
				resp.body.getBandResp.iqDataCapability = SEquipCtrlMsg::IQDATA_PRESENT;
			}

			resp.body.getBandResp.hfIonoInfoSourceType = SEquipCtrlMsg::NOT_APPLICABLE;

			m_config->GetBandwidths(true, CConfig::EMode::MEASURE, narrowHfRxBw, wideHfRxBw, resp.body.getBandResp.hfMeasurementBw);
			m_config->GetBandwidths(true, CConfig::EMode::PAN, narrowHfRxBw, wideHfRxBw, resp.body.getBandResp.hfPanDisplayBw);
			m_config->GetBandwidths(true, CConfig::EMode::OCCUPANCY, narrowHfRxBw, wideHfRxBw, resp.body.getBandResp.hfOccupancyBw);
			m_config->GetBandwidths(true, CConfig::EMode::SCAN_DF, narrowHfRxBw, wideHfRxBw, resp.body.getBandResp.hfDfScanBw);
			m_config->GetBandwidths(true, CConfig::EMode::DEMOD, narrowHfRxBw, wideHfRxBw, resp.body.getBandResp.hfReceiverBw);
			m_config->GetBandwidths(false, CConfig::EMode::MEASURE, narrowVushfRxBw, wideVushfRxBw, resp.body.getBandResp.vushfMeasurementBw);
			m_config->GetBandwidths(false, CConfig::EMode::PAN, narrowVushfRxBw, wideVushfRxBw, resp.body.getBandResp.vushfPanDisplayBw);
			m_config->GetBandwidths(false, CConfig::EMode::OCCUPANCY, narrowVushfRxBw, wideVushfRxBw, resp.body.getBandResp.vushfOccupancyBw);
			m_config->GetBandwidths(false, CConfig::EMode::SCAN_DF, narrowVushfRxBw, wideVushfRxBw, resp.body.getBandResp.vushfDfScanBw);
			m_config->GetBandwidths(false, CConfig::EMode::DEMOD, narrowVushfRxBw, wideVushfRxBw, resp.body.getBandResp.vushfReceiverBw);

			m_config->GetDwellTimes(true, narrowHfRxBw, wideHfRxBw, CAntenna::NumAnts(m_mutableConfig.hfConfig.antCable.antenna.get(), 0),
				resp.body.getBandResp.hfMeasurementDwell, GetEstimatedLatency);
			m_config->GetDwellTimes(false, narrowVushfRxBw, wideVushfRxBw,
			        CAntenna::NumAnts(m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get(), 0),
				resp.body.getBandResp.vushfMeasurementDwell, GetEstimatedLatency);
			Send(source, resp);	//	, _T("GET_BAND_RESPONSE"));
		}
		break;


		case SEquipCtrlMsg::GET_DWELL:
		{
			CLog::Log(CLog::INFORMATION, _T("GET_DWELL from %s"), peer->ai_canonname);

			CTask task(msg, source);

			// Respond with status
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::GET_DWELL_RESPONSE;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SGetDwellResp);
			resp.body.getDwellResp.status = ErrorCodes::SUCCESS;
			unsigned long dwellTime = std::max(1ul, static_cast<unsigned long>(1000 * m_config->GetDwellTime(task.GetProcParams()))); // ms
			resp.body.getDwellResp.freqDwellTime = dwellTime;
			resp.body.getDwellResp.bwDwellTime = dwellTime;
			resp.body.getDwellResp.modulationDwellTime = dwellTime;
			resp.body.getDwellResp.fieldStrengthDwellTime = dwellTime;
			int numAnts = 9;
			resp.body.getDwellResp.dfDwellTime = numAnts * dwellTime;
			Send(source, resp, _T("GET_DWELL_RESPONSE"));
		}

		break;

		case SEquipCtrlMsg::GET_GPS:
		{
//			if (peer != nullptr)
//			{
//				CLog::Log(CLog::VERBOSE, _T("Got GET_GPS from %s"), peer->ai_canonname);	// Too many
//			}
			CTask(msg, source); // Just to validate the message

			// Respond with status
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::GET_GPS_RESPONSE;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SGetGpsResp);
			resp.body.getGpsResp.status = ErrorCodes::SUCCESS;
			memset(&resp.body.getGpsResp.gpsResponse.status, 0, sizeof(resp.body.getGpsResp.gpsResponse.status));
//			TciGps::GetDATE(resp.body.getGpsResp.gpsResponse.dateTime);
			resp.body.getGpsResp.gpsResponse.dateTime = Utility::CurrentTimeAsDATE();
//			resp.body.getGpsResp.gpsResponse.latitude = 37.474543;
//			resp.body.getGpsResp.gpsResponse.longitude = -121.941380;

			resp.body.getGpsResp.status = m_navigation->GetGpsResponse(resp.body.getGpsResp.gpsResponse);
			Send(source, resp); // Send(source, resp, _T("GET_GPS_RESPONSE"));	 Too many to log
		}

		break;

	case SEquipCtrlMsg::GET_HEADING:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_HEADING from %s"), peer->ai_canonname);
			}
			CTask(msg, source); // Just to validate the message

			// Respond with status
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::GET_HEADING_RESPONSE;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SGetHeadingResp);
			resp.body.getHeadingResp.status = m_navigation->GetStatus();
			resp.body.getHeadingResp.magDeclination = long(floor(m_navigation->GetDeclination() * 100 + 0.5f));
			resp.body.getHeadingResp.hfFluxgateUsed = false;
//			resp.body.getHeadingResp.hfFluxgateUsed = (m_config->HasMobileAntenna(true) &&
//				m_navigation->IsPresent() && m_navigation->GetStatus() == ErrorCodes::SUCCESS);
			resp.body.getHeadingResp.hfRawMagHeading =
				static_cast<unsigned long>(m_navigation->GetRawMagHeading(true, 0) * 100 + 0.5f) % (Units::DEG_PER_CIRCLE * 100);
			resp.body.getHeadingResp.hfMagHeading =
				static_cast<unsigned long>(m_navigation->GetMagHeading(true, 0) * 100 + 0.5f) % (Units::DEG_PER_CIRCLE * 100);
			resp.body.getHeadingResp.hfTrueHeading =
				static_cast<unsigned long>(m_navigation->GetTrueHeading(true, 0) * 100 + 0.5f) % (Units::DEG_PER_CIRCLE * 100);
			resp.body.getHeadingResp.vhfFluxgateUsed = false;
//			resp.body.getHeadingResp.vhfFluxgateUsed = (m_config->HasMobileAntenna(false) &&
//				m_navigation->IsPresent() && m_navigation->GetStatus() == ErrorCodes::SUCCESS);
			resp.body.getHeadingResp.vhfRawMagHeading =
				static_cast<unsigned long>(m_navigation->GetRawMagHeading(false, 0) * 100 + 0.5f) % (Units::DEG_PER_CIRCLE * 100);
			resp.body.getHeadingResp.vhfMagHeading =
				static_cast<unsigned long>(m_navigation->GetMagHeading(false, 0) * 100 + 0.5f) % (Units::DEG_PER_CIRCLE * 100);
			resp.body.getHeadingResp.vhfTrueHeading =
				static_cast<unsigned long>(m_navigation->GetTrueHeading(false, 0) * 100 + 0.5f) % (Units::DEG_PER_CIRCLE * 100);
			Send(source, resp, _T("GET_HEADING_RESPONSE"));
		}

		break;

	case SEquipCtrlMsg::GET_MEAS:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_MEAS from %s"), peer->ai_canonname);
			}

			if (m_config->IsProcWithAntSelectionSwitch())
			{
				SEquipCtrlMsg::EAnt ant = msg->body.getMeasCmd.ant;
				bool hf;

				//Validate Scorpio antenna selection
				CheckMultAntValue(ant, hf);
			}


			CMeasurementTask::MeasurementTask task(CMeasurementTask::Create(msg, source/*, priority*/));

			// Check for empty request
			if(task->GetAllTaskParams().empty())
			{
				Send(source, task->GetResponse(), _T("GET_MEAS_RESPONSE"));
				CTask::DeleteTask(task->m_taskIdKey);
			}
			else
			{
				try
				{
					CHwControl::AddTask(task, ADD_TASK_TIMEOUT);
				}
				catch (CHwControl::TaskSched::EProblem)
				{
					throw ErrorCodes::HARDWAREBUSY;
				}
			}
		}

		break;

		case SEquipCtrlMsg::INIT_FIELDSTRENGTH:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got INIT_FIELDSTRENGTH from %s"), peer->ai_canonname);
			}

			CTask(msg, source); // Just to validate the message

			// Respond
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::INIT_FIELDSTRENGTH_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SGenericResp);
			resp.body.genericResp.status = ErrorCodes::SUCCESS;
			Send(source, resp, _T("INIT_FIELDSTRENGTH_RESP"));
		}

		break;

	case SEquipCtrlMsg::VALIDATE_MEAS:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got VALIDATE_MEAS from %s"), peer->ai_canonname);
			}

			SEquipCtrlMsg resp = CMeasurementTask::Validate(msg->body.getMeasCmd);

			// Respond with status unless from CMetrics
			if (source != nullptr)
			{
				Send(source, resp, _T("VALIDATE_MEAS_RESPONSE"));
			}
		}

		break;

#ifdef INCLUDE_PAN_CAPABILITIES
	case SEquipCtrlMsg::GET_PAN_CAPABILITIES_REQUEST:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_PAN_CAPABILITIES_REQUEST from %s"), peer->ai_canonname);
			}
			// Build response
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::GET_PAN_CAPABILITIES_RESPONSE;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SPanCapabilitiesResponse);

			// TODO: Turn into local function
			Units::Frequency narrowHfRxBw = 0;
			Units::Frequency wideHfRxBw = 0;

			if(m_mutableConfig.hfConfig.rxType != CConfig::SMutableConfig::SHfConfig::NONE)
			{
				narrowHfRxBw = C2630::m_configData.hfBwHz;
				wideHfRxBw = C2630::m_configData.hfBwHz;
			}

			Units::Frequency narrowVushfRxBw = Units::Frequency(C2630::m_configData.narrowBwHz);
			Units::Frequency wideVushfRxBw(narrowVushfRxBw);

			if(m_mutableConfig.vushfConfig.rxType == CConfig::SMutableConfig::SVushfConfig::WIDE_DUAL_2630 ||
				m_mutableConfig.vushfConfig.rxType == CConfig::SMutableConfig::SVushfConfig::WIDE_2630)
			{
				wideVushfRxBw = C2630::m_configData.wideBwHz;
			}

			m_config->GetBandwidthBins(true, CConfig::PAN, narrowHfRxBw, wideHfRxBw, resp.body.panCapabilitiesResponse.hfPanDisplayBw);
			m_config->GetBandwidthBins(false, CConfig::PAN, narrowVushfRxBw, wideVushfRxBw, resp.body.panCapabilitiesResponse.vushfPanDisplayBw);

//			resp.body.panCapabilitiesResponse.hfPanDisplayBw.numCapabilities = 0;
//			resp.body.panCapabilitiesResponse.vushfPanDisplayBw.numCapabilities = 29;
//			for (size_t i = 0; i < 29; ++i)
//			{
//				resp.body.panCapabilitiesResponse.vushfPanDisplayBw.bandwidth[i] = Units::Frequency(bws[i]).GetRaw();
//				resp.body.panCapabilitiesResponse.vushfPanDisplayBw.numBins[i] = bins[i];
//			}

			Send(source, resp, _T("GET_PAN_CAPABILITIES_RESPONSE"));
		}
		break;
#endif
	default:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got invalid METRICS_CTRL subtype %u from %s"), msg->hdr.msgSubType, peer->ai_canonname);
		}
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle OCCUPANCY_CTRL message
//
void CEquipControl::OnOccupancyCtrl(_In_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source)
{
	auto peer = (source != nullptr ? source->GetPeerAddress() : nullptr);

	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_OCCUPANCY:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_OCCUPANCY from %s"), peer->ai_canonname);
			}

			if (m_config->IsProcWithAntSelectionSwitch())
			{
				SEquipCtrlMsg::EAnt ant = msg->body.getOccupancyCmd.ant;
				bool hf;

				//Validate Scorpio antenna selection
				CheckMultAntValue(ant, hf);
			}

			COccupancyTask::OccupancyTask task(COccupancyTask::Create(msg, source));

			// Put on queue
			try
			{
				CHwControl::AddTask(task, ADD_TASK_TIMEOUT, false);		// Add as not-runnable
			}
			catch(CHwControl::TaskSched::EProblem)
			{
				throw ErrorCodes::HARDWAREBUSY;
			}

			// Respond with status
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::TASK_STATUS;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SEquipTaskStatusResp);
//			TciGps::GetDATE(resp.body.equipTaskStatusResp.dateTime);
			resp.body.equipTaskStatusResp.dateTime = Utility::CurrentTimeAsDATE();
			resp.body.equipTaskStatusResp.key = task->m_taskIdKey.key;
			resp.body.equipTaskStatusResp.status = SEquipCtrlMsg::SEquipTaskStatusResp::STARTED;
			resp.body.equipTaskStatusResp.taskId = task->m_taskIdKey.taskId;
			Send(source, resp, _T("TASK_STATUS"));

			// Send task state
			resp.hdr.msgSubType = SEquipCtrlMsg::OCCUPANCY_STATE_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SStateResp);
			resp.body.stateResp = CSuspendableTask::GetState(task->m_taskIdKey);
			Send(source, resp, _T("OCCUPANCY_STATE_RESP"));

			if (task->m_reportInterval > 0)
			{
				// Send frequency vs bands
				SendFrequencyVsChannelResp(source, resp, msg->body.getOccupancyCmd, task->GetAllTaskParams());
			}

			// Send realtime start message
			SendRealtimeStart(task);

			// Allow the task to run
			CHwControl::SetRunnable(task->m_taskIdKey, true);
		}

		break;

	case SEquipCtrlMsg::GET_TASK_STATE:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_TASK_STATE from %s"), peer->ai_canonname);
			}

			CTask(msg, source); // Just to validate the message

			// Respond with status
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::OCCUPANCY_SOLICIT_STATE_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SStateResp);
			resp.body.stateResp = CSuspendableTask::GetState(msg->body.taskIdKey);
			Send(source, resp, _T("OCCUPANCY_SOLICIT_STATE_RESP"));
		}

		break;

	case SEquipCtrlMsg::RESUME_OCCUPANCY:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got RESUME_OCCUPANCY from %s"), peer->ai_canonname);
			}
			CTask(msg, source); // Just to validate the message

			if (COccupancyTask::OccupancyTask task = std::dynamic_pointer_cast<COccupancyTask>(CSuspendableTask::Suspend(msg->body.taskIdKey, false)))
			{
				// Update completion time
				DATE now = Utility::CurrentTimeAsDATE();
//				TciGps::GetDATE(now);
				task->UpdateCompletionTime(now);

				// Put on queue if not there (as non-runnable)
				try
				{
					CHwControl::ReplaceTask(task, ADD_TASK_TIMEOUT, false);
				}
				catch(CHwControl::TaskSched::EProblem)
				{
					throw ErrorCodes::HARDWAREBUSY;
				}

				// Respond with status
				SEquipCtrlMsg resp;
				resp.hdr = msg->hdr;
				resp.hdr.msgSubType = SEquipCtrlMsg::OCCUPANCY_SOLICIT_STATE_RESP;
				resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SStateResp);
				resp.body.stateResp = CSuspendableTask::GetState(msg->body.taskIdKey);
				Send(source, resp, _T("OCCUPANCY_SOLICIT_STATE_RESP"));

				// Send realtime occupancy start message
				SendRealtimeStart(task);

				// Allow the task to run
//				CHwControl::UnfreezeTask(task);
				CHwControl::SetRunnable(task->m_taskIdKey, true);
				TRACE("Setting task runnable\n");
			}
		}

		break;

	case SEquipCtrlMsg::SUSPEND_OCCUPANCY:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got SUSPEND_OCCUPANCY from %s"), peer->ai_canonname);
		}
		CTask(msg, source); // Just to validate the message

		if (CSuspendableTask::Suspend(msg->body.taskIdKey, true) != nullptr)
		{
			// Respond with status
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::OCCUPANCY_SOLICIT_STATE_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SStateResp);
			resp.body.stateResp = CSuspendableTask::GetState(msg->body.taskIdKey);
			Send(source, resp, _T("OCCUPANCY_SOLICIT_STATE_RESP"));
		}

		break;

	case SEquipCtrlMsg::VALIDATE_OCCUPANCY:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got VALIDATE_OCCUPANCY from %s"), peer->ai_canonname);
			}

			// Respond with status unless from CMetrics
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::VALIDATE_OCC_RESPONSE;
			resp.body.validateOccupancyResp.status = ErrorCodes::SUCCESS;

			//Create local copy of SGetOccupancyCmd with a single band.
			SEquipCtrlMsg::SGetOccupancyCmd occupancyCmd( msg->body.getOccupancyCmd);
			if (occupancyCmd.numBands > 0 )
			{
				occupancyCmd.numBands = 1;
			}
			resp.body.validateOccupancyResp.occCmd = occupancyCmd;

			// Set numBands to be 1 and make the size of response msg to be fixed.
			resp.hdr.bodySize = sizeof(resp.body.validateOccupancyResp);

			try
			{
				COccupancyTask::Validate(msg->body.getOccupancyCmd);
			}
			catch(ErrorCodes::EErrorCode error)
			{
				if (source == nullptr)
				{
					// Rethrow for CMetrics
					throw;
				}

				resp.body.validateOccupancyResp.status = error;
			}

			if (source != nullptr)
			{
				Send(source, resp, _T("VALIDATE_OCC_RESPONSE"));
			}
		}

		break;


	default:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got invalid METRICS_CTRL subtype %u from %s"), msg->hdr.msgSubType, peer->ai_canonname);
		}
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle OCCUPANCYDF_CTRL message
//
void CEquipControl::OnOccupancyDfCtrl(_In_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source)
{
	auto peer = (source != nullptr ? source->GetPeerAddress() : nullptr);

	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_OCCUPANCY_SCANDF:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_OCCUPANCY_SCANDF from %s"), peer->ai_canonname);
			}

			CScanDfTask::ScanDfTask task(CScanDfTask::Create(msg, source));

			// Put on queue
			try
			{
				CHwControl::AddTask(task, ADD_TASK_TIMEOUT, false);		// Add as not-runnable
			}
			catch(CHwControl::TaskSched::EProblem)
			{
				throw ErrorCodes::HARDWAREBUSY;
			}

			// Send task status
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::TASK_STATUS;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SEquipTaskStatusResp);
			resp.body.equipTaskStatusResp.dateTime = Utility::CurrentTimeAsDATE();
			resp.body.equipTaskStatusResp.key = task->m_taskIdKey.key;
			resp.body.equipTaskStatusResp.status = SEquipCtrlMsg::SEquipTaskStatusResp::STARTED;
			resp.body.equipTaskStatusResp.taskId = task->m_taskIdKey.taskId;
			Send(source, resp, _T("TASK_STATUS"));

			// Send task state
			resp.hdr.msgSubType = SEquipCtrlMsg::OCCUPANCY_STATE_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SStateResp);
			resp.body.stateResp = CSuspendableTask::GetState(task->m_taskIdKey);
			Send(source, resp, _T("OCCUPANCY_STATE_RESP"));

			if(task->m_reportInterval > 0)
			{
				// Send frequency vs bands
				std::shared_ptr<SEquipCtrlMsg> occMsg(
					new(offsetof(SEquipCtrlMsg, body.getOccupancyCmd.band[msg->body.getScanDfCmd.numBands])) SEquipCtrlMsg);
				occMsg->body.getOccupancyCmd = msg->body.getScanDfCmd;
				SendFrequencyVsChannelResp(source, resp, occMsg->body.getOccupancyCmd, task->GetAllTaskParams());
			}

			// Send realtime DF start message
			SendRealtimeStart(task);

			// Allow the task to run
			CHwControl::SetRunnable(task->m_taskIdKey, true);
		}

		break;

	case SEquipCtrlMsg::GET_TASK_STATE:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_TASK_STATE from %s"), peer->ai_canonname);
			}
			CTask(msg, source); // Just to validate the message

			// Respond with status
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::OCCUPANCY_SOLICIT_STATE_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SStateResp);
			resp.body.stateResp = CSuspendableTask::GetState(msg->body.taskIdKey);
			Send(source, resp, _T("OCCUPANCY_SOLICIT_STATE_RESP"));
		}

		break;

	case SEquipCtrlMsg::RESUME_OCCUPANCY:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got RESUME_OCCUPANCY from %s"), peer->ai_canonname);
			}
			CTask(msg, source); // Just to validate the message

			if (CSuspendableTask::SuspendableTask task = CSuspendableTask::Suspend(msg->body.taskIdKey, false))
			{
				// Put on queue if not there (as non-runnable)
				try
				{
					CHwControl::ReplaceTask(task, ADD_TASK_TIMEOUT, false);
				}
				catch(CHwControl::TaskSched::EProblem)
				{
					throw ErrorCodes::HARDWAREBUSY;
				}

				// Respond with status
				SEquipCtrlMsg resp;
				resp.hdr = msg->hdr;
				resp.hdr.msgSubType = SEquipCtrlMsg::OCCUPANCY_SOLICIT_STATE_RESP;
				resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SStateResp);
				resp.body.stateResp = CSuspendableTask::GetState(msg->body.taskIdKey);
				Send(source, resp, _T("OCCUPANCY_SOLICIT_STATE_RESP"));

				// Send realtime DF start message
				SendRealtimeStart(task);

				// Allow the task to run
				CHwControl::SetRunnable(task->m_taskIdKey, true);
			}
		}

		break;

	case SEquipCtrlMsg::SUSPEND_OCCUPANCY:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got SUSPEND_OCCUPANCY from %s"), peer->ai_canonname);
		}
		CTask(msg, source); // Just to validate the message

		if (CSuspendableTask::Suspend(msg->body.taskIdKey, true) != nullptr)
		{
			// Respond with status
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::OCCUPANCY_SOLICIT_STATE_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SStateResp);
			resp.body.stateResp = CSuspendableTask::GetState(msg->body.taskIdKey);
			Send(source, resp, _T("OCCUPANCY_SOLICIT_STATE_RESP"));
		}

		break;

	case SEquipCtrlMsg::VALIDATE_OCCUPANCY_SCANDF:
		{
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got VALIDATE_OCCUPANCY_SCANDF from %s"), peer->ai_canonname);
			}

			// Respond with status, unless from CMetrics
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::VALIDATE_OCC_RESPONSE;
			resp.body.validateOccupancyResp.status = ErrorCodes::SUCCESS;

			//Create local copy of SGetAutoViolateCmd with a single band.
			SEquipCtrlMsg::SGetScanDfCmd scanDfCmd(msg->body.getScanDfCmd);
			if( scanDfCmd.numBands > 0 )
			{
				scanDfCmd.numBands = 1;
			}
			resp.body.validateOccupancyResp.occCmd = scanDfCmd;

			//Set numBands to be 1 and make the size of response msg to be fixed.
			resp.hdr.bodySize = sizeof(resp.body.validateOccupancyResp);

			try
			{
				CScanDfTask::Validate(msg->body.getScanDfCmd);
			}
			catch(ErrorCodes::EErrorCode error)
			{
				if (source == nullptr)
				{
					throw; // Rethrow for CMetrics
				}

				resp.body.validateOccupancyResp.status = error;
			}

			if(source != nullptr)
			{
				Send(source, resp, _T("VALIDATE_OCC_RESPONSE"));
			}
		}

		break;

	default:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got invalid OCCUPANCYDF_CTRL subtype %u from %s"), msg->hdr.msgSubType, peer->ai_canonname);
		}
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle PAN_DISP_CTRL message
//
void CEquipControl::OnPanDispCtrl(_In_ const SEquipCtrlMsg* msg, _In_ CNetConnection<void>* source)
{
	auto peer = (source != nullptr ? source->GetPeerAddress() : nullptr);

	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_PAN:
	case SEquipCtrlMsg::GET_PAN_AGC:
		{
//			if (peer != nullptr)
//			{
//				CLog::Log(CLog::INFORMATION,
//						(msg->hdr.msgSubType == SEquipCtrlMsg::GET_PAN ? _T("Got GET_PAN from %s") : _T("Got GET_PAN_AGC from %s")),
//						peer->ai_canonname);
//			}
			// See if there are panParam and audioParam for this client
			std::chrono::milliseconds agcTime;
			SEquipCtrlMsg::EAnt ant;
			Units::Frequency powerBw;
			bool found = false;
			{
				std::unique_lock<std::mutex> lock(m_panParamMutex);
				auto pan = m_panParamMap.find(source);
				if (pan == m_panParamMap.end())
				{
					SPanParam param(6000, 1000, SEquipCtrlMsg::ANT1);
					pan = m_panParamMap.insert(PanParamMap::value_type(source, param)).first;
				}
				agcTime = pan->second.m_agcTime;
				ant = pan->second.m_ant;
				powerBw = pan->second.m_powerBw;
				found = true;
			}
			CPanTask::PanTask task(CPanTask::Create(msg, source, ant));	// Create and validate
			if (found)
			{
				task->m_agcTime = agcTime;
				task->m_powerBw = powerBw;
			}
			task->CheckAntenna();	// This just checks that specified antenna exists

			//task->UpdatePan is called by CPanTask::Create above, through CTask::InitPanDispCtrl
			//task->UpdatePan();		// This does, among other things, a frequency check for the selected antenna
			m_pantoSlave = false;
			if (m_processorNode->IsProcessorDfMaster())
			{
				m_pantoSlave = m_config->IsSlaveAntenna(task->m_ant, task->GetTaskParams().hf);
			}
			if (m_mutableConfig.miscConfig.enhancedDynRng)
			{
				// If there's not already a pan task in the scheduler for this source or if it has changed, set m_spurCal true
				CTask::Task existingTask;
				if (CHwControl::GetTask(source, SEquipCtrlMsg::PAN_DISP_CTRL, existingTask))
				{
					// Found task, see if it's the same pan data
					task->m_spurCal = true;
					if (task->m_taskParams.size() == 1 && existingTask->m_taskParams.size() == 1)
					{
						auto& a = task->m_taskParams[0];
						auto& b = existingTask->m_taskParams[0];
						if (a.startFreq == b.startFreq && a.bw == b.bw && task->m_rcvrAtten == existingTask->m_rcvrAtten && task->m_ant == existingTask->m_ant)
						{
							// Copy any existing terminatedData from the existing task to the new one.
							CPanTask::PanTask panTask(std::dynamic_pointer_cast<CPanTask>(existingTask));
							if (panTask && panTask->m_terminatedData.size() > 0)
							{
								task->m_spurCal = false;
								task->m_terminatedData = panTask->m_terminatedData;
							}
						}
					}
				}
			}
			try
			{
				CHwControl::AddTask(task, ADD_TASK_TIMEOUT, true, SEquipCtrlMsg::PAN_DISP_CTRL);
			}
			catch(CHwControl::TaskSched::EProblem&)
			{
				throw ErrorCodes::HARDWAREBUSY;
			}

		}

		break;

	default:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got invalid PAN_DISP_CTRL subtype %u from %s"), msg->hdr.msgSubType, peer->ai_canonname);
		}
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle STATUS_CTRL message
//
void CEquipControl::OnStatusCtrl(_In_ const SEquipCtrlMsg* msg, _In_ CNetConnection<void>* source)
{
	auto peer = (source != nullptr ? source->GetPeerAddress() : nullptr);

	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_FAULT_REQUEST:
		{
//			if (peer != nullptr)
//			{
//				CLog::Log(CLog::INFORMATION, _T("Got GET_FAULT_REQUEST from %s"), peer->ai_canonname);
//			}
#if 0
			CTask(msg, source); // Just to validate the message
#endif
			// Respond with status
			SEquipCtrlMsg resp;
			resp.hdr = msg->hdr;
			resp.hdr.msgSubType = SEquipCtrlMsg::GET_CSMS_FAULT_RESP;
			resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SGetCsmsFaultResp);
			resp.hdr.respVersion = GetResponseVersion(source, resp.hdr);
			TRACE("setting GET_CSMS_FAULT_RESP respVersion = %u\n", resp.hdr.respVersion);
			CHwControl::GetFaultDetails(resp.body.getCsmsFaultResp);
			Send(source, resp);	// , _T("GET_CSMS_FAULT_RESP"));
		}
		break;

	default:
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Got invalid STATUS_CTRL subtype %u from %s"), msg->hdr.msgSubType, peer->ai_canonname);
		}
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}

//////////////////////////////////////////////////////////////////////
//
// Handle VCP_CTRL message
//
void CEquipControl::OnVCPCtrl(_In_ const SVCPMsg* msg, _In_ CEquipCtrlNet* source)
{
	auto peer = (source != nullptr ? source->GetPeerAddress() : nullptr);

	std::unique_ptr<SVCPMsg> resp(new SVCPMsg);
//	SVCPMsg resp;
	ErrorCodes::EErrorCode status = ErrorCodes::SUCCESS;

	switch(msg->hdr.msgSubType)
	{
		// SEquipCtrlMsg::GET_MSG_VERSIONS handled earlier

		case SVCPMsg::SET_VCP_MONITOR:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("VCP monitor mode = %d from %s"), msg->body.setMonitorMode.bMonitorMode ,peer->ai_canonname);
			}
			if (!msg->body.setMonitorMode.bMonitorMode)	// GOTO control mode (like fast mode)
			{
				std::unique_lock<std::mutex> lock(m_vcpCritSect);
				if (m_vcpControlClient != nullptr && m_vcpControlClient != source)
				{
					// Already have controlling client and it's not this one!
					throw ErrorCodes::CMDINVALIDVCPMODE;
				}
				if (m_vcpControlClient != nullptr)
				{
					// Already in control mode, not an error but nothing to do.
					status = ErrorCodes::CMDINVALIDVCPMODE;
					lock.unlock();
				}
				else
				{
					// Not in control mode, put it there.
					m_vcpControlClient = source;
					lock.unlock();
					m_suspendedStandardModeTasks = CSuspendableTask::SuspendAll(true);
				}
			}
			else	// GOTO monitor mode (like standard mode)
			{
				std::unique_lock<std::mutex> lock(m_vcpCritSect);
				if (m_vcpControlClient != nullptr && m_vcpControlClient != source)
				{
					// Have controlling client and it's not this one!
					throw ErrorCodes::CMDINVALIDVCPMODE;
				}
				if (m_vcpControlClient == nullptr)
				{
					// Already in monitor mode, not an error but nothing to do.
					status = ErrorCodes::CMDINVALIDSTDMODE;
					lock.unlock();
				}
				else
				{
					m_vcpControlClient = nullptr;
					lock.unlock();
					Resume(m_suspendedStandardModeTasks);

					// Reschedule deferred tasks
					SEquipCtrlMsg resched;
					resched.hdr.msgType = SEquipCtrlMsg::METRICS_CTRL;
					resched.hdr.msgSubType = SEquipCtrlMsg::CAL_SCHED;
					resched.hdr.bodySize = 0;

					try
					{
						CWeakSingleton<CPersistentData>()->AddMsg(resched);
					}
					catch(CWeakSingleton<CPersistentData>::NoInstance&)
					{
						// Ignore
					}
				}
			}

			resp->hdr = msg->hdr;
			resp->hdr.msgSubType = SVCPMsg::SET_VCP_MONITOR_RESP;
			resp->hdr.bodySize = sizeof(SVCPMsg::SSetMonitorModeResp);
			resp->body.setMonitorModeResp.status = status;
			resp->body.setMonitorModeResp.bMonitorMode = msg->body.setMonitorMode.bMonitorMode;
			Send(source, *resp, _T("SET_VCP_MONITOR_RESP"));
			break;

		case SVCPMsg::GET_REG_VALS:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_REG_VALS from %s"), peer->ai_canonname);
			}
			status = GetRegValues(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_REG_VALS_RESP"));
			break;

		case SVCPMsg::GET_RX_DATA:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_RX_DATA from %s"), peer->ai_canonname);
			}
			status = GetReceiverData(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_RX_DATA_RESP"));
			break;

		case SVCPMsg::GET_RX_CAPABILITIES:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_RX_CAPABILITIES from %s"), peer->ai_canonname);
			}
			status = GetRxCapabilities(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_RX_CAPABILITIES_RESP"));
			break;

		case SVCPMsg::GET_RX_MOC:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_RX_MOC from %s"), peer->ai_canonname);
			}
			status = GetRxMoc(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_RX_MOC_RESP"));
			break;

		case SVCPMsg::GET_RX_ATTEN:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_RX_ATTEN from %s"), peer->ai_canonname);
			}
			status = GetRxAtten(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_RX_ATTEN_RESP"));
			break;

		case SVCPMsg::GET_RX_CALGEN:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_RX_CALGEN from %s"), peer->ai_canonname);
			}
			status = GetRxCalgen(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_RX_CALGEN_RESP"));
			break;

		case SVCPMsg::GET_RX_OCXO:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_RX_OCXO from %s"), peer->ai_canonname);
			}
			status = GetRxOcxo(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_RX_OCXO_RESP"));
			break;

		case SVCPMsg::GET_RX_TEMPS:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_RX_TEMPS from %s"), peer->ai_canonname);
			}
			status = GetRxTemps(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_RX_TEMPS_RESP"));
			break;

		case SVCPMsg::GET_DIGI_TEMPS:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_DIGI_TEMPS from %s"), peer->ai_canonname);
			}
			status = GetDigiTemps(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_DIGI_TEMPS_RESP"));
			break;

		case SVCPMsg::GET_DIGI_VOLTS:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_DIGI_VOLTS from %s"), peer->ai_canonname);
			}
			status = GetDigiVolts(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_DIGI_VOLTS_RESP"));
			break;

		case SVCPMsg::GET_ANTENNA_SWITCH:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_ANTENNA_SWITCH from %s"), peer->ai_canonname);
			}
			status = GetAntennaSwitch(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_ANTENNA_SWITCH_RESP"));
			break;

		case SVCPMsg::SET_ANTENNA_SWITCH:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got SET_ANTENNA_SWITCH from %s"), peer->ai_canonname);
			}
			ValidateVCPControlClient(source);
			status = SetAntennaSwitch(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("SET_ANTENNA_SWITCH_RESP"));
			break;

		case SVCPMsg::RX_TUNE:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got RX_TUNE from %s"), peer->ai_canonname);
			}
			ValidateVCPControlClient(source);
			status = RxTune(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("RX_TUNE_RESP"));
			break;

		case SVCPMsg::SET_RX_ATTEN:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got SET_RX_ATTEN from %s"), peer->ai_canonname);
			}
			ValidateVCPControlClient(source);
			status = SetRxAtten(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("SET_RX_ATTEN_RESP"));
			break;

		case SVCPMsg::SET_RX_CALGEN:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got SET_RX_CALGEN from %s"), peer->ai_canonname);
			}
			ValidateVCPControlClient(source);
			status = SetRxCalgen(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("SET_RX_CALGEN_RESP"));
			break;

		case SVCPMsg::GET_SPECTRUM_DATA:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_SPECTRUM_DATA from %s"), peer->ai_canonname);
			}
			ValidateVCPControlClient(source);
			status = GetSpectrumData(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_SPECTRUM_DATA_RESP"));
			break;

		default:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got invalid VCP_CTRL subtype %u from %s"), msg->hdr.msgSubType, peer->ai_canonname);
			}
			throw ErrorCodes::INVALIDSUBTYPE;
			break;

	}
}


//////////////////////////////////////////////////////////////////////
//
// Remove a pan entry
//
void CEquipControl::RemovePanEntry(_In_ const CNetConnection<void>* client)
{
	{
		std::unique_lock<std::mutex> lock(m_panParamMutex);
		PanParamMap::iterator pan = m_panParamMap.find(client);

		if(pan != m_panParamMap.end())
		{
			m_panParamMap.erase(pan);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Resume suspended tasks
//
void CEquipControl::Resume(_Inout_ std::vector<SEquipCtrlMsg::STaskIdKey>& tasks)
{
	for (unsigned int i = 0; i < tasks.size(); ++i)
	{
		if(CSuspendableTask::SuspendableTask task = CSuspendableTask::Suspend(tasks[i], false))
		{
			try
			{
				CHwControl::AddTask(task, ADD_TASK_TIMEOUT);
				SendRealtimeStart(task);
			}
			catch(CHwControl::TaskSched::EProblem)
			{
				throw ErrorCodes::HARDWAREBUSY;
			}
		}
	}

	tasks.clear();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send a message
//
void CEquipControl::Send(_In_opt_ CNetConnection<void>* client, _In_ const SEquipCtrlMsg& msg,
						 _In_opt_ LPCTSTR log)
{
	if (client != nullptr)
	{
		// First try as if the client is an EquipControl client
		bool success = CEquipCtrlNet::Send(static_cast<CEquipCtrlNet*>(client), &msg);

		if(!success)
		{
			// Otherwise try as a Metrics client
			try
			{
				success = CWeakSingleton<CMetrics>()->TranslateAndSend(static_cast<CMetricsNet*>(client), msg);
			}
			catch(CWeakSingleton<CMetrics>::NoInstance&)
			{
				// Shutting down
			}
		}

		// Verbose logging?
		if(success && log != nullptr && CLog::GetLogLevel() >= CLog::VERBOSE)
		{
			auto peer = (client != nullptr ? client->GetPeerAddress() : nullptr);
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Sent %s to %s"), log, peer->ai_canonname);
			}
		}
	}
	else
	{
		// Forward to database
		try
		{
			CWeakSingleton<CPersistentData>()->AddMsg(msg);
		}
		catch(CWeakSingleton<CPersistentData>::NoInstance&)
		{
			// Shutting down
			ASSERT(FALSE);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send a message
//
void CEquipControl::Send(_In_ CNetConnection<void>* client, _In_ const SVCPMsg& msg,
						 _In_opt_ LPCTSTR log)
{
	if (CEquipCtrlNet::Send(static_cast<CEquipCtrlNet*>(client), &msg) && log != nullptr && CLog::GetLogLevel() >= CLog::VERBOSE)
	{
		auto peer = (client != nullptr ? client->GetPeerAddress() : nullptr);
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Sent %s to %s"), log, peer->ai_canonname);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Helper function to build and send SFrequencyVsChannelResp messages
//
void CEquipControl::SendFrequencyVsChannelResp(_In_opt_ CNetConnection<void>* source, _In_ SEquipCtrlMsg& resp, _In_ const SEquipCtrlMsg::SGetOccupancyCmd& cmd,
	_In_ const CTask::TaskParams& taskParams) const
{
	resp.hdr.msgSubType = SEquipCtrlMsg::OCC_FREQUENCY_RESULT;
	resp.hdr.bodySize = sizeof(SEquipCtrlMsg::SFrequencyVsChannelResp); // TODO support variable length messages
	SEquipCtrlMsg::SFrequencyVsChannelResp& freqVsChan = resp.body.frequencyVsChannelResp;
	freqVsChan.occHdr.status = ErrorCodes::SUCCESS;
	m_navigation->GetGpsResponse(freqVsChan.occHdr.gpsResponse);
	freqVsChan.occHdr.numTotalChannels = 0;
	freqVsChan.occHdr.numTimeOfDays = 0;
	freqVsChan.numBands = int(taskParams.size());

	for (int band = 0; band < int(SEquipCtrlMsg::MAX_OCCBANDS); ++band)
	{
		// Exclude bands (numBlocks == 0) specify 0 channels
		freqVsChan.numChannels[band] = (band < freqVsChan.numBands && taskParams[band].numBlocks > 0 ?
			static_cast<unsigned long>((taskParams[band].stopFreq - taskParams[band].startFreq) / taskParams[band].bw) : 0);
		freqVsChan.occHdr.numTotalChannels += freqVsChan.numChannels[band];
	}

	freqVsChan.saveIntermediateData = cmd.saveIntermediateData;
	freqVsChan.useSecondaryThreshold = cmd.useSecondaryThreshold;
	freqVsChan.occPrimaryThreshold[0] = cmd.occPrimaryThreshold[0];
	freqVsChan.occPrimaryThreshold[1] = cmd.occPrimaryThreshold[1];
	freqVsChan.occSecondaryThreshold[0] = cmd.occSecondaryThreshold[0];
	freqVsChan.occSecondaryThreshold[1] = cmd.occSecondaryThreshold[1];
	freqVsChan.selectedAntenna = cmd.ant;

	const std::string& name = m_config->GetServerName();
	strncpy(freqVsChan.hostName, name.c_str(), sizeof(freqVsChan.hostName));

	size_t band = 0;
	size_t bandChan = 0;

	// May need multiple messages
	for (freqVsChan.occHdr.firstChannel = 0;
		freqVsChan.occHdr.firstChannel < freqVsChan.occHdr.numTotalChannels;
		freqVsChan.occHdr.firstChannel += SEquipCtrlMsg::MAX_OCCCHANNELS)
	{
		freqVsChan.occHdr.numChannels =
			(freqVsChan.occHdr.numTotalChannels - freqVsChan.occHdr.firstChannel > SEquipCtrlMsg::MAX_OCCCHANNELS ?
			SEquipCtrlMsg::MAX_OCCCHANNELS : freqVsChan.occHdr.numTotalChannels - freqVsChan.occHdr.firstChannel);

		for (size_t chan = 0; chan < SEquipCtrlMsg::MAX_OCCCHANNELS; ++chan)
		{
			if (chan < freqVsChan.occHdr.numChannels)
			{
				while (bandChan == freqVsChan.numChannels[band])
				{
					// Next band
					++band;
					bandChan = 0;
				}

				freqVsChan.frequencies[chan] = (taskParams[band].startFreq +
					bandChan * taskParams[band].bw + taskParams[band].bw / 2).GetRaw();
				++bandChan;
			}
			else
			{
				freqVsChan.frequencies[chan] = Units::Frequency(0).GetRaw();
			}
		}
		Send(source, resp, _T("OCC_FREQUENCY_RESULT"));
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send a RT_SPECTRUM_START or RT_DF_START message
//
void CEquipControl::SendRealtimeStart(_In_ const CTask::Task& task) const
{
	const CTask::TaskParams& taskParams = task->GetAllTaskParams();
	assert(taskParams.size() > 0);
	unsigned long numBands = 0;

	for (size_t band = 0; band < taskParams.size(); ++band)
	{
		if (taskParams[band].numBlocks > 0 &&
			(taskParams[band].mode == CConfig::EMode::AVD || taskParams[band].mode == CConfig::EMode::OCCUPANCY ||
			taskParams[band].mode == CConfig::EMode::SCAN_DF || taskParams[band].mode == CConfig::EMode::FAST_SCAN_DF))
		{
			++numBands;
		}
	}

	SSmsRealtimeMsg* startMsg = new(offsetof(SSmsRealtimeMsg, body.startV2.band) + numBands * sizeof(SSmsRealtimeMsg::SStartV2::SBand)) SSmsRealtimeMsg;
	startMsg->hdr.msgType = (task->UsingDf() ? SSmsRealtimeMsg::RT_DF_START : SSmsRealtimeMsg::RT_SPECTRUM_START);

	// Version 2
	startMsg->hdr.msgTypeVersion = 2;
	startMsg->hdr.bodySize = offsetof(SSmsRealtimeMsg, body.startV2.band) + numBands * sizeof(SSmsRealtimeMsg::SStartV2::SBand);
	startMsg->body.startV2.taskId = task->m_taskIdKey.taskId;
	startMsg->body.startV2.numBands = 0;

	for (size_t band = 0; band < taskParams.size(); ++band)
	{
		if (taskParams[band].numBlocks > 0 &&
			(taskParams[band].mode == CConfig::EMode::AVD || taskParams[band].mode == CConfig::EMode::OCCUPANCY ||
			taskParams[band].mode == CConfig::EMode::SCAN_DF || taskParams[band].mode == CConfig::EMode::FAST_SCAN_DF))
		{
			// Include band
			startMsg->body.startV2.band[startMsg->body.startV2.numBands].firstChanFreq = (taskParams[band].startFreq + taskParams[band].bw / 2).GetRaw();
			startMsg->body.startV2.band[startMsg->body.startV2.numBands].chanSize = taskParams[band].bw.GetRaw();
			startMsg->body.startV2.band[startMsg->body.startV2.numBands].numChan =
				static_cast<unsigned long>((taskParams[band].stopFreq - taskParams[band].startFreq) / taskParams[band].bw);
			++startMsg->body.startV2.numBands;
		}
	}

	m_realtimeNet.SendToAllClients(startMsg);

	// Version 1
	startMsg->hdr.msgTypeVersion = 1;
	startMsg->hdr.bodySize = sizeof(SSmsRealtimeMsg::SStart);
	startMsg->body.start.taskId = task->m_taskIdKey.taskId;
	startMsg->body.start.numBands = 0;

	for (size_t band = 0; band < taskParams.size(); ++band)
	{
		if (taskParams[band].numBlocks > 0 &&
			(taskParams[band].mode == CConfig::EMode::AVD || taskParams[band].mode == CConfig::EMode::OCCUPANCY ||
			taskParams[band].mode == CConfig::EMode::SCAN_DF || taskParams[band].mode == CConfig::EMode::FAST_SCAN_DF))
		{
			try
			{
				// Include band
				startMsg->body.start.firstChanFreq[startMsg->body.start.numBands] = (taskParams[band].startFreq + taskParams[band].bw / 2).Hz<unsigned long>(true);
				startMsg->body.start.chanSize[startMsg->body.start.numBands] = taskParams[band].bw.Hz<unsigned long>();
				startMsg->body.start.numChan[startMsg->body.start.numBands] =
					static_cast<unsigned long>((taskParams[band].stopFreq - taskParams[band].startFreq) / taskParams[band].bw);

				if (++startMsg->body.start.numBands == SSmsRealtimeMsg::SStart::MAX_OCCBANDS - 1)
				{
					break;
				}
			}
			catch(std::overflow_error&)
			{
				// Frequency out of 32-bit range so skip
			}
		}
	}

	if(startMsg->body.start.numBands > 0)
	{
		m_realtimeNet.SendToAllClients(startMsg);
	}

	delete startMsg;

	return;
}

//////////////////////////////////////////////////////////////////////
///<summary>
///Validate that the requested antenna and frequency are in
/// conformance with TCI5143_MULTIPLE_ANT antenna specifications.
///</summary>
///
/// <param name="ant">
///@param[in] Has the requested antenna value.
/// </param>
///
/// <param name="freq">
///@param[in] Has the requested measurement frequency.
/// </param>
///
/// <returns>
///@return: ANT1 for various V/USHF antenna connected to RF1 or HF antenna.
///	    ANT1H for Horizontal polarized antenna.
///	    ANT2 for SHF Extension Horn antenna.
/// </returns>
///
/// <remarks>
/// @note: 1. This method will throw when error is found.
///	   2. The code assumes caller has checked for TCI5143_MULTIPLE_ANT system.
/// </remarks>
///
inline SEquipCtrlMsg::EAnt CEquipControl::ValidateMultAnt(SEquipCtrlMsg::EAnt ant, Units::Frequency freq) const
{
	ASSERT(m_config->IsProcWithAntSelectionSwitch());
	auto retAntVal = ant;

	//Only verify custom defined antenna.
	if(ant >= SEquipCtrlMsg::EAnt::ANT1_HF && ant <= SEquipCtrlMsg::EAnt::ANT1_647H_MON)
	{
		auto forcedHf = false;
		retAntVal = CheckMultAntValue(ant, forcedHf);

		if(forcedHf)
		{
			if(!m_config->IsInHfFreqRange(Units::Frequency(freq)))
			{
				throw ErrorCodes::INVALIDFREQUENCY;
			}
		}
		else if((retAntVal == SEquipCtrlMsg::EAnt::ANT1 ||
		    retAntVal == SEquipCtrlMsg::EAnt::ANT1H) &&
		    !m_config->IsFreqAboveVuhfMin(retAntVal, freq))
		{
			throw ErrorCodes::INVALIDFREQUENCY;
		}
	}

	return(retAntVal);

}
