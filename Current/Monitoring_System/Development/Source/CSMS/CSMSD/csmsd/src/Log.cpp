/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include <Log.h>
#include <syslog.h>
#include "Utility.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
CRWLock CLog::m_critSect;
CLog::ELogLevel CLog::m_logLevel = CLog::INFORMATION;

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CLog::CLog()
{
	openlog("csmsd", 0, LOG_LOCAL0);
}

//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CLog::~CLog()
{
	closelog();
}

//////////////////////////////////////////////////////////////////////
//
// Returns a singleton instance of this class
//
CLog& CLog::Instance(void)
{
	CLockGuard lock(m_critSect);
	static CLog singleton;
	lock.Unlock();

	return singleton;
}


//////////////////////////////////////////////////////////////////////
//
// Log an Event
//
void CLog::Log(ELogLevel level, const char* format, ...)
{
	const CLog& instance = Instance();

	CLockGuard lock(m_critSect);

	// Check logging level
	if(instance.m_logLevel == VERBOSE ||
		(instance.m_logLevel == INFORMATION && level != VERBOSE) ||
		(instance.m_logLevel == WARNINGS && (level == WARNINGS || level == ERRORS)) ||
		(instance.m_logLevel == ERRORS && level == ERRORS))
	{
		std::string string = Utility::CurrentTimeAsString(Utility::MSEC);

		switch(level)
		{
		case VERBOSE:
		case INFORMATION:
			string += _T(' ');
			break;

		case WARNINGS:
			string += _T(" Warning: ");
			break;

		case ERRORS:
			string += _T(" ERROR: ");
			break;
		}

		char str[256];
		va_list args;
		va_start(args, format);
		vsnprintf(str, 256, format, args);
		va_end(args);
		string += str;

		printf("%s\n", string.c_str());

		syslog(LOG_INFO, "%s", string.c_str());
	}
	return;
}
