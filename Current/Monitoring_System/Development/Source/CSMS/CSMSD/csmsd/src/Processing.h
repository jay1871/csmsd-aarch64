/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "Config.h"
#include "Digitizer.h"
#include "EquipCtrlMsg.h"
#include "HwControl.h"
#include "Ne10Vec.h"
#include "NetConnection.h"
#include "NoiseEstimate.h"
#include "PbCal.h"
#include "PriorityQueue.h"
#include "ProcessorNode.h"
#include "RadioEquip.h"
#include "RealtimeNet.h"
#include "Singleton.h"
#include "Task.h"
#include "Units.h"
#include "Watchdog.h"

class CDf;

class CProcessing
{
private:
	// Constants
	static const unsigned int QUEUE_DEPTH = 256;

public:
	// Constants
	// Thermal noise power is -204 dbwatts @ 1Hz. => 4e-21 watts/Hz
	static constexpr const float SPUR_CAL_NOISE_FLOOR_FACTOR = 4e-22f;	// 10 dB below thermal noise?

#ifdef CSMS_DEBUG
	static bool s_traceAvd;
#endif

	// Types
	typedef CPriorityQueue<CTask::Task, QUEUE_DEPTH> TaskQueue;

	// Functions
	void AddTask(_In_ const CTask::Task& task, int priority, unsigned int timeout);
	static std::pair<int, int> BandLimit(Ne10F32Vec& data, const Units::FreqPair& dataLimits, const Units::FreqPair& freqLimits);
	static Units::Frequency BandMidFrequency(const Units::FreqPair& dataLimits, const Units::FreqPair& freqLimits);
	void Stop(void);

	// Data
	static bool m_debug;

protected:
	friend class CAudio;

	// Functions
	CProcessing(void);	// Can only be constructed by a derived class
	virtual ~CProcessing(void);

	// Data
	mutable CRealtimeNet m_realtimeNet;

private:
	// Constants
	static constexpr double PAN_TIMEOUT = 2.0 / 86400.0; // 2 seconds
	static constexpr const float SPUR_CAL_THRESHOLD = pow(10.0f, 10.0f / 10.0f);  // 10 dB as ratio
	static constexpr const float SPUR_CAL_THRESHOLD_PAN = pow(10.0f, 14.0f / 10.0f);  // 14 dB as ratio
	static constexpr const float TWO_DB = pow(10.0f, 2.0f / 10.0f); // 2 dB as ratio

	// Types
	typedef CProcessorNode::SDfDataPtr MsgMapValue;

	struct SDataPointers
	{
		struct SPtr
		{
			uncached_ptr iq;
			uncached_ptr psd;
			uncached_ptr fftI;
			uncached_ptr fftQ;
		};
		std::vector<SPtr> data;
		SPtr term;
	};

	// Functions
	float CalcShfExtGain(SEquipCtrlMsg::EAnt eAnt, Units::Frequency tunedFreq);
	void DoAvd(_In_ const CTask::Task& task);
	void DoDf(const CTask::Task& task);
	void DoMeasure(_In_ const CTask::Task& task);
	void DoOccupancy(_In_ const CTask::Task& task);
	void DoPan(_In_ const CTask::Task& task);
	void DoPbCal(_In_ const CTask::Task& task);
	void DoScanDf(_In_ const CTask::Task& task);
	bool FindDataBlocks(const CTask::Task& task, unsigned int numAnts, SDataPointers& ptrs, unsigned int timeout = CSBufferPSdma::SAMPLES_TIMEOUT);
	void FlushData(bool needSlave, const CTask::SBlockState& blockState);
	void GetCorrectedSpectrum(const CTask::Task& task, int numBins, const uncached_ptr& psdData, Ne10F32Vec& watts, bool usedShf) const;
	void GetCorrections(const CTask::Task& task, int numBins, float& gainAdj, Ne10F32Vec& adjPower, bool usedShf) const;
	void GetData(const CTask::Task& task, bool needSlave, const SDataPointers& ptrs, int numBins, Ne10F32Vec& watts, Ne10F32cVec& samples, Ne10F32Vec& terminatedData, bool usedShf) const;
	void GetData(const CTask::Task& task, bool needSlave, const SDataPointers& ptrs, int numBins, Ne10F32Vec& watts, Ne10F32Vec& terminatedData, bool usedShf) const;
	void GetData(const CTask::Task& task, bool needSlave, const SDataPointers& ptrs, int numBins, Ne10F32Vec& watts, bool usedShf) const;
	bool GetDfData(const CTask::Task& task, const SDataPointers& ptrs, unsigned long numBins, std::vector<Ne10F32Vec>& wattsBins, Ne10F32Vec& terminatedData);
	void GetSlaveData(unsigned long bufferKey, const std::vector<size_t>& sentIndex, std::vector<Ne10F32Vec>& slaveWatts,
		std::vector< std::vector<unsigned long> >& slaveIq) const;
	void GetSlaveWatts(unsigned long bufferKey, const std::vector<size_t>& sentIndex, std::vector<Ne10F32Vec>& slaveWatts) const;
#ifdef CSMS_DEBUG
	void OutVoltages(size_t firstBin, size_t lastBin) const;
#endif
	void SaveRestartData(_In_ const CTask::Task& task);
	void SendRealtimeSpectrumMsg(const CTask::Task& task, const Ne10F32Vec& chanWatts);
	void SendRealtimeDfMsg(const CTask::Task& task, const Ne10F32Vec& chanWatts,
			const CDf& df, bool horiz, unsigned long offset);
	void Thread(void);
	bool WaitForSlaves(const std::vector<size_t>& sentIndex, unsigned long tag, MsgMapValue& responses) const;

	// Data
	CSingleton<const CConfig> m_config;
	std::vector<Ne10F32Vec> m_dfWatts;
	CSingleton<CDigitizer> m_digitizer;
	std::vector<CNoiseEstimate::NoiseEstimate> m_noise;
	CSingleton<CPbCal> m_pbCal;
	int m_processingPingSource;
	TaskQueue m_queue;
	std::vector<unsigned char> m_realtimeBuf;
	CSingleton<CRadioEquip> m_radioEquip;
	std::vector<Ne10F32cVec2> m_refVolts;
	std::vector<Ne10F32cVec2> m_sampleVolts;
	std::thread m_thread;
	CSingleton<CWatchdog> m_watchdog;
};

