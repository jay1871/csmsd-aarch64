/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "StatsNoHist.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CStatsNoHist::CStatsNoHist(void) :
	m_count(0),
	m_max(-DBL_MAX),
	m_min(DBL_MAX),
	m_sum(0),
	m_sumSq(0)
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CStatsNoHist::~CStatsNoHist(void)
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update running statistics
//
void CStatsNoHist::AddValue(double value)
{
	++m_count;
	m_sum += value;
	m_sumSq += value * value;

	if (value > m_max)
	{
		m_max = value;
	}

	if (value < m_min)
	{
		m_min = value;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Clear running statistics
//
void CStatsNoHist::Clear(void)
{
	m_count = 0;
	m_max = -DBL_MAX;
	m_min = DBL_MAX;
	m_sum = 0;
	m_sumSq = 0;

	return;
}
