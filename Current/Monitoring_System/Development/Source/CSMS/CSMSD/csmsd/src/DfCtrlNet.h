/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "Bist.h"
#include "Config.h"
#include "DfCtrlMsg.h"
#include "VCPMsg.h"
#include "NetConnection.h"
//#include "PriorityQueue.h"
#include "RWLock.h"
#include "SlaveControl.h"

template <class internT, class externT, class stateT>
struct codecvt : std::codecvt<internT,externT,stateT>
{ ~codecvt(){} };

class CDfCtrlNet : public CNetConnection<CDfCtrlNet> //, protected CBist
{
public:
	// Types
//	typedef std::unique_ptr<SDfCtrlMsg> TaggedResponseQueueItem;
	// Functions
	CDfCtrlNet(CProcessorNode* const proc, unsigned long myNodeId, LPCTSTR service); // Server
	CDfCtrlNet(SOCKET socket, _In_ CDfCtrlNet*); // Dynamic connection
	CDfCtrlNet(CProcessorNode* const proc, const CConfig::SProcessorNodes::SNode& node, unsigned long myNodeId, LPCTSTR service);	// Client connection
	~CDfCtrlNet(void);
	void AddBistResults(std::vector<CBist::SBistResult>& allResults) const;
	bool DequeueTaggedResponse(unsigned long tag, std::unique_ptr<SDfCtrlMsg>& dfMsg, unsigned int timeout = 1000)
	{
		return m_taggedResponseQueue.Dequeue(tag, dfMsg, timeout);
	}
	bool GetFaultResp(SDfCtrlMsg::SGetSlaveCsmsFaultResp& resp) const;
	bool GetSlaveAudioParamsResp(SDfCtrlMsg::SSetSlaveAudioParamsResp& resp) const;
	bool IsBistDone(void) const { CSharedLockGuard lock(m_bistResultLock); return m_bistResultDone; }
	void Ping(void);
	virtual bool Send(_In_bytecount_(size) const void* msg, size_t size);
	static bool Send(_In_ CDfCtrlNet* client, _In_ const SDfCtrlMsg& msg)
	{
		return CNetConnection<CDfCtrlNet>::Send(client, &msg, offsetof(SDfCtrlMsg, body) + msg.hdr.bodySize, m_clientSendTimeout);
	}
	bool SendDoBist(void);
	bool SendGetFault(void);
	bool SendSetAudioParams(const C3230::SDecimation& decimations, const SEquipCtrlMsg::SAudioParamsCmd& audiocmd, const CNetConnection<void>* audiosource);
	bool SendFreeSlaveAudioChannel(unsigned long channelnum, bool freeall, const CNetConnection<void>* audiosource);
	void SendError(_In_ const SDfCtrlMsg::SHdr& hdr, unsigned long code);
	static void SetStartupDone(void) { m_startupDone = true; }
	static bool GetStartupDoneState(void) {return m_startupDone;}
	bool UsingExternalClock(void) const { return m_usingExternalClock; }

private:
	// Constants
	static const DWORD m_clientSendTimeout = 10000;		// msec


	struct SClientControlMap
	{
		std::map<SDfCtrlMsg::SVersionKey, SDfCtrlMsg> msgMap;	// holds client requests send and responses received
		mutable std::mutex msgMapLock;
	};

	// Functions
	inline virtual size_t BodySize(_In_ const void* hdr) const;
	static bool DownConvert(_Inout_ const SDfCtrlMsg*& msg);
	static bool DownConvert(_Inout_ const SVCPMsg*& msg);
	virtual size_t HeaderSize(void) const { return sizeof(SDfCtrlMsg::SHdr); }
	bool IsRepeatClientControlMsg(const SDfCtrlMsg* msg);
	virtual void OnClose(int errorCode); // Callback
	virtual void OnConnect(void); // Callback
	virtual void OnMessage(_In_bytecount_(size) const void* msg, size_t size); // Callback
	void OnInternalDfCtrl(const SDfCtrlMsg* msg, CDfCtrlNet* source);
	void SendMsgVersions(unsigned long subType) const;
	static void TimerThread(sigval arg) { if (arg.sival_ptr) static_cast<CDfCtrlNet*>(arg.sival_ptr)->TimerThread(); return; }
	void TimerThread(void);
	static void UpConvert(_Inout_ const SDfCtrlMsg*& msg);

	// Data
	static bool m_startupDone;		// SHared and used by all client instances
	mutable CRWLock m_bistResultLock;
	std::vector<SDfCtrlMsg::SDoBistResp> m_bistResult;	// bist result kept in client instance
	bool m_bistResultDone;								// bist result kept in client instance
	SClientControlMap m_clientControlMsgMap;			// holds client requests send and responses received
	SSmsMsg::VersionMap m_clientVersionMap;
	mutable CRWLock m_faultRespLock;
	mutable CRWLock m_SetAudioParamsLock;
	SDfCtrlMsg::SGetSlaveCsmsFaultResp m_faultResp;
	SDfCtrlMsg::SSetSlaveAudioParamsResp m_setslaveaudioparamsResp;
	bool m_faultRespDone;
	CProcessorNode* const m_proc;
	const unsigned long m_remoteNodeId;
	const unsigned long m_thisNodeId;
	static SSmsMsg::VersionMap m_versionMap;
	CDfCtrlNet* m_server;
	std::unique_ptr< CSingleton<CSlaveControl> > m_slaveControl;
	std::unique_ptr< CSingleton<CDigitizer> > m_digitizer;
	CTaggedQueue m_taggedResponseQueue;
	timer_t m_timerId;
	bool m_usingExternalClock;		// status of slave node kept in client instance
	bool m_setAudioParamsDone;
};


