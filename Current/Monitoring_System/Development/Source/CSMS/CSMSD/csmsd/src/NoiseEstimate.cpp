/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2012-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/
#include "stdafx.h"

#include "NoiseEstimate.h"

const float CNoiseEstimate::DEFAULT_SNR = 10.0f;

/////////////////////////////////////////////////////////////////////////////
//
// Constructor
//
CNoiseEstimate::CNoiseEstimate(const Ne10F32Vec& watts, unsigned long numChans, int suppressed, float snr, unsigned long binsPerChan, float enbw) :
	m_noise(std::max(1ul, (numChans - abs(suppressed / int(binsPerChan))) / CHANS_PER_BLOCK)),
	m_numBinsPerChan(binsPerChan),
	m_numChannels(numChans - abs(suppressed / int(binsPerChan))),
	m_offset(suppressed <= 0 ? 0 : suppressed / binsPerChan),
	m_snr(snr)
{
	if (numChans > watts.size())
	{
		TRACE("CNoiseEstimate ctor, numChans(%lu) > watts.size(%u)\n", numChans, watts.size());
		assert(false);
	}
	unsigned long scaledByBins = (m_numBinsPerChan < 3 ? 1 : m_numBinsPerChan - 2);	// Excluded guard bins

	if (numChans < 2 * CHANS_PER_BLOCK)
	{
		if (m_numBinsPerChan < 3)
		{
			// Use all bins
			ne10sNthMaxElement(&watts[m_offset], m_numChannels * m_numBinsPerChan, m_numChannels * m_numBinsPerChan * (100 - NOISE_PERCENTILE) / 100, m_noise[0]);
			m_noise[0] /= enbw;
		}
		else
		{
			// Use only guard bins
			Ne10F32Vec guardBins(2 * m_numChannels);

			for (auto chan = 0u; chan < m_numChannels; ++chan)
			{
				guardBins[2 * chan] = watts[m_offset + chan * m_numBinsPerChan];
				guardBins[2 * chan + 1] = watts[m_offset + (chan + 1) * m_numBinsPerChan - 1];
			}

			ne10sNthMaxElement(&guardBins[0], 2 * m_numChannels, m_numChannels, m_noise[0]);
		}

		m_noise[0] *= scaledByBins;
	}
	else
	{
		Ne10F32Vec guardBins(2 * CHANS_PER_BLOCK);
		size_t block;

		for (block = 0; block < m_noise.size() - 1; ++block)
		{
			if (m_numBinsPerChan < 3)
			{
				// Use all bins
				ne10sNthMaxElement(&watts[m_offset + block * CHANS_PER_BLOCK * m_numBinsPerChan], CHANS_PER_BLOCK * m_numBinsPerChan, CHANS_PER_BLOCK * m_numBinsPerChan * (100 - NOISE_PERCENTILE) / 100, m_noise[block]);
				m_noise[block] /= enbw;
			}
			else
			{
				// Use only guard bins
				for(auto chan = 0u; chan < CHANS_PER_BLOCK; ++chan)
				{
					guardBins[2 * chan] = watts[m_offset + block * CHANS_PER_BLOCK + chan * m_numBinsPerChan];
					guardBins[2 * chan + 1] = watts[m_offset + block * CHANS_PER_BLOCK + (chan + 1) * m_numBinsPerChan - 1];
				}

				ne10sNthMaxElement(&guardBins[0], 2 * CHANS_PER_BLOCK, CHANS_PER_BLOCK, m_noise[block]);
			}

			m_noise[block] *= scaledByBins;
		}

		if (m_numBinsPerChan < 3)
		{
			// Use all bins
			unsigned long numBinsCurBlock = (m_numChannels - (block * CHANS_PER_BLOCK)) * m_numBinsPerChan;
			ne10sNthMaxElement(&watts[m_offset + block * CHANS_PER_BLOCK * m_numBinsPerChan], numBinsCurBlock, numBinsCurBlock * (100 - NOISE_PERCENTILE) / 100, m_noise[block]);
			m_noise[block] /= enbw;
		}
		else
		{
			// Use only guard bins
			auto numChans = m_numChannels - block * CHANS_PER_BLOCK;
			guardBins.resize(2 * numChans);

			for(auto chan = 0u; chan < numChans; ++chan)
			{
				guardBins[2 * chan] = watts[m_offset + block * CHANS_PER_BLOCK + chan * m_numBinsPerChan];
				guardBins[2 * chan + 1] = watts[m_offset + block * CHANS_PER_BLOCK + (chan + 1) * m_numBinsPerChan - 1];
			}

			ne10sNthMaxElement(&guardBins[0], 2 * numChans, numChans, m_noise[block]);
		}

		m_noise[block] *= scaledByBins;
	}

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Get noise estimate as the minimum estamate over the whole spectrum
//
float CNoiseEstimate::GetNoise(void) const
{
	return m_noise.minimum();
}


/////////////////////////////////////////////////////////////////////////////
//
// Get noise estimate for a specific channel
//
float CNoiseEstimate::GetNoise(unsigned long chan) const
{
	ASSERT(chan < (m_numChannels + m_offset));
	chan = (chan < m_offset ? 0 : chan - m_offset);

	if (chan <= CHANS_PER_BLOCK / 2 || m_numChannels < 2 * CHANS_PER_BLOCK)
	{
		return m_noise[0];
	}
	else if (chan >= m_noise.size() * CHANS_PER_BLOCK - CHANS_PER_BLOCK / 2)
	{
		return m_noise[m_noise.size() - 1];
	}
	else
	{
		// Interpolate
		size_t lower = (chan - CHANS_PER_BLOCK / 2) / CHANS_PER_BLOCK;
		return m_noise[lower] + (chan - lower * CHANS_PER_BLOCK - CHANS_PER_BLOCK / 2) * (m_noise[lower + 1] - m_noise[lower]) / CHANS_PER_BLOCK;
	}
}


/////////////////////////////////////////////////////////////////////////////
//
// Update with new spectrum
//
void CNoiseEstimate::Update(const Ne10F32Vec& watts, float timeConstant, float enbw)
{
	ASSERT(m_numChannels <= watts.size());
	float noise;
	unsigned long scaledByBins = (m_numBinsPerChan < 3 ? 1 : m_numBinsPerChan - 2);	// Excluded guard bins

	if (m_numChannels < 2 * CHANS_PER_BLOCK)
	{
		if (m_numBinsPerChan < 3)
		{
			// Use all bins and 70th percentile
			ne10sNthMaxElement(&watts[m_offset], m_numChannels * m_numBinsPerChan, m_numChannels * m_numBinsPerChan * (100 - NOISE_PERCENTILE) / 100, noise);
			noise /= enbw;
		}
		else
		{
			// Use only guard bins and median
			Ne10F32Vec guardBins(2 * m_numChannels);

			for (auto chan = 0u; chan < m_numChannels; ++chan)
			{
				guardBins[2 * chan] = watts[m_offset + chan * m_numBinsPerChan];
				guardBins[2 * chan + 1] = watts[m_offset + (chan + 1) * m_numBinsPerChan - 1];
			}

			ne10sNthMaxElement(&guardBins[0], 2 * m_numChannels, m_numChannels, noise);
		}

		noise *= scaledByBins;
		m_noise[0] = (1 - timeConstant) * m_noise[0] + timeConstant * noise;
	}
	else
	{
		size_t block;
		Ne10F32Vec guardBins(2 * CHANS_PER_BLOCK);

		for (block = 0; block < m_noise.size() - 1; ++block)
		{
			if (m_numBinsPerChan < 3)
			{
				// Use all bins
				ne10sNthMaxElement(&watts[m_offset + block * CHANS_PER_BLOCK * m_numBinsPerChan], (CHANS_PER_BLOCK * m_numBinsPerChan), (CHANS_PER_BLOCK * m_numBinsPerChan * (100 - NOISE_PERCENTILE)) / 100, noise);
				noise /= enbw;
			}
			else
			{
				// Use only guard bins
				for(auto chan = 0u; chan < CHANS_PER_BLOCK; ++chan)
				{
					guardBins[2 * chan] = watts[m_offset + block * CHANS_PER_BLOCK + chan * m_numBinsPerChan];
					guardBins[2 * chan + 1] = watts[m_offset + block * CHANS_PER_BLOCK + (chan + 1) * m_numBinsPerChan - 1];
				}

				ne10sNthMaxElement(&guardBins[0], 2 * CHANS_PER_BLOCK, CHANS_PER_BLOCK, noise);
			}

			noise *= scaledByBins;
			m_noise[block] = (1 - timeConstant) * m_noise[block] + timeConstant * noise;
		}

		if (m_numBinsPerChan < 3)
		{
			// Use all bins
			unsigned long numBinsCurBlock = (m_numChannels - block * CHANS_PER_BLOCK) * m_numBinsPerChan;
			ne10sNthMaxElement(&watts[m_offset + block * CHANS_PER_BLOCK * m_numBinsPerChan], numBinsCurBlock, numBinsCurBlock * (100 - NOISE_PERCENTILE) / 100, noise);
			noise /= enbw;
		}
		else
		{
			// Use only guard bins
			auto numChans = m_numChannels - block * CHANS_PER_BLOCK;
			guardBins.resize(2 * numChans);

			for(auto chan = 0u; chan < numChans; ++chan)
			{
				guardBins[2 * chan] = watts[m_offset + block * CHANS_PER_BLOCK + chan * m_numBinsPerChan];
				guardBins[2 * chan + 1] = watts[m_offset + block * CHANS_PER_BLOCK + (chan + 1) * m_numBinsPerChan - 1];
			}

			ne10sNthMaxElement(&guardBins[0], 2 * numChans, numChans, noise);
		}

		noise *= scaledByBins;
		m_noise[block] = (1 - timeConstant) * m_noise[block] + timeConstant * noise;
	}

	return;
}
