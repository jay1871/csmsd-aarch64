/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "Config.h"
#include "Digitizer.h"
#include "ProcessorNode.h"
#include "Singleton.h"
#include "Watchdog.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CDigitizer::CDigitizer() :
	C3230(CSingleton<CProcessorNode>()->IsProcessorDfSlave(), CSingleton<CConfig>()->AllowShutdown()),
	m_watchdog(),
	m_watchdogPingSource(0)
{
	if (!Is3230Ready())		// 3230 failed to initialize
	{
		throw std::runtime_error("Unable to initialize 3230 digitizer");
	}
	return;
}

//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CDigitizer::~CDigitizer()
{
	TRACE("CDigitizer dtor\n");
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Enable the watchdog
//
void CDigitizer::EnableWatchdog(bool enable)
{
	if (enable)
	{
		m_watchdog.reset(new CSingleton<CWatchdog>);
		m_watchdogPingSource = (*m_watchdog)->RegisterPingSource(_T("3230 Digitizer Thread"));
	}
	else
	{
		(*m_watchdog)->UnregisterPingSource(m_watchdogPingSource.exchange(0));
		m_watchdog = nullptr;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Ping the watchdog (virtual call from C32202001)
//
void CDigitizer::PingWatchdog(void)
{
	if (m_watchdogPingSource != 0)
	{
		(*m_watchdog)->Ping(m_watchdogPingSource);
	}

	return;
}



