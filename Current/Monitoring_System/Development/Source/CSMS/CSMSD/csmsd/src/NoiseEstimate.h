/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2012-2015 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once
#include "Ne10Vec.h"

class CNoiseEstimate
{
public:
	// Constants
	static const float DEFAULT_SNR;

	// Types
	typedef std::unique_ptr<CNoiseEstimate> NoiseEstimate;

	// Functions
	static NoiseEstimate Create(const Ne10F32Vec& watts, unsigned long numChans, int suppressed, float snr = DEFAULT_SNR, unsigned long binsPerChan = 1, float enbw = 1)
		{ return NoiseEstimate(new CNoiseEstimate(watts, numChans, suppressed, snr, binsPerChan, enbw)); }
	float GetNoise(void) const;
	float GetNoise(unsigned long chan) const;
	float GetThreshold(void) const { return GetNoise() * m_snr; }
	float GetThreshold(unsigned long chan) const { return GetNoise(chan) * m_snr; }
	void Update(const Ne10F32Vec& watts, float timeConstant, float enbw = 1);

private:
	// Constants
	static const size_t CHANS_PER_BLOCK = 400;
	static const unsigned char NOISE_PERCENTILE = 30;

	// Functions
	CNoiseEstimate(const Ne10F32Vec& watts, unsigned long numChans, int suppressed, float snr, unsigned long binsPerChan, float enbw);

	// Data
	Ne10F32Vec m_noise;				// In Channels
	unsigned long m_numBinsPerChan;	// Number of bins in a single channel
	unsigned long m_numChannels;	// In Channels (total bins = m_numChannels * numBinsPerChan)
	unsigned long m_offset;			// In Channels
	float m_snr;					// Ratio
};

