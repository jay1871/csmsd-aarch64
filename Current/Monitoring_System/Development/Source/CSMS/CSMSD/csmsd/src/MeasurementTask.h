/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "Ionogram.h"
#include "Navigation.h"
#include "Ne10Vec.h"
#include "Stats.h"
#include "Task.h"
#include "Units.h"

template <typename T> class CNetConnection;
struct SEquipCtrlMsg;

class CMeasurementTask :
	// Inheritance
	public virtual CTask
{
	// Friends
	friend class CAvdTask;

public:
	// Constants
//	static const unsigned int MAX_IQ_SAMPLES = 2097152;

	// Types
	typedef std::shared_ptr<CMeasurementTask> MeasurementTask;

	// Functions
	void AddBlock(const Ne10F32cVec& samples, const Ne10F32Vec& watts, const CTask::Task& task);
	void AddDf(float az, float el, float conf, DATE time, float refPowerDbm, float maxSamplePowerDbm,
		size_t maxSamplePowerIdx, bool horiz);
	void AddPower(float dbm, float snr, bool horiz);
	void AddVolts(const Ne10F32cVec& volts, float dbmRef, float dbmSample, size_t refAntNum, size_t dfAntNum,
		size_t count, bool horiz);
	void CalcDfSummary(void);
	void Calibrate(_In_ const CTask::Task& task, size_t calibrateStep, size_t numCalSteps, float watts);
	static MeasurementTask Create(_In_opt_ const SEquipCtrlMsg* cmd, _In_opt_ CNetConnection<void>* source/*, EPriority priority*/);
	bool EnoughGoodCuts(void) const;
	unsigned long GetDigitizerBlockCount(void) const { return m_digitizerBlockCount; }
	DATE GetIqStartTime(void) const { return m_cmd.iqCmd.startTime; }
	unsigned long GetDfNumBins(void) const { return m_dfNumBins; }
	ne10_float32_t GetPrevAz(bool horiz) const { return m_prevAz[horiz ? 1 : 0]; }
	const SEquipCtrlMsg& GetResponse(void) const { return m_resp; }
	const Ne10F32Vec& GetTerminatedData(void) const { return m_terminatedData; }
	void InitDf(DATE time) { m_dfStartTime = time; }
	bool IsAvd(void) const { return m_msgHdr.msgType == SEquipCtrlMsg::AUTOVIOLATE_CTRL; }
	void SetTerminatedData(_In_ const Ne10F32Vec& terminatedData) { m_terminatedData = terminatedData; }
	void UpdateIqData(const uncached_ptr samples, const std::vector<unsigned long>& slaveSamples, const unsigned int numSamples, const unsigned int packetIndex, const CTask::Task& task, bool lastPacket, int slaveSampleIndex = 0);
	static SEquipCtrlMsg Validate(_In_ const SEquipCtrlMsg::SGetMeasCmd& cmd);

protected:
	// Functions

	// Data

private:
	// Constants
	static const unsigned int AZ_HIST_BINSIZE = 5; // degrees
	static constexpr const float HALF_AZ_PEAK_WIDTH = 10 * Units::D2R; // 10 degrees

	// Types
	struct Deleter
	{
		void operator()(_In_ CMeasurementTask* task) { delete task; }
	};

	struct SDfData
	{
		SDfData(void) : azConfVec(), elConfVec(), cut(0) {}
		SDfData(const ne10_fft_cpx_float32_t& azConfVec_, const ne10_fft_cpx_float32_t& elConfVec_, size_t cut_) :
			azConfVec(azConfVec_), elConfVec(elConfVec_), cut(cut_) {}
		ne10_fft_cpx_float32_t azConfVec;
		ne10_fft_cpx_float32_t elConfVec;
		size_t cut;
	};

	typedef std::list<SDfData> DfData;

	// Functions
	CMeasurementTask(_In_ SRestartData& restartData);
	virtual unsigned long RestartDataType(void) const { return 0; }
	virtual void SaveRestartData(_Inout_ std::vector<unsigned char>& data) const;

	// Functions
	CMeasurementTask(_In_opt_ const SEquipCtrlMsg* cmd, _In_opt_ CNetConnection<void>* source/*, EPriority priority*/);
	~CMeasurementTask(void);
	double GetGeoidValue(double latitude, double longitude);
	const SEquipCtrlMsg GetValidateResponse(void) const;
	void CalcBw(double freqOffsetLo, double freqOffsetHi, Units::Frequency freq, double binSize, double correction,
		_Out_ double& freqLo, _Out_ double& freqHi, _Out_ double& bw);
	void UnwrapPhase(const Ne10F32cVec& samples, Ne10F32Vec& unwrappedPhase, float& intercept, float& slope);
	void UpdateBw(const Ne10F32Vec& watts, double binSize, size_t count);
	void UpdateFieldStrength(_In_ const Ne10F32cVec& samples, float gainAdj, _In_ const CTask::Task& task);
	void UpdateFieldStrength(const Ne10F32Vec& magnitude, const CTask::Task& task);
	void UpdateFreq(float slope, double sampleRate, size_t count, size_t step);
	void UpdateFreq(const Ne10F32Vec& watts, double binSize, size_t count, size_t step);
	void UpdateFreqDwell(size_t count, size_t step);
	void UpdateMod(_In_ const Ne10F32cVec& samples, const Ne10F32Vec& unwrappedPhase, float phaseOffset, float phaseSlope, double sampleRate, double bw, size_t count);
	// Data

	double m_altitude;
	CStats m_amDepth;
	CStats m_amMinus;
	CStats m_amPlus;
	unsigned int m_azHist[2][Units::DEG_PER_CIRCLE / AZ_HIST_BINSIZE];
	CStats m_betaFreqOffsetHi;
	CStats m_betaFreqOffsetLo;
	unsigned int m_bwDwellCount;
	float m_cableLoss; // Switch / SHF converter output to chassis input correction
	bool m_calibrated;
	SEquipCtrlMsg::SGetMeasCmd m_cmd;
	float m_conf;
	mutable std::mutex m_critSect;
	unsigned long m_digitizerBlockCount;  // currently only used for iqData
	DfData m_dfData[2];
	unsigned int m_dfDwellCount;
	unsigned long m_dfNumBins;
	DATE m_dfStartTime;
	CStats m_dwellAmMinus;
	CStats m_dwellAmPlus;
	CStats m_dwellFieldStrength;
	double m_dwellFmPeak;
	CStats m_dwellFmRms;
	CStats m_dwellFreqOffset;
	double m_dwellMaxHoldPower;
	Ne10F32Vec m_dwellMaxHoldSpectrum;
	double m_dwellPmPeak;
	CStats m_dwellPmRms;
	CStats m_fieldStrength;
	unsigned int m_fieldStrengthDwellCount;
	CStats m_fmPeak;
	CStats m_fmRms;
	unsigned int m_freqDwellCount;
	CStats m_freqOffset;
	CSingleton<const CIonogram> m_ionogram;
	unsigned int m_iqDataSamples;
	SEquipCtrlMsg m_iqResp;
	double m_maxHoldPower;
	unsigned int m_modDwellCount;
	CSingleton<const CNavigation> m_navigation;
	size_t m_numDfCuts[2];
	CStats m_pmPeak;
	CStats m_pmRms;
	float m_power; // Used to accumulate calibration data
	ne10_float32_t m_prevAz[2];
	float m_rxCorr; // Correction to chassis input (voltage ratio)
	SEquipCtrlMsg m_resp;
	double m_snr; // As a ratio
	Ne10F32Vec m_spectrum;
	Ne10F32Vec m_terminatedData;
	CStats m_x1FreqOffsetHi;
	CStats m_x1FreqOffsetLo;
	CStats m_x2FreqOffsetHi;
	CStats m_x2FreqOffsetLo;
};
