/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *E
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "EquipCtrlMsg.h"
#include "Units.h"

class C72362001
{
public:
	// Types
	enum ERefMode { RECEIVE, TX_BITE };

	// Constants
	static const size_t NUM_ANTS = 9;
	enum ESource // Some values match hardware - don't change!
	{
		TERM	= 0,
		MONITOR	= -1,
		REF		= -2,
		BITE	= -3,
		NOISE	= -4,
		ANT_1	= 0x01,
		ANT_2	= 0x02,
		ANT_3	= 0x03,
		ANT_4	= 0x04,
		ANT_5	= 0x05,
		ANT_6	= 0x06,
		ANT_7	= 0x07,
		ANT_8	= 0x08,
		ANT_9	= 0x09,
		ANT_REF	= 0x0a,
		ANT_NOISE = 0x0f
	};

	// Functions
	static float GetCalDbm(size_t ch);
	static Units::Frequency GetCalFreq(void);
	static unsigned long GetControlWord(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf);
	unsigned long GetControlWord(const std::vector<unsigned long>& pattern);
	void GetCurrentControlWord(unsigned long& controlWord) const
		{ std::lock_guard<std::mutex> lock(m_critSect); controlWord = m_controlWord; return; }
	bool IsPresent(void) const { return m_isPresent; }
	void SetSwitch(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf);
	void SetSwitch(const SVCPMsg::USwitch::SHf& req);
	static ESource Source(size_t index);
	static ESource Source(SEquipCtrlMsg::EAnt ant);

protected:
	// Types
	struct SCalSettings
	{
		Units::Frequency freq;
		float refDbm;
		float sampleDbm;
//		float passRangeDbm;		//Half range where the measured value should lay.
//		unsigned long control;
	};

	// Constants
	static const SCalSettings CAL_SETTINGS;

	// Functions
	C72362001(void); // Can only be constructed by a derived class
	virtual ~C72362001(void);

private:
	// Types

	// Constants
//	static const unsigned long CROSSOVER_FREQ_HZ = 285000;

	// Functions
	static unsigned int CalcSettleTime(const unsigned long currCtrlWord, const unsigned long newCtrlWord);
	void WaitForSettle(unsigned long usec);

	// Data
	mutable std::mutex m_critSect;
	volatile unsigned long m_controlWord;
	static Units::Frequency m_crossOverFreq;
	bool m_isPresent;
};


//////////////////////////////////////////////////////////////////////
//
// ++ operator for ESource
//
inline C72362001::ESource& operator++(C72362001::ESource& s)
{
	switch(s)
	{
	case C72362001::ANT_1:
	case C72362001::ANT_2:
	case C72362001::ANT_3:
	case C72362001::ANT_4:
	case C72362001::ANT_5:
	case C72362001::ANT_6:
	case C72362001::ANT_7:
	case C72362001::ANT_8:
		s = static_cast<C72362001::ESource>(s + 1);
		break;

	case C72362001::ANT_9:
		s = C72362001::ANT_1;
		break;

	default:
		break;
	}

	return s;
}


//////////////////////////////////////////////////////////////////////
//
// Conversion function for ESource
//
inline C72362001::ESource C72362001::Source(size_t index)
{
	ASSERT(index < NUM_ANTS);
	if (index == 0)
		return ESource::ANT_REF;

	return static_cast<ESource>(index);
}


//////////////////////////////////////////////////////////////////////
//
// Conversion function for ESource
//
inline C72362001::ESource C72362001::Source(SEquipCtrlMsg::EAnt ant)
{
	if (CConfig::IsTermAnt(ant))
		return C72362001::TERM;

	switch(ant)
	{
	case SEquipCtrlMsg::ANT1:
		return C72362001::MONITOR;

	case SEquipCtrlMsg::ANT2:
	case SEquipCtrlMsg::ANT3:
	case SEquipCtrlMsg::SMPL_RF2:
	case SEquipCtrlMsg::SMPL_RF3:
		return C72362001::TERM;

	case SEquipCtrlMsg::DF_ANT_1V:
		return C72362001::ANT_1;

	case SEquipCtrlMsg::DF_ANT_2V:
		return C72362001::ANT_2;

	case SEquipCtrlMsg::DF_ANT_3V:
		return C72362001::ANT_3;

	case SEquipCtrlMsg::DF_ANT_4V:
		return C72362001::ANT_4;

	case SEquipCtrlMsg::DF_ANT_5V:
		return C72362001::ANT_5;

	case SEquipCtrlMsg::DF_ANT_6V:
		return C72362001::ANT_6;

	case SEquipCtrlMsg::DF_ANT_7V:
		return C72362001::ANT_7;

	case SEquipCtrlMsg::DF_ANT_8V:
		return C72362001::ANT_8;

	case SEquipCtrlMsg::DF_ANT_9V:
		return C72362001::ANT_9;

	default:
		ASSERT(FALSE);
		return C72362001::TERM;
	}
}



