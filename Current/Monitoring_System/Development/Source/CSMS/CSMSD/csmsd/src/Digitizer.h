/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "3230.h"
#include "Watchdog.h"

class CDigitizer :
	// Inheritance
	public C3230
{
	// Friends
	friend class CSingleton<CDigitizer>;

public:
	// Functions
	void EnableWatchdog(bool enable);

protected:
	// Functions
	CDigitizer(void);
	~CDigitizer(void);

private:
	// Functions
	virtual void PingWatchdog(void);

	// Data
	std::unique_ptr<CSingleton<CWatchdog>> m_watchdog;
	std::atomic_int m_watchdogPingSource;
};

