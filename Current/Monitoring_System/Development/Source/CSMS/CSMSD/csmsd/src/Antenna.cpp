/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"
#include "Antenna.h"
#include "mspack.h"
#include "CabFile.h"
#include "PatternDatFile.h"
#include "Utility.h"

//////////////////////////////////////////////////////////////////////
//
// Create the derived antenna class based on the antenna type
//
CAntenna* CAntenna::Create(const EAntennaType& e)
{
	switch (e)
	{
	case EAntennaType::UHF_641: return new C641_ant;
	case EAntennaType::UHF_641_COMPASS: return new C641_compass_ant;
	case EAntennaType::UHF_643_3: return new C643_3_ant;
	case EAntennaType::UHF_643_3_COMPASS: return new C643_3_compass_ant;
	case EAntennaType::UHF_645: return new C645_ant;
	case EAntennaType::UHF_645_COMPASS: return new C645_compass_ant;
	case EAntennaType::UHF_645_8: return new C645_8_ant;
	case EAntennaType::UHF_645_8_COMPASS: return new C645_8_compass_ant;
	case EAntennaType::USHF_647: return new C647_ant;
	case EAntennaType::USHF_647_COMPASS: return new C647_compass_ant;
	case EAntennaType::USHF_647D: return new C647D_ant;
	case EAntennaType::USHF_647D_COMPASS: return new C647D_compass_ant;
	case EAntennaType::USHF_647D_HOR_MON_SWTCH: return new C647D_HMon_Switch_ant;
	case EAntennaType::USHF_647D_VERT_MON_SWTCH: return new C647D_VMon_Switch_ant;
	case EAntennaType::USHF_649_8: return new C649_8_ant;
	case EAntennaType::USHF_649_8_COMPASS: return new C649_8_compass_ant;
	case EAntennaType::HF_632: return new C632_ant;
	case EAntennaType::HF_7235_COMPASS: return new C7235_compass_ant;
	case EAntennaType::HF_7235: return new C7235_ant;
	case EAntennaType::HF_620: return new C620_ant;
	case EAntennaType::HF_7031: return new C7031_ant;
	case EAntennaType::VUHF_640_DSC: return new C640_DSC_ant;
	case EAntennaType::SHF_640_2: return new C640_2_ant;
	case EAntennaType::VUHF_AUX1: return new CAux1_ant;
	case EAntennaType::CUSTOM_ANT: return new CCustom_ant;
	case EAntennaType::CUSTOM_LPA_ANT: return new CCustom_LPA_ant;
	default: return nullptr;
	}
}

////
//////////////////////////////////////////////////////////////////
//
// Get the dual pol fourier pattern
//
bool CAntenna::GetDualPolFourierPattern(const SWfaParams2& wfaParams2, size_t numAnts, Units::Frequency freq, bool interpolate,
	DualPolPattern& outPat)
{
	std::vector<SDualPolCoef> coef;		// normalized ampl/phase coefficients for same and opposite polarization as antenna
	const DualPolFourierPatterns& dualPolPat = wfaParams2.dualPolPat;
	DualPolFourierPatterns::const_iterator entry = dualPolPat.lower_bound(freq);	// first element with key >= freq
	if (entry == dualPolPat.end())
	{
		if (dualPolPat.empty())
		{
			// No patterns
			return false;
		}
		else
		{
			// Above highest pattern frequency, use last value
			coef = dualPolPat.rbegin()->second.coef;
		}
	}
	else if (entry == dualPolPat.begin())
	{
		// Below lowest pattern frequency
		coef = dualPolPat.begin()->second.coef;
	}
	else
	{
		// Pick closest in frequency or interpolate
		CAntenna::DualPolFourierPatterns::const_iterator prev(entry);
		--prev;

		if (interpolate)
		{
			float frac = (freq - prev->first).Hz<float>() / (entry->first - prev->first).Hz<float>();
			coef.resize(entry->second.coef.size());
			for (size_t i = 0; i < entry->second.coef.size(); ++i)
			{
				const CAntenna::SDualPolCoef& entryCoef = entry->second.coef[i];
				const CAntenna::SDualPolCoef& prevCoef = prev->second.coef[i];
				coef[i].amplSame = frac * (entryCoef.amplSame - prevCoef.amplSame) + prevCoef.amplSame;
				coef[i].phaseSame = frac * (entryCoef.phaseSame - prevCoef.phaseSame) + prevCoef.phaseSame;
				coef[i].amplOther = frac * (entryCoef.amplOther - prevCoef.amplOther) + prevCoef.amplOther;
				coef[i].phaseOther = frac * (entryCoef.phaseOther - prevCoef.phaseOther) + prevCoef.phaseOther;
			}
		}
		else
		{
			if (freq - prev->first < entry->first - freq)
				coef = prev->second.coef;
			else
				coef = entry->second.coef;
		}
	}

	// Normalize
	double normSame = 0;
	double normOther = 0;
	for (size_t i = 0; i < coef.size(); ++i)
	{
		normSame += coef[i].amplSame * coef[i].amplSame;
	}
	for (size_t i = 0; i < coef.size(); ++i)
	{
		normOther += coef[i].amplOther * coef[i].amplOther;
	}
	if (normSame > 0)
	{
		double norm = sqrt(normSame);
		outPat.normSame = static_cast<float>(norm);
		for (size_t i = 0; i < coef.size(); ++i)
			coef[i].amplSame = static_cast<float>(coef[i].amplSame / norm);
	}
	else
	{
		outPat.normSame = 0;
	}
	if (normOther > 0)
	{
		double norm = sqrt(normOther);
		outPat.normOther = static_cast<float>(norm);
		for (size_t i = 0; i < coef.size(); ++i)
			coef[i].amplOther = static_cast<float>(coef[i].amplOther / norm);
	}
	else
	{
		outPat.normOther = 0;
	}

	// Convert Fourier coefficients to conjugate cartesian scaled so that pattern vectors are unit vectors
	float pnorm = sqrt(static_cast<float>(numAnts));
	outPat.coefSame.resize(coef.size());
	outPat.coefOther.resize(coef.size());
	for (unsigned long i = 0; i < coef.size(); ++i)
	{
		outPat.coefSame[i].r = coef[i].amplSame * cos(coef[i].phaseSame * Units::D2R) / pnorm;
		outPat.coefSame[i].i = -coef[i].amplSame * sin(coef[i].phaseSame * Units::D2R) / pnorm;
		outPat.coefOther[i].r = coef[i].amplOther * cos(coef[i].phaseOther * Units::D2R) / pnorm;
		outPat.coefOther[i].i = -coef[i].amplOther * sin(coef[i].phaseOther * Units::D2R) / pnorm;
	}
	outPat.normSame /= pnorm;
	outPat.normOther /= pnorm;

	// Calculate the dot product of the same and other coefficients
	Ne10F32cVec conjOther(outPat.coefOther.size());
	ne10sConj(&outPat.coefOther[0], &conjOther[0], outPat.coefOther.size());
	ne10sDotProd(outPat.coefSame, conjOther, outPat.otherSame);
	outPat.otherSame = outPat.otherSame * static_cast<float>(numAnts);
	outPat.maxMode = wfaParams2.maxMode;

	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Get the (complex conjugate) antenna pattern for this frequency for wfa
//
const CAntenna::WfaConjPattern* CAntenna::GetWfaConjPattern(Units::Frequency freq)
{
	WfaConjPatterns::const_iterator upper = m_wfaConjPatterns.upper_bound(freq);

	if (upper == m_wfaConjPatterns.end())
	{
		if (m_wfaConjPatterns.empty())
		{
			// No patterns
			return nullptr;
		}
		else
		{
			// Above highest pattern frequency
			return &(--upper)->second;
		}
	}
	else if (upper == m_wfaConjPatterns.begin())
	{
		// Below lowest pattern frequency
		return &upper->second;
	}
	else
	{
		// Pick closest in frequency
		WfaConjPatterns::const_iterator lower(upper);

		if (freq - (--lower)->first > upper->first - freq)
		{
			return &upper->second;
		}
		else
		{
			return &lower->second;
		}
	}
}


//////////////////////////////////////////////////////////////////////
//
// Load the symmetry df parameters
//
void CAntenna::LoadSymmetryDfParams(SSymmetryDfParams& p, const std::string& filename, const std::string& section)
{
	CIniFile ini(filename, section);

	p.numAnts = static_cast<unsigned char>(ini.GetEntry(section, "numAnts", 0ul));
	p.arrayOffset = ini.GetEntry(section, "arrayOffset", 0.0f, false);

	std::string wfa = ini.GetEntry(section, "wfaFile", std::string(), false);
	if (!wfa.empty())
	{
		LoadWfaDfParams2(TCIPaths::configDir + wfa, p.wfaParams2);
	}

	int numMethods = ini.GetEntry(section, "numMethods", 0ul);
	p.methods.resize(numMethods);

	// If any method specifies WFA make sure we have WFA params
	for (int i = 0; i < numMethods; ++i)
	{
		p.methods[i].freq = ini.GetEntry(section, "freq", Units::Frequency(0), true, i);
		p.methods[i].method = static_cast<SSymmetryDfParams::SMethod::EMethod>(ini.GetEntry(section, "method", 0ul, true, i));
		p.methods[i].uniqueConf = ini.GetEntry(section, "uniqueConf", 0.75f, true, i);
		p.methods[i].gridWidth = static_cast<unsigned char>(ini.GetEntry(section, "gridWidth", 30ul, true, i));
		p.methods[i].mainLobeWidth = static_cast<unsigned short>(ini.GetEntry(section, "mainLobeWidth", 180ul, true, i));
		p.methods[i].phaseAdjust = (ini.GetEntry(section, "phaseAdjust", 1ul, false, i) == 1ul);
		if (p.methods[i].method == SSymmetryDfParams::SMethod::WFA || p.methods[i].method == SSymmetryDfParams::SMethod::WFA_MODES_INV)
		{
			if (wfa.empty())
			{
				CLog::Log(CLog::WARNINGS, "Config data missing: key %s not found in section %s\n", "wfaFile", section.c_str());
				throw std::runtime_error("wfaFile is missing");
			}
		}
	}

	for (size_t i = 0; i < ini.GetEntry(section, "numRadTerms", 0ul); ++i)
	{
		p.radiusMap[ini.GetEntry(section, "radFreq", Units::Frequency(0), true, i)] =  ini.GetEntry(section, "radius", 0.0f, true, i);
	}

	for (size_t i = 0; i <  ini.GetEntry(section, "numOffsetTerms", 0ul, false); ++i)
	{
		p.offsetMap[ini.GetEntry(section, "offFreq", Units::Frequency(0), false, i)] =  ini.GetEntry(section, "offset", 0.0f, false, i);
	}

	p.pairFactor = ini.GetEntry(section, "pairFactor", 0.0f, false);

	for (size_t i = 0; i < ini.GetEntry(section, "numModesTerms", 0ul, false); ++i)
	{
		p.modesfactorMap[ini.GetEntry(section, "modesFreq", Units::Frequency(0), false, i)] = ini.GetEntry(section, "modesFactor", 0.0f, false, i);
	}

	for (size_t i = 0; i < ini.GetEntry(section, _T("numPostCorrTerms"), 0ul, false); ++i)
	{
		p.postcorrMap[ini.GetEntry(section, _T("postcorrFreq"), Units::Frequency(0), false, i)] = ini.GetEntry(section, "postcorrOffset", 0.0f, false, i);
	}

	for (size_t i = 0; i < ini.GetEntry(section, "numModesAzimMapTerms", 0ul, false); ++i)
	{
		p.modesazimMap[ini.GetEntry(section, "modesazimFreq", Units::Frequency(0), false, i)] = ini.GetEntry(section, "modesazimFactor", 0.0f, false, i);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Load the watson-watt params
//
void CAntenna::LoadWatsonWattDfParams(SWatsonWattParams& p, const std::string& filename, const std::string& section)
{
	CIniFile ini(filename, section);

	unsigned int numPhases = ini.GetEntry(section, "numPhases", 0ul);
	p.phaseFreq.resize(numPhases);
	p.phase.resize(numPhases);

	for(unsigned int i = 0; i < numPhases; ++i)
	{
		p.phaseFreq[i] = ini.GetEntry(section, "phaseFreq", 0ul, true, i);
		p.phase[i] = short(ini.GetEntry(section, "phase", 0ul, true, i));
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Load the WFA params
//
void CAntenna::LoadWfaDfParams(SWfaParams& p, const std::string& filename, const std::string& section)
{
	CIniFile ini(filename, section);

	int numAnts = ini.GetEntry(section, "numAnts", 5ul);
	p.coarseAzStep = ini.GetEntry(section, "coarseAzStep", 2ul);
	p.uniqueConf = ini.GetEntry(section, "uniqueConf", 1.0f);
	bool evenSymmetry = false;
	std::string wfa = ini.GetEntry(section, "patternFile", std::string());
	if (wfa.empty())
	{
		CLog::Log(CLog::WARNINGS, "Config data missing: key %s not found in section %s\n", "patternFile", section.c_str());
		throw std::runtime_error("patternFile is missing");
	}
	std::string patternFile(TCIPaths::configDir + wfa);

	// This function makes the assumption that the original windows cab file has been converted
	// to a new format (using a Windows program) which is a single binary file in the following format:
	//
	// Record 1: 36 bytes
	//   int numPols;	// offset 0
	//   int zSym;		// offset 4
	//   int numEls;	// offset 8
	//   int numFileAz; // offset 12
	//   float azStart;	// offset 16
	//   float azInc;	// offset 20
	//   double freqMhz; // offset 24
	//   int nfc;		// offset 32
	//
	// Record 2: 8 * numFileAz bytes
	//   { float mag; float phase; }[numFileAz];
	//
	// Records 3,5,... are same as Record 1
	// Records 2,4,... are same as Record 2
	//

	CCabFile cabFile(patternFile.c_str());
	TRACE("Loading patterns from file %s\n", patternFile.c_str());
	char errMsg[40];
 #ifdef CSMS_DEBUG
	size_t loopcnt = 0;
 #endif


	do
	{
		std::string fname = cabFile.GetFileName();
		TRACE(" %s...\n", fname.c_str());
		std::string line;

		// Read parameters, skipping blank line(s)
		while(cabFile.GetLine(line) && line.length() == 0);

		// Parse and store parameters
		int numPols;
		int zSym;
		int numEls;
		int numFileAz;
		float dummy;
		float azStart;
		float azInc;
		double freqMhz;
		int nfc;
		int ret;

		if((ret=sscanf(line.c_str(), "%d %d %d %d %f %f %f %f %lf %d",
			&numPols, &zSym, &numEls, &numFileAz, &dummy, &dummy,
			&azStart, &azInc, &freqMhz, &nfc)) != 10 || (numPols != 1) || (numEls != 1))
		{
			sprintf(errMsg, "invalid data cab file =%s  ret = %d numPols = %d numEls = %d", fname.c_str(), ret, numPols, numEls);
			//printf("numPols = %d zSym= %d numEls=%d numFileAz = %d dummy = %.2f azStart = %.2f azInc=%.2f freqMhz = %.2f nfc = %d\n",
			//		numPols, zSym, numEls, numFileAz, dummy, azStart, azInc, freqMhz, nfc);
			throw std::runtime_error(errMsg);
		}

		ASSERT(azStart == 0.0); // TODO - fix this
		int numAz = int(360.0 / azInc + 0.5);
		ASSERT(numFileAz == numAz / 2 + 1 || numFileAz == numAz || numFileAz == numAz + 1);

		if (m_wfaConjPatterns.empty())
		{
			if (numFileAz != numAz && numFileAz != numAz + 1)
			{
				evenSymmetry = true;
			}
		}
		else
		{
			if ((evenSymmetry && numFileAz == int(360.0 / azInc + 0.5)) ||
				(!evenSymmetry && numFileAz < int(360.0 / azInc + 0.5)))
			{
				TRACE("AntParms not consistent, loopcnt = %u\n", loopcnt);
				throw std::runtime_error("Invalid data in patternFile header record (inconsistent)");
			}
		}

		WfaConjPattern& conjPattern = (m_wfaConjPatterns[Units::Frequency(freqMhz * 1e6 + 0.5)]
				= WfaConjPattern(numAz, Ne10F32cVec(numAnts)));

		// Read in voltage vectors
		if(!cabFile.GetLine(line))
		{
			sprintf(errMsg, "invalid data - reading voltage vectors cab file =%s", fname.c_str());
			throw std::runtime_error(errMsg);
		}

		float scale;

		if(sscanf(line.c_str(), "%f", &scale) != 1)
		{
			sprintf(errMsg, "invalid data - reading scale cab file =%s", fname.c_str());
			throw std::runtime_error(errMsg);
		}

		for(int wiraAz = 0; wiraAz < numFileAz; ++wiraAz)
		{
			// WIRA patterns are in cartesian coordinates - convert to compass coordinates
			int az = (wiraAz == 0 ? 0 : numAz - wiraAz);
			short magOct;
			short phaseOct;

			if(!cabFile.GetOctalShort(magOct) || magOct < 0 || !cabFile.GetOctalShort(phaseOct))
			{
				sprintf(errMsg, "invalid data - GetOctal  reading scale cab file =%s", fname.c_str());
				throw std::runtime_error(errMsg);
			}

#ifdef CSMS_DEBUG
			if(magOct > 16383)
			{
				TRACE("wiraAz = %d, magOct %d, phaseOct %d\n", wiraAz, int(magOct), int(phaseOct));
			}
 #endif

			if(wiraAz >= numAz)
			{
				continue;
			}

			float mag = magOct * scale;
			float phase = float(phaseOct * Units::TWO_PI / (SHRT_MAX + 1));
			conjPattern[az][0].r = mag * cos(phase);
			conjPattern[az][0].i = -mag * sin(phase);

			// Expand symmetries
			if(evenSymmetry && az != 0 && az != numAz / 2)
			{
				conjPattern[numAz - az][0] = conjPattern[az][0];
			}

			for(int ant = 1; ant < numAnts; ant++)
			{
				conjPattern[(az + ant * numAz / numAnts) % numAz][ant] = conjPattern[az][0];

				if(evenSymmetry && az != 0 && az != numAz / 2)
				{
					conjPattern[(numAz - az + ant * numAz / numAnts) % numAz][ant] = conjPattern[az][0];
				}
			}
		}

		for(int az = 0; az < numAz; ++az)
		{
			ne10_float32_t norm;
			ne10sNorm_L2(conjPattern[az], norm);
			conjPattern[az] /= norm;
		}

		short temp;

		if(cabFile.GetOctalShort(temp))
		{
			sprintf(errMsg, "invalid data - extra GetOctal cab file =%s", fname.c_str());
			throw std::runtime_error(errMsg);
		}
	}
	while(cabFile.OpenNextFile());

	TRACE("CAntenna::LoadWfaDfParams Done: m_wfaConjPatterns.size %u\n",
			m_wfaConjPatterns.size());

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Load the dual polarization (fourier patterns) WFA params
//
void CAntenna::LoadWfaDfParams2(const std::string& filename, SWfaParams2& wfaParams2)
{
	// Open the wfa parameters file
	std::ifstream file(filename.c_str());
	if (!file)
	{
		TRACE("File %s notfound!\n", filename.c_str());
		throw std::runtime_error("wfaFile is missing");
	}

	wfaParams2.maxMode = ~0UL;

	// Read and parse the first line, skipping comment lines
	std::string line;
	while (!std::getline(file, line).eof())
	{
		if (isdigit(line[0]))		// Skip comment lines (any line starting with non-digit)
		{
			// Parse the first line to get required parameters and size the data elements
			unsigned long ul1, ul2;
			if (sscanf(line.c_str(), "%lu,%lu", &ul1, &ul2) == 2)
			{
				wfaParams2.wfaType = SWfaParams2::EWfaType(ul1);
				if (wfaParams2.wfaType != SWfaParams2::DUAL_POL_FOURIER)	// Only supported wfaType for now
				{
					throw std::runtime_error("wfaType is invalid");
				}
				wfaParams2.maxMode = ul2;
			}
			break;
		}
	}
	if (wfaParams2.maxMode == ~0UL)
	{
		throw std::runtime_error("missing definition of maxMode in wfaFile");
	}

	// Read the rest of the data lines.  Note: by construction, all frequencies will have same number of coefs
	while (!std::getline(file, line).eof())
	{
		if (isdigit(line[0]))		// Skip comment lines (any line starting with non-digit)
		{
			double frq;
			long mode;
			float f1, f2, f3, f4;
			if (sscanf(line.c_str(), "%lf,%ld,%f,%f,%f,%f", &frq, &mode, &f1, &f2, &f3, &f4) == 6)
			{
				if (static_cast<unsigned long>(abs(mode)) > wfaParams2.maxMode)	// Bad value for mode
				{
					throw std::runtime_error("abs(mode) must not be greater than maxMode in wfaFile");
				}
				Units::Frequency freq(frq * 1000000.);
				if (wfaParams2.dualPolPat.find(freq) == wfaParams2.dualPolPat.end())
				{
					wfaParams2.dualPolPat[freq].coef.resize(2 * wfaParams2.maxMode + 1);
				}
				unsigned long modeIndex = wfaParams2.maxMode + mode;
				wfaParams2.dualPolPat[freq].coef[modeIndex].amplSame = f1;
				wfaParams2.dualPolPat[freq].coef[modeIndex].phaseSame = f2;
				wfaParams2.dualPolPat[freq].coef[modeIndex].amplOther = f3;
				wfaParams2.dualPolPat[freq].coef[modeIndex].phaseOther = f4;
			}
		}
	}
	// Verify all pattern coefficients are available
	for (DualPolFourierPatterns::const_iterator it = wfaParams2.dualPolPat.begin(); it != wfaParams2.dualPolPat.end(); ++it)
	{
		for (unsigned long i = 0; i < 2 * wfaParams2.maxMode + 1; ++i)
		{
			if (it->second.coef[i].amplSame < 0 || it->second.coef[i].amplOther < 0)
			{
				char buf[72];
				snprintf(buf, 72, "missing coefficient for frequency %lf mode %ld in wfaFile",
					it->first.Hz<double>(), static_cast<long>(i) - static_cast<long>(wfaParams2.maxMode));
				throw std::runtime_error(buf);
			}
		}
	}
	return;
}



