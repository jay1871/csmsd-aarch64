/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2015 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "Ne10Vec.h"
#include "NoiseEstimate.h"
#include "SuspendableTask.h"

struct SEquipCtrlMsg;
template<typename T> class CNetConnection;


class COccupancyTask :
	// Inheritance
	public CSuspendableTask
{
public:
	// Types
	typedef std::shared_ptr<COccupancyTask> OccupancyTask;

	// Functions
	virtual void AbandonDwell(void);
	void AddBlock(_In_ const Ne10F32Vec& watts, bool partialBand);
	void AddTerminatedDataBlock(_In_ const Ne10F32Vec& terminatedData, size_t step, size_t block, bool partialBlock);
	static OccupancyTask Create(_In_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source);
	static void Create(_In_ SRestartData& restartData);
	void EnhanceBlock(_In_ const Ne10F32Vec& terminatedData, unsigned long numBins, float noiseFloor);
	void GetChanWatts(size_t band, size_t block, unsigned long binsPerChan, unsigned long numChan, Ne10F32Vec& wattsRT, float enbw);
	void NextDwell(void) { m_resetDwell = true;}
	void SendReports(bool last);
	void Update(size_t band, size_t block, Units::Frequency firstChanFreq, float switchTemp, DATE dataTime);
	void UpdateCompletionTime(DATE dataTime);
	static void Validate(_In_ const SEquipCtrlMsg::SGetOccupancyCmd& cmd);

private:
	// Constants
	static const float FRACTION_DONE_NEEDED;
	static const float NOISE_TIME_CONSTANT;

	// Types
	struct Deleter
	{
		void operator()(_In_ COccupancyTask* task) { delete task; }
	};

	struct SEFieldDataValue
	{
		SEFieldDataValue(void) : numAbove(0), sumEField(0), maxEField(-FLT_MAX) {}

		unsigned int numAbove;
		float sumEField;
		float maxEField;
	};

	struct SOccDataValue
	{
		SOccDataValue(void) : numAbove(0), prevNumAbove(0), maxOcc(0) {}

		unsigned int numAbove;
		unsigned int prevNumAbove;
		float maxOcc;
	};

	struct SMsgLenValue
	{
		SMsgLenValue(void) : numMsg(0), timeOff(0), timeOn(0), sumLen(0), maxLen(0) {}

		unsigned int numMsg;
		DATE timeOff;
		DATE timeOn;
		DATE sumLen;
		DATE maxLen;
	};

	// Functions
	COccupancyTask(_In_ SRestartData& restartData);
	virtual unsigned long RestartDataType(void) const { return SEquipCtrlMsg::OCCUP_RESTART_DATA; }
	virtual void SaveRestartData(_Inout_ std::vector<unsigned char>& data) const;

	COccupancyTask(_In_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source);
	virtual ~COccupancyTask(void);
	static SEquipCtrlMsg::SOccResult::SResultData CalcEFieldValue(_Inout_ SEFieldDataValue& value, unsigned long numLooks);
	SEquipCtrlMsg::SOccResult::SResultData GetChannelMsgLenData(_Inout_ SMsgLenValue& value, size_t chan);
	SEquipCtrlMsg::SMsgLengthDistributionResp::SMsgLenHistData
		GetChannelMsgLenDistData(_Inout_ SEquipCtrlMsg::SMsgLengthDistributionResp::SMsgLenHistData& value, size_t) { return value; }
	SEquipCtrlMsg::SOccResult::SResultData GetChannelOccData(_Inout_ SOccDataValue& value, size_t chan);
	SEquipCtrlMsg::SOccResult::SResultData GetChannelTodData(_Inout_ SOccDataValue& value, size_t chan);
	SEquipCtrlMsg::SOccResult::SResultData GetEFieldData(_Inout_ SEFieldDataValue& value, size_t chan);
	SEquipCtrlMsg::SOccResult::SResultData GetEFieldTodData(_Inout_ SEFieldDataValue& value, size_t chan);
	SEquipCtrlMsg::SOccResult::SResultData GetSpectrogramData(_Inout_ SEFieldDataValue& value, size_t chan);

	// Data
	double m_confFactor;
	size_t m_count;
	unsigned long m_currTodBin;
	std::vector<SEFieldDataValue> m_eFieldData;
	float m_fixedThresh1;
	unsigned long m_measurementTime;
	DATE m_minGap;
	std::vector<SMsgLenValue> m_msgLenData;
	std::vector<SEquipCtrlMsg::SMsgLengthDistributionResp::SMsgLenHistData> m_msgLenDistData;
	std::vector<std::vector<CNoiseEstimate::NoiseEstimate> > m_noise;
	float m_noiseThresh1;
	unsigned long m_numLooks;
	unsigned long m_numTods;
	std::vector<SOccDataValue> m_occData;
	SEquipCtrlMsg::SOccupancyOutputFlags m_outputFlags;
	unsigned long m_prevNumLooks;
	bool m_resetDwell;
	std::vector<SEFieldDataValue> m_spectrogramData;
	DATE m_startTime;
	std::vector<SOccDataValue> m_todData;
	std::vector<SEFieldDataValue> m_todEFields;
	std::vector<unsigned long> m_todNumLooks;
	std::vector<unsigned long> m_todPrevNumLooks;
	bool m_useSecondaryThreshold;
	Ne10F32Vec m_watts;
};
