/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "Stats.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CStats::CStats(void) :
	m_hist(),
	m_histHiValue(0),
	m_histLoValue(0)
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CStats::~CStats(void)
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update running statistics
//
void CStats::AddValue(double value)
{
	m_plainStats.AddValue(value);

	if (size_t numBins = m_hist.size())
	{
		int bin = int(numBins * (value - m_histLoValue) / (m_histHiValue - m_histLoValue));

		if (bin >= 0 && bin < int(numBins))
		{
			++m_hist[bin];
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Clear running statistics
//
void CStats::Clear(void)
{
	m_plainStats.Clear();
	m_hist.clear();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Least-squares line fit
//
void CStats::LeastSquaresFit(const Ne10F32Vec& data, ne10_float32_t& slope, ne10_float32_t& intercept)
{
	size_t count = data.size();
	double sumX = count * double(count - 1) / 2;
	double sumXX = count * double(count - 1) * (2 * count - 1) / 6;
	double sumY = data.sum();
	double sumXY = 0;

	for (size_t i = 0; i < count; ++i)
	{
		sumXY += i * data[i];
	}

	slope = static_cast<ne10_float32_t>((count * sumXY - sumX * sumY) / (count * sumXX - sumX * sumX));
	intercept = static_cast<ne10_float32_t>((sumY - slope * sumX) / count);

	return;
}


ne10_float32_t CStats::Percentile(const Ne10F32Vec& data, float percentile)
{
	ne10_float32_t res;
	ne10sNthMaxElement(&data[0], data.size(), size_t(data.size() * (100 - percentile) / 100), res);

	return res;
}


//////////////////////////////////////////////////////////////////////
//
// Setup histogram
//
void CStats::SetHist(double loValue, double hiValue, unsigned int numBins)
{
	m_hist.assign(numBins, 0);
	m_histHiValue = hiValue;
	m_histLoValue = loValue;

	return;
}

