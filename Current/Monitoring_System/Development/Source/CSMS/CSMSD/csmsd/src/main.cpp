/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2014-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"

#include "DfCtrlNet.h"
#include "EquipCtrlNet.h"
#include "Failure.h"
#include "Log.h"
#include "MetricsNet.h"
#include "ProcessorNode.h"
#include "ProductVersion.h"
#include "Utility.h"

#include <linux/watchdog.h>

bool RunAsDaemon(const char*, int&);
void CreateDirectories(void);

int main(int argc, char* argv[])
{
	// test code for usb port
	/*unsigned char usbport = 1;
	unsigned char retState;
	if (Utility::USBInit())
	{
		if (Utility::SetUSBPort(&usbport))
		{
			if (Utility::GetUSBPort(&retState))
			{
				if (retState == usbport)
					printf ("usb port set to %02hhx\n", retState);
			}

		}
		else
		{
			printf("requested usb port %02hhx set to %02hhx\n", usbport, retState);
		}
	}*/
	std::string pidFile = (std::string(TCIPaths::dir) + "csmsd.pid").c_str();	// "/media/tci/csms/csmsd.pid";
	std::string connectStateFile = (std::string(TCIPaths::dir) + "csmsd.state").c_str();	// "/media/tci/csms/csmsd.state";
	bool runAsDaemon = false;
	if (argc >= 2)
	{
		if (strncmp(argv[1], "--version", 9) == 0)
		{
			printf("%s(%s)\n", VersionInfo::ProductVersion, VersionInfo::FileVersion);
			return 0;
		}
		if (strncmp(argv[1], "--machineid", 11) == 0)
		{
			CCsmsLicense::SMachineId machineId;
			if (C3230::GetDeviceDnaStatic(machineId.dna) && C2630::GetMax10UniqueIdStatic(machineId.max10Id))
			{
				printf("MachineId=%06lx-%08lx:%08lx-%08lx\n", machineId.dna[0], machineId.dna[1], machineId.max10Id[0], machineId.max10Id[1]);
			}
			return 0;
		}
		if (strncmp(argv[1], "--daemon", 8) == 0)
		{
			runAsDaemon = true;
		}
		if (strncmp(argv[1], "--radiobdtype", 13) == 0) // check radio board version (spin 3 or4?)
		{
			printf("%lu\n", C2630::GetrxProcessorStatic());
			return 0;
		}
		if (strncmp(argv[1], "--radiobdver", 12) == 0) // check radio board version (spin 3 or4?)
		{
			printf("%08lx\n", C2630::GetFirmwareVersionStatic());
			return 0;
		}
	}
	// Before doing anything else, make sure that the data and log directories exist
	CreateDirectories();

	CLog::SetLogLevel(CLog::VERBOSE);

	int code;
	if (runAsDaemon && !RunAsDaemon(pidFile.c_str(), code))
	{
		return code;
	}

	sigset_t set;
	sigemptyset(&set);
	sigaddset(&set, SIGHUP);
	sigaddset(&set, SIGQUIT);
	sigaddset(&set, SIGTERM);
	sigaddset(&set, SIGUSR1);
//	sigaddset(&set, SIGINT);		// sent whenever toggling breakpoints! as well as when terminating!
	pthread_sigmask(SIG_BLOCK, &set, nullptr);	// blocks the above signals so that they can be handled at correct time

	CSingleton<CCsmsLicense> license;		// This creates the one and only instance of the license class

	CSingleton<CProcessorNode> processorNode;
	CSingleton<CConfig> configsetting;

	static const size_t maxTries = 4;
	for (size_t tries = 0; tries < maxTries; ++tries)
	{
		try
		{
			auto connectFilehandle = open(connectStateFile.c_str(), O_RDWR | O_CREAT, 0600);
			char connectStr[10];
			snprintf(connectStr, 10, "%d\n", 0);
			write(connectFilehandle, connectStr, strlen(connectStr));
			close(connectFilehandle);

			if (configsetting->IsDDRSystem())
			{
				CEquipCtrlNet equipCtrlNet("3303");
				connectFilehandle = open(connectStateFile.c_str(), O_RDWR | O_CREAT, 0600);
				snprintf(connectStr, 10, "%d\n", 1);
				write(connectFilehandle, connectStr, strlen(connectStr));
				close(connectFilehandle);
				CLog::Log(CLog::INFORMATION, _T("csmsd service started on node-%lu-DDR system"), processorNode->GetNodeId());
#if ALLOW_EQUIPCONTROL_TDOA == 1
				CLog::Log(CLog::INFORMATION, "******************************************************************");
				CLog::Log(CLog::INFORMATION, "********* THIS BUILD ALLOWS TDOA FROM EQUIPCONTROL PORT **********");
				CLog::Log(CLog::INFORMATION, "******************************************************************");
#endif
				// Wait for shutdown
				int sigNum;
				while(true)
				{
					while((sigNum = sigwaitinfo(&set, nullptr)) == -1 && errno == EINTR);
					if (sigNum != SIGUSR1)
					{
						Utility::SetQuitAll();
						CLog::Log(CLog::INFORMATION, "Received signal %d", sigNum);
						break;
					}
					CHwControl::m_debug = true;
					CProcessing::m_debug = true;
				}
				CFailure::CheckFail();

			}
			else
			{
				if (processorNode->IsProcessorMaster())
				{
					processorNode->DoDfMaster();
					CMetricsNet metricsNet("3302");
					CEquipCtrlNet equipCtrlNet("3303");

					connectFilehandle = open(connectStateFile.c_str(), O_RDWR | O_CREAT, 0600);
					snprintf(connectStr, 10, "%d\n", 1);
					write(connectFilehandle, connectStr, strlen(connectStr));
					close(connectFilehandle);
					CLog::Log(CLog::INFORMATION, _T("csmsd service started on node %lu"), processorNode->GetNodeId());
	#if ALLOW_EQUIPCONTROL_TDOA == 1
					CLog::Log(CLog::INFORMATION, "******************************************************************");
					CLog::Log(CLog::INFORMATION, "********* THIS BUILD ALLOWS TDOA FROM EQUIPCONTROL PORT **********");
					CLog::Log(CLog::INFORMATION, "******************************************************************");
	#endif
					// Wait for shutdown
					int sigNum;
					while(true)
					{
						while((sigNum = sigwaitinfo(&set, nullptr)) == -1 && errno == EINTR);
						if (sigNum != SIGUSR1)
						{
							Utility::SetQuitAll();
							CLog::Log(CLog::INFORMATION, "Received signal %d", sigNum);
							break;
						}
						CHwControl::m_debug = true;
						CProcessing::m_debug = true;
					}
					CFailure::CheckFail();
				}
				else
				{
					processorNode->DoDfSlave();

					connectFilehandle = open(connectStateFile.c_str(), O_RDWR | O_CREAT, 0600);
					snprintf(connectStr, 10, "%d\n", 1);
					write(connectFilehandle, connectStr, strlen(connectStr));
					close(connectFilehandle);
					CLog::Log(CLog::INFORMATION, _T("csmsd service started on node %lu"), processorNode->GetNodeId());

					// Wait for shutdown
					int sigNum;
					while(true)
					{
						while((sigNum = sigwaitinfo(&set, nullptr)) == -1 && errno == EINTR);
						if (sigNum != SIGUSR1) {
							Utility::SetQuitAll();
							CLog::Log(CLog::INFORMATION, "Received signal %d", sigNum);
							break;
						}
						CHwControl::m_debug = true;
						CProcessing::m_debug = true;
					}
					CFailure::CheckFail();
				}
			}
			break;
		}
		catch(CNetConnection<CMetricsNet>::CNetError& err)
		{
			if (tries == maxTries - 1)
			{
//				fprintf(stderr, "Terminating unexpectedly: CMetricsNet\n");
				remove(pidFile.c_str());
				remove(connectStateFile.c_str());
				CLog::Log(CLog::ERRORS, "Terminating unexpectedly: CMetricsNet");
				return 1;
			}
			else
			{
//				fprintf(stderr, "Retrying CMetricsNet %u\n", tries + 1);
				CLog::Log(CLog::INFORMATION, "Retrying CMetricsNet %u", tries + 1);
				sleep(30);
			}
		}
		catch(CNetConnection<CEquipCtrlNet>::CNetError& err)
		{
			if (tries == maxTries - 1)
			{
//				fprintf(stderr, "Terminating unexpectedly: CEquipCtrlNet\n");
				remove(pidFile.c_str());
				remove(connectStateFile.c_str());
				CLog::Log(CLog::ERRORS, "Terminating unexpectedly: CEquipCtrlNet");
				return 1;
			}
			else
			{
//				fprintf(stderr, "Retrying CEquipCtrlNet %u\n", tries + 1);
				CLog::Log(CLog::INFORMATION, "Retrying CEquipCtrlNet %u", tries + 1);
				sleep(30);
			}
		}
		catch(std::exception& e)
		{
//			fprintf(stderr, "Terminating unexpectedly: %s\n", e.what());
			remove(pidFile.c_str());
			remove(connectStateFile.c_str());
			CLog::Log(CLog::ERRORS, "Terminating unexpectedly: %s", e.what());
			return 1;
		}
		catch(...)
		{
//			fprintf(stderr, "Terminating unexpectedly: Unknown error\n");
			remove(pidFile.c_str());
			remove(connectStateFile.c_str());
			CLog::Log(CLog::ERRORS,  "Terminating unexpectedly: Unknown error\n");
			return 1;
		}

	}
	CLog::Log(CLog::INFORMATION, _T("csmsd service stopped on node %lu"), processorNode->GetNodeId());

	remove(pidFile.c_str());
	remove(connectStateFile.c_str());

	return 0;
}

bool RunAsDaemon(const char* pidFile, int& code)
{
	printf("csmsd daemon starting\n");

	auto pid = fork();
	if (pid < 0)
	{
		return EXIT_FAILURE;
	}
	if (pid > 0)
	{
		printf("csmsd child created: %d. Parent exiting.\n", pid);
		code = 0;
		return false;
	}
	CLog::Log(CLog::INFORMATION, "csmsd child process started");

	umask(0);	// File permissions
	auto sid = setsid();	// create a new session id for the child
	if (sid < 0)
	{
		CLog::Log(CLog::ERRORS, "setsid failed");
		code = EXIT_FAILURE;
		return false;
	}
	// Close all descriptors (except STDERR, STDOUT for now) and re-open as null
	close(STDIN_FILENO);
//		close(STDOUT_FILENO);
//		close(STDERR_FILENO);
	if (open("/dev/null", O_RDWR) == -1)
	{
		CLog::Log(CLog::ERRORS, "Open STDIN failed");
		code = EXIT_FAILURE;
		return false;
	}
//		if (open("/dev/null", O_RDWR) == -1)
//		{
//			CLog::Log(CLog::ERRORS, "Open STDOUT failed");
//			return EXIT_FAILURE;
//		}
//		if (open("/dev/null", O_RDWR) == -1)
//		{
//			CLog::Log(CLog::ERRORS, "Open STDERR failed");
//			return EXIT_FAILURE;
//		}
//	if (chdir("/media/tci/csms/") == -1)	// Change running directory
	if (chdir(TCIPaths::dir) == -1)	// Change running directory
	{
		CLog::Log(CLog::ERRORS, "Unable to change working directory.");
		code = EXIT_FAILURE;
		return false;
	}
	// Use a pidFile to make sure only one copy of program is running
	auto pidFilehandle = open(pidFile, O_RDWR | O_CREAT, 0600);
	if (pidFilehandle == -1)
	{
		CLog::Log(CLog::ERRORS, "Unable to open PID lock file.");
		code = EXIT_FAILURE;
		return false;
	}
	if (lockf(pidFilehandle, F_TLOCK, 0) == -1)
	{
		CLog::Log(CLog::ERRORS, "Unable to lock PID lock file.");
		code = EXIT_FAILURE;
		return false;
	}
	char pidStr[10];
	snprintf(pidStr, 10, "%d\n", getpid());
	write(pidFilehandle, pidStr, strlen(pidStr));

	CLog::Log(CLog::INFORMATION, "csmsd daemon running");
	code = 0;
	return true;
}

void CreateDirectories(void)
{
	std::string dirs[2] = { TCIPaths::dataDir, TCIPaths::logDir }; // { "data", "log" };
	for (size_t i = 0; i < 2; ++i)
	{
		auto rv = mkdir(dirs[i].c_str(), S_IRWXU | S_IRGRP | S_IXGRP| S_IROTH | S_IXOTH);
		if (rv == -1)
		{
			if (errno == EEXIST)
			{
				printf("%s directory exists\n", dirs[i].c_str());
			}
			else
			{
				printf("Create %s directory failed %d %s\n", dirs[i].c_str(), errno, strerror(errno));
			}
		}
		else
		{
			printf("Create %s directory successful\n", dirs[i].c_str());
		}
	}
}



