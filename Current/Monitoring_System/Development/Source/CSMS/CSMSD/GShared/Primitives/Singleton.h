/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2007-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include <atomic>
#include <mutex>
#include <stdexcept>

//template<typename T> class CWeakSingleton;

//////////////////////////////////////////////////////////////////////
//
// Smart pointer template for a ref-counted deterministic singleton using double-checked locking
//
template<typename T> class CSingleton
{
public:
	// Functions
	CSingleton(void) : CSingleton(false) {}
	CSingleton(_In_ const CSingleton&) { ++m_refCount; }
	CSingleton(_Inout_ CSingleton&&) { ++m_refCount; }
	~CSingleton(void);
	const CSingleton& operator=(const CSingleton&) { return *this; } // No-op
	const CSingleton& operator=(CSingleton&&) { return *this; } // No-op
	_Ret_ T* operator->(void) const { return m_instance; };
	_Ret_ T& operator*(void) const { return *m_instance; };

protected:
	// Functions
	CSingleton(bool weak);

	// Data
	static T* m_instance;
	static std::mutex m_mutex;
	static std::atomic_long m_refCount;
};


//////////////////////////////////////////////////////////////////////
//
// Partial specialization to make T and const T use the same instance
//
template<typename T> class CSingleton<const T>
:
	// Inheritance
	private CSingleton<T>
{
public:
	// Functions
	CSingleton(void) = default;
	CSingleton(_In_ const CSingleton&) = default;
	CSingleton(_Inout_ CSingleton&&) {}
	const CSingleton& operator=(const CSingleton&) { return *this; } // No-op
	const CSingleton& operator=(CSingleton&&) { return *this; } // No-op
	_Ret_ const T* operator->(void) const { return CSingleton<T>::m_instance; }; // Override base class
	_Ret_ const T& operator*(void) const { return *CSingleton<T>::m_instance; }; // Override base class
};


//////////////////////////////////////////////////////////////////////
//
// Weak singleton
//
template<typename T> class CWeakSingleton
:
	// Inheritance
	private CSingleton<T>
{
public:
	// Types
	struct NoInstance : public std::logic_error
	{
		NoInstance(void) : std::logic_error("no instance for weak singleton") {}
	};

	// Functions
	CWeakSingleton(void) : CSingleton<T>(true) {}
	CWeakSingleton(_In_ const CWeakSingleton&) = default;
	CWeakSingleton(_Inout_ CWeakSingleton&&) {}
	const CWeakSingleton& operator=(const CWeakSingleton&) { return *this; } // No-op
	const CWeakSingleton& operator=(CWeakSingleton&&) { return *this; } // No-op
	_Ret_ T* operator->(void) const { return CSingleton<T>::m_instance; };
	_Ret_ T& operator*(void) const { return *CSingleton<T>::m_instance; };
};


//////////////////////////////////////////////////////////////////////
//
// Partial specialization to make T and const T use the same instance
//
template<typename T> class CWeakSingleton<const T>
:
	// Inheritance
	private CWeakSingleton<T>
{
public:
	// Functions
	CWeakSingleton(void) = default;
	CWeakSingleton(_In_ const CWeakSingleton&) = default;
	CWeakSingleton(_Inout_ CWeakSingleton&&) {}
	const CWeakSingleton& operator=(const CWeakSingleton&) { return *this; } // No-op
	const CWeakSingleton& operator=(CWeakSingleton&&) { return *this; } // No-op
	_Ret_ const T* operator->(void) const { return CSingleton<T>::m_instance; }; // Override base class
	_Ret_ const T& operator*(void) const { return *CSingleton<T>::m_instance; }; // Override base class
};


//////////////////////////////////////////////////////////////////////
//
// Static data
//
template<typename T> T* CSingleton<T>::m_instance = nullptr;
template<typename T> std::mutex CSingleton<T>::m_mutex;
template<typename T> std::atomic_long CSingleton<T>::m_refCount = ATOMIC_VAR_INIT(0);


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
template<typename T> inline CSingleton<T>::CSingleton(bool weak)
{
	// Atomically increment reference count unless it's zero
	long count = m_refCount;
	while(count != 0 && !m_refCount.compare_exchange_weak(count, count + 1));

	if(count == 0)
	{
		if(weak)
		{
			// Weak singleton must not create the instance
			throw typename CWeakSingleton<T>::NoInstance();
		}
		else
		{
			// Acquire lock
			std::lock_guard<std::mutex> lock(m_mutex);

			// Check that no-one else created it or decremented the count to zero and did not delete it before we got the lock
			if(m_instance == nullptr)
			{
				// Construct instance
				m_instance = new T;
			}

			++m_refCount;
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
template<typename T> inline CSingleton<T>::~CSingleton(void)
{
	if(--m_refCount == 0)
	{
		// Acquire lock
		std::lock_guard<std::mutex> lock(m_mutex);

		// Is reference count still zero (someone else may have incremented the count before we got the lock)
		if(m_refCount == 0)
		{
			// Delete instance
			delete m_instance;
			m_instance = nullptr;
		}
	}

	return;
}
