/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#ifndef _MSC_VER
#include "MSCompat.h"
#endif

#include <typeinfo>

//////////////////////////////////////////////////////////////////////
//
// Efficient pool allocator for frequently used message structures.
// Performs deallocation and memory leak checking in a debug build.
//
// Use like this:
//
//	class CFoo
// {
// 		void* operator new(size_t s) { return CPoolAllocator<CFoo>::Allocate(s); }
//		void operator delete(void* p, size_t s) { CPoolAllocator<CFoo>::Deallocate(p, s); }
// };
//
template <typename T, size_t N = 1, size_t ALIGN = MEMORY_ALLOCATION_ALIGNMENT, size_t BLOCKS_PER_CHUNK = 64> class CPoolAllocator
{
public:
	_Ret_writes_bytes_to_(size, 0) static T* Allocate(size_t size) { return m_implementation.Allocate(size); }
	static void Deallocate(_Pre_readable_byte_size_(size) _Post_ptr_invalid_ void* block, size_t size) { m_implementation.Deallocate(block, size); }
#if defined(_DEBUG)
	_Ret_writes_bytes_to_(size, 0) static T* Allocate(size_t size, bool chkDealloc)
	{ SetDeAllocVal(chkDealloc);  return m_implementation.Allocate(size); }
	static void SetDeAllocVal(bool chkDealloc) { m_implementation.s_verifyDeAlloc = chkDealloc; return; }
#endif

private:
	class CImplementation
	{
		friend class CPoolAllocator;

	private:
		static const size_t ALIGNED_SIZE = ((N * sizeof(T) - 1) & ~(ALIGN - 1)) + ALIGN;
		static_assert(ALIGN >= MEMORY_ALLOCATION_ALIGNMENT, "ALIGN too small");
		static_assert(ALIGNED_SIZE >= sizeof(SLIST_ENTRY) && ALIGN >= sizeof(SLIST_ENTRY), "SLIST_ENTRY too large");

		CImplementation(void)
		{
#if defined(_DEBUG) || !defined(NDEBUG)
#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
			if(!InitializeCriticalSectionAndSpinCount(&m_lock, 0))
			{
				throw std::bad_alloc();
			}
#else
			InitializeSRWLock(&m_lock);
#endif
#pragma warning(suppress : 6309 6387) // No way ntdll.dll is not around!
			m_rtlFirstEntrySList = reinterpret_cast<SLIST_ENTRY* (NTAPI *)(SLIST_HEADER*)>(GetProcAddress(GetModuleHandle(_T("ntdll.dll")), "RtlFirstEntrySList"));
#else
			m_rtlFirstEntrySList = &RtlFirstEntrySList;
#endif
#endif
			// Initialize free pool with one chunk
			InitializeSListHead(&m_chunks);
			InitializeSListHead(&m_freePool);
			AddChunk();
		}

		~CImplementation(void)
		{
#if defined(_DEBUG) || !defined(NDEBUG)
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
			DeleteCriticalSection(&m_lock);

#endif
			if( s_verifyDeAlloc && (QueryDepthSList(&m_freePool) != QueryDepthSList(&m_chunks) * BLOCKS_PER_CHUNK) )
			{
				_RPT1(_CRT_ERROR, "Memory leak in pool allocator for %s\n", typeid(T).name());
			}
#endif
				
			// Free the chunks
			while(auto chunk = InterlockedPopEntrySList(&m_chunks))
			{
				_aligned_free(chunk);
			}
		}

		void AddChunk(void)
		{
			// Add chunk to list
			auto chunk = _aligned_malloc(ALIGN + BLOCKS_PER_CHUNK * ALIGNED_SIZE, ALIGN);

			if(chunk == nullptr)
			{
				throw std::bad_alloc();
			}

			InterlockedPushEntrySList(&m_chunks, static_cast<SLIST_ENTRY*>(chunk));

			// Add free blocks to list
			for(auto block = static_cast<BYTE*>(chunk) + ALIGN;
				block < static_cast<BYTE*>(chunk) + BLOCKS_PER_CHUNK * ALIGNED_SIZE + ALIGN; block += ALIGNED_SIZE)
			{
				InterlockedPushEntrySList(&m_freePool, reinterpret_cast<SLIST_ENTRY*>(block));
			}
		}

		_Ret_notnull_ _Post_writable_byte_size_(size) T* Allocate(size_t size)
		{
			if(size == N * sizeof(T))
			{
#if defined(_DEBUG) || !defined(NDEBUG)
#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
				EnterCriticalSection(&m_lock);
#else
				AcquireSRWLockExclusive(&m_lock);
#endif
#else
				std::lock_guard<std::mutex> lock(m_lock);
#endif
#endif
				void* block;
				
				while((block = InterlockedPopEntrySList(&m_freePool)) == nullptr)
				{
					// No free blocks - get another chunk
					AddChunk();
				}

#if defined(_MSC_VER) && defined(_DEBUG)
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
				LeaveCriticalSection(&m_lock);
#else
				ReleaseSRWLockExclusive(&m_lock);
#endif
#endif

				return static_cast<T*>(block);
			}
			else
			{
				// Defer to default
				return static_cast<T*>(::operator new(size));
			}
		}

		void Deallocate(_Pre_readable_byte_size_(size) _Post_ptr_invalid_ void* block, size_t size)
		{
			if(block == nullptr)
			{
				return;
			}

			if(size == N * sizeof(T))
			{
				// Return block to free pool
#if defined(_DEBUG) || !defined(NDEBUG)
#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
				EnterCriticalSection(&m_lock);
#else
				AcquireSRWLockExclusive(&m_lock);
#endif
#else
				std::lock_guard<std::mutex> lock(m_lock);
#endif

#if defined(_DEBUG)
				if( s_verifyDeAlloc )
				{
					// Validate block points to a valid block
					bool valid = false;

					for( SLIST_ENTRY* chunk = m_rtlFirstEntrySList(&m_chunks); chunk != nullptr; chunk = chunk->Next )
					{
						if( reinterpret_cast<BYTE*>(block) >= reinterpret_cast<BYTE*>(chunk)+ALIGN &&
							block <reinterpret_cast<BYTE*>(chunk)+BLOCKS_PER_CHUNK * ALIGNED_SIZE + ALIGN &&
							(reinterpret_cast<BYTE*>(block)-reinterpret_cast<BYTE*>(chunk)-ALIGN) % ALIGNED_SIZE == 0 )
						{
							// Valid address
							valid = true;
							break;
						}
					}

					if( !valid )
					{
						_RPT1(_CRT_ERROR, "Free of invalid block address in pool allocator for %s\n", typeid(T).name());
					}

					for( SLIST_ENTRY* free = m_rtlFirstEntrySList(&m_freePool); free != nullptr; free = free->Next )
					{
						if( block == free )
						{
							_RPT1(_CRT_ERROR, "Free of already freed block in pool allocator for %s\n", typeid(T).name());
						}
					}

#endif
				}
#endif

				InterlockedPushEntrySList(&m_freePool, reinterpret_cast<SLIST_ENTRY*>(block));
#if defined (_MSC_VER) && defined(_DEBUG)
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
				LeaveCriticalSection(&m_lock);
#else
				ReleaseSRWLockExclusive(&m_lock);
#endif
#endif
			}
			else
			{
				// Defer to default
				::operator delete(block);
			}

			return;
		}

#ifdef _MSC_VER
		__declspec(align(MEMORY_ALLOCATION_ALIGNMENT)) SLIST_HEADER m_chunks;
		__declspec(align(MEMORY_ALLOCATION_ALIGNMENT)) SLIST_HEADER m_freePool;
#else
		SLIST_HEADER m_chunks __attribute__((aligned(MEMORY_ALLOCATION_ALIGNMENT)));
		SLIST_HEADER m_freePool __attribute__((aligned(MEMORY_ALLOCATION_ALIGNMENT)));
#endif
#if defined(_DEBUG) || !defined(NDEBUG)
#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
		CRITICAL_SECTION m_lock;
#else
		SRWLOCK m_lock;
#endif
#else
		std::mutex m_lock;
#endif
		SLIST_ENTRY* (WINAPI *m_rtlFirstEntrySList)(SLIST_HEADER*);
#endif


#if defined(_DEBUG)
		static bool s_verifyDeAlloc;
#endif
	};

	CPoolAllocator(void) = delete;
	CPoolAllocator(const CPoolAllocator&) = delete;
	~CPoolAllocator(void) = delete;
	CPoolAllocator& operator=(const CPoolAllocator&) = delete;

	static CImplementation m_implementation;

};

// Static data
template <typename T, size_t N, size_t ALIGN, size_t BLOCKS_PER_CHUNK> typename CPoolAllocator<T, N, ALIGN, BLOCKS_PER_CHUNK>::CImplementation
	CPoolAllocator<T, N, ALIGN, BLOCKS_PER_CHUNK>::m_implementation;

#if defined(_DEBUG)
template <typename T, size_t N, size_t ALIGN, size_t BLOCKS_PER_CHUNK> bool CPoolAllocator<T, N, ALIGN, BLOCKS_PER_CHUNK>::CImplementation::s_verifyDeAlloc = true;
#endif
