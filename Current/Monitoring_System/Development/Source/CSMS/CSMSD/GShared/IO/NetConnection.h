/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2004-2018 TCI International, Inc. All rights reserved         *
**************************************************************************/

/////////////////////////////////////////////////////////////////////////////
//
// This class implements a generic TCP connection. It can act as either a
// client or server.
//
// To use the class, derive your own class from it, passing your class as
// the template parameter. You must implement constructors, a destructor and
// two pure virtual functions. You can implement three callbacks OnMessage,
// OnConnect and OnClose. There are functions to send a message and get connection
// status
//
// There are three modes in which the class is used:
//  Server: Listens for connections
//  Client: A persistent (reconnects if dropped) connection
//  Dynamic: Created by the server when a client connects. Deleted automatically
//   when the connection is dropped
// 
// Example:
//
//#include "NetConnection.h"
//
//class CTestConnection : public CNetConnection<CTestConnection>
//{
//public:
//	CTestConnection(LPCTSTR host, LPCTSTR service) : CNetConnection<CTestConnection>(host, service) { Startup(); } // Client
//	CTestConnection(LPCTSTR service) : CNetConnection<CTestConnection>(service) { Startup(); } // Server
//	CTestConnection(SOCKET socket, CTestConnection* server) : CNetConnection<CTestConnection>(socket) { Startup(); } // Dynamic connection to server
//	~CTestConnection(void) { Shutdown(); }
//
//private:
//	virtual size_t BodySize(const void* hdr) const;
//	virtual size_t HeaderSize() const;
//	virtual void OnClose(int errorCode); // Callback
//	virtual void OnConnect(void); // Callback
//	virtual void OnMessage(const void* msg, size_t size); // Callback
//};

#pragma once

#include <algorithm>
#include <math.h>
#include <memory>
#include <stdexcept>
#include <thread>
#include <unordered_map>
#include <vector>
#ifdef _MSC_VER
#include <process.h>
#include <winsock2.h>
#pragma warning(push)
#pragma warning(disable : 6386) // Mstcpip.h and Ws2tcpip.h in Windows 7 SDK generate spurious warnings 
#include <Mstcpip.h>
#include <Ws2tcpip.h>
#pragma warning(pop)
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
#include "RWLock.h"
#else
#include "SlimRWLock.h"
#endif
#else
#include <errno.h>
#include <fcntl.h>
#include <mutex>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <time.h>
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>

#include "MSCompat.h"
#include "RWLock.h"
#endif

template<typename T> class CNetConnection;

// Base class explicit specialization
template<> class CNetConnection<void>
{
public:
	virtual	~CNetConnection(void) {}
	_Ret_ virtual const ADDRINFOT* GetPeerAddress(void) const = 0;
};

/////////////////////////////////////////////////////////////////////////////
//
// Class Definition
//
template<typename T> class CNetConnection
	:
	// Inheritance
	public CNetConnection<void>,
	public std::enable_shared_from_this<T>
{
public:
	// Types
	typedef void (T::* ForEachCallback)(void* arg); // Callback for ForEachDynamicConnection
	enum EMode { SERVER, CLIENT, DYNAMIC }; // Mode

	class CNetError : public std::runtime_error
	{
	private:
		static std::string ErrorStr(void)
		{
#ifdef _MSC_VER
			std::string str("Unknown error");
			char* buffer;

			if(FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
				nullptr, GetLastError(), 0, reinterpret_cast<LPSTR>(&buffer), 0, nullptr))
			{
				str = buffer;
				LocalFree(buffer);
			}

			return str;
#else
			char buffer[256];

			return strerror_r(errno, buffer, sizeof(buffer));
#endif
		}

	public:
		CNetError(void) : std::runtime_error(ErrorStr().c_str()) {}
		virtual ~CNetError(void) throw() {}
	};

	// Functions
	virtual			~CNetConnection(void); // Destructor
	void			Close(void); // Close the connection
	void			DisableNewConnections(bool disable);
	void			ForEachConnection(ForEachCallback callback, _In_opt_ void* arg = nullptr); // Call function on each dynamic connection
	EMode			GetMode(void) const { return m_mode; } // Server, Client or Dynamic connection
	size_t			GetNumConnections(void); // Get number of connections
	_Ret_ virtual const ADDRINFOT* GetPeerAddress(void) const { return m_peerAddress; } // Address of peer
	double			GetReceiveLoadFactor(void) const;
	_Ret_opt_ T*	GetServer(void); // Get server associated with dynamic connection
	SOCKET			GetSocket(void) const { return m_socket; } // Get the socket
	bool			GetSockOpt(int optionName, _Out_bytecap_(*optionLen) void* optionValue, _Inout_ socklen_t* optionLen,
		int level = SOL_SOCKET); // Get socket option
	bool			IsConnected(void) const { CLockGuard lock(m_critSect); return m_connected; } // Connection state
	bool			IsPeerLocalHost(void) const; // Connected to localhost
	static bool		Send(_In_opt_ CNetConnection* connection, _In_bytecount_(size) const void* msg, size_t size, DWORD timeout = INFINITE); // Safely send a message using a dynamic connection
	virtual bool	Send(_In_bytecount_(size) const void* msg, size_t size) { return Send(msg, size, INFINITE); } // Send message
	virtual bool	Send(_In_bytecount_(size) const void* msg, size_t size, DWORD timeout); // Send message
	static void		SetFail(void (*FailFn)(std::exception_ptr)) { m_failFn = FailFn; } // Register thread failure callback
	bool			SetSockOpt(int optionName, _In_bytecount_(optionLen) const void* optionValue, socklen_t optionLen, int level = SOL_SOCKET); // Set socket option

protected:
	// Functions
	CNetConnection(LPCTSTR host, LPCTSTR service, DWORD retryConnectInterval = 30000, DWORD receiveTimeout = 30000,
		DWORD sendTimeout = INFINITE, bool connectionsDisabled = false); // Client constructor
	CNetConnection(LPCTSTR service, bool connectionsDisabled = false, size_t maxConnections = 0); // Server constructor
	CNetConnection(SOCKET socket, DWORD receiveTimeout = 30000, DWORD sendTimeout = INFINITE); // Dynamic constructor (for server connections)
	virtual size_t	BodySize(_In_ const void*) const { return 0; }; // Size of message body (given header)
	virtual size_t	HeaderSize(void) const { return 1; }; // Size of message header
	virtual void	OnClose(int errorCode); // Callback when connection is closed
	virtual void	OnConnect(void); // Callback when connection is established
	virtual void	OnMessage(_In_bytecount_(size) const void* msg, size_t size); // Callback when message is received
	void			SetNoReverse(bool noReverse) { m_noReverse = noReverse; }
	void			SetupServerSocket(void); // Configure listening socket
	void			Startup(bool initializeCom = false, DWORD coInit = COINIT_MULTITHREADED); // MUST be called by derived class constructors
	void			Shutdown(void); // MUST be called by derived class destructor
	virtual char	Delimiter(void) const { return '\n'; } // Delimiter character when HeaderSize returns 0

private:
	// Constants
	static const DWORD IDLE_POLL_INTERVAL = 1000; // ms
	static const unsigned int LOAD_FACTOR_TIME_CONSTANT = 5; // seconds

	// Types
	typedef std::unordered_map<std::shared_ptr<T>, T*> ConnectionMap;

#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
	class CLockGuard // Lock guard for CRITICAL_SECTION
	{
	public:
		CLockGuard(CRITICAL_SECTION& critSect) : critSect(critSect) { Lock(); }
		~CLockGuard(void) { Unlock(); }

		template<typename F> static auto CallLocked(CRITICAL_SECTION& critSect, F f) -> decltype(f())
		{
			CLockGuard lock(critSect);

			return f();
		}

		template<typename F> auto CallUnlocked(F f) -> decltype(f())
		{
			struct Unlocker // Releases lock while instance is in scope
			{
				Unlocker(CLockGuard& lock) : lock(lock) { lock.Unlock(); }
				~Unlocker(void) { lock.Lock(); }
				CLockGuard& lock;
			} unlock(*this);

			return f();
		}

	private:
		_Acquires_exclusive_lock_(critSect) void Lock(void) { EnterCriticalSection(&critSect); }
		_Releases_exclusive_lock_(critSect) void Unlock(void) { LeaveCriticalSection(&critSect); }

		CRITICAL_SECTION& critSect;
	};
#endif

	// Functions
	CNetConnection(void) = delete; // Disable default constructor
	CNetConnection(const CNetConnection&) = delete; // Disable copy constructor
	CNetConnection& operator=(const CNetConnection&) = delete; // Disable assignment
	void CallbackThread(bool initializeCom, DWORD coInit); // Callback thread
	void EnableKeepAlives(void); // Turn on keep-alives
	void NetworkThread(void); // Network thread
#ifdef _MSC_VER
	void OnNetwork(void); // Helper for network thread
#else
	void OnNetwork(uint32_t events); // Helper for network thread
#endif
	void OpenSocket(int family); // Open a socket
	void OnRecvDone(void); // Helper for network thread
	void OnTimeout(bool idle); // Helper for network thread
	void OnServer(void); // Helper for network thread
	size_t PurgeConnections(_In_opt_ T* const server);	// Helper to remove unneeded connections
	static void SetThreadName(LPCSTR name);
	void UpdateLoadFactor(void); // Update receive load factor

	// Data
#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
	mutable CRITICAL_SECTION m_critSect; // Critical section for thread data
	mutable CRITICAL_SECTION m_sendMutex; // Mutex for sending messages
#else
	mutable CRWLock			m_critSect; // Critical section for thread data
	mutable CRWLock			m_sendMutex; // Mutex for sending messages
#endif
	HANDLE					m_connectEvent; // Need to call OnConnect
	HANDLE					m_msgSent; // Message sent event
	HANDLE					m_recvDoneEvent; // Event to signal network thread that callback has finished with receive buffer
	HANDLE					m_recvEvent; // Need to call OnMessage
	HANDLE					m_serverEvent; // Event to signal network thread to perform server actions
	HANDLE					m_shutdown; // Event to signal thread shutdown
	WSAEVENT				m_wsaEvent; // Network event
#else
	mutable std::mutex		m_critSect; // Critical section for thread data
	mutable std::mutex		m_sendMutex; // Mutex for sending messages
	int						m_connectEvent; // Need to call OnConnect
	int						m_msgSent; // Message sent event
	int						m_recvDoneEvent; // Event to signal network thread that callback has finished with receive buffer
	int						m_recvEvent; // Need to call OnMessage
	int						m_serverEvent; // Event to signal network thread to perform server actions
	int						m_shutdown; // Event to signal thread shutdown
	int						m_callbackEpoll; // epoll FD for callback thread
	int						m_networkEpoll ;// epoll FD for network thread
	int						m_sendEpoll; // epoll FD for message sent event
	epoll_event				m_socketEvent; // epoll event settings for socket
#endif
	ADDRINFOT*				m_addrinfo; // Linked list of peer addresses
	size_t					m_bodySize; // Body size of received message
	std::thread				m_callbackThread; // Thread for callbacks
	bool					m_connected; // Socket is connected
	static ConnectionMap	m_connections; // Collection of dynamic connections
	bool					m_connectionsDisabled; // Disable new dynamic connections or client reconnects
	char					m_delimiter; // Delimiter character when m_headerSize == 0
	static void				(*m_failFn)(std::exception_ptr); // Called when a thread throws
	size_t					m_headerSize; // Header size of received message
	LARGE_INTEGER			m_lastLoadUpdate; // Performance counter when load factor last updated
	double					m_loadFactor; // Load factor for receive data
	static CRWLock			m_mapRWLock; // Readers-writer lock for connection map
	size_t					m_maxConnections; // Max connections in server mode (0 => no limit)
	EMode					m_mode; // Connection mode
	int						m_networkBufSize; // Size of the TCP network buffer
	std::thread				m_networkThread; // Thread for accepting, connecting and reading
	std::thread::id			m_networkThreadId; // Network thread's ID
	bool					m_noReverse; // Don't do reverse lookup for dynamic connections
	ADDRINFOT*				m_peerAddress; // Address of peer
	LARGE_INTEGER			m_perfFreq; // Performance counter frequency
	std::vector<char>		m_recvBuf; // Receiver message buffer
	size_t					m_recvBufCount; // Bytes in buffer
	DWORD					m_recvTimeout; // Timeout while receiving message
	DWORD					m_retryConnectInterval; // Interval between connection attempts for client
	size_t					m_sentBytesSoFar; // Bytes sent to date (per message)
	const char*				m_sendMsg; // Message to send
	size_t					m_sendSize; // Size of message to send
	DWORD					m_sendTimeout; // Timeout while sending message
	SOCKET					m_socket; // Socket for this connection
};


/////////////////////////////////////////////////////////////////////////////
//
// Static Data
//
template<typename T> typename CNetConnection<T>::ConnectionMap CNetConnection<T>::m_connections;
template<typename T> CRWLock CNetConnection<T>::m_mapRWLock;
template<typename T> void (*CNetConnection<T>::m_failFn)(std::exception_ptr) = nullptr;



/////////////////////////////////////////////////////////////////////////////
//
// Client-mode Constructor
//
template<typename T> CNetConnection<T>::CNetConnection(LPCTSTR host, LPCTSTR service,
	DWORD retryConnectInterval /* ms */, DWORD receiveTimeout /* ms */,
	DWORD sendTimeout, /* ms */
	bool connectionsDisabled)
	:
#ifdef _MSC_VER
	m_connectEvent(CreateEvent(nullptr, FALSE, FALSE, nullptr)),
	m_msgSent(CreateEvent(nullptr, FALSE, FALSE, nullptr)),
	m_recvDoneEvent(CreateEvent(nullptr, FALSE, FALSE, nullptr)),
	m_recvEvent(CreateEvent(nullptr, FALSE, FALSE, nullptr)),
	m_serverEvent(CreateEvent(nullptr, FALSE, FALSE, nullptr)),
	m_shutdown(CreateEvent(nullptr, TRUE, FALSE, nullptr)),
	m_wsaEvent(WSA_INVALID_EVENT),
#else
	m_connectEvent(eventfd(0, 0)),
	m_msgSent(eventfd(0, 0)),
	m_recvDoneEvent(eventfd(0, 0)),
	m_recvEvent(eventfd(0, 0)),
	m_serverEvent(eventfd(0, 0)),
	m_shutdown(eventfd(0, EFD_SEMAPHORE)),
	m_callbackEpoll(epoll_create1(0)),
	m_networkEpoll(epoll_create1(0)),
	m_sendEpoll(epoll_create1(0)),
#endif
	m_addrinfo(nullptr),
	m_bodySize(0),
	m_callbackThread(),
	m_connected(false),
	m_connectionsDisabled(connectionsDisabled),
	m_headerSize(1),
	m_loadFactor(0),
	m_mode(CLIENT),
	m_networkBufSize(0),
	m_networkThread(),
	m_noReverse(false),
	m_peerAddress(nullptr),
	m_recvBufCount(0),
	m_recvTimeout(receiveTimeout),
	m_retryConnectInterval(retryConnectInterval),
	m_sendSize(0),
	m_sendTimeout(sendTimeout),
	m_socket(INVALID_SOCKET)
{
#ifdef _MSC_VER
	if(m_connectEvent == nullptr || m_msgSent == nullptr || m_recvDoneEvent == nullptr || m_recvEvent == nullptr ||
		m_recvDoneEvent == nullptr || m_serverEvent == nullptr || m_shutdown == nullptr)
	{
		throw CNetError();
	}

	QueryPerformanceFrequency(&m_perfFreq);

	// Startup Winsock
	WSADATA wsaData;

	if(WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		throw CNetError();
	}

	if((m_wsaEvent = WSACreateEvent()) == WSA_INVALID_EVENT)
	{
		throw CNetError();
	}
#else
	if(m_connectEvent == -1 || m_msgSent == -1 || m_recvDoneEvent == -1 || m_recvEvent == -1 ||
		m_recvDoneEvent == -1 || m_serverEvent == -1 || m_shutdown == -1)
	{
		throw CNetError();
	}

	epoll_event epollEvent;
	epollEvent.events = EPOLLIN;
	epollEvent.data.fd = m_msgSent;
	epoll_ctl(m_sendEpoll, EPOLL_CTL_ADD, m_msgSent, &epollEvent);

	timespec res;
	clock_getres(CLOCK_MONOTONIC, &res);
	m_perfFreq.QuadPart = 1000000000 / res.tv_nsec;
#endif

	// Setup address
	ADDRINFOT hints;
#if defined(_MSC_VER) &&_WIN32_WINNT < _WIN32_WINNT_VISTA
	hints.ai_flags = AI_CANONNAME;
	hints.ai_family = PF_INET;
#else
	hints.ai_flags = AI_CANONNAME | AI_ALL | AI_V4MAPPED;
	hints.ai_family = PF_INET6;
#endif
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_addrlen = 0;
	hints.ai_canonname = nullptr;
	hints.ai_addr = nullptr;
	hints.ai_next = nullptr;

	if(GetAddrInfo(host, service, &hints, &m_addrinfo) != 0)
	{
		// See if converting a IPV4 dotted address to a IPV6 IPV4-compatible address works
		std::basic_string<TCHAR> ipv6Host(_T("::ffff:"));
		ipv6Host += host;
		hints.ai_flags |= AI_NUMERICHOST;

		if(GetAddrInfo(ipv6Host.c_str(), service, &hints, &m_addrinfo) != 0)
		{
			ATLTRACE(_T("Unable to resolve %s:%s: error %d\n"), host, service, WSAGetLastError());
			throw CNetError();
		}
	}

	m_peerAddress = m_addrinfo;
	OpenSocket(m_peerAddress->ai_addr->sa_family);
	EnableKeepAlives();

	if((m_headerSize = HeaderSize()) == 0)
	{
		m_delimiter = Delimiter();
	}

	// Size receive buffer
	socklen_t len2 = sizeof(m_networkBufSize);
#ifndef _MSC_VER
	static const int SNDBUF = 1048576;
	if (!SetSockOpt(SO_RCVBUF, &SNDBUF, sizeof(SNDBUF), SOL_SOCKET))
	{
		printf("client Set SO_SNDBUF to %d failed\n", SNDBUF);
	}
#endif
	if(getsockopt(m_socket, SOL_SOCKET, SO_RCVBUF, reinterpret_cast<char*>(&m_networkBufSize), &len2) == SOCKET_ERROR)
	{
		ATLTRACE(_T("Unable to get socket buffer size: error %d\n"), WSAGetLastError());
		throw CNetError();
	}
	m_recvBuf.resize(size_t(m_networkBufSize) > m_headerSize ? m_networkBufSize : m_headerSize);
#if defined(_MSC_VER) &&_WIN32_WINNT < _WIN32_WINNT_VISTA

	if(InitializeCriticalSectionAndSpinCount(&m_critSect, 0) == 0)
	{
		ATLTRACE(_T("Unable to initialize critical section: error %d\n"), GetLastError());
		throw CNetError();
	}
	if(InitializeCriticalSectionAndSpinCount(&m_sendMutex, 0) == 0)
	{
		ATLTRACE(_T("Unable to initialize critical section: error %d\n"), GetLastError());
		throw CNetError();
	}
#endif

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Server-mode Constructor
//
template<typename T> CNetConnection<T>::CNetConnection(LPCTSTR service, bool connectionsDisabled, size_t maxConnections)
	:
#ifdef _MSC_VER
	m_connectEvent(CreateEvent(nullptr, FALSE, FALSE, nullptr)),
	m_msgSent(nullptr),
	m_recvDoneEvent(CreateEvent(nullptr, FALSE, FALSE, nullptr)),
	m_recvEvent(CreateEvent(nullptr, FALSE, FALSE, nullptr)),
	m_serverEvent(CreateEvent(nullptr, FALSE, FALSE, nullptr)),
	m_shutdown(CreateEvent(nullptr, TRUE, FALSE, nullptr)),
	m_wsaEvent(WSA_INVALID_EVENT),
#else
	m_connectEvent(eventfd(0, 0)),
	m_msgSent(-1),
	m_recvDoneEvent(eventfd(0, 0)),
	m_recvEvent(eventfd(0, 0)),
	m_serverEvent(eventfd(0, 0)),
	m_shutdown(eventfd(0, EFD_SEMAPHORE)),
	m_callbackEpoll(-1),
	m_networkEpoll(epoll_create1(0)),
	m_sendEpoll(-1),
#endif
	m_addrinfo(nullptr),
	m_bodySize(0),
	m_callbackThread(),
	m_connected(false),
	m_connectionsDisabled(connectionsDisabled),
	m_headerSize(0),
	m_loadFactor(0),
	m_maxConnections(maxConnections),
	m_mode(SERVER),
	m_networkThread(),
	m_noReverse(false),
	m_peerAddress(nullptr),
	m_recvBufCount(0),
	m_recvTimeout(WSA_INFINITE),
	m_retryConnectInterval(WSA_INFINITE),
	m_sendSize(0),
	m_sendTimeout(INFINITE),
	m_socket(INVALID_SOCKET)
{
#ifdef _MSC_VER
	if(m_connectEvent == nullptr || m_recvEvent == nullptr || m_recvDoneEvent == nullptr ||
		m_serverEvent == nullptr || m_shutdown == nullptr)
	{
		throw CNetError();
	}

	// Startup Winsock
	WSADATA wsaData;

	if(WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		throw CNetError();
	}

	if((m_wsaEvent = WSACreateEvent()) == WSA_INVALID_EVENT)
	{
		throw CNetError();
	}
#else
	if(m_connectEvent == -1 || m_recvEvent == -1 || m_recvDoneEvent == -1 ||
		m_serverEvent == -1 || m_shutdown == -1)
	{
		throw CNetError();
	}

	epoll_event epollEvent;
	epollEvent.events = EPOLLIN;
	epollEvent.data.fd = m_msgSent;
	epoll_ctl(m_sendEpoll, EPOLL_CTL_ADD, m_msgSent, &epollEvent);
#endif

	ADDRINFOT hints;
	hints.ai_flags = AI_PASSIVE;
#if defined(_MSC_VER) &&_WIN32_WINNT < _WIN32_WINNT_VISTA
	hints.ai_family = AF_INET;
#else
	hints.ai_family = AF_INET6;
#endif
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_addrlen = 0;
	hints.ai_canonname = nullptr;
	hints.ai_addr = nullptr;
	hints.ai_next = nullptr;

	// Resolve address
	if(GetAddrInfo(nullptr, service, &hints, &m_addrinfo) != 0)
	{
		ATLTRACE(_T("Unable to resolve service %s: error %d\n"), service, WSAGetLastError());
		throw CNetError();
	}

	m_peerAddress = m_addrinfo;
#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
	if(InitializeCriticalSectionAndSpinCount(&m_critSect, 0) == 0)
	{
		ATLTRACE(_T("Unable to initialize critical section: error %d\n"), GetLastError());
		throw CNetError();
	}
	if(InitializeCriticalSectionAndSpinCount(&m_sendMutex, 0) == 0)
	{
		ATLTRACE(_T("Unable to initialize critical section: error %d\n"), GetLastError());
		throw CNetError();
	}
#endif

	if(!m_connectionsDisabled)
	{
		// Set up socket
		CLockGuard lock(m_critSect);
		SetupServerSocket();
	}

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Dynamic-mode Constructor
//
template<typename T> CNetConnection<T>::CNetConnection(SOCKET socket, DWORD receiveTimeout /* ms */, DWORD sendTimeout /* ms */)
	:
#ifdef _MSC_VER
	m_connectEvent(CreateEvent(nullptr, FALSE, FALSE, nullptr)),
	m_msgSent(CreateEvent(nullptr, FALSE, FALSE, nullptr)),
	m_recvDoneEvent(CreateEvent(nullptr, FALSE, FALSE, nullptr)),
	m_recvEvent(CreateEvent(nullptr, FALSE, FALSE, nullptr)),
	m_serverEvent(CreateEvent(nullptr, FALSE, FALSE, nullptr)),
	m_shutdown(CreateEvent(nullptr, TRUE, FALSE, nullptr)),
	m_wsaEvent(WSA_INVALID_EVENT),
#else
	m_connectEvent(eventfd(0, 0)),
	m_msgSent(eventfd(0, 0)),
	m_recvDoneEvent(eventfd(0, 0)),
	m_recvEvent(eventfd(0, 0)),
	m_serverEvent(eventfd(0, 0)),
	m_shutdown(eventfd(0, EFD_SEMAPHORE)),
	m_callbackEpoll(epoll_create1(0)),
	m_networkEpoll(epoll_create1(0)),
	m_sendEpoll(epoll_create1(0)),
#endif
	m_addrinfo(nullptr),
	m_bodySize(0),
	m_callbackThread(),
	m_connected(true),
	m_headerSize(0),
	m_loadFactor(0),
	m_mode(DYNAMIC),
	m_networkBufSize(0),
	m_networkThread(),
	m_noReverse(false),
	m_peerAddress(nullptr),
	m_recvBufCount(0),
	m_recvTimeout(receiveTimeout),
	m_retryConnectInterval(WSA_INFINITE),
	m_sendSize(0),
	m_sendTimeout(sendTimeout),
	m_socket(socket)
{
#ifdef _MSC_VER
	if(m_connectEvent == nullptr || m_msgSent == nullptr || m_recvDoneEvent == nullptr ||
		m_recvEvent == nullptr || m_serverEvent == nullptr || m_shutdown == nullptr)
	{
		throw CNetError();
	}

	QueryPerformanceFrequency(&m_perfFreq);

	// Startup Winsock
	WSADATA wsaData;

	if(WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		throw CNetError();
	}

	if((m_wsaEvent = WSACreateEvent()) == WSA_INVALID_EVENT)
	{
		throw CNetError();
	}
#else
	if(m_connectEvent == -1 || m_msgSent == -1 || m_recvDoneEvent == -1 ||
		m_recvEvent == -1 || m_serverEvent == -1 || m_shutdown == -1)
	{
		throw CNetError();
	}

	epoll_event epollEvent;
	epollEvent.events = EPOLLIN;
	epollEvent.data.fd = m_msgSent;
	epoll_ctl(m_sendEpoll, EPOLL_CTL_ADD, m_msgSent, &epollEvent);
	timespec res;
	clock_getres(CLOCK_MONOTONIC, &res);
	m_perfFreq.QuadPart = 1000000000 / res.tv_nsec;
#endif

	// Setup address
	if((m_headerSize = HeaderSize()) == 0)
	{
		m_delimiter = Delimiter();
	}

	// Set keepalives
	EnableKeepAlives();

	// Size receive buffer
	socklen_t len2 = sizeof(m_networkBufSize);

#ifndef _MSC_VER
	static const int SNDBUF = 1048576;
	if (!SetSockOpt(SO_RCVBUF, &SNDBUF, sizeof(SNDBUF), SOL_SOCKET))
	{
		printf("dynamic Set SO_SNDBUF to %d failed\n", SNDBUF);
	}
#endif
	if(getsockopt(m_socket, SOL_SOCKET, SO_RCVBUF, reinterpret_cast<char*>(&m_networkBufSize), &len2) == SOCKET_ERROR)
	{
		ATLTRACE(_T("Unable to get socket buffer size: error %d\n"), WSAGetLastError());
		throw CNetError();
	}

	m_recvBuf.resize(size_t(m_networkBufSize) > m_headerSize ? m_networkBufSize : m_headerSize);
#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
	if(InitializeCriticalSectionAndSpinCount(&m_critSect, 0) == 0)
	{
		ATLTRACE(_T("Unable to initialize critical section: error %d\n"), GetLastError());
		throw CNetError();
	}
	if(InitializeCriticalSectionAndSpinCount(&m_sendMutex, 0) == 0)
	{
		ATLTRACE(_T("Unable to initialize critical section: error %d\n"), GetLastError());
		throw CNetError();
	}
#endif

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Destructor
//
template<typename T> CNetConnection<T>::~CNetConnection(void)
{
	// Did you forget to call Shutdown() in your derived class destructor?
	ATLASSERT(!m_networkThread.joinable());

	// Close socket
	if(m_socket != INVALID_SOCKET)
	{
		closesocket(m_socket);
	}

	if(m_mode == DYNAMIC)
	{
		// Client m_addrinfo is not allocated by GetAddrInfo
		if(m_addrinfo != nullptr)
		{
			delete[] m_addrinfo->ai_canonname;
			delete m_addrinfo->ai_addr;
			delete m_addrinfo;
		}
	}
	else
	{
		FreeAddrInfo(m_addrinfo);
	}

#ifdef _MSC_VER
	// Shutdown Windows Sockets
	WSACleanup();

	if(m_wsaEvent != WSA_INVALID_EVENT)
	{
		WSACloseEvent(m_wsaEvent);
	}
#else
	if(m_mode != SERVER)
	{
		close(m_callbackEpoll);
		close(m_sendEpoll);
	}

	close(m_networkEpoll);
#endif

	// Delete dynamic connection
	if(m_mode == SERVER)
	{
		PurgeConnections(static_cast<T*>(this));
	}

#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
	DeleteCriticalSection(&m_sendMutex);
	DeleteCriticalSection(&m_critSect);
#endif

	if(m_connectEvent != nullptr)
	{
		CloseHandle(m_connectEvent);
	}

	if(m_msgSent != nullptr)
	{
		CloseHandle(m_msgSent);
	}

	if(m_recvEvent != nullptr)
	{
		CloseHandle(m_recvEvent);
	}

	if(m_recvDoneEvent != nullptr)
	{
		CloseHandle(m_recvDoneEvent);
	}

	if(m_serverEvent != nullptr)
	{
		CloseHandle(m_serverEvent);
	}

	if(m_shutdown != nullptr)
	{
		CloseHandle(m_shutdown);
	}
#else
	if(m_connectEvent != -1)
	{
		close(m_connectEvent);
	}

	if(m_msgSent != -1)
	{
		close(m_msgSent);
	}

	if(m_recvEvent != -1)
	{
		close(m_recvEvent);
	}

	if(m_recvDoneEvent != -1)
	{
		close(m_recvDoneEvent);
	}

	if(m_serverEvent != -1)
	{
		close(m_serverEvent);
	}

	if(m_shutdown != -1)
	{
		close(m_shutdown);
	}
#endif

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Thread for callbacks
//
#ifdef _MSC_VER
template<typename T> void CNetConnection<T>::CallbackThread(bool initializeCom, DWORD coInit)
#else
template<typename T> void CNetConnection<T>::CallbackThread(bool, DWORD)
#endif
{
	try
	{
		SetThreadName("CallbackThread");


#ifdef _MSC_VER
		if(initializeCom)
		{
			if(FAILED(CoInitializeEx(nullptr, coInit)))
			{
				throw CNetError();
			}
		}

		HANDLE events[3] = { m_shutdown, m_connectEvent, m_recvEvent };
		DWORD event;

		// Loop until shutdown
		while((event = WaitForMultipleObjects(_countof(events), events, FALSE, INFINITE)) != WAIT_OBJECT_0)
		{
			switch(event)
			{
#else
		epoll_event epollEvent;
		epollEvent.events = EPOLLIN;
		epollEvent.data.fd = m_shutdown;
		epoll_ctl(m_callbackEpoll, EPOLL_CTL_ADD, epollEvent.data.fd, &epollEvent);
		epollEvent.data.fd = m_connectEvent;
		epoll_ctl(m_callbackEpoll, EPOLL_CTL_ADD, epollEvent.data.fd, &epollEvent);
		epollEvent.data.fd = m_recvEvent;
		epoll_ctl(m_callbackEpoll, EPOLL_CTL_ADD, epollEvent.data.fd, &epollEvent);

		while(true)
		{
			while(epoll_wait(m_callbackEpoll, &epollEvent, 1, -1) == -1 && errno == EINTR);
			auto event = epollEvent.data.fd;
			eventfd_t value;
			eventfd_read(epollEvent.data.fd, &value);

			if(event == m_shutdown)
			{
				break;
			}

#endif
#ifdef _MSC_VER
			case WAIT_OBJECT_0 + 1:
#else
			if(event == m_connectEvent)
#endif
			{
				// Connected
				CLockGuard lock(m_critSect);
				m_loadFactor = 0;
#ifdef _MSC_VER
				QueryPerformanceCounter(const_cast<LARGE_INTEGER*>(&m_lastLoadUpdate));
#else
				timespec t;
				clock_gettime(CLOCK_MONOTONIC, &t);
				m_lastLoadUpdate.QuadPart = t.tv_sec * 1000000000ll + t.tv_nsec;
#endif
				lock.CallUnlocked([&] { OnConnect(); });

				// Re-enable read events
				if(m_socket != INVALID_SOCKET)
				{
					m_recvBufCount = 0;

					if((m_headerSize = HeaderSize()) == 0)
					{
						m_delimiter = Delimiter();
					}
#ifdef _MSC_VER
					WSAEventSelect(m_socket, m_wsaEvent, FD_CONNECT | FD_READ | FD_WRITE | FD_CLOSE);
#else
					m_socketEvent.events |= EPOLLIN;
					epoll_ctl(m_networkEpoll, EPOLL_CTL_MOD, m_socket, &m_socketEvent);
#endif
				}
			}

#ifdef _MSC_VER
			break;

			case WAIT_OBJECT_0 + 2:
#else
			else if(event == m_recvEvent)
#endif
			{
				// Received message(s)
				{
					CLockGuard lock(m_critSect);
					size_t msgStart = 0;

					while(m_headerSize == 0 ? true : msgStart + m_headerSize + m_bodySize <= m_recvBufCount)
					{
						size_t size;

						if(m_headerSize == 0)
						{
							// Delimited
							size_t delim;

							for(delim = msgStart; delim < m_recvBufCount && m_recvBuf[delim] != m_delimiter; ++delim);

							if(delim == m_recvBufCount)
							{
								break;
							}

							size = delim - msgStart + 1;
						}
						else
						{
							// Header + body
							size = m_headerSize + m_bodySize;
						}

						const void* msg = &m_recvBuf[msgStart];
						lock.CallUnlocked([&] { OnMessage(msg, size); });
						msgStart += size;

						if((m_headerSize = HeaderSize()) == 0)
						{
							m_delimiter = Delimiter();
						}

						if(msgStart + m_headerSize <= m_recvBufCount)
						{
							m_bodySize = BodySize(&m_recvBuf[msgStart]);
						}
					}

					if(msgStart > 0 && msgStart < m_recvBufCount)
					{
						memmove(&m_recvBuf[0], &m_recvBuf[msgStart], m_recvBufCount - msgStart);
					}

					m_recvBufCount -= msgStart;
				}

#ifdef _MSC_VER
				SetEvent(m_recvDoneEvent);
#else
				eventfd_write(m_recvDoneEvent, 1);
#endif
			}

#ifdef _MSC_VER
			break;

			default:
				ATLASSERT(FALSE);
			break;
			}
#endif
		}

#ifdef _MSC_VER
		if(initializeCom)
		{
			CoUninitialize();
		}
#endif
	}
	catch(...)
	{
		if(m_failFn != nullptr)
		{
			m_failFn(std::current_exception());
		}
		else
		{
			throw;
		}
	}

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Close connection
//
template<typename T> void CNetConnection<T>::Close(void)
{
	CLockGuard lock(m_critSect);

	switch(m_mode)
	{
	case CLIENT:
		// Close and reconnect
		if(m_socket != INVALID_SOCKET)
		{
			shutdown(m_socket, SD_SEND);
		}

		break;

	case SERVER:
		ATLASSERT(FALSE);
		break;

	case DYNAMIC:
		// Close
		if(m_socket != INVALID_SOCKET)
		{
			shutdown(m_socket, SD_SEND);
		}

		break;

	default:
		ATLASSERT(FALSE);
		break;
	}

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Disable new dynamic connections or client reconnects
//
template<typename T> void CNetConnection<T>::DisableNewConnections(bool disable)
{
	CLockGuard::CallLocked(m_critSect, [&] { m_connectionsDisabled = disable; });

	if(m_mode == SERVER)
	{
#ifdef _MSC_VER
		SetEvent(m_serverEvent);
#else
		eventfd_write(m_serverEvent, 1);
#endif
	}

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Enable keep-alives on the socket
//
template<typename T> void CNetConnection<T>::EnableKeepAlives(void)
{
	if(m_recvTimeout == WSA_INFINITE)
	{
		return;
	}

	static const int INTERVAL = 1; // seconds
	static const int ON = 1;
#ifdef _MSC_VER
	DWORD len;
	tcp_keepalive keepalive = { 1, m_recvTimeout, INTERVAL * 1000 };

	if(WSAIoctl(m_socket, SIO_KEEPALIVE_VALS, &keepalive, sizeof(keepalive), nullptr, 0, &len, nullptr, nullptr) == SOCKET_ERROR ||
#else
//	return;			// TODO: keepalives not working properly??
	int timeout = m_recvTimeout / 1000;

	if(setsockopt(m_socket, IPPROTO_TCP, TCP_KEEPINTVL, &INTERVAL, sizeof(INTERVAL)) == -1 ||
		setsockopt(m_socket, IPPROTO_TCP, TCP_KEEPIDLE, &timeout, sizeof(timeout)) == -1 ||
#endif
		setsockopt(m_socket, SOL_SOCKET, SO_KEEPALIVE, reinterpret_cast<const char*>(&ON), sizeof(ON)) == SOCKET_ERROR)
	{
		ATLTRACE(_T("Unable to set keepalives on socket: error %d\n"), WSAGetLastError());
	}


	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Call function on each dynamic connection
//
template<typename T> void CNetConnection<T>::ForEachConnection(ForEachCallback callback, _In_opt_ void* arg)
{
	CNetConnection* server;

	switch(m_mode)
	{
	case CLIENT:
		(static_cast<T*>(this)->*callback)(arg);
		return;

	case SERVER:
		server = this;
		break;

	case DYNAMIC:
		server = GetServer();
		break;

	default:
		ATLASSERT(FALSE);
		return;
	}

#if _WIN32_WINNT < _WIN32_WINNT_VISTA
	m_mapRWLock.LockForRead();
#else
	CSharedLockGuard lock(m_mapRWLock);
#endif

	for(auto connection = m_connections.begin(); connection != m_connections.end(); ++connection)
	{
		if(connection->second == server)
		{
			(static_cast<T*>(connection->first.get())->*callback)(arg);
		}
	}

#if _WIN32_WINNT < _WIN32_WINNT_VISTA
	m_mapRWLock.Unlock();
#endif

#if defined(_MSC_VER) && _WIN32_WINNT >= _WIN32_WINNT_VISTA
	__analysis_assume_lock_released(m_mapRWLock); //CSharedLockGuard dtor will release m_mapRWLock.
#endif
	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Get number of connections
//
template<typename T> size_t CNetConnection<T>::GetNumConnections(void)
{
	switch(m_mode)
	{
	case CLIENT:
	{
		CLockGuard lock(m_critSect);
		return m_connected ? 1 : 0;
	}

	case SERVER:
	{
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
		m_mapRWLock.LockForRead();
#else
		CSharedLockGuard lock(m_mapRWLock);
#endif
		size_t size = m_connections.size();
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
		m_mapRWLock.Unlock();
#endif

#if defined _MSC_VER && _WIN32_WINNT >= _WIN32_WINNT_VISTA
		__analysis_assume_lock_released(m_mapRWLock); //CSharedLockGuard dtor will release m_mapRWLock.
#endif
		return size;
	}

	case DYNAMIC:
		return 1;

	default:
		ATLASSERT(FALSE);
		return 0;
	}
}


/////////////////////////////////////////////////////////////////////////////
//
// Get the server associated with a connection
//
template<typename T> double CNetConnection<T>::GetReceiveLoadFactor(void) const
{
	CLockGuard lock(m_critSect);

	return m_loadFactor;
}


/////////////////////////////////////////////////////////////////////////////
//
// Get the server associated with a connection
//
template<typename T> _Ret_opt_ T* CNetConnection<T>::GetServer(void)
{
	switch(m_mode)
	{
	case CLIENT:
		return nullptr;

	case SERVER:
		return static_cast<T*>(this);

	case DYNAMIC:
	{
		T* server = nullptr;
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
		m_mapRWLock.LockForRead();
#else
		CSharedLockGuard lock(m_mapRWLock);
#endif
		try
		{
			auto entry = m_connections.find(this->shared_from_this());

			if(entry != m_connections.end())
			{
				server = entry->second;
			}
		}
		catch(std::bad_weak_ptr&)
		{
			// Object is being destroyed
			return nullptr;
		}

#if _WIN32_WINNT < _WIN32_WINNT_VISTA
		m_mapRWLock.Unlock();
#endif

#if defined(_MSC_VER) && _WIN32_WINNT >= _WIN32_WINNT_VISTA
		__analysis_assume_lock_released(m_mapRWLock); //CSharedLockGuard dtor will release m_mapRWLock.
#endif
		return server;
	}

	default:
		ATLASSERT(FALSE);
		return nullptr;
	}
}


/////////////////////////////////////////////////////////////////////////////
//
// Get socket option
//
template<typename T> bool CNetConnection<T>::GetSockOpt(int optionName,
	_Out_bytecap_(*optionLen) void* optionValue,
	_Inout_ socklen_t* optionLen,
	int level)
{
	CLockGuard lock(m_critSect);

	return m_socket != INVALID_SOCKET && getsockopt(m_socket, level, optionName, static_cast<char*>(optionValue), optionLen) == 0;
}


/////////////////////////////////////////////////////////////////////////////
//
// Is the peer localhost?
//
template<typename T> bool CNetConnection<T>::IsPeerLocalHost(void) const
{
	ATLASSERT(m_mode != SERVER);

	return m_peerAddress != nullptr ? (m_peerAddress->ai_family == AF_INET && reinterpret_cast<SOCKADDR_IN*>(m_peerAddress->ai_addr)->sin_addr.s_addr == htonl(INADDR_LOOPBACK)) ||
		(m_peerAddress->ai_family == AF_INET6 && IN6_IS_ADDR_LOOPBACK(&reinterpret_cast<sockaddr_in6*>(m_peerAddress->ai_addr)->sin6_addr)) : false;
}


/////////////////////////////////////////////////////////////////////////////
//
// Thread function for network operations
//
template<typename T> void CNetConnection<T>::NetworkThread(void)
{
	try
	{
		m_networkThreadId = std::this_thread::get_id();
		SetThreadName("NetworkThread");

		{
			// Do mode-specific setup
			CLockGuard lock(m_critSect);

			switch(m_mode)
			{
			case CLIENT:
#ifdef _MSC_VER
				WSAEventSelect(m_socket, m_wsaEvent, FD_CONNECT | FD_READ | FD_WRITE | FD_CLOSE);
#else
				m_socketEvent.events |= EPOLLOUT;
				epoll_ctl(m_networkEpoll, EPOLL_CTL_MOD, m_socket, &m_socketEvent);
#endif

				// Connect asynchronously
				if(::connect(m_socket, m_peerAddress->ai_addr, int(m_peerAddress->ai_addrlen)) != SOCKET_ERROR)
				{
					m_connected = true;
#ifdef _MSC_VER
					WSAEventSelect(m_socket, m_wsaEvent, FD_WRITE);
					SetEvent(m_connectEvent);
#else
					m_socketEvent.events = 0;
					epoll_ctl(m_networkEpoll, EPOLL_CTL_MOD, m_socket, &m_socketEvent);
					eventfd_write(m_connectEvent, 1);
#endif
				}

				break;

			case SERVER:
				if(m_socket != INVALID_SOCKET)
				{
					// Listen
#ifdef _MSC_VER
					WSAEventSelect(m_socket, m_wsaEvent, FD_ACCEPT);
#else
					m_socketEvent.events = EPOLLIN;
					epoll_ctl(m_networkEpoll, EPOLL_CTL_MOD, m_socket, &m_socketEvent);
#endif
					listen(m_socket, SOMAXCONN);
				}

				break;

			case DYNAMIC:
			{
				SOCKADDR_STORAGE address;
				socklen_t addrLen = sizeof(address);
				getpeername(m_socket, reinterpret_cast<SOCKADDR*>(&address), &addrLen);

				if(address.ss_family == AF_INET6 && IN6_IS_ADDR_V4MAPPED(&reinterpret_cast<SOCKADDR_IN6*>(&address)->sin6_addr))
				{
					// Convert back to IPV4
//					const auto addr = *reinterpret_cast<const IN_ADDR*>(reinterpret_cast<char*>(&reinterpret_cast<SOCKADDR_IN6*>(&address)->sin6_addr.s6_addr) + 12);
					IN_ADDR addr;
					memcpy(&addr, reinterpret_cast<SOCKADDR_IN6*>(&address)->sin6_addr.s6_addr + 12, 4);
					reinterpret_cast<SOCKADDR_IN*>(&address)->sin_family = AF_INET;
					reinterpret_cast<SOCKADDR_IN*>(&address)->sin_port = reinterpret_cast<SOCKADDR_IN6*>(&address)->sin6_port;
					reinterpret_cast<SOCKADDR_IN*>(&address)->sin_addr = addr;
					memset(reinterpret_cast<SOCKADDR_IN*>(&address)->sin_zero, 0, sizeof(reinterpret_cast<SOCKADDR_IN*>(&address)->sin_zero));
					addrLen = sizeof(SOCKADDR_IN);
				}

				TCHAR host[NI_MAXHOST];
				TCHAR service[NI_MAXSERV];		// Not used except in GetNameInfo call.

#ifdef _USING_V110_SDK71_
				GetNameInfo(reinterpret_cast<SOCKADDR*>(&address), addrLen, host, NI_MAXHOST, service, NI_MAXSERV, NI_NUMERICSERV);
#else
				if (m_noReverse || GetNameInfo(reinterpret_cast<SOCKADDR*>(&address), addrLen, host, NI_MAXHOST, service, NI_MAXSERV, NI_NUMERICSERV) != 0)
				{
					if (address.ss_family == AF_INET)
					{
						InetNtop(address.ss_family, &(reinterpret_cast<SOCKADDR_IN*>(&address)->sin_addr), host, NI_MAXHOST);
					}
					else
					{
						InetNtop(address.ss_family, &(reinterpret_cast<sockaddr_in6*>(&address)->sin6_addr), host, NI_MAXHOST);
					}
					ATLTRACE(_T("noReverse = %d, host: %s\n"), m_noReverse, host);
				}
#endif
				// Build an ADDRINFOT structure
				try
				{
					m_addrinfo = new ADDRINFOT;
					m_addrinfo->ai_flags = 0;
					m_addrinfo->ai_family = address.ss_family;
					m_addrinfo->ai_socktype = SOCK_STREAM;
					m_addrinfo->ai_protocol = IPPROTO_TCP;
					m_addrinfo->ai_addrlen = addrLen;
					m_addrinfo->ai_canonname = nullptr;
					m_addrinfo->ai_addr = nullptr;
					m_addrinfo->ai_canonname = new TCHAR[_tcslen(host) + 1];
					_tcscpy_s(m_addrinfo->ai_canonname, _tcslen(host) + 1, host);
					m_addrinfo->ai_addr = reinterpret_cast<sockaddr*>(new SOCKADDR_STORAGE);
					memcpy_s(m_addrinfo->ai_addr, sizeof(SOCKADDR_STORAGE), &address, m_addrinfo->ai_addrlen);
					m_addrinfo->ai_next = nullptr;
				}
				catch(...)
				{
					if(m_addrinfo != nullptr)
					{
						delete[] m_addrinfo->ai_canonname;
						delete m_addrinfo->ai_addr;
						delete m_addrinfo;
						m_addrinfo = nullptr;
					}
				}

				m_peerAddress = m_addrinfo;
#ifdef _MSC_VER
				WSAEventSelect(m_socket, m_wsaEvent, FD_WRITE);
				SetEvent(m_connectEvent);
#else
				m_socketEvent.events = 0;
				m_socketEvent.data.fd = m_socket;
				epoll_ctl(m_networkEpoll, EPOLL_CTL_ADD, m_socket, &m_socketEvent);
				eventfd_write(m_connectEvent, 1);
#endif
			}

			break;
			}
		}

		// Setup events etc
		DWORD timeout = WSA_INFINITE;
		bool idle = false;
#ifdef _MSC_VER
		DWORD event;
		const HANDLE events[] = { m_recvDoneEvent, m_serverEvent, m_wsaEvent, m_shutdown };

		// Loop until shutdown
		while((event = WSAWaitForMultipleEvents(_countof(events), events, FALSE, timeout, FALSE)) != WSA_WAIT_EVENT_0 + 3)
		{
			switch(event)
			{
			case WSA_WAIT_EVENT_0:
				// Callback thread done with receive buffer
				OnRecvDone();
				break;

			case WSA_WAIT_EVENT_0 + 1:
				// Do server actions
				OnServer();
				break;

			case WSA_WAIT_EVENT_0 + 2:
				// Network event
				OnNetwork();
				break;

			case WSA_WAIT_TIMEOUT:
				// Timeout
				OnTimeout(idle);
				break;

			case WSA_WAIT_FAILED:
				break;

			default:
				ATLASSERT(FALSE);
				break;
			}
#else
		epoll_event epollEvent;
		epollEvent.events = 0;
		epollEvent.data.fd = m_socket;
		epoll_ctl(m_networkEpoll, EPOLL_CTL_ADD, epollEvent.data.fd, &epollEvent);
		epollEvent.events = EPOLLIN;
		epollEvent.data.fd = m_recvDoneEvent;
		epoll_ctl(m_networkEpoll, EPOLL_CTL_ADD, epollEvent.data.fd, &epollEvent);
		epollEvent.data.fd = m_serverEvent;
		epoll_ctl(m_networkEpoll, EPOLL_CTL_ADD, epollEvent.data.fd, &epollEvent);
		epollEvent.data.fd = m_shutdown;
		epoll_ctl(m_networkEpoll, EPOLL_CTL_ADD, epollEvent.data.fd, &epollEvent);
		bool shutdown = false;

		while(!shutdown)
		{
			switch(epoll_wait(m_networkEpoll, &epollEvent, 1, int(timeout)))
			{
			case 0:
				// Timeout
				OnTimeout(idle);
				break;

			case 1:
				if(epollEvent.data.fd == m_socket)
				{
					OnNetwork(epollEvent.events);
				}
				else
				{
					eventfd_t value;
					eventfd_read(epollEvent.data.fd, &value);

					if(epollEvent.data.fd == m_recvDoneEvent)
					{
						// Callback thread done with receive buffer
						OnRecvDone();
					}
					else if(epollEvent.data.fd == m_serverEvent)
					{
						// Do server actions
						OnServer();
					}
					else if(epollEvent.data.fd == m_shutdown)
					{
						shutdown = true;
						continue;
					}
				}

				break;

			default:
				// eg EINTR
				continue;
			}
#endif

			CLockGuard lock(m_critSect);

			if(!m_connected)
			{
				timeout = m_retryConnectInterval;
			}
			else
			{
				timeout = IDLE_POLL_INTERVAL;
				idle = true;

				if(m_sendSize > 0)
				{
					timeout = m_sendTimeout;
					idle = false;
				}

				if(m_recvBufCount > 0 && m_headerSize + m_bodySize > m_recvBufCount && (idle || timeout > m_recvTimeout))
				{
					timeout = m_recvTimeout;
					idle = false;
				}
			}
		}

		CLockGuard lock(m_critSect);
		m_connected = false;
		closesocket(m_socket);
		m_socket = INVALID_SOCKET;
#ifdef _MSC_VER
		SetEvent(m_msgSent);
#else
		eventfd_write(m_msgSent, 1);
#endif
		}
	catch(...)
	{
		if(m_failFn != nullptr)
		{
			m_failFn(std::current_exception());
		}
		else
		{
			throw;
		}
	}

	return;
	}


/////////////////////////////////////////////////////////////////////////////
//
// Callback (Can be overridden)
//
template<typename T> void CNetConnection<T>::OnClose(int errorCode)
{
	UNREFERENCED_PARAMETER(errorCode);
	ATLTRACE(_T("%s disconnected from %s with error code %d\n"), m_peerAddress->ai_canonname,
		m_mode == CLIENT ? _T("client") : _T("server"), errorCode);

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Callback (Can be overridden)
//
template<typename T> void CNetConnection<T>::OnConnect(void)
{
	ATLTRACE(_T("%s connected to %s\n"), m_mode == CLIENT ? _T("Client") : m_peerAddress->ai_canonname,
		m_mode == CLIENT ? m_peerAddress->ai_canonname : _T("server"));

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Callback (Should be overridden)
//
template<typename T> void CNetConnection<T>::OnMessage(_In_bytecount_(size) const void*, size_t size)
{
	UNREFERENCED_PARAMETER(size);
	ATLTRACE(_T("Message from %s (%d bytes)\n"), m_peerAddress->ai_canonname, size);

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Helper for network thread
//
#ifdef _MSC_VER
template<typename T> void CNetConnection<T>::OnNetwork(void)
{
	WSANETWORKEVENTS wsaEvents;

	{
		CLockGuard lock(m_critSect);

		if(m_socket == INVALID_SOCKET)
		{
			WSAResetEvent(m_wsaEvent);
			return;
		}

		WSAEnumNetworkEvents(m_socket, m_wsaEvent, &wsaEvents);
	}

	if(wsaEvents.lNetworkEvents & FD_ACCEPT)
	{
		ATLASSERT(m_mode == SERVER);
		WSASetLastError(wsaEvents.iErrorCode[FD_ACCEPT_BIT]);

		if(wsaEvents.iErrorCode[FD_ACCEPT_BIT] == ERROR_SUCCESS)
#else
template<typename T> void CNetConnection<T>::OnNetwork(uint32_t events)
{
	if(m_mode == SERVER && (events & EPOLLIN) != 0)
	{
		if((events & (EPOLLERR | EPOLLHUP)) == 0)
#endif
		{
			// Accept a connection and add it to the map
#ifdef _MSC_VER
			auto socket = ::accept(m_socket, nullptr, nullptr);
#else
			// NOTE: On Linux, the new socket returned by accept() does not inherit file status flags such as O_NONBLOCK from the listening socket.
			auto socket = ::accept4(m_socket, nullptr, nullptr, SOCK_NONBLOCK);
#endif
			if(socket != SOCKET_ERROR)
			{
#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
				m_mapRWLock.LockForWrite();
#else
				CLockGuard lock(m_mapRWLock);
#endif

				try
				{
					m_connections.insert(typename ConnectionMap::value_type(std::make_shared<T>(socket, static_cast<T*>(this)), static_cast<T*>(this)));

					if(m_maxConnections != 0 && m_connections.size() >= m_maxConnections)
					{
						DisableNewConnections(true);
					}
				}
				catch(const std::exception& e)
				{
#ifdef _MSC_VER
					UNREFERENCED_PARAMETER(e);
					ATLTRACE(_T("Incoming connection setup failed: %hs\n"), e.what());
#else
					fprintf(stderr, "Incoming connection setup failed: %s\n", e.what());
#endif
					closesocket(socket);
				}
				catch(...)
				{
#ifdef _MSC_VER
					ATLTRACE(_T("Incoming connection setup failed\n"));
#else
					fprintf(stderr, "Incoming connection setup failed\n");
#endif
					closesocket(socket);
				}

#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
				m_mapRWLock.Unlock();
#endif
			}
		}
	}

#ifdef _MSC_VER
	if(wsaEvents.lNetworkEvents & FD_CONNECT)
	{
		// Connection completed (with or without error)
		ATLASSERT(m_mode == CLIENT);
		WSASetLastError(wsaEvents.iErrorCode[FD_CONNECT_BIT]);
		CLockGuard lock(m_critSect);

		if(wsaEvents.iErrorCode[FD_CONNECT_BIT] == ERROR_SUCCESS)
#else
	if(m_mode == CLIENT && !m_connected && (events & EPOLLOUT) != 0)
	{
//		TRACE("NetworkThread connect: events %x\n", events);
		if((events & (EPOLLERR | EPOLLHUP)) == 0)
#endif
		{
			// Succeeded - we'll get an FD_WRITE to trigger any pending send
			m_connected = true;

			// Set options
			EnableKeepAlives();

			// Disable read events until the callback is done
#ifdef _MSC_VER
			WSAEventSelect(m_socket, m_wsaEvent, FD_WRITE);
			SetEvent(m_connectEvent);
#else
			m_socketEvent.events &= ~EPOLLIN;		// Is this correct or not?????
			m_socketEvent.events &= ~EPOLLOUT;
			epoll_ctl(m_networkEpoll, EPOLL_CTL_MOD, m_socket, &m_socketEvent);
			eventfd_write(m_connectEvent, 1);
#endif
		}
		else
		{
			// Failed - cancel any pending send and retry
			if(m_sendSize != 0)
			{
				m_sendSize = 0;
#ifdef _MSC_VER
				SetEvent(m_msgSent);
#else
				eventfd_write(m_msgSent, 1);
#endif
			}
		}
	}

#ifdef _MSC_VER
	if(wsaEvents.lNetworkEvents & FD_READ)
	{
		// Data available
		ATLASSERT(m_mode != SERVER);
		WSASetLastError(wsaEvents.iErrorCode[FD_READ_BIT]);

		if(wsaEvents.iErrorCode[FD_READ_BIT] != ERROR_SUCCESS)
		{
			// Error
			wsaEvents.lNetworkEvents |= FD_CLOSE;
#else
	if(m_mode != SERVER && (events & EPOLLIN) != 0)
	{
		if((events & EPOLLERR) != 0)
		{
			// Error
			events |= EPOLLHUP;
#endif
		}
		else
		{
			// Read whatever is available up to the size of the buffer
			CLockGuard lock(m_critSect);
			int got = recv(m_socket, &m_recvBuf[m_recvBufCount], int(m_recvBuf.size() - m_recvBufCount), 0);

			if((got == SOCKET_ERROR && WSAGetLastError() != WSAEWOULDBLOCK) || got == 0)
			{
				// Error or closed connection
#ifdef _MSC_VER
				wsaEvents.lNetworkEvents |= FD_CLOSE;
#else
				events |= EPOLLHUP;
#endif
			}
			else if(got > 0)
			{
				// Got something
				m_recvBufCount += got;

				if(m_recvBufCount >= m_headerSize)
				{
					if(m_headerSize > 0)
					{
						m_bodySize = BodySize(&m_recvBuf[0]);
					}

					if(m_headerSize == 0 ? memchr(&m_recvBuf[0], m_delimiter, m_recvBufCount) != nullptr : m_recvBufCount >= m_headerSize + m_bodySize)
					{
						// Got at least a message
#ifdef _MSC_VER
						WSAEventSelect(m_socket, m_wsaEvent, FD_CONNECT | FD_WRITE | FD_CLOSE);
						SetEvent(m_recvEvent);
#else
						m_socketEvent.events &= ~EPOLLIN;
						// TODO: MAY BE IMPORTANT! Sometimes epoll_ctl here blocks for over 300 msec. Don't know why yet. (SS)
						epoll_ctl(m_networkEpoll, EPOLL_CTL_MOD, m_socket, &m_socketEvent);
						eventfd_write(m_recvEvent, 1);
#endif
					}
					else
					{
						if(m_headerSize + m_bodySize > m_recvBuf.size())
						{
							// Increase buffer size
							try
							{
								m_recvBuf.resize(m_headerSize + m_bodySize);
							}
							catch(std::bad_alloc&)
							{
#ifdef _MSC_VER
								wsaEvents.lNetworkEvents |= FD_CLOSE;
#else
								events |= EPOLLHUP;
#endif
							}
#if defined(_MSC_VER) && defined(_AFX)
							catch(CMemoryException* e)
							{
								e->Delete();
								wsaEvents.lNetworkEvents |= FD_CLOSE;
							}
#endif
						}
					}
				}
			}
		}
	}

#ifdef _MSC_VER
	if(wsaEvents.lNetworkEvents & FD_WRITE)
	{
		// Socket writeable
		ATLASSERT(m_mode != SERVER);
		WSASetLastError(wsaEvents.iErrorCode[FD_WRITE_BIT]);
		CLockGuard lock(m_critSect);

		if(wsaEvents.iErrorCode[FD_WRITE_BIT] != ERROR_SUCCESS)
		{
			// Error
			wsaEvents.lNetworkEvents |= FD_CLOSE;
#else
	if(m_connected && (events & EPOLLOUT) != 0)
	{
		CLockGuard lock(m_critSect);		// SS added this on 3/31/2017
		if((events & EPOLLERR) != 0)
		{
			events |= EPOLLHUP;
#endif
		}
		else if(m_sendSize != 0)
		{
			// Message to send
			while(true)
			{
				// Try to send what's left 
#ifdef _MSC_VER
				int sent = send(m_socket, &m_sendMsg[m_sentBytesSoFar], int(m_sendSize - m_sentBytesSoFar), 0);
#else
				int sent = send(m_socket, &m_sendMsg[m_sentBytesSoFar], int(m_sendSize - m_sentBytesSoFar), MSG_NOSIGNAL);
#endif

				if(sent == SOCKET_ERROR)
				{
					if(WSAGetLastError() != WSAEWOULDBLOCK)
					{
						// Error
#ifdef _MSC_VER
						wsaEvents.lNetworkEvents |= FD_CLOSE;
#else
						events |= EPOLLHUP;
#endif
					}

					// Done for now - we'll get another FD_WRITE later
					break;
				}
				else
				{
					m_sentBytesSoFar += sent;
				}

				if(m_sentBytesSoFar == m_sendSize)
				{
					// Message sent
					m_sendSize = 0;
#ifdef _MSC_VER
					SetEvent(m_msgSent);
#else
					eventfd_write(m_msgSent, 1);
#endif

					break;
				}
			}
		}
	}

#ifdef _MSC_VER
	if((wsaEvents.lNetworkEvents & FD_CLOSE))
#else
	if((events & EPOLLHUP) != 0)
#endif
	{
		// Connection closed
		ATLASSERT(m_mode != SERVER);
		CLockGuard lock(m_critSect);
#ifndef _MSC_VER
		bool wasConnected = m_connected;
#endif
		m_connected = false;

		// Cancel any pending send
		if(m_sendSize != 0)
		{
			m_sendSize = 0;
#ifdef _MSC_VER
			SetEvent(m_msgSent);
#else
			eventfd_write(m_msgSent, 1);
#endif
		}

		if(m_socket != INVALID_SOCKET)
		{
			if(m_mode == DYNAMIC)
			{
				lock.CallUnlocked([&] { OnClose(WSAGetLastError()); });
#ifdef _MSC_VER
				WSAEventSelect(m_socket, m_wsaEvent, 0);
#endif
				ATLTRACE(_T("Closing socket %d\n"), m_socket);
				closesocket(m_socket);

				m_socket = INVALID_SOCKET;
				lock.CallUnlocked([&]
				{
					// Mark for removal from map
#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
					m_mapRWLock.LockForRead();
#else
					{
						CSharedLockGuard lock(m_mapRWLock);
#ifdef _MSC_VER
#pragma warning(suppress: 26165) //m_mapRWLock is unlocked by CSharedLockGuard dtor.
#endif
#endif
						try
						{
							auto entry = m_connections.find(this->shared_from_this());

							if(entry != m_connections.end() && entry->second != nullptr)
							{
								T* server = entry->second;
								entry->second = nullptr;
#ifdef _MSC_VER
								SetEvent(server->m_serverEvent);
#else
								eventfd_write(server->m_serverEvent, 1);
#endif
							}
						}
						catch(std::bad_weak_ptr&)
						{
							// Object is already being destroyed
						}

#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
						m_mapRWLock.Unlock();
#else
					}
#endif
				} );
			}
			else if(m_mode == CLIENT)
			{
#ifdef _MSC_VER
				lock.CallUnlocked([&] { OnClose(WSAGetLastError()); });
				WSAEventSelect(m_socket, m_wsaEvent, 0);
#else
				if(wasConnected)
				{
					lock.CallUnlocked([&] { OnClose(WSAGetLastError()); });
#endif
					closesocket(m_socket);

					// Reopen socket
					OpenSocket(m_peerAddress->ai_addr->sa_family);

					// Set options
					EnableKeepAlives();
#ifdef _MSC_VER
					WSAEventSelect(m_socket, m_wsaEvent, FD_CONNECT | FD_READ | FD_WRITE | FD_CLOSE);
#else
					m_socketEvent.events |= EPOLLOUT;
					epoll_ctl(m_networkEpoll, EPOLL_CTL_MOD, m_socket, &m_socketEvent);
				}
				else
				{
					// !wasconnected && m_mode == CLIENT
					// Disable EPOLLOUT
					epoll_ctl(m_networkEpoll, EPOLL_CTL_DEL, m_socket, nullptr);
				}
#endif
			}
		}
	}

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Helper for network thread
//
template<typename T> void CNetConnection<T>::OnRecvDone(void)
{
	ATLASSERT(m_mode != SERVER);

	// Update load factor
	CLockGuard lock(m_critSect);
	UpdateLoadFactor();

	// Re-enable read events
	if(m_socket != INVALID_SOCKET)
	{
#ifdef _MSC_VER
		WSAEventSelect(m_socket, m_wsaEvent, FD_CONNECT | FD_READ | FD_WRITE | FD_CLOSE);
#else
		m_socketEvent.events |= EPOLLIN;
		epoll_ctl(m_networkEpoll, EPOLL_CTL_MOD, m_socket, &m_socketEvent);
#endif
	}

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Helper for network thread
//
template<typename T> void CNetConnection<T>::OnServer(void)
{
	ATLASSERT(m_mode == SERVER);

	// Garbage collect closed dynamic connections
	size_t numConnections;
	numConnections = PurgeConnections(nullptr);

	// Enable / disable connections
	CLockGuard lock(m_critSect);

	if(m_connectionsDisabled && m_socket != INVALID_SOCKET)
	{
		// Close socket to stop listening
#ifdef _MSC_VER
		WSAEventSelect(m_socket, m_wsaEvent, 0);
#endif
		closesocket(m_socket);
		m_socket = INVALID_SOCKET;
	}
	else if(!m_connectionsDisabled && m_socket == INVALID_SOCKET)
	{
		// Open socket and listen
		SetupServerSocket();
#ifdef _MSC_VER
		WSAEventSelect(m_socket, m_wsaEvent, FD_ACCEPT);
#else
		m_socketEvent.events = EPOLLIN;
		epoll_ctl(m_networkEpoll, EPOLL_CTL_MOD, m_socket, &m_socketEvent);
#endif
		::listen(m_socket, SOMAXCONN);
	}
	else if(m_connectionsDisabled && m_socket == INVALID_SOCKET && numConnections < m_maxConnections)
	{
		// Reopen listening socket
		m_connectionsDisabled = false;
		SetupServerSocket();
#ifdef _MSC_VER
		WSAEventSelect(m_socket, m_wsaEvent, FD_ACCEPT);
#else
		m_socketEvent.events = EPOLLIN;
		epoll_ctl(m_networkEpoll, EPOLL_CTL_MOD, m_socket, &m_socketEvent);
#endif
		::listen(m_socket, SOMAXCONN);
	}

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Helper for network thread
//
template<typename T> void CNetConnection<T>::OnTimeout(bool idle)
{
	CLockGuard lock(m_critSect);

	if(m_socket == INVALID_SOCKET)
	{
		return;
	}

	ATLASSERT(m_mode != SERVER);

	if(idle)
	{
		LARGE_INTEGER perfCounter;
#ifdef _MSC_VER
		QueryPerformanceCounter(&perfCounter);
#else
		timespec time;
		clock_gettime(CLOCK_MONOTONIC, &time);
		perfCounter.QuadPart = time.tv_sec * 100000000ll + time.tv_nsec;
#endif
		double interval = double(perfCounter.QuadPart - m_lastLoadUpdate.QuadPart) / m_perfFreq.QuadPart;
		m_lastLoadUpdate.QuadPart = perfCounter.QuadPart;
		m_loadFactor *= exp(-interval / LOAD_FACTOR_TIME_CONSTANT);
	}
	else if(m_connected)
	{
		// Timed out during send or receive - disconnect
		ATLTRACE(_T("Timed out sending or receiving message with %s\n"), m_peerAddress->ai_canonname);
		m_connected = false;
#ifdef _MSC_VER
		WSAEventSelect(m_socket, m_wsaEvent, 0);
#endif
		closesocket(m_socket);
		m_socket = INVALID_SOCKET;

		if(m_sendSize != 0)
		{
			m_sendSize = 0;
#ifdef _MSC_VER
			SetEvent(m_msgSent);
#else
			eventfd_write(m_msgSent, 1);
#endif
		}

		m_bodySize = 0;
		lock.CallUnlocked([&] { OnClose(WSAETIMEDOUT); } );

		if(m_mode != CLIENT)
		{
			// Mark for removal from map
			lock.CallUnlocked([&]

			{
				try
				{
#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
					m_mapRWLock.LockForWrite();
#else
					{
						CLockGuard lock(m_mapRWLock);
#endif
						auto entry = m_connections.find(this->shared_from_this());

						if(entry != m_connections.end() && entry->second != nullptr)
						{
							T* server = entry->second;
							entry->second = nullptr;
#ifdef _MSC_VER
							SetEvent(server->m_serverEvent);
#else
							eventfd_write(server->m_serverEvent, 1);
#endif
						}
						else
						{
							ATLASSERT(FALSE);
						}

#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
						m_mapRWLock.Unlock();
#else
					}
#endif
				}
				catch(std::bad_weak_ptr&)
				{
					// Object is already being destroyed
				}
			});
		}
	}

	if(m_mode == CLIENT && !m_connected)
	{
		// Retry connection
		if(!m_connectionsDisabled)
		{
			// Try next address entry if there is one
			if(m_peerAddress->ai_next != nullptr)
			{
				m_peerAddress = m_peerAddress->ai_next;
			}
			else if(m_peerAddress != m_addrinfo)
			{
				m_peerAddress = m_addrinfo;
			}

			// Reopen socket
			if(m_socket != INVALID_SOCKET)
			{
#ifdef _MSC_VER
				WSAEventSelect(m_socket, m_wsaEvent, 0);
#endif
				closesocket(m_socket);
			}

			OpenSocket(m_peerAddress->ai_addr->sa_family);
			EnableKeepAlives();
#ifdef _MSC_VER
			WSAEventSelect(m_socket, m_wsaEvent, FD_CONNECT | FD_READ | FD_WRITE | FD_CLOSE);
#else
			m_socketEvent.events |= EPOLLOUT;
			epoll_ctl(m_networkEpoll, EPOLL_CTL_MOD, m_socket, &m_socketEvent);
#endif

			if(::connect(m_socket, m_peerAddress->ai_addr, int(m_peerAddress->ai_addrlen)) != SOCKET_ERROR)
			{
				m_connected = true;
#ifdef _MSC_VER
				WSAEventSelect(m_socket, m_wsaEvent, FD_WRITE | FD_CLOSE);
				SetEvent(m_connectEvent);
#else
				m_socketEvent.events = 0;
				epoll_ctl(m_networkEpoll, EPOLL_CTL_MOD, m_socket, &m_socketEvent);
				eventfd_write(m_connectEvent, 1);
#endif
			}
		}
	}

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Helper to open socket and set mode
//
template<typename T> void CNetConnection<T>::OpenSocket(int family)
{
#ifdef _MSC_VER
	m_socket = socket(family, SOCK_STREAM, IPPROTO_TCP);
#else
	m_socket = socket(family, SOCK_STREAM | SOCK_NONBLOCK, IPPROTO_TCP);
	m_socketEvent.events = 0;
	m_socketEvent.data.fd = m_socket;
	epoll_ctl(m_networkEpoll, EPOLL_CTL_ADD, m_socket, &m_socketEvent);
#endif
	static const int OFF = 0;

	if(family == AF_INET6 && setsockopt(m_socket, IPPROTO_IPV6, IPV6_V6ONLY, reinterpret_cast<const char*>(&OFF), sizeof(OFF)) != 0)
	{
		ATLTRACE(_T("Unable to set socket to dual IPv4/IPV6 mode: error %d\n"), WSAGetLastError());
	}

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Helper to purge connection map of unneeded connections
//
template<typename T> size_t CNetConnection<T>::PurgeConnections(_In_opt_ T* const server)
{
	size_t numConnections;
	std::vector<std::shared_ptr<T>> purges;	// List of connections to delete

#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
	m_mapRWLock.LockForRead();

	for(ConnectionMap::iterator p = m_connections.begin(); p != m_connections.end();)
	{
		if(p->second == server || p->second == nullptr)
		{
			m_mapRWLock.Upgrade();
			purges.push_back(std::move(p->first));
			p = m_connections.erase(p);
			m_mapRWLock.Downgrade();
		}
		else
		{
			++p;
		}
	}
	numConnections = m_connections.size();
	m_mapRWLock.Unlock();
#else
	bool needToErase = false;

	CSharedLockGuard::CallLocked(m_mapRWLock, [&]
	{
		// Scan with shared lock
		numConnections = m_connections.size();

		for(auto p = m_connections.begin(); p != m_connections.end(); ++p)
		{
			if(p->second == server || p->second == nullptr)
			{
				needToErase = true;
				break;
			}
		}
	});

	if(needToErase)
	{
		// Need to take exclusive lock
		CLockGuard lock(m_mapRWLock);

		for(auto p = m_connections.begin(); p != m_connections.end();)
		{
			if(p->second == server || p->second == nullptr)
			{
				purges.push_back(std::move(p->first));
				p = m_connections.erase(p);
			}
			else
			{
				++p;
			}
		}

		numConnections = m_connections.size();
	}
#endif

	// All locks are released so let purges be destructed

	return numConnections;
}


/////////////////////////////////////////////////////////////////////////////
//
// Safely send a message using a dynamic connection
//
template<typename T> bool CNetConnection<T>::Send(_In_opt_ CNetConnection* connection,
	_In_bytecount_(size) const void* msg, size_t size, DWORD timeout)
{
	if(connection == nullptr)
	{
		WSASetLastError(WSAENOTCONN);
		return false;
	}

#if _WIN32_WINNT < _WIN32_WINNT_VISTA
	m_mapRWLock.LockForRead();
#else
	CSharedLockGuard lock(m_mapRWLock);
#endif
	bool result = false;

	for(auto& connectionEntry : m_connections)
	{
		if(connection == connectionEntry.first.get())
		{
			if(connectionEntry.second != nullptr)
			{
				result = (timeout == INFINITE ? connection->Send(msg, size) : connection->Send(msg, size, timeout));
			}
			else
			{
				WSASetLastError(WSAENOTCONN);
			}

			break;
		}
	}

#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
	m_mapRWLock.Unlock();
#endif

#ifdef _MSC_VER
#pragma warning(suppress: 26165) //m_mapRWLock is unlocked by CSharedLockGuard dtor.
#endif
	return result;
}


/////////////////////////////////////////////////////////////////////////////
//
// Send a message
//
template<typename T> bool CNetConnection<T>::Send(_In_bytecount_(size) const void* msg, size_t size, DWORD timeout)
{
	if(std::this_thread::get_id() == m_networkThreadId)
	{
		// Can't send from network thread (deadlock)
		ATLASSERT(FALSE);
		WSASetLastError(WSAEADDRNOTAVAIL);
		return false;
	}

	if(m_mode == SERVER)
	{
		ATLASSERT(FALSE);
		WSASetLastError(WSAEADDRNOTAVAIL);
		return false;
	}

	ATLASSERT(m_callbackThread.joinable()); // Did you forget to call Startup() in your derived constructors?
	CLockGuard lock(m_sendMutex);
	CLockGuard lock2(m_critSect);
	m_sendMsg = static_cast<const char*>(msg);
	m_sentBytesSoFar = 0;
	m_sendSize = size;

	// Clear event
#ifdef _MSC_VER
	ResetEvent(m_msgSent);
#else
	epoll_event epollEvent;

	if(epoll_wait(m_sendEpoll, &epollEvent, 1, 0))
	{
		eventfd_t value;
		eventfd_read(m_msgSent, &value);
	}
#endif

	if(!m_connected)
	{
		if(m_mode == DYNAMIC)
		{
			// Give up - connection is down and will never come back
			WSASetLastError(WSAENOTCONN);
			return false;
		}
		else if(m_mode == CLIENT && !m_connectionsDisabled)
		{
			// Try next address entry if there is one
			if(m_peerAddress->ai_next != nullptr)
			{
				m_peerAddress = m_peerAddress->ai_next;
			}
			else if(m_peerAddress != m_addrinfo)
			{
				m_peerAddress = m_addrinfo;
			}

			// Retry connection
			if(::connect(m_socket, m_peerAddress->ai_addr, int(m_peerAddress->ai_addrlen)) != SOCKET_ERROR)
			{
				m_connected = true;
#ifdef _MSC_VER
				WSAEventSelect(m_socket, m_wsaEvent, FD_WRITE | FD_CLOSE);
				SetEvent(m_connectEvent);
#else
				m_socketEvent.events = 0;
				epoll_ctl(m_networkEpoll, EPOLL_CTL_MOD, m_socket, &m_socketEvent);
				eventfd_write(m_connectEvent, 1);
#endif
			}
		}
		else
		{
			// Give up
			m_sendSize = 0;
		}
	}
	else
	{
		// Send what we can immediately
		while(m_sentBytesSoFar != m_sendSize)
		{
#ifdef _MSC_VER
			int sent = send(m_socket, &m_sendMsg[m_sentBytesSoFar], int(m_sendSize - m_sentBytesSoFar), 0);
#else
			int sent = send(m_socket, &m_sendMsg[m_sentBytesSoFar], int(m_sendSize - m_sentBytesSoFar), MSG_NOSIGNAL);
#endif

			if(sent == SOCKET_ERROR)
			{
				if(WSAGetLastError() != WSAEWOULDBLOCK || timeout == 0)
				{
					m_sendSize = 0;
				}

				break;
			}
			else
			{
				m_sentBytesSoFar += sent;
			}
		}

		// Update load factor
		UpdateLoadFactor();
	}

	if(m_sentBytesSoFar == m_sendSize)
	{
		// Message sent
		m_sendSize = 0;
	}
	else if(m_sendSize != 0)
	{
		// Wait for network thread to finish sending message
#ifdef _MSC_VER
		if(lock2.CallUnlocked([&] { return WaitForSingleObject(m_msgSent, timeout); } ) == WAIT_TIMEOUT)
		{
			if(m_sentBytesSoFar == 0)
			{
				m_sendSize = 0;
				WSASetLastError(WSAETIMEDOUT);
			}
			else
			{
				lock2.CallUnlocked([&] { WaitForSingleObject(m_msgSent, INFINITE); } );

				if(m_sentBytesSoFar != size)
				{
					m_sendSize = 0;
					WSASetLastError(WSASYSCALLFAILURE);
				}
			}
		}
#else
		m_socketEvent.events |= EPOLLOUT;
		epoll_ctl(m_networkEpoll, EPOLL_CTL_MOD, m_socket, &m_socketEvent);

		if(lock2.CallUnlocked([&] { int status; while((status = epoll_wait(m_sendEpoll, &epollEvent, 1, timeout)) == -1 && errno == EINTR); return status; } ) == 0)
		{
			if(m_sentBytesSoFar == 0)
			{
				m_sendSize = 0;
				errno = ETIMEDOUT;
			}
			else
			{
				lock2.CallUnlocked([&] { eventfd_t value; eventfd_read(m_msgSent, &value); });
			}
		}

		m_socketEvent.events &= ~EPOLLOUT;
		epoll_ctl(m_networkEpoll, EPOLL_CTL_MOD, m_socket, &m_socketEvent);
#endif
	}

	// Return success code
	return m_sentBytesSoFar == size;
}


/////////////////////////////////////////////////////////////////////////////
//
// Set socket option
//
template<typename T> bool CNetConnection<T>::SetSockOpt(int optionName,
	_In_bytecount_(optionLen) const void* optionValue,
	socklen_t optionLen,
	int level)
{
	CLockGuard lock(m_critSect);

	return m_socket != INVALID_SOCKET && setsockopt(m_socket, level, optionName, static_cast<const char*>(optionValue), optionLen) == 0;
}


/////////////////////////////////////////////////////////////////////////////
//
// Set the thread's name in the debugger
//
template<typename T> void CNetConnection<T>::SetThreadName(LPCSTR name)
{
#ifdef _MSC_VER
#ifdef _DEBUG
#pragma warning(push)
#pragma warning(disable : 4616 6312 6322) // EXCEPTION_CONTINUE_EXECUTION and empty _except block
	// The call to RaiseException() below is meant for the DEBUGGER ONLY.
	// Don't do it if the app is NOT running through a debugger, or else we'll get an unhandled exception that we didn't want.
	if(IsDebuggerPresent())
	{
		const ULONG_PTR INFO[] = { 0x1000, reinterpret_cast<ULONG_PTR>(name), ULONG_PTR(-1), 0 };

		__try
		{
			RaiseException(0x406D1388, 0, _countof(INFO), INFO);
		}
		__except(EXCEPTION_CONTINUE_EXECUTION)
		{
		}
	}
#pragma warning(pop)
#else
	UNREFERENCED_PARAMETER(name);
#endif
#else
	pthread_setname_np(pthread_self(), name);
#endif

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Setup a server mode socket
//
template<typename T> void CNetConnection<T>::SetupServerSocket(void)
{
	ATLASSERT(m_mode == SERVER);

	// Setup address
	OpenSocket(m_peerAddress->ai_addr->sa_family);

	// Bind to any local address
	if(::bind(m_socket, m_peerAddress->ai_addr, int(m_peerAddress->ai_addrlen)) == SOCKET_ERROR)
	{
		ATLTRACE(_T("Unable to bind socket: error %d\n"), WSAGetLastError());
		throw CNetError();
	}

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Stop threads (must be called from derived class destructor)
//
template<typename T> void CNetConnection<T>::Shutdown(void)
{
	// Signal threads to stop
#ifdef _MSC_VER
	SetEvent(m_shutdown);
#else
	eventfd_write(m_shutdown, m_mode == SERVER ? 1 : 2);
#endif

	// Wait for callback thread to exit
	if(m_callbackThread.joinable())
	{
		m_callbackThread.join();
	}

	// Wait for network thread to exit
	if(m_networkThread.joinable())
	{
		m_networkThread.join();
	}

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Start threads (must be called from derived class constructors)
//
template<typename T> void CNetConnection<T>::Startup(bool initializeCom, DWORD coInit)
{
	// Start threads
	if(m_mode != SERVER)
	{
		m_callbackThread = std::thread(&CNetConnection<T>::CallbackThread, this, initializeCom, coInit);
	}

	m_networkThread = std::thread(&CNetConnection<T>::NetworkThread, this);

	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// Helper function. Call with m_critSect held
//
template<typename T> void CNetConnection<T>::UpdateLoadFactor(void)
{
	LARGE_INTEGER perfCounter;
#ifdef _MSC_VER
	QueryPerformanceCounter(&perfCounter);
#else
	timespec time;
	clock_gettime(CLOCK_MONOTONIC, &time);
	perfCounter.QuadPart = time.tv_sec * 100000000ll + time.tv_nsec;
#endif
	double interval = double(perfCounter.QuadPart - m_lastLoadUpdate.QuadPart) / m_perfFreq.QuadPart;
	m_lastLoadUpdate.QuadPart = perfCounter.QuadPart;
	unsigned long bytesWaiting;
#ifdef _MSC_VER
#pragma warning(suppress : 6001) // FIONREAD writes to bytesWaiting
#endif
	if(ioctlsocket(m_socket, FIONREAD, &bytesWaiting) == 0)
	{
		if(bytesWaiting == 0)
		{
			m_loadFactor *= exp(-interval / LOAD_FACTOR_TIME_CONSTANT);
		}
		else
		{
			double fractionFull = double(bytesWaiting) / m_networkBufSize;
			m_loadFactor = fractionFull - (fractionFull - m_loadFactor) * exp(-interval / LOAD_FACTOR_TIME_CONSTANT);
		}
	}

	return;
}
