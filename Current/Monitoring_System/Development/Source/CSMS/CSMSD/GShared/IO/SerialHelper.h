/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/
#pragma once

#include "StdAfx.h"

#include "serialport.h"

/// <summary>
/// This namespace has helper method to facilitate access to
///  the serial port implementation of PJ Naughter.
/// </summary>
namespace CSerialHelper
{
	// Constants
	enum EReturnVals { BYTE_READ, TIMEOUT_ERR, LINE_ERR };

	//Functions

	/// <summary>
	/// This method reads 1 char from the serial port with a timeout specified by
	///  the caller so that it will not block forever.
	/// </summary>
	/// <param name="timeoutMsec">
	/// Number of msec to wait for the timeout.
	/// </param>
	/// <param name="result">
	/// On successful return (BYTE_READ) will have the char as read from serial
	///  port. Otherwise the value is undefined.
	/// </param>
	/// <param name="port">
	/// Instance of CSerialPort object where the char should be read from.
	/// </param>
	/// <returns>
	/// BYTE_READ: char read.
	/// LINE_ERR: Error while reading the char.
	/// TIMEOUT_ERR: Timeout waiting for the char.
	/// </returns>
	inline EReturnVals ReadWithTimeout(_In_ DWORD timeoutMsec,
						unsigned char & result, CSerialPort & port)
	{
		DWORD errors;
		port.ClearError(errors);
		EReturnVals retVal = BYTE_READ;

		if(errors)
		{
			retVal = LINE_ERR;
		}

		COMMTIMEOUTS timeouts = { 0, 0, timeoutMsec, 0, 0 };
		port.SetTimeouts(timeouts);
		DWORD bytes;
		CEvent ioDone(FALSE, TRUE);
		OVERLAPPED data = { 0 };
		memset(&data, 0, sizeof(data));
		data.hEvent = ioDone.m_hObject;

		if(port.Read(&result, 1, data, &bytes))
		{
			if(bytes != 1)
			{
				retVal = TIMEOUT_ERR;
			}
		}
		else
		{
			// Char received
			port.GetOverlappedResult(data, bytes, TRUE);

			if(bytes != 1)
			{
				retVal = TIMEOUT_ERR;
			}
		}

		return retVal;
	}

	//////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Write string of characters to the serial port.
	/// </summary>
	/// <param name="pStr">
	/// Pointer to the string to output.
	/// </param>
	/// <param name="ostrLen">
	/// Number of characters to output.
	/// </param>
	/// <param name="port">
	/// Instance of CSerialPort object where the char should be read from.
	/// </param>
	/// <returns>
	/// true: The string has been output successfully.
	/// false: Error in outputting the string to the serial port.
	/// </returns>
	/// <remarks>
	/// @note: 1. It is assumed caller has locked serial access possibly
	///           with critical section object.
	///        2. Caller will handle serial port exception that may be generated.
	/// </remarks>
	inline bool WriteString(_In_ const char *pStr, _In_ size_t ostrLen, CSerialPort & port)
	{
		CEvent ioDone(FALSE, TRUE);
		OVERLAPPED o1;
		memset(&o1, 0, sizeof(o1));
		o1.hEvent = ioDone.m_hObject;
		bool success = true;

		if( !port.Write(pStr, ostrLen, o1) )
		{
			DWORD bytes;
			port.GetOverlappedResult(o1, bytes, TRUE);

			if(bytes != static_cast<DWORD>(ostrLen))
			{
				success = false;
			}
		}

		return success;
	}
};
