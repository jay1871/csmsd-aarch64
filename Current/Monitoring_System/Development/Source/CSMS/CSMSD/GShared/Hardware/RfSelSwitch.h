/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2017 TCI International, Inc. All rights reserved			  *
**************************************************************************/
#pragma once

#include "EquipCtrlMsg.h"

template<typename T> class CSingleton;

///<summary>
/// This class controls RF Antenna Switch selections.
///</summary>
///
/// <remarks>
///@note: 1. Currently it assumes the following switch input connection:
///		J1 - Not connected. J2 - LPA Antenna (20 MHz - 6 GHz).
///		J3 - 647 Vertical monitor. J4 - 647 Horizontal monitor.
///	     However a different antenna connection can be allowed simply
///		by expanding the EANTSetup.
/// </remarks>
///
class CRfSelSwitch
{
	// Friends
	friend class CSingleton < CRfSelSwitch >;

public:
	// Types
	enum EANTSetup
	{
		C_NO_SWITCH = 0,
		C_TCI5143,
	};

	enum ESwitchState
	{
		C_NOT_CONNECTED = 0,
		C_ANT_VAL_NOT_MATCHED,
		C_CONNECTED,		//Connected to RF switch & input value read.
		C_READ_ERR,
		C_SET_ERR,
		C_SET_NOT_VERIFIED,
	};

	const std::string& GetStatusStr(bool & isOk) const;
	bool Init(EANTSetup sysType);
	bool IsAntConnected(SEquipCtrlMsg::EAnt antVal, ESwitchState & failReason) const;
	bool Need2VerifySetAnt(void) const {return(m_switchState == C_SET_NOT_VERIFIED);}
	bool ReadSwitchAntVal(SEquipCtrlMsg::EAnt & ant);
	bool SetAnt(SEquipCtrlMsg::EAnt ant, bool dontInit = false);

private:
	// Types
	enum EMiniCircuitInput : unsigned char
	{
		C_INVALID_INPUT = 0,
		///@note: The numerical value specified must correspond to
		///	   Mini-Circuits State value.
		C_INPUT1 = 1,
		C_INPUT2 = 2,
		C_INPUT3 = 3,
		C_INPUT4 = 4,
	};

	//SSwitchAntInput has the mapping of switch input to the antenna
	// connected to that input.
	struct SSwitchAntInput
	{
		EMiniCircuitInput	switchIn;
		SEquipCtrlMsg::EAnt	ant;
	};

	// Constants
	static const SEquipCtrlMsg::EAnt C_5143_DEFAULT_ANT = SSmsMsg::ANT1_647V_MON;
	static const std::string C_CONNECTED_STR;
	static const std::string C_NOT_CONNECTED_STR;
	static const std::string C_READ_ERR_STR;
	static const std::string C_SET_ERR_STR;
	static const std::string C_UNKNOWN_STR;
	static const SSwitchAntInput	TCI5143_ANT_MAP[];

	// Data
	SEquipCtrlMsg::EAnt m_ant;
	EMiniCircuitInput m_currAntPort;
	size_t m_numAntPort;
	const SSwitchAntInput *m_pSwitch2Ant;
	ESwitchState m_switchState;
	EANTSetup m_switchType;

	// Functions
	CRfSelSwitch(void);	//Only CSingleton can ctor & dtor.
	~CRfSelSwitch(void);
	//Disallow compiler auto-generated methods.
	CRfSelSwitch(const CRfSelSwitch & copyFrom);
	CRfSelSwitch & operator=(const CRfSelSwitch & right);

	SEquipCtrlMsg::EAnt GetConnectedAnt(EMiniCircuitInput inPortNum) const;
	bool GetSwitchPort(SEquipCtrlMsg::EAnt ant, unsigned char & portNum) const;
};

//////////////////////////////////////////////////////////////////////
///<summary>
/// Return the switch status in string and whether it is in error.
///</summary>
///
/// <param name="isOk">
///@param[out] true: RF Switch works correctly.
///			   false: RF switch is in error.
/// </param>
///
/// <returns>
///@return: String that has the switch status.
/// </returns>
///
inline const std::string& CRfSelSwitch::GetStatusStr(bool & isOk) const
{
	switch(m_switchState)
	{
	case ESwitchState::C_NOT_CONNECTED:
		isOk = false;
		return(C_NOT_CONNECTED_STR);
		break;

	case ESwitchState::C_ANT_VAL_NOT_MATCHED:
		//pass through
	case ESwitchState::C_CONNECTED:
		//pass through
	case ESwitchState::C_SET_NOT_VERIFIED:
		isOk = true;
		return(C_CONNECTED_STR);
		break;

	case ESwitchState::C_READ_ERR:
		isOk = false;
		return(C_READ_ERR_STR);
		break;

	case ESwitchState::C_SET_ERR:
		isOk = false;
		return(C_SET_ERR_STR);
		break;

	default:
		isOk = false;
		ASSERT(false);
		return(C_UNKNOWN_STR);
		break;
	}


	isOk = false;
	return(C_UNKNOWN_STR);
}
