/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013-2014 TCI International, Inc. All rights reserved		  *
**************************************************************************/

#pragma once

#include "Config.h"
#include "N9010A544B25.h"

#ifdef _MSC_VER
#define CEShfExt CConfig
#define CEShfExtStatus CConfig
#else
#define CEShfExt CConfig::EShfExt
#define CEShfExtStatus CConfig::EShfExtStatus
#endif

template<typename T> class CSingleton;

class CShfExt
{
	// Friends
	friend class CSingleton<CShfExt>;

public:
	CShfExt(void);
	~CShfExt(void) {}

	bool DisableIfOutput(void);
	bool EnableIfOutput(void);
#ifdef _MSC_VER
	static const CString & GetId(void);
#else
	static const std::string & GetId(void);
#endif
	static Units::Frequency GetIfBandwidth(void);
	static Units::Frequency GetIfFrequency(void);
	static Units::FreqPair GetRfFrequencyLimits(void);  // Careful: this is hardware limits, not necessarily processing limits
#ifdef _MSC_VER
	static const CString & GetOptStr(void);
#else
	static const std::string & GetOptStr(void);
#endif
	static Units::Frequency GetRfMaxFreq(void);
	static CConfig::EShfExtStatus GetShfExtStatus(void);
	bool IsSpectrumInverted(_In_ Units::Frequency freq) const;
	bool Tune(_In_ Units::Frequency freq);
	static void UpdateAnalyzer(_In_ CConfig::EShfExt shfExt);
	void Wait(void);

private:
	bool m_IFOutputEnabled;
	Units::Frequency m_lastTuneFreq;
#ifdef _MSC_VER
	static CString s_emptyStr;
#else
	static std::string s_emptyStr;
#endif
	static std::unique_ptr<CN9010A544> s_pN9010AAnalyzer;
	static CConfig::EShfExt s_shfExt;

	// Functions
	static void ChangeAnalyzer(_In_ CConfig::EShfExt shfExt);

	//Disallow compiler auto-generated methods.
	CShfExt(const CShfExt & copyFrom);
	CShfExt & operator=(_In_ const CShfExt & right);
};

//////////////////////////////////////////////////////////////////////
/// <summary>
/// Returns the IDN response string of the SHF Extension Signal Analyzer.
/// </summary>
/// <returns>
/// String response from SHF Extension Signal Analyzer.
/// Empty string if NO_SHF_EXT or not connected.
/// </returns>
#ifdef _MSC_VER
inline const CString & CShfExt::GetId(void)
#else
inline const std::string & CShfExt::GetId(void)
#endif
{
	if (s_shfExt == CEShfExt::NO_SHF_EXT)
	{
		return(s_emptyStr);
	}
	else
	{
		return(s_pN9010AAnalyzer->GetId());
	}
}

//////////////////////////////////////////////////////////////////////
/// <summary>
/// Returns the Opt response string of the SHF Extension Signal Analyzer.
/// </summary>
/// <returns>
/// String response from SHF Extension Signal Analyzer.
/// Empty string if NO_SHF_EXT or not connected.
/// </returns>
#ifdef _MSC_VER
inline const CString & CShfExt::GetOptStr(void)
#else
inline const std::string & CShfExt::GetOptStr(void)
#endif
{
	if (s_shfExt == CEShfExt::NO_SHF_EXT)
	{
		return(s_emptyStr);
	}
	else
	{
		return(s_pN9010AAnalyzer->GetOptStr());
	}
}

//////////////////////////////////////////////////////////////////////
/// <summary>
/// Returns the SHF Extension Signal Analyzer connection status.
/// </summary>
/// <returns>
/// Connection status of SHF Extension Signal Analyzer.
/// </returns>
inline CConfig::EShfExtStatus CShfExt::GetShfExtStatus(void)
{
	CConfig::EShfExtStatus statusCode = CEShfExtStatus::SHF_EXT_NOT_SPECIFIED;

	if( s_shfExt != CEShfExt::NO_SHF_EXT )
	{
		statusCode = s_pN9010AAnalyzer->GetStatus();
	}

	return(statusCode);
}
