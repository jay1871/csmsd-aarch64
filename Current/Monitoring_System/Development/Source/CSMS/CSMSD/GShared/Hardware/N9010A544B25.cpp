/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013-2014 TCI International, Inc. All rights reserved		  *
**************************************************************************/

#include "stdafx.h"

#include "N9010A544B25.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#ifdef _MSC_VER
#define CEShfExt CConfig
#define CEShfExtStatus CConfig
#else
#define CEShfExt CConfig::EShfExt
#define CEShfExtStatus CConfig::EShfExtStatus
#endif

//////////////////////////////////////////////////////////////////////
//
// Static data
//
unsigned long long CN9010A544::s_ifBw = CN9010A544::IF_25MHZ_BANDWIDTH;
unsigned long long CN9010A544::s_ifCenterFreq = CN9010A544::IF_25MHZ_CENTER_FREQ;
unsigned long long CN9010A544::s_maxFreq = CN9010A544::RF_44GHZ_MAX_FREQ;
unsigned long long CN9010A544::s_needRespCnt = 0;
unsigned long long CN9010A544::s_respCnt = 0;

#ifdef CSMS_DEBUG
static const size_t C_PRINT_PERIOD = 200;
static unsigned long long s_origTuneUsec = 0;
static unsigned long long s_prevTuneUsec = 0;
static unsigned long long s_maxTuneElapsedUsec = 0;
static unsigned long long s_minTuneElapsedUsec = 0;
static unsigned long long s_totalTuneUsec = 0;
static size_t s_tuneCalled = 0;
static size_t s_waitCalled = 0;
static unsigned long long s_maxWaitElapsedUsec = 0;
static unsigned long long s_minWaitElapsedUsec = 0;
static unsigned long long s_totalWaitUsec = 0;
static unsigned s_maxWaitLoopCnt = 0;
static size_t s_totalWaitLoopCnt = 0;
#endif
//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CN9010A544::CN9010A544(CConfig::EShfExt shfExtType)
:
	CShfExtNet(CConfig::GetShfExtIpAddr(), CConfig::GetShfExtIpPort()),
	m_hardwareConnected(false), m_shfExt(shfExtType),
	m_status(CEShfExtStatus::ANALYZER_NOT_FOUND)
{
	switch( shfExtType )
	{
	case CEShfExt::N9010A_544_B25:
		s_ifBw = IF_25MHZ_BANDWIDTH;
		s_ifCenterFreq = IF_25MHZ_CENTER_FREQ;
		s_maxFreq = RF_44GHZ_MAX_FREQ;
		break;

	case CEShfExt::N9010A_544_B40:
		s_ifBw = IF_40MHZ_BANDWIDTH;
		s_ifCenterFreq = IF_40MHZ_CENTER_FREQ;
		s_maxFreq = RF_44GHZ_MAX_FREQ;
		break;

	case CEShfExt::N9010A_526_B40:
		s_ifBw = IF_40MHZ_BANDWIDTH;
		s_ifCenterFreq = IF_40MHZ_CENTER_FREQ;
		s_maxFreq = RF_26GHZ_MAX_FREQ;
		break;

	case CEShfExt::N9010A_532_B40:
		s_ifBw = IF_40MHZ_BANDWIDTH;
		s_ifCenterFreq = IF_40MHZ_CENTER_FREQ;
		s_maxFreq = RF_32GHZ_MAX_FREQ;
		break;

	default:
		ASSERT(false);
		break;

	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CN9010A544::~CN9010A544(void)
{
	if(m_hardwareConnected)
	{
#ifdef _MSC_VER
		CStringA send;
#else
		std::string send;
#endif
		// enable instrument display
		send = ":DISP:ENAB ON\n";
#ifdef _MSC_VER
		SendShf(send.GetBuffer(), send.GetLength());
		send.ReleaseBuffer();
		Sleep(20L);	//Sleep 20 msec.
#else
		SendShf(send.c_str(), send.length());
		usleep(20000);	//Sleep 20 msec.
#endif
		// enable instrument auto calibrations
		send = ":CAL:AUTO ON\n";
#ifdef _MSC_VER
		SendShf(send.GetBuffer(), send.GetLength());
#else
		SendShf(send.c_str(), send.length());

		if (m_thread.joinable())
		{
			m_thread.join();
		}
#endif
	}
}


//////////////////////////////////////////////////////////////////////
//
// Tune the instrument
//   note -- for this instrument, tune is also used to disable/enable the IF output
//
bool CN9010A544::Tune(Units::Frequency freq)
{
#ifdef CSMS_DEBUG
	static timespec l_res;
	static size_t l_count = 1;
	static const unsigned long long C_MAX_ELAPSED_TIME = 48LL * 60LL * 60LL * 1000000LL; //2 days.

	if(s_tuneCalled != s_waitCalled)
	{
		TRACE("CN9010A544::Tune(count %u): s_tuneCalled(%u) != s_waitCalled(%u), min %llu usec, max %llu usec, ave %llu usec, total %lld usec, count %u\n",
		      l_count, s_tuneCalled, s_waitCalled, s_minTuneElapsedUsec, s_maxTuneElapsedUsec,
		      s_totalTuneUsec/static_cast<unsigned long long>(l_count), s_totalTuneUsec, l_count);
	}

	++s_tuneCalled;
#endif

	if(!m_hardwareConnected)
	{
		return false;
	}

#ifdef CSMS_DEBUG
	timespec ts;

	if(s_origTuneUsec == 0)
	{
		if (clock_getres(CLOCK_MONOTONIC, &l_res) != 0)
		{
			TRACE("CN9010A544::Tune: clock_getres failed (%d => %s)\n", errno, std::strerror(errno));
		}
		else
		{
			if (clock_gettime(CLOCK_MONOTONIC, &ts) != 0)
		    	{
				TRACE("CN9010A544::Tune: clock_gettime failed (%d => %s)\n", errno, std::strerror(errno));
		    	}
			else
			{
				s_prevTuneUsec = s_origTuneUsec = static_cast<unsigned long long>(ts.tv_sec) *1000000LL +
				    static_cast<unsigned long long>(ts.tv_nsec) /1000LL;
			}
			TRACE("CN9010A544::Tune: Resolution sec = %ld nsec =%ld; Current sec = %ld nsec =%ld\n",
			      l_res.tv_sec, l_res.tv_nsec, ts.tv_sec, ts.tv_nsec);
		}
	}
	else
	{
		if (clock_gettime(CLOCK_MONOTONIC, &ts) != 0)
	    	{
			TRACE("CN9010A544::Tune2: clock_gettime failed (%d => %s)\n", errno, std::strerror(errno));
	    	}
	    	else
	    	{
	    		unsigned long long currentTime = static_cast<unsigned long long>(ts.tv_sec) * 1000000LL +
	    		      static_cast<unsigned long long>(ts.tv_nsec) /1000LL;

	    		if(currentTime < s_prevTuneUsec)
	    		{
	    			TRACE("CN9010A544::Tune: currentTime(%lld usec) < s_prevTuneUsec(%lld usec); origTime %lld usec, total %lld usec, count %u, Current sec = %ld nsec =%ld\n",
	    			      currentTime, s_prevTuneUsec, s_origTuneUsec, s_totalTuneUsec, l_count,
	    			      ts.tv_sec, ts.tv_nsec);
	    			ASSERT(false);
	    		}
	    		else
	    		{
		    		auto elapsedTime = currentTime  - s_prevTuneUsec;

		    		if(elapsedTime > C_MAX_ELAPSED_TIME)
		    		{
		    			TRACE("CN9010A544::Tune: elapsedTime %llu over limit, currentTime(%llu usec), s_prevTuneUsec(%llu usec); origTime %llu usec, total %llu usec, count %u, Current sec = %ld nsec =%ld\n",
		    			      elapsedTime, currentTime, s_prevTuneUsec, s_origTuneUsec, s_totalTuneUsec, l_count,
		    			      ts.tv_sec, ts.tv_nsec);
		    			clock_gettime(CLOCK_MONOTONIC, &ts);
		    			currentTime = static_cast<unsigned long long>(ts.tv_sec) * 1000000LL +
		    				    		      static_cast<unsigned long long>(ts.tv_nsec) /1000LL;
		    			elapsedTime = currentTime  - s_prevTuneUsec;
		    			ASSERT(elapsedTime < C_MAX_ELAPSED_TIME);
		    		}

		    		s_totalTuneUsec += elapsedTime;
		    		s_prevTuneUsec = currentTime;
				++l_count;

		    		if(s_minTuneElapsedUsec == 0 || s_minTuneElapsedUsec > elapsedTime)
		    		{
		    			s_minTuneElapsedUsec = elapsedTime;
		    		}

		    		if(s_maxTuneElapsedUsec < elapsedTime)
		    		{
		    			s_maxTuneElapsedUsec = elapsedTime;
		    		}
	    		}

	    		if(l_count % C_PRINT_PERIOD == 0)
	    		{
				TRACE("CN9010A544::Tune(count %u): min %llu usec, max %llu usec, ave %llu usec, total %lld usec, count %u\n",
				      l_count, s_minTuneElapsedUsec, s_maxTuneElapsedUsec,
				      s_totalTuneUsec/static_cast<unsigned long long>(l_count), s_totalTuneUsec, l_count);
	    		}
	    	}
	}
#endif

#ifdef _MSC_VER
		CStringA send;
		// :SENS:FREQ:CENT %lf MHZ tunes device
		// :INIT:IMM performs a 1 point sweep operation (needed for wait logic)
		// *OPC? returns a 1 AFTER sweep operation is complete  (needed for wait logic)
		send.Format(":SENS:FREQ:CENT %lf MHZ;:INIT:IMM;*OPC?\n", Units::Frequency(freq).Hz<double>() / 1000000.0);
		SendShf(send.GetBuffer(), send.GetLength(), true);
#else
		std::string send = ":SENS:FREQ:CENT " + std::to_string(Units::Frequency(freq).Hz<double>() / 1000000.0) +
				    " MHZ;:INIT:IMM;*OPC?\n";
		SendShf(send.c_str(), send.length(), true);
#endif

	//tune command needs respond.
	++s_needRespCnt;

	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for operation (1 point sweep following tune) to complete
//
void CN9010A544::Wait(void)
{
#ifdef CSMS_DEBUG
	static size_t l_count = 0;

	++s_waitCalled;
	timespec startTime;
	clock_gettime(CLOCK_MONOTONIC, &startTime);
#endif

	if(!m_hardwareConnected || (s_needRespCnt == s_respCnt))
	{
		return;
	}

#ifdef _MSC_VER
	auto respLen = GetResponse().GetLength();
#else
	//usleep(500000);	//Sleep 500 msec.
	//Sleep(10000L);	//Sleep 10 sec.
	//usleep(50000);	//Sleep 50 msec.
	static const unsigned C_DEFAULT_WAIT = 25; //25 (Old:200) msec wait.
	static const unsigned C_MAX_WAIT = 5000; //5 second max. wait.
	static const unsigned C_LOOP_CNT = C_MAX_WAIT/C_DEFAULT_WAIT;
	size_t respLen;
	std::string resp;
	unsigned i = 0;

	for(; i < C_LOOP_CNT; ++i)
	{
		resp = GetResponse(C_DEFAULT_WAIT);
		respLen = resp.length();

		if(respLen > 0)
		{
			break; //Respond accepted.
		}
	}
#endif
	if(0 == respLen)
	{
		ASSERT(FALSE);  // response timeout
	}

#ifdef CSMS_DEBUG
	if(i > s_maxWaitLoopCnt)
	{
		s_maxWaitLoopCnt = i;
	}
	s_totalWaitLoopCnt += i;

	timespec endTime;
	clock_gettime(CLOCK_MONOTONIC, &endTime);

	if ( !FindStr(resp, "1") )
	{
		TRACE("CN9010A544::Wait Err Resp(%s): Start(sec = %ld nsec =%ld; Current sec = %ld nsec =%ld, maxLoop %u, totalLoop %u\n",
		      resp.c_str(), startTime.tv_sec, startTime.tv_nsec, endTime.tv_sec, endTime.tv_nsec,
		      s_maxWaitLoopCnt, s_totalWaitLoopCnt);

	}

	unsigned long long endUsec = static_cast<unsigned long long>(endTime.tv_sec) * 1000000LL +
				      static_cast<unsigned long long>(endTime.tv_nsec) /1000LL;
	unsigned long long startUsec = static_cast<unsigned long long>(startTime.tv_sec) * 1000000LL +
					      static_cast<unsigned long long>(startTime.tv_nsec) /1000LL;
	auto elapsedUsec = endUsec - startUsec;
	s_totalWaitUsec += elapsedUsec;
	++l_count;

	if(s_minWaitElapsedUsec == 0 || s_minWaitElapsedUsec > elapsedUsec)
	{
		s_minWaitElapsedUsec = elapsedUsec;
	}

	if(s_maxWaitElapsedUsec < elapsedUsec)
	{
		s_maxWaitElapsedUsec = elapsedUsec;
	}


	if(l_count % C_PRINT_PERIOD == 0)
	{
		TRACE("CN9010A544::Wait(count %u): min %llu usec, max %llu usec, ave %llu usec, total %lld usec, curr %llu usec, maxLoop %u, totalLoop %u\n",
		      l_count, s_minWaitElapsedUsec, s_maxWaitElapsedUsec,
		      s_totalWaitUsec/static_cast<unsigned long long>(l_count), s_totalWaitUsec, elapsedUsec,
		      s_maxWaitLoopCnt, s_totalWaitLoopCnt);
	}
#endif
	++s_respCnt;

	return;
}

//////////////////////////////////////////////////////////////////////
//
// Tests for proper hardware, initializes the hardware, and maintains m_hardwareConnected
//
void CN9010A544::OnConnection(bool connect)
{
	if(connect)
	{
#ifdef _MSC_VER
	    AfxBeginThread(InitDeviceThread, this);
#else
	    m_thread = std::thread(&CN9010A544::DeviceThread, this);
#endif
	}
	else if( m_hardwareConnected )
	{
#ifdef _MSC_VER
		CSingleLock lock(&m_critSect, TRUE);
#else
		std::lock_guard<std::recursive_mutex> lock(m_critSect);
#endif
		m_hardwareConnected = false;
		if( m_status == CEShfExtStatus::ANALYZER_CONNECTED )
		{
			m_status = CEShfExtStatus::ANALYZER_NOT_FOUND;
		}
		if (m_thread.joinable())
		{
			m_thread.join();
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Initialize spectrum analyzer
//  (invoked as thread because started in response to OnConnection(), but 
//   initialization relies on OnMessage(), which is blocked until 
//   OnConnection() returns)
//
void CN9010A544::DeviceThread(void)
{
	// following sets m_hardwareConnected to false if improper instrument identification or response timeout
	bool hardwareConnected = true;
#ifdef _MSC_VER
	CStringA send("*IDN?\n");
	SendShf(send.GetBuffer(), send.GetLength(), true);
	send.ReleaseBuffer();
	CStringA resp = GetResponse();
	m_id = resp;
	if(-1 == resp.Find("N9010A"))
#else
	std::string send("*IDN?\n");
	SendShf(send.c_str(), send.length(), true);
	std::string resp = GetResponse();
	m_id = resp;
	//if(resp.find("N9010A") == std::string::npos && resp.find("N9010B") == std::string::npos)
	if(resp.find("N9010A") == std::string::npos) //Currently we only support N9010A Analyzer.
#endif
	{
#ifdef _MSC_VER
		CSingleLock lock(&m_critSect, TRUE);
#else
		std::lock_guard<std::recursive_mutex> lock(m_critSect);
#endif
		m_status = CEShfExtStatus::INCOMPATIBLE_ANALYZER;
		hardwareConnected = false;  // wrong hardware or response timeout
	}
#ifdef _MSC_VER
	else
	{
		CSingleLock lock(&m_critSect, TRUE);
#else
	else
	{
		std::unique_lock<std::recursive_mutex> lock(m_critSect);
#endif
		m_status = CEShfExtStatus::ANALYZER_CONNECTED;
		send = "*OPT?\n";
#ifdef _MSC_VER
		lock.Unlock();
		SendShf(send.GetBuffer(), send.GetLength(), true);
		send.ReleaseBuffer();
		resp = GetResponse();
		m_opt = resp;
		bool is526 = (-1 != resp.Find("526"));
		bool is532 = (-1 != resp.Find("532"));
		bool is544 = (-1 != resp.Find("544"));
#else
		lock.unlock();
		SendShf(send.c_str(), send.length(), true);
		resp = GetResponse();
		m_opt = resp;
		bool is526 = (std::string::npos != resp.find("526"));
		bool is532 = (std::string::npos != resp.find("532"));
		bool is544 = (std::string::npos != resp.find("544"));
#endif
		if (!is544 && !is526 && !is532)
		{
#ifdef _MSC_VER
			lock.Lock();
#else
			lock.lock();
#endif
			hardwareConnected = false;  // wrong hardware or response timeout
			m_status = CEShfExtStatus::INCOMPATIBLE_ANALYZER;
		}
		else
		{
			auto	typeMatched = false;

			if (is526)
			{
				if (m_shfExt == CEShfExt::N9010A_526_B40)
				{
					//25 MHz or 40 MHz will work because the IF
					// output is always > 40 MHz.
					//The B/W specification is after FFT processing.
					typeMatched = FindStr(resp, "B40") ||
					    FindStr(resp, "B25");
				}
			}
			else if (is532)
			{
				if (m_shfExt == CEShfExt::N9010A_532_B40)
				{
					//25 MHz or 40 MHz will work because the IF
					// output is always > 40 MHz.
					//The B/W specification is after FFT processing.
					typeMatched = FindStr(resp, "B40") ||
					    FindStr(resp, "B25");
				}
			}
			else
			{
				if (m_shfExt == CEShfExt::N9010A_544_B25)
				{
					typeMatched = FindStr(resp, "B25");
				}
				else if (m_shfExt == CEShfExt::N9010A_544_B40)
				{
					typeMatched = FindStr(resp, "B40");
				}
			}
			
			if(!typeMatched)
			{
#ifdef _MSC_VER
				lock.Lock();
#else
				lock.lock();
#endif
				hardwareConnected = false;  // wrong hardware
				m_status = CEShfExtStatus::INCOMPATIBLE_ANALYZER;
			}
			else
			{
				send = "*RST\n";
#ifdef _MSC_VER
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(100L);
				send = ":SENS:FREQ:SPAN 0 HZ\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":SENS:POW:RF:ATT 0\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":SENS:BAND:RES 8 MHZ\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":SENS:BAND:VID 8 MHZ\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":SENS:SWE:TIME 0.001\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":SENS:SWE:POIN 1\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":SENS:IF:GAIN:FFT:AUTO OFF\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":SENS:IF:GAIN:FFT HIGH\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":SENS:IF:GAIN:SWEP:AUTO OFF\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":SENS:IF:GAIN:SWEP ON\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":POW:GAIN:BAND FULL\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":POW:GAIN ON\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":OUTP:AUX SIF\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":CAL:AUTO:ALER NONE\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":CAL:AUTO OFF\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":INIT:CONT OFF\n";
				SendShf(send.GetBuffer(), send.GetLength());
				send.ReleaseBuffer();
				Sleep(20L);
				send = ":DISP:ENAB OFF\n";
				SendShf(send.GetBuffer(), send.GetLength());
#else
				SendShf(send.c_str(), send.length());
				usleep(100000);	//Sleep 100 msec.
				send = ":SENS:FREQ:SPAN 0 HZ\n";
				SendShf(send.c_str(), send.length());
				usleep(20000);	//Sleep 20 msec.
				send = ":SENS:POW:RF:ATT 0\n";
				SendShf(send.c_str(), send.length());
				usleep(20000);	//Sleep 20 msec.
				send = ":SENS:BAND:RES 8 MHZ\n";
				SendShf(send.c_str(), send.length());
				usleep(20000);	//Sleep 20 msec.
				send = ":SENS:BAND:VID 8 MHZ\n";
				SendShf(send.c_str(), send.length());
				usleep(20000);	//Sleep 20 msec.
				send = ":SENS:SWE:TIME 0.001\n";	//Keysight 11/21/17 email to 1 msec.
				SendShf(send.c_str(), send.length());
				usleep(20000);	//Sleep 20 msec.
				send = ":SENS:SWE:POIN 1\n";
				SendShf(send.c_str(), send.length());
				usleep(20000);	//Sleep 20 msec.
				send = ":SENS:IF:GAIN:FFT:AUTO OFF\n";
				SendShf(send.c_str(), send.length());
				usleep(20000);	//Sleep 20 msec.
				send = ":SENS:IF:GAIN:FFT HIGH\n";
				SendShf(send.c_str(), send.length());
				usleep(20000);	//Sleep 20 msec.
				send = ":SENS:IF:GAIN:SWEP:AUTO OFF\n";
				SendShf(send.c_str(), send.length());
				usleep(20000);	//Sleep 20 msec.
				send = ":SENS:IF:GAIN:SWEP ON\n";
				SendShf(send.c_str(), send.length());
				usleep(20000);	//Sleep 20 msec.
				send = ":POW:GAIN:BAND FULL\n";
				SendShf(send.c_str(), send.length());
				usleep(20000);	//Sleep 20 msec.
				send = ":POW:GAIN ON\n";
				SendShf(send.c_str(), send.length());
				usleep(20000);	//Sleep 20 msec.
				send = ":OUTP:AUX SIF\n";
				SendShf(send.c_str(), send.length());
				usleep(20000);	//Sleep 20 msec.
				send = ":CAL:AUTO:ALER NONE\n";
				SendShf(send.c_str(), send.length());
				usleep(20000);	//Sleep 20 msec.
				send = ":CAL:AUTO OFF\n";
				SendShf(send.c_str(), send.length());
				usleep(20000);	//Sleep 20 msec.
				send = ":INIT:CONT OFF\n";
				SendShf(send.c_str(), send.length());
 #ifndef CSMS_DEBUG
				usleep(20000);	//Sleep 20 msec.
				send = ":DISP:ENAB OFF\n";
				SendShf(send.c_str(), send.length());
 #endif
#endif
				if (m_shfExt == CEShfExt::N9010A_544_B40 ||
				    m_shfExt == CEShfExt::N9010A_544_B25 ||
				    m_shfExt == CEShfExt::N9010A_526_B40 ||
				    m_shfExt == CEShfExt::N9010A_532_B40)
				{
					//40MHz Analyzer must be in Signal Analyzer mode for the
					// IF frequency to be at 322.5 MHz.
#ifdef _MSC_VER
					send.ReleaseBuffer();
					Sleep(20L);
					send = ":INST:SEL SA\n";
					SendShf(send.GetBuffer(), send.GetLength());
					send.ReleaseBuffer();
					Sleep(20L);
					send = ":INST:SEL?\n";
					SendShf(send.GetBuffer(), send.GetLength(), true);
					resp = GetResponse();
#else
					usleep(20000);	//Sleep 20 msec.
					send = ":INST:SEL SA\n";
					SendShf(send.c_str(), send.length());
					usleep(20000);	//Sleep 20 msec.
					send = ":INST:SEL?\n";
					SendShf(send.c_str(), send.length(), true);
					resp = GetResponse();
#endif
					if ( !FindStr(resp, "SA") )
					{
#ifdef _MSC_VER
						lock.Lock();
#else
						lock.lock();
#endif
						m_status = CEShfExtStatus::ANALYZER_WRONG_MODE;
						hardwareConnected = false;  // wrong hardware or response timeout
					}
				}

			}
		}
	}

	{
#ifdef _MSC_VER
		CSingleLock lock(&m_critSect, TRUE);
#else
		std::lock_guard<std::recursive_mutex> lock(m_critSect);
#endif
		m_hardwareConnected = hardwareConnected;
	}

	if(!m_hardwareConnected)
	{
		// connected, but improper instrument identification or instrument not responding so disconnect to try again
		Close();
	}
}

#ifdef _MSC_VER
//////////////////////////////////////////////////////////////////////
//
// Thread wrapper
//
uint32_t CN9010A544::InitDeviceThread(_In_ void* arg)
{
#ifdef _DEBUG
	// Notify debugger of thread name
	static const ULONG_PTR INFO[] = { 0x1000, reinterpret_cast<ULONG_PTR>("CN9010A544::InitDeviceThread"), ULONG_PTR(-1), 0 };

	__try
	{
		RaiseException(0x406D1388, 0, _countof(INFO), INFO);
	}
#pragma warning(suppress : 6312) // EXCEPTION_CONTINUE_EXECUTION
	__except(EXCEPTION_CONTINUE_EXECUTION)
#pragma warning(suppress : 6322) // Empty _except block
	{
	}
#endif

	static_cast<CN9010A544*>(arg)->DeviceThread();
	
	return 0;
}
#endif
