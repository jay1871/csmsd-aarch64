/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#pragma once

#include "NetConnection.h"

#ifndef _MSC_VER
#ifndef DWORD
#define DWORD uint32_t
#endif
#endif

class CShfExt;

class CShfExtNet
:
	// Inheritance
	public CNetConnection<CShfExtNet>
{
public:
	// Functions
	CShfExtNet(LPCTSTR host, LPCTSTR service); // Client
	CShfExtNet(SOCKET socket, _In_ CShfExtNet*); // Dynamic connection
	~CShfExtNet(void) { Shutdown(); }

protected:
#ifdef _MSC_VER
	CStringA GetResponse(_In_ DWORD waitMsec = 5000);
#else
	std::string GetResponse(_In_ DWORD waitMsec = 5000);
#endif
	template<typename T> static bool Send(_In_ CNetConnection<CShfExtNet>* connection, _In_ const T* msg);
	bool SendShf(_In_bytecount_(size) const void* msg, size_t size, _In_ bool expectResponse = false);

private:
	// Functions
	virtual void OnClose(int errorCode); // Callback
	virtual void OnConnect(void); // Callback
	virtual void OnConnection(bool ) {return;};
	virtual void OnMessage(_In_bytecount_(size) const void* msg, size_t size); // Callback

	// Data
#ifdef _MSC_VER
	CCriticalSection m_critSect;
	CEvent m_rcvEvent;
#else
	mutable std::recursive_mutex m_critSect;
	bool m_rcvEvent;
	std::condition_variable m_rcvEventCond;
	mutable std::mutex m_rcvEventMutex;
#endif
	std::vector<char> m_rcvMsg[2];
	unsigned int m_rcvMsgIdx;  // 0 or 1
	size_t m_rcvMsgSize[2];

#ifdef DEBUG
	//Used in debugging no response.
	static size_t s_rcvGotEvent;
	static size_t s_rcvResetEvent;
	static size_t s_rcvSetEvent;
#endif // DEBUG

};


//////////////////////////////////////////////////////////////////////
//
// Send a message
//
template<typename T> bool CShfExtNet::Send(_In_ CNetConnection<CShfExtNet>* connection, _In_ const T* msg)
{
	return(CNetConnection<CShfExtNet>::Send(connection, msg, offsetof(T, body) + msg->hdr.bodySize));
}


