/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#include "stdafx.h"

#include "ShfExt.h"
#include "ShfExtNet.h"
#include "Log.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#ifdef DEBUG
//Static variables.
size_t CShfExtNet::s_rcvGotEvent = 0;
size_t CShfExtNet::s_rcvResetEvent = 0;
size_t CShfExtNet::s_rcvSetEvent = 0;
#endif // DEBUG

//////////////////////////////////////////////////////////////////////
//
// Constructor for client
//
CShfExtNet::CShfExtNet(LPCTSTR host, LPCTSTR service)
:
	CNetConnection<CShfExtNet>(host, service, 10000, 10000),
#ifdef _MSC_VER
	m_rcvEvent(FALSE, TRUE),
#else
	m_rcvEvent(false),
#endif
	m_rcvMsg()

{
	m_rcvMsg[0].resize(200);  // initial buffer size
	m_rcvMsg[1].resize(200);  // initial buffer size
	Startup();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Constructor for dynamic server
//
CShfExtNet::CShfExtNet(SOCKET socket, _In_ CShfExtNet*)
:
#ifdef _DEBUG
	CNetConnection<CShfExtNet>(socket)
#else
	CNetConnection<CShfExtNet>(socket, 30000, 5000)
#endif
{
	static const int NODELAY = 1;
	SetSockOpt(TCP_NODELAY, &NODELAY, sizeof(int), IPPROTO_TCP); // turn off Nagle to eliminate 200 ms latency
	Startup();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CShfExtNet::OnClose(int errorCode)
{
	if(auto peer = GetPeerAddress())
	{
#ifdef _MSC_VER
		CLog::Log(SHF_EXT_DISCONNECTED, LOG_CATEGORY_CLIENT, peer->ai_canonname);
#else
		CLog::Log(CLog::INFORMATION, "Disconnected from SHF Extension %s, code = %d", peer->ai_canonname, errorCode);
#endif
	}

	OnConnection(false);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CShfExtNet::OnConnect(void)
{
	m_rcvMsgIdx = 0;
	m_rcvMsgSize[0] = 0;
	m_rcvMsgSize[1] = 0;

	if(auto peer = GetPeerAddress())
	{
#ifdef _MSC_VER
		CLog::Log(SHF_EXT_CONNECTED, LOG_CATEGORY_CLIENT, peer->ai_canonname);
#else
		CLog::Log(CLog::INFORMATION, "Connected to SHF Extension %s", peer->ai_canonname);
#endif
	}

	OnConnection(true);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CShfExtNet::OnMessage(_In_bytecount_(size) const void* msg, size_t size)
{
	UNREFERENCED_PARAMETER(size);
	ASSERT(size == 1);
	char input = reinterpret_cast<const char*>(msg)[0];
#ifdef _MSC_VER
	CSingleLock sLock(&m_critSect, TRUE);
#else
	std::unique_lock<std::recursive_mutex> sLock(m_critSect);
#endif
	if(input != '\n')
	{
		m_rcvMsg[m_rcvMsgIdx][m_rcvMsgSize[m_rcvMsgIdx]] = input;
		m_rcvMsgSize[m_rcvMsgIdx]++;
		if(m_rcvMsgSize[m_rcvMsgIdx] >= m_rcvMsg[m_rcvMsgIdx].size())
		{
			// increase buffer size
			m_rcvMsg[m_rcvMsgIdx].resize(m_rcvMsg[m_rcvMsgIdx].size() + 200);
		}
	}
	else
	{
		m_rcvMsg[m_rcvMsgIdx][m_rcvMsgSize[m_rcvMsgIdx]] = '\0';  // replace '\n' with '\0'
		m_rcvMsgSize[m_rcvMsgIdx]++;
		if(m_rcvMsgSize[m_rcvMsgIdx] >= m_rcvMsg[m_rcvMsgIdx].size())
		{
			// increase buffer size
			m_rcvMsg[m_rcvMsgIdx].resize(m_rcvMsg[m_rcvMsgIdx].size() + 200);
		}
		// swap buffers
		m_rcvMsgIdx = m_rcvMsgIdx == 0 ? 1 : 0;
		m_rcvMsgSize[m_rcvMsgIdx] = 0;
#ifdef _MSC_VER
		sLock.Unlock();
		SetEvent(m_rcvEvent);  // used by GetResponse()
#else
		sLock.unlock();
		std::lock_guard<std::mutex> lock(m_rcvEventMutex);
		m_rcvEvent = true;
#endif

#ifdef DEBUG
		++s_rcvSetEvent;
		ASSERT(s_rcvSetEvent <= s_rcvResetEvent);
#endif // DEBUG
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset event if response is expected and send message
//
bool CShfExtNet::SendShf(_In_bytecount_(size) const void* msg, size_t size, _In_ bool expectResponse)
{
	if(expectResponse)
	{
#ifdef _MSC_VER
		CSingleLock sLock(&m_critSect, TRUE);
#else
		std::unique_lock<std::recursive_mutex> sLock(m_critSect);
#endif
		if (m_rcvMsgSize[m_rcvMsgIdx] != 0)
		{
		    m_rcvMsgSize[m_rcvMsgIdx] = 0;      //Clear the char in the buffer.
		}

#ifdef _MSC_VER
		sLock.Unlock();	//Unlock through dtor.
		ResetEvent(m_rcvEvent);  // used by GetResponse()
#else
		sLock.unlock();	//Unlock through dtor.
		std::lock_guard<std::mutex> lock(m_rcvEventMutex);
		m_rcvEvent = false;
#endif
#ifdef DEBUG
		++s_rcvResetEvent;
#endif // DEBUG
	}
	return CNetConnection<CShfExtNet>::Send(msg, size);
}


//////////////////////////////////////////////////////////////////////
/// <summary>
/// Wait for and get response message.
/// </summary>
/// <param name="waitMsec">
/// Wait-time in msec for response.
/// @note: Defaulted to 5 second if no wait value specified.
/// </param>
/// <returns>
/// String collected.
/// (returns empty string if timeout has occurred)
/// </returns>
#ifdef _MSC_VER
CStringA CShfExtNet::GetResponse(_In_ DWORD waitMsec)
#else
std::string CShfExtNet::GetResponse(_In_ DWORD waitMsec)
#endif
{
#ifdef _MSC_VER
	CStringA str = "";

	if(WAIT_OBJECT_0 == WaitForSingleObject(m_rcvEvent, waitMsec))  //timeout default to 5 second by .h.
	{
		CSingleLock lock(&m_critSect, TRUE);
#else
	std::string str = "";

	// Poll waiting for rcvEvent
	auto timeout = std::chrono::milliseconds{waitMsec};
	auto WaitRcvEvent = [this, timeout]
	{

	  std::unique_lock<std::mutex> lock(m_rcvEventMutex);
	  return m_rcvEventCond.wait_for(lock, timeout, [this] { return m_rcvEvent; });
	};

	if(WaitRcvEvent())
	{
		std::lock_guard<std::recursive_mutex> lock(m_critSect);
#endif
		str = m_rcvMsg[m_rcvMsgIdx == 0 ? 1 : 0].data();

#ifdef DEBUG
		++s_rcvGotEvent;
#endif // DEBUG

	}
	return str;
}
