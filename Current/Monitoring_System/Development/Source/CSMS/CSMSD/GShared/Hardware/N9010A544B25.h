/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013-2014 TCI International, Inc. All rights reserved		  *
**************************************************************************/

#pragma once

#include "Config.h"
#include "ShfExtNet.h"
#include "Units.h"

#ifdef _MSC_VER
#define CEShfExt CConfig
#else
#include <thread>

#define CEShfExt CConfig::EShfExt
#endif

/// <summary>
/// This class interfaces with Agilent N9010A Signal Analyzer with 25 MHz and
///  40 MHz bandwidth. It supports both 44 GHz and 26.5 GHz in 40 MHz BW.
/// </summary>
/// <remarks>
/// @note: 1. The class is renamed from CN9010A544B25 to CN9010A544 when 40MHz
///			  bandwith is supported.
/// </remarks>

class CN9010A544
:
	// Inheritance
	public CShfExtNet

{
public:

	CN9010A544(CConfig::EShfExt shfExtType = CEShfExt::N9010A_544_B25);
	~CN9010A544(void);

#ifdef _MSC_VER
	const CString & GetId(void) const { return(m_id); }
	const CString & GetOptStr(void) const { return(m_opt); }
#else
	const std::string & GetId(void) const { return(m_id); }
	const std::string & GetOptStr(void) const { return(m_opt); }
#endif

	static unsigned long long GetIfBw(void) { return(s_ifBw); }
	static unsigned long long GetIfCenterFreq(void) { return(s_ifCenterFreq); }
	static unsigned long long GetMaxFreq(void) { return(s_maxFreq); }
	static unsigned long long GetMinFreq(void) { return(RF_MIN_FREQ); }
	CConfig::EShfExtStatus GetStatus(void) const { return(m_status); }
	bool IsSpectrumInverted(_In_ Units::Frequency freq) const;
	bool Tune(_In_ Units::Frequency freq);
	void Wait(void);

private:
	// Constants
	static const unsigned long long IF_25MHZ_BANDWIDTH = 25000000;
	static const unsigned long long IF_25MHZ_CENTER_FREQ = 322500000;
	static const unsigned long long IF_40MHZ_BANDWIDTH = 40000000;
	static const unsigned long long IF_40MHZ_CENTER_FREQ = 322500000; //322.5 MHz IF in SA mode.
	static const unsigned long long RF_26GHZ_MAX_FREQ = 26500000000; //26.5 GHz Analyzer.
	static const unsigned long long RF_32GHZ_MAX_FREQ = 32000000000; //32 GHz Analyzer.
	static const unsigned long long RF_44GHZ_MAX_FREQ = 44000000000;
	static const unsigned long long RF_MIN_FREQ = 9000;
	
	// Data
#ifdef _MSC_VER
	CCriticalSection m_critSect;
	CString m_id;
	CString m_opt;
#else
	mutable std::recursive_mutex m_critSect;
	std::string m_id;
	std::string m_opt;
	std::thread m_thread;
#endif

	bool m_hardwareConnected;
	CConfig::EShfExt	m_shfExt;
	CConfig::EShfExtStatus m_status;
	static unsigned long long s_ifBw;
	static unsigned long long s_ifCenterFreq;
	static unsigned long long s_maxFreq;
	static unsigned long long s_needRespCnt;
	static unsigned long long s_respCnt;

#ifdef _MSC_VER
	static bool FindStr(_In_ const CStringA & srcStr, _In_ const CStringA & patStr)
	{
	  return (-1 != srcStr.Find(patStr));
	}
#else
	static bool FindStr(_In_ const std::string & srcStr, _In_ const std::string & patStr)
	{
	  return(srcStr.find(patStr) != std::string::npos);
	}
#endif
	void DeviceThread(void);
	virtual void OnConnection(bool connect);
#ifdef _MSC_VER
	static uint32_t InitDeviceThread(_In_ void* arg);
#endif
	//Disallow compiler auto-generated methods.
	CN9010A544(_In_ const CN9010A544 & copyFrom);
	CN9010A544 & operator=(_In_ const CN9010A544 & right);
};

//////////////////////////////////////////////////////////////////////
/// <summary>
/// Is spectrum inverted.
/// </summary>
/// <returns>
/// Always true as checked by Al.
/// </returns>
/// <remarks>
/// Note: This is just spectrum analyzer (not whole system while using SHF
///		  extension)
///		  (2612 & 2614 downconverters are inverted over whole frequency range).
/// </remarks>
///
inline bool CN9010A544::IsSpectrumInverted(_In_ Units::Frequency ) const
{

	return(true);
}
