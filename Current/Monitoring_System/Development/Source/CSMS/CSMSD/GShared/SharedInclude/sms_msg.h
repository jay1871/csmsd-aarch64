/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2005 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/
/*************************************************************************
 *************************************************************************
 **									**
 **	Project:	SMS PROJECT                        
 **									**
 **	Module:		SMS_MSG.H					
 **									**
 **	Creation Date:	1/21/98
 **									**
 **	Revision Date:	$Date: 9/28/07 11:41p $ - $Modtime: 9/26/07 8:53p $
 **									**
 *************************************************************************
 *************************************************************************

	Revision Log:
	$Log: /Venezuela_Namibia/Source/Monitoring System/Scorpio Server/Shared Include/sms_msg.h $
 * 
 * 337   9/28/07 11:41p Al
 * 
 * 336   3/05/07 6:06p Al
 * 
 * 335   8/31/06 10:06p Al
 * 
 * 334   8/16/06 8:33p Al
 * added METRICS_CTRL / GET_HEADING
 * 
 * 333   7/03/06 5:14p Al
 * 
 * 332   6/07/06 12:09p Al
 * 
 * 331   5/09/06 5:42p Al
 * 
 * 330   2/27/06 1:27p Al
 * 
 * 329   1/10/06 4:50p Al
 * 
 * 328   1/10/06 11:01a Al
 * 
 * 327   9/06/05 12:06p Stevem
 * Move client-specific stuff to the client code.  Steve M.
 * 
 * 326   8/09/05 3:25p Stevem
 * Respect the current system double-click speed (set from the Control
 * Panel 'Mouse' applet) when popping up (or down) the Popup Pan display.
 * Steve M.
 * 
 * 325   7/18/05 2:40p Stevem
 * Resize the Map display dialog.  Steve M.
 * 
 * 324   7/18/05 1:57p Stevem
 * Resize the Map display dialog in the Station Selection screen.  Steve
 * M.
 * 
 * 323   7/18/05 1:42p Stevem
 * Allow screens to be resized according to the Application's size.
 * Screens that auto-resize include:  AVD, DF Scan and Spectrum Occupancy.
 * Steve M.
 * 
 * 322   4/29/05 6:34p Al
 * 
 * 321   4/02/05 12:54a Cal
 * added new define for subtype GET_NEXT_SCANDF_MSG
 * 
 * 320   3/29/05 7:01a Cal
 * changed structure of SCHED_MSG_CAL_RESPONSE for workload
 * 
 * 319   3/29/05 5:14a Cal
 * SCHED_MSG_SCANDF_CAL_RESPONSE changed to add workload data
 * 
 * 318   3/29/05 5:00a Cal
 * SCHED_MSG_SCANDF_CAL_RESPONSE changed to allow added server workload
 * data
 * 
 * 317   3/29/05 4:27a Stevem
 * 
 * 316   3/25/05 4:55p Stevem
 * 1.  When scheduling Task Calendar, AVD, DF Scan and Occupancy
 * measurements (and BITE), pass on the client's HOSTNAME and IP Address.
 * These will be echoed back from the Server in the Server Workload
 * response.
 * Steve M.
 * 
 * 315   12/09/04 6:49p Al
 * 
 * 314   12/08/04 11:53p Al
 * 
 * 313   12/03/04 10:50p Al
 * 
 * 312   8/20/04 12:43a Stevem
 * Station names (Scorpio Servers) can be longer than 15 characters.  This
 * can easily happen if the name are 'fully qualified', containing partial
 * or full hierarchical names.  Steve M.
 * 
 * 311   7/22/04 4:59p Al
 * 
 * 310   7/21/04 10:16p Al
 * 
 * 309   7/21/04 7:04p Al
 * 
 * 308   5/12/04 5:41p Admin
 * The Pans stopped running when navigating between different screens with
 * pan displays (Monitor Receiver, Pan Control and PushbuttonDF screens).
 * The problem was that just before leaving one of these screens a PAN
 * REQUEST would go out, switches to another screen, but since there is an
 * outstanding PAN request, another request would not be sent.  In the
 * meantime, the first screen would receive the PAN DATA, but the new
 * screen was oblivious to this (so a pan request would never go out until
 * the operator forced it by pressing <start> or entering the screen
 * again).  Steve M.
 * 
 * 307   3/15/04 4:30p Stevem
 * Don't force the operator to restart Scorpio Client after changing the
 * [Auto Disconnect] ScorpioClient.ini settings.  If the timer has been
 * disabled (set Timer=0 in the .ini file) and then enabled (set Timer >
 * 0), the operator must disconnect and reconnect to the Scorpio Server in
 * order to restart the timer.  Steve M.
 * 
 * 306   2/13/04 12:34a Al
 * 
 * 305   2/10/04 5:05p Stevem
 * For the "Pan" display screens (this includes the "Pan Control",
 * "Monitor Receiver" and "Pushbutton DF/Metrics" screens).
 * 1.  Maintain the state of outstanding Pan Request (currently waiting
 * for a Pan data response or not).  Do not send another Pan request (for
 * example when the operator presses the <Run> button or enters a "Pan"
 * display screen) if still waiting for previous Pan response.
 * 2.  Don't keep tearing-down/creating the interactive socket connection
 * when leave/enter a "Pan" display screen.  Only tear-down the connection
 * when leaving a "Pan" screen and entering a non-"Pan" screen (such as
 * the Task Calendar, HomingDF, all screens that do not have pan
 * capabilities).  Only create the connection when leaving a non-"Pan"
 * screen and entering a "Pan" screen (or initially going to a "Pan"
 * screen after startup).  If moving between "Pan" screens, maintain and
 * use the connection.  Steve M.
 * 
 * 304   1/20/04 11:41 Stevem
 * 
 * 303   1/13/04 1:14a Al
 * 
 * 302   12/31/03 3:59p Cal
 * Changed Greeting message in SMS_MSG and added CLIENT_DATA which is used
 * in response to MEASUREMENT_ERROR_REPORT
 * 
 * 301   12/29/03 9:39a Rajpreet
 * 
 * 300   11/18/03 6:53p Cal
 * added status: HIGHER_PRIORITY_RUNNING
 * to indicate a client has "taken priority" and requested measurement not
 * started or completed.
 * 
 * 299   11/04/03 9:15p Al
 * 
 * 298   11/04/03 6:45p Al
 * 
 * 297   10/15/03 11:42a Rajpreet
 * Added timer for requesting TCI Audio from Dino. - RKT
 * 
 * 296   10/14/03 11:56p Al
 * 
 * 295   10/10/03 7:23p Al
 * 
 * 294   9/12/03 8:11p Al
 * 
 * 293   8/30/03 12:39a Al
 * 
 * 292   8/28/03 8:46p Al
 * 
 * 291   5/21/03 2:17p Al
 * 
 * 290   2/03/03 10:50a Al
 * 
 * 289   1/31/03 5:59p Al
 * 
 * 288   3/15/02 10:02a Sporky
 * 
 * 287   3/12/02 4:50p Sporky
 * "Server Workload" request/response interface (structures, messages,
 * constants).  Steve M.
 * 
 * 286   2/05/02 2:18p Sporky
 * 
 * 285   1/11/02 6:47p Miso
 * 
 * 284   1/03/02 1:45a Al
 * 
 * 283   1/01/02 1:28a Al
 * 
 * 282   12/27/01 3:58p Miso
 * 
 * 281   9/27/01 9:24p Sporky
 * When sending a BEARING from the Scorpio Router, do not truncate the
 * server's GPS location (lat, long) by converting from a double to a long
 * value.  Otherwise you lose accuracy.  Steve M
 * 
 * 280   6/21/01 8:56p Al
 * 
 * 279   3/19/01 3:32p Sporky
 * Added ID_HOMING_TESTDF (aid in automated HomingDF testing in Scorpio
 * Client).  Steve M.
 * 
 * 278   3/02/01 5:22p Sporky
 * Removed unused timer id (SELF_PACING_PANS_MONRX).  Steve M.
 * 
 * 277   1/18/01 6:32p Sporky
 * Online / Offline support.  Steve M.
 * 
 * 276   12/15/00 5:47p Sporky
 * 
 * 275   11/06/00 5:23p Sporky
 * Added TASKCALENDAR_DELETETASK and TASKCALENDAR_DELETETASK_INTERVAL.
 * Used by the Client when sending a Delete Measurement Request.  Steve M.
 * 
 * 274   11/03/00 12:26a Cal
 * added "no data" types for occupancy and avd
 * 
 * 273   10/13/00 12:06p Al
 * 
 * 272   10/13/00 10:00a Al
 * 
 * 271   10/13/00 1:02a Al
 * 
 * 270   9/14/00 11:51p Cal
 * to add new error codes 516 and 517 for calendar error checking and
 * server time sync errors
 * 
 * 268   8/28/00 11:00a Cal
 * 
 * 267   8/23/00 12:51a Al
 * 
 * 266   8/21/00 11:19p Al
 * 
 * 265   8/21/00 3:04p Aung
 * Added structure and message definitions for GPS information from the
 * server. - Auz
 * 
 * 264   7/20/00 3:31p Sporky
 * Scorpio Server capabilities stuff.
 * 1 . Added BAND_REQUEST_NETTED msg and it's associated structure
 * MEASURE_CTRL_MSG_BANDNETTED_REQ.
 * 2.  Added BAND_RESPONSE_NETTED msg and it's associated structure
 * getBandRespNetted.
 * 
 * Steve M..
 * 
 * 263   7/06/00 12:00a Al
 * 
 * 262   4/24/00 10:20p Sporky
 * 
 * 261   4/24/00 4:30p Cal
 * added MEASURE_CTR BAND_REQUEST and BAND_RESPONSE message subtypes for
 * com between dino and client
 * 
 * 260   4/21/00 1:25a Al
 * 
 * 259   4/21/00 12:02a Cal
 * to add new getBandCmd and getBandResp
 * 
 * 258   4/12/00 4:08p Sporky
 * Added WM_LAST_SCREEN_PROCESSING message #define.  Steve M.
 * 
 * 257   3/15/00 4:57p Cal
 * 
 * 251   3/09/00 4:25p Sporky
 * Added STATUSBAR_UPDATE_TIMER timer id.  Steve M.
 * 
 * 250   3/05/00 4:01 Sporky
 * Added a timer used by Scorpio Client's AVD feature.  Steve M.
 * 
 * 249   3/03/00 6:41p Cal
 * To add more AVD defs
 * 
 * 248   3/02/00 14:08 Sporky
 * Added some timer ids.  Steve M.
 * 
 * 247   3/02/00 1:30p Cal
 * To add AVD messages and defines
 * 
 * 246   2/11/00 5:54p Sporky
 * Added #define ID_GETPUSHBTNDFRESULTS_TIMER and #define
 * ID_PUSHBTNRESPFROMSERVER_TIMER.  Steve M
 * 
 * 245   2/08/00 2:16p Sporky
 * Added #define DFCMD_PUSHBUTTON.  Steve M.
 * 
 * 244   2/08/00 10:42a Sporky
 * Added #define SRCWINDOW_PUSHBUTTONDF.  Steve M.
 * 
 * 243   2/04/00 9:57a Al
 * 
 * 242   00/02/01 12:38p Sporky
 * Added some #defines for AVD.  Steve M.
 * 
 * 241   1/20/00 5:44p Al
 * 
 * 240   1/20/00 2:45p Al
 * 
 * 239   1/19/00 5:09p Al
 * 
 * 238   1/19/00 12:27p Al
 * 
 * 237   1/18/00 10:47p Al
 * 
 * 236   1/17/00 7:47p Al
 * 
 * 235   1/15/00 12:15a Al
 * 
 * 234   1/13/00 5:55p Al
 * 
 * 233   00/01/03 12:16p Sporky
 * Now able to build Scorpio Client (or Scorpio Router) for any project
 * (DGT, Namibia, etc...) for either 8067 (DGT, or VNC).  Steve M.
 * 
 * 232   12/14/99 4:15p Al
 * 
 * 231   12/04/99 12:17a Al
 * 
 * 230   12/02/99 12:32p Steve
 * Added autoviolation detection.
 * 
 * 229   11/18/99 12:14p Al
 * 
 * 228   11/17/99 3:22p Al
 * 
 * 227   11/16/99 5:01p Al
 * 
 * 226   11/16/99 8:36p Al
 * 
 * 225   11/15/99 2:36p Al
 * 
 * 224   11/15/99 7:54p Al
 * 
 * 223   11/04/99 4:04p Al
 * 
 * 222   11/03/99 3:34p Al
 * 
 * 221   11/01/99 6:27p Al
 * 
 * 220   10/29/99 11:40a Al
 * 
 * 218   9/10/99 10:19a Al
 * increased BWBINS_MAX
 * 
 * 217   8/17/99 11:04a William
 * Added ability to use external language dll
 * 
 * 216   7/19/99 8:06p Cal
 * change ClientID from long to unsigned long
 * 
 * 215   7/11/99 10:19p Sporky
 * Scorpio Router now has the capability of accepting DF Measurement
 * requests for a list of stations; and also the capability of accepting
 * Station Connect / Disconnect messages.  Steve M.
 * 
 * 214   7/07/99 10:05a Sporky
 * Change the 'PING NETTED STATIONS' from using a TIMER to a thread.
 * Steve M.
 * 
 * 213   7/06/99 10:46p Sporky
 * Keep 'PING'ing all connected Netted Stations, in order to maintain the
 * (ISDN) connection.  Steve M.
 * 
 * 212   6/25/99 2:37p Al
 * 
 * 211   6/24/99 8:59a Sporky
 * Attempt to increase the PAN speed.  Steve M.
 * 
 * 210   6/21/99 4:14p Al
 * 
 * 209   6/18/99 3:06p Al
 * 
 * 208   6/18/99 10:40a Al
 * 
 * 207   6/17/99 5:03p Al
 * 
 * 200   6/16/99 3:13p Al
 * 
 * 198   6/15/99 2:06a Sporky
 * 
 * 197   6/11/99 5:53p Al
 * 
 * 196   6/10/99 6:46p Sporky
 * More External Tasking work.  Steve M.
 * 
 * 195   6/10/99 1:07p Sporky
 * More External Tasking work.  Steve M.
 * 
 * 194   6/08/99 5:03p Sporky
 * External Inferface work.  Steve M.
 * 
 * 193   6/07/99 9:51p Sporky
 * More External Tasking work (external interface to Scorpio Client
 * 'Router' application).  I.E.  _PROJ_EXTERNAL_TASKING work.  Steve M.
 * 
 * 192   6/07/99 9:35p Steve
 * 
 * 191   6/07/99 7:05p Steve
 * Changed MAX_BANDWIDTHS from 10 to 12.
 * 
 * 190   6/07/99 10:38a Sporky
 * 
 * 189   6/07/99 10:36a Sporky
 * 
 * 188   6/03/99 9:42a Sporky
 * 
 * 187   6/01/99 12:33p Sporky
 * 
 * 186   5/28/99 6:31p Steve
 * Added BITE_RESPONSE and NOBITE_RESPONSE
 * 
 * 185   5/27/99 5:27p Sporky
 * Automatically request the most recent BITE results (from the 8067) when
 * Adding a station in the Station Select dialog.
 * 
 * 184   5/26/99 8:50p Steve
 * 
 * 183   5/25/99 3:03p Sporky
 * Compress/Uncompress messages between Scorpio Client and DINO.  Steve M.
 * 
 * 182   5/17/99 4:01p Cal
 * add SCANDF_NONE_DATA
 * 
 * 181   5/11/99 3:13p Al
 * 
 * 180   5/11/99 7:01p Steve
 * 
 * 179   5/11/99 1:29p Al
 * 
 * 178   4/27/99 8:38a Cal
 * 
 * 177   4/16/99 1:50p Al
 * 
 * 176   4/16/98 10:41a Sporky
 * Indicate from where a DF Request has been made (either from the Scorpio
 * Client Task Calendar, Netted DF screen, or Homing DF screen)
 * 
 * 175   4/14/99 2:05a Steve
 * 
 * 174   4/09/99 4:56p Sporky
 * Increase max number of netted df stations from 6 to 12
 * 
 * 173   4/09/99 2:41p Sporky
 * Indicate if the measurement request is for Training Mode or not (see
 * 'isTraining' flag)
 * 
 * 172   4/08/99 3:28p Jonathon
 * added audio switch messages to be used by client and DINO.
 * 
 * 171   4/05/99 6:45p Steve
 * 
 * 170   4/01/99 7:31p Steve
 * 
 * 169   3/31/99 6:12p Cal
 * 
 * 168   3/31/99 5:39p Cal
 * to add callsign to meas client options
 * 
 * 167   3/31/99 4:21p Sporky
 * Add automatic BITE when you Add the measurement station (in the Station
 * Select dialog)
 * 
 * 166   3/29/99 11:07a Steve
 * 
 * 165   3/28/99 3:48p Steve
 * 
 * 164   3/28/99 2:54p Steve
 * 
 * 163   3/25/99 12:29p Steve
 * 
 * 162   3/23/99 2:45p Steve
 * Add DSP initialize.
 * 
 * 161   3/22/99 4:51p Steve
 * Added DSP_CTRL and DSP_INITIALIZE.
 * 
 * 160   3/20/99 2:52p Steve
 * Add OCCUPANCYDF_CTRL for scanning DF.
 * 
 * 159   3/18/99 7:01p Jonathon
 * changed "notstarted" to "unknown" in OccupancyCtrlstate enum type
 * 
 * 158   3/18/99 1:19p Cal
 * 
 * 157   3/17/99 12:45p Cal
 * 
 * 156   3/15/99 1:13p Cal
 * resume, suspend,etc SCANDF
 * 
 * 155   3/15/99 12:44p Jonathon
 * added defines for ScanDF
 * 
 * 154   3/15/99 7:15a Cal
 * add MEASURE_SCAN_FREQ_RESULTS_RESPONSE
 * 
 * 153   3/12/99 10:02a Cal
 * Add resume,suspend,etc subtypes for ScanDF
 * 
 * 152   3/12/99 9:50a Sporky
 * Netted DF
 * 
 * 151   3/10/99 2:37p Sporky
 * 
 * 150   3/09/99 6:48p Jonathon
 * Un-comment line in the body of structure SCHED_MSG_SCANDF_REQUEST
 * 
 * 149   3/10/99 9:08a Cal
 * SCHED_SCAN_RESPONSE unique
 * 
 * 148   3/08/99 5:02p Cal
 * changes for scandf
 * 
 * 147   3/03/99 11:04a Al
 * 
 * 146   2/18/99 5:34p Al
 * 
 * 145   2/08/99 10:06a Steve
 * Added scan_ctrl and its error codes.
 * 
 * 144   2/03/99 2:47p Sporky
 * When there is an error returned (from measurment service) when
 * RETRIEVING DATA, SCHEDULING A MEASUREMENT, OR DELETING A MEASUREMENT,
 * display an (English) string, instead of just the error value.  Steve M.
 * 
 * 143   2/01/99 11:13a Sporky
 * 
 * 142   1/26/99 1:55p Steve
 * 
 * 141   1/14/99 12:19p Steve
 * 
 * 140   1/07/99 8:37p Steve
 * Add validate measurement message.
 * 
 * 139   1/04/99 9:48p Sporky
 * Fixed bug #122.  Allow the user to adjust the rate at which PAN data is
 * requested (for Waterfall and Spectrogram displays only).  Fine-tuning
 * this value will avoid the 100% CPU usage condition.  Steve M.  For
 * build 1.0.43
 * 
 * 138   12/03/98 3:41p Al
 * 
 * 137   12/03/98 3:33p Al
 * 
 * 136   11/20/98 10:47a Cal
 * add update time to occupancy state response.
 * 
 * 135   11/16/98 5:03p Steve
 * 
 * 134   11/12/98 12:08p Cal
 * add occupancy state response for Arnold
 * 
 * 133   11/12/98 10:16a Steve
 * 
 * 133   11/11/98 4:12p Sporky
 * 
 * 131   11/10/98 6:11p Sporky
 * Fixed bugs 10, 12, 13 + other miscellaneous fixes, Steve M.  For build
 * 1.0.31
 * 
 * 130   11/02/98 15:48 Sporky
 * 
 * 129   10/29/98 11:56a Cal
 * 
 * 128   10/28/98 12:08 Sporky
 * Added ID_RESPONSEFROMSERVER_TIMER, used by HomingDF
 * 
 * 127   10/28/98 9:39 Sporky
 * put #define ID_GETDFRESULTS_TIMER back in
 * 
 * 126   10/27/98 9:47 Sporky
 * 
 * 125   10/26/98 3:49p Al
 * 
 * 124   10/26/98 3:20p Steve
 * 
 * 123   10/26/98 2:54p Al
 * 
 * 122   10/23/98 2:40p Steve
 * 
 * 121   10/22/98 3:03p Steve
 * 
 * 120   10/19/98 12:19p Steve
 * 
 * 119   10/16/98 4:27p Steve
 * 
 * 118   10/16/98 3:12p Cal
 * 
 * 117   10/15/98 5:38p Steve
 * 
 * 116   10/13/98 6:45p Steve
 * 
 * 115   10/12/98 1:27p Cal
 * 
 * 114   10/12/98 1:00p Steve
 * 
 * 113   10/08/98 6:27p Steve
 * 
 * 112   10/07/98 11:54a Cal
 * 
 * 111   10/05/98 10:03a Cal
 * 
 * 110   10/05/98 7:43a Cal
 * 
 * 109   10/05/98 7:36a Cal
 * 
 * 108   10/02/98 5:37p Cal
 * 
 * 107   10/02/98 10:44a Cal
 * 
 * 106   10/02/98 9:36a Cal
 * 
 * 105   10/02/98 9:17a Cal
 * 
 * 104   10/02/98 8:58a Cal
 * 
 * 103   9/30/98 17:24 Jonathon
 * 
 * 102   9/30/98 2:06p Cal
 * Add Occupancy header structure
 * 
 * 101   9/28/98 11:22a Steve
 * 
 * 100   9/24/98 4:44p Al
 * 
 * 99    9/24/98 4:01p Cal
 * 
 * 98    9/24/98 9:48a Cal
 * 
 * 97    9/23/98 4:12p Steve
 * 
 * 96    9/23/98 1:55p Cal
 * 
 * 95    9/23/98 11:45a Cal
 * occupancy options added as header
 * 
 * 94    9/22/98 19:10 Jonathon
 * 
 * 93    9/22/98 2:36p Steve
 * 
 * 92    9/21/98 2:44p Cal
 * add sched Occupancy msg
 * 
 * 91    9/21/98 2:33p Steve
 * 
 * 90    9/16/98 3:31p Steve
 * 
 * 89    9/16/98 12:43p Steve
 * 
 * 88    9/16/98 11:38a Steve
 * 
 * 87    9/14/98 5:40p Steve
 * 
 * 86    9/04/98 4:26p Al
 * 
 * 85    9/03/98 10:43a Cal
 * 
 * 84    9/03/98 7:37a Cal
 * modify bite request for results command
 * 
 * 83    9/02/98 1:21p Cal
 * add definitions for BITE
 * 
 * 82    8/28/98 1:26p Cal
 * change state_msg and add COLLIDE code.
 * 
 * 81    8/27/98 5:46p Al
 * 
 * 80    8/26/98 2:47p Al
 * 
 * 79    8/25/98 5:16p Sporky
 * Added #define SRCWINDOW_FIX
 * 
 * 78    8/21/98 3:28p Steve
 * 
 * 77    8/20/98 11:47a Cal
 * Add BITE command and response messages.
 * 
 * 76    8/19/98 4:19p Steve
 * Add BITE messages.
 * 
 * 75    8/19/98 9:29a Sporky
 * Added #defines for some Windows Timer IDs.  Also #defines to identify
 * the source (window) of a DINO service request message.
 * 
 * 74    8/14/98 7:49a Cal
 * 
 * 73    8/10/98 8:18p Al
 * 
 * 72    8/10/98 3:07p Steve
 * 
 * 71    8/10/98 2:49p Al
 * 
 * 70    8/07/98 8:16a Cal
 * 
 * 69    8/07/98 7:47a Cal
 * 
 * 68    8/06/98 3:40p Al
 * 
 * 67    8/06/98 4:32p Al
 * 
 * 66    8/06/98 2:07p Al
 * 
 * 65    8/06/98 11:00a Cal
 * 
 * 64    8/06/98 7:51a Cal
 * add msgs for state request/response.
 * 
 * 63    8/05/98 7:35a Cal
 * 
 * 62    8/05/98 2:39p Al
 * 
 * 61    8/04/98 4:37p Steve
 * 
 * 60    8/03/98 6:38p Steve
 * 
 * 59    7/31/98 8:09a Cal
 * 
 * 58    7/30/98 12:29p Cal
 * 
 * 57    7/29/98 7:29p Cal
 * 
 * 56    7/29/98 5:12p Bill
 * Modified to have "task completed" error code
 * 
 * 55    7/29/98 4:43p Cal
 * 
 * 54    7/29/98 4:16p Cal
 * 
 * 53    7/29/98 1:48p Cal
 * 
 * 52    7/28/98 4:30p Steve
 * 
 * 51    7/23/98 3:56p Steve
 * 
 * 50    7/01/98 12:23p Dat
 * back to rev 48
 * 
 * 48    6/25/98 5:42p Al
 * 
 * 47    6/25/98 5:37p Al
 * 
 * 46    6/18/98 10:07a Al
 * 
 * 45    6/09/98 5:16p Cal
 * Merge Dino and Equipcontrol
 * 
 * 44    6/08/98 3:22p Steve
 * 
 * 43    6/05/98 5:11p Steve
 * 
 * 42    6/04/98 6:03p Miso
 * 
 * 41    6/04/98 5:09p Steve
 * 
 * 40    6/04/98 3:06p Steve
 * 
 * 39    6/04/98 2:31p Steve
 * 
 * 38    6/04/98 10:49a Steve
 * 
 * 37    6/03/98 6:17p Steve
 * 
 * 36    6/01/98 9:34a Steve
 * 
 * 35    5/08/98 5:08p Steve
 * 
 * 34    5/06/98 12:43p Steve
 * 
 * 33    5/06/98 10:55a Al
 * 
 * 32    5/01/98 6:49p Al
 * 
 * 31    5/01/98 3:09p Al
 * 
 * 30    4/21/98 12:55p Al
 * 
 * 29    4/16/98 4:33p Steve
 * 
 * 28    4/13/98 7:23p Al
 * 
 * 27    4/13/98 5:50p Al
 * 
 * 26    4/13/98 5:27p Al
 * 
 * 25    4/13/98 5:04p Steve
 * 
 * 24    4/13/98 2:29p Al
 * 
 * 23    4/09/98 10:06a Steve
 * 
 * 22    4/03/98 11:24a Steve
 * 
 * 21    4/02/98 6:44p Steve
 * 
 * 20    4/02/98 12:26p Miso
 * 
 * 19    4/02/98 12:08p Miso
 * 
 * 18    4/01/98 5:39p Miso
 * 
 * 17    4/01/98 3:57p Miso
 * 
 * 16    3/31/98 3:30p Miso
 * 
 * 15    3/30/98 3:08p Miso
 * 
 * 14    3/27/98 1:07a Al
 * 
 * 13    3/18/98 4:12p Steve
 * 
 * 12    3/16/98 3:36p Steve
 * 
 * 11    3/13/98 4:41p Steve
 * 
 * 10    3/11/98 3:36p Steve
 * 
 * 9     3/05/98 5:21p Steve
 * 
 * 8     3/05/98 3:50p Steve
 * 
 * 7     3/03/98 5:09p Steve
 * 
 * 6     2/28/98 4:07p Steve
 * 
 * 5     2/27/98 4:32p Steve

	Description:
	   This module contains the definitions for all SMS messages.

	Public Routines:		None

	Public Data Structures:		None

   $NoKeywords: $

*/


#ifndef SMS_MSG_H
#define SMS_MSG_H
//#include <afxdisp.h>

#ifndef __cplusplus
   typedef int bool; 
#endif

#include "msgtypes.h"
#include "errmsgcodes.h"
#include "dsp_msg.h"
#include "sndrmsg.h"

#define	MAX_STATIONNAME_LEN		(128)

#define	WM_NETMESSAGE			(6701)



// message subtypes for message type MEASURE_CTRL
#define	SCHED_REQUEST			71000L
#define SCHED_RESPONSE			72000L
#define	DELETE_REQUEST			71001L
#define DELETE_RESPONSE			72001L
#define	MEASUREMENT_REQUEST		71002L
#define MEASUREMENT_RESPONSE	72002L
#define	DWELL_REQUEST			71003L
#define DWELL_RESPONSE			72003L	
#define STATE_REQUEST			72004L
#define STATE_RESPONSE			73004L
#define	BITE_REQUEST			72005L
#define BITE_SCHED_RESPONSE		73005L
#define SCHED_OCCUP_REQUEST		71006L
#define SCHED_OCCUP_RESPONSE	73006L
#define SCHED_AVD_REQUEST		71015L
#define SCHED_AVD_RESPONSE		73015L
#define BAND_REQUEST			71016L
#define BAND_RESPONSE			73016L
#define BAND_REQUEST_NETTED		71017L
#define BAND_RESPONSE_NETTED	73017L
#define OCCUP_MEASUREMENT_REQUEST  71007L
#define AVD_MEASUREMENT_REQUEST  71008L
#define OCC_FREQ_MEASURE		72046L
#define OCC_CHANNEL_MEASURE		72047L
#define EFLD_CHANNEL_MEASURE	72048L
#define OCC_TIMEOFDAY_MEASURE	72049L
#define MSGLEN_CHANNEL_MEASURE	72050L
#define OCC_SPECGRAM_MEASURE	72051L
#define OCCUP_SUSPEND_REQUEST	 71008L // from client
#define OCCUP_RESUME_REQUEST	 71009L // from client
#define OCCUP_DELETE_REQUEST	 71010L // from client
#define OCCUP_TERMINATE_REQUEST	 71011L // from client
#define OCCUP_STATE_REQUEST		 71012L // from client
#define OCCUP_SUSPEND_RESPONSE	 72008L // from dino
#define OCCUP_RESUME_RESPONSE	 72009L // from dino
#define OCCUP_DELETE_RESPONSE	 72010L // from dino
#define OCCUP_TERMINATE_RESPONSE 72011L // from dino
#define OCCUP_STATE_RESPONSE	 72012L // from dino
#define OCCUP_SUSPEND_CAL_RESPONSE	 73008L	// from calendar to dino
#define OCCUP_RESUME_CAL_RESPONSE	 73009L	// from calendar to dino
#define OCCUP_DELETE_CAL_RESPONSE	 73010L	// from calendar to dino
#define OCCUP_TERMINATE_CAL_RESPONSE 73011L // from calendar to dino 
#define OCCUP_STATE_CAL_RESPONSE 73012L // from calendar to dino 
#define OCCUP_STATE_CAL_UPDATE	 72013L	// from dino to calendar (no response)

#define AVD_SUSPEND_REQUEST	 78008L // from client
#define AVD_RESUME_REQUEST	 78009L // from client
#define AVD_DELETE_REQUEST	 78010L // from client
#define AVD_TERMINATE_REQUEST	 78011L // from client
#define AVD_STATE_REQUEST		 78012L // from client
#define AVD_SUSPEND_RESPONSE	 72008L // from dino
#define AVD_RESUME_RESPONSE	 79009L // from dino
#define AVD_DELETE_RESPONSE	 79010L // from dino
#define AVD_TERMINATE_RESPONSE 79011L // from dino
#define AVD_STATE_RESPONSE	 79012L // from dino
#define AVD_SUSPEND_CAL_RESPONSE	 79108L	// from calendar to dino
#define AVD_RESUME_CAL_RESPONSE	 79109L	// from calendar to dino
#define AVD_DELETE_CAL_RESPONSE	 79110L	// from calendar to dino
#define AVD_TERMINATE_CAL_RESPONSE 79111L // from calendar to dino 
#define AVD_STATE_CAL_RESPONSE 79112L // from calendar to dino 
#define AVD_STATE_CAL_UPDATE	 79113L	// from dino to calendar (no response)

#define SCANDF_FREQ_MEASURE		75046L
#define SCANDF_DATA_MEASURE		75047L
#define SCANDF_DATA_NONE		75048L

#define SCHED_SCANDF_REQUEST	74006L
#define SCANDF_MEASUREMENT_REQUEST  74007L
#define SCHED_SCANDF_RESPONSE	74014L
#define SCANDF_SUSPEND_REQUEST	 74008L // from client
#define SCANDF_RESUME_REQUEST	 74009L // from client
#define SCANDF_DELETE_REQUEST	 74010L // from client
#define SCANDF_TERMINATE_REQUEST	 74011L // from client
#define SCANDF_STATE_REQUEST		 74012L // from client
#define SCANDF_SUSPEND_RESPONSE	 75008L // from dino
#define SCANDF_RESUME_RESPONSE	 75009L // from dino
#define SCANDF_DELETE_RESPONSE	 75010L // from dino
#define SCANDF_TERMINATE_RESPONSE 75011L // from dino
#define SCANDF_SUSPEND_CAL_RESPONSE	 76008L	// from calendar to dino
#define SCANDF_RESUME_CAL_RESPONSE	 76009L	// from calendar to dino
#define SCANDF_DELETE_CAL_RESPONSE	 76010L	// from calendar to dino
#define SCANDF_TERMINATE_CAL_RESPONSE 76011L // from calendar to dino 
#define SCANDF_STATE_CAL_RESPONSE 76012L // from calendar to dino 
#define SCANDF_STATE_RESPONSE	 76012L // from dino to client 
#define SCANDF_STATE_CAL_UPDATE	 76013L	// from dino to calendar (no response)

// To request a list of all running/suspended/scheduled tasks
#define SERVER_WORKLOAD_REQUEST_MSG		71100L	// from client to dino
#define SERVER_WORKLOAD_RESPONSE_MSG	72101L	// from dino to client

// Auz 8/21/00
// This is a new message required to get server GPS location from Dino.
#define SERVER_LOCATION_REQUEST		74000L
#define SERVER_LOCATION_RESPONSE	74001L

// measurement request failure codes
#define REQUEST_TIME_OVERLAP	500L	// overlap with scheduled event.
#define	MEASURE_REQUEST_ERROR	501L	// incorrect request format or values
#define REQUEST_INVALID_TIME	502L	// time(s) in invalid format
#define	REQUEST_INVALID_TIMESPAN 503L	// timespan exceeds limits (min or max)
#define	REQUEST_PRIORITY_FAIL	504L	// timespan scheduled by higher priority request
#define	MEASURE_ALLOC_FAIL		505L	//  no allocation equipment not available
#define REQUEST_FAIL_DATABASE	506L	//	unable to open/append to db
#define	REQUEST_UNKNOWN			507L	//	unknown failure
#define	REQUEST_FAIL_DELETE		508L	//	unable to delete requested record.	
#define REQUEST_FAIL_NOT_FOUND	509L	//	ID of record not found in db.
#define	MEASURE_REQUEST_PAST_TIME 510L	//	requested time in the past.
#define MEASURE_DATA_NOT_STORED 511L	//	results not stored in database, fail on store.
#define MEASURE_NOT_ONTIME_FINI 512L	//	the equip did not complete measurement by time scheduled. 
#define REQUEST_FAIL_NETWORK	513L	//	the request fail due to network not enabled.
#define REQUEST_COLLIDE			514L	//  overlap with concurrent request for same time slot.(try again)
#define MEASURE_CANCELLED		515L	//	measurement cancelled.
#define REQUEST_FAIL_CALENDAR	516L	//	Calendar service not running
#define	SERVER_NOT_SYNC			517L	//	Server not syncd with client by over one minute.
#define	SERVER_FAIL_WORKLOAD	518L	//	Server Workload cannot be determined
#define HIGHER_PRIORITY_RUNNING 519L	//  A Client has 'Taken Priority" unable to retrieve measurement.


//  measurement request success codes
#define	TASK_COMPLETED			600L	//  request completed successfully, no errors.
#define	MEASURE_SCHEDULED		601L	//	measurement req scheduled OK.			
#define	MEASURE_ALLOC_PARTIAL	602L	//	request filled for one or more requests
#define MEASURE_TIME_ADJUST		603L	//	timespan adjusted for one or more requested timespans
#define	MEASURE_TIME_REDUCT		604L	//  timespan reduced for one or more requested timespan
#define	MEASURE_IN_PROGRESS		605L	//	request for results which are not complete
#define	CALENDAR_FREE			606L	//  calendar is free for request


typedef enum
{
	immediate,
	convenient,
	rigid
}MetricsCtrlCalType;

// message formats

// METRICS_CTRL/GET_GPS message has no body
// METRICS_CTRL/GET_GPS_RESPONSE message body uses getGPSResp
typedef struct
{
	long			status;
	double			dateTime;	// using COleTime format
	double			latitude;	// degrees (+ North, - South)
	double			longitude;	// degrees (+ East, - West)
} GPS_RESPONSE;

typedef struct
{
	long			status;
	GPS_RESPONSE	GPSResponse;
} getGPSResp;

// Auz 8/21/00
// This structure is required to get GPS information back from Dino.
// This structure will include the server id.
typedef struct
{
	long			status;
	double			dateTime;	// using COleTime format
	double			latitude;	// degrees (+ North, - South)
	double			longitude;	// degrees (+ East, - West)
	char			szStationName[MAX_STATIONNAME_LEN];
} SERVER_GPS_RESPONSE;

// METRICS_CTRL/GET_HEADING message has no body
// METRICS_CTRL/GET_HEADING_RESPONSE message body uses getHeadingResp
typedef struct
{
	long			status;
	long			magDeclination;		// (1/100 degree -> -18000 to +18000)
	bool			hfFluxgateUsed;		// true = fluxgate present & used; false = fluxgate not present or failed
	unsigned long	hfRawMagHeading;	// antenna orientation wrt magnetic north prior to mast up/down correction (1/100 degree -> 0 to 35999)
	unsigned long	hfMagHeading;		// antenna orientation wrt magnetic north (1/100 degree -> 0 to 35999)
	unsigned long	hfTrueHeading;		// antenna orientation wrt true north (1/100 degree -> 0 to 35999)
	bool			vhfFluxgateUsed;	// true = fluxgate present & used; false = fluxgate not present or failed
	unsigned long	vhfRawMagHeading;	// antenna orientation wrt magnetic north prior to mast up/down correction (1/100 degree -> 0 to 35999)
	unsigned long	vhfMagHeading;		// antenna orientation wrt magnetic north (1/100 degree -> 0 to 35999)
	unsigned long	vhfTrueHeading;		// antenna orientation wrt true north (1/100 degree -> 0 to 35999)
} getHeadingResp;

// METRICS_CTRL message formats

// message subtypes for message type METRICS_CTRL
#define GET_MEAS				1L
#define GET_DWELL				2L
#define GET_DWELL_RESPONSE		3L
#define VALIDATE_MEAS			4L
#define INIT_FIELDSTRENGTH		5L
#define GET_BAND_REQUEST		6L
#define GET_GPS                 7L
#define GET_HEADING             8L
#define GET_BAND_RESPONSE		65536L
#define GET_MEAS_RESPONSE		65537L
#define VALIDATE_MEAS_RESPONSE	65538L
#define INIT_FIELDSTRENGTH_RESP 65539L
#define GET_GPS_RESPONSE        65540L
#define GET_HEADING_RESPONSE    65541L
#define GET_CAL_RESP			70000L
#define	DELETE_CAL_MEAS			70001L
#define RESULTS_CAL_MEAS		70002L
#define CAL_SCHED				70003L
#define CAL_DEL					70004L
#define CAL_MEAS				70005L
#define CAL_RESULTS				70006L
#define CAL_STATE				70007L
#define	GET_NEXT_ID				70008L
#define GET_NEXT_MSG			70009L
#define GET_NEXT_OCCUP_MSG		70010L
#define GET_NEXT_SCANDF_MSG		70011L

// message subtypes for message type DEMOD_CTRL
#define SET_RCVR				1L
#define AGC_ONOFF				2L
#define SET_RCVR_MAN			3L
#define SET_LAN_AUDIO			4L
#define SET_RCVR_RESP			65537L
#define AGC_ONOFF_RESP			65538L
#define SET_RCVR_MAN_RESP		65539L
#define SET_LAN_AUDIO_RESP		65540L

// message subtypes for message type PAN_DISP_CTRL
#define GET_PAN					1L
#define GET_PAN_AGC				2L
#define GET_PAN_RESPONSE		65537L

// message subtypes for message type ANT_CTRL
#define GET_ANT					1L
#define SET_ANT					2L
#define GET_ANT_RESPONSE		65537L
#define SET_ANT_RESPONSE		65538L

// message subtypes for message type WB_DIGITIZER_SAMPLE
#define SNGL_SAMPLE				1L
#define SNGL_SAMPLE_RESP		65537L

// message subtypes for message type SYSTEM_STATE_CTRL
// System state is either all measurements suspeneded or normal

#define	SYSTEM_STATE_NORMAL				0L
#define	SYSTEM_STATE_SUSPENDED			1L

#define SET_SYSTEM_STATE				1L
#define SET_SYSTEM_STATE_RESPONSE		65537L

// message subtypes for message type DF_CTRL
#define PASSBAND_CAL                            1L
#define INITIALIZE_DF							2L
#define DF_CMD                                  6L
#define LOAD_IONOGRAM							7L
#define GET_IONOGRAM							8L
#define PASSBAND_CAL_RESPONSE               65537L
#define INITIALIZE_DF_RESPONSE				65538L
#define DF_RESPONSE                         65542L
#define LOAD_IONOGRAM_RESPONSE				65543L
#define GET_IONOGRAM_RESPONSE				65544L

// message subtypes for message type STATUS_CTRL
#define PING_SERVER								1L
//snm - may not even need this!
#define AUDIOTASKSTATUS							2L	// uses STATUS_CTRL_AUDIOTASKSTATUS_REQUEST 
//snm - may not even need this!
#define AUDIOTASKSTATUS_RESPONSE			65537L	// uses STATUS_CTRL_AUDIOTASKSTATUS_RESPONSE
#define GET_MSG_VERSIONS						4L
#define GET_MSG_VERSIONS_RESP				65536L
// SHUTDOWN message format

// message subtypes for message type SHUTDOWN
#define DO_SHUTDOWN			1L

#define OUTPUT_NONE			0L
#define OUTPUT_DETAILS2		3L
#define ILLEGAL_BAND		0L

typedef struct
{
	long status;
} InitFieldStrengthResp;

typedef struct
{
	unsigned long taskID;
	long band;
	unsigned long frequency;
	unsigned long rxFrequency;
	unsigned long rxBandwidth;
	unsigned long sampleRate;
	unsigned long invertSpectrum;
	unsigned long decimation;
	unsigned long ddcFirBW;
	unsigned long bandwidth;
} CONTROL_DATA;

typedef struct
{
	unsigned long sampleSize;
	unsigned long numBins;
	unsigned long binSize;
} SAMPLE_DATA;

typedef struct 
{
	unsigned long dwellTime;
	unsigned long betaParam;
	unsigned long yParam;
	unsigned long x1Param;
	unsigned long x2Param;
	unsigned long repeatCount;
	unsigned long aveMethod;
	unsigned long outputType;
} getBWCmd;

#define IFM_FREQ			1L
#define FFT_FREQ			2L
typedef struct 
{
	unsigned long freqMethod;
	unsigned long dwellTime;
	unsigned long repeatCount;
	unsigned long aveMethod;
	unsigned long outputType;
} getFreqCmd;

typedef struct
{
	unsigned long dwellTime;
	unsigned long repeatCount;
	unsigned long aveMethod;
	unsigned long outputType;
} getModulationCmd;

typedef struct
{
	unsigned long fieldMethod;
	unsigned long dwellTime;
	unsigned long repeatCount;
	unsigned long aveMethod;
	unsigned long outputType;
} getFieldStrengthCmd;

typedef struct
{
	unsigned long dwellTime;		// maximum DF time for the measurement
	unsigned long repeatCount;		// number of cuts
	unsigned long confThreshold;	// expressed as whole percent
	unsigned long DFBandwidth;		// in Hz
	unsigned long outputType;		// 1=summary;2=w/cuts;3=w/volts
	unsigned long srcOfRequest;		// 0=TaskCalendar, 1=NettedDF, 2=HomingDF
} getDFCmd;

// valid getDFCmd.srcOfRequest values (all indicate the source of the request from Scorpio Client)
#define DFCMD_TASKCALENDAR  0L	// Requesting a DF Measurement from the Task Calendar
#define DFCMD_NETTED        1L	// Requesting a DF Measurement from the Netted DF screen
#define DFCMD_HOMING        2L	// Requesting a DF Measurement from the Homing DF screen
#define DFCMD_PUSHBUTTON    3L	// Requesting a DF Measurement from the Pushbutton DF screen

typedef struct 
{
	unsigned long freq;
	unsigned long bandwidth;
	unsigned long ant;  // ANT1 = 1 = ref; ANT2 = 2 = aux
	getBWCmd BWCmd;
	getFreqCmd FreqCmd;
	getModulationCmd ModulationCmd;
	getFieldStrengthCmd FieldStrengthCmd;
	getDFCmd DFCmd;
} getMeasCmd;

// Dwell Command message body
typedef struct 
{
	unsigned long freq;
	unsigned long bandwidth;
} getDwellCmd;

// Bandwidth Dwell Response message body
typedef struct
{
	long			status;
	unsigned long	BWdwellTime;
	unsigned long	FreqdwellTime;
	unsigned long	ModulationdwellTime;
	unsigned long	FieldStrengthdwellTime;
	unsigned long	DFdwellTime;
} getDwellResp;

// Band Command message body
// zero length message

// Band Response message body
#define MAX_BANDWIDTHS 23
typedef struct
{
	unsigned long	numBandwidths;
	unsigned long	bandwidth[MAX_BANDWIDTHS];  // bandwidth in Hz
} BANDWIDTHLIST;

typedef struct
{
	unsigned long	numBandwidths;
	unsigned long	frequency[MAX_BANDWIDTHS];			// dwell time in ms
	unsigned long	bandwidth[MAX_BANDWIDTHS];			// dwell time in ms
	unsigned long	modulation[MAX_BANDWIDTHS];			// dwell time in ms
	unsigned long	fieldstrength[MAX_BANDWIDTHS];		// dwell time in ms
	unsigned long	directionfinding[MAX_BANDWIDTHS];	// dwell time in ms
} DWELLTIMELIST;

typedef struct
{
	long			status;
	unsigned char	HFmetricHardwarePresent;
	unsigned char	HFdfHardwarePresent;
	unsigned char	VUHFmetricHardwarePresent;
	unsigned char	VUHFdfHardwarePresent;
	unsigned long	HFmetricLowFreq;
	unsigned long	HFmetricHighFreq;
	unsigned long	HFdfLowFreq;
	unsigned long	HFdfHighFreq;
	unsigned long	VUHFmetricLowFreq;
	unsigned long	VUHFmetricHighFreq;
	unsigned long	VUHFdfLowFreq;
	unsigned long	VUHFdfHighFreq;
	BANDWIDTHLIST	HFmeasurementBW;
	BANDWIDTHLIST	HFpandisplayBW;
	BANDWIDTHLIST	HFoccupancyBW;
	BANDWIDTHLIST	HFdfScanBW;
	BANDWIDTHLIST	HFreceiverBW;
	BANDWIDTHLIST	VUHFmeasurementBW;
	BANDWIDTHLIST	VUHFpandisplayBW;
	BANDWIDTHLIST	VUHFoccupancyBW;
	BANDWIDTHLIST	VUHFdfScanBW;
	BANDWIDTHLIST	VUHFreceiverBW;
	DWELLTIMELIST	HFmeasurementDwell;
	DWELLTIMELIST	VUHFmeasurementDwell;
} getBandResp;

// Band Response message body
typedef struct
{
	getBandResp		bandResp;
	char			szStationName[MAX_STATIONNAME_LEN];
} getBandRespNetted;

// Validate Measurement Response message body
typedef struct
{
	long			status;
	unsigned long	BWdwellTime;
	unsigned long	FreqdwellTime;
	unsigned long	ModulationdwellTime;
	unsigned long	FieldStrengthdwellTime;
	unsigned long	DFdwellTime;
	unsigned long	totalTime;
} validateMeasurementResp;

#define BWBIN_OFFSET 100

// Receiver Control Command message body
//  SET_RCVR subtype
typedef struct
{
	unsigned long	freq;			// frequency in Hertz
	unsigned long	bandwidth;		// bandwidth in Hertz
	unsigned long	detMode;		// AM, CW, FM, USB, or LSB
	unsigned long	AGCTime;		// AGC attack time in ms
	long			BFO;			// BFO in Hertz (+/- 8000)
} RcvrCtrlCmd;

// Receiver Control Response
//  SET_RCVR_RESP subtype
typedef struct
{
	long			status;
} RcvrCtrlResp;

// Receiver Control Command message body
//  AGC_ONOFF subtype
typedef struct
{
	unsigned long	onOff;	// use AGCOFF or AGCON (defined in DSP_MSG.H)
} AGCOnOffCmd;

// Receiver Control Response
//  AGC_ONOFF_RESP subtype
typedef struct
{
	long			status;
} AGCOnOffResp;

// Receiver Control Command message body
//  SET_RCVR_MAN subtype
// NOTE: use AGC_ONOFF prior to this command
typedef struct
{
	unsigned long	freq;			// frequency in Hertz
	unsigned long	bandwidth;		// rcvr bandwidth in Hertz
	unsigned long	atten;			// rcvr attenuation
} RcvrManCmd;

// Receiver Control Response
//  SET_RCVR_MAN_RESP subtype
typedef struct
{
	long			status;
} RcvrManResp;

// Receiver Control Command message body
//  SET_LAN_AUDIO subtype
#define AUDIO_OFF 0L
#define AUDIO_ON  1L
typedef struct
{
	unsigned long	cmd;			// use above defines (may be others in future)
	unsigned long	port;			// port ID
	unsigned long	timeout;		// seconds -- audio stays on this long
//	long			messageKey;
	char			IPAddr[20];		// IP address -> 123.123.123.123
} RcvrLANCmd;

// Receiver Control Response
//  SET_LAN_AUDIO_RESP subtype
typedef struct
{
	long			status;
} RcvrLANResp;

// Antenna Status Control Command has NO message body
//  GET_ANT subtype

// Antenna Status Control Response
//  GET_ANT_RESPONSE subtype
typedef struct
{
	long			status;
	unsigned long	antenna;			// Antenna choice
} AntGetCtrlResp;

// Antenna Set Control Command message body
//  SET_ANT subtype
typedef struct
{
	unsigned long	antenna;			// Antenna choice
} AntSetCtrlCmd;
// use INVALID_ANT, ANT1, or ANT2 defined in dsp_msg.h

// Antenna Control Response
//  SET_ANT_RESP subtype
typedef struct
{
	long			status;
	unsigned long	antenna;			// Antenna state
} AntSetCtrlResp;

// WB_DIGITIZER_SAMPLE type
//  SNGL_SAMPLE subtype
//  uses DSPWBSmplCmd in dsp_msg.h (taskID not used)

// WB_DIGITIZER_SAMPLE type
//  SNGL_SAMPLE_RESP subtype
//  uses DSPWBSmplResp in dsp_msg.h (taskID not used)

// Set System State Control Command message body
//  SET_SYSTEM_STATE subtype
typedef struct
{
	unsigned long	state;			// State choice
} SystemStateSetCtrlCmd;
// use NORMAL or SUSPENDED

// Set System State Response
//  SET_SYSTEM_STATE_RESP subtype
typedef struct
{
	long			status;
	long			state ;
} SystemStateSetCtrlResp;

// Pan Display Command message body
typedef struct 
{
	unsigned long	freq;
	unsigned long	bandwidth;
	long			rcvrAtten;	// used only for GET_PAN_AGC (can be any value for GET_PAN)
								// use 0 when freq changes or bandwidth changes from <= 200000 to > 200000 or bandwidth
								// changes from > 200000 to <= 200000; else use rcvrAtten returned in last response
} getPanCmd;

// Pan Display Response message body
#define PAN_OFFSET 192
typedef struct
{
	long			status;
	double			dateTime;	// using COleTime format
	unsigned long	freq;
	long			powerDBm;
	unsigned long	binSize;
	unsigned long	numBins;
	long			rcvrAtten;
	unsigned char	binData[NUMBINS_MAX];
} getPanResp;

#include "equipmentctrl_msg.h"
#include "occupancy_msg.h"
#include "scan_msg.h"

// METRICS_CTRL/GET_MEAS_RESPONSE message body
typedef struct
{
   UINT32 azim;
   UINT32 elev;
   INT32  conf;
   UINT32 range;
   UINT32 ms;
} DF_CUT_DATA1;

typedef struct
{
	GPS_RESPONSE	GPSResponse;
	BW_RESPONSE		BWResponse;
	FREQ_RESPONSE	FreqResponse;
	MODULATION_RESPONSE	ModulationResponse;
	FIELDSTRENGTH_RESPONSE FieldStrengthResponse;
	DF_SUMMARY_RESPONSE   DFResponse;
	unsigned char	BWBinData[BWBINS_MAX];
	unsigned char	AMProbDist[NUMHISTBINS_MAX];
	unsigned char	FMProbDist[NUMHISTBINS_MAX];
	unsigned char	PMProbDist[NUMHISTBINS_MAX];
	DF_CUT_DATA1   DFCutData[DFCUTS_MAX];
	DF_VOLTAGE_DATA  DFVoltageData[10];       //limited to 1st 10 cuts !!!
} getMeasResp;

// Initialize DF Response message body
typedef struct
{
	long VHFAntCalStatus;
	long initIntStatus;
	long initWWStatus;
	long initWFAStatus;
	long initSymStatus;
	long PBCalStatus[MAX_BANDWIDTHS+7];	// vhf + 1 for HF + 4 for fast mode
} InitializeDFResp;

// BITE Command message body    (header only)

// BITE Response message body
typedef struct
{
	unsigned long	status;			// used for go/nogo
	double			startDateTime;	// using COleTime format
	double			completionDateTime;
	unsigned long	PCStatus;
	unsigned long	GPSStatus;
	unsigned long	FluxGateStatus;
	unsigned long	CV8Status;
	unsigned long	ClockSynthStatus;
	unsigned long	Access256Status;
	unsigned long	AudioCardStatus;
	unsigned long	HFDFInterfaceCardStatus;
	unsigned long	HFChassisStatus;
	unsigned long	HFDFReceiverStatus;
	unsigned long	UHFDFMuxIFStatus;
	unsigned long	HFReceiverStatus;
	unsigned long	HFDFSwitchStatus;
	unsigned long	HFAntennaStatus;
	unsigned long	VHFReceiverStatus;
	unsigned long	UHFDFSwitchStatus;
	unsigned long	UHFDFMuxRFStatus;
	unsigned long	UHFAntennaStatus;
	unsigned long	VHFDFSwitchStatus;
	unsigned long	VHFAntennaStatus;
} getBITEResp;


////////  MESSAGES FROM CLIENT TO MEASURE CONTROL ////////////////////////////////

typedef struct {
	unsigned long	clientTaskId;
	double			StartMeasureDateTime;
	unsigned long	StartMeasureTimeBar;
	unsigned long	MeasureDurSpanRequest;
	unsigned long	MeasureDurSpanMinimum;
	unsigned long	MeasurePriority;
	MetricsCtrlCalType calRequest;
	unsigned long	MeasureQuantity;
	unsigned long	MeasureQuantityDelta;
	unsigned long	ClientID;
	unsigned char	callSign[12];
	unsigned long	isTraining;		// 0 "regular" request, 1 Training Mode request
	char			szClientName[MAX_STATIONNAME_LEN];
	char			szIPAddr[16];		// IP address -> 123.123.123.123
} SCHED_MSG_OPTIONS_REQUEST;

typedef struct {
	unsigned long	MeasureID;
	unsigned long	clientTaskId;
	double			StartMeasureDateTime;
	unsigned long	ClientID;
	char			szClientName[MAX_STATIONNAME_LEN];
	char			szIPAddr[16];		// IP address -> 123.123.123.123
} SCHED_MSG_OCCUPANCY_OPTIONS_REQUEST;

typedef struct {
	SCHED_MSG_OPTIONS_REQUEST	clientOptions;
	getMeasCmd					clientMeasure;
}SCHED_MSG_REQUEST;

typedef struct {
	SCHED_MSG_OCCUPANCY_OPTIONS_REQUEST	clientOptions;
	getOccupancyCmd				clientMeasure;
}SCHED_MSG_OCCUPANCY_REQUEST;

typedef struct {
	SCHED_MSG_OCCUPANCY_OPTIONS_REQUEST	clientOptions;
	getAutoViolateCmd				clientMeasure;
}SCHED_MSG_AUTOVIOLATE_REQUEST;

typedef struct {
	SCHED_MSG_OCCUPANCY_OPTIONS_REQUEST	clientOptions;
	getScanDFCmd				clientMeasure;
}SCHED_MSG_SCANDF_REQUEST;


typedef struct {
	unsigned long	MeasureID;
	unsigned long	ClientID;
}MEASURE_CTRL_MSG_DELETE_REQUEST;


#define	LASTBITE	(0)	// set MeasureID to request the most recent BITE results
						//  (only used when msgType == BITE_CTRL)
typedef struct {
	unsigned long	MeasureID;
	unsigned long	ClientID;
}MEASURE_CTRL_MSG_RESULTS_REQUEST;

typedef struct {
	getDwellCmd		dwellRequest;
	unsigned long	ClientID;
}MEASURE_CTRL_DWELL_REQUEST;

typedef struct {
	SCHED_MSG_OPTIONS_REQUEST	clientOptions;
//	getBITECmd					BITEMeasure;
}MEASURE_CTRL_MSG_BITE_REQUEST;

typedef struct {
	char	szStationName[MAX_STATIONNAME_LEN];
}MEASURE_CTRL_MSG_BANDNETTED_REQ;

typedef struct {
	char	szStationName[MAX_STATIONNAME_LEN];
}MEASURE_CTRL_MSG_STATION_REQ;

typedef struct
{
	unsigned long MeasureID;		
	unsigned long ClientID;				
}OCCUP_CTRL_MSG_SUSPEND_REQUEST;

typedef struct
{
	unsigned long MeasureID;		
	unsigned long ClientID;				
}OCCUP_CTRL_MSG_RESUME_REQUEST;

typedef struct
{
	unsigned long MeasureID;		
	unsigned long ClientID;				
}OCCUP_CTRL_MSG_TERMINATE_REQUEST;

typedef struct
{
	unsigned long MeasureID;		
	unsigned long ClientID;				
}OCCUP_CTRL_MSG_DELETE_REQUEST;

typedef struct
{
	unsigned long MeasureID;		
	unsigned long ClientID;				
}OCCUP_CTRL_MSG_STATUS_REQUEST;

typedef struct
{
	unsigned long MeasureID;		
	unsigned long ClientID;				
}AVD_CTRL_MSG_STATUS_REQUEST;

typedef struct
{
	unsigned long MeasureID;		
	unsigned long ClientID;				
}SCANDF_CTRL_MSG_SUSPEND_REQUEST;

typedef struct
{
	unsigned long MeasureID;		
	unsigned long ClientID;				
}SCANDF_CTRL_MSG_RESUME_REQUEST;

typedef struct
{
	unsigned long MeasureID;		
	unsigned long ClientID;				
}SCANDF_CTRL_MSG_TERMINATE_REQUEST;

typedef struct
{
	unsigned long MeasureID;		
	unsigned long ClientID;				
}SCANDF_CTRL_MSG_DELETE_REQUEST;

typedef struct
{
	unsigned long MeasureID;		
	unsigned long ClientID;				
}SCANDF_CTRL_MSG_STATUS_REQUEST;


//snm
//snm - may not even need this!
typedef struct {
	unsigned long	ClientID;	// is this needed?
}STATUS_CTRL_AUDIOTASKSTATUS_REQUEST;



// Request Server workload message body structures
typedef struct {
	double StartMeasureDateTime;
	double EndMeasureDateTime;
	unsigned long ClientID;			// the requesting Scorpio Client
} SERVER_WORKLOAD_OPTIONS;

typedef struct {
	bool reqMetricsMeasurements;	// requesting Metrics or not
	bool reqAVDMeasurements;		// requesting AVD or not
	bool reqDFScanMeasurements;		// requesting DF Scan or not
	bool reqSpecOccMeasurements;	// requesting Occupancy or not
} getWorkloadCmd;

// SERVER_WORKLOAD_REQUEST used with message MEASURE_CTRL, subtype SERVER_WORKLOAD_REQUEST_MSG
typedef struct {
	SERVER_WORKLOAD_OPTIONS	clientOptions;
	getWorkloadCmd			workloadRequest;
} SERVER_WORKLOAD_REQUEST;


////////  MESSAGES FROM MEASURE CONTROL TO CLIENT ////////////////////////////////
typedef enum
	{
		invalidRequest,
		schedInProgress,
		schedFini,
		schedBumped,
		resultsIncomplete,
		resultsFail,
		measureInProgress,
		resultsComplete,
		resultsRetrievedByClient,
		schedCancel
	}MetricsCtrlRequestState;

typedef enum
	{
		idle,
		running,
		completed,
		suspended,
		resumed,
		terminated,
		unknown,
		failed
	}OccupancyCtrlState;


typedef struct {
	unsigned long	clientTaskId;
	unsigned long	MeasureID;
	double			startMeasure;  // scheduled start time
	double			endMeasure;		//scheduled end time.	
	MetricsCtrlRequestState	stateEvent;	// state of event:  complete, retrieved, etc.
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}SCHED_MSG_RESPONSE;

typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	getMeasResp							equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_CTRL_MSG_RESULTS_RESPONSE;


typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	frequencyVsChannelResp				equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_OCC_FREQ_RESULTS_RESPONSE;

typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	frequencyVsChannelResp				equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_SCANDF_FREQ_RESULTS_RESPONSE;

typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	unsigned long	equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_SCANDF_NONE_RESPONSE;

typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	unsigned long	equipMeasurement; // 
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_OCC_ERROR_RESULTS_RESPONSE;

typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	unsigned long	equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_AVD_ERROR_RESPONSE;
typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	scanDFVsChannelResp				equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_SCANDF_DATA_RESULTS_RESPONSE;

typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	occupancyVsChannelResult				equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_OCC_CHAN_RESULTS_RESPONSE;

typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	occupancyVsChannelResult				equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_AVD_CHAN_RESULTS_RESPONSE;


typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	AVDFrequencyResult				equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_AVD_FREQ_RESPONSE;

typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	AVDBandwidthResult				equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_AVD_BANDWIDTH_RESPONSE;

typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	EFieldVsChannelResult				equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_OCC_EFLD_RESULTS_RESPONSE;

typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST		clientRequest;
	occupancyVsTimeOfDayResult				equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_OCC_TIMEOFDAY_RESULTS_RESPONSE;

typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	msglengthVsChannelResult			equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_OCC_MSGLEN_RESULTS_RESPONSE;

typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	spectrogramResult				equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_OCC_SPECGRAM_RESULTS_RESPONSE;

typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	frequencyVsChannelResp  				equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_AVD_FREQ_RESULTS_RESPONSE;

typedef struct
{
	MEASURE_CTRL_MSG_RESULTS_REQUEST	clientRequest;
	AVDBandwidthResult				equipMeasurement; // data
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_AVD_BW_RESULTS_RESPONSE;

typedef struct  {
	MEASURE_CTRL_MSG_DELETE_REQUEST		clientRequest;
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	stationId;
}MEASURE_CTRL_MSG_DELETE_RESPONSE;

typedef struct {
	getDwellResp	dwellResponse;
	unsigned long			ClientID;
	unsigned long	stationId;
}MEASURE_CTRL_DWELL_RESPONSE;

typedef struct {
	getBITEResp		BITEResponse;
	unsigned long	statusOfRequest;	// success or error status
	unsigned long	ClientID;
	unsigned long	stationId;
}MEASURE_CTRL_MSG_BITE_RESPONSE;

typedef struct {
	unsigned long		MeasureID;
	OccupancyCtrlState	occupState;
	double				completionTime;
	double				lastUpdate;			// most recent update from equipControl
	unsigned long		statusOfRequest;	// success or error status
	unsigned long		stationId;
}MEASURE_CTRL_OCCUP_STATE_RESPONSE;

typedef struct {
	unsigned long		MeasureID;
	OccupancyCtrlState	occupState;
	double				completionTime;
	double				lastUpdate;			// most recent update from equipControl
	unsigned long		statusOfRequest;	// success or error status
	unsigned long		stationId;
}MEASURE_CTRL_AVD_STATE_RESPONSE;

typedef struct {
	unsigned long		MeasureID;
	OccupancyCtrlState	scanDFState;
	double				completionTime;
	double				lastUpdate;			// most recent update from equipControl
	unsigned long		statusOfRequest;	// success or error status
	unsigned long		stationId;
}MEASURE_CTRL_SCANDF_STATE_RESPONSE;

//snm
//snm - may not even need this!
typedef struct {
	bool		statusOfRequest;		// true if there is a currently running DF Scan
										//  task recording audio, false otherwise.
}STATUS_CTRL_AUDIOTASKSTATUS_RESPONSE;

typedef enum
{
	workloadtype_metrics, 
	workloadtype_avd, 
	workloadtype_dfscan, 
	workloadtype_specocc
} MeasWorkloadType;


#define MAX_WORKLOAD_ITEMS	(500)

typedef struct {
	unsigned long		clientID;	// the client that scheduled the task
	double				startTime;	// start time of the task
	double				endTime;	// end time of the task
	OccupancyCtrlState	state;		// state of the task (only idle, running or suspended)
	char				szClientName[MAX_COMPUTERNAME_LENGTH + 1];
	char				szIPAddr[16];		// IP address -> 123.123.123.123
} MEAS_WORKLOAD_INFORMATION;

typedef struct {
	MeasWorkloadType	measType;		// metrics, avd, dfscan, or specocc
	unsigned long		numMeasurements;// number of tasks in the 'info' array
	MEAS_WORKLOAD_INFORMATION info[MAX_WORKLOAD_ITEMS];
} MEAS_WORKLOAD;

// SERVER_WORKLOAD_RESPONSE used with message MEASURE_CTRL, subtype SERVER_WORKLOAD_RESPONSE_MSG
typedef struct {
	unsigned long	statusOfRequest;// status: success (TASK_COMPLETED) or error (SERVER_FAIL_WORKLOAD)

	// All scheduled (within request), running & suspended metrics tasks
	MEAS_WORKLOAD MetricsWorkload;

	// All scheduled (within request), running & suspended AVD tasks
	MEAS_WORKLOAD AVDWorkload;

	// All scheduled (within request), running & suspended DF Scan tasks
	MEAS_WORKLOAD DFScanWorkload;

	// All scheduled (within request), running & suspended Occupancy tasks
	MEAS_WORKLOAD SpecOccWorkload;

	bool InPriorityMode;
} SERVER_WORKLOAD_RESPONSE;



////////  MESSAGES BETWEEN CALENDAR AND MEASURE CONTROL (DINO)  //////////////////////

#define MAX_OLE_DATETIME 99999.9

typedef struct {
	SCHED_MSG_RESPONSE		calendarResponse;
//	getBITECmd				clientMeasure;
}SCHED_MSG_CAL_BITE_RESPONSE;

typedef struct {
	SCHED_MSG_RESPONSE		calendarResponse;
	getMeasCmd				clientMeasure;
	unsigned long			ClientID;
	char szClientName[MAX_COMPUTERNAME_LENGTH + 1];
	char szIPAddr[16];		// IP address -> 123.123.123.123
}SCHED_MSG_CAL_RESPONSE;

typedef struct {
	SCHED_MSG_RESPONSE		calendarResponse;
	getOccupancyCmd				clientMeasure;
}SCHED_MSG_OCCUP_CAL_RESPONSE;

typedef struct {
	SCHED_MSG_RESPONSE		calendarResponse;
	getAutoViolateCmd		clientMeasure;
	unsigned long			ClientID;
}SCHED_MSG_AVD_CAL_RESPONSE;

typedef struct {
	SCHED_MSG_RESPONSE		calendarResponse;
	getScanDFCmd			clientMeasure;
	unsigned long			ClientID;
	char szClientName[MAX_COMPUTERNAME_LENGTH + 1];
	char szIPAddr[16];		// IP address -> 123.123.123.123
}SCHED_MSG_SCANDF_CAL_RESPONSE;

typedef struct {
	unsigned long				MeasureID;
	MetricsCtrlRequestState		currentState;	// RESPONSE BY CALENDAR	
	bool						bornNow;
}SCHED_STATE_MSG;								// REQUEST BY MEASURE CONTROL

typedef struct {
	unsigned long				MeasureID;
	OccupancyCtrlState			currentState;
	bool						bornNow;
}SCHED_OCCUP_STATE_MSG;								

typedef struct
{
	OCCUP_CTRL_MSG_SUSPEND_REQUEST clientRequest;		
	suspendOccupancyCmd equipCmd;
	bool status;			
}OCCUP_CTRL_CAL_MSG_SUSPEND;

typedef struct
{
	OCCUP_CTRL_MSG_RESUME_REQUEST clientRequest;		
	resumeOccupancyCmd equipCmd;				
	bool status;			
}OCCUP_CTRL_CAL_MSG_RESUME;

typedef struct
{
	OCCUP_CTRL_MSG_TERMINATE_REQUEST clientRequest;		
	taskTerminateCmd  equipCmd;				
	bool status;			
}OCCUP_CTRL_CAL_MSG_TERMINATE;

typedef struct
{
	OCCUP_CTRL_MSG_DELETE_REQUEST clientRequest;		
	bool status;			
}OCCUP_CTRL_CAL_MSG_DELETE;

typedef struct
{
	OCCUP_CTRL_MSG_STATUS_REQUEST clientRequest;		
	bool status;
	double completionTime;
}OCCUP_CTRL_CAL_MSG_STATUS;

typedef struct
{
	SCANDF_CTRL_MSG_SUSPEND_REQUEST clientRequest;		
	suspendScanDFCmd equipCmd;
	bool status;			
}SCANDF_CTRL_CAL_MSG_SUSPEND;

typedef struct
{
	SCANDF_CTRL_MSG_RESUME_REQUEST clientRequest;		
	resumeScanDFCmd equipCmd;				
	bool status;			
}SCANDF_CTRL_CAL_MSG_RESUME;

typedef struct
{
	SCANDF_CTRL_MSG_TERMINATE_REQUEST clientRequest;		
	taskTerminateCmd  equipCmd;				
	bool status;			
}SCANDF_CTRL_CAL_MSG_TERMINATE;

typedef struct
{
	SCANDF_CTRL_MSG_DELETE_REQUEST clientRequest;		
	bool status;			
}SCANDF_CTRL_CAL_MSG_DELETE;

typedef struct
{
	SCANDF_CTRL_MSG_STATUS_REQUEST clientRequest;		
	bool status;
	double completionTime;
}SCANDF_CTRL_CAL_MSG_STATUS;

////////////////////////////////////////////////////////////////////////////////////

// measurement service messages

typedef struct {
	long messageKey;
	getPanCmd panCmd;
}PAN_MEASUREMENT_REQ_MSG;

typedef struct {
	long messageKey;
	RcvrCtrlCmd recvCmd;
}RECVR_CNTL_REQ_MSG;

typedef struct
{
	unsigned long	ClientID;
	char clientName[32];
}CLIENT_DATA;

typedef struct
{
	long requestPriority;
	CLIENT_DATA echoClient;
}GREETING_MEASUREMENT_REQ_MSG;

typedef struct
{
	long messageKey;
} GREETING_MEASUREMENT_RESP_MSG;

/////////// EXTERNAL CONTROL /////////////////////////////
#define	LEN_STATIONNAME		(24)
#define	LEN_USERNAME		(64)
#define	LEN_PASSWORD		(64)

typedef struct {
	long	MessageType;
	long	MessageLen;
} EXTERNAL_MSGHDR;

#define	EXT_STATIONSTATUS_UP	(0)
#define	EXT_STATIONSTATUS_DOWN	(1)
typedef struct {
	long	StationNumber;		// The station number of the first station in the list
	char	StationName[LEN_STATIONNAME];	// The station name in ASCII format.  All unused bytes
								//  in the field contain the NULL character (ASCII 0)
	long	StationStatus;		// The current status of the station.  0=UP, any other
								//  value indicates the station is down.
} EXT_SINGLESTATION;


#define	EXT_MAX_STATIONS	(100)	// Should not be "too" small because the MAX_EXT_BODY_LEN
								//  is based (indirectly) on this number.  MAX_STATIONS is
								//  used to determine the size of the EXT_STATION_RES_BODY
								//  which, in turn, is used to calculate the MAX_EXT_BODY_LEN.
								// MAX_EXT_BODY_LEN must be large enough to accommodate all messages!
typedef struct {
	long	NumberStations;		// The number of stations in the list
	EXT_SINGLESTATION stations[EXT_MAX_STATIONS];	// All stations' information
} EXT_STATIONLIST_RES_BODY;


// MAX_EXT_BODY_LEN must be large enough to accommodate all messages!
#define	MAX_EXT_BODY_LEN	(sizeof(EXT_STATIONLIST_RES_BODY))

typedef struct {
	EXTERNAL_MSGHDR externalMsgHdr;
	unsigned char externalMsgBody[MAX_EXT_BODY_LEN];
} EXTERNAL_MSG;

typedef struct {
	//sizeof(EXT_CONNECTION_REQ_BODY) == 128
	char	UserName[LEN_USERNAME];
	char	UserPassword[LEN_PASSWORD];
} EXT_CONNECTION_REQ_BODY;

typedef struct {
	bool	ConnectAction;		// true == Want to connect to the Station, false == want to disconnect
	char	StationName[LEN_STATIONNAME];	// The station name in ASCII format.  All unused bytes
								//  in the field contain the NULL character (ASCII 0)
} EXT_STATIONCONNECT_REQ_BODY;

typedef struct {
	bool	ConnectAction;		// just echoed from the request
	bool	Status;				// Status of the requested connect/disconnect (true == successful, false == unsuccessful)
	char	StationName[LEN_STATIONNAME];	// The station name (just echoed from the request)
} EXT_STATIONCONNECT_RES_BODY;

typedef struct {
	//sizeof(EXT_BEARINGMEAS_REQ_BODY) == 16 + 4*EXT_MAX_STATIONS
	long	Frequency;	// The desired measurement frequency in hz
	long	Bandwidth;	// The desired bandwidth of the measurement.
	unsigned long TaskNumber;	// The user defined task number
	unsigned long NumberStations;	// The number of stations in the station list
	unsigned long StationNumbers[EXT_MAX_STATIONS];	// The station numbers of the 
									// measurement stations to be included in the
									// bearing measurement
} EXT_BEARINGMEAS_REQ_BODY;


#define	BEARINGSTATUS_GOOD			(0)		// Bearing result is good
#define	BEARINGSTATUS_NOGOOD		(-1)	// Bearing result could not be taken (generic)
#define	BEARINGSTATUS_TOOFEWSERVERS	(-2)	// Cannot take measurement, no servers selected
#define	BEARINGSTATUS_BUSY			(-3)	// Cannot take measurement, busy with measurment already
#define	BEARINGSTATUS_INVALIDPARMS	(-4)	// Invalid measurement parameters (freq, bw, number of stations, ...)

#define	GPSSTATUS_GOOD				(0)
#define	GPSSTATUS_NOGOOD			(-1)

#define	FLUXGATESTATUS_GOOD			(0)
#define	FLUXGATESTATUS_NOGOOD		(-1)

typedef struct {
	//sizeof(EXT_BEARINGMEAS_RES_BODY) == 80
	long	GPSstatus;
	long	DateYear;
	long	DateMonth;
	long	DateDay;
	long	DateHour;
	long	DateMinute;
	long	DateSecond;
	double	Latitude;
	double	Longitude;
	long	BearingStatus;
	long	fluxgateStatus;
	unsigned long	headingCorrection;
	unsigned long	numSumCuts;
	unsigned long	bearing;
	unsigned long	BearingQuality;
	unsigned long	stdDev;
	unsigned long	StationNumber;
	unsigned long	TaskNumber;
} EXT_BEARINGMEAS_RES_BODY;
/////////// EXTERNAL CONTROL /////////////////////////////


typedef struct
{
	unsigned long	taskID;
	unsigned long	errorCode;
} EquipTimerResp;

typedef enum
{
	deleteMeasureID,
	statusMeasureID,
	resultsRequest
}MetricsCtrlUpdateType;

typedef struct {
	MetricsCtrlUpdateType updateRequest;
	unsigned long	MeasureID;
}METRICS_CTRL_UPDATE_MSG;


//////////////////////////////////////////////////////////////////
// message subtypes for message type AUDIO_SWITCH
#define SET_SWITCH				1L
#define GET_SWITCH_STATUS		2L
#define SET_PHONE_HOOK			3L
#define SET_AUTOANSWER_MODE		4L
#define RESET_SWITCH			5L
#define SWITCH_STATUS_RESPONSE  65537L
#define AUTOANSWER_STATUS		65538L
typedef struct
{
    unsigned long speaker;
    unsigned long soundBoardLeft;
    unsigned long soundBoardRight;
    unsigned long FSKDemod;
    unsigned long lineAudio;
    unsigned long phone;
} SetAudioSwitchCmd;

typedef struct
{
	unsigned long voiceData;
	unsigned long offHook;
} SetPhoneHookCmd;

typedef struct
{
	unsigned long OnOff;
	unsigned long voiceData;
} SetAutoAnswerCmd;

typedef struct
{
	unsigned long PSQ;
	unsigned long ringDetect;
} AudioSwitchStatusResp;

// audio switch command messages from client to DINO

typedef struct {
	long messageKey;
	SetAudioSwitchCmd setSwitch;
}SET_AUDIO_SWITCH_MSG;

typedef struct {
	long messageKey;
	SetPhoneHookCmd	setPhone;
}SET_PHONE_HOOK_MSG;

typedef struct {
	long messageKey;
	SetAutoAnswerCmd setAutoAns;
}SET_AUTO_ANSWER_MSG;


// message subtypes for message type BITE_CTRL
#define GET_BITE						1L  // (no body)
#define GET_DIAGNOSTICS					2L  // (no body)
#define GET_BITE_RESULT					3L  // (no body)
#define GET_BITE_RESPONSE				65537L  // 8067A & 8067B (uses getBITEResp)
#define GET_DIAGNOSTICS_RESPONSE		65538L  // 8067A & 8067B (uses getDiagResp)
#define BITE_RESPONSE					65539L  // 8067A & 8067B (uses getBITEResp)
#define NOBITE_RESPONSE					65540L  // (no body)
#define GET_BITE_RESPONSE_8068			65541L  // 8068 (uses GetBistResp)
#define GET_DIAGNOSTICS_RESPONSE_8068	65542L  // 8068 (uses GetBistResp)
#define BITE_RESPONSE_8068				65543L  // 8068 (uses GetBistResp)

typedef struct
{
	unsigned long       status;
	double				startDateTime;	// using COleTime format
	double				completionDateTime;
	char                computerName[32];
	unsigned long       ipAdr[4];
	char				moduleName[3][100];
	char				lastModified[3][30];
	char				fileVer[30];
	long				fileSize[3];
	long                GPSStatus;
	DIAG_FLUXGATE       diagFluxgate;
	DIAG_DSP            diagDSP;
	DIAG_CLK_SYNTH      diagClkSynth;
	DIAG_ADC_NBT        diagADC_NBT;
	DIAG_AUDIO_SWITCH   diagAudioSwitch;
	DIAG_ADC_IO         diagADC_IO;
	DIAG_HF_CHASSIS     diagHFChassis;
	DIAG_HFDF_RCVR      diagHFDFRcvr;
	DIAG_UHF_MUX        diagUHFMux;
	DIAG_HF_RCVR        diagHFRcvr;
	DIAG_HF_SWITCH      diagHFSwitch;
	DIAG_HF_ANTENNA     diagHFAntenna;
	DIAG_VHF_RCVR       diagVHFRcvr;
	DIAG_UHF_SWITCH     diagUHFSwitch;
	DIAG_ANTENNA        diagUHFAntenna;
	DIAG_VHF_SWITCH     diagVHFSwitch;
	DIAG_ANTENNA        diagVHFAntenna;
	DIAG_SYS_GAIN       diagSysGain;
} getDiagResp;

enum EResult { PASS, FAIL, WARNING };
typedef	struct
{
	unsigned long result;  // uses EResult
	size_t textLen;
	bool last; // Last message of complete BIST result
	wchar_t text[1]; // Variable length (textLen); "test name\tresult string"
} GetBistResp;


////////////////////////////////////
// message subtypes for message type DSP_CTRL
#define DO_DSP_INIT					1L
#define DO_DSP_INIT_RESPONSE		65537L

// DO_DSP_INIT Command message body
// no message body for this command

// DO_DSP_INIT_RESPONSE message body
typedef struct
{
	unsigned long	DSPStatus;
} DoDSPInitResp;

// message subtype LOAD_IONOGRAM (message type DF_CTRL)
typedef struct
{
	unsigned long taskID;
	IONOGRAM_DATA_MSG ionodata;
} LOAD_IONOGRAM_STRUCT;

// message subtype LOAD_IONOGRAM_RESP (message type DF_CTRL)
typedef struct
{
	unsigned long taskID;
	unsigned long status;
} LOAD_IONOGRAM_RESP_STRUCT;
	 
// message subtype GET_IONOGRAM (message type DF_CTRL)
typedef struct
{
	unsigned long taskID;
} GET_IONOGRAM_STRUCT;

// message subtype GET_IONOGRAM_RESP (message type DF_CTRL)
typedef struct
{
	unsigned long taskID;
	unsigned long status;
	IONOGRAM_DATA_MSG ionodata;
} GET_IONOGRAM_RESP_STRUCT;
	 
typedef struct {
	unsigned long nextId;
	double		startTime;
	double		dueTime;
} calNextData;

typedef struct
{
	calNextData calNextId;
	getMeasCmd  calMeas; 
}calNextMsg;


// Miscellaneous
#define	COMPRESS_MESSAGES	TRUE	// Determines if messages are compressed before being sent

// Message versioning
typedef struct
{
	unsigned short msgType;
	unsigned long msgSubType;
}VersionKey;

typedef struct
{
	unsigned char cmdVersion;
	unsigned char respVersion;
}VersionValue;

typedef struct
{
	VersionKey msgId;
	VersionValue version;
}VersionData;

typedef struct
{
	unsigned long count;
	VersionData versionData[10];
}GetMsgVersionsData;

#endif
