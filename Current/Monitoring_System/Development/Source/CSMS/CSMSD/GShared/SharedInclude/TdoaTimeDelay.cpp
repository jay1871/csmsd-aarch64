/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"
#include "TdoaTimeDelay.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
//
// To optimize the Intel IPP performance, the ippStaticInit( ) function should be called one time only prior
//   to using this class (or any other IPP functions).  This function automatically initializes static code
//   that is the most appropriate for the current processor type.  For example:
//
//	if(ippStaticInit() != ippStsNoErr)
//	{
//		error processing
//	}
//


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CTDOATimeDelay::CTDOATimeDelay(void)
:
	m_FFTAllocationOK(false),
	m_numSamples(0)
{
	m_pFFTBuffer[0] = NULL;
	m_pFFTBuffer[1] = NULL;
	m_pFFTBuffer[2] = NULL;
	m_pFFTBuffer[3] = NULL;
	m_pFFTSpec[0] = NULL;
	m_pFFTSpec[1] = NULL;
	m_pFFTSpec[2] = NULL;
	m_pFFTSpec[3] = NULL;
	ippsFFTInitAlloc_C_64fc(&m_pFFTSpecC[0], 17, IPP_FFT_DIV_FWD_BY_N, ippAlgHintAccurate);
	ippsFFTInitAlloc_C_64fc(&m_pFFTSpecC[1], 17, IPP_FFT_DIV_FWD_BY_N, ippAlgHintAccurate);
	ippsFFTInitAlloc_C_64fc(&m_pFFTSpecC[2], 17, IPP_FFT_DIV_FWD_BY_N, ippAlgHintAccurate);
	ippsFFTInitAlloc_C_64fc(&m_pFFTSpecC[3], 17, IPP_FFT_DIV_FWD_BY_N, ippAlgHintAccurate);
	int bufferSize; // FFT external work buffer size
	ippsFFTGetBufSize_C_64fc(m_pFFTSpecC[0], &bufferSize);
	m_pFFTBufferC[0] = ippsMalloc_8u(bufferSize);
	m_pFFTBufferC[1] = ippsMalloc_8u(bufferSize);
	m_pFFTBufferC[2] = ippsMalloc_8u(bufferSize);
	m_pFFTBufferC[3] = ippsMalloc_8u(bufferSize);

	return;

}

//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CTDOATimeDelay::~CTDOATimeDelay(void)
{
	ippsFFTFree_C_64fc(m_pFFTSpecC[0]);
	ippsFFTFree_C_64fc(m_pFFTSpecC[1]);
	ippsFFTFree_C_64fc(m_pFFTSpecC[2]);
	ippsFFTFree_C_64fc(m_pFFTSpecC[3]);
	if(m_pFFTSpec[0] != NULL)
	{
		ippsFFTFree_C_64fc(m_pFFTSpec[0]);
	}
	if(m_pFFTSpec[1] != NULL)
	{
		ippsFFTFree_C_64fc(m_pFFTSpec[1]);
	}
	if(m_pFFTSpec[2] != NULL)
	{
		ippsFFTFree_C_64fc(m_pFFTSpec[2]);
	}
	if(m_pFFTSpec[3] != NULL)
	{
		ippsFFTFree_C_64fc(m_pFFTSpec[3]);
	}
	if(m_pFFTBuffer[0] != NULL)
	{
		ippsFree(m_pFFTBuffer[0]);
	}
	if(m_pFFTBuffer[1] != NULL)
	{
		ippsFree(m_pFFTBuffer[1]);
	}
	if(m_pFFTBuffer[2] != NULL)
	{
		ippsFree(m_pFFTBuffer[2]);
	}
	if(m_pFFTBuffer[3] != NULL)
	{
		ippsFree(m_pFFTBuffer[3]);
	}
	if(m_pFFTBufferC[0] != NULL)
	{
		ippsFree(m_pFFTBufferC[0]);
	}
	if(m_pFFTBufferC[1] != NULL)
	{
		ippsFree(m_pFFTBufferC[1]);
	}
	if(m_pFFTBufferC[2] != NULL)
	{
		ippsFree(m_pFFTBufferC[2]);
	}
	if(m_pFFTBufferC[3] != NULL)
	{
		ippsFree(m_pFFTBufferC[3]);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Find maximum possible Doppler frequency offset (frequency bins)
//
unsigned int CTDOATimeDelay::CalcMaxDopplerBinOffset(_In_ const Units::Frequency frequency, _In_ const Units::Frequency sampleRate,
		_In_ const double velocitySensor1, _In_ const double velocitySensor2, _In_ const double maxVelocityTarget, _In_ const unsigned int numSamples)
{
//  velocity is meters / sec (= 0.44704 * mph)

	double velocity;
	if(velocitySensor1 > velocitySensor2)
	{
		velocity = velocitySensor1;
		if(velocitySensor2 > maxVelocityTarget)
		{
			velocity += velocitySensor2;
		}
		else
		{
			velocity += maxVelocityTarget;
		}
	}	
	else
	{
		velocity = velocitySensor2;
		if(velocitySensor1 > maxVelocityTarget)
		{
			velocity += velocitySensor1;
		}
		else
		{
			velocity += maxVelocityTarget;
		}
	}

	// minimum velocity of 2.5 m/s  // TODO -- is 2.5 correct?
	velocity = std::max(velocity, 2.5);

	unsigned int bins = unsigned int(((((299792500.0 + velocity) / 299792500.0) * frequency.Hz<double>()) - frequency.Hz<double>()) /
			(sampleRate.Hz<double>() /(2.0 * double(numSamples)))+ 0.9);

	return(bins);
}


//////////////////////////////////////////////////////////////////////
//
// Find number of time bins to process
//
size_t CTDOATimeDelay::CalcNumCorrelationBins(_In_ const double latitude1, _In_ const double longitude1, _In_ const double latitude2,
		_In_ const double longitude2, _In_ const Units::Frequency sampleRate)
{
	// 2 * distance between sensors(m) * sample rate(Hz) / speed of light(m/s)
	// Add a little bit for edge cases. (was 2.5 now is 16)
	// TODO: This should be calculated as LOS distance, not GC distance
	//	size_t numCorr = size_t(16.0 + 2.0 * Units::EARTH_RADIUS_M * acos(sin(Units::D2R * latitude1) * sin(Units::D2R * latitude2) +
	//			cos(Units::D2R * latitude1) * cos(Units::D2R * latitude2) * cos(Units::D2R * longitude2 - Units::D2R * longitude1)) *
	//			sampleRate.Hz<double>() / 299792500.0);
	size_t numCorr = size_t(5.0 + 16.e-7 * sampleRate.Hz<double>() + 2.0 * Units::EARTH_RADIUS_M * acos(sin(Units::D2R * latitude1) * sin(Units::D2R * latitude2) +
		cos(Units::D2R * latitude1) * cos(Units::D2R * latitude2) * cos(Units::D2R * longitude2 - Units::D2R * longitude1)) *
		sampleRate.Hz<double>() / 299792500.0);

	// TEMPORARY CHANGE TO ALLOW VERY LARGE TIME DELAYS - REMOVE WHEN DONE TESTING //
//	numCorr += 300;
	/////////////////////////////////////////////////////////////////////////////////
	// make even
	if((numCorr & 1) == 1)
	{
		numCorr++;
	}
	// minimum of 8
	return(std::max(numCorr, 8u));
}


//////////////////////////////////////////////////////////////////////
//
// Verifies IQ data overlap amongst all sensors
//
bool CTDOATimeDelay::ComputeDataOverlap(_In_ const STDOATimeDelayInput& input, _Out_ TDOADataOverlapOutput& output)
{
	// Convert to V2
	if (input.iqType == EIqType::IQ_16SC)
		return ComputeDataOverlap(STDOATimeDelayInputV2<Ipp16s>(input), output);

	else if (input.iqType == EIqType::IQ_32SC)
		return ComputeDataOverlap(STDOATimeDelayInputV2<Ipp32s>(input), output);

	else if (input.iqType == EIqType::IQ_32FC)
		return ComputeDataOverlap(STDOATimeDelayInputV2<Ipp32f>(input), output);

	else if (input.iqType == EIqType::IQ_64FC)
		return ComputeDataOverlap(STDOATimeDelayInputV2<Ipp64f>(input), output);

	else 
		return ComputeDataOverlap(STDOATimeDelayInputV2<Ipp16s>(input), output);
}

//////////////////////////////////////////////////////////////////////
//
// Magnitude computation of complex vector
//
double CTDOATimeDelay::ComputeMagnitude(_In_ const Ipp64fcVec& signal, _In_ const int len)
{
	Ipp64fVec magBuf(len);
	double magSum;
	ippsMagnitude_64fc(&(signal[0]), &magBuf[0], len);
	ippsSum_64f(&magBuf[0], len, &magSum);
	return magSum;
}


//////////////////////////////////////////////////////////////////////
//
// Overall TDOA time delay calculation
//
bool CTDOATimeDelay::ComputeTimeDelay(_In_ const STDOATimeDelayInput& input, _Out_ TDOATimeDelayOutput& output)
{
	// clear output map
	output.clear();

	TDOATimeDelayOutputV1 outputV1;
	// Convert to V2
	bool rv;
	if (input.iqType == EIqType::IQ_16SC)
		rv = ComputeTimeDelay(STDOATimeDelayInputV2<Ipp16s>(input), outputV1);

	else if (input.iqType == EIqType::IQ_32SC)
		rv = ComputeTimeDelay(STDOATimeDelayInputV2<Ipp32s>(input), outputV1);

	else if (input.iqType == EIqType::IQ_32FC)
		rv = ComputeTimeDelay(STDOATimeDelayInputV2<Ipp32f>(input), outputV1);

	else if (input.iqType == EIqType::IQ_64FC)
		rv = ComputeTimeDelay(STDOATimeDelayInputV2<Ipp64f>(input), outputV1);

	else
		rv = ComputeTimeDelay(STDOATimeDelayInputV2<Ipp16s>(input), outputV1);

	for (const auto& it : outputV1)
	{
		output.emplace(it.first, it.second);
	}
	return rv;
}


//////////////////////////////////////////////////////////////////////
//
// Configure FFT buffers based on sample size
//
bool CTDOATimeDelay::ConfigureFFTBuffers(_In_ const unsigned int numSamples)
{
	if(numSamples != m_numSamples)
	{
		if(m_pFFTSpec[0] != NULL)
		{
			ippsFFTFree_C_64fc(m_pFFTSpec[0]);
			m_pFFTSpec[0] = NULL;
		}
		if(m_pFFTSpec[1] != NULL)
		{
			ippsFFTFree_C_64fc(m_pFFTSpec[1]);
			m_pFFTSpec[1] = NULL;
		}
		if(m_pFFTSpec[2] != NULL)
		{
			ippsFFTFree_C_64fc(m_pFFTSpec[2]);
			m_pFFTSpec[2] = NULL;
		}
		if(m_pFFTSpec[3] != NULL)
		{
			ippsFFTFree_C_64fc(m_pFFTSpec[3]);
			m_pFFTSpec[3] = NULL;
		}
		if(m_pFFTBuffer[0] != NULL)
		{
			ippsFree(m_pFFTBuffer[0]);
			m_pFFTBuffer[0] = NULL;
		}
		if(m_pFFTBuffer[1] != NULL)
		{
			ippsFree(m_pFFTBuffer[1]);
			m_pFFTBuffer[1] = NULL;
		}
		if(m_pFFTBuffer[2] != NULL)
		{
			ippsFree(m_pFFTBuffer[2]);
			m_pFFTBuffer[2] = NULL;
		}
		if(m_pFFTBuffer[3] != NULL)
		{
			ippsFree(m_pFFTBuffer[3]);
			m_pFFTBuffer[3] = NULL;
		}
		m_FFTAllocationOK = false;

		int order = 0;
		unsigned int tmpSize = 2 * numSamples;
		while(tmpSize > 1)
		{
			order++;
			tmpSize /= 2;
		}
		IppStatus status = ippsFFTInitAlloc_C_64fc(&m_pFFTSpec[0], order, IPP_FFT_DIV_FWD_BY_N, ippAlgHintAccurate);
		if(ippStsNoErr != status)
		{
			m_pFFTSpec[0] = NULL;
			m_numSamples = 0;
			return false;
		}
		status = ippsFFTInitAlloc_C_64fc(&m_pFFTSpec[1], order, IPP_FFT_DIV_FWD_BY_N, ippAlgHintAccurate);
		if(ippStsNoErr != status)
		{
			ippsFFTFree_C_64fc(m_pFFTSpec[0]);
			m_pFFTSpec[0] = NULL;
			m_pFFTSpec[1] = NULL;
			m_numSamples = 0;
			return false;
		}
		status = ippsFFTInitAlloc_C_64fc(&m_pFFTSpec[2], order, IPP_FFT_DIV_FWD_BY_N, ippAlgHintAccurate);
		if(ippStsNoErr != status)
		{
			ippsFFTFree_C_64fc(m_pFFTSpec[0]);
			m_pFFTSpec[0] = NULL;
			ippsFFTFree_C_64fc(m_pFFTSpec[1]);
			m_pFFTSpec[1] = NULL;
			m_pFFTSpec[2] = NULL;
			m_numSamples = 0;
			return false;
		}
		status = ippsFFTInitAlloc_C_64fc(&m_pFFTSpec[3], order, IPP_FFT_DIV_FWD_BY_N, ippAlgHintAccurate);
		if(ippStsNoErr != status)
		{
			ippsFFTFree_C_64fc(m_pFFTSpec[0]);
			m_pFFTSpec[0] = NULL;
			ippsFFTFree_C_64fc(m_pFFTSpec[1]);
			m_pFFTSpec[1] = NULL;
			ippsFFTFree_C_64fc(m_pFFTSpec[2]);
			m_pFFTSpec[2] = NULL;
			m_pFFTSpec[3] = NULL;
			m_numSamples = 0;
			return false;
		}
		int bufferSize; // FFT external work buffer size
		ippsFFTGetBufSize_C_64fc(m_pFFTSpec[0], &bufferSize);
		m_pFFTBuffer[0] = ippsMalloc_8u(bufferSize);
		if(m_pFFTBuffer[0] == NULL)
		{
			ippsFFTFree_C_64fc(m_pFFTSpec[0]);
			m_pFFTSpec[0] = NULL;
			ippsFFTFree_C_64fc(m_pFFTSpec[1]);
			m_pFFTSpec[1] = NULL;
			ippsFFTFree_C_64fc(m_pFFTSpec[2]);
			m_pFFTSpec[2] = NULL;
			ippsFFTFree_C_64fc(m_pFFTSpec[3]);
			m_pFFTSpec[3] = NULL;
			m_numSamples = 0;
			return false;
		}
		m_pFFTBuffer[1] = ippsMalloc_8u(bufferSize);
		if(m_pFFTBuffer[1] == NULL)
		{
			ippsFFTFree_C_64fc(m_pFFTSpec[0]);
			m_pFFTSpec[0] = NULL;
			ippsFFTFree_C_64fc(m_pFFTSpec[1]);
			m_pFFTSpec[1] = NULL;
			ippsFFTFree_C_64fc(m_pFFTSpec[2]);
			m_pFFTSpec[2] = NULL;
			ippsFFTFree_C_64fc(m_pFFTSpec[3]);
			m_pFFTSpec[3] = NULL;
			ippsFree(m_pFFTBuffer[0]);
			m_pFFTBuffer[0] = NULL;
			m_numSamples = 0;
			return false;
		}
		m_pFFTBuffer[2] = ippsMalloc_8u(bufferSize);
		if(m_pFFTBuffer[2] == NULL)
		{
			ippsFFTFree_C_64fc(m_pFFTSpec[0]);
			m_pFFTSpec[0] = NULL;
			ippsFFTFree_C_64fc(m_pFFTSpec[1]);
			m_pFFTSpec[1] = NULL;
			ippsFFTFree_C_64fc(m_pFFTSpec[2]);
			m_pFFTSpec[2] = NULL;
			ippsFFTFree_C_64fc(m_pFFTSpec[3]);
			m_pFFTSpec[3] = NULL;
			ippsFree(m_pFFTBuffer[0]);
			m_pFFTBuffer[0] = NULL;
			ippsFree(m_pFFTBuffer[1]);
			m_pFFTBuffer[1] = NULL;
			m_numSamples = 0;
			return false;
		}
		m_pFFTBuffer[3] = ippsMalloc_8u(bufferSize);
		if(m_pFFTBuffer[3] == NULL)
		{
			ippsFFTFree_C_64fc(m_pFFTSpec[0]);
			m_pFFTSpec[0] = NULL;
			ippsFFTFree_C_64fc(m_pFFTSpec[1]);
			m_pFFTSpec[1] = NULL;
			ippsFFTFree_C_64fc(m_pFFTSpec[2]);
			m_pFFTSpec[2] = NULL;
			ippsFFTFree_C_64fc(m_pFFTSpec[3]);
			m_pFFTSpec[3] = NULL;
			ippsFree(m_pFFTBuffer[0]);
			m_pFFTBuffer[0] = NULL;
			ippsFree(m_pFFTBuffer[1]);
			m_pFFTBuffer[1] = NULL;
			ippsFree(m_pFFTBuffer[2]);
			m_pFFTBuffer[2] = NULL;
			m_numSamples = 0;
			return false;
		}
		m_FFTAllocationOK = true;
		m_numSamples = numSamples;
	}
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Cross correlation including interpolation and parabolic fit
//
int CTDOATimeDelay::CrossCorrelation(_In_ unsigned int maxDopplerOffsetBins, _In_ const int lowLag, _In_ const Ipp64fcVec& refCSignal,
		_In_ const Ipp64fcVec& delayCSignal, _Out_ double& xcorrMaxIndex, _Inout_ Ipp64fVec& xcorr, _Out_ int& maxXCorrIndex, _Out_ double& maxXCorr,
		_Out_ double& minXCorr, _In_ const int sampleLength)
{
	size_t i;
	unsigned int size = 2*sampleLength;
	size_t numcorr = xcorr.size();
	Ipp64fcVec signal3;
	Ipp64fcVec signal4;
	Ipp64fcVec shiftedSignal;

	if((maxDopplerOffsetBins & 1) == 1)
	{
		maxDopplerOffsetBins++;  // make an even value
	}

	if(!m_FFTAllocationOK)
	{
		// FFT not properly initialized
		return 1;
	}

#if(0)
	Ipp64fcVec tmpSignal(sampleLength);
	double binOffset = 0.75;
	double testFreq = binOffset / (sampleLength * 2.0);
	double testPhase = 0.0;
	ippsTone_Direct_64fc(&tmpSignal[0], sampleLength, 1.0, testFreq, &testPhase, ippAlgHintAccurate);
	ippsMul_64fc_I(&tmpSignal[0], &refCSignal[0], sampleLength);
#endif

	try
	{
		m_refSignal[3].resize(size + maxDopplerOffsetBins);
		m_delaySignal.resize(size + maxDopplerOffsetBins);
	}
	catch(std::bad_alloc&)
	{
		// out of memory
		return 1;
	}
	Ipp64fc* refSignal;
	size_t maxIdx1 = 0;
	size_t maxIdx2 = 0;
	m_processTaskResponse.firstIdx1 = 0;
	m_processTaskResponse.firstIdx2 = maxDopplerOffsetBins;
	m_processTaskResponse.lastIdx1 = maxDopplerOffsetBins;

	if(maxDopplerOffsetBins > 0 && sampleLength > 65536)
	{
		// perform coarse doppler search
		unsigned int coarseMaxDopplerOffsetBins = maxDopplerOffsetBins * 65536 / sampleLength;
		if((coarseMaxDopplerOffsetBins & 1) == 1)
		{
			coarseMaxDopplerOffsetBins++;  // make an even value
		}
		unsigned int fineMaxDopplerOffsetBins = sampleLength / 65536;
		maxDopplerOffsetBins = coarseMaxDopplerOffsetBins * fineMaxDopplerOffsetBins;
		ippsZero_64fc(&m_refSignal[3][0], 131072 + coarseMaxDopplerOffsetBins);
		ippsZero_64fc(&m_delaySignal[0], 131072 + coarseMaxDopplerOffsetBins);
		ippsMove_64fc(&refCSignal[0], &m_refSignal[3][65536 + coarseMaxDopplerOffsetBins/2], 65536); 
		ippsMove_64fc(&delayCSignal[0], &m_delaySignal[coarseMaxDopplerOffsetBins/2], 65536); 
		ippsFFTFwd_CToC_64fc_I(&m_refSignal[3][coarseMaxDopplerOffsetBins/2], m_pFFTSpecC[3], m_pFFTBufferC[3]);
		ippsFFTFwd_CToC_64fc_I(&m_delaySignal[coarseMaxDopplerOffsetBins/2], m_pFFTSpecC[3], m_pFFTBufferC[3]);
		ippsConj_64fc_I(&m_delaySignal[coarseMaxDopplerOffsetBins/2], 131072);
		maxIdx1 = 0;
		maxIdx2 = 0;
		try
		{
			shiftedSignal.resize(65536);
			m_refSignal[0].resize(131072 + coarseMaxDopplerOffsetBins);
			m_refSignal[1].resize(131072 + coarseMaxDopplerOffsetBins);
			m_refSignal[2].resize(131072 + coarseMaxDopplerOffsetBins);
		}
		catch(std::bad_alloc&)
		{
			// out of memory
			return 1;
		}
		ippsZero_64fc(&m_refSignal[0][0], 131072 + coarseMaxDopplerOffsetBins);
		ippsZero_64fc(&m_refSignal[1][0], 131072 + coarseMaxDopplerOffsetBins);
		ippsZero_64fc(&m_refSignal[2][0], 131072 + coarseMaxDopplerOffsetBins);
		double testPhase = 0.0;
		ippsTone_Direct_64fc(&shiftedSignal[0], 65536, 1.0, 0.25 / 131072.0, &testPhase, ippAlgHintAccurate);
		ippsMul_64fc(&shiftedSignal[0], &refCSignal[0], &m_refSignal[0][65536 + coarseMaxDopplerOffsetBins/2], 65536);
		ippsFFTFwd_CToC_64fc_I(&m_refSignal[0][coarseMaxDopplerOffsetBins/2], m_pFFTSpecC[3], m_pFFTBufferC[3]);
		ippsTone_Direct_64fc(&shiftedSignal[0], 65536, 1.0, 0.5 / 131072.0, &testPhase, ippAlgHintAccurate);
		ippsMul_64fc(&shiftedSignal[0], &refCSignal[0], &m_refSignal[1][65536 + coarseMaxDopplerOffsetBins/2], 65536);
		ippsFFTFwd_CToC_64fc_I(&m_refSignal[1][coarseMaxDopplerOffsetBins/2], m_pFFTSpecC[3], m_pFFTBufferC[3]);
		ippsTone_Direct_64fc(&shiftedSignal[0], 65536, 1.0, 0.75 / 131072.0, &testPhase, ippAlgHintAccurate);
		ippsMul_64fc(&shiftedSignal[0], &refCSignal[0], &m_refSignal[2][65536 + coarseMaxDopplerOffsetBins/2], 65536);
		ippsFFTFwd_CToC_64fc_I(&m_refSignal[2][coarseMaxDopplerOffsetBins/2], m_pFFTSpecC[3], m_pFFTBufferC[3]);
		
		m_processTaskResponse.size = 131072;
		m_processTaskResponse.maxDopplerOffsetBins = coarseMaxDopplerOffsetBins;
		m_processTaskResponse.lowLag = lowLag;
		m_processTaskResponse.coarse = true;
		m_processTaskResponse.firstIdx1 = 0;
		m_processTaskResponse.firstIdx2 = coarseMaxDopplerOffsetBins;
		m_processTaskResponse.lastIdx1 = coarseMaxDopplerOffsetBins;

		SThreadStruct threadStruct0;
		SThreadStruct threadStruct1;
		SThreadStruct threadStruct2;
		threadStruct0.ptr = this;
		threadStruct0.index = 0;
		threadStruct1.ptr = this;
		threadStruct1.index = 1;
		threadStruct2.ptr = this;
		threadStruct2.index = 2;

		m_thread[0] = AfxBeginThread(Thread0, LPVOID(&threadStruct0), THREAD_PRIORITY_BELOW_NORMAL, 0, CREATE_SUSPENDED, NULL);
		m_thread[1] = AfxBeginThread(Thread1, LPVOID(&threadStruct1), THREAD_PRIORITY_BELOW_NORMAL, 0, CREATE_SUSPENDED, NULL);
		m_thread[2] = AfxBeginThread(Thread2, LPVOID(&threadStruct2), THREAD_PRIORITY_BELOW_NORMAL, 0, CREATE_SUSPENDED, NULL);
		m_thread[0]->m_bAutoDelete = FALSE;
		m_thread[1]->m_bAutoDelete = FALSE;
		m_thread[2]->m_bAutoDelete = FALSE;
		m_thread[0]->ResumeThread();
		m_thread[1]->ResumeThread();
		m_thread[2]->ResumeThread();

		PartitionedDopplerSearch(3);

		HANDLE events[3] = { m_thread[0]->m_hThread, m_thread[1]->m_hThread, m_thread[2]->m_hThread };
		DWORD waitStatus = WaitForMultipleObjects(_countof(events), events, TRUE, 300000);  // 5 minute timeout
		delete m_thread[0];
		delete m_thread[1];
		delete m_thread[2];
		if(waitStatus == WAIT_TIMEOUT)
		{
			return 2;
		}

		Ipp64f max = 0.0;
		for(i = 0; i < 4; i++)
		{
			if(m_processTaskResponse.mallocError[i])
			{
				// out of memory
				return 1;
			}
			if(m_processTaskResponse.max[i] > max)
			{
				max = m_processTaskResponse.max[i];
				maxIdx1 = m_processTaskResponse.maxIdx1[i];
				maxIdx2 = m_processTaskResponse.maxIdx2[i];
			}
		}

		if(maxIdx1 + maxIdx2 == coarseMaxDopplerOffsetBins)
		{
			m_processTaskResponse.firstIdx1 = maxIdx1 * fineMaxDopplerOffsetBins;
			if(m_processTaskResponse.firstIdx1 >= fineMaxDopplerOffsetBins / 2)
			{
				m_processTaskResponse.firstIdx1 -= fineMaxDopplerOffsetBins / 2;
			}
			else
			{
				m_processTaskResponse.firstIdx1 = 0;
			}
			m_processTaskResponse.firstIdx2 = maxIdx2 * fineMaxDopplerOffsetBins + fineMaxDopplerOffsetBins / 2;
			m_processTaskResponse.lastIdx1 = maxIdx1 * fineMaxDopplerOffsetBins + fineMaxDopplerOffsetBins / 2;
		}
		else
		{
			m_processTaskResponse.firstIdx1 = maxIdx1 * fineMaxDopplerOffsetBins;
			m_processTaskResponse.firstIdx2 = maxIdx2 * fineMaxDopplerOffsetBins + fineMaxDopplerOffsetBins;
			m_processTaskResponse.lastIdx1 = maxIdx1 * fineMaxDopplerOffsetBins + fineMaxDopplerOffsetBins;
		}
		if(m_processTaskResponse.firstIdx2 > maxDopplerOffsetBins)
		{
			m_processTaskResponse.firstIdx2 = maxDopplerOffsetBins;
		}
		if(m_processTaskResponse.lastIdx1 > maxDopplerOffsetBins)
		{
			m_processTaskResponse.lastIdx1 = maxDopplerOffsetBins;
		}
	}

	ippsZero_64fc(&m_refSignal[3][0], size + maxDopplerOffsetBins);
	ippsZero_64fc(&m_delaySignal[0], size + maxDopplerOffsetBins);
	ippsMove_64fc(&refCSignal[0], &m_refSignal[3][size/2 + maxDopplerOffsetBins/2], size/2); 
	ippsMove_64fc(&delayCSignal[0], &m_delaySignal[maxDopplerOffsetBins/2], size/2); 
	ippsFFTFwd_CToC_64fc_I(&m_refSignal[3][maxDopplerOffsetBins/2], m_pFFTSpec[3], m_pFFTBuffer[3]); 
	ippsFFTFwd_CToC_64fc_I(&m_delaySignal[maxDopplerOffsetBins/2], m_pFFTSpec[3], m_pFFTBuffer[3]);
	ippsConj_64fc_I(&m_delaySignal[maxDopplerOffsetBins/2], size);
	maxIdx1 = 0;
	maxIdx2 = 0;
	if(maxDopplerOffsetBins > 0)
	{
		try
		{
			m_refSignal[0].resize(size + maxDopplerOffsetBins);
			m_refSignal[1].resize(size + maxDopplerOffsetBins);
			m_refSignal[2].resize(size + maxDopplerOffsetBins);
			shiftedSignal.resize(sampleLength);
		}
		catch(std::bad_alloc&)
		{
			// out of memory
			return 1;
		}
		ippsZero_64fc(&m_refSignal[0][0], size + maxDopplerOffsetBins);
		ippsZero_64fc(&m_refSignal[1][0], size + maxDopplerOffsetBins);
		ippsZero_64fc(&m_refSignal[2][0], size + maxDopplerOffsetBins);
		double testPhase = 0.0;
		ippsTone_Direct_64fc(&shiftedSignal[0], sampleLength, 1.0, 0.25 / double((sampleLength) * 2.0), &testPhase, ippAlgHintAccurate);
		ippsMul_64fc(&shiftedSignal[0], &refCSignal[0], &m_refSignal[0][size/2 + maxDopplerOffsetBins/2], sampleLength);
		ippsFFTFwd_CToC_64fc_I(&m_refSignal[0][maxDopplerOffsetBins/2], m_pFFTSpec[3], m_pFFTBuffer[3]);
		ippsTone_Direct_64fc(&shiftedSignal[0], sampleLength, 1.0, 0.5 / double((sampleLength) * 2.0), &testPhase, ippAlgHintAccurate);
		ippsMul_64fc(&shiftedSignal[0], &refCSignal[0], &m_refSignal[1][size/2 + maxDopplerOffsetBins/2], sampleLength);
		ippsFFTFwd_CToC_64fc_I(&m_refSignal[1][maxDopplerOffsetBins/2], m_pFFTSpec[3], m_pFFTBuffer[3]);
		ippsTone_Direct_64fc(&shiftedSignal[0], sampleLength, 1.0, 0.75 / double((sampleLength) * 2.0), &testPhase, ippAlgHintAccurate);
		ippsMul_64fc(&shiftedSignal[0], &refCSignal[0], &m_refSignal[2][size/2 + maxDopplerOffsetBins/2], sampleLength);
		ippsFFTFwd_CToC_64fc_I(&m_refSignal[2][maxDopplerOffsetBins/2], m_pFFTSpec[3], m_pFFTBuffer[3]);
		
		m_processTaskResponse.size = size;
		m_processTaskResponse.maxDopplerOffsetBins = maxDopplerOffsetBins;
		m_processTaskResponse.lowLag = lowLag;
		m_processTaskResponse.coarse = false;

		SThreadStruct threadStruct0;
		SThreadStruct threadStruct1;
		SThreadStruct threadStruct2;
		threadStruct0.ptr = this;
		threadStruct0.index = 0;
		threadStruct1.ptr = this;
		threadStruct1.index = 1;
		threadStruct2.ptr = this;
		threadStruct2.index = 2;

		m_thread[0] = AfxBeginThread(Thread0, LPVOID(&threadStruct0), THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
		m_thread[1] = AfxBeginThread(Thread1, LPVOID(&threadStruct1), THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
		m_thread[2] = AfxBeginThread(Thread2, LPVOID(&threadStruct2), THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
		m_thread[0]->m_bAutoDelete = FALSE;
		m_thread[1]->m_bAutoDelete = FALSE;
		m_thread[2]->m_bAutoDelete = FALSE;
		m_thread[0]->ResumeThread();
		m_thread[1]->ResumeThread();
		m_thread[2]->ResumeThread();

		PartitionedDopplerSearch(3);

		HANDLE events[3] = { m_thread[0]->m_hThread, m_thread[1]->m_hThread, m_thread[2]->m_hThread };
		DWORD waitStatus = WaitForMultipleObjects(_countof(events), events, TRUE, 300000);    // 5 minute timeout
		delete m_thread[0];
		delete m_thread[1];
		delete m_thread[2];
		if(waitStatus == WAIT_TIMEOUT)
		{
			return 2;
		}
		Ipp64f max = 0.0;
		size_t index = 0;
		for(i = 0; i < 4; i++)
		{
			if(m_processTaskResponse.mallocError[i])
			{
				// out of memory
				return 1;
			}
			if(m_processTaskResponse.max[i] > max)
			{
				max = m_processTaskResponse.max[i];
				maxIdx1 = m_processTaskResponse.maxIdx1[i];
				maxIdx2 = m_processTaskResponse.maxIdx2[i];
				index = i;
			}
		}
		refSignal = &m_refSignal[index][0];
	}
	else
	{
		// maxDopplerOffsetBins = 0
		refSignal = &m_refSignal[3][0];
		maxIdx1 = 0;
		maxIdx2 = 0;
	}

	try
	{
		signal3.resize(size);
		signal4.resize(size);
	}
	catch(std::bad_alloc&)
	{
		// out of memory
		return 1;
	}

	// do final using maxIdx1 & maxIdx2
	ippsMul_64fc(&refSignal[maxIdx1], &m_delaySignal[maxIdx2], &signal3[size/2], size/2);
	ippsMul_64fc(&refSignal[maxIdx1+size/2], &m_delaySignal[maxIdx2+size/2], &signal3[0], size/2);
	ippsFFTFwd_CToC_64fc(&signal3[0], &signal4[0], m_pFFTSpec[3], m_pFFTBuffer[3]);
	ippsPowerSpectr_64fc(&signal4[size/2 + lowLag], &xcorr[0], numcorr);

	// Normalize
	Ipp64fVec refSquared(sampleLength);
	ippsPowerSpectr_64fc(&refCSignal[0], &refSquared[0], sampleLength);
	Ipp64fVec delSquared(sampleLength);
	ippsPowerSpectr_64fc(&delayCSignal[0], &delSquared[0], sampleLength);

	Ipp64f normRef;
	Ipp64f normDel;

	// find peak
	ippsMaxIndx(xcorr, maxXCorr, maxXCorrIndex);
	// find min
	int minXCorrIndex;
	ippsMinIndx_64f(&xcorr[0], numcorr, &minXCorr, &minXCorrIndex);  // need to normalize minXCorr

	size_t idel1;
	size_t iref1;
	size_t numterms;
	size_t i2 = std::min(delSquared.size(), refSquared.size());
	if (lowLag >= 0 || maxXCorrIndex >= -lowLag)
	{
		idel1 = maxXCorrIndex + lowLag;
		iref1 = 0;
		numterms = i2 - idel1;
	}
	else	// lowLag < 0 && i + lowLag < 0
	{
		idel1 = 0;
		iref1 = -(maxXCorrIndex + lowLag);
		numterms = i2 - iref1;
	}
	ippsSum(&refSquared[iref1], numterms, &normRef);
	ippsSum(&delSquared[idel1], numterms, &normDel);

	normRef *= normDel;
	double DFTRef = double(size) * double(size) / normRef;
	double FFTRef = DFTRef * double(size) * double(size);
	xcorr[maxXCorrIndex] *= FFTRef;
	minXCorr *= FFTRef;
	if(maxXCorrIndex > 0 && maxXCorrIndex < int(numcorr) - 1)
	{
		//
		// if low frequency resolution (high Hz/bin) indicated
		// by small maxDopplerOffsetBins, then don't do interpolation
		//
		if(maxDopplerOffsetBins <= 2)                        
		{                                                      
			xcorrMaxIndex = double(lowLag + maxXCorrIndex);  
			                                                 
			return 0;                                         
		}                                                    

		// perform interpolation around highest point
		size_t interpLength = 513;  // must be odd value
		Ipp64fVec interpIndex(interpLength);  // X value
		Ipp64fVec interpResult(interpLength);  // Y value
		Ipp64fc result[2];
		interpIndex[0] = 0.0;
		interpResult[0] = xcorr[maxXCorrIndex-1] * FFTRef;
		double startIndex = double(size / 2 + lowLag + maxXCorrIndex - 1);
		double interpInterval = double(interpLength - 1);
		Ipp64f rFreq[2];
		for(i = 1; i < interpLength - 1; i += 2)
		{
			interpIndex[i] = 2.0 * double(i) / interpInterval;
			interpIndex[i+1] = 2.0 * double(i + 1) / interpInterval;
			rFreq[0] = (startIndex + interpIndex[i]) / double(size);
			rFreq[1] = (startIndex + interpIndex[i+1]) / double(size);
			ippsGoertzTwo_64fc(&signal3[0], size, result, rFreq);
			interpResult[i] = (result[0].re * result[0].re + result[0].im * result[0].im) * DFTRef;
			interpResult[i+1] = (result[1].re * result[1].re + result[1].im * result[1].im) * DFTRef;
		}

		// find interpolation max
		int xxx;
		double yyy;
		ippsMaxIndx(interpResult, yyy, xxx);

		if(yyy < 0.1)
		{
			// use all interpolated points to perform least squares parabolic fit
			double vertexX;
			double vertexY;
			if(ParabolicFit(interpIndex, interpResult, vertexX, vertexY))
			{
				xcorr[maxXCorrIndex] = std::min(1.0, vertexY);
				xcorrMaxIndex = vertexX + startIndex - double(size / 2);
			}
			else
			{
				// indeterminent parabolic fit, use interpolation max
				xcorr[maxXCorrIndex] = std::min(1.0, yyy);
				xcorrMaxIndex = interpIndex[xxx] + startIndex - double(size / 2);
			}
		}
		else
		{
			// use interpolated points within 0.1 of maximum to perform least squares parabolic fit
			size_t xLow = xxx;
			size_t xHigh = xxx;
			do
			{
				xLow--;
			} while(interpResult[xLow] >= (yyy - 0.1) && xLow > 0);
			xLow++;
			do
			{
				xHigh++;
			} while(interpResult[xHigh] >= (yyy - 0.1) && xHigh < (interpLength - 1));
			xHigh--;
			size_t interp2Size = xHigh - xLow + 1;
			Ipp64fVec interp2Index(interp2Size);  // X value
			Ipp64fVec interp2Result(interp2Size);  // Y value
			ippsMove_64f(&interpIndex[xLow], &interp2Index[0], interp2Size); 
			ippsMove_64f(&interpResult[xLow], &interp2Result[0], interp2Size); 
			double vertexX;
			double vertexY;
			if(ParabolicFit(interp2Index, interp2Result, vertexX, vertexY))
			{
				xcorr[maxXCorrIndex] = std::min(1.0, vertexY);
				xcorrMaxIndex = vertexX + startIndex - double(size / 2);
			}
			else
			{
				// indeterminent parabolic fit, use interpolation max
				xcorr[maxXCorrIndex] = std::min(1.0, yyy);
				xcorrMaxIndex = interpIndex[xxx] + startIndex - double(size / 2);
			}
		}
	}
	else
	{
		// largest at edge, no interpolation possible
		xcorrMaxIndex = double(lowLag + maxXCorrIndex);
		return 3;
	}
 
	return 0;
}


//////////////////////////////////////////////////////////////////////
//
// Least squares parabolic fit of x, y vectors
//
_Success_(return) bool CTDOATimeDelay::ParabolicFit(_In_ const Ipp64fVec X, _In_ const Ipp64fVec Y, _Out_ double& vertexX, _Out_ double& vertexY)
// least squares parabolic fit
// inputs data coordinate vectors (X, Y) 
// outputs parabola vertex coordinates (vertexX, vertexY)
// returns success flag
{
	if(X.size() != Y.size())
	{
		// input X & Y vectors must be the same length
		// shouldn't happen
		return(false);
	}
	size_t vectorSize = X.size();
	if(vectorSize < 3)
	{
		// input X & Y vector lengths must be >= 3
		// shouldn't happen
		return(false);
	}
	Ipp64fVec XX(vectorSize);
	Ipp64f sumX;
	Ipp64f sumXX;
	Ipp64f sumXXX;
	Ipp64f sumXXXX;
	Ipp64f sumY;
	Ipp64f sumXY;
	Ipp64f sumXXY;
	ippsMul_64f(&(X[0]), &(X[0]), &(XX[0]), vectorSize);
	ippsSum_64f(&(X[0]), vectorSize, &sumX);
	ippsSum_64f(&(XX[0]), vectorSize, &sumXX);
	ippsDotProd_64f(&(X[0]), &(XX[0]), vectorSize, &sumXXX);
	ippsDotProd_64f(&(XX[0]), &(XX[0]), vectorSize, &sumXXXX);
	ippsSum_64f(&(Y[0]), vectorSize, &sumY);
	ippsDotProd_64f(&(X[0]), &(Y[0]), vectorSize, &sumXY);
	ippsDotProd_64f(&(XX[0]), &(Y[0]), vectorSize, &sumXXY);
	Ipp64f A[3*3] = {Ipp64f(vectorSize),sumX,	sumXX,
					 sumX,				sumXX,	sumXXX,
					 sumXX,				sumXXX,	sumXXXX};
	Ipp64f pBuff[3*3+3];
	Ipp64f B[3*3];
	Ipp64f d[3*1] = {	sumY,
						sumXY,
						sumXXY};
	Ipp64f e[3*1];
	int stride1 = 3 * sizeof(Ipp64f);
	int stride2 = sizeof(Ipp64f);
	IppStatus status = ippmInvert_m_64f(A, stride1, stride2, pBuff, B, stride1, stride2, 3);
	if(status != ippStsNoErr)
	{
		// matrix inverse is indeterminant for input X & Y vectors 
		return(false);
	}
	ippmMul_mm_64f(B, stride1, stride2, 3, 3, d, stride2, stride2, 1, 3, e, stride2, stride2);
	vertexX = -e[1] / (2.0 * e[2]);
	vertexY = e[2] * vertexX * vertexX + e[1] * vertexX + e[0];

	return(true);
}


//////////////////////////////////////////////////////////////////////
//
// Portion of cross correlation split out to utilize multi-core processing
//
void CTDOATimeDelay::PartitionedDopplerSearch(_In_ const size_t index)
// index	0 = shifted, 1/4 bin
//			1 = shifted, 1/2 bin
//			2 = shifted, 3/4 bin
//			3 = unshifted
{
	size_t numCorr = m_numCorr;
	int maxXCorrIndex;
	double maxXCorr;
	Ipp64fVec xcorr;
	Ipp64fc* refSignal;
	IppsFFTSpec_C_64fc* FFTSpec;
	Ipp8u* FFTBuffer;
	Ipp64fcVec signal3;
	try
	{
		signal3.resize(m_processTaskResponse.size);
		xcorr.resize(numCorr);
	}
	catch(std::bad_alloc&)
	{
		// out of memory
		m_processTaskResponse.mallocError[index] = true;
		return;
	}
	m_processTaskResponse.mallocError[index] = false;
	refSignal = &m_refSignal[index][0];
	if(m_processTaskResponse.coarse)
	{
		FFTSpec = m_pFFTSpecC[index];
		FFTBuffer = m_pFFTBufferC[index];
	}
	else
	{
		FFTSpec = m_pFFTSpec[index];
		FFTBuffer = m_pFFTBuffer[index];
	}
	size_t idx1 = m_processTaskResponse.firstIdx1;
	size_t idx2 = m_processTaskResponse.firstIdx2;
	m_processTaskResponse.max[index] = 0.0;
	m_processTaskResponse.maxIdx1[index] = m_processTaskResponse.maxDopplerOffsetBins/2;
	m_processTaskResponse.maxIdx2[index] = m_processTaskResponse.maxDopplerOffsetBins/2;
	for(;;)
	{
		ippsMul_64fc(&refSignal[idx1], &m_delaySignal[idx2], &signal3[m_processTaskResponse.size/2], m_processTaskResponse.size/2);
		ippsMul_64fc(&refSignal[idx1+m_processTaskResponse.size/2], &m_delaySignal[idx2+m_processTaskResponse.size/2], &signal3[0], m_processTaskResponse.size/2);
		ippsFFTFwd_CToC_64fc_I(&signal3[0], FFTSpec, FFTBuffer);
		ippsPowerSpectr_64fc(&signal3[m_processTaskResponse.size / 2 + m_processTaskResponse.lowLag], &xcorr[0], numCorr);
		ippsMaxIndx(xcorr, maxXCorr, maxXCorrIndex);
		if(maxXCorr > m_processTaskResponse.max[index])
		{
			m_processTaskResponse.maxIdx1[index] = idx1;
			m_processTaskResponse.maxIdx2[index] = idx2;
			m_processTaskResponse.max[index] = maxXCorr;
		}
		if(idx1 == m_processTaskResponse.lastIdx1) break;
		idx2--;
		ippsMul_64fc(&refSignal[idx1], &m_delaySignal[idx2], &signal3[m_processTaskResponse.size/2], m_processTaskResponse.size/2);
		ippsMul_64fc(&refSignal[idx1+m_processTaskResponse.size/2], &m_delaySignal[idx2+m_processTaskResponse.size/2], &signal3[0], m_processTaskResponse.size/2);
		ippsFFTFwd_CToC_64fc_I(&signal3[0], FFTSpec, FFTBuffer);
		ippsPowerSpectr_64fc(&signal3[m_processTaskResponse.size / 2 + m_processTaskResponse.lowLag], &xcorr[0], numCorr);
		ippsMaxIndx(xcorr, maxXCorr, maxXCorrIndex);
		if(maxXCorr > m_processTaskResponse.max[index])
		{
			m_processTaskResponse.maxIdx1[index] = idx1;
			m_processTaskResponse.maxIdx2[index] = idx2;
			m_processTaskResponse.max[index] = maxXCorr;
		}
		idx1++;
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Thread that processes 1/4 bin shift
//
UINT CTDOATimeDelay::Thread0(_In_ void* arg)
{
#ifdef _DEBUG
	if (IsDebuggerPresent())
	{
	// Notify debugger of thread name
	static ULONG_PTR INFO[] = { 0x1000, reinterpret_cast<ULONG_PTR>("CTDOATimeDelay::Thread0"), ULONG_PTR(-1), 0 };

	__try
	{
		RaiseException(0x406D1388, 0, _countof(INFO), INFO);
	}
#pragma warning(suppress : 6312) // EXCEPTION_CONTINUE_EXECUTION
	__except(EXCEPTION_CONTINUE_EXECUTION)
#pragma warning(suppress : 6322) // Empty _except block
	{
	}
	}
#endif

	static_cast<SThreadStruct*>(arg)->ptr->PartitionedDopplerSearch(static_cast<SThreadStruct*>(arg)->index);
	
	return 0;
}


//////////////////////////////////////////////////////////////////////
//
// Thread that processes 1/2 bin shift
//
UINT CTDOATimeDelay::Thread1(_In_ void* arg)
{
#ifdef _DEBUG
	if (IsDebuggerPresent())
	{
	// Notify debugger of thread name
	static ULONG_PTR INFO[] = { 0x1000, reinterpret_cast<ULONG_PTR>("CTDOATimeDelay::Thread1"), ULONG_PTR(-1), 0 };

	__try
	{
		RaiseException(0x406D1388, 0, _countof(INFO), INFO);
	}
#pragma warning(suppress : 6312) // EXCEPTION_CONTINUE_EXECUTION
	__except(EXCEPTION_CONTINUE_EXECUTION)
#pragma warning(suppress : 6322) // Empty _except block
	{
	}
	}
#endif

	static_cast<SThreadStruct*>(arg)->ptr->PartitionedDopplerSearch(static_cast<SThreadStruct*>(arg)->index);
	
	return 0;
}


//////////////////////////////////////////////////////////////////////
//
// Thread that processes 3/4 bin shift
//
UINT CTDOATimeDelay::Thread2(_In_ void* arg)
{
#ifdef _DEBUG
	if (IsDebuggerPresent())
	{
	// Notify debugger of thread name
	static ULONG_PTR INFO[] = { 0x1000, reinterpret_cast<ULONG_PTR>("CTDOATimeDelay::Thread2"), ULONG_PTR(-1), 0 };

	__try
	{
		RaiseException(0x406D1388, 0, _countof(INFO), INFO);
	}
#pragma warning(suppress : 6312) // EXCEPTION_CONTINUE_EXECUTION
	__except(EXCEPTION_CONTINUE_EXECUTION)
#pragma warning(suppress : 6322) // Empty _except block
	{
	}
	}
#endif

	static_cast<SThreadStruct*>(arg)->ptr->PartitionedDopplerSearch(static_cast<SThreadStruct*>(arg)->index);
	
	return 0;
}


//////////////////////////////////////////////////////////////////////
//
// Helpers
//
void CTDOATimeDelay::STDOATimeDelayInputV2<Ipp16s>::STDOADataV2<Ipp16s>::CopyData(_In_ EIqType iqType, _In_ const STDOATimeDelayInput::STDOAData& v0)
{
	ASSERT(iqType == EIqType::IQ_16SC);
	if (iqType != EIqType::IQ_16SC) return;
	if (v0.iqData) iqData = *v0.iqData;
	return;
}
void CTDOATimeDelay::STDOATimeDelayInputV2<Ipp32s>::STDOADataV2<Ipp32s>::CopyData(_In_ EIqType iqType, _In_ const STDOATimeDelayInput::STDOAData& v0)
{
	ASSERT(iqType == EIqType::IQ_32SC);
	if (iqType != EIqType::IQ_32SC) return;
	if (v0.iqData32sc) iqData = *v0.iqData32sc;
	return;
}
void CTDOATimeDelay::STDOATimeDelayInputV2<Ipp32f>::STDOADataV2<Ipp32f>::CopyData(_In_ EIqType iqType, _In_ const STDOATimeDelayInput::STDOAData& v0)
{
	ASSERT(iqType == EIqType::IQ_32FC);
	if (iqType != EIqType::IQ_32FC) return;
	if (v0.iqData32fc) iqData = *v0.iqData32fc;
	return;
}
void CTDOATimeDelay::STDOATimeDelayInputV2<Ipp64f>::STDOADataV2<Ipp64f>::CopyData(_In_ EIqType iqType, _In_ const STDOATimeDelayInput::STDOAData& v0)
{
	ASSERT(iqType == EIqType::IQ_64FC);
	if (iqType != EIqType::IQ_64FC) return;
	if (v0.iqData64fc) iqData = *v0.iqData64fc;
	return;
}

