/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include <map>

#include "ErrorCodes.h"
#include "SmsMsg.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4480)
#endif

struct SMetricsMsg
:
	SSmsMsg
{
	// Message subtypes for message types AUTOVIOLATE_CTRL, MEASURE_CTRL, OCCUPANCY_CTRL, OCCUPANCYDF_CTRL
	enum EMeasureCtrl
	{
		REQUEST_FAIL_NOT_FOUND			= ErrorCodes::REQUEST_FAIL_NOT_FOUND, // Yes, really
		AVD_FREQ_MEASURE				= 65560,
		AVD_CHANNEL_MEASURE				= 65561,
		AVD_FREQMEAS_MEASURE			= 65562,	
		AVD_BWMEAS_MEASURE				= 65563,	
		SCHED_REQUEST					= 71000,
		DELETE_REQUEST					= 71001,
		MEASUREMENT_REQUEST				= 71002,
		DWELL_REQUEST					= 71003,
		SCHED_OCCUP_REQUEST				= 71006,
		OCCUP_MEASUREMENT_REQUEST		= 71007,
		AVD_MEASUREMENT_REQUEST			= 71008,
		OCCUP_SUSPEND_REQUEST			= 71008, // from client
		OCCUP_RESUME_REQUEST			= 71009, // from client
		OCCUP_DELETE_REQUEST			= 71010, // from client
		OCCUP_TERMINATE_REQUEST			= 71011, // from client
		OCCUP_STATE_REQUEST				= 71012, // from client
		SCHED_AVD_REQUEST				= 71015,
		BAND_REQUEST					= 71016,
		BAND_REQUEST_NETTED				= 71017,
		MEASUREMENT_IQDATA_REQUEST		= 71018,
		HEADING_REQUEST					= 71020, //has 0 bodySize.
		SERVER_WORKLOAD_REQUEST_MSG		= 71100, // from client to dino
		SCHED_RESPONSE					= 72000,
		DELETE_RESPONSE					= 72001,
		MEASUREMENT_RESPONSE			= 72002,
		DWELL_RESPONSE					= 72003,
		STATE_REQUEST					= 72004,
		BIST_REQUEST					= 72005,
		BIST_RESULT_RESPONSE			= 72006, // New BIST format
		OCCUP_SUSPEND_RESPONSE			= 72008, // from dino
		AVD_SUSPEND_RESPONSE			= 72008, // from dino
		OCCUP_RESUME_RESPONSE			= 72009, // from dino
		OCCUP_DELETE_RESPONSE			= 72010, // from dino
		OCCUP_TERMINATE_RESPONSE		= 72011, // from dino
		OCCUP_STATE_RESPONSE			= 72012, // from dino
		OCCUP_STATE_CAL_UPDATE			= 72013, // from dino to calendar (no response)
		MEASUREMENT_IQDATA_RESPONSE		= 72014,
		OCC_FREQ_MEASURE				= 72046,
		OCC_CHANNEL_MEASURE				= 72047,
		EFLD_CHANNEL_MEASURE			= 72048,
		OCC_TIMEOFDAY_MEASURE			= 72049,
		MSGLEN_CHANNEL_MEASURE			= 72050,
		OCC_SPECGRAM_MEASURE			= 72051,
		MSGLEN_DIST_MEASURE				= 72052,
		OCC_EFLD_TIMEOFDAY_MEASURE		= 72053, //Field Strength vs. TOD.
		SERVER_WORKLOAD_RESPONSE_MSG	= 72101, // from dino to client
		STATE_RESPONSE					= 73004,
		BIST_SCHED_RESPONSE				= 73005,
		SCHED_OCCUP_RESPONSE			= 73006,
		OCCUP_SUSPEND_CAL_RESPONSE		= 73008, // from calendar to dino
		OCCUP_RESUME_CAL_RESPONSE		= 73009, // from calendar to dino
		OCCUP_DELETE_CAL_RESPONSE		= 73010, // from calendar to dino
		OCCUP_TERMINATE_CAL_RESPONSE	= 73011, // from calendar to dino
		OCCUP_STATE_CAL_RESPONSE		= 73012, // from calendar to dino
		SCHED_AVD_RESPONSE				= 73015,
		BAND_RESPONSE					= 73016,
		BAND_RESPONSE_NETTED			= 73017,
		HEADING_RESPONSE				= 73018,
		SERVER_LOCATION_REQUEST			= 74000,
		SERVER_LOCATION_RESPONSE		= 74001,
		SCHED_SCANDF_REQUEST			= 74006,
		SCANDF_MEASUREMENT_REQUEST		= 74007,
		SCANDF_SUSPEND_REQUEST			= 74008, // from client
		SCANDF_RESUME_REQUEST			= 74009, // from client
		SCANDF_DELETE_REQUEST			= 74010, // from client
		SCANDF_TERMINATE_REQUEST		= 74011, // from client
		SCANDF_STATE_REQUEST			= 74012, // from client
		SCHED_SCANDF_RESPONSE			= 74014,
		SCANDF_SUSPEND_RESPONSE			= 75008, // from dino
		SCANDF_RESUME_RESPONSE			= 75009, // from dino
		SCANDF_DELETE_RESPONSE			= 75010, // from dino
		SCANDF_TERMINATE_RESPONSE		= 75011, // from dino
		SCANDF_FREQ_MEASURE				= 75046,
		SCANDF_DATA_MEASURE				= 75047,
		SCANDF_DATA_NONE				= 75048,
		SCANDF_SUSPEND_CAL_RESPONSE		= 76008, // from calendar to dino
		SCANDF_RESUME_CAL_RESPONSE		= 76009, // from calendar to dino
		SCANDF_DELETE_CAL_RESPONSE		= 76010, // from calendar to dino
		SCANDF_TERMINATE_CAL_RESPONSE	= 76011, // from calendar to dino
		SCANDF_STATE_CAL_RESPONSE		= 76012, // from calendar to dino
		SCANDF_STATE_RESPONSE			= 76012, // from dino to client
		SCANDF_STATE_CAL_UPDATE			= 76013, // from dino to calendar (no response)
		AVD_SUSPEND_REQUEST				= 78008, // from client
		AVD_RESUME_REQUEST				= 78009, // from client
		AVD_DELETE_REQUEST				= 78010, // from client
		AVD_TERMINATE_REQUEST			= 78011, // from client
		AVD_STATE_REQUEST				= 78012, // from client
		AVD_RESUME_RESPONSE				= 79009, // from dino
		AVD_DELETE_RESPONSE				= 79010, // from dino
		AVD_TERMINATE_RESPONSE			= 79011, // from dino
		AVD_STATE_RESPONSE				= 79012, // from dino
		AVD_SUSPEND_CAL_RESPONSE		= 79108, // from calendar to dino
		AVD_RESUME_CAL_RESPONSE			= 79109, // from calendar to dino
		AVD_DELETE_CAL_RESPONSE			= 79110, // from calendar to dino
		AVD_TERMINATE_CAL_RESPONSE		= 79111, // from calendar to dino
		AVD_STATE_CAL_RESPONSE			= 79112, // from calendar to dino
		AVD_STATE_CAL_UPDATE			= 79113  // from dino to calendar (no response)
	};

	// Message subtypes for message type SYSTEM_STATE_CTRL
	enum ESystemStateCtrl
	{
		SET_SYSTEM_STATE			= 1,
		SET_SYSTEM_STATE_RESPONSE	= 65537
	};


	// Common enums
	enum ECustomAntenna : unsigned long
	{
		C_NO_ANTENNA = 0,	//No custom antenna connected.

		//@note: The value from ANT1 to SMPL_RF3 must match
		//		 with SSmsMsg::EAnt values.
		C_REFERENCE = 1, // ANT1 in SSmsMsg::EAnt.
		C_AUX = 2, // ANT2 in SSmsMsg::EAnt.
		C_AUX2 = 3, // ANT3 in SSmsMsg::EAnt.
		C_REF_HORIZONTAL = 4, // ANT1H in.
		C_DF_ANT_1V = 11,
		C_DF_ANT_2V = 12,
		C_DF_ANT_3V = 13,
		C_DF_ANT_4V = 14,
		C_DF_ANT_5V = 15,
		C_DF_ANT_6V = 16,
		C_DF_ANT_7V = 17,
		C_DF_ANT_8V = 18,
		C_DF_ANT_9V = 19,
		C_DF_ANT_1H = 21,
		C_DF_ANT_2H = 22,
		C_DF_ANT_3H = 23,
		C_DF_ANT_4H = 24,
		C_DF_ANT_5H = 25,
		C_DF_ANT_6H = 26,
		C_DF_ANT_7H = 27,
		C_DF_ANT_8H = 28,
		C_DF_ANT_9H = 29,
		C_SMPL_RF2 = 31,
		C_SMPL_RF3 = 32,

		C_ANT1_HF = 50,			// Non auto-switch RF HF input.

		C_ANT1_LPA = 51,		//Log periodic antenna (30 MHz - 6 GHz) sit on rotator.
		C_ANT1_647V_MON = 52,	//647 Vertical monitor output (30 MHz - 6 GHz).
		C_ANT1_647H_MON = 53,	//647 Horizontal monitor output (30 MHz - 3 GHz).

		C_HORIZONTAL_MONITOR = 80, //Horizontal polarization monitoring output.
		C_OMNI = 81,			//Omni antenna: 1 - 26 GHz.
		C_LPA = 82,			//LPA antenna: 20 MHz - 4 GHz.
		C_HORN_18GHZ = 83,	//Horn antenna: 1 GHz - 18 GHz.
		C_HORN_26GHZ = 84,	//Horn antenna: 18 GHz - 26.5 GHz.
        C_REF_HAS_HF = 85,  //Vertical Reference/HF.
		C_LPA_7GHZ = 86,	//LPA antenna: 20 MHz - 7 GHz.
		C_HORN_2_18GHZ = 87, //Horn antenna: 2 GHz - 18 GHz (Horizontal & Vertical).

		// For use with the CSMS type 'TCI5143_DUAL_AUX1':
		C_LPA_800MHZ_TO_8GHZ = 100,	// Used to rename the 'Aux 1' antenna
		C_LPA_80MHZ_TO_2GHZ = 101,	// Used to rename the 'Sample RF 2' antenna

		C_UNKNOWN_CUST_ANT = C_LPA_80MHZ_TO_2GHZ + 1 //@note: This must always be the last constant.
	};

	enum ESystemState : unsigned long
	{
		// System state is either all measurements suspended or normal
		NORMAL		= 0,
		SUSPENDED	= 1
	};

	enum EIpType : unsigned long
	{
		IP_V4		= 0,
		IP_V6		= 1
	};

	// Common constants
	static const size_t	MAX_STATIONNAME_LEN = 128;


	// Common structures
	struct SAnt2NameMap
	{
		SSmsMsg::EAnt ant;
		ECustomAntenna antNameId;
	};

	struct SClientInfo
	{
		unsigned long	clientId;	//Has 0 if BAND_REQUEST is Upconvert from version 0.
#ifdef _MSC_VER
		wchar_t
#else
		char16_t
#endif
		clientName[MAX_STATIONNAME_LEN];	//NULL terminated client name.
		EIpType			ipType;		//IP address type. 0 = v4 (4 bytes), 1 = v6 (16 bytes).
		unsigned char	ipAddr[16];	//IP address in its binary value, i.e: v4 IP address
									// 192.168.120.61: Array[] = {0xc0, 0xa8, 0x78, 0x3d}
									//@note: There is no null terminated character
									//       since the array has the actual binary value.
	};


	struct SSchedMsgOccupancyOptions
	{
		unsigned long	measureId;
		unsigned long	clientTaskId;
		double			startMeasureDateTime;
		unsigned long	clientId;
		char			clientName[MAX_STATIONNAME_LEN];
		char			ipAddr[16];		// IP address -> 123.123.123.123
	};

	struct SSchedMsgOptions // GET_BIST
	{
		unsigned long	clientTaskId;
		double			startMeasureDateTime;
		unsigned long	startMeasureTimeBar;
		unsigned long	measureDurSpanRequest;
		unsigned long	measureDurSpanMinimum;
		unsigned long	measurePriority;

		enum EMetricsCtrlCalType : unsigned long
		{
			IMMEDIATE, CONVENIENT, RIGID
		}  calRequest;

		unsigned long	measureQuantity;
		unsigned long	measureQuantityDelta;
		unsigned long	clientId;
		unsigned char	callSign[12];
		unsigned long	isTraining;		// 0 "regular" request, 1 Training Mode request
		char			clientName[MAX_STATIONNAME_LEN];
		char			ipAddr[16];		// IP address -> 123.123.123.123
	};


	// Message version data
	static const SVersionData VERSION_DATA[];

	// Constants
#ifdef _MSC_VER
	static const CString C_ANT_HORIZONTAL_MON_STR;
	static const CString C_ANT_HORN_18GHZ_STR;
	static const CString C_ANT_HORN_26GHZ_STR;
	static const CString C_ANT_HORN_2_18GHZ_STR;
	static const CString C_ANT_LPA_STR;
	static const CString C_ANT_LPA_7GHZ_STR;
	static const CString C_ANT_NOT_CONN_STR;
	static const CString C_ANT_OMNI_STR;
	static const CString C_ANT_REF_HF_STR;
	static const CString C_ANT_UNKNOWN_STR;
#endif

	// Body compression data
	typedef std::map<SVersionKey, SVersionValue> CompressionMap;
	static const SVersionData COMPRESSION_DATA[];

	// Message body structures
	struct SClientData // MEASUREMENT_ERROR_REPORT
	{
		unsigned long clientId;
		char clientName[32];
	};

	//@note: There is no SBandRequestV0 as version 0 BAND_REQUEST has no body.
	struct SBandRequestV1 // BAND_REQUEST version 1
	{
		SClientInfo		clientInfo;
	};

	typedef SBandRequestV1 SBandRequest;

	struct SGetBandRespNettedV1; // Forward declaration

	struct SGetBandRespNettedV0 // BAND_RESPONSE_NETTED V0
	{
		inline operator SGetBandRespNettedV1(void) const; // Convert to V1; definition below

		SGetBandRespV0	bandResp;
		char			stationName[MAX_STATIONNAME_LEN];
	};

	struct SGetBandRespNettedV2; // Forward declaration

	struct SGetBandRespNettedV1 // BAND_RESPONSE_NETTED V1
	{
		operator SGetBandRespNettedV0(void) const // Convert to V0
		{
			SGetBandRespNettedV0 v0;
			v0.bandResp = bandResp;
			memcpy(v0.stationName, stationName, MAX_STATIONNAME_LEN);

			return v0;
		}

		inline operator SGetBandRespNettedV2(void) const; // Convert to V2; definition below

		SGetBandRespV1	bandResp;
		char			stationName[MAX_STATIONNAME_LEN];
	};

	struct SGetBandRespNettedV3; // Forward declaration

	struct SGetBandRespNettedV2 // BAND_RESPONSE_NETTED V2
	{
		operator SGetBandRespNettedV1(void) const // Convert to V1
		{
			SGetBandRespNettedV1 v1;
			v1.bandResp = bandResp;
			memcpy(v1.stationName, stationName, MAX_STATIONNAME_LEN);

			return v1;
		}

		inline operator SGetBandRespNettedV3(void) const; // Convert to V3; definition below

		SGetBandRespV2	bandResp;
		char			stationName[MAX_STATIONNAME_LEN];
	};

	struct SGetBandRespNettedV4; // Forward declaration

	struct SGetBandRespNettedV3 // BAND_RESPONSE_NETTED V3
	{
		operator SGetBandRespNettedV2(void) const // Convert to V1
		{
			SGetBandRespNettedV2 v2;
			v2.bandResp = bandResp;
			memcpy(v2.stationName, stationName, MAX_STATIONNAME_LEN);

			return v2;
		}

		inline operator SGetBandRespNettedV4(void) const; // Convert to V4; definition below

		SGetBandRespV3	bandResp;
		char			stationName[MAX_STATIONNAME_LEN];
	};

	struct SGetBandRespNettedV5; // Forward declaration

	struct SGetBandRespNettedV4 // BAND_RESPONSE_NETTED V4
	{
		operator SGetBandRespNettedV3(void) const // Convert to V3
		{
			SGetBandRespNettedV3 v3;
			v3.bandResp = bandResp;
			memcpy(v3.stationName, stationName, MAX_STATIONNAME_LEN);

			return v3;
		}

		inline operator SGetBandRespNettedV5(void) const; // Convert to V5; definition below

		SGetBandRespV4	bandResp;
		char			stationName[MAX_STATIONNAME_LEN];
	};

	struct SGetBandRespNettedV6; // Forward declaration

	struct SGetBandRespNettedV5 // BAND_RESPONSE_NETTED V5
	{
		operator SGetBandRespNettedV4(void) const // Convert to V4
		{
			SGetBandRespNettedV4 v4;
			v4.bandResp = bandResp;
			memcpy(v4.stationName, stationName, MAX_STATIONNAME_LEN);

			return v4;
		}

		inline operator SGetBandRespNettedV6(void) const; // Convert to V6; definition below

		SGetBandRespV5	bandResp;
		char			stationName[MAX_STATIONNAME_LEN];
	};

	struct SGetBandRespNettedV6 // BAND_RESPONSE_NETTED V6
	{
		operator SGetBandRespNettedV5(void) const // Convert to V5
		{
			SGetBandRespNettedV5 v5;
			v5.bandResp = bandResp;
			memcpy(v5.stationName, stationName, MAX_STATIONNAME_LEN);

			return v5;
		}

		SGetBandRespV6	bandResp;
		char			stationName[MAX_STATIONNAME_LEN];
	};

	typedef SGetBandRespNettedV6 SGetBandRespNetted;

	struct SGetConnInfoRespV1	 // METRIC_CONN_INFO_RESP. There is no version 0.
	{
		unsigned long	numClients;// number of clients in 'clientInfo' array
		SClientInfo		clientInfo[1]; //Variable size could be 0
	};
	
	typedef SGetConnInfoRespV1 SGetConnInfoResp;
	
	struct SAntNameChangeRespV1	 // METRIC_ANT_NAME_CHANGE_RESP. There is no version 0.
	{
		unsigned long	numNames;// number of antenna to name mapping.
		SAnt2NameMap	ant2Name[1]; //Variable size as specified by numNames.
	};

	typedef SAntNameChangeRespV1 SAntNameChangeResp;

	struct SGreetingMeasurementReqMsgV0 // MEASUREMENT_GREETING_REQ
	{
		long requestPriority;
		SClientData echoClient;
	};

	typedef SGreetingMeasurementReqMsgV0 SGreetingMeasurementReqMsgV1;
	typedef SGreetingMeasurementReqMsgV1 SGreetingMeasurementReqMsg;

	struct SGreetingMeasurementRespMsgV0 // MEASUREMENT_GREETING_RES
	{
		long messageKey;
	};

	typedef SGreetingMeasurementRespMsgV0 SGreetingMeasurementRespMsgV1;
	typedef SGreetingMeasurementRespMsgV1 SGreetingMeasurementRespMsg;

	struct SMeasurementErrorReportA // MEASUREMENT_ERROR_REPORTA
	{
		unsigned long code;
		SClientData clientData;
	};

	//SMeasureCtrlMsgGenericRequest is used by these:
	// AVD_DELETE_REQUEST, AVD_MEASUREMENT_REQUEST, AVD_RESUME_REQUEST,
	// AVD_STATE_REQUEST, AVD_SUSPEND_REQUEST, AVD_TERMINATE_REQUEST,
	// DELETE_REQUEST, OCCUP_MEASUREMENT_REQUEST, MEASUREMENT_REQUEST
	// SCANDF_MEASUREMENT_REQUEST, SCANDF_DELETE_REQUEST, SCANDF_STATE_REQUEST, SCANDF_RESUME_REQUEST,
	// SCANDF_SUSPEND_REQUEST, SCANDF_TERMINATE_REQUEST
	struct SMeasureCtrlMsgGenericRequestV0
	{
		unsigned long	measureId;
		unsigned long	clientId;
	};

	typedef SMeasureCtrlMsgGenericRequestV0 SMeasureCtrlMsgGenericRequestV1; // same as V0 except body is not compressed
	typedef SMeasureCtrlMsgGenericRequestV1 SMeasureCtrlMsgGenericRequest;

	struct SMeasureAvdResultsResponseV1; // Forward declaration

	struct SMeasureAvdResultsResponseV0 // AVD_BWMEAS_MEASURE, AVD_FREQMEAS_MEASURE V0
	{
		operator SMeasureAvdResultsResponseV1(void) const; // Convert to V1; defined below

		SMeasureCtrlMsgGenericRequestV0	clientRequest;
		SAvdMeasureResultV0				equipMeasurement; // data
		ErrorCodes::EErrorCode			statusOfRequest;	// success or error status
		unsigned long					stationId;
	};

	struct SMeasureAvdResultsResponseV1 // AVD_BWMEAS_MEASURE, AVD_FREQMEAS_MEASURE V1 (variable length)
	{
		operator SMeasureAvdResultsResponseV0(void) const // Convert to V0
		{
			SMeasureAvdResultsResponseV0 v0;
			v0.clientRequest = clientRequest;
			v0.equipMeasurement = equipMeasurement;
			v0.statusOfRequest = statusOfRequest;
			v0.stationId = stationId;

			return v0;
		}

		SMeasureCtrlMsgGenericRequestV0	clientRequest;
		ErrorCodes::EErrorCode			statusOfRequest;	// success or error status
		unsigned long					stationId;
		SAvdMeasureResultV1				equipMeasurement; // data
	};

	typedef SMeasureAvdResultsResponseV1 SMeasureAvdResultsResponse;

	struct SMeasureChanResultsResponseV0 // OCC_CHANNEL_MEASURE, EFLD_CHANNEL_MEASURE, OCC_TIMEOFDAY_MEASURE
										// MSGLEN_CHANNEL_MEASURE, OCC_SPECGRAM_MEASURE, OCC_EFLD_TIMEOFDAY_MEASURE
										// AVD_CHANNEL_MEASURE
	{
		SMeasureCtrlMsgGenericRequestV0	clientRequest;
		SOccResult				equipMeasurement;
		ErrorCodes::EErrorCode	statusOfRequest;
		unsigned long			stationId;
	};
	typedef SMeasureChanResultsResponseV0 SMeasureChanResultsResponseV1;	// V1 is same as V0 but uncompressed
	typedef SMeasureChanResultsResponseV1 SMeasureChanResultsResponse;

	struct SMeasureCtrlDwellRequestV1; // Forward declaration

	struct SMeasureCtrlDwellRequestV0 // DWELL_REQUEST V0
	{
		inline operator SMeasureCtrlDwellRequestV1(void) const; // Convert to V1; defined below

		SGetDwellCmdV0	dwellRequest;
		unsigned long	clientId;
	};

	struct SMeasureCtrlDwellRequestV1 // DWELL_REQUEST V1
	{
		operator SMeasureCtrlDwellRequestV0(void) const // Convert to V0
		{
			SMeasureCtrlDwellRequestV0 v0;
			v0.dwellRequest = dwellRequest;
			v0.clientId = clientId;

			return v0;
		}

		SGetDwellCmdV1	dwellRequest;
		unsigned long	clientId;
	};

	typedef SMeasureCtrlDwellRequestV1 SMeasureCtrlDwellRequest;

	struct SMeasureCtrlDwellResponse // DWELL_RESPONSE
	{
		SGetDwellResp	dwellResponse;
		unsigned long	clientId;
		unsigned long	stationId;
	};

	struct SMeasureCtrlMsgBistResponseV1;	// Forward declaration

	struct SMeasureCtrlMsgBistResponseV0 // BIST_CTRL / BIST_RESPONSE (new BIST format)
	{
		SMeasureCtrlMsgBistResponseV0& operator=(const SMeasureCtrlMsgBistResponseV1& v1) // Convert from V1
		{
			statusOfRequest = v1.statusOfRequest;
			clientId = v1.clientId;
			stationId = v1.stationId;
			bistResponse.result = v1.bistResponse.result;
			bistResponse.textLen = v1.bistResponse.textLen;
			bistResponse.last = v1.bistResponse.last;
			memcpy(bistResponse.text, v1.bistResponse.text, v1.bistResponse.textLen * sizeof(v1.bistResponse.text[0]));

			return *this;
		}

		ErrorCodes::EErrorCode	statusOfRequest; // Success or error or last message status
		unsigned long			clientId;
		unsigned long			stationId;
		SGetBistResp			bistResponse; // Variable length
	};

	struct SMeasureCtrlMsgBistResponseV1 // // Same as V0 but not compressed
	{
		SMeasureCtrlMsgBistResponseV1& operator=(const SMeasureCtrlMsgBistResponseV0& v0) // Convert from V0
		{
			statusOfRequest = v0.statusOfRequest;
			clientId = v0.clientId;
			stationId = v0.stationId;
			bistResponse.result = v0.bistResponse.result;
			bistResponse.textLen = v0.bistResponse.textLen;
			bistResponse.last = v0.bistResponse.last;
			memcpy(bistResponse.text, v0.bistResponse.text, v0.bistResponse.textLen * sizeof(v0.bistResponse.text[0]));

			return *this;
		}

		ErrorCodes::EErrorCode	statusOfRequest; // Success or error or last message status
		unsigned long			clientId;
		unsigned long			stationId;
		SGetBistResp			bistResponse; // Variable length
	};

	typedef SMeasureCtrlMsgBistResponseV1 SMeasureCtrlMsgBistResponse;

	struct SMeasureCtrlMsgGenericResponseV0 //OCCUPANCYDF_CTRL: SCANDF_DELETE_RESPONSE
	{
		SMeasureCtrlMsgGenericRequestV0	clientRequest;
		ErrorCodes::EErrorCode			statusOfRequest;	// success or error status
		unsigned long					stationId;
	};

	typedef SMeasureCtrlMsgGenericResponseV0 SMeasureCtrlMsgGenericResponseV1;	// Same as V0, uncompressed.
	typedef SMeasureCtrlMsgGenericResponseV1 SMeasureCtrlMsgGenericResponse;

	struct SMeasureCtrlMsgOldBistResponse // BIST_CTRL / MEASUREMENT_RESPONSE (old BIST format)
	{
		SGetBistRespOld			bistResponse;
		ErrorCodes::EErrorCode	statusOfRequest; // Success or error status
		unsigned long			clientId;
		unsigned long			stationId;
	};

	struct SMeasureCtrlMsgResultsResponseV1; // Forward declaration

	struct SMeasureCtrlMsgResultsResponseV0 // MEASUREMENT_RESPONSE V0
	{
		inline operator SMeasureCtrlMsgResultsResponseV1(void) const; // Convert to V1; definition below

		SMeasureCtrlMsgGenericRequestV0	clientRequest;
		SGetMeasRespV0					equipMeasurement; // data
		ErrorCodes::EErrorCode			statusOfRequest;	// success or error status
		unsigned long					stationId;
	};

	struct SMeasureCtrlMsgResultsResponseV2; // Forward declaration

	struct SMeasureCtrlMsgResultsResponseV1 // MEASUREMENT_RESPONSE V1
	{
		operator SMeasureCtrlMsgResultsResponseV0(void) const // Convert to V0
		{
			SMeasureCtrlMsgResultsResponseV0 v0;
			v0.clientRequest = clientRequest;
			v0.equipMeasurement = equipMeasurement;
			v0.statusOfRequest = statusOfRequest;
			v0.stationId = stationId;

			return v0;
		}

		inline operator SMeasureCtrlMsgResultsResponseV2(void) const; // Convert to V2; definition below

		SMeasureCtrlMsgGenericRequestV0	clientRequest;
		SGetMeasRespV1					equipMeasurement; // data
		ErrorCodes::EErrorCode			statusOfRequest;	// success or error status
		unsigned long					stationId;
	};

	struct SMeasureCtrlMsgResultsResponseV3; // Forward declaration

	struct SMeasureCtrlMsgResultsResponseV2 // MEASUREMENT_RESPONSE V2
	{
		operator SMeasureCtrlMsgResultsResponseV1(void) const // Convert to V1
		{
			SMeasureCtrlMsgResultsResponseV1 v1;
			v1.clientRequest = clientRequest;
			v1.equipMeasurement = equipMeasurement;
			v1.statusOfRequest = statusOfRequest;
			v1.stationId = stationId;

			return v1;
		}

		inline operator SMeasureCtrlMsgResultsResponseV3(void) const; // Convert to V3; definition below

		SMeasureCtrlMsgGenericRequestV0	clientRequest;
		SGetMeasRespV2					equipMeasurement; // data
		ErrorCodes::EErrorCode			statusOfRequest;	// success or error status
		unsigned long					stationId;
	};

	struct SMeasureCtrlMsgResultsResponseV3 // MEASUREMENT_RESPONSE V3
	{
		operator SMeasureCtrlMsgResultsResponseV2(void) const // Convert to V2
		{
			SMeasureCtrlMsgResultsResponseV2 v2;
			v2.clientRequest = clientRequest;
			v2.equipMeasurement = equipMeasurement;
			v2.statusOfRequest = statusOfRequest;
			v2.stationId = stationId;

			return v2;
		}

		SMeasureCtrlMsgGenericRequestV0	clientRequest;
		SGetMeasRespV3					equipMeasurement; // data
		ErrorCodes::EErrorCode			statusOfRequest;	// success or error status
		unsigned long					stationId;
	};

	typedef SMeasureCtrlMsgResultsResponseV3 SMeasureCtrlMsgResultsResponse;

	struct SMeasureCtrlMsgIqDataRequestV0 // MEASUREMENT_IQDATA_REQUEST
	{
		unsigned long	measureId;
		unsigned long	clientId;
		unsigned long	startingPacketIndex;
	};

	typedef SMeasureCtrlMsgIqDataRequestV0 SMeasureCtrlMsgIqDataRequest;

	struct SMeasureCtrlMsgResultsIqDataResponseV1; // Forward declaration

	struct SMeasureCtrlMsgResultsIqDataResponseV0 // MEASUREMENT_IQDATA_RESPONSE V0
	{
		operator SMeasureCtrlMsgResultsIqDataResponseV1(void) const // Convert from V0 to V1
		{
			SMeasureCtrlMsgResultsIqDataResponseV1 v1;
			v1.clientRequest = clientRequest;
			v1.stationId = stationId;
			v1.iqData = iqData;

			return v1;
		}
		SMeasureCtrlMsgGenericRequestV0	clientRequest;
		unsigned long					stationId;
		SIqDataRespV0					iqData; // data
	};

	struct SMeasureCtrlMsgResultsIqDataResponseV1 // MEASUREMENT_IQDATA_RESPONSE V1
	{
		operator SMeasureCtrlMsgResultsIqDataResponseV0(void) const // Convert from V1 to V0
		{
			SMeasureCtrlMsgResultsIqDataResponseV0 v0;
			v0.clientRequest = clientRequest;
			v0.stationId = stationId;
			v0.iqData = iqData;

			return v0;
		}

		SMeasureCtrlMsgGenericRequestV0	clientRequest;
		unsigned long					stationId;
		SIqDataRespV1					iqData; // data
	};

	typedef SMeasureCtrlMsgResultsIqDataResponseV1 SMeasureCtrlMsgResultsIqDataResponse;

	struct SMeasureCtrlMsgStateResponseV0 // OCCUP_STATE_RESPONSE, AVD_STATE_RESPONSE, SCANDF_STATE_RESPONSE
	{
		unsigned long	measureId;

		enum EState : unsigned long
		{
			IDLE,
			RUNNING,
			COMPLETED,
			SUSPENDED,
			RESUMED,
			TERMINATED,
			UNKNOWN,
			FAILED
		} state;

		double					completionTime;
		double					lastUpdate;			// most recent update from equipControl
		ErrorCodes::EErrorCode	statusOfRequest;	// success or error status
		unsigned long			stationId;
	};

	typedef SMeasureCtrlMsgStateResponseV0 SMeasureCtrlMsgStateResponseV1;	// same as V0, uncompressed.
	typedef SMeasureCtrlMsgStateResponseV1 SMeasureCtrlMsgStateResponse;

	struct SMeasureCtrlMsgStationNameV0 // SERVER_LOCATION_REQUEST, BAND_REQUEST_NETTED
	{
		char stationName[MAX_STATIONNAME_LEN];
	};

	typedef SMeasureCtrlMsgStationNameV0 SMeasureCtrlMsgStationNameV1;	// same as V0, uncompressed.
	typedef SMeasureCtrlMsgStationNameV1 SMeasureCtrlMsgStationName;

	struct SMeasureErrorResponseV0	//OCCUPANCYDF_CTRL: REQUEST_FAIL_NOT_FOUND
	{
		SMeasureCtrlMsgGenericRequestV0	clientRequest;
		unsigned long			equipMeasurement;
		ErrorCodes::EErrorCode	statusOfRequest;	// success or error status
		unsigned long			stationId;
	};

	typedef SMeasureErrorResponseV0 SMeasureErrorResponseV1;	// same as V0, uncompressed.
	typedef SMeasureErrorResponseV1 SMeasureErrorResponse;

	struct SMeasureFreqResultsResponseV1; // Forward declaration

	struct SMeasureFreqResultsResponseV0 // OCC_FREQ_MEASURE, SCANDF_FREQ_MEASURE, AVD_FREQ_MEASURE V0
	{
		inline operator SMeasureFreqResultsResponseV1(void) const; // Convert to V1; defined below

		SMeasureCtrlMsgGenericRequestV0	clientRequest;
		SFrequencyVsChannelRespV0		equipMeasurement; // data
		ErrorCodes::EErrorCode			statusOfRequest;	// success or error status
		unsigned long					stationId;
	};

	struct SMeasureFreqResultsResponseV1 // OCC_FREQ_MEASURE, SCANDF_FREQ_MEASURE, AVD_FREQ_MEASURE V1 (variable length)
	{
		operator SMeasureFreqResultsResponseV0(void) const // Convert to V0
		{
			SMeasureFreqResultsResponseV0 v0;
			v0.clientRequest = clientRequest;
			v0.equipMeasurement = equipMeasurement;
			v0.statusOfRequest = statusOfRequest;
			v0.stationId = stationId;

			return v0;
		}

		SMeasureCtrlMsgGenericRequestV0	clientRequest;
		ErrorCodes::EErrorCode			statusOfRequest;	// success or error status
		unsigned long					stationId;
		SFrequencyVsChannelRespV1		equipMeasurement; // data
	};

	typedef SMeasureFreqResultsResponseV1 SMeasureFreqResultsResponse;

	struct SMeasureMsgLenDistResultsResponseV0 // MSGLEN_DIST_MEASURE
	{
		SMeasureCtrlMsgGenericRequestV0	clientRequest;
		SMsgLengthDistributionResp		equipMeasurement; // data
		ErrorCodes::EErrorCode			statusOfRequest;	// success or error status
		unsigned long					stationId;
	};

	typedef SMeasureMsgLenDistResultsResponseV0 SMeasureMsgLenDistResultsResponseV1;
	typedef SMeasureMsgLenDistResultsResponseV1 SMeasureMsgLenDistResultsResponse;

	struct SMeasureScanDfDataResultsResponseV0 // SCANDF_DATA_MEASURE
	{
		SMeasureCtrlMsgGenericRequestV0	clientRequest;
		SScanDfVsChannelResp			equipMeasurement; // data
		ErrorCodes::EErrorCode			statusOfRequest;	// success or error status
		unsigned long					stationId;
	};

	typedef SMeasureScanDfDataResultsResponseV0 SMeasureScanDfDataResultsResponseV1;	// Same as V0, uncompressed.
	typedef SMeasureScanDfDataResultsResponseV1 SMeasureScanDfDataResultsResponse;

	struct SPanMeasurementReqMsgV1; // Forward declaration

	struct SPanMeasurementReqMsgV0 // MEASUREMENT_PAN_REQ V0
	{
		inline operator SPanMeasurementReqMsgV1(void) const; // Convert to V1; defintion below

		long messageKey;
		SGetPanCmdV0 panCmd;
	};

	struct SPanMeasurementReqMsgV2; // Forward declaration

	struct SPanMeasurementReqMsgV1 // MEASUREMENT_PAN_REQ V1
	{
		operator SPanMeasurementReqMsgV0(void) const
		{
			SPanMeasurementReqMsgV0 v0;
			v0.messageKey = messageKey;
			v0.panCmd = panCmd;

			return v0;
		}

		inline operator SPanMeasurementReqMsgV2(void) const; // Convert to V2; defintion below

		long messageKey;
		SGetPanCmdV1 panCmd;
	};

	struct SPanMeasurementReqMsgV2 // MEASUREMENT_PAN_REQ V2
	{
		operator SPanMeasurementReqMsgV1(void) const
		{
			SPanMeasurementReqMsgV1 v1;
			v1.messageKey = messageKey;
			v1.panCmd = panCmd;

			return v1;
		}

		long messageKey;
		SGetPanCmdV2 panCmd;
	};

	typedef SPanMeasurementReqMsgV2 SPanMeasurementReqMsg;

	struct SRecvrCntlReqMsgV1; // Forward declaration

	struct SRecvrCntlReqMsgV0 // SET_RCVR V0
	{
		inline operator SRecvrCntlReqMsgV1(void) const; // Convert to V1; defined below

		long messageKey;
		SRcvrCtrlCmdV0 recvCmd;
	};

	struct SRecvrCntlReqMsgV1 // SET_RCVR V1
	{
		operator SRecvrCntlReqMsgV0(void) const // Convert to V0
		{
			SRecvrCntlReqMsgV0 v0;
			v0.messageKey = messageKey;
			v0.recvCmd = recvCmd;

			return v0;
		}

		long messageKey;
		SRcvrCtrlCmdV1 recvCmd;
	};

	typedef SRecvrCntlReqMsgV1 SRecvrCntlReqMsg;

	struct SPanParaReqMsgV1 // SET_PAN_PARA. There is no version 0.
	{
		long messageKey;
		SPanParaCmdV1 panCmd;
	};

	typedef SPanParaReqMsgV1 SPanParaReqMsg;

	struct SSchedMsgAutoViolateRequestV1;  // Forward declaration

	struct SSchedMsgAutoViolateRequestV0 // SCHED_AVD_REQUEST v0
	{
		inline operator SSchedMsgAutoViolateRequestV1(void) const; // Convert to V1; definition below

		SSchedMsgOccupancyOptions	clientOptions;
		SGetAutoViolateCmdV0		clientMeasure;
	};

	struct SSchedMsgAutoViolateRequestV2; // Forward declaration

	struct SSchedMsgAutoViolateRequestV1 // SCHED_AVD_REQUEST V1
	{
		operator SSchedMsgAutoViolateRequestV0(void) const // Convert to V0
		{
			SSchedMsgAutoViolateRequestV0 v0;
			v0.clientOptions = clientOptions;
			v0.clientMeasure = clientMeasure;

			return v0;
		}

		SSchedMsgOccupancyOptions	clientOptions;
		SGetAutoViolateCmdV1		clientMeasure;
	};
	
	struct SSchedMsgAutoViolateRequestV3; // Forward declaration
	struct SSchedMsgAutoViolateRequestV2 // SCHED_AVD_REQUEST V2 (variable length)
	{
		operator SSchedMsgAutoViolateRequestV1(void) const // Convert to V1
		{
			SSchedMsgAutoViolateRequestV1 v1;
			v1.clientOptions = clientOptions;
			v1.clientMeasure = clientMeasure;

			return v1;
		}

		SSchedMsgAutoViolateRequestV2& operator= (const SSchedMsgAutoViolateRequestV1 & v1) // Convert from V1
		{
			clientOptions = v1.clientOptions;
			clientMeasure = v1.clientMeasure;

			return *this;
		}

		SSchedMsgAutoViolateRequestV2& operator= (const SSchedMsgAutoViolateRequestV3 & v3); //Down-Convert from V3; definition below
			
		SSchedMsgOccupancyOptions	clientOptions;
		SGetAutoViolateCmdV2		clientMeasure;
	};

	struct SSchedMsgAutoViolateRequestV3 // SCHED_AVD_REQUEST V3 (variable length)
	{
		SSchedMsgAutoViolateRequestV3& operator= (const SSchedMsgAutoViolateRequestV2 & v2) // Convert from V2
		{
			const SSchedMsgAutoViolateRequestV2* src = &v2;

			if(static_cast<void*>(this) == static_cast<const void*>(&v2))
			{
				// Make a copy
				if((src = static_cast<const SSchedMsgAutoViolateRequestV2*>(malloc(
						offsetof(SSchedMsgAutoViolateRequestV2, clientMeasure) + SizeOf(v2.clientMeasure)))) == nullptr)
				{
					AfxThrowMemoryException();
				}

				memcpy(const_cast<SSchedMsgAutoViolateRequestV2*>(src), &v2, 
					offsetof(SSchedMsgAutoViolateRequestV2, clientMeasure) + SizeOf(v2.clientMeasure));
			}
			
			clientOptions = src->clientOptions;
			clientMeasure = src->clientMeasure;

			if(src != &v2)
			{
				free(const_cast<SSchedMsgAutoViolateRequestV2*>(src));
			}

			return *this;
		}

		SSchedMsgOccupancyOptions	clientOptions;
		SGetAutoViolateCmdV3		clientMeasure;
	};

	typedef SSchedMsgAutoViolateRequestV3 SSchedMsgAutoViolateRequest;

	struct SSchedMsgOccupancyRequestV1;  // Forward declaration

	struct SSchedMsgOccupancyRequestV0 // SCHED_OCCUP_REQUEST V0
	{
		inline operator SSchedMsgOccupancyRequestV1(void) const; // Convert to V1; definition below

		SSchedMsgOccupancyOptions	clientOptions;
		SGetOccupancyCmdV0			clientMeasure;
	};

	struct SSchedMsgOccupancyRequestV2;  // Forward declaration

	struct SSchedMsgOccupancyRequestV1 // SCHED_OCCUP_REQUEST V1
	{
		operator SSchedMsgOccupancyRequestV0(void) const // Convert to V0
		{
			SSchedMsgOccupancyRequestV0 v0;
			v0.clientOptions = clientOptions;
			v0.clientMeasure = clientMeasure;

			return v0;
		}

		inline operator SSchedMsgOccupancyRequestV2(void) const; //Up-Convert to V2; definition below
		
		SSchedMsgOccupancyOptions	clientOptions;
		SGetOccupancyCmdV1			clientMeasure;
	};

	struct SSchedMsgOccupancyRequestV3; // Forward declaration

	struct SSchedMsgOccupancyRequestV2 // SCHED_OCCUP_REQUEST V2 (variable length)
	{
		operator SSchedMsgOccupancyRequestV1(void) const // Convert to V1
		{
#ifndef CSMS_2016
			ASSERT(!IsOnStack(this) ||
				offsetof(SMetricsMsg, body.schedMsgOccupancyRequestV2.clientMeasure.band[clientMeasure.numBands]) <= sizeof(SMetricsMsg));
#endif
			SSchedMsgOccupancyRequestV1 v1;
			v1.clientOptions = clientOptions;
			v1.clientMeasure = clientMeasure;

			return v1;
		}

		SSchedMsgOccupancyRequestV2& operator =(SSchedMsgOccupancyRequestV1 v1) // Convert from V1
		{
#ifndef CSMS_2016
			ASSERT(!IsOnStack(this) ||
				offsetof(SMetricsMsg, body.schedMsgOccupancyRequestV2.clientMeasure.band[clientMeasure.numBands]) <= sizeof(SMetricsMsg));
#endif
			clientOptions = v1.clientOptions;
			clientMeasure = v1.clientMeasure;

			return *this;
		}

		SSchedMsgOccupancyRequestV2& operator =(const SSchedMsgOccupancyRequestV3& v3); // Convert from V3

		SSchedMsgOccupancyOptions	clientOptions;
		SGetOccupancyCmdV2			clientMeasure;
	private:
		// method that is disallowed due to variable size structure.
		operator SSchedMsgOccupancyRequestV3(void) const; // Convert to V3; Cannot be implemented due to variable size.
	};

	struct SSchedMsgOccupancyRequestV4; // Forward declaration

	struct SSchedMsgOccupancyRequestV3 // SCHED_OCCUP_REQUEST V3 (variable length)
	{
		SSchedMsgOccupancyRequestV3& operator =(const SSchedMsgOccupancyRequestV2& v2) // Convert from V2
		{
			const SSchedMsgOccupancyRequestV2* src = &v2;

			if(static_cast<void*>(this) == static_cast<const void*>(&v2))
			{
				std::size_t strucSize = offsetof(SSchedMsgOccupancyRequestV2, clientMeasure)
										+ SizeOf(v2.clientMeasure);
				// Make a copy
				if((src = static_cast<const SSchedMsgOccupancyRequestV2*>(malloc(strucSize))) == nullptr)
				{
					AfxThrowMemoryException();
				}

				memcpy(const_cast<SSchedMsgOccupancyRequestV2*>(src), &v2, strucSize);
			}

			clientOptions = src->clientOptions;
			clientMeasure = src->clientMeasure;

			if(src != &v2)
			{
				free(const_cast<SSchedMsgOccupancyRequestV2*>(src));
			}
			
			return *this;
		}

		SSchedMsgOccupancyRequestV3& operator =(const SSchedMsgOccupancyRequestV4& v4); // Down-Convert from V4; definition below

		SSchedMsgOccupancyOptions	clientOptions;
		SGetOccupancyCmdV3			clientMeasure;

	private:
		// method that is disallowed due to variable size structure.
		operator SSchedMsgOccupancyRequestV2(void) const; // Convert to V2;Cannot be implemented due to variable size.
	};

	struct SSchedMsgOccupancyRequestV5; // Forward declaration

	struct SSchedMsgOccupancyRequestV4 // SCHED_OCCUP_REQUEST V4 (variable length)
	{
		SSchedMsgOccupancyRequestV4& operator =(const SSchedMsgOccupancyRequestV3& v3) // Convert from V3
		{
			const SSchedMsgOccupancyRequestV3* src = &v3;

			if(static_cast<void*>(this) == static_cast<const void*>(&v3))
			{
				std::size_t strucSize = offsetof(SSchedMsgOccupancyRequestV3, clientMeasure)
										+ SizeOf(v3.clientMeasure);
				// Make a copy
				if((src = static_cast<const SSchedMsgOccupancyRequestV3*>(malloc(strucSize))) == nullptr)
				{
					AfxThrowMemoryException();
				}

				memcpy(const_cast<SSchedMsgOccupancyRequestV3*>(src), &v3, strucSize);
			}

			clientOptions = src->clientOptions;
			clientMeasure = src->clientMeasure;

			if(src != &v3)
			{
				free(const_cast<SSchedMsgOccupancyRequestV3*>(src));
			}

			return *this;
		}

		SSchedMsgOccupancyRequestV4& operator =(const SSchedMsgOccupancyRequestV5& v5); // Down-Convert from V5; definition below
		
		SSchedMsgOccupancyOptions	clientOptions;
		SGetOccupancyCmdV4			clientMeasure;

	private:
		// method that is disallowed due to variable size structure.
		operator SSchedMsgOccupancyRequestV3(void) const; // Convert to V3;Cannot be implemented due to variable size.
	};

	struct SSchedMsgOccupancyRequestV5 // SCHED_OCCUP_REQUEST V5 (variable length)
	{
		SSchedMsgOccupancyRequestV5& operator =(const SSchedMsgOccupancyRequestV4& v4) // Convert from V3
		{
			const SSchedMsgOccupancyRequestV4* src = &v4;

			if (static_cast<void*>(this) == static_cast<const void*>(&v4))
			{
				std::size_t strucSize = offsetof(SSchedMsgOccupancyRequestV4, clientMeasure)
					+ SizeOf(v4.clientMeasure);
				// Make a copy
				if ((src = static_cast<const SSchedMsgOccupancyRequestV4*>(malloc(strucSize))) == nullptr)
				{
					AfxThrowMemoryException();
				}

				memcpy(const_cast<SSchedMsgOccupancyRequestV4*>(src), &v4, strucSize);
			}

			clientOptions = src->clientOptions;
			clientMeasure = src->clientMeasure;

			if (src != &v4)
			{
				free(const_cast<SSchedMsgOccupancyRequestV4*>(src));
			}

			return *this;
		}

		SSchedMsgOccupancyOptions	clientOptions;
		SGetOccupancyCmdV5			clientMeasure;

	private:
		// method that is disallowed due to variable size structure.
		operator SSchedMsgOccupancyRequestV4(void) const; // Convert to V4;Cannot be implemented due to variable size.
	};

	typedef SSchedMsgOccupancyRequestV5 SSchedMsgOccupancyRequest;

	struct SSchedMsgRequestV1; // Forward declaration

	struct SSchedMsgRequestV0 // SCHED_REQUEST V0
	{
		inline operator SSchedMsgRequestV1(void) const; // Convert to V1; definition below

		SSchedMsgOptions	clientOptions;
		SGetMeasCmdV0		clientMeasure;
	};

	struct SSchedMsgRequestV2; // Forward declaration

	struct SSchedMsgRequestV1 // SCHED_REQUEST V1
	{
		operator SSchedMsgRequestV0(void) const // Convert to V0
		{
			SSchedMsgRequestV0 v0;
			v0.clientOptions = clientOptions;
			v0.clientMeasure = clientMeasure;

			return v0;
		}

		inline operator SSchedMsgRequestV2(void) const; // Convert to V2; definition below

		SSchedMsgOptions	clientOptions;
		SGetMeasCmdV1		clientMeasure;
	};

	struct SSchedMsgRequestV3; // Forward declaration

	struct SSchedMsgRequestV2 // SCHED_REQUEST V2
	{
		operator SSchedMsgRequestV1(void) const // Convert to V1
		{
			SSchedMsgRequestV1 v1;
			v1.clientOptions = clientOptions;
			v1.clientMeasure = clientMeasure;

			return v1;
		}

		inline operator SSchedMsgRequestV3(void) const; // Convert to V3; definition below

		SSchedMsgOptions	clientOptions;
		SGetMeasCmdV2		clientMeasure;
	};

	struct SSchedMsgRequestV4; // Forward declaration

	struct SSchedMsgRequestV3 // SCHED_REQUEST V3
	{
		operator SSchedMsgRequestV2(void) const // Convert to V2
		{
			SSchedMsgRequestV2 v2;
			v2.clientOptions = clientOptions;
			v2.clientMeasure = clientMeasure;

			return v2;
		}

		inline operator SSchedMsgRequestV4(void) const; // Convert to V4; definition below

		SSchedMsgOptions	clientOptions;
		SGetMeasCmdV3		clientMeasure;
	};

	struct SSchedMsgRequestV5; // Forward declaration

	struct SSchedMsgRequestV4 // SCHED_REQUEST V4
	{
		operator SSchedMsgRequestV3(void) const // Convert to V3
		{
			SSchedMsgRequestV3 v3;
			v3.clientOptions = clientOptions;
			v3.clientMeasure = clientMeasure;

			return v3;
		}

		inline operator SSchedMsgRequestV5(void) const; // Convert to V5; definition below

		SSchedMsgOptions	clientOptions;
		SGetMeasCmdV4		clientMeasure;
	};

	struct SSchedMsgRequestV5 // SCHED_REQUEST V5
	{
		operator SSchedMsgRequestV4(void) const // Convert to V4
		{
			SSchedMsgRequestV4 v4;
			v4.clientOptions = clientOptions;
			v4.clientMeasure = clientMeasure;

			return v4;
		}

		SSchedMsgOptions	clientOptions;
		SGetMeasCmdV5		clientMeasure;
	};

	typedef SSchedMsgRequestV5 SSchedMsgRequest;

	struct SSchedMsgResponseV0 // SCHED_RESPONSE
	{
		unsigned long	clientTaskId;
		unsigned long	measureId;
		double			startMeasure;  // scheduled start time
		double			endMeasure;		//scheduled end time

		enum EState : unsigned long
		{
			INVALID_REQUEST,
			SCHED_IN_PROGRESS,
			SCHED_FINI,
			SCHED_BUMPED,
			RESULTS_INCOMPLETE,
			RESULTS_FAIL,
			MEASURE_IN_PROGRESS,
			RESULTS_COMPLETE,
			RESULTS_RETRIEVED_BY_CLIENT,
			SCHED_CANCEL
		} state;	// state of event:  complete, retrieved, etc

		ErrorCodes::EErrorCode	statusOfRequest;	// success or error status
		unsigned long			stationId;
	};

	typedef SSchedMsgResponseV0 SSchedMsgResponseV1;	// Same as V0, uncompressed.
	typedef SSchedMsgResponseV1 SSchedMsgResponse;

	struct SSchedMsgScanDfRequestV0 // SCHED_SCANDF_REQUEST V0
	{
		SSchedMsgOccupancyOptions	clientOptions;
		SGetScanDfCmdV0				clientMeasure;
	};

	struct SSchedMsgScanDfRequestV1 // SCHED_SCANDF_REQUEST V1 (variable length)
	{
		operator SSchedMsgScanDfRequestV0(void) const // Convert to V0
		{
			SSchedMsgScanDfRequestV0 v0;
			v0.clientOptions = clientOptions;
			v0.clientMeasure = clientMeasure;

			return v0;
		}

		SSchedMsgScanDfRequestV1& operator= (SSchedMsgScanDfRequestV0 v0) // Convert from v0
		{
			clientOptions = v0.clientOptions;
			clientMeasure = v0.clientMeasure;

			return *this;
		}

		SSchedMsgOccupancyOptions	clientOptions;
		SGetScanDfCmdV1				clientMeasure;
	};

	typedef SSchedMsgScanDfRequestV1 SSchedMsgScanDfRequest;

	struct SSchedMsgScanDfResponseV0 // SCHED_SCANDF_RESPONSE V0
	{
		SSchedMsgResponseV0		schedResponse;
		SGetScanDfCmdV0			clientMeasure;
		unsigned long			clientId;
		char clientName[MAX_COMPUTERNAME_LENGTH + 1];
		char ipAddr[16];		// IP address -> 123.123.123.123
	};

	struct SSchedMsgScanDfResponseV1 // SCHED_SCANDF_RESPONSE V1 (variable length)
	{
		operator SSchedMsgScanDfResponseV0(void) const // Convert to V0
		{
			SSchedMsgScanDfResponseV0 v0;
			v0.schedResponse = schedResponse;
			v0.clientMeasure = clientMeasure;
			v0.clientId = clientId;
			strcpy_s(v0.clientName, clientName);
			strcpy_s(v0.ipAddr, ipAddr);

			return v0;
		}

		SSchedMsgScanDfResponseV1& operator= (SSchedMsgScanDfResponseV0 v0) // Convert from V0
		{
			schedResponse = v0.schedResponse;
			clientId = v0.clientId;
			strcpy_s(clientName, v0.clientName);
			strcpy_s(ipAddr, v0.ipAddr);
			clientMeasure = v0.clientMeasure;

			return *this;
		}

		SSchedMsgResponseV0		schedResponse;
		unsigned long			clientId;
		char clientName[MAX_COMPUTERNAME_LENGTH + 1];
		char ipAddr[16];		// IP address -> 123.123.123.123
		SGetScanDfCmdV1			clientMeasure;
	};

	typedef SSchedMsgScanDfResponseV1 SSchedMsgScanDfResponse;

	struct SServerGpsResponseV0 // SERVER_LOCATION_RESPONSE
	{
		ErrorCodes::EErrorCode	status;
		double					dateTime;	// using COleTime format
		double					latitude;	// degrees (+ North, - South)
		double					longitude;	// degrees (+ East, - West)
		char					stationName[MAX_STATIONNAME_LEN];
	};

	typedef SServerGpsResponseV0 SServerGpsResponseV1;	// Same as V0, uncompressed.
	typedef SServerGpsResponseV1 SServerGpsResponse;

	struct SServerWorkloadRequestV0 // SERVER_WORKLOAD_REQUEST_MSG
	{
		struct SServerWorkloadOptions
		{
			double startMeasureDateTime;
			double endMeasureDateTime;
			unsigned long clientId;			// the requesting Scorpio Client
		};

		struct  SGetWorkloadCmd
		{
			bool reqMetricsMeasurements;	// requesting Metrics or not
			bool reqAvdMeasurements;		// requesting AVD or not
			bool reqDfScanMeasurements;		// requesting DF Scan or not
			bool reqSpecOccMeasurements;	// requesting Occupancy or not
		};

		SServerWorkloadOptions	clientOptions;
		SGetWorkloadCmd			workloadRequest;
	};

	typedef SServerWorkloadRequestV0 SServerWorkloadRequestV1;		// same message, but uncompressed
	typedef SServerWorkloadRequestV1 SServerWorkloadRequest;

	struct SServerWorkloadResponseV0 // SERVER_WORKLOAD_RESPONSE
	{
		struct SMeasWorkload
		{
			static const unsigned int MAX_WORKLOAD_ITEMS = 500;

			enum EMeasWorkloadType
			{
				METRICS, 
				AVD, 
				DFSCAN, 
				OCCUPANCY
			};

			struct SMeasWorkloadInformation
			{
				unsigned long		clientId;	// the client that scheduled the task
				double				startTime;	// start time of the task
				double				endTime;	// end time of the task
				SMeasureCtrlMsgStateResponse::EState state;		// state of the task (only idle, running or suspended)
				char				clientName[MAX_COMPUTERNAME_LENGTH + 1];
				char				ipAddr[16];		// IP address -> 123.123.123.123
			};

			EMeasWorkloadType	measType;
			unsigned long		numMeasurements;// number of tasks in the 'info' array
			SMeasWorkloadInformation info[MAX_WORKLOAD_ITEMS];
		};

		ErrorCodes::EErrorCode	statusOfRequest;	// status: success (TASK_COMPLETED) or error (SERVER_FAIL_WORKLOAD)
		SMeasWorkload			metricsWorkload;	// All scheduled (within request), running & suspended metrics tasks
		SMeasWorkload			avdWorkload;		// All scheduled (within request), running & suspended AVD tasks
		SMeasWorkload			dfScanWorkload;		// All scheduled (within request), running & suspended DF Scan tasks
		SMeasWorkload			specOccWorkload;	// All scheduled (within request), running & suspended Occupancy tasks
		bool inPriorityMode;
	};

	typedef SServerWorkloadResponseV0 SServerWorkloadResponseV1;		// same message, but uncompressed
	typedef SServerWorkloadResponseV0 SServerWorkloadResponse;

	struct SSetAudioSwitchMsg // SET_SWITCH
	{
		long messageKey;
		SSetAudioSwitchCmd setSwitch;
	};

	struct SSetAutoAnswerMsg // SET_AUTOANSWER_MODE
	{
		long messageKey;
		SSetAutoAnswerCmd setAutoAns;
	};

	struct SSetPhoneHookMsg // SET_PHONE_HOOK
	{
		long messageKey;
		SSetPhoneHookCmd setPhone;
	};

	struct SSystemStateSetCtrlCmdV0 // SET_SYSTEM_STATE
	{
		ESystemState state;
	};

	typedef SSystemStateSetCtrlCmdV0 SSystemStateSetCtrlCmdV1;	// Same as V0, uncompressed.
	typedef SSystemStateSetCtrlCmdV1 SSystemStateSetCtrlCmd;

	struct  SSystemStateSetCtrlRespV0 // SET_SYSTEM_STATE_RESP
	{
		long			status; // 0 = fail, 1 = success
		ESystemState	state;
	};

	typedef SSystemStateSetCtrlRespV0 SSystemStateSetCtrlRespV1;	// Same as V0, uncompressed.
	typedef SSystemStateSetCtrlRespV1 SSystemStateSetCtrlResp;

	struct SGetAudioParamsRespV1		// There is no version 0
	{
		static const size_t MAX_AUDIO_CHANNELS = 8;
		struct SParam
		{
			unsigned long				channel;		// audio (demod) channel
			SClientInfo					clientInfo;
			Units::Frequency::Raw		freq;			// frequency in Hertz
			Units::Frequency::Raw		bandwidth;		// demod bandwidth in Hertz
			SRcvrCtrlCmdV1::EDetMode	detMode;		// OFF, AM, CW, FM, USB, LSB or EXTERNAL
			Units::Frequency::Raw		bfo;			// BFO in Hertz (+/- 8000)
			bool started;
		};
		unsigned long numChannels;
		SParam params[MAX_AUDIO_CHANNELS];
	};

	typedef SGetAudioParamsRespV1 SGetAudioParamsResp;

	// Message format
#pragma pack(push, 4)
	SHdr hdr;

	union UBody
	{
		SAntGetSetCtrlResp antGetSetCtrlResp;
		SAntGetSetCtrlRespV0 antGetSetCtrlRespV0;
		SAntGetSetCtrlRespV1 antGetSetCtrlRespV1;
		SAntSetCtrlCmd antSetCtrlCmd;
		SAntSetCtrlCmdV0 antSetCtrlCmdV0;
		SAntSetCtrlCmdV1 antSetCtrlCmdV1;
		SAudioParamsCmd audioParamsCmd;
		SAudioParamsCmdV1 audioParamsCmdV1;
		SAudioParamsCmdV2 audioParamsCmdV2;
		SAudioParamsResp audioParamsResp;
		SAudioParamsRespV1 audioParamsRespV1;
		SAudioParamsRespV2 audioParamsRespV2;
		SAudioSwitchStatusResp audioSwitchStatusResp;
		SBandRequest bandReq;
		SBandRequestV1 bandReqV1;
		SClientData clientData;
		SFreeAudioChannelCmd freeAudioChannelCmd;
		SFreeAudioChannelCmdV1 freeAudioChannelCmdV1;
		SGetAudioParamsResp getAudioParamsResp;
		SGetAudioParamsRespV1 getAudioParamsRespV1;
		SGetConnInfoResp connResp;
		SGetConnInfoRespV1 connRespV1;
		SGenericResp genericResp;
		SGenericRespV0 genericRespV0;
		SGenericRespV1 genericRespV1;
		SGetBandResp getBandResp;
		SGetBandRespV0 getBandRespV0;
		SGetBandRespV1 getBandRespV1;
		SGetBandRespV2 getBandRespV2;
		SGetBandRespV3 getBandRespV3;
		SGetBandRespV4 getBandRespV4;
		SGetBandRespV5 getBandRespV5;
		SGetBandRespV6 getBandRespV6;
		SGetBandRespNetted getBandRespNetted;
		SGetBandRespNettedV0 getBandRespNettedV0;
		SGetBandRespNettedV1 getBandRespNettedV1;
		SGetBandRespNettedV2 getBandRespNettedV2;
		SGetBandRespNettedV3 getBandRespNettedV3;
		SGetBandRespNettedV4 getBandRespNettedV4;
		SGetBandRespNettedV5 getBandRespNettedV5;
		SGetBandRespNettedV6 getBandRespNettedV6;
		SGetHeadingResp getHeadingResp; //SMetricsMsg::SGetHeadingResp version starts at 1.
		SGetFaultResp getFaultResp;
		SGetCsmsFaultResp getCsmsFaultResp;
		SGetCsmsFaultRespV1 getCsmsFaultRespV1;
		SGetCsmsFaultRespV2 getCsmsFaultRespV2;
		SGetMsgVersionsData getMsgVersionsData; // Variable length
		SGetMsgVersionsDataV0 getMsgVersionsDataV0; // Variable length
		SGetMsgVersionsDataV1 getMsgVersionsDataV1; // Variable length
		SGetPanResp getPanResp; // Variable length
		SGetPanRespV0 getPanRespV0;
		SGetPanRespV1 getPanRespV1; // Variable length
		SGreetingMeasurementReqMsg greetingMeasurementReqMsg;
		SGreetingMeasurementReqMsgV0 greetingMeasurementReqMsgV0;
		SGreetingMeasurementReqMsgV1 greetingMeasurementReqMsgV1;
		SGreetingMeasurementRespMsg greetingMeasurementRespMsg;
		SGreetingMeasurementRespMsgV0 greetingMeasurementRespMsgV0;
		SGreetingMeasurementRespMsgV1 greetingMeasurementRespMsgV1;
		SAntNameChangeResp antNameChangeResp; // Variable length
		SAntNameChangeRespV1 antNameChangeRespV1; // Variable length
		SMeasureAvdResultsResponse measureAvdResultsResponse; // Variable length
		SMeasureAvdResultsResponseV0 measureAvdResultsResponseV0;
		SMeasureAvdResultsResponseV1 measureAvdResultsResponseV1; // Variable length
		SMeasureChanResultsResponse measureChanResultsResponse;
		SMeasureChanResultsResponseV0 measureChanResultsResponseV0;
		SMeasureCtrlDwellRequest measureCtrlDwellRequest;
		SMeasureCtrlDwellRequestV0 measureCtrlDwellRequestV0;
		SMeasureCtrlDwellRequestV1 measureCtrlDwellRequestV1;
		SMeasureCtrlDwellResponse measureCtrlDwellResponse;
		SMeasureCtrlMsgBistResponse measureCtrlMsgBistResponse; // Variable length
		SMeasureCtrlMsgBistResponseV0 measureCtrlMsgBistResponseV0; // Variable length
		SMeasureCtrlMsgBistResponseV1 measureCtrlMsgBistResponseV1; // Variable length
		SMeasureCtrlMsgGenericRequest measureCtrlMsgGenericRequest;
		SMeasureCtrlMsgGenericRequestV0 measureCtrlMsgGenericRequestV0;
		SMeasureCtrlMsgGenericRequestV1 measureCtrlMsgGenericRequestV1;
		SMeasureCtrlMsgGenericResponse measureCtrlMsgGenericResponse;
		SMeasureCtrlMsgGenericResponseV0 measureCtrlMsgGenericResponseV0;
		SMeasureCtrlMsgGenericResponseV1 measureCtrlMsgGenericResponseV1;
		SMeasureCtrlMsgOldBistResponse measureCtrlMsgOldBistResponse;
		SMeasureCtrlMsgResultsResponse measureCtrlMsgResultsResponse;
		SMeasureCtrlMsgResultsResponseV0 measureCtrlMsgResultsResponseV0;
		SMeasureCtrlMsgResultsResponseV1 measureCtrlMsgResultsResponseV1;
		SMeasureCtrlMsgResultsResponseV2 measureCtrlMsgResultsResponseV2;
		SMeasureCtrlMsgResultsResponseV3 measureCtrlMsgResultsResponseV3;
		SMeasureCtrlMsgIqDataRequest measureCtrlMsgIqDataRequest;
		SMeasureCtrlMsgIqDataRequestV0 measureCtrlMsgIqDataRequestV0;
		SMeasureCtrlMsgResultsIqDataResponse measureCtrlMsgResultsIqDataResponse; // Variable length
		SMeasureCtrlMsgResultsIqDataResponseV0 measureCtrlMsgResultsIqDataResponseV0; // Variable length
		SMeasureCtrlMsgResultsIqDataResponseV1 measureCtrlMsgResultsIqDataResponseV1; // Variable length
		SMeasureCtrlMsgStationName measureCtrlMsgStationName;
		SMeasureCtrlMsgStationNameV0 measureCtrlMsgStationNameV0;
		SMeasureCtrlMsgStationNameV1 measureCtrlMsgStationNameV1;
		SMeasureCtrlMsgStateResponse measureCtrlMsgStateResponse;
		SMeasureCtrlMsgStateResponseV0 measureCtrlMsgStateResponseV0;
		SMeasureCtrlMsgStateResponseV1 measureCtrlMsgStateResponseV1;
		SMeasureErrorResponse measureErrorResponse;
		SMeasureErrorResponseV0 measureErrorResponseV0;
		SMeasureErrorResponseV1 measureErrorResponseV1;
		SMeasureFreqResultsResponse measureFreqResultsResponse; // Variable length
		SMeasureFreqResultsResponseV0 measureFreqResultsResponseV0;
		SMeasureFreqResultsResponseV1 measureFreqResultsResponseV1; // Variable length
		SMeasurementErrorReportA measurementErrorReportA;
		SMeasureMsgLenDistResultsResponse measureMsgLenDistResultsResponse;
		SMeasureMsgLenDistResultsResponseV0 measureMsgLenDistResultsResponseV0;
		SMeasureScanDfDataResultsResponse measureScanDfDataResultsResponse;
		SMeasureScanDfDataResultsResponseV0 measureScanDfDataResultsResponseV0;
		SMeasureScanDfDataResultsResponseV1 measureScanDfDataResultsResponseV1;
//		SPanCapabilitiesRequest panCapabilitiesRequest;
//		SPanCapabilitiesResponse panCapabilitiesResponse;;
		SPanMeasurementReqMsg panMeasurementReqMsg;
		SPanMeasurementReqMsgV0 panMeasurementReqMsgV0;
		SPanMeasurementReqMsgV1 panMeasurementReqMsgV1;
		SPanMeasurementReqMsgV2 panMeasurementReqMsgV2;
		SRecvrCntlReqMsg recvrCntlReqMsg;
		SRecvrCntlReqMsgV0 recvrCntlReqMsgV0;
		SRecvrCntlReqMsgV1 recvrCntlReqMsgV1;
		SPanParaReqMsg panParaReqMsg;
		SPanParaReqMsgV1 panParaReqMsgV1;
		SSchedMsgAutoViolateRequest schedMsgAutoViolateRequest; // Variable length
		SSchedMsgAutoViolateRequestV0 schedMsgAutoViolateRequestV0;
		SSchedMsgAutoViolateRequestV1 schedMsgAutoViolateRequestV1;
		SSchedMsgAutoViolateRequestV2 schedMsgAutoViolateRequestV2; // Variable length
		SSchedMsgAutoViolateRequestV3 schedMsgAutoViolateRequestV3; // Variable length
		SSchedMsgOccupancyRequest schedMsgOccupancyRequest; // Variable length
		SSchedMsgOccupancyRequestV0 schedMsgOccupancyRequestV0;
		SSchedMsgOccupancyRequestV1 schedMsgOccupancyRequestV1;
		SSchedMsgOccupancyRequestV2 schedMsgOccupancyRequestV2; // Variable length
		SSchedMsgOccupancyRequestV3 schedMsgOccupancyRequestV3; // Variable length
		SSchedMsgOccupancyRequestV4 schedMsgOccupancyRequestV4; // Variable length
		SSchedMsgOccupancyRequestV5 schedMsgOccupancyRequestV5; // Variable length
		SSchedMsgOptions schedMsgOptions;
		SSchedMsgRequest schedMsgRequest;
		SSchedMsgRequestV0 schedMsgRequestV0;
		SSchedMsgRequestV1 schedMsgRequestV1;
		SSchedMsgRequestV2 schedMsgRequestV2;
		SSchedMsgRequestV3 schedMsgRequestV3;
		SSchedMsgRequestV4 schedMsgRequestV4;
		SSchedMsgRequestV5 schedMsgRequestV5;
		SSchedMsgResponse schedMsgResponse;
		SSchedMsgResponseV0 schedMsgResponseV0;
		SSchedMsgResponseV1 schedMsgResponseV1;
		SSchedMsgScanDfRequest schedMsgScanDfRequest; // Variable length
		SSchedMsgScanDfRequestV0 schedMsgScanDfRequestV0;
		SSchedMsgScanDfRequestV1 schedMsgScanDfRequestV1; // Variable length
		SSchedMsgScanDfResponse schedMsgScanDfResponse; // Variable length
		SSchedMsgScanDfResponseV0 schedMsgScanDfResponseV0;
		SSchedMsgScanDfResponseV1 schedMsgScanDfResponseV1; // Variable length
		SServerGpsResponse serverGpsResponse;
		SServerGpsResponseV0 serverGpsResponseV0;
		SServerGpsResponseV1 serverGpsResponseV1;
		SServerWorkloadRequest serverWorkloadRequest;
		SServerWorkloadRequestV0 serverWorkloadRequestV0;
		SServerWorkloadRequestV1 serverWorkloadRequestV1;
		SServerWorkloadResponse serverWorkloadResponse;
		SServerWorkloadResponseV0 serverWorkloadResponseV0;
		SServerWorkloadResponseV1 serverWorkloadResponseV1;
		SSetAudioSwitchMsg setAudioSwitchMsg;
		SSetAutoAnswerCmd setAutoAnswerCmd;
		SSetAutoAnswerMsg setAutoAnswerMsg;
		SRcvrLanCmd rcvrLanCmd;
		SSetPhoneHookMsg setPhoneHookMsg;
		SSystemStateSetCtrlCmd systemStateSetCtrlCmd;
		SSystemStateSetCtrlCmdV0 systemStateSetCtrlCmdV0;
		SSystemStateSetCtrlCmdV1 systemStateSetCtrlCmdV1;
		SSystemStateSetCtrlResp systemStateSetCtrlResp;
		SSystemStateSetCtrlRespV0 systemStateSetCtrlRespV0;
		SSystemStateSetCtrlRespV1 systemStateSetCtrlRespV1;
		SSounderSetupCmd sounderSweepParamsSetCmd;
		SSounderSetupCmdV1 sounderSweepParamsSetCmdV1;
		SBlankerListCmd sounderBlankerFreqParamsSetCmd;
		SBlankerListCmdV1 sounderBlankerFreqParamsSetCmdV1;
		SSounderSweepCtrlCmd sounderSweepOnOffCtrlCmd;
		SSounderSweepCtrlCmdV1 sounderSweepOnOffCtrlCmdV1;
		SSounderSelfTestCmd sounderSelfTestCmd;
		SSounderSelfTestCmdV1 sounderSelfTestCmdV1;
		SGetSounderStatusResp sounderStatusResp;
		SGetSounderStatusRespV1 sounderStatusRespV1;
		SSounderGenericResp   sounderGenericResp;
		SSounderGenericRespV1   sounderGenericRespV1;

	} body;
#pragma pack(pop)

#ifdef _MSC_VER
	static const CString & GetAntennaString(ECustomAntenna id);
#endif
	
	// Reallocate memory
	static void Realloc(SMetricsMsg*& msg, size_t size)
	{
		void* newMsg = realloc(msg, size);
	
		if(newMsg == NULL)
		{
			ASSERT(FALSE);
			throw std::bad_alloc();
		}

		msg = static_cast<SMetricsMsg*>(newMsg);
	}

#ifdef _DEBUG
	static void Realloc(SMetricsMsg*& msg, size_t size, LPCSTR fileName, int line)
	{
		void* newMsg = _realloc_dbg(msg, size, _NORMAL_BLOCK, fileName, line);
	
		if(newMsg == NULL)
		{
			ASSERT(FALSE);
			throw std::bad_alloc();
		}

		msg = static_cast<SMetricsMsg*>(newMsg);
	}
#endif
};

#ifdef _MSC_VER
//////////////////////////////////////////////////////////////////////
/// <summary>
/// Returns String description of the antenna id.
/// </summary>
/// <param name="id">
/// Has the antenna identifier.
/// </param>
/// <returns>
/// const reference to the antenna string description.
/// </returns>
inline const CString & SMetricsMsg::GetAntennaString(ECustomAntenna id)
{
	switch (id)
	{
	case C_NO_ANTENNA:
		return(C_ANT_NOT_CONN_STR);

	case C_HORIZONTAL_MONITOR:
		return(C_ANT_HORIZONTAL_MON_STR);

	case C_OMNI:
		return(C_ANT_OMNI_STR);

	case C_LPA:
		return(C_ANT_LPA_STR);

	case C_HORN_18GHZ:
		return(C_ANT_HORN_18GHZ_STR);

	case C_HORN_26GHZ:
		return(C_ANT_HORN_26GHZ_STR);

	case C_REF_HAS_HF:
		return(C_ANT_REF_HF_STR);

	case C_LPA_7GHZ:
		return(C_ANT_LPA_7GHZ_STR);

	case C_HORN_2_18GHZ:
		return(C_ANT_HORN_2_18GHZ_STR);

	default:
		ASSERT(false);
		return(C_ANT_UNKNOWN_STR);
	}
}
#endif

// Version conversion operators
inline SMetricsMsg::SGetBandRespNettedV0::operator SMetricsMsg::SGetBandRespNettedV1(void) const // Convert to V1
{
	SGetBandRespNettedV1 v1;
	v1.bandResp = bandResp;
	memcpy(v1.stationName, stationName, MAX_STATIONNAME_LEN);

	return v1;
}

inline SMetricsMsg::SGetBandRespNettedV1::operator SMetricsMsg::SGetBandRespNettedV2(void) const // Convert to V2
{
	SGetBandRespNettedV2 v2;
	v2.bandResp = bandResp;
	memcpy(v2.stationName, stationName, MAX_STATIONNAME_LEN);

	return v2;
}

inline SMetricsMsg::SGetBandRespNettedV2::operator SMetricsMsg::SGetBandRespNettedV3(void) const // Convert to V3
{
	SGetBandRespNettedV3 v3;
	v3.bandResp = bandResp;
	memcpy(v3.stationName, stationName, MAX_STATIONNAME_LEN);

	return v3;
}

inline SMetricsMsg::SGetBandRespNettedV3::operator SMetricsMsg::SGetBandRespNettedV4(void) const // Convert to V4
{
	SGetBandRespNettedV4 v4;
	v4.bandResp = bandResp;
	memcpy(v4.stationName, stationName, MAX_STATIONNAME_LEN);

	return v4;
}

inline SMetricsMsg::SGetBandRespNettedV4::operator SMetricsMsg::SGetBandRespNettedV5(void) const // Convert to V5
{
	SGetBandRespNettedV5 v5;
	v5.bandResp = bandResp;
	memcpy(v5.stationName, stationName, MAX_STATIONNAME_LEN);

	return v5;
}

inline SMetricsMsg::SGetBandRespNettedV5::operator SMetricsMsg::SGetBandRespNettedV6(void) const // Convert to V6
{
	SGetBandRespNettedV6 v6;
	v6.bandResp = bandResp;
	memcpy(v6.stationName, stationName, MAX_STATIONNAME_LEN);

	return v6;
}
inline SMetricsMsg::SMeasureAvdResultsResponseV0::operator SMetricsMsg::SMeasureAvdResultsResponseV1(void) const // Convert to V1
{
	SMeasureAvdResultsResponseV1 v1;
	v1.clientRequest = clientRequest;
	v1.statusOfRequest = statusOfRequest;
	v1.stationId = stationId;
	v1.equipMeasurement = equipMeasurement;

	return v1;
}

inline SMetricsMsg::SMeasureCtrlDwellRequestV0::operator SMetricsMsg::SMeasureCtrlDwellRequestV1(void) const // Convert to V1
{
	SMeasureCtrlDwellRequestV1 v1;
	v1.dwellRequest = dwellRequest;
	v1.clientId = clientId;

	return v1;
}

inline SMetricsMsg::SMeasureCtrlMsgResultsResponseV0::operator SMetricsMsg::SMeasureCtrlMsgResultsResponseV1(void) const // Convert to V1
{
	SMeasureCtrlMsgResultsResponseV1 v1;
	v1.clientRequest = clientRequest;
	v1.equipMeasurement = equipMeasurement;
	v1.statusOfRequest = statusOfRequest;
	v1.stationId = stationId;

	return v1;
}

inline SMetricsMsg::SMeasureCtrlMsgResultsResponseV1::operator SMetricsMsg::SMeasureCtrlMsgResultsResponseV2(void) const // Convert to V2
{
	SMeasureCtrlMsgResultsResponseV2 v2;
	v2.clientRequest = clientRequest;
	v2.equipMeasurement = equipMeasurement;
	v2.statusOfRequest = statusOfRequest;
	v2.stationId = stationId;

	return v2;
}

inline SMetricsMsg::SMeasureCtrlMsgResultsResponseV2::operator SMetricsMsg::SMeasureCtrlMsgResultsResponseV3(void) const // Convert to V3
{
	SMeasureCtrlMsgResultsResponseV3 v3;
	v3.clientRequest = clientRequest;
	v3.equipMeasurement = equipMeasurement;
	v3.statusOfRequest = statusOfRequest;
	v3.stationId = stationId;

	return v3;
}

inline SMetricsMsg::SMeasureFreqResultsResponseV0::operator SMetricsMsg::SMeasureFreqResultsResponseV1(void) const // Convert to V1
{
	SMeasureFreqResultsResponseV1 v1;
	v1.clientRequest = clientRequest;
	v1.equipMeasurement = equipMeasurement;
	v1.statusOfRequest = statusOfRequest;
	v1.stationId = stationId;

	return v1;
}

inline SMetricsMsg::SPanMeasurementReqMsgV0::operator SMetricsMsg::SPanMeasurementReqMsgV1(void) const // Convert to V1
{
	SPanMeasurementReqMsgV1 v1;
	v1.messageKey = messageKey;
	v1.panCmd = panCmd;

	return v1;
}

inline SMetricsMsg::SPanMeasurementReqMsgV1::operator SMetricsMsg::SPanMeasurementReqMsgV2(void) const // Convert to V2
{
	SPanMeasurementReqMsgV2 v2;
	v2.messageKey = messageKey;
	v2.panCmd = panCmd;

	return v2;
}

inline SMetricsMsg::SRecvrCntlReqMsgV0::operator SMetricsMsg::SRecvrCntlReqMsgV1(void) const // Convert to V1
{
	SRecvrCntlReqMsgV1 v1;
	v1.messageKey = messageKey;
	v1.recvCmd = recvCmd;

	return v1;
}

inline SMetricsMsg::SSchedMsgAutoViolateRequestV0::operator SMetricsMsg::SSchedMsgAutoViolateRequestV1(void) const // Convert to V1
{
	SSchedMsgAutoViolateRequestV1 v1;
	v1.clientOptions = clientOptions;
	v1.clientMeasure = clientMeasure;

	return v1;
}

inline SMetricsMsg::SSchedMsgAutoViolateRequestV2& 
	SMetricsMsg::SSchedMsgAutoViolateRequestV2::operator =
	(const SSchedMsgAutoViolateRequestV3 & v3)
{
	const SSchedMsgAutoViolateRequestV3* src = &v3;

	if(static_cast<void*>(this) == static_cast<const void*>(&v3))
	{
		// Make a copy
		if((src = static_cast<const SSchedMsgAutoViolateRequestV3*>(malloc(
			offsetof(SSchedMsgAutoViolateRequestV3, clientMeasure) + SizeOf(v3.clientMeasure)))) == nullptr)
		{
			AfxThrowMemoryException();
		}

		memcpy(const_cast<SSchedMsgAutoViolateRequestV3*>(src), &v3, 
					offsetof(SSchedMsgAutoViolateRequestV3, clientMeasure) + SizeOf(v3.clientMeasure));
	}
			
	clientOptions = src->clientOptions;
	clientMeasure = src->clientMeasure;

	if(src != &v3)
	{
		free(const_cast<SSchedMsgAutoViolateRequestV3*>(src));
	}

	return *this;
}

inline SMetricsMsg::SSchedMsgOccupancyRequestV0::operator SMetricsMsg::SSchedMsgOccupancyRequestV1(void) const // Convert to V1
{
	SSchedMsgOccupancyRequestV1 v1;
	v1.clientOptions = clientOptions;
	v1.clientMeasure = clientMeasure;

	return v1;
}

inline SMetricsMsg::SSchedMsgOccupancyRequestV1::operator SMetricsMsg::SSchedMsgOccupancyRequestV2(void) const //Up-Convert to V2
{
	SSchedMsgOccupancyRequestV2 v2;
	v2.clientOptions = clientOptions;
	v2.clientMeasure = clientMeasure;

	return v2;
}

inline SMetricsMsg::SSchedMsgOccupancyRequestV2& SMetricsMsg::SSchedMsgOccupancyRequestV2::operator =(const SSchedMsgOccupancyRequestV3& v3) // Convert from V3
{
	const SSchedMsgOccupancyRequestV3* src = &v3;

	if(static_cast<void*>(this) == static_cast<const void*>(&v3))
	{
		std::size_t strucSize = offsetof(SSchedMsgOccupancyRequestV3, clientMeasure)
										+ SizeOf(v3.clientMeasure);
		// Make a copy
		if((src = static_cast<const SSchedMsgOccupancyRequestV3*>(malloc(strucSize))) == nullptr)
		{
			AfxThrowMemoryException();
		}

		memcpy(const_cast<SSchedMsgOccupancyRequestV3*>(src), &v3, strucSize);
	}

	clientOptions = src->clientOptions;
	clientMeasure = src->clientMeasure;

	if(src != &v3)
	{
		free(const_cast<SSchedMsgOccupancyRequestV3*>(src));
	}

	return *this;
}

inline SMetricsMsg::SSchedMsgOccupancyRequestV3& SMetricsMsg::SSchedMsgOccupancyRequestV3::operator =(const SSchedMsgOccupancyRequestV4& v4) // Down-Convert from V3
{
	const SSchedMsgOccupancyRequestV4* src = &v4;

	if(static_cast<void*>(this) == static_cast<const void*>(&v4))
	{
		std::size_t strucSize = offsetof(SSchedMsgOccupancyRequestV4, clientMeasure)
										+ SizeOf(v4.clientMeasure);
		// Make a copy
		if((src = static_cast<const SSchedMsgOccupancyRequestV4*>(malloc(strucSize))) == nullptr)
		{
			AfxThrowMemoryException();
		}

		memcpy(const_cast<SSchedMsgOccupancyRequestV4*>(src), &v4, strucSize);
	}

	clientOptions = src->clientOptions;
	clientMeasure = src->clientMeasure;

	if(src != &v4)
	{
		free(const_cast<SSchedMsgOccupancyRequestV4*>(src));
	}

	return *this;
}

//////////////////////////////////////////////////////////////////////
/// <summary>
/// operator = that converts SSchedMsgOccupancyRequestV5 to
///	 SSchedMsgOccupancyRequestV4.
/// </summary>
/// <param name="v5">
/// SSchedMsgOccupancyRequestV5 to be converted.
/// </param>
/// <returns>
/// Reference to converted SSchedMsgOccupancyRequestV4.
/// </returns>
/// <remarks>
/// @note: If both SSchedMsgOccupancyRequestV5 & SSchedMsgOccupancyRequestV4 occupied the
///		   same RAM, SSchedMsgOccupancyRequestV5 will be transfer to dynamic allocated
///		   RAM before conversion.
/// </remarks>
inline SMetricsMsg::SSchedMsgOccupancyRequestV4& SMetricsMsg::SSchedMsgOccupancyRequestV4::operator =(const SSchedMsgOccupancyRequestV5& v5)
{
	const SSchedMsgOccupancyRequestV5* src = &v5;

	if (static_cast<void*>(this) == static_cast<const void*>(&v5))
	{
		std::size_t strucSize = offsetof(SSchedMsgOccupancyRequestV5, clientMeasure)
			+ SizeOf(v5.clientMeasure);
		// Make a copy
		if ((src = static_cast<const SSchedMsgOccupancyRequestV5*>(malloc(strucSize))) == nullptr)
		{
			AfxThrowMemoryException();
		}

		memcpy(const_cast<SSchedMsgOccupancyRequestV5*>(src), &v5, strucSize);
	}

	clientOptions = src->clientOptions;
	clientMeasure = src->clientMeasure;

	if (src != &v5)
	{
		free(const_cast<SSchedMsgOccupancyRequestV5*>(src));
	}

	return *this;

}

inline SMetricsMsg::SSchedMsgRequestV0::operator SMetricsMsg::SSchedMsgRequestV1(void) const // Convert to V1
{
	SSchedMsgRequestV1 v1;
	v1.clientOptions = clientOptions;
	v1.clientMeasure = clientMeasure;

	return v1;
}

inline SMetricsMsg::SSchedMsgRequestV1::operator SMetricsMsg::SSchedMsgRequestV2(void) const // Convert to V2
{
	SSchedMsgRequestV2 v2;
	v2.clientOptions = clientOptions;
	v2.clientMeasure = clientMeasure;

	return v2;
}

inline SMetricsMsg::SSchedMsgRequestV2::operator SMetricsMsg::SSchedMsgRequestV3(void) const // Convert to V3
{
	SSchedMsgRequestV3 v3;
	v3.clientOptions = clientOptions;
	v3.clientMeasure = clientMeasure;

	return v3;
}

inline SMetricsMsg::SSchedMsgRequestV3::operator SMetricsMsg::SSchedMsgRequestV4(void) const // Convert to V4
{
	SSchedMsgRequestV4 v4;
	v4.clientOptions = clientOptions;
	v4.clientMeasure = clientMeasure;

	return v4;
}

inline SMetricsMsg::SSchedMsgRequestV4::operator SMetricsMsg::SSchedMsgRequestV5(void) const // Convert to V5
{
	SSchedMsgRequestV5 v5;
	v5.clientOptions = clientOptions;
	v5.clientMeasure = clientMeasure;

	return v5;
}

// Message versions (selectany lets this go in the .h file)
#ifdef _MSC_VER
__declspec(selectany)
#else
__attribute__((__weak__))
#endif
const SMetricsMsg::SVersionData SMetricsMsg::VERSION_DATA[] =
{
	{ { SMetricsMsg::ANT_CTRL, SMetricsMsg::GET_ANT }, { 0, 1 } },
	{ { SMetricsMsg::ANT_CTRL, SMetricsMsg::GET_ANT_RESPONSE }, { 0, 1 } },
	{ { SMetricsMsg::ANT_CTRL, SMetricsMsg::SET_ANT }, { 1, 1 } },
	{ { SMetricsMsg::ANT_CTRL, SMetricsMsg::SET_ANT_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_BWMEAS_MEASURE }, { 0, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_CHANNEL_MEASURE }, { 0, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_DELETE_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_DELETE_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_FREQ_MEASURE }, { 0, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_FREQMEAS_MEASURE }, { 0, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_MEASUREMENT_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_RESUME_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_STATE_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_STATE_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_SUSPEND_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_TERMINATE_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::REQUEST_FAIL_NOT_FOUND }, { 0, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::SCHED_AVD_REQUEST }, { 3, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::SCHED_AVD_RESPONSE }, { 3, 1 } },
	{ { SMetricsMsg::BIST_CTRL, SMetricsMsg::MEASUREMENT_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::BIST_CTRL, SMetricsMsg::BIST_RESULT_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::BIST_CTRL, SMetricsMsg::BIST_SCHED_RESPONSE }, { 0, 1 } },
	{ { SMetricsMsg::BIST_CTRL, SMetricsMsg::GET_BIST }, { 1, 1 } },
	{ { SMetricsMsg::BIST_CTRL, SMetricsMsg::GET_DIAGNOSTICS }, { 1, 1 } },
#ifndef SMS_SRV_BUILT
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::GET_AUDIO_PARAMS }, { 1, 1 } },
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::GET_AUDIO_PARAMS_RESP }, { 1, 1 } },
#endif
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::SET_RCVR }, { 1, 1 } },
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::SET_RCVR_RESP }, { 1, 1 } },
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::SET_PAN_PARA }, { 1, 1 } },
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::SET_PAN_PARA_RESP }, { 1, 1 } },
#ifndef SMS_SRV_BUILT
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::SET_AUDIO_PARAMS }, { 2, 2 } },
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::SET_AUDIO_PARAMS_RESP }, { 2, 2 } },
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::FREE_AUDIO_CHANNEL }, { 1, 1 } },
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::FREE_AUDIO_CHANNEL_RESP }, { 1, 1 } },
#endif
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::BAND_REQUEST }, { 1, 6 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::BAND_REQUEST_NETTED }, { 1, 6 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::BAND_RESPONSE }, { 1, 6 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::BAND_RESPONSE_NETTED }, { 1, 6 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::DELETE_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::DELETE_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::HEADING_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::HEADING_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::DWELL_REQUEST }, { 1, 0 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::MEASUREMENT_IQDATA_REQUEST }, { 0, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::MEASUREMENT_IQDATA_RESPONSE }, { 0, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::MEASUREMENT_REQUEST }, { 1, 3 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::MEASUREMENT_RESPONSE }, { 1, 3 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::SCHED_REQUEST }, { 5, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::SCHED_RESPONSE }, { 5, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::SERVER_LOCATION_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::SERVER_LOCATION_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::SERVER_WORKLOAD_REQUEST_MSG }, { 1, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::SERVER_WORKLOAD_RESPONSE_MSG }, { 1, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::REQUEST_FAIL_NOT_FOUND }, { 0, 1 } },
#ifndef SMS_SRV_BUILT
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::DWELL_RESPONSE }, { 1, 0 } },
#endif
	{ { SMetricsMsg::MEASUREMENT_ERROR_REPORTA, 0 }, { 1, 1 } },
	{ { SMetricsMsg::MEASUREMENT_GREETING_REQ, 0 }, { 1, 1 } },
	{ { SMetricsMsg::MEASUREMENT_GREETING_RES, 0 }, { 1, 1 } },
	{ { SMetricsMsg::MEASUREMENT_PAN_REQ, SMetricsMsg::GET_PAN }, { 2, 1 } },
	{ { SMetricsMsg::MEASUREMENT_PAN_REQ, SMetricsMsg::GET_PAN_AGC }, { 2, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCC_FREQ_MEASURE }, { 0, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCC_CHANNEL_MEASURE }, { 0, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::EFLD_CHANNEL_MEASURE }, { 0, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCC_TIMEOFDAY_MEASURE }, { 0, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::MSGLEN_CHANNEL_MEASURE }, { 0, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCC_SPECGRAM_MEASURE }, { 0, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::MSGLEN_DIST_MEASURE }, { 0, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCC_EFLD_TIMEOFDAY_MEASURE }, { 0, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_MEASUREMENT_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_DELETE_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_DELETE_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::SCHED_OCCUP_REQUEST }, { 5, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::SCHED_OCCUP_RESPONSE }, { 5, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_STATE_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_STATE_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_RESUME_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_SUSPEND_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_TERMINATE_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::REQUEST_FAIL_NOT_FOUND }, { 0, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_FREQ_MEASURE }, { 0, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_DATA_MEASURE }, { 0, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_MEASUREMENT_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_DELETE_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_DELETE_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCHED_SCANDF_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCHED_SCANDF_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_STATE_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_STATE_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_RESUME_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_SUSPEND_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_TERMINATE_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::REQUEST_FAIL_NOT_FOUND }, { 0, 1 } },
	{ { SMetricsMsg::PAN_DISP_CTRL, SMetricsMsg::GET_PAN_RESPONSE }, { 2, 1 } },
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::GET_MSG_VERSIONS }, { 1, 1 } },
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::GET_MSG_VERSIONS_RESP }, { 1, 1 } },
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::GET_FAULT_REQUEST }, { 1, 1 } },
#ifndef CSMS_SRV_BUILT
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::GET_FAULT_RESP }, { 1, 1 } },
#endif
#ifndef SMS_SRV_BUILT
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::GET_CSMS_FAULT_RESP }, { 1, 2 } },
#endif
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::METRIC_CONN_INFO_GET }, { 1, 1 } },
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::METRIC_CONN_INFO_RESP }, { 1, 1 } },
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::METRIC_ANT_NAME_CHANGE_REQUEST }, { 1, 1 } },
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::METRIC_ANT_NAME_CHANGE_RESP }, { 1, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_SWEEP_SETUP }, { 1, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_SETUP_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_BLANKER_FREQ }, { 1, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_BLANKER_FREQ_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_START_STOP }, { 1, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_START_STOP_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_TEST }, { 1, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_TEST_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_IONOREQUEST }, { 1, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_IONO_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_STATUS_REQ }, { 1, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_STATUS_RESPONSE }, { 1, 1 } },
	{ { SMetricsMsg::SYSTEM_STATE_CTRL, SMetricsMsg::SET_SYSTEM_STATE }, { 1, 1 } },
	{ { SMetricsMsg::SYSTEM_STATE_CTRL, SMetricsMsg::SET_SYSTEM_STATE_RESPONSE }, { 1, 1 } }
};

// Minimum uncompressed body versions (default is compressed). 0xff => N/A
//********* Note: Once the msg entry is made, it must not be changed,			*******
//*********       i.e.: Newer msg version is defined and this table has older   *******
//*********             version number. The older version number must stay here.*******
#ifdef _MSC_VER
__declspec(selectany)
#else
__attribute__((__weak__))
#endif
const SMetricsMsg::SVersionData SMetricsMsg::COMPRESSION_DATA[] =
{
	{ { SMetricsMsg::ANT_CTRL, SMetricsMsg::SET_ANT }, { 1, 0xff } },
	{ { SMetricsMsg::ANT_CTRL, SMetricsMsg::GET_ANT_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::ANT_CTRL, SMetricsMsg::SET_ANT_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_BWMEAS_MEASURE }, { 0xff, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_CHANNEL_MEASURE }, { 0xff, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_DELETE_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_DELETE_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_FREQ_MEASURE }, { 0xff, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_FREQMEAS_MEASURE }, { 0xff, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_MEASUREMENT_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_RESUME_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_STATE_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_STATE_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_SUSPEND_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::AVD_TERMINATE_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::REQUEST_FAIL_NOT_FOUND }, { 0xff, 1 } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::SCHED_AVD_REQUEST }, { 2, 0xff } },
	{ { SMetricsMsg::AUTOVIOLATE_CTRL, SMetricsMsg::SCHED_AVD_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::BIST_CTRL, SMetricsMsg::MEASUREMENT_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::BIST_CTRL, SMetricsMsg::BIST_RESULT_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::BIST_CTRL, SMetricsMsg::BIST_SCHED_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::BIST_CTRL, SMetricsMsg::GET_BIST }, { 1, 0xff } },
	{ { SMetricsMsg::BIST_CTRL, SMetricsMsg::GET_DIAGNOSTICS }, { 1, 0xff } },
#ifndef SMS_SRV_BUILT
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::GET_AUDIO_PARAMS }, { 1, 0xff } },
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::GET_AUDIO_PARAMS_RESP }, { 0xff, 1 } },
#endif
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::SET_RCVR }, { 1, 0xff } },
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::SET_RCVR_RESP }, { 0xff, 1 } },
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::SET_PAN_PARA }, { 1, 0xff } },
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::SET_PAN_PARA_RESP }, { 0xff, 1 } },
#ifndef SMS_SRV_BUILT
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::SET_AUDIO_PARAMS }, { 1, 0xff } },
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::SET_AUDIO_PARAMS_RESP }, { 0xff, 1 } },
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::FREE_AUDIO_CHANNEL }, { 1, 0xff } },
	{ { SMetricsMsg::DEMOD_CTRL, SMetricsMsg::FREE_AUDIO_CHANNEL_RESP }, { 0xff, 1 } },
#endif
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::BAND_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::BAND_REQUEST_NETTED }, { 1, 0xff } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::BAND_RESPONSE }, { 0xff, 2 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::BAND_RESPONSE_NETTED }, { 0xff, 2 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::DELETE_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::DELETE_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::HEADING_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::HEADING_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::DWELL_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::MEASUREMENT_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::MEASUREMENT_RESPONSE }, { 0xff, 2 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::MEASUREMENT_IQDATA_REQUEST }, { 0, 0xff } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::MEASUREMENT_IQDATA_RESPONSE }, { 0xff, 0 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::SCHED_REQUEST }, { 2, 0xff } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::SCHED_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::SERVER_LOCATION_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::SERVER_LOCATION_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::SERVER_WORKLOAD_REQUEST_MSG }, { 1, 0xff } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::SERVER_WORKLOAD_RESPONSE_MSG }, { 0xff, 1 } },
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::REQUEST_FAIL_NOT_FOUND }, { 0xff, 1 } },
#ifndef SMS_SRV_BUILT
	{ { SMetricsMsg::MEASURE_CTRL, SMetricsMsg::DWELL_RESPONSE }, { 1, 0xff } },
#endif
	{ { SMetricsMsg::MEASUREMENT_ERROR_REPORTA, 0 }, { 0xff, 1 } },
	{ { SMetricsMsg::MEASUREMENT_GREETING_REQ, 0 }, { 1, 0xff } },
	{ { SMetricsMsg::MEASUREMENT_GREETING_RES, 0 }, { 0xff, 1 } },
	{ { SMetricsMsg::MEASUREMENT_PAN_REQ, SMetricsMsg::GET_PAN }, { 1, 0xff } },
	{ { SMetricsMsg::MEASUREMENT_PAN_REQ, SMetricsMsg::GET_PAN_AGC }, { 1, 0xff } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCC_FREQ_MEASURE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCC_CHANNEL_MEASURE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::EFLD_CHANNEL_MEASURE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCC_TIMEOFDAY_MEASURE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::MSGLEN_CHANNEL_MEASURE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCC_SPECGRAM_MEASURE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::MSGLEN_DIST_MEASURE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCC_EFLD_TIMEOFDAY_MEASURE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_MEASUREMENT_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_DELETE_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_DELETE_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::SCHED_OCCUP_REQUEST }, { 3, 0xff } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::SCHED_OCCUP_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_STATE_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_STATE_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_RESUME_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_SUSPEND_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::OCCUP_TERMINATE_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::OCCUPANCY_CTRL, SMetricsMsg::REQUEST_FAIL_NOT_FOUND }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_FREQ_MEASURE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_DATA_MEASURE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_MEASUREMENT_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_DELETE_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_DELETE_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCHED_SCANDF_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCHED_SCANDF_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_STATE_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_STATE_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_RESUME_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_SUSPEND_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::SCANDF_TERMINATE_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::OCCUPANCYDF_CTRL, SMetricsMsg::REQUEST_FAIL_NOT_FOUND }, { 0xff, 1 } },
	{ { SMetricsMsg::PAN_DISP_CTRL, SMetricsMsg::GET_PAN_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::GET_MSG_VERSIONS }, { 1, 0xff } },
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::GET_MSG_VERSIONS_RESP }, { 0xff, 1 } },
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::GET_FAULT_REQUEST }, { 1, 0xff } },
#ifndef CSMS_SRV_BUILT
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::GET_FAULT_RESP }, { 0xff, 1 } },
#endif
#ifndef SMS_SRV_BUILT
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::GET_CSMS_FAULT_RESP }, { 0xff, 1 } },
#endif
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::METRIC_CONN_INFO_GET }, { 1, 0xff } },
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::METRIC_CONN_INFO_RESP }, { 0xff, 1 } },
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::METRIC_ANT_NAME_CHANGE_REQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::STATUS_CTRL, SMetricsMsg::METRIC_ANT_NAME_CHANGE_RESP }, { 0xff, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_SWEEP_SETUP }, {1, 0xff } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_SETUP_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_BLANKER_FREQ }, { 1, 0xff } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_BLANKER_FREQ_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_START_STOP }, {1, 0xff } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_START_STOP_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_TEST }, { 1, 0xff } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_TEST_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_IONOREQUEST }, { 1, 0xff } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_IONO_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_STATUS_REQ }, { 1, 0xff } },
	{ { SMetricsMsg::SOUNDER_CTRL, SMetricsMsg::SOUNDER_STATUS_RESPONSE }, { 0xff, 1 } },
	{ { SMetricsMsg::SYSTEM_STATE_CTRL, SMetricsMsg::SET_SYSTEM_STATE }, { 1, 0xff } },
	{ { SMetricsMsg::SYSTEM_STATE_CTRL, SMetricsMsg::SET_SYSTEM_STATE_RESPONSE }, { 0xff, 1 } }
};
//*********					Read the note above.                       			*******

//////////////////////////////////////////////////////////////////////
//
// Static data
//
#ifdef _MSC_VER
__declspec(selectany) const CString SMetricsMsg::C_ANT_HORIZONTAL_MON_STR = _T("Horizontal Ref");
__declspec(selectany) const CString SMetricsMsg::C_ANT_HORN_18GHZ_STR = _T("HORN 1-18GHz");
__declspec(selectany) const CString SMetricsMsg::C_ANT_HORN_26GHZ_STR = _T("HORN 18-26.5GHz");
__declspec(selectany) const CString SMetricsMsg::C_ANT_HORN_2_18GHZ_STR = _T("HORN 2-18GHz");
__declspec(selectany) const CString SMetricsMsg::C_ANT_LPA_STR = _T("LPA .02-4GHz");
__declspec(selectany) const CString SMetricsMsg::C_ANT_LPA_7GHZ_STR = _T("LPA .02-7GHz");
__declspec(selectany) const CString SMetricsMsg::C_ANT_OMNI_STR = _T("OMNI 1-26.5GHz");
__declspec(selectany) const CString SMetricsMsg::C_ANT_NOT_CONN_STR = _T("Antenna parts not connected.");
__declspec(selectany) const CString SMetricsMsg::C_ANT_REF_HF_STR = _T("Vertical Ref/HF");
__declspec(selectany) const CString SMetricsMsg::C_ANT_UNKNOWN_STR = _T("Antenna type Unknown");
#endif

#ifdef _MSC_VER
#pragma warning(pop)
#endif
