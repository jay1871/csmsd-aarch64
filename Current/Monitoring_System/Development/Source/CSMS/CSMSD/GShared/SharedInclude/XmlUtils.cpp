#include "stdafx.h"
#include "XmlUtils.h"

#include <Windows.h>	// For ::MessageBox (remove this #include if decide not to use message box - probably the case), also no need to link with User32.lib
#include <msxml6.h>		// xml
#include <assert.h>
#include <wchar.h>		// for _wtoi64, ...
#include <ObjBase.h>	// for CoInitialize and CoUninitialize, ...
#include <OleAuto.h>	// for SysAllocString and SysFreeString, ...

#include <iostream>

#include "shlobj.h"		// for SHCreateDirectoryEx
#include "shlwapi.h"	// for PathFileExists


TciXmlUtils::CTciXmlResp::CTciXmlResp(const TCHAR* inputFilename, const TCHAR* outputFilename)
	: _pXMLDom(NULL), _validXmlInputFile(false), _siteType(TciXmlUtils::SITETYPE_FIXED), 
	  _siteHeight(0), _hOutputFile(INVALID_HANDLE_VALUE), _measStatus(MEAS_SUCCESS),
      _siteLong(0.0), _siteLat(0.0)
{
	// Sanity checks.
	if (!inputFilename)
	{
		throw std::runtime_error("Input file path is empty.");
	}
	if (!PathFileExists(inputFilename))
	{
		char charStr[1000];
		#if defined(UNICODE) || defined(_UNICODE)
			sprintf_s(charStr, sizeof(charStr), "Input file does not exist:  %S", inputFilename);
		#else
			sprintf_s(charStr, sizeof(charStr), "Input file does not exist:  %s", inputFilename);
		#endif	// defined(UNICODE) || defined(_UNICODE)

		throw std::runtime_error(charStr);
	}

	_inputXmlFilename.clear();
	_inputXmlFilename += inputFilename;

	if (outputFilename)
	{
		// Client provided an output (XML) filename, so go ahead and create an output (XML) file with that name
		_outputXmlFilename = outputFilename;	// up to client to make sure this is a valid filename, valid path
	}
	else
	{
		// Client did not provide an output (XML) filename, so go create an output (XML) file based on the input filename
		_outputXmlFilename = _inputXmlFilename;
		_outputXmlFilename.erase(_outputXmlFilename.length() - 6, 6);	// erase the "-O.xml" at the end
		_outputXmlFilename += _T("-R.xml");								// replace it with "-R.xml"
	}

	// Create the output file folder, if it does not exit yet.
	tstring filePath(_outputXmlFilename);
	size_t pos = filePath.find_last_of(_T("\\/"));
	tstring outputFolderPath = filePath.substr(0, pos); 
	if (!PathFileExists(outputFolderPath.c_str()))
	{
		switch (SHCreateDirectoryEx(NULL, outputFolderPath.c_str(), NULL))
		{
			case ERROR_SUCCESS:
				// Success in creating the output folder.
				break;
			case ERROR_BAD_PATHNAME:
				// The pszPath parameter was set to a relative path.
				throw std::runtime_error("The given path was set to a relative path.");
				break;
			case ERROR_FILENAME_EXCED_RANGE:
				// The path pointed to by pszPath is too long.
				throw std::runtime_error("The given path is too long.");
				break;
			case ERROR_PATH_NOT_FOUND:
				// The system cannot find the path pointed to by pszPath. The path may contain an invalid entry.
				throw std::runtime_error("The system cannot find the given path. The path may contain an invalid entry.");
				break;
			default:
				throw std::runtime_error("An unexpected error has occurred in creating output file folder.");
				break;
			}
	}
	
	// Create the output XML file.
	_hOutputFile = CreateFile(_outputXmlFilename.c_str(),	// name of the output XML file to be written
								GENERIC_WRITE,				// open for writing
								0,							// do not share
								NULL,						// default security
								CREATE_ALWAYS,				// create a new file, always
								FILE_ATTRIBUTE_NORMAL,		// normal file
								NULL);						// no attr. template
	if (INVALID_HANDLE_VALUE != _hOutputFile)
	{
#ifdef CALL_COINITIALIZE
		HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);	//Still need to link with Ole32.lib (try to use Ole32.dll later)
		if (SUCCEEDED(hr))
#endif
		{
			_tcharStr[0]		    = 
			_orderType[0]		    = 
			_orderName[0]		    = 
            _orderSender[0]         =
			_orderSenderPc[0]	    = 
			_orderCreator[0]	    = 
			_orderAddressee[0]	    = 
			_orderVersion[0]	    = 
			_orderId[0]			    = 0;

            // Set the default OrderSender, just in case the order itself doesn't have one and the calling code did not set it either.
            _tcsncpy_s(_orderSenderDefault, _countof(_orderSenderDefault), _T("SMS4DC"), _TRUNCATE);

			// Get time-zone information; use when formatting date/time for result XML files (they include "+-H:MM")
			TIME_ZONE_INFORMATION timeZoneInfo;
			GetTimeZoneInformation(&timeZoneInfo);
			_timeZonePlus = (timeZoneInfo.Bias <= 0);	// true: time zone is UTC + some time, false: time zone is UTC - some time
			_timeZoneHours	 = abs(timeZoneInfo.Bias) / 60;
			_timeZoneMinutes = abs(timeZoneInfo.Bias) % 60;
		}
#ifdef CALL_COINITIALIZE
		else
		{
			throw std::runtime_error("Error from CoInitialize");
		}
#endif
	}
	else
	{
		DWORD errorCode = GetLastError();
		LPVOID lpMsgBuf;
		DWORD bufLen = FormatMessageA(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			errorCode,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			/*(LPTSTR)*/ (LPSTR) &lpMsgBuf,
			0, NULL );
		if (bufLen)
		{
			LPCSTR lpMsgStr = (LPCSTR)lpMsgBuf;
			std::string result(lpMsgStr, lpMsgStr+bufLen);
      		LocalFree(lpMsgBuf);

			char charStr[1000];
#if defined(UNICODE) || defined(_UNICODE)
			sprintf_s(charStr, sizeof(charStr), "Failed to create output xml file %S\n%s\nError Code = %ld", _outputXmlFilename.c_str(), result.c_str(), errorCode);
#else
			sprintf_s(charStr, sizeof(charStr), "Failed to create output xml file %s\n%s\nError Code = %ld", _outputXmlFilename.c_str(), result.c_str(), errorCode);
#endif	// defined(UNICODE) || defined(_UNICODE)

			throw std::runtime_error(charStr);
		}
		else
		{
			throw std::runtime_error("Failed to create output xml file");
		}
	}
}

TciXmlUtils::CTciXmlResp::~CTciXmlResp()
{
	if (INVALID_HANDLE_VALUE != _hOutputFile)
	{
		// Get file size before the file handle is gone.
		DWORD dwOutputFileSize = GetFileSize(_hOutputFile, NULL);

		CloseHandle(_hOutputFile);

		// Delete the output file, if it's empty.
		if (PathFileExists(_outputXmlFilename.c_str()) && (dwOutputFileSize == 0))
		{
			DeleteFile(_outputXmlFilename.c_str());
		}
	}

	SAFE_RELEASE(_pXMLDom);

#ifdef CALL_COINITIALIZE
	CoUninitialize();
#endif
}

void TciXmlUtils::CTciXmlResp::SiteLocation(double siteLat, double siteLong)
{
    // Do some basic value validation, to provent potential buffer overflow when writing data into XML
    // e.g. in CTciXmlGspResp::writeMssLong() or CTciXmlGspResp::writeMssLat() with oversized data values
    if ((siteLat >= -90.0) && (siteLat <= 90.0))
    {
        _siteLat = siteLat;
    }
    if ((siteLong >= -180.0) && (siteLong <= 180.0))
    {
        _siteLong = siteLong;
    }
}

bool TciXmlUtils::CTciXmlResp::loadDOMRaw(TCHAR* inputXmlFilename)
{
	bool bSuccess = false;
    _pXMLDom = NULL;		// This member will be non-NULL if successfully loaded the XML file

    IXMLDOMParseError *pXMLErr = NULL;

    BSTR bstrErr = NULL;
    VARIANT_BOOL varStatus;
    VARIANT varFileName;
    VariantInit(&varFileName);

    if (SUCCEEDED(createAndInitDOM(&_pXMLDom)))
	{
		// XML file name to load
		if (SUCCEEDED(variantFromString(inputXmlFilename, varFileName)) &&	
			SUCCEEDED(_pXMLDom->load(varFileName, &varStatus)))
		{
            bSuccess = (varStatus == VARIANT_TRUE);
            if (!bSuccess)
            {
				// Failed to load xml, get last parsing error
				TCHAR tcharError[1000];
				if (SUCCEEDED(_pXMLDom->get_parseError(&pXMLErr)) && SUCCEEDED(pXMLErr->get_reason(&bstrErr)))
				{
					#if defined(UNICODE) || defined(_UNICODE)
						_stprintf_s(tcharError, _T("Failed to load DOM from xml File: %s. Error: %s"), inputXmlFilename, bstrErr);
					#else
						tstring strErr;
						TciXmlUtils::BstrToStdString(bstrErr, strErr);
						_stprintf_s(tcharError, _T("Failed to load DOM from xml File: %s. Error: %s"), inputXmlFilename, strErr.c_str());
					#endif
				}
				else
				{
					_stprintf_s(tcharError, _T("Failed to load DOM from xml File: %s. Could not identify the problem"), inputXmlFilename);
				}

				::MessageBox(NULL, tcharError, _T("Error"), MB_OK);	// Do we even want to show a message box?

				SysFreeString(bstrErr);
			}
		}

		SAFE_RELEASE(pXMLErr);
	}

    VariantClear(&varFileName);

	_validXmlInputFile = bSuccess;
	return bSuccess;
}


// If pParentOfField is NULL, this will search for, and return only the FIRST occurrence of the key pFieldToRead (and fill in pFieldValue)
// If pParentOfField is not NULL, this will search for, and return the FIRST occurrence fo the key pFieldToRead inside a pParentOfField section
//
// If sizeFieldValue is not large enough to strcpy into pFieldValue, the program will just crash (would change this behavior by using the 
//	function _set_invalid_parameter_handler() to set the invalid parameter handler to my own function, rather than than default (invoke the 
//	Watson crash reporting, along with a failed assertion in debug mode)
bool TciXmlUtils::CTciXmlResp::getFieldValue(TCHAR* pFieldToRead, TCHAR* pFieldValue, short sizeFieldValue, TCHAR* pParentOfField)
{
	bool bSuccess = false;

	*pFieldValue = 0;	// null-terminate to start (if fails to read, then this will be null string)

	if (_pXMLDom)
	{
		// Input (request) XML file was successfully opened and read into memory using XML DOM (MSXML6 COM interface).
		//	Now try to find the value in the section/key specified

		// Process xml file here
		IXMLDOMNodeList* pIDOMNodeList = NULL;
		IXMLDOMNode* pIDOMNode = NULL;

		// Finds a specific key (and all subkeys); shows all of the keys' values
		try
		{
			long value;
			tstring strFindText(pFieldToRead);

			#if defined(UNICODE) || defined(_UNICODE)
                HRESULT hr2 = _pXMLDom->getElementsByTagName((TCHAR*)strFindText.c_str(), &pIDOMNodeList);
			#else
				BSTR bstr = TciXmlUtils::BstrFromTchar(pFieldToRead);	// todo when to call SysFreeString?
                HRESULT hr2 = _pXMLDom->getElementsByTagName(bstr, &pIDOMNodeList);
			#endif
	
			SUCCEEDED(hr2) ? 0 : throw hr2;

			hr2 = pIDOMNodeList->get_length(&value);
			if (SUCCEEDED(hr2))
			{
				BSTR bstrItemText;
				pIDOMNodeList->reset();

				IXMLDOMNode* pIDOMNodeParent = NULL;
				BSTR bstrParentNodeName;
				bool bFound = false;

				for (long i = 0; i < value; i++)
				{
					pIDOMNodeList->get_item(i, &pIDOMNode);
					if (pIDOMNode)
					{
						//Same as pFieldToRead, so don't need to look it up: pIDOMNode->get_nodeName(&bstrNodeName);	// get the <key> name 
						
						if (!pParentOfField)
						{
							bFound = true;
						}
						else
						{
							// If the parent is == pParentOfField, then we have found the correct pFieldToRead
							pIDOMNode->get_parentNode(&pIDOMNodeParent);
							if (pIDOMNodeParent)
							{
								pIDOMNodeParent->get_nodeName(&bstrParentNodeName);	// get the parent's <key> name 

								// can remove #if here and call BstrToStdString for both (different definitions of that function)
								#if 1
									bFound = (0 == /*wcscmp*/ _tcscmp(TciXmlUtils::BstrToStdString(bstrParentNodeName).c_str(), pParentOfField)) ? true : false;
								#else
									#if defined(UNICODE) || defined(_UNICODE)
										bFound = (0 == /*wcscmp*/ _tcscmp(bstrParentNodeName, pParentOfField)) ? true : false;
									#else
										bFound = (0 == /*wcscmp*/ _tcscmp(TciXmlUtils::BstrToStdString(bstrParentNodeName).c_str(), pParentOfField)) ? true : false;
									#endif
								#endif
							}
						}

						if (bFound)
						{
							pIDOMNode->get_text(&bstrItemText);	// get the key's value ( <key> value </key> )

							// Can remove #if here and call BstrToStdString for both (different definitions of that function)
							#if 1
								/*wcscpy_s*/ _tcscpy_s(pFieldValue, sizeFieldValue, TciXmlUtils::BstrToStdString(bstrItemText).c_str());
							#else
								#if defined(UNICODE) || defined(_UNICODE)
									/*wcscpy_s*/ _tcscpy_s(pFieldValue, sizeFieldValue, bstrItemText);
								#else
									/*wcscpy_s*/ _tcscpy_s(pFieldValue, sizeFieldValue, TciXmlUtils::BstrToStdString(bstrItemText).c_str());
								#endif
							#endif

							bSuccess = true;

							pIDOMNode->Release();
							pIDOMNode = NULL;
							break;
						}
					}
				}

                if (pIDOMNodeParent)
                {
                    pIDOMNodeParent->Release();
                }
			}
		}
		catch(...)
		{
		}

        if (pIDOMNodeList)
        {
            pIDOMNodeList->Release();
        }
        if (pIDOMNode)
        {
            pIDOMNode->Release();
        }
	}

	return bSuccess;
}

//snm-test-read entire section into a /*std::wstring*/ tstring (do not hold onto the pFieldToRead (the section) <key> or </key>, that will be done outside while writing back to the xml result file)
//  this is so we can format a little better. Also this function does not read comments, attributes, etc.., only the '<key>value</key>' items inside the <pFieldToRead> </pFieldToRead>.
//	Also, this function can only handle simple '<key>value</key>' items, not other sections inside of those value(s).
bool TciXmlUtils::CTciXmlResp::readSection(const TCHAR* pFieldToRead, /*std::wstring*/ tstring& sectionToFill, TCHAR* pParentOfField)
{
	bool bSuccess = false;

	sectionToFill.clear();

	if (_pXMLDom)
	{
		// Input (request) XML file was successfully opened and read into memory using XML DOM (MSXML6 COM interface).
		//	Now try to find the value in the section/key specified

		// Process xml file here
		IXMLDOMNodeList* pIDOMNodeList = NULL;
		IXMLDOMNode* pIDOMNode = NULL;

		// finds a specific key (and all subkeys); shows all of the keys' values
		try
		{
			long value;
			tstring strFindText(pFieldToRead);

			HRESULT hr2;
			#if defined(UNICODE) || defined(_UNICODE)
				hr2 = _pXMLDom->getElementsByTagName((TCHAR*)strFindText.c_str(), &pIDOMNodeList);
			#else
				BSTR bstr = TciXmlUtils::BstrFromTchar(pFieldToRead);	// todo when to call SysFreeString?
				hr2 = _pXMLDom->getElementsByTagName(bstr, &pIDOMNodeList);
			#endif
			SUCCEEDED(hr2) ? 0 : throw hr2;

			hr2 = pIDOMNodeList->get_length(&value);
			if (SUCCEEDED(hr2))
			{
				pIDOMNodeList->reset();

				IXMLDOMNode* pIDOMNodeParent = NULL;
				BSTR bstrParentNodeName;
				bool bFound = false;

				for (int i = 0; i < value; i++)
				{
					pIDOMNodeList->get_item(i, &pIDOMNode);
					if (pIDOMNode)
					{
						if (!pParentOfField)
						{
							bFound = true;
						}
						else
						{
							// If the parent is == pParentOfField, then we have found the correct pFieldToRead
							pIDOMNode->get_parentNode(&pIDOMNodeParent);
							if (pIDOMNodeParent)
							{
								pIDOMNodeParent->get_nodeName(&bstrParentNodeName);	// Get the parent's <key> name 

								// Can remove #if here and call BstrToStdString for both (different definitions of that function)
                                bFound = (0 == _tcscmp(TciXmlUtils::BstrToStdString(bstrParentNodeName).c_str(), pParentOfField)) ? true : false;
							}
						}

						if (bFound)
						{
							// Should really make a (recursively called) function that reads/processed the <key>value<key>, given 
							//	the key name. That way, could call for nested <key>value<key> information inside a value.
							DOMNodeType nodeType;
							pIDOMNode->get_nodeType(&nodeType);
							switch (nodeType)
							{
								case NODE_ELEMENT:
								{
									TCHAR partsOfSection[512];

									// Don't include the looked-up <key> in the returned string, this will be done outside this function (so appropriate tabs can be prepended)

									// Process Child nodes
									IXMLDOMNodeList* TempXMLNodeList;
									pIDOMNode->get_childNodes(&TempXMLNodeList);
									
									long numChildren;
									TempXMLNodeList->get_length(&numChildren);

									BSTR bstrChildNodeName;
									BSTR bstrChildNodeValue;

									bool bIgnoreNode = false;	// Could ignore (not maintain) specific "<key> value </key>" data if we wanted
									IXMLDOMNode* pIDOMNodeChild = NULL;
									for (int j=0; j<numChildren; ++j)
									{
										TempXMLNodeList->get_item(j, &pIDOMNodeChild);

										pIDOMNodeChild->get_nodeName(&bstrChildNodeName);
										pIDOMNodeChild->get_text(&bstrChildNodeValue);	// get the key's value ( <key> value </key> ).

										if (!bIgnoreNode)
										{
											if (j+1 < numChildren)
											{
												#if defined(UNICODE) || defined(_UNICODE)
													_stprintf_s(partsOfSection, _T("<%s>%s</%s>\r\n%c"), bstrChildNodeName, bstrChildNodeValue, bstrChildNodeName, '\t');	// later tokenize based on \t, replacing with the correct number of tabs (to look nice - keep the levels intact when writing this back to the order response file)
												#else
													tstring strNodeName, strNodeValue;
													TciXmlUtils::BstrToStdString(bstrChildNodeName, strNodeName);
													TciXmlUtils::BstrToStdString(bstrChildNodeValue, strNodeValue);
													_stprintf_s(partsOfSection, _T("<%s>%s</%s>\r\n%c"), strNodeName.c_str(), strNodeValue.c_str(), strNodeName.c_str(), '\t');	// later tokenize based on \t, replacing with the correct number of tabs (to look nice - keep the levels intact when writing this back to the order response file)
												#endif

											}
											else
											{
												#if defined(UNICODE) || defined(_UNICODE)
													_stprintf_s(partsOfSection, _T("<%s>%s</%s>\r\n"), bstrChildNodeName, bstrChildNodeValue, bstrChildNodeName);	// don't want to add tabs after the last child (<key> </key>) pair in this section
												#else
													tstring strNodeName, strNodeValue;
													TciXmlUtils::BstrToStdString(bstrChildNodeName, strNodeName);
													TciXmlUtils::BstrToStdString(bstrChildNodeValue, strNodeValue);
													_stprintf_s(partsOfSection, _T("<%s>%s</%s>\r\n"), strNodeName.c_str(), strNodeValue.c_str(), strNodeName.c_str());	// don't want to add tabs after the last child (<key> </key>) pair in this section
												#endif
											}

											sectionToFill += partsOfSection;
										}
									}

									// Don't include the looked-up </key> in the returned string, this will be done outside this function (so appropriate tabs can be prepended)

									break;
								}
								
								case NODE_ATTRIBUTE:
								{
									// could potentially hold onto all attributes too
									break;
								}

								case NODE_TEXT:
								{
									// could potentially hold onto all text too
									break;
								}

								default:
								{
									break;
								}
							}
						
							bSuccess = true;

							pIDOMNode->Release();
							pIDOMNode = NULL;
							break;
						}
					}
				}

				if (pIDOMNodeParent)
					pIDOMNodeParent->Release();
			}
		}
		catch(...)
		{
		}

		if (pIDOMNodeList)
			pIDOMNodeList->Release();

		if (pIDOMNode)
			pIDOMNode->Release();
	}

	return bSuccess;
}

void TciXmlUtils::CTciXmlResp::writeSection(const TCHAR* pSection, tstring& sectionContents, short level)
{
	if (sectionContents.length() > 0)
	{
		writeTabs(level);
		_stprintf_s(_tcharStr, _T("<%s>"), pSection);
		writeOutput(_tcharStr);
		writeEol();

		if (sectionContents.length() > 0)	// Move this if up if we don't want to write to the XML file a section without any contents. Probably ok
		{
			++level;
			writeTabs(level);

			// Just write back the entire <FREQ_PARAM> section from the input OR(der) XML file (is that the right thing to do? Does it matter?)
			writeSectionContents(sectionContents, level);
			--level;
		}

		writeTabs(level);
		_stprintf_s(_tcharStr, _T("</%s>"), pSection);
		writeOutput(_tcharStr);
		writeEol();
	}
}

void TciXmlUtils::CTciXmlResp::writeSectionContents(tstring& sectionContents, short level)
{
	// Tokenize a copy
	long len = sectionContents.length() + 1;
	TCHAR* strCopy = new TCHAR[len];
	if (strCopy)
	{
		_tcscpy_s(strCopy, len, sectionContents.c_str());

		// Tokenize using meta-data '\t' inside the _inputFreqParamSection. for each meta-data '\t', write the correct number of tabs (to make the xml file look nice)
		// Cheated here, instead of looking for each meta-data '\t' inside the _inputFreqParamSection and replacing that with the correct number of tabs, 
		//	instead I just write the correct number of tabs (call writeTabs function) for all but the last substring.
		TCHAR strTok[] = _T("\t");
		TCHAR* context = NULL;
		TCHAR* ptr = _tcstok_s(strCopy, strTok, &context);
		while (ptr)
		{
			writeOutput(ptr);

			ptr = _tcstok_s(NULL, strTok, &context);
            if (ptr)
            {
                writeTabs(level);
            }
		}

		delete[] strCopy;
	}
}

void TciXmlUtils::CTciXmlResp::writeTag(const TCHAR* tagName, /*std::wstring*/ tstring& tagValue, short level)
{
	writeTag(tagName, (TCHAR*)tagValue.c_str(), level);
}


void TciXmlUtils::CTciXmlResp::writeTag(const TCHAR* tagName, TCHAR* tagValue, short level)
{
	if (tagName)		// Cannot have an empty tagName
	{
		writeTabs(level);

		if (tagValue)
		{
			_stprintf_s(_tcharStr, _T("<%s>%s</%s>"), tagName, tagValue, tagName);
		}
		else
		{
			_stprintf_s(_tcharStr, _T("<%s></%s>"), tagName, tagName);
		}

		writeOutput(_tcharStr);

		writeEol();
	}
}

void TciXmlUtils::CTciXmlResp::writeComment(const TCHAR* comment, short level)
{
	// Seems that if there are comments (wrong place? too many? any at all?), I cannot 
	//	get the SMS4DC 'ORM Order' to work properly (the unit's name, MSS_RMS, MSS_RMC_PC, 
	//	MSS_ST, etc.. don't show properly, only the comments show!). 
	// SO BE CAREFUL WHEN ADDING COMMENTS (test, test and more test, or just don't add any comments)

	if (comment)		// cannot have an empty comment
	{
		writeTabs(level);

		_stprintf_s(_tcharStr, _T("<!-- %s -->"), comment);
		writeOutput(_tcharStr);

		writeEol();
	}
}

void TciXmlUtils::CTciXmlResp::writeTabs(short level)
{
	for (short tabs=0; tabs<level; ++tabs)
	{
		writeOutput(_T("\t"));
	}
}

void TciXmlUtils::CTciXmlResp::writeEol()
{
	writeOutput(_T("\r\n"));
}


// strToBeWritten must be a null-terminated string !
bool TciXmlUtils::CTciXmlResp::writeOutput(const TCHAR* strToBeWritten)
{
	bool bSuccess = false;
	tout << strToBeWritten;	// don't need to write to stdout once we have the output file (below)

	if (INVALID_HANDLE_VALUE != _hOutputFile)		// don't really have to check for this, because if the file handle is not valid, the contructor (which sets the handle) throws an exception
	{
		// Write to an output XML file, but never in wide characters
		DWORD bytesWritten;

		#if defined(UNICODE) || defined(_UNICODE)

			// Don't write to any output XML file in UNICODE, just use char*
			static char buf[10000];	//???
			size_t retval;
			wcstombs_s(&retval, buf, strToBeWritten, 10000);
			DWORD bytesToWrite = strlen(buf);

			BOOL ret = WriteFile(_hOutputFile, 
							buf, 
							bytesToWrite,			// number of bytes to write
							&bytesWritten,			// number of bytes that were written
							NULL);					// no overlapped structure
			bSuccess = (ret==TRUE) ? true : false;

		#else

			DWORD bytesToWrite = _tcslen(strToBeWritten);
			BOOL ret = WriteFile(_hOutputFile, 
							strToBeWritten,			// start of data to write
							bytesToWrite,			// number of bytes to write (snm-verify in UNICODE, Wide, and not-set)
							&bytesWritten,			// number of bytes that were written
							NULL);					// no overlapped structure

		#endif	// defined(UNICODE) || defined(_UNICODE)

		bSuccess = (TRUE == ret && bytesToWrite==bytesWritten) ? true : false;
	}

	return bSuccess;
}

void TciXmlUtils::CTciXmlResp::writeOrderType(TCHAR* tagValue, TCHAR* tagValueDefault, short level)
{
	writeTag(_T("ORDER_TYPE"), (tagValue && tagValue[0]) ? tagValue : tagValueDefault, level);
}

void TciXmlUtils::CTciXmlResp::writeOrderName(TCHAR* tagValue, TCHAR* tagValueDefault, short level)
{
	writeTag(_T("ORDER_NAME"), (tagValue && tagValue[0]) ? tagValue : tagValueDefault, level);
}

void TciXmlUtils::CTciXmlResp::writeOrderSender(TCHAR* tagValue, TCHAR* tagValueDefault, short level)
{
	writeTag(_T("ORDER_SENDER"), (tagValue && tagValue[0]) ? tagValue : tagValueDefault, level);
}

void TciXmlUtils::CTciXmlResp::writeOrderSenderPc(TCHAR* tagValue, TCHAR* tagValueDefault, short level)
{
	writeTag(_T("ORDER_SENDER_PC"), (tagValue && tagValue[0]) ? tagValue : tagValueDefault, level);
}

void TciXmlUtils::CTciXmlResp::writeOrderCreator(TCHAR* tagValue, TCHAR* tagValueDefault, short level)
{
	writeTag(_T("ORDER_CREATOR"), (tagValue && tagValue[0]) ? tagValue : tagValueDefault, level);
}

void TciXmlUtils::CTciXmlResp::writeOrderAddressee(TCHAR* tagValue, TCHAR* tagValueDefault, short level)
{
	writeTag(_T("ORDER_ADDRESSEE"), (tagValue && tagValue[0]) ? tagValue : tagValueDefault, level);
}

void TciXmlUtils::CTciXmlResp::writeOrderVer(TCHAR* tagValue, TCHAR* tagValueDefault, short level)
{
	writeTag(_T("ORDER_VER"), (tagValue && tagValue[0]) ? tagValue : tagValueDefault, level);
}


//////////////////////////////////////////////////////////////////////
//////////////                                          //////////////
//////////////             <ACT_DEF> section:           //////////////
//////////////                                          //////////////
//////////////////////////////////////////////////////////////////////

void TciXmlUtils::CTciXmlResp::writeActDef(TCHAR* orderId, short level)
{
	writeTabs(level);
	writeOutput(_T("<ACT_DEF>"));
	writeEol();

		++level;
		//do we have to change (remove the last 3 digits of the) orderId for the AcdUserstring?
		writeAcdUserString(orderId, level);		// If ORDER_TYPE = OR: Unique SubOrder ID; e.g. ORDER_TYPE and timestamp. If ORDER_TYPE != OR: ORDER_ID

		//snm-client can call MeasurementStatus(MEAS_ERROR) if there is some error running measurement (unfortunately, i cannot get SMS4DC to show the AcdErrMessage string anywhere!!!)
		#ifdef SIMULATE_DATA
			MeasurementStatus( /*MEAS_ERROR*/  MEAS_SUCCESS);	// client should call this if error (we assume success)
		#endif
		writeAcdErr(level);				// will be success ("S") or Error ("E"), based on MeasurementStatus() from the client
		//writeAcdErr(_T("S"), level);				// should determine success ("S") or failure ("F")?? from the SMS server, hard-code to "S" for now (S: Success, W: Warning, E: Error)

		if (MeasurementStatus() == MEAS_ERROR)
		{
			//snm-cannot get SMS4DC to show any error text (AcdErrorMessage), but let the client set these (at least they will be in the result XML file)
			// maybe overload writeAcdErrCode(TCHAR*) and (unsigned int/long?)
			writeAcdErrCode(_T("9999"), level);	// if there is some error, add some code here (Only used in the SMD Interface. ACD_ERR_MESS must be filled too. 0-12: Success. 100-1000: Warning. 1000-xx: Error code)
			
			//writeAcdErrMessage(_T("Text explaining the ACD_ERR_CODE here"), level);	// Text explaining ACD_ERR_CODE
			#ifdef SIMULATE_DATA
				MeasurementError(_T("some text here explaining why the measurement failed"));
			#endif
			writeAcdErrMessage(level);
		}
		--level;

	writeTabs(level);
	writeOutput(_T("</ACT_DEF>"));
	writeEol();
}

void TciXmlUtils::CTciXmlResp::MeasurementStatus(EMeasurementStatus measStatus, const TCHAR* measStatusMsg)	
{ 
	_measStatus = measStatus;
	if ((_measStatus != MEAS_SUCCESS) && !measStatusMsg)
	{
		MeasurementError(measStatusMsg); 
	}
}

void TciXmlUtils::CTciXmlResp::writeAcdUserString(TCHAR* tagValue, short level)
{
	writeTag(_T("ACD_USERSTRING"), tagValue, level);
}

//void TciXmlUtils::CTciXmlResp::writeAcdErr(TCHAR* tagValue, short level)
void TciXmlUtils::CTciXmlResp::writeAcdErr(short level)
{
	// S: Success
	// W: Warning
	// E: Error

	// in case of error no results will be provided
	//get from member var (init to success, set to error by client if error running meas) writeTag(_T("ACD_ERR"), tagValue, level);


	TCHAR stat[2];
	stat[1] = 0;

	switch (MeasurementStatus())
	{
		case MEAS_SUCCESS:
			stat[0] = _T('S');
			break;
		case MEAS_WARNING:
			stat[0] = _T('W');
			break;
		case MEAS_ERROR:
		default:
			stat[0] = _T('E');
			break;
	}

	writeTag(_T("ACD_ERR"), stat, level);	
}

void TciXmlUtils::CTciXmlResp::writeAcdErrCode(TCHAR* tagValue, short level)
{
	// ACD_ERR_MESS must be filled too
	//	0-12:	  Success
	//	100-1000: Warning
	//	1000-xx:  Error code
	writeTag(_T("ACD_ERR_CODE"), tagValue, level);
}

//void TciXmlUtils::CTciXmlResp::writeAcdErrMessage(TCHAR* tagValue, short level)
void TciXmlUtils::CTciXmlResp::writeAcdErrMessage(short level)
{
	// Text explaining ACD_ERR_CODE
	//writeTag(_T("ACD_ERR_MESS"), tagValue, level);
	writeTag(_T("ACD_ERR_MESS"), MeasurementError(), level);
}

// Helper function to create a DOM instance. 
HRESULT TciXmlUtils::CTciXmlResp::createAndInitDOM(IXMLDOMDocument **ppDoc)
{
    HRESULT hr = CoCreateInstance(__uuidof(DOMDocument60), NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(ppDoc));
    if (FAILED(hr))
    {
        // Somehow DOMDocument60 is not exposed/available at this computer
        // (e.g. some not-well-updated Windows XP x64 systems would have this issue if MSXML6.dll is missing or not properly registered.)
        // Try with the older MSXML interface
        hr = CoCreateInstance(__uuidof(DOMDocument), NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(ppDoc));
    }

    if (SUCCEEDED(hr))
    {
        // these methods should not fail so don't inspect result
        (*ppDoc)->put_async(VARIANT_FALSE);  
        (*ppDoc)->put_validateOnParse(VARIANT_FALSE);
        (*ppDoc)->put_resolveExternals(VARIANT_FALSE);
    }

    return hr;
}

// Helper function to create a VT_BSTR variant from a null terminated string. 

#if defined(UNICODE) || defined(_UNICODE)
HRESULT TciXmlUtils::CTciXmlResp::variantFromString(PCWSTR wszValue, VARIANT &Variant)
#else
HRESULT TciXmlUtils::CTciXmlResp::variantFromString(const TCHAR* wszValue, VARIANT &Variant)
#endif
{
    HRESULT hr = S_OK;
	BSTR bstr;

	#if defined(UNICODE) || defined(_UNICODE)
	    bstr = SysAllocString(wszValue);
	#else
		bstr = TciXmlUtils::BstrFromTchar(wszValue);	// todo when to call SysFreeString?
	#endif

	//CHK_ALLOC(bstr);
	if (bstr)
	{
		V_VT(&Variant)   = VT_BSTR;
		V_BSTR(&Variant) = bstr;
	}

    return hr;
}




////////////////////////////////////////////////////////////////////////////////////////

TciXmlUtils::CTciXmlGspResp::CTciXmlGspResp(TCHAR* inputFilename, const TCHAR* outputFilename) 
	: CTciXmlResp(inputFilename, outputFilename)
{
    _siteMinFreq = 0;
    _siteMaxFreq = 0;
    ZeroMemory(_stationName, sizeof(_stationName));
    ZeroMemory(&_serverCapabilities, sizeof(SMetricsMsg::SGetBandResp));

    if (loadDOMRaw(inputFilename))
	{
		readOrderInfo(inputFilename);
	}
}

TciXmlUtils::CTciXmlGspResp::~CTciXmlGspResp(void)
{
}


// derived class must override this to create it's approrpriate XML Response file (create the GSP response XML file)
void TciXmlUtils::CTciXmlGspResp::WriteResponse()
{
	if (_validXmlInputFile)
	{
		writeGspResponse();
	}
}

void TciXmlUtils::CTciXmlGspResp::StationCapabilities(SMetricsMsg::SGetBandResp& capabilities)
{
    // Make a copy of the server capabilities.
    memcpy(&_serverCapabilities, &capabilities, sizeof(SMetricsMsg::SGetBandResp));

    switch (_serverCapabilities.siteInfo)
    {
    case SMetricsMsg::FIXED_SITE:
        SiteType(TciXmlUtils::SITETYPE_FIXED);
        break;
    case SMetricsMsg::MOBILE_SITE:
        SiteType(TciXmlUtils::SITETYPE_MOBILE);
        break;
    default:
        SiteType(TciXmlUtils::SITETYPE_UNKNOWN);
        break;
    }

    std::list<double> bandwidthList;
    CString strBandwidth;
    double bandwidthKHz; // in kHz
    // Number of HF Measurement Bandwidths
    for (unsigned long i = 0; i < _serverCapabilities.hfMeasurementBw.numBandwidths; ++i)
    {
        bandwidthKHz = Units::Frequency(_serverCapabilities.hfMeasurementBw.bandwidth[i]).Hz<double>() / 1000.0;

        // Store the measurement bandwidth
        bandwidthList.push_back(bandwidthKHz);
    }
    // Number of V/U/SHF Measurement Bandwidths;
    for (unsigned long i = 0; i < _serverCapabilities.vushfMeasurementBw.numBandwidths; ++i)
    {
        bandwidthKHz = Units::Frequency(_serverCapabilities.vushfMeasurementBw.bandwidth[i]).Hz<double>() / 1000.0;

        // Store the measurement bandwidth
        bandwidthList.push_back(bandwidthKHz);
    }

    // Sort and get rid of duplicate values.
    bandwidthList.sort();
    bandwidthList.unique();

    // Set the measurement bandwidth list.
    for (std::list<double>::iterator it = bandwidthList.begin(); it != bandwidthList.end(); ++it)
    {
        strBandwidth = formatBandwidthToString(*it);
        StoreMeasurementBw((LPSTR)(LPCTSTR)strBandwidth);
    }
}

CString TciXmlUtils::CTciXmlGspResp::formatBandwidthToString(double bandwidthKHz)
{
    CString strBandwidth;
    char buff[128];
    int buffSize = sizeof(buff);
    CString strUnit;

    if (bandwidthKHz >= ONE_THOUSANDD)		// At least 1,000 kHz, display in MHz
    {
        _snprintf_s(buff, buffSize, _TRUNCATE, "%.6f", bandwidthKHz / ONE_THOUSANDD);
        strUnit.LoadString(IDS_MHZ);
    }
    else
    {
        _snprintf_s(buff, buffSize, _TRUNCATE, "%.6f", bandwidthKHz);
        strUnit.LoadString(IDS_KHZ);
    }

    // Get rid of the tailing 0's
    int len = strlen(buff);
    char *pos = buff + len - 1;
    while (*pos == '0')
        *pos-- = '\0';
    if (*pos == '.')
        *pos = '\0';

    // Add kHz/MHz to the end
    strBandwidth.Format("%s %s", buff, strUnit);

    return strBandwidth;
}

bool TciXmlUtils::CTciXmlGspResp::readOrderInfo(TCHAR* inputFilename)
{
	// Read the GSP Request XML file fields that will be echoed-back to the GSP Response XML file
	// fill in:
	//TCHAR		_orderType[_sizeField];
	//TCHAR		_orderName[_sizeField];
	//TCHAR		_orderSender[_sizeField];
	//TCHAR		_orderSenderPc[_sizeField];		// may be omitted
	//TCHAR		_orderState[_sizeField];		// do not read (will hard code to 'Finished' in response file)
	//TCHAR		_orderCreator[_sizeField];
	//TCHAR		_orderAddressee[_sizeField];	// may be omitted
	//TCHAR		_orderVersion[_sizeField];
	//TCHAR		_executiontype					// do not read (will hard code to 'A' or 'M' in response file)

	UNREFERENCED_PARAMETER(inputFilename);

	getFieldValue(_T("ORDER_ID"),		_orderId,		_countof(_orderId) /*_sizeOrderId*/);		//64
	getFieldValue(_T("ORDER_TYPE"),		_orderType,		_countof(_orderType) /*_sizeField*/);		//32
	getFieldValue(_T("ORDER_NAME"),		_orderName,		_countof(_orderName) /*_sizeField*/);		//32
	getFieldValue(_T("ORDER_SENDER"),	_orderSender,	_countof(_orderSender) /*_sizeField*/);		//32
	getFieldValue(_T("ORDER_SENDER_PC"),_orderSenderPc,	_countof(_orderSenderPc) /*_sizeField*/);	//32
	getFieldValue(_T("ORDER_CREATOR"),	_orderCreator,	_countof(_orderCreator) /*_sizeField*/);	//32
	getFieldValue(_T("ORDER_ADDRESSEE"),_orderAddressee,_countof(_orderAddressee) /*_sizeField*/);	//32
	getFieldValue(_T("ORDER_VER"),		_orderVersion,	_countof(_orderVersion) /*_sizeField*/);	//32

	return true;
}

void TciXmlUtils::CTciXmlGspResp::writeGspResponse(/*TCHAR* orderId*/)
{
	writeOutput(_T("<?xml version=\"1.0\" encoding=\"utf-8\" ?>"));
	writeEol();

	writeComment(_T("TCI Scorpio Client response for \"Get System Parameters\" order"), 0);

	writeOrderDef(_orderId/*orderId*/, 0);
}

void TciXmlUtils::CTciXmlGspResp::writeOrderDef(TCHAR* orderId, short level)
{
	writeTabs(level);
	writeOutput(_T("<ORDER_DEF>"));
	writeEol();

		++level;
        writeTag(_T("ORDER_ID"), orderId, level);
		writeOrderType(_orderType, _T("GSP"), level);				// read from the <ORDER_TYPE> in the GSP Request file, default to to "GSP" if missing
		writeOrderName(_orderName, _T("OrderName1"), level);		// read from the <ORDER_NAME> in the GSP Request file, default to to "OrderName" if missing
        writeOrderSender(_orderSender, _orderSenderDefault, level);		// read from the <ORDER_SENDER> in the GSP Request file, default to to "SMS4DC" if missing
		writeOrderSenderPc(_orderSenderPc, _T("SENDER-PC"), level);	// read from the <ORDER_SENDER_PC> in the GSP Request file, default to to "SENDER-PC" if missing
        writeTag(_T("ORDER_STATE"), _T("Finished"), level);						// hard-code to "Finished"
		writeOrderCreator(_orderCreator, _T("Extern"), level);		// read from the <ORDER_CREATOR> in the GSP Request file, default to to "Extern" if missing
		writeOrderAddressee(_orderAddressee, _T("Scorpio Client"), level);	// ??? should read the <ORDER_ADDRESSEE> from the GSP Request file, hard-code to "Scorpio" for now (we are using the R&S i/f from SMS4DC, so <ORDER_ADDRESSEE> is 'ARGUS' - we don't want to use that)
		writeOrderVer(_orderVersion, _T("300"), level);				// read from the <ORDER_VER> in the GSP Request file, default to to "300" if missing
        writeTag(_T("EXECUTION_TYPE"), _T("A"), level);							// SHOLD read from the <EXECUTION_TYPE> in the GSP Request file???, default to to "A" if missing 

		writeActDef(orderId, level);

		writeMonSysStructure(/*orderId, */ level);
		--level;

	writeTabs(level);
	writeOutput(_T("</ORDER_DEF>"));
	writeEol();
}

void TciXmlUtils::CTciXmlGspResp::writeMonSysStructure(short level)
{
	writeTabs(level);
	writeOutput(_T("<MONSYS_STRUCTURE>"));
	writeEol();

		++level;

		//snm-todo. What should these be? anything from the TCI server? we don't have a regional monitoring center (RMC) !
        // Name of regional monitoring center
        writeTag(_T("MSS_RMC"),     _T("MSSRMC"),           level);	// should we read the <MSS_RMC> from the GSP Request file from the <ORDER_SENDER>??
        writeTag(_T("MSS_RMC_PC"),  _T("MSSRMCPC"),         level); // should we read the <MSS_RMC_PC> from the GSP Request file from the <ORDER_SENDER_PC>??
        writeTag(_T("MSS_ST_NAME"), _T("TCI SMS Server"),   level);	// should we read the <MSS_ST_NAME> from the GSP Request file???

		#ifdef SIMULATE_DATA
			SiteType(TciXmlUtils::SITETYPE_FIXED);	//1=Fixed, 2=Mobile			//call this mutator from scorpio client after receiving server's capabilities
		#endif
		writeMssStType(level);

		// writing the lat and long as either deg,min,sec,hemisphere or decimal both seem to work ok, that is, seems to work as GSP response (can select ORder afterwards)
		// Create the MSS_LONG and MSS_LAT keys

		#ifdef SIMULATE_DATA
			SiteLong(-121.951555);		//call this mutator from scorpio client after receiving server's gps location
		#endif
		writeMssLong(level);

		#ifdef SIMULATE_DATA
			SiteLat(37.473333);			//call this mutator from scorpio client after receiving server's gps location
		#endif
		writeMssLat(level);

        writeMssDateTime(level);

		writeMssPaths(level);
		--level;

	writeTabs(level);
	writeOutput(_T("</MONSYS_STRUCTURE>"));
	writeEol();
}



//////////////////////////////////////////////////////////////////////
//////////////                                          //////////////
//////////////       <MONSYS_STRUCTURE> section:        //////////////
//////////////                                          //////////////
//////////////////////////////////////////////////////////////////////

void TciXmlUtils::CTciXmlGspResp::writeMssStType(short level)
{
	// Type of measurement station
	//	F = fixed
	//	M = mobile

	// Get the site type (Fixed or Mobile) from somewhere else (if in Scorpio Client, get from a server's capabilities and call SiteType(unsigned long siteType)
	TCHAR* tagSiteType;
	switch (SiteType())
	{
		case TciXmlUtils::SITETYPE_MOBILE:	// 2 = Mobile Site
		{
			tagSiteType = _T("M");
			break;
		}

		default:
		{
			tagSiteType = _T("F");
			break;
		}
	}

    writeTag(_T("MSS_ST_TYPE"), tagSiteType, level);
}

void TciXmlUtils::CTciXmlGspResp::writeMssLong4Parts(short level)
{
	double siteLong = SiteLong();
	writeMssLong4Parts(siteLong, level);
}

void TciXmlUtils::CTciXmlGspResp::writeMssLong4Parts(double siteLong, short level)
{
	const size_t bufSize = 32;
	TCHAR tcharStr[bufSize];

	TciXmlUtils::CLatLong latLong;
	short deg, min, sec;
	TCHAR ew;

	latLong.Lon2dms(siteLong, deg, min, sec, ew);

	_stprintf_s(tcharStr, _T("%d"), deg);
	writeTag(_T("MSS_LONG_DEG"), tcharStr, level);

	_stprintf_s(tcharStr, _T("%d"), min);
	writeTag(_T("MSS_LONG_MIN"), tcharStr, level);

	_stprintf_s(tcharStr, _T("%d"), sec);
	writeTag(_T("MSS_LONG_SEC"), tcharStr, level);

	_stprintf_s(tcharStr, _T("%c"), ew);
	writeTag(_T("MSS_LONG_HEM"), tcharStr, level);
}

void TciXmlUtils::CTciXmlGspResp::writeMssLat4Parts(short level)
{
	double siteLat = SiteLat();
	writeMssLat4Parts(siteLat, level);
}

void TciXmlUtils::CTciXmlGspResp::writeMssLat4Parts(double siteLat, short level)
{
	const size_t bufSize = 32;
	TCHAR tcharStr[bufSize];

	TciXmlUtils::CLatLong latLong;
	short deg, min, sec;
	TCHAR ns;

	latLong.Lat2dms(siteLat, deg, min, sec, ns);

	_stprintf_s(tcharStr, _T("%d"), deg);
	writeTag(_T("MSS_LAT_DEG"), tcharStr, level);

	_stprintf_s(tcharStr, _T("%d"), min);
	writeTag(_T("MSS_LAT_MIN"), tcharStr, level);

	_stprintf_s(tcharStr, _T("%d"), sec);
	writeTag(_T("MSS_LAT_SEC"), tcharStr, level);

	_stprintf_s(tcharStr, _T("%c"), ns);
	writeTag(_T("MSS_LAT_HEM"), tcharStr, level);
}

void TciXmlUtils::CTciXmlGspResp::writeMssLong(short level)
{
	double siteLong = SiteLong();

	const size_t bufSize = 32;
	TCHAR tcharStr[bufSize];
	_stprintf_s(tcharStr, _T("%.6lf"), siteLong);
    writeTag(_T("MSS_LONG"), tcharStr, level);
}

void TciXmlUtils::CTciXmlGspResp::writeMssLat(short level)
{
	double siteLat = SiteLat();

	const size_t bufSize = 32;
	TCHAR tcharStr[bufSize];
	_stprintf_s(tcharStr, _T("%.6lf"), siteLat);
    writeTag(_T("MSS_LAT"), tcharStr, level);
}

void TciXmlUtils::CTciXmlGspResp::writeMssDateTime(short level)
{
    tstring wstrDate;
    wstrDate = TciXmlUtils::Format(_T("%02d-%02d-%d"), _lastConnectDateTime.wMonth, _lastConnectDateTime.wDay, _lastConnectDateTime.wYear);
    writeTag(_T("MSS_DATE"), wstrDate, level);
    tstring wstrTime;
    wstrTime = TciXmlUtils::Format(_T("%02d:%02d:%02d"), _lastConnectDateTime.wHour, _lastConnectDateTime.wMinute, _lastConnectDateTime.wSecond);
    writeTag(_T("MSS_TIME"), wstrTime, level);
}

void TciXmlUtils::CTciXmlGspResp::writeMssPaths(short level)
{
	//snm- for now, just write the same MSS_PATHS section exactly from the r&s example (just the 2nd one)
	writeTabs(level);
	writeOutput(_T("<MSS_PATHS>"));
	writeEol();

		++level;

		#ifdef SIMULATE_DATA
			StationName(_T("Tci's station name here"));	// the scorpio client should call this to set the server's name (no more than 30 chars)
		#endif
		writeMpName(level);		// todo From where to get <MP_NAME> ? Get from client (create mutator)

		#ifdef SIMULATE_DATA
			SiteMinFreq(20000000);			// call this mutator from scorpio client after receiving server's capabilities (minimum frequency of server)
		#endif
		writeMpFrL(level);

		#ifdef SIMULATE_DATA
			SiteMaxFreq(3000000000);			// call this mutator from scorpio client after receiving server's capabilities (maximum frequency of server)
		#endif
		writeMpFrU(level);
		
		writeMpDev(level);

		--level;

	writeTabs(level);
	writeOutput(_T("</MSS_PATHS>"));
	writeEol();
}


void TciXmlUtils::CTciXmlGspResp::writeMpDev(short level)
{
	writeTabs(level);
	writeOutput(_T("<MP_DEV>"));
	writeEol();

		++level;
		writeTag(_T("D_NAME"), /*_T("DDF0xE") */ _T("Device"), level);    // Device Name
		writeTag(_T("D_DRIVER"), /* _T("DDF0xA_") */ _T("Driver"), level); // Device Driver (not filled if this table is used in MEAS_DATA_HDR)

		// todo not sure what these control:
		// D_MPARAM: Available measurement parameters of the device
		writeTag(_T("D_MPARAM"), _T("Bearing"), level);
		writeTag(_T("D_MPARAM"), _T("Quality"), level);
		writeTag(_T("D_MPARAM"), _T("Level"), level);
		//NA: writeTag(_T("D_MPARAM"), _T("IF"), level);
		//NA: writeTag(_T("D_MPARAM"), _T("Audio"), level);
		writeTag(_T("D_MPARAM"), _T("FM"), level);
		writeTag(_T("D_MPARAM"), _T("AM"), level);
		writeTag(_T("D_MPARAM"), _T("PM"), level);
		writeTag(_T("D_MPARAM"), _T("BandWidth"), level);
		writeTag(_T("D_MPARAM"), _T("Offset"), level);

		//NA: writeTag(_T("D_MPARAM"), _T("Scan Level"), level);
		//NA: writeTag(_T("D_MPARAM"), _T("Scan Offset"), level);
		//NA: writeTag(_T("D_MPARAM"), _T("FL Scan Level"), level);
		//NA: writeTag(_T("D_MPARAM"), _T("FL Scan Offs"), level);
		//NA: writeTag(_T("D_MPARAM"), _T("DScan Level"), level);
		//NA: writeTag(_T("D_MPARAM"), _T("Shorttest"), level);
		//NA: writeTag(_T("D_MPARAM"), _T("Longtest"), level);
		//NA: writeTag(_T("D_MPARAM"), _T("Reset"), level);


		// todo later write these from the server's capabilities (measurement bandwidths from TCI server go into the 'IF Bandwidth' in SMS4DC because that's what's used for meas. bw)
		// IF Bandwidth
		#ifdef SIMULATE_DATA
			StoreMeasurementBw(_T("1 kHz"));
			StoreMeasurementBw(_T("2 kHz"));
			StoreMeasurementBw(_T("5 kHz"));
			StoreMeasurementBw(_T("8.333 kHz"));
			StoreMeasurementBw(_T("10 kHz"));
			StoreMeasurementBw(_T("12.5 kHz"));
			StoreMeasurementBw(_T("20 kHz"));
			StoreMeasurementBw(_T("25 kHz"));
			StoreMeasurementBw(_T("50 kHz"));
			StoreMeasurementBw(_T("100 kHz"));
			StoreMeasurementBw(_T("200 kHz"));
			StoreMeasurementBw(_T("300 kHz"));
			StoreMeasurementBw(_T("400 kHz"));
			StoreMeasurementBw(_T("600 kHz"));
		#endif

		// now write all IF (meas) BW to the GSP XML result file (take from collection of meas bw strings provided by the client)
		tstring strBw;
		TCHAR bw[128];

        // Available IF bandwidths of the device
        MEASBW_ITER iter;
		for (iter = _measBandwidths.begin(); iter != _measBandwidths.end(); ++iter)
		{
			strBw = *iter;
			_stprintf_s(bw, _T("%s"), strBw.c_str());
            writeTag(_T("D_IFBW"), bw, level);
		}

		// todo later write these from the server's capabilities, or just hard-coded (or from Registry): see what the scorpio client does now
		// Available Demodulations of the device
		writeTag(_T("D_DEMOD"), _T("AM"), level);
		writeTag(_T("D_DEMOD"), _T("FM"), level);
		writeTag(_T("D_DEMOD"), _T("CW"), level);
		writeTag(_T("D_DEMOD"), _T("USB"), level);
		writeTag(_T("D_DEMOD"), _T("LSB"), level);

		// todo later write these from the server's capabilities, or just hard-coded (or from Registry): see what the scorpio client does now ??
		// Available Modes of the direction finder
		writeTag(_T("D_MODE"), _T("Normal"), level);
		//NA: writeTag(_T("D_MODE"), _T("Cont"), level);
		//NA: writeTag(_T("D_MODE"), _T("Gate"), level);
		//NA: writeTag(_T("D_MODE"), _T("Single"), level);

		// Available detectors of the device
		writeTag(_T("D_DET"), _T("Average"), level);
		//NA: writeTag(_T("D_DET"), _T("Peak"), level);
		//NA: writeTag(_T("D_DET"), _T("Fast"), level);

		// Available IF Attenuations of the device

		// Available RF-Attenuations of the device
		//NA: writeTag(_T("D_RFATTN"), _T("0 dB"), level);
		//NA: writeTag(_T("D_RFATTN"), _T("30 dB"), level);
		writeTag(_T("D_RFATTN"), _T("Auto"), level);

		// Available Measurement Times of the device
		//NA: ??? 
		writeTag(_T("D_MTIME"), _T("-1.000000"), level);
		writeTag(_T("D_MTIME"), _T("900.000000"), level);

		// Available DF Bandwidths of the device
		//NA: ?? review these?
		//NA: writeTag(_T("D_DFBW"), _T("3 kHz"), level);
		writeTag(_T("D_DFBW"), _T("5 kHz"), level);
		writeTag(_T("D_DFBW"), _T("10 kHz"), level);
		writeTag(_T("D_DFBW"), _T("50 kHz"), level);
		writeTag(_T("D_DFBW"), _T("1 MHz"), level);
		writeTag(_T("D_DFBW"), _T("2 MHz"), level);
		writeTag(_T("D_DFBW"), _T("10 MHz"), level);

        --level;

	writeTabs(level);
	writeOutput(_T("</MP_DEV>"));
	writeEol();
}


//////////////////////////////////////////////////////////////////////
//////////////                                          //////////////
//////////////            <MSS_PATHS> section:          //////////////
//////////////                                          //////////////
//////////////////////////////////////////////////////////////////////

void TciXmlUtils::CTciXmlGspResp::writeMpName(short level)
{
	writeTag(_T("MP_NAME"), StationName(), level);
}

void TciXmlUtils::CTciXmlGspResp::writeMpFrL(short level)
{
	unsigned __int64 siteMinimumFreq = SiteMinFreq();

	const size_t bufSize = 32;
	TCHAR tcharStr[bufSize];
	_stprintf_s(tcharStr, _T("%I64u"), siteMinimumFreq);
    writeTag(_T("MP_FR_L"), tcharStr, level);
}

void TciXmlUtils::CTciXmlGspResp::writeMpFrU(short level)
{
	unsigned __int64 siteMaximumFreq = SiteMaxFreq();

	const size_t bufSize = 32;
	TCHAR tcharStr[bufSize];
	_stprintf_s(tcharStr, _T("%I64u"), siteMaximumFreq);
    writeTag(_T("MP_FR_U"), tcharStr, level);
}


//////////////////////////////////////////////////////////////////////
//////////////                                          //////////////
//////////////             <MP_DEV> section:            //////////////
//////////////                                          //////////////
//////////////////////////////////////////////////////////////////////


void TciXmlUtils::CTciXmlGspResp::writeD_IfSpan(TCHAR* tagValue, short level)
{
	// Available IF spans of the device
	writeTag(_T("D_IFSPAN"), tagValue, level);
}

void TciXmlUtils::CTciXmlGspResp::writeD_DfTime(TCHAR* tagValue, short level)
{
	// Available DF integration times of the device
	writeTag(_T("D_DFTIME"), tagValue, level);
}

void TciXmlUtils::CTciXmlGspResp::writeD_Preamp(TCHAR* tagValue, short level)
{
	// Available preamplifiers of the device
	writeTag(_T("D_PREAMP"), tagValue, level);
}

void TciXmlUtils::CTciXmlGspResp::writeD_Azimuth(TCHAR* tagValue, short level)
{
	// Azimuth property of the antenna/super antenna
	writeTag(_T("D_AZI"), tagValue, level);
}

void TciXmlUtils::CTciXmlGspResp::writeD_Height(TCHAR* tagValue, short level)
{
	// Height property of the antenna/super antenna
	writeTag(_T("D_HGT"), tagValue, level);
}

void TciXmlUtils::CTciXmlGspResp::writeD_Elevation(TCHAR* tagValue, short level)
{
	// Elevation property of the antenna/super antenna
	writeTag(_T("D_ELE"), tagValue, level);
}

void TciXmlUtils::CTciXmlGspResp::writeD_Polarization(TCHAR* tagValue, short level)
{
	// Polarization property of the antenna/super antenna
	writeTag(_T("D_POL"), tagValue, level);
}


////////////////////////////////////////////////////////////////////////////////////////

TciXmlUtils::CTciXmlOrderResp::CTciXmlOrderResp(TCHAR* inputFilename, const TCHAR* outputFilename)
	: CTciXmlResp(inputFilename, outputFilename), index(0)
{
	_subOrderName[0]	=
	_subOrderTask[0]	=
	_subOrderPrio[0]	=
	_resultType[0]		= 0;

	if (loadDOMRaw(inputFilename))
	{
		readOrderInfo(inputFilename);
	}
}

TciXmlUtils::CTciXmlOrderResp::~CTciXmlOrderResp(void)
{
}


void TciXmlUtils::CTciXmlOrderResp::WriteResponse()
{
	if (_validXmlInputFile)
	{
		writeOrderResponse();
	}
}


void TciXmlUtils::CTciXmlOrderResp::StoreMeasurementDatum_Fs(unsigned __int64 measFreqHz, SYSTEMTIME measTime, double measDataFs, TCHAR* measDataFsUnit)
{
	UNREFERENCED_PARAMETER(measDataFsUnit);

	addDatumFs(measFreqHz, measTime, measDataFs);
}

void TciXmlUtils::CTciXmlOrderResp::StoreMeasurementDatum_Df(unsigned __int64 measFreqHz, SYSTEMTIME measTime, double measDataDfBearing, short measDataDfQuality)
{
	addDatumDf(measFreqHz, measTime, measDataDfBearing, measDataDfQuality);
}

void TciXmlUtils::CTciXmlOrderResp::StoreMeasurementDatum_FrOff(unsigned __int64 measFreqHz, SYSTEMTIME measTime, double measDataFreqOffHz)
{
	addDatumFrOff(measFreqHz, measTime, measDataFreqOffHz);
}

void TciXmlUtils::CTciXmlOrderResp::StoreMeasurementDatum_Bw(unsigned __int64 measFreqHz, SYSTEMTIME measTime, double measDataBwKhz)
{
	addDatumBw(measFreqHz, measTime, measDataBwKhz);
}

void TciXmlUtils::CTciXmlOrderResp::StoreMeasurementDatum_Mod(unsigned __int64 measFreqHz, SYSTEMTIME measTime, unsigned long measDataMod, TciXmlUtils::EModulationType modulationType)
{
	addDatumMod(measFreqHz, measTime, measDataMod, modulationType);
}

//accessors
bool TciXmlUtils::CTciXmlOrderResp::GetFirstFreq(unsigned __int64& freq)
{
	bool bFound = false;
	index = 0;

	switch (FrequencyMode())
	{
		case SFrequencyMode::SINGLE:
		{
			freq = SingleFrequencyHz();
			bFound = true;
			break;
		}

		case SFrequencyMode::RANGE:
		{
			freq = LowerFrequencyHz();
			bFound = true;
			break;
		}

		case SFrequencyMode::LIST:
		{
			if (_freqMode.freqs.freqList.numFreq > 0)
			{
				freq = _freqMode.freqs.freqList.listOfFreqsHz[0];
				bFound = true;
			}
			break;
		}
	}

	return bFound;
}

bool TciXmlUtils::CTciXmlOrderResp::GetNextFreq(unsigned __int64& freq)
{
	bool bFound = false;
	++index;

	switch (FrequencyMode())
	{
		case SFrequencyMode::SINGLE:
		{
			bFound = false;
			break;
		}

		case SFrequencyMode::RANGE:
		{
			freq = LowerFrequencyHz() + (StepSizeHz() * index);
			bFound = (freq <= UpperFrequencyHz());
			break;
		}

		case SFrequencyMode::LIST:
		{
			if (_freqMode.freqs.freqList.numFreq > index)
			{
				freq = _freqMode.freqs.freqList.listOfFreqsHz[index];
				bFound = true;
			}
			break;
		}
	}

	return bFound;
}




bool TciXmlUtils::CTciXmlOrderResp::readOrderInfo(TCHAR* inputFilename)
{
	// Read the OR(der) Request XML file.
	//
	// Fields that will be echoed-back to the OR(der) Response XML file:
	//_orderType, orderName, _orderSender, orderCreator, _orderVersion
	//_orderSenderPc, _orderAddressee				// may be omitted
	//_orderState, _executiontype					// do not read (will hard-code to 'Finished' and 'A' (really?), respectively, in response XML file

	UNREFERENCED_PARAMETER(inputFilename);

	//snm?? MSP_SIG_PATH ?
	// From the input OR(der) XML file's <ORDER_DEF> section (I think these keys are unique, so don't really have to pass "ORDER_DEF" as the parent; doesn't hurt to)
	getFieldValue(_T("ORDER_ID"),		_orderId,		_countof(_orderId), _T("ORDER_DEF") /*_sizeOrderId*/);		//64
	getFieldValue(_T("ORDER_TYPE"),		_orderType,		_countof(_orderType), _T("ORDER_DEF") /*_sizeField*/);		//32
	getFieldValue(_T("ORDER_NAME"),		_orderName,		_countof(_orderName), _T("ORDER_DEF") /*_sizeField*/);		//32
	getFieldValue(_T("ORDER_SENDER"),	_orderSender,	_countof(_orderSender), _T("ORDER_DEF") /*_sizeField*/);		//32
	getFieldValue(_T("ORDER_SENDER_PC"),_orderSenderPc,	_countof(_orderSenderPc), _T("ORDER_DEF") /*_sizeField*/);	//32
	getFieldValue(_T("ORDER_CREATOR"),	_orderCreator,	_countof(_orderCreator), _T("ORDER_DEF") /*_sizeField*/);	//32
	getFieldValue(_T("ORDER_ADDRESSEE"),_orderAddressee,_countof(_orderAddressee), _T("ORDER_DEF") /*_sizeField*/);	//32
	getFieldValue(_T("ORDER_VER"),		_orderVersion,	_countof(_orderVersion), _T("ORDER_DEF") /*_sizeField*/);	//32

	// From the input OR(der) XML file's <SUB_ORDER_DEF> section (I think these keys are unique, so don't really have to pass "SUB_ORDER_DEF" as the parent; doesn't hurt to)
	getFieldValue(_T("SUBORDER_NAME"),	_subOrderName,	_countof(_subOrderName), _T("SUB_ORDER_DEF") /*_sizeField*/);	//32
	getFieldValue(_T("SUBORDER_TASK"),	_subOrderTask,	_countof(_subOrderTask), _T("SUB_ORDER_DEF") /*_sizeField*/);	//32
	getFieldValue(_T("SUBORDER_PRIO"),	_subOrderPrio,	_countof(_subOrderPrio), _T("SUB_ORDER_DEF") /*_sizeField*/);	//32
	getFieldValue(_T("RESULT_TYPE"),	_resultType,	_countof(_resultType), _T("SUB_ORDER_DEF") /*_sizeField*/);		//32

	// From the input OR(der) XML file's <MEAS_STAT_PARAM> section (just what I need)
	getFieldValue(_T("MSP_SIG_PATH"),	_MSP_SIG_PATH,	_countof(_MSP_SIG_PATH), _T("MEAS_STAT_PARAM") /*_sizeField*/);	//32


	// Don't support all SUBORDER_TASK types:
	if ( (0 == /*wcscmp*/ _tcscmp(_subOrderTask, _T("DSCAN"))) ||	// 'Digiscan'
		 (0 == /*wcscmp*/ _tcscmp(_subOrderTask, _T("TLSCAN")))		// 'Transmitter List Scan'
	   )
	{
		::MessageBox(NULL, _T("Unsupported SUBORDER_TASK"), _T("Error"), MB_OK);	// do we even want to show a message box?

		// todo throw exception? return and do nothing? return and set return value? have to think about this.

		return false;
	}

	// Handle cases where these are missing completely, no keys/values inside? Note that this function (readSection) 
	//	only reads section data (inside <key> sectionData </key>) that does NOT have sections inside, that is, 
	//	can only read a single level of sectionData. All sectionData must be in the form "<subkey> data </subkey>" 
	//
	// From the input OR(der) XML file's <SUB_ORDER_DEF> section (I think these keys are unique, so don't really have to pass "SUB_ORDER_DEF" as the parent; doesn't hurt to)
	readSection(_T("FREQ_PARAM"),		_inputFreqParamSection, _T("SUB_ORDER_DEF"));
	readSection(_T("MDT_PARAM"),		_inputMdtParamSection, _T("SUB_ORDER_DEF"));
	readSection(_T("ANT_SET"),			_inputAntSetSection, _T("SUB_ORDER_DEF"));
	readSection(_T("TIME_PARAM"),		_inputTimeParamSection, _T("SUB_ORDER_DEF"));
	readSection(_T("TIME_PARAM_LIST"),	_inputTimeParamListSection, _T("SUB_ORDER_DEF"));
	readSection(_T("MEAS_STAT_PARAM"),	_inputMeasStatParamSection, _T("SUB_ORDER_DEF"));
	readSection(_T("MEAS_LOC_PARAM"),	_inputMeasLocParamSection, _T("SUB_ORDER_DEF"));
	readSection(_T("MEAS_PREP_PARAM"),	_inputMeasPrepParamSection, _T("SUB_ORDER_DEF"));
	readSection(_T("REQ_MEAS_PARAM"),	_inputMeasParamSection, _T("SUB_ORDER_DEF"));
	readSection(_T("RI_PARAM"),			_inputRiParamSection, _T("SUB_ORDER_DEF"));
	readSection(_T("RI_RESULT"),		_inputRiResultSection, _T("SUB_ORDER_DEF"));
	//snm-are there other sections that should be read/maintained (to be written to the output xml file??)
	// is it better to (can we?) simply iterate through all of the 'sections' inside the (input xml file) SUB_ORDER_DEF section?



	//snm-TESTING ONLY finding a value in a field inside another section (these two calls to getFieldValue)
	//getFieldValue(_T("MLL_VAL"),	_tcharStr,	_countof(_tcharStr) /*_bufSize*/, _T("MEAS_LIMIT_LINE_L"));
	//getFieldValue(_T("MLL_VAL"),	_tcharStr,	_countof(_tcharStr) /*_bufSize*/, _T("MEAS_LIMIT_LINE_U"));
	//
	//	<MEAS_LIMIT_LINE_L>
	//		<MLL_VAL>20</MLL_VAL>
	//	</MEAS_LIMIT_LINE_L>
	//
	//	<MEAS_LIMIT_LINE_U>
	//		<MLL_VAL>10</MLL_VAL>
	//	</MEAS_LIMIT_LINE_U>

	readFreqMode();


	// What type of measurement(s) were requested? This is set in the <MEAS_DATA_TYPE> field. Will be up to four (4)
	//	measurement types. Each measurement type is separated by a space. 
	//LV: Level
	//FM: Freq. Modulation
	//AM: Ampl. Modulation 
	//PM: phase modulation
	//BW: Bandwidth Meas. 
	//FO: Offset Meas.
	//FR: Frequency meas.
	//BE: Bearing (automatic: quality)
	//LO: Location (not supported, if measurement result type ADC is selected)
	//SA: SubAudioTone
	//Additional for RDS or Data row decoder
	//PI: PI Code (Hex. Code identifying radio program)
	//PR: Program (ARD, ZDF, Bayern4,�)
	//SI: Sound ID (Mono/Stereo)
	//
	//AU: Audio
	//IF: IF spectrum
	//(both without occupancy)
	//Up to 4 combinations of measurements may be requested by specification of the corresponding types separated by blank space; LO and BE shall not be combined)
	//For occupancy add letter �O�, e.g. LVO for level and level occupancy
	//Or add �OO� for occupancy only, e.g. LVOO to request occupancy values only
	//
	readMeasDataTypes();

	readMeasBandwidth();


	return true;
}

bool TciXmlUtils::CTciXmlOrderResp::readFreqMode()
{
	bool bSuccess = false;

	getFieldValue(_T("FREQ_PAR_MODE"),	_tcharStr,	_countof(_tcharStr) /*_bufSize*/, _T("FREQ_PARAM"));


	//snm-todo. Should we allocate enough space to hold the FREQ_PAR_MODE string and then use /*wcscpy_s*/ _tcscpy_s? For now it's always a single TCHAR
	_freqMode.strMode[0] = _tcharStr[0];
	_freqMode.strMode[1] = 0;

	bSuccess = true;	// what does it mean to be successful or not? Successful processing XML file? successful if process/read frequency mode that we support?

	switch (_freqMode.strMode[0])
	{
		case 'S':
		{
			FrequencyMode(SFrequencyMode::SINGLE);
			getFieldValue(_T("FREQ_PAR_S"),	_tcharStr,	_countof(_tcharStr), _T("FREQ_PARAM"));
			SingleFrequencyHz(_tcharStr);
			break;
		}

		case 'R':
		{
			FrequencyMode(SFrequencyMode::RANGE);
			getFieldValue(_T("FREQ_PAR_RG_L"),	_tcharStr,	_countof(_tcharStr), _T("FREQ_PARAM"));
			LowerFrequencyHz(_tcharStr);
			getFieldValue(_T("FREQ_PAR_RG_U"),	_tcharStr,	_countof(_tcharStr), _T("FREQ_PARAM"));
			UpperFrequencyHz(_tcharStr);

			if (0 == _tcscmp(_subOrderTask, _T("SCAN")))
			{
				getFieldValue(_T("FREQ_PAR_STEP"), _tcharStr, _countof(_tcharStr), _T("FREQ_PARAM"));
				StepSizeHz(_tcharStr);
			}
			else if (0 == _tcscmp(_subOrderTask, _T("SWEEP")))
			{
				getFieldValue(_T("FREQ_PAR_POINTS"), _tcharStr,	_countof(_tcharStr), _T("FREQ_PARAM"));
				unsigned long ulBwHz = static_cast<unsigned long>(((UpperFrequencyHz() - LowerFrequencyHz()) / (_tstoi64(_tcharStr)-1)));
				StepSizeHz(ulBwHz);
			}
			break;
		}

		case 'L':
		{
			FrequencyMode(SFrequencyMode::LIST);
			_freqMode.freqs.freqList.numFreq = 0;

			IXMLDOMNodeList *pIDOMNodeList = NULL;
			IXMLDOMNode* pIDOMNode = NULL;

			long value;
			HRESULT hr2;

			#if defined(UNICODE) || defined(_UNICODE)
				hr2 = _pXMLDom->getElementsByTagName(_T("FREQ_LST"), &pIDOMNodeList);
			#else
				BSTR bstr = TciXmlUtils::BstrFromTchar(_T("FREQ_LST"));	// todo when to call SysFreeString?
				hr2 = _pXMLDom->getElementsByTagName(bstr, &pIDOMNodeList);
			#endif

			SUCCEEDED(hr2) ? 0 : throw hr2;
			hr2 = pIDOMNodeList->get_length(&value);
			if (SUCCEEDED(hr2))
			{
				BSTR bstrFreqs;
				pIDOMNodeList->reset();

				if (value == 1)	// should be 1 because there should be a <FREQ_LST> section for the list of frequencies specified in SMS4DC. 
				{
					pIDOMNodeList->get_item(0, &pIDOMNode);
					if (pIDOMNode)
					{
						// Now get all of the frequencies (inside <FREQ_LST> section are the <FREQ> fields, each has a frequency.
						// get_text will be filled in with all frequency values, separated by a 'space'
						pIDOMNode->get_text(&bstrFreqs);	// get add descendend elements from the <FREQ_LST> section, that is, all frequencies in the <FREQ> fields inside <FREQ_LST>

						// can remove #if here and call BstrToStdString for both (different definitions of that function)
						#if 1
							tstring tstrFreqs = TciXmlUtils::BstrToStdString(bstrFreqs);
						#else
							// this works, but make better above
							tstring tstrFreqs;
							#if defined(UNICODE) || defined(_UNICODE)
								tstrFreqs = TciXmlUtils::BstrToStdString(bstrFreqs);
							#else
								tstrFreqs = TciXmlUtils::BstrToStdString(bstrFreqs);
							#endif
						#endif

						long len = tstrFreqs.length() + 1;
						TCHAR* strCopy = new TCHAR[len];
						if (strCopy)
						{
							unsigned __int64 theFreq = 0;

							_tcscpy_s(strCopy, len, tstrFreqs.c_str());

							TCHAR strTok[] = _T(" ");		// All frequencies types use ' ' as a field delimeter
							TCHAR* context = NULL;
							TCHAR* ptr = _tcstok_s(strCopy, strTok, &context);
							while (ptr)
							{
								_stscanf_s(ptr, _T("%I64u"), &theFreq);

								_freqMode.freqs.freqList.listOfFreqsHz[_freqMode.freqs.freqList.numFreq] = theFreq;
								++_freqMode.freqs.freqList.numFreq;

								ptr = _tcstok_s(NULL, strTok, &context);
							}

							delete[] strCopy;
						}
					}
				}
			}
			break;
		}

		default:
		{
			// unsupported / unknown
			_freqMode.mode = SFrequencyMode::UNKNOWN;
			break;
		}
	}


	// this is what the client of this code might do, so just testing it now (works nicely)
	// Client can get all frequencies to be tasked by calling GetFirstFreq() and GetNextFreq()
	unsigned __int64 freq;
	TCHAR tcharTemp[1000];

	if (GetFirstFreq(freq))
	{
		_stprintf_s(tcharTemp, _T("Frequency[0] = %I64u"), freq);

		while (GetNextFreq(freq))
		{
			_stprintf_s(tcharTemp, _T("Frequency[%ld] = %I64u"), index, freq);
		}
	}

	return bSuccess;
}

bool TciXmlUtils::CTciXmlOrderResp::readMeasDataTypes()
{
	bool bSuccess = false;

	getFieldValue(_T("MEAS_DATA_TYPE"),	_tcharStr,	_countof(_tcharStr));

	size_t len = _tcslen(_tcharStr)+1;	// include null
	TCHAR* measDataTypesCopy = new TCHAR[len];
	if (measDataTypesCopy)
	{
		_tcscpy_s(measDataTypesCopy, len, _tcharStr);

		TCHAR strTok[] = _T(" ");	// All measurement types use ' ' as a field delimeter, and have can be up to three of them (up to four measurement types)
		TCHAR* context = NULL;
		TCHAR* ptr = _tcstok_s(measDataTypesCopy, strTok, &context);
		while (ptr)
		{
			if ( (ptr[0]=='F' && ptr[1]=='R') || (ptr[0]=='F' && ptr[1]=='O') )	//todo: Could not get Frequency measurement to show in SMS4DC, only Frequency Offset
			{
				FreqMeasSelected(true);				// frequency (offset) measurement requested by SMS4DC
			}

			//snm - todo? Should I just read and process the first modulation measurement type? Cannot get SMS4DC to show more than one modulation measurement result
			else if (ptr[0]=='A' && ptr[1]=='M')
			{
				ModAmMeasSelected(true);			// modulation measurement (AM) requested by SMS4DC
			}
			else if (ptr[0]=='F' && ptr[1]=='M')
			{
				ModFmMeasSelected(true);			// modulation measurement (FM) requested by SMS4DC
			}
			else if (ptr[0]=='P' && ptr[1]=='M')
			{
				ModPmMeasSelected(true);			// modulation measurement (PM) requested by SMS4DC
			}

			else if (ptr[0] == 'B' && ptr[1] == 'W')
			{
				BwMeasSelected(true);				// bandwidth measurement requested by SMS4DC
			}
			else if (ptr[0]=='L' && ptr[1]=='V')
			{
				FsMeasSelected(true);				// field strength ('level') measurement requested by SMS4DC
			}
			else if (ptr[0]=='B' && ptr[1]=='E')
			{
				DfMeasSelected(true);				// DF measurement requested by SMS4DC
			}

			ptr = _tcstok_s(NULL, strTok, &context);
		}

		bSuccess = true;

		delete[] measDataTypesCopy;
	}

	return bSuccess;
}

bool TciXmlUtils::CTciXmlOrderResp::readMeasBandwidth()
{
	bool bSuccess = true;

	getFieldValue(_T("FREQ_PAR_BWIDTH"), _tcharStr, _countof(_tcharStr) /*_bufSize*/);	//512
	MeasBandwidthHz(_tcharStr);

    return bSuccess;
}

// derived class must override this to create it's approrpriate XML Response file (create the GSP response XML file)
void TciXmlUtils::CTciXmlOrderResp::writeOrderResponse(/*TCHAR* orderId*/)
{
	writeOutput(_T("<?xml version=\"1.0\" encoding=\"utf-8\" ?>"));
	writeEol();

	writeComment(_T("TCI Scorpio response an \"Order Request\" order from SMS4DC"), 0);

	writeOrderDef(_orderId, 0);
}

void TciXmlUtils::CTciXmlOrderResp::writeOrderDef(TCHAR* orderId, short level)
{
	writeTabs(level);
	writeOutput(_T("<ORDER_DEF>"));
	writeEol();

		++level;
        writeTag(_T("ORDER_ID"), orderId, level);
		writeOrderType(_orderType, _T("OR"), level);				// should read the <ORDER_TYPE> from the GSP Request file, hard-code to "GSP" for now
		writeOrderName(_orderName, _T("Example Order"), level);		// should read the <ORDER_NAME> from the GSP Request file, hard-code to "Example Order" for now
        writeOrderSender(_orderSender, _orderSenderDefault, level);		// should read the <ORDER_SENDER> from the GSP Request file, hard-code to "SMS4DC" for now
		writeOrderSenderPc(_orderSenderPc, _T("SENDER-PC"), level); // should read the <ORDER_SENDER_PC> from the GSP Request file, hard-code to "SENDER-PC" for now
        writeTag(_T("ORDER_STATE"), _T("Finished"), level);
		writeOrderCreator(_orderCreator, _T("Extern"), level);		// should read the <ORDER_CREATOR> from the GSP Request file, hard-code to "Extern" for now
		writeOrderAddressee(/*_orderAddressee*/_T("Scorpio"), _T("Scorpio"), level);	// ??? should read the <ORDER_ADDRESSEE> from the GSP Request file, hard-code to "Scorpio" for now (we are using the R&S i/f from SMS4DC, so <ORDER_ADDRESSEE> is 'ARGUS'
		writeOrderVer(_orderVersion, _T("300"), level);				// should read the <ORDER_VER> from the GSP Request file, hard-code to "300" for now
        writeTag(_T("EXECUTION_TYPE"), _T("A"), level);				// should read the <EXECUTION_TYPE> from the OR Request file?, hard-code to "A" for now (A = automatic)


		writeSubOrderDef(orderId, level);
		--level;

	writeTabs(level);
	writeOutput(_T("</ORDER_DEF>"));
	writeEol();
}

void TciXmlUtils::CTciXmlOrderResp::writeSubOrderDef(TCHAR* orderId, short level)
{
	writeTabs(level);
	writeOutput(_T("<SUB_ORDER_DEF>"));
	writeEol();

		++level;
		writeSubOrderName(_subOrderName, _T("Example SubOrder"), level);	// read from the <SUBORDER_NAME> in the OR Request file, default to to "Example SubOrder" if missing
        writeTag(_T("SUBORDER_STATE"), _T("Finished"), level);
		writeSubOrderTask(_subOrderTask, _T("FFM"), level);	// have to get this from the input OR- request XML file  (<SUBORDER_TASK>)
		writeSubOrderPrio(_subOrderPrio, _T("LOW"), level);	// have to get this from the input OR- request XML file  (<SUBORDER_PRIO>)
		writeResultType(_resultType, _T("MR"), level);		// have to get this from the input OR- request XML file  (<RESULT_TYPE>)
        writeTag(_T("RESULT_FORMAT"), _T("XML"), level);	// hard-code to "XML"

		//snm-try (put into <ORDER_DEF> section instead of in <SUB_ORDER_DEF> section, see if can see error message in SMS4DC: no difference, so just keep it here
		writeActDef(orderId, level);

		writeFreqParam(level);
		writeMdtParamSection(level);
		writeAntSetSection(level);
		writeTimeParamSection(level);
		writeTimeParamListSection(level);
		writeMeasStatParamSection(level);
		writeMeasLocParamSection(level);
		writeMeasPrepParamSection(level);
		writeMeasParamSection(level);
		writeRiParamSection(level);
		writeRiResultSection(level);

		writeMeasDataHdr(level);	// start writing the actual measurement data (header first, data inside the datahdr section, unless there is any measurement error)
		--level;

	writeTabs(level);
	writeOutput(_T("</SUB_ORDER_DEF>"));
	writeEol();
}

void TciXmlUtils::CTciXmlOrderResp::writeSubOrderName(TCHAR* tagValue, TCHAR* tagValueDefault, short level)
{
	writeTag(_T("SUBORDER_NAME"), (tagValue && tagValue[0]) ? tagValue : tagValueDefault, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeSubOrderTask(TCHAR* tagValue, TCHAR* tagValueDefault, short level)
{
	writeTag(_T("SUBORDER_TASK"), (tagValue && tagValue[0]) ? tagValue : tagValueDefault, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeSubOrderPrio(TCHAR* tagValue, TCHAR* tagValueDefault, short level)
{
	writeTag(_T("SUBORDER_PRIO"), (tagValue && tagValue[0]) ? tagValue : tagValueDefault, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeResultType(TCHAR* tagValue, TCHAR* tagValueDefault, short level)
{
	writeTag(_T("RESULT_TYPE"), (tagValue && tagValue[0]) ? tagValue : tagValueDefault, level);
}


void TciXmlUtils::CTciXmlOrderResp::writeFreqParam(short level)
{
	writeSection(_T("FREQ_PARAM"), _inputFreqParamSection, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeMdtParamSection(short level)
{
	writeSection(_T("MDT_PARAM"), _inputMdtParamSection, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeAntSetSection(short level)
{
	writeSection(_T("ANT_SET"), _inputAntSetSection, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeTimeParamSection(short level)
{
	writeSection(_T("TIME_PARAM"), _inputTimeParamSection, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeTimeParamListSection(short level)
{
	writeSection(_T("TIME_PARAM_LIST"), _inputTimeParamListSection, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeMeasStatParamSection(short level)
{
	writeSection(_T("MEAS_STAT_PARAM"), _inputMeasStatParamSection, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeMeasLocParamSection(short level)
{
	writeSection(_T("MEAS_LOC_PARAM"), _inputMeasLocParamSection, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeMeasPrepParamSection(short level)
{
	writeSection(_T("MEAS_PREP_PARAM"), _inputMeasPrepParamSection, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeMeasParamSection(short level)
{
	writeSection(_T("REQ_MEAS_PARAM"), _inputMeasParamSection, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeRiParamSection(short level)
{
	writeSection(_T("RI_PARAM"), _inputRiParamSection, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeRiResultSection(short level)
{
	writeSection(_T("RI_RESULT"), _inputRiResultSection, level);
}

//snm-put this function in the 'correct' place (ordered nicely, in .h file too)
// start writing the actual measurement data (header first, data inside the datahdr section)
void TciXmlUtils::CTciXmlOrderResp::writeMeasDataHdr(short level)
{
	// Do not write any measurement data if the client indicated that there is a measurement error
	if (MeasurementStatus() != MEAS_ERROR)
	{
		writeTabs(level);
		writeOutput(_T("<MEAS_DATA_HDR>"));
		writeEol();

			++level;
			//snm-Q1: do I want to have specific functions for each of these? I do that elsewhere (see TciXmlUtils::CTciXmlGspResp::WriteOrderDef() for example).
			// A1: probably yes
			//
			// Q2: is that going overboard? 
			// A2: Probably not (may want to use member variables to be the source of each of the key's values, so separate functions are a nice way to do that.
			// For now (temp), just hard-code values for these, so don't need functions for each yet.

			writeTag(_T("MD_ID_NAME"), _T("TCI SMS Server") /*_T("1_000065_040225_0101")*/, level);	// Name of Spectrum Monitoring application meas.result (what is this?)
			writeTag(_T("MSP_SIG_PATH"), _MSP_SIG_PATH, level);		// Name of the measurement station (originally taken from GSP's <MSP_SIG_PATH>)

			#ifdef SIMULATE_DATA
				SiteType(TciXmlUtils::SITETYPE_FIXED);	//1=Fixed, 2=Mobile			// The (Scorpio) client should call this mutator after getting the server's capability (which includes the server type)
			#endif
			writeMeasDataHdrSiteType(level);

		
			// Coordinates of the measurement station:
			#ifdef SIMULATE_DATA
				SiteLong(-121.555);	// The (Scorpio) client should call this mutator after getting the server's longitude
			#endif
			writeHeasDataHdrSiteLong(level);

			#ifdef SIMULATE_DATA
				SiteLat(37.999);	// The (Scorpio) client should call this mutator after getting the server's latitude
			#endif
			writeMeasDataHdrSiteLat(level);

			#ifdef SIMULATE_DATA
				SiteHeight(1234);	// The (Scorpio) client should call this mutator after getting the server's height
			#endif
			writeMeasDataHdrHeight(level);

			//start/stop: can we use? 1899-12-30T00:00:00.000000+07:00 ??
			//snm todo. Consider adding a couple of mutators that the client can call to set the start and stop times of the mesurement, or 
			//  
	//snm-do we need this at all? each measurement result has its own date/time:		writeTag(_T("MD_M_START"), _T("2013-11-05T14:22:10.155000+07:00"), level);		// Start time of measurement 
	//snm-do we need this at all? each measurement result has its own date/time:		writeTag(_T("MD_M_STOP"),  _T("2013-11-05T14:22:30.155000+07:00"), level);		// Stop time of measurement 
			writeTag(_T("MD_M_REF"), _subOrderName, level);		// Reference of measurement (what is this?). Doc says this is required (use _subOrderName? default to ??? if missing?)
		
			//MR: Measurement Result
			//CMR: Compressed Meas. Results
			//AMR: Measurement Result during an Alarm 
			//LOG: Start and End of an Alarm
			//AMR_CMR: Measurement Result during and Compressed Meas. Results outside an Alarm)
			//MH MaxHold
			//XT: Text
			writeTag(_T("MD_RESULT_TYPE"), _T("MR"), level);	// Doc says this is required (not in rs example)

#ifdef SIMULATE_DATA
			// Now write sets of <MEAS_DATA> sections for the actual measurement data (from sms server)
			unsigned __int64 measFreqHz = 88100000;

			SYSTEMTIME measTime;
			GetSystemTime(&measTime);
			//GetLocalTime(&measTime);

			// For LV-Level (field strength?) measurement results
			double measDataFs = 20.0;				// field strength 'level' (is also used with the 'BE', aka DF measurement results - shown in SMS4DC DF results chart)
			TCHAR measDataFsUnit[/*2*/] = _T("D");	// Level unit (D = dB�v/m); others as full text (only for level/aka fs??)

			// For BE (DF bearing/quality) measurement results
			double measDataDfBearing = 10.0;	// azim (?)
			short measDataDfQuality = 80;		// DF confidence?


			// For FO (Frequency Offset) measurement results
			double measDataFreqOffHz = 100.0;		// Hz
			TCHAR measDataFreqOffUnit[2] = _T("H");	// H(z), K(Hz), M(Hz), G(Hz)

			// For BW (Bandwidth) measurement results
			double measDataBwKhz = 5.0;				// kHz ??
			TCHAR measDataBwUnit[2] = _T("K");		// H(z), K(Hz), M(Hz), G(Hz)

			// For AM, FM, PM (Modulation) measurement results (don't know how to show more than the first MODULATION result in SMS4DC
			unsigned long measDataMod = 5;			// %, kHz or radians
//hard-code based on modulation type			TCHAR measDataModUnit[2] = _T("%");		// %, k(Hz), r(adians)		// will be "%" for AM, "k" for FM and "r" for PM
//get from client now			TCHAR measDataModType[2] = _T("A");		// A(mplitude), F(requency), P(hase)


			// Simulate measurement results for now:
			// 1. Call mutator function(s) to store measurement data (like what the Scorpio client might do)
			// 2. Then use accessor function(s) to get that data out and write to the ORDER results XML file (this only what this namespace would do)
		
			// Do #1
			for (int i=1; i<=10; ++i)
			{
				// client will be calling these functions after getting measurement result(s) from a TCI server
				StoreMeasurementDatum_Fs(measFreqHz, measTime, measDataFs, measDataFsUnit);
				StoreMeasurementDatum_Df(measFreqHz, measTime, measDataDfBearing, measDataDfQuality);
				StoreMeasurementDatum_FrOff(measFreqHz, measTime, measDataFreqOffHz);
				StoreMeasurementDatum_Bw(measFreqHz, measTime, measDataBwKhz);

//				StoreMeasurementDatum_Mod(measFreqHz, measTime, measDataMod, TciXmlUtils::MODULATION_AM);
				//snm?
				if (ModAmMeasSelected())
				{
					StoreMeasurementDatum_Mod(measFreqHz, measTime, measDataMod, TciXmlUtils::MODULATION_AM);
				}
				if (ModFmMeasSelected())
				{
					StoreMeasurementDatum_Mod(measFreqHz, measTime, measDataMod, TciXmlUtils::MODULATION_FM);
				}
				if (ModPmMeasSelected())
				{
					StoreMeasurementDatum_Mod(measFreqHz, measTime, measDataMod, TciXmlUtils::MODULATION_PM);
				}


				measFreqHz += 25000;
				//measFreqHz += 200000;
				Sleep(11);
				GetSystemTime(&measTime);

				measDataFs += i /3;
				//measDataFs += i;
				if (measDataFs > 100.0)
					measDataFs = 20.0;

				measDataDfBearing += i/2;
				//measDataDfBearing += i;
				if (measDataDfBearing > 360.0)
					measDataDfBearing = 0.0;

				measDataFreqOffHz += i/4;
				if (measDataFreqOffHz > 200000.0)
					measDataFreqOffHz = 100.0;

				measDataBwKhz += i/5;
				if (measDataBwKhz > 200000.0)
					measDataBwKhz = 5.0;

				measDataMod += i;
				if (measDataMod > 99)
					measDataMod = 5;
			}
#else
			TCHAR measDataFsUnit[/*2*/] = _T("D");	// Level unit (D = dB�v/m); others as full text (only for level/aka fs??)
			TCHAR measDataFreqOffUnit[2] = _T("H");	// H(z), K(Hz), M(Hz), G(Hz)
			TCHAR measDataBwUnit[2] = _T("K");		// H(z), K(Hz), M(Hz), G(Hz)
#endif	// SIMULATE_DATA

			// Now do #2 (iterate through map of measurement results, writing to XML file)
			// Should the client (here) be responsible to decide which modulation measurement results to show (value, units). I can request AM, FM and PM modulation measurements 
			//	from SMS4DC, but I don't know how to show them all; I can get only one to show!
			for (MEASUREMENTDATA::const_iterator iter = _measurementData.cbegin(); iter != _measurementData.cend(); iter++)	// go through map by frequency? or insert into the map sorted by frequency?
			{
				writeMeasData(iter->first, iter->second.measTime,								// measurement frequency and time
					iter->second.bHasFs, iter->second.measDataFs, measDataFsUnit,				// Field strength result and unit (if Field Strength measurement requested)
					iter->second.bHasDf, iter->second.measDataDfBearing, iter->second.measDataDfQuality,	// DF bearing and quality (if DF measurement requested)
					iter->second.bHasFr, iter->second.measDataFreqOffHz, measDataFreqOffUnit,	// Frequency Offset result and unit (if Frequency Offset measurement requested)
					iter->second.bHasBw, iter->second.measDataBwKhz, measDataBwUnit,			// Bandwidth result and unit (if Bandwidth measurement requested)
					iter->second.bHasMod, iter->second.measDataMod, iter->second.measModType,	// Modulation result, type (AM, FM, PM) - unit will be determined base on type (if Modulation measurement requested)
					level);

			}
			--level;

		writeTabs(level);
		writeOutput(_T("</MEAS_DATA_HDR>"));
		writeEol();
	}
}



// Write the type of measurement station
void TciXmlUtils::CTciXmlOrderResp::writeMeasDataHdrSiteType(short level)
{
	// Type of measurement station
	//	F = fixed
	//	M = mobile

	// Get the site type (Fixed or Mobile) from somewhere else (if in Scorpio Client, get from a server's capabilities and call SiteType(unsigned long siteType)
	TCHAR* tagSiteType;
	switch (SiteType())
	{
		case TciXmlUtils::SITETYPE_MOBILE:	// 2 = Mobile Site
		{
			tagSiteType = _T("M");
			break;
		}

		default:
		{
			tagSiteType = _T("F");
			break;
		}
	}

    writeTag(_T("MD_S_TYPE"), tagSiteType, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeHeasDataHdrSiteLong(short level)
{
	double siteLong = SiteLong();

	const size_t bufSize = 32;
	TCHAR tcharStr[bufSize];
	_stprintf_s(tcharStr, _T("%.6lf"), siteLong);
    writeTag(_T("MD_S_LONG"), tcharStr, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeMeasDataHdrSiteLat(short level)
{
	double siteLat = SiteLat();

	const size_t bufSize = 32;
	TCHAR tcharStr[bufSize];
	_stprintf_s(tcharStr, _T("%.6lf"), siteLat);
    writeTag(_T("MD_S_LAT"), tcharStr, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeMeasDataHdrHeight(short level)
{
	signed long siteHeight = SiteHeight();

	const size_t bufSize = 32;
	TCHAR tcharStr[bufSize];
	_stprintf_s(tcharStr, _T("%ld"), siteHeight);
    writeTag(_T("MD_S_H_NN"), tcharStr, level);
}

void TciXmlUtils::CTciXmlOrderResp::writeMeasData(unsigned __int64 measFreqHz, SYSTEMTIME measTime,		// measurement frequency and time (UTC)
						bool bHasFs, double measDataFs, TCHAR* measDataFsUnit,				// Field strength result and unit (always "D" ?) (if Field Strength measurement requested)
						bool bHasDf, double measDataDfBearing, short measDataDfQuality,		// DF bearing and quality (if DF measurement requested)
						bool bHasFr, double measDataFreqOffHz, TCHAR* measDataFreqOffUnit,	// Frequency Offset result and unit (always "H" ?) (if Frequency Offset measurement requested)
						bool bHasBw, double measDataBw, TCHAR* measDataBwUnit,				// Bandwidth result and unit (always "H" ?) (if Bandwidth measurement requested)
						bool bHasMod, unsigned long measDataMod, TciXmlUtils::EModulationType modulationType,	// Modulation result, type (AM, FM, PM) - unit will be determined base on type (if Modulation measurement requested)
						short level)
{
	const size_t bufSize = 64;
	TCHAR tBuf[bufSize];		// could use /*std::wstring*/ tstring and '/*std::wstring*/ tstring format()' instead of /*swprintf_s*/ _stprintf_s (/*std::wstring*/ tstring can increase in size if needed, but probably must slower)

	writeTabs(level);
	writeOutput(_T("<MEAS_DATA>"));
	writeEol();

		++level;
		// Center Frequency (channel) and Time of the measurement data (required for all measurement data types) 
		_stprintf_s(tBuf, _T("%I64u"), measFreqHz);
		writeTag(_T("MD_M_FREQ"), tBuf, level);			// Frequency (channel) of measurement (Hz)
		tstring wstrDateTime;
		wstrDateTime = TciXmlUtils::Format(_T("%04d-%02d-%02dT%02d:%02d:%02d.%03d000%c%02d:%02d"), 
										measTime.wYear, measTime.wMonth, measTime.wDay, 
										measTime.wHour, measTime.wMinute, measTime.wSecond, measTime.wMilliseconds, 
										_timeZonePlus ? _T('+') : _T('-'), _timeZoneHours, _timeZoneMinutes);
		writeTag(_T("MD_TIME"), wstrDateTime, level);

		if (FsMeasSelected() && bHasFs)		// Just because the ORder input xml file asked for the Level (Field Strength) measurement (FsMeasSelected()) doesn't mean we actually got them (bHasFs)
		{
			// Level (Field Strength) measurement data *********************************************************
			_stprintf_s(tBuf, _T("%0.1lf"), measDataFs);
			writeTag(_T("MD_LEV"), tBuf, level);				// field strength measurement value ?
			writeTag(_T("MD_D_LEV_U"), measDataFsUnit, level);	// unit of the measurement (D= dBuv/m)
		}

		if (DfMeasSelected() && bHasDf)		// Just because the ORder input xml file asked for the Bearing (DF) measurement (DfMeasSelected()) doesn't mean we actually got them (bHasDf)
		{
			// Bearing (Direction and Quality) measurement data ************************************************
			_stprintf_s(tBuf, _T("%0.1lf"), measDataDfBearing);
			writeTag(_T("MD_DIR"), tBuf, level);			// Azimuth measurement value
			_stprintf_s(tBuf, _T("%d"), measDataDfQuality);
			writeTag(_T("MD_QUALITY"), tBuf, level);		// quality of measurement (df confidence ?)
		}

		if (FreqMeasSelected() && bHasFr)		// Just because the ORder input xml file asked for the Frequency (offset) measurement (FreqMeasSelected()) doesn't mean we actually got them (bHasFr)
		{
			// Frequency Offset measurement data ***************************************************************
			// could not get the frequency measurement to show in SMS4DC, try Frequency Offset
			_stprintf_s(tBuf, _T("%0.1lf"), measDataFreqOffHz);
			writeTag(_T("MD_FR_OFF"), tBuf, level);			// frequency offset measurement value
			writeTag(_T("MD_D_FR_OFF_U"), measDataFreqOffUnit, level);	// Unit of frequency offset measurement: H(z), K(Hz), M(Hz), G(Hz).
		}

		if (BwMeasSelected() && bHasBw)		// Just because the ORder input xml file asked for the Bandwidth measurement (BwMeasSelected()) doesn't mean we actually got them (bHasBw)
		{
			// Bandwidth measurement data **********************************************************************
			_stprintf_s(tBuf, _T("%0.1lf"), measDataBw);
			writeTag(_T("MD_BW"), tBuf, level);				// bandwidth measurement value
			writeTag(_T("MD_D_BW_U"), measDataBwUnit, level);		// Unit of bandwidth measurement: H(z), K(Hz), M(Hz), G(Hz)
		}

		if (ModMeasSelected() && bHasMod)		// Just because the ORder input xml file asked for the Modulation measurement (ModMeasSelected()) doesn't mean we actually got them (bHasMod)
		{
			//snm todo. Can this function be used (by client) to set the one-and-only-one type of modulation measurement results (AM, FM or PM) ?
			// Modulation measurement data *********************************************************************
			// Can only get the first modulation type (MD_D_MOD_T, MD_D_MOD_U and MD_MOD) to show in SMS4DC - so far
			_stprintf_s(tBuf, _T("%ld"), measDataMod);
			writeTag(_T("MD_MOD"), tBuf, level);			// modulation measurement value
			TCHAR measDataModType[2];	// Modulation type: A(mplitude), F(requency), P(hase)
			TCHAR measDataModUnit[2];	// Unit of modulation measurement: %, k(Hz), r(adians)
			measDataModType[1] = measDataModUnit[1] = 0;

			switch (modulationType)
			{
				case TciXmlUtils::MODULATION_FM:
					measDataModType[0] = _T('F');
					measDataModUnit[0] = _T('k');
					break;

				case TciXmlUtils::MODULATION_PM:
					measDataModType[0] = _T('P');
					measDataModUnit[0] = _T('r');
					break;

				case TciXmlUtils::MODULATION_AM:
				default:		// should not happen
					measDataModType[0] = _T('A');
					measDataModUnit[0] = _T('%');
					break;
			}
			writeTag(_T("MD_D_MOD_T"), measDataModType, level);			// Modulation type: A(mplitude), F(requency), P(hase) - ignored??
			writeTag(_T("MD_D_MOD_U"), measDataModUnit, level);			// Unit of modulation measurement: %, k(Hz), r(adians)

			// Cannot get the mod2 or mod3 to show in SMS4DC GUI; I don't know what I'm doing wrong:
			#if 0
			/*swprintf_s*/ _stprintf_s(tBuf, /* bufSize, */ _T("%ld"), (bEven) ? (measTime.wMilliseconds*4)%100 : (measTime.wMilliseconds*5)%100);	//TODO. pass in frequency measurement results (for now, just use the frequency of the measurement +-
			writeTag(_T("MD_MOD2"), tBuf, level);			// modulation measurement value
			writeTag(_T("MD_D_MOD2_T"), _T("F"), level);		// Modulation type: A(mplitude), F(requency), P(hase) - ignored??
			writeTag(_T("MD_D_MOD2_U"), _T("%"), level);		// Unit of modulation measurement: %, k(Hz), r(adians)
			/*swprintf_s*/ _stprintf_s(tBuf, /* bufSize, */ _T("%ld"), (bEven) ? (measTime.wMilliseconds*6)%100 : (measTime.wMilliseconds*7)%100);	//TODO. pass in frequency measurement results (for now, just use the frequency of the measurement +-
			writeTag(_T("MD_MOD3"), tBuf, level);			// modulation measurement value
			writeTag(_T("MD_D_MOD3_T"), _T("P"), level);		// Modulation type: A(mplitude), F(requency), P(hase) - ignored??
			writeTag(_T("MD_D_MOD3_U"), _T("%"), level);		// Unit of modulation measurement: %, k(Hz), r(adians)
			#endif
		}
		--level;

	writeTabs(level);
	writeOutput(_T("</MEAS_DATA>"));
	writeEol();
}
