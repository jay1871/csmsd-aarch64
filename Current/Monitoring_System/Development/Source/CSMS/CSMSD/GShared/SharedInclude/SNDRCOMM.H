/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2005 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/
/*
        THIS FILE IS TO BE USED WITH AN INTERFACE TO THE ANY SUPPORTED SOUNDER SYSTEM

        (there are other header files specific to a sounder system,
         e.g., sndr9028/sndrmsg.h, one of which must also be included)

*/

#ifndef SOUNDERCOMMON_HOST_MSG_DEF
#define SOUNDERCOMMON_HOST_MSG_DEF


#define MAX_SCAN_LINES 568
#define MAX_SCAN_POINTS 10
#define MAX_FINISHED_IONOGRAM_POINTS 50

#define MAX_SOUNDER_MSG_BUFFER	16384

#define MSG_BASE 128
#define MSG_SOUNDER_TEST                MSG_BASE+1
#define MSG_SOUNDER_TEST_RESP		MSG_BASE+2
#define MSG_SOUNDER_START               MSG_BASE+3
#define MSG_SOUNDER_START_RESP		MSG_BASE+4
#define MSG_SOUNDER_STOP                MSG_BASE+5
#define MSG_SOUNDER_STOP_RESP		MSG_BASE+6
#define MSG_SOUNDER_SETUP               MSG_BASE+7
#define MSG_SOUNDER_SETUP_RESP		MSG_BASE+8
#define MSG_SOUNDER_BLANKER_FREQ	MSG_BASE+9
#define MSG_SOUNDER_BLANKER_FREQ_RESP	MSG_BASE+10
#define MSG_SOUNDER_ABORT               MSG_BASE+11
#define MSG_SOUNDER_ABORT_RESP		MSG_BASE+12
#define MSG_SOUNDER_SCHEDULE            MSG_BASE+13
#define MSG_SOUNDER_SCHEDULE_RESP       MSG_BASE+14
#define MSG_SOUNDER_ION_DATA            MSG_BASE+18
#define MSG_SOUNDER_ION_DATA_ACK	MSG_BASE+19
#define MSG_SOUNDER_ION_DATA_REQ        MSG_BASE+20     // For DFService, not for 820
/* 
 * Status indicators
 */
#define STATUS_OK	1
#define STATUS_BAD  0

#define SOUNDER_IDLE                    0
#define SOUNDER_ACTIVE			1
#define SOUNDER_NOTRESPONDING	2

#define RES_IONOGRAM_DATA	123
#define RES_SOUNDER_RESP	124

typedef struct
{
	unsigned short int	length;
	unsigned char		src;
	unsigned char		dst;
	unsigned char		msgId;
	unsigned char		body[MAX_SOUNDER_MSG_BUFFER];
} SOUNDER_HOST_MSG;

typedef struct
{
	unsigned long status;
}GENERIC_STATUS_MSG;

typedef struct
{
	short int interval;
	short int hour;
	short int min;
	short int sec;
}SOUNDER_SCHEDULE;

typedef struct
{
	long testFreq;
	short int onOffFlag;
	short int powerLevel;
} SOUNDER_SIG_GEN;

typedef struct
{
	float freq;
	float trueHeight;
	float virtualHeight;
}IONOGRAM_POINT;


typedef struct 
{
	short int numPoints;
	IONOGRAM_POINT ionData[MAX_FINISHED_IONOGRAM_POINTS];
} FINISHED_IONOGRAM;

typedef struct
{
	unsigned short int numScanLines;	 								// Number Of Scan Lines
	unsigned short int startFreq;									// Start Freq in 10KHz
	unsigned short int stepSize;                                     // Step Size in Khz
	unsigned short int rawIonogram[MAX_SCAN_LINES*MAX_SCAN_POINTS];	
} RAW_IONOGRAM;


#endif	// SOUNDERCOMMON_HOST_MSG_DEF
