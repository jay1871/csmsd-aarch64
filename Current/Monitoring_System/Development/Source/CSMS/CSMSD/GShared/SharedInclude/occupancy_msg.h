/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2005 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/
#ifndef OCCUPANCY_MSG_H
#define OCCUPANCY_MSG_H

#include "OccupancyDefs.h"
#include "dsp_msg.h"

// message subtypes for message type OCCUPANCY_CTRL
#define GET_OCCUPANCY			1L
#define SUSPEND_OCCUPANCY		2L
#define RESUME_OCCUPANCY		3L
#define RESTART_OCCUPANCY		4L
#define OCC_DATA_RESTART_RESPONSE 5L
#define OCC_NODATA_RESTART_RESPONSE 6L
#define VALIDATE_OCCUPANCY		7L
#define GET_TASK_STATE			8L
#define GET_OCCUPANCY_SCANDF	9L
#define VALIDATE_OCCUPANCY_SCANDF 10L
#define GET_AUTOVIOLATE			11L		// added for AVD
#define VALIDATE_AUTOVIOLATE	12L		// added for AVD
#define NEXT_OCCUPANCY			32769L
#define SEND_OCCUPANCY_RESPONSE	32770L
#define AVD_MEAS				32771l	// added for AVD
#define VALIDATE_OCC_RESPONSE	65537L
#define TASK_STATUS				65538L
#define OCC_CHANNEL_RESPONSE	65539L
#define EFLD_CHANNEL_RESPONSE	65540L
#define OCC_TIMEOFDAY_RESPONSE	65541L
#define MSGLEN_CHANNEL_RESPONSE	65542L
#define MSGLEN_DIST_RESPONSE	65543L
#define OCCUPANCY_STATE_RESP	65544L
#define OCCUPANCY_SOLICIT_STATE_RESP 65545L
#define OCC_FREQUENCY_RESULT	65546L
#define OCC_CHANNEL_RESULT		65547L
#define EFLD_CHANNEL_RESULT		65548L
#define OCC_TIMEOFDAY_RESULT	65549L
#define MSGLEN_CHANNEL_RESULT	65550L
#define OCC_SPECGRAM_RESULT		65551L
#define OCC_DATA_RESTART_REQUEST 65552L
#define SCANDF_DATA_RESULT		65553L
#define AUTOVIOLATE_FREQMEAS_RESULT	65556L	// added for AVD
#define AUTOVIOLATE_BWMEAS_RESULT	65557L	// added for AVD
#define AVD_FREQUENCY_RESULT	65558L
#define AVD_CHANNEL_RESULT		65559L
#define AVD_FREQ_MEASURE		65560L
#define AVD_CHANNEL_MEASURE		65561L
#define AVD_FREQMEAS_MEASURE	65562L	
#define AVD_BWMEAS_MEASURE		65563L	
#define OCC_INTER_DATA_RESULT	65564L	// this is RA3 intermediate data


// Occupancy Command message body
#define MAX_OCCBANDS 100
#define MAX_OCCCHANNELS 1000
#define MAX_OCCRESULTCHANNELS 6000
#define MAX_OCCDWELLS 1000

#define OCCUPANCY_INCLUDE 0
#define OCCUPANCY_EXCLUDE 1
#define FIXEDDURATIONMETHOD 0
#define CONFIDENCEDURATIONMETHOD 1
#define FIXEDTHRESHOLDMETHOD 0
#define NOISETHRESHOLDMETHOD 1
#define FIXEDNOISETHRESHOLDMETHOD 2

typedef struct	// all following are true/false flags
{
	unsigned char occupancyVsChannel;
	unsigned char EFieldVsChannel;
	unsigned char occupancyVsTimeOfDay;
	unsigned char msglengthVsChannel;
	unsigned char msglengthDistribution;
	unsigned char spectrogram;
	unsigned char timegram;
} occupancyOutputFlags;


// Indicies into occPrimaryThreshold[] and occSecondaryThreshold[] in getOccupancyCmd struct
#define FIXEDthresholdIndex	(0)	
#define NOISEthresholdIndex	(1)
typedef struct 
{
	unsigned long numBands;		// number of frequency bands
	unsigned long lowFrequency[MAX_OCCBANDS];	// low frequency (Hz)
	unsigned long highFrequency[MAX_OCCBANDS];	// high frequency (Hz), = low frequency for single channel
	unsigned long channelBandwidth[MAX_OCCBANDS]; // (Hz)
	unsigned long includeExclude[MAX_OCCBANDS];
	unsigned long storageTime;		// seconds
	unsigned long durationMethod;
	unsigned long measurementTime;	// seconds
	unsigned long confidenceLevel;	// percent
	unsigned long desiredAccuracy;	// percent
	unsigned long thresholdMethod;	// for RA3 -- added FIXEDNOISETHRESHOLDMETHOD
	unsigned long useSecondaryThreshold;  // 0 = don't use  >0 = use (for RA3 intermediate data)
	// following two parameters
	//  the FIXED threshold (dBuV/m) is always in index 0
	//  the NOISE threshold (dB) is always in index 1 
	//  which thresholds used are based upon thresholdMethod
	//  any unused thresholds do not need to be set (won't be validated)
	unsigned long occPrimaryThreshold[2];
	unsigned long occSecondaryThreshold[2]; // used only if useSecondaryThreshold >0
	unsigned long occupancyMinGap;	// seconds
	occupancyOutputFlags Output;
	unsigned long saveIntermediateData;  // 0 = don't save >0 = save (RA3 data)
	unsigned long ant;  // ANT1 = 1 = ref; ANT2 = 2 = aux
	bool forceNarrowBand;
} getOccupancyCmd;

// Get/Validate Autoviolate Command message body
#define AVD_RATE_AUTO	(0)
#define AVD_RATE_ALL	(1)
#define AVD_INCLUDE		(0)
#define AVD_EXCLUDE		(0)	// Current cannot exclude bands in AVD !
typedef struct 
{
	unsigned long numBands;		// number of frequency bands
	unsigned long lowFrequency[MAX_OCCBANDS];	// low frequency (Hz)
	unsigned long highFrequency[MAX_OCCBANDS];	// high frequency (Hz), = low frequency for single channel
	unsigned long channelBandwidth[MAX_OCCBANDS]; // (Hz)
	unsigned long includeExclude[MAX_OCCBANDS];
	unsigned long storageTime;		// seconds
	unsigned long measurementTime;	// seconds
	unsigned long AVDThreshold;		// dB above NOISE
	unsigned long measurementRate;	// see defines AVD_RATE_xxx
	unsigned long ant;  // ANT1 = 1 = ref; ANT2 = 2 = aux
	unsigned long unused[11];		// needed to make this Cmd > getOccupancyCmd
} getAutoViolateCmd;

typedef struct 
{
	getOccupancyCmd getCmd;
	double dateTime;
	int state;
	unsigned long taskID;
	unsigned long key;
} restartOccupancyCmd;

typedef struct
{
	unsigned long taskID;			// equipcontrol ID for task (from start msg)
	unsigned long key;				// equipcontrol key for task (from start msg)
} suspendOccupancyCmd;

typedef struct
{
	unsigned long taskID;			// equipcontrol ID for task (from start msg)
	unsigned long key;				// equipcontrol key for task (from start msg)
} resumeOccupancyCmd;

typedef struct
{
	unsigned long taskID;			// equipcontrol ID for task (from start msg)
	unsigned long key;				// equipcontrol key for task (from start msg)
} getTaskStateCmd;

// Occupancy Response message bodies
typedef struct
{
	long			status;
	getOccupancyCmd	occCmd;
} validateOccupancyResp;

typedef struct
{
	long			status;
	GPS_RESPONSE	GPSResponse;
	unsigned long	numTotalChannels;
	unsigned long	firstChannel;
	unsigned long	numChannels;
	unsigned long	numTimeOfDays;
} occupancyHeader;

typedef struct
{
	occupancyHeader occHdr;
	resultData FreqDist[MAX_OCCRESULTCHANNELS];
} occupancyVsChannelResult;

typedef struct
{
	occupancyHeader occHdr;
	resultData EFieldFreqDist[MAX_OCCRESULTCHANNELS];
} EFieldVsChannelResult;

typedef struct
{
	occupancyHeader occHdr;
	resultData TimeOfDay[MAX_OCCRESULTCHANNELS];
} occupancyVsTimeOfDayResult;

typedef struct
{
	occupancyHeader occHdr;
	resultData MessageLength[MAX_OCCRESULTCHANNELS];
} msglengthVsChannelResult;

typedef struct
{
	occupancyHeader occHdr;
	resultData EField[MAX_OCCRESULTCHANNELS];
} spectrogramResult;

typedef struct
{
	occupancyHeader occHdr;
	missionData FreqDist[MAX_OCCCHANNELS];
} occupancyVsChannelResp;

typedef struct
{
	occupancyHeader occHdr;
	statsData EFieldFreqDist[MAX_OCCCHANNELS];
} EFieldVsChannelResp;

typedef struct
{
	occupancyHeader occHdr;
	missionData TimeOfDay[MAX_OCCCHANNELS];
} occupancyVsTimeOfDayResp;

typedef struct
{
	occupancyHeader occHdr;
	statsData MessageLength[MAX_OCCCHANNELS];
} msglengthVsChannelResp;

typedef struct
{
	long channel;
	float length;
} MSGLENHISTDATA;

typedef struct
{
	occupancyHeader occHdr;
	MSGLENHISTDATA	histData[MAX_OCCCHANNELS];
} msglengthDistributionResp;

typedef struct
{
	occupancyHeader occHdr;
	unsigned long Frequencies[MAX_OCCCHANNELS];
	int numBands;
	unsigned long numChannels[MAX_OCCBANDS];
	unsigned long saveIntermediateData;  // 0 - don't save; > 0 - save (RA3 data)
	unsigned long	useSecondaryThreshold;  // 0 = don't use  >0 = use
	// following two parameters
	//  the FIXED threshold (dBuV/m) is always in index 0
	//  the NOISE threshold (dB) is always in index 1 
	//  which thresholds used are based upon thresholdMethod in command
	unsigned long occPrimaryThreshold[2];
	unsigned long occSecondaryThreshold[2]; // used only if useSecondaryThreshold >0
	unsigned long selectedAntenna;  //uses ANT1 & ANT2 defines in dsp_msg.h
	unsigned long chanBandwidth;    // in Hz -- reserved -- currently not used
	char hostName[32];
} frequencyVsChannelResp;

typedef struct
{
	occupancyHeader occHdr;
	int numAzimuths;
	int numChannels;
	int scanDFData[MAX_OCCCHANNELS];
	int aveRange[MAX_OCCCHANNELS];    // 1/100ths kilometers
	int aveFldStr[MAX_OCCCHANNELS];    // dBuV/m
} scanDFVsChannelResp;

// AVD Result Messages
typedef struct
{
	occupancyHeader occHdr;
	measureData FrequencyMeas[MAX_OCCCHANNELS];
} AVDFrequencyResult;

typedef struct
{
	occupancyHeader occHdr;
	measureData BWMeas[MAX_OCCCHANNELS];
} AVDBandwidthResult;

// end of AVD result messages

#define MAX_RESTART_BODY sizeof(occupancyVsChannelResp)
typedef struct
{
	unsigned long taskID;
	unsigned long subType;
	char body[MAX_RESTART_BODY];
} occupancyDataRestart;

typedef struct
{
	unsigned long taskID;
	unsigned long subType;
	unsigned long channel;
} databaseAck;

typedef struct
{
	unsigned long taskID;
	unsigned long subType;
} occupancyNoDataRestart;

typedef struct
{
	unsigned long measureID;
	unsigned long taskID;
	unsigned long subType;
	unsigned long firstChannel;
} occupancyDataRestartRequest;

#define STATE_IDLE 0
#define STATE_RUNNING 1
#define STATE_SUSPENDED 2
typedef struct
{
	int state;
	double completionTime;
} occupancyStateResp;

typedef struct 
{
	unsigned long	taskID;
} nextOccupancyCmd;

typedef struct
{
	unsigned long	taskID;
	long	status;
	int		index;
	long	firstChannel;
} sendOccupancyResponse;

typedef struct
{
	unsigned long taskID;
	unsigned long frequency;
	unsigned long bandwidth;
} AVDMeasCmd;

typedef struct
{
	float			noiseFloor;		// noise floor in uV/m
	unsigned long	freq;			// tune freq in Hz
	unsigned short	numLooks;		// number of samples
	unsigned short	numChan;		// number of signal channels
} intermediateData;

typedef struct
{
	occupancyHeader occHdr;
	float			chanData[MAX_OCCRESULTCHANNELS][2];	// RF input power in uV/m
	unsigned short	numAbove[MAX_OCCRESULTCHANNELS][2];	// occupancy number above threshold
	unsigned short	numDwells;
	intermediateData dwellData[MAX_OCCDWELLS];
	float			chanDataMax[MAX_OCCRESULTCHANNELS];        // max RF input power in uV/m
	unsigned short	numDetections[MAX_OCCRESULTCHANNELS][2];   // number of "signal ups"
	unsigned short	aveTransmission[MAX_OCCRESULTCHANNELS][2]; // average transmission time (sec)
	unsigned short	maxTransmission[MAX_OCCRESULTCHANNELS][2]; // maximum transmission time (sec)
} intermediateDataResponse;

// REALTIME PORTS MESSAGES

// message from port 3305 (tci-equipoccupancy)
typedef struct
{
	unsigned long	numBytes;				// number of following bytes sent
	unsigned long	taskID;					// taskID (unique number, although can repeat -- not a good collection key)
	unsigned long	bandIndex;				// band index (for each taskID, goes from 0 to the number of tasked bands-1)
	unsigned long	firstChanFreq;			// center rf frequency of the first (lowest) channel (Hz)
	unsigned long	chanSize;				// channel spacing or bandwidth (Hz)
	unsigned long	numChan;				// number of channels
	unsigned char	noiseFloor;				// noise floor (dBm + 200)
	unsigned char	chanData[NUMOCCBINS_MAX];  // channel spectrum data (dBm + 200)
} realtimeOccupancySpectrum;

typedef struct
{
	unsigned short	azimData;  // channel lob data (1/100 degree; 0 to 35999)
						// 65535 = no df result(signal not present, low confidence, etc)
	unsigned char	specData;  // channel spectrum data (dBm + 200)
} OccDFChanData;

// message from port 3306 (tci-equipoccupdf)
typedef struct
{
	unsigned long	numBytes;				// number of following bytes sent
	unsigned long	taskID;					// taskID (unique number, although can repeat -- not a good collection key)
	unsigned long	bandIndex;				// band index (for each taskID, goes from 0 to the number of tasked bands-1)
	unsigned long	firstChanFreq;			// center rf frequency of the first (lowest) channel (Hz)
	unsigned long	chanSize;				// channel spacing or bandwidth (Hz)
	unsigned long	numChan;				// number of channels
	unsigned char	noiseFloor;				// noise floor (dBm + 200)
	OccDFChanData	chanData[NUMOCCBINS_MAX];
} realtimeOccupancyDFData;

// see realtime_msg.h for message definitions for messages from port 3307 (tci-equiprealtime)

#endif
