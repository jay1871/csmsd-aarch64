/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2005 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/
#ifndef OCCUPANCYDEFS_DEF
#define OCCUPANCYDEFS_DEF

typedef struct
{
	unsigned long lowFrequency;
	unsigned long highFrequency;
	unsigned long channelBandwidth;
	unsigned long numChannelsPerDwell;
	unsigned long processBandwidth;
	unsigned long numChannels;
	unsigned long numDwells;
	unsigned long lastChannel1;
	int index;
} occupancyBand;

typedef struct
{
	float numLooks;
	float numAbove;
	float sum;
	float max;
} groupData;

typedef struct
{
	float numLooks;
	float numAbove;
	float max;
} missionData;

typedef struct
{
	float sum;
	float num;
	float max;
} statsData;

typedef struct
{
	float timeOn;
	float timeOff;
} msglenData;

typedef struct
{
	char avg;
	char max;
} resultData;

typedef struct
{
	unsigned long result;
	unsigned long stdev;
} measureData;

#endif
