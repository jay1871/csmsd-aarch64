/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2002-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include <algorithm>
#include <array>
#ifdef __GNUG__
#include <condition_variable>
#include <mutex>
#include "MSCompat.h"
#endif

////////////////////////////////////////////////////////////////////////////////////
//
// This class template defines a thread-safe priority queue. The addItem and getItem
// functions will block (with optional timeout) if the queue is full or empty
// respectively. Items can be removed if they match a criteria.
//
// It is intended to be used with queues of pointers, but will work (maybe
// inefficiently) with any type
//
////////////////////////////////////////////////////////////////////////////////////

template<class T, size_t DEPTH> class CPriorityQueue
{

////////////////////////////////////////////////////////////////////////////////////

public:
	// Constants
	static const int FROZEN = 0x7fffffff;

	// Exceptions
	// HELD is thrown if an operation times out while the queue is held.
	enum EProblem { TIMEDOUT, CLOSED, HELD, NOTMATCHED };

	// Hold criteria
	enum EHold { NONE, ADD, GET, BOTH };

	////////////////////////////////////////////////////////////////////////////////
	//
	// Constructor
	//
	CPriorityQueue(void)
	:
#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
		m_mutex(CreateMutex(nullptr, FALSE, nullptr)),
		m_getable(CreateEvent(nullptr, TRUE, FALSE, nullptr)),
		m_addable(CreateEvent(nullptr, TRUE, TRUE, nullptr)),
#else
		m_lowWaterMark(0),
#endif
		m_closed(false),
		m_count(0),
		m_entries(),
		m_head(0),
		m_hold(NONE),
		m_priorities()
	{
#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
		m_getLocks[0] = m_mutex;
		m_getLocks[1] = m_getable;
		m_addLocks[0] = m_mutex;
		m_addLocks[1] = m_addable;
#else
		InitializeSRWLock(&m_mutex);
		InitializeConditionVariable(&m_getable);
		InitializeConditionVariable(&m_addable);
		InitializeConditionVariable(&m_lowWater);
#endif
#endif
		return;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Move constructor
	//
	CPriorityQueue(CPriorityQueue&& q)
	:
#if defined _MSC_VER && _WIN32_WINNT < _WIN32_WINNT_VISTA
		m_mutex(CreateMutex(nullptr, FALSE, nullptr)),
		m_getable(CreateEvent(nullptr, TRUE, FALSE, nullptr)),
		m_addable(CreateEvent(nullptr, TRUE, FALSE, nullptr)),
#endif
		m_entries(),
		m_priorities()
	{
		CSingleLock lock(q.m_mutex);
#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA

		if(WaitForSingleObject(q.m_getable, 0) == WAIT_OBJECT_0)
		{
			SetEvent(m_getable);
		}

		if(WaitForSingleObject(q.m_addable, 0) == WAIT_OBJECT_0)
		{
			SetEvent(m_addable);
		}

		m_getLocks[0] = m_mutex;
		m_getLocks[1] = m_getable;
		m_addLocks[0] = m_mutex;
		m_addLocks[1] = m_addable;
#else
		InitializeSRWLock(&m_mutex);
		InitializeConditionVariable(&m_getable);
		InitializeConditionVariable(&m_addable);
		InitializeConditionVariable(&m_lowWater);
#endif
#endif
#if !defined _MSC_VER || _WIN32_WINNT >= _WIN32_WINNT_VISTA
		m_lowWaterMark = q.m_lowWaterMark;
		q.m_lowWaterMark = 0;
#endif
		m_count = q.m_count;
		m_head = q.m_head;
		m_hold = q.m_hold;
		m_closed = q.m_closed;
		m_entries.swap(std::move(q.m_entries));
		m_priorities.swap(std::move(q.m_priorities));
		q.m_count = 0;
		q.m_head = 0;
		q.m_hold = NONE;
		q.m_closed = false;
#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
		ResetEvent(q.m_getable);
		SetEvent(q.m_addable);
#else
		WakeAllConditionVariable(&q.m_addable);
#endif
#else
		q.m_addable.notify_all();
#endif
		return;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Destructor
	//
	virtual ~CPriorityQueue(void)
	{
		_ASSERT(IsEmpty());

#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
		if(m_addable != INVALID_HANDLE_VALUE)
		{
			CloseHandle(m_addable);
		}

		if(m_getable != INVALID_HANDLE_VALUE)
		{
			CloseHandle(m_getable);
		}

		if(m_mutex != INVALID_HANDLE_VALUE)
		{
			CloseHandle(m_mutex);
		}
#endif

		return;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Move assignment operator
	//
	CPriorityQueue& operator=(CPriorityQueue&& q)
	{
		if(&q != this)
		{
			CSingleLock lock(q.m_mutex);
			CSingleLock lock2(m_mutex);
#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
			if(WaitForSingleObject(q.m_getable, 0) == WAIT_OBJECT_0)
			{
				SetEvent(m_getable);
			}

			if(WaitForSingleObject(q.m_addable, 0) == WAIT_OBJECT_0)
			{
				SetEvent(m_addable);
			}

#else
			m_lowWaterMark = q.m_lowWaterMark;
			q.m_lowWaterMark = 0;
#endif
			m_count = q.m_count;
			m_head = q.m_head;
			m_hold = q.m_hold;
			m_closed = q.m_closed;
			m_entries.swap(std::move(q.m_entries));
			m_priorities.swap(std::move(q.m_priorities));
			q.m_count = 0;
			q.m_head = 0;
			q.m_hold = NONE;
			q.m_closed = false;
#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
			ResetEvent(q.m_getable);
			SetEvent(q.m_addable);
#else
			WakeAllConditionVariable(&q.m_addable);
			WakeAllConditionVariable(&q.m_lowWater);
#endif
#else
			q.m_addable.notify_all();
			q.m_lowWater.notify_all();
#endif
		}

		return *this;
	}


	////////////////////////////////////////////////////////////////////////////////
	//
	// Get the depth of the queue
	//
	size_t Depth(void) const
	{
		return DEPTH;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Get the number of items in the queue
	//
	size_t Count(void) const
	{
		CSingleLock lock(m_mutex);

		return m_count;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Is the queue empty?
	//
	bool IsEmpty(void) const
	{
		CSingleLock lock(m_mutex);

		return m_count == 0;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Is the queue full?
	//
	bool IsFull(void) const
	{
		CSingleLock lock(m_mutex);

		return m_count == DEPTH;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Is the queue closed?
	//
	bool IsClosed(void) const
	{
		CSingleLock lock(m_mutex);

		return m_closed;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Is the queue held?
	//
	bool IsHeld(EHold hold = GET) const
	{
		CSingleLock lock(m_mutex);

		return m_hold == hold || (m_hold == BOTH && hold != NONE);
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Add an item to the queue. Items with numerically smaller priority are added
	// towards the head
	//
	void AddItem(const T& entry, int priority = 0, unsigned int timeout = INFINITE)
	{
		AddItem(std::move(T(entry)), priority, timeout);
	}

	void AddItem(T&& entry, int priority = 0, unsigned int timeout = INFINITE)
	{
		try
		{
#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
			CMultiLock lock(m_addLocks, timeout);
#else
			CSingleLock lock(m_mutex);

			while(!m_closed && (m_count == DEPTH || m_hold == ADD || m_hold == BOTH))
			{
#ifdef _MSC_VER
				if(!SleepConditionVariableSRW(&m_addable, &m_mutex, timeout, 0))
				{
					throw TIMEDOUT;
				}
#else
				if(timeout == INFINITE)
				{
					m_addable.wait(lock);
				}
				else if(m_addable.wait_for(lock, std::chrono::milliseconds(timeout)) == std::cv_status::timeout)
				{
					throw TIMEDOUT;
				}
#endif
			}
#endif

			if(m_closed)
			{
				throw CLOSED;
			}

			size_t insert = (m_count++ + m_head) % DEPTH;
			size_t prior = (insert == 0 ? DEPTH - 1 : insert - 1);

			while(insert != m_head && m_priorities[prior] > priority)
			{
				m_entries[insert] = std::move(m_entries[prior]);
				m_priorities[insert] = m_priorities[prior];
				insert = prior;
				prior = (prior == 0 ? DEPTH - 1 : prior - 1);
			}

			m_entries[insert] = std::move(entry);
			m_priorities[insert] = priority;

			if(m_hold == NONE && priority != FROZEN)
			{

#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
				SetEvent(m_getable);
#else
				if(m_count == 1)
				{
#ifdef _MSC_VER
					WakeAllConditionVariable(&m_getable);
#else
					m_getable.notify_all();
#endif
				}
#endif
			}

#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
			if(m_count == DEPTH)
			{
				ResetEvent(m_addable);
			}
#endif

			return;
		}
		catch(EProblem&)
		{
			if(m_hold == ADD || m_hold == BOTH)
			{
				throw HELD;
			}
			else
			{
				throw;
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Get an item off the head of the queue with optional timeout
	//
	T GetItem(unsigned int timeout = INFINITE)
	{
		try
		{
#if defined (_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
			CMultiLock lock(m_getLocks, timeout);
#else
			CSingleLock lock(m_mutex);

			while(((m_count == 0 || m_priorities[m_head] == FROZEN) && !m_closed) || m_hold == GET || m_hold == BOTH)
			{
#ifdef _MSC_VER
				if(!SleepConditionVariableSRW(&m_getable, &m_mutex, timeout, 0))
				{
					throw TIMEDOUT;
				}
#else
				if(timeout == INFINITE)
				{
					m_getable.wait(lock);
				}
				else if(m_getable.wait_for(lock, std::chrono::milliseconds(timeout)) == std::cv_status::timeout)
				{
					throw TIMEDOUT;
				}
#endif
			}
#endif

			if(m_closed && (m_count == 0 || m_priorities[m_head] == FROZEN))
			{
				throw CLOSED;
			}

			T item;
			using std::swap;
			swap(m_entries[m_head], item);
			--m_count;
			++m_head %= DEPTH;

			if(m_hold == NONE)
			{
#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
				SetEvent(m_addable);
#else
				if(m_count == DEPTH - 1)
				{
#ifdef _MSC_VER
					WakeAllConditionVariable(&m_addable);
#else
					m_addable.notify_all();
#endif
				}
#endif
			}

#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
			if((m_count == 0 || m_priorities[m_head] == FROZEN) && !m_closed)
			{
				ResetEvent(m_getable);
			}
#else
			if(m_count == m_lowWaterMark)
			{
#ifdef _MSC_VER
				WakeAllConditionVariable(&m_lowWater);
#else
				m_lowWater.notify_all();
#endif
			}
#endif

			return item;
		}
		catch(EProblem&)
		{
			if(m_hold == GET || m_hold == BOTH)
			{
				throw HELD;
			}
			else
			{
				throw;
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	//
	// Get an item off the head of the queue with optional timeout, but only if it matches supplied functor
	//
	template <typename U> T GetItemIfMatches(U matches, unsigned int timeout = INFINITE)
	{
		try
		{
#if defined (_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
			CMultiLock lock(m_getLocks, timeout);
#else
			CSingleLock lock(m_mutex);

			while(((m_count == 0 || m_priorities[m_head] == FROZEN) && !m_closed) || m_hold == GET || m_hold == BOTH)
			{
#ifdef _MSC_VER
				if(!SleepConditionVariableSRW(&m_getable, &m_mutex, timeout, 0))
				{
					throw TIMEDOUT;
				}
#else
				if(timeout == INFINITE)
				{
					m_getable.wait(lock);
				}
				else if(m_getable.wait_for(lock, std::chrono::milliseconds(timeout)) == std::cv_status::timeout)
				{
					throw TIMEDOUT;
				}
#endif
			}
#endif

			if(m_closed && (m_count == 0 || m_priorities[m_head] == FROZEN))
			{
				throw CLOSED;
			}
			if (!matches(m_entries[m_head]))
			{
				throw NOTMATCHED;
			}
			T item;
			using std::swap;
			swap(m_entries[m_head], item);
			--m_count;
			++m_head %= DEPTH;

			if(m_hold == NONE)
			{
#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
				SetEvent(m_addable);
#else
				if(m_count == DEPTH - 1)
				{
#ifdef _MSC_VER
					WakeAllConditionVariable(&m_addable);
#else
					m_addable.notify_all();
#endif
				}
#endif
			}

#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
			if((m_count == 0 || m_priorities[m_head] == FROZEN) && !m_closed)
			{
				ResetEvent(m_getable);
			}
#else
			if(m_count == m_lowWaterMark)
			{
#ifdef _MSC_VER
				WakeAllConditionVariable(&m_lowWater);
#else
				m_lowWater.notify_all();
#endif
			}
#endif

			return item;
		}
		catch(EProblem&)
		{
			if(m_hold == GET || m_hold == BOTH)
			{
				throw HELD;
			}
			else
			{
				throw;
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	//
	// Get a copy of an item off the head of the queue with optional timeout
	// Note: This function will not compile if T is a unique_ptr
	//
	T PeekItem(unsigned int timeout = 0)
	{
		try
		{
#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
			CMultiLock lock(m_getLocks, timeout);
#else
			CSingleLock lock(m_mutex);

			while(((m_count == 0 || m_priorities[m_head] == FROZEN) && !m_closed) || m_hold == GET || m_hold == BOTH)
			{
#ifdef _MSC_VER
				if(!SleepConditionVariableSRW(&m_getable, &m_mutex, timeout, 0))
#else
				if(m_getable.wait_for(lock, std::chrono::milliseconds(timeout)) == std::cv_status::timeout)
#endif
				{
					throw TIMEDOUT;
				}
			}
#endif

			if(m_closed && m_count == 0)
			{
				throw CLOSED;
			}

			return m_entries[m_head];
		}
		catch(EProblem&)
		{
			if(m_hold == GET || m_hold == BOTH)
			{
				throw HELD;
			}
			else
			{
				throw;
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	//
	// Get the priority of the item on the head of the queue with optional timeout
	//
	int PeekPriority(unsigned int timeout = 0)
	{
		try
		{
#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
			CMultiLock lock(m_getLocks, timeout);
#else
			CSingleLock lock(m_mutex);

			while(((m_count == 0 || m_priorities[m_head] == FROZEN) && !m_closed) || m_hold == GET || m_hold == BOTH)
			{
#ifdef _MSC_VER
				if(!SleepConditionVariableSRW(&m_getable, &m_mutex, timeout, 0))
#else
				if(m_getable.wait_for(lock, std::chrono::milliseconds(timeout)) == std::cv_status::timeout)
#endif
				{
					throw TIMEDOUT;
				}
			}
#endif

			if(m_closed && m_count == 0)
			{
				throw CLOSED;
			}

			return m_priorities[m_head];
		}
		catch(EProblem&)
		{
			if(m_hold == GET || m_hold == BOTH)
			{
				throw HELD;
			}
			else
			{
				throw;
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	//
	// Try to remove an item. The first item for which the supplied functor returns
	// true will be removed and optionally returned using the supplied pointer. The
	// function returns true if it succeeds. It does not block.
	//
	template<typename U> _Success_(return) bool RemoveItem(U matches, _Out_opt_ T* item = nullptr)
	{
		CSingleLock lock(m_mutex);
		size_t i;

		for(i = 0; i < m_count && !matches(m_entries[(m_head + i) % DEPTH]); ++i);

		if(i == m_count)
		{			
			return false;
		}

		using std::swap;

		for(; i < m_count - 1; ++i)
		{
			size_t next = (m_head + i + 1) % DEPTH;
			swap(m_entries[(m_head + i) % DEPTH], m_entries[next]);
			m_priorities[(m_head + i) % DEPTH] = m_priorities[next];
		}

		if(item == nullptr)
		{
			m_entries[(m_head + m_count - 1) % DEPTH] = T();
		}
		else
		{
			swap(m_entries[(m_head + m_count - 1) % DEPTH], *item);
		}

		--m_count;

		if(m_hold == NONE)
		{
#if defined (_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
			SetEvent(m_addable);
#else
			if(m_count == DEPTH - 1)
			{
#ifdef _MSC_VER
				WakeAllConditionVariable(&m_addable);
#else
				m_addable.notify_all();
#endif
			}
#endif
		}

#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
		if((m_count == 0 || m_priorities[m_head] == FROZEN) && !m_closed)
		{
			ResetEvent(m_getable);
		}
#else
		if(m_count == m_lowWaterMark)
		{
#ifdef _MSC_VER
			WakeAllConditionVariable(&m_lowWater);
#else
			m_lowWater.notify_all();
#endif
		}
#endif

		return true;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Set the priority for the first item for which the supplied functor returns true.
	// Returns false if no such item.
	//
	template<typename U> bool SetPriority(U matches, int priority)
	{
		CSingleLock lock(m_mutex);
		size_t i;

		for(i = 0; i < m_count && !matches(m_entries[(m_head + i) % DEPTH]); ++i);

		if(i == m_count)
		{			
			return false;
		}

		m_priorities[(m_head + i) % DEPTH] = priority;
		using std::swap;

		for(; i < m_count - 1 && m_priorities[(m_head + i) % DEPTH] > m_priorities[(m_head + i + 1) % DEPTH]; ++i)
		{
			swap(m_entries[(m_head + i) % DEPTH], m_entries[(m_head + i + 1) % DEPTH]);
			swap(m_priorities[(m_head + i) % DEPTH], m_priorities[(m_head + i + 1) % DEPTH]);
		}

		for(; i > 1 && m_priorities[(m_head + i) % DEPTH] < m_priorities[(m_head + i - 1) % DEPTH]; --i)
		{
			swap(m_entries[(m_head + i) % DEPTH], m_entries[(m_head + i - 1) % DEPTH]);
			swap(m_priorities[(m_head + i) % DEPTH], m_priorities[(m_head + i - 1) % DEPTH]);
		}

		if((m_hold == NONE || m_hold == ADD) && m_priorities[m_head] != FROZEN)
		{
#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
			SetEvent(m_getable);
#else
			WakeAllConditionVariable(&m_getable);
#endif
#else
			m_getable.notify_all();
#endif
		}
#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
		else if(m_priorities[m_head] == FROZEN)
		{
			ResetEvent(m_getable);
		}
#endif

		return true;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Hold the queue.
	//
	void Hold(EHold hold = GET)
	{
		CSingleLock lock(m_mutex);
		m_hold = hold;

#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
		if(hold == GET || hold == BOTH)
		{
			ResetEvent(m_getable);
		}

		if(hold == ADD || hold == BOTH)
		{
			ResetEvent(m_addable);
		}
#endif

		return;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Release the queue.
	//
	void Release(EHold release = GET)
	{
		CSingleLock lock(m_mutex);

		switch(release)
		{
		case NONE:
			return;

		case GET:
			switch(m_hold)
			{
			case GET:
				m_hold = NONE;
				break;

			case BOTH:
				m_hold = ADD;
				break;

			default:
				return;
			}

			break;

		case ADD:
			switch(m_hold)
			{
			case ADD:
				m_hold = NONE;
				break;

			case BOTH:
				m_hold = GET;
				break;

			default:
				return;
			}
			
			break;

		case BOTH:
			m_hold = NONE;
			break;
		}

		if((m_hold == NONE || m_hold == ADD) && m_count > 0 && m_priorities[m_head] != FROZEN)
		{
#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
			SetEvent(m_getable);
#else
			WakeAllConditionVariable(&m_getable);
#endif
#else
			m_getable.notify_all();
#endif
		}

		if((m_hold == NONE || m_hold == GET) && m_count < DEPTH)
		{
#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
			SetEvent(m_addable);
#else
			WakeAllConditionVariable(&m_addable);
#endif
#else
			m_addable.notify_all();
#endif
		}

		return;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Close the queue.
	//
	void Close(void)
	{
		CSingleLock lock(m_mutex);
		m_closed = true;

		if(m_hold == BOTH)
		{
			m_hold = ADD;
		}
		else if(m_hold == ADD)
		{
			m_hold = NONE;
		}

		if(m_hold != GET && (m_count == 0 || m_priorities[m_head] == FROZEN))
		{
#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
			SetEvent(m_getable);
#else
			WakeAllConditionVariable(&m_getable);
#endif
#else
			m_getable.notify_all();
#endif
		}

#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
		SetEvent(m_addable);
#else
		WakeAllConditionVariable(&m_addable);
#endif
#else
			m_addable.notify_all();
#endif

		return;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	// Reopen the queue. Resets hold state
	//
	void Reopen(void)
	{
		CSingleLock lock(m_mutex);
		m_closed = false;
		m_hold = NONE;

#if defined(_MSC_VER) && _WIN32_WINNT < _WIN32_WINNT_VISTA
		if(m_count == 0 || m_priorities[m_head] == FROZEN)
		{
			ResetEvent(m_getable);
		}
#endif

		return;
	}

#if (defined(_MSC_VER) && _WIN32_WINNT >= _WIN32_WINNT_VISTA) || defined(__GNUG__)
	////////////////////////////////////////////////////////////////////////////////
	//
	// Set the low-water mark
	//
	void SetLowWaterMark(size_t lowWaterMark)
	{
		CSingleLock lock(m_mutex);
		m_lowWaterMark = lowWaterMark;

		return;
	}
#endif

	////////////////////////////////////////////////////////////////////////////////
	//
	// Wait for queue to be below the low-water mark
	//
	void WaitForLowWater(DWORD timeout = INFINITE)
	{
		CSingleLock lock(m_mutex);
#if (defined(_MSC_VER) && _WIN32_WINNT >= _WIN32_WINNT_VISTA)
		while(m_count > m_lowWaterMark && SleepConditionVariableSRW(&m_lowWater, &m_mutex, timeout, 0));
#elif defined(__GNUG__)
		if(timeout == INFINITE)
		{
			m_lowWater.wait(lock, [&] { return m_count > m_lowWaterMark; });
		}
		else
		{
			m_lowWater.wait_for(lock, std::chrono::milliseconds(timeout), [&] { return m_count > m_lowWaterMark; });
		}
#endif

		return;
	}


////////////////////////////////////////////////////////////////////////////////////

protected:

#ifdef _MSC_VER
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
	HANDLE m_mutex;
	HANDLE m_getable;
	HANDLE m_addable;
	HANDLE m_getLocks[2];
	HANDLE m_addLocks[2];
#else
	mutable SRWLOCK m_mutex;
	CONDITION_VARIABLE m_getable;
	CONDITION_VARIABLE m_addable;
	CONDITION_VARIABLE m_lowWater;
	size_t m_lowWaterMark;
#endif
#else
	mutable std::mutex m_mutex;
	std::condition_variable m_getable;
	std::condition_variable m_addable;
	std::condition_variable m_lowWater;
	size_t m_lowWaterMark;
#endif
	bool m_closed;
	size_t m_count;
	std::array<T, DEPTH> m_entries;
	size_t m_head;
	EHold m_hold;
	std::array<int, DEPTH> m_priorities;

////////////////////////////////////////////////////////////////////////////////////

private:
	// Disable copy constructor and assignment operator
	CPriorityQueue(const CPriorityQueue&) = delete;
	CPriorityQueue& operator=(const CPriorityQueue&) = delete;

	// MFC-like lock guards
#ifdef _MSC_VER
	class CSingleLock
	{
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
	public:
		CSingleLock(_In_ HANDLE mutex) : m_mutex(mutex) { WaitForSingleObject(m_mutex, INFINITE); }
		~CSingleLock(void) { ReleaseMutex(m_mutex); }

	private:
		HANDLE m_mutex;
#else
	public:
		CSingleLock(_In_ SRWLOCK& mutex) : m_mutex(mutex) { AcquireSRWLockExclusive(&m_mutex); }
		~CSingleLock(void) { ReleaseSRWLockExclusive(&m_mutex); }

	private:
		SRWLOCK& m_mutex;
#endif
	};
#else
	typedef std::unique_lock<std::mutex> CSingleLock;
#endif

#if defined _MSC_VER && _WIN32_WINNT < _WIN32_WINNT_VISTA
	class CMultiLock
	{
	public:
		CMultiLock(const HANDLE (&objects)[2], DWORD timeout) // object[0] is a mutex, object[1] is an event
		:
			m_mutex(objects[0])
		{
			if(timeout == 0)
			{
				// Don't throw TIMEDOUT while waiting for the mutex which should only be held briefly
				WaitForSingleObject(objects[0], INFINITE);

				if(WaitForSingleObject(objects[1], 0) == WAIT_TIMEOUT)
				{
					ReleaseMutex(m_mutex);
					throw TIMEDOUT;
				}
			}
			else
			{
				if(WaitForMultipleObjects(2, objects, TRUE, timeout) == WAIT_TIMEOUT)
				{
					throw TIMEDOUT;
				}
			}
		}

		~CMultiLock(void) { ReleaseMutex(m_mutex); }

	private:
		HANDLE m_mutex;
	};
#endif
};
