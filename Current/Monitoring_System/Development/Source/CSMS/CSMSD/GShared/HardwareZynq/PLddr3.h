/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

////////////////////  This class wraps a pointer to the PS DMA memory //////////////////
class uncached_ptr
{
public:
	uncached_ptr(void) : p(nullptr) {}
	uncached_ptr(const uncached_ptr& other) : p(other.p) {}
	~uncached_ptr(void) {}
	uncached_ptr& operator=(const uncached_ptr& other) { p = other.p; return *this; }
	uncached_ptr& operator+=(size_t n) { p += n; return *this; }
	uncached_ptr operator+(size_t n) { uncached_ptr t = *this; t += n; return t; }
	explicit operator bool(void) const { return p != nullptr; }
	volatile unsigned long* get(void) const { return p; }
	void set(unsigned long* addr) { p = addr; }
#ifdef CSMS_2016
	unsigned long GetAt(int i) const { return p[i]; }
	// copying needed to handle wrap around of data
	bool GetBlock(const uncached_ptr& begin, const uncached_ptr& end, unsigned long count, std::vector<unsigned long>& data) const
	{
		try
		{
			data.resize(count);
		}
		catch(std::bad_alloc& e)
		{
			return false;
		}
		auto s = p;
		auto numToCopy = std::min(count, static_cast<unsigned long>(end.p - s));
		/*if (count <= numToCopy)
		{
			data = p;
		}
		else
		{
			size_t i;
			for (i = 0; i < numToCopy; ++i)
			{
				data[i] = s[i];
			}
			if (numToCopy < count)
			{
				// Wrapped
				s = begin.p;
				for (size_t j = 0; i < count; ++i, ++j)
				{
					data[i] = s[j];
				}
			}
		}*/
		size_t i;
		for (i = 0; i < numToCopy; ++i)
		{
			data[i] = s[i];
		}
		if (numToCopy < count)
		{
			//printf("data wrapped %u\n", count);
			// Wrapped
			s = begin.p;
			for (size_t j = 0; i < count; ++i, ++j)
			{
				data[i] = s[j];
			}
		}
		return true;
	}
#endif
private:
	volatile unsigned long* p;
};

// Opaque class to represent virtual address in PL DDR3 memory
class CPLddr3
{
public:
	CPLddr3(void) : m_p(nullptr) {}
	~CPLddr3(void) {}
	CPLddr3& operator=(const CPLddr3& other) { m_p = other.m_p; return *this; }
	CPLddr3& operator+=(size_t n) { m_p += n; return *this; }
	CPLddr3 operator+(size_t n) { CPLddr3 t = *this; t += n; return t; }
	explicit operator bool(void) const { return m_p != nullptr; }
	void Assign(volatile unsigned long* ddr3) { assert(m_p == nullptr); m_p = ddr3; }
	static size_t ItemSize(void) { return sizeof(unsigned long); }
	volatile unsigned long* Get(void) const { return m_p; }
	unsigned long GetAt(int i) const { return m_p[i]; }
#if 0
	bool GetBlock(const CPLddr3& begin, const CPLddr3& end, unsigned long count, std::vector<unsigned long>& data) const
	{
		try
		{
			data.resize(count);
		}
		catch(std::bad_alloc& e)
		{
			return false;
		}
		auto s = m_p;
		for (size_t i = 0; i < count; ++i)
		{
			data[i] = *s;
			if (++s == end.m_p)
				s = begin.m_p;
		}
		return true;
	}
#endif
	bool GetBlock(const CPLddr3& begin, const CPLddr3& end, unsigned long count, std::vector<unsigned long>& data) const
	{
		try
		{
			data.resize(count);
		}
		catch(std::bad_alloc& e)
		{
			return false;
		}
		auto s = m_p;
		auto numToCopy = std::min(count, static_cast<unsigned long>(end.m_p - s));
		size_t i;
		for (i = 0; i < numToCopy; ++i)
		{
			data[i] = s[i];
		}
//		for (size_t i = 0; i < numToCopy; ++i)
//		{
//			printf("%u %11.6g %11.6g\n", i, *reinterpret_cast<volatile float*>(&s[i]), *reinterpret_cast<float*>(&data[i]));
//		}
		if (numToCopy < count)
		{
			// Wrapped
			s = begin.m_p;
			for (size_t j = 0; i < count; ++i, ++j)
			{
				data[i] = s[j];
			}
		}
		return true;
	}
	void FillBlock(const CPLddr3& begin, const CPLddr3& end, unsigned long count)
	{
		auto s = m_p;
		auto numToFill = std::min(count, static_cast<unsigned long>(end.m_p - s));
		unsigned long i;
		for (i = 0; i < numToFill; ++i)
		{
			s[i] = i;
		}
		if (numToFill < count)
		{
			// Wrapp
			s = begin.m_p;
			for (size_t j = 0; i < count; ++i, ++j)
			{
				s[j] = i;
			}
		}
	}
private:
	volatile unsigned long* m_p;
};

