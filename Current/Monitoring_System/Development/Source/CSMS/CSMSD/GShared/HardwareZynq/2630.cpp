/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"
#include "2630.h"
#include "Digitizer.h"
#include "Log.h"
#include <sys/mman.h>

// Static data
int	C2630::m_driverDesc = -1;
int	C2630::m_driverDesc1 = -1;

#ifdef CSMS_DEBUG
bool	C2630::s_printResult = false;
bool	C2630::s_printTuneInfo = false;
#endif

//C2630::SConfigData C2630::m_configData;
//bool C2630::m_configDataIsLoaded = false;
//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C2630::C2630(std::pair<bool, bool> dfType) :
	m_master(false),
	m_sharedMemory(SHMEM_NAME, sizeof(S2630State)),
	m_shMem(m_sharedMemory.SharedMemory()),
//	m_sim(sim),
//	m_useExternal(useExternal),
	m_state(static_cast<S2630State*>(m_sharedMemory.State()))
{
//	if (sim)
//	{
//		printf("C2630 is using simulator!!!\n");
//		return;
//	}

	if (m_sharedMemory.IsMaster())
	{
		m_master = true;
		printf("C2630 I will be master\n");
	}
	else
	{
		printf("C2630 VCP is master\n");
	}

	// Initialize data
	if (m_driverDesc == -1)
		m_driverDesc = open("/dev/spidev32766.0", O_RDWR);
	if (m_driverDesc1 == -1)
		m_driverDesc1 = open("/dev/TciSpi", O_RDONLY);

	// Try to read data from the 2630
	/*if (ioctl(m_driverDesc1, IOCTL_GetRxData, &m_rxData) > 0)
	{
		printf("C2630 ctor: read %lu from 2630\n", m_rxData.transferSize);
	}*/

	if (ReadFirmwareVersion(fwVersion))
	{
		TRACE("GET_FIRMWARE_VERSION %08lx\n", fwVersion);
	}
	else
	{
		throw std::runtime_error("Unable to get firmware version");
	}
	unsigned long fwDate;
	if (ReadFirmwareDate(fwDate))
	{
		TRACE("GET_FIRMWARE_DATE %08lx\n", fwDate);
	}
	else
	{
		throw std::runtime_error("Unable to get firmware date");
	}
	unsigned long fpgaVersion;
	if (ReadFpgaVersion(fpgaVersion))
	{
		TRACE("GET_FPGA_VERSION %08lx\n", fpgaVersion);
	}
	else
	{
		throw std::runtime_error("Unable to get fpga version");
	}
	S2630Msg::SRxCapabilities rxCapabilities;
	if (ReadRxCapabilities(rxCapabilities))
	{
		TRACE("RX_CAPABILITIES %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %s\n",
		rxCapabilities.maxVushfFreqMHz,
		rxCapabilities.minVushfFreqMHz,
		rxCapabilities.attenStep,
		rxCapabilities.finalIfFreqMHz,
		rxCapabilities.hfBwMHz,
		rxCapabilities.lowestHeterodyneFreqMHz,
		rxCapabilities.minHfFreqHz,
		rxCapabilities.maxHfFreqMHz,
		rxCapabilities.minCalGenFreqMHz,
		rxCapabilities.maxCalGenFreqMHz,
		rxCapabilities.minCalGenFreqStepBelow_3200MHz,
		rxCapabilities.minCalGenFreqStep3200AndAboveMHz,
		rxCapabilities.maxCalGenFreqStepMHz,
		rxCapabilities.narrowBwMHz,
		rxCapabilities.tuneBwMHz,
		rxCapabilities.tuneResolutionMHz,
		rxCapabilities.tuneCrossOverMHz,
		rxCapabilities.maxFPI,
		rxCapabilities.maxAntennaInputs,
		rxCapabilities.maxModes,
		rxCapabilities.firmwareVersion,
		rxCapabilities.fpgaVersion,
		rxCapabilities.flashVersion,
		rxCapabilities.firmwareDate,
		rxCapabilities.requiredTransferBufferSize,
		rxCapabilities.max10UniqueIdLow,
		rxCapabilities.max10UniqueIdHigh,
		rxCapabilities.rfCalTableIndexLimit,
		rxCapabilities.ifCalTableIndexLimit,
		rxCapabilities.normalAttenCalTableIndexLimit,
		rxCapabilities.ruralAttenCalTableIndexLimit,
		rxCapabilities.urbanAttenCalTableIndexLimit,
		rxCapabilities.congestedAttenCalTableIndexLimit,
		rxCapabilities.calGenTableIndexLimit,
		rxCapabilities.interfaceType,
		rxCapabilities.rxProcessor,
		rxCapabilities.tNumber);
	}
	else
	{
		throw std::runtime_error("Unable to get rx capabilities");
	}

	if (rxCapabilities.rxProcessor >= 41)
	{
		if (fwVersion < S2630Msg::MIN_2630_VERSION_SPIN4)
		{
			char s[21];
			snprintf(s, 21, "%08lx vs %08lx", S2630Msg::MIN_2630_VERSION_SPIN4, fwVersion);
			std::string err("2630 Firmware version must be at least ");
			err += s;
			CLog::Log(CLog::ERRORS, "%s", err.c_str());
			throw std::runtime_error(err);
		}
	}
	else
	{
		if (fwVersion < S2630Msg::MIN_2630_VERSION)
		{
			char s[21];
			snprintf(s, 21, "%08lx vs %08lx", S2630Msg::MIN_2630_VERSION, fwVersion);
			std::string err("2630 Firmware version must be at least ");
			err += s;
			CLog::Log(CLog::ERRORS, "%s", err.c_str());
			throw std::runtime_error(err);
		}
	}

	// Initialize static data
	m_configData = rxCapabilities;		// Just sets base class members

	S2630Msg::SFreqPlanTable2 freqPlanTable;
	unsigned long numFreqPlanRows;
	if (ReadRxTable(freqPlanTable, numFreqPlanRows))
	{
		printf("maxFPI %lu ==? %lu\n", m_configData.maxFPI, numFreqPlanRows);
		if (m_configData.maxFPI != numFreqPlanRows)
		{
			throw std::runtime_error("Rx FPI table has incorrect number of rows.");
		}
		for (size_t i = 0; i < numFreqPlanRows; ++i)
		{
			auto& r = freqPlanTable.rows[i];
			SFreqBand b;
			b.psf1 = Units::Frequency(r.psFreqLow * 1000000ULL);
			b.psf2 = Units::Frequency(r.psFreqHigh * 1000000ULL);
			b.ftop = Units::Frequency(r.freqHigh * 1000000ULL);
			b.ftopMHz = r.freqHigh;
			b.direct = (r.secondIF == 0);
			b.inverted = (r.spectralSense == 0);
			assert(r.calIfTableIndex <= 255);
			b.ifIndex = r.calIfTableIndex;
			assert(r.calAttenNormalTableIndex < 255);
			b.attIndexes[size_t(EGainMode::NORMAL)] = r.calAttenNormalTableIndex;
			assert(r.calAttenRuralTableIndex < 255);
			b.attIndexes[size_t(EGainMode::RURAL)] = r.calAttenRuralTableIndex;
			assert(r.calAttenUrbanTableIndex < 255);
			b.attIndexes[size_t(EGainMode::URBAN)] = r.calAttenUrbanTableIndex;
			assert(r.calAttenCongestedTableIndex < 255);
			b.attIndexes[size_t(EGainMode::CONGESTED)] = r.calAttenCongestedTableIndex;

			for (size_t gm = 0; gm < size_t(EGainMode::NUM_GAINMODES); ++gm)
			{
				if (b.attIndexes[gm] > m_configData.attenCalTableIndexLimits[gm])
					m_configData.attenCalTableIndexLimits[gm] = b.attIndexes[gm];
			}

			// First two entries are for HF
			if (i < 2)
			{
				// HF
				b.f1 = b.psf1;
				b.f2 = b.psf2;
				m_hfBands.push_back(b);
			}
			else
			{
				// VUSHF
				b.f1 = std::max((m_vushfBands.empty() ? b.psf1 : m_vushfBands.back().ftop - Units::Frequency(40000000)), b.psf1);
				b.f2 = std::min(b.ftop + Units::Frequency(40000000), b.psf2);

				m_vushfBands.push_back(b);
			}
			printf("%u: %lu %lu %lu %lu %lu %lu %f %f %u %u %u %u %u\n", i, r.psFreqLow, r.psFreqHigh, r.freqHigh, r.index, r.secondIF, r.spectralSense,
				b.f1.Hz<double>() / 1000000., b.f2.Hz<double>() / 1000000., b.ifIndex,
				b.attIndexes[size_t(EGainMode::NORMAL)], b.attIndexes[size_t(EGainMode::RURAL)], b.attIndexes[size_t(EGainMode::URBAN)], b.attIndexes[size_t(EGainMode::CONGESTED)]);

		}
	}
	else
	{
		throw std::runtime_error("Unable to get rx frequency plan table");
	}

#ifdef TEST2630API
	testApi();
#endif
	S2630Msg::SRxSettings rxSettings;
	ReadRxSettings(rxSettings);
	TRACE("RX_SETTINGS %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu\n",
		rxSettings.currentAntennaSetting,
		rxSettings.currentAttenuation,
		rxSettings.currentBandWidth,
		rxSettings.currentFirmwareVersion,
		rxSettings.currentFpgaVersion,
		rxSettings.currentFlashVersion,
		rxSettings.currentFirmwareDate,
		rxSettings.currentFrequency,
		rxSettings.currentIf1Frequency,
		rxSettings.currentIf2Frequency,
		rxSettings.currentLo1Frequency,
		rxSettings.currentLo2Frequency,
		rxSettings.currentFPI,
		rxSettings.currentMode,
		rxSettings.currentPsFreqLow,
		rxSettings.currentPsFreqHigh,
		rxSettings.currentLnaStatus,
		rxSettings.currentLoLockStatus,
		rxSettings.currentSpectralSense);

	// initialize m_state secondary variables
	if (rxSettings.currentFPI < (rxSettings.currentFPI < 3 ? m_hfBands.size() + 1 : m_vushfBands.size() + 3))
	{
		auto& b =  (rxSettings.currentFPI < 3 ? m_hfBands[rxSettings.currentFPI - 1] : m_vushfBands[rxSettings.currentFPI - 3]);
		printf("C2630 initializing direct,inverted = %d %d from FPI %lu\n", b.direct, b.inverted, rxSettings.currentFPI);
		m_state->direct = b.direct;
		m_state->inverted = b.inverted;
	}
	else
	{
		printf("C2630 invalid currentFPI\n");
	}
	// Get cal data from radio
	LoadCalibrationTables();

	bool setOn;
	ReadCalTone(setOn);
	unsigned long setFreqMHz;
	ReadCalToneMHz(setFreqMHz);

	if (dfType.first) // Master
	{
		// Set the output clock frequency to 100 MHz for sending to slave
		EExternalRef setRef;
		if (!SetExternalReference(EExternalRef::EXT100MHZ, setRef))
		{
			printf("C2630(master df): failed to set external reference to 100 MHz\n");
		}
		else if (setRef != EExternalRef::EXT100MHZ)
		{
			printf("C2630(master df): failed to set external reference to 100 MHz %d\n", (int) setRef);
		}
		else
		{
			printf("C2630(master df): set external reference to 100 MHz\n");
		}
	}
	else if (dfType.second)	// Slave
	{
		// Set to receive 100 MHz and enable sending the 1PPS pulse to the digital board
		EInternalRef setRef;
		if (!SetInternalReference(EInternalRef::EXT100MHZ, setRef))
		{
			printf("C2630(slave df): failed to set internal reference to 100 MHz\n");
		}
		else if (setRef != EInternalRef::EXT100MHZ)
		{
			printf("C2630(slave df): failed to set internal reference to 100 MHz %d\n", (int) setRef);
		}
		else
		{
			printf("C2630(slave df): set internal reference to 100 MHz\n");
		}
	}
	else	// not a df system at all
	{
		// Set the output clock frequency to 10 MHz for other uses, like signal generator lock
		EExternalRef setRef;
		if (!SetExternalReference(EExternalRef::EXT10MHZ, setRef))
		{
			printf("C2630(no df): failed to set external reference to 10 MHz\n");
		}
		else if (setRef != EExternalRef::EXT10MHZ)
		{
			printf("C2630(no df): failed to set external reference to 10 MHz %d\n", (int) setRef);
		}
		else
		{
			printf("C2630(no df): set external reference to 10 MHz\n");
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C2630::~C2630(void)
{
	TRACE("C2630 dtor\n");
	if (m_driverDesc != -1)
	{
		close(m_driverDesc);
		m_driverDesc = -1;
	}

	if (m_driverDesc1 != -1)
	{
		close(m_driverDesc1);
		m_driverDesc1 = -1;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the FPI corresponding to the frequency
//
int C2630::BandIndex(unsigned long fMHz) const
{
	for (int i = 0; i < int(m_vushfBands.size()); ++i)
	{
		if (fMHz < m_vushfBands[i].ftopMHz)
		{
			return i;
		}
	}
	// Special case for last band
	if (fMHz == m_vushfBands[m_vushfBands.size() - 1].ftopMHz)
	{
		return m_vushfBands.size() - 1;
	}
	return -1;
}


//////////////////////////////////////////////////////////////////////
//
// Calculate the receiver frequency (in MHz)
//
unsigned long C2630::CalcRxFreqMHz(bool hf, Units::Frequency frequency) const
{
	if (hf)
	{
		return 15;
	}
	auto freq = frequency.Hz<double>() / 1000000.;		// MHz
	if (freq <= m_configData.tuneCrossOverMHz)
	{
		return 50;
	}
	unsigned long ifreq = m_configData.tuneResolutionMHz * static_cast<unsigned long>(freq / m_configData.tuneResolutionMHz + 0.5);
	if (ifreq < m_configData.lowestHeterodyneFreqMHz)
	{
		ifreq = m_configData.lowestHeterodyneFreqMHz;
	}
	return ifreq;
}


//////////////////////////////////////////////////////////////////////
//
// Delay by specified number of microseconds (up to 1 second max)
//
void C2630::Delay(unsigned long usec)
{
	if (usec == 0) return;
	if (usec > 1000000) return;

	timespec ts;
	if (clock_gettime(CLOCK_MONOTONIC, &ts) == -1)
	{
		printf("C2630::Delay: clock_gettime error\n");
		return;
	}
	// add 1000 * usec to ts
	ts.tv_nsec += 1000 * usec;
	if (ts.tv_nsec >= 1000000000)
	{
		ts.tv_sec += ts.tv_nsec / 1000000000;
		ts.tv_nsec %= 1000000000;
	}
	for (size_t i = 0; i < 100; ++i)	// Don't like infinite loops - this will at least exit eventually
	{
		auto s = clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &ts, nullptr);
		if (s == 0)
			break;

		if (s != EINTR)
		{
			printf("C2630::Delay: error %d\n", s);
			return;
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset the receiver
//
bool C2630::DoRxReset(void)
{
	if (!Cmd(S2630Msg::ECommand::DO_RX_FIRMWARE_RESET) == 0)
	{
		return false;
	}
	if (!Cmd(S2630Msg::ECommand::DO_RX_HARDWARE_RESET) == 0)
	{
		return false;
	}
	return true;

}


//////////////////////////////////////////////////////////////////////
//
// Get the radio frequency limits
//
bool C2630::GetFreqLimits(Units::FreqPair& freqLimits)
{
	unsigned char fpi = 0;
	Units::Frequency actualRadioFreq;
	Units::Frequency actualRadioBw;
	{
		CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
		fpi = m_state->presel;
		actualRadioFreq = Units::Frequency(m_state->ifreq * 1000000.);
		actualRadioBw = Units::Frequency(m_state->bw == 10 ? 20000000 : 90000000);	// not used in HF or direct
	}
	if (fpi == 0)
		return false;

	unsigned char index = fpi - 1;
	if (index < m_hfBands.size())	// hf
	{
		freqLimits.first = m_hfBands[index].psf1;
		freqLimits.second = m_hfBands[index].psf2;
	}
	else
	{
		index -= m_hfBands.size();
		if (index < m_vushfBands.size())
		{
			if (index == 0)		// direct path
			{
				freqLimits.first = m_vushfBands[index].psf1;
				freqLimits.second = m_vushfBands[index].psf2;
			}
			else
			{
				freqLimits.first = std::max(actualRadioFreq - actualRadioBw / 2, m_vushfBands[index].psf1);
				freqLimits.second = std::min(actualRadioFreq + actualRadioBw / 2, m_vushfBands[index].psf2);
			}
		}
		else
		{
			return false;
		}
	}
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Get the gain compensated for temperature (ratio)
//
float C2630::GetGainRatio(float temp, size_t& rfGainTableIdx, long& ifGainTableIdx, float& gainAdjustmentTemp) const
{
	// Get gain ratio at the radio tune frequency (center of if filter).
	rfGainTableIdx = 0;
	ifGainTableIdx = -1;
	gainAdjustmentTemp = 1.0f;
	size_t tempGainTableIdx = 0;
	unsigned char tempIdx = 0;
	double frequency = 0;
	double offsetFrequency = 0;
	unsigned char bw = 0;
	unsigned char gainMode = size_t(EGainMode::NORMAL);		// default value
	unsigned char atten = 0;
#ifdef GAINDEBUG
	bool dbgLna = false;
	unsigned char dbgFpi = 0;
#endif
	{
		CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
#ifdef GAINDEBUG
		dbgLna = m_state->lna;
		dbgFpi = m_state->presel;
#endif
		rfGainTableIdx = 2 * (m_state->presel - 1) + (m_state->lna ? 1 : 0);	// Determine Rf gain table from presel(fpi) and lna
		tempIdx = m_state->presel - 1;
		tempGainTableIdx = (m_state->lna ? 1 : 0);
		frequency = m_state->frequency;
		offsetFrequency = frequency - m_state->ifreq * 1000000.;
		gainMode = size_t(m_state->gainMode);
		bw = m_state->bw;
		atten = m_state->atten;
	}
	// Get rfGain at the tunefreq m_state->frequency
	if (rfGainTableIdx >= m_rfGainTable.size())
	{
		printf("GetGainRatio: rfGainTable index error\n");
		return 0;
	}
	auto& rfGainTable = m_rfGainTable[rfGainTableIdx];

	float rfGain;
	if (rfGainTable.lowFreq != 0 && frequency < rfGainTable.lowFreq)
	{
		rfGain = InterpolateGain(frequency, rfGainTable.lowTable.startValue, rfGainTable.lowTable.stepValue, rfGainTable.lowTable.data, true);
	}
	else
	{
		rfGain = InterpolateGain(frequency, rfGainTable.startValue, rfGainTable.stepValue, rfGainTable.data, true);
	}

	// Get ifGain at the offset frequency - radioFrequency
	float ifGain = 1.f;
	if (rfGainTable.ifIndex == 0)
	{
		ifGainTableIdx = -1;	// to return to caller
	}
	else
	{
		ifGainTableIdx = rfGainTable.ifIndex - 1;
		if (bw == 80) ifGainTableIdx += m_configData.ifCalTableIndexLimit / 2;
		if (ifGainTableIdx >= long(m_ifGainTable.size()))
		{
			printf("GetGainRatio: ifGainTable index error\n");
			return 0;
		}
		auto& ifGainTable = m_ifGainTable[ifGainTableIdx];
		ifGain = InterpolateGain(offsetFrequency, ifGainTable.startValue, ifGainTable.stepValue, ifGainTable.data, true);
	}

	// Get nominalTemp and gainSlope for this FPI/lna combination
	if (tempGainTableIdx >= m_tempGainTable.size())
	{
		printf("GetGainRatio: tempGainTable index error\n");
		return 0;
	}
	auto& tempGainTable = m_tempGainTable[tempGainTableIdx];
	if (tempIdx >= tempGainTable.data.size())
	{
		printf("GetGainRatio: tempGain fpi index error\n");
		return 0;
	}
	float slope = 0.f;	// db/degC
	if (temp < tempGainTable.nominalTemp)
	{
		slope = tempGainTable.data[tempIdx].belowData;	// db/deg
	}
	else if (temp > tempGainTable.nominalTemp)
	{
		slope = tempGainTable.data[tempIdx].aboveData;	// db/deg
	}
	gainAdjustmentTemp = pow(10.0f, (temp - tempGainTable.nominalTemp) * slope / 10.0f);
	float fullGain = gainAdjustmentTemp * rfGain * ifGain;

	// Get attenuation value
	size_t attenTableIdx = rfGainTable.attIndexes[gainMode];
	auto& attenModeTable = m_attenTables[gainMode];
	if (attenTableIdx >= attenModeTable.size())
	{
		printf("GetGainRatio: attenTable index error\n");
		return 0;
	}
	auto& attenTable = attenModeTable[attenTableIdx];
	unsigned char attIdx = atten / attenTable.stepValue;

	float gain = 0;
	float attenValue = 0;
	if (attIdx < attenTable.data.size())
	{
		attenValue = attenTable.data[attIdx];
		if (atten != attIdx * attenTable.stepValue)
		{
			attenValue *= pow(10.0f, float(atten - attIdx * attenTable.stepValue) / 10.0f);
		}
		gain = fullGain / attenValue;
	}

#ifdef CSMS_DEBUG
	if(s_printResult)
	{
		s_printResult = false;
		TRACE("GetGainRatio: f=%f fpi = %u lna = %d rfidx = %u rfgain = %f ifidx = %ld ifgain = %f temp = %f tempIdx = %u tempCorr = %f fullGain = %f "
			"mode = %d atIdx = %u att = %u attval = %f gain = %f\n",
			frequency, m_state->presel, m_state->lna, rfGainTableIdx, 10.f*log10(rfGain), ifGainTableIdx, 10.f*log10(ifGain), temp, tempIdx,
			(temp - tempGainTable.nominalTemp) * slope, 10.f*log10(fullGain), gainMode, attenTableIdx, atten,
			10.f*log10(attenValue), 10.f*log10(gain));
	}
#endif

#ifdef GAINDEBUG
	struct SDbgSave
	{
		unsigned char fpi;
		bool lna;
		size_t rfidx;
		float rfGain;
		long ifidx;
		float ifGain;
		float temp;
		unsigned char tempIdx;
		float slope;
		float fullGain;
		unsigned char gainMode;
		size_t atidx;
		unsigned char atten;
		float attenValue;
		float gain;
	};
	static SDbgSave dbgSave = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	if (dbgFpi != dbgSave.fpi ||
		dbgLna != dbgSave.lna ||
		rfGainTableIdx != dbgSave.rfidx ||
		rfGain != dbgSave.rfGain ||
		ifGainTableIdx != dbgSave.ifidx ||
		ifGain != dbgSave.ifGain ||
		temp != dbgSave.temp ||
		tempIdx != dbgSave.tempIdx ||
		slope != dbgSave.slope ||
		fullGain != dbgSave.fullGain ||
		gainMode != dbgSave.gainMode ||
		attenTableIdx != dbgSave.atidx ||
		atten != dbgSave.atten ||
		attenValue != dbgSave.attenValue ||
		gain != dbgSave.gain)
	{
		dbgSave.fpi = dbgFpi;
		dbgSave.lna = dbgLna;
		dbgSave.rfidx = rfGainTableIdx;
		dbgSave.rfGain = rfGain;
		dbgSave.ifidx = ifGainTableIdx;
		dbgSave.ifGain = ifGain;
		dbgSave.temp = temp;
		dbgSave.tempIdx = tempIdx;
		dbgSave.slope = slope;
		dbgSave.fullGain = fullGain;
		dbgSave.gainMode = gainMode;
		dbgSave.atidx = attenTableIdx;
		dbgSave.atten = atten;
		dbgSave.attenValue = attenValue;
		dbgSave.gain = gain;

		printf("GetGainRatio: f=%f fpi = %u lna = %d rfidx = %u rfgain = %f ifidx = %ld ifgain = %f temp = %f tempIdx = %u tempCorr = %f fullGain = %f "
			"mode = %d atIdx = %u att = %u attval = %f gain = %f\n",
			frequency, dbgFpi, dbgLna, rfGainTableIdx, 10.f*log10(rfGain), ifGainTableIdx, 10.f*log10(ifGain), temp, tempIdx,
			(temp - tempGainTable.nominalTemp) * slope, 10.f*log10(fullGain), gainMode, attenTableIdx, atten,
			10.f*log10(attenValue), 10.f*log10(gain));
	}
#endif
	return gain;
}


//////////////////////////////////////////////////////////////////////
//
// Get board temperature limits
//
std::vector<std::array<float, 2> > C2630::GetTempLimits(void) const
{
	std::vector<std::array<float, 2> > limits(3);

	limits[0][0] = m_tempLimits[S2630Msg::ETemperatureSensors::NIOS][0];
	limits[0][1] = m_tempLimits[S2630Msg::ETemperatureSensors::NIOS][1];

	limits[1][0] = m_tempLimits[S2630Msg::ETemperatureSensors::SF2450][0];
	limits[1][1] = m_tempLimits[S2630Msg::ETemperatureSensors::SF2450][1];

	limits[1][0] = m_tempLimits[S2630Msg::ETemperatureSensors::OCXO][0];
	limits[2][1] = m_tempLimits[S2630Msg::ETemperatureSensors::OCXO][1];

	return limits;
}


//////////////////////////////////////////////////////////////////////
//
// Calibration table interpolation
//
float C2630::InterpolateGain(double frequency, double startFreq, double stepFreq, const std::vector<float>& data, bool extend)
{
	if (data.empty())
		return 0;

	float x;
	double calBin = (frequency - startFreq) / stepFreq;
	if (calBin < 0)
	{
		x = (extend ? data[0] : 0);
	}
	else if (calBin + 1 > data.size())
	{
		x = (extend ? data[data.size() - 1] : 0);
	}
	else if (calBin == data.size() - 1)
	{
		x = data[data.size() - 1];
	}
	else
	{
		auto loBin = int(calBin);
		x = data[loBin] + (data[loBin + 1] - data[loBin]) * float(calBin - loBin);
	}
	return x;
}


//////////////////////////////////////////////////////////////////////
//
// Get the calibration tables from the radio
//
void C2630::LoadCalibrationTables(void)
{
	size_t numRfTables = m_configData.maxFPI;
	size_t numIfTables = m_configData.ifCalTableIndexLimit;
	size_t numAttTables[size_t(EGainMode::NUM_GAINMODES)];
	for (size_t gm = 0; gm < size_t(EGainMode::NUM_GAINMODES); ++gm)
	{
		numAttTables[gm] = m_configData.attenCalTableIndexLimits[gm] + 1;
	}
	size_t numCalGenTables = m_configData.calGenTableIndexLimit + 1;

	S2630Msg::SCalTable calTable;
	m_rfGainTable.resize(2 * numRfTables);
	for (unsigned short bandIdx = 0; bandIdx < numRfTables; ++bandIdx)
	{
		for (size_t lna = 0; lna < 2; ++lna)
		{
			size_t rfGainTableIdx = 2 * bandIdx + lna;
			auto& g = m_rfGainTable[rfGainTableIdx];
			if (!ReadCalTable(S2630Msg::RFCALDATA, (lna == 0 ? S2630Msg::LNA_OFF : S2630Msg::LNA_ON), bandIdx + 1, calTable))
			{
				std::string errstr = "Unable to get rfcaltable: lna=" + std::to_string(lna) + " fpi=" + std::to_string(bandIdx + 1);
				throw std::runtime_error(errstr);
			}
			g.startValue = calTable.fStartValueMHz * (bandIdx == 0 ? 1000.0 : 1000000.0);
			g.stepValue = calTable.fStepValueMHz * (bandIdx == 0 ? 1000.0 : 1000000.0);
			g.data.resize(calTable.numData);
			for (size_t i = 0; i < calTable.numData; ++i)
			{
				g.data[i] = pow(10.0f, calTable.data[i] / 10.0f);
			}
			// Add some data from the FPI table:
			auto& band = (bandIdx < 2 ? m_hfBands[bandIdx] : m_vushfBands[bandIdx - 2]);
			g.ifIndex = band.ifIndex;
			for (size_t j = 0; j < size_t(EGainMode::NUM_GAINMODES); ++j)
				g.attIndexes[j] = band.attIndexes[j];
			g.lowFreq = 0;
//			printf("RfGain[%u] %f %f %u %u %u\n", bandIdx, g.startValue, g.stepValue, g.data.size(), g.ifIndex, g.attIndex);
			if (bandIdx == 0)
			{
				// Get the low frequency data for fpi 1
				ReadCalTable(S2630Msg::RFCALDATA, (lna == 0 ? S2630Msg::LNA_OFF : S2630Msg::LNA_ON), 0, calTable);
				g.lowFreq = g.startValue;
				g.lowTable.startValue = calTable.fStartValueMHz * 1000.0;
				g.lowTable.stepValue = calTable.fStepValueMHz * 1000.0;
				g.lowTable.data.resize(calTable.numData);
				for (size_t i = 0; i < calTable.numData; ++i)
				{
					g.lowTable.data[i] = pow(10.0f, calTable.data[i] / 10.0f);
				}
//				printf("RfGain[%u] %f %f %u\n", bandIdx, g.lowTable.startValue, g.lowTable.stepValue, g.lowTable.data.size());
			}
		}
	}

	m_ifGainTable.resize(numIfTables);
	for (unsigned short ifIdx = 0; ifIdx < numIfTables; ++ifIdx)
	{
		auto& g = m_ifGainTable[ifIdx];	// 0-11
		if (!ReadCalTable(S2630Msg::IFCALDATA, 0, ifIdx + 1, calTable))	// 1-12
		{
			std::string errstr = "Unable to get ifcaltable: idx=" + std::to_string(ifIdx + 1);
			throw std::runtime_error(errstr);
		}
		g.startValue = calTable.fStartValueMHz * 1000000.0;
		g.stepValue = calTable.fStepValueMHz * 1000000.0;
		g.data.resize(calTable.numData);
		for (size_t i = 0; i < calTable.numData; ++i)
		{
			g.data[i] = pow(10.0f, calTable.data[i] / 10.0f);
		}
//		printf("IfGain[%u] %f %f %u\n", i, g.startFreq, g.stepFreq, g.data.size());
	}

	for (size_t gm = 0; gm < size_t(EGainMode::NUM_GAINMODES); ++gm)
	{
		m_attenTables[gm].resize(numAttTables[gm]);
		for (unsigned short attIdx = 0; attIdx < numAttTables[gm]; ++attIdx)
		{
			auto& a = m_attenTables[gm][attIdx];
			if (!ReadCalTable(S2630Msg::ATTCALDATA, gm, attIdx, calTable))
			{
				std::string errstr = "Unable to get attcaltable: gainMode=" + std::to_string(gm) + " idx=" + std::to_string(attIdx);
				throw std::runtime_error(errstr);
			}
			a.startValue = calTable.fStartValueMHz;
			a.stepValue = calTable.fStepValueMHz;
			a.data.resize(calTable.numData);
			for (size_t i = 0; i < calTable.numData; ++i)
			{
				a.data[i] = pow(10.0f, calTable.data[i] / 10.0f);
			}
			auto maxAtten = a.startValue + a.stepValue * calTable.numData;		// This tables max value
			if (maxAtten > m_configData.maxAtten)
				m_configData.maxAtten = maxAtten;		// Overall maximum attenuation supported.
//			printf("Atten[%u] %u\n", i, a.data.size());
		}
	}

	// Temperature tables
	m_tempGainTable.resize(2);
	for (size_t lna = 0; lna < 2; ++lna)
	{
		size_t tempGainTableIdx = lna;
		for (size_t abIdx = 0; abIdx < 2; ++abIdx)	// above-below index
		{
			auto& g = m_tempGainTable[tempGainTableIdx];
			if (!ReadCalTable(S2630Msg::TEMP_COMP, (lna == 0 ? S2630Msg::LNA_OFF : S2630Msg::LNA_ON), abIdx, calTable))
			{
				std::string errstr = "Unable to get tempcaltable: lna=" + std::to_string(lna) + " abIdx=" + std::to_string(abIdx);
				throw std::runtime_error(errstr);
			}
//			printf("%u %f %f %u\n", abIdx, calTable.fStartValueMHz, calTable.fStepValueMHz, calTable.numData);
			if (calTable.numData != m_configData.maxFPI)
			{
				std::string errstr = "Temp caldata has incorrect number of data items: " + std::to_string(calTable.numData);
				throw std::runtime_error(errstr);
			}
			if (abIdx == 0)
			{
				g.data.resize(m_configData.maxFPI);
				g.nominalTemp = 40.f;
				for (size_t i = 0; i < g.data.size(); ++i)
				{
					g.data[i].belowData = calTable.data[i];
				}
			}
			else
			{
				for (size_t i = 0; i < g.data.size(); ++i)
				{
					g.data[i].aboveData = calTable.data[i];
				}
			}
		}
	}

	m_calGenTable.resize(numCalGenTables);
	for (unsigned short cgIdx = 0; cgIdx < numCalGenTables; ++cgIdx)
	{
		auto& g = m_calGenTable[cgIdx];
		// Table includes n points from fstart to fstart + (n-1)*fstep but gets used up to fstart + n*fstep
		if (!ReadCalTable(S2630Msg::CALGENDATA, S2630Msg::LNA_OFF, cgIdx, calTable))
		{
			std::string errstr = "Unable to get calgencaltable: idx=" + std::to_string(cgIdx);
			throw std::runtime_error(errstr);
		}
//		printf("%u %f %f %u\n", cgIdx, calTable.fStartValueMHz, calTable.fStepValueMHz, calTable.numData);
		if (calTable.fStepValueMHz <= 0)
		{
			std::string errstr = "Invalid calgencaltable step value: idx=" + std::to_string(cgIdx);
			throw std::runtime_error(errstr);
		}
		g.startValue = calTable.fStartValueMHz * 1000000.0;
		g.stepValue = calTable.fStepValueMHz * 1000000.0;
		g.data.assign(calTable.data, calTable.data + calTable.numData);		// This is in dbm here
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the maximum attenuation value for the specified parameters
//
unsigned char C2630::CurrentMaxAtten(EGainMode gainMode) const
{
	size_t rfGainTableIdx;
	{
		CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
		rfGainTableIdx = 2 * (m_state->presel - 1);	// Use lna = 0 table (both should be same)
	}
	if (rfGainTableIdx >= m_rfGainTable.size())
	{
		printf("CurrentMaxAtten: rfGainTable index error\n");
		return 0;
	}
	size_t attenTableIdx = m_rfGainTable[rfGainTableIdx].attIndexes[size_t(gainMode)];
	auto& attenModeTable = m_attenTables[size_t(gainMode)];
	if (attenTableIdx >= attenModeTable.size())
	{
		printf("CurrentMaxAtten: attenTable index error\n");
		return 0;
	}
	auto& attenTable = attenModeTable[attenTableIdx];
	auto maxAtten = attenTable.data.size() * attenTable.stepValue;
	if (maxAtten > UCHAR_MAX)
		maxAtten = UCHAR_MAX;
	return static_cast<unsigned char>(maxAtten);
}


//////////////////////////////////////////////////////////////////////
//
// Get the antenna setting
//
bool C2630::ReadAntenna(unsigned char& ant)
{
	if (m_master)
	{
		if (GetImmediate(S2630Msg::ECommand::GET_ANTENNA_SETTING, ant) != 0)
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->ant = ant;
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get the attenuation setting
//
bool C2630::ReadAttenuation(unsigned char& atten, EGainMode& gainMode, bool& lna)
{
	if (m_master)
	{
		unsigned char other;
		if (GetImmediate(S2630Msg::ECommand::GET_ATTENUATION_SETTING, atten, other) != 0)
		{
			auto temp = other & 0x0f;
			if (temp == size_t(EGainMode::NORMAL)) gainMode = EGainMode::NORMAL;
			else if (temp == size_t(EGainMode::RURAL)) gainMode = EGainMode::RURAL;
			else if (temp == size_t(EGainMode::URBAN)) gainMode = EGainMode::URBAN;
			else if (temp == size_t(EGainMode::CONGESTED)) gainMode = EGainMode::CONGESTED;
			else gainMode = EGainMode::NORMAL;
			lna = (other & 0x10) != 0;
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->atten = atten;
			m_state->gainMode = gainMode;
			m_state->lna = lna;
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get the receiver bandwidth
//
bool C2630::ReadBandWidth(unsigned char& bandwidth)
{
	if (m_master)
	{
		if (GetImmediate(S2630Msg::ECommand::GET_BAND_WIDTH, bandwidth) != 0)
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->bw = bandwidth;
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get a receiver calibration table
//
bool C2630::ReadCalTable(S2630Msg::ECalibrationType calType, unsigned char subType, unsigned short index, S2630Msg::SCalTable& calTable)
{
	if (m_master)
	{
		unsigned long calReq = ((calType & 0xff) << 24) + (subType << 16) + index;
		unsigned long numBytes = GetLong(S2630Msg::ECommand::GET_RX_CAL_TABLE, calReq, calTable, true);
		if (numBytes != 0)
		{
			if (numBytes != sizeof(S2630Msg::UHeader) + offsetof(S2630Msg::SCalTable, data) + calTable.numData * sizeof(float))
			{
				printf("C2630::ReadCalTable error %lu != %u * 4 + %u\n", numBytes, calTable.numData,
					sizeof(S2630Msg::UHeader) + offsetof(S2630Msg::SCalTable, data));
			}
			return true;
		}
	}
	return false;
}


///////////////////////////////////////////////////////////////////////
//
// Read the caltone generator on/off state into m_state
//
bool C2630::ReadCalTone(bool& setOn)
{
	if (m_master)
	{
		unsigned char enable = 2;
		unsigned char setEnable;
		if (SetImmediate(S2630Msg::ECommand::RX_CAL_GEN_CONTROL, enable, setEnable) > 0)
		{
			printf("ReadCalTone %u %u\n", enable, setEnable);		// For debugging only
			setOn = (setEnable == 1);
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->calgenEnable = setOn;
			return true;
		}
	}
	return false;
}


///////////////////////////////////////////////////////////////////////
//
// Read the caltone generator frequency
//
bool C2630::ReadCalToneMHz(unsigned long& setFreqMHz)
{
	if (m_master)
	{
		unsigned long freqMHz;
		unsigned char extra;
		if (!GetShort(S2630Msg::ECommand::GET_RX_CAL_GEN_FREQUENCY, freqMHz, extra) == 0)
		{
			setFreqMHz = freqMHz;
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->calFreqMHz = freqMHz;
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Read the clock state
//
bool C2630::ReadClockState(EClockState& clockState)
{
//	if (m_sim)
//	{
//		clockStatus = (m_useExternal ? EXTERNAL : INTERNAL);
//		return true;
//	}
	if (m_master)
	{
		unsigned char state;
		if (GetImmediate(S2630Msg::ECommand::GET_CLOCK_STATE, state) != 0)
		{
			TRACE("ReadClockState = %u\n", state);
			if (state == 0)
			{
				clockState = EClockState::INTERNAL;
			}
			else if (state == 1)
			{
				clockState = EClockState::EXTERNAL;
			}
			else
			{
				return false;
			}
			return true;
		}
	}
	return false;
}

bool C2630::ReadOrSetClockStateStatic(bool doSet, EClockState newState, EClockState& clockState)
{
	auto driverDesc = m_driverDesc;
	if (driverDesc == -1)
	{
		driverDesc = open("/dev/spidev32766.0", O_RDWR);
		m_driverDesc = driverDesc;
	}
	if (driverDesc == -1)
	{
		printf("Failed to open /dev/spidev32766.0 errno = %d\n", errno);
		return false;
	}
	auto driverDesc1 = m_driverDesc1;
	if (driverDesc1 == -1)
	{
		driverDesc1 = open("/dev/TciSpi", O_RDONLY);
		m_driverDesc1 = driverDesc1;
	}
	if (driverDesc1 == -1)
	{
		printf("Failed to open /dev/TciSpi errno = %d\n", errno);
		close(driverDesc);
		return false;
	}

	S2630Msg::UHeader sendHdr;
	sendHdr.shortHdr.msgType = S2630Msg::SHORTMSG;
	sendHdr.shortHdr.command = (doSet ? S2630Msg::ECommand::SET_CLOCK_STATE : S2630Msg::ECommand::GET_CLOCK_STATE);
	sendHdr.shortHdr.numPackets = 0;
	sendHdr.shortHdr.body = static_cast<unsigned char>(newState);

//	unsigned long sendData = (sendHdr.msgType << 24) + (sendHdr.command << 16) + (sendHdr.numPackets << 8) + sendHdr.body;
//	unsigned long item = ByteSwap(sendData);
	int numwritten;

	TCISPI_RXDATA rxData;
	const S2630Msg* recvMsg;
	bool passed = false;
	for (size_t itry = 0; itry < 5; ++itry)
	{
		unsigned long item = ByteSwap(sendHdr.header);
		numwritten = write(driverDesc, &item, 4);
		if (numwritten <= 0)
		{
			CLog::Log(CLog::ERRORS, "write failed error num written = %d retry = %u", numwritten, itry);
			continue;
		}

		if (ioctl(driverDesc1, IOCTL_GetRxData, &rxData) == -1)
		{
			printf("Failed to read clock state - ioctl failed retry %u\n", itry);
			sleep(5);
			continue;
		}
		if (rxData.transferSize != sizeof(S2630Msg::UHeader))
		{
			printf("Failed to read clock states - transferSize != recvSize %lu %u retry %u\n", rxData.transferSize, sizeof(S2630Msg::UHeader), itry);
			sleep(5);
			continue;
		}

		recvMsg = reinterpret_cast<const S2630Msg*>(rxData.spiData);
		if (recvMsg->hdr.shortHdr.msgType != S2630Msg::SHORTMSG || recvMsg->hdr.shortHdr.command != sendHdr.shortHdr.command || recvMsg->hdr.shortHdr.numPackets != 0)
		{
			printf("Failed to read clock states retry %u\n", itry);
			sleep(5);
			continue;
		}
		passed = true;
		break;
	}
	if (!passed)
	{
		close(driverDesc1);
		close(driverDesc);
		m_driverDesc = -1;
		m_driverDesc1 = -1;
		return false;
	}
	//CLog::Log(CLog::INFORMATION, "ReadOrSetClockStateStatic passed\n"); // debug only, to be removed
	if (doSet && (sendHdr.shortHdr.body != recvMsg->hdr.shortHdr.body))
	{
		printf("clockstate set incorrectly - wanted %u received %u\n", sendHdr.shortHdr.body, recvMsg->hdr.shortHdr.body);
	}
	unsigned char state = recvMsg->hdr.shortHdr.body;

	//close(driverDesc1);
	//close(driverDesc);

	TRACE("ReadClockState = %u\n", state);
	if (state == 0)
	{
		clockState = EClockState::INTERNAL;
	}
	else if (state == 1)
	{
		clockState = EClockState::EXTERNAL;
	}
	else
	{
		return false;
	}
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Read the clock external reference
//
bool C2630::ReadExternalReference(EExternalRef& extRef)
{
	if (m_master)
	{
		unsigned char ref;
		if (GetImmediate(S2630Msg::ECommand::GET_EXTERNAL_REFERENCE, ref) != 0)
		{
			TRACE("GET_EXTERNAL_REFERENCE = %u\n", ref);
			if (ref == 0)
			{
				extRef = EExternalRef::EXT100MHZ;
			}
			else if (ref == 1)
			{
				extRef = EExternalRef::EXT10MHZ;
			}
			else
			{
				return false;
			}
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Read the clock internal reference
//
bool C2630::ReadInternalReference(EInternalRef& intRef)
{
	if (m_master)
	{
		unsigned char ref;
		if (GetImmediate(S2630Msg::ECommand::GET_INTERNAL_REFERENCE, ref) != 0)
		{
			TRACE("GET_INTERNAL_REFERENCE = %u\n", ref);
			if (ref == 0)
			{
				intRef = EInternalRef::INTERNAL;
			}
			else if (ref == 1)
			{
				intRef = EInternalRef::EXT10MHZ;
			}
			else if (ref == 2)
			{
				intRef = EInternalRef::EXT1PPS;
			}
			else if (ref == 3)
			{
				intRef = EInternalRef::EXT100MHZ;
			}
			else
			{
				return false;
			}
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get the firmware date
//
bool C2630::ReadFirmwareDate(unsigned long& fwDate)
{
	if (m_master)
	{
		if (GetShort(S2630Msg::ECommand::GET_FIRMWARE_DATE, fwDate) != 0)
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
//			m_state->fwDate = fwDate;
			m_state->versionInfo.fwDatestamp = fwDate;
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get the firmware version
//
bool C2630::ReadFirmwareVersion(unsigned long& fwVersion)
{
	if (m_master)
	{
		if (GetShort(S2630Msg::ECommand::GET_FIRMWARE_VERSION, fwVersion) != 0)
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
//			m_state->fwVersion = fwVersion;
			m_state->versionInfo.fwRevision = fwVersion;
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get the fpga version
//
bool C2630::ReadFpgaVersion(unsigned long& fpgaVersion)
{
	if (m_master)
	{
		if (GetShort(S2630Msg::ECommand::GET_FPGA_VERSION, fpgaVersion) != 0)
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
//			m_state->fpgaVersion = fpgaVersion;
			m_state->versionInfo.fpgaRevision = fpgaVersion;
			return true;
		}
	}
	return false;
}


#if 0
//////////////////////////////////////////////////////////////////////
//
// Get the receiver tune frequency
//
bool C2630::ReadFrequency(double& frequency)
{
	if (m_master)
	{
		if (GetShort(S2630Msg::ECommand::GET_FREQUENCY, frequency) != 0)
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->ifreq = frequency;
			return true;
		}
	}
	return false;
}
#endif

//////////////////////////////////////////////////////////////////////
//
// Get the receiver tune frequency
//
bool C2630::ReadFrequency(unsigned long& frequency)
{
	if (m_master)
	{
		if (GetShort(S2630Msg::ECommand::GET_INTEGER_FREQUENCY, frequency) != 0)
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->ifreq = frequency;
			return true;
		}
	}
	return false;
 }


//////////////////////////////////////////////////////////////////////
//
// Get the LO frequencies
//
bool C2630::ReadLoFrequencies(unsigned long& lo1, unsigned long& lo2)
{
	if (m_master)
	{
		S2630Msg::SGetLoFreqs loFreqs;
		if (GetShort(S2630Msg::ECommand::GET_LO_FREQUENCIES, loFreqs) != 0)
		{
			lo1 = loFreqs.lo1Frequency;
			lo2 = loFreqs.lo2Frequency;
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->lo1 = lo1;
			m_state->lo2 = lo2;
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get the Multiple Operations Command values
//
bool C2630::ReadMOC(unsigned long& freq, unsigned char& presel, unsigned char& bw, unsigned char& ant)
{
	if (m_master)
	{
		unsigned long moc;
		unsigned char extra;
		if (!GetShort(S2630Msg::ECommand::GET_MOC, moc, extra) == 0)	// 24 bit frequency, 8 bit preselector
		{
			freq = moc & 0x00ffffff;
			presel = moc >> 24;
			bw = ((extra & 0x01) == 0 ? 10 : 80);
			ant = (extra >> 1) & 0x07;
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->ifreq = freq;
			m_state->presel = presel;
			m_state->bw = bw;
			m_state->ant = ant;
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get the ocxo values
//
bool C2630::ReadOcxo(unsigned char& loopStatus, unsigned long& code, unsigned long& interval)
{
	if (m_master)
	{
		if (GetShort(S2630Msg::ECommand::GET_RX_OCXO_DAC_CODE, code) != 0 &&
			GetShort(S2630Msg::ECommand::GET_RX_OCXO_INTERVAL_BETWEEN_CORRECTIONS, interval) != 0 &&
			GetImmediate(S2630Msg::ECommand::GET_RX_OCXO_LOOP_STABLE_STATUS, loopStatus) != 0)
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->ocxoDacCode = code;
			m_state->ocxoInterval = interval;
			m_state->ocxoLoopStatus = loopStatus;
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get the ocxo dac code
//
bool C2630::ReadOcxoDacCode(unsigned long& code)
{
	if (m_master)
	{
		if (GetShort(S2630Msg::ECommand::GET_RX_OCXO_DAC_CODE, code) != 0)
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->ocxoDacCode = code;
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get the ocxo interval between corrections
//
bool C2630::ReadOcxoInterval(unsigned long& interval)
{
	if (m_master)
	{
		if (GetShort(S2630Msg::ECommand::GET_RX_OCXO_INTERVAL_BETWEEN_CORRECTIONS, interval) != 0)
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->ocxoInterval = interval;
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get the ocxo loop status
//
bool C2630::ReadOcxoLoopStatus(unsigned char& loopStatus)
{
	if (m_master)
	{
		if (GetImmediate(S2630Msg::ECommand::GET_RX_OCXO_LOOP_STABLE_STATUS, loopStatus) != 0)		// bit 0
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->ocxoLoopStatus = loopStatus;
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get the preselector index
//
bool C2630::ReadPreselector(unsigned char& preselector)
{
	if (m_master)
	{
		if (GetImmediate(S2630Msg::ECommand::GET_PRESELECTOR_SETTING, preselector) != 0)
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->presel = preselector;
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get the receiver capabilities
//
bool C2630::ReadRxCapabilities(S2630Msg::SRxCapabilities& rxCapabilities)
{
	if (m_master)
	{
		if (GetShort(S2630Msg::ECommand::GET_RX_CAPABILITIES, rxCapabilities) != 0)
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->rxCapabilities = rxCapabilities;
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get the receiver settings
//
bool C2630::ReadRxSettings(S2630Msg::SRxSettings& rxSettings)
{
	if (m_master)
	{
		if (GetShort(S2630Msg::ECommand::GET_RX_SETTINGS, rxSettings) != 0)
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->ant = static_cast<unsigned char>(rxSettings.currentAntennaSetting);
			m_state->atten = static_cast<unsigned char>(rxSettings.currentAttenuation);
			m_state->bw = static_cast<unsigned char>(rxSettings.currentBandWidth);
			m_state->flashVersion = rxSettings.currentFlashVersion;
			m_state->ifreq = rxSettings.currentFrequency;
			m_state->if1 = rxSettings.currentIf1Frequency;
			m_state->if2 = rxSettings.currentIf2Frequency;
			m_state->lo1 = rxSettings.currentLo1Frequency;
			m_state->lo2 = rxSettings.currentLo2Frequency;
			m_state->freqIndex = static_cast<unsigned char>(rxSettings.currentFPI);
			if (rxSettings.currentMode == size_t(EGainMode::NORMAL)) m_state->gainMode = EGainMode::NORMAL;
			else if (rxSettings.currentMode == size_t(EGainMode::RURAL)) m_state->gainMode = EGainMode::RURAL;
			else if (rxSettings.currentMode == size_t(EGainMode::URBAN)) m_state->gainMode = EGainMode::URBAN;
			else if (rxSettings.currentMode == size_t(EGainMode::CONGESTED)) m_state->gainMode = EGainMode::CONGESTED;
			else m_state->gainMode = EGainMode::NORMAL;
			m_state->psf1 = rxSettings.currentPsFreqLow;
			m_state->psf2 = rxSettings.currentPsFreqHigh;
			m_state->lna = rxSettings.currentLnaStatus != 0;
			m_state->sense = static_cast<unsigned char>(rxSettings.currentSpectralSense);
			m_state->versionInfo.fwRevision = rxSettings.currentFirmwareVersion;
			m_state->versionInfo.fwDatestamp = rxSettings.currentFirmwareDate;
			m_state->versionInfo.fpgaRevision = rxSettings.currentFpgaVersion;
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get the receiver frequency plan table
//
bool C2630::ReadRxTable(S2630Msg::SFreqPlanTable2& freqPlanTable, unsigned long& numFreqPlanRows)
{
	if (m_master)
	{
		unsigned long numBytes = GetLong(S2630Msg::ECommand::GET_RX_FPI_TABLE, freqPlanTable, true);
		if (numBytes != 0)
		{
			numFreqPlanRows = numBytes / sizeof(S2630Msg::SRxTable);
			if (numFreqPlanRows * sizeof(S2630Msg::SRxTable) != numBytes)
			{
				printf("C2630::ReadRxTable error %lu != %lu * %u\n", numBytes, numFreqPlanRows, sizeof(S2630Msg::SRxTable));
			}
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get radio temperatures
//
bool C2630::ReadRxTemperatures(std::vector<float>& temperatures)
{
	if (m_master)
	{
		temperatures.clear();
		for (unsigned char i = 0; i < 3; ++i)		// TODO: Get from enum
		{
			float temp;
			unsigned char extra;
			if (GetShort(S2630Msg::ECommand::GET_RX_TEMPERATURE, i, temp, extra) == 0)
				return false;	// Unable to get temperatures

			temperatures.push_back(temp);
		}
		CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
		m_state->temperatures[0] = temperatures[0];
		m_state->temperatures[1] = temperatures[1];
		m_state->temperatures[2] = temperatures[2];
		return true;
	}
	return false;
}


#if 0
//////////////////////////////////////////////////////////////////////
//
// Get the receiver temperature
//
bool C2630::ReadRxTemperature(signed char& temperature)
{
	if (m_master)
	{
		unsigned char temp;
		if (GetImmediate(S2630Msg::ECommand::GET_RX_TEMPERATURE, temp) != 0)
		{
			temperature = static_cast<signed char>(temp);
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->temperature = temperature;
			return true;
		}
	}
	return false;
}
#endif

//////////////////////////////////////////////////////////////////////
//
// Find the index of the highest band containing the frequency f
//
int C2630::HighestContainedBand(Units::Frequency f) const
{
	for (int i = 0; i < int(m_vushfBands.size()); ++i)
	{
		if (f < m_vushfBands[i].f1)
		{
			if (i == 0)
			{
//				printf("HighestContainedBand warning, frequency too low %f\n", f.Hz<double>());
				return 0;
			}
			return i - 1;
		}
	}
	return int(m_vushfBands.size()) - 1;
}


//////////////////////////////////////////////////////////////////////
//
// RUn the receiver internal BIST
//
bool C2630::RunBist(S2630Msg::SBistResults& bistResults)
{
	if (m_master)
	{
		unsigned long numBytes;
		bool usenewbist = false;
		//if (m_configData.rxProcessor >= 41 && fwVersion >= 0x02060000)
		if ((m_configData.interfaceType & 0x02) != 0)
		{
			usenewbist = true;
			numBytes = GetLong(S2630Msg::ECommand::GET_BIST, bistResults, true, 1); // use new Bist version 1 if spin 4 board and firmware > 205
		}
		else
		{
			numBytes = GetLong(S2630Msg::ECommand::GET_BIST, bistResults, true);
		}
		if (numBytes != 0)
		{
			if (numBytes != offsetof(S2630Msg::SBistResults, scanTest) + bistResults.maxFpi * sizeof(S2630Msg::SFpiTestResults))
			{
				if (!usenewbist)
					printf("old bist bytes mismatch\n");
				unsigned long bytestobe =  offsetof(S2630Msg::SBistResults, scanTest) + bistResults.maxFpi * sizeof(S2630Msg::SFpiTestResults) + sizeof(S2630Msg::SBistExt1);
				if (usenewbist)
					printf("new bist bytes mismatch got = %lu expected = %lu\n", numBytes, bytestobe);
			}
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Set the receiver attenuation
//
bool C2630::SetAttenuation(EGainMode gainMode, unsigned char attenuation, unsigned char& setAttenuation, EGainMode& setGainMode, bool& setLNA)
{
	unsigned char maxAtten = CurrentMaxAtten(gainMode);
	auto atten = std::min(attenuation, maxAtten);

	// Note: Even if the attenuation and gainMode have not changed, if the fpi has changed
	//       it is possible that the LNA value will have changed. So, in this case, we
	//       will read the current attenuator values to update the lna flag.

	bool noChange = false;
	{
		CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
		noChange = (atten == m_state->atten && gainMode == m_state->gainMode);
	}	// unlocks the shared memory lock which is also used by ReadAttenuator
	if (noChange)
	{
		// Update the current state values for attenuator and lna
		ReadAttenuation(setAttenuation, setGainMode, setLNA);
		return true;
	}

	if (m_master)
	{
		unsigned char other = size_t(gainMode) & 0x0f;
		unsigned char setAtt;
		unsigned char setOther;
		if (SetImmediate(S2630Msg::ECommand::SET_ATTENUATION, atten, other, setAtt, setOther) > 0)
		{
//			printf("SetAttenuation %u %u\n", atten, gainMode);	// for debugging
			setAttenuation = setAtt;
			auto temp = setOther & 0x0f;
			if (temp == size_t(EGainMode::NORMAL)) setGainMode = EGainMode::NORMAL;
			else if (temp == size_t(EGainMode::RURAL)) setGainMode = EGainMode::RURAL;
			else if (temp == size_t(EGainMode::URBAN)) setGainMode = EGainMode::URBAN;
			else if (temp == size_t(EGainMode::CONGESTED)) setGainMode = EGainMode::CONGESTED;
			else setGainMode = EGainMode::NORMAL;
			setLNA = (setOther & 0x10) != 0;
			if (atten != setAtt || gainMode != setGainMode)
			{
				static unsigned char l_atten;
				static unsigned char l_setAtten;
				static EGainMode l_gainMode;
				static EGainMode l_setGainMode;

				if(l_atten != atten || l_setAtten != setAtt ||
				    l_gainMode != gainMode || l_setGainMode != setGainMode)
				{
					printf("SetAttenuation wanted %u[%u] got %u[%u]\n", atten, unsigned(gainMode), setAtt, unsigned(setGainMode));
					l_atten = atten;
					l_setAtten = setAtt;
					l_gainMode = gainMode;
					l_setGainMode = setGainMode;
			    }
			}
			bool needDelay = false;
			{
				CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
				m_state->atten = setAtt;
				m_state->lna = setLNA;
				m_state->gainMode = setGainMode;
				needDelay = m_state->direct;
			}
			if (needDelay) Delay(DELAY_TO_DIRECT_USEC);	// 5 msec
			return true;
		}
		printf("SetAttenuation %u[%d] failed\n", atten, (int) gainMode);
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// set the receiver rf input
//
bool C2630::SetAntenna(unsigned char ant, unsigned char& setAntenna)
{
	{
		CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
		if (ant == m_state->ant)
		{
			setAntenna = ant;
			return true;
		}
	}
	if (m_master)
	{
		unsigned char setAnt;
		if (SetImmediate(S2630Msg::ECommand::SET_ANTENNA, ant, setAnt) > 0)
		{
//			printf("SetAntenna %u %u\n", ant, setAnt);
			if (ant != setAnt)
			{
				printf("SetAntenna wanted %u got %u\n", ant, setAnt);
			}
			setAntenna = setAnt;

			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->ant = setAnt;
			return true;
		}
		printf("SetAntenna %u failed\n", ant);
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// set the receiver bandwidth
//
bool C2630::SetBandWidth(unsigned char bandWidth, unsigned char& setBandwidth)
{
	auto bw = static_cast<unsigned char>(bandWidth <= m_configData.narrowBwMHz ? m_configData.narrowBwMHz : m_configData.tuneBwMHz);

	{
		CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
		if (bw == m_state->bw)
		{
			setBandwidth = bw;
			return true;
		}
	}
	if (m_master)
	{
		unsigned char setBw;
		if (SetImmediate(S2630Msg::ECommand::SET_BAND_WIDTH, bw, setBw) > 0)
		{
			printf("SetBandWidth %u %u\n", bw, setBw);
			if (bw != setBw)
			{
				printf("SetBandWidth wanted %u got %u\n", bw, setBw);
			}
			setBandwidth = setBw;
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->bw = setBw;
			return true;
		}
		printf("SetBandwidth %u failed\n", bw);
	}
	return false;
}


///////////////////////////////////////////////////////////////////////
//
// Set the caltone generator on/off
//
bool C2630::SetCalTone(bool on, bool& setOn)
{
	// Sets the caltone generator on or off - no caching
	if (m_master)
	{
		unsigned char enable = (on ? 1 : 0);
		unsigned char setEnable;
		if (SetImmediate(S2630Msg::ECommand::RX_CAL_GEN_CONTROL, enable, setEnable) > 0)
		{
			printf("SetCalTone %u %u\n", enable, setEnable);		// For debugging only
			if (enable != setEnable)
			{
				printf("SetCalTone wanted %u got %u\n", enable, setEnable);
			}
			setOn = (setEnable == 1);
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->calgenEnable = setOn;
			return true;
		}
		printf("SetCalTone %u failed\n", on);
		return true;
	}
	return false;
}


/////////////////////////////////////////////////////////////////////
//
// Set the caltone generator frequency
//
bool C2630::SetCalToneMHz(unsigned long freqMHz, unsigned long& setFreqMHz)
{
	// Sets the caltone generator to the closest frequency to the specified one.
	unsigned long calFreqMHz = 0;
	if (freqMHz < m_configData.minCalGenFreqMHz)
	{
		calFreqMHz = m_configData.minCalGenFreqMHz;
	}
	else if (freqMHz > m_configData.maxCalGenFreqMHz)
	{
		calFreqMHz = m_configData.maxCalGenFreqMHz;
	}
	else if (freqMHz < 3200)
	{
		unsigned long res = m_configData.minCalGenFreqStepBelow_3200MHz;
		calFreqMHz = (freqMHz + (res / 2)) / res * res;
		// 25 => 25, 26 => 25, 27 => 25, 28 => 30, ...
		// 3193 => 3195, 3194 => 3195, 3195 => 3195, 3196 => 3195, 3197 => 3195, 3198 => 3200, 3199 => 3200
	}
	else
	{
		unsigned long res = m_configData.minCalGenFreqStepBelow_3200MHz;
		calFreqMHz = (freqMHz + (res / 2)) / res * res;
		// 3200 => 3200, 3201 => 3200, 3202 => 3200, 3203 => 3200, 3204 => 3200, 3205 => 3210, 3206 => 3210
	}
	if (m_master)
	{
		unsigned char extra = 0;	// CalGen only
		unsigned long setFreq;
		unsigned char setExtra;
		if (SetShort(S2630Msg::ECommand::SET_RX_CAL_GEN_FREQUENCY, calFreqMHz, extra, setFreq, setExtra) != 0)
		{
//			printf("SET_RX_CAL_GEN_FREQUENCY %lu %lu\n", calFreqMHz, setFreq);

			if (calFreqMHz != setFreq || extra != setExtra)
			{
				printf("SetCalToneMHz wanted %lu %u got %lu %u\n", calFreqMHz, extra, setFreq, setExtra);
			}
			setFreqMHz = setFreq;

			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->calFreqMHz = setFreq;
			return true;
		}
		printf("SET_RX_CAL_GEN_FREQUENCY %lu failed\n", calFreqMHz);
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Set the clock state
//
bool C2630::SetClockState(EClockState state, EClockState& setState)
{
//	if (m_sim)
//	{
//		throw std::runtime_error("Simulated reboot");
//	}
	if (m_master)
	{
		unsigned char setVal;
		if (state == EClockState::INTERNAL) setVal = 0;
		else if (state == EClockState::EXTERNAL) setVal = 1;
		else return false;

		unsigned char getVal;
		if (SetImmediate(S2630Msg::ECommand::SET_CLOCK_STATE, setVal, getVal) > 0)
		{
			TRACE("SetClockState %u %u\n", setVal, getVal);
			if (setVal != getVal)
			{
				printf("SetClockState wanted %u got %u\n", setVal, getVal);
			}
			if (getVal == 0) setState = EClockState::INTERNAL;
			else if (getVal == 1) setState = EClockState::EXTERNAL;
			else return false;

			return true;
		}
		printf("SetClockState %u failed\n", (unsigned int) state);
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Set the clock external ref
//
bool C2630::SetExternalReference(EExternalRef ref, EExternalRef& setRef)
{
	if (m_master)
	{
		unsigned char setVal;
		if (ref == EExternalRef::EXT100MHZ) setVal = 0;
		else if (ref == EExternalRef::EXT10MHZ) setVal = 1;
		else return false;

		unsigned char getVal;
		if (SetImmediate(S2630Msg::ECommand::SET_EXTERNAL_REFERENCE, setVal, getVal) > 0)
		{
			TRACE("SetExternalReference %u %u\n", setVal, getVal);
			if (setVal != getVal)
			{
				printf("SetExternalReference wanted %u got %u\n", setVal, getVal);
			}
			// debug only
			if (getVal ==0)
				printf("got external reference to 100 Hz\n");
			else if (getVal == 1)
				printf("got external reference to 10 Hz\n");
			else
				printf("external reference return not 1 or 0\n");
			if (getVal == 0) setRef = EExternalRef::EXT100MHZ;
			else if (getVal == 1) setRef = EExternalRef::EXT10MHZ;
			else return false;

			return true;
		}
		printf("SetExternalReference %u failed\n", (unsigned int) ref);
	}
	return false;
}

bool C2630::SetExternalReferenceStatic(EExternalRef ref, EExternalRef& setRef)
{
	auto driverDesc = m_driverDesc;
	if (driverDesc == -1)
	{
		driverDesc = open("/dev/spidev32766.0", O_RDWR);
		m_driverDesc = driverDesc;
	}
	if (driverDesc == -1)
	{
		printf("Failed to open /dev/spidev32766.0\n");
		return false;
	}
	auto driverDesc1 = m_driverDesc1;
	if (driverDesc1 == -1)
	{
		driverDesc1 = open("/dev/TciSpi", O_RDONLY);
		m_driverDesc1 = driverDesc1;
	}
	if (driverDesc1 == -1)
	{
		printf("Failed to open /dev/TciSpi\n");
		close(driverDesc);
		return false;
	}

	unsigned char setVal;
	if (ref == EExternalRef::EXT100MHZ) setVal = 0;
	else if (ref == EExternalRef::EXT10MHZ) setVal = 1;
	else return false;

	S2630Msg::UHeader sendHdr;
	sendHdr.shortHdr.msgType = S2630Msg::SHORTMSG;
	sendHdr.shortHdr.command = S2630Msg::ECommand::SET_EXTERNAL_REFERENCE;
	sendHdr.shortHdr.numPackets = 0;
	sendHdr.shortHdr.body = static_cast<unsigned char>(setVal);

	unsigned long item = ByteSwap(sendHdr.header);
	write(driverDesc, &item, 4);

	TCISPI_RXDATA rxData;
	if (ioctl(driverDesc1, IOCTL_GetRxData, &rxData) == -1)
	{
		printf("Failed to SetExternalReference - ioctl failed\n");
		close(driverDesc1);
		close(driverDesc);
		return false;
	}

	if (rxData.transferSize != sizeof(S2630Msg::UHeader))
	{
		printf("Failed to SetExternalReference - transferSize != recvSize %lu %u\n", rxData.transferSize, sizeof(S2630Msg::UHeader));
		close(driverDesc1);
		close(driverDesc);
		return false;
	}

	auto recvMsg = reinterpret_cast<const S2630Msg*>(rxData.spiData);
	if (recvMsg->hdr.shortHdr.msgType != S2630Msg::SHORTMSG || recvMsg->hdr.shortHdr.command != sendHdr.shortHdr.command || recvMsg->hdr.shortHdr.numPackets != 0)
	{
		printf("Failed to SetExternalReference \n");
		close(driverDesc1);
		close(driverDesc);
		return false;
	}

	if (sendHdr.shortHdr.body != recvMsg->hdr.shortHdr.body)
	{
		printf("externalref set incorrectly - wanted %u received %u\n", sendHdr.shortHdr.body, recvMsg->hdr.shortHdr.body);
	}

	unsigned char getVal = recvMsg->hdr.shortHdr.body;

	//close(driverDesc1);
	//close(driverDesc);

	TRACE("SetExternalReference = %u\n", getVal);

	if (getVal == 0) setRef = EExternalRef::EXT100MHZ;
	else if (getVal == 1) setRef = EExternalRef::EXT10MHZ;
	else return false;

	return true;
}

bool C2630::ReadMasterStateStatic()
{
	auto driverDesc = m_driverDesc;
	if (driverDesc == -1)
	{
		driverDesc = open("/dev/spidev32766.0", O_RDWR);
		m_driverDesc = driverDesc;
	}
	if (driverDesc == -1)
	{
		printf("Failed to open /dev/spidev32766.0\n");
		return false;
	}
	auto driverDesc1 = m_driverDesc1;
	if (driverDesc1 == -1)
	{
		driverDesc1 = open("/dev/TciSpi", O_RDONLY);
		m_driverDesc1 = driverDesc1;
	}
	if (driverDesc1 == -1)
	{
		printf("Failed to open /dev/TciSpi\n");
		close(driverDesc);
		return false;
	}

	unsigned char setVal = 0;

	S2630Msg::UHeader sendHdr;
	sendHdr.shortHdr.msgType = S2630Msg::SHORTMSG;
	sendHdr.shortHdr.command = S2630Msg::ECommand::GET_MASTER_STATE;
	sendHdr.shortHdr.numPackets = 0;
	sendHdr.shortHdr.body = static_cast<unsigned char>(setVal);

	unsigned long item = ByteSwap(sendHdr.header);
	write(driverDesc, &item, 4);

	TCISPI_RXDATA rxData;
	if (ioctl(driverDesc1, IOCTL_GetRxData, &rxData) == -1)
	{
		printf("Failed to GetMasterState - ioctl failed\n");
		close(driverDesc1);
		close(driverDesc);
		return false;
	}

	if (rxData.transferSize != sizeof(S2630Msg::UHeader))
	{
		printf("Failed to GetMasterState - transferSize != recvSize %lu %u\n", rxData.transferSize, sizeof(S2630Msg::UHeader));
		close(driverDesc1);
		close(driverDesc);
		return false;
	}

	auto recvMsg = reinterpret_cast<const S2630Msg*>(rxData.spiData);
	if (recvMsg->hdr.shortHdr.msgType != S2630Msg::SHORTMSG || recvMsg->hdr.shortHdr.command != sendHdr.shortHdr.command || recvMsg->hdr.shortHdr.numPackets != 0)
	{
		printf("Failed to GetMasterState \n");
		close(driverDesc1);
		close(driverDesc);
		return false;
	}

	unsigned char getVal = recvMsg->hdr.shortHdr.body;

	//close(driverDesc1);
	//close(driverDesc);

	TRACE("GetMasterState = %u\n", getVal);

	return getVal;
}

bool C2630::SetMasterStateStatic(unsigned char mState)
{
	auto driverDesc = m_driverDesc;
	if (driverDesc == -1)
	{
		driverDesc = open("/dev/spidev32766.0", O_RDWR);
		m_driverDesc = driverDesc;
	}
	if (driverDesc == -1)
	{
		printf("Failed to open /dev/spidev32766.0\n");
		return false;
	}
	auto driverDesc1 = m_driverDesc1;
	if (driverDesc1 == -1)
	{
		driverDesc1 = open("/dev/TciSpi", O_RDONLY);
		m_driverDesc1 = driverDesc1;
	}
	if (driverDesc1 == -1)
	{
		printf("Failed to open /dev/TciSpi\n");
		close(driverDesc);
		return false;
	}

	unsigned char setVal;
	setVal = mState;

	S2630Msg::UHeader sendHdr;
	sendHdr.shortHdr.msgType = S2630Msg::SHORTMSG;
	sendHdr.shortHdr.command = S2630Msg::ECommand::SET_MASTER_STATE;
	sendHdr.shortHdr.numPackets = 0;
	sendHdr.shortHdr.body = static_cast<unsigned char>(setVal);

	unsigned long item = ByteSwap(sendHdr.header);
	write(driverDesc, &item, 4);

	TCISPI_RXDATA rxData;
	if (ioctl(driverDesc1, IOCTL_GetRxData, &rxData) == -1)
	{
		printf("Failed to SetMasterState - ioctl failed\n");
		close(driverDesc1);
		close(driverDesc);
		return false;
	}

	if (rxData.transferSize != sizeof(S2630Msg::UHeader))
	{
		printf("Failed to SetMasterState - transferSize != recvSize %lu %u\n", rxData.transferSize, sizeof(S2630Msg::UHeader));
		close(driverDesc1);
		close(driverDesc);
		return false;
	}

	auto recvMsg = reinterpret_cast<const S2630Msg*>(rxData.spiData);
	if (recvMsg->hdr.shortHdr.msgType != S2630Msg::SHORTMSG || recvMsg->hdr.shortHdr.command != sendHdr.shortHdr.command || recvMsg->hdr.shortHdr.numPackets != 0)
	{
		printf("Failed to SetMasterState \n");
		close(driverDesc1);
		close(driverDesc);
		return false;
	}

	if (sendHdr.shortHdr.body != recvMsg->hdr.shortHdr.body)
	{
		printf("master state set incorrectly - wanted %u received %u\n", sendHdr.shortHdr.body, recvMsg->hdr.shortHdr.body);
	}

	unsigned char getVal = recvMsg->hdr.shortHdr.body;

	//close(driverDesc1);
	//close(driverDesc);

	TRACE("SetMasterState = %u\n", getVal);

	if (getVal != setVal)
		return false;

	return true;
}

bool C2630::SetFlashLockStatic(unsigned char mLockState)
{
	auto driverDesc = m_driverDesc;
	if (driverDesc == -1)
	{
		driverDesc = open("/dev/spidev32766.0", O_RDWR);
		m_driverDesc = driverDesc;
	}
	if (driverDesc == -1)
	{
		printf("Failed to open /dev/spidev32766.0\n");
		return false;
	}
	auto driverDesc1 = m_driverDesc1;
	if (driverDesc1 == -1)
	{
		driverDesc1 = open("/dev/TciSpi", O_RDONLY);
		m_driverDesc1 = driverDesc1;
	}
	if (driverDesc1 == -1)
	{
		printf("Failed to open /dev/TciSpi\n");
		close(driverDesc);
		return false;
	}

	unsigned char setVal;
	setVal = mLockState;

	S2630Msg::UHeader sendHdr;
	sendHdr.shortHdr.msgType = S2630Msg::SHORTMSG;
	sendHdr.shortHdr.command = S2630Msg::ECommand::QSPI_SET_FLASH_LOCK_STATE;
	sendHdr.shortHdr.numPackets = 0;
	sendHdr.shortHdr.body = static_cast<unsigned char>(setVal);

	unsigned long item = ByteSwap(sendHdr.header);
	write(driverDesc, &item, 4);

	TCISPI_RXDATA rxData;
	if (ioctl(driverDesc1, IOCTL_GetRxData, &rxData) == -1)
	{
		printf("Failed to SetFlashLockState - ioctl failed\n");
		close(driverDesc1);
		close(driverDesc);
		return false;
	}

	if (rxData.transferSize != sizeof(S2630Msg::UHeader))
	{
		printf("Failed to SetFlashLockState - transferSize != recvSize %lu %u\n", rxData.transferSize, sizeof(S2630Msg::UHeader));
		close(driverDesc1);
		close(driverDesc);
		return false;
	}

	auto recvMsg = reinterpret_cast<const S2630Msg*>(rxData.spiData);
	if (recvMsg->hdr.shortHdr.msgType != S2630Msg::SHORTMSG || recvMsg->hdr.shortHdr.command != sendHdr.shortHdr.command || recvMsg->hdr.shortHdr.numPackets != 0)
	{
		printf("Failed to SetFlashLockState \n");
		close(driverDesc1);
		close(driverDesc);
		return false;
	}

	if (sendHdr.shortHdr.body != recvMsg->hdr.shortHdr.body)
	{
		printf("SetFlashLockState set incorrectly - wanted %u received %u\n", sendHdr.shortHdr.body, recvMsg->hdr.shortHdr.body);
	}

	unsigned char getVal = recvMsg->hdr.shortHdr.body;

	//close(driverDesc1);
	//close(driverDesc);

	TRACE("SetMasterState = %u\n", getVal);

	if (getVal != setVal)
	{
		if (setVal)
			printf("failed to lock flash statu\ns");
		else
			printf("failed to unlock flash statu\ns");
		return false;
	}

	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Set the clock internal ref
//
bool C2630::SetInternalReference(EInternalRef ref, EInternalRef& setRef)
{
	if (m_master)
	{
		unsigned char setVal;
		if (ref == EInternalRef::INTERNAL) setVal = 0;
		else if (ref == EInternalRef::EXT10MHZ) setVal = 1;
		else if (ref == EInternalRef::EXT1PPS) setVal = 2;
		else if (ref == EInternalRef::EXT100MHZ) setVal = 3;
		else return false;

		unsigned char getVal;
		if (SetImmediate(S2630Msg::ECommand::SET_INTERNAL_REFERENCE, setVal, getVal) > 0)
		{
			TRACE("SetInternalReference %u %u\n", setVal, getVal);
			if (setVal != getVal)
			{
				printf("SetInternalReference wanted %u got %u\n", setVal, getVal);
			}
			if (getVal == 0) setRef = EInternalRef::INTERNAL;
			else if (getVal == 1) setRef = EInternalRef::EXT10MHZ;
			else if (getVal == 2) setRef = EInternalRef::EXT1PPS;
			else if (getVal == 3) setRef = EInternalRef::EXT100MHZ;
			else return false;

			return true;
		}
		printf("SetInternalReference %u failed\n", (unsigned int) ref);
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// set the receiver frequency
//
bool C2630::SetFrequency(unsigned long ifreq, unsigned long& setIfreq)
{
	{
		CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
		if (ifreq == m_state->ifreq)
		{
			setIfreq = m_state->ifreq;
			return true;
		}
	}

	if (m_master)
	{
		unsigned long setFreq;
		if (SetShort(S2630Msg::ECommand::SET_INTEGER_FREQUENCY, ifreq, setFreq) != 0)
		{
			printf("SET_INTEGER_FREQUENCY %lu %lu\n", ifreq, setFreq);

			if (ifreq != setFreq)
			{
				printf("SetFrequency wanted %lu got %lu\n", ifreq, setFreq);
			}
			setIfreq = setFreq;

			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->ifreq = setFreq;
			return true;
		}
		printf("SET_INTEGER_FREQUENCY %lu failed\n", ifreq);
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Received multiple operations command - frequency, preselector, bandwidth, antenna
//
bool C2630::SetMOC(unsigned long freq, unsigned char presel, unsigned char bw, unsigned char ant,
	unsigned long& setFreq, unsigned char& setPresel, unsigned char& setBw, unsigned char& setAnt)
{
	if (freq > 0x01000000)
		return false;
	if ((presel >= 3 && bw != 80 && bw != 10) || (presel < 3 && bw != 30))
	{
		//printf ("setmoc error presel=%d----bw = %d\n", presel, bw); // debug only
		return false;
	}
	{
		CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
		if (freq == m_state->ifreq && presel == m_state->presel && bw == m_state->bw && ant == m_state->ant)
		{
//			printf("SetMOC %lu %u %u %u => not setting\n", freq, presel, bw, ant);
			//printf("setmoc no change---\n"); //debug only
			setFreq = freq;
			setPresel = presel;
			setBw = bw;
			setAnt = ant;
			return true;
		}
	}

	if (m_master)
	{
		unsigned long moc = (freq & 0x00ffffff) | (presel << 24);
		unsigned char extra = (bw == 10 ? 0 : 1) | ((ant & 7) << 1);
		unsigned long setMoc;
		unsigned char setExtra;
		if (SetShort(S2630Msg::ECommand::SET_MOC, moc, extra, setMoc, setExtra) > 0)
		{
//			printf("SetMOC fr:%lu ps:%u bw:%u rf:%u => %lu %lu\n", freq, presel, bw, ant, moc, setMoc);
			if (moc != setMoc || extra != setExtra)
			{
				printf("SetMOC wanted %lu %u got %lu %u\n", moc, extra, setMoc, setExtra);
			}
			setFreq = setMoc & 0x00ffffff;
			setPresel = setMoc >> 24;
			if (setPresel >= 3)
				setBw = ((setExtra & 1) == 0 ? 10 : 80);
			else
				setBw = 30;
			setAnt = (setExtra >> 1) & 7;

			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->ifreq = setFreq;
			m_state->presel = setPresel;
			m_state->bw = setBw;
			m_state->ant = setAnt;
			//printf("setAnt=%d\n", setAnt);
			return true;
		}
		printf("SetMOC %08lx %02x failed\n", moc, extra);
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Set the preselector
//
bool C2630::SetPreselector(unsigned char freqPlanIndex, unsigned char& setIndex)
{
	{
		CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
		if (freqPlanIndex == m_state->presel)
		{
			setIndex = freqPlanIndex;
			return true;
		}
	}

	if (m_master)
	{
		unsigned char setFpi;
		if (SetImmediate(S2630Msg::ECommand::SET_PRESELECTORS, freqPlanIndex, setFpi) > 0)
		{
			printf("SetPreselector %u %u\n", freqPlanIndex, setFpi);
			if (freqPlanIndex != setFpi)
			{
				printf("SetPreselector wanted %u got %u\n", freqPlanIndex, setIndex);
			}
			setIndex = setFpi;

			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			m_state->presel = setFpi;
			return true;
		}
		printf("SetPreselector %u failed\n", freqPlanIndex);
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Tune the radio (frequency, bandwidth, preselector, rfinput)
//
bool C2630::Tune(Units::Frequency frequency, Units::Frequency rxBw, Units::Frequency procBw, bool hf, S2630Msg::EAntennaType rxInput,
	EBandSelect bandSelect, bool& inverted, bool& direct, size_t& band, Units::Frequency& radioFreq, Units::Frequency& f1, Units::Frequency& f2,
	Units::Frequency& retVal)
{
//	TRACE("f=%f rxbw=%f procbw=%f hf=%d\n", frequency.Hz<double>(), rxBw.Hz<double>(), procBw.Hz<double>(), hf);
	assert((bandSelect == EBandSelect::OPTIMUM) == (procBw == Units::Frequency()));

#ifdef CSMS_DEBUG
	int freqIndex = 0;
	int psIndex = 0;
	unsigned long ifreq = 0;

	if(s_printTuneInfo)
	{
		TRACE("C2630::Tune f=%f rxbw=%f procbw=%f hf=%d\n", frequency.Hz<double>(), rxBw.Hz<double>(), procBw.Hz<double>(), hf);
	}

#endif
	// debug only
	//printf("C2630::Tune f=%f rxbw=%f procbw=%f hf=%d\n", frequency.Hz<double>(), rxBw.Hz<double>(), procBw.Hz<double>(), hf);

	{
		CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
		m_state->frequency = frequency.Hz<double>();
	}
	if (hf)
	{
		if (procBw == Units::Frequency())
		{
			procBw = rxBw;
		}
		int index = 1;
		if (frequency - procBw / 2 < m_hfBands[index].f1)
		{
			index = 0;
		}
		f1 = std::max(frequency - procBw / 2, m_hfBands[index].psf1);
		f2 = std::min(frequency + procBw / 2, m_hfBands[index].psf2 + procBw);		// Allow extended limit

		inverted = m_hfBands[index].inverted;
		direct = m_hfBands[index].direct;
		bool needDelay = false;
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			needDelay = (direct && !m_state->direct);
			m_state->inverted = inverted;
			m_state->direct = direct;
		}
		band = index + 1;
		unsigned long getFreq;
		unsigned char getPresel;
		unsigned char getBw;
		unsigned char getAnt;
//		TRACE("SetMOC f=%lu bw=%u idx=%u\n", 15UL, 30, index);
		auto ifreq = CalcRxFreqMHz(hf, frequency);
		SetMOC(ifreq, index + 1, 30, rxInput, getFreq, getPresel, getBw, getAnt);	// index starts at 1 for hf
		radioFreq = Units::Frequency(ifreq * 1000000ULL);
		retVal = frequency;
		if (needDelay) Delay(DELAY_TO_DIRECT_USEC);	// 5 msec
		return true;
	}
	else
	{
#ifndef CSMS_DEBUG
		int freqIndex, psIndex;
		unsigned long ifreq;
#endif
		if (procBw == Units::Frequency())
		{
			auto freq = frequency.Hz<double>() / 1000000.;		// MHz
			if (freq > m_configData.maxVushfFreqMHz)
			{
				retVal = Units::Frequency();
				return false;
			}

			if (freq > m_configData.tuneCrossOverMHz)
			{
				// Convert frequency to nearest 10 MHz
				ifreq = m_configData.tuneResolutionMHz * static_cast<unsigned long>(freq / m_configData.tuneResolutionMHz + 0.5);
			}
			else
			{
				// Set to nearest MHz
				ifreq = std::min(static_cast<unsigned long>(freq + 0.5), m_configData.tuneCrossOverMHz - 1UL);
			}
			freqIndex = BandIndex(ifreq);
			if (freqIndex == 0)
			{
				ifreq = 50;
			}
			else if (freqIndex > 0 && ifreq < m_configData.lowestHeterodyneFreqMHz)
			{
				ifreq = m_configData.lowestHeterodyneFreqMHz;
				freqIndex = BandIndex(ifreq);		// Should be same as before.
			}
			psIndex = freqIndex;
			f1 = std::max(frequency - rxBw / 2, m_vushfBands[freqIndex].psf1);
			f2 = std::min(frequency + rxBw / 2, m_vushfBands[freqIndex].psf2);

#ifdef CSMS_DEBUG
			// "Automatic" preselector determination for discrete channel measurements, pan
			unsigned long rxFreq = CalcRxFreqMHz(hf, frequency);
			assert(ifreq == rxFreq);	// TODO: replace ifreq above by rxFreq.
#endif
		}
		else
		{
			auto freq = frequency.Hz<double>() / 1000000.;		// MHz
			psIndex = HighestContainedBand(frequency - procBw / 2) + size_t(bandSelect);	// HCB will return 0 if freq - procBw / 2 < 20 MHz
			if (psIndex >= int(m_vushfBands.size()))
			{
				psIndex = int(m_vushfBands.size() - 1);
			}
			if (psIndex > 0 && frequency < m_vushfBands[psIndex - 1].ftop && frequency + procBw / 2 <= m_vushfBands[psIndex - 1].f2)
			{
				--psIndex;
			}
			if (psIndex == -1)	// Automatic FPI determination - won't happen here.
			{
				printf("index = -1 error\n");
				if (freq > m_configData.tuneCrossOverMHz)
				{
					// Convert frequency to nearest 10 MHz
					ifreq = m_configData.tuneResolutionMHz * static_cast<unsigned long>(freq / m_configData.tuneResolutionMHz + 0.5);
				}
				else
				{
					// Set to nearest MHz
					ifreq = std::min(static_cast<unsigned long>(freq + 0.5), m_configData.tuneCrossOverMHz - 1UL);
				}
			}
			if (psIndex == 0)
			{
				// Set to nearest MHz
				ifreq = static_cast<unsigned long>(std::min(freq + 0.5, static_cast<double>(m_vushfBands[psIndex].ftopMHz - 1.0)));
			}
			else	// psIndex > 0
			{
				// Convert frequency to nearest 10 MHz
				ifreq = m_configData.tuneResolutionMHz * static_cast<unsigned long>(freq / m_configData.tuneResolutionMHz + 0.5);
				if (ifreq < m_configData.lowestHeterodyneFreqMHz)
				{
					ifreq = m_configData.lowestHeterodyneFreqMHz;
				}
				else if (ifreq > m_configData.maxVushfFreqMHz)
				{
					ifreq = m_configData.maxVushfFreqMHz;
				}
			}
			freqIndex = BandIndex(ifreq);			//  Note: freqIndex is not the same as index!
			if (freqIndex == 0)
			{
				ifreq = 50;
			}
			if (bandSelect == EBandSelect::BAND1)
			{
				f1 = frequency - procBw / 2;
			}
			else
			{
				f1 = std::max(frequency - procBw / 2, (psIndex == 0 ? m_vushfBands[0].psf1 : m_vushfBands[psIndex - 1].ftop));
			}
			if (bandSelect == EBandSelect::BAND3)
			{
				f2 = frequency + procBw / 2;
			}
			else
			{
				if (ifreq >= m_configData.maxVushfFreqMHz)	// Special case at top of range
				{
					f2 = Units::Frequency(m_configData.maxVushfFreqMHz * 1000000.0) + procBw / 2;
				}
				else
				{
					f2 = (frequency + procBw / 2 >= m_vushfBands[psIndex].psf2 ? m_vushfBands[psIndex].ftop : frequency + procBw / 2);
				}
			}
		}

		inverted = m_vushfBands[freqIndex].inverted;
		direct = m_vushfBands[freqIndex].direct;
		bool needDelay = false;
		{
			CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
			needDelay = (direct && !m_state->direct);
			m_state->inverted = inverted;
			m_state->direct = direct;
		}

		unsigned long getFreq;
		unsigned char getPresel;
		unsigned char getBw;
		unsigned char getAnt;

		unsigned char setBw;
		if (psIndex == 1)
		{
			setBw = 80;		// psIndex 1 must use 80 MHz bw due to low frequency limitation
		}
		else
		{
			setBw = (rxBw > Units::Frequency(10000000) ? 80 : 10);
		}

#ifdef CSMS_DEBUG
		if(s_printTuneInfo)
		{
			TRACE("C2630::Tune: SetMOC f=%lu bw=%u idx=%u\n", ifreq, setBw, (freqIndex == 0 ? 0 : psIndex) + 3);
		}
#endif
		// debug only
		//printf("C2630::Tune: SetMOC f=%lu bw=%u idx=%u\n", ifreq, setBw, (freqIndex == 0 ? 0 : psIndex) + 3);
//		TRACE("SetMOC f=%lu bw=%u idx=%u\n", ifreq, setBw, (freqIndex == 0 ? 0 : psIndex) + 3);
		bool rvSetMoc;
		if (!(rvSetMoc = SetMOC(ifreq, (freqIndex == 0 ? 0 : psIndex) + 3, setBw, rxInput, getFreq, getPresel, getBw, getAnt)))
		{
			assert(rvSetMoc);
		}
		radioFreq = Units::Frequency(ifreq * 1000000ULL);
		if (freqIndex == 0)
		{
			band = 3;
			retVal = frequency;
		}
		else
		{
			band = psIndex + 3;
			auto actualRadioFreq = Units::Frequency(getFreq * 1000000ULL);
			auto freqOffset = (inverted ? actualRadioFreq - frequency : frequency - actualRadioFreq);
#ifndef NDEBUG
			// By design, it is not necessary to check that f1-f2 falls withing the "extended" IF filters
			assert((f1 >= actualRadioFreq - Units::Frequency(setBw == 10 ? 20000000 : 90000000) / 2) &&
				(f2 <= actualRadioFreq + Units::Frequency(setBw == 10 ? 20000000 : 90000000) / 2));

			unsigned long limit = 5000000;
			if (freqIndex == 1 || getFreq == m_configData.maxVushfFreqMHz)
			{
				limit = 80000000;
			}
			else if (freqIndex == 2)
			{
				limit = 20000000;
			}
			assert(freqOffset >= -Units::Frequency(limit) && freqOffset <= Units::Frequency(limit));
#endif
			retVal = Units::Frequency(m_configData.finalIfFreqMHz * 1000000.0) + freqOffset;
		}
		if (needDelay) Delay(DELAY_TO_DIRECT_USEC);	// 5 msec
#ifdef CSMS_DEBUG
		if(s_printTuneInfo)
		{
			TRACE("C2630::Tune: rxInput = %u freqIndex=%i psIndex=%i inverted=%u direct =%u band = %u radioFreq = %fMHz f1 = %fMHz f2 = %fMHz retVal= %fMHz\n",
			      rxInput, freqIndex, psIndex, (inverted ? 1 : 0), (direct ? 1 : 0), band, radioFreq.Hz<float>()/1000000,
			      f1.Hz<float>()/1000000, f2.Hz<float>()/1000000, retVal.Hz<float>()/1000000);
			s_printTuneInfo = false;
		}
#endif
		//debug only
		//printf("C2630::Tune: rxInput = %u freqIndex=%i psIndex=%i inverted=%u direct =%u band = %u radioFreq = %fMHz f1 = %fMHz f2 = %fMHz retVal= %fMHz\n",
		//	      rxInput, freqIndex, psIndex, (inverted ? 1 : 0), (direct ? 1 : 0), band, radioFreq.Hz<float>()/1000000,
		//	      f1.Hz<float>()/1000000, f2.Hz<float>()/1000000, retVal.Hz<float>()/1000000);
		return true;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Tune the radio raw (used for VCP) (frequency, bandwidth, preselector, rfinput)
//
bool C2630::TuneRaw(const Units::Frequency& frequency, unsigned char rxBw, bool hf, bool autoFpi, unsigned char fpi,
	S2630Msg::EAntennaType rxInput, bool& inverted, bool& direct, Units::Frequency& radioFreq, Units::Frequency& ifFreq)
{
	inverted = false;
	direct = false;
	radioFreq = Units::Frequency();
	ifFreq = Units::Frequency();
	{
		CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
		m_state->frequency = frequency.Hz<double>();
	}
	unsigned long getFreq;
	unsigned char getPresel;
	unsigned char getBw;
	unsigned char getAnt;
	bool rv = false;
	int index = (fpi > 0 ? fpi - 1 : 0);
	unsigned long ifreq = CalcRxFreqMHz(hf, frequency);
	if (hf)
	{
		if (autoFpi)
		{
			index = (frequency < m_hfBands[1].f1 ? 0 : 1);
		}
		if (index < 0 || index >= int(m_hfBands.size()))
		{
			return false;
		}
		inverted = m_hfBands[index].inverted;
		direct = m_hfBands[index].direct;
	}
	else	// not hf
	{
		if (autoFpi)
		{
			index = BandIndex(ifreq) + 2;
		}
		if (index < 2 || index >= int(m_vushfBands.size()) + 2)
		{
			return false;
		}
		inverted = m_vushfBands[index - 2].inverted;
		direct = m_vushfBands[index - 2].direct;
	}
	bool pathChanged = false;
	bool needDelay = false;
	{
		CSharedMemory::CLockGuard lock(m_shMem->sharedMutex);
		needDelay = (direct && !m_state->direct);
		pathChanged = (direct == m_state->direct);
		m_state->inverted = inverted;
		// Select A/D
		m_state->direct = direct;
	}

	radioFreq = Units::Frequency(ifreq * 1000000ULL);
	rv = SetMOC(ifreq, index + 1, rxBw, rxInput, getFreq, getPresel, getBw, getAnt);

	if (rv && pathChanged)
	{
		try
		{
			CWeakSingleton<CDigitizer> digitizer;
			digitizer->SetADCChannel(direct);
		}
		catch(CWeakSingleton<CDigitizer>::NoInstance&)
		{
			TRACE("C2630::unable to create digitizer instance\n");
		}
	}

	//printf("hf = %d----autofpi = %d----ifreq = %lu--- index= %d ---- rxBw = %u--- rxInput = %d----getFreq= %lu------getPresel=%u, getbw= %u---- getAnt= %d\n", hf, autoFpi, ifreq,
	//		index, rxBw, (int)rxInput, getFreq, getPresel, getBw, getAnt);

	if (index <= 2)	// same as direct?
	{
		ifFreq = frequency;
	}
	else
	{
		auto actualRadioFreq = Units::Frequency(getFreq * 1000000ULL);
		auto freqOffset = (inverted ? actualRadioFreq - frequency : frequency - actualRadioFreq);
		ifFreq = Units::Frequency(m_configData.finalIfFreqMHz * 1000000.0) + freqOffset;
	}
	if (needDelay) Delay(DELAY_TO_DIRECT_USEC);	// 5 msec
	return rv;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for receiver to settle
//
void C2630::WaitForSettle(unsigned long usec)
{
	timespec ts, tr;
	ts.tv_sec = 0;
	ts.tv_nsec = 1000 * usec;	// approx 1 msec default
	nanosleep(&ts, &tr);
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Write to SPI followed by read
//
size_t C2630::WriteRead(const void* sendMsg, size_t sendSize, void* recvMsg, size_t recvSize, bool varLen)
{
	std::lock_guard<std::mutex> lock(m_2630Mutex);

	for (size_t itry = 0; itry < 5; ++itry)
	{
		// Send request to receiver via SPI.
		if (sendMsg)
		{
			auto data = reinterpret_cast<const unsigned long*>(sendMsg);
	//		TRACE("sendMsg %u\n", sendSize);
			for (unsigned long i = 0; i < sendSize / sizeof(unsigned long); ++i)
			{
				unsigned long item = *(data + i);
				item = ByteSwap(item);
				int numwritten;
				numwritten = write(m_driverDesc, &item, 4);
				if (numwritten <= 0)
				{
					CLog::Log(CLog::ERRORS, "write failed error num written = %d retry = %u", numwritten, itry);
					continue;
				}
	//			TRACE("%08lx\n", item);
			}
		}

		if (recvMsg)
		{
			if (ioctl(m_driverDesc1, IOCTL_GetRxData, &m_rxData) == -1)
			{
				CLog::Log(CLog::ERRORS, "ioctl failed %u", itry);
				continue;	//	return 0;
			}
			if (varLen)
			{
				if (m_rxData.transferSize > recvSize)
				{
					{
						CLog::Log(CLog::ERRORS, "transferSize > recvSize %lu %u %u\n", m_rxData.transferSize, recvSize, itry);
						CLog::Log(CLog::ERRORS, "%u %u %u %u\n", m_rxData.spiData[0], m_rxData.spiData[1], m_rxData.spiData[2], m_rxData.spiData[3]);
						continue;	// return 0;
					}
				}
			}
			else
			{
				if (m_rxData.transferSize != recvSize)
				{
					CLog::Log(CLog::ERRORS, "transferSize != recvSize %lu %u %u\n", m_rxData.transferSize, recvSize, itry);
					CLog::Log(CLog::ERRORS, "%u %u %u %u\n", m_rxData.spiData[0], m_rxData.spiData[1], m_rxData.spiData[2], m_rxData.spiData[3]);
					continue; 	//	return 0;
				}
			}
			memcpy(recvMsg, m_rxData.spiData, m_rxData.transferSize);
			return m_rxData.transferSize;
		}
		else
		{
			CLog::Log(CLog::ERRORS, "no receive message specified retry %u\n", itry);
			return 0;	// No receive message specified
		}
	}
	return 0;	// error return
}


unsigned long C2630::Cmd(S2630Msg::ECommand command)
{
	S2630Msg::UHeader sendHdr;

	sendHdr.shortHdr.msgType = S2630Msg::SHORTMSG;
	sendHdr.shortHdr.command = command;
	sendHdr.shortHdr.numPackets = 0;
	sendHdr.shortHdr.body = 0;

	S2630Msg recvMsg;
	size_t numBytesRead = WriteRead(&sendHdr, sizeof(S2630Msg::UHeader), &recvMsg, sizeof(S2630Msg::UHeader));

	if (numBytesRead >= sizeof(S2630Msg::UHeader))
	{
		if (recvMsg.hdr.shortHdr.command == command)
		{
			return 1;
		}
	}
	return 0;
}


unsigned long C2630::GetImmediate(S2630Msg::ECommand command, unsigned char& value)
{
	return SetImmediate(command, 0, value);
}


unsigned long C2630::SetImmediate(S2630Msg::ECommand command, unsigned char setValue, unsigned char& getValue)
{
	S2630Msg::UHeader sendHdr;

	sendHdr.shortHdr.msgType = S2630Msg::SHORTMSG;
	sendHdr.shortHdr.command = command;
	sendHdr.shortHdr.numPackets = 0;
	sendHdr.shortHdr.body = setValue;

	S2630Msg recvMsg;
	size_t numBytesRead = WriteRead(&sendHdr, sizeof(S2630Msg::UHeader), &recvMsg, sizeof(S2630Msg::UHeader));

	if (numBytesRead >= sizeof(S2630Msg::UHeader))
	{
		if (recvMsg.hdr.shortHdr.command == command)
		{
			if (recvMsg.hdr.shortHdr.numPackets == 0)
			{
				getValue = recvMsg.hdr.shortHdr.body;
				return 1;
			}
		}
	}
	return 0;
}


unsigned long C2630::GetImmediate(S2630Msg::ECommand command, unsigned char& value0, unsigned char& value1)
{
	return SetImmediate(command, 0, 0, value0, value1);
}


unsigned long C2630::SetImmediate(S2630Msg::ECommand command, unsigned char setValue0, unsigned char setValue1, unsigned char& getValue0, unsigned char& getValue1)
{
	S2630Msg::UHeader sendHdr;

	sendHdr.shortHdr16.msgType = S2630Msg::SHORT16MSG;
	sendHdr.shortHdr16.command = command;
	sendHdr.shortHdr16.body0 = setValue0;
	sendHdr.shortHdr16.body1 = setValue1;

	S2630Msg recvMsg;
	size_t numBytesRead = WriteRead(&sendHdr, sizeof(S2630Msg::UHeader), &recvMsg, sizeof(S2630Msg::UHeader));

	if (numBytesRead >= sizeof(S2630Msg::UHeader))
	{
		if (recvMsg.hdr.shortHdr16.command == command)
		{
			getValue0 = recvMsg.hdr.shortHdr16.body0;
			getValue1 = recvMsg.hdr.shortHdr16.body1;
			return 2;
		}
	}
	return 0;
}


#ifdef TEST2630API
void C2630::testApi(void)
{
	// TEST CODE
	unsigned char antennaSetting;
	GetAntenna(antennaSetting);
	TRACE("GetAntenna %u\n", antennaSetting);
	unsigned char attenuationSetting;
	GetAttenuation(attenuationSetting);
	TRACE("GetAttenuation %u\n", attenuationSetting);
	unsigned char bandwidth;
	GetBandWidth(bandwidth);
	TRACE("GetBandWidth %u\n", bandwidth);
	unsigned long fwVersion;
	GetFirmwareVersion(fwVersion);
	TRACE("GetFirmwareVersion %lu\n", fwVersion);
	double frequency;
	GetFrequency(frequency);
	TRACE("GetFrequency %f\n", frequency);
	unsigned long ifrequency;
	GetFrequency(ifrequency);
	TRACE("GetIntegerFrequency %ld\n", ifrequency);
	unsigned long mocfreq;
	unsigned char mocpresel, mocbw;
	GetMOC(mocfreq, mocpresel, mocbw);
	TRACE("GetMOC %lu %u %u\n", mocfreq, mocpresel, mocbw);
	unsigned long code;
	GetOcxoDacCode(code);
	TRACE("GetOcxoDacCode %lu\n", code);
	unsigned long interval;
	GetOcxoInterval(interval);
	TRACE("GetOcxoInterval %lu\n", interval);
	unsigned char loopStatus;
	GetOcxoLoopStatus(loopStatus);
	TRACE("GetOcxoLoopStatus %u\n", loopStatus);
	unsigned char preselector;
	GetPreselector(preselector);
	TRACE("GetPreselector %u\n", preselector);
	signed char temperature;
	GetRxTemperature(temperature);
	TRACE("GetRxTemperature %d\n", temperature);

	unsigned char newAnt = 1;
	unsigned char setAnt;
	SetAntenna(newAnt, setAnt);
	TRACE("SetAntenna to %u %u\n", newAnt, setAnt);
	SetAntenna(antennaSetting, setAnt);
	TRACE("SetAntenna to %u %u\n", antennaSetting, setAnt);
	unsigned char newAtten = 43;
	unsigned char att;
	SetAttenuation(newAtten, att);
	TRACE("SetAttenuation to %u %u\n", newAtten, att);
	SetAttenuation(attenuationSetting, att);
	TRACE("SetAttenuation to %u %u\n", attenuationSetting, att);

	unsigned char newBw;
	if (bandwidth == 10)
	{
		newBw = 80;
	}
	else // (bandwidth == 80)
	{
		newBw = 10;
	}
	unsigned char bw;
	SetBandWidth(newBw, bw);
	TRACE("SetBandWidth to %u %u\n", newBw, bw);
	SetBandWidth(bandwidth, bw);
	TRACE("SetBandWidth to %u %u\n", bandwidth, bw);

	unsigned long oldFreq = frequency;
	unsigned long newFreq(50);
	unsigned long freq;
	SetFrequency(newFreq, freq);
	TRACE("SetFrequency to %lu %lu\n", newFreq, freq);
	newFreq = 1000;
	SetFrequency(newFreq, freq);
	TRACE("SetFrequency to %lu %lu\n", newFreq, freq);
	SetFrequency(oldFreq, freq);
	TRACE("SetFrequency to %lu %lu\n", oldFreq, freq);

	unsigned char newPresel = preselector + 1;
	unsigned char presel;
	SetMOC(newFreq, newPresel, newBw, newAnt, freq, presel, bw, setAnt);
	TRACE("SetMOC to %lu %u %u %u %lu %u %u %u\n", newFreq, newPresel, newBw, newAnt, freq, presel, bw, setAnt);
	SetMOC(oldFreq, preselector, bandwidth, antennaSetting, freq, presel, bw, setAnt);
	TRACE("SetMOC to %lu %u %u %u %lu %u %u %u\n", oldFreq, preselector, bandwidth, antennaSetting, freq, presel, bw, setAnt);

	TRACE("TestApi done\n");
	// END OF TEST CODE
}
#endif


bool C2630::GetRxCapabilitiesStatic(const S2630Msg::SRxCapabilities** rxcapabilities)
{
	auto driverDesc = m_driverDesc;
	if (driverDesc == -1)
	{
		driverDesc = open("/dev/spidev32766.0", O_RDWR);
		m_driverDesc = driverDesc;
	}
	if (driverDesc == -1)
	{
		printf("Failed to open /dev/spidev32766.0\n");
		return false;
	}
	auto driverDesc1 = m_driverDesc1;
	if (driverDesc1 == -1)
	{
		driverDesc1 = open("/dev/TciSpi", O_RDONLY);
		m_driverDesc1 = driverDesc1;
	}
	if (driverDesc1 == -1)
	{
		printf("Failed to open /dev/TciSpi\n");
		close(driverDesc);
		return false;
	}

	S2630Msg::UHeader sendHdr;
	sendHdr.shortHdr.msgType = S2630Msg::SHORTMSG;
	sendHdr.shortHdr.command = S2630Msg::ECommand::GET_RX_CAPABILITIES;
	sendHdr.shortHdr.numPackets = 0;
	sendHdr.shortHdr.body = 0;

//	unsigned long sendData = (sendHdr.msgType << 24) + (sendHdr.command << 16) + (sendHdr.numPackets << 8) + sendHdr.body;
//	unsigned long item = ByteSwap(sendData);
	unsigned long item = ByteSwap(sendHdr.header);
	write(driverDesc, &item, 4);

	TCISPI_RXDATA rxData;
	if (ioctl(driverDesc1, IOCTL_GetRxData, &rxData) == -1)
	{
		printf("Failed to read rx capabilities - ioctl failed\n");
		close(driverDesc1);
		close(driverDesc);
		return false;
	}
	if (rxData.transferSize != sizeof(S2630Msg::UHeader) + sizeof(S2630Msg::SRxCapabilities))
	{
		printf("Failed to read rx capabilities - transferSize != recvSize %lu %u\n", rxData.transferSize, sizeof(S2630Msg::UHeader) + sizeof(S2630Msg::SRxCapabilities));
		close(driverDesc1);
		close(driverDesc);
		return false;
	}
	auto recvMsg = reinterpret_cast<const S2630Msg*>(rxData.spiData);
	if (recvMsg->hdr.shortHdr.msgType != S2630Msg::SHORTMSG || recvMsg->hdr.shortHdr.command != sendHdr.shortHdr.command || recvMsg->hdr.shortHdr.numPackets * 4 != sizeof(S2630Msg::SRxCapabilities))
	{
		printf("Failed to read rx capabilities\n");
		close(driverDesc1);
		close(driverDesc);
		return false;
	}

	auto rxCapabilities = reinterpret_cast<const S2630Msg::SRxCapabilities*>(recvMsg->body);
	*rxcapabilities = rxCapabilities;
	return true;

}

unsigned long C2630::GetrxProcessorStatic()
{
	//const S2630Msg::SRxCapabilities** rxCapabilities = new (const S2630Msg::SRxCapabilities*);
	std::shared_ptr<const S2630Msg::SRxCapabilities *> rxCapabilities = std::make_shared<const S2630Msg::SRxCapabilities *> ();
	if (GetRxCapabilitiesStatic(rxCapabilities.get()))
	{
		return (*rxCapabilities)->rxProcessor;
	}
	else
		return 0;
}

unsigned long C2630::GetFirmwareVersionStatic()
{
	//const S2630Msg::SRxCapabilities** rxCapabilities = new (const S2630Msg::SRxCapabilities*);
	std::shared_ptr<const S2630Msg::SRxCapabilities *> rxCapabilities = std::make_shared<const S2630Msg::SRxCapabilities *> ();
	if (GetRxCapabilitiesStatic(rxCapabilities.get()))
	{
		return (*rxCapabilities)->firmwareVersion;
	}
	else
		return 0;
}

bool C2630::GetMax10UniqueIdStatic(unsigned long(&id)[2])
{
	//const S2630Msg::SRxCapabilities** rxCapabilities = new (const S2630Msg::SRxCapabilities*);
	std::shared_ptr<const S2630Msg::SRxCapabilities*> rxCapabilities = std::make_shared<const S2630Msg::SRxCapabilities*>();
	if (GetRxCapabilitiesStatic(rxCapabilities.get()))
	{
		id[0] = (*rxCapabilities)->max10UniqueIdLow;
		id[1] = (*rxCapabilities)->max10UniqueIdHigh;
	}
	else
		return false;

	//close(driverDesc1);
	//close(driverDesc);
	return true;
}

