/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/
// CsmsLicenseSign.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <fstream>
#include <string>
#include <ctime>
#include "CsmsLicense.h"
#include "CsmsLicenseSign.h"

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc < 2)
	{
		printf("Usage: CsmsLicenseSign.exe <filename>\n");
		return 0;
	}
	char buf[256];
	if (sprintf_s(buf, "%ls", argv[1]) <= 0)
	{
		printf("Usage: CsmsLicenseSign.exe <filename>\n");
		return 0;
	}

	CCsmsLicenseSign sign(buf);
	return 0;
}


#ifndef __GNUG__
const char* CCsmsLicense::MEMERR = "memory allocation error %d";
#endif
CCsmsLicenseSign::CCsmsLicenseSign(const char* fnm)
{
	std::ifstream file(fnm);
	if (!file)
	{
		char errbuf[256];
		if (strerror_s(errbuf, errno) == 0)
		{
			printf("Failed to open file %s\n", errbuf);
		}
		else
		{
			printf("Failed to open file\n");
		}
		return;
	}

	std::string line;

	// Read contents of license file up to security code
	std::string secCode;
	std::string secData;
	std::streamoff secCodeOffset = 0;
	std::streamoff offset = 0;
	while (!std::getline(file, line).eof())
	{
		line.erase(line.find_last_not_of(" \t\n\r") + 1);
		if (line.find("SecurityCode", 0, 12) == 0)
		{
			secCodeOffset = offset;
			secCode = line.substr(13);
		}
		else if (line.find("SignDate", 0, 8) == std::string::npos)
		{
			secData += line;
		}
		// Get the offset to the next line in the file
		offset = file.tellg();
	}
//	printf("%s\n%s\n", secCode.c_str(), secData.c_str());
	if (secCode.empty())
	{
		printf("Missing SecurityCode in file\n");
		return;
	}
	secData += CCsmsLicense::MEMERR;

	file.close();

	std::ofstream wfile(fnm, std::ios_base::in | std::ios_base::out);

	// Hash the file+private_key
	std::string stringbuf;
	CCsmsLicense::HashToString(secData, stringbuf);
//	printf("%s\n", stringbuf.c_str());

	// Seek to the security code position
	wfile.seekp(secCodeOffset);

	// write the security line
	line = "SecurityCode=" + stringbuf + "\n";
	wfile.write(line.c_str(), line.length());

	// write the current date/time
	auto t = std::time(nullptr);
	struct tm ttm;
	char tstr[20] = {"0000/00/00 00:00:00"};
	if (gmtime_s(&ttm, &t) == 0)
	{
		std::strftime(tstr, sizeof(tstr), "%Y/%m/%d %H:%M:%S", &ttm);
	}

	line = "SignDate=" + std::string(tstr) + "\n";
	wfile.write(line.c_str(), line.length());

	wfile.close();

	return;
}



