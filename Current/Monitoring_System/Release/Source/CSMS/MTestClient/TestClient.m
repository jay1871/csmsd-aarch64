function varargout = TestClient(varargin)
% TESTCLIENT MATLAB code for TestClient.fig
%      TESTCLIENT, by itself, creates a new TESTCLIENT or raises the existing
%      singleton*.
%
%      H = TESTCLIENT returns the handle to a new TESTCLIENT or the handle to
%      the existing singleton*.
%
%      TESTCLIENT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TESTCLIENT.M with the given input arguments.
%
%      TESTCLIENT('Property','Value',...) creates a new TESTCLIENT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TestClient_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TestClient_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TestClient

% Last Modified by GUIDE v2.5 29-May-2015 17:35:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TestClient_OpeningFcn, ...
                   'gui_OutputFcn',  @TestClient_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TestClient is made visible.
function TestClient_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TestClient (see VARARGIN)

% Choose default command line output for TestClient
set(handles.figure1,'Name','CSMS Test Client')
handles.output = hObject;

set(handles.togglebutton1,'Value',0);
set(handles.togglebutton1,'String', 'Set Range');
set(handles.togglebutton1,'Value',0);
set(handles.togglebutton2,'String', 'Set Range');
handles.Chart1.start=0;
handles.Chart1.end=1;
handles.Chart1.Visible='false';
handles.Chart1.fStart= 0.0;
handles.Chart1.fEnd= 1.0;
handles.Chart2.start=0.0;
handles.Chart2.end=1.0;
handles.Chart2.Visible='false';
handles.Chart2.fStart= 0.0;
handles.Chart2.fEnd= 1.0;
handles.IPAddress = '10.16.108.176';
handles.DataSet1 = [];
handles.DataSet2 = [];
handles.Fc= 50e6;
handles.Stop= 368.64e6/2;
handles.Start= 0.0;
handles.Fs= 368.64e6;
handles.Reference_Level =0;
handles.dB_per_div=10;
handles.FullScale=65536;
set(handles.edit1,'String',handles.IPAddress);
set(handles.edit2,'String',handles.Fc);
set(handles.popupmenu1,'Value',1);
%handles.pushbutton2.Visible='off';
%handles.pushbutton3.Visible='off';
handles.pushbutton5.Visible='off';
handles.pushbutton6.Visible='off';
handles.pushbutton7.Visible='off';

set(hObject,'HandleVisibility','Callback');

% This sets up the initial plot - only do when we are invisible
% so window can get raised using Signature_Demo.
%if strcmp(get(hObject,'Visible'),'off')
%    handles.popupmenu11='plot trace';
%end
hPushbutton5=findobj(hObject,'Tag','pushbutton5');
hPushbutton6=findobj(hObject,'Tag','pushbutton6');
hPushbutton7=findobj(hObject,'Tag','pushbutton6');

set(hObject,'CloseRequestFcn',@closefig);   % Stops & deletes timers if figure is closed

% Timer code.  This pulls the data from the remote unit and plot it
% Create a timer that will handle the fetching and plotting of the data
if 1
timer_data=struct('handle1',hPushbutton5, 'handle2',hPushbutton6,'handle3',hPushbutton7,...
    'handle4',eventdata,'handle5',handles);

hTime = timer('Name',[mfilename,'Timer'],...    % Give it a name that coresponds to the file name
	'ExecutionMode','fixedSpacing',...          % Make the plot update on a fixed Rate
	'Period',0.1,...                            % Update the plot 10 times a second
    'StartDelay',0.1,...                        % Delay the start to allow finish creating graphics
	'StopFcn',@stoptimer,...                    % On Stop closes the figure and clear timer
	'TimerFcn',{@timerfunction},...             % The real work of the plot
	'ErrorFcn',@timererror,...                  % The real work of the plot
	'BusyMode','drop',...
    'UserData',timer_data);                     % Keep a handle to the plot to be updated.


% Store a handle to the timer in the figure, so that it can be deleted
handles.hTimer = hTime;
end
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TestClient wait for user response (see UIRESUME)
% uiwait(handles.figure1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If you need to do something once when the plot stops put it here
%
function stoptimer(hTime,varargin)
%udata = get(hTime,'UserData');
%hPushbutton=udata.handle1;
%hFigure=get(hPushbutton,'Parent');
%handles=guidata(hFigure);
%delete(hFigure);
%delete(hTime);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Each time the timer fires the display update routine is called
function timerfunction(hTime,varargin)

udata = get(hTime,'UserData');
hObject1=udata.handle1;
hObject2=udata.handle2;
hObject3=udata.handle3;
eventdata=udata.handle4;
handles=guidata(hObject1);
pushbutton5_Callback(hObject1, eventdata, handles);
handles=guidata(hObject1);
pushbutton6_Callback(hObject2, eventdata, handles);
pushbutton7_Callback(hObject3, eventdata, handles);
%drawnow;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% An error happend when executing a callback.  We need to clean up and try
% to fix the problem or at least but MATLAB in a state where the user can
% restart the plot
function timererror(varargin)

% Get a handle to the figuer and close it.
hFig = findobj('Name',mfilename);
if ~isempty(hFig)
	close(hFig);
end;
disp(['error', varargin{:}]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure is closing so stop the timer and let the timer close the figure
% window when it is called.  This is needed in this order so that the timer
% does not try to updata the plot after the figure is closed.
%
function closefig(hFig,varargin)
try
    handles = guidata(hFig);
    stop(handles.hTimer);
    hTime=handles.hTimer;
    delete(hFig);
    delete(hTime);    
catch
    closereq;   
end


% --- Outputs from this function are returned to the command line.
function varargout = TestClient_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%Alternatively use the following for reading image files of your own choosing

if(get(handles.togglebutton3,'Value') == 1)
    helpdlg('Please Stop Remote Data Updates First');
    return;
end

[filename, pathname] = uigetfile({'*.txt', 'A Text File (*.txt)'; ...
        '*.*',                   'All Files (*.*)'}, ...
        'Pick a Sample Data file');
%       
% %This code checks if the user pressed cancel on the dialog.
if isequal(filename,0) || isequal(pathname,0)
       disp('User pressed cancel')
       DataSet1= handles.DataSet1; 
else
    disp(['User selected ', fullfile(pathname, filename)])
 
%    DataSet1= dlmread(fullfile(pathname, filename),'\0');

    fid=fopen(fullfile(pathname, filename),'r');
    Count=0;
    nElem =0;
    if( handles.Chart1.start ==0 && handles.Chart1.start ==0)
       handles.Chart1.end =100000;       
    end
    
    try
      while (~feof(fid)) 
           Count= Count +1;
           line = fgetl(fid);
           if(Count > handles.Chart1.start && Count < handles.Chart1.end)
                nElem = nElem+1;
                DataSet1(nElem) = shex2dec(line(1,1:length(line)));
            end
        end
    catch err
      rethrow(err);
    end
    fclose(fid);
end

popup_sel_index = get(handles.popupmenu1, 'Value');

switch popup_sel_index        
    case 1
        plot(handles.axes1,DataSet1);
        dualcursor('on',[],[],[],handles.axes1);
        handles.Chart1.Visible= 'true';
    case 2
        WindowLength=length(DataSet1);
        WindowArray=window(@blackman,length(DataSet1))';
        WindowAmplitudeCorrection=WindowLength/sum(WindowArray);
        DataSet1=DataSet1.*WindowArray*WindowAmplitudeCorrection;
         
        NFFT = 2^nextpow2(length(DataSet1)); % Next power of 2 from length of y
        spectrum=20*log10(abs(fftshift(fft(DataSet1))));
        freq= handles.Fs/2*linspace(0,1,NFFT/2);
        plot(handles.axes1,freq,spectrum(NFFT/2+1:NFFT));
        dualcursor('on',[],[],[],handles.axes1);
        handles.Chart1.Visible= 'true';
end
    
% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if(get(handles.togglebutton3,'Value') == 1)
    helpdlg('Please Stop Remote Data Updates First');
    return;
end

[filename, pathname] = uigetfile({'*.txt', 'A Text File (*.txt)'; ...
        '*.*',                   'All Files (*.*)'}, ...
        'Pick a Sample Data file');
%       
% %This code checks if the user pressed cancel on the dialog.
if isequal(filename,0) || isequal(pathname,0)
       disp('User pressed cancel')
       DataSet2= handles.DataSet2; 
else
    disp(['User selected ', fullfile(pathname, filename)])
 
    fid=fopen(fullfile(pathname, filename),'r');
    Count=0;
    nElem =0;
    if( handles.Chart2.start ==0 && handles.Chart2.end ==0)
       handles.Chart2.end =100000;       
    end
    
    try
       while (~feof(fid)) 
            Count= Count +1;
            line = fgetl(fid);
            if(Count > handles.Chart2.start && Count < handles.Chart2.end)
                nElem = nElem+1;
                DataSet2(nElem) = shex2dec(line(1,1:length(line)));
            end
        end
    catch err
      rethrow(err);
    end
    fclose(fid);
end

popup_sel_index = get(handles.popupmenu2, 'Value');

switch popup_sel_index

    case 1
        plot(handles.axes2,DataSet2);
        dualcursor('on',[],[],[],handles.axes2);
        handles.Chart2.Visible = 'true';
    case 2 
        WindowLength=length(DataSet2);
        WindowArray=window(@blackman,length(DataSet2))';
        WindowAmplitudeCorrection=WindowLength/sum(WindowArray);
        DataSet2=DataSet2.*WindowArray*WindowAmplitudeCorrection;

        NFFT = 2^nextpow2(length(DataSet2)); % Next power of 2 from length of y
        spectrum=20*log10(abs(fftshift(fft(DataSet2))));
        freq= handles.Fs/2*linspace(0,1,NFFT/2);
        plot(handles.axes2,freq,spectrum(NFFT/2+1:NFFT));
        dualcursor('on',[],[],[],handles.axes2);
        handles.Chart2.Visible= 'true';
end    
   
% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton1
val = dualcursor(handles.axes1);
if(get(hObject,'Value') == 1)
    set(handles.togglebutton1,'String', 'Clear Range');
%    if (strcmp(handles.Chart2.Visible, 'true'))
    popup_sel_index = get(handles.popupmenu1, 'Value'); 
    switch popup_sel_index        
        case 1
           xl = xlim(handles.axes1);
           lim = localObjboundsCopy(handles.axes1);  % Problem using objbounds with hggroup objects, so I have a simple version of my own
           lim = lim(1:2);         %x values only
           xl(isinf(xl)) = lim(isinf(xl));
           width = diff(xl);     %Axis width
           if(handles.Chart1.start ==0)               
                handles.Chart1.start = val(1,1)/width;
           else
               handles.Chart1.start = handles.Chart2.start*val(1,1)/width;
           end
           handles.Chart1.end = val(1,3)/width;
        case 2
            
    end
else
    set(handles.togglebutton1,'String', 'Set Range');
        handles.Chart1.start = 0;
        handles.Chart1.end = 1;
end
guidata(hObject, handles);

% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton2
val = dualcursor(handles.axes2);
if(get(hObject,'Value') == 1)
    set(handles.togglebutton2,'String', 'Clear Range');
%    if (strcmp(handles.Chart2.Visible, 'true'))
    popup_sel_index = get(handles.popupmenu2, 'Value'); 
    switch popup_sel_index        
        case 1
           xl = xlim(handles.axes2);
           lim = localObjboundsCopy(handles.axes2);  % Problem using objbounds with hggroup objects, so I have a simple version of my own
           lim = lim(1:2);         %x values only
           xl(isinf(xl)) = lim(isinf(xl));
           width = diff(xl);     %Axis width
           if(handles.Chart2.start ==0)               
                handles.Chart2.start = val(1,1)/width;
           else
               handles.Chart2.start = handles.Chart2.start*val(1,1)/width;
           end
           handles.Chart2.end = val(1,3)/width;
        case 2
            
    end
else
    set(handles.togglebutton2,'String', 'Set Range');
        handles.Chart2.start = 0;
        handles.Chart2.end = 1;
end
guidata(hObject, handles);

% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function ExitMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to ExitMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

closefig(handles.figure1);

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double

handles.IPAddress = get(hObject,'String');
% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

import java.net.*;
import java.io.*;

t = Socket(handles.IPAddress,5025);
out = BufferedWriter(OutputStreamWriter(t.getOutputStream()));
in = BufferedReader(InputStreamReader(t.getInputStream()));
%str = sprintf('*idn?%c',10);
%out.write(str);
%out.flush();
%idnResponse = in.readLine(); % Reads the id string
str= sprintf('TRACE%c',10);
out.write(str);
out.flush();
idnResponse = char(in.readLine()); % Reads the id string
ch1StartIdx = 4;
ch2StartIdx= 7+8192*4;
try
    for idx = 1:8192
     handles.DataSet1(idx) = shex2dec(idnResponse(1,ch1StartIdx+4*(idx-1):ch1StartIdx+4*(idx-1)+3));
     handles.DataSet2(idx) = shex2dec(idnResponse(1,ch2StartIdx+4*(idx-1):ch2StartIdx+4*(idx-1)+3));
    end
catch err
    rethrow(err);
end
  
guidata(hObject, handles);

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double

handles.Fc = get(hObject,'String');
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in togglebutton3.
function togglebutton3_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton3

if(get(hObject,'Value') == 1)
    %Tell the timer to go and do its work.
    set(hObject,'String', 'Stop');
    start(handles.hTimer);
else 
    %Tell the timer to stop its updates
    set(hObject,'String', 'Upadte');
    stop(handles.hTimer);
end
guidata(hObject, handles);

% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

popup_sel_index = get(handles.popupmenu1, 'Value');

switch popup_sel_index        
    case 1
        plot(handles.axes1,handles.DataSet1);
        xlabel(handles.axes1,'Sample #');
        ylabel(handles.axes1,'|Raw Ampitude 0n 16 bit Signed Scale|');
        dualcursor('on',[[.05 -.08;.8 -.08]],[],[],handles.axes1);
        handles.Chart1.Visible= 'true';
    case 2
        NFFT = 2^nextpow2(length(handles.DataSet1)); % Next power of 2 from length of y
        WindowArray=window(@ blackmanharris,NFFT)';
        WindowAmplitudeCorrection=NFFT/sum(WindowArray);
        DataSet1=handles.DataSet1.*WindowArray*WindowAmplitudeCorrection;
        
        spectrum=20*log10(2*abs(fftshift(fft(DataSet1)/length(handles.DataSet1))));
        spectrum = spectrum - 20*log10(handles.FullScale/WindowAmplitudeCorrection);
        freq= handles.Fs/2*linspace(0,1,NFFT/2);
        plot(handles.axes1,freq,spectrum(NFFT/2+1:NFFT));
        axis(handles.axes1,[handles.Stop*handles.Chart1.fStart,handles.Stop*handles.Chart1.fEnd,handles.Reference_Level-handles.dB_per_div*14,handles.Reference_Level]);
        xlabel(handles.axes1,'Frequency (MHz)');
        ylabel(handles.axes1,'Amplitude (dBFS)');
        legend(handles.axes1,'IF Path Spectrum');
        dualcursor('on',[[.05 -.08;.8 -.08]],[],[],handles.axes1);
        grid (handles.axes1, 'on');
        handles.Chart1.Visible= 'true';     

end
    
% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

popup_sel_index = get(handles.popupmenu2, 'Value');

switch popup_sel_index

    case 1
        plot(handles.axes2,handles.DataSet2);
        xlabel(handles.axes2,'Sample #');
        ylabel(handles.axes2,'|Raw Ampitude 0n 16 bit Signed Scale|');
        dualcursor('on',[[.05 -.08;.8 -.08]],[],[],handles.axes2);
        handles.Chart2.Visible = 'true';
    case 2
        NFFT = 2^nextpow2(length(handles.DataSet2)); % Next power of 2 from length of y
        WindowArray=window(@ blackmanharris,NFFT)';
        WindowAmplitudeCorrection=NFFT/sum(WindowArray);
        DataSet2=handles.DataSet2.*WindowArray*WindowAmplitudeCorrection;

        spectrum=20*log10(abs(2.0*fftshift(fft(DataSet2)/length(handles.DataSet2))));
        spectrum = spectrum - 20*log10(handles.FullScale/WindowAmplitudeCorrection);
        freq= handles.Fs/2*linspace(0,1,NFFT/2);
        plot(handles.axes2,freq,spectrum(NFFT/2+1:NFFT));
        axis(handles.axes2,[handles.Stop*handles.Chart2.fStart,handles.Stop*handles.Chart2.fEnd,handles.Reference_Level-handles.dB_per_div*14,handles.Reference_Level]);
        xlabel(handles.axes2,'Frequency (MHz)');
        ylabel(handles.axes2,'Amplitude (dBFS)');
        legend(handles.axes2,'Direct Conversion Path Spectrum; 10 dB Amp, 9 KHz-85 MHz LPF');
        dualcursor('on',[[.05 -.08;.8 -.08]],[],[],handles.axes2);
        handles.Chart2.Visible= 'true';
        grid (handles.axes2, 'on');
end   

function lim = localObjboundsCopy(axh)
% Get x limits of all data in axes axh
kids = get(axh,'Children');
xmin = Inf; xmax = -Inf;
ymin = Inf; ymax = -Inf;
for ii=1:length(kids)
    try % Pass through if can't get data.  hopefully we hit at least one
        xd = get(kids(ii),'XData');
        xmin = min([xmin min(xd(:))]);
        xmax = max([xmax max(xd(:))]);

        yd = get(kids(ii),'YData');
        ymin = min([ymin min(yd(:))]);
        ymax = max([ymax max(yd(:))]);
    end
end
% Nuclear option, in case things went really bad
xmin(xmin==Inf) = 0;  xmax(xmax==-Inf) = 1;
ymin(ymin==Inf) = 0;  ymax(ymax==-Inf) = 1;

lim = [xmin xmax ymin ymax];


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

handles.Chart1.fStart = get(hObject,'Value');
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

if(get(hObject,'Value') > handles.Chart1.fStart)
     handles.Chart1.fEnd = get(hObject,'Value');
     % Update handles structure
    guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

handles.Chart2.fStart = get(hObject,'Value');
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider4_Callback(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

if(get(hObject,'Value') > handles.Chart2.fStart)
    handles.Chart2.fEnd = get(hObject,'Value');
    % Update handles structure
    guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
