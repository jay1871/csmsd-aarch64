/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2017 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include <assert.h>
#include <algorithm>
#include "TciNeon.h"
#include "vdtMath.h"

namespace TciNeon
{
// Real float vector
ne10_float32_t* Ne10F32Vec::ne10sMalloc(int size)
{
	if(ne10_float32_t* buffer = static_cast<ne10_float32_t*>(malloc(size * sizeof(ne10_float32_t))))
	{
		return buffer;
	}
	throw std::bad_alloc();
}

ne10_float32_t* Ne10F32Vec::Allocate(int size)
{
	return size == 0 ? nullptr : ne10sMalloc(size);
}

void Ne10F32Vec::Deallocate(ne10_float32_t* vector)
{
	free(vector);
}

Ne10F32Vec::Ne10F32Vec(void) :
	m_reserved(0),
	m_size(0),
	m_vector(nullptr)
{
}

Ne10F32Vec::Ne10F32Vec(int size) :
	m_reserved(size),
	m_size(size),
	m_vector(Allocate(size))
{
}

Ne10F32Vec::Ne10F32Vec(int size, ne10_float32_t val) :
	m_reserved(size),
	m_size(size),
	m_vector(Allocate(size))
{
	if (m_size)
	{
		ne10sSet(val, m_vector, m_size);
	}
}

Ne10F32Vec::Ne10F32Vec(const ne10_float32_t* v, int size) :
	m_reserved(size),
	m_size(size),
	m_vector(Allocate(size))
{
	if (m_size)
	{
		ne10sCopy(v, m_vector, m_size);
	}
}

Ne10F32Vec::Ne10F32Vec(const ne10_int16_t* v, int size) :
	m_reserved(size),
	m_size(size),
	m_vector(Allocate(size))
{
	if (m_size)
	{
		ne10sConvert(v, m_vector, m_size, 0);
	}
}

Ne10F32Vec::Ne10F32Vec(const Ne10F32Vec& v) :
	m_reserved(v.m_size),
	m_size(v.m_size),
	m_vector(Allocate(v.m_size))
{
	if (m_size)
	{
		ne10sCopy(v.m_vector, m_vector, m_size);
	}
}

Ne10F32Vec::~Ne10F32Vec(void)
{
	Deallocate(m_vector);
}

Ne10F32Vec& Ne10F32Vec::operator=(const Ne10F32Vec& v)
{
	if (&v != this)
	{
		m_size = v.m_size;

		if(m_size > m_reserved)
		{
			Deallocate(m_vector);
			m_reserved = m_size;
			m_vector = Allocate(m_reserved);
		}

		if (m_size != 0)
		{
			ne10sCopy(v.m_vector, m_vector, m_size);
		}
	}

	return *this;
}

Ne10F32Vec& Ne10F32Vec::operator=(Ne10F32Vec&& v)
{
	if (&v != this)
	{
		Deallocate(m_vector);
		m_reserved = v.m_reserved;
		m_size = v.m_size;
		m_vector = v.m_vector;
		v.m_reserved = 0;
		v.m_size = 0;
		v.m_vector = nullptr;
	}

	return *this;
}

Ne10F32Vec& Ne10F32Vec::operator=(ne10_float32_t val)
{
	if (m_size > 0)
	{
		ne10sSet(val, m_vector, m_size);
	}

	return *this;
}

ne10_float32_t& Ne10F32Vec::operator[](int i)
{
	assert(i < m_size);
	return m_vector[i];
}

const ne10_float32_t& Ne10F32Vec::operator[](int i) const
{
	assert(i < m_size);
	return m_vector[i];
}

Ne10F32Vec& Ne10F32Vec::operator+=(const Ne10F32Vec& v)
{
	assert(v.m_size == m_size);
	ne10sAdd(v.m_vector, m_vector, m_size);
	return *this;
}

Ne10F32Vec& Ne10F32Vec::operator+=(ne10_float32_t val)
{
	ne10sAddC(val, m_vector, m_size);
	return *this;
}

Ne10F32Vec& Ne10F32Vec::operator*=(const Ne10F32Vec& v)
{
	assert(v.m_size == m_size);
	ne10sMul(v.m_vector, m_vector, m_size);
	return *this;
}

Ne10F32Vec& Ne10F32Vec::operator*=(ne10_float32_t val)
{
	ne10sMulC(val, m_vector, m_size);
	return *this;
}

Ne10F32Vec& Ne10F32Vec::operator-=(const Ne10F32Vec& v)
{
	assert(v.m_size == m_size);
	ne10sSub(v.m_vector, m_vector, m_size);
	return *this;
}

Ne10F32Vec& Ne10F32Vec::operator-=(ne10_float32_t val)
{
	ne10sSubC(val, m_vector, m_size);
	return *this;
}

Ne10F32Vec& Ne10F32Vec::operator/=(const Ne10F32Vec& v)
{
	assert(v.m_size == m_size);
	ne10sDiv(v.m_vector, m_vector, m_size);
	return *this;
}

Ne10F32Vec& Ne10F32Vec::operator/=(ne10_float32_t val)
{
	ne10sDivC(val, m_vector, m_size);
	return *this;
}

Ne10F32Vec& Ne10F32Vec::operator<<=(int rot)
{
	if (m_size > 0)
	{
		rot %= m_size;

		if (rot > int(m_size / 2))
		{
			*this >>= -rot;
		}
		else if(rot != 0)
		{
			reserve(m_size + rot);
			ne10sCopy(m_vector, &m_vector[m_size], rot);
			ne10sMove(&m_vector[rot], m_vector, m_size);
		}
	}

	return *this;
}

Ne10F32Vec& Ne10F32Vec::operator>>=(int rot)
{
	if (m_size > 0)
	{
		rot %= m_size;

		if (rot > int(m_size / 2))
		{
			*this <<= -rot;
		}
		else if (rot != 0)
		{
			reserve(m_size + rot);
			ne10sMove(m_vector, &m_vector[rot], m_size);
			ne10sCopy(&m_vector[m_size], m_vector, rot);
		}
	}

	return *this;
}

void Ne10F32Vec::assign(int size, ne10_float32_t val)
{
	if(size > int(m_reserved))
	{
		Deallocate(m_vector);
		m_reserved = size;
		m_vector = Allocate(m_reserved);
	}
	m_size = size;
	if(m_size > 0)
	{
		ne10sSet(val, m_vector, m_size);
	}
	return;
}

void Ne10F32Vec::assign(const ne10_float32_t* v, int size)
{
	if(size > int(m_reserved))
	{
		Deallocate(m_vector);
		m_reserved = size;
		m_vector = Allocate(m_reserved);
	}
	m_size = size;
	if(m_size > 0)
	{
		ne10sCopy(v, m_vector, m_size);
	}
	return;
}

ne10_float32_t* Ne10F32Vec::data(void)
{
	return m_vector;
}
const ne10_float32_t* Ne10F32Vec::data(void) const
{
	return m_vector;
}

ne10_float32_t Ne10F32Vec::maximum(void) const
{
	ne10_float32_t max;
	ne10sMax(m_vector, m_size, &max);
	return max;
}

ne10_float32_t Ne10F32Vec::mean(void) const
{
	ne10_float32_t mean;
	ne10sMean(m_vector, m_size, &mean);
	return mean;
}

ne10_float32_t Ne10F32Vec::minimum(void) const
{
	ne10_float32_t min;
	ne10sMin(m_vector, m_size, &min);
	return min;
}

void Ne10F32Vec::reserve(int size)
{
	if(size > int(m_reserved))
	{
		ne10_float32_t* newVector = Allocate(size);

		if(m_size != 0)
		{
			ne10sCopy(m_vector, newVector, m_size);
		}

		Deallocate(m_vector);
		m_reserved = size;
		m_vector = newVector;
	}
}

void Ne10F32Vec::resize(int size)
{
	if(size > int(m_reserved))
	{
		reserve(size);
	}

	m_size = size;
}

size_t Ne10F32Vec::size(void) const
{
	return m_size;
}

ne10_float32_t Ne10F32Vec::sum(void) const
{
	ne10_float32_t sum;
	ne10sSum(m_vector, m_size, &sum);
	return sum;
}
// End of float vector class Ne10F32Vec

// Complex float vector
ne10_fft_cpx_float32_t* Ne10F32cVec::ne10sMallocCmplx(int size)
{
	if(ne10_fft_cpx_float32_t* buffer = static_cast<ne10_fft_cpx_float32_t*>(malloc(size * sizeof(ne10_fft_cpx_float32_t))))
	{
		return buffer;
	}
	throw std::bad_alloc();
}
ne10_fft_cpx_float32_t* Ne10F32cVec::Allocate(int size) { return size == 0 ? nullptr : ne10sMallocCmplx(size); }
void Ne10F32cVec::Deallocate(ne10_fft_cpx_float32_t* vector) { free(vector); }

// Constructors
Ne10F32cVec::Ne10F32cVec(void) :
	m_reserved(0),
	m_size(0),
	m_vector(nullptr)
{
}

Ne10F32cVec::Ne10F32cVec(int size) :
	m_reserved(size),
	m_size(size),
	m_vector(Allocate(size))
{
}

Ne10F32cVec::Ne10F32cVec(int size, const ne10_fft_cpx_float32_t& val) :
	m_reserved(size),
	m_size(size),
	m_vector(Allocate(size))
{
	if (m_size)
	{
		assert(m_vector);
		ne10sSet(val, m_vector, m_size);
	}
}

Ne10F32cVec::Ne10F32cVec(const ne10_fft_cpx_float32_t* v, int size) :
	m_reserved(size),
	m_size(size),
	m_vector(Allocate(size))
{
	if (m_size)
	{
		assert(m_vector);
		ne10sCopy(v, m_vector, m_size);
	}
}

Ne10F32cVec::Ne10F32cVec(const ne10_fft_cpx_int16_t* v, int size) :
	m_reserved(size),
	m_size(size),
	m_vector(Allocate(size))
{
	if (m_size)
	{
		assert(m_vector);
		ne10sConvert(v, m_vector, m_size, 0);
	}
}

Ne10F32cVec::Ne10F32cVec(const Ne10F32cVec& v) :
	m_reserved(v.m_size),
	m_size(v.m_size),
	m_vector(Allocate(v.m_size))
{
	if (m_size)
	{
		assert(m_vector);
		ne10sCopy(v.m_vector, m_vector, m_size);
	}
}

Ne10F32cVec::Ne10F32cVec(const Ne10F32cVec2& v) :
	m_reserved(v.m_size),
	m_size(v.m_size),
	m_vector(Allocate(v.m_size))
{
	if (m_size)
	{
		assert(m_vector);
		auto vImgOffset = v.imgOffset();
		for (size_t i = 0; i < m_size; ++i)
		{
			m_vector[i].r = v.m_vector[i];
			m_vector[i].i = v.m_vector[vImgOffset + i];
		}
	}
}

Ne10F32cVec::Ne10F32cVec(const ne10_float32_t* real, const ne10_float32_t* imag, int size) :
	m_reserved(size),
	m_size(size),
	m_vector(Allocate(size))
{
	if (m_size)
	{
		assert(m_vector);
		ne10sRealToCplx(real, imag, m_vector, m_size);
	}
}

Ne10F32cVec::~Ne10F32cVec(void)
{
	Deallocate(m_vector);
}

Ne10F32cVec& Ne10F32cVec::operator=(const Ne10F32cVec& v)
{
	if (&v != this)
	{
		m_size = v.m_size;

		if(m_size > m_reserved)
		{
			Deallocate(m_vector);
			m_reserved = m_size;
			m_vector = Allocate(m_reserved);
		}

		if (m_size != 0)
		{
			ne10sCopy(v.m_vector, m_vector, m_size);
		}
	}

	return *this;
}

Ne10F32cVec& Ne10F32cVec::operator=(Ne10F32cVec&& v)
{
	if (&v != this)
	{
		Deallocate(m_vector);
		m_reserved = v.m_reserved;
		m_size = v.m_size;
		m_vector = v.m_vector;
		v.m_reserved = 0;
		v.m_size = 0;
		v.m_vector = nullptr;
	}

	return *this;
}

Ne10F32cVec& Ne10F32cVec::operator=(const ne10_fft_cpx_float32_t& val)
{
	if (m_size > 0)
	{
		ne10sSet(val, m_vector, m_size);
	}

	return *this;
}

ne10_fft_cpx_float32_t& Ne10F32cVec::operator[](int i)
{
	assert(i < m_size);
	return m_vector[i];
}

Ne10F32cVec& Ne10F32cVec::operator+=(const Ne10F32cVec& v)
{
	assert(v.m_size == m_size);
	ne10sAdd(v.m_vector, m_vector, m_size);
	return *this;
}

Ne10F32cVec& Ne10F32cVec::operator*=(const Ne10F32cVec& v)
{
	assert(v.m_size == m_size);
	ne10sMul(v.m_vector, m_vector, m_size);
	return *this;
}

Ne10F32cVec& Ne10F32cVec::operator*=(ne10_float32_t val)
{
	ne10sMulC(val, m_vector, m_size);
	return *this;
}

Ne10F32cVec& Ne10F32cVec::operator/=(const Ne10F32cVec& v)
{
	assert(v.m_size == m_size);
	ne10sDiv(v.m_vector, m_vector, m_size);
	return *this;
}

Ne10F32cVec& Ne10F32cVec::operator/=(const Ne10F32Vec& v)
{
	assert(v.m_size == m_size);
	ne10sDiv(v.m_vector, m_vector, m_size);
	return *this;
}

Ne10F32cVec& Ne10F32cVec::operator/=(ne10_float32_t val)
{
	ne10sDivC(val, m_vector, m_size);
	return *this;
}

Ne10F32cVec& Ne10F32cVec::operator<<=(int rot)
{
	if (m_size > 0)
	{
		if (rot < 0)
		{
			rot += m_size;
		}
		rot %= m_size;

		if (rot > int(m_size / 2))
		{
			*this >>= -rot;
		}
		else if(rot != 0)
		{
			reserve(m_size + rot);
			ne10sCopy(m_vector, &m_vector[m_size], rot);
			ne10sMove(&m_vector[rot], m_vector, m_size);
		}
	}

	return *this;
}

Ne10F32cVec& Ne10F32cVec::operator>>=(int rot)
{
	if (m_size > 0)
	{
		if (rot < 0)
		{
			rot += m_size;
		}
		rot %= m_size;

		if (rot > int(m_size / 2))
		{
			*this <<= -rot;
		}
		else if (rot != 0)
		{
			reserve(m_size + rot);
			ne10sMove(m_vector, &m_vector[rot], m_size);
			ne10sCopy(&m_vector[m_size], m_vector, rot);
		}
	}

	return *this;
}

const ne10_fft_cpx_float32_t& Ne10F32cVec::operator[](int i) const
{
	assert(i < m_size);
	return m_vector[i];
}

void Ne10F32cVec::assign(int size, const ne10_fft_cpx_float32_t& val)
{
	if(size > int(m_reserved))
	{
		Deallocate(m_vector);
		m_reserved = size;
		m_vector = Allocate(m_reserved);
	}
	m_size = size;
	if(m_size > 0)
	{
		ne10sSet(val, m_vector, m_size);
	}
	return;
}

void Ne10F32cVec::assign(const ne10_fft_cpx_float32_t* v, int size)
{
	if(size > int(m_reserved))
	{
		Deallocate(m_vector);
		m_reserved = size;
		m_vector = Allocate(m_reserved);
	}
	m_size = size;
	if(m_size > 0)
	{
		ne10sCopy(v, m_vector, m_size);
	}
	return;
}

ne10_fft_cpx_float32_t* Ne10F32cVec::data(void)
{
	return m_vector;
}
const ne10_fft_cpx_float32_t* Ne10F32cVec::data(void) const
{
	return m_vector;
}

void Ne10F32cVec::reserve(int size)
{
	if(size > int(m_reserved))
	{
		ne10_fft_cpx_float32_t* newVector = Allocate(size);

		if (m_size != 0)
		{
			ne10sCopy(m_vector, newVector, m_size);
		}

		Deallocate(m_vector);
		m_reserved = size;
		m_vector = newVector;
	}
}

void Ne10F32cVec::resize(int size)
{
	if(size > int(m_reserved))
	{
		reserve(size);
	}

	m_size = size;
}

size_t Ne10F32cVec::size(void) const
{
	return m_size;
}

ne10_fft_cpx_float32_t Ne10F32cVec::sum(void) const
{
	ne10_fft_cpx_float32_t sum;
	ne10sSum(m_vector, m_size, &sum);
	return sum;
}
// End of complex float vector class Ne10F32cVec

// Complex float vector type 2
ne10_float32_t* Ne10F32cVec2::ne10sMallocCmplx2(int size)
{
	if(ne10_float32_t* buffer = static_cast<ne10_float32_t*>(malloc(2 * size * sizeof(ne10_float32_t))))
	{
		return buffer;
	}
	throw std::bad_alloc();
}

ne10_float32_t* Ne10F32cVec2::Allocate(int size) { return size == 0 ? nullptr : ne10sMallocCmplx2(size); }
void Ne10F32cVec2::Deallocate(ne10_float32_t* vector) { free(vector); }

// Constructors
Ne10F32cVec2::Ne10F32cVec2(void) :
	m_imgOffset(0),
	m_reserved(0),
	m_size(0),
	m_vector(nullptr)
{
}

Ne10F32cVec2::Ne10F32cVec2(const Ne10F32cVec2& v) :
	m_imgOffset(v.m_imgOffset),
	m_reserved(v.m_size),
	m_size(v.m_size),
	m_vector(Allocate(v.m_size))
{
	if (m_size)
	{
		assert(m_vector);
		//Copy real part.
		ne10sCopy(v.m_vector, m_vector, m_size);
		//copy imaginary part.
		ne10sCopy(&(v.m_vector[v.m_imgOffset]), &m_vector[m_imgOffset], m_size);
	}
}

Ne10F32cVec2::~Ne10F32cVec2(void)
{
	Deallocate(m_vector);
}


Ne10F32cVec2& Ne10F32cVec2::operator*=(const Ne10F32cVec2& v)
{
	assert(v.m_size == m_size);
	if (!ne10sVerifyArgs("ne10sMul", v.m_vector, m_vector, 2 * m_size)) return *this;

	for (int i = 0; i < int(m_size); ++i)
	{
		ne10_float32_t temp = m_vector[i] * v.m_vector[i] - m_vector[m_imgOffset + i] * v.m_vector[v.m_imgOffset + i];
		m_vector[m_imgOffset + i] = m_vector[i] * v.m_vector[v.m_imgOffset + i] + m_vector[m_imgOffset + i] * v.m_vector[i];
		m_vector[i] = temp;
	}

	return *this;
}

Ne10F32cVec2& Ne10F32cVec2::operator*=(ne10_float32_t val)
{
	if(m_imgOffset == m_size)
	{
		ne10sMulC(val, m_vector, 2 * m_size);
	}
	else
	{
		//Multiply real part.
		ne10sMulC(val, m_vector, m_size);
		//Multiply imaginary part.
		ne10sMulC(val, &m_vector[m_imgOffset], m_size);

	}
	return *this;
}

Ne10F32cVec2& Ne10F32cVec2::operator/=(const Ne10F32Vec& v)
{
	if(v.m_size != m_size || m_size != m_imgOffset)
	{
		printf("Ne10F32cVec2::operator/= error v_size %u, size %u, img %u",
				v.m_size, m_size, m_imgOffset);
		///@note: m_size must equal m_imgOffset in order for ne10sDiv to work correctly.
		assert(false);
		//assert(v.m_size == m_size && m_size == m_imgOffset);
	}

	//Divide real part.
	ne10sDiv(v.m_vector, m_vector, 2 * m_size);
	return *this;
}

Ne10F32cVec2& Ne10F32cVec2::operator/=(const Ne10F32cVec2& v)
{
	assert(v.m_size == m_size);
	if (!ne10sVerifyArgs("ne10sDiv", v.m_vector, m_vector, 2 * m_size)) return *this;

	// First, in-place conjugate product
	for (int i = 0; i < int(m_size); ++i)
	{
		auto temp = (m_vector[i] * v.m_vector[i] + m_vector[m_imgOffset + i] * v.m_vector[v.m_imgOffset + i]);
		m_vector[m_imgOffset + i] = (m_vector[m_imgOffset + i] * v.m_vector[i] - m_vector[i] * v.m_vector[v.m_imgOffset + i]);
		m_vector[i] = temp;
	}
	// Second, divide by square
	for (int i = 0; i < int(m_size); ++i)
	{
		auto denom = v.m_vector[i] * v.m_vector[i] + v.m_vector[v.m_imgOffset + i] * v.m_vector[v.m_imgOffset + i];
		// ? handle 0 denom??
		m_vector[i] /= denom;
		m_vector[m_imgOffset + i] /= denom;
	}

	return *this;
}


void Ne10F32cVec2::assign(const ne10_float32_t* v, int size)
{
	if(size > int(m_reserved))
	{
		Deallocate(m_vector);
		m_reserved = size;
		m_vector = Allocate(m_reserved);
	}

	m_imgOffset = m_size = size;

	if(size > 0)
	{
		ne10sCopy(v, m_vector, 2 * size);
	}

	return;
}

ne10_float32_t* Ne10F32cVec2::dataReal(void)
{
	return m_vector;
}

ne10_float32_t* Ne10F32cVec2::dataImag(void)
{
	return m_vector + m_imgOffset;
}

const ne10_float32_t* Ne10F32cVec2::dataReal(void) const
{
	return m_vector;
}

const ne10_float32_t* Ne10F32cVec2::dataImag(void) const
{
	return m_vector + m_imgOffset;
}

ne10_float32_t& Ne10F32cVec2::real(int i)
{
	assert(i < m_size);
	return m_vector[i];
}

ne10_float32_t& Ne10F32cVec2::imag(int i)
{
	assert(i < m_size);
	return m_vector[m_imgOffset + i];
}

const ne10_float32_t& Ne10F32cVec2::real(int i) const
{
	assert(i < m_size);
	return m_vector[i];
}

const ne10_float32_t& Ne10F32cVec2::imag(int i) const
{
	assert(i < m_size);
	return m_vector[m_imgOffset + i];
}


void Ne10F32cVec2::reserve(int size, bool preservedContent)
{
	if(size_t(size) > m_reserved)
	{
		ne10_float32_t* newVector = Allocate(size);

		if (preservedContent)
		{
			//Copy real part.
			ne10sCopy(m_vector, newVector, m_size);
			assert(m_size <= m_imgOffset && m_imgOffset < size);
			//Copy imag part.
			ne10sCopy(&m_vector[m_imgOffset], &newVector[size], m_size);
		}

		m_imgOffset = size;
		Deallocate(m_vector);
		m_reserved = size;
		m_vector = newVector;
	}

	if(!preservedContent)
	{
		//If content is not preserved, the imaginary data will be at the 2nd
		// half of the array.
		m_size  = m_imgOffset = size;
	}
}

void Ne10F32cVec2::resize(int size)
{
	if(size > int(m_reserved))
	{
		reserve(size, true);
	}

	if(m_imgOffset == 0 || size == 0)
	{
		m_imgOffset = size;
	}
	else if(size_t(size) > m_imgOffset)
	{
		printf("Ne10F32cVec2::resize error, size = %u, offset %u\n", size, m_imgOffset);
		assert(false);
	}

	m_size = size;
}

size_t Ne10F32cVec2::size(void) const
{
	return m_size;
}

// End of complex float vector type 2 class Ne10F32cVec2

ne10_fft_cpx_float32_t& operator+=(ne10_fft_cpx_float32_t& a, const ne10_fft_cpx_float32_t& b)
{
	a.r += b.r;
	a.i += b.i;
	return a;
}

ne10_fft_cpx_float32_t operator-(const ne10_fft_cpx_float32_t& a, const ne10_fft_cpx_float32_t& b)
{
	ne10_fft_cpx_float32_t subtract = { a.r - b.r, a.i - b.i };
	return subtract;
}

ne10_fft_cpx_float32_t operator*(const ne10_fft_cpx_float32_t& a, ne10_fft_cpx_float32_t b)
{
	ne10_fft_cpx_float32_t mult = { a.r * b.r - a.i * b.i, a.r * b.i + a.i * b.r };
	return mult;
}

ne10_fft_cpx_float32_t operator*(const ne10_fft_cpx_float32_t& a, float b)
{
	ne10_fft_cpx_float32_t mult = { a.r * b, a.i * b };
	return mult;
}

ne10_fft_cpx_float32_t operator/(const ne10_fft_cpx_float32_t& a, float b)
{
	ne10_fft_cpx_float32_t div = { a.r / b, a.i / b };
	return div;
}

ne10_fft_cpx_float32_t operator/(const ne10_fft_cpx_float32_t& a, const ne10_fft_cpx_float32_t& b)
{
	ne10_fft_cpx_float32_t div;
	auto quot = b.r * b.r + b.i * b.i;
	div.r = (a.r * b.r + a.i * b.i) / quot;
	div.i = (a.i * b.r - a.r * b.i) / quot;
	return div;
}

ne10_fft_cpx_float32_t MultByConj(const ne10_fft_cpx_float32_t& a, const ne10_fft_cpx_float32_t& b)
{
	ne10_fft_cpx_float32_t mult = { a.r * b.r + a.i * b.i, a.i * b.r - a.r * b.i };
	return mult;
}

void ne10sAbs(ne10_float32_t* pSrcDst, int len)
{
	for (int i = 0; i < len; ++i)
	{
		pSrcDst[i] = fabsf(pSrcDst[i]);
	}
	return;
}

void ne10sAdd(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pSrcDst, int len)
{
	if (!ne10sVerifyArgs("ne10sAdd", pSrc, pSrcDst, len)) return;

	for (int i = 0; i < len; ++i)
	{
		pSrcDst[i] += pSrc[i];
	}
	return;
}

void ne10sAddC(ne10_float32_t val, ne10_float32_t* pSrcDst, int len)
{
	for (int i = 0; i < len; ++i)
	{
		pSrcDst[i] += val;
	}
	return;
}

void ne10sAdd(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pSrcDst, int len)
{
	if (!ne10sVerifyArgs("ne10sAdd", pSrc, pSrcDst, len)) return;

	auto srcDst = reinterpret_cast<ne10_float32_t*>(pSrcDst);
	auto src = reinterpret_cast<const ne10_float32_t*>(pSrc);
	for (int i = 0; i < 2 * len; ++i)
	{
		srcDst[i] += src[i];
	}
	return;
}

void ne10sAutoCorr(const ne10_fft_cpx_float32_t* __restrict__ pSrc, int srcLen, ne10_fft_cpx_float32_t* __restrict__ pDst, int dstLen, int lowLag)
{
	if (!ne10sVerifyArgs("ne10sAutoCorr", pSrc, pDst, srcLen, dstLen)) return;

	for (int n = 0; n < dstLen; ++n)
	{
		double tempReal = 0;
		double tempImag = 0;

		int i = std::max(0, -(n + lowLag));
		int j = n + i + lowLag;
		for (; i < srcLen && j < srcLen; ++i, ++j)
		{
			tempReal += pSrc[i].r * pSrc[j].r + pSrc[i].i * pSrc[j].i;
			tempImag += pSrc[i].r * pSrc[j].i - pSrc[i].i * pSrc[j].r;
		}
		pDst[n].r = static_cast<ne10_float32_t>(tempReal);
		pDst[n].i = static_cast<ne10_float32_t>(tempImag);
	}
	return;
}

void ne10sCartToPolar(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDstMagn, ne10_float32_t* __restrict__ pDstPhase, int len)
{
	if (!ne10sVerifyArgs("ne10sCartToPolar", pSrc, pDstMagn, pDstPhase, len)) return;

	ne10sMagnitude(pSrc, pDstMagn, len);
	ne10sPhase(pSrc, pDstPhase, len);
	return;
}

void ne10sConj(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pDst, int len)
{
	if (!ne10sVerifyArgs("ne10sConj", pSrc, pDst, len)) return;

	for (int i = 0; i < len; ++i)
	{
		pDst[i].r = pSrc[i].r;
		pDst[i].i = -pSrc[i].i;
	}
	return;
}

void ne10sConvert(const ne10_int16_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len, int scaleFactor)
{
	if (!ne10sVerifyArgs("ne10sConvert", pSrc, pDst, len)) return;

	if (scaleFactor == 0)
	{
		for (int i = 0; i < len; ++i)
		{
			pDst[i] = static_cast<ne10_float32_t>(pSrc[i]);
		}
	}
	else
	{
		float factor = static_cast<float>(1 << scaleFactor);
		for (int i = 0; i < len; ++i)
		{
			pDst[i] = static_cast<ne10_float32_t>(pSrc[i]) / factor;
		}
	}
	return;
}

void ne10sConvert(const ne10_float32_t* __restrict__ pSrc, ne10_int32_t* __restrict__ pDst, int len, ne10_int32_t low, ne10_int32_t high)
{
	// Rounds and clamps the signed output data
	for (int i = 0; i < len; ++i)
	{
		pDst[i] = static_cast<int>(pSrc[i] + (pSrc[i] > 0 ? 0.5f : -0.5f));
		pDst[i] = (pDst[i] < low ? low : (pDst[i] > high ? high : pDst[i]));
	}
	return;
}

void ne10sConvert(const ne10_int32_t* __restrict__ pSrc, ne10_int8_t* __restrict__ pDst, int len)
{
	for (int i = 0; i < len; ++i)
	{
		pDst[i] = static_cast<ne10_int8_t>(pSrc[i]);
	}
	return;
}

void ne10sConvert(const ne10_float32_t* __restrict__ pSrc, ne10_uint8_t* __restrict__ pDst, int len, int scaleFactor)
{
	if (!ne10sVerifyArgs("ne10sConvert", pSrc, pDst, len)) return;

	// Note: assumes data is all positive. Rounds.
	if (scaleFactor == 0)
	{
		for (int i = 0; i < len; ++i)
		{
			pDst[i] = static_cast<ne10_uint8_t>(pSrc[i] + 0.5f);
		}
	}
	else
	{
		float factor = static_cast<float>(1 << scaleFactor);
		for (int i = 0; i < len; ++i)
		{
			pDst[i] = static_cast<ne10_uint8_t>((pSrc[i] + 0.5f) * factor);
		}
	}

	return;
}
void ne10sConvert(const ne10_fft_cpx_int16_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pDst, int len, int scaleFactor)
{
	auto src = reinterpret_cast<const ne10_int16_t*>(pSrc);
	auto dst = reinterpret_cast<ne10_float32_t*>(pDst);
	ne10sConvert(src, dst, 2 * len, scaleFactor);
	return;
}

void ne10sCrossCorr(const ne10_fft_cpx_float32_t* __restrict__ pSrc1, int src1Len, const ne10_fft_cpx_float32_t* __restrict__ pSrc2, int src2Len,
	ne10_fft_cpx_float32_t* __restrict__ pDst, int dstLen, int lowLag)
{
	if (!ne10sVerifyArgs("ne10sCrossCorr", pSrc1, pSrc2, pDst, src1Len, src2Len, dstLen)) return;

	for (int n = 0; n < dstLen; ++n)
	{
		double tempReal = 0;
		double tempImag = 0;

		int i = std::max(0, -(n + lowLag));
		int j = n + i + lowLag;
		for (; i < src1Len && j < src2Len; ++i, ++j)
		{
			tempReal += pSrc1[i].r * pSrc2[j].r + pSrc1[i].i * pSrc2[j].i;
			tempImag += pSrc1[i].r * pSrc2[j].i - pSrc1[i].i * pSrc2[j].r;
		}
		pDst[n].r = static_cast<ne10_float32_t>(tempReal);
		pDst[n].i = static_cast<ne10_float32_t>(tempImag);
	}
	return;
}

void ne10sCopy(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len)
{
	if (!ne10sVerifyArgs("ne10sCopy", pSrc, pDst, len)) return;

	for (int i = 0; i < len; ++i)
	{
		pDst[i] = pSrc[i];
	}
	return;
}

void ne10sCopy(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pDst, int len)
{
	if (!ne10sVerifyArgs("ne10sCopy", pSrc, pDst, len)) return;

	for (int i = 0; i < len; ++i)
	{
		pDst[i] = pSrc[i];
	}
	return;
}

void ne10sCopyUncached(const unsigned long* __restrict__ pSrc, unsigned long* __restrict__ pDst, int len)
{
	if (!ne10sVerifyArgs("ne10sCopyUncached", pSrc, pDst, len)) return;

	int len2 = len & 0xfffffff0;	// truncate to be a multiple of 16
	if (len2 > 0)
	{
		int sz = len2 * sizeof(float);
		asm volatile
		(
			"1: \n"
			"vldm %[pSrc]!, {d0-d7}\n"
			"vstm %[pDst]!, {d0-d7}\n"
			"subs %[sz], %[sz], #0x40\n"
			"bgt 1b\n"
			: [pDst]"+r"(pDst), [pSrc]"+r"(pSrc), [sz]"+r"(sz) : : "d0", "d1", "d2", "d3", "d4", "d5", "d6", "d7", "cc", "memory"
		);
	}
	if ((len & 0x0f) == 0) return;
	for (int i = 0; i < (len & 0x0f); ++i)
	{
		pDst[i] = pSrc[i];
	}
	return;
}

void ne10sCopyUncached(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len)
{
	if (!ne10sVerifyArgs("ne10sCopyUncached", pSrc, pDst, len)) return;

	int len2 = len & 0xfffffff0;	// truncate to be a multiple of 16
	if (len2 > 0)
	{
		int sz = len2 * sizeof(float);
		asm volatile
		(
			"1: \n"
			"vldm %[pSrc]!, {d0-d7}\n"
			"vstm %[pDst]!, {d0-d7}\n"
			"subs %[sz], %[sz], #0x40\n"
			"bgt 1b\n"
			: [pDst]"+r"(pDst), [pSrc]"+r"(pSrc), [sz]"+r"(sz) : : "d0", "d1", "d2", "d3", "d4", "d5", "d6", "d7", "cc", "memory"
		);
	}
	if ((len & 0x0f) == 0) return;
	for (int i = 0; i < (len & 0x0f); ++i)
	{
		pDst[i] = pSrc[i];
	}
	return;
}

void ne10sCopyUncached(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pDst, int len)
{
	if (!ne10sVerifyArgs("ne10sCopyUncached", pSrc, pDst, len)) return;

	int len2 = len & 0xfffffff8;	// truncate to be a multiple of 8
	if (len2 > 0)
	{
		int sz = len2 * sizeof(ne10_fft_cpx_float32_t);
		asm volatile
		(
			"1: \n"
			"vldm %[pSrc]!, {d0-d7}\n"
			"vstm %[pDst]!, {d0-d7}\n"
			"subs %[sz], %[sz], #0x40\n"
			"bgt 1b\n"
			: [pDst]"+r"(pDst), [pSrc]"+r"(pSrc), [sz]"+r"(sz) : : "d0", "d1", "d2", "d3", "d4", "d5", "d6", "d7", "cc", "memory"
		);
	}
	if ((len & 0x07) == 0) return;
	for (int i = 0; i < (len & 0x07); ++i)
	{
		pDst[i] = pSrc[i];
	}
	return;
}

void ne10sDeriv31(const ne10_fft_cpx_float32_t* __restrict__ pSrc2, ne10_fft_cpx_float32_t* __restrict__ pDp)
{
	static const ne10_float32_t pSrc1[] = // 0.4 bandwidth differentiating filter
	{
		 0.0000162263f, -0.0000743415f,  0.0002114815f, -0.0004933145f,
		 0.0010110274f, -0.0018882593f,  0.0032861079f, -0.0054119059f,
		 0.0085386709f, -0.0130516783f,  0.0195638868f, -0.0292225688f,
		 0.0446508354f, -0.0737358728f,  0.1561588101f,
//		 0.0000000000f,
//		-0.1561588101f,  0.0737358728f, -0.0446508354f,  0.0292225688f,
//		-0.0195638868f,  0.0130516783f, -0.0085386709f,  0.0054119059f,
//		-0.0032861079f,  0.0018882593f, -0.0010110274f,  0.0004933145f,
//		-0.0002114815f,  0.0000743415f, -0.0000162263f
	};
	static const int len = 31;
	static const int len2 = 15;

	if (!ne10sVerifyArgs("ne10sDeriv31", pSrc2, len, pDp)) return;

	double tempr = 0;
	double tempi = 0;

	for (int i = 0; i < len2; ++i)
	{
		tempr += pSrc1[i] * (pSrc2[i].r - pSrc2[len-1-i].r);
	}
	for (int i = 0; i < len2; ++i)
	{
		tempi += pSrc1[i] * (pSrc2[i].i - pSrc2[len-1-i].i);
	}

	pDp->r = static_cast<float>(tempr);
	pDp->i = static_cast<float>(tempi);

	return;
}

void ne10sDiv(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pSrcDst, int len)
{
	if (!ne10sVerifyArgs("ne10sDiv", pSrc, pSrcDst, len)) return;

	// First, in-place conjugate product
	for (int i = 0; i < len; ++i)
	{
		auto temp = (pSrcDst[i].r * pSrc[i].r + pSrcDst[i].i * pSrc[i].i);
		pSrcDst[i].i = (pSrcDst[i].i * pSrc[i].r - pSrcDst[i].r * pSrc[i].i);
		pSrcDst[i].r = temp;
	}
	// Second, divide by square
	for (int i = 0; i < len; ++i)
	{
		auto denom = pSrc[i].r * pSrc[i].r + pSrc[i].i * pSrc[i].i;
		// ? handle 0 denom??
		pSrcDst[i].r /= denom;
		pSrcDst[i].i /= denom;
	}
	return;
}

void ne10sDiv(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pSrcDst, int len, ne10_float32_t val)
{
	if (!ne10sVerifyArgs("ne10sDiv", pSrc, pSrcDst, len)) return;

	// First, in-place conjugate product
	for (int i = 0; i < len; ++i)
	{
		auto temp = (pSrcDst[i].r * pSrc[i].r + pSrcDst[i].i * pSrc[i].i);
		pSrcDst[i].i = (pSrcDst[i].i * pSrc[i].r - pSrcDst[i].r * pSrc[i].i);
		pSrcDst[i].r = temp;
	}
	// Second, divide by square
	float valsq = val * val;
	for (int i = 0; i < len; ++i)
	{
		auto denom = pSrc[i].r * pSrc[i].r + pSrc[i].i * pSrc[i].i;
		if (denom < valsq)
			denom = valsq;
		pSrcDst[i].r /= denom;
		pSrcDst[i].i /= denom;
	}
	return;
}

void ne10sDiv(const ne10_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pSrcDst, int len)
{
	if (!ne10sVerifyArgs("ne10sDiv", pSrcDst, pSrc, len)) return;

	for (int i = 0; i < len; ++i)
	{
		// ? handle 0 denom??
		pSrcDst[i].r /= pSrc[i];
		pSrcDst[i].i /= pSrc[i];
	}
	return;
}

void ne10sDivC(ne10_float32_t val, ne10_float32_t* pSrcDst, int len)
{
	for (int i = 0; i < len; ++i)
	{
		// ? handle 0 denom??
		pSrcDst[i] /= val;
	}
	return;
}

void ne10sDiv(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pSrcDst, int len)
{
	if (!ne10sVerifyArgs("ne10sDiv", pSrc, pSrcDst, len)) return;

	for (int i = 0; i < len; ++i)
	{
		// ? handle 0 denom??
		pSrcDst[i] /= pSrc[i];
	}
	return;
}

void ne10sDivC(ne10_float32_t val, ne10_fft_cpx_float32_t* pSrcDst, int len)
{
	auto srcDst = reinterpret_cast<ne10_float32_t*>(pSrcDst);
	for (int i = 0; i < 2 * len; ++i)
	{
		// ? handle 0 denom??
		srcDst[i] /= val;
	}
	return;
}

void ne10sDotProd(const ne10_float32_t* __restrict__ pSrc1, const ne10_float32_t* __restrict__ pSrc2, int len, ne10_float32_t* __restrict__ pDp)
{
	if (pSrc1 != pSrc2)
	{
		if (!ne10sVerifyArgs("ne10sDotProd", pSrc1, pSrc2, len, pDp)) return;
	}

	double temp = 0;
	for (int i = 0; i < len; ++i)
	{
		temp += pSrc1[i] * pSrc2[i];
	}
	*pDp = static_cast<float>(temp);
	return;
}

void ne10sDotProd(const ne10_float32_t* __restrict__ pSrc1, const ne10_fft_cpx_float32_t* __restrict__ pSrc2, int len, ne10_fft_cpx_float32_t* __restrict__ pDp)
{
	if (!ne10sVerifyArgs("ne10sDotProd", pSrc1, pSrc2, len, pDp)) return;

	double tempr = 0;
	double tempi = 0;
	for (int i = 0; i < len; ++i)
	{
		tempr += pSrc1[i] * pSrc2[i].r;
		tempi += pSrc1[i] * pSrc2[i].i;
	}
	pDp->r = static_cast<float>(tempr);
	pDp->i = static_cast<float>(tempi);
	return;
}

void ne10sDotProd(const ne10_fft_cpx_float32_t* __restrict__ pSrc1, const ne10_fft_cpx_float32_t* __restrict__ pSrc2, int len, ne10_fft_cpx_float32_t* __restrict__ pDp)
{
	if (pSrc1 != pSrc2)
	{
		if (!ne10sVerifyArgs("ne10sDotProd", pSrc1, pSrc2, len, pDp)) return;
	}

	double tempr = 0;
	double tempi = 0;
	for (int i = 0; i < len; ++i)
	{
		tempr += pSrc1[i].r * pSrc2[i].r - pSrc1[i].i * pSrc2[i].i;
		tempi += pSrc1[i].r * pSrc2[i].i + pSrc1[i].i * pSrc2[i].r;
	}
	pDp->r = static_cast<float>(tempr);
	pDp->i = static_cast<float>(tempi);
	return;
}

void ne10sImag(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDstIm, int len)
{
	if (!ne10sVerifyArgs("ne10sImag", pSrc, pDstIm, len)) return;

	for (int i = 0; i < len; ++i)
	{
		pDstIm[i] = pSrc[i].i;
	}
	return;
}

void ne10sLn(ne10_float32_t* pSrcDst, int len)
{
	for (int i = 0; i < len; ++i)
	{
		pSrcDst[i] = vdt::fast_logf(pSrcDst[i]);
//		pSrcDst[i] = logf(pSrcDst[i]);
	}
	return;
}

void ne10sLn(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len)
{
	if (!ne10sVerifyArgs("ne10sLn", pSrc, pDst, len)) return;

	for (int i = 0; i < len; ++i)
	{
		pDst[i] = vdt::fast_logf(pSrc[i]);		// Almost twice as fast!
//		pDst[i] = logf(pSrc[i]);
	}
	return;
}

void ne10sLog10(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len)
{
	static const ne10_float32_t FLOG10_E = 0.434294482f; // log10(e)
	if (!ne10sVerifyArgs("ne10sLog10", pSrc, pDst, len)) return;

	ne10sLn(pSrc, pDst, len);
	ne10sMulC(FLOG10_E, pDst, len);
	return;
}

void ne10sMagnitude(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len)
{
	if (!ne10sVerifyArgs("ne10sMagnitude", pSrc, pDst, len)) return;

	for (int i = 0; i < len; ++i)
	{
		pDst[i] = pSrc[i].r * pSrc[i].r + pSrc[i].i * pSrc[i].i;
	}
	for (int i = 0; i < len; ++i)
	{
		pDst[i] = sqrtf(pDst[i]);
	}
	return;
}

void ne10sMagnitudeSqr(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len)
{
	if (!ne10sVerifyArgs("ne10sMagnitudeSqr", pSrc, pDst, len)) return;

	for (int i = 0; i < len; ++i)
	{
		pDst[i] = pSrc[i].r * pSrc[i].r + pSrc[i].i * pSrc[i].i;
	}
	return;
}

void ne10sMaxEvery(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pSrcDst, int len)
{
	if (!ne10sVerifyArgs("ne10sMaxEvery", pSrc, pSrcDst, len)) return;

	for (int i = 0; i < len; ++i)
	{
		pSrcDst[i] = fmaxf(pSrcDst[i], pSrc[i]);
	}
	return;
}

void ne10sMax(const ne10_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pMax)
{
	if (!ne10sVerifyArgs("ne10sMax", pSrc, len, pMax)) return;

	*pMax = -__FLT_MAX__;
	for (int i = 0; i < len; ++i)
	{
		*pMax = fmaxf(*pMax, pSrc[i]);
	}
	return;
}

void ne10sMaxandMaxIndex(const ne10_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pMax, int& maxindex)
{
	if (!ne10sVerifyArgs("ne10sMax", pSrc, len, pMax)) return;

	*pMax = -__FLT_MAX__;
	for (int i = 0; i < len; ++i)
	{
		if (pSrc[i] > *pMax)
		{
			*pMax = pSrc[i];
			maxindex = i;
		}
	}
	return;
}


void ne10sMean(const ne10_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pMean)
{
	if (!ne10sVerifyArgs("ne10sMean", pSrc, len, pMean)) return;

	double temp = 0.;
	for (int i = 0; i < len; ++i)
	{
		temp += pSrc[i];
	}
	*pMean = (len > 0 ? temp / len : 0.f);
	return;
}

void ne10sMin(const ne10_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pMin)
{
	if (!ne10sVerifyArgs("ne10sMin", pSrc, len, pMin)) return;

	*pMin = __FLT_MAX__;
	for (int i = 0; i < len; ++i)
	{
		*pMin = fminf(*pMin, pSrc[i]);
	}
	return;
}

void ne10sMove(const ne10_float32_t* pSrc, ne10_float32_t* pDst, int len)
{
	__builtin_memmove(pDst, pSrc, len * sizeof(ne10_float32_t));
	return;
}

void ne10sMove(const ne10_fft_cpx_float32_t* pSrc, ne10_fft_cpx_float32_t* pDst, int len)
{
	__builtin_memmove(pDst, pSrc, len * sizeof(ne10_fft_cpx_float32_t));
	return;
}

void ne10sMul(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pSrcDst, int len)
{
	if (!ne10sVerifyArgs("ne10sMul", pSrc, pSrcDst, len)) return;

	for (int i = 0; i < len; ++i)
	{
		pSrcDst[i] *= pSrc[i];
	}
	return;
}

void ne10sMulC(ne10_float32_t val, ne10_float32_t* pSrcDst, int len)
{
	for (int i = 0; i < len; ++i)
	{
		pSrcDst[i] *= val;
	}
	return;
}

void ne10sMulC(ne10_float32_t val, const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len)
{
	for (int i = 0; i < len; ++i)
	{
		pDst[i] = pSrc[i] * val;
	}
	return;
}

void ne10sMul(ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pSrcDst, int len)
{
	if (!ne10sVerifyArgs("ne10sMul", pSrc, pSrcDst, len)) return;

	for (int i = 0; i < len; ++i)
	{
		ne10_float32_t temp = pSrcDst[i].r * pSrc[i].r - pSrcDst[i].i * pSrc[i].i;
		pSrcDst[i].i = pSrcDst[i].r * pSrc[i].i + pSrcDst[i].i * pSrc[i].r;
		pSrcDst[i].r = temp;
	}
	return;
}

void ne10sMulC(ne10_float32_t val, ne10_fft_cpx_float32_t* pSrcDst, int len)
{
	auto srcDst = reinterpret_cast<ne10_float32_t*>(pSrcDst);
	for (int i = 0; i < 2 * len; ++i)
	{
		srcDst[i] *= val;
	}
}

void ne10sNthMaxElement(const ne10_float32_t* __restrict__ pSrc, int len, int N, ne10_float32_t* __restrict__ pRes)
{
	if (!ne10sVerifyArgs("ne10sNthMaxElement", pSrc, len, pRes)) return;

	// Find the Nth maximal element of pSrc

	// Method 1: Create a temporary vector on the stack that's a copy of pSrc
//	float temp[len];		// GNU extension - allocates on the stack but can cause stack overflow!
//	__builtin_memcpy(temp, pSrc, len * sizeof(float));
//	std::nth_element(temp, temp + N, temp + len, std::greater<float>());

	// Method 2: Create a temporary vector on the heap that's a copy of pSrc - safer but slower
	std::vector<ne10_float32_t> temp(pSrc, pSrc + len);
	std::nth_element(temp.begin(), temp.begin() + N, temp.end(), std::greater<ne10_float32_t>());
	*pRes = temp[N];
	return;
}

void ne10sNorm_L2(const ne10_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pNorm)
{
	if (!ne10sVerifyArgs("ne10sNorm_L2", pSrc, len, pNorm)) return;

	double temp = 0;
	for (int i = 0; i < len; ++i)
	{
		temp += pSrc[i] * pSrc[i];
	}
	*pNorm = static_cast<float>(sqrt(temp));
	return;
}

void ne10sNorm_L2(const ne10_fft_cpx_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pNorm)
{
	if (!ne10sVerifyArgs("ne10sNorm_L2", pSrc, len, pNorm)) return;

	double temp = 0;
	for (int i = 0; i < len; ++i)
	{
		temp += (pSrc[i].r * pSrc[i].r + pSrc[i].i * pSrc[i].i);
	}
	*pNorm = static_cast<float>(sqrt(temp));
	return;
}

void ne10sPhase(const ne10_fft_cpx_float32_t*__restrict__  pSrc, ne10_float32_t* __restrict__ pDst, int len)
{
	if (!ne10sVerifyArgs("ne10sPhase", pSrc, pDst, len)) return;

	for (int i = 0; i < len; ++i)
	{
		pDst[i] = vdt::fast_atan2f(pSrc[i].i, pSrc[i].r);
	}
	return;
}

void ne10sPolarToCart(const ne10_float32_t* __restrict__ pSrcMagn, const ne10_float32_t* __restrict__ pSrcPhase, ne10_fft_cpx_float32_t* __restrict__ pDst, int len)
{
	if (!ne10sVerifyArgs("ne10sPolarToCart", pDst, pSrcMagn, pSrcPhase, len)) return;

	for (int i = 0; i < len; ++i)
	{
		vdt::fast_sincosf(pSrcPhase[i], pDst[i].i, pDst[i].r);
	}
	for (int i = 0; i < len; ++i)
	{
		pDst[i].r *= pSrcMagn[i];
		pDst[i].i *= pSrcMagn[i];
	}

	return;
}

void ne10sRealToCplx(const ne10_float32_t* __restrict__ pSrcRe, const ne10_float32_t* __restrict__ pSrcIm, ne10_fft_cpx_float32_t* __restrict__ pDst, int len)
{
	if (!ne10sVerifyArgs("ne10sRealToCplx", pDst, pSrcRe, pSrcIm, len)) return;

	for (int i = 0; i < len; ++i)
	{
		pDst[i].r = pSrcRe[i];
		pDst[i].i = pSrcIm[i];
	}
	return;
}

void ne10sRealToCplx(ne10_float32_t val, const ne10_float32_t* __restrict__ pSrcRe, const ne10_float32_t* __restrict__ pSrcIm, ne10_fft_cpx_float32_t* __restrict__ pDst, int len)
{
	if (!ne10sVerifyArgs("ne10sRealToCplx", pDst, pSrcRe, pSrcIm, len)) return;

	for (int i = 0; i < len; ++i)
	{
		pDst[i].r = pSrcRe[i] * val;
		pDst[i].i = pSrcIm[i] * val;
	}
	return;
}

void ne10sSet(ne10_float32_t val, ne10_float32_t* pDst, int len)
{
	for (int i = 0; i < len; ++i)
	{
		pDst[i] = val;
	}
	return;
}

void ne10sSet(ne10_fft_cpx_float32_t val, ne10_fft_cpx_float32_t* pDst, int len)
{
	for (int i = 0; i < len; ++i)
	{
		pDst[i].r = val.r;
		pDst[i].i = val.i;
	}
	return;
}

void ne10sSqr(ne10_float32_t* pSrcDst, int len)
{
	for (int i = 0; i < len; ++i)
	{
		pSrcDst[i] *= pSrcDst[i];
	}
	return;
}

void ne10sSqrt(ne10_float32_t* pSrcDst, int len)
{
	for (int i = 0; i < len; ++i)
	{
		pSrcDst[i] = (pSrcDst[i] >= 0 ? sqrtf(pSrcDst[i]) : 0);
	}
	return;
}

void ne10sStdDev(const ne10_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pStdDev)
{
	if (!ne10sVerifyArgs("ne10sStdDev", pSrc, len, pStdDev)) return;

	if (len <= 1)
	{
		*pStdDev = 0.f;
		return;
	}
	double mean = 0.;
	for (int i = 0; i < len; ++i)
	{
		mean += pSrc[i];
	}
	mean /= len;
	double sum = 0.;
	for (int i = 0; i < len; ++i)
	{
		double t = pSrc[i] - mean;
		sum += (t * t);
	}
	sum /= (len - 1);
	*pStdDev = static_cast<ne10_float32_t>(sum > 0. ? sqrt(sum) : 0.);
	return;
}

void ne10sSub(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pSrcDst, int len)
{
	if (!ne10sVerifyArgs("ne10sSub", pSrc, pSrcDst, len)) return;

	for (int i = 0; i < len; ++i)
	{
		pSrcDst[i] -= pSrc[i];
	}
	return;
}

void ne10sSubC(ne10_float32_t val, ne10_float32_t* pSrcDst, int len)
{
//	ne10_subc_float_c(pSrcDst, pSrcDst, val, len);
	for (int i = 0; i < len; ++i)
	{
		pSrcDst[i] -= val;
	}
	return;
}

void ne10sSum(const ne10_float32_t* __restrict__ pSrc, int len, ne10_float32_t* __restrict__ pSum)
{
	if (!ne10sVerifyArgs("ne10sSum", pSrc, len, pSum)) return;

	double temp = 0.;
	for (int i = 0; i < len; ++i)
	{
		temp += pSrc[i];
	}
	*pSum = temp;
	return;
}

void ne10sSum(const ne10_fft_cpx_float32_t* __restrict__ pSrc, int len, ne10_fft_cpx_float32_t* __restrict__ pSum)
{
	if (!ne10sVerifyArgs("ne10sSum", pSrc, len, pSum)) return;

	double tempr = 0.;
	double tempi = 0.;
	for (int i = 0; i < len; ++i)
	{
		tempr += pSrc[i].r;
		tempi += pSrc[i].i;
	}
	pSum->r = tempr;
	pSum->i = tempi;
	return;
}

void ne10sThreshold_LT(const ne10_fft_cpx_float32_t* __restrict__ pSrc, ne10_fft_cpx_float32_t* __restrict__ pDst, int len, ne10_float32_t level)
{
	if (!ne10sVerifyArgs("ne10sThreshold_LT", pSrc, pDst, len)) return;

	float levelsq = level * level;
	for (int i = 0; i < len; ++i)
	{
		float magsq = pSrc[i].r * pSrc[i].r + pSrc[i].i * pSrc[i].i;
		if (magsq < levelsq)
		{
			float mag = sqrtf(magsq);
			pDst[i].r = (mag > 0 ? pSrc[i].r * level / mag : level);
			pDst[i].i = (mag > 0 ? pSrc[i].i * level / mag : 0);
		}
		else
		{
			pDst[i].r = pSrc[i].r;
			pDst[i].i = pSrc[i].i;
		}
	}

	return;
}

void ne10sThreshold_LTVal(ne10_float32_t* pSrcDst, int len, ne10_float32_t level, ne10_float32_t value)
{
	for (int i = 0; i < len; ++i)
	{
		pSrcDst[i] = (pSrcDst[i] < level ? value : pSrcDst[i]);
	}
	return;
}

void ne10sThreshold_LTVal(const ne10_float32_t* __restrict__ pSrc, ne10_float32_t* __restrict__ pDst, int len, ne10_float32_t level, ne10_float32_t value)
{
	if (!ne10sVerifyArgs("ne10sThreshold_LTVal", pSrc, pDst, len)) return;

	for (int i = 0; i < len; ++i)
	{
		pDst[i] = (pSrc[i] < level ? value : pSrc[i]);
	}
	return;
}

void ne10sThreshold_LTVal(ne10_fft_cpx_float32_t* pSrcDst, int len, ne10_float32_t level, ne10_fft_cpx_float32_t value)
{
	float levelsq = level * level;
	for (int i = 0; i < len; ++i)
	{
		if (pSrcDst[i].r * pSrcDst[i].r + pSrcDst[i].i * pSrcDst[i].i < levelsq)
		{
			pSrcDst[i].r = value.r;
			pSrcDst[i].i = value.i;
		}
	}
	return;
}

void ne10sThreshold_GTVal(ne10_fft_cpx_float32_t* pSrcDst, int len, ne10_float32_t level, ne10_fft_cpx_float32_t value)
{
	float levelsq = level * level;
	for (int i = 0; i < len; ++i)
	{
		if (pSrcDst[i].r * pSrcDst[i].r + pSrcDst[i].i * pSrcDst[i].i > levelsq)
			pSrcDst[i] = value;
	}
	return;
}

void ne10sTone_Direct(ne10_fft_cpx_float32_t* __restrict__ pDst, int len, float magn, float rFreq, float* __restrict__ pPhase)
{
	if (!ne10sVerifyArgs("ne10sTone_Direct", pDst, len, pPhase)) return;

	static const float TWOPI = 8 * atan(1.0f);
	for (int i = 0; i < len; ++i)
	{
		vdt::fast_sincosf(*pPhase + TWOPI * rFreq * i, pDst[i].i, pDst[i].r);
	}
	if (magn != 1)
	{
		for (int i = 0; i < len; ++i)
		{
			pDst[i].r *= magn;
			pDst[i].i *= magn;
		}
	}
	*pPhase += TWOPI * len * rFreq;
	return;
}

void ne10sVectorRamp(ne10_float32_t* pDst, int len, float offset, float slope)
{
	for (int i = 0; i < len; ++i)
	{
		pDst[i] = offset + slope * i;
	}
	return;
}

bool ne10sVerifyArgs(const char* str, const unsigned long* a, const unsigned long* b, int len)
{
	if (a + len <= b || b + len <= a)
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

bool ne10sVerifyArgs(const char* str, const ne10_float32_t* a, const ne10_float32_t* b, int len)
{
	if (a + len <= b || b + len <= a)
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, const ne10_fft_cpx_float32_t* b, int len)
{
	if (a + len <= b || b + len <= a)
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, const ne10_float32_t* b, const ne10_float32_t* c, int len)
{
	auto aa = reinterpret_cast<const ne10_float32_t*>(a);
	if ((aa + 2 * len <= b || b + len <= aa) &&
		(aa + 2 * len <= c || c + len <= aa) &&
		(b + len <= c || c + len <= b))
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

bool ne10sVerifyArgs(const char* str, const ne10_int16_t* a, const ne10_float32_t* b, int len)
{
	auto bb = reinterpret_cast<const ne10_int16_t*>(b);
	if (a + len <= bb || bb + 2 * len <= a)
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

bool ne10sVerifyArgs(const char* str, const ne10_float32_t* a, const ne10_uint8_t* b, int len)
{
	auto aa = reinterpret_cast<const ne10_uint8_t*>(a);
	if (aa + 4 * len <= b || b + len <= aa)
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, const ne10_fft_cpx_float32_t* b, int aLen, int bLen)
{
	if ((a + aLen <= b || b + bLen <= a))
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

bool ne10sVerifyArgs(const char* str, const ne10_float32_t* a, const ne10_float32_t* b, int aLen, int bLen)
{
	if ((a + aLen <= b || b + bLen <= a))
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, const ne10_fft_cpx_float32_t* b,
	const ne10_fft_cpx_float32_t* c, int aLen, int bLen, int cLen)
{
	if ((a + aLen <= b || b + bLen <= a) &&
		(a + aLen <= c || c + cLen <= a) &&
		(b + bLen <= c || c + cLen <= b))
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, const ne10_float32_t* b, int len)
{
	auto aa = reinterpret_cast<const ne10_float32_t*>(a);
	if (aa + 2 * len <= b || b + len <= aa)
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

bool ne10sVerifyArgs(const char* str, const ne10_float32_t* a, const ne10_float32_t* b, int len, const ne10_float32_t* c)
{
	if ((a + len <= b || b + len <= a) &&
		(a + len <= c || c + 1 <= a) &&
		(b + len <= c || c + 1 <= b))
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

bool ne10sVerifyArgs(const char* str, const ne10_float32_t* a, const ne10_fft_cpx_float32_t* b, int len, const ne10_fft_cpx_float32_t* c)
{
	auto bb = reinterpret_cast<const ne10_float32_t*>(b);
	auto cc = reinterpret_cast<const ne10_float32_t*>(c);
	if ((a + len <= bb || bb + 2 * len <= a) &&
		(a + len <= cc || cc + 2 <= a) &&
		(bb + 2 * len <= cc || cc + 2 <= bb))
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, const ne10_fft_cpx_float32_t* b, int len, const ne10_fft_cpx_float32_t* c)
{
	if ((a + len <= b || b + len <= a) &&
		(a + len <= c || c + 1 <= a) &&
		(b + len <= c || c + 1 <= b))
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

bool ne10sVerifyArgs(const char* str, const ne10_float32_t* a, int len, const ne10_float32_t* b)
{
	if (a + len <= b || b + 1 <= a)
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, int len, const ne10_float32_t* b)
{
	auto aa = reinterpret_cast<const ne10_float32_t*>(a);
	if (aa + 2 * len <= b || b + 1 <= aa)
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

bool ne10sVerifyArgs(const char* str, const ne10_fft_cpx_float32_t* a, int len, const ne10_fft_cpx_float32_t* b)
{
	if (a + len <= b || b + 1 <= a)
		return true;

	printf("%s has overlapping arguments\n", str);
	return false;
}

// Functions using Ne10 vectors

void ne10sAbs(Ne10F32Vec& srcDst)
{
	ne10sAbs(srcDst.data(), srcDst.size());
	return;
}

void ne10sAutoCorr(const Ne10F32cVec& __restrict__ src, Ne10F32cVec& __restrict__ dst, int lowLag)
{
	ne10sAutoCorr(src.data(), src.size(), dst.data(), dst.size(), lowLag);
	return;
}

void ne10sCartToPolar(const Ne10F32cVec& __restrict__ src, Ne10F32Vec& __restrict__ dstMagn, Ne10F32Vec& __restrict__ dstPhase)
{
	dstMagn.resize(0);
	dstPhase.resize(0);
	dstMagn.resize(src.size());
	dstPhase.resize(src.size());
	ne10sCartToPolar(src.data(), dstMagn.data(), dstPhase.data(), src.size());
	return;
}

void ne10sCrossCorr(const Ne10F32cVec& __restrict__ src1, const Ne10F32cVec& __restrict__ src2, Ne10F32cVec& __restrict__ dst, int lowLag)
{
	ne10sCrossCorr(src1.data(), src1.size(), src2.data(), src2.size(), dst.data(), dst.size(), lowLag);
	return;
}

void ne10sCrossCorr(const Ne10F32cVec& __restrict__ src1, const Ne10F32cVec& __restrict__ src2, ne10_fft_cpx_float32_t& __restrict__ dst)
{
	ne10sCrossCorr(src1.data(), src1.size(), src2.data(), src2.size(), &dst, 1, 0);
	return;
}

void ne10sDotProd(const Ne10F32Vec& __restrict__ src1, const Ne10F32Vec& __restrict__ src2, ne10_float32_t& dp)
{
	assert(src1.size() == src2.size());
	ne10sDotProd(src1.data(), src2.data(), src1.size(), &dp);
	return;
}

void ne10sDotProd(const Ne10F32Vec& __restrict__  src1, const Ne10F32cVec& __restrict__  src2, ne10_fft_cpx_float32_t& __restrict__  dp)
{
	assert(src1.size() == src2.size());
	ne10sDotProd(src1.data(), src2.data(), src1.size(), &dp);
	return;
}

void ne10sDotProd(const Ne10F32cVec& src1, const Ne10F32cVec& src2, ne10_fft_cpx_float32_t& dp)
{
	assert(src1.size() == src2.size());
	ne10sDotProd(src1.data(), src2.data(), src1.size(), &dp);
	return;
}
#if 0
void ne10sFFTFwd_CToC(const Ne10F32cVec& src, Ne10F32cVec& dst, ne10_fft_cfg_float32_t cfg)
{
	ne10_fft_c2c_1d_float32_neon(&dst[0], const_cast<ne10_fft_cpx_float32_t*>(&src[0]), cfg, 0);
	dst /= static_cast<float>(src.size());
	return;
}
#endif
void ne10sImag(const Ne10F32cVec& __restrict__ src, Ne10F32Vec& __restrict__ dstIm)
{
	dstIm.resize(0);	// sets m_size and avoids copying existing vector when resizing
	dstIm.resize(src.size());
	ne10sImag(src.data(), dstIm.data(), src.size());
	return;
}

void ne10sLn(Ne10F32Vec& srcDst)
{
	ne10sLn(srcDst.data(), srcDst.size());
	return;
}

void ne10sLog10(const Ne10F32Vec& __restrict__ src, Ne10F32Vec& __restrict__ dst)
{
	dst.resize(0);
	dst.resize(src.size());
	ne10sLog10(src.data(), dst.data(), src.size());
	return;
}

void ne10sMagnitude(const Ne10F32cVec& __restrict__ src, Ne10F32Vec& __restrict__ dst)
{
	dst.resize(0);
	dst.resize(src.size());
	ne10sMagnitude(src.data(), dst.data(), src.size());
	return;
}

void ne10sMagnitude(const Ne10F32cVec2& __restrict__ src, Ne10F32Vec& __restrict__ dst)
{
	dst.resize(0);
	dst.resize(src.size());
	if (!ne10sVerifyArgs("ne10sMagnitude", src.dataReal(), dst.data(), 2 * src.size(), dst.size())) return;

	for (int i = 0; i < int(dst.size()); ++i)
	{
		dst[i] = src.real(i) * src.real(i) + src.imag(i) * src.imag(i);
	}
	for (int i = 0; i < int(dst.size()); ++i)
	{
		dst[i] = sqrtf(dst[i]);
	}

	return;
}

void ne10sMaxEvery(const Ne10F32Vec& __restrict__ src, Ne10F32Vec& __restrict__ srcDst)
{
	assert(src.size() == srcDst.size());
	ne10sMaxEvery(src.data(), srcDst.data(), src.size());
	return;
}

void ne10sNthMaxElement(const ne10_float32_t* __restrict__ pSrc, int len, int N, ne10_float32_t& __restrict__ res)
{
	ne10sNthMaxElement(pSrc, len, N, &res);
	return;
}

void ne10sNorm_L2(const Ne10F32Vec& __restrict__ src, ne10_float32_t& __restrict__ norm)
{
	ne10sNorm_L2(src.data(), src.size(), &norm);
	return;
}

void ne10sNorm_L2(const Ne10F32cVec& __restrict__ src, ne10_float32_t& __restrict__ norm)
{
	ne10sNorm_L2(src.data(), src.size(), &norm);
	return;
}

void ne10sPhase(const Ne10F32cVec& __restrict__ src, Ne10F32Vec& __restrict__ dst)
{
	dst.resize(0);
	dst.resize(src.size());
	ne10sPhase(src.data(), dst.data(), src.size());
	return;
}

void ne10sPolarToCart(const Ne10F32Vec& __restrict__ srcMagn, const Ne10F32Vec& __restrict__ srcPhase, Ne10F32cVec& __restrict__ dst)
{
	assert(srcMagn.size() == srcPhase.size());
	dst.resize(0);
	dst.resize(srcMagn.size());
	ne10sPolarToCart(srcMagn.data(), srcPhase.data(), dst.data(), srcMagn.size());
	return;
}

void ne10sPowerSpectr(const ne10_fft_cpx_float32_t* __restrict__ pSrc, Ne10F32Vec& __restrict__ dst)
{
	ne10sMagnitudeSqr(pSrc, dst.data(), dst.size());
	return;
}

void ne10sPowerSpectr(const Ne10F32cVec& __restrict__ src, Ne10F32Vec& __restrict__ dst)
{
	dst.resize(0);
	dst.resize(src.size());
	ne10sMagnitudeSqr(src.data(), dst.data(), dst.size());
	return;
}

void ne10sSqr(Ne10F32Vec& srcDst)
{
	ne10sSqr(srcDst.data(), srcDst.size());
	return;
}

void ne10sSqrt(Ne10F32Vec& srcDst)
{
	ne10sSqrt(srcDst.data(), srcDst.size());
	return;
}

void ne10sStdDev(const Ne10F32Vec& __restrict__ src, ne10_float32_t& __restrict__ stdDev)
{
	ne10sStdDev(src.data(), src.size(), &stdDev);
	return;
}

void ne10sThreshold_LT(const ne10_fft_cpx_float32_t* __restrict__ pSrc, Ne10F32cVec& __restrict__ dst, ne10_float32_t level)
{
	ne10sThreshold_LT(pSrc, dst.data(), dst.size(), level);
	return;
}

void ne10sThreshold_LTVal(Ne10F32Vec& srcDst, ne10_float32_t level, ne10_float32_t value)
{
	ne10sThreshold_LTVal(srcDst.data(), srcDst.size(), level, value);
	return;
}

void ne10sThreshold_LTVal(const Ne10F32Vec& __restrict__ src, Ne10F32Vec& __restrict__ dst, ne10_float32_t level, ne10_float32_t value)
{
	dst.resize(0);
	dst.resize(src.size());
	ne10sThreshold_LTVal(src.data(), dst.data(), src.size(), level, value);
	return;
}

void ne10sThreshold_LTVal(Ne10F32cVec& srcDst, ne10_float32_t level, ne10_fft_cpx_float32_t value)
{
	ne10sThreshold_LTVal(srcDst.data(), srcDst.size(), level, value);
	return;
}

void ne10sThreshold_LTVal(Ne10F32cVec2& srcDst, ne10_float32_t level, ne10_fft_cpx_float32_t value)
{
	float levelsq = level * level;
	for (int i = 0; i < int(srcDst.size()); ++i)
	{
		if (srcDst.real(i) * srcDst.real(i) + srcDst.imag(i) * srcDst.imag(i) < levelsq)
		{
			srcDst.real(i) = value.r;
			srcDst.imag(i) = value.i;
		}
	}
	return;
}

void ne10sThreshold_GTVal(Ne10F32cVec& srcDst, ne10_float32_t level, ne10_fft_cpx_float32_t value)
{
	ne10sThreshold_GTVal(srcDst.data(), srcDst.size(), level, value);
	return;
}

void ne10sThreshold_GTVal(Ne10F32cVec2& srcDst, ne10_float32_t level, ne10_fft_cpx_float32_t value)
{
	float levelsq = level * level;
	for (int i = 0; i < int(srcDst.size()); ++i)
	{
		if (srcDst.real(i) * srcDst.real(i) + srcDst.imag(i) * srcDst.imag(i) > levelsq)
		{
			srcDst.real(i) = value.r;
			srcDst.imag(i) = value.i;
		}
	}
	return;
}

void ne10sVectorRamp(Ne10F32Vec& dst, float offset, float slope)
{
	ne10sVectorRamp(dst.data(), dst.size(), offset, slope);
	return;
}

void ne10sTone_Direct(Ne10F32cVec& __restrict__ dst, float magn, float rFreq, float& __restrict__ phase)
{
	ne10sTone_Direct(dst.data(), dst.size(), magn, rFreq, &phase);
	return;
}

};
