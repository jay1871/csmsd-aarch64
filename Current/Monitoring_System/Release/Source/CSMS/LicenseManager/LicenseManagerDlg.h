
// LicenseManagerDlg.h : header file
//

#pragma once

#include <string>
// CLicenseManagerDlg dialog
class CLicenseManagerDlg : public CDialogEx
{
// Construction
public:
	CLicenseManagerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_LICENSEMANAGER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedGenerate();
private:
	const std::string VERSION = "Version=0";
	const std::string PRECISION_TIMESTAMP = "HasPrecisionTimestamp";
	const std::string EIGHTY_MHZ = "Has80MHzRadio";
	const std::string HF_ENABLED = "HasHF";
	const std::string SHFEXT_ENABLED = "HasSHFExt";
	const std::string VUHF_MHZ = "HasVUHF";
	const std::string MACHINEID = "MachineId";
	const std::string SERIALNUM = "SerialNumber";
	const std::string CUSTOMER = "; CsmsServer License File -";
	const std::string HOST = "; Hostname:";
	CString filename;
	CMenu m_filemenu;
	bool ValidateInputs();
	void GenerateSecuritykeyandLicenseFile();
public:
	afx_msg void OnFileLoadlicense();
};
