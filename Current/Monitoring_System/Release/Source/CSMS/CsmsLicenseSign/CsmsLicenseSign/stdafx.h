// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>



// TODO: reference additional headers your program requires here
namespace TCIPaths
{
	const char* const dir = "/media/tci/csms/";
	const char* const configDir = "/media/tci/csms/etc/";
	const char* const dataDir = "/media/tci/csms/data/";
	const char* const logDir = "/media/tci/csms/log/";
}
