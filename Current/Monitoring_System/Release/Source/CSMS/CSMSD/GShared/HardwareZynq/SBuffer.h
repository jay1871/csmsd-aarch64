/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "PLddr3.h"

class CSBufferBase
{
public:
	// Constants
#ifdef CSMS_DEBUG
	static size_t s_onSampleCnt;
	static size_t s_sampleFlushCnt;
	static size_t s_sampleFndCnt;
	static bool s_sampleTimeout;
	static size_t s_sampleWaitCnt;
	static bool s_traceAvd;


	static bool IsSampleTimeout(void) { return(s_sampleTimeout); }
#endif

	// Functions
	void DoneWithSamples(unsigned long sampleCount);
	bool FlushSamples(unsigned long sampleCount, unsigned long count);
	off_t GetNextPhysicalAddress(void) const;
	void Init(off_t addr);
	void OnSamples(void);
	void Reset(void);

protected:
	// Types
	struct SMappedMemory
	{
		volatile unsigned long* addr;
		size_t size;
	};

	// Functions
	CSBufferBase(void) : m_bufSize(0), m_usedPhysicalMemory(0) {}
	virtual ~CSBufferBase(void);

	bool FindSamples(unsigned long sampleCount, unsigned long count, unsigned long timeout);
	bool MarkSamplesInUse(unsigned long sampleCount, unsigned long count);
	bool WaitForBufferSpace(unsigned long count, unsigned long& sampleCount) const;

	// Data
	unsigned long m_bufSize;	// Number of 32-bit i/q samples or psd points in buffer
	size_t m_usedPhysicalMemory;
	off_t m_nextPhysicalAddress;
	std::vector<SMappedMemory> m_mappedMemory;

private:
	// Constants
	static const size_t MAX_BLOCKS_IN_BUFFER = 20;

	// Types
	struct SInUse
	{
		unsigned long offset;	// Offset in 32-bit entries
		unsigned long count;	// Number of 32-bit i/q samples or psd points in buffer
	};

	// Functions
	void ShowBuffer(const char* str) const;

	// Data
	mutable std::mutex m_dataCritSect;
	std::deque<SInUse> m_reserved;
	std::deque<SInUse> m_available;
	mutable std::condition_variable m_moreBufferSpace;
	std::condition_variable m_newSamples;
};


////////////////////  This class uses the PL DDR3 memory for the buffer //////////////////
class CSBufferPLddr3 : public CSBufferBase
{
public:
	// Functions
	CSBufferPLddr3(void) : CSBufferBase(), m_basePhysicalAddress(0) {}
	~CSBufferPLddr3(void) {}
	void Assign(const CPLddr3& addr, size_t size, off_t base);
	void FillMemory(unsigned long sampleCount, unsigned long count);
	bool FindSamples(unsigned long sampleCount, unsigned long count, std::vector<unsigned long>& data, unsigned long timeout = SAMPLES_TIMEOUT);
	bool MapMemory(int driverDesc, size_t totalSize, size_t size, CPLddr3& virtualAddress);
	bool MarkSamplesInUse(unsigned long sampleCount, unsigned long count);
	unsigned long PhysicalAddress(unsigned long sampleCount);
	unsigned long PhysicalAddress(unsigned long offsetbase, unsigned long sampleCount);
	void ReadMemory(unsigned long sampleCount, unsigned long count, std::vector<unsigned long>& data);
	bool WaitForBufferSpace(unsigned long count, unsigned long& sampleCount) const;

private:
	// Constants
	static const unsigned int SAMPLES_TIMEOUT = 2000; // ms

	// Data
	CPLddr3 m_buffer;	// Memory-mapped buffer pointer
	off_t m_basePhysicalAddress;
};


////////////////////  This class uses the PS DMA memory for the buffer //////////////////
class CSBufferPSdma : public CSBufferBase
{
public:
	// Constants
	static const unsigned int SAMPLES_TIMEOUT = 2000; // ms

	// Functions
	CSBufferPSdma(void) : CSBufferBase(), m_basePhysicalAddress(0) {}
	~CSBufferPSdma(void) {}
	void Assign(const uncached_ptr& addr, size_t size, off_t base);
	bool FindSamples(unsigned long sampleCount, unsigned long count, uncached_ptr& data, unsigned long timeout = SAMPLES_TIMEOUT);
	bool MapMemory(int driverDesc, size_t totalSize, size_t size, uncached_ptr& virtualAddress);
	bool MarkSamplesInUse(unsigned long sampleCount, unsigned long count);
	unsigned long PhysicalAddress(unsigned long sampleCount);
	bool WaitForBufferSpace(unsigned long count, unsigned long& sampleCount) const;

private:
	// Constants

	// Data
	uncached_ptr m_buffer;	// Memory-mapped buffer pointer
	off_t m_basePhysicalAddress;
};
