/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

struct S2630Msg
{
	static constexpr const unsigned long MIN_2630_VERSION = 0x01050000;
	static constexpr const unsigned long MIN_2630_VERSION_SPIN4 = 0x02040000;

	// Message definitions
	enum EType : unsigned char { SHORTMSG = 0xf0, LONGMSG = 0xf1, SHORT16MSG = 0xf2 };

	enum ECommand : unsigned char
	{
		DO_RX_FIRMWARE_RESET = 0,		// Not Implemented

		DO_RX_HARDWARE_RESET = 1,		// Send: 0xf0 | DO_RX_HARDWARE_RESET | 0 | 0
		   	   	   	   	   	   	   	   	// Reply: 0xf0 | DO_RX_HARDWARE_RESET | 0 | 1

		GET_ANTENNA_SETTING = 2,		// Send: 0xf0 | GET_ANTENNA_SETTING | 0 | 0
										// Reply: 0xf0 | GET_ANTENNA_SETTING | 0 | Setting:0 - 3

		GET_ATTENUATION_SETTING = 3,	// Send: 0xf2 | GET_ATTENUATION_SETTING | 0 | 0
        								// Reply: 0xf2 | GET_ATTENUATION_SETTING | mode Bits(0-3) LNA(bit-4) | Setting:0 - 0x60
        								// Mode is of type:
        								// EAttenuationType : unsigned char { NORMAL, RURAL, URBAN, CONGESTED };
        								// LNA 1=On 0=Off

		GET_BAND_WIDTH = 4,				// Send: 0xf0 | GET_BAND_WIDTH | 0 | 0
										// Reply: 0xf0 | GET_BAND_WIDTH | 0 | Setting:0x50/0x0a

		GET_BIST = 5,					// Send: 0xf1 | GET_BIST | 0 | 0
										// Reply: 0xf1 | GET_BIST | sizeof SBistResults (16 bit value)
										// pkt00: (32-Bit Value)
										// ...
										// pkt012a: (32-Bit Value) (last packet)
										// (See SBistResults structure for details.)

		GET_FIRMWARE_DATE = 6,			// Send: 0xf0 | GET_FIRMWARE_DATE | 0 | 0
										// Reply: 0xf0 | GET_FIRMWARE_DATE | 01 | (Reserved (8-Bits))
										// Pkt00: Year(16-bits) | Month(8-Bits) | Day(8-Bits)
										// Note, date values are in decimal.

		GET_FIRMWARE_VERSION = 7,		// Send: 0xf0 | GET_FIRMWARE_VERSION | 0 | 0
										// Reply: 0xf0 | GET_FIRMWARE_VERSION | 01 | (Reserved (8-Bits))
										// Pkt00: Major(8-Bits) | Minor(8-Bits) | Build(16-Bits)

		GET_FPGA_VERSION = 8,			// Send: 0xf0 | GET_FPGA_VERSION | 0 | 0
										// Reply: 0xf0 | GET_FPGA_VERSION | 01 | (Reserved (8-Bits))
										// Pkt00: Major(8-Bits) | Minor(8-Bits) | Build(16-Bits)

		// Retrieve Frequency as a double precision floating point value.
		//
		GET_FREQUENCY = 9,				// Send: 0xf0 |  GET_FREQUENCY | 0 | 0
										// Reply: 0xf0 |  GET_FREQUENCY | 02 | (Reserved (8-Bits))
										// Pkt00: Frequency LSB (32-Bit) Double Value
										// Pkt01: Frequency MSB (32-Bit) Double Value

		// Get Frequency as a 32-Bit integer value.
		//
		GET_INTEGER_FREQUENCY = 10,		// Send: 0xf0 | GET_INTEGER_FREQUENCY | 0 | 0
										// Reply: 0xf0 | GET_INTEGER_FREQUENCY | 01 | (Reserved (8-Bits))
										// Pkt00: Frequency (32-Bit) Integer Value

		// Get current Frequency settings for LO1 and Lo2.
		//
		GET_LO_FREQUENCIES = 11,		// Send: 0xf0 | GET_LO_FREQUENCIES | 0 | 0
										// Reply: 0xf0 | GET_LO_FREQUENCIES | 02 | Preselector Setting:4-0x20
										// Pkt00: LO1 Frequency (32-Bit) Integer Value
										// Pkt01: Frequency LO2 (32-Bit) Integer Value

		// Multiple Operations Command.
		//
		GET_MOC = 12,					// Send: 0xf0 |  GET_MOC | 0 | 0
										// Reply: 0xf0 |  GET_MOC | 01 | (Bits(4-7) Reserved) (Bits(1-3) Antenna) (Bit0 BW 1=80 MHz, 0=10 MHz)
										// Pkt00: (Bits 24-31 = PreSelector) (Bits 0-23 = Frequency)

		GET_PRESELECTOR_SETTING = 13,	// Send: 0xf0 | GET_PRESELECTOR_SETTING | 0 | 0
										// Reply: 0xf0 | GET_PRESELECTOR_SETTING | 0 | Preselector Setting:4-0x20

		GET_RX_CAL_GEN_FREQUENCY = 14,	// Send: 0xf0 | GET_RX_CAL_GEN_FREQUENCY | 0 | 0
										// Reply: 0xf0 | GET_RX_CAL_GEN_FREQUENCY | 01 | (Reserved (8-Bits))
										// Pkt00: Frequency (32-Bit) Integer Value

		GET_RX_CAL_TABLE = 15,			// Send: 0xf1 | GET_RX_CAL_TABLE | 0 | 01
										// Send: TableType (16-bit) | TableIndex (16-bit)
										// table type = ECalibrationType(24-31)
										// ERfCalDataType(16-23) or EAttenuationType(16-23) or
										// 0 for bits(16-23) for IFCALDATA.
										// TableIndex(16-bit) = 0 - 0x20 for RF Cal Data
										// 0 - 0x0c for IF Cal Data and 0 - 0x08 for Atten Cal Data
										// Reply: 0xf1 | GET_RX_CAL_TABLE | Length (16-bits)
										// PKT:00
										// ...
										// pkt:(length-1).
										// (See SCalTable structure for details.)

		GET_RX_CAPABILITIES = 16,		// Send: 0xf0 | GET_RX_CAPABILITIES | 0 | 0
										// Reply: 0xf0 | GET_RX_CAPABILITIES | 0x1f | (Reserved (8-Bits))
										// pkt00: (32-Bit Value)
										// ...
										// pkt0x1e: (32-Bit Value)
										// (See GET_RX_CAPABILITIES structure for details.)

		GET_RX_FPI_TABLE = 17,			// Send: 0xf1 | GET_RX_FPI_TABLE | 0 | 0
										// Reply: 0xf1 | GET_RX_FPI_TABLE | length (16-Bit 0x680)
										// Pkt00: Value (32-Bit)
										// ...
										// Pkt0x679: Value (32-Bit)
										// (See Frequency Plan table for details.)

		GET_RX_OCXO_DAC_CODE = 18,		// Send: 0xf0 | GET_RX_OCXO_DAC_CODE | 0 | 0
										// Reply: 0xf0 | GET_RX_OCXO_DAC_CODE | 01 | (Reserved (8-Bits))
										// Pkt00: Ocxo DAC Code (32-Bits)

		GET_RX_OCXO_INTERVAL_BETWEEN_CORRECTIONS = 19,
										// Send: 0xf0 | GET_RX_OCXO_INTERVAL_BETWEEN_CORRECTIONS | 0 | 0
										// Reply: 0xf0 | GET_RX_OCXO_INTERVAL_BETWEEN_CORRECTIONS | 01 | (Reserved (8-Bits))
										// OcxO Interval Between Corrections (32-Bits)

		GET_RX_OCXO_LOOP_STABLE_STATUS = 20,
										// Send: 0xf0 | GET_RX_OCXO_LOOP_STABLE_STATUS | 0 | 0
										// Reply: 0xf0 | GET_RX_OCXO_LOOP_STABLE_STATUS | 0 | (Reserved (Bits 1-7)) (Bit 0 1=Loop Stable, 0=Loop Not Stable)

		GET_RX_REGISTERS = 21,			// Not Implemented

		GET_RX_SETTINGS = 22, 			// Send: 0xf0 | GET_RX_SETTINGS | 0 | 0
										// Reply: 0xf0 | GET_RX_SETTINGS | 0x12 | (Reserved (8-Bits))
										// Pkt00: Integer Value (32-Bits)
										// ...
										// Pkt0x11: Integer Value (32-Bit)
										// (See GET_RX_SETTINGS structure for details.)

		GET_RX_TEMPERATURE = 23,		// Send: 0xf0 | GET_RX_TEMPERATURE | 0 | ETemperatureSensors
										// Reply: 0xf0 | GET_RX_TEMPERATURE | 01 | ETemperatureSensors
										// pkt00: (32-bit) floating point value representing Temp. in Degrees C

		QSPI_CHECK_FLASH_STATUS = 24,	// Send: 0xf0 |  QSPI_CHECK_FLASH_STATUS | 0 | 0
										// Reply: 0xf0 | QSPI_CHECK_FLASH_STATUS | 0 | EFlashStatus(8-bits)
										// EFlashStatus { CORRUPT, EMPTY, FULL, OK };

		QSPI_ERASE_ALL_FLASH_PAGES=25,	// Send: 0xf0 | QSPI_ERASE_ALL_FLASH_PAGES | 0 | 0
										// Reply: 0xf0 | QSPI_ERASE_ALL_FLASH_PAGES | 0 | 01=Success 00=Failure
										// Expect upto a 60 Sec delay before getting a response.

		QSPI_ERASE_FLASH_PAGE = 26, 	// Send: 0xf0 | QSPI_ERASE_FLASH_PAGE | 01 | 0
										// Send: page address (32-bit) Integer Value
										// Note, page addresses are on a 64K boundary: 0, 0x10000, 0x20000 etc.
										// Reply: 0xf0 | QSPI_ERASE_FLASH_PAGE | 0 | 01=Success 00=Failure

		QSPI_ERASE_FLASH_TABLE = 27,

		QSPI_READ_FLASH_SECTOR = 28,	// Send: 0xf0 | QSPI_READ_FLASH_SECTOR | 01 | 0
										// Send: sector address (32-bit) Integer Value
										// Note, sector addresses are on a 256 byte boundary: 0, 0x100, 0x200 etc.
										// Reply: 0xf0 | QSPI_READ_FLASH_SECTOR | 0x40 | 01=Success 00=Failure
										// Pkt01: 32-bit data value
										// ...
										// Pkt64: 32-bit data value

		QSPI_READ_TABLE_FROM_FLASH = 29,// Pending Test.

		QSPI_WRITE_FLASH_SECTOR = 30,	// Pending Test.

		QSPI_WRITE_TABLE_TO_FLASH = 31,
										// Send: 0xf1 | QSPI_WRITE_TABLE_TO_FLASH | 03 as a (16-Bit) value
										// Send: TableType (24-bit) | reserved (8-bit) value = 0

										/*  table type = ERxTableCategories (24-31)  ATTENUATION, CALIBRATION, FPI etc.

										   TableType based on category (16-23) of EAttenuationType, ECalibrationType, EFpiType etc.
										   Table Sub-type based on TableType (8-15) EAttenuationType, ELnaDataType, etc.
										*/

										// Send: TableSize in bytes (32-Bit) integer value
										// Send: TableVersion (32-bit) integer value
										//
										// Reply: 0xf1 | QSPI_WRITE_TABLE_TO_FLASH | 01 (16-Bit) value
										// Pkt00: (32-Bit) value lower 16 bits number of expected blocks, Upper 16 bits expected block size in bytes.
										//
										// Subsequent sends and receives should be sent as follows until expected blocks is 0.
										//
										// Send: 0xf1 | QSPI_WRITE_TABLE_TO_FLASH | NumPackets (16-Bit) value

										/*         NumPackets is the sum of TableType (32-bit) value + (blocksize / 4) packets.
										Note, BlockSize is returned in bytes
												  must be converted to(32-bit) words since table items are generally floating point values.
										*/
										// Send: TableType (32-Bit) integer value
										// Send: (blockSize / 4) (32-Bit) table values
										// Reply: 0xf1 | QSPI_WRITE_TABLE_TO_FLASH | 01 (16-Bit) value
										// Pkt00: (32-Bit) value lower 16 bits number of expected blocks, Upper 16 bits expected block size in bytes.

		READ_WRITE_REGISTER_CONTROL=32,
										// Send: 0xf0 | READ_WRITE_REGISTER_CONTROL | 0 | 01=Enable 00=Disable 02=Query
										// Reply: 0xf0 | READ_WRITE_REGISTER_CONTROL | 0 | 01=Enable 00=Disable

		RX_CAL_GEN_CONTROL = 33,		// Send: 0xf0 | RX_CAL_GEN_CONTROL | 0 | 01=Enable 00=Disable
										// Reply: 0xf0 | RX_CAL_GEN_CONTROL | 0 | 01=Enable 00=Disable

		SET_ANTENNA = 34,				// Send: 0xf0 |  SET_ANTENNA | 0 | (Bits3-7 Reserved) (Bits0-2 Antenna Setting)
										// Reply: 0xf0 |  SET_ANTENNA | 0 | (Bits3-7 Reserved) (Bits0-2 Antenna Setting)

		SET_ATTENUATION = 35,			// Send: 0xf2 | SET_ATTENUATION | Mode Bits(0-3) | Atten:0-0x60
										// Reply: 0xf2 | SET_ATTENUATION | Mode Bits(0-3) LNA(Bit-4) | Atten:0-0x60
										// Mode is of type:
										// EAttenuationType : unsigned char { NORMAL, RURAL, URBAN, CONGESTED };
										// LNA 1=On 0=Off

		SET_BAND_WIDTH = 36,			// Send: 0xf0 | SET_BAND_WIDTH | 0 | BW:0x50/0x0a
										// Reply: 0xf0 | SET_BAND_WIDTH | 0 | BW:0x50/0x0a
										// 0x0a = 10 MHz IBW  0x50 = 80 MHz IBW

		SET_FREQUENCY = 37,				// Send: 0xf0 |  SET_FREQUENCY | 02 | (Reserved (8-Bits))
										// Send: Frequency (32-bit) LSB Double Value
										// Send: Frequency (32-bit) MSB Double Value
										// Reply: 0xf0 |  SET_FREQUENCY | 02 | (Reserved (8-Bits))
										// Pkt00:: Frequency (32-bit) LSB Double Value
										// Pkt01: Frequency (32-bit) MSB Double Value

		SET_INTEGER_FREQUENCY = 38,		// Send: 0xf0 |  SET_INTEGER_FREQUENCY | 01 | (Reserved (8-Bits))
										// Send: Frequency (32-bit) Integer Value
										// Reply: 0xf0 |  SET_INTEGER_FREQUENCY | 01 | (Reserved (8-Bits))
										// Pkt00: Frequency (32-bit) Integer Value

		SET_MOC = 39,					// Send: 0xf0 | SET_MOC | 01 | (Bits(4-7) Reserved) (Bits(1-3) Antenna) (Bit0 BW 1=80 MHz, 0=10 MHz)
										// Send: (Bits 24-31 = (FPI 1-32) (Bits 0-23 = Frequency)
										// Reply: 0xf0 | SET_MOC | 01 | (Bits(4-7) Reserved) (Bits(1-3) Antenna) (Bit0 BW 1=80 MHz, 0=10 MHz)
										// Pkt00: (Bits 24-31 = PreSelector) (Bits 0-23 = Frequency)

		SET_PRESELECTORS = 40,			// Send: 0xf0 | SET_PRESELECTORS | 0 | Preselector:4-0x20
										// Reply: 0xf0 | SET_PRESELECTORS | 0 | Preselector:4-0x20

		SET_RX_CAL_GEN_FREQUENCY = 41,	// Send: 0xf0 |  SET_CAL_GEN_FREQUENCY | 01 | 00=CalGen only 01=CalGen and RX
										// Send: Frequency (32-bit) Integer Value
										// Reply: 0xf0 |  SET_CAL_GEN_FREQUENCY | 01 | 00=CalGen only 01=CalGen and RX
										// Pkt00: Frequency (32-bit) Integer Value

		SET_RX_REGISTERS = 42,  		// Send: 0xf0 | SET_RX_REGISTERS | NumRegs(1-0xff) | 0
										// Send: Reg01 (32-bits) integer value
										// Send: Reg02 (32-bits) integer value
										// ...
										// Send: Last Reg (32-bits) integer value

										// Note, the NumRegs field contains the number of registers to be sent.
										// The maximum is 255. The minimum is 1.

										// Reply: 0xf0 | SET_RX_REGISTERS | 0 | 0=failed or Value > 0 NumRegs Programmed

										// Note, it is necessary to enable READ_WRITE_REGISTER_CONTROL before
										// sending registers via SET_RX_REGISTERS. If not enabled, a failure code of 0
										// is returned. Send the following to avoid a fail code of 0.
										// Send: 0xf0 | READ_WRITE_REGISTER_CONTROL | 0 | 01

		GET_RSSI_VALUE = 43, 			// Send: 0xf0 | GET_RSSI_VALUE | 0 | RSSI Channel number (value:1-7)
										// Reply: 0xf0 | GET_RSSI_VALUE | 01 | Channel Number (1-7) or 0 = invalid Channel
										// Pkt00: RSSI (32-Bit) floating point Value in Volts

		GET_RSSI_GAIN_VALUE = 44, 		// Send: 0xf0 | GET_RSSI_GAIN_VALUE | 0 | RSSI Channel number (value:1-7)
										// Reply: 0xf0 | GET_RSSI_GAIN_VALUE | 01 | Channel Number (1-7) or 0 = invalid Channel
										// Pkt00: RSSI (32-Bit) floating point Value in dB

		QSPI_WRITE_FLASH_DEFAULTS = 45,
										// send: 0xf0 | QSPI_WRITE_FLASH_DEFAULTS | 0 | 0
										// Reply: 0xf0 | QSPI_WRITE_FLASH_DEFAULTS | 0 | (8-bit) Dec value of Num Tables Written

		GET_EXTERNAL_REFERENCE = 46,	// Send: 0xf0 | GET_EXTERNAL_REFERENCE | 0 | 0
										// Reply: 0xf0 | GET_EXTERNAL_REFERENCE | 0 | Reference-Setting (8-bit) value
										// Reference-Setting: 0=100 MHz 1=10 MHz

		SET_EXTERNAL_REFERENCE = 47,	// Send: 0xf0 | SET_EXTERNAL_REFERENCE | 0 | Reference-Setting (8-bit) value
										// Reply: 0xf0 | SET_EXTERNAL_REFERENCE | 0 | Reference-Setting (8-bit) value
										// Reference-Setting: 0=100 MHz 1=10 MHz

		GET_INTERNAL_REFERENCE = 48,	// Send: 0xf0 | GET_INTERNAL_REFERENCE | 0 | 0
										// Reply: 0xf0 | GET_INTERNAL_REFERENCE | 0 | 0=Internal 1=10 MHz Ext. 2=1PPS 3=100 MHz

		SET_INTERNAL_REFERENCE = 49,	// Send: 0xf0 | SET_INTERNAL_REFERENCE | 0 | Reference-Setting (8-bit) value
										// Reply: 0xf0 | SET_INTERNAL_REFERENCE | 0 | Reference-Setting (8-bit) value
										// Reference-Setting: 0=Internal 1=10 MHz Ext. 2=1PPS 3=100 MHz

		GET_CLOCK_STATE = 50,			// Send: 0xf0 | GET_CLOCK_STATE | 0 | (bits 7-1 reserved) bit 0: 1=External 0=Internal
										// Reply: 0xf0 | GET_CLOCK_STATE | 0 | (bits 7-1 reserved) bit 0 1=External 0=Internal

		SET_CLOCK_STATE = 51,			// Send: 0xf0 | SET_CLOCK_STATE | 0 | (bits 7-1 reserved) bit 0: 1=External 0=Internal
										// Reply: 0xf0 | SET_CLOCK_STATE | 0 | (bits 7-1 reserved) bit 0 1=External 0=Internal

		QSPI_GET_FLASH_LOCK_STATE=52,	// Send: 0xf0 | QSPI_GET_FLASH_LOCK_STATE | 0 | 0
										// Reply: 0xf0 | QSPI_GET_FLASH_LOCK_STATE | 0 | 0=Unlock 1=Lock

		QSPI_SET_FLASH_LOCK_STATE=53,	// Send: 0xf0 | QSPI_SET_FLASH_LOCK_STATE | 0 | 0=Unlock 1=Lock
										// Reply: 0xf0 | QSPI_SET_FLASH_LOCK_STATE | 0 | 0=Unlock 1=Lock
		GET_BIST_CONSTRAINT_TABLE=54,

		GET_MASTER_STATE=55,			// 1= df master 0 = not df master

		SET_MASTER_STATE=56,
		END_OF_COMMANDS
	};

	enum EAntennaType : unsigned char { TERMINATE, VUSHFANT1, VUSHFANT2, HFANT, VUSHFCAL, HFCAL };
	enum EFlashStatus { UNLOCKED, LOCKED, CORRUPT, EMPTY, FULL, OK, RECOVERED };
	enum ETemperatureSensors : unsigned char { NIOS, SF2450, OCXO };
	enum ERxTableCategories : unsigned char
	{
		ATTENUATION = 0,
		CALIBRATION = 1,
		FPI = 2,
		LO = 3,
		PRESELECTOR = 4,
		USER = 5,
		BIST = 6,
		SYSTEM = 7
	};
	enum EAttenuationType : unsigned char { NORMAL, RURAL, URBAN, CONGESTED };
	enum ECalibrationType : unsigned char { RFCALDATA, IFCALDATA, ATTCALDATA, TEMP_COMP, CALGENDATA };
	enum ELnaDataType : unsigned char { LNA_OFF, LNA_ON };
	enum EFpiType : unsigned char { IBW10, IBW80 };
	enum ELoType : unsigned char { LO1, LO2, CalGen, VTF, FIXED };
	enum EBistTestsPerFPI : unsigned long
	{
		ESignalPresentTest	= 0x00000001,
		ELo1LockTest		= 0x00000002,
		ELo2LockTest		= 0x00000004,
		ECalLockTest		= 0x00000008
	};

	static const int CALIB_TEMP_INDEX = OCXO;
	static const size_t MAX_FPI_ROWS = 32;

#if 0
	enum EReply : unsigned long
	{
		NAV,  //Not Available
		ACK,
		NAK,
		ERR,
	    BAD_HDR,
	    BAD_PKT,
	    BUSY,
	    PENDING,
	    COMPLETE,
	    GET,
	    SET,
	    UNKNOWN
	};
#endif
	struct SRxCapabilities
	{
		unsigned long maxVushfFreqMHz; // MHz
		unsigned long minVushfFreqMHz; // MHz
		unsigned long attenStep; // dB
		unsigned long finalIfFreqMHz; // MHz
		unsigned long hfBwMHz; // MHz
		unsigned long lowestHeterodyneFreqMHz;
		unsigned long minHfFreqHz; // Hz
		unsigned long maxHfFreqMHz;
		unsigned long minCalGenFreqMHz;
		unsigned long maxCalGenFreqMHz;
		unsigned long minCalGenFreqStepBelow_3200MHz;
		unsigned long minCalGenFreqStep3200AndAboveMHz;
		unsigned long maxCalGenFreqStepMHz;
		unsigned long narrowBwMHz; // MHz
		unsigned long tuneBwMHz; // Hz
		unsigned long tuneResolutionMHz;	// MHz
		unsigned long tuneCrossOverMHz;		// MHz
		unsigned long maxFPI;
		unsigned long maxAntennaInputs;
		unsigned long maxModes;
		unsigned long firmwareVersion;
		unsigned long fpgaVersion;
		unsigned long flashVersion;
		unsigned long firmwareDate;
		unsigned long requiredTransferBufferSize;
		unsigned long max10UniqueIdLow;
		unsigned long max10UniqueIdHigh;
		unsigned long rfCalTableIndexLimit;
		unsigned long ifCalTableIndexLimit;
		unsigned long normalAttenCalTableIndexLimit;
		unsigned long ruralAttenCalTableIndexLimit;
		unsigned long urbanAttenCalTableIndexLimit;
		unsigned long congestedAttenCalTableIndexLimit;
		unsigned long calGenTableIndexLimit;
		unsigned long interfaceType;
		unsigned long rxProcessor;
		unsigned char tNumber[16];
	};

	struct SRxSettings
	{
		unsigned long currentAntennaSetting;
		unsigned long currentAttenuation;
		unsigned long currentBandWidth;
		unsigned long currentFirmwareVersion;
		unsigned long currentFpgaVersion;
		unsigned long currentFlashVersion;
		unsigned long currentFirmwareDate;
		unsigned long currentFrequency;
		unsigned long currentIf1Frequency;
		unsigned long currentIf2Frequency;
		unsigned long currentLo1Frequency;
		unsigned long currentLo2Frequency;
		unsigned long currentFPI;
		unsigned long currentMode;
		unsigned long currentPsFreqLow;
		unsigned long currentPsFreqHigh;
		unsigned long currentLnaStatus;
		unsigned long currentLoLockStatus;
		unsigned long currentSpectralSense;
	};

	struct SRxTable
	{
		unsigned long index;
		unsigned long psFreqLow;
		unsigned long psFreqHigh;
		unsigned long firstIF;
		unsigned long lo1HiLo;
		unsigned long lo1FreqLow;
		unsigned long lo1FreqHigh;
		unsigned long lo2HiLo;
		unsigned long lo2Freq;
		unsigned long secondIF;
		unsigned long spectralSense;
		unsigned long freqLow;
		unsigned long freqHigh;
		unsigned long calIfTableIndex;
		unsigned long calAttenNormalTableIndex;
		unsigned long calAttenRuralTableIndex;
		unsigned long calAttenUrbanTableIndex;
		unsigned long calAttenCongestedTableIndex;
	};

	struct SFpiTestResults
	{
		unsigned long testStatus;			// Test status defined by enum EBistTestsPerFPI (commands2630.h)
		unsigned long signalTestFrequency;	// Tuned frequency for FPI test
		float inputSignalLevel;				// Cal Gen. input signal level
		float outputSignalLevel;			// Output signal level detected for test
		float signalAboveThresholdValue;	// Signal must be above this threshold
											// to be considered present.

		unsigned long rssiPath;				// RSSI numbers detecting a valid signal.
											// Direct path should reveal RSSI(s) (2 and 6)
											// Conversion path should reveal RSSI(s) (1 3 5 7)
											// between 80 and 3Ghz and
											// (1 4 5 7) above 3 GHz.

		unsigned char attenuationUsed;		// Attenuation setting for FPI test.
		unsigned char modeUsed;				// Attenuation mode used for FPI test.
		unsigned char selectedRfInput;		// Either HFCAL or VUSHFCAL

		unsigned char bandWidthUsed;		// Either 10 or 80 IBW. This is deternined
											// by the bandwidth setting prior to running BIST.
	};

	struct SBistExt1
    {
	  unsigned long bistVeresion = 0;
      unsigned long dacTableVersion = 0;
      float tempSensor1 = 0;
      float tempSensor2 = 0;
      unsigned long tempSensorTest = 0;
      unsigned long inputReference = 0;
      unsigned long outputReference = 0;
      float rssiValues[ MAX_FPI_ROWS ][ 7 ];
    } ;


	struct SBistResults
	{
		unsigned long firmwareVersion;
		unsigned long firmwareDate;
		unsigned long fpgaVersion;
		unsigned long attenTableNormalVersion;
		unsigned long attenTableRuralVersion;
		unsigned long attenTableUrbanVersion;
		unsigned long attenTableCongestedVersion;
		unsigned long fpiTable10IbwVersion;
		unsigned long fpiTable80IbwVersion;
		unsigned long psTableVersion;
		unsigned long lo1TableVersion;
		unsigned long lo2TableVersion;
		unsigned long calGenTableVersion;
		unsigned long bistRssiTableVersion;
		unsigned long rfCalTableNoLnaVersion;
		unsigned long rfCalTableLnaVersion;
		unsigned long ifCalTableVersion;
		unsigned long attenCalTableNormalVersion;
		unsigned long attenCalTableRuralVersion;
		unsigned long attenCalTableUrbanVersion;
		unsigned long attenCalTableCongestedVersion;
		unsigned long calGenDataTableVersion;
		unsigned long tempCompTableNoLnaVersion;
		unsigned long tempCompTableLnaVersion;
		unsigned long vtfTableVersion;
		unsigned long qspiFlashStatus;			// value represented by EFlashStatus
		unsigned long qspiFlashSize;			// hex value in megabytes
		unsigned long qspiAvailableFlashPages; 	// decimal value of free pages
		unsigned long qspiFlashLockStatus;
		float VUSHFCALSSI;						// Signal strength of VUSHFCAL input in dB
		float HFCALSSI;							// Signal strength of HFCAL input in dB

		unsigned long calGenTest;				// Bit 0: 1=VUSHFCAL present 0=not present
												// Bit 1: 1=HFCAL present 0=not present

		unsigned long bandWidthTest;			// Bit0: 1=10 IBW test past 0=Failed
												// Bit1: 1=80 IBW test past 0=Failed

		unsigned long bandWidthTestFrequency;	//Tuned frequency used for IBW test.
		float bandWidthTestSignalLevel;			//Avg Signal level detected during test
		float bandWidthTestBelowThreshold;		//Threshold in dB used to conduct IBW test
		float voltages[8];						// Eight primary voltages for the 2630.

		unsigned long maxFpi;
		SFpiTestResults scanTest[MAX_FPI_ROWS];	// Contains test results for each FPI. (maxFPI)												// See SFpiTestResults struct above.
		SBistExt1 bistExtend1;
	};


	struct SFreqPlanTable2
	{
		SRxTable rows[MAX_FPI_ROWS];
	};

	struct SGetLoFreqs
	{
		unsigned long lo1Frequency;
		unsigned long lo2Frequency;
	};


	struct SCalTable
	{
		static const size_t MAX_CAL_ITEMS = UCHAR_MAX;
		unsigned short idx;
		ECalibrationType calType;
		unsigned char numData;
		union
		{
			struct
			{
				float fStartValueMHz;
				float fStepValueMHz;
			};
			struct
			{
				unsigned long ulStartValue;
				unsigned long ulStepValue;
			};
		};
		float data[MAX_CAL_ITEMS];
	};

	// Message layout
#if 0
	struct SHeaderShort
	{
		unsigned char body;
		unsigned char numPackets;
		ECommand command;
		EType msgType;
	};

	struct SHeaderShort16
	{
		unsigned char body0;
		unsigned char body1;
		ECommand command;
		EType msgType;
	};

	struct SHeaderLong
	{
		unsigned short numPackets;
		ECommand command;
		EType msgType;
	};
#endif
	// Only contains "long" messages - used to make sure that the body is large enough for the largest messages
	union UBody
	{
		SRxCapabilities rxCapabilities;
		SRxSettings rxSettings;
		SFreqPlanTable2 freqPlanTable;
		SCalTable calTable;
	};

	union UHeader
	{
		struct SHeaderShort
		{
			unsigned char body;
			unsigned char numPackets;
			ECommand command;
			EType msgType;
		};

		struct SHeaderShort16
		{
			unsigned char body0;
			unsigned char body1;
			ECommand command;
			EType msgType;
		};

		struct SHeaderLong
		{
			unsigned short numPackets;
			ECommand command;
			EType msgType;
		};
		unsigned long header;
		SHeaderShort shortHdr;
		SHeaderShort16 shortHdr16;
		SHeaderLong longHdr;
	} hdr;
	static_assert(sizeof(hdr) == 4, "header size err");

	unsigned long body[sizeof(UBody) / 4];
	static_assert(sizeof(body) == sizeof(UBody), "body size error");
};

static_assert(offsetof(S2630Msg, body) == sizeof(S2630Msg::UHeader), "hdr-body padding");

// TODO: Remove these eventually
static_assert(offsetof(S2630Msg, hdr) == 0, "err");
static_assert(offsetof(S2630Msg, hdr.header) == 0, "err");
static_assert(offsetof(S2630Msg, hdr.shortHdr.msgType) == 3, "err");
static_assert(offsetof(S2630Msg, hdr.shortHdr.command) == 2, "err");
static_assert(offsetof(S2630Msg, hdr.shortHdr.numPackets) == 1, "err");
static_assert(offsetof(S2630Msg, hdr.shortHdr.body) == 0, "err");
static_assert(offsetof(S2630Msg, hdr.longHdr.msgType) == 3, "err");
static_assert(offsetof(S2630Msg, hdr.longHdr.command) == 2, "err");
static_assert(offsetof(S2630Msg, hdr.longHdr.numPackets) == 0, "err");
//static_assert(sizeof(S2630Msg) == 3604, "err");


