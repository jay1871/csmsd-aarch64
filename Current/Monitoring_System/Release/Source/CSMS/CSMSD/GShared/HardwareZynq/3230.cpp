/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"
#include <sys/mman.h>

#include "3230.h"
#include "Failure.h"
#include "Log.h"
#include "Utility.h"
#include "CsmsDigi.h"
#include "Config.h"

// Static data
C3230::SConfigData C3230::m_configData;

//////////////////////////////////////////////////////////////////////
//
// Statics
//
using namespace C3230Fdd;
constexpr const unsigned long CDemodParams::DEMOD_BANDWIDTHS[][2];
constexpr const unsigned long CDemodControl::DEMOD_CHAN_ON_OFF[];

#ifdef CSMS_DEBUG
C3230::SIntrStatus C3230::s_intr[C_INTR_ID_SZ];
size_t C3230::s_psdFpgaSetCnt = 0;
size_t C3230::s_psdSampleCnt = 0;
bool C3230::s_scratchRegValid = false;
volatile unsigned long* C3230::s_scratchRegs = nullptr;
bool C3230::s_trace3220 = false;
#endif

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C3230::C3230(bool dfSlave, int allowShutdown) :
	m_setSeconds(false),
	m_allowShutdown(allowShutdown),
	m_decimation(0, 0, 0, 0, 0, 0),
	m_direct(false),
	m_DMAregs(nullptr),
	m_DMAregSize(0),
	m_driverDesc(-1),
	m_freq(0),
	m_hasTimestamp(false),
	m_invertSpectrumData(false),
	m_master(false),
	m_memDesc(-1),
	m_regs(nullptr),
	m_regSize(0),
	m_sampleRate(ADC_CLOCK_RATE),
	m_sharedMemory(SHMEM_NAME, 0),
	m_shift(false),
	m_shutdown(false),
	m_versionInfo(),
	m_waitDone(false),
	m_dbg(false),
	m_newDFmode(true)


{
//	if (sim)
//	{
//		printf("C3230 is using simulator!!!\n");
//		return;
//	}
	if (m_sharedMemory.IsMaster())
	{
		m_master = true;
		printf("C3230 I will be master\n");
	}
	else
	{
		printf("C3230 VCP is master\n");
	}

	m_configData.sampleRate = ADC_CLOCK_RATE;

	for (size_t ch = 0; ch < NUM_INTERRUPT_THREADS; ++ch)
	{
		m_callbacks[ch] = nullptr;
		m_callbackParams[ch] = nullptr;
		m_waitMaskAndCount[ch] = SetWaitMask(ECsmsDigiInterruptSource(0xffffffff), 0xffffffff);
	}

	m_driverDesc = open(csmsDigiDevice, O_RDWR);
	if (m_driverDesc < 0)
	{
		CLog::Log(CLog::ERRORS, "C3230: Failed to open %s: %s (%d)", csmsDigiDevice, std::strerror(errno), errno);
		return;
	}

	// Get driver version and instance
	CsmsDigiGetDriverVersion driverVersion;
	if (ioctl(m_driverDesc, IOCTL_CSMSDIGI_GET_DRIVER_VERSION, &driverVersion) == -1)
	{
		close(m_driverDesc);
		m_driverDesc = -1;
		CLog::Log(CLog::ERRORS, "C3230: IOCTL_CSMSDIGI_GET_DRIVER_VERSION failed");
		return;
	}
	if (driverVersion.version < MIN_CSMSDIGI_VERSION)
	{
		char s[21];
		snprintf(s, 21, "%08lx vs %08lx", MIN_CSMSDIGI_VERSION, driverVersion.version);
		std::string err("CsmsDigi driver version must be at least ");
		err += s;
		throw std::runtime_error(err);
	}
	m_versionInfo.driverRevision = driverVersion.version;
	m_versionInfo.driverDatestamp = driverVersion.date;
	TRACE("Driver version: %u.%u.%hx\n", m_versionInfo.driverRevision.majorVersion, m_versionInfo.driverRevision.revision, m_versionInfo.driverRevision.subrevision);
	TRACE("Driver date: %lu-%02lu-%02lu\n", m_versionInfo.driverDatestamp.year, m_versionInfo.driverDatestamp.month, m_versionInfo.driverDatestamp.date);

	CsmsDigiGetDriverInstanceInfo driverInstanceInfo;
	memset(&driverInstanceInfo, 0, sizeof(driverInstanceInfo));
	if (ioctl(m_driverDesc, IOCTL_CSMSDIGI_GET_DRIVER_INSTANCE_INFO, &driverInstanceInfo) == -1)
	{
		close(m_driverDesc);
		m_driverDesc = -1;
		CLog::Log(CLog::ERRORS, "C3230: IOCTL_CSMSDIGI_GET_DRIVER_INSTANCE_INFO failed");
		return;
	}
	TRACE("Driver instance:%lu mask:%lx\n", driverInstanceInfo.driverInstance, driverInstanceInfo.interruptMask);

	// Get register mapping (fpga registers)
	CsmsDigiRegs regs;
	if (ioctl(m_driverDesc, IOCTL_CSMSDIGI_GET_REGS, &regs) == -1 || regs.Regs == 0 || regs.size == 0)
	{
		close(m_driverDesc);
		m_driverDesc = -1;
		CLog::Log(CLog::ERRORS, "C3230: IOCTL_CSMSDIGI_GET_REGS failed");
		return;
	}
	auto ret = mmap(NULL, regs.size, PROT_READ | PROT_WRITE, MAP_SHARED, m_driverDesc, regs.Regs);
	if (ret == MAP_FAILED)
	{
		close(m_driverDesc);
		m_driverDesc = -1;
		CLog::Log(CLog::ERRORS, "C3230: Failed to map registers: %s (%d)", std::strerror(errno), errno);
		return;
	}
	m_regs = static_cast<volatile unsigned long*>(ret);
	m_regSize = regs.size;
	CLog::Log(CLog::INFORMATION, "C3230: User space regs address: %p size %u", m_regs, m_regSize);

#ifdef CSMS_DEBUG
	if (ioctl(m_driverDesc, IOCTL_CSMSDIGI_GET_SCRATCH_REGS, &regs) == -1 || regs.Regs == 0 || regs.size == 0)
	{
		close(m_driverDesc);
		m_driverDesc = -1;
		CLog::Log(CLog::ERRORS, "C3230: IOCTL_CSMSDIGI_GET_SCRATCH_REGS failed");
	}
	else
	{
		s_scratchRegValid = true;
	}

	if(s_scratchRegValid)
	{
		ret = mmap(NULL, regs.size, PROT_READ | PROT_WRITE, MAP_SHARED, m_driverDesc, regs.Regs);

		if (ret == MAP_FAILED)
		{
			close(m_driverDesc);
			m_driverDesc = -1;
			CLog::Log(CLog::ERRORS, "C3230: Failed to map (s_scratchRegs)registers: %s (%d)", std::strerror(errno), errno);
			s_scratchRegValid = false;
			ASSERT(false);
		}
		else
		{
			ASSERT(regs.size >= 34);
			ScratchRegsOutTrace();
		}

		s_scratchRegs = static_cast<volatile unsigned long*>(ret);
	}


#endif

	// Get register mapping (CDMA registers)
	CsmsDigiRegs DMAregs;
	if (ioctl(m_driverDesc, IOCTL_CSMSDIGI_GET_CDMA_REGS, &DMAregs) == -1 || DMAregs.Regs == 0 || DMAregs.size == 0)
	{
		close(m_driverDesc);
		m_driverDesc = -1;
		CLog::Log(CLog::ERRORS, "C3230: IOCTL_CSMSDIGI_GET_CDMA_REGS failed");
		return;
	}
	auto DMAret = mmap(NULL, DMAregs.size, (PROT_READ | PROT_WRITE ), MAP_SHARED, m_driverDesc, DMAregs.Regs);
	if (DMAret == MAP_FAILED)
	{
		close(m_driverDesc);
		m_driverDesc = -1;
		CLog::Log(CLog::ERRORS, "C3230: Failed to map DMA registers: %s (%d)", std::strerror(errno), errno);
		return;
	}
	m_DMAregs = static_cast<volatile unsigned long*>(DMAret);
	m_DMAregSize = DMAregs.size;
	CLog::Log(CLog::INFORMATION, "C3230: User space DMA regs address: %p size %u", m_DMAregs, m_DMAregSize);

	// Get device id, firmware revision, firmware type
	m_versionInfo.deviceId = PeekUser(EReg::GLOBAL_DEVICEID);
	m_versionInfo.boardRevision = static_cast<unsigned short>(PeekUser(EReg::GLOBAL_BOARDREVISION) & 0xffff);
	m_versionInfo.fwDatestamp = PeekUser(EReg::GLOBAL_DATESTAMP);
	auto fwRevision = PeekUser(EReg::GLOBAL_FIRMWAREREVISION);

	if (fwRevision < MIN_3230_VERSION)
	{
		char s[21];
		snprintf(s, 21, "%08lx vs %08lx", MIN_3230_VERSION, fwRevision);
		std::string err("3230 Firmware version must be at least ");
		err += s;
		throw std::runtime_error(err);
	}
	m_versionInfo.fwRevision = fwRevision;

	switch (PeekUser(EReg::GLOBAL_FIRMWAREIMAGE))
	{
	case 0:
		m_versionInfo.type = SVersionInfo::EFwType::GOLDEN;
		break;

	case 1:
		m_versionInfo.type = SVersionInfo::EFwType::MICRO_SMS;
		break;

	default:
		m_versionInfo.type = SVersionInfo::EFwType::UNKNOWN;
		break;
	}
	m_versionInfo.totalDDCs = static_cast<unsigned char>(PeekUser(EReg::DDC_TOTALDDCS) & 0x000f);
	m_versionInfo.audioDDCs = (m_versionInfo.totalDDCs > 3 ? m_versionInfo.totalDDCs - 3 : 0);

	TRACE("GLOBAL_DEVICEID: %x-%x-%x-%x-%x\n",
		m_versionInfo.deviceId.revision, m_versionInfo.deviceId.family, m_versionInfo.deviceId.subFamily, m_versionInfo.deviceId.device,
		m_versionInfo.deviceId.manufacturerId);
	TRACE("GLOBAL_BOARDREVISION: %hu\n", m_versionInfo.boardRevision);
	TRACE("GLOBAL_DATESTAMP: %lu-%02lu-%02lu\n", m_versionInfo.fwDatestamp.year, m_versionInfo.fwDatestamp.month, m_versionInfo.fwDatestamp.date);
	TRACE("GLOBAL_FIRMWAREREVISION: %u.%u.%hx\n", m_versionInfo.fwRevision.majorVersion, m_versionInfo.fwRevision.revision, m_versionInfo.fwRevision.subrevision);
	TRACE("GLOBAL_FIRMWAREIMAGE: %u\n", (unsigned int) m_versionInfo.type);
	TRACE("DDC_TOTALDDCS: %u audio:%u\n", m_versionInfo.totalDDCs, m_versionInfo.audioDDCs);

	m_configData.numAudioChannels = m_versionInfo.audioDDCs;
	m_configData.isLoaded = true;

	// Init arrays
	for (unsigned char ch = 0; ch < m_versionInfo.totalDDCs; ++ch)
	{
		m_ddc.push_back({0, 0, false, false});
		m_IQOn[ch]=false;
	}
	m_adm.resize(m_versionInfo.audioDDCs);
	m_iqNarrowadm.resize(m_versionInfo.audioDDCs);
	m_demod.resize(m_versionInfo.audioDDCs);

	// Reset board - must be done after register mapping
	TRACE("Reset 3230\n");
	if (!Reset3230(dfSlave))
	{
		close(m_driverDesc);
		m_driverDesc = -1;
		CLog::Log(CLog::ERRORS, "C3230: Board is not ready for use");
		return;
	}

	// Get DDR3 and DMA memory mapping

	if (m_master)
	{
		SetLed(true);


		// PL DDR3 for sequencer
		CsmsDigiDMAMemory memory;
		if (ioctl(m_driverDesc, IOCTL_CSMSDIGI_GET_DMA_MEMORY, &memory) == -1 || memory.Mem == 0 || memory.size == 0)
		{
			close(m_driverDesc);
			m_driverDesc = -1;
			CLog::Log(CLog::ERRORS, "C3230: IOCTL_CSMSDIGI_GET_DMA_MEMORY failed");
			return;
		}
		m_sbuffer.Init(reinterpret_cast<off_t>(memory.Mem));

		// Map 128 Mb memory for data ddr3
		CPLddr3 ddr3;
		size_t ddr3Size = 0x8000000;
		if (!m_sbuffer.MapMemory(m_driverDesc,memory.size, ddr3Size, ddr3))
		{
			close(m_driverDesc);
			m_driverDesc = -1;
			return;
		}
		//m_NewDFIQaddrbuffer.Init(reinterpret_cast<off_t>(memory.Mem)+ddr3Size); // add 128 MB ddr 3 prev allocated, this is only used to hold physical address
		// allocate additional to hold new df iq address when not in iq mode (new df psd mode still needs iq address programmed)
		CPLddr3 newDFIQaddr;
		size_t IQddr3Size = 0x8000000;
		if (!m_sbuffer.MapMemory(m_driverDesc,memory.size, IQddr3Size, newDFIQaddr))
		{
			close(m_driverDesc);
			m_driverDesc = -1;
			return;
		}

		// DMA memory for sequencer
		CsmsDigiDMAMemory DMAmemory;
		// No IOCTL to get addresses
		DMAmemory.Mem = (void*)0x30000000;		// Note that this should come from system somehow
		DMAmemory.size = 0x10000000; // total size expanded from 128 MB to 256MB
		size_t DMAusedsize = 0x8000000; // 128 used for sequencer

		m_sdma.Init(reinterpret_cast<off_t>(DMAmemory.Mem));

		// Map DMA
		uncached_ptr dma;
		if ((m_memDesc = open("/dev/mem", O_RDWR)) < 0)
		{
			close(m_driverDesc);
			m_driverDesc = -1;
			return;
		}

		if (!m_sdma.MapMemory(m_memDesc, DMAmemory.size, DMAusedsize, dma))
		{
			close(m_driverDesc);
			m_driverDesc = -1;
			return;
		}




		m_sbuffer.Assign(ddr3, ddr3Size, reinterpret_cast<off_t>(memory.Mem));	// m_sbuffer will not be used
		m_sdma.Assign(dma, DMAusedsize, reinterpret_cast<off_t>(DMAmemory.Mem));

		// Map 4 Mb memory for each audio ddr3
		m_audioMem.resize(m_versionInfo.audioDDCs);
		m_iqNarrowBandMem.resize(m_versionInfo.audioDDCs);


		for (unsigned char ch = 0; ch < m_versionInfo.audioDDCs; ++ch)
		{
			m_audioMem[ch].size = 0x01000000;		// 16 MB per audio channel
			m_audioMem[ch].physicalAddress = m_sdma.GetNextPhysicalAddress();	//	m_nextPhysicalAddress;
			uncached_ptr chdma;
			if (!m_sdma.MapMemory(m_memDesc, DMAmemory.size, m_audioMem[ch].size, chdma))
			{
				close(m_driverDesc);
				m_driverDesc = -1;
				return;
			}
			m_audioMem[ch].psddr3 = chdma;
			m_audioMem[ch].psddr3End = m_audioMem[ch].psddr3 + m_audioMem[ch].size / sizeof(unsigned long);
			m_audioMem[ch].outputRate = 0;
		}

		for (unsigned char ch = 0; ch < m_versionInfo.audioDDCs; ++ch)
		{
			m_iqNarrowBandMem[ch].size = 0x01000000;		// 16 MB per audio channel
			m_iqNarrowBandMem[ch].physicalAddress = m_sdma.GetNextPhysicalAddress();	//	m_nextPhysicalAddress;
			uncached_ptr chdma;
			if (!m_sdma.MapMemory(m_memDesc, DMAmemory.size, m_iqNarrowBandMem[ch].size, chdma))
			{
				close(m_driverDesc);
				m_driverDesc = -1;
				return;
			}
			m_iqNarrowBandMem[ch].psddr3 = chdma;
			m_iqNarrowBandMem[ch].psddr3End = m_iqNarrowBandMem[ch].psddr3 + m_iqNarrowBandMem[ch].size / sizeof(unsigned long);
			m_iqNarrowBandMem[ch].outputRate = 0;
		}

		/*for (unsigned char ch = 0; ch < m_versionInfo.audioDDCs; ++ch)
		{
			m_iqNarrowBandMem[ch].size = 0x01000000;		// 16 MB per audio channel
			m_iqNarrowBandMem[ch].physicalAddress = m_sbuffer.GetNextPhysicalAddress();	//	m_nextPhysicalAddress;
			//uncached_ptr chdma;
			//if (!m_sdma.MapMemory(m_memDesc, DMAmemory.size, m_iqNarrowBandMem[ch].size, chdma))
			if (!m_sbuffer.MapMemory(m_driverDesc, memory.size, m_iqNarrowBandMem[ch].size, m_iqNarrowBandMem[ch].ddr3))
			{
				close(m_driverDesc);
				m_driverDesc = -1;
				return;
			}
			m_iqNarrowBandMem[ch].ddr3End = m_iqNarrowBandMem[ch].ddr3 + m_iqNarrowBandMem[ch].size / sizeof(unsigned long);
			m_iqNarrowBandMem[ch].outputRate = 0;
		}*/


		// set up for wide band ddc
		m_iqWideBandMem.size = 0x04000000;
		m_iqWideBandMem.physicalAddress = m_sdma.GetNextPhysicalAddress();
		uncached_ptr chdma;
		if (!m_sdma.MapMemory(m_memDesc, DMAmemory.size, m_iqWideBandMem.size, chdma))
		{
			close(m_driverDesc);
			m_driverDesc = -1;
			return;
		}
		m_iqWideBandMem.psddr3 = chdma;
		m_iqWideBandMem.psddr3End = m_iqWideBandMem.psddr3 + m_iqWideBandMem.size / sizeof(unsigned long);
		m_iqWideBandMem.outputRate = 0;

		m_deviceDna[0] = PeekUser(EReg::GLOBAL_DEVICEDNA_UPPER);
		m_deviceDna[1] = PeekUser(EReg::GLOBAL_DEVICEDNA_LOWER);

		TRACE("GLOBAL_DEVICEDNA: %04lx %04lx\n", m_deviceDna[0], m_deviceDna[1]);

		SetLed(false);

		// Restart threads
		StopThreads();
		StartThreads();
		totalinterrupt = 0; // debug only
		//totalagcint = 0;	// debug only
		totaldfscan = 0;	// debug only
		totaltimediff = 0 ;
	}

	return;

}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C3230::~C3230(void)
{
	TRACE("C3230 dtor entered\n");

	StopThreads();

	SetLed(false);
	SetFaultStatusLed(false);

	if (m_regs != nullptr && m_regSize != 0)
	{
		munmap(const_cast<unsigned long*>(m_regs), m_regSize);
	}

	if (m_DMAregs != nullptr && m_DMAregSize != 0)
	{
		munmap(const_cast<unsigned long*>(m_DMAregs), m_DMAregSize);
	}

	//sleep(15); //debug only

	if (m_driverDesc >= 0)
	{
		close(m_driverDesc);
	}

	TRACE("C3230 dtor exited\n");
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Adjust decimations rate to closest legal value
//
unsigned long C3230::AdjustAudioDdcDecimation(const Units::Frequency& bw, const SDecimation& decimation)
{
	if (bw <= 0)
	{
		return CDdcDecimation::MAX_AUDIO_DDC_CICDECIMATION;
	}
	// nonCicRate is the sample rate if the DDC CIC was = 1
	unsigned long nonCicRate = ADC_CLOCK_RATE / decimation.ddcFIRDecimation;	// FIR filter is 4x

	unsigned long iqCic = static_cast<unsigned long>(0.8 * nonCicRate / bw.Hz<unsigned long>(true));
	if (iqCic < CDdcDecimation::MIN_AUDIO_DDC_CICDECIMATION)
	{
		iqCic = CDdcDecimation::MIN_AUDIO_DDC_CICDECIMATION;
	}
	else if (iqCic >= CDdcDecimation::MAX_AUDIO_DDC_CICDECIMATION)
	{
		iqCic = CDdcDecimation::MAX_AUDIO_DDC_CICDECIMATION;
	}
	else
	{
		while (nonCicRate / iqCic * iqCic != nonCicRate && iqCic > CDdcDecimation::MIN_AUDIO_DDC_CICDECIMATION)
		{
			--iqCic;
		}
	}
	return iqCic;
}


//////////////////////////////////////////////////////////////////////
//
// Adjust inversion for direct vs indirect input to digitizer
//
bool C3230::AdjustInversion(_In_ bool /*direct*/, _In_ bool inverted)
{
	// Note: for direct path, finalIfFrequency will be positive and should be negative, so sense needs to be reversed
	//       for non-direct path, finalIfFrequency is above Nyquist rate and so sense needs to be inverted.
	return !inverted;
}


/////////////////////////////////////////////////////////////////////
//
// Blink the LED for specified number of seconds (delays)
//
bool C3230::BlinkLedStatic(unsigned char seconds)
{
	auto driverDesc = open(csmsDigiDevice, O_RDWR);
	volatile unsigned long* regsmap;
	if (MapRegisters(driverDesc, regsmap))
	{
		bool on = true;
		for (size_t i = 0; i < 2 * seconds; ++i)
		{
			Utility::Delay(500000);		// .5 seconds
			unsigned long data = regsmap[static_cast<unsigned long>(EReg::BIST_GPIOREGISTER)];	// current
			regsmap[static_cast<unsigned long>(EReg::BIST_GPIOREGISTER)] =
				(on ? data |= CBistGPIORegister::LED2_P : data &= ~CBistGPIORegister::LED2_P);
			on = !on;
		}
		close(driverDesc);
	}
	return true;
}


/////////////////////////////////////////////////////////////////////
//
// Helper function - calculate fft delay value
//
unsigned long C3230::CalcFftDelay(unsigned long count, bool withPsd, bool withFft)
{
	return 15;

	if (withPsd)
	{
		if (withFft)
		{
			return (count == 65536 ? 9 : 10);
		}
		else
		{
			return 5;
		}
	}
	else
	{
		return 0;
	}
}


/////////////////////////////////////////////////////////////////////
//
// Helper function
//
unsigned long C3230::CalcFreqWord(Units::Frequency freq, Units::Frequency sampleRate) const
{
//	TRACE("CalcFreqWord %.6f %lu\n", freq.Hz<double>(), static_cast<unsigned long>(freq.Hz<double>() * 4294967296 / sampleRate.Hz<double>()));
	return static_cast<unsigned long>(freq.Hz<double>() * 4294967296 / sampleRate.Hz<double>());
}


/////////////////////////////////////////////////////////////////////
//
// Calculate power from threshold counts assuming noise
//
float C3230::CalcNoiseDbm(unsigned int adcCount, const unsigned int (&counts)[4], const unsigned short (&thresh)[4])
{
	static const float SQRT2 = sqrtf(2);

	float sum = 0;
	unsigned int numGood = 0;

	for (unsigned int counter = 0; counter < 4; ++counter)
	{
		float fraction = float(counts[counter]) / adcCount;

		if (fraction > 0.05f && fraction < 0.95f)
		{
			float sigma = thresh[counter] / (SQRT2 * InvErf(1 - fraction));

			sum += sigma;
			++numGood;
		}
	}
	float power;
	if (numGood > 0)
	{
		power = 20 * log10(SQRT2 * sum / numGood / ADC_FULL_SCALE_COUNT) + ADC_FULL_SCALE_DBM;
	}
	else if (float(counts[2]) / adcCount < 0.5f)
	{
		power = -std::numeric_limits<float>::infinity(); // underflow
	}
	else
	{
		power = std::numeric_limits<float>::infinity(); // overflow
	}

	return power;
}


/////////////////////////////////////////////////////////////////////
//
// Calculate power from threshold counts assuming single tone
//
float C3230::CalcToneDbm(unsigned int adcCount, const unsigned int (&counts)[4], const unsigned short (&thresh)[4])
{
	float sum = 0;
	unsigned int numGood = 0;

	for (unsigned int counter = 0; counter < 4; ++counter)
	{
		float fraction = float(counts[counter]) / adcCount;

		if (fraction > 0.05f && fraction < 0.95f)
		{
			float peak = thresh[counter] / sin(Units::HALF_PI * (1 - fraction));

			if (peak <= ADC_FULL_SCALE_COUNT)
			{
				sum += peak;
				++numGood;
			}
		}
	}
	float power;
	if (numGood > 0)
	{
		power = 20 * log10(sum / numGood / ADC_FULL_SCALE_COUNT) + ADC_FULL_SCALE_DBM;
	}
	else if (float(counts[2]) / adcCount < 0.5f)
	{
		power = -std::numeric_limits<float>::infinity(); // underflow
	}
	else
	{
		power = std::numeric_limits<float>::infinity(); // overflow
	}

	return power;
}

//////////////////////////////////////////////////////////////////////
//
// Check that memory gets written properly by fpga
//
bool C3230::CheckMemory(void)
{
	m_memoryCheck.assign(6, 0);

	if (!m_master)
	{
		printf("C3230::CheckMemory can only be called by master\n");
		return false;
	}


	SDecimation decimations(1, 3, 5, 6, 1, 1);	// Full 80 MHz
	SetDecimation(decimations);
	Tune(Units::Frequency(260000000), false, false, false);
	unsigned long sampleCount;
	if (!m_sdma.WaitForBufferSpace(512, sampleCount) || !m_sdma.MarkSamplesInUse(sampleCount, 512))
	{
		printf("C3230::CheckMemory WaitForBufferSpace failed\n");
		return false;
	}

	m_memoryCheck[0] = 1;	// First check passed

	// Fill PLDDR3 memory @ sampleCount with pattern
	m_sbuffer.FillMemory(sampleCount, 512);

	// Read back PFDDR3 memory to verify
	std::vector<unsigned long> data;
	m_sbuffer.ReadMemory(sampleCount, 512, data);
	for (size_t i = 0; i < 512; ++i)
	{
		if (data[i] != i)
		{
			printf("C3230::CheckMemory first readback failed\n");
			return false;
		}
	}
	m_memoryCheck[1] = 1;	// second check passed

	// Fire the sequencer and collect the data
	auto fftDelay = CalcFftDelay(512, true, false);
	std::vector<unsigned long> pattern(1, 0);	// set antenna to term
	StartSampling(sampleCount, 512, 1844, 15, fftDelay, false, true, false, 0, Units::Timestamp(), EDfMode::NODF, pattern);
	// Adjust the collectionTimeSec used for timeout
	if (!WaitForSequencer(false, true, false, true, 0., Units::Timestamp()))
	{
		m_sdma.DoneWithSamples(sampleCount);
		printf("C3230::CheckMemory waitforsequencer failed\n");
		return false;
	}
	m_memoryCheck[2] = 1;	// third check passed

	unsigned int counts[4];
	unsigned short thresh[4];
	GetThresholdCounters(counts, thresh);	// Get and ignore

	uncached_ptr found;
	auto foundData = m_sdma.FindSamples(sampleCount, 512, found);
	if (!foundData || !found)
	{
		m_sdma.DoneWithSamples(sampleCount);
		printf("C3230::CheckMemory FindSamples failed\n");
		return false;
	}
	m_memoryCheck[3] = 1;	// fourth check passed

	// Read and Compare data
	m_sbuffer.ReadMemory(sampleCount, 512, data);

	int numMatchPattern = 0;
	int numMismatchPLPS = 0;
	auto pDma = found.get();
	for (size_t i = 0; i < 512; ++i)
	{
		if (pDma[i] == i)
		{
			printf("%u %lu %lu\n", i, pDma[i], data[i]);
			++numMatchPattern;
		}
		if (pDma[i] != data[i])
		{
			printf("%u %lu\n", i, pDma[i]);
			++numMismatchPLPS;
		}
	}
	m_sdma.DoneWithSamples(sampleCount);

	if (numMatchPattern > 0)
	{
		printf("C3230::CheckMemory pattern data compare failed %d\n", numMatchPattern);
		return false;
	}

	m_memoryCheck[4] = 1;	// fifth check passed

	if (numMismatchPLPS > 0)
	{
		printf("C3230::CheckMemory PLPS data compare failed %d\n", numMismatchPLPS);
		return false;
	}

	m_memoryCheck[5] = 1;	// sixth check passed

	return true;
}

bool C3230::CheckMemory2(void)
{
	if (!m_master)
	{
		printf("C3230::CheckMemory can only be called by master\n");
		return false;
	}

	unsigned long sampleCount;

	if (!m_sdma.WaitForBufferSpace(8192, sampleCount) || !m_sdma.MarkSamplesInUse(sampleCount, 8192))
	{
		printf("C3230::CheckMemory WaitForBufferSpace failed\n");
		return false;
	}



	// Fire the sequencer and collect the data
	auto fftDelay = CalcFftDelay(8192, true, false);
	std::vector<unsigned long> pattern(1, 0);	// set antenna to term
	StartSampling(sampleCount, 8192, 7376, 60, fftDelay, false, true, false, 0, Units::Timestamp(), EDfMode::NODF, pattern);
	// Adjust the collectionTimeSec used for timeout
	if (!WaitForSequencer(false, true, false, true, 0., Units::Timestamp()))
	{
		m_sdma.DoneWithSamples(sampleCount);
		printf("C3230::CheckMemory waitforsequencer failed\n");
		return false;
	}

	unsigned int counts[4];
	unsigned short thresh[4];
	GetThresholdCounters(counts, thresh);	// Get and ignore

	uncached_ptr found;
	auto foundData = m_sdma.FindSamples(sampleCount, 8192, found);
	if (!foundData || !found)
	{
		m_sdma.DoneWithSamples(sampleCount);
		printf("C3230::CheckMemory FindSamples failed\n");
		return false;
	}
	std::vector<unsigned long> data;
	// Read and Compare data
	m_sbuffer.ReadMemory(sampleCount, 8192, data);

	int numMatchPattern = 0;
	int numMismatchPLPS = 0;
	auto pDma = found.get();
	for (size_t i = 0; i < 8192; ++i)
	{
		if (pDma[i] == i)
		{
			printf("%u %lu %lu\n", i, pDma[i], data[i]);
			++numMatchPattern;
		}
		if (pDma[i] != data[i])
		{
			printf("%u %lu\n", i, pDma[i]);
			++numMismatchPLPS;
		}
	}
	m_sdma.DoneWithSamples(sampleCount);

	if (numMatchPattern > 0)
	{
		printf("C3230::CheckMemory pattern data compare failed %d\n", numMatchPattern);
		return false;
	}


	if (numMismatchPLPS > 0)
	{
		printf("C3230::CheckMemory PLPS data compare failed %d\n", numMismatchPLPS);
		return false;
	}


	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Check for valid clocks and timestamp status, return values and optionally log errors
//
bool C3230::CheckTimestamp(bool logErrors, bool startup, unsigned long (&values)[5], bool (&errors)[5])
{
	values[0] = PeekUser(EReg::GLOBAL_HMC1035_MEASCLKFREQUENCY);
	values[1] = PeekUser(EReg::GLOBAL_SI5338_100MEASCLKFREQUENCY);
	values[2] = PeekUser(EReg::TIMESTAMP_STATUS);
	values[3] = PeekUser(EReg::TIMESTAMP_MEASUREDCLKFREQMETHOD1);
	values[4] = PeekUser(EReg::TIMESTAMP_MEASUREDCLKFREQMETHOD2);

	errors[0] = false;
	errors[1] = false;
	errors[2] = false;
	errors[3] = false;
	errors[4] = false;

	bool has3230Time = true;
	if (values[0] < C3230::ADC_CLOCK_RATE - 10 || values[0] > C3230::ADC_CLOCK_RATE + 10)
	{
		if (logErrors) CLog::Log(CLog::ERRORS, "C3230: %lu clock out of spec (%lu)", C3230::ADC_CLOCK_RATE, values[0]);
		errors[0] = true;
		has3230Time = false;
	}
	if (values[1] < 100000000ul - 3 || values[1] > 100000000ul + 3)
	{
		if (logErrors) CLog::Log(CLog::ERRORS, "C3230: %lu clock out of spec (%lu)", 100000000ul, values[1]);
		errors[1] = true;
		has3230Time = false;
	}
	auto mask = (startup ? CTimestampStatus::TIMESTAMP_STARTUP_STATUS_MASK : CTimestampStatus::TIMESTAMP_STATUS_MASK);
	auto ready = (startup ? CTimestampStatus::TIMESTAMP_STARTUP_STATUS_READY : CTimestampStatus::TIMESTAMP_STATUS_READY);
	if ((values[2] & mask) != ready)
	{
		if (logErrors) CLog::Log(CLog::ERRORS, "C3230: timestamp status error (%08lx)", values[2]);
		errors[2] = true;
		has3230Time = false;
	}
	if (values[3] < C3230::ADC_CLOCK_RATE - 10 || values[3] > C3230::ADC_CLOCK_RATE + 10)
	{
		if (logErrors) CLog::Log(CLog::WARNINGS, "C3230: %lu clock1 out of spec (%lu)", C3230::ADC_CLOCK_RATE, values[3]);
		errors[3] = true;
//		has3230Time = false;
	}
	if (values[4] < C3230::ADC_CLOCK_RATE - 10 || values[4] > C3230::ADC_CLOCK_RATE + 10)
	{
		if (logErrors) CLog::Log(CLog::WARNINGS, "C3230: %lu clock2 out of spec (%lu)", C3230::ADC_CLOCK_RATE, values[4]);
		errors[4] = true;
//		has3230Time = false;
	}
	m_hasTimestamp = has3230Time;
	return has3230Time;
}

bool C3230::CheckTimestamp(void)
{
	unsigned long values[5];
	values[0] = PeekUser(EReg::GLOBAL_HMC1035_MEASCLKFREQUENCY);
	values[1] = PeekUser(EReg::GLOBAL_SI5338_100MEASCLKFREQUENCY);
	values[2] = PeekUser(EReg::TIMESTAMP_STATUS);
	values[3] = PeekUser(EReg::TIMESTAMP_MEASUREDCLKFREQMETHOD1);
	values[4] = PeekUser(EReg::TIMESTAMP_MEASUREDCLKFREQMETHOD2);

	bool bad3230Time = (
		(values[0] < C3230::ADC_CLOCK_RATE - 10 || values[0] > C3230::ADC_CLOCK_RATE + 10) ||
		(values[1] < 100000000ul - 3 || values[1] > 100000000ul + 3) ||
		((values[2] & CTimestampStatus::TIMESTAMP_STATUS_MASK) != CTimestampStatus::TIMESTAMP_STATUS_READY)
	);

	m_hasTimestamp = !bad3230Time;
	return !bad3230Time;
}


//////////////////////////////////////////////////////////////////////
//
// Collect threshold counts
//
void  C3230::CollectCounts(bool direct, _Inout_ unsigned int& adcCount, unsigned int(&counts)[4], unsigned short(&thresh)[4])
{
#ifdef CSMS_DEBUG
	if(s_trace3220)
	{
		TRACE("C3230::CollectCounts: direct %d, adcCount %u\n",
				(direct ? 1 : 0), adcCount);
	}
#endif

	// adcCount is the number of A/D samples wanted.
	if (adcCount < 18)		// 18 = 16 + 2
		adcCount = 18;

	if (m_master)
	{
		SetLed(true);
		if ((PeekUser(EReg::AGC_STATUS) & CAgcStatus::CCDF_FIFO_EMPTY) == 0)
		{
			TRACE(_T("AGC FIFO not empty\n"));
			ResetThresholdCounters();
		}

		PokeUser(EReg::TIMESTAMP_CONTROL, 0x0000, CTimestampControl::INTERNAL_SEQUENCE_TRIG);		// Immediate start

		// Select A/D
		if (direct != m_direct)
		{
			PokeUser(EReg::SIGNAL_CONTROL, (direct ? CSignalControl::ADC_CHANNEL2_SELECT : CSignalControl::ADC_CHANNEL1_SELECT), CSignalControl::ADC_CHANNEL2_SELECT);
			m_direct = direct;
		}
		// Collect counts only
		PokeUser(EReg::AGC_WINDOWSIZE, adcCount - 2);
		PokeUser(EReg::AGC_CONTROL, CAgcControl::CCDF_FIFO_FLUSH, CAgcControl::CCDF_FIFO_FLUSH);
		PokeUser(EReg::FORMAT_CONTROL, 0, CFormatControl::ENABLE_DDCM_IQ_SAMPLES | CFormatControl::ENABLE_FRONTEND_IQ_SAMPLES);
	//	PokeUser(EReg::FORMAT_CONTROL, static_cast<unsigned long>(EFormatControl::FORMAT_FIFO_FLUSH), static_cast<unsigned long>(EFormatControl::FORMAT_FIFO_FLUSH));
		PokeUser(EReg::FFT_CONTROL, 0, CFftControl::ENABLE_PSD_OUTPUT | CFftControl::ENABLE_FFT_OUTPUT);
		PokeUser(EReg::FFT_CONTROL, 0, CFftControl::FFT_SOURCE_SELECT);
		PokeUser(EReg::FORMAT_CONTROL, 0, CFormatControl::FORMAT_SOURCE_SELECT);  // to be backward compatible before new df mode
		//printf("collect counts-- 0 for source select\n");
	//	PokeUser(EReg::FFT_CONTROL, static_cast<unsigned long>(EFftControl::FFT_FIFO_FLUSH), static_cast<unsigned long>(EFftControl::FFT_FIFO_FLUSH));

		// Set up wait-for interrupts mask
		double collectionTimeSec = (m_configData.sampleRate > 0 ? adcCount / static_cast<double>(m_configData.sampleRate) : 2.);	// sec
		if (m_waitDone)
		{
			printf("C3230::CollectCounts: m_waitDone is already true, setting to false\n");	// Should not normally happen
			m_waitDone = false;
		}
		m_waitMaskAndCount[0] = SetWaitMask(CsmsDigi_agcDone, 1);	// agc count is 1
//		TRACE("C3230::CollectCounts: Fire sequencer - %u %.6f\n", adcCount, collectionTimeSec);
		PokeUser(EReg::AGC_CONTROL, CAgcControl::AGC_BLOCK_START, CAgcControl::AGC_BLOCK_START);

		//printf("agc block started agc window size = %u", adcCount -2); // debug only
		if (!WaitForSequencer(false, false, false, true, collectionTimeSec, Units::Timestamp()))
		{
			ASSERT(false);
		}

		// Read AGC counters
		if (!GetThresholdCounters(counts, thresh))
		{
			counts[0] = counts[1] = counts[2] = counts[3] = 0;
		}
		SetLed(false);
	}
	else
	{
		counts[0] = counts[1] = counts[2] = counts[3] = 0;
		thresh[0] = thresh[1] = thresh[2] = thresh[3] = 0;
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Collect samples
//
void C3230::CollectSamples(unsigned long sampleCount, unsigned long count, unsigned long adcCount, unsigned long delay,
	bool withIq, bool withPsd, bool withFft, bool doSamples, double collectionTimeSec, unsigned long subBlockCount,
	Units::Timestamp startTime, EDfMode dfMode, const std::vector<unsigned long>& pattern)
{
	auto fftDelay = CalcFftDelay(count, withPsd, withFft);
	if (m_master)
	{
		SetLed(true);
		if (m_waitDone)
			printf("CollectSamples: m_waitDone is already true\n");

#ifdef CSMS_DEBUG
		if(s_trace3220 && s_scratchRegValid)
		{
			ScratchRegsOutTrace();
		}
#endif
		if (pattern.empty())
		{
			// Copy current antenna setting for sequencer
			std::vector<unsigned long> pattern1(1, PeekUser(EReg::SEQUENCER_CURRENT_ANTENNA) & 0x0f);
//			TRACE("Sequencer current antenna = %lu\n", pattern1[0]);
#ifdef CSMS_DEBUG

			if(!StartSampling(sampleCount, count, adcCount, delay, fftDelay,
					withIq, withPsd, withFft, subBlockCount, startTime, dfMode, pattern1))
			{
				ASSERT(false);
			}
#else
			StartSampling(sampleCount, count, adcCount, delay, fftDelay, withIq, withPsd, withFft, subBlockCount, startTime, dfMode, pattern1);
#endif
		}
		else
		{
#ifdef CSMS_DEBUG
			if(!StartSampling(sampleCount, count, adcCount, delay, fftDelay,
					withIq, withPsd, withFft, subBlockCount, startTime, dfMode, pattern))
			{
				ASSERT(false);
			}
#else
			StartSampling(sampleCount, count, adcCount, delay, fftDelay, withIq, withPsd, withFft, subBlockCount, startTime, dfMode, pattern);
#endif
		}
		// Adjust the collectionTimeSec used for timeout:
		if (count > 0)
			collectionTimeSec *= (static_cast<double>(count + DDC_EXTRA_SAMPLES_REQUIRED) / count);
		if (!pattern.empty())
			collectionTimeSec *= pattern.size();
		if(!WaitForSequencer(withIq, withPsd, withFft, doSamples, collectionTimeSec, startTime))
		{
#ifdef CSMS_DEBUG
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			TRACE("CC3230::CollectSamples: sec = %ld nsec =%ld; WaitForSequencer false\n",
								ts.tv_sec, ts.tv_nsec);

			if(s_scratchRegValid)
			{
				ScratchRegsOutTrace();
			}
			sleep(5);
			ASSERT(false);
#endif
		}
		SetLed(false);
	}
	return;

}


//////////////////////////////////////////////////////////////////////
//
// Check for FIFO overrun
//
bool C3230::FifoHasOverrun(void)
{
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Get the antenna control word
//
unsigned long C3230::GetAntennaControl(void) const
{
	return PeekUser(EReg::SEQUENCER_ANTENNA_CONTROL);
}


//////////////////////////////////////////////////////////////////////
//
// Get the antenna status that is always available
//
void C3230::GetAntennaStatus(bool& mastDown, bool& whipFault) const
{
	unsigned long status = PeekUser(EReg::SEQUENCER_ANTENNA_STATUS);
	mastDown = (status & CSequencerAntennaStatus::MAST_UP_DOWN) != 0;
	whipFault = (status & CSequencerAntennaStatus::WHIP_ANT_FAULT) != 0;
	return;
}

//////////////////////////////////////////////////////////////////////
//
// Get the antenna status
//
unsigned long C3230::GetAntennaStatus(unsigned char& antennaType) const
{
	unsigned long status;
	{
		CRegLock lock(m_driverDesc);
		const_cast<C3230*>(this)->PokeUser(EReg::SEQUENCER_CONTROL, 0, CSequencerControl::ANTENNA_WORD_WRITE);
		sleep(1);
		status = PeekUser(EReg::SEQUENCER_ANTENNA_STATUS);
		const_cast<C3230*>(this)->PokeUser(EReg::SEQUENCER_CONTROL, CSequencerControl::ANTENNA_WORD_WRITE, CSequencerControl::ANTENNA_WORD_WRITE);
	}
	antennaType = 0;
	if ((status & CSequencerAntennaStatus::K11242_D0) != 0) antennaType |= 0x01;
	if ((status & CSequencerAntennaStatus::K11242_D1) != 0) antennaType |= 0x02;
	if ((status & CSequencerAntennaStatus::K11242_D2) != 0) antennaType |= 0x04;
	if ((status & CSequencerAntennaStatus::K11242_D3) != 0) antennaType |= 0x08;
	if ((status & CSequencerAntennaStatus::K11242_D4) != 0) antennaType |= 0x10;
	if ((status & CSequencerAntennaStatus::K11242_D5) != 0) antennaType |= 0x20;
	if ((status & CSequencerAntennaStatus::K11242_D6) != 0) antennaType |= 0x40;

	return status;
};

//////////////////////////////////////////////////////////////////////
//
// Get all the available clock frequencies
//
std::vector<unsigned long> C3230::GetClockFrequencies(void) const
{
	std::vector<unsigned long> clocks;
	clocks.push_back(PeekUser(EReg::GLOBAL_HMC1035_MEASCLKFREQUENCY));
	clocks.push_back(PeekUser(EReg::GLOBAL_SI5338_200MEASCLKFREQUENCY));
	clocks.push_back(PeekUser(EReg::GLOBAL_SI5338_100MEASCLKFREQUENCY));
	clocks.push_back(PeekUser(EReg::GLOBAL_SI5338_150MEASCLKFREQUENCY));

	return clocks;
}


//////////////////////////////////////////////////////////////////////
//
// Get the current antenna
//
unsigned long C3230::GetCurrentAntenna(void) const
{
	return PeekUser(EReg::SEQUENCER_CURRENT_ANTENNA) & 0x0f;
}


//////////////////////////////////////////////////////////////////////
//
// Request and get the current time
//
bool C3230::GetCurrentTime(Units::Timestamp& timestamp) const
{
	bool has3230Timestamp = m_hasTimestamp && m_setSeconds;
	if (!has3230Timestamp)
	{
		timestamp = Units::Timestamp();
		return false;
	}
	unsigned long sampleRate = ADC_CLOCK_RATE;

	CRegLock lock(m_driverDesc);
	const_cast<C3230*>(this)->PokeUser(EReg::BIST_CONTROL, CBistControl::READ_CURRENT_TIME, CBistControl::READ_CURRENT_TIME);
	while ((PeekUser(EReg::TIMESTAMP_STATUS) & CTimestampStatus::CURRENT_TIME_VALID) == 0) ;
	unsigned long long seconds = PeekUser(EReg::TIMESTAMP_1PPSCURRENTTIMEINTEGER);
	unsigned long long offset = PeekUser(EReg::TIMESTAMP_1PPSCURRENTTIMEFRACTIONAL);
	timestamp.m_timestamp = (seconds << 32) | ((offset << 32) / sampleRate);
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Get decimation
//
double C3230::GetDecimation(void) const
{
	return m_decimation.GetTotal();
}


//////////////////////////////////////////////////////////////////////
//
// Get gain ratio
//
float C3230::GetGainRatio(bool direct) const
{
	// Direct path has 10 db gain
	return (direct ? 10.0f : 1.0f);
}


//////////////////////////////////////////////////////////////////////
//
// Get the global status register
//
unsigned short C3230::GetGlobalStatus(void) const
{
	return static_cast<unsigned short>(PeekUser(EReg::GLOBAL_STATUS) & 0xffff);
}


//////////////////////////////////////////////////////////////////////
//
// Get the estimated latency in seconds
//
float C3230::GetEstimatedLatency(Units::Frequency sampleRate, const SDecimation& decimations)
{
	auto decimation = decimations.GetTotal();
	auto front = decimations.zifCICDecimation * decimations.downResample / double(decimations.upResample);
	auto back = decimations.ddcCICDecimation * decimations.downResample / double(decimations.upResample);
	double clocks;
	if (decimations.ddcCICDecimation == 1)	// DDC is bypassed
	{
		clocks = 134 + 15 * front;
	}
	else if (decimations.upResample == 1)	// with DDC and no resampler
	{
		clocks = 134 + (16 + 12 * decimations.ddcCICDecimation) * decimations.zifCICDecimation + DDC_EXTRA_SAMPLES_REQUIRED * decimation;
	}
	else	// with DDC and resampler
	{
		clocks = 184 + (19 + 12 * back) * decimations.zifCICDecimation + DDC_EXTRA_SAMPLES_REQUIRED * decimation;
	}
	return static_cast<float>(clocks / sampleRate.Hz<double>());
}


//////////////////////////////////////////////////////////////////////
//
// Get the overall latency in clock counts
//
unsigned long C3230::GetLatencyCounter(void) const
{
	return PeekUser(EReg::FORMAT_LATENCYCNTR);
};


//////////////////////////////////////////////////////////////////////
//
// Get the memory check results
//
std::vector<unsigned char> C3230::GetMemoryChecks(void) const
{
	return m_memoryCheck;
}


//////////////////////////////////////////////////////////////////////
//
// Get board temperatures
//
std::vector<float> C3230::GetTemperatures(void) const
{
	CRegLock lock(m_driverDesc);

	std::vector<float> temps;

	const_cast<C3230*>(this)->PokeUser(EReg::BIST_CONTROL, CBistControl::ZYNQ_INTERNAL_TEMP, CBistControl::SENSOR_REG_ADDR_MASK);
	temps.push_back((PeekUser(EReg::BIST_STATUS) & CBistStatus::ADC_CODE) * 503.975f / CBistStatus::ADC_SCALE - 273.15f);

	const_cast<C3230*>(this)->PokeUser(EReg::BIST_CONTROL, CBistControl::BOARD_TEMP, CBistControl::SENSOR_REG_ADDR_MASK);
	temps.push_back(100.f * ((PeekUser(EReg::BIST_STATUS) & CBistStatus::ADC_CODE) * 2.0f / CBistStatus::ADC_SCALE - 0.75f));

	return temps;
}


//////////////////////////////////////////////////////////////////////
//
// Get board temperature limits
//
std::vector<std::array<float, 2> > C3230::GetTempLimits(void) const
{
	std::vector<std::array<float, 2> > limits(2);

	limits[0][0] = m_bistLimits[CBistControl::ZYNQ_INTERNAL_TEMP][0];
	limits[0][1] = m_bistLimits[CBistControl::ZYNQ_INTERNAL_TEMP][1];

	limits[1][0] = m_bistLimits[CBistControl::BOARD_TEMP][0];
	limits[1][1] = m_bistLimits[CBistControl::BOARD_TEMP][1];

	return limits;
}


//////////////////////////////////////////////////////////////////////
//
// Get threshold counter values for both channels
//
_Success_(return) bool C3230::GetThresholdCounters(unsigned int(&counts)[4], unsigned short(&thresh)[4])
{
	if (m_master)
	{
		GetThresholds(thresh);

		unsigned long status = PeekUser(EReg::AGC_STATUS);
		if ((status & (CAgcStatus::CCDF_FIFO_FULL | CAgcStatus::CCDF_FIFO_EMPTY)) != 0)
		{
			TRACE(_T("AGC FIFO overflow/empty: %08lx\n"), status);
			ResetThresholdCounters();
			return false;
		}
		if ((status & CAgcStatus::CCDF_FIFO_CNT) + 2 < 4)
		{
			TRACE("AGC FIFO count < 4\n");
			return false;
		}
		{
			CRegLock lock(m_driverDesc);
			for(size_t i = 0; i < 4; ++i)
			{
				counts[i] = PeekUser(EReg::AGC_CCDF);
			}
		}
	//	TRACE(_T("AGC FIFO values (%u, %u, %u, %u)\n"),
	//		counts[0], counts[1], counts[2], counts[3]);

		if (counts[0] < counts[1] || counts[1] < counts[2] || counts[2] < counts[3])
		{
			TRACE(_T("AGC FIFO values bad (%u, %u, %u, %u)\n"),
				counts[0], counts[1], counts[2], counts[3]);
			return false;
		}

		return true;
	}
	else
	{
		return false;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the AGC thresholds for both channels
//
void C3230::GetThresholds(unsigned short(&thresh)[4]) const
{
	CRegLock lock(m_driverDesc);
	unsigned long reg = PeekUser(EReg::AGC_SLICE0);
	thresh[0] = reg & 0xffff;
	thresh[1] = (reg >> 16) & 0xffff;
	reg = PeekUser(EReg::AGC_SLICE1);
	thresh[2] = reg & 0xffff;
	thresh[3] = (reg >> 16) & 0xffff;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the current timestamp
//
bool C3230::GetTimeStamp(Units::Timestamp& timestamp) const
{
	bool has3230Timestamp = m_hasTimestamp && m_setSeconds;
	if (!has3230Timestamp)
	{
		timestamp = Units::Timestamp();
		return false;
	}

	unsigned long sampleRate = ADC_CLOCK_RATE;

	unsigned long long seconds1 = PeekUser(EReg::TIMESTAMP_1PPSBLOCKSTARTTIMEINTEGER);

	if (seconds1 == 0)
	{
		return false;
	}

	unsigned long long offset = PeekUser(EReg::TIMESTAMP_1PPSBLOCKSTARTTIMEFRACTIONAL);
	unsigned long long seconds2 = PeekUser(EReg::TIMESTAMP_1PPSBLOCKSTARTTIMEINTEGER);

	if(seconds1 != seconds2)
	{
		offset = PeekUser(EReg::TIMESTAMP_1PPSBLOCKSTARTTIMEFRACTIONAL);
		TRACE("*********** seconds1 != seconds2\n");
	}

	timestamp.m_timestamp = (seconds2 << 32) | ((offset << 32) / sampleRate);

//	TRACE("GetTimeStamp %llu %llu\n", seconds2, offset);

	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Get the timestamp status register
//
unsigned long C3230::GetTimestampStatus(void) const
{
	return PeekUser(EReg::TIMESTAMP_STATUS);
}


//////////////////////////////////////////////////////////////////////
//
// Get board voltage limits
//
std::vector<std::array<float, 2> > C3230::GetVoltageLimits(void) const
{
	std::vector<std::array<float, 2> > limits;
	for (size_t i = 0; i < std::extent<decltype(m_bistVoltages)>::value; ++i)
	{
		assert(m_bistVoltages[i] < std::extent<decltype(m_bistLimits)>::value);

		std::array<float, 2> lim;
		lim[0] = m_bistLimits[m_bistVoltages[i]][0];
		lim[1] = m_bistLimits[m_bistVoltages[i]][1];
		limits.push_back(lim);
	}

	return limits;
}


//////////////////////////////////////////////////////////////////////
//
// Get board voltages
//
std::vector<float> C3230::GetVoltages(void) const
{
	CRegLock lock(m_driverDesc);

	std::vector<float> voltages;

	for (size_t i = 0; i < std::extent<decltype(m_bistVoltages)>::value; ++i)
	{
		const_cast<C3230*>(this)->PokeUser(EReg::BIST_CONTROL, m_bistVoltages[i], CBistControl::SENSOR_REG_ADDR_MASK);
		voltages.push_back((PeekUser(EReg::BIST_STATUS) & CBistStatus::ADC_CODE) * m_bistScaleFactors[m_bistVoltages[i]] / CBistStatus::ADC_SCALE);
//		TRACE("Voltage Reg:%lu Value:%lu\n", m_bistVoltages[i], PeekUser(EReg::BIST_STATUS) & CBistStatus::ADC_CODE);
	}

	return voltages;
}


//////////////////////////////////////////////////////////////////////
//
// Calculate an approximation to the inverse error function
//
float C3230::InvErf(float x)
{
	static const float A = 8 * (Units::PI - 3) / (3 * Units::PI * (4 - Units::PI));
	static const float PI_A = 8 * (Units::PI - 3) / (3 * (4 - Units::PI));
	float temp1 = log(1 - x * x);
	float temp2 = (2 / PI_A + temp1 / 2);
	float invErf = sqrt(sqrt(temp2 * temp2 - temp1 / A) - temp2);

	return x < 0 ? -invErf : invErf;
}


//////////////////////////////////////////////////////////////////////
//
// Map the digitizer registers
//
bool C3230::MapRegisters(int driverDesc, volatile unsigned long*& regsmap)
{
	if (driverDesc < 0)
	{
		printf("Failed to open %s: %s (%d)\n", csmsDigiDevice, std::strerror(errno), errno);
		return false;
	}
	CsmsDigiRegs regs;
	if (ioctl(driverDesc, IOCTL_CSMSDIGI_GET_REGS, &regs) == -1 || regs.Regs == 0 || regs.size == 0)
	{
		close(driverDesc);
		printf("IOCTL_CSMSDIGI_GET_REGS failed\n");
		return false;
	}
	auto ret = mmap(NULL, regs.size, PROT_READ | PROT_WRITE, MAP_SHARED, driverDesc, regs.Regs);
	if (ret == MAP_FAILED)
	{
		close(driverDesc);
		printf("Failed to map registers: %s (%d)\n", std::strerror(errno), errno);
		return false;
	}
	regsmap = static_cast<volatile unsigned long*>(ret);
	auto globalStatus = static_cast<unsigned short>(regsmap[static_cast<unsigned long>(EReg::GLOBAL_STATUS)]);
	if ((globalStatus & CGlobalStatus::GLOBAL_STATUS_READY) != CGlobalStatus::GLOBAL_STATUS_READY)
	{
		close(driverDesc);
		printf("Board is not ready for use, status = %04x\n", globalStatus);
		return false;
	}
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Measure noise power using threshold counters
//
void C3230::MeasureNoiseDbm(bool direct, float& power)
{
	unsigned int adcCount = 102400;
	unsigned int counts[4];
	unsigned short thresh[4];

	CollectCounts(direct, adcCount, counts, thresh);

	power = CalcNoiseDbm(adcCount, counts, thresh);
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Measure tone power using threshold counters
//
void C3230::MeasureToneDbm(bool direct, float& power)
{
	unsigned int adcCount = 102400;
	unsigned int counts[4];
	unsigned short thresh[4];

	CollectCounts(direct, adcCount, counts, thresh);

	power = CalcToneDbm(adcCount, counts, thresh);
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Read a user register
//
unsigned long C3230::PeekUser(EReg reg) const
{
	if (m_regs == nullptr)
		return 0;

	return m_regs[static_cast<unsigned long>(reg)];
}


//////////////////////////////////////////////////////////////////////
//
// Read a user register
//
unsigned long C3230::PeekDMA(EDMAReg reg) const
{
	if (m_DMAregs == nullptr)
		return 0;

	return m_DMAregs[static_cast<unsigned long>(reg)];
}


//////////////////////////////////////////////////////////////////////
//
// Write a user register
//
void C3230::PokeUser(EReg reg, unsigned long data, unsigned long mask)
{
	if (m_regs == nullptr)
		return;

	if(mask != 0xfffffffful)
	{
		if (data & ~mask)
		{
			TRACE("PokeUser mask\n");
		}
		data |= (PeekUser(reg) & ~mask);
	}

//	if (m_dbg)
//	{
//		TRACE("PokeUser @ %x: %lx\n", reg, data);
//	}
	m_regs[static_cast<unsigned long>(reg)] = data;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Write a user register
//
void C3230::PokeDMA(EDMAReg reg, unsigned long data, unsigned long mask)
{
	if (m_DMAregs == nullptr)
		return;

	if(mask != 0xfffffffful)
	{
		if (data & ~mask)
		{
			TRACE("PokeUser mask\n");
		}
		data |= (PeekDMA(reg) & ~mask);
	}

//	if (m_dbg)
//	{
//		TRACE("PokeDMA @ %x: %lx\n", reg, data);
//	}
	m_DMAregs[static_cast<unsigned long>(reg)] = data;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Register a callback for new samples arrived or samples processed
//
void C3230::RegisterNewSamplesCallback(_In_ NewSamplesCallback callback, _In_opt_ void* param, unsigned char ch)
{
	ASSERT(ch < NUM_INTERRUPT_THREADS);

	if (m_master)
	{
		std::lock_guard<std::mutex> lock(m_callbackCritSect[ch]);
		m_callbacks[ch] = callback;
		m_callbackParams[ch] = param;
	}

	return;
}



//////////////////////////////////////////////////////////////////////
//
// Reset entire board
//
bool C3230::Reset3230(bool dfSlave)
{
	if (m_master)
	{
		m_sbuffer.Reset();
		m_sdma.Reset();

		unsigned short thresh[4];
		GetThresholds(thresh);

		// Reset board
		PokeUser(EReg::GLOBAL_CONTROL, CGlobalControl::PLDDR3_RESET, CGlobalControl::PLDDR3_RESET);
		sleep(1);

		if (ioctl(m_driverDesc, IOCTL_CSMSDIGI_RESET_DIGITIZER, nullptr) == -1)
		{
			printf("Unable to reset 3220!\n");
		}
//		sleep(5);
		// Wait for global status to be 0x3f
		size_t i = 0;
		unsigned short globalStatus;
		do
		{
			globalStatus = static_cast<unsigned short>(PeekUser(EReg::GLOBAL_STATUS) & 0xffff);
			++i;
			if (i > 1000) break;
			usleep(10000);	// 10 ms

		} while ((globalStatus & CGlobalStatus::GLOBAL_STATUS_READY) != CGlobalStatus::GLOBAL_STATUS_READY);
		printf("GLOBAL_STATUS[%u]: %04x\n", i, globalStatus);
		if ((globalStatus & CGlobalStatus::GLOBAL_STATUS_READY) != CGlobalStatus::GLOBAL_STATUS_READY)
		{
			return false;
		}
		printf("GlobalControl: %lx\n", PeekUser(EReg::GLOBAL_CONTROL));
		sleep(1);
		// Init board
		ResetTimestamp();	// This will be handled by the driver reset
		sleep(1);
//		PokeUser(EReg::GLOBAL_CONTROL, CGlobalControl::ONEPPS_PORT_DIRECTION | CGlobalControl::HMC1035_REGULATOR_ENABLE | CGlobalControl::NIOSIICPU_RESET);
		PokeUser(EReg::GLOBAL_CONTROL, (dfSlave ? 0 : CGlobalControl::ONEPPS_PORT_DIRECTION), CGlobalControl::ONEPPS_PORT_DIRECTION);

		PokeUser(EReg::TIMESTAMP_CLKFREQUENCY, ADC_CLOCK_RATE);
	//	PokeUser(EReg::SEQUENCER_BLOCKCONTROL, 0x2); // Flush sequencer FIFO
	//	PokeUser(EReg::SEQUENCER_DEFAULTANTENNA, 0x000); // LVDS Out
	//	PokeUser(EReg::SIG_CONTROL, 0x01c); // DC block, low gain, no dither, no randomize
		ResetFifos();
		SetThresholdCounters(thresh);
	}

	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Reset clock frequency
//
void C3230::ResetFrequency(void)
{
	if (m_master)
	{
		PokeUser(EReg::TIMESTAMP_CLKFREQUENCY, ADC_CLOCK_RATE);
	}
	return;

}


//////////////////////////////////////////////////////////////////////
//
// Reset drift
//
void C3230::ResetDrift(void)
{
	if (m_master)
	{
		PokeUser(EReg::TIMESTAMP_CONTROL, CTimestampControl::RESET_DRIFT, CTimestampControl::RESET_DRIFT);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset sampling fifos
//
void C3230::ResetFifos(void)
{
	if (m_master)
	{
		PokeUser(EReg::FORMAT_CONTROL, CFormatControl::X2_GAIN | CFormatControl::S2MM_DM_RST | CFormatControl::FORMAT_FIFO_FLUSH);
		if ((PeekUser(EReg::FORMAT_STATUS) & CFormatStatus::FORMAT_READY) != CFormatStatus::FORMAT_READY)
		{
			TRACE("Problem resetting Format fifo\n");
		}

		PokeUser(EReg::FFT_CONTROL, CFftControl::FFT_S2MM_DM_RST | CFftControl::FFT_FIFO_FLUSH);

#ifdef CSMS_DEBUG
		if(s_trace3220)
		{
			TRACE("C3230::ResetFifos: FFT_CONTROL (0x00000102)\n");
		}
#endif

		if ((PeekUser(EReg::FFT_STATUS) & CFftStatus::FFT_READY) != CFftStatus::FFT_READY)
		{
			TRACE("Problem resetting FFT fifo (FFT_READY\n");
		}
		if ((PeekUser(EReg::FFT_STATUS1) & CFftStatus1::FFT_IQ_READY) != CFftStatus1::FFT_IQ_READY)
		{
			TRACE("Problem resetting FFT fifo (FFT_IQ_READY\n");
		}
		sleep(1);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset threshold counters
//
void C3230::ResetThresholdCounters(void)
{
	if (m_master)
	{
		PokeUser(EReg::AGC_CONTROL, CAgcControl::CCDF_FIFO_FLUSH);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset timestamp module and set internal flag
//
void C3230::ResetTimestamp(void)
{
	if (m_master)
	{
		PokeUser(EReg::TIMESTAMP_CONTROL, CTimestampControl::MODULE_RESET, CTimestampControl::MODULE_RESET);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the antenna control
//
void C3230::SetAntennaControl(unsigned long controlWord)
{
	if (m_master)
	{
		PokeUser(EReg::SEQUENCER_CONTROL, CSequencerControl::ANTENNA_WORD_WRITE, CSequencerControl::ANTENNA_WORD_WRITE);
		PokeUser(EReg::SEQUENCER_ANTENNA_CONTROL, controlWord);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the audio parameters
//
bool C3230::SetAudioParams(unsigned char audioChannel, const SAudioParams& params, bool enable, unsigned char gainShift, unsigned long streamid)
{
	EAudioMode audiomode;

	bool isIQ = (params.mode == EDemodMode::IQ);
	if (isIQ)
	{
		//printf("in iq mode enable = %d\n", enable);
		audiomode = EAudioMode::NARROWDDC;
		m_iqNarrowBandMem[audioChannel].outputRate = (params.mode == EDemodMode::IQ
			? (m_sampleRate / (params.decimation.ddcCICDecimation * params.decimation.ddcFIRDecimation)).Hz<double>()
			: CAdmPacketSize::AUDIO_RATE);

	}
	else if (params.mode == EDemodMode::WIDE_IQ)
	{
		//printf("in wide DDCiq mode\n");
		audiomode = EAudioMode::WIDEDDC;
		m_iqWideBandMem.outputRate = (params.mode == EDemodMode::WIDE_IQ
			? (m_sampleRate / (params.decimation.ddcCICDecimation * params.decimation.ddcFIRDecimation)).Hz<double>()
			: CAdmPacketSize::AUDIO_RATE);
	}
	else
	{
		//printf("in demod mode enable = %d\n", enable);
		audiomode = EAudioMode::DEMOD;
		m_audioMem[audioChannel].outputRate = (params.mode == EDemodMode::IQ
			? (m_sampleRate / (params.decimation.ddcCICDecimation * params.decimation.ddcFIRDecimation)).Hz<double>()
			: CAdmPacketSize::AUDIO_RATE);
	}
//	printf("output:%f sample:%f cic:%lu fir:%lu\n", m_audioMem[audioChannel].outputRate, m_sampleRate.Hz<double>(),
//		params.decimation.ddcCICDecimation, params.decimation.ddcFIRDecimation);

	unsigned char ddcChannel;
	if (params.mode != EDemodMode::WIDE_IQ)
		ddcChannel = audioChannel + 3;
	else
		ddcChannel = 2;  // only one wideband IQ occuping ddc ch 2
	bool wasEnabled = m_demod[audioChannel].enable;

	if (isIQ)
	{
		// if nothing has changed, don't do anything
		if (ddcChannel < m_versionInfo.totalDDCs && params.decimation.ddcCICDecimation == m_ddc[ddcChannel].rate &&
			params.procFreq == m_ddc[ddcChannel].freq && params.invertSpectrumData == m_ddc[ddcChannel].invertSpectrumData &&
			enable == m_ddc[ddcChannel].enable &&
			m_iqNarrowBandMem[audioChannel].outputRate == m_iqNarrowadm[audioChannel].outputRate && params.mode == m_iqNarrowadm[audioChannel].mode)
		{
			return true;
		}
	}
	else if (params.mode == EDemodMode::WIDE_IQ)
	{
		// if nothing has changed, don't do anything
		if (ddcChannel < m_versionInfo.totalDDCs && params.decimation.ddcCICDecimation == m_ddc[ddcChannel].rate &&
			params.procFreq == m_ddc[ddcChannel].freq && params.invertSpectrumData == m_ddc[ddcChannel].invertSpectrumData &&
			enable == m_ddc[ddcChannel].enable &&
			m_iqWideBandMem.outputRate == m_iqWideadm.outputRate && params.mode == m_iqWideadm.mode)
		{
			return true;
		}
	}
	else
	{
		// if nothing has changed, don't do anything
		if (ddcChannel < m_versionInfo.totalDDCs && params.decimation.ddcCICDecimation == m_ddc[ddcChannel].rate &&
			params.procFreq == m_ddc[ddcChannel].freq && params.invertSpectrumData == m_ddc[ddcChannel].invertSpectrumData &&
			enable == m_ddc[ddcChannel].enable &&
			params.mode == m_demod[audioChannel].mode && params.bw == m_demod[audioChannel].bw && params.decimation == m_demod[audioChannel].decimation &&
			params.bfo == m_demod[audioChannel].bfo && enable == m_demod[audioChannel].enable &&
			m_audioMem[audioChannel].outputRate == m_adm[audioChannel].outputRate && params.mode == m_adm[audioChannel].mode)
		{
			return true;
		}
	}

	if (enable)
	{
		bool onlyFrequency = ddcChannel < m_versionInfo.totalDDCs && params.decimation.ddcCICDecimation == m_ddc[ddcChannel].rate &&
			params.procFreq != m_ddc[ddcChannel].freq && params.invertSpectrumData == m_ddc[ddcChannel].invertSpectrumData && wasEnabled;

		if (onlyFrequency)
		{
			if (!SetDDC(ddcChannel, params.decimation.ddcCICDecimation, params.procFreq, params.invertSpectrumData, true, gainShift, audiomode))
				return false;
			return true;
		}

		if (!SetDDC(ddcChannel, params.decimation.ddcCICDecimation, params.procFreq, params.invertSpectrumData, false, gainShift, audiomode))
			return false;

		if (!SetDemod(audioChannel, params, false))
			return false;

		if (params.mode == EDemodMode::IQ)
		{
			if (!SetADM(audioChannel, m_iqNarrowBandMem[audioChannel].outputRate, params.mode, streamid))
				return false;
		}
		else if (params.mode == EDemodMode::WIDE_IQ)
		{
			if (!SetADM(audioChannel, m_iqWideBandMem.outputRate, params.mode, streamid))
				return false;
		}
		else
		{
			if (!SetADM(audioChannel, m_audioMem[audioChannel].outputRate, params.mode, streamid))
				return false;
		}

		if (!SetDemod(audioChannel, params, true))
			return false;

		if (!SetDDC(ddcChannel, params.decimation.ddcCICDecimation, params.procFreq, params.invertSpectrumData, true, gainShift, audiomode))
			return false;
		}
	else
	{
		auto turniqoff = false;
		if (m_iqNarrowadm[audioChannel].mode == EDemodMode::IQ && !enable)
		{
			m_iqNarrowadm[audioChannel].streamid = streamid;
			turniqoff = true;
		}
		if (params.mode == EDemodMode::IQ)
		{
			m_iqNarrowadm[audioChannel].outputRate = m_iqNarrowBandMem[audioChannel].outputRate;
			m_iqNarrowadm[audioChannel].mode = params.mode;
		}
		else if (params.mode == EDemodMode::WIDE_IQ)
		{
			m_iqWideadm.outputRate = m_iqWideBandMem.outputRate;
			m_iqWideadm.mode = params.mode;
		}
		else
		{
			m_adm[audioChannel].outputRate = m_audioMem[audioChannel].outputRate;
			m_adm[audioChannel].mode = params.mode;
		}

		if (!SetDDC(ddcChannel, params.decimation.ddcCICDecimation, params.procFreq, params.invertSpectrumData, false, gainShift, audiomode))
			return false;

		if (!SetDemod(audioChannel, params, false))
			return false;

		// set EOS right away and send empty packet via call back
		if (turniqoff)
		{
			m_iqNarrowBandMem[audioChannel].m_EOS.data = true;
			std::unique_lock<std::mutex> lock(m_callbackCritSect[2]);
			auto Callback = m_callbacks[2];
			auto CallbackParams = m_callbackParams[2];
			lock.unlock();
			if (Callback != nullptr)
			{
				//printf("EOS 1 called stream id = %lu\n", streamid);
				Callback(std::vector<unsigned long>(), audioChannel, true, CallbackParams);
			}
		}

	}
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Set the audio data mover
//
bool C3230::SetADM(unsigned char audioChannel, double outputRate, EDemodMode mode, unsigned long streamid)
{
	if (m_master)
	{
#ifdef CSMS_DEBUG
		if(s_trace3220)
		{
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			TRACE("C3230::SetADM: sec = %ld nsec =%ld; chan = %u, rate %lf, mode %d, streamid %ld\n",
					ts.tv_sec, ts.tv_nsec, unsigned(audioChannel), outputRate, int(mode), streamid);
		}
#endif
		SAdmParams *adm = nullptr;
		if (mode == EDemodMode::IQ)
		{
			adm = &(m_iqNarrowadm[audioChannel]);
		}
		else if (mode == EDemodMode::WIDE_IQ)
		{
			adm = &m_iqWideadm;
		}
		else
		{
			adm = &(m_adm[audioChannel]);
		}
		if (outputRate == adm->outputRate && mode == adm->mode && adm->streamid == streamid)
		{
			// No change
			return true;
		}

		adm->outputRate = outputRate;
		adm->mode = mode;

		auto packetSamples = 32 * static_cast<unsigned long>(ceil(AUDIO_INTERRUPT_RATE * outputRate / 32.)); 	// Multiple of 32
		packetSamples += CAdmPacketSize::HEADER_SIZE;	// add header samples
		if (packetSamples < CAdmPacketSize::AUDIO_MIN_PACKET) packetSamples = CAdmPacketSize::AUDIO_MIN_PACKET;
		else if (packetSamples > CAdmPacketSize::AUDIO_MAX_PACKET) packetSamples = CAdmPacketSize::AUDIO_MAX_PACKET;
		if (mode == EDemodMode::IQ || mode == EDemodMode::WIDE_IQ)
		{
			adm->streamid = streamid;
			// packetSamples |= CAdmPacketSize::AUDIO_BYPASS; // demod and ddc streaming no longer multiplexed
			streamid = (streamid << 20) & CAdmPacketSize::STREAM_ID;
			packetSamples |= streamid;
		}
		CRegLock lock(m_driverDesc);
		size_t physicalAddress;
		if (mode == EDemodMode::IQ)
		{
			physicalAddress = m_iqNarrowBandMem[audioChannel].physicalAddress;
		}
		else if (mode == EDemodMode::WIDE_IQ)
		{
			physicalAddress = m_iqWideBandMem.physicalAddress;
		}
		else
		{
			physicalAddress = m_audioMem[audioChannel].physicalAddress;
		}
		unsigned char admchanselect = 0;
		if (mode == EDemodMode::WIDE_IQ)
		{
			admchanselect = 7;
		}
		else if (mode == EDemodMode::IQ)
		{
			admchanselect = audioChannel;
		}
		else
		{
			admchanselect = audioChannel + 8;
		}
		PokeUser(EReg::ADM_CHANNELSELECT, admchanselect);	// audio channel
		PokeUser(EReg::ADM_PLDDR3ADDRESS, physicalAddress);
		PokeUser(EReg::ADM_PACKETSIZE, packetSamples);
		TRACE("output rate = %.2f\n", outputRate);
		TRACE("ADM_CHANNELSELECT = %u\n", admchanselect);
		TRACE("ADM_PLDDR3ADDRESS = %08x\n", physicalAddress); // NCO
		TRACE("ADM_PACKETSIZE = %08lx\n", packetSamples);
	}
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Set a narrowband DDC
//
bool C3230::SetDDC(unsigned char ddcChannel, unsigned long rate, Units::Frequency freq, bool invertSpectrumData, bool enable, unsigned char gainShift,  EAudioMode audiomode)
{
#ifdef CSMS_DEBUG
	if(s_trace3220)
	{
		timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		TRACE("C3230::SetDDC: sec = %ld nsec =%ld; chan %u, rate %ld, freq %lf, invert %d, enable %d, gainShift %u, audiomode %d\n",
				ts.tv_sec, ts.tv_nsec, unsigned(ddcChannel), rate, freq.Hz<double>(),
				(invertSpectrumData ? 1 :0), (enable ? 1 :0), unsigned(gainShift),
				int(audiomode));
	}
#endif

	bool keepEnableOn = false;
	assert(ddcChannel == 0
		? rate >= CDdcDecimation::MIN_DATA_DDC_CICDECIMATION && rate <= CDdcDecimation::MAX_DATA_DDC_CICDECIMATION
		: rate >= CDdcDecimation::MIN_AUDIO_DDC_CICDECIMATION && rate <= CDdcDecimation::MAX_AUDIO_DDC_CICDECIMATION);

	if (ddcChannel >= m_versionInfo.totalDDCs)
		return false;		// Invalid parameter


	// NOTE: THIS MAY NOT BE THREAD-SAFE. (m_ddc[])

	if (m_master)
	{
		if (rate == m_ddc[ddcChannel].rate && freq == m_ddc[ddcChannel].freq && invertSpectrumData == m_ddc[ddcChannel].invertSpectrumData && enable == m_ddc[ddcChannel].enable)
		{
			// No change, still set DDC_CICDECIMATION to reset ddc
			CRegLock lock(m_driverDesc);
			PokeUser(EReg::DDC_CHANNELSELECT, ddcChannel);	// DDCM channel
			PokeUser(EReg::DDC_CICDECIMATION, rate - 1);
			return true;
		}
		// skip narrowband DDC param setting if Demod on since they share same ddc channel (Demod takes precedence)
		if (ddcChannel > 2 && audiomode == EAudioMode::NARROWDDC)
		{
			if (m_SaveDemodgaincomp[ddcChannel - 3] & CDdcGainCompensation::CHANNEL_ENABLE)
			{
				unsigned long gainComp = m_SaveDemodgaincomp[ddcChannel - 3]; // remember old demod setting
				CRegLock lock(m_driverDesc);
				PokeUser(EReg::DDC_CHANNELSELECT, ddcChannel);	// DDCM channel
				if (enable)
				{
					gainComp |= CDdcGainCompensation::IQ_STREAMING_ON;
					m_IQOn[ddcChannel - 3] = true;
				}
				else
				{
					gainComp &= 0xFFDFFFFF; //clear bit 21
					m_IQOn[ddcChannel - 3] = false;
				}
				TRACE("IQ only-DDC_CICGAINCOMPENSATION = %08lx stream on %d\n", gainComp, enable);
				PokeUser(EReg::DDC_CICGAINCOMPENSATION, gainComp);
				return true;
			}
		}

		m_ddc[ddcChannel].rate = rate;
		m_ddc[ddcChannel].freq = freq;
		m_ddc[ddcChannel].invertSpectrumData = invertSpectrumData;
		m_ddc[ddcChannel].enable = enable;

		unsigned long gainComp;
		if (rate == 1)		// Can only be channel 0 in bypass mode
		{
			gainComp = 0x000000;
		}
		else
		{
			int shift = int(5 * (14 - log2(float(rate))));	// rate:[2...16384] => shift:[65..0]
			int gain = int(pow(2.0, (81 - shift)) / rate / rate / rate / rate / rate);
			assert(shift < 66 && gain < 4096);
			gainComp = ((shift + gainShift) << 12) | gain;
			if (ddcChannel != 0 && invertSpectrumData)
			{
				gainComp |= (CDdcGainCompensation::MIXER_NEG_EVEN_IMAG | CDdcGainCompensation::MIXER_NEG_ODD_IMAG);
			}
		}

		// Get sample rate at input to DDC and convert to nco value
		unsigned long ncoFreq = 0;
		if (freq != 0)
		{
			auto frontDecimation = (ddcChannel == 0 ? m_decimation.GetTotalFront() : 1);
			auto sampleRate = m_sampleRate;
			if (frontDecimation > 0)
				sampleRate /= frontDecimation;
			ncoFreq = CalcFreqWord(freq, sampleRate);
		}
		if (ddcChannel > 0)
		{
			if ((audiomode == EAudioMode::NARROWDDC) || (audiomode == EAudioMode::WIDEDDC))
			{
				if (enable)
				{
					//printf("set iq on\n");
					gainComp |= CDdcGainCompensation::IQ_STREAMING_ON;
					if (ddcChannel > 2)
						m_IQOn[ddcChannel - 3] = true;
				}
				else
				{
					//printf("set iq off\n");
					if (ddcChannel > 2)
						m_IQOn[ddcChannel - 3] = false;
				}
			}
			else
			{
				if (ddcChannel > 2 && m_IQOn[ddcChannel - 3]) // need to remember iq so setting demod will not toggle iq stream on/off
				{
					//printf("keep iq on\n");
					gainComp |= CDdcGainCompensation::IQ_STREAMING_ON;
				}
			}
			printf("DDC_CHANNELSELECT = %u\n", ddcChannel);	// DDCM channel
			printf("DDC_CHANNELNCOFREQUENCY = %lu\n", ncoFreq); // NCO
			printf("DDC_CICDECIMATION = %lu\n", rate - 1);
			printf("DDC_CICGAINCOMPENSATION = %08lx   stream on %d demod mode = %d enable =%d\n", gainComp, m_IQOn[ddcChannel - 3], int(audiomode), enable);
			if (enable && rate > 1 ) // enable bit needs to be kept on when either iq or demod are on.
			{
				printf("DDC_CICGAINCOMPENSATION = %08lx\n", gainComp | CDdcGainCompensation::CHANNEL_ENABLE);
			}
			else if ((ddcChannel > 2 && m_IQOn[ddcChannel - 3]) || (m_SaveDemodgaincomp[ddcChannel - 3] & CDdcGainCompensation::CHANNEL_ENABLE))
			{
				if (audiomode == EAudioMode::NARROWDDC)
				{
					keepEnableOn = true;
					printf("DDC_CICGAINCOMPENSATION = %08lx\n", gainComp | CDdcGainCompensation::CHANNEL_ENABLE);
				}
			}
		}
		CRegLock lock(m_driverDesc);
		PokeUser(EReg::DDC_CHANNELSELECT, ddcChannel);	// DDCM channel
		PokeUser(EReg::DDC_CHANNELNCOFREQUENCY, ncoFreq); // NCO
		PokeUser(EReg::DDC_CICDECIMATION, rate - 1);	// rate = 1 is illegal here but will be setting bypass later
		PokeUser(EReg::DDC_CICGAINCOMPENSATION, gainComp);
		if (ddcChannel > 2 && audiomode == EAudioMode::DEMOD)
			m_SaveDemodgaincomp[ddcChannel - 3] &= 0xFFEFFFFF; //clear bit 20;
		if (enable && rate > 1)		// Note: when called for ddcChannel 0, enable is always true
		{
			PokeUser(EReg::DDC_CICGAINCOMPENSATION, gainComp | CDdcGainCompensation::CHANNEL_ENABLE);
			if (ddcChannel > 2 && audiomode == EAudioMode::DEMOD)
			{
				m_SaveDemodgaincomp[ddcChannel - 3] = gainComp | CDdcGainCompensation::CHANNEL_ENABLE;
			}
			if (ddcChannel >= 3 && ddcChannel < m_versionInfo.audioDDCs + 3)
			{
				m_iqNarrowBandMem[ddcChannel - 3].expectedSequence.data = 0;
				m_iqNarrowBandMem[ddcChannel - 3].m_EOS.data = false;
			}
			else if (ddcChannel == 2)
			{
				m_iqWideBandMem.expectedSequence.data = 0;
			}
		}
		else if (keepEnableOn)
		{
			PokeUser(EReg::DDC_CICGAINCOMPENSATION, gainComp | CDdcGainCompensation::CHANNEL_ENABLE);
			if (ddcChannel > 2 && audiomode == EAudioMode::DEMOD)
			{
				m_SaveDemodgaincomp[ddcChannel - 3] = gainComp | CDdcGainCompensation::CHANNEL_ENABLE;
			}
		}
	}
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Set the NCO for a narrowband DDC
//
bool C3230::SetDDCNCO(unsigned char ddcChannel, Units::Frequency freq)
{
	if (ddcChannel >= m_versionInfo.totalDDCs)
		return false;		// Invalid parameter

	if (m_master)
	{
		// BC 7-14-17 skip if demod enabled?
		unsigned long ncoFreq = 0;
		if (freq != 0)
		{
			auto frontDecimation = (ddcChannel == 0 ? m_decimation.GetTotalFront() : 1);
			auto sampleRate = m_sampleRate;
			if (frontDecimation > 0)
				sampleRate /= frontDecimation;
			ncoFreq = CalcFreqWord(freq, sampleRate);
		}
		CRegLock lock(m_driverDesc);
		PokeUser(EReg::DDC_CHANNELSELECT, ddcChannel);	// DDCM channel
		PokeUser(EReg::DDC_CHANNELNCOFREQUENCY, ncoFreq); // NCO
	}
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Set audio demodulator
//
bool C3230::SetDemod(unsigned char audioChannel, const SAudioParams& params, bool enable)
{
#define DEMOD_NO_ALC
	if (m_master)
	{
		if (params.mode == m_demod[audioChannel].mode && params.bw == m_demod[audioChannel].bw && params.decimation == m_demod[audioChannel].decimation &&
			params.bfo == m_demod[audioChannel].bfo && enable == m_demod[audioChannel].enable)
		{
			// No change
			return true;
		}

		if (params.mode == EDemodMode::IQ || (params.mode == EDemodMode::WIDE_IQ))
		{
			return true; // skip now if non demod setting since they can be on at same time
		}

		m_demod[audioChannel].mode = params.mode;
		m_demod[audioChannel].bw = params.bw;
		m_demod[audioChannel].decimation = params.decimation;
		m_demod[audioChannel].bfo = params.bfo;
		m_demod[audioChannel].enable = enable;

		bool channelOn = enable;
#ifdef DEMOD_NO_ALC
		unsigned long demodParams = CDemodParams::DEMOD_ALC_DISABLE;
		unsigned long demodReset = CDemodParams::DEMOD_CHANNEL_RESET | CDemodParams::DEMOD_ALC_DISABLE;
#else
		unsigned long demodParams = 0;
		unsigned long demodReset = CDemodParams::DEMOD_CHANNEL_RESET;
#endif

#ifdef CSMS_DEBUG
		if(s_trace3220)
		{
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			TRACE("C3230::SetDemod: sec = %ld nsec =%ld; chan %u, enable %d, mode %d, bw %lf, bfo %lf, demodParams %lu, demodReset 0x%lX\n",
				ts.tv_sec, ts.tv_nsec, unsigned(audioChannel), (enable ? 1 :0), int(params.mode), params.bw.Hz<double>(),
				params.bfo.Hz<double>(), demodParams, demodReset);
	}
#endif
		// Set demodulation type
		switch(params.mode)
		{
		case EDemodMode::FM:
			demodParams |= CDemodParams::DEMOD_TYPE_FM;
			break;
		case EDemodMode::AM:
			demodParams |= CDemodParams::DEMOD_TYPE_AM;
			break;
		case EDemodMode::USB:
			demodParams |= CDemodParams::DEMOD_TYPE_USB;
			break;
		case EDemodMode::LSB:
			demodParams |= CDemodParams::DEMOD_TYPE_LSB;
			break;
		case EDemodMode::CW:
			demodParams |= CDemodParams::DEMOD_TYPE_CW;
			break;
		default:
			channelOn = false;
			break;
		}

		// Set bandwidth
		unsigned long searchBw = params.bw.Hz<unsigned long>(true);
		if (params.mode == EDemodMode::USB || params.mode == EDemodMode::LSB)
			searchBw *= 2;	// Adjust filter for ssb

		auto numBws = std::extent<decltype(CDemodParams::DEMOD_BANDWIDTHS)>::value;
		if (searchBw >= CDemodParams::DEMOD_BANDWIDTHS[numBws - 1][0])
		{
			demodParams |= CDemodParams::DEMOD_BANDWIDTHS[numBws - 1][1];
		}
		else
		{
			for (size_t i = 0; i < numBws; ++i)
			{
				if (searchBw <= CDemodParams::DEMOD_BANDWIDTHS[i][0])
				{
					demodParams |= CDemodParams::DEMOD_BANDWIDTHS[i][1];
					break;
				}
			}
		}
		auto bfoSampleRate = m_sampleRate.Hz<double>() / params.decimation.GetTotal();
		auto bfoPhaseIncr = static_cast<unsigned long>(params.bfo.Hz<double>() * 1048576 / bfoSampleRate) & CDemodBfoPhaseIncr::DEMOD_BFO_MASK;
//		TRACE("CalcBfoIncr %.6f %lu\n", params.bfo.Hz<double>(), bfoPhaseIncr);

		// Set control word
		unsigned long controlMask;
		unsigned long controlData;
		if (audioChannel < std::extent<decltype(CDemodControl::DEMOD_CHAN_ON_OFF)>::value)
		{
			controlMask = CDemodControl::DEMOD_CHAN_ON_OFF[audioChannel];
			controlData = (channelOn ? controlMask : 0);
		}
		else
		{
			controlMask = CDemodControl::DEMOD_CHAN_MASK;
			controlData = 0;
		}

		CRegLock lock(m_driverDesc);
		PokeUser(EReg::DEMOD_CHANNELSELECT, audioChannel);	// audio channel
		PokeUser(EReg::DEMOD_PARAMS, demodReset, demodReset);
		PokeUser(EReg::DEMOD_PARAMS, demodParams);
		PokeUser(EReg::DEMOD_BFOPHASEINCR, bfoPhaseIncr);
		PokeUser(EReg::DEMOD_CONTROL, controlData, controlMask);

		TRACE("DEMOD_CHANNELSELECT %u demod mode %d enable %d\n", audioChannel, (int) params.mode, enable);
		TRACE("DEMOD_PARAMS %08lx\n", CDemodParams::DEMOD_CHANNEL_RESET);
		TRACE("DEMOD_PARAMS %08lx\n", demodParams);
		TRACE("DEMOD_BFOPHASEINCR %lu\n", bfoPhaseIncr);
		TRACE("DEMOD_CONTROL %08lx %08lx\n", controlData, controlMask);

	}
	return true;

}


/////////////////////////////////////////////////////////////////////
//
// Set the FPGA decimation
//
bool C3230::SetDecimation(const SDecimation& decimations, unsigned char gainShift)
{
	// Note: only used for DDCM channel 0
	if (m_master)
	{
		auto rv = false;

		// NOTE: THIS FUNCTION IS NOT THREAD-SAFE! May need to be fixed if used from more than one thread.
		if (decimations != m_decimation)
		{
			m_decimation = decimations;
			rv = true;	// Changed decimation

			// Data channel, has front-end, resampler plus narrowband ddc

			// Set front-end
			assert(decimations.zifCICDecimation > 0 && decimations.zifCICDecimation <= 32);
			PokeUser(EReg::SIGNAL_CICDECIMATION, decimations.zifCICDecimation - 1, 0x00ff);
			auto rate = decimations.zifCICDecimation;
			if (rate > 1)
			{
				int shift = int(5 * (5 - log2(float(rate))));	// rate:[2...32] => shift:[20...0]
				int gain = int(pow(2.0, (32 - shift)) / rate / rate / rate / rate / rate);
				assert(shift < 21 && gain < 256);
				PokeUser(EReg::SIGNAL_CICGAINCOMPENSATION, ((shift + gainShift) << 8) | gain, 0x0000ffff);
			}

			// Set resampler
			if (decimations.upResample == 5 && decimations.downResample == 6)
			{
				PokeUser(EReg::SIGNAL_CONTROL, CSignalControl::OUTPUT_RESAMPLER56, CSignalControl::OUTPUT_RESAMPLER_MASK);
			}
			else if (decimations.upResample == 8 && decimations.downResample == 9)
			{
				PokeUser(EReg::SIGNAL_CONTROL, CSignalControl::OUTPUT_RESAMPLER89, CSignalControl::OUTPUT_RESAMPLER_MASK);
			}
			else
			{
				PokeUser(EReg::SIGNAL_CONTROL, CSignalControl::OUTPUT_NORESAMPLER, CSignalControl::OUTPUT_RESAMPLER_MASK);
			}
		}

		// Set narrowband DDC for all channels even if no change (to reset ddc)
		SetDDC(0, decimations.ddcCICDecimation, 0, false, true, gainShift);		// center-tuned, not inverted (for channel 0)
		return rv;
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Turn the LED on or off
//
void C3230::SetLed(bool on)
{
	if (m_master)
	{
		PokeUser(EReg::BIST_GPIOREGISTER, (on ? CBistGPIORegister::LED2_P : 0), CBistGPIORegister::LED2_P);
	}
	return;
}

//////////////////////////////////////////////////////////////////////
//
// Turn the LED on or off
//
void C3230::SetFaultStatusLed(bool on)
{
	if (m_master)
	{
		PokeUser(EReg::SEQUENCER_ANTENNA_CONTROL, (on ? CSequencerAntennaControl::LED0_N : 0), CSequencerAntennaControl::LED0_N);
	}
}



//////////////////////////////////////////////////////////////////////
//
// Flip the state of the LED
//
void C3230::ToggleFaultStatusLed(void)
{
	if (m_master)
	{
		auto currentLed = PeekUser(EReg::SEQUENCER_ANTENNA_CONTROL) & CSequencerAntennaControl::LED0_N;
		PokeUser(EReg::SEQUENCER_ANTENNA_CONTROL, (currentLed ^ CSequencerAntennaControl::LED0_N), CSequencerAntennaControl::LED0_N);
	}
	return;
}

bool C3230::SetLedStatic(bool on)
{
	auto driverDesc = open(csmsDigiDevice, O_RDWR);
	volatile unsigned long* regsmap;
	if (MapRegisters(driverDesc, regsmap))
	{
		unsigned long data = regsmap[static_cast<unsigned long>(EReg::BIST_GPIOREGISTER)];	// current
		regsmap[static_cast<unsigned long>(EReg::BIST_GPIOREGISTER)] =
				(on ? data |= CBistGPIORegister::LED2_P : data &= ~CBistGPIORegister::LED2_P);
		close(driverDesc);
	}
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Read and display the digitizer settings
//
void C3230::ShowDigitizerSettings(void)
{
	if (!m_master) return;

	// Data channel - front end
	printf("SIGNAL_NCOFREQUENCY = %lu\n", PeekUser(EReg::SIGNAL_NCOFREQUENCY));
	printf("SIGNAL_CICDECIMATiON = %lu + 1\n", PeekUser(EReg::SIGNAL_CICDECIMATION));
	printf("SIGNAL_CICGAINCOMPENSATION = %08lx\n", PeekUser(EReg::SIGNAL_CICGAINCOMPENSATION));
	printf("SIGNAL_CONTROL = %lu\n", PeekUser(EReg::SIGNAL_CONTROL));
	printf("FORMAT_CONTROL = %lu\n", PeekUser(EReg::FORMAT_CONTROL));
	printf("FORMAT_PACKETSIZE = %lu\n", PeekUser(EReg::FORMAT_PACKETSIZE));
	printf("FFT_CONTROL = %lu\n", PeekUser(EReg::FFT_CONTROL));
	printf("FFT_SIZE = %lu\n", PeekUser(EReg::FFT_SIZE));

	// Data channel - ddc
	unsigned char ddcChannel = 0;
	{
		CRegLock lock(m_driverDesc);
		PokeUser(EReg::DDC_CHANNELSELECT, ddcChannel | CDdcChannelSelect::READ_SELECT_FREQUENCY);
		printf("DDC_CHANNELNCOFREQUENCY[%u] = %lu\n", ddcChannel, PeekUser(EReg::DDC_STATUS));
		PokeUser(EReg::DDC_CHANNELSELECT, ddcChannel | CDdcChannelSelect::READ_SELECT_DECIMATION);
		printf("DDC_CICDECIMATION[%u] = %lu + 1\n", ddcChannel, PeekUser(EReg::DDC_STATUS));
		PokeUser(EReg::DDC_CHANNELSELECT, ddcChannel | CDdcChannelSelect::READ_SELECT_GAINCOMP);
		printf("DDC_CICGAINCOMPENSATION[%u] = %08lx\n", ddcChannel, PeekUser(EReg::DDC_STATUS));
	}
	return;
}


#ifdef CSMS_DEBUG

//////////////////////////////////////////////////////////////////////
//
// Get the scratch register value.
//
void C3230::ScratchRegsGetVal(unsigned long scratchMem[], size_t scratchMemSize)
{
	ASSERT(scratchMemSize <= 35);

	for(size_t i; i < scratchMemSize; ++i)
	{
		scratchMem[i] = s_scratchRegs[i];
	}

	return;
}

//////////////////////////////////////////////////////////////////////
//
// Output the scratch register value.
//
void C3230::ScratchRegsOutTrace(void)
{
	if(s_scratchRegValid && s_scratchRegs != nullptr)
	{
		TRACE("DMA0[0]%lu, DMA1[1]%lu, DMA2[2]%lu, DMA3[3]%lu, DMA4[4]%lu, DMA5[5]%lu, DMA6[6]%lu, DMA7[7]%lu, AGCDone[8]%lu,\n",
				s_scratchRegs[(0)], s_scratchRegs[(1)], s_scratchRegs[(2)], s_scratchRegs[(3)], s_scratchRegs[(4)],
				s_scratchRegs[(5)], s_scratchRegs[(6)], s_scratchRegs[(7)], s_scratchRegs[(8)]);
		TRACE("agcFifoFull[9]%lu, iqDone[10]%lu, iqBlockDone[11]%lu, iqDataMoverErr[12]%lu, formatFifoDataErr[13]%lu, fftDone[14]%lu, fftDataMoverErr[15]%lu, fftOutputFifoDataErr[16]%lu,\n",
				s_scratchRegs[9], s_scratchRegs[10], s_scratchRegs[11], s_scratchRegs[12],
				s_scratchRegs[13], s_scratchRegs[14], s_scratchRegs[15], s_scratchRegs[16]);
		TRACE("fftInputFifoDataErr[17]%lu, sequencerDone[18]%lu, sequencerBlockDone[19]%lu, fftIDone[20]%lu, fftQDone[21]%lu, shutDown[22]%lu, rfPllLock[23]%lu, bistTimer[24]%lu,\n",
				s_scratchRegs[17], s_scratchRegs[18], s_scratchRegs[19], s_scratchRegs[20],
				s_scratchRegs[21], s_scratchRegs[22], s_scratchRegs[23], s_scratchRegs[24]);
		TRACE("overTemp[25]%lu, onePps[26]%lu, ADM[27]%lu, ADMD[28]%lu, shutDown2[29]%lu, CDMA_DONE[30]%lu, WB_DDC[31]%lu, Unused[32]%lu,Process[33]%lu,total[34]%lu,\n",
					s_scratchRegs[25], s_scratchRegs[26], s_scratchRegs[27], s_scratchRegs[28],
					s_scratchRegs[29], s_scratchRegs[30], s_scratchRegs[31],
					s_scratchRegs[32], s_scratchRegs[33], s_scratchRegs[34]);
	}

	return;
}
#endif


//////////////////////////////////////////////////////////////////////
//
// Set the seconds register
//
bool C3230::SetSeconds(Units::Timestamp timestamp)
{
	if (m_master)
	{
		// Set the seconds register from the high-order 32 bits of the timestamp
		auto currentSecs = PeekUser(EReg::TIMESTAMP_1PPSCONTTIME);
		auto newSecs = static_cast<unsigned long>(timestamp.m_timestamp >> 32);
		if (newSecs != currentSecs)
		{
		TRACE("C3230::SecSeconds changing from %lu to %lu\n", currentSecs, newSecs);
		PokeUser(EReg::TIMESTAMP_1PPSWRTIME, newSecs);
		}
		m_setSeconds = true;
		return true;
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Set the AGC slice levels
//
void C3230::SetThresholdCounters(const unsigned short (&thresh)[4])
{
	if (m_master)
	{
		CRegLock lock(m_driverDesc);
		PokeUser(EReg::AGC_SLICE0, (thresh[1] << 16) | thresh[0]);
		PokeUser(EReg::AGC_SLICE1, (thresh[3] << 16) | thresh[2]);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the internal trigger timestamp
//
bool C3230::SetTimeStamp(const Units::Timestamp& timestamp)
{
	if (m_master)
	{
		bool rv = true;

		bool has3230Timestamp = m_hasTimestamp && m_setSeconds;
		if (!has3230Timestamp)
		{
			TRACE("*************SetTimeStamp called when unavailable\n");
			rv = false;
		}

		unsigned long sampleRate = ADC_CLOCK_RATE;

		PokeUser(EReg::TIMESTAMP_INTEGERSECOND, static_cast<unsigned long>(timestamp.m_timestamp >> 32));  // seconds
		PokeUser(EReg::TIMESTAMP_FRACTIONALSECOND, static_cast<unsigned long>(((timestamp.m_timestamp & 0xffffffff) * sampleRate) >> 32));  // fraction in clock intervals

//		TRACE("SetTimeStamp %lu %lu\n", static_cast<unsigned long>(timestamp.m_timestamp >> 32), static_cast<unsigned long>(((timestamp.m_timestamp & 0xffffffff) * sampleRate) >> 32));
		return rv;
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Start collecting blocks of samples (single channel)
//
bool C3230::StartSampling(unsigned long sampleCount, unsigned long count, unsigned long adcCount, unsigned long delay, unsigned long fftDelay,
	bool withIq, bool withPsd, bool withFft, unsigned long subBlockCount, Units::Timestamp startTime, EDfMode dfMode, const std::vector<unsigned long>& pattern)
{
#ifdef CSMS_DEBUG
	if(s_trace3220)
	{
		timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		TRACE("C3230::StartSampling: sec = %ld nsec =%ld; sampleCount %lu, count %lu, adcCount %lu, delay %lu, fftDelay %lu, withIq %d, withPsd %d, withFft %d, subBlockCount %ld, time %s, dfMode %d, pattern %u\n",
				ts.tv_sec, ts.tv_nsec, sampleCount, count, adcCount, delay, fftDelay,
				(withIq ? 1 : 0), (withPsd? 1: 0), (withFft? 1 : 0), subBlockCount,
				startTime.Format(true).c_str(), int(dfMode), pattern.size());
	}
#endif

	m_newDFmode = true;
	/*bool disableNewDF = (PeekUser(EReg::BIST_GPIOREGISTER) & 0x0001) > 0;
	if (dfMode == EDfMode::NODF || !CSingleton<CConfig>()->AllowFastSeqMode()) // only allow new df for df master and df slave for now.
		disableNewDF = true;

	if (disableNewDF == m_newDFmode)
	{
		if (disableNewDF)
		{
			//printf ("switch to old DF mode running\n");
			m_newDFmode = false;
		}
		else
		{
			// switch for now only if
			//printf ("switch to new DF mode running\n");
			m_newDFmode = true;
		}
	}*/

	if (dfMode == EDfMode::NODF || !CSingleton<CConfig>()->AllowFastSeqMode()) // only allow new df for df master and df slave for now.
		m_newDFmode = false;

	if (m_master)
	{
		// Assumes that the correct amount of space has been reserved in DDR3 starting at offset sampleCount (2 * numAnts * count or twice if enhdynrng)
		if (withIq && withFft)
		{
			TRACE("StartSampling: iqdata and fftdata cannot be requested together\n");
			return false;
		}
		if (withFft && !withPsd)
		{
			TRACE("StartSampling: psddata must be requested if fftdata\n");
			return false;
		}
		if (startTime > Units::Timestamp() && dfMode != EDfMode::NODF)
		{
			TRACE("StartSampling: cannot have df and tdoa together\n");
			return false;
		}
		if (withIq)
		{
			assert(count >= 64 && count <= 2097150);
			if (count < 64 || count > 2097150)
				return false;
		}
		if (withPsd || withFft)
		{
			assert(count >= 64 && count <= 65536 && (count & (count - 1)) == 0);
			if (count < 64 || count > 65536 || (count & (count - 1)) != 0)	// Range and power-of-2 check
				return false;
		}

#ifdef CSMS_DEBUG
		bool startGtNow = false;
#endif
		if (startTime > Units::Timestamp())
		{
			if (!SetTimeStamp(startTime))
				return false;
			PokeUser(EReg::TIMESTAMP_CONTROL, CTimestampControl::INTERNAL_SEQUENCE_TRIG, CTimestampControl::INTERNAL_SEQUENCE_TRIG);
//			printf("StartSampling @ %s now:%s\n", startTime.Format(true).c_str(), Utility::CurrentTimeAsString(Utility::NSEC).c_str());
#ifdef CSMS_DEBUG
			startGtNow = true;
#endif
		}
		else
		{
			PokeUser(EReg::TIMESTAMP_CONTROL, 0x0000, CTimestampControl::INTERNAL_SEQUENCE_TRIG);
		}

		if ((PeekUser(EReg::AGC_STATUS) & CAgcStatus::CCDF_FIFO_EMPTY) == 0)
		{
			TRACE(_T("StartSampling: AGC FIFO not empty\n"));
		}
		PokeUser(EReg::AGC_WINDOWSIZE, adcCount - 2);
		PokeUser(EReg::AGC_CONTROL, CAgcControl::CCDF_FIFO_FLUSH, CAgcControl::CCDF_FIFO_FLUSH);

#ifdef CSMS_DEBUG
		if(s_trace3220)
		{
			TRACE("C3230::StartSampling: AGC_CONTROL CCDF_FIFO_FLUSH (0x01) set; startGtNow %d\n",
					(startGtNow ? 1 :0));
		}
#endif

		// Setup format and fft controls
		if (withIq)
		{
			PokeUser(EReg::FORMAT_PACKETSIZE, count);
			if (subBlockCount > 0)
			{
				PokeUser(EReg::FORMAT_SUBBLOCKSIZE, std::min(count / subBlockCount, 0xFFFFLU));
			}
			else
			{
				PokeUser(EReg::FORMAT_SUBBLOCKSIZE, 0xFFFFLU);
			}
			if (m_decimation.ddcCICDecimation == 1)
			{
				PokeUser(EReg::FORMAT_CONTROL, CFormatControl::ENABLE_FRONTEND_IQ_SAMPLES, CFormatControl::ENABLE_FRONTEND_IQ_SAMPLES | CFormatControl::ENABLE_DDCM_IQ_SAMPLES);
				if (m_newDFmode)
				{
					//printf("IQ-- df mode %d input ddr 3 format front end\n", (int) dfMode);
					PokeUser(EReg::FFT_CONTROL, CFftControl::FFT_INPUT_DDR3, CFftControl::FFT_SOURCE_SELECT);
				}
				else
				{
					//printf("IQ-- source format front end\n");
					PokeUser(EReg::FFT_CONTROL, CFftControl::FFT_INPUT_FRONTEND, CFftControl::FFT_SOURCE_SELECT);
				}
				PokeUser(EReg::FORMAT_CONTROL, CFormatControl::FORMAT_INPUT_FRONTEND, CFormatControl::FORMAT_SOURCE_SELECT);
			}
			else
			{
				PokeUser(EReg::FORMAT_CONTROL, CFormatControl::ENABLE_DDCM_IQ_SAMPLES, CFormatControl::ENABLE_FRONTEND_IQ_SAMPLES | CFormatControl::ENABLE_DDCM_IQ_SAMPLES);
				if (m_newDFmode)
				{
					//printf("IQ-- df mode %d input ddr 3 format ddcm\n", (int) dfMode);
					PokeUser(EReg::FFT_CONTROL, CFftControl::FFT_INPUT_DDR3, CFftControl::FFT_SOURCE_SELECT);
				}
				else
				{
					//printf("IQ-- fft ddcm format ddcm\n");
					PokeUser(EReg::FFT_CONTROL, CFftControl::FFT_INPUT_DDCM, CFftControl::FFT_SOURCE_SELECT);
				}
				PokeUser(EReg::FORMAT_CONTROL, CFormatControl::FORMAT_INPUT_DDCM, CFormatControl::FORMAT_SOURCE_SELECT);

			}
			if ((PeekUser(EReg::FORMAT_STATUS) & CFormatStatus::FORMAT_READY) != CFormatStatus::FORMAT_READY)
			{
				TRACE("StartSampling: Format not ready %lx %lu!\n", PeekUser(EReg::FORMAT_STATUS), PeekUser(EReg::FORMAT_PACKETCOUNT));
				PokeUser(EReg::FORMAT_CONTROL, CFormatControl::S2MM_DM_RST | CFormatControl::FORMAT_FIFO_FLUSH, CFormatControl::S2MM_DM_RST | CFormatControl::FORMAT_FIFO_FLUSH);		// TODO: Remove?
			}
			PokeUser(EReg::FORMAT_CONTROL, CFormatControl::FORMAT_FIFO_FLUSH, CFormatControl::FORMAT_FIFO_FLUSH);
		}
		else
		{
			PokeUser(EReg::FORMAT_CONTROL, 0, CFormatControl::ENABLE_FRONTEND_IQ_SAMPLES | CFormatControl::ENABLE_DDCM_IQ_SAMPLES);
			PokeUser(EReg::FORMAT_CONTROL, CFormatControl::FORMAT_FIFO_FLUSH, CFormatControl::FORMAT_FIFO_FLUSH);
		}

		if (withPsd || withFft)
		{
			PokeUser(EReg::FFT_SIZE, count);
//			TRACE("C3230::StartSampling: offset = %lu count = %lu\n", offset, count);
			unsigned long fftControl = 0;
#ifdef CSMS_DEBUG
			if (withPsd)
			{
				fftControl |= CFftControl::ENABLE_PSD_OUTPUT;
				++s_psdFpgaSetCnt;
			}
#else
			if (withPsd) fftControl |= CFftControl::ENABLE_PSD_OUTPUT;
#endif
			if (withFft) fftControl |= CFftControl::ENABLE_FFT_OUTPUT;
			PokeUser(EReg::FFT_CONTROL, fftControl, CFftControl::ENABLE_PSD_OUTPUT | CFftControl::ENABLE_FFT_OUTPUT);
			if (m_decimation.ddcCICDecimation == 1)
			{
				if (m_newDFmode)
				{
					//printf("psd-- df mode %d input ddr 3 format front end\n", (int) dfMode);
					PokeUser(EReg::FORMAT_CONTROL, CFormatControl::ENABLE_FRONTEND_IQ_SAMPLES, CFormatControl::ENABLE_FRONTEND_IQ_SAMPLES | CFormatControl::ENABLE_DDCM_IQ_SAMPLES);
					PokeUser(EReg::FFT_CONTROL, CFftControl::FFT_INPUT_DDR3, CFftControl::FFT_SOURCE_SELECT);
				}
				else
				{
					//printf("psd-- source format front end\n");
					PokeUser(EReg::FFT_CONTROL, CFftControl::FFT_INPUT_FRONTEND, CFftControl::FFT_SOURCE_SELECT);
				}
				PokeUser(EReg::FORMAT_CONTROL, CFormatControl::FORMAT_INPUT_FRONTEND, CFormatControl::FORMAT_SOURCE_SELECT);
			}
			else
			{
				if (m_newDFmode)
				{
					//printf("psd-- df mode %d input ddr 3 format ddcm\n", (int) dfMode);
					PokeUser(EReg::FORMAT_CONTROL, CFormatControl::ENABLE_DDCM_IQ_SAMPLES, CFormatControl::ENABLE_FRONTEND_IQ_SAMPLES | CFormatControl::ENABLE_DDCM_IQ_SAMPLES);
					PokeUser(EReg::FFT_CONTROL, CFftControl::FFT_INPUT_DDR3, CFftControl::FFT_SOURCE_SELECT);
				}
				else
				{
					//printf("psd-- fft ddcm format ddcm\n");
					PokeUser(EReg::FFT_CONTROL, CFftControl::FFT_INPUT_DDCM, CFftControl::FFT_SOURCE_SELECT);
				}
				PokeUser(EReg::FORMAT_CONTROL, CFormatControl::FORMAT_INPUT_DDCM, CFormatControl::FORMAT_SOURCE_SELECT);
			}
			if ((PeekUser(EReg::FFT_STATUS) & CFftStatus::FFT_READY) != CFftStatus::FFT_READY)
			{
				TRACE("StartSampling: FFT not ready (FFT_READY)!\n");
			}
			if ((PeekUser(EReg::FFT_STATUS1) & CFftStatus1::FFT_IQ_READY) != CFftStatus1::FFT_IQ_READY)
			{
				TRACE("StartSampling: FFT not ready (FFT_IQ_READY)!\n");
			}
			PokeUser(EReg::FFT_CONTROL, CFftControl::FFT_FIFO_FLUSH, CFftControl::FFT_FIFO_FLUSH);

#ifdef CSMS_DEBUG
			if(s_trace3220)
			{
				TRACE("C3230::StartSampling: FFT_CONTROL (FFT_FIFO_FLUSH 0X02 SET) ddcCICDecimation %lu, fpgaSetCnt =%u\n",
						m_decimation.ddcCICDecimation, s_psdFpgaSetCnt);
			}
#endif

		}
		else
		{
			PokeUser(EReg::FFT_CONTROL, 0, CFftControl::ENABLE_PSD_OUTPUT | CFftControl::ENABLE_FFT_OUTPUT);
			PokeUser(EReg::FFT_CONTROL, CFftControl::FFT_FIFO_FLUSH, CFftControl::FFT_FIFO_FLUSH);
		}
		// Setup sequencer

		if ((PeekUser(EReg::SEQUENCER_STATUS) & CSequencerStatus::SEQUENCER_FIFO_EMPTY) == 0)
		{
			TRACE(_T("StartSampling: Sequencer FIFO not empty\n"));
			//PokeUser(EReg::SEQUENCER_CONTROL, CSequencerControl::SEQUENCER_FIFO_FLUSH, CSequencerControl::SEQUENCER_FIFO_FLUSH);
		}
		// always flush seq fifo
		PokeUser(EReg::SEQUENCER_CONTROL, CSequencerControl::SEQUENCER_FIFO_FLUSH, CSequencerControl::SEQUENCER_FIFO_FLUSH);

		// Setup FFT delay control
		PokeUser(EReg::FFT_CONTROL, (fftDelay << CFftControl::FFT_DELAY_SHIFT) & CFftControl::FFT_DELAY_CONTROL, CFftControl::FFT_DELAY_CONTROL);

		// Debug output
//		TRACE("Before setting up sequencer, SEQUENCER_ANTENNA_CONTROL = %lx\n", PeekUser(EReg::SEQUENCER_ANTENNA_CONTROL));

		// Address selection (first block)
		unsigned long iqPLDDR3Address = m_sbuffer.PhysicalAddress(0);
		unsigned long psdPLDDR3Address = m_sbuffer.PhysicalAddress(0);
		unsigned long fftIPLDDR3Address = m_sbuffer.PhysicalAddress(0);
		unsigned long fftQPLDDR3Address = m_sbuffer.PhysicalAddress(0);
		//unsigned long newDFiqPLDDR3Address = m_NewDFIQaddrbuffer.GetNextPhysicalAddress(); // used only to hold iq address for New DF mode for non-IQ mode

		auto offset = sampleCount;	// initial offset for data collection
		auto newdfIQoffset = sampleCount;

		unsigned long DMASrcAddress = m_sbuffer.PhysicalAddress(offset);
		unsigned long DMADestAddress = m_sdma.PhysicalAddress(offset);	// DMA starts at the initial offset.
		unsigned long DMAtotalcount = 0;

//		if (m_dbg)
//		{
//			printf("pre-fire %lx %lx %p\n", DMADestAddress, offset, m_sdma.m_buffer + offset);
//			auto addr = m_sdma.m_buffer + offset;
//			for (size_t i = 0; i < count; i += 8)
//			{
//				char temp[20];
//				snprintf(temp, 20, "pre-fire:%3u", i);
//				std::string str(temp);
//				for (size_t j = i; j < i + 8; ++j)
//				{
//					if (j == count) break;
//					snprintf(temp, 20, " %08lx", addr[j]);
//					str += temp;
//				}
//				printf("%s\n", str.c_str());
//			}
//		}
		// new df mode requires programming of IQ address but not interfere with psd/fft consecutive address scheme
		unsigned long iqPLDDR3Addr[pattern.size()];
		if (!withIq && m_newDFmode)
		{
			size_t ddr3Size = 0x8000000;
			for (size_t step = 0; step < pattern.size(); ++step)
			{
				iqPLDDR3Addr[step] = m_sbuffer.PhysicalAddress(ddr3Size, newdfIQoffset);
				newdfIQoffset += count;
			}
		}

		//debug only
		/*if (m_newDFmode && (pattern.size() > 1))
		{
			if ((totalinterrupt % 9) != 0)
			totalinterrupt = 0;
		}*/
		if (pattern.size() == 9)  // debug only
			totaldfscan++; // debug only
		else
			totaldfscan = 0;
		//debug end
		for (size_t step = 0; step < pattern.size(); ++step)
		{
			if (withIq)
			{
				iqPLDDR3Address = m_sbuffer.PhysicalAddress(offset);
				DMAtotalcount += count;
				offset += count;
			}
			if (withPsd)
			{
				psdPLDDR3Address = m_sbuffer.PhysicalAddress(offset);
				DMAtotalcount += count;
				offset += count;
			}
			if (withFft)
			{
				fftIPLDDR3Address = m_sbuffer.PhysicalAddress(offset);
				offset += count;
				fftQPLDDR3Address = m_sbuffer.PhysicalAddress(offset);
				offset += count;
				DMAtotalcount += 2 * count;
			}

			// delay 10 us debug only
			//Utility::Delay(20);

			PokeUser(EReg::SEQUENCER_DATA, pattern[step]);		// Antenna control word
			//printf ("multiply delay = %lu X4", delay); //debug only
			//delay *= 4; // debug only
			//PokeUser(EReg::SEQUENCER_DATA, 0xE70);
			//printf("delay in sequencer data = %lx\n", delay);
			PokeUser(EReg::SEQUENCER_DATA, delay);				// Delay in 1/ADC_CLOCK_RATE units  5 * 368.64 = 1830
			PokeUser(EReg::SEQUENCER_DATA, count);				// Block size
			if (!withIq && m_newDFmode)
			{
				iqPLDDR3Address = iqPLDDR3Addr[step];
			}

#ifdef CSMS_DEBUG
			if(s_trace3220)
			{
				if (withIq)
					TRACE("iq processing\n");
				if (withPsd)
					TRACE("psd processing step %u\n", step);
				if (withFft)
					TRACE("fft processing step %u\n", step);

				TRACE("iq plddr3 addr = %lx\n", iqPLDDR3Address);
				TRACE("psd plddr3 addr = %lx\n", psdPLDDR3Address);
				TRACE("fftI plddr3 addr = %lx\n", fftIPLDDR3Address);
				TRACE("fftQ plddr addr = %lx\n", fftQPLDDR3Address);
			}
#endif
			PokeUser(EReg::SEQUENCER_DATA, iqPLDDR3Address);
			PokeUser(EReg::SEQUENCER_DATA, psdPLDDR3Address);
			PokeUser(EReg::SEQUENCER_DATA, fftIPLDDR3Address);
			PokeUser(EReg::SEQUENCER_DATA, fftQPLDDR3Address);

			// new for speeding up DF by putting into PLDDR 3 memory, first word is block size
			if (m_newDFmode)
			{
				//printf("fft sequencer data word1-5 sent-- step=%u\n",step);

				PokeUser(EReg::FFT_SEQUENCER_DATA, count);
				PokeUser(EReg::FFT_SEQUENCER_DATA, iqPLDDR3Address);
				PokeUser(EReg::FFT_SEQUENCER_DATA, psdPLDDR3Address);
				PokeUser(EReg::FFT_SEQUENCER_DATA, fftIPLDDR3Address);
				PokeUser(EReg::FFT_SEQUENCER_DATA, fftQPLDDR3Address);
			}
//			TRACE("SEQUENCER_DATA: %lu %lu %lu %lx %lx %lx %lx\n", pattern[step], delay, count, iqPLDDR3Address, psdPLDDR3Address,
//				fftIPLDDR3Address, fftQPLDDR3Address);
		}

		// Program DMA registers to inform fpga to copy to dma address
		PokeDMA(EDMAReg::CDMA_IP_CONTROL, CDMARegControl::DMA_RESET_CONTROL);
		PokeDMA(EDMAReg::CDMA_IP_CONTROL, CDMARegControl::DMA_ENABLE_INTERRUPT);
		PokeDMA(EDMAReg::CDMA_IP_DDRSRCADDRESS, DMASrcAddress);
		PokeDMA(EDMAReg::CDMA_IP_DMADESTADDRESS, DMADestAddress);
		PokeDMA(EDMAReg::CDMA_IP_TOTALBYTES, DMAtotalcount * 4);

#ifdef CSMS_DEBUG
		if(s_trace3220)
		{
			TRACE("C3230::StartSampling: count = %lu DMAtotalcount = %lu from %lx to %lx\n",
					count, DMAtotalcount, DMASrcAddress, DMADestAddress);
		}
#endif
		// Set up wait-for interrupts mask and fire the sequencer
		if (m_waitDone)
		{
			printf("StartSampling: m_waitDone is already true, setting to false\n");	// Should not normally happen
			m_waitDone = false;
		}

		//auto mask = CsmsDigi_agcDone;
		//if (withIq || withPsd || withFft) mask |= (CsmsDigi_sequencerDone | CsmsDigi_CDMA_DONE);
		auto mask = CsmsDigi_sequencerDone;
		if (withIq || withPsd || withFft) mask |= CsmsDigi_CDMA_DONE;
		m_waitMaskAndCount[0] = SetWaitMask(mask, pattern.size());

		//printf ("sequencer --- mask = %lx -- agc window size = %lu\n", mask, adcCount - 2); // debug only
//		TRACE("C3230::StartSampling: Fire sequencer - %d %d %lu %.6f\n", withIq, withPsd, count, collectionTimeSec);
		if (dfMode == EDfMode::NODF)
		{
			PokeUser(EReg::SEQUENCER_CONTROL, 0, CSequencerControl::DF_SLAVE | CSequencerControl::DF_MODE);
			PokeUser(EReg::SEQUENCER_CONTROL, CSequencerControl::SEQUENCER_BLOCK_START, CSequencerControl::SEQUENCER_BLOCK_START);
		}
		else if (dfMode == EDfMode::DFMASTER)
		{

			PokeUser(EReg::SEQUENCER_CONTROL, CSequencerControl::DF_MODE, CSequencerControl::DF_SLAVE | CSequencerControl::DF_MODE);
			PokeUser(EReg::SEQUENCER_CONTROL, CSequencerControl::SEQUENCER_BLOCK_START, CSequencerControl::SEQUENCER_BLOCK_START);
//			CLog::Log(CLog::INFORMATION, "Master fired sequencer for DF");
		}
		else if (dfMode == EDfMode::DFSLAVE)
		{
			PokeUser(EReg::SEQUENCER_CONTROL, CSequencerControl::DF_SLAVE | CSequencerControl::DF_MODE, CSequencerControl::DF_SLAVE | CSequencerControl::DF_MODE);
//			CLog::Log(CLog::INFORMATION, "Slave armed for DF");
		}
		else
		{
			assert(false);
		}
		return true;
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Start all threads
//
void C3230::StartThreads(void)
{
	{
		std::lock_guard<std::mutex> lock(m_shutdownMutex);
		m_shutdown = false;
	}

	ECsmsDigiInterruptSource sourceMask[NUM_INTERRUPT_THREADS];

	static_assert(NUM_INTERRUPT_THREADS >= 4, "NUM_INTERRUPT_THREADS");
	sourceMask[0] = CsmsDigi_agcDone | CsmsDigi_iqBlockDone | CsmsDigi_sequencerDone | CsmsDigi_CDMA_DONE; // Data (block) interrupt
	sourceMask[1] = CsmsDigi_shutDown | CsmsDigi_shutDown2; // Shutdown button interrupts
//	sourceMask[1] = CsmsDigi_shutDown; // Shutdown button interrupts
	sourceMask[2] = CsmsDigi_ADM;	// Real-time IQ data interrupt
	sourceMask[3] = CsmsDigi_WB_DDC;	// Real-time wideband IQ data interrupt

	for (size_t i = 0; i < NUM_INTERRUPT_THREADS; ++i)
	{
		if (sourceMask[i] != CsmsDigi_NONE)
		{
			m_thread[i] = std::thread(&C3230::Thread, this, sourceMask[i], i);
			sched_param param;
			int policy;
			int err = pthread_getschedparam(m_thread[i].native_handle(), &policy, &param);
			if (err != 0)
			{
				printf("Failed to getschedparam: %s\n", std::strerror(err));
			}
			else
			{
				int newPriority = sched_get_priority_max(SCHED_FIFO);
				if (newPriority == -1)
				{
					printf("Failed to get_priority_max: %s\n", std::strerror(errno));
				}
				else
				{
					printf("newPriority: %d\n", newPriority);
					param.sched_priority = newPriority;
					err = pthread_setschedparam(m_thread[i].native_handle(), SCHED_FIFO, &param);
					if (err != 0)
					{
						printf("Failed to setschedparam: %s\n", std::strerror(err));
					}
				}
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Stop all threads
//
void C3230::StopThreads(void)
{
	{
		std::lock_guard<std::mutex> lock(m_shutdownMutex);
		m_shutdown = true;
	}
	for (size_t i = 0; i < NUM_INTERRUPT_THREADS; ++i)
	{
		if (m_thread[i].joinable())
		{
			m_thread[i].join();
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Thread function
//
void C3230::Thread(ECsmsDigiInterruptSource sourceMask, unsigned long idx)
{
	assert(m_master);
	assert(idx < NUM_INTERRUPT_THREADS);

	TRACE("Thread %lu started.\n", idx);
	auto originalMask = sourceMask;		// save original interrupt mask

	auto driverDesc = open(csmsDigiDevice, O_RDWR);
	if (driverDesc < 0)
	{
		CLog::Log(CLog::ERRORS, "C3230::Thread[%lu]: Failed to open %s: %s (%d)", idx, csmsDigiDevice, std::strerror(errno), errno);
		return;
	}
	// Get driver instance (for info only)
	CsmsDigiGetDriverInstanceInfo driverInstanceInfo;
	memset(&driverInstanceInfo, 0, sizeof(driverInstanceInfo));
	if (ioctl(driverDesc, IOCTL_CSMSDIGI_GET_DRIVER_INSTANCE_INFO, &driverInstanceInfo) == -1)
	{
		close(driverDesc);
		CLog::Log(CLog::ERRORS, "C3230::Thread[%lu]: IOCTL_CSMSDIGI_GET_DRIVER_INSTANCE_INFO failed", idx);
		return;
	}
	TRACE("C3230::Thread[%lu] driver instance:%lu\n", idx, driverInstanceInfo.driverInstance);
	unsigned long timeout = THREAD_TIMEOUT / 10;	// in jiffies
	if (ioctl(driverDesc, IOCTL_CSMSDIGI_SET_WAIT_TIMEOUT, &timeout) == -1)
	{
		close(driverDesc);
		CLog::Log(CLog::ERRORS, "C3230::Thread[%lu]: IOCTL_CSMSDIGI_SET_WAIT_TIMEOUT failed", idx);
		return;
	}

	bool callShutdown = true;
	int disableCount = 0;
	CsmsDigi_INTERRUPT interrupts;

	try
	{
#ifdef CSMS_DEBUG
		size_t intrIdx = 0;
#endif
		while (true)
		{
			// Wait for interrupt or timeout
			{
				std::lock_guard<std::mutex> lock(m_shutdownMutex);
				if (m_shutdown)
				{
					TRACE("C3230::Thread[%lu] shutdown\n", idx);
					break;
				}
			}
			interrupts.source = (disableCount == 0 ? originalMask : sourceMask);

			int rv;
			ECsmsDigiInterruptSource mask;
			unsigned long agcCount;
			rv = ioctl(driverDesc, IOCTL_CSMSDIGI_WAIT_FOR_INTERRUPT, &interrupts);
#ifdef CSMS_DEBUG
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			s_intr[intrIdx].intr_count = totalinterrupt;
			s_intr[intrIdx].ioctl_reVal = rv;
			s_intr[intrIdx].ts = ts;
			s_intr[intrIdx].digiIntrId = interrupts;
			++intrIdx;
			intrIdx &= (C_INTR_ID_SZ-1);	//Wrapped around
#endif
			switch(rv)
			{
			case -1:
////			printf("C3230::Thread[%lu] timed out\n", idx);
				if (disableCount > 0) --disableCount;
				break;
			case 1:
				close(driverDesc);
				CLog::Log(CLog::ERRORS, "C3230::Thread[%lu] interrupt already in use", idx);
				return;
			case 0:
				switch(interrupts.source)
				{
				case CsmsDigi_agcDone:
				case CsmsDigi_iqDone:
				case CsmsDigi_iqBlockDone:
				case CsmsDigi_fftDone:
				case CsmsDigi_sequencerDone:
				case CsmsDigi_CDMA_DONE:
					totalinterrupt++; // debug only
					GetWaitMask(m_waitMaskAndCount[0], mask, agcCount);
					//printf("got interrupt source = %lx\n", (long) interrupts.source);
					if (interrupts.source == CsmsDigi_agcDone)
					{
						//printf("got agc done interrupt \n");
						if (mask & CsmsDigi_agcDone)
						{
							if (agcCount > 0)
							{
								--agcCount;
								//printf("got agc done  agcCount = %lu\n ", agcCount);
							}
							else printf("C3230::Thread agcCount zero at interrupt!\n");
							if (agcCount == 0)
							{
								mask &= ~interrupts.source;
							}
						}
					}
					else
					{
						if (interrupts.source == CsmsDigi_CDMA_DONE)
						{
							// check if sequencer done not received yet!
							//if ((mask & CsmsDigi_sequencerDone))
							//	printf("error - cdma before sequencer\n");

							unsigned short agcintcounter =  static_cast<unsigned short>(PeekUser(EReg::AGC_INTERRUPT_CNT) & 0x00ff);
							if (agcCount <= agcintcounter) // clear cdma only if agc counter matches what's expected
							{
								//printf("got expected agc count %d\n", agcintcounter);
								agcCount = 0;
								//mask &= ~(CsmsDigi_agcDone);
								mask &= ~interrupts.source;
							}
							else
							{
								mask &= ~interrupts.source; // clear out of sequence CDMA for now to avoid time out
								//printf("cdma received but agc count not correct expected %lu   got -- %d\n", agcCount, agcintcounter);
							}
						}
						else
						{
							if (interrupts.source == CsmsDigi_sequencerDone)
							{
								unsigned short agcintcounter =  static_cast<unsigned short>(PeekUser(EReg::AGC_INTERRUPT_CNT) & 0x00ff);
								if (agcCount <= agcintcounter)
								{
									mask &= ~interrupts.source;
									//printf("sequencer got expected agc count\n");
								}
							}
							//	printf("got sequencer done interrupt \n");
							//mask &= ~interrupts.source;
						}
					}
//					printf("C3230::Thread[%lu] interrupt source %x, waitMask now %x, %lu\n", idx, interrupts.source, mask, agcCount);
					if (mask == CsmsDigi_NONE)
					{
						// Notify since mask is now zero
//						printf("C3230::Thread notify WaitForSequencer %x %x\n", interrupts.source, m_waitMask[idx].load());
						m_waitMaskAndCount[idx] = SetWaitMask(ECsmsDigiInterruptSource(0xffffffff), 0xffffffff);	// Disable waitMask until reset again
						{
							std::lock_guard<std::mutex> lock(m_waitDoneMutex);
//							printf("C3230::Thread notifying\n");
							//printf("all interrupt cleared!\n");
							m_waitDone = true;
						}
						m_waitDoneCond.notify_one();
					}
					else
					{
						m_waitMaskAndCount[idx] = SetWaitMask(mask, agcCount);	// Update waitMask and count
					}
					break;

				case CsmsDigi_ADM:
					{
						std::string string = Utility::CurrentTimeAsString(Utility::MSEC);
//						printf("%s C3230::Thread[%lu] source:%08x offset:%08lx size:%08lx channel:%ld\n", string.c_str(), idx,
//							interrupts.source, interrupts.offset, interrupts.size, interrupts.channel);
						// Error checking:


						if (interrupts.channel >= m_versionInfo.audioDDCs)
						{
							CLog::Log(CLog::ERRORS, "C3230::Thread[%lu] invalid interrupt channel %lu", idx, interrupts.channel);
							break;
						}

						if (m_iqNarrowBandMem[interrupts.channel].m_EOS.data)
						{
								m_iqNarrowBandMem[interrupts.channel].m_EOS.data = false;
								break;
						}

						auto minOffset = static_cast<unsigned long>(m_iqNarrowBandMem[interrupts.channel].physicalAddress);
						auto maxOffset = minOffset + m_iqNarrowBandMem[interrupts.channel].size;
						if (interrupts.offset < minOffset)
						{
							// Looks like address has wrapped
							interrupts.offset +=  m_iqNarrowBandMem[interrupts.channel].size;
						}
						if (interrupts.offset < minOffset || interrupts.offset >= maxOffset)
						{
							CLog::Log(CLog::ERRORS, "C3230::Thread[%lu] invalid interrupt offset %lu not between %lu and %u", idx,
								interrupts.offset, minOffset, maxOffset);
							break;
						}
						auto samples = m_iqNarrowBandMem[interrupts.channel].psddr3 + (interrupts.offset - minOffset) / sizeof(unsigned long);
						//auto samples = m_iqNarrowBandMem[interrupts.channel].ddr3 + (interrupts.offset - minOffset) / sizeof(unsigned long);
						auto sequence = static_cast<unsigned short>(samples.GetAt(0) >> 16);

						auto channel = static_cast<unsigned char>(samples.GetAt(0) & 0x07);
						if (interrupts.offset != samples.GetAt(1))
						{
							printf("C3230::Thread[%lu]: interrupts.offset:%lu != streamAddr:%lu\n", idx, interrupts.offset, samples.GetAt(1));
						}
						if (interrupts.channel != channel)
						{
							printf("C3230::Thread[%lu]: interrupts.channel:%lu != streamChannel:%u\n", idx, interrupts.channel, channel);
						}
						auto seq = static_cast<unsigned short>(m_iqNarrowBandMem[interrupts.channel].expectedSequence.data++);
						if (seq && seq != sequence)
						{
							printf("C3230::Thread[%lu]: seq:%hu != sequence:%hu\n", idx, seq, sequence);
						}
						// End of error checking
						std::unique_lock<std::mutex> lock(m_callbackCritSect[idx]);
						auto Callback = m_callbacks[idx];
						auto CallbackParams = m_callbackParams[idx];
						lock.unlock();
						if (Callback != nullptr)
						{
							// Assemble ddr3 data into a local buffer
							std::vector<unsigned long> data;
							//printf ("interrupt size = %u \n", interrupts.size);
							if (samples.GetBlock(m_iqNarrowBandMem[interrupts.channel].psddr3, m_iqNarrowBandMem[interrupts.channel].psddr3End, interrupts.size, data))
							//if (samples.GetBlock(m_iqNarrowBandMem[interrupts.channel].psddr3, m_iqNarrowBandMem[interrupts.channel].psddr3End, 5152, data))
							{
								Callback(data, interrupts.channel, false, CallbackParams);
								//TRACE("got adm interrupt ch%d\n", interrupts.channel);
							}
							else
							{
								assert(false);
							}
						}
					}	// case CsmsDigi_ADM
					break;
				case CsmsDigi_WB_DDC:
				{
					if (interrupts.channel != 7)
					{
						CLog::Log(CLog::ERRORS, "C3230::Thread[%lu] invalid interrupt channel %lu", idx, interrupts.channel);
						break;
					}

					if (m_iqWideBandMem.m_EOS.data)
					{
							//m_iqWideBandMem.m_EOS.data = false;
							break;
					}

					auto minOffset = static_cast<unsigned long>(m_iqWideBandMem.physicalAddress);
					auto maxOffset = minOffset + m_iqWideBandMem.size;
					if (interrupts.offset < minOffset)
					{
						// Looks like address has wrapped
						interrupts.offset +=  m_iqWideBandMem.size;
					}
					if (interrupts.offset < minOffset || interrupts.offset >= maxOffset)
					{
						CLog::Log(CLog::ERRORS, "C3230::Thread[%lu] invalid interrupt offset %lu not between %lu and %u", idx,
							interrupts.offset, minOffset, maxOffset);
						break;
					}
					auto samples = m_iqWideBandMem.psddr3 + (interrupts.offset - minOffset) / sizeof(unsigned long);
					//auto samples = m_iqNarrowBandMem[interrupts.channel].ddr3 + (interrupts.offset - minOffset) / sizeof(unsigned long);
					auto sequence = static_cast<unsigned short>(samples.GetAt(0) >> 16);

					auto channel = static_cast<unsigned char>(samples.GetAt(0) & 0x07);
					if (interrupts.offset != samples.GetAt(1))
					{
						printf("C3230::Thread[%lu]: interrupts.offset:%lu != streamAddr:%lu\n", idx, interrupts.offset, samples.GetAt(1));
					}
					if (interrupts.channel != channel)
					{
						printf("C3230::Thread[%lu]: interrupts.channel:%lu != streamChannel:%u\n", idx, interrupts.channel, channel);
					}
					auto seq = static_cast<unsigned short>(m_iqWideBandMem.expectedSequence.data++);
					if (seq && seq != sequence)
					{
						printf("C3230::Thread[%lu]: seq:%hu != sequence:%hu\n", idx, seq, sequence);
					}
					// End of error checking
					std::unique_lock<std::mutex> lock(m_callbackCritSect[idx]);
					auto Callback = m_callbacks[idx];
					auto CallbackParams = m_callbackParams[idx];
					lock.unlock();
					if (Callback != nullptr)
					{
						// Assemble ddr3 data into a local buffer
						std::vector<unsigned long> data;
						//printf ("interrupt size = %u \n", interrupts.size);
						if (samples.GetBlock(m_iqWideBandMem.psddr3, m_iqWideBandMem.psddr3End, interrupts.size, data))
						//if (samples.GetBlock(m_iqNarrowBandMem[interrupts.channel].psddr3, m_iqNarrowBandMem[interrupts.channel].psddr3End, 5152, data))
						{
							Callback(data, interrupts.channel, false, CallbackParams);
							//TRACE("got adm interrupt ch%d\n", interrupts.channel);
						}
						else
						{
							assert(false);
						}
					}
				}	// case CsmsDigi_ADM


					break;

				case CsmsDigi_shutDown:
				case CsmsDigi_shutDown2:
					{
						printf("C3230::Thread[%lu] received shutdown interrupt %08x\n", idx, interrupts.source);
						if (callShutdown)
						{
							CLog::Log(CLog::INFORMATION, "Received shutdown interrupt %08x", interrupts.source);

							if (m_allowShutdown == 0)
							{
								printf("Ignoring shutdown interrupt\n");
								// Disable shutdown2 interrupt
								sourceMask = originalMask & (~CsmsDigi_shutDown2);
								disableCount = 5;
							}
							else
							{
								callShutdown = false;		// Only callback once
								// Shutdown the computer after 10 seconds
								LinuxUtility::ShutdownSystem(10, (m_allowShutdown == 1 ? true: false), []() { BlinkLedStatic(10); });
							}
						}
					}	// case CsmsDigi_shutDown
					break;

				default:
					printf("C3230::Thread[%lu] unexpected interrupt source %u %lu %lu %lu\n", idx, interrupts.source, interrupts.offset, interrupts.size,
						interrupts.channel);
					break;
				}
				break;
			default:
				printf("C3230::Thread[%lu] unexpected return from ioctl %d\n", idx, rv);
				break;
			}
			// Ping watchdog (each instance)
			std::lock_guard<std::mutex> lock(m_wdMutex);
			PingWatchdog();
			if (!callShutdown)
			{
				// shutdown button was pressed
				break;	// Exit now
			}
		}
		// Shutdown
		close(driverDesc);
		printf("C3230::Thread[%lu] exiting: %x\n", idx, sourceMask);
	}
	catch(...)
	{
		CFailure::OnFail(std::current_exception());
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Flip the state of the LED
//
void C3230::ToggleLed(void)
{
	if (m_master)
	{
		CRegLock lock(m_driverDesc);
		auto currentLed = PeekUser(EReg::BIST_GPIOREGISTER) & CBistGPIORegister::LED2_P;
		PokeUser(EReg::BIST_GPIOREGISTER, (currentLed ^ CBistGPIORegister::LED2_P), CBistGPIORegister::LED2_P);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Tune the front-end DDC
//
void C3230::Tune(Units::Frequency freq, bool invertSpectrumData, bool direct, bool shift)
{
	if (m_master)
	{
//		TRACE("DDC freq=%f inv=%d direct=%d\n", freq.Hz<double>(), invertSpectrumData, direct);

		if ((freq != m_freq || invertSpectrumData != m_invertSpectrumData || direct != m_direct || shift != m_shift))
		{
			PokeUser(EReg::SIGNAL_NCOFREQUENCY, CalcFreqWord(freq, m_sampleRate)); // NCO
			unsigned long mix = (invertSpectrumData ? (CFormatControl::MIXER_NEG_EVEN_IMAG | CFormatControl::MIXER_NEG_ODD_IMAG) : 0x00);
			if (shift)
			{
				mix ^= (CFormatControl::MIXER_NEG_EVEN_IMAG | CFormatControl::MIXER_NEG_EVEN_REAL);
			}

			PokeUser(EReg::FORMAT_CONTROL, mix, 0x000f0);
			PokeUser(EReg::SIGNAL_CONTROL, (direct ? CSignalControl::ADC_CHANNEL2_SELECT : CSignalControl::ADC_CHANNEL1_SELECT), CSignalControl::ADC_CHANNEL2_SELECT);
			m_freq = freq;
			m_invertSpectrumData = invertSpectrumData;
			m_direct = direct;
			m_shift = shift;
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Unregister a callback
//
void C3230::UnregisterNewSamplesCallback(unsigned char ch)
{
	ASSERT(ch < NUM_INTERRUPT_THREADS);

	std::lock_guard<std::mutex> lock(m_callbackCritSect[ch]);
	m_callbacks[ch] = nullptr;
	m_callbackParams[ch] = nullptr;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for the sequencer to complete
//
bool C3230::WaitForSequencer(bool withIq, bool withPsd, bool withFft, bool doSamples, double collectionTimeSec, Units::Timestamp startTime)
{
#ifdef CSMS_DEBUG
	if(s_trace3220)
	{
		timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		TRACE("C3230::WaitForSequencer: sec = %ld nsec =%ld; withIq %d, withPsd %d, withFft %d, doSamples %d, sec = %lf, time %s\n",
				ts.tv_sec, ts.tv_nsec, (withIq ? 1 : 0), (withPsd? 1: 0), (withFft? 1 : 0), (doSamples ? 1 :0), collectionTimeSec,
			startTime.Format(true).c_str());
	}
#endif
	//auto startseq = Utility::CurrentTimeAsTimestamp(); // debug only
	assert(m_master);

	int priority = -1;		// Just to avoid maybe-uninitialized warning later
	bool changePriorityOk = true;

	int policy;
	sched_param param;
	if (pthread_getschedparam(pthread_self(), &policy, &param) != 0)
	{
		printf("getschedparam failed\n");
		changePriorityOk = false;
	}
	if (changePriorityOk)
	{
		priority = param.sched_priority;
		param.sched_priority = sched_get_priority_max(policy);
		if (param.sched_priority == -1)
		{
			printf("sched_get_priority_max failed\n");
			changePriorityOk = false;
		}
		else if (pthread_setschedparam(pthread_self(), policy, &param) != 0)
		{
			printf("setschedparam1 failed\n");
			changePriorityOk = false;
		}
	}

	auto now = Utility::CurrentTimeAsTimestamp();
//	printf("WaitForSequencer @ %.9f now:%.9f %d %d\n", startTime * 86400., now * 86400., withIq, withPsd);
	if (startTime > now)
	{
		auto delaySecs = (startTime - now).NanoSeconds<long long>() * 1.e-9;
		collectionTimeSec += delaySecs;
	}
	auto timeout = std::max(static_cast<unsigned long>(std::min(collectionTimeSec, 300.0) * 1000.0), 500UL) + 1000UL;	// msec
	{
		std::unique_lock<std::mutex> lock(m_waitDoneMutex);
//		if (m_waitDone)
//			printf("m_waitDone is already true\n");
		if (!m_waitDoneCond.wait_for(lock, std::chrono::milliseconds(timeout), [this] { return m_waitDone; }))
		{
			printf("m_waitDone is false\n");
		}
#if 0
		while (!m_waitDone)
		{
			if (m_waitDoneCond.wait_for(lock, std::chrono::milliseconds(timeout)) == std::cv_status::timeout)
			{
				break;
			}
		}
#endif
		if (!m_waitDone)
		{
			ECsmsDigiInterruptSource mask;
			unsigned long agcCount;
			GetWaitMask(m_waitMaskAndCount[0], mask, agcCount);
			printf("m_waitDone is false waitmask = %08x agcCount = %lu\n", mask, agcCount);
		}
		m_waitDone = false;		// Reset for next time
	}

//	auto now1 = Utility::CurrentTimeAsDATE();
//	printf("WaitForSequencer after wait now:%.9f\n", now1 * 86400.);
//	if ((PeekUser(EReg::AGC_STATUS) & CAgcStatus::AGC_DONE) == 0)
//	{
//		printf("Threshold counters not ready\n");
//	}
	bool sequencerOk = true;
	if (withIq && (PeekUser(EReg::FORMAT_STATUS) & CFormatStatus::TRANSFER_OK) == 0)
	{
		TRACE("Data not ready %lu\n", timeout);
		sequencerOk = false;
	}
	if (withPsd && (PeekUser(EReg::FFT_STATUS) & CFftStatus::FFT_DM_TRANSFER_OK) == 0)
	{
		TRACE("FFT not ready %lu\n", timeout);
		sequencerOk = false;
	}
	if (withFft && (PeekUser(EReg::FFT_STATUS1) & CFftStatus1::FFT_IQ_DM_TRANSFER_OK) != CFftStatus1::FFT_IQ_DM_TRANSFER_OK)
	{
		TRACE("FFTIQ not ready %lu\n", timeout);
		sequencerOk = false;
	}
	if ((PeekUser(EReg::SEQUENCER_STATUS) & CSequencerStatus::SEQUENCER_FIFO_EMPTY) == 0)
	{
		TRACE(_T("WaitForSequencer: Sequencer FIFO not empty\n"));
		sequencerOk = false;
	}
//	if ((PeekUser(EReg::GLOBAL_DEBUG_REGISTER) & 0x0001) != 0)
//	{
//		printf("WaitForSequencer: fft_delay is too small!\n");
//		PokeUser(EReg::SEQUENCER_CONTROL, CSequencerControl::SEQUENCER_FIFO_FLUSH, CSequencerControl::SEQUENCER_FIFO_FLUSH); // reset bit
//	}
	// Let processing know that data is available
	if (doSamples && (withIq || withPsd || withFft))
	{
#ifdef CSMS_DEBUG
		if(withPsd)
		{
			++s_psdSampleCnt;
		}
#endif
		m_sdma.OnSamples();
	}

	if (changePriorityOk)
	{
		param.sched_priority = priority;
		if (pthread_setschedparam(pthread_self(), policy, &param) != 0)
		{
			printf("setschedparam2 failed\n");
		}
	}
	// debug only
	//printf ("total df scan = %lu", totaldfscan);
	/*auto timeDiff = (Utility::CurrentTimeAsTimestamp() - startseq).NanoSeconds<long>(); // debug only
	if (totaldfscan > 0)
		totaltimediff += timeDiff;
	if (totaldfscan > 0 && (totaldfscan % 10) == 0)
	{
		auto avgtimediff = totaltimediff / 10;
		totaltimediff = 0;
		printf("average of 10 waitforseq on df scans%ld\n", avgtimediff);//debug only
	}*/
	//debug end

#ifdef CSMS_DEBUG
	if(s_trace3220)
	{
		timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		TRACE("C3230::WaitForSequencer ret: sec = %ld nsec =%ld; ok =%d; psdCnt = %u, fpgaSetCnt = %u\n",
				ts.tv_sec, ts.tv_nsec, (sequencerOk ? 1 : 0), s_psdSampleCnt, s_psdFpgaSetCnt);
	}
#endif
	return sequencerOk;
}


void C3230::SetADCChannel(bool direct)
{
	if (direct != m_direct)
	{
		PokeUser(EReg::SIGNAL_CONTROL, (direct ? CSignalControl::ADC_CHANNEL2_SELECT : CSignalControl::ADC_CHANNEL1_SELECT), CSignalControl::ADC_CHANNEL2_SELECT);
		m_direct = direct;
	}
}

bool C3230::GetDeviceDnaStatic(unsigned long(&dna)[2])
{
	auto driverDesc = open(csmsDigiDevice, O_RDWR);
	volatile unsigned long* regsmap;
	if (MapRegisters(driverDesc, regsmap))
	{
		dna[0] = regsmap[static_cast<unsigned long>(EReg::GLOBAL_DEVICEDNA_UPPER)];
		dna[1] = regsmap[static_cast<unsigned long>(EReg::GLOBAL_DEVICEDNA_LOWER)];
		close(driverDesc);
	}
	return true;
}

//////////////////////////////////////////////////////////////////////
//
// Constructor for CRegLock helper class
//
C3230::CRegLock::CRegLock(int driverDesc) :
	m_driverDesc(driverDesc)
{
	if (ioctl(m_driverDesc, IOCTL_CSMSDIGI_LOCK_REGS, nullptr) == -1)
	{
		CLog::Log(CLog::ERRORS, "C3230: IOCTL_WbDigi_LOCK_Regs failed");
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor for CRegLock helper class
//
C3230::CRegLock::~CRegLock(void)
{
	if (ioctl(m_driverDesc, IOCTL_CSMSDIGI_UNLOCK_REGS, nullptr) == -1)
	{
		CLog::Log(CLog::ERRORS, "C3230: IOCTL_WbDigi_UNLOCK_Regs failed");
	}
	return;
}
