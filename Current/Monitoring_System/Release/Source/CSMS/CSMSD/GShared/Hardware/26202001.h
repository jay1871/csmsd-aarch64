/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2010 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#pragma once

#include "DownConverter.h"
#include "Singleton.h"
#include "Units.h"
#include "Vme.h"

class C26202001
:
	// Inheritance
	public CDownConverter,
	protected virtual CVme
{
	// Friends
	friend class C26202001Page; // VCP needs direct access to set 10 MHz filters (not used outside VCP)

public:
	// Constants
	static const unsigned char ATTEN_STEP = 10; // dB
	static const signed char BYPASS_LOSS = 3; // dB
	static const unsigned char MAX_ATTEN = 30; // dB
	static const signed char MAX_GAIN = 36; // dB
	static const unsigned char MIN_ATTEN = 0; // dB
	static const unsigned long NARROW_BW_HZ = 500000; // Hz
	static const short NOISE_LEVEL_DBMHZ = -130; // dBm/Hz
	static const unsigned long NOISE_LOW_FREQ_HZ = 1200000; // Hz
	static const unsigned long RF_HIGH_FREQ_HZ = 30000000; // Hz
	static const unsigned long RF_LOW_FREQ_HZ = 2000000; // Hz
	static const unsigned long WIDE_BW_HZ = 2000000; // Hz

	// Functions
	static Units::Frequency GetCalFreq(Units::Frequency freq) { return freq < NOISE_LOW_FREQ_HZ ? 0 : freq; }

protected:
	// Constants

	// Types
	enum EInput { RF = 0x1, NOISE = 0x2, TERM = 0x3 }; // Values match hardware

	struct SCardStatus
	{
		unsigned short id : 3;
		unsigned short cardType : 3;
		unsigned short cardClass : 2;
		unsigned short : 5;
		unsigned short version : 3;
	};

	// Functions
	C26202001(void);
	~C26202001(void) {}
	unsigned short GetControlReg(void) const { return Peek(CONTROL_REG); }
	unsigned char GetVersion(void) const;
	bool IsDualChannel(void) const { return m_id == 0; }
	bool IsPresent(void) const { return m_present; }
	unsigned char GetAtten(void) const { return m_atten; }
	bool GetBypass(void) const;
	SCardStatus GetCardStatus(bool force = false) const;
	float GetGainRatio(void) const;
	unsigned char SetAtten(unsigned char atten);
	void SetBand(unsigned char band);
	void SetBypass(bool bypass);
	void SetInput(EInput input, signed char chOrBoth = -1);
	void Tune(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ bool& bypass, _Out_ Units::FreqPair& freqLimits,
		_Out_ size_t& band);

private:
	// Constants
	static const SBandSettings BAND_SETTINGS_[];
	static const BandSettings BAND_SETTINGS;
	static const unsigned int CARD_CLASS = 0xc;
	static const unsigned int CARD_TYPE = 0x6;
	static const unsigned int VME_BASE_ADDR = (0x770000 | (CARD_CLASS << 12) | (CARD_TYPE << 8));
	static const unsigned int STATUS_REG = 0x00;
	static const unsigned int CONTROL_REG = 0x02;
	static LPCTSTR MUTEX_NAME;

	// Functions
	unsigned short Peek(unsigned char reg) const { return CVme::Peek(VME_BASE_ADDR + 32 * m_id + reg); }
	void Poke(unsigned char reg, unsigned short data) { CVme::Poke(VME_BASE_ADDR + 32 * m_id + reg, data); }
	virtual void SetSettleTime(unsigned int) {}

	// Data
	unsigned char m_atten;
	bool m_first;
	unsigned short m_id;
	mutable CMutex m_mutex;
	bool m_present;
};

// Use selectany to put this here rather than in the .cpp so the size of the array can be determined by users of the class
__declspec(selectany) const CDownConverter::SBandSettings C26202001::BAND_SETTINGS_[] =
{ // low 20 dB, low 3 dB, high 3 dB, high 20 dB, data
	{        0,     1000,  2250000,  2500000, 0x0c40 },
	{  1800000,  2200000,  4200000,  4800000, 0x0000 },
	{  2800000,  3250000,  6200000,  7000000, 0x0100 },
	{  4500000,  5500000,  8800000, 10000000, 0x0200 },
	{  6800000,  7750000, 12000000, 13400000, 0x0300 },
	{  9300000, 10750000, 16000000, 18000000, 0x0400 },
	{ 13000000, 14750000, 21250000, 23500000, 0x0500 },
	{ 17000000, 19250000, 25750000, 29000000, 0x0600 },
	{ 23000000, 24500000, 30012500, 33000000, 0x0700 }
};

