/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#pragma once

#include "80682017.h"
#include "Digitizer.h"
#include "EquipCtrlMsg.h"
#include "Singleton.h"
#include "Units.h"

class C72332004
:
	// Inheritance
	protected virtual C80682017
{
public:
	// Constants
	static const unsigned long BITE_FREQ_HZ = 242730000;
	static const long BITE_DBM = -30;
	static const unsigned long CROSSOVER_FREQ_HZ = 1050000000;
	static const float MONITOR_SPLITTER_LOSS_DBM;
	static const unsigned char NUM_ANTS = 9;

	// Functions

	//Horizontal frequency limit for 643 Antenna is the same as the supported
	// frequency spectrum range of 643 Antenna.
	static Units::Frequency Get643HorizontalLowerFreqLimit() { return( Units::Frequency(RF643_LOW_FREQ_HZ) ); };
	static Units::Frequency Get643HorizontalUpperFreqLimit() { return( Units::Frequency(RF643_HIGH_FREQ_HZ) ); };
	
	//643-3 Horizontal Metric frequency range.
	static Units::Frequency Get643_3HorMetricLowerFreqLimit() { return( Units::Frequency(RF643_LOW_FREQ_HZ) ); };
	static Units::Frequency Get643_3HorMetricUpperFreqLimit() { return( Units::Frequency(RF643_HIGH_FREQ_HZ) ); };

protected:
	// Types
	enum ERefMode { RECEIVE, TX_BITE };

	enum ESource // Some values match hardware - don't change!
	{
		VHF		= 0x00,
		ANT_1	= 0x10,
		ANT_2	= 0x20,
		ANT_3	= 0x30,
		ANT_4	= 0x40,
		ANT_5	= 0x50,
		ANT_6	= 0x60,
		ANT_7	= 0x70,
		ANT_8	= 0x80,
		ANT_9	= 0x90,
		AUX		= 0xa0,
		NOISE	= 0x21,
		ANT_1H	= 0x17,
		ANT_2H	= 0x27,
		ANT_3H	= 0x37,
		ANT_4H	= 0x47,
		ANT_5H	= 0x57,
		ANT_6H	= 0x67,
		ANT_7H	= 0x77,
		ANT_8H	= 0x87,
		ANT_9H	= 0x97,
		AUX_H	= 0xa7,
		REF		= 1,
		BITE	= 2,
		EXT		= 3,
		TERM	= 4,

		//Define special (overflow) control byte value for Reference Antenna
		// with Horizontal polarization as for now, hardware does not have
		// the option to connect to Horizontal Reference Antenna with
		// Terminated sample output.
		REF_HORIZON = 0x101
	};

public:
	// Constants
	static const ESource GRAY_CODE[2][NUM_ANTS];

	// Functions
	inline static size_t Index(ESource source);

protected:
	// Friends
	friend ESource& operator++(ESource& s);

	// Functions
	C72332004(void); // Can only be constructed by a derived class
	virtual ~C72332004(void);
	static unsigned char GetControlByte(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf);
	unsigned char GetCurrentControlByte(void) const { CSingleLock lock(&m_critSect, TRUE); return m_controlByte; }
	void SetSwitch(unsigned char data, bool force);
	void SetSwitch(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf);
	inline static ESource Source(SEquipCtrlMsg::EAnt ant);
	void ValidateLoopback(unsigned char loopback);

private:
	// Constants
	static const unsigned long RF643_LOW_FREQ_HZ = 20000000; // Hz. 20 MHz
	static const unsigned long RF643_HIGH_FREQ_HZ = 3000000000; // Hz. 3 GHz.
	
	//============= Define CONTROL BYTE definition ===
	
	//Lower nibble CONTROL BYTE value when Vertical sample antenna not
	// connected to sample Rx.
	static const unsigned char N_SAMPLE_VERT_ANT_OFF = 0x01;
	
	//The following upper nibble is only valid if sample antenna is not
	// connected to the sample Rx.
	static const unsigned char N_REF_TERM_SAMPLE_TERM = 0x10;
	static const unsigned char N_REF_BITE_SAMPLE_TERM = 0x40;
	static const unsigned char N_REF_EXT_SAMPLE_TERM = 0x60;

	static const unsigned char B_HIGH_BAND = 0x08;
	static const unsigned char C_BITE_TX_REFOUT_TERMINATED = 0x04;
	static const unsigned char C_REF_VERTICAL_ON_SAMPLE_TERM = 0x01;

	//@note: 643-3 does not have selection of Ref Antenna to Ref Rx with
	//       terminated sample for Horizontal polarization, so AUX10 input
	//       is used for Sample Rx.
	static const unsigned char C_REF_HORIZONTAL_ON_SAMPLE_TERM = 0xA7;
	
	//============= End of CONTROL BYTE definition ===
	
	// Functions
	unsigned int GetSettleTime(unsigned char controlByte) const;
	virtual void SetSettleTime(unsigned int) {}

	// Data
	volatile unsigned char m_controlByte;
	mutable CCriticalSection m_critSect;
	CSingleton<CDigitizer> m_digitizer;
	unsigned int m_validateFailCount;
};


//////////////////////////////////////////////////////////////////////
//
// ++ operator for ESource
//
inline C72332004::ESource& operator++(C72332004::ESource& s)
{
	switch(s)
	{
	case C72332004::ANT_1:
	case C72332004::ANT_2:
	case C72332004::ANT_3:
	case C72332004::ANT_4:
	case C72332004::ANT_5:
	case C72332004::ANT_6:
	case C72332004::ANT_7:
	case C72332004::ANT_8:
	case C72332004::ANT_1H:
	case C72332004::ANT_2H:
	case C72332004::ANT_3H:
	case C72332004::ANT_4H:
	case C72332004::ANT_5H:
	case C72332004::ANT_6H:
	case C72332004::ANT_7H:
	case C72332004::ANT_8H:
		s = static_cast<C72332004::ESource>(s + 0x10);
		break;

	case C72332004::ANT_9:
		s = C72332004::ANT_1;
		break;

	case C72332004::ANT_9H:
		s = C72332004::ANT_1H;
		break;

	default:
		break;
	}

	return s;
}


//////////////////////////////////////////////////////////////////////
//
// Conversion function for ESource
//
inline C72332004::ESource C72332004::Source(SEquipCtrlMsg::EAnt ant)
{
	switch(ant)
	{
	case SEquipCtrlMsg::ANT1:
		return C72332004::REF;

	case SEquipCtrlMsg::ANT1H:
		return C72332004::REF_HORIZON;

	case SEquipCtrlMsg::ANT2:
	case SEquipCtrlMsg::ANT3:
	case SEquipCtrlMsg::SMPL_RF2:
	case SEquipCtrlMsg::SMPL_RF3:
		return C72332004::TERM;

	case SEquipCtrlMsg::DF_ANT_1V:
		return C72332004::ANT_1;

	case SEquipCtrlMsg::DF_ANT_2V:
		return C72332004::ANT_2;

	case SEquipCtrlMsg::DF_ANT_3V:
		return C72332004::ANT_3;

	case SEquipCtrlMsg::DF_ANT_4V:
		return C72332004::ANT_4;

	case SEquipCtrlMsg::DF_ANT_5V:
		return C72332004::ANT_5;

	case SEquipCtrlMsg::DF_ANT_6V:
		return C72332004::ANT_6;

	case SEquipCtrlMsg::DF_ANT_7V:
		return C72332004::ANT_7;

	case SEquipCtrlMsg::DF_ANT_8V:
		return C72332004::ANT_8;

	case SEquipCtrlMsg::DF_ANT_9V:
		return C72332004::ANT_9;

	case SEquipCtrlMsg::DF_ANT_1H:
		return C72332004::ANT_1H;

	case SEquipCtrlMsg::DF_ANT_2H:
		return C72332004::ANT_2H;

	case SEquipCtrlMsg::DF_ANT_3H:
		return C72332004::ANT_3H;

	case SEquipCtrlMsg::DF_ANT_4H:
		return C72332004::ANT_4H;

	case SEquipCtrlMsg::DF_ANT_5H:
		return C72332004::ANT_5H;

	case SEquipCtrlMsg::DF_ANT_6H:
		return C72332004::ANT_6H;

	case SEquipCtrlMsg::DF_ANT_7H:
		return C72332004::ANT_7H;

	case SEquipCtrlMsg::DF_ANT_8H:
		return C72332004::ANT_8H;

	case SEquipCtrlMsg::DF_ANT_9H:
		return C72332004::ANT_9H;

	default:
		ASSERT(FALSE);
		return C72332004::TERM;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Conversion function for ESource to get DF antenna index
//
inline size_t C72332004::Index(ESource source)
{
	switch(source)
	{
	case ANT_1:
	case ANT_2:
	case ANT_3:
	case ANT_4:
	case ANT_5:
	case ANT_6:
	case ANT_7:
	case ANT_8:
	case ANT_9:
	case ANT_1H:
	case ANT_2H:
	case ANT_3H:
	case ANT_4H:
	case ANT_5H:
	case ANT_6H:
	case ANT_7H:
	case ANT_8H:
	case ANT_9H:
		return ((source & 0xf0) >> 4) - 1;

	default:
		ASSERT(FALSE);
		return 0;
	}
}
