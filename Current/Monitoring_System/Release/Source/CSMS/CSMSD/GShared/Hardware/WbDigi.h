/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2011-2015 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#ifdef CSMS_DIGI
// Wb Digitizer User Registers.
//
typedef struct
{
	unsigned long Regs;
	size_t size;
} SWbDigiRegs;

// Wb Digitizer DMA memory.
//
typedef struct
{
	unsigned long Mem;
	size_t size;
} SWbDigiDMAMemory;
#else

#ifndef CTL_CODE
#include <WinIoCtl.h>
#endif


// Wb Digitizer User Registers.
//
typedef struct
{
	void* Regs;
	size_t size;
} SWbDigiRegs;


// Wb Digitizer DMA memory.
//
typedef struct
{
	void* Mem;
	size_t size;
} SWbDigiDMAMemory;
#endif


// Wb Digitizer program Forward DMA(s).
//
typedef struct
{
	size_t channel; // Forward DMA channel to use.
	size_t size;    // Size in bytes.
	unsigned long count;    // 0 = Streaming Mode, > 0 = Number of blocks for Block mode.
} SWbDigiFwdDMA;

typedef struct
{
	size_t channel; // Forward DMA channel to use.
	size_t count;    // Number of streaming packets before generating an interrupt. 
                 // Must be >= 1. At present, nly valid for forward DMA(s).
} SWbDigiDMAStreamPacketCount;


// Wb Digitizer program Reverse DMA(s).
//
typedef struct
{
	size_t channel;     // Reverse DMA channel to use.

	size_t fwdChannel;  // Forward DMA input channel to use if not in Block mode. 
	                    // Note, Guardian uses either 0 or 1.

	size_t offset;      // Starting offset of data in Reverse DMA user memory if in Block mode.
	                    // Otherwise, user specified offset when streaming from DMA 0 or 1.

	size_t size;        // Size in bytes.

	size_t control;     // 0 = Block_MODE_CONTROL 
	                    // 1 = RT_OFFSET_CONTROL 
	                    // 2 = USER_OFFSET_CONTROL
} SWbDigiRevDMA;


// Wb Digitizer DMA data transfer for first, second and third DMA. 
// Legacy
//
typedef struct
{
	size_t size;   // Size in bytes.
	unsigned long count;   // 0 = Streaming Mode, > 0 = Number of blocks
} SWbDigiChannelDMA;


// Legacy 
//
typedef struct
{
	void* Mem;
	size_t size;
} SWbDigiChannelDMAMemory;


// Legacy 
//
typedef struct
{
	size_t offset;   // Starting offset of data in Reverse DMA user memory.
	unsigned long  size;     // Size in bytes.
} SWbDigiRevChannelDMA;


// Legacy
//
typedef struct
{
	size_t offset;   // Starting offset of data in DMA 3 user memory.
	unsigned long size;   // Size in bytes of data.
} SWbDigiChannel3DMA;


// Mode types used by IOCTL_WbDigi_PROG_REV_DMA.
//
enum REV_DMA_CONTROLS
{
	BLOCK_MODE_CONTROL=0,
	RT_OFFSET_CONTROL=2,
	USER_OFFSET_CONTROL=3
};


// Wb Digitizer and CSMS interrupt(s) source
//
#ifdef CSMS_DIGI
typedef enum
{
	CsmsDigi_DMA0 = 0x00000001,
	CsmsDigi_DMA1 = 0x00000002,
	CsmsDigi_DMA2 = 0x00000004,
	CsmsDigi_DMA3 = 0x00000008,
	CsmsDigi_DMA4 = 0x00000010,
	CsmsDigi_DMA5 = 0x00000020,
	CsmsDigi_DMA6 = 0x00000040,
	CsmsDigi_DMA7 = 0x00000080,
	CsmsDigi_agcDone  = 0x00000100,
	CsmsDigi_agcFifoFull = 0x00000200,
	CsmsDigi_iqDone = 0x00000400,
	CsmsDigi_iqBlockDone = 0x00000800,
	CsmsDigi_iqDataMoverErr = 0x00001000,
	CsmsDigi_formatFifoDataErr = 0x00002000,
	CsmsDigi_fftDone = 0x00004000,
	CsmsDigi_fftDataMoverErr = 0x00008000,
	CsmsDigi_fftOutputFifoDataErr = 0x00010000,
	CsmsDigi_fftInputFifoDataErr = 0x00020000,
	CsmsDigi_reserved0 = 0x00040000,
	CsmsDigi_reserved1 = 0x00080000,
	CsmsDigi_reserved2 = 0x00100000,
	CsmsDigi_reserved3 = 0x00200000,
	CsmsDigi_shutDown = 0x00400000,
	CsmsDigi_rfPllLock = 0x00800000,
	CsmsDigi_bistTimer = 0x01000000,
	CsmsDigi_overTemp = 0x02000000,
	CsmsDigi_onePps = 0x04000000
} ECsmsDigiInterruptSource;
#else
typedef enum
{
	WbDigi_INTERRUPT_DMA1 = 0x00000001, // Legacy
	WbDigi_INTERRUPT_DMA2 = 0x00000002, // Legacy
	WbDigi_INTERRUPT_DMA3 = 0x00000004, // Legacy
	WbDigi_INTERRUPT_DMA4 = 0x00000008, // Legacy
	WbDigi_INTERRUPT_DMA5 = 0x00000010, // Legacy
	WbDigi_INTERRUPT_DMA6 = 0x00000020, // Legacy
	WbDigi_INTERRUPT_DMA7 = 0x00000040, // Legacy
	WbDigi_INTERRUPT_DMA8 = 0x00000080, // Legacy
	WbDigi_OLD_SQ_FINISHED = 0x00000100, // Legacy
	WbDigi_SQ_FINISHED = 0x00000200, // Legacy
	WbDigi_BLK_FINISHED = 0x00000400, // Legacy
	WbDigi_DMA0_V1 = 0x00000001,
	WbDigi_DMA1_V1 = 0x00000002,
	WbDigi_DMA2_V1 = 0x00000004,
	WbDigi_DMA3_V1 = 0x00000008,
	WbDigi_DMA4_V1 = 0x00000010,
	WbDigi_DMA5_V1 = 0x00000020,
	WbDigi_DMA6_V1 = 0x00000040,
	WbDigi_DMA7_V1 = 0x00000080,
	WbDigi_OSQF_EOT0_V1 = 0x00000100,
	WbDigi_SQF_EOT1_V1 = 0x00000200,
	WbDigi_BLKF_EOT2_V1 = 0x00000400,
	WbDigi_NO_CLK = 0x00000800,
	WbDigi_PLL_UNLOCK = 0x00001000,
	WbDigi_NO_EXT_CLK = 0x00002000,
	WbDigi_DF_DATA_LOSS = 0x00004000,
	WbDigi_DEMOD_DATA_LOSS = 0x00008000,
	WbDigi_CH_A_OVERFLOW = 0x00010000,
	WbDigi_CH_B_OVERFLOW = 0x00020000,
	WbDigi_SQ_ERROR = 0x00040000,
	WbDigi_AGC_FIFO_FULL = 0x00080000,
	WbDigi_TEMP_ALARM = 0x00100000,
	WbDigi_VCC_INT_ALARM = 0x00200000,
	WbDigi_VCC_AUX_ALARM = 0x00400000,
	WbDigi_CFG_PAGE_DONE = 0x00800000,
	WbDigi_RD_BLK_RDY = 0x01000000,
	WbDigi_EOS_1 = 0x02000000,
	WbDigi_EOS_2 = 0x04000000
} EWbDigiInterruptSource;
#endif


// Wb Digitizer Interrupt(s) request.
//
typedef struct
{
	unsigned long offset;  // Offset in memory where data exists.
	unsigned long size;  // Size in bytes of available data.
	unsigned long mode;  // 0 = block mode, 1 = streaming mode.

#ifdef CSMS_DIGI
	ECsmsDigiInterruptSource source;  // Source of interrupt.
#else
	EWbDigiInterruptSource source;  // Source of interrupt.
#endif
} WbDigi_INTERRUPT;


#ifdef CSMS_DIGI
// IOCTLs
// Acquire a lock on the Regs
//	Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_WbDigi_LOCK_Regs 0x801

// Release a lock on the Regs  = 0x802
//	Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_WbDigi_UNLOCK_Regs 0x803

// Get Regs.
//	Input buffer:  NULL
//	Output buffer: SWbDigiRegs*
//
#define IOCTL_WbDigi_GET_Regs 0x804

// Wait for interrupt
//	Input buffer:  const unsigned long* (mask of EWbDigiInterruptSource)
//	Output buffer: EWbDigiInterruptSource*
//
#define IOCTL_WbDigi_WAIT_FOR_INTERRUPT 0x805

// Flush Interrupt Queue
// Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_WbDigi_FLUSH_INTERRUPT_QUEUE 0x806

// Reset Digitizer
// Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_WbDigi_RESET_DIGITIZER 0x807


//	Input buffer:  const unsigned long* (mask of EWbDigiInterruptSource
//	Output buffer: NULL
//
#define IOCTL_WbDigi_ENABLE_INTERRUPTS 0x808

// Get DMA Memory
//	Input buffer:  ULONG channel number 0 - 7.
//	Output buffer: SWbDigiDMAMemory*
//
#define IOCTL_WbDigi_GET_DMA_MEMORY 0x809

// Block/Stream Data transfer via Forward DMA
// Input buffer:  SWbDigiFwdDMA*
//	Output buffer: NULL
// 
#define IOCTL_WbDigi_PROG_FWD_DMA 0x810

// Block/Stream Data transfer via Reverse DMA
// Input buffer:  SWbDigiRevDMA*
//	Output buffer: NULL
// 
#define IOCTL_WbDigi_PROG_REV_DMA 0x8011

// Stop Forward DMA stream.
// Input buffer:  ULONG forward DMA channel number 0 - 3.
//	Output buffer: NULL
// 
#define IOCTL_WbDigi_STOP_FWD_DMA 0x8012

// Flush DMA queue.
// Input buffer:  ULONG DMA channel number 0 - 7.
//	Output buffer: NULL
//
#define IOCTL_WbDigi_FLUSH_DMA_QUEUE 0x8013

// DMA Reset
// Input buffer:  ULONG channel number 0 - 7.
//	Output buffer: NULL
//
#define IOCTL_WbDigi_RESET_DMA  0x8014

// DMA Stream Packet Count
// Input buffer:  SWbDigiDMAStreamPacketCount*
//	Output buffer: NULL
//
#define IOCTL_WbDigi_DMA_STREAM_PACKET_COUNT 0x8015
#else // IOCTLS for Windows WbDigi 3220 driver.
// IOCTLs
// Acquire a lock on the Regs
//	Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_WbDigi_LOCK_Regs CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800, METHOD_NEITHER, FILE_ANY_ACCESS)

// Release a lock on the Regs
//	Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_WbDigi_UNLOCK_Regs CTL_CODE(FILE_DEVICE_UNKNOWN, 0x801, METHOD_NEITHER, FILE_ANY_ACCESS)

// Get Regs.
//	Input buffer:  NULL
//	Output buffer: SWbDigiRegs*
//
#define IOCTL_WbDigi_GET_Regs CTL_CODE(FILE_DEVICE_UNKNOWN, 0x802, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Wait for interrupt
//	Input buffer:  const unsigned long* (mask of EWbDigiInterruptSource)
//	Output buffer: EWbDigiInterruptSource*
//
#define IOCTL_WbDigi_WAIT_FOR_INTERRUPT CTL_CODE(FILE_DEVICE_UNKNOWN, 0x803, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Get Channel 1 DMA Memory (Legacy)
//	Input buffer:  NULL
//	Output buffer: SWbDigiChannelDMAMemory*
//
#define IOCTL_WbDigi_GET_CHANNEL1_DMA_MEMORY CTL_CODE(FILE_DEVICE_UNKNOWN, 0x804, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Data Transfer Channel 1 DMA (Legacy)
// Input buffer:  SWbDigiChannelDMA*
//	Output buffer: NULL
//
#define IOCTL_WbDigi_CHANNEL1_DMA CTL_CODE(FILE_DEVICE_UNKNOWN, 0x805, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Get Channel 2 DMA Memory (Legacy)
//	Input buffer:  NULL
//	Output buffer: SWbDigiChannelDMAMemory*
//
#define IOCTL_WbDigi_GET_CHANNEL2_DMA_MEMORY CTL_CODE(FILE_DEVICE_UNKNOWN, 0x806, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Data Transfer Channel 2 DMA (Legacy)
// Input buffer:  SWbDigiChannelDMA*
//	Output buffer: NULL
//
#define IOCTL_WbDigi_CHANNEL2_DMA CTL_CODE(FILE_DEVICE_UNKNOWN, 0x807, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Stop Streaming on DMA Channel1 (Legacy)
// Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_WbDigi_CHANNEL1_DMA_STOP_STREAMING CTL_CODE(FILE_DEVICE_UNKNOWN, 0x808, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Stop Streaming on DMA Channel2 (Legacy)
// Input buffer:  Null
//	Output buffer: Null
//
#define IOCTL_WbDigi_CHANNEL2_DMA_STOP_STREAMING CTL_CODE(FILE_DEVICE_UNKNOWN, 0x809, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Channel1 DMA Reset (Legacy)
// Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_WbDigi_CHANNEL1_DMA_RESET CTL_CODE(FILE_DEVICE_UNKNOWN, 0x80a, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Channel 2 DMA  Reset (Legacy)
// Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_WbDigi_CHANNEL2_DMA_RESET CTL_CODE(FILE_DEVICE_UNKNOWN, 0x80b, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Flush Interrupt Queue
// Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_WbDigi_FLUSH_INTERRUPT_QUEUE CTL_CODE(FILE_DEVICE_UNKNOWN, 0x80c, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Flush DMA1 Queue (Legacy)
// Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_WbDigi_FLUSH_DMA1_QUEUE CTL_CODE(FILE_DEVICE_UNKNOWN, 0x80d, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Flush DMA2 Queue (Legacy)
// Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_WbDigi_FLUSH_DMA2_QUEUE CTL_CODE(FILE_DEVICE_UNKNOWN, 0x80e, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Reset Digitizer
// Input buffer:  NULL
//	Output buffer: NULL
//
#define IOCTL_WbDigi_RESET_DIGITIZER CTL_CODE(FILE_DEVICE_UNKNOWN, 0x80f, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Get Channel 3 DMA Memory (Used by legacy driver T615-MM-1F and earlier.)
//	Input buffer: NULL
//	Output buffer: SWbDigiChannel3DMAMemory*
#define IOCTL_WbDigi_GET_CHANNEL3_DMA_MEMORY CTL_CODE(FILE_DEVICE_UNKNOWN, 0x810, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Data Transfer Channel 3 DMA (Used by legacy driver T615-MM-1F and earlier.)
// Input buffer: SWbDigiChannel3DMA*
//	Output buffer: 
#define IOCTL_WbDigi_CHANNEL3_DMA CTL_CODE(FILE_DEVICE_UNKNOWN, 0x811, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Channel3 DMA Reset (Used by legacy driver T615-MM-1F and earlier.)
// Input buffer:
//	Output buffer: 
#define IOCTL_WbDigi_CHANNEL3_DMA_RESET CTL_CODE(FILE_DEVICE_UNKNOWN, 0x8012, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Enable interrupt(s)
//	Input buffer:  const unsigned long* (mask of EWbDigiInterruptSource
//	Output buffer: NULL
//
#define IOCTL_WbDigi_ENABLE_INTERRUPTS CTL_CODE(FILE_DEVICE_UNKNOWN, 0x8013, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Get DMA Memory
//	Input buffer:  ULONG channel number 0 - 7.
//	Output buffer: SWbDigiDMAMemory*
//
#define IOCTL_WbDigi_GET_DMA_MEMORY CTL_CODE(FILE_DEVICE_UNKNOWN, 0x814, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Block/Stream Data transfer via Forward DMA
// Input buffer:  SWbDigiFwdDMA*
//	Output buffer: NULL
// 
#define IOCTL_WbDigi_PROG_FWD_DMA CTL_CODE(FILE_DEVICE_UNKNOWN, 0x815, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Block/Stream Data transfer via Reverse DMA
// Input buffer:  SWbDigiRevDMA*
//	Output buffer: NULL
// 
#define IOCTL_WbDigi_PROG_REV_DMA CTL_CODE(FILE_DEVICE_UNKNOWN, 0x816, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Stop Forward DMA stream.
// Input buffer:  ULONG forward DMA channel number 0 - 3.
//	Output buffer: NULL
// 
#define IOCTL_WbDigi_STOP_FWD_DMA CTL_CODE(FILE_DEVICE_UNKNOWN, 0x817, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Flush DMA queue.
// Input buffer:  ULONG DMA channel number 0 - 7.
//	Output buffer: NULL
//
#define IOCTL_WbDigi_FLUSH_DMA_QUEUE CTL_CODE(FILE_DEVICE_UNKNOWN, 0x818, METHOD_BUFFERED, FILE_ANY_ACCESS)

// DMA Reset
// Input buffer:  ULONG channel number 0 - 7.
//	Output buffer: NULL
//
#define IOCTL_WbDigi_RESET_DMA CTL_CODE(FILE_DEVICE_UNKNOWN, 0x819, METHOD_BUFFERED, FILE_ANY_ACCESS)

// DMA Stream Packet Count
// Input buffer:  SWbDigiDMAStreamPacketCount*
//	Output buffer: NULL
//
#define IOCTL_WbDigi_DMA_STREAM_PACKET_COUNT CTL_CODE(FILE_DEVICE_UNKNOWN, 0x81A, METHOD_BUFFERED, FILE_ANY_ACCESS)
#endif

