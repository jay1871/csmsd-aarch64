/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "Digitizer.h"
#include "DownConverter.h"
#include "Singleton.h"
#include "Units.h"

class C51412010
:
	// Inheritance
	public CDownConverter
{
public:
	// Types
	struct SStatus
	{
		unsigned char loLocked : 1;
		unsigned char tenMhzAccurate : 1;
		unsigned char : 8;
	};

	// Constants
	// @note: At cross-over ANT_CROSSOVER_FREQ_HZ value, high-band Antenna
	//        input will be used.
	static const unsigned long long ANT_CROSSOVER_FREQ_HZ = 200000000;

	//Frequency range for 5141 system with 640 Antenna is 20 MHz to Max. as
    // specified by C-Band (usually at 6 GHz).
	static const unsigned long long LOW_FREQ_HZ = 20000000; // 20 MHz

	static const unsigned long long MAX_FREQ_HZ = 8000000000; // 8 GHz
	static const float NOMINAL_TEMP; // �C

	// Functions
	static float GetConverterGainAdjustmentDb(float temp) { return (temp - NOMINAL_TEMP) * CONVERTER_GAIN_SLOPE; };
	static Units::FreqPair GetRfFreqs(Units::Frequency rfFreq, _In_ const Units::FreqPair& rxFreqs);
	static Units::Frequency GetRxFreq(Units::Frequency rfFreq);
	static bool IsSpectrumInverted(Units::Frequency freq) { return GetBand(freq) != 0; }

protected:
	// Constants
	static const BandSettings BAND_SETTINGS;
	static const Units::Frequency LO_SETTINGS[];

	// Functions
	C51412010(void); // Can only be constructed by a derived class
	template<size_t N> C51412010(const CDownConverter::EGainMode (&gainModes)[N]);
	virtual ~C51412010(void);
	static unsigned char GetControlByte(Units::Frequency freq);
	unsigned char GetCurrentControlByte(void) const { return unsigned short(m_digitizer->GetTtlData() & 0x00ff); }
	CDownConverter::EGainMode GetGainMode(Units::Frequency freq) const { return CDownConverter::GetGainMode(freq, BAND_SETTINGS); }
	SStatus GetStatus(void) const { SStatus status =
		{ unsigned char((m_digitizer->GetTtlData() & 0x100) >> 8), unsigned char((m_digitizer->GetTtlData() & 0x200) >> 9) }; return status; }
	void SetBand(unsigned char controlByte) { m_digitizer->SetTtlData(controlByte); }
	void Tune(Units::Frequency freq);

private:
	// Constants
	static const SBandSettings BAND_SETTINGS_[];
	static const float CONVERTER_GAIN_SLOPE; // dB / �C
	static LPCTSTR MUTEX_NAME;

	// Types
	struct SECURITY_ATTRIBUTES_ALL_ACCESS : public SECURITY_ATTRIBUTES
	{
		SECURITY_ATTRIBUTES_ALL_ACCESS(void)
		{
			if(!InitializeSecurityDescriptor(&m_secDesc, SECURITY_DESCRIPTOR_REVISION))
			{
				AfxThrowFileException(CFileException::genericException, GetLastError());
			}

#pragma warning(suppress : 6248) // Null DACL
			if(!SetSecurityDescriptorDacl(&m_secDesc, TRUE, nullptr, TRUE))
			{
				AfxThrowFileException(CFileException::genericException, GetLastError());
			}

			nLength = sizeof(SECURITY_ATTRIBUTES);
			lpSecurityDescriptor = &m_secDesc;
			bInheritHandle = FALSE;

			return;
		}

	private:
		SECURITY_DESCRIPTOR m_secDesc;
	};

	// Functions
	static size_t GetBand(Units::Frequency freq);
	_Ret_ virtual const CDownConverter::SReceiverGainModes& GetGainModes(void) const { return m_gainModes; }
	unsigned int GetSettleTime(unsigned char controlByte) const;
	virtual void SetSettleTime(unsigned int) {}

	// Data
	CSingleton<CDigitizer> m_digitizer;
	bool m_first;
	CDownConverter::SReceiverGainModes m_gainModes;
	mutable CMutex m_mutex;
};


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
template<size_t N> C51412010::C51412010(const CDownConverter::EGainMode (&gainModes)[N])
:
	CDownConverter(),
	m_gainModes(&gainModes[0], &gainModes[_countof(BAND_SETTINGS_)])
{
	static_assert(N >= _countof(BAND_SETTINGS_), "Not enough values in gainModes array");
	Tune(0);

	return;
}


// Use selectany to put this here rather than in the .cpp so the size of the array can be determined by users of the class
__declspec(selectany) const CDownConverter::SBandSettings C51412010::BAND_SETTINGS_[] =
{ // low 20 dB, low 1 dB, high 1 dB, high 20 dB, data
	{   20000000,   20000000, 2700000000, 2700000000, 0x00 },
	{ 2300000000, 2700000000, 3200000000, 3700000000, 0x09 },
	{ 2800000000, 3200000000, 4000000000, 4500000000, 0x12 },
	{ 3300000000, 4000000000, 4800000000, 5300000000, 0x1b },
	{ 3900000000, 4800000000, 5600000000, 6200000000, 0x24 },
	{ 4900000000, 5600000000, 6400000000, 7000000000, 0x2d },
	{ 5500000000, 6400000000, 7200000000, 8100000000, 0x36 },
	{ 6000000000, 7200000000, 8000000000, 8800000000, 0x3f }
};
