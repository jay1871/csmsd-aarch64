/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2010-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "StdAfx.h"

#include <memory>

#include "26202001.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
const CDownConverter::BandSettings C26202001::BAND_SETTINGS(BAND_SETTINGS_, &BAND_SETTINGS_[_countof(BAND_SETTINGS_)]);
LPCTSTR C26202001::MUTEX_NAME = _T("Global\\TCI.{195F1597-7A7B-4F6C-947C-F9C20B2D92C5}");


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C26202001::C26202001(void)
:
	CVme(),
	m_id(0),
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get())
{
	m_first = !(GetLastError() == ERROR_ALREADY_EXISTS); // Status from m_mutex construction
	SCardStatus status = GetCardStatus(true);
	m_present = (status.cardClass == (CARD_CLASS & 0x3) && status.cardType == CARD_TYPE && status.id == 0);

	if(!m_present)
	{
		m_id = 15;
		status = GetCardStatus(true);
		m_present = (status.cardClass == (CARD_CLASS & 0x3) && status.cardType == CARD_TYPE + 1 && status.id == 7);
	}

	if(m_present)
	{
		if(m_first)
		{
			SetAtten(MAX_ATTEN);
			bool bypass;
			Units::FreqPair freqLimits;
			size_t band;
			Tune(30000000, 0, OPTIMUM, bypass, freqLimits, band);
			SetInput(TERM);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the bypass state
//
bool C26202001::GetBypass(void) const
{
	return (Peek(CONTROL_REG) & 0x0040) != 0;
}


//////////////////////////////////////////////////////////////////////
//
// Get the card status
//
C26202001::SCardStatus C26202001::GetCardStatus(bool force) const
{
	unsigned short reg = 0;

	if(force || m_present)
	{
		reg = Peek(STATUS_REG);
	}

	return *reinterpret_cast<SCardStatus*>(&reg);
}


//////////////////////////////////////////////////////////////////////
//
// Get the receiver gain
//
float C26202001::GetGainRatio(void) const
{
	return pow(10.0f, (MAX_GAIN - m_atten) / 10.0f);
}


//////////////////////////////////////////////////////////////////////
//
// Get the board version
//
unsigned char C26202001::GetVersion(void) const
{
	return GetCardStatus().version;
}


//////////////////////////////////////////////////////////////////////
//
// Set the attenuation
//
unsigned char C26202001::SetAtten(unsigned char atten)
{
	static const unsigned short ATTEN_DATA[] = { 0x0000, 0x0010, 0x0020, 0x0030 };

	if(atten / ATTEN_STEP >= _countof(ATTEN_DATA))
	{
		atten = ATTEN_STEP * _countof(ATTEN_DATA) - ATTEN_STEP;
	}

	unsigned short data = ATTEN_DATA[atten / ATTEN_STEP];

	if(m_present)
	{
		unsigned short reg = Peek(CONTROL_REG);

		if((reg & 0x0030) != data)
		{
			Poke(CONTROL_REG, (reg & 0xffcf) | data);
			SetSettleTime(20);
		}

		m_atten = atten / ATTEN_STEP * ATTEN_STEP;
	}

	return m_atten;
}


//////////////////////////////////////////////////////////////////////
//
// Set the RF band (bypass is band 0, off is band 0xff)
//
void C26202001::SetBand(unsigned char band)
{
	unsigned short data = 0x0c00; // Off

	if(band != 0xff)
	{
		ASSERT(band < _countof(BAND_SETTINGS_));

		if(band < _countof(BAND_SETTINGS_))
		{
			data = BAND_SETTINGS_[band].data;
		}
	}

	if(m_present)
	{
		unsigned short reg = Peek(CONTROL_REG);

		if((reg & 0x0f40) != data)
		{
			Poke(CONTROL_REG, (reg & 0xf0bf) | data);
			SetSettleTime((reg & 0x0040) == 0x0040 ? 20000 : 20);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Put in bypass mode
//
void C26202001::SetBypass(bool on)
{
	if(m_present)
	{
		unsigned short reg = Peek(CONTROL_REG);
		unsigned short data = (on ? 0x0040 : 0x0000);

		if((reg & 0x0040) != data)
		{
			Poke(CONTROL_REG, (reg & 0xffbf) | data);
			SetSettleTime(on ? 10000 : 1000);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the input source
//
void C26202001::SetInput(EInput input, signed char chOrBoth)
{
	if(m_present)
	{
		unsigned short reg = Peek(CONTROL_REG);
		unsigned short data;

		switch(chOrBoth)
		{
		case 0:
			data = (unsigned short(input) << 2) | (reg & 0x0003);
			break;

		case 1:
			ASSERT(m_id == 0);
			data = (reg & 0x000c) | unsigned short(input);
			break;

		case -1:
			data = (unsigned short(input) << 2) | unsigned short(input);
			break;

		default:
			ASSERT(FALSE);
			data = 0x000a;
			break;
		}

		if((reg & 0x000f) != data)
		{
			Poke(CONTROL_REG, (reg & 0xfff0) | data);
			SetSettleTime(((reg & 0x0003) == 0x0002 && (data & 0x0003) != 0x0002) ||
				((reg & 0x0003) != 0x0002 && (data & 0x0003) == 0x0002) ||
				((reg & 0x000c) == 0x0008 && (data & 0x000c) != 0x0008) ||
				((reg & 0x000c) != 0x0008 && (data & 0x000c) == 0x0008) ? 10000 : 20);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the signal routing
//
void C26202001::Tune(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ bool& bypass,
					 _Out_ Units::FreqPair& freqLimits, _Out_ size_t& band)
{
	ASSERT(freq <= RF_HIGH_FREQ_HZ + rxBw / 2);
	band = GetBand(freq, rxBw, bandSelect, freqLimits, BAND_SETTINGS);
	unsigned short data = BAND_SETTINGS_[band].data;
	bypass = ((data & 0x0040) != 0);
	SetBand(unsigned char(band));

	return;
}
