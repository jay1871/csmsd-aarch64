/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "StdAfx.h"

//@note: CPowerSupply is not available for XP built.
#ifndef _USING_V110_SDK71_

#include <limits>
#include <vector>

#include "DelayLoad.h"
#include "PowerSupply.h"
#include "Service.h"
#include "SLABCP2112.h"
#include "SlimRWLock.h"

const float CPowerSupply::DEPLETED_VOLTAGE = 13.7f;
const float CPowerSupply::LOW_VOLTAGE = 14.0f;
const float CPowerSupply::MINIMUM_VOLTAGE = 11.0f;
const float CPowerSupply::ZERO_VOLTAGE = 7.0f;
LPCSTR CPowerSupply::SLABSMBUSDLL = "SLABHIDtoSMBus.dll";

BEGIN_MESSAGE_MAP(CPowerSupply::CDeviceChangeWindow, CWnd)
	ON_WM_DEVICECHANGE()
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CPowerSupply::CPowerSupply(void)
:
	m_deviceChangeThread(nullptr),
	m_devNotify(nullptr),
	m_hidSmbusDevice(nullptr),
	m_isPresent(false),
	m_powerOffInProgress(false),
	m_timer(nullptr)
{
	try
	{
		// See if DLLs are present
		if(!CDelayLoad::LoadDelayLoadImports(SLABSMBUSDLL))
		{
			throw HID_SMBUS_DEVICE_NOT_FOUND;
		}

		// Register for device removal and arrival
		if(CService::IsDebug())
		{
			// Create and register window
			m_deviceChangeThread = AfxBeginThread(DeviceChangeThread, this);
		}
		else
		{
			// Register service
			DEV_BROADCAST_DEVICEINTERFACE notificationFilter = {};
			notificationFilter.dbcc_size = sizeof(notificationFilter);
			notificationFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
			m_devNotify = RegisterDeviceNotification(CService::GetServiceStatusHandle(), &notificationFilter, DEVICE_NOTIFY_SERVICE_HANDLE | DEVICE_NOTIFY_ALL_INTERFACE_CLASSES);
		}

		TryOpen();
	}
	catch(HID_SMBUS_STATUS error)
	{
		UNREFERENCED_PARAMETER(error);
		TRACE(_T("CPowerSupply::CPowerSupply error %d\n"), error);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CPowerSupply::~CPowerSupply(void)
{
	SetLed(YELLOW);
	Close();

	if(m_deviceChangeThread)
	{
		PostThreadMessage(m_deviceChangeThread->m_nThreadID, WM_QUIT, 0, 0);
	}

	if(m_devNotify)
	{
		UnregisterDeviceNotification(m_devNotify);
	}

	return;
}

//////////////////////////////////////////////////////////////////////
/// <summary>
/// This routine checks the battery power and return the appropriate code.
/// </summary>
/// <param name="batterySource">
/// On Exit will have the different power source status.
/// </param>
/// <param name="runTime">
/// On Exit will have the estimated battery run-time in minutes.
/// </param>
/// <remarks>
/// It is assumed that caller has called CPowerSupply::IsPresent()
///  and got affirmation before calling this method.
/// </remarks>
CPowerSupply::EPowerStatus CPowerSupply::ChkPower(_Out_ EPowerSource & batterySource,
						_Out_ unsigned short & runTime)
{
	ASSERT(IsPresent());

	//static variables.
	static size_t  s_criticalBatteryCount = 0;
	static bool s_lowBattery = false;
	static EPowerSource s_powerSource = INADEQUATE;

	EPowerStatus powerStatus = C_POWER_STATUS_NO_CHANGE;
	batterySource = SOURCE_SAME;
	runTime = USHRT_MAX;

	if( IsPowerOffInProgress() )
	{
		// Shut down now
		CPowerSupply::SetLed(CPowerSupply::YELLOW);
		powerStatus = C_POWER_OFF;
	}
	else
	{
		auto externalVoltages = GetExternalVoltages();
		auto internalBatteryStatus = GetInternalBatteryStatus();
		auto currentPowerSource = GetPowerSource(internalBatteryStatus, externalVoltages);
		auto currentCriticalBattery = false;
		auto currentLowBattery = false;

		for( auto i = 0u; i < internalBatteryStatus.size(); ++i )
		{
			if( internalBatteryStatus[i].valid )
			{
				if( internalBatteryStatus[i].timeToEmpty <= CPowerSupply::CRITICAL_RUN_TIME &&
					(currentPowerSource == CPowerSupply::INTERNAL_BATTERY || currentPowerSource == CPowerSupply::INTERNAL_BATT_AND_AUX) )
				{
					currentCriticalBattery = true;

					if( ++s_criticalBatteryCount == 4 )
					{
						// Critical power level - shut down
						powerStatus = C_INTERNAL_CRITICAL;
						break;
					}
				}

				runTime = std::min(runTime, internalBatteryStatus[i].timeToEmpty);
			}
		}

		if( currentPowerSource == CPowerSupply::EXTERNAL_BATTERY || currentPowerSource == CPowerSupply::EXTERNAL_BATT_AND_AUX )
		{
			if( (externalVoltages[0] < CPowerSupply::DEPLETED_VOLTAGE || externalVoltages[0] < CPowerSupply::DEPLETED_VOLTAGE) )
			{
				if( !currentCriticalBattery && ++s_criticalBatteryCount == 4 )
				{
					// Critical power level - shut down
					powerStatus = C_EXTERNAL_CRITICAL;
				}

				currentCriticalBattery = true;
			}
		}

		if( !currentCriticalBattery )
		{
			s_criticalBatteryCount = 0;

			for( auto i = 0u; i < internalBatteryStatus.size(); ++i )
			{
				if( internalBatteryStatus[i].valid )
				{
					if( internalBatteryStatus[i].timeToEmpty <= CPowerSupply::LOW_RUN_TIME &&
						(currentPowerSource == CPowerSupply::INTERNAL_BATTERY || currentPowerSource == CPowerSupply::INTERNAL_BATT_AND_AUX) )
					{
						// Low power level
						if( !s_lowBattery )
						{
							powerStatus = C_INTERNAL_LOW;
							s_lowBattery = true;
						}

						currentLowBattery = true;
						break;
					}
				}
			}

			if( currentPowerSource == CPowerSupply::EXTERNAL_BATTERY || currentPowerSource == CPowerSupply::EXTERNAL_BATT_AND_AUX )
			{
				if( externalVoltages[0] < CPowerSupply::LOW_VOLTAGE || externalVoltages[0] < CPowerSupply::LOW_VOLTAGE )
				{
					// Low power level
					if( !s_lowBattery )
					{
						powerStatus = C_EXTERNAL_LOW;
						s_lowBattery = true;
					}

					currentLowBattery = true;
				}
			}

			if( s_lowBattery && !currentLowBattery )
			{
				powerStatus = C_RESTORED;
				s_lowBattery = false;
			}

			if( currentPowerSource != s_powerSource )
			{
				s_powerSource = batterySource = currentPowerSource;
			}

		}
	}

	return powerStatus;

}

//////////////////////////////////////////////////////////////////////
//
// Close the device
//
void CPowerSupply::Close(void)
{
	if(m_timer)
	{
		// Close the timer
		SetThreadpoolTimer(m_timer, nullptr, 0, 0);
		WaitForThreadpoolTimerCallbacks(m_timer, TRUE);
		CloseThreadpoolTimer(m_timer);
		m_timer = nullptr;
	}

	if(m_hidSmbusDevice)
	{
		HidSmbus_Close(m_hidSmbusDevice);
		m_hidSmbusDevice = nullptr;
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Thread to manage a window to receive device change notifications when running as a console app
//
UINT AFX_CDECL CPowerSupply::DeviceChangeThread(_In_ void* arg)
{
	// Create window
	CDeviceChangeWindow deviceChangeWindow(static_cast<CPowerSupply*>(arg));
	deviceChangeWindow.m_hWnd = nullptr;

	if(!deviceChangeWindow.CreateEx(0, AfxRegisterWndClass(0), _T("Device Change Notification Sink"), WS_OVERLAPPED, 0, 0, 0, 0, nullptr, nullptr))
	{
		TRACE(_T("Unable to create device change window\n"));
		return 0;
	}

	ASSERT(deviceChangeWindow.m_hWnd != nullptr && CWnd::FromHandlePermanent(deviceChangeWindow.m_hWnd) == &deviceChangeWindow);

	// Register for notifications
	DEV_BROADCAST_DEVICEINTERFACE notificationFilter = {};
	notificationFilter.dbcc_size = sizeof(notificationFilter);
	notificationFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
#pragma warning(suppress : 6387) // deviceChangeWindow.m_hWnd cannot be nullptr
	auto devNotify = RegisterDeviceNotification(deviceChangeWindow.m_hWnd, &notificationFilter, DEVICE_NOTIFY_WINDOW_HANDLE | DEVICE_NOTIFY_ALL_INTERFACE_CLASSES);

	// Message pump
	MSG msg;
	PeekMessage(&msg, 0, WM_USER, WM_USER, PM_NOREMOVE);

	while(GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	UnregisterDeviceNotification(devNotify);
	deviceChangeWindow.DestroyWindow();

	return 0;
}


//////////////////////////////////////////////////////////////////////
//
// Get external voltages
//
std::array<float, 3> CPowerSupply::GetExternalVoltages(void) const
{
	std::array<float, 3> voltages =
		{ std::numeric_limits<float>::quiet_NaN(), std::numeric_limits<float>::quiet_NaN(), std::numeric_limits<float>::quiet_NaN() };

	try
	{
		for(BYTE i = 0; i < 3; ++i)
		{
			CLockGuard lock(m_critSect);
			voltages[i] = 21.79f * ReadAdc(i, 1) / 0x8000;
		}
	}
	catch(HID_SMBUS_STATUS error)
	{
		UNREFERENCED_PARAMETER(error);
		TRACE(_T("CPowerSupply::GetExternalVoltages error %d\n"), error);
	}

	return voltages;
}


//////////////////////////////////////////////////////////////////////
//
// Get battery status
//
std::vector<CPowerSupply::SInternalBatteryStatus> CPowerSupply::GetInternalBatteryStatus(void) const
{
	std::vector<SInternalBatteryStatus> batteryStatus;

	if(m_isPresent)
	{
		// Loop over all batteries
		for(BYTE battery = 0; battery < NUM_INTERNAL_BATTERIES; ++battery)
		{
			SInternalBatteryStatus status = {};

			try
			{
				CLockGuard lock(m_critSect);
				SelectMux(battery);
				status.timeToEmpty = ReadSMBus<unsigned short>(BATTERY_ADDRESS, 0x12);
				status.voltage = ReadSMBus<unsigned short>(BATTERY_ADDRESS, 0x09);
				status.current = ReadSMBus<unsigned short>(BATTERY_ADDRESS, 0x0a);
				short temp = (ReadSMBus<short>(BATTERY_ADDRESS, 0x08) - 2732) / 10;
				if (temp < -128 || temp > 127) TRACE(_T("CPowerSupply::GetInternalBatteryStatus temp = %hd\n"), temp);
				status.temperature = signed char(temp & 0xff); // deciKelvin to C
				unsigned short charge = ReadSMBus<unsigned short>(BATTERY_ADDRESS, 0x0d);
				if (charge > 255) TRACE(_T("CPowerSupply::GetInternalBatteryStatus charge = %hu\n"), charge);
				status.chargeLevel = unsigned char(charge & 0xff);
				auto statusFlags = ReadSMBus<unsigned short>(BATTERY_ADDRESS, 0x16);
				status.charged = ((statusFlags & 0x0020) != 0);
				status.discharged = ((statusFlags & 0x0010) != 0);
				status.needsConditioning = ((ReadSMBus<unsigned short>(BATTERY_ADDRESS, 0x03) & 0x0080) != 0);
				status.needsInitializing = !((statusFlags & 0x0080) != 0);
				status.valid = true;
			}
			catch(HID_SMBUS_STATUS error)
			{
				UNREFERENCED_PARAMETER(error);
				TRACE(_T("CPowerSupply::GetInternalBatteryStatus error %d\n"), error);
			}

			batteryStatus.push_back(status);
		}
	}
	
	return batteryStatus;
}


//////////////////////////////////////////////////////////////////////
//
// Figure out power source
//
CPowerSupply::EPowerSource CPowerSupply::GetPowerSource(_In_ const std::vector<SInternalBatteryStatus>& internalBatteryStatus,
		_In_ const std::array<float, 3> externalVoltages)
{
	for(auto i = 0u; i < internalBatteryStatus.size(); ++i)
	{
		if(internalBatteryStatus[i].valid)
		{
			if(internalBatteryStatus[i].current < -50)
			{
				return externalVoltages[2] < MINIMUM_VOLTAGE ? INTERNAL_BATTERY : INTERNAL_BATT_AND_AUX;
			}
		}
	}

	if(_isnan(externalVoltages[0]) || _isnan(externalVoltages[1]) || _isnan(externalVoltages[2]))
	{
		return UNKNOWN;
	}

	if(externalVoltages[2] > MINIMUM_VOLTAGE &&
		(externalVoltages[2] > externalVoltages[0] || externalVoltages[3] > externalVoltages[1]))
	{
		return EXTERNAL_AUX;
	}
	else if(externalVoltages[0] > MINIMUM_VOLTAGE || externalVoltages[1] > MINIMUM_VOLTAGE)
	{
		return externalVoltages[2] < MINIMUM_VOLTAGE ? EXTERNAL_BATTERY : EXTERNAL_BATT_AND_AUX;
	}
	else
	{
		return INADEQUATE;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get system temperature
//
std::pair<float, CPowerSupply::ETemperature> CPowerSupply::GetTemperatures(void) const
{
	auto temps = std::pair<float, ETemperature>{std::numeric_limits<float>::quiet_NaN(), INVALID};

	if(m_isPresent)
	{
		try
		{
			CLockGuard lock(m_critSect);
			BYTE gpio;
			Check(HidSmbus_ReadLatch(m_hidSmbusDevice, &gpio));
			temps.second = static_cast<ETemperature>((gpio & 0x18) >> 3);
			temps.first = 204.8f * ReadAdc(3, 2) / 0x8000 - 50;
		}
		catch(HID_SMBUS_STATUS error)
		{
			UNREFERENCED_PARAMETER(error);
			TRACE(_T("CPowerSupply::GetTemperatures error %d\n"), error);
		}
	}

	return temps;
}


//////////////////////////////////////////////////////////////////////
//
// Check for powering off state
//
bool CPowerSupply::IsPowerOffInProgress(void)
{
	bool powerOff = false;

	if(m_isPresent)
	{
		try
		{
			CLockGuard lock(m_critSect);
			BYTE gpio;
			Check(HidSmbus_ReadLatch(m_hidSmbusDevice, &gpio));
			powerOff = ((gpio & 0x01) == 0);

			if(powerOff)
			{
				// Clear my request if set
				Check(HidSmbus_WriteLatch(m_hidSmbusDevice, 0x80, 0x80));
				m_powerOffInProgress = true;
			}
		}
		catch(HID_SMBUS_STATUS error)
		{
			UNREFERENCED_PARAMETER(error);
			TRACE(_T("CPowerSupply::IsPowerOffInProgress error %d\n"), error);
		}
	}

	return powerOff;
}


//////////////////////////////////////////////////////////////////////
//
// Handle device removal or arrival notifications
//
void CPowerSupply::OnDeviceEvent(DWORD dwEventType, _In_ const DEV_BROADCAST_HDR* devBroadcastHdr)
{
	if(devBroadcastHdr->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE)
	{
		auto devBroadcastDevInterface = reinterpret_cast<const DEV_BROADCAST_DEVICEINTERFACE*>(devBroadcastHdr);

		try
		{
			CLockGuard lock(m_critSect);
			GUID hidGuid;
			Check(HidSmbus_GetHidGuid(&hidGuid));

			if(devBroadcastDevInterface->dbcc_classguid == hidGuid)
			{
				if(dwEventType == DBT_DEVICEREMOVECOMPLETE && m_hidSmbusDevice)
				{
					// Check to see if our device was removed by comparing device paths
					HID_SMBUS_DEVICE_STR devPath;
					Check(HidSmbus_GetOpenedString(m_hidSmbusDevice, devPath, HID_SMBUS_GET_PATH_STR));

					if(CString(devBroadcastDevInterface->dbcc_name).CompareNoCase(CString(devPath)) == 0)
					{
						lock.Unlock();
						Close();
					}
				}
				else if(dwEventType == DBT_DEVICEARRIVAL)
				{
					TryOpen();
				}
			}
		}
		catch(HID_SMBUS_STATUS error)
		{
			UNREFERENCED_PARAMETER(error);
			TRACE(_T("CPowerSupply::OnDeviceEvent error %d\n"), error);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Timer callback
//
void CPowerSupply::OnTimer(void) const
{
	for(BYTE battery = 0; battery < NUM_INTERNAL_BATTERIES; ++battery)
	{
		try
		{
			CLockGuard lock(m_critSect);
			SelectMux(battery);
			WriteSMBus<unsigned short>(BATTERY_ADDRESS, 0x03, 0xe000); // Disable battery alarms and charger broadcasts and use mWh
		}
		catch(HID_SMBUS_STATUS error)
		{
			UNREFERENCED_PARAMETER(error);
			TRACE(_T("CPowerSupply::OnTimer error %d\n"), error);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Read the A/D (see ADS1015 data sheet)
//
short CPowerSupply::ReadAdc(BYTE input, BYTE pga) const
{
	ASSERT(input < 4 && pga < 8);
	SelectMux(6);
	WriteSMBus<unsigned short>(ADC_ADDRESS, 0x01, _byteswap_ushort(0xc183 | (input << 12) | (pga << 9)));
	while((_byteswap_ushort(ReadSMBus<unsigned short>(ADC_ADDRESS, 0x01)) & 0x8000) == 0x0000);

	return _byteswap_ushort(ReadSMBus<unsigned short>(ADC_ADDRESS, 0x00));
}


//////////////////////////////////////////////////////////////////////
//
// Select a mux input
//
void CPowerSupply::SelectMux(BYTE input) const
{
	WriteSMBus(MUX_ADDRESS, std::vector<BYTE>(1, input | 0x08));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Turn the system LED on or off
//
void CPowerSupply::SetLed(ELed color)
{
	if(m_isPresent)
	{
		try
		{
			CLockGuard lock(m_critSect);
			Check(HidSmbus_WriteLatch(m_hidSmbusDevice, BYTE(color << 5), 0x60));
		}
		catch(HID_SMBUS_STATUS error)
		{
			UNREFERENCED_PARAMETER(error);
			TRACE(_T("CPowerSupply::SetLED error %d\n"), error);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Try to open the device
//
void CPowerSupply::TryOpen(void)
{
	// See if DLLs are present
	if(!CDelayLoad::LoadDelayLoadImports(SLABSMBUSDLL))
	{
		throw HID_SMBUS_DEVICE_NOT_FOUND;
	}

	// Only support one CP2112 device
	DWORD numDevices;
	Check(HidSmbus_GetNumDevices(&numDevices, VID, PID));

	if(numDevices > 1)
	{
		throw HID_SMBUS_DEVICE_NOT_SUPPORTED;
	}

	// Configure the CP2112
	Check(HidSmbus_Open(&m_hidSmbusDevice, 0, VID, PID));
	Check(HidSmbus_SetSmbusConfig(m_hidSmbusDevice, 50000, 0x02, FALSE, 100, 100, TRUE, 1));
	Check(HidSmbus_SetGpioConfig(m_hidSmbusDevice, 0xe0, 0xe0, 0x00, 0));

	// Set up to turn off the battery alarms every 45 seconds (they auto-reenable themselves)
	m_timer = CreateThreadpoolTimer(OnTimer, this, nullptr);
	static const FILETIME ZERO = {};
	SetThreadpoolTimer(m_timer, const_cast<PFILETIME>(&ZERO), 45000, 0);
	m_isPresent = true;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Turn off the system power
//
void CPowerSupply::TurnOffPower(void)
{
	if(m_isPresent && !m_powerOffInProgress)
	{
		try
		{
			CLockGuard lock(m_critSect);
			Check(HidSmbus_WriteLatch(m_hidSmbusDevice, 0x00, 0x80));
		}
		catch(HID_SMBUS_STATUS error)
		{
			UNREFERENCED_PARAMETER(error);
			TRACE(_T("CPowerSupply::TurnOffPower error %d\n"), error);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Write to an SMBus device
//
void CPowerSupply::WriteSMBus(BYTE address, _In_ const std::vector<BYTE>& data) const
{
	ASSERT(data.size() <= 61);
	Check(HidSmbus_WriteRequest(m_hidSmbusDevice, address, const_cast<BYTE*>(&data[0]), BYTE(data.size())));
	HID_SMBUS_S0 status = HID_SMBUS_S0_BUSY;
	HID_SMBUS_S1 detailedStatus;
	WORD numRetries;
	WORD bytesRead;

	for(int attempt = 0; attempt < 10 && status != HID_SMBUS_S0_COMPLETE; ++attempt)
	{
		Check(HidSmbus_TransferStatusRequest(m_hidSmbusDevice));
		Check(HidSmbus_GetTransferStatusResponse(m_hidSmbusDevice, &status, &detailedStatus, &numRetries, &bytesRead));
	}

	if(status != HID_SMBUS_S0_COMPLETE)
	{
		throw HID_SMBUS_WRITE_ERROR;
	}

	return;
}

#endif  //End of #ifndef _USING_V110_SDK71_