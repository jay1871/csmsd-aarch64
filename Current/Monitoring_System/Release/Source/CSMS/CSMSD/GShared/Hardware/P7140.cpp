/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "StdAfx.h"

#include <array>
#pragma warning(push)
#pragma warning(disable : 6011) // STL bug
#include <deque>
#pragma warning(pop)
#include <gc4016.h>
#include <intrin.h>
#include <ipp.h>
#include <limits>
#include <math.h>

// Fix Pentek bug
#undef GC4016_SCALE_MAX
#define GC4016_SCALE_MAX 5

#ifdef PI
#undef PI // We have our own definition in Units.h
#endif

#include <mmsystem.h>
#include <ptkddr.h>
#include <ptkfilt.h>
#include <setupapi.h>
#include <vector>
#include <winioctl.h>

#include "Fft.h"
#include "Log.h"
#include "P7140.h"
#include "Service.h"
#include "Singleton.h"
#include "Timer.h"
#include "Units.h"
#include "Utility.h"
#include "WatchDog.h"
#include "WinUtility.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
//
// Static data
//
const float CP7140::ADC_FULL_SCALE_DBM = 5.6f;
const float CP7140::ADC_FULL_SCALE_WATTS = pow(10.0f, (CP7140::ADC_FULL_SCALE_DBM - 30.0f) / 10.0f);
bool CP7140::m_constructed = false;
LPCTSTR CP7140::MUTEX_NAME = _T("Global\\TCI.{3C7A41D8-9042-46d5-B58B-EC2413527DD2}");


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CP7140::CP7140(void)
:
	m_badClock(false),
	m_board(INVALID_HANDLE_VALUE),
	m_dataCritSect(),
	m_demodFreq(0),
	m_freq(0),
	m_invertSpectrum01(false),
	m_invertSpectrum2(false),
	m_needGainAdjust(true),
	m_needRetune(true),
	m_needSync(true),
	m_regCritSect(),
	m_sampleRate(0),
	m_shift(false),
	m_shutdown(FALSE, TRUE), // Manual reset
	m_tempSensor(INVALID_HANDLE_VALUE),
	m_thread(nullptr),
	m_timer(nullptr),
	m_voltSensor(INVALID_HANDLE_VALUE),
	m_watchDog(nullptr),
	m_watchDogSource(nullptr),
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get())
{
	// Must be a singleton
	ASSERT(!m_constructed);
	m_constructed = true;

	// Check result from m_mutex CreateMutex to see if we created the global mutex
	m_master = (GetLastError() == ERROR_SUCCESS);

	// Set critical section spin counts
	SetCriticalSectionSpinCount(m_dataCritSect, SPIN_COUNT);
	SetCriticalSectionSpinCount(m_regCritSect, SPIN_COUNT);

	try
	{
		m_callbacks[0] = m_callbacks[1] = m_callbacks[2] = nullptr;
		m_chan[0] = m_chan[1] = m_chan[2] = INVALID_HANDLE_VALUE;
		m_cfir[0] = m_cfir[1] = m_cfir[2] = GC4016_CFIR_80;
		m_decimation[0] = m_decimation[1] = m_decimation[2] = 0;
		m_demod[0] = m_demod[1] = m_demod[2] = false;
		m_firBw[0] = m_firBw[1] = m_firBw[2] = 0;
		m_gcDecimation[0] = m_gcDecimation[1] = m_gcDecimation[2] = 0;
		m_pfir[0] = m_pfir[1] = m_pfir[2] = GC4016_PFIR_80;
		m_wbDecimation[0] = m_wbDecimation[1] = m_wbDecimation[2] = 0;

		// Setup high precision timers
		TIMECAPS tc;

		if (timeGetDevCaps(&tc, sizeof(tc)) == MMSYSERR_NOERROR)
		{
			m_periodMin = tc.wPeriodMin;
			timeBeginPeriod(m_periodMin);
		}
		else
		{
			ASSERT(FALSE);
		}

		// Get the mutex
		CSingleLock lock(&m_mutex, TRUE);

		// Open board
		m_board = OpenDevice(_T("\\\\.\\p7140i1"), FILE_FLAG_OVERLAPPED);

		// Get driver info
		char buffer[32]; // Different driver versions return different sized DRIVER_INFO structures
		DWORD bR;
		VERIFY(Utility::DeviceIoControlSync(m_board, IOCTL_GETDRIVERINFO, nullptr, 0, buffer, sizeof(buffer), bR));
		ASSERT(bR <= sizeof(buffer));
		m_driverVersion = reinterpret_cast<DRIVER_INFO*>(buffer)->Version;

		if(m_master)
		{
			m_timer.reset(new CTimer(CTimer::LONG));

			// Reset the board
			PokePci(0x0070, 0x00008000);  // Board reset reg / bit
			PokePci(0x0070, 0x00000000);
			Sleep(1000);

			// See if we need to load the FPGA
			COleDateTime minDate;
			minDate.ParseDateTime(CService::Instance()->GetProfileString(_T("P7140"), _T("VirtexMinDate")),
				LOCALE_NOUSEROVERRIDE | VAR_DATEVALUEONLY);

			// Default minimum date 11 November 2006
			unsigned long minFpgaDate = 0x06111100;

			if(minDate.GetStatus() == COleDateTime::valid)
			{
				minFpgaDate = ((minDate.GetYear() % 100) << 24) | (minDate.GetMonth() << 16) | ((minDate.GetDay() % 100) << 8);
			}

			if(PeekFpga(0x8048) != 0x1421 || (((unsigned long(PeekFpga(0x8038)) << 16) | PeekFpga(0x8040)) & 0xffffff00) < minFpgaDate ||
				(PeekFpga(0x8038) == 0x0612 && PeekFpga(0x8040) == 0x1101))
			{
				// Wait to reduce chance of Windows needing a reboot when the driver is reloaded right after system boot
				Sleep(30000);
				LoadVirtex();

				// Reload
				CloseHandle(m_board);
				Sleep(1000);
				ReloadDriver();
				Sleep(1000);
				m_board = OpenDevice(_T("\\\\.\\p7140i1"), FILE_FLAG_OVERLAPPED);

				// Reset the board
				PokePci(0x0070, 0x00008000);  // Board reset reg / bit
				PokePci(0x0070, 0x00000000);
				Sleep(1000);

				if(PeekFpga(0x8048) != 0x1421 || (((PeekFpga(0x8038) << 16) | PeekFpga(0x8040)) & 0xffffff00) < minFpgaDate ||
					(PeekFpga(0x8038) == 0x0612 && PeekFpga(0x8040) == 0x1101))
				{
					AfxThrowFileException(CFileException::genericException, ERROR_INVALID_DATA, _T("FPGA Image"));
				}
			}
		}

		TRACE(_T("%s\n"), LPCTSTR(GetVersion()));
		m_tempSensor = OpenDevice(_T("\\\\.\\p7140i1\\t1"));
		m_voltSensor = OpenDevice(_T("\\\\.\\p7140i1\\t0"));

		if(m_master)
		{
			// Open the other devices
			m_chan[0] = OpenDevice(_T("\\\\.\\p7140i1\\dn\\0c"), FILE_FLAG_OVERLAPPED);
			m_chan[1] = OpenDevice(_T("\\\\.\\p7140i1\\dn\\1c"), FILE_FLAG_OVERLAPPED);
			m_chan[2] = OpenDevice(_T("\\\\.\\p7140i1\\dn\\2c"), FILE_FLAG_OVERLAPPED);

			// Modify 4016 output clock for wideband core
			Poke4016(31, 16, 0x80, 0x80); // Disable chan D
			Poke4016(98, 20, 0x00, 0xc0); // BLOCK_SIZE = 0
			Poke4016(98, 21, 0x00); // SCLK_RATE = 0

			// Setup bus and enable external clock
			PokeFpga(0x80b0, 0x0001); // Sync mask DDC = 1
			PokeFpga(0x8c00, 0x0000); // A/D sync bus select SEL = BUS A
			PokeFpga(0x8400, 0x0000); // D/A sync bus select SEL = BUS A
			PokeFpga(0x8080, 0x000f, 0x001f); // Master bus A control TERM = terminated MASTR = master OSC DIS = 1 SEL CLK = ext
			PokeFpga(0x8098, 0x000f, 0x001f); // Master bus B control TERM = terminated MASTR = master OSC DIS = 1 SEL CLK = ext
			PokeFpga(0x8c68, 0x0080, 0x0080); // DDC C FIFO control GATE SEL = gate B
			PokeFpga(0x8080, 0x000c, 0x001c);
			PokeFpga(0x8098, 0x000c, 0x001c);

			// Check clock
			m_timer->Delay(50000); // Per Pentek, wait 50 ms after changing clock input
			PokeFpga(0x8108, 0x0004); // System interrupt flag clear CLK A LOSS
			m_timer->Delay(1000);

			if(PeekFpga(0x8108) & 0x0004) // System interrupt flag CLK A LOSS
			{
				// Switch back to internal
				m_badClock = true;
				PokeFpga(0x8080, 0x0000, 0x001c); // Master bus A control TERM = terminated MASTR = master OSC DIS = 0 SEL CLK = osc
				m_timer->Delay(50000); // Per Pentek, wait 50 ms after changing clock input
				PokeFpga(0x8108, 0x0004); // System interrupt flag clear CLK A LOSS
			}

			// Set inputs
			int channel = 0;
			VERIFY(Utility::DeviceIoControlSync(m_chan[0], IOCTL_INCHANSET, &channel, sizeof(channel), nullptr, 0, bR));
			channel = 1;
			VERIFY(Utility::DeviceIoControlSync(m_chan[1], IOCTL_INCHANSET, &channel, sizeof(channel), nullptr, 0, bR));
			channel = 0;
			VERIFY(Utility::DeviceIoControlSync(m_chan[2], IOCTL_INCHANSET, &channel, sizeof(channel), nullptr, 0, bR));

			// Turn off gates
			SetGate(false, 0);
			SetGate(false, 1);

			// Setup buffers
			m_bufSize[0] = m_bufSize[1] = BUF_SIZE_UINTS;
			m_intSize[0] = m_intSize[1] = INT_SIZE_UINTS;
			m_bufSize[2] = DEMOD_BUF_SIZE_UINTS;
			m_intSize[2] = DEMOD_INT_SIZE_UINTS;
			BUFFER_CFG bc;

			for(unsigned char ch = 0; ch < NUM_CHANS; ++ch)
			{
				VERIFY(Utility::DeviceIoControlSync(m_chan[ch], IOCTL_BUFGET, nullptr, 0, &bc, sizeof(bc), bR));

				if(bc.bufsize != m_bufSize[ch] || bc.intbufsize != m_intSize[ch])
				{
					bc.bufno = 0;
					bc.bufsize = m_bufSize[ch];
					bc.intbufsize = m_intSize[ch];
					bc.physAddr = 0;

					if(!Utility::DeviceIoControlSync(m_chan[ch], IOCTL_BUFSET, &bc, sizeof(bc), nullptr, 0, bR))
					{
						AfxThrowFileException(CFileException::genericException, GetLastError(), _T("DMA buffer"));
					}
				}

				// Setup memory-mapped buffer access
				if(!Utility::DeviceIoControlSync(m_chan[ch], IOCTL_MMAP, nullptr, 0, &m_buffer[ch], sizeof(m_buffer[ch]), bR))
				{
					AfxThrowFileException(CFileException::genericException, GetLastError(), _T("DMA buffer"));
				}

				m_bufferPci[ch] = 0xffffffff;

				for(unsigned char desc = 0; desc < 4; ++desc)
				{
					unsigned int pci = PeekPci(0x1018 + ch * 0x1000 + desc * 0x0010);

					if(pci < m_bufferPci[ch])
					{
						m_bufferPci[ch] = pci;
					}
				}
			}

			Init();

			// Setup counters
			const unsigned short thresh[4] = { 0, 0, 0, 0 };
			SetThresholdCounters(thresh, thresh);

			// Adjust upper temperature limits
			std::vector<std::array<float, 2> > limits = GetTempLimits();
			ASSERT(limits.size() == 4);
			limits[0][1] = 85;
			limits[1][1] = 127; // Disable - it doesn't work
			limits[2][1] = 75;
			limits[3][1] = 75;
			SetTempLimits(limits);

			// Start monitoring thread
			m_shutdown.ResetEvent();
			Utility::SThreadWrapperArg<CP7140>* arg = new Utility::SThreadWrapperArg<CP7140>(this, &CP7140::Thread, &CP7140::OnFail, "CP7140::Thread");
			m_thread = AfxBeginThread(Utility::ThreadWrapper<CP7140>, arg, THREAD_PRIORITY_TIME_CRITICAL, 0, CREATE_SUSPENDED, nullptr);
			m_thread->m_bAutoDelete = FALSE;
			m_thread->ResumeThread();
		}
	}
	catch(CFileException* ex)
	{
		ex->Delete();
		m_board = INVALID_HANDLE_VALUE;
		m_buffer[0] = nullptr;
		m_buffer[1] = nullptr;
		m_buffer[2] = nullptr;
		m_newestSample[0] = nullptr;
		m_newestSample[1] = nullptr;
		m_newestSample[2] = nullptr;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CP7140::~CP7140(void)
{
	if(m_board != INVALID_HANDLE_VALUE)
	{
		if(m_thread != nullptr)
		{
			// Shutdown thread
			m_shutdown.SetEvent();

			if(WaitForSingleObject(m_thread->m_hThread, THREAD_TIMEOUT) == WAIT_TIMEOUT)
			{
				CLog::Log(SMS_SERVICE_FAILED, LOG_CATEGORY_SERVICE, _T("CP7140::Shutdown timed out"));
				_exit(WAIT_TIMEOUT);
			}

			delete m_thread;
			m_thread = nullptr;
		}

		if(m_master)
		{
			// Set clock to internal
			int clksrc = CLK_SRC_INTERN;
			DWORD bR;
			VERIFY(Utility::DeviceIoControlSync(m_chan[0], IOCTL_CLKSRCSET, &clksrc, sizeof(clksrc), nullptr, 0, bR));
			VERIFY(Utility::DeviceIoControlSync(m_chan[1], IOCTL_CLKSRCSET, &clksrc, sizeof(clksrc), nullptr, 0, bR));
			VERIFY(Utility::DeviceIoControlSync(m_chan[2], IOCTL_CLKSRCSET, &clksrc, sizeof(clksrc), nullptr, 0, bR));

			// Close devices
			CloseHandle(m_chan[2]);
			CloseHandle(m_chan[1]);
			CloseHandle(m_chan[0]);
			CloseHandle(m_board);
		}

		CloseHandle(m_voltSensor);
		CloseHandle(m_tempSensor);
		m_voltSensor = m_tempSensor = m_chan[2] = m_chan[1] = m_chan[0] = m_board = INVALID_HANDLE_VALUE;
	}

	timeEndPeriod(m_periodMin);

	if(m_watchDog != nullptr)
	{
		(*m_watchDog)->UnregisterPingSource(m_watchDogSource);
		delete m_watchDog;
	}

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Adjust decimation to closest legal value
//
void CP7140::AdjustDecimation(_Inout_ unsigned long& decimation, _In_ bool adjustDown) const
{
	if(decimation <= 4 * GC4016_DECIMATION_MIN)
	{
		// Wideband - must be 2, 4, 8, 16 or 32
		if(adjustDown)
		{
			if(decimation < 4)
			{
				decimation = 2;
			}
			else if(decimation < 8)
			{
				decimation = 4;
			}
			else if(decimation < 16)
			{
				decimation = 8;
			}
			else if(decimation < 32)
			{
				decimation = 16;
			}
			else
			{
				decimation = 32;
			}
		}
		else
		{
			if(decimation < 3)
			{
				decimation = 2;
			}
			else if(decimation < 6)
			{
				decimation = 4;
			}
			else if(decimation < 10)
			{
				decimation = 8;
			}
			else if(decimation < 20)
			{
				decimation = 16;
			}
			else
			{
				decimation = 32;
			}
		}
	}
	else if (decimation <= 4u * MaxGcDecimation())
	{
		if(adjustDown)
		{
			// GC4016 - multiple of 4
			decimation = decimation / 4 * 4;
		}
		else
		{
			// GC4016 - multiple of 4
			decimation = (decimation + 2) / 4 * 4;
		}
	}
	else
	{
		if(adjustDown)
		{
			// Both
			unsigned long wbDecimation = std::min(decimation / 2 / MaxGcDecimation(), 4ul * GC4016_DECIMATION_MIN);
			AdjustDecimation(wbDecimation, true);
			if(wbDecimation == 2)
			{
				// minimum for cascaded downconverters
				wbDecimation = 4;
			}
			unsigned long gcDecimation = decimation / wbDecimation;
			AdjustDecimation(gcDecimation, true);
			ASSERT(gcDecimation >= 32);  // don't think this can happen, but not sure
			decimation = gcDecimation * wbDecimation;
		}
		else
		{
			// Both
			unsigned long wbDecimation = std::min(decimation / 2 / MaxGcDecimation(), 4ul * GC4016_DECIMATION_MIN);
			AdjustDecimation(wbDecimation);
			if(wbDecimation == 2)
			{
				// minimum for cascaded downconverters
				wbDecimation = 4;
			}
			unsigned long gcDecimation = decimation / wbDecimation;
			AdjustDecimation(gcDecimation);
			ASSERT(gcDecimation >= 32);  // don't think this can happen, but not sure
			decimation = gcDecimation * wbDecimation;
		}
	}

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Helper function
//
unsigned long CP7140::CalcFreqWord(Units::Frequency freq) const
{
	const unsigned __int64 rawFreq = freq.GetRaw().internal;
	const unsigned __int64 rawSampleRate = m_sampleRate.GetRaw().internal;
	unsigned long index;

	// Avoid 64-bit overflow
	if(_BitScanReverse(&index, *(reinterpret_cast<const unsigned long*>(&rawFreq) + 1))) // Tests the HIGH word of rawFreq
	{
		return unsigned long(((rawFreq << (31 - index)) / rawSampleRate) << (index + 1));
	}
	else
	{
		return unsigned long((rawFreq << 32) / rawSampleRate);
	}
}


/////////////////////////////////////////////////////////////////////
//
// Collect threshold counts
//
void CP7140::CollectCounts(unsigned int count,
		_Out_writes_all_(4) unsigned int (&countA)[4],
		_Out_writes_all_(4) unsigned int (&countB)[4],
		_Out_writes_all_(4) unsigned short (&threshA)[4],
		_Out_writes_all_(4) unsigned short (&threshB)[4],
		_Out_ unsigned short& maxA, _Out_ unsigned short& maxB)
{
	if(m_board != INVALID_HANDLE_VALUE && m_master)
	{
		ResetThresholdCounters();

		// Program DDC A & B Trigger Length Registers
		PokeFpga(0x8c10, unsigned short(count & 0x0000ffff));
		PokeFpga(0x8c18, unsigned short(count >> 16));
		PokeFpga(0x8c40, unsigned short(count & 0x0000ffff));
		PokeFpga(0x8c48, unsigned short(count >> 16));

		// Set TRIG mode and toggle TRIG CLEAR in DDC A & B FIFO Control registers
		PokeFpga(0x8c08, 0x0014, 0x0014);
		PokeFpga(0x8c08, 0x0000, 0x0010);
		PokeFpga(0x8c38, 0x0014, 0x0014);
		PokeFpga(0x8c38, 0x0000, 0x0010);

		// Enable TRIGGER CAPTURE in DDC A FIFO Interrupt Mask Register
		PokeFpga(0x8c20, 0x0020, 0x0020);

		// Clear TRIGGER CAPTURE flag in Application Interrupt Flag register
		PokeFpga(0x8138, 0x0400);

		// Fire the trigger
		PokeFpga(0x8090, 0x0000);
		PokeFpga(0x8090, 0x0001);

		// Wait until it's done
		while(!(PeekFpga(0x8138) & 0x0400))
		{
			Sleep(1); // Hack
		}

		// Clear TRIG mode in DDC A & B FIFO Control registers
		PokeFpga(0x8c08, 0x0000, 0x0004);
		PokeFpga(0x8c38, 0x0000, 0x0004);
		GetThresholdCounters(countA, countB, threshA, threshB, maxA, maxB);
	}
	else
	{
		for(auto i = 0; i < 4; ++i)
		{
			countA[i] = 0;
			countB[i] = 0;
			threshA[i] = 0;
			threshB[i] = 0;
		}

		maxA = maxB = 0;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Collect samples
//
void CP7140::CollectSamples(unsigned int count)
{
	StartSampling(count, 0, nullptr, nullptr);
	WaitForSampling();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Mark samples as OK to overwrite
//
void CP7140::DoneWithSamples(void)
{
	CSingleLock lock(&m_dataCritSect, TRUE);
	m_oldestInUse.pop_front();
	m_moreBufferSpace.SetEvent();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Enable watchdog
//
void CP7140::EnableWatchDog(bool enable)
{
	// Set up watchdog
	CSingleLock lock(&m_dataCritSect, TRUE);

	if(enable)
	{
		m_watchDog = new CSingleton<CWatchDog>;
		m_watchDogSource = (*m_watchDog)->RegisterPingSource(_T("data collection thread"));
	}
	else
	{
		(*m_watchDog)->UnregisterPingSource(m_watchDogSource);
		m_watchDogSource = nullptr;
		delete m_watchDog;
		m_watchDog = nullptr;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Check for FIFO overrun
//
bool CP7140::FifoHasOverrun(unsigned char ch)
{
	if(PeekFpga(0x8148 + ch * 0x0008) & 0x0800)
	{
		PokeFpga(0x8148 + ch * 0x0008, 0x0800);
		TRACE(_T("FIFO %u has overrun (now 0x%04x)\n"), ch, PeekFpga(0x8148 + ch * 0x0008));
		return true;
	}
	else
	{
		return false;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Find samples in buffer (waiting if necessary)
//
_Ret_opt_count_(count) const Ipp16sc* CP7140::FindSamples(unsigned int sampleCount, unsigned int count, unsigned char ch, DWORD timeout)
{
	if(m_buffer[ch] == nullptr)
	{
		return nullptr;
	}

	ASSERT(int(count) < m_bufSize[ch]);
	SamplesAdded(ch);
	int offset; // Offset of start of found samples in buffer
	volatile const unsigned int* found;
	CSingleLock lock(&m_dataCritSect, TRUE);
	CSingleLock wait(&m_newSamples[ch]);

	// Loop until we find them or timeout
	while(true)
	{
		if(sampleCount > m_sampleCountOffset[ch])
		{
			offset = sampleCount - m_sampleCountOffset[ch];
		}
		else
		{
			offset = -int(m_sampleCountOffset[ch] - sampleCount);
		}

		if(offset < 0)
		{
			// Insert point has wrapped
			found = m_buffer[ch] + offset + m_bufSize[ch];

			if(offset < -m_bufSize[ch] || found < m_newestSample[ch])
			{
				TRACE(_T("Samples have been overwritten in buffer for channel %u\n"), ch);
				found = nullptr;
				break;
			}
			else if(found + count <= m_buffer[ch] + m_bufSize[ch])
			{
				// Found them
				break;
			}
			else if(found + count < m_newestSample[ch] + m_bufSize[ch])
			{
				// Found them but they wrap, so make a copy
				if(count > m_wrapBuffer[ch].size())
				{
					m_wrapBuffer[ch].resize(count);
				}

				memcpy_s(&m_wrapBuffer[ch][0], count * sizeof(m_wrapBuffer[ch][0]),
					const_cast<const unsigned int*>(found), (m_buffer[ch] + m_bufSize[ch] - found) * sizeof(m_wrapBuffer[ch][0]));
				memcpy_s(&m_wrapBuffer[ch][0] + (m_buffer[ch] + m_bufSize[ch] - found),
					(count - (m_buffer[ch] + m_bufSize[ch] - found)) * sizeof(m_wrapBuffer[0]),
					const_cast<const unsigned int*>(m_buffer[ch]), (found + count - m_buffer[ch] - m_bufSize[ch]) * sizeof(m_wrapBuffer[ch][0]));
				found = &m_wrapBuffer[ch][0];

				break;
			}
			else
			{
				if(timeout > 0)
				{
					// Need to wait for new samples
					m_newSamples[ch].ResetEvent();
					lock.Unlock();

					if(!wait.Lock(timeout))
					{
						TRACE(_T("Timed out waiting for %u samples from %u for channel %u\n"), count, sampleCount, ch);
						found = nullptr;
						break;
					}

					wait.Unlock();
					lock.Lock();
				}
				else
				{
					found = nullptr;
					break;
				}
			}
		}
		else
		{
			found = m_buffer[ch] + offset;

			if(found + count > m_newestSample[ch])
			{
				if(timeout > 0)
				{
					// Need to wait for new samples
					m_newSamples[ch].ResetEvent();
					lock.Unlock();

					if(!wait.Lock(timeout))
					{
						TRACE(_T("Timed out waiting for %u samples from %u for channel %u\n"), count, sampleCount, ch);
						found = nullptr;
						break;
					}

					wait.Unlock();
					lock.Lock();
				}
				else
				{
					found = nullptr;
					break;
				}
			}
			else
			{
				// Found them
				break;
			}
		}
	}

	return const_cast<const Ipp16sc*>(reinterpret_cast<const volatile Ipp16sc*>(found));
}


//////////////////////////////////////////////////////////////////////
//
// Get FPGA firmware date
//
COleDateTime CP7140::GetFpgaFwDate(void) const
{
	unsigned short reg1 = PeekFpga(0x8038);
	unsigned short reg2 = PeekFpga(0x8040);
	return COleDateTime(2000 + 10 * ((reg1 >> 12) & 0xf) + ((reg1 >> 8) & 0xf), 10 * ((reg1 >> 4) & 0xf) + (reg1 & 0xf),
		10 * ((reg2 >> 12) & 0xf) + ((reg2 >> 8) & 0xf), 0, 0, 0);
}


//////////////////////////////////////////////////////////////////////
//
// Get newest sample count in buffer
//
unsigned int CP7140::GetNewestSampleCount(unsigned char ch) const
{
	CSingleLock lock(&m_dataCritSect, TRUE);

	return m_sampleCountOffset[ch] + (m_newestSample[ch] - m_buffer[ch]);
}


//////////////////////////////////////////////////////////////////////
//
// Get PCI bus speed
//
unsigned char CP7140::GetPciBusSpeed(void) const
{
	return (PeekPci(0x00a0) & 0x00000001) ? 66 : 33;
}


//////////////////////////////////////////////////////////////////////
//
// Get PCI bus width
//
unsigned char CP7140::GetPciBusWidth(void) const
{
	return (PeekPci(0x1000) & 0x00000080) ? 64 : 32;
}


//////////////////////////////////////////////////////////////////////
//
// Get PCI DMA burst size
//
unsigned short CP7140::GetPciDmaBurstSize(void) const
{
	return unsigned short((PeekPci(0x1000) & 0xffff0000) >> 16);
}


//////////////////////////////////////////////////////////////////////
//
// Get PCI firmware date
//
COleDateTime CP7140::GetPciFwDate(void) const
{
	unsigned int reg = PeekPci(0x0078);
	return COleDateTime(2000 + 10 * ((reg >> 28) & 0xf) + ((reg >> 24) & 0xf), 10 * ((reg >> 20) & 0xf) + ((reg >> 16) & 0xf),
		10 * ((reg >> 12) & 0xf) + ((reg >> 8) & 0xf), 0, 0, 0);
}


//////////////////////////////////////////////////////////////////////
//
// Get sample counter values
//
void CP7140::GetSampleCount(_Out_writes_all_(NUM_DATA_CHANS) unsigned int(& count)[NUM_DATA_CHANS]) const
{
	const_cast<CP7140*>(this)->PokeFpga(0x8088, 0x0080, 0x0080);
	const_cast<CP7140*>(this)->PokeFpga(0x8088, 0x0000, 0x0080);
	count[0] = PeekFpga(0x8aa0) | (PeekFpga(0x8aa8) << 16);
	count[1] = PeekFpga(0x8ab0) | (PeekFpga(0x8ab8) << 16);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get board temperatures
//
std::vector<float> CP7140::GetTemperatures() const
{
	std::vector<float> temperatures(4, 0);

	if(m_board != INVALID_HANDLE_VALUE)
	{
		CSingleLock lock(&m_mutex, TRUE);
		double temperature[4];
		DWORD bR;

		if(::DeviceIoControl(m_tempSensor, IOCTL_GETETEMP1, nullptr, 0, &temperature[0], sizeof(temperature[0]), &bR, nullptr) &&
			bR == sizeof(temperature[0]) &&
			::DeviceIoControl(m_tempSensor, IOCTL_GETETEMP2, nullptr, 0, &temperature[1], sizeof(temperature[1]), &bR, nullptr) &&
			bR == sizeof(temperature[1]) &&
			::DeviceIoControl(m_tempSensor, IOCTL_GETETEMP3, nullptr, 0, &temperature[2], sizeof(temperature[2]), &bR, nullptr) &&
			bR == sizeof(temperature[2]) &&
			::DeviceIoControl(m_tempSensor, IOCTL_GETITEMP, nullptr, 0, &temperature[3], sizeof(temperature[3]), &bR, nullptr) &&
			bR == sizeof(temperature[3]))
		{
			// Fix Pentek negative temp problems
			for(size_t i = 0; i < 4; ++i)
			{
				if(temperature[i] > 2147483648.0)
				{
					temperature[i] -= 4294967296.0;
				}
				else if(temperature[i] > 128)
				{
					temperature[i] -= 256;
				}
				else if(temperature[i] <= -2147483648.0)
				{
					temperature[i] += 2147483647;
				}

				temperatures[i] = float(temperature[i]);
			}
		}
	}

	return temperatures;
}


//////////////////////////////////////////////////////////////////////
//
// Get board temperature limits
//
std::vector<std::array<float, 2> > CP7140::GetTempLimits(void) const
{
	std::vector<std::array<float, 2> > limits(4, std::array<float, 2>());

	if(m_board != INVALID_HANDLE_VALUE)
	{
		CSingleLock lock(&m_mutex, TRUE);
		LIMIT_CFG lim;
		DWORD bR;
#pragma warning(push)
#pragma warning(disable : 6001) // Pentek API needs input even when not used
		VERIFY(::DeviceIoControl(m_tempSensor, IOCTL_GETETEMP1LIM, &lim, sizeof(lim), &lim, sizeof(lim), &bR, nullptr));
		ASSERT(bR == sizeof(lim));
		limits[0][0] = lim.lowLimit;
		limits[0][1] = lim.highLimit;
		VERIFY(::DeviceIoControl(m_tempSensor, IOCTL_GETETEMP2LIM, &lim, sizeof(lim), &lim, sizeof(lim), &bR, nullptr));
		ASSERT(bR == sizeof(lim));
		limits[1][0] = lim.lowLimit;
		limits[1][1] = lim.highLimit;
		VERIFY(::DeviceIoControl(m_tempSensor, IOCTL_GETETEMP3LIM, &lim, sizeof(lim), &lim, sizeof(lim), &bR, nullptr));
		ASSERT(bR == sizeof(lim));
		limits[2][0] = lim.lowLimit;
		limits[2][1] = lim.highLimit;
		VERIFY(::DeviceIoControl(m_tempSensor, IOCTL_GETITEMPLIM, &lim, sizeof(lim), &lim, sizeof(lim), &bR, nullptr));
#pragma warning(pop)
		ASSERT(bR == sizeof(lim));
		limits[3][0] = lim.lowLimit;
		limits[3][1] = lim.highLimit;
	}

	return limits;
}


//////////////////////////////////////////////////////////////////////
//
// Get threshold counter values
//
void CP7140::GetThresholdCounters(_Out_writes_all_(4) unsigned int (&countA)[4],
								  _Out_writes_all_(4) unsigned int (&countB)[4],
								  _Out_writes_all_(4) unsigned short (&threshA)[4],
								  _Out_writes_all_(4) unsigned short (&threshB)[4],
								  _Out_ unsigned short& maxA, _Out_ unsigned short& maxB) const
{
	const_cast<CP7140*>(this)->PokeFpga(0x8088, 0x0004, 0x0004);

	for(unsigned char ctr = 0; ctr < 4; ++ctr)
	{
		countA[ctr] = PeekFpga(0x8990 + 16 * ctr) | (PeekFpga(0x8998 + 16 * ctr) << 16);
		countB[ctr] = PeekFpga(0x89d0 + 16 * ctr) | (PeekFpga(0x89d8 + 16 * ctr) << 16);
		threshA[ctr] = PeekFpga(0x8940 + 8 * ctr);
		threshB[ctr] = PeekFpga(0x8960 + 8 * ctr);
	}

	maxA = PeekFpga(0x8980);
	maxB = PeekFpga(0x8988);
	const_cast<CP7140*>(this)->PokeFpga(0x8088, 0x0000, 0x0004);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get version string
//
CString CP7140::GetVersion(void)
{
	if(m_board != INVALID_HANDLE_VALUE)
	{
		CString version;
		version.Format(_T("PCI7140 firmware %s rev %x, FPGA firmware %s rev %x, FPGA core option %04x, driver %s"),
			LPCTSTR(GetPciFwDate().Format(_T("%x"))), GetPciFwRev(), LPCTSTR(GetFpgaFwDate().Format(_T("%x"))),
			GetFpgaFwRev(), GetFpgaCore(), LPCTSTR(m_driverVersion));

		return version;
	}
	else
	{
		return _T("Board not responding");
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get board voltage limits
//
std::vector<std::array<float, 2> > CP7140::GetVoltageLimits(void) const
{
	std::vector<std::array<float, 2> > limits(5, std::array<float, 2>());

	if(m_board != INVALID_HANDLE_VALUE)
	{
		CSingleLock lock(&m_mutex, TRUE);
		LIMIT_CFG lim;
		DWORD bR;
#pragma warning(push)
#pragma warning(disable : 6001) // Pentek API needs input even when not used
		VERIFY(::DeviceIoControl(m_voltSensor, IOCTL_GETVCCP1LIM, &lim, sizeof(lim), &lim, sizeof(lim), &bR, nullptr));
		ASSERT(bR == sizeof(lim));
		limits[0][0] = lim.lowLimit;
		limits[0][1] = lim.highLimit;
		VERIFY(::DeviceIoControl(m_voltSensor, IOCTL_GETAIN2LIM, &lim, sizeof(lim), &lim, sizeof(lim), &bR, nullptr));
		ASSERT(bR == sizeof(lim));
		limits[1][0] = lim.lowLimit;
		limits[1][1] = lim.highLimit;
		VERIFY(::DeviceIoControl(m_voltSensor, IOCTL_GETVCCLIM, &lim, sizeof(lim), &lim, sizeof(lim), &bR, nullptr));
		ASSERT(bR == sizeof(lim));
		limits[2][0] = lim.lowLimit;
		limits[2][1] = lim.highLimit;
		VERIFY(::DeviceIoControl(m_voltSensor, IOCTL_GET5VLIM, &lim, sizeof(lim), &lim, sizeof(lim), &bR, nullptr));
		ASSERT(bR == sizeof(lim));
		limits[3][0] = lim.lowLimit;
		limits[3][1] = lim.highLimit;
		VERIFY(::DeviceIoControl(m_voltSensor, IOCTL_GET12VLIM, &lim, sizeof(lim), &lim, sizeof(lim), &bR, nullptr));
#pragma warning(pop)
		ASSERT(bR == sizeof(lim));
		limits[4][0] = lim.lowLimit;
		limits[4][1] = lim.highLimit;
	}

	return limits;
}


//////////////////////////////////////////////////////////////////////
//
// Get board voltages
//
std::vector<float> CP7140::GetVoltages(void) const
{
	std::vector<float> voltages(5, 0);

	if(m_board != INVALID_HANDLE_VALUE)
	{
		CSingleLock lock(&m_mutex, TRUE);
		double voltage[5];
		DWORD bR;

		if(::DeviceIoControl(m_voltSensor, IOCTL_GETVCCP1, nullptr, 0, &voltage[0], sizeof(voltage[0]), &bR, nullptr) &&
			bR == sizeof(voltage[0]) &&
			::DeviceIoControl(m_voltSensor, IOCTL_GETAIN2, nullptr, 0, &voltage[1], sizeof(voltage[1]), &bR, nullptr) &&
			bR == sizeof(voltage[1]) &&
			::DeviceIoControl(m_voltSensor, IOCTL_GETVCC, nullptr, 0, &voltage[2], sizeof(voltage[2]), &bR, nullptr) &&
			bR == sizeof(voltage[2]) &&
			::DeviceIoControl(m_voltSensor, IOCTL_GET5V, nullptr, 0, &voltage[3], sizeof(voltage[3]), &bR, nullptr) &&
			bR == sizeof(voltage[3]) &&
			::DeviceIoControl(m_voltSensor, IOCTL_GET12V, nullptr, 0, &voltage[4], sizeof(voltage[4]), &bR, nullptr) &&
			bR == sizeof(voltage[4]))
		{
			for(size_t i = 0; i < 5; ++i)
			{
				voltages[i] = float(voltage[i]);
			}
		}
	}

	return voltages;
}


//////////////////////////////////////////////////////////////////////
//
// Initialize the board
//
void CP7140::Init(void)
{
	bool bad = true;

	do
	{
		ResetSampling();

		// Set defaults and reset DDCs
		m_sampleRate = 0;
		m_cfir[0] = m_cfir[1] = m_cfir[2] = GC4016_CFIR_80;
		m_decimation[0] = m_decimation[1] = m_decimation[2] = 0;
		m_demod[0] = m_demod[1] = m_demod[2] = false;
		m_firBw[0] = m_firBw[1] = m_firBw[2] = 0;
		m_gcDecimation[0] = m_gcDecimation[1] = m_gcDecimation[2] = 0;
		m_pfir[0] = m_pfir[1] = m_pfir[2] = GC4016_PFIR_80;
		m_wbDecimation[0] = m_wbDecimation[1] = m_wbDecimation[2] = 0;
		m_invertSpectrum01 = false;
		m_invertSpectrum2 = false;
		m_shift = false;
		m_freq = 0;
		m_demodFreq = 0;
		PokeFpga(0x8e00, 0x0000); // mux 0
		PokeFpga(0x8e60, 0x0001); // mux 1
		PokeFpga(0x8ec0, 0x0002); // mux 2
		PokeFpga(0x8e20, 0x0001); // DDC 0
		PokeFpga(0x8e80, 0x0001); // DDC 1
		PokeFpga(0x8ee0, 0x0001); // DDC 2
		PokeFpga(0x8ee0, 0x0000);
		PokeFpga(0x8e80, 0x0000);
		PokeFpga(0x8e20, 0x0000);
		Sleep(10);
		PokeWb(0, 0x0000, 0x387a00db);
		PokeWb(1, 0x0000, 0x387a00db);
		PokeWb(2, 0x0000, 0x187a00db);
		PokeWb(0, 0x0002, 0x00000000);
		PokeWb(1, 0x0002, 0x00000000);
		PokeWb(2, 0x0002, 0x00000000);
		SetDemodMode(0xff);
		SetSampleRate(81920000);
		SetFirBw(80, 0);
		SetFirBw(80, 1);
		SetFirBw(80, 2);
		SetDecimation(16, 0);
		SetDecimation(16, 1);
		SetDecimation(16, 2);
		Tune(0, false, true);
		TuneDemod(0, false);

		// Check the wideband DDCs by looking for a test tone in the right bin - sometimes they don't come up right
		SetWbTestTone(true, 1280000, 0);
		SetWbTestTone(true, 1280000, 1);
		SetWbTestTone(true, 1280000, 2);
		Settle(true);
		SetGate(true, 0);
		SetGate(true, 1);
		const Ipp16sc* samples[NUM_CHANS];
		const unsigned int NUM_BINS = 512;

		// Wait until data has made it through the DMA
		for(unsigned char ch = 0; ch < NUM_CHANS; ++ch)
		{
			while((samples[ch] = FindSamples(0, NUM_BINS, ch, 0)) == nullptr)
			{
				_mm_pause();
			}
		}

		SetGate(false, 0);
		SetGate(false, 1);
		bad = false;

		for(unsigned char ch = 0; ch < NUM_CHANS; ++ch)
		{
#pragma warning(suppress : 6387) // samples[ch] cannot be nullptr (see "while" above)
			CFft<float> fft(NUM_BINS, samples[ch]);
			Ipp32fVec power;
			fft.GetPowerSpectrumWatts(power, NUM_BINS, 1);
			float max;
			int idx;
			ippsMaxIndx(power, max, idx);

			if(max < 0.5 || idx != 384)
			{
				TRACE(_T("DDC %u bad (max = %.3f, idx = %u) - resetting\n"), ch, max, idx);
				bad = true;
			}
		}
	}
	while(GetClockStatus() && bad);

	if(!bad)
	{
		TRACE(_T("DDCs OK\n"));
	}

	SetWbTestTone(false, 0, 0);
	SetWbTestTone(false, 0, 1);
	SetWbTestTone(false, 0, 2);
	SetDecimation(320, 0);
	SetDecimation(320, 1);
	SetDecimation(320, 2);
	Tune(8000000, true, true);
	TuneDemod(8000000, true);
	Settle(true);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Calculate an approximation to the inverse error function
//
float CP7140::InvErf(float x)
{
	static const float A = 8 * (Units::PI - 3) / (3 * Units::PI * (4 - Units::PI));
	static const float PI_A = 8 * (Units::PI - 3) / (3 * (4 - Units::PI));
	float temp1 = log(1 - x * x);
	float temp2 = (2 / PI_A + temp1 / 2);
	float invErf = sqrt(sqrt(temp2 * temp2 - temp1 / A) - temp2);

	return x < 0 ? -invErf : invErf;
}


//////////////////////////////////////////////////////////////////////
//
// Load the FPGA with default configuration or from an external file
//
void CP7140::LoadVirtex(bool upload)
{
	CString virtexFile;
	CStdioFile file;

	if(upload)
	{
		// Open file (Intel Hex format)
		if((virtexFile = CService::Instance()->GetProfileString(_T("P7140"), _T("VirtexFile"))).IsEmpty())
		{
			AfxThrowFileException(CFileException::genericException, ERROR_CANTREAD, _T("P7140\\VirtexFile"));
		}
		else
		{
			CFileException exception;

			if(!file.Open(virtexFile, CFile::modeRead | CFile::shareDenyNone, &exception))
			{
				AfxThrowFileException(exception.m_cause, exception.m_lOsError, exception.m_strFileName);
			}
		}
	}

	// Disable interrupts
	unsigned long intAState = PeekPci(0x0010);
	unsigned long intBState = PeekPci(0x0018);
	unsigned long intCState = PeekPci(0x0020);
	unsigned long intDState = PeekPci(0x0028);
	PokePci(0x0010, 0x00000000);
	PokePci(0x0018, 0x00000000);
	PokePci(0x0020, 0x00000000);
	PokePci(0x0028, 0x00000000);

	// Select PROGRAM and source of data
	PokePci(0x0080, upload ? 0x00000001 : 0x00000000);
	Sleep(100);
	PokePci(0x0080, upload ? 0x00000003 : 0x00000002);

	// Wait for INIT to assert
	while(PeekPci(0x0080) & 0x00000008)
	{
		Sleep(1);
	}

	Sleep(100);

	// Drop PROGRAM
	PokePci(0x0080, upload ? 0x00000021 : 0x00000020);

	// Wait for INIT to clear
	while(!(PeekPci(0x0080) & 0x00000008))
	{
		Sleep(1);
	}

	// Wait for DONE to clear
	while(PeekPci(0x0080) & 0x00000004)
	{
		Sleep(1);
	}

	Sleep(100);

	if(upload)
	{
		CString line;
		TRACE(_T("Loading FPGA..\n"));

		// Loop over lines
		while(file.ReadString(line))
		{
			unsigned int numBytes;
			unsigned int addrLsw;
			unsigned int recordType;

			// Read and check line header characters
			if(_stscanf_s(line, _T(":%2x%4x%2x"), &numBytes, &addrLsw, &recordType) != 3 ||
				line.GetLength() != int(2 * numBytes + 11))
			{
				AfxThrowFileException(CFileException::genericException, ERROR_BAD_FORMAT, virtexFile);
			}

			unsigned int checksum = numBytes + (addrLsw & 0xff) + (addrLsw >> 8) + recordType;
			unsigned int value;

			// Read data
			for(unsigned int i = 0; i < numBytes; ++i)
			{
				_stscanf_s(&LPCTSTR(line)[9 + 2 * i], _T("%2x"), &value);
				checksum += value;

				if(recordType == 0)
				{
					// Program FPGA with byte
					PokePci(0x0080, (value << 8) | 0x000000a1);

					while(PeekPci(0x0080) & 0x0010)
					{
//						Sleep(0);
					}
				}
			}

			// Read and check checksum
			_stscanf_s(&LPCTSTR(line)[9 + 2 * numBytes], _T("%2x"), &value);

			if(((checksum + value) & 0x000000ffu) != 0x00)
			{
				AfxThrowFileException(CFileException::genericException, ERROR_CRC, virtexFile);
			}
		}

		for(int i = 0; i < 20; ++i)
		{
			PokePci(0x0080, 0x000040a1);
			PokePci(0x0080, 0x000000a1);
			PokePci(0x0080, 0x000000a1);
			PokePci(0x0080, 0x000000a1);
		}

		TRACE(_T("FPGA load done\n"));
	}
	else
	{
		Sleep(100);
	}

	// Check for success
	if(!(PeekPci(0x0080) & 0x00000008))
	{
		AfxThrowFileException(CFileException::genericException, ERROR_BAD_FORMAT, virtexFile);
	}
	else
	{
		// Wait for DONE to assert
		while(!(PeekPci(0x0080) & 0x00000004))
		{
			Sleep(1);
		}
	}

	// Release write and upload
	PokePci(0x0080, 0x00000000);
	Sleep(100);

	// Clear any local interrupt flags
	PokePci(0x0000, 0xffffffff);

	// Restore interrupts
	PokePci(0x0010, intAState);
	PokePci(0x0018, intBState);
	PokePci(0x0020, intCState);
	PokePci(0x0028, intDState);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Mark samples as in-use
//
void CP7140::MarkSamplesInUse(_In_reads_(NUM_DATA_CHANS) const unsigned int(& sampleCount)[NUM_DATA_CHANS], unsigned int count)
{
	if(m_buffer[0] == nullptr || m_buffer[1] == nullptr)
	{
		return;
	}

	ASSERT(count <= unsigned int(m_bufSize[0]) && count <= unsigned int(m_bufSize[1]));
	CSingleLock lock(&m_dataCritSect, TRUE);
	m_newestInUse[0] = sampleCount[0] + count;
	m_newestInUse[1] = sampleCount[1] + count;
	m_oldestInUse.push_back(std::pair<unsigned int, unsigned int>(sampleCount[0], sampleCount[1]));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Measure noise power using threshold counters
//
void CP7140::MeasureNoiseDbm(_Out_writes_all_(2) float (&power)[2])
{
	static const unsigned int COUNTS = 10240;
	static const unsigned short THRESHOLDS[4] = { 2700, 900, 300, 100 };
	static const float SQRT2 = sqrtf(2);
	unsigned int counts[2][4];
	unsigned short thresholds[2][4];
	unsigned short dummy[4];
	GetThresholdCounters(counts[0], counts[1], thresholds[0], thresholds[1], dummy[0], dummy[0]);
	SetThresholdCounters(THRESHOLDS, THRESHOLDS);
	CollectCounts(COUNTS, counts[0], counts[1], dummy, dummy, dummy[0], dummy[0]);

	for(size_t i = 0; i < 2; ++i)
	{
		float sum = 0;
		unsigned int numGood = 0;

		for(unsigned int counter = 0; counter < 4; ++counter)
		{
			float fraction = float(counts[i][counter]) / COUNTS;

			if(fraction > 0.05f && fraction < 0.95f)
			{
				float sigma = THRESHOLDS[counter] / (SQRT2 * InvErf(1 - fraction));
				sum += sigma;
				++numGood;
			}
		}

		if(numGood > 0)
		{
			power[i] = 20 * log10(SQRT2 * sum / numGood / ADC_FULL_SCALE_COUNT) + GetAdcFullScaleDbm();
		}
		else if(float(counts[i][2]) / COUNTS < 0.5f)
		{
			power[i] = -std::numeric_limits<float>::infinity(); // underflow
		}
		else
		{
			power[i] = std::numeric_limits<float>::infinity(); // overflow
		}
	}

	SetThresholdCounters(thresholds[0], thresholds[1]);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Measure tone power using threshold counters
//
void CP7140::MeasureToneDbm(_Out_writes_all_(2) float (&power)[2])
{
	static const unsigned int COUNTS = 10240;
	static const unsigned short THRESHOLDS[4] = { 2700, 900, 300, 100 };
	unsigned int counts[2][4];
	unsigned short thresholds[2][4];
	unsigned short dummy[4];
	GetThresholdCounters(counts[0], counts[1], thresholds[0], thresholds[1], dummy[0], dummy[0]);
	SetThresholdCounters(THRESHOLDS, THRESHOLDS);
	CollectCounts(COUNTS, counts[0], counts[1], dummy, dummy, dummy[0], dummy[0]);

	for(size_t i = 0; i < 2; ++i)
	{
		float sum = 0;
		unsigned int numGood = 0;

		for(unsigned int counter = 0; counter < 4; ++counter)
		{
			float fraction = float(counts[i][counter]) / COUNTS;

			if(fraction > 0.05f && fraction < 0.95f)
			{
				float peak = THRESHOLDS[counter] / sin(Units::HALF_PI * (1 - fraction));

				if(peak <= ADC_FULL_SCALE_COUNT)
				{
					sum += peak;
					++numGood;
				}
			}
		}

		if(numGood > 0)
		{
			power[i] = 20 * log10(sum / numGood / ADC_FULL_SCALE_COUNT) + GetAdcFullScaleDbm();
		}
		else if(float(counts[i][2]) / COUNTS < 0.5f)
		{
			power[i] = -std::numeric_limits<float>::infinity(); // underflow
		}
		else
		{
			power[i] = std::numeric_limits<float>::infinity(); // overflow
		}
	}

	SetThresholdCounters(thresholds[0], thresholds[1]);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback for thread failure
//
void CP7140::OnFail(DWORD error, _In_ LPCTSTR description)
{
	CLog::Log(SMS_SERVICE_FAILED, LOG_CATEGORY_SERVICE, description);
	CService::Instance()->OnAbort(error);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Open a Pentek device
//
HANDLE CP7140::OpenDevice(LPCTSTR device, DWORD flags)
{
	HANDLE handle = CreateFile(device, GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | flags, nullptr);
		
	if(handle == INVALID_HANDLE_VALUE)
	{
		AfxThrowFileException(CFileException::genericException, GetLastError(), device);
	}

	return handle;
}


//////////////////////////////////////////////////////////////////////
//
// Overlapped wait for DMA
//
inline void CP7140::OverlappedWaitDma(HANDLE chan, _Out_ unsigned int& count, _Inout_ OVERLAPPED& o)
{
	// Setup overlapped IO
	o.Internal = o.InternalHigh = 0;
	o.Offset = o.OffsetHigh = 0;

	if(!::DeviceIoControl(chan, IOCTL_WAITDMA, &count, sizeof(count), &count, sizeof(count), nullptr, &o))
	{
		// Overlapped IO in progress
		if(GetLastError() != ERROR_IO_PENDING)
		{
			AfxThrowFileException(CFileException::genericException, GetLastError(), _T("P7140"));
		}
	}
	else
	{
		// Returned immediately
		SetEvent(o.hEvent);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Read a register
//
inline unsigned int CP7140::Peek(unsigned int page, unsigned int offset) const
{
	if(m_board != INVALID_HANDLE_VALUE)
	{
		ARG_PEEKPOKE pp = { offset, 0, page, 0 };
		DWORD bR;

		if (Utility::DeviceIoControlSync(m_board, IOCTL_REGGET, &pp, sizeof(pp), &pp, sizeof(pp), bR))
		{
			ASSERT(bR == sizeof(pp));
			return pp.value;
		}

		ASSERT(false);
	}

	return 0;
}


//////////////////////////////////////////////////////////////////////
//
// Read a GC4016 register
//
inline unsigned char CP7140::Peek4016(unsigned char page, unsigned char reg) const
{
	ASSERT(page <= 127 && reg <= 31);

	if(m_board != INVALID_HANDLE_VALUE)
	{
		ARG_PEEKPOKE pp = { reg, 0, page, 0 };
		DWORD bR;

		if (Utility::DeviceIoControlSync(m_chan[0], IOCTL_REGGET, &pp, sizeof(pp), &pp, sizeof(pp), bR))
		{
			ASSERT(bR == sizeof(pp));
			return unsigned char(pp.value);
		}

		ASSERT(FALSE);
	}

	return 0;
}


//////////////////////////////////////////////////////////////////////
//
// Read a demod register
//
inline unsigned short CP7140::PeekDemod(unsigned char ch, unsigned short addr) const
{
	ASSERT(ch <= 2);
	CSingleLock lock(&m_mutex, TRUE);
	const_cast<CP7140*>(this)->PokeFpga(0x8e38 + ch * 0x0060, addr);

	return PeekFpga(0x8e40 + ch * 0x0060);
}


//////////////////////////////////////////////////////////////////////
//
// Read a wideband DDC register
//
inline unsigned int CP7140::PeekWb(unsigned char ch, unsigned short addr) const
{
	ASSERT(ch <= 2);
	const_cast<CP7140*>(this)->PokeFpga(0x8e08 + ch * 0x0060, addr);

	return PeekFpga(0x8e10 + ch * 0x0060) | (PeekFpga(0x8e18 + ch * 0x0060) << 16);
}


//////////////////////////////////////////////////////////////////////
//
// Write a register
//
inline void CP7140::Poke(unsigned int page, unsigned int offset, unsigned int value, unsigned int mask)
{
	if(m_board != INVALID_HANDLE_VALUE)
	{
		ARG_PEEKPOKE pp = { offset, value, page, mask };
		DWORD bR;
		VERIFY(Utility::DeviceIoControlSync(m_board, IOCTL_REGSET, &pp, sizeof(pp), nullptr, 0, bR));
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Write a GC4016 register
//
inline void CP7140::Poke4016(unsigned char page, unsigned char reg, unsigned char value, unsigned char mask)
{
	ASSERT(page <= 127 && reg <= 31);

	if(m_board != INVALID_HANDLE_VALUE)
	{
		ARG_PEEKPOKE pp = { reg, value, page, 0 };
		DWORD bR;

		if(mask != 0)
		{
			if(Utility::DeviceIoControlSync(m_chan[0], IOCTL_REGGET, &pp, sizeof(pp), &pp, sizeof(pp), bR))
			{
				if((pp.value & mask) == value)
				{
					return;
				}

				pp.value &= ~mask;
				pp.value |= value;
			}
			else
			{
				ASSERT(FALSE);
				pp.offset = reg;
				pp.value = value;
				pp.page = page;
				pp.mask = mask;
			}
		}

		VERIFY(Utility::DeviceIoControlSync(m_chan[0], IOCTL_REGSET, &pp, sizeof(pp), nullptr, 0, bR));
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Write a demod register
//
inline void CP7140::PokeDemod(unsigned char ch, unsigned short addr, unsigned short value, unsigned short mask)
{
	ASSERT(ch <= 2);
	CSingleLock lock(&m_mutex, TRUE);

	if(mask != 0)
	{
		unsigned short old = PeekDemod(ch, addr);

		if((old & mask) == value)
		{
			return;
		}

		value |= (old & ~mask);
	}

	PokeFpga(0x8e38 + ch * 0x0060, addr);
	PokeFpga(0x8e40 + ch * 0x0060, value);
	PokeFpga(0x8e48 + ch * 0x0060, 0);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Write a wideband DDC register
//
inline void CP7140::PokeWb(unsigned char ch, unsigned short addr, unsigned int value, unsigned int mask)
{
	ASSERT(ch <= 2);

	if(mask != 0)
	{
		unsigned int old = PeekWb(ch, addr);

		if((old & mask) == value)
		{
			return;
		}

		value |= (old & ~mask);
	}

	PokeFpga(0x8e08 + ch * 0x0060, addr);
	PokeFpga(0x8e10 + ch * 0x0060, unsigned short(value & 0x0000ffff));
	PokeFpga(0x8e18 + ch * 0x0060, unsigned short(value >> 16));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Register a callback
//
void CP7140::RegisterNewSamplesCallback(_In_ NewSamplesCallback callback, _In_opt_ void* param, unsigned char ch)
{
	CSingleLock lock(&m_dataCritSect, TRUE);
	m_callbacks[ch] = callback;
	m_callbackParams[ch] = param;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reload the Windows driver for the 7140
//
void CP7140::ReloadDriver(void)
{
	GUID guid;
	DWORD numGuids;

	// Find 7140 device class
	if(SetupDiClassGuidsFromName(_T("Data Acquisition"), &guid, 1, &numGuids))
	{
		// Find first 7140 device instance
		HDEVINFO devInfoSet = SetupDiGetClassDevs(&guid, nullptr, nullptr, DIGCF_PRESENT);

		if(devInfoSet != INVALID_HANDLE_VALUE)
		{
			SP_DEVINFO_DATA devInfo;
			devInfo.cbSize = sizeof(devInfo);

			if(SetupDiEnumDeviceInfo(devInfoSet, 0, &devInfo))
			{
				// Setup property change structure
				SP_PROPCHANGE_PARAMS propChangeParams;
				propChangeParams.ClassInstallHeader.cbSize = sizeof(SP_CLASSINSTALL_HEADER);
				propChangeParams.ClassInstallHeader.InstallFunction = DIF_PROPERTYCHANGE;
				propChangeParams.StateChange = DICS_PROPCHANGE;
				propChangeParams.Scope = DICS_FLAG_GLOBAL;
				propChangeParams.HwProfile = 0;

				// Notify device of property change
				if(SetupDiSetClassInstallParams(devInfoSet, &devInfo, &propChangeParams.ClassInstallHeader, sizeof(propChangeParams)) &&
					SetupDiCallClassInstaller(DIF_PROPERTYCHANGE, devInfoSet, &devInfo))
				{
					// Make sure reboot is not needed
					SP_DEVINSTALL_PARAMS devInstallParams;
					devInstallParams.cbSize = sizeof(SP_DEVINSTALL_PARAMS);

					if(SetupDiGetDeviceInstallParams(devInfoSet, &devInfo, &devInstallParams) &&
						devInstallParams.Flags & (DI_NEEDREBOOT | DI_NEEDRESTART))
					{
						// Reboot the PC
						WinUtility::ShutdownSystem(_T("Pentek 7140 driver needs reboot"), 10, true);
						SetupDiDestroyDeviceInfoList(devInfoSet);
						AfxThrowFileException(CFileException::genericException, ERROR_SUCCESS_REBOOT_REQUIRED, _T("Pentek 7140"));
					}
				}
			}

			SetupDiDestroyDeviceInfoList(devInfoSet);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset sample counter
//
void CP7140::ResetSampleCounters(void)
{
	PokeFpga(0x8088, 0x0040, 0x0040);
	PokeFpga(0x8088, 0x0000, 0x0040);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset sampling chains
//
void CP7140::ResetSampling(void)
{
	if(m_sampleRate > 0)
	{
		// Empty buffers
		unsigned long decimation0 = m_decimation[0];
		unsigned long decimation1 = m_decimation[1];
		SetDecimation(4, 0);
		SetDecimation(4, 1);
		Settle(false);
		WaitForBufferSpace(m_intSize[0] + FIFO_SIZE_UINTS);
		StartSampling(m_intSize[0] + FIFO_SIZE_UINTS, 0, nullptr, nullptr);
		WaitForBufferSpace(m_bufSize[0]);
		WaitForSampling();
		SetDecimation(decimation0, 0);
		SetDecimation(decimation1, 1);
		Settle(true);
	}

	// Reset counters
	CSingleLock lock(&m_dataCritSect, TRUE);
	ASSERT(m_oldestInUse.empty());
	m_oldestInUse.clear();
	ResetSampleCounters();

	for(unsigned char ch = 0; ch < NUM_CHANS; ++ch)
	{
		// Setup buffer pointers
		m_sampleCountOffset[ch] = 0;
		m_newestSample[ch] = m_buffer[ch];

		if(ch < NUM_DATA_CHANS)
		{
#pragma warning(suppress : 6201)
			m_newestInUse[ch] = 0;
		}

		// Reset buffers
		DWORD bR;

		if(m_driverVersion[0] > _T('1'))
		{
			VERIFY(Utility::DeviceIoControlSync(m_chan[ch], IOCTL_BRDFLUSH, nullptr, 0, nullptr, 0, bR));
		}
		else
		{
			VERIFY(Utility::DeviceIoControlSync(m_chan[ch], IOCTL_CHANFLUSH, nullptr, 0, nullptr, 0, bR));
		}

		// Clear FIFO flags
		PokeFpga(0x8148 + ch * 0x0008, 0x0800);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset threshold counters
//
void CP7140::ResetThresholdCounters(void)
{
	PokeFpga(0x8088, 0x0002, 0x0002);
	PokeFpga(0x8088, 0x0000, 0x0002);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Called from thread when new samples have been added to the buffer
//
void CP7140::SamplesAdded(unsigned char ch)
{
	CSingleLock lock(&m_dataCritSect, TRUE);
	const volatile unsigned int* newestSample = m_buffer[ch] + (PeekPci(0x0118 + ch * 0x0010) - m_bufferPci[ch]) / sizeof(unsigned int);

	if(newestSample < m_newestSample[ch])
	{
		// Buffer wrapped
		m_sampleCountOffset[ch] += m_bufSize[ch];

		if(m_callbacks[ch] != nullptr)
		{
			if(m_newestSample[ch] < m_buffer[ch] + m_bufSize[ch])
			{
				m_callbacks[ch](const_cast<const Ipp16sc*>(reinterpret_cast<const volatile Ipp16sc*>(m_newestSample[ch])),
					unsigned int(m_buffer[ch] + m_bufSize[ch] - m_newestSample[ch]), m_callbackParams[ch]);
			}

			if(newestSample > m_buffer[ch])
			{
				m_callbacks[ch](const_cast<const Ipp16sc*>(reinterpret_cast<const volatile Ipp16sc*>(m_buffer[ch])),
					unsigned int(newestSample - m_buffer[ch]), m_callbackParams[ch]);
			}
		}
	}
	else if(newestSample > m_newestSample[ch])
	{
		if(m_callbacks[ch] != nullptr)
		{
			m_callbacks[ch](const_cast<const Ipp16sc*>(reinterpret_cast<const volatile Ipp16sc*>(m_newestSample[ch])),
				unsigned int(newestSample - m_newestSample[ch]), m_callbackParams[ch]);
		}
	}

	m_newestSample[ch] = newestSample;

	// Signal new data arrived
	m_newSamples[ch].SetEvent();
//	TRACE(_T("Ch %u sample count %u\n"), ch, m_sampleCountOffset[ch] + (newestSample - m_buffer[ch]));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the LVDS data out port
//
void CP7140::SetDataOut(unsigned short data)
{
	if(m_master)
	{
		PokeFpga(0x8010, data);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the DDC decimation for ref and sample channels
//
bool CP7140::SetDecimation(unsigned long decimation, unsigned char ch)
{
	ASSERT(decimation >= 2);
	ASSERT(ch < NUM_CHANS);
	__analysis_assume(ch < NUM_CHANS);

	if(decimation != m_decimation[ch])
	{
		unsigned char oldWbDecimation = m_wbDecimation[ch];
		unsigned short oldGcDecimation = m_gcDecimation[ch];
		CalcDecimations(decimation, m_sampleRate, m_gcDecimation[ch], m_wbDecimation[ch]);

		// Need to retune if switching out of or into cascade mode or in cascade mode & decimation changed
		if(((m_wbDecimation[ch] == 0 || m_gcDecimation[ch] == 0)  && oldWbDecimation > 0 && oldGcDecimation > 0) ||
			((oldWbDecimation == 0 || oldGcDecimation == 0)  && m_wbDecimation[ch] > 0 && m_gcDecimation[ch] > 0) ||
			(m_wbDecimation[ch] > 0 && m_gcDecimation[ch] > 0 && (m_wbDecimation[ch] != oldWbDecimation || m_gcDecimation[ch] != oldGcDecimation)))
		{
			m_needRetune = true;
		}

		m_decimation[ch] = decimation;
		m_needGainAdjust = true;
		m_needSync = true;
		unsigned short mux = ch | (m_demod[ch] ? 0x0080 : 0x0000);

		if(m_gcDecimation[ch] > 0 && m_wbDecimation[ch] > 0)
		{
			// Cascade downconverters
			mux |= (0x0070 | (ch == 1 ? 0x0008: 0x0000));
			PokeWb(ch, 0x0000, 0x00000400, 0x00000400); // Complex input
		}
		else if(m_wbDecimation[ch] > 0)
		{
			// Use wideband downconverter only
			mux |= (0x0060 | (ch == 1 ? 0x0008 : 0x0000));
			PokeWb(ch, 0x0000, 0x00000000, 0x00000400); // Real input
		}

		if(m_gcDecimation[ch] > 0)
		{
			// Set 4016 decimation
			ASSERT(m_gcDecimation[ch] <= GC4016_DECIMATION_MAX);

#ifdef _DEBUG
			if(m_sampleRate > 100000000 || m_sampleRate / (4 * m_gcDecimation[ch]) <= 10000)
			{
				TRACE(_T("GC4016 out of spec: sample rate %.6f MHz, channel %u, decimation %u\n"), m_sampleRate.Hz<double>() / 1e6,
					ch, m_gcDecimation[ch]);
			}
#endif

			Poke4016(7 + 8 * ch, 21, unsigned char((m_gcDecimation[ch] - 1) & 0xff));
			Poke4016(7 + 8 * ch, 22, unsigned char((m_gcDecimation[ch] - 1) >> 8), 0x0f);
		}

		if(m_wbDecimation[ch] > 0)
		{
			// Set wideband DDC decimation
			ASSERT(m_wbDecimation[ch] <= 6);
			unsigned int controlReg = (7 - m_wbDecimation[ch] - (m_decimation[ch] > 4u * MaxGcDecimation() ? 1 : 0)) << 24 |
				m_wbDecimation[ch] << 16;
			PokeWb(ch, 0x0000, controlReg, 0x07070000);
		}

		// Set mux
		PokeFpga(0x8e00 + ch * 0x0060, mux);

		return true;
	}
	else
	{
		return false;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Set the demodulation mode (oxff => off)
//
void CP7140::SetDemodMode(unsigned char mode)
{
	m_demod[2] = (mode != 0xff);
	CSingleLock lock(&m_regCritSect, TRUE);

	if(m_demod[2])
	{
		PokeDemod(2, 0x0000, mode);
	}

	PokeFpga(0x8ec0, m_demod[2] ? 0x0080 : 0x0000, 0x0080);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the demodulation parameters
//
void CP7140::SetDemodParams(unsigned short timeA, unsigned short timeB,
	unsigned short alcTimeA, unsigned short alcTimeB, long ncoPhaseInc, unsigned char skip, unsigned char interp)
{
	CSingleLock lock(&m_regCritSect, TRUE);

	if(m_invertSpectrum2)
	{
		ncoPhaseInc = -ncoPhaseInc;
	}

	PokeDemod(2, 0x0002, timeA);
	PokeDemod(2, 0x0003, timeB);
	PokeDemod(2, 0x0004, alcTimeA);
	PokeDemod(2, 0x0005, alcTimeB);
	PokeDemod(2, 0x0006, unsigned short(ncoPhaseInc & 0x0000ffff));
	PokeDemod(2, 0x0007, unsigned short(ncoPhaseInc >> 16));
	PokeDemod(2, 0x0009, skip);
	PokeDemod(2, 0x000a, interp);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the demodulation gain
//
void CP7140::SetDemodGain(unsigned short gain, bool ignoreIfFm)
{
	CSingleLock lock(&m_regCritSect, TRUE);

	// Ignore in FM mode
	if(!ignoreIfFm || PeekDemod(2, 0x0000) != 1)
	{
		PokeDemod(2, 0x0001, gain);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the DDC FIR bandwidth
//
void CP7140::SetFirBw(unsigned char firBw, unsigned char ch)
{
	if(firBw != m_firBw[ch])
	{
		m_firBw[ch] = firBw;

		switch(m_firBw[ch])
		{
		case 17:
			m_cfir[ch] = GC4016_CFIR_17;
			m_pfir[ch] = GC4016_PFIR_17;
			break;

		case 34:
			m_cfir[ch] = GC4016_CFIR_34;
			m_pfir[ch] = GC4016_PFIR_34;
			break;

		case 68:
			m_cfir[ch] = GC4016_CFIR_68;
			m_pfir[ch] = GC4016_PFIR_68;
			break;

		case 80:
			m_cfir[ch] = GC4016_CFIR_80;
			m_pfir[ch] = GC4016_PFIR_80;
			break;

		case 100:
			m_cfir[ch] = GC4016_CFIR_100;
			m_pfir[ch] = GC4016_PFIR_100;
			break;

		case 150:
			m_cfir[ch] = GC4016_CFIR_150;
			m_pfir[ch] = GC4016_PFIR_150;
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		// Load CFIR coefficients
		for(unsigned char i = 0; i < GC4016_CFIR_TABLE_SIZE; ++i)
		{
			Poke4016(8 * ch + i / 8, 16 + 2 * (i % 8), unsigned char(m_cfir[ch][i] & 0x00ff));
			Poke4016(8 * ch + i / 8, 17 + 2 * (i % 8), unsigned char((m_cfir[ch][i] & 0xff00) >> 8));
		}

		// Load PFIR coefficients
		for(unsigned char i = 0; i < GC4016_PFIR_TABLE_SIZE; ++i)
		{
			Poke4016(2 + 8 * ch + i / 8, 16 + 2 * (i % 8), unsigned char(m_pfir[ch][i] & 0x00ff));
			Poke4016(2 + 8 * ch + i / 8, 17 + 2 * (i % 8), unsigned char((m_pfir[ch][i] & 0xff00) >> 8));
		}

		// CFIR gain
		m_cfirGain = 0;

		for(size_t i = 0; i < GC4016_CFIR_TABLE_SIZE; ++i)
		{
			m_cfirGain += 2 * m_cfir[ch][i];
		}

		m_cfirGain -= m_cfir[ch][GC4016_CFIR_TABLE_SIZE - 1];
		m_cfirGain /= 65536;

		// PFIR gain
		m_pfirGain = 0;

		for(size_t i = 0; i < GC4016_PFIR_TABLE_SIZE; ++i)
		{
			m_pfirGain += 2 * m_pfir[ch][i];
		}

		m_pfirGain -= m_pfir[ch][GC4016_PFIR_TABLE_SIZE - 1];
		m_pfirGain /= 65536;

		// Always use the 80% wideband filter
		PokeWb(ch, 0x0000, 0x00000000, 0x00006000);

		// Resync
		m_needGainAdjust = true;
		m_needSync = true;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the 4016 gain to unity
//
bool CP7140::SetGainToUnity(unsigned char ch)
{
	bool success = true;

	if(m_gcDecimation[ch] > 0)
	{
		// Set 4016 gain
		unsigned char shift;
		unsigned char bigScale;
		unsigned char scale;
		unsigned char mix20b;
		unsigned long long decimation5 = unsigned long long(m_gcDecimation[ch]) * m_gcDecimation[ch] * m_gcDecimation[ch] *
			m_gcDecimation[ch] * m_gcDecimation[ch];

		// Decimation settings
		unsigned long index;

		if(_BitScanReverse(&index, (decimation5 - 1) >> 32))
		{
			index += 32;
		}
		else if(!_BitScanReverse(&index, DWORD(decimation5 - 1)))
		{
			index = 0;
		}

		unsigned char temp = unsigned char(61 - index);

		if(temp < 4)
		{
			shift = temp;
			bigScale = 0;
			scale = 0;
			mix20b = 0;
		}
		else
		{
			shift = 4;
			temp = temp - shift;
			bigScale = temp / 6;
			scale = temp % 6;
			mix20b = 1;

			while(bigScale > GC4016_BIG_SCALE_MAX)
			{
				if(++shift > GC4016_SHIFT_MAX)
				{
					ASSERT(FALSE);
					shift = GC4016_SHIFT_MAX;
					bigScale = GC4016_BIG_SCALE_MAX;
					scale = GC4016_SCALE_MAX;
					success = false;
					break;
				}

				bigScale = --temp / 6;
				scale = temp % 6;
			}

		}

		// CIC gain
		double cicGain = double(decimation5) / (1ull << (62 - shift - scale - 6 * bigScale));
		ASSERT(cicGain <= 1);

		// Calculate coarse and fine gains for overall unity gain
		double desiredGain = 2; // To account for real to complex
		unsigned long coarseGain;

		if(!_BitScanReverse(&coarseGain, unsigned long(desiredGain * 1024 / (cicGain * m_cfirGain * m_pfirGain * GC4016_FINE_GAIN_MAX) + 0.5)))\

		{
			coarseGain = 0;
		}

		unsigned short fineGain;

		if(coarseGain > GC4016_COARSE_GAIN_MAX)
		{
			ASSERT(FALSE);
			coarseGain = GC4016_COARSE_GAIN_MAX;
			fineGain = GC4016_FINE_GAIN_MAX;
			success = false;
		}
		else
		{
#pragma warning(suppress : 6297) // 32-bit shift overflow
			fineGain = unsigned short(1024 * desiredGain / (cicGain * m_cfirGain * m_pfirGain * (1 << coarseGain)) + 0.5);

			if(fineGain >= GC4016_FINE_GAIN_MAX)
			{
				fineGain = GC4016_FINE_GAIN_MAX;
				ASSERT(FALSE);
				success = false;
			}
		}

		Poke4016(7 + 8 * ch, 22, 0x70, 0x70);
		Poke4016(7 + 8 * ch, 16, shift | 0x08, 0x0f);
		Poke4016(7 + 8 * ch, 23, scale | (bigScale << 3) | (mix20b << 6));
		Poke4016(7 + 8 * ch, 25, unsigned char(coarseGain << 4), 0x70);
		Poke4016(7 + 8 * ch, 30, unsigned char(fineGain & 0x00ff));
		Poke4016(7 + 8 * ch, 31, unsigned char((fineGain >> 8) & 0x003f));
	}

	return success;
}


//////////////////////////////////////////////////////////////////////
//
// Turn gate A on or off
//
void CP7140::SetGate(bool on, unsigned char bus)
{
	PokeFpga(0x8090 + bus * 0x0018, on ? 0x0000 : 0x0001);

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Set the sampling rate
//
void CP7140::SetSampleRate(Units::Frequency sampleRate)
{
	ASSERT(sampleRate >= 15000000 && sampleRate <= 105000000);

	if(sampleRate != m_sampleRate)
	{
		m_sampleRate = sampleRate;
		m_needRetune = true;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Setup sequencer
//
void CP7140::SetSequencer(const std::vector<unsigned short>& pattern, unsigned int delay, unsigned short samples)
{
	ASSERT(pattern.size() <= MAX_SEQUENCER_BLOCKS);
	PokeFpga(0x8a20, unsigned short(pattern.size()));
	PokeFpga(0x8a70, unsigned short(delay & 0x0000ffff));
	PokeFpga(0x8a78, unsigned short(delay >> 16));
	PokeFpga(0x8a80, samples);

	for(unsigned char i = 0; i < pattern.size(); ++i)
	{
		PokeFpga(0x8a28 + 8 * i, pattern[i]);
	}

	PokeFpga(0x8088, 0x0200, 0x0200);
	PokeFpga(0x8088, 0x0000, 0x0200);
	m_sequencerTime = unsigned long(pattern.size() * (delay + samples * m_decimation[0]) / (m_sampleRate.Hz<float>() / 1e6f)); // us

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set temperature limits
//
void CP7140::SetTempLimits(_In_ const std::vector<std::array<float, 2> >& limits)
{
	ASSERT(limits.size() == 4);

	if(m_board != INVALID_HANDLE_VALUE)
	{
		LIMIT_CFG lim;
		DWORD bR;
		lim.lowLimit = limits[0][0];
		lim.highLimit = limits[0][1];
		VERIFY(::DeviceIoControl(m_tempSensor, IOCTL_SETETEMP1LIM, &lim, sizeof(lim), &lim, sizeof(lim), &bR, nullptr));
		ASSERT(bR == sizeof(lim));
		lim.lowLimit = limits[1][0];
		lim.highLimit = limits[1][1];
		VERIFY(::DeviceIoControl(m_tempSensor, IOCTL_SETETEMP2LIM, &lim, sizeof(lim), &lim, sizeof(lim), &bR, nullptr));
		ASSERT(bR == sizeof(lim));
		lim.lowLimit = limits[2][0];
		lim.highLimit = limits[2][1];
		VERIFY(::DeviceIoControl(m_tempSensor, IOCTL_SETETEMP3LIM, &lim, sizeof(lim), &lim, sizeof(lim), &bR, nullptr));
		ASSERT(bR == sizeof(lim));
		lim.lowLimit = limits[3][0];
		lim.highLimit = limits[3][1];
		VERIFY(::DeviceIoControl(m_tempSensor, IOCTL_SETITEMPLIM, &lim, sizeof(lim), &lim, sizeof(lim), &bR, nullptr));
		ASSERT(bR == sizeof(lim));
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set threshold counter thresholds
//
void CP7140::SetThresholdCounters(_In_reads_(4) const unsigned short (&threshA)[4],
								  _In_reads_(4) const unsigned short (&threshB)[4])
{
	for(unsigned char ctr = 0; ctr < 4; ++ctr)
	{
		PokeFpga(0x8940 + 8 * ctr, threshA[ctr]);
		PokeFpga(0x8960 + 8 * ctr, threshB[ctr]);
	}

	ResetThresholdCounters();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for filters etc to settle
//
void CP7140::Settle(bool wait)
{
	if(m_needRetune)
	{
		m_needRetune = false;
		wait = true;
		Units::Frequency freq = m_freq;
		m_freq = 0;
		Tune(freq, m_invertSpectrum01, m_shift);
		freq = m_demodFreq;
		m_demodFreq = 0;
		TuneDemod(freq, m_invertSpectrum2);
	}

	if(m_needGainAdjust)
	{
		m_needGainAdjust = false;
		wait = true;
		SetGainToUnity(0);
		SetGainToUnity(1);
		SetGainToUnity(2);
	}

	if(m_needSync)
	{
		m_needSync = false;
		wait = true;
		PokeFpga(0x8088, 0x0000, 0x0001);
		m_timer->Delay(5);
		PokeFpga(0x8088, 0x0001, 0x0001);
	}

	if(wait)
	{
		// Wait for filters to settle
		m_timer->Delay(unsigned long(GetLatency(0) / (m_sampleRate.Hz<float>() / 1e6f)));
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the internal wideband DDC test tone
//
void CP7140::SetWbTestTone(bool on, Units::Frequency freq, unsigned char ch)
{
	ASSERT(m_wbDecimation[ch] > 0 && m_gcDecimation[ch] == 0);
	m_needSync = true;
	PokeWb(ch, 0x0001, CalcFreqWord(on ? freq : m_freq));
	PokeWb(ch, 0x0000, on ? 0x00000200 : 0x00000000, 0x00000300);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Start collecting a block of samples
//
void CP7140::StartSampling(unsigned int count, unsigned long pollInterval, _In_opt_ CTimer::PollCallback pollCallback, _In_opt_ void* param)
{
	if(m_board != INVALID_HANDLE_VALUE)
	{
		// Adjust for decimation and length scaling
		count *= m_decimation[0];

		// Program DDC A & B Trigger Length Registers
		PokeFpga(0x8c10, unsigned short(count & 0x0000ffff));
		PokeFpga(0x8c18, unsigned short(count >> 16));
		PokeFpga(0x8c40, unsigned short(count & 0x0000ffff));
		PokeFpga(0x8c48, unsigned short(count >> 16));

		// Set TRIG mode and toggle TRIG CLEAR in DDC A & B FIFO Control registers
		PokeFpga(0x8c08, 0x0014, 0x0014);
		PokeFpga(0x8c08, 0x0000, 0x0010);
		PokeFpga(0x8c38, 0x0014, 0x0014);
		PokeFpga(0x8c38, 0x0000, 0x0010);

		// Enable TRIGGER CAPTURE in DDC A FIFO Interrupt Mask Register
		PokeFpga(0x8c20, 0x0020, 0x0020);

		// Clear TRIGGER CAPTURE flag in Application Interrupt Flag register
		PokeFpga(0x8138, 0x0400);

		// Fire the trigger
		PokeFpga(0x8090, 0x0000);
		PokeFpga(0x8090, 0x0001);

		// Start a timer
		m_timer->Start(unsigned long(count / (m_sampleRate.Hz<float>() / 1e6f)), pollInterval, pollCallback, param);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Start the sequencer. Returns sampling time in us
//
void CP7140::StartSequencer(unsigned long pollInterval, _In_opt_ CTimer::PollCallback pollCallback, _In_opt_ void* param)
{
	if(m_board != INVALID_HANDLE_VALUE)
	{
		// Clear the interrupt status bit
		PokeFpga(0x8088, 0x0800, 0x0800);
		PokeFpga(0x8088, 0x0000, 0x0800);
		PokeFpga(0x8108, 0x0040, 0x0040);

		// Toggle the sequencer start bit and enable gate
		PokeFpga(0x8088, 0x3000, 0xf000);
		PokeFpga(0x8088, 0x0400, 0x0400);
		PokeFpga(0x8088, 0x0000, 0x0400);

		// Start a timer
		m_timer->Start(m_sequencerTime, pollInterval, pollCallback, param);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Thread function
//
void CP7140::Thread(void)
{
	if(m_board != INVALID_HANDLE_VALUE)
	{
		// Setup overlapped IO
		unsigned int count[NUM_CHANS] = { 0, 0, 0 };
		const HANDLE events[] = { CreateEvent(nullptr, TRUE, FALSE, nullptr), CreateEvent(nullptr, TRUE, FALSE, nullptr),
			CreateEvent(nullptr, TRUE, FALSE, nullptr), CreateEvent(nullptr, TRUE, FALSE, nullptr), HANDLE(m_shutdown) };

		if(events[0] ==  nullptr || events[1] ==  nullptr || events[2] ==  nullptr || events[3] ==  nullptr)
		{
			AfxThrowResourceException();
		}

		OVERLAPPED o[NUM_CHANS + 1] = { 0 };
		o[0].hEvent = events[0];
		o[1].hEvent = events[1];
		o[2].hEvent = events[2];
		o[3].hEvent = events[3];

		// Setup for first events
		OverlappedWaitDma(m_chan[0], count[0], o[0]);
		OverlappedWaitDma(m_chan[1], count[1], o[1]);
		OverlappedWaitDma(m_chan[2], count[2], o[2]);

		// Setup for clock loss detection
		EVENT_CFG ev;
		ev.event = 0;
		ev.mask = EVENT_CLKLOSS1;
		DWORD bR;
		VERIFY(Utility::DeviceIoControlSync(m_board, IOCTL_CFGEVENT, &ev, sizeof(ev), nullptr, 0, bR));
		ASSERT(bR == 0);

		if(!::DeviceIoControl(m_board, IOCTL_WAITEVENT, &ev, sizeof(ev), nullptr, 0, &bR, &o[3]))
		{
			ASSERT(GetLastError() == ERROR_IO_PENDING);
		}

		PokeFpga(0x8118, 0x8000, 0x8000); // Pentek bug
		ASSERT(bR == 0);
		unsigned char signaled;

		// Loop until shutdown event signaled
		while((signaled = unsigned char(WaitForMultipleObjects(_countof(events), events, FALSE, INFINITE) - WAIT_OBJECT_0)) != _countof(events) - 1)
		{
			if(signaled == 3)
			{
				// Clock lost - switch back to internal
				m_badClock = true;
				PokeFpga(0x8080, 0x0000, 0x001c); // Master bus A control TERM = terminated MASTR = master OSC DIS = 0 SEL CLK = osc
				PokeFpga(0x8108, 0x0004); // System interrupt flag clear CLK A LOSS
				ResetEvent(events[3]);
			}
			else
			{
				ASSERT(signaled < NUM_CHANS);
				__analysis_assume(signaled < NUM_CHANS);
				SamplesAdded(signaled);

				// Setup for next event
				OverlappedWaitDma(m_chan[signaled], count[signaled], o[signaled]);

				// Ping watchdog
				CSingleLock lock(&m_dataCritSect, TRUE);

				if(m_watchDog != nullptr)
				{
					(*m_watchDog)->Ping(m_watchDogSource);
				}
			}
		}

		CancelIo(m_chan[0]);
		CancelIo(m_chan[1]);
		CancelIo(m_chan[2]);
		CancelIo(m_board);
		WaitForMultipleObjects(_countof(events), events, TRUE, INFINITE);
		CloseHandle(events[0]);
		CloseHandle(events[1]);
		CloseHandle(events[2]);
		CloseHandle(events[3]);
	}
}


//////////////////////////////////////////////////////////////////////
//
// Tune the DDRs (chs 0 and 1)
//
void CP7140::Tune(Units::Frequency freq, bool invertSpectrum, bool shift)
{
	TEST_LOGIC_ERROR(freq <= m_sampleRate / 2);

	if(freq != m_freq || invertSpectrum != m_invertSpectrum01 || shift != m_shift)
	{
		m_needRetune = false;
		m_needSync = true;

		for(unsigned char ch = 0; ch < NUM_DATA_CHANS; ++ch)
		{
			unsigned long freqWord = CalcFreqWord(freq);

			// Extra inversion to compensate for Is and Qs being reversed in output word
			unsigned char negCtl = (!invertSpectrum ? 0x0a : 0x00);

			if(m_gcDecimation[ch] > 0 && m_wbDecimation[ch] > 0)
			{
				// Cascade downconverters

				// Offset 4016 tuning to work around DC spur in wideband DDC
				if(invertSpectrum)
				{
					freqWord -= 0x80000000 / m_decimation[0];
				}
				else
				{
					freqWord += 0x80000000 / m_decimation[0];
				}

				PokeWb(ch, 0x0000, shift ? 0x40000000 : 0x00000000, 0x60000300);
				PokeWb(ch, 0x0001, 0x80000000 >> m_wbDecimation[ch]);
			}
			else
			{
				// Extra inversion to compensate for Is and Qs being reversed in output word
				if(shift)
				{
					PokeWb(ch, 0x0000, !invertSpectrum ? 0x60000000 : 0x40000000, 0x60000300);

					// Shift GC4016 spectrum
					negCtl ^= 0x03;
				}
				else
				{
					PokeWb(ch, 0x0000, !invertSpectrum ? 0x20000000 : 0x00000000, 0x60000300);
				}

				PokeWb(ch, 0x0001, freqWord);
			}

			// Tune GC4016
			Poke4016(7 + 8 * ch, 24, negCtl, 0x0f);
			Poke4016(7 + 8 * ch, 17, 0x70, 0x70);
			Poke4016(6 + 8 * ch, 18, unsigned char(freqWord & 0x000000ff));
			Poke4016(6 + 8 * ch, 19, unsigned char((freqWord & 0x0000ff00) >> 8));
			Poke4016(6 + 8 * ch, 20, unsigned char((freqWord & 0x00ff0000) >> 16));
			Poke4016(6 + 8 * ch, 21, unsigned char((freqWord & 0xff000000) >> 24));
			Poke4016(7 + 8 * ch, 17, 0x20, 0x70);
		}

		m_freq = freq;
		m_invertSpectrum01 = invertSpectrum;
		m_shift = shift;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Tune the DDRs (ch 2)
//
void CP7140::TuneDemod(Units::Frequency freq, bool invertSpectrum)
{
	ASSERT(freq < m_sampleRate / 2);

	if(freq != m_demodFreq || invertSpectrum != m_invertSpectrum2)
	{
		unsigned long freqWord = CalcFreqWord(freq);
		m_needRetune = false;
		m_needSync = true;
		if(m_gcDecimation[2] > 0 && m_wbDecimation[2] > 0)
		{
			// Cascade downconverters

			// Offset 4016 tuning to work around DC spur in wideband DDC
			if(invertSpectrum)
			{
				freqWord -= 0x80000000 / m_decimation[2];
			}
			else
			{
				freqWord += 0x80000000 / m_decimation[2];
			}

			PokeWb(2, 0x0000, 0x00000000, 0x60000300);
			PokeWb(2, 0x0001, 0x80000000 >> m_wbDecimation[2]);
		}
		else
		{
			PokeWb(2, 0x0000, !invertSpectrum ? 0x20000000 : 0x00000000, 0x60000300);
			PokeWb(2, 0x0001, freqWord);
		}

		// Tune GC4016
		Poke4016(23, 24, !invertSpectrum ? 0x0a : 0x00, 0x0f);
		Poke4016(23, 17, 0x70, 0x70);
		Poke4016(22, 18, unsigned char(freqWord & 0x000000ff));
		Poke4016(22, 19, unsigned char((freqWord & 0x0000ff00) >> 8));
		Poke4016(22, 20, unsigned char((freqWord & 0x00ff0000) >> 16));
		Poke4016(22, 21, unsigned char((freqWord & 0xff000000) >> 24));
		Poke4016(23, 17, 0x20, 0x70);

		m_demodFreq = freq;
		m_invertSpectrum2 = invertSpectrum;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Unregister a callback
//
void CP7140::UnregisterNewSamplesCallback(unsigned char ch)
{
	CSingleLock lock(&m_dataCritSect, TRUE);
	m_callbacks[ch] = nullptr;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for enough free space in a DMA buffer
//
void CP7140::WaitForBufferSpace(unsigned int count) const
{
	ASSERT(int(count) <= m_bufSize[0] && int(count) <= m_bufSize[1]); // Check buffer big enough
	unsigned int sampleCount[NUM_DATA_CHANS];
	CSingleLock lock(&m_dataCritSect, TRUE);
	CSingleLock wait(&m_moreBufferSpace);

	while(m_oldestInUse.size() > 0)
	{
		GetSampleCount(sampleCount);
		unsigned int freeSpace0 = (m_bufSize[0] + m_oldestInUse.front().first - sampleCount[0]) % m_bufSize[0];
		unsigned int freeSpace1 = (m_bufSize[1] + m_oldestInUse.front().second - sampleCount[1]) % m_bufSize[1];
		unsigned int queuedBlocksSize0;
		unsigned int queuedBlocksSize1;

		if(m_oldestInUse.size() == 1)
		{
			queuedBlocksSize0 = sampleCount[0] - m_oldestInUse.front().first;
			queuedBlocksSize1 = sampleCount[1] - m_oldestInUse.front().second;
		}
		else if(m_oldestInUse.size() <= MIN_QUEUED_BLOCKS)
		{
			queuedBlocksSize0 = m_newestInUse[0] - m_oldestInUse.front().first;
			queuedBlocksSize1 = m_newestInUse[1] - m_oldestInUse.front().second;
		}
		else
		{
			queuedBlocksSize0 = m_oldestInUse[MIN_QUEUED_BLOCKS].first - m_oldestInUse.front().first;
			queuedBlocksSize1 = m_oldestInUse[MIN_QUEUED_BLOCKS].second - m_oldestInUse.front().second;
		}

		if(freeSpace0 >= count && freeSpace1 >= count && 
			int(freeSpace0 + queuedBlocksSize0) + m_intSize[0] + FIFO_SIZE_UINTS >= m_bufSize[0] &&
			int(freeSpace1 + queuedBlocksSize1) + m_intSize[1] + FIFO_SIZE_UINTS >= m_bufSize[1])
		{
			// Both enough free space and not too many blocks pending
			break;
		}

		// Need to wait
		m_moreBufferSpace.ResetEvent();
		lock.Unlock();
		wait.Lock();
		wait.Unlock();
		lock.Lock();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for the sample collection to complete
//
void CP7140::WaitForSampling(void)
{
	if(m_board != INVALID_HANDLE_VALUE)
	{
		// Wait until it's done
		if(PeekFpga(0x8138) & 0x0400)
		{
			m_timer->Stop();
		}
		else
		{
			m_timer->WaitForCompletion();

			while(!(PeekFpga(0x8138) & 0x0400))
			{
				_mm_pause();
			}
		}
	}

	// Clear TRIG mode in DDC A & B FIFO Control registers
	PokeFpga(0x8c08, 0x0000, 0x0004);
	PokeFpga(0x8c38, 0x0000, 0x0004);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for the sequencer to complete
//
void CP7140::WaitForSequencer(void)
{
	if(m_board != INVALID_HANDLE_VALUE)
	{
		if(PeekFpga(0x8108) & 0x0040)
		{
			m_timer->Stop();
		}
		else
		{
			m_timer->WaitForCompletion();

			// Wait for interrupt status bit
			while(!(PeekFpga(0x8108) & 0x0040))
			{
				_mm_pause();
			}
		}
	}

	// Clear the interrupt status bit and gate enables
	PokeFpga(0x8088, 0x0800, 0xf800);
	PokeFpga(0x8088, 0x0000, 0x0800);
	PokeFpga(0x8108, 0x0040, 0x0040);

	return;
}


