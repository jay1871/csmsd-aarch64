/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#pragma once

#include <array>
#include <deque>
#include <gc4016.h>
#include <ipp.h>
#include <memory>
#include <ptkddr.h>
#include <vector>

#ifdef PI
#undef PI // We have our own definition in Units.h
#endif

#include "Timer.h"
#include "Units.h"
#include "WatchDog.h"

template<typename T> class CSingleton;

class CP7140
{
private:
	// Constants
	static const unsigned int NUM_DATA_CHANS = 2;

public:
	// Constants
	static const int ADC_FULL_SCALE_COUNT = 8182;
	static const int BUF_SIZE_UINTS = 2097152;
	static const unsigned int NUM_CHANS = 3;
	static const size_t MAX_SEQUENCER_BLOCKS = 9;

	// Functions
	unsigned short GetDataIn(void) const { return PeekFpga(0x8008); }
	unsigned short GetDemodAlcData(unsigned char ch) const { return PeekDemod(ch, 0x0008); }
	unsigned short GetFpgaCore(void) const { return PeekFpga(0x8048); }
	unsigned long GetLatency(unsigned char ch) const { return GetLatency(m_gcDecimation[ch], m_wbDecimation[ch]); }
	unsigned char GetPciBusSpeed(void) const;
	unsigned char GetPciBusWidth(void) const;
	unsigned short GetPciDmaBurstSize(void) const;
	COleDateTime GetPciFwDate(void) const;
	unsigned char GetPciFwRev(void) const { return unsigned char(PeekPci(0x0078) & 0xff); }
	void SetDemodGain(unsigned short gain, bool ignoreIfFm);
	void SetDemodMode(unsigned char mode);
	void SetDemodParams(unsigned short timeA, unsigned short timeB, unsigned short alcTimeA, unsigned short alcTimeB, long ncoPhaseInc,
		unsigned char skip, unsigned char interp);

protected:
	// Types
	typedef void (*NewSamplesCallback)(const Ipp16sc* samples, unsigned int count, void* param);

	// Functions
	CP7140(void); // Can only be constructed by a derived class
	~CP7140(void);
	void AdjustDecimation(_Inout_ unsigned long& decimation, _In_ bool adjustDown = false) const;
	void CollectCounts(unsigned int count,
		_Out_writes_all_(4) unsigned int (&countA)[4],
		_Out_writes_all_(4) unsigned int (&countB)[4],
		_Out_writes_all_(4) unsigned short (&threshA)[4],
		_Out_writes_all_(4) unsigned short (&threshB)[4],
		_Out_ unsigned short& maxA, _Out_ unsigned short& maxB);
	void CollectSamples(unsigned int count);
	void DoneWithSamples(void);
	void EnableWatchDog(bool enable);
	bool FifoHasOverrun(unsigned char ch);
	_Ret_opt_count_(count) const Ipp16sc* FindSamples(unsigned int sampleCount, unsigned int count,
		unsigned char ch, DWORD timeout);
	float GetAdcFullScaleDbm(void) const { return ADC_FULL_SCALE_DBM; }
	float GetAdcFullScaleWatts(void) const { return ADC_FULL_SCALE_WATTS; }
	int GetBufferIntSize(unsigned char ch) const { return m_intSize[ch]; }
	int GetBufferSize(unsigned char ch) const { return m_bufSize[ch]; }
	bool GetClockStatus(void) const { return !m_badClock; }
	unsigned short GetDataOut(void) const { return PeekFpga(0x8010); }
	unsigned long GetDecimation(unsigned char ch) const { return m_decimation[ch]; }
	CString GetDriverVersion(void) const { return m_driverVersion; }
	unsigned char GetFirBw(unsigned char ch) const { return m_firBw[ch]; }
	COleDateTime GetFpgaFwDate(void) const;
	unsigned char GetFpgaFwRev(void) const { return unsigned char(PeekFpga(0x8040) & 0xff); }
	static float GetLatency(Units::Frequency sampleRate, unsigned long decimation);
	unsigned int GetNewestSampleCount(unsigned char ch) const;
	void GetSampleCount(_Out_writes_all_(NUM_DATA_CHANS) unsigned int(& count)[NUM_DATA_CHANS]) const;
	Units::Frequency GetSampleRate(void) const { return m_sampleRate; }
	std::vector<float>  GetTemperatures(void) const;
	std::vector<std::array<float, 2> > GetTempLimits(void) const;
	void GetThresholdCounters(_Out_writes_all_(4) unsigned int (&countA)[4],
		_Out_writes_all_(4) unsigned int (&countB)[4],
		_Out_writes_all_(4) unsigned short (&threshA)[4],
		_Out_writes_all_(4) unsigned short (&threshB)[4],
		_Out_ unsigned short& maxA, _Out_ unsigned short& maxB) const;
	CString GetVersion(void);
	std::vector<std::array<float, 2> > GetVoltageLimits(void) const;
	std::vector<float> GetVoltages(void) const;
	void Init(void);
	bool IsMaster(void) const { return m_master; }
	bool IsPresent(void) const { return m_board != INVALID_HANDLE_VALUE; }
	void MarkSamplesInUse(_In_reads_(NUM_DATA_CHANS) const unsigned int(& sampleCount)[NUM_DATA_CHANS], unsigned int count);
	void MeasureNoiseDbm(_Out_writes_all_(2) float (&power)[2]);
	void MeasureToneDbm(_Out_writes_all_(2) float (&power)[2]);
	void RegisterNewSamplesCallback(_In_ NewSamplesCallback callback, _In_opt_ void* param, unsigned char ch);
	void ResetSampling(void);
	void ResetThresholdCounters(void);
	void SetWbTestTone(bool on, Units::Frequency freq, unsigned char ch);
	void SetDataOut(unsigned short data);
	bool SetDecimation(unsigned long decimation, unsigned char ch);
	void SetFirBw(unsigned char firBw, unsigned char ch);
	void SetGate(bool on, unsigned char bus);
	void SetSampleRate(Units::Frequency sampleRate);
	void SetSequencer(const std::vector<unsigned short>& pattern, unsigned int delay, unsigned short samples);
	void SetTempLimits(_In_ const std::vector<std::array<float, 2> >& limits);
	void SetThresholdCounters(_In_reads_(4) const unsigned short (&threshA)[4],
		_In_reads_(4) const unsigned short (&threshB)[4]);
	void Settle(bool wait);
	void StartSampling(unsigned int count, unsigned long pollInterval, _In_opt_ CTimer::PollCallback pollCallback, _In_opt_ void* param);
	void StartSequencer(unsigned long pollInterval, _In_opt_ CTimer::PollCallback pollCallback, _In_opt_ void* param);
	void Tune(Units::Frequency freq, bool invertSpectrum, bool shift);
	void TuneDemod(Units::Frequency freq, bool invertSpectrum);
	void UnregisterNewSamplesCallback(unsigned char ch);
	void WaitForBufferSpace(unsigned int count) const;
	void WaitForSampling(void);
	void WaitForSequencer(void);

private:
	// Constants
	static const float ADC_FULL_SCALE_WATTS;
	static const float ADC_FULL_SCALE_DBM;
	static const int INT_SIZE_UINTS = 2048;
	static const int DEMOD_BUF_SIZE_UINTS = 262144;
	static const int DEMOD_INT_SIZE_UINTS = 1024;
	static const int FIFO_SIZE_UINTS = 2048;
	static const unsigned int MIN_QUEUED_BLOCKS = 2;
	static LPCTSTR MUTEX_NAME;
	static const DWORD SPIN_COUNT = 4000;
	static const unsigned int THREAD_TIMEOUT = 10000; // ms

	// Types
	struct SECURITY_ATTRIBUTES_ALL_ACCESS : public SECURITY_ATTRIBUTES
	{
		SECURITY_ATTRIBUTES_ALL_ACCESS(void)
		{
			if(!InitializeSecurityDescriptor(&m_secDesc, SECURITY_DESCRIPTOR_REVISION))
			{
				AfxThrowResourceException();
			}

#pragma warning(suppress : 6248) // Null DACL
			if(!SetSecurityDescriptorDacl(&m_secDesc, TRUE, nullptr, TRUE))
			{
				AfxThrowResourceException();
			}

			nLength = sizeof(SECURITY_ATTRIBUTES);
			lpSecurityDescriptor = &m_secDesc;
			bInheritHandle = FALSE;

			return;
		}

	private:
		SECURITY_DESCRIPTOR m_secDesc;
	};

	// Functions
	CP7140(const CP7140&); // Not allowed
	CP7140& operator=(const CP7140&); // Not allowed
	static void CalcDecimations(unsigned long decimation, Units::Frequency sampleRate, _Out_ unsigned short& gcDecimation,
		_Out_ unsigned char& wbDecimation);
	unsigned long CalcFreqWord(Units::Frequency freq) const;
	static unsigned long GetLatency(unsigned short gcDecimation, unsigned char wbDecimation);
	void LoadVirtex(bool upload = true);
	static float InvErf(float x);
	static unsigned short MaxGcDecimation(Units::Frequency sampleRate);
	inline unsigned short MaxGcDecimation(void) const { return MaxGcDecimation(m_sampleRate); }
	void OnFail(DWORD error, _In_ LPCTSTR description);
	HANDLE OpenDevice(LPCTSTR device, DWORD flags = 0);
	inline static void OverlappedWaitDma(HANDLE chan, _Out_ unsigned int& count, _Inout_ OVERLAPPED& o);
	inline unsigned int Peek(unsigned int page, unsigned int offset) const;
	inline unsigned char Peek4016(unsigned char page, unsigned char reg) const;
	inline unsigned short PeekDemod(unsigned char ch, unsigned short addr) const;
	inline unsigned short PeekFpga(unsigned short addr) const { return unsigned short(Peek(2, addr) & 0x0000ffff); }
	inline unsigned int PeekPci(unsigned short addr) const { return Peek(0, addr); }
	inline unsigned int PeekWb(unsigned char ch, unsigned short addr) const;
	inline void Poke(unsigned int page, unsigned int offset, unsigned int value, unsigned int mask = 0);
	inline void Poke4016(unsigned char page, unsigned char reg, unsigned char value, unsigned char mask = 0);
	inline void PokeDemod(unsigned char ch, unsigned short addr, unsigned short value, unsigned short mask = 0);
	inline void PokeFpga(unsigned short addr, unsigned short value, unsigned short mask = 0) { Poke(2, addr, value, mask); }
	inline void PokePci(unsigned short addr, unsigned int value, unsigned int mask = 0) { Poke(0, addr, value, mask); }
	inline void PokeWb(unsigned char ch, unsigned short addr, unsigned int value, unsigned int mask = 0);
	void ReloadDriver(void);
	void ResetSampleCounters(void);
	void SamplesAdded(unsigned char ch);
	bool SetGainToUnity(unsigned char ch);
	void Thread(void);

	// Data
	volatile bool m_badClock; // External clock is bad
	HANDLE m_board; // Device handle for board
	const volatile unsigned int* m_buffer[NUM_CHANS]; // Memory-mapped buffer pointers
	unsigned int m_bufferPci[NUM_CHANS]; // Buffer PCI addresses
	int m_bufSize[NUM_CHANS]; // Buffer size
	NewSamplesCallback m_callbacks[NUM_CHANS]; // Callback functions
	void* m_callbackParams[NUM_CHANS]; // Params for callbacks
	const int* m_cfir[NUM_CHANS];
	double m_cfirGain;
	HANDLE m_chan[NUM_CHANS]; // Device handles for sample channels
	static bool m_constructed; // Is class constructed?
	mutable CCriticalSection m_dataCritSect; // Protects data buffer
	unsigned long m_decimation[NUM_CHANS]; // Current channel decimation value
	bool m_demod[NUM_CHANS]; // Demod is active
	Units::Frequency m_demodFreq; // DDC freq (chs 0 and 1)
	CString m_driverVersion; // Driver version
	unsigned char m_firBw[NUM_CHANS]; // FIR filter bandwidth (%)
	Units::Frequency m_freq; // DDC freq (ch 2)
	unsigned short m_gcDecimation[NUM_CHANS]; // 4016 CIC decimation
	int m_intSize[NUM_CHANS]; // Interrupt block size
	bool m_invertSpectrum01; // Spectrum is inverted (chs 0 and 1)
	bool m_invertSpectrum2; // Spectrum is inverted (ch 2)
	bool m_master; // Am I the master (or only) instance across all processes?
	mutable CEvent m_moreBufferSpace; // More free space in DMA buffer;
	bool m_needGainAdjust; // GC4016 needs gain adjustment
	bool m_needRetune; // GC4016 needs a retune;
	bool m_needSync; // GC4016 needs a sync
	unsigned int m_newestInUse[NUM_DATA_CHANS]; // Sample count of newest in-use samples
	const volatile unsigned int* m_newestSample[NUM_CHANS]; // Pointers to newest samples
	CEvent m_newSamples[NUM_CHANS]; // New samples received automatic reset events
	std::deque<std::pair<unsigned int, unsigned int> > m_oldestInUse; // Sample count of oldest in-use samples
	UINT m_periodMin; // Minimum time resolution
	const int* m_pfir[NUM_CHANS];
	double m_pfirGain;
	mutable CCriticalSection m_regCritSect; // Protects registers
	unsigned int m_sampleCountOffset[NUM_CHANS]; // Sample counts for begining of sample buffers
	Units::Frequency m_sampleRate; // Sample rate
	unsigned long m_sequencerTime; // Time to spin sequencer (us)
	bool m_shift; // Spectrum is shifted for first 2 channels
	CEvent m_shutdown; // Event to shutdown thread
	HANDLE m_tempSensor; // Device handle for temperature sensor
	CWinThread* m_thread; // Thread
	std::unique_ptr<CTimer> m_timer; // Hardware timer
	HANDLE m_voltSensor; // Device handle for voltage sensor
	CSingleton<CWatchDog>* m_watchDog;
	CWatchDog::HPINGSOURCE m_watchDogSource; // Watchdog handle
	unsigned char m_wbDecimation[NUM_CHANS]; // Decimation setting for wideband DDC (log scale)
	std::vector<unsigned int> m_wrapBuffer[NUM_CHANS]; // Buffer to reassemble samples that wrap
	mutable CMutex m_mutex; // Cross-process mutex. Must be declared last so that GetLastError() can be used in the constructor body
};

// The following static functions are placed in the .h file to avoid the use of P7140.h in the Config project pulling in dependencies from P7140.cpp.
// TODO: Could also use a PIMPL implementation

/////////////////////////////////////////////////////////////////////
//
// Calculate decimations for the GC4016 and wideband DDC
//
inline void CP7140::CalcDecimations(unsigned long decimation, Units::Frequency sampleRate, _Out_ unsigned short& gcDecimation, _Out_ unsigned char& wbDecimation)
{
	unsigned short maxGcDecimation = MaxGcDecimation(sampleRate);

	if(decimation <= 4 * GC4016_DECIMATION_MIN)
	{
		// Use wideband downconverter only
		ASSERT(decimation == 2 || decimation == 4 || decimation == 8 || decimation == 16 || decimation == 32);
		gcDecimation = 0;
		wbDecimation = 0;

		while((decimation >>= 1) > 0)
		{
			++wbDecimation;
		}
	}
	else if(decimation > 4u * maxGcDecimation)
	{
		// Cascade downconverters
		ASSERT(decimation % 16 == 0);
		wbDecimation = 2; // Minimum wideband decimation of 4 because of offset 4016 tuning
		gcDecimation = unsigned short(decimation / 16);

		while(gcDecimation > maxGcDecimation)
		{
			ASSERT(wbDecimation < 6);
			ASSERT(!(gcDecimation & 1));
			++wbDecimation;
			gcDecimation >>= 1;
		}
	}
	else
	{
		// Use 4016 only
		ASSERT(decimation % 4 == 0);
		gcDecimation = unsigned short(decimation / 4);
		wbDecimation = 0;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the DDR latency in seconds
//
inline float CP7140::GetLatency(Units::Frequency sampleRate, unsigned long decimation)
{
	unsigned short gcDecimation;
	unsigned char wbDecimation;
	CalcDecimations(decimation, sampleRate, gcDecimation, wbDecimation);

	return GetLatency(gcDecimation, wbDecimation) / sampleRate.Hz<float>();
}


//////////////////////////////////////////////////////////////////////
//
// Get the DDR latency in ADC samples
//
inline unsigned long CP7140::GetLatency(unsigned short gcDecimation, unsigned char wbDecimation)
{
	unsigned long latency;

	if(wbDecimation > 0 && gcDecimation > 0)
	{
		// Cascaded
		latency = 88 * gcDecimation + 48 + 4 * gcDecimation * (28 * (1 << wbDecimation) + 64);
	}
	else if(gcDecimation > 0)
	{
		// GC4016 only
		latency = 88 * gcDecimation + 48;
	}
	else
	{
		// Wideband only
		latency = 28 * (1 << wbDecimation) + 64;
	}

	return latency;
}


//////////////////////////////////////////////////////////////////////
//
// Maximum GC4016 decimation (see p55 of GC4016 datasheet)
//
inline unsigned short CP7140::MaxGcDecimation(Units::Frequency sampleRate)
{
	return std::min(unsigned short(GC4016_DECIMATION_MAX), (sampleRate / 40000).Hz<unsigned short>());
}

