/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include <math.h>

#include "DownConverter.h"
#include "Units.h"
#include "Vme.h"

class C26122001
:
	// Inheritance
	public CDownConverter, 
	protected virtual CVme
{
	// Friends
	friend class C26122002;

public:
	// Constants
	static const unsigned char ATTEN_STEP = 2; // dB
	static const unsigned char MAX_ATTEN = 84; // dB
	static const signed char MAX_GAIN = 70; // dB
	static const unsigned char MIN_CONGESTED_ATTEN = 10; // dB
	static const unsigned long NARROW_BW_HZ = 2000000; // Hz
	static const unsigned long RF_HIGH_FREQ_HZ = 3000000000; // Hz
	static const unsigned long RF_LOW_FREQ_HZ = 20000000; // Hz
	static const unsigned long WIDE_BW_HZ = 20000000; // Hz

protected:
	// Constants
	static const SBandSettings BAND_SETTINGS_[];
	static const float NOMINAL_TEMP; // �C

	// Functions
	C26122001(void); // Can only be constructed by a derived class
	template<size_t N> C26122001(const CDownConverter::EGainMode (&gainModes)[N]);
	virtual ~C26122001(void);
	unsigned char GetAtten(unsigned char id) const { return m_atten[id]; }
	unsigned short GetAttenRaw(unsigned char id) const { return Peek(id, ATTEN_REG); }
	static size_t GetBand(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ Units::FreqPair& freqLimits);
	bool GetDetector(unsigned char id) const;
	static float GetGainAdjustmentDb(float temp) { return (temp - NOMINAL_TEMP) * GAIN_SLOPE; }
	CDownConverter::EGainMode GetGainMode(Units::Frequency freq) const { return CDownConverter::GetGainMode(freq, BAND_SETTINGS); }
	float GetGainRatio(float temp, unsigned char id) const;
	unsigned short GetSwitchReg(unsigned char id) const { return Peek(id, SWITCH_REG); }
	unsigned char GetVersion(unsigned char id) const;
	bool IsPresent(unsigned char id) const { return m_present[id]; }
	bool IsWideband(unsigned char id) const { return m_wideband[id]; }
	unsigned char MinAtten(CDownConverter::EGainMode mode) const { return mode == CDownConverter::CONGESTED ? MIN_CONGESTED_ATTEN : 0; }
	unsigned char MinAtten(Units::Frequency freq) const { return MinAtten(GetGainMode(freq)); }
	unsigned short Peek(unsigned char id, unsigned char reg) const { return CVme::Peek(m_address[id] + reg); }
	void Poke(unsigned char id, unsigned char reg, unsigned short data) { CVme::Poke(m_address[id] + reg, data); }
	unsigned char SetAtten(unsigned char atten, Units::Frequency freq, signed char idOrAll = -1);
	unsigned char SetAtten(unsigned char atten, CDownConverter::EGainMode mode, signed char idOrAll = -1);
	void SetAttenRaw(unsigned short bits, signed char idOrAll = -1);
	void SetBand(unsigned char band, signed char idOrAll = -1);
	unsigned long SetCalTone(bool on, signed char idOrAll = -1);
	void SetDetector(bool on, signed char idOrAll = -1);
	void SetInput(unsigned char input, signed char idOrAll = -1);
	void SetWideBw(bool wide, signed char idOrAll = -1);
	void Tune(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ Units::FreqPair& freqLimits,
		_Out_ size_t& band, signed char idOrAll = -1);

private:
	// Constants
	static const BandSettings BAND_SETTINGS;
	static const unsigned int CARD_CLASS = 0xc;
	static const unsigned int CARD_TYPE_NARROW = 0x2;
	static const unsigned int CARD_TYPE_WIDE = 0x1;
	static const unsigned int MAX_INSTANCES = 3;
	static const unsigned int VME_BASE_ADDR_NARROW = (0x770000 | (CARD_CLASS << 12) | (CARD_TYPE_NARROW << 8));
	static const unsigned int VME_BASE_ADDR_WIDE = (0x770000 | (CARD_CLASS << 12) | (CARD_TYPE_WIDE << 8));
	static const unsigned int STATUS_REG = 0x00;
	static const unsigned int SWITCH_REG = 0x02;
	static const unsigned int ATTEN_REG = 0x04;
	static const float GAIN_SLOPE; // dB / �C
	static LPCTSTR MUTEX_NAME;

	// Functions
	_Ret_ virtual const CDownConverter::SReceiverGainModes& GetGainModes(void) const { return m_gainModes; }
	virtual void SetSettleTime(unsigned int) {}

	// Data
	unsigned long m_address[MAX_INSTANCES];
	unsigned char m_atten[MAX_INSTANCES];
	unsigned short m_attenReg[MAX_INSTANCES];
	bool m_first;
	CDownConverter::SReceiverGainModes m_gainModes;
	mutable CMutex m_mutex;
	bool m_present[MAX_INSTANCES];
	unsigned short m_switchReg[MAX_INSTANCES];
	bool m_wideband[MAX_INSTANCES];
};

// Use selectany to put this here rather than in the .cpp so the size of the array can be determined by users of the class
__declspec(selectany) const CDownConverter::SBandSettings C26122001::BAND_SETTINGS_[] =
{ // low 20 dB, low 1 dB, high 1 dB, high 20 dB, data
	{    7000000,   14000000,   47000000,   66000000, 0x0000 },
	{   24000000,   35000000,   87000000,  138000000, 0x0008 },
	{   46000000,   71000000,  150000000,  221000000, 0x0010 },
	{  111000000,  130000000,  270000000,  409000000, 0x0018 },
	{  162000000,  250000000,  410000000,  538000000, 0x0020 },
	{  328000000,  390000000,  610000000,  682000000, 0x0028 },
	{  536000000,  590000000,  970000000, 1132000000, 0x0002 },
	{  845000000,  950000000, 1410000000, 1539000000, 0x000a },
	{ 1139000000, 1390000000, 1710000000, 1970000000, 0x0004 },
	{ 1278000000, 1690000000, 2110000000, 2416000000, 0x000c },
	{ 1628000000, 2090000000, 2510000000, 2816000000, 0x0014 },
	{ 1970000000, 2490000000, 3010000000, 3381000000, 0x001c }
};


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
template<size_t N> C26122001::C26122001(const CDownConverter::EGainMode (&gainModes)[N])
:
	CVme(),
	m_gainModes(&gainModes[0], &gainModes[_countof(BAND_SETTINGS_)]),
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get())
{
	static_assert(N >= _countof(BAND_SETTINGS_), "Not enough values in gainModes array");
	m_first = !(GetLastError() == ERROR_ALREADY_EXISTS); // Status from m_mutex construction

	for(unsigned char id = 0; id < MAX_INSTANCES; ++id)
	{
		m_address[id] = VME_BASE_ADDR_WIDE + 32 * id;
		m_present[id] = ((Peek(id, STATUS_REG) & 0x00ffu) == (((CARD_CLASS & 0x3) << 6) | ((CARD_TYPE_WIDE & 0x7) << 3) | id));
		m_wideband[id] = true;

		if(!m_present[id])
		{
			m_address[id] = VME_BASE_ADDR_NARROW + 32 * id;
			m_present[id] = ((Peek(id, STATUS_REG) & 0x00ffu) == (((CARD_CLASS & 0x3) << 6) | ((CARD_TYPE_NARROW & 0x7) << 3) | id));
			m_wideband[id] = false;
		}

		if(m_present[id])
		{
			if(m_first)
			{
				m_attenReg[id] = 0x0fff;
				Poke(id, ATTEN_REG, m_attenReg[id]);
				m_switchReg[id] = 0x0000;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetAtten(MAX_ATTEN, 98500000, id);
				SetInput(0, id);
				SetCalTone(false, id);
				SetDetector(false, id);
				Units::FreqPair freqLimits;
				size_t band;
				Tune(98500000, NARROW_BW_HZ, OPTIMUM, freqLimits, band, id);
			}
			else
			{
				m_attenReg[id] = Peek(id, ATTEN_REG);
				m_switchReg[id] = Peek(id, SWITCH_REG);
			}
		}
	}

	return;
}
