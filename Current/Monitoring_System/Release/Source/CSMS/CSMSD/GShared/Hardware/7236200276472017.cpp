/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2009 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#include "StdAfx.h"

#include "7236200276472017.h"
#include "80682017.h"
#include "Timer.h"


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C7236200276342017::C7236200276342017(EPoePort poePort)
:
	m_digitizer(),
	m_validateFailCount(0)
{
	m_controlBytes[0] = 0xff;
	m_controlBytes[1] = 0xff;
	m_controlBytes[2] = 0xff;

	switch(poePort)
	{
	case UHF:
		m_poePortBits = CDigitizer::B_UHF_SELECT;
		break;

	case VHF:
		m_poePortBits = CDigitizer::B_VHF_SELECT;
		break;

	case HF:
		m_poePortBits = CDigitizer::B_HF_SELECT;
		break;

	default:
		ASSERT(FALSE);
		m_poePortBits = CDigitizer::B_HF_SELECT;
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the switch by programming the LVDS bus
//
void C7236200276342017::SetSwitch(_In_reads_(3) const unsigned char (&controlBytes)[3], bool force)
{
	CSingleLock lock(&m_critSect, TRUE);

	// This logic ensures that the steering bits are manipulated in a grey code sequence and the data is stable
	// while the steering bits change, otherwise bad things happen in the hardware
	if(!force && controlBytes[0] != m_controlBytes[0] && controlBytes[1] == m_controlBytes[1] && controlBytes[2] == m_controlBytes[2])
	{
		// Set register 1 only
		m_digitizer->SetDataOut(controlBytes[0]);
		m_digitizer->SetDataOut(m_poePortBits | controlBytes[0]);
		m_digitizer->SetDataOut(m_poePortBits | 0x0040 | controlBytes[0]);
		CTimer(CTimer::SHORT).Delay(5);
		m_digitizer->SetDataOut(m_poePortBits | controlBytes[0]);
		m_digitizer->SetDataOut(controlBytes[0]);
		m_digitizer->SetDataOut(0x0000);
		m_controlBytes[0] = controlBytes[0];
	}
	else if(!force && controlBytes[0] == m_controlBytes[0] && controlBytes[1] != m_controlBytes[1] && controlBytes[2] == m_controlBytes[2])
	{
		// Set register 2 only
		m_digitizer->SetDataOut(controlBytes[1]);
		m_digitizer->SetDataOut(m_poePortBits | controlBytes[1]);
		m_digitizer->SetDataOut(m_poePortBits | 0x0080 | controlBytes[1]);
		CTimer(CTimer::SHORT).Delay(5);
		m_digitizer->SetDataOut(m_poePortBits | controlBytes[1]);
		m_digitizer->SetDataOut(controlBytes[1]);
		m_digitizer->SetDataOut(0x0000);
		m_controlBytes[1] = controlBytes[1];
	}
	else if(!force && controlBytes[0] != m_controlBytes[0] && controlBytes[1] != m_controlBytes[1] && controlBytes[2] == m_controlBytes[2])
	{
		// Set registers 1 and 2 only
		CTimer timer(CTimer::SHORT);
		m_digitizer->SetDataOut(controlBytes[0]);
		m_digitizer->SetDataOut(m_poePortBits | controlBytes[0]);
		m_digitizer->SetDataOut(m_poePortBits | 0x0040 | controlBytes[0]);
		timer.Delay(5);
		m_digitizer->SetDataOut(m_poePortBits | controlBytes[0]);
		m_digitizer->SetDataOut(m_poePortBits | controlBytes[1]);
		m_digitizer->SetDataOut(m_poePortBits | 0x0080 | controlBytes[1]);
		timer.Delay(5);
		m_digitizer->SetDataOut(m_poePortBits | controlBytes[1]);
		m_digitizer->SetDataOut(controlBytes[1]);
		m_digitizer->SetDataOut(0x0000);
		m_controlBytes[0] = controlBytes[0];
		m_controlBytes[1] = controlBytes[1];
	}
	else if(force || controlBytes[0] != m_controlBytes[0] || controlBytes[1] != m_controlBytes[1] || controlBytes[2] != m_controlBytes[2])
	{
		// All other cases - use grey code sequence
		CTimer timer(CTimer::SHORT);

		// Use this order so that the control bits used by the 647 block downconverter synthesizer are not glitched
		m_digitizer->SetDataOut(controlBytes[1]);
		m_digitizer->SetDataOut(m_poePortBits | controlBytes[1]);
		m_digitizer->SetDataOut(m_poePortBits | 0x0080 | controlBytes[1]);
		timer.Delay(5);
		m_digitizer->SetDataOut(m_poePortBits | 0x00c0 | controlBytes[1]);
		m_digitizer->SetDataOut(m_poePortBits | 0x00c0 | controlBytes[2]);
		timer.Delay(5);
		m_digitizer->SetDataOut(m_poePortBits | 0x0040 | controlBytes[2]);
		m_digitizer->SetDataOut(m_poePortBits | 0x0040 | controlBytes[0]);
		timer.Delay(5);
		m_digitizer->SetDataOut(m_poePortBits | controlBytes[0]);
		m_digitizer->SetDataOut(controlBytes[0]);
		m_digitizer->SetDataOut(0x0000);
		m_controlBytes[0] = controlBytes[0];
		m_controlBytes[1] = controlBytes[1];
		m_controlBytes[2] = controlBytes[2];
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate loopback data against local state
//
void C7236200276342017::ValidateLoopback(unsigned int loopback)
{
	CSingleLock lock(&m_critSect, TRUE);

	// Don't test byte 0 as that's the commutating sample antenna
	if(m_controlBytes[1] != ((loopback & 0x00fc0) >> 6) || m_controlBytes[2] != ((loopback & 0x3f000) >> 12))
	{
		if(++m_validateFailCount >= 10)
		{
			// Force switch control
			TRACE(_T("72362001 or 7234 control reset\n"));
			C80682017::WriteShMemRegs();
			SetSwitch(const_cast<unsigned char(&)[3]>(m_controlBytes), true);
			m_validateFailCount = 0;
		}
	}
	else
	{
		m_validateFailCount = 0;
	}

	return;
}

