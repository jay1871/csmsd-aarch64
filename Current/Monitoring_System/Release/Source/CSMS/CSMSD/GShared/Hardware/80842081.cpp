/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "StdAfx.h"

#include <memory>

#include "80842081.h"
#include "80842082.h"
#include "DownConverter.h"
#include "Units.h"
#include "Vme.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
//
// Static data
//
const CDownConverter::BandSettings C80842081::BAND_SETTINGS(BAND_SETTINGS_, &BAND_SETTINGS_[_countof(BAND_SETTINGS_)]);
LPCTSTR C80842081::MUTEX_NAME = _T("Global\\TCI.{1883F919-8966-41c2-BF08-071AEC180B1F}");


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C80842081::C80842081(void)
:
	CVme(),
	m_gainModes(),
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get())
{
	m_first = !(GetLastError() == ERROR_ALREADY_EXISTS); // Status from m_mutex construction

	for(unsigned char id = 0; id < MAX_INSTANCES; ++id)
	{
		SCardStatus status = GetCardStatus(id, true);
		m_present[id] = (status.cardClass == (CARD_CLASS & 0x3) && status.cardType == CARD_TYPE && status.id == id);

		if(m_present[id])
		{
			if(m_first)
			{
				SetAtten(MAX_ATTEN, 30000000, id);
				SetBlanking(true, id);
				SetBypass(false, id);
				SetBiteComb(false, id);
				SetCalRelay(false, id);
				SetDetector(false, id);
				bool bypass;
				Units::FreqPair freqLimits;
				size_t band;
				Tune(30000000, NARROW_BW_HZ, OPTIMUM, bypass, freqLimits, band, id);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C80842081::~C80842081(void)
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the closest calibration frequency in same band
//
Units::Frequency C80842081::GetCalFreq(Units::Frequency freq)
{
	ASSERT(_countof(BAND_SETTINGS_) > 1);
	size_t band;

	for(band = 0; band < _countof(BAND_SETTINGS_) - 1; ++band)
	{
		if(freq < (BAND_SETTINGS_[band].high1dbFreq + BAND_SETTINGS_[band + 1].low1dbFreq) / 2)
		{
			break;
		}
	}

	if(band == 0)
	{
		// Bypass - no cal available
		return 0;
	}
	else if(band == _countof(BAND_SETTINGS_) - 1)
	{
		return GetCalFreq(freq,
			(BAND_SETTINGS_[band - 1].high1dbFreq + BAND_SETTINGS_[band].low1dbFreq) / 2,
			BAND_SETTINGS_[band].high1dbFreq - 1);
	}
	else
	{
		return GetCalFreq(freq,
			(BAND_SETTINGS_[band - 1].high1dbFreq + BAND_SETTINGS_[band].low1dbFreq) / 2,
			(BAND_SETTINGS_[band].high1dbFreq + BAND_SETTINGS_[band + 1].low1dbFreq) / 2 - 1);
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the closest calibration frequency in a range
//
Units::Frequency C80842081::GetCalFreq(Units::Frequency freq, Units::Frequency loFreq, Units::Frequency hiFreq)
{
	Units::Frequency calFreq = (freq.Hz<unsigned long>() + 500000) / 1000000 * 1000000; // Round to nearest MHz

	while(calFreq > hiFreq || calFreq > CAL_HIGH_FREQ_HZ)
	{
		calFreq -= 1000000;
	}

	while(calFreq < loFreq || calFreq < CAL_LOW_FREQ_HZ)
	{
		calFreq += 1000000;
	}

	return calFreq;
}


//////////////////////////////////////////////////////////////////////
//
// Get the card status
//
C80842081::SCardStatus C80842081::GetCardStatus(unsigned char id, bool force) const
{
	unsigned short reg = 0;

	if(force || m_present[id])
	{
		reg = Peek(id, STATUS_REG);
	}

	union
	{
		unsigned short reg;
		SCardStatus status;
	} status = { reg };

	return status.status;
}


//////////////////////////////////////////////////////////////////////
//
// Get the receiver gain
//
float C80842081::GetGainRatio(unsigned char id) const
{
	return pow(10.0f, (MAX_GAIN - m_atten[id] + GetGainAdjustmentDb()) / 10.0f);
}


//////////////////////////////////////////////////////////////////////
//
// Get the receiver gain adjustment for temperature
//
float C80842081::GetGainAdjustmentDb(void) const
{
	switch(GetCardStatus(0).temp)
	{
	case 0:
		return 0.35f;

	case 1:
		return 0.00f;

	case 3:
		return -0.35f;

	case 7:
		return -0.70f;

	case 15:
		return -1.05f;

	default:
		return 0.00f;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the board version
//
unsigned char C80842081::GetVersion(unsigned char id) const
{
	return GetCardStatus(id).version;
}


//////////////////////////////////////////////////////////////////////
//
// Set the attenuation
//
unsigned char C80842081::SetAtten(unsigned char atten, Units::Frequency freq, signed char idOrAll)
{
	return SetAtten(atten, GetGainMode(freq, BAND_SETTINGS), idOrAll);
}


//////////////////////////////////////////////////////////////////////
//
// Set the attenuation
//
unsigned char C80842081::SetAtten(unsigned char atten, CDownConverter::EGainMode mode, signed char idOrAll)
{
	static const unsigned short ATTEN_DATA[][2] = 
	{	// Rural, urban
		{ 0x0000, 0x0000 },
		{ 0x0001, 0x0001 },
		{ 0x0002, 0x0002 },
		{ 0x0003, 0x0003 },
		{ 0x0004, 0x0004 },
		{ 0x0005, 0x0005 },
		{ 0x0006, 0x0103 },
		{ 0x0007, 0x0104 },
		{ 0x0105, 0x0105 },
		{ 0x0106, 0x0106 },
		{ 0x0107, 0x0107 },
		{ 0x0108, 0x0108 },
		{ 0x0118, 0x0206 },
		{ 0x0128, 0x0207 },
		{ 0x0138, 0x0208 },
		{ 0x0148, 0x0218 },
		{ 0x0228, 0x0228 },
		{ 0x0238, 0x0238 },
		{ 0x0248, 0x0318 },
		{ 0x0258, 0x0328 },
		{ 0x0268, 0x0338 },
		{ 0x0278, 0x0348 },
		{ 0x0288, 0x0358 },
		{ 0x0289, 0x0368 },
		{ 0x0378, 0x0378 },
		{ 0x0388, 0x0388 },
		{ 0x0389, 0x0389 },
		{ 0x0399, 0x0399 },
		{ 0x039a, 0x039a },
		{ 0x03aa, 0x03aa },
		{ 0x03ab, 0x03ab },
		{ 0x03bb, 0x03bb },
		{ 0x03bc, 0x03bc },
		{ 0x03cc, 0x03cc },
		{ 0x03cd, 0x03cd },
		{ 0x03dd, 0x03dd },
		{ 0x03de, 0x03de },
		{ 0x03ee, 0x03ee },
		{ 0x03ef, 0x03ef },
		{ 0x03ff, 0x03ff },
	};

	if(mode == CDownConverter::CONGESTED)
	{
		// Congested is just urban with a minimum attenuation of 12 dB
		mode = CDownConverter::URBAN;

		if(atten < 12)
		{
			atten = 12;
		}
	}

	ASSERT(mode == CDownConverter::RURAL || mode == CDownConverter::URBAN);

	if(mode > CDownConverter::URBAN)
	{
		mode = CDownConverter::URBAN;
	}

	if(atten / 2 >= _countof(ATTEN_DATA))
	{
		atten = 2 * _countof(ATTEN_DATA) - 2;
	}

	unsigned short data = ATTEN_DATA[atten / 2][mode];

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			unsigned short reg = (Peek(id, ATTEN_REG) & 0x03ff);

			if((reg & 0x03ff) != data)
			{
				Poke(id, ATTEN_REG, data);
				unsigned int settleTime = 5;

				if((reg ^ data) & 0x0300)
				{
					settleTime = 55;

					if(Peek(id, MODE_REG) & 0x0002)
					{
						settleTime += 15;
					}
				}

				SetSettleTime(settleTime);
			}

			m_atten[id] = atten / 2 * 2;
		}
	}

	return m_atten[idOrAll == -1 ? 0 : idOrAll];
}


//////////////////////////////////////////////////////////////////////
//
// Set the attenuation
//
void C80842081::SetAttenRaw(unsigned short bits, signed char idOrAll)
{
	bits &= 0x03ff;

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if((Peek(id, ATTEN_REG) & 0x03ff) != bits)
			{
				Poke(id, ATTEN_REG, bits);
				SetSettleTime(100);
				m_atten[id] = 0xff;
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the RF band
//
void C80842081::SetBand(unsigned char band, signed char idOrAll)
{
	ASSERT(band < _countof(BAND_SETTINGS_) - 1);

	if(band  < _countof(BAND_SETTINGS_) - 1)
	{
		unsigned short data = BAND_SETTINGS_[band + 1].data; // Band 0 is bypass

		for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? int(MAX_INSTANCES) : idOrAll + 1); ++id)
		{
			if(m_present[id])
			{
				unsigned short reg = Peek(id, MODE_REG);

				if((reg & 0x001c) != data)
				{
					unsigned int settleTime = 105;

					if(reg & 0x0002)
					{
						settleTime += 15;
					}

					reg &= 0x01e3;
					reg |= data;
					Poke(id, MODE_REG, reg);
					SetSettleTime(settleTime);
				}
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Turn the BITE comb generator on or off
//
void C80842081::SetBiteComb(bool on, signed char idOrAll)
{
	unsigned short data = (on ? 0x0020 : 0x0000);

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			unsigned short reg = Peek(id, MODE_REG);

			if((reg & 0x0020) != data)
			{
				reg &= 0x01df;
				reg |= data;
				Poke(id, MODE_REG, reg);
				SetSettleTime(5);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Blank the input
//
void C80842081::SetBlanking(bool on, signed char idOrAll)
{
	unsigned short data = (on ? 0x0080 : 0x0000);

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			unsigned short reg = Peek(id, MODE_REG);

			if((reg & 0x0080) != data)
			{
				unsigned int settleTime = 105;

				if(reg & 0x0002)
				{
					settleTime += 15;
				}

				reg &= 0x017f;
				reg |= data;
				Poke(id, MODE_REG, reg);
				SetSettleTime(settleTime);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Put in bypass mode
//
void C80842081::SetBypass(bool on, signed char idOrAll)
{
	unsigned short data = (on ? 0x0100 : 0x0000);

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			unsigned short reg = Peek(id, MODE_REG);

			if((reg & 0x0100) != data)
			{
				unsigned int settleTime = 10;

				if(!on && (reg & 0x0002))
				{
					settleTime += 15;
				}

				reg &= 0x00ff;
				reg |= data;
				Poke(id, MODE_REG, reg);
				SetSettleTime(settleTime);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Turn the cal relay on or off
//
void C80842081::SetCalRelay(bool on, signed char idOrAll)
{
	unsigned short data = (on ? 0x0040 : 0x0000);

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			unsigned short reg = Peek(id, MODE_REG);

			if((reg & 0x0040) != data)
			{
				reg &= 0x01bf;
				reg |= data;
				Poke(id, MODE_REG, reg);
				SetSettleTime(10000);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Turn the detector on or off
//
void C80842081::SetDetector(bool on, signed char idOrAll)
{
	unsigned short data = (on ? 0x0001 : 0x0000);
	unsigned int settleTime = 0;

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			unsigned short reg = Peek(id, MODE_REG);

			if((reg & 0x0001) != data)
			{
				reg &= 0x01fe;
				reg |= data;
				Poke(id, MODE_REG, reg);
				settleTime = (on ? 1000 : 5);
			}
		}
	}

	if(on)
	{
		settleTime += 500;
	}

	SetSettleTime(settleTime);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set wide bandwidth
//
void C80842081::SetWideBw(bool wide, signed char idOrAll)
{
	unsigned short data = (wide ? 0x0000 : 0x0002);

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			unsigned short reg = Peek(id, MODE_REG);

			if((reg & 0x0002) != data)
			{
				reg &= 0x01fd;
				reg |= data;
				Poke(id, MODE_REG, reg);
				SetSettleTime(5);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the signal routing
//
void C80842081::Tune(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ bool& bypass, _Out_ Units::FreqPair& freqLimits,
								_Out_ size_t& band, signed char idOrAll)
{
	ASSERT(freq <= RF_HIGH_FREQ_HZ + rxBw / 2);
	band = GetBand(freq, rxBw, bandSelect, freqLimits);
	unsigned short data = BAND_SETTINGS_[band].data;
	bypass = ((data & 0x0100) != 0);

	if(rxBw <= NARROW_BW_HZ)
	{
		data |= 0x0002;
	}

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? int(MAX_INSTANCES) : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			unsigned short reg = Peek(id, MODE_REG);

			if((reg & 0x011e) != data)
			{
				unsigned int settleTime = 105;

				if(reg & 0x0002)
				{
					settleTime += 15;
				}

				reg &= 0x00e1;
				reg |= data;
				Poke(id, MODE_REG, reg);
				SetSettleTime(settleTime);
			}
		}
	}

	return;
}
