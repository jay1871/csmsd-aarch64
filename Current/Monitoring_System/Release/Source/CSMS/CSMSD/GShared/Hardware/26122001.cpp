/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "StdAfx.h"

#include <algorithm>
#include <memory>

#include "26122001.h"
#include "26122002.h"
#include "80682017.h"
#include "Units.h"
#include "Vme.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
//
// Static data
//
const CDownConverter::BandSettings C26122001::BAND_SETTINGS(BAND_SETTINGS_, &BAND_SETTINGS_[_countof(BAND_SETTINGS_)]);
const float C26122001::GAIN_SLOPE = -0.09f; // dB / �C
LPCTSTR C26122001::MUTEX_NAME = _T("Global\\TCI.{08725070-8C75-41de-91BD-4E730AED8003}");
const float C26122001::NOMINAL_TEMP = 25; // �C

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C26122001::C26122001(void)
:
	CVme(),
	m_gainModes(),
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get())
{
	m_first = !(GetLastError() == ERROR_ALREADY_EXISTS); // Status from m_mutex construction

	for(unsigned char id = 0; id < MAX_INSTANCES; ++id)
	{
		m_address[id] = VME_BASE_ADDR_WIDE + 32 * id;
		m_present[id] = ((Peek(id, STATUS_REG) & 0x00ffu) == (((CARD_CLASS & 0x3) << 6) | ((CARD_TYPE_WIDE & 0x7) << 3) | id));
		m_wideband[id] = true;

		if(!m_present[id])
		{
			m_address[id] = VME_BASE_ADDR_NARROW + 32 * id;
			m_present[id] = ((Peek(id, STATUS_REG) & 0x00ffu) == (((CARD_CLASS & 0x3) << 6) | ((CARD_TYPE_NARROW & 0x7) << 3) | id));
			m_wideband[id] = false;
		}

		if(m_present[id])
		{
			if(m_first)
			{
				m_attenReg[id] = 0x0fff;
				Poke(id, ATTEN_REG, m_attenReg[id]);
				m_switchReg[id] = 0x0000;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetAtten(MAX_ATTEN, 98500000, id);
				SetInput(0, id);
				SetCalTone(false, id);
				SetDetector(false, id);
				Units::FreqPair freqLimits;
				size_t band;
				Tune(98500000, NARROW_BW_HZ, OPTIMUM, freqLimits, band, id);
			}
			else
			{
				m_attenReg[id] = Peek(id, ATTEN_REG);
				m_switchReg[id] = Peek(id, SWITCH_REG);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C26122001::~C26122001(void)
{
	for(signed char id = 0; id < MAX_INSTANCES; ++id)
	{
		if(m_present[id] && m_first)
		{
			SetAtten(84, id);
			SetInput(3, id);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the band
//
size_t C26122001::GetBand(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ Units::FreqPair& freqLimits)
{
	// Round freq like the synth does so we stay in sync
	Units::Frequency roundedFreq = C26122002::MIN_TUNE_STEP * C26122002::GetTuneStepNumber(freq);

	// 600 MHz and 1400 MHz breaks are special
	if(roundedFreq > C26122002::FIRST_IF_BAND_BREAK_FREQ1_HZ - WIDE_BW_HZ && roundedFreq < C26122002::FIRST_IF_BAND_BREAK_FREQ1_HZ)
	{
		freqLimits = Units::FreqPair(freq - rxBw / 2, freq + rxBw / 2);
		return 5;
	}
	else if(roundedFreq >= C26122002::FIRST_IF_BAND_BREAK_FREQ1_HZ && roundedFreq < C26122002::FIRST_IF_BAND_BREAK_FREQ1_HZ + WIDE_BW_HZ)
	{
		freqLimits = Units::FreqPair(freq - rxBw / 2, freq + rxBw / 2);
		return 6;
	}
	else if(roundedFreq > C26122002::FIRST_IF_BAND_BREAK_FREQ2_HZ - WIDE_BW_HZ && roundedFreq < C26122002::FIRST_IF_BAND_BREAK_FREQ2_HZ)
	{
		freqLimits = Units::FreqPair(freq - rxBw / 2, freq + rxBw / 2);
		return 7;
	}
	else if(roundedFreq >= C26122002::FIRST_IF_BAND_BREAK_FREQ2_HZ && roundedFreq < C26122002::FIRST_IF_BAND_BREAK_FREQ2_HZ + WIDE_BW_HZ)
	{
		freqLimits = Units::FreqPair(freq - rxBw / 2, freq + rxBw / 2);
		return 8;
	}
	else
	{
		// Default behavior
		return CDownConverter::GetBand(freq, rxBw, bandSelect, freqLimits, BAND_SETTINGS);
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the detector state
//
bool C26122001::GetDetector(unsigned char id) const
{
	return (Peek(id, STATUS_REG) & 0x0100) != 0x0000;
}


//////////////////////////////////////////////////////////////////////
//
// Get the gain compensated for temperature (ratio)
//
float C26122001::GetGainRatio(float temp, unsigned char id) const
{
	return pow(10.0f, (MAX_GAIN - m_atten[id] + GetGainAdjustmentDb(temp)) / 10.0f);
}


//////////////////////////////////////////////////////////////////////
//
// Get the board version
//
unsigned char C26122001::GetVersion(unsigned char id) const
{
	return unsigned char((Peek(id, STATUS_REG) & 0xe000u) >> 13);
}


//////////////////////////////////////////////////////////////////////
//
// Set the attenuation
//
unsigned char C26122001::SetAtten(unsigned char atten, Units::Frequency freq, signed char idOrAll)
{
	return SetAtten(atten, GetGainMode(freq), idOrAll);
}


//////////////////////////////////////////////////////////////////////
//
// Set the attenuation
//
	unsigned char C26122001::SetAtten(unsigned char atten, CDownConverter::EGainMode mode, signed char idOrAll)
{
	static const unsigned short ATTEN_DATA[][3] = 
	{	// Rural, urban, congested
		{ 0x0000, 0x0000, 0x000a },
		{ 0x0100, 0x0100, 0x000a },
		{ 0x0200, 0x0200, 0x000a },
		{ 0x0300, 0x0300, 0x000a },
		{ 0x0400, 0x0400, 0x000a },
		{ 0x0500, 0x0500, 0x000a },
		{ 0x0600, 0x0600, 0x010a },
		{ 0x0700, 0x0700, 0x020a },
		{ 0x0800, 0x0800, 0x030a },
		{ 0x0900, 0x0900, 0x040a },
		{ 0x0a00, 0x0902, 0x040c },
		{ 0x0b00, 0x0a02, 0x050c },
		{ 0x0c00, 0x0a04, 0x0603 },
		{ 0x0d00, 0x0b04, 0x0703 },
		{ 0x0e00, 0x0b06, 0x0705 },
		{ 0x0f00, 0x0c06, 0x0707 },
		{ 0x0f02, 0x0c08, 0x0807 },
		{ 0x0f22, 0x0d08, 0x0809 },
		{ 0x0f24, 0x0d0a, 0x0909 },
		{ 0x0f44, 0x0e0a, 0x090b },
		{ 0x0f64, 0x0f0a, 0x0a0b },
		{ 0x0f66, 0x0f0c, 0x0a0d },
		{ 0x0f86, 0x0f0e, 0x0b0d },
		{ 0x0fa6, 0x0f07, 0x0b0f },
		{ 0x0fa8, 0x0f27, 0x0c0f },
		{ 0x0fc8, 0x0f29, 0x0c11 },
		{ 0x0fe8, 0x0f49, 0x0d11 },
		{ 0x0fea, 0x0f4b, 0x0d13 },
		{ 0x0fec, 0x0f6b, 0x0e13 },
		{ 0x0fee, 0x0f6d, 0x0e15 },
		{ 0x0fe7, 0x0f8d, 0x0f15 },
		{ 0x0fe9, 0x0f8f, 0x0f17 },
		{ 0x0feb, 0x0faf, 0x0f19 },
		{ 0x0fed, 0x0fb1, 0x0f39 },
		{ 0x0fef, 0x0fd1, 0x0f59 },
		{ 0x0ff1, 0x0fd3, 0x0f79 },
		{ 0x0ff3, 0x0ff3, 0x0f7b },
		{ 0x0ff5, 0x0ff5, 0x0f9b },
		{ 0x0ff7, 0x0ff7, 0x0f9d },
		{ 0x0ff9, 0x0ff9, 0x0fbd },
		{ 0x0ffb, 0x0ffb, 0x0fdd },
		{ 0x0ffd, 0x0ffd, 0x0ffd },
		{ 0x0fff, 0x0fff, 0x0fff }
	};

	if(mode > CDownConverter::CONGESTED)
	{
		mode = CDownConverter::CONGESTED;
	}

	if(mode == CDownConverter::CONGESTED && atten < 10)
	{
		atten = 10;
	}

	if(atten / 2 >= _countof(ATTEN_DATA))
	{
		atten = 2 * _countof(ATTEN_DATA) - 2;
	}

	unsigned short data = ATTEN_DATA[atten / 2][mode];

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if(m_attenReg[id] != data)
			{
				m_attenReg[id] = data;
				Poke(id, ATTEN_REG, m_attenReg[id]);
				SetSettleTime(100);
			}

			m_atten[id] = atten / 2 * 2;
		}
	}

	return m_atten[idOrAll == -1 ? 0 : idOrAll];
}


//////////////////////////////////////////////////////////////////////
//
// Set the attenuation
//
void C26122001::SetAttenRaw(unsigned short bits, signed char idOrAll)
{
	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if(m_attenReg[id] != bits)
			{
				m_attenReg[id] = bits;
				Poke(id, ATTEN_REG, m_attenReg[id]);
				SetSettleTime(5);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the RF band
//
void C26122001::SetBand(unsigned char band, signed char idOrAll)
{
	unsigned short data = BAND_SETTINGS_[band].data;

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? int(MAX_INSTANCES) : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if((m_switchReg[id] & 0x003eu) != data)
			{
				m_switchReg[id] &= 0x03c1u;
				m_switchReg[id] |= data;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetSettleTime(5);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Turn the cal tone on or off (returns frequency)
//
unsigned long C26122001::SetCalTone(bool on, signed char idOrAll)
{
	unsigned short data = (on ? 0x0100 : 0x0000);

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if((m_switchReg[id] & 0x0100) != data)
			{
				m_switchReg[id] &= 0x02ff;
				m_switchReg[id] |= data;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetSettleTime(on ? 2000 : 200);
			}
		}
	}

	return 25000000;
}


//////////////////////////////////////////////////////////////////////
//
// Turn the detector on or off
//
void C26122001::SetDetector(bool on, signed char idOrAll)
{
	unsigned short data = (on ? 0x0200 : 0x0000);

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if((m_switchReg[id] & 0x0200) != data)
			{
				m_switchReg[id] &= 0x01ff;
				m_switchReg[id] |= data;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetSettleTime(200);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the RF input
//
void C26122001::SetInput(unsigned char input, signed char idOrAll)
{
	unsigned short data = (input << 6);

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if((m_switchReg[id] & 0x00c0) != data)
			{
				m_switchReg[id] &= 0x033f;
				m_switchReg[id] |= data;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetSettleTime(5);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set wide bandwidth
//
void C26122001::SetWideBw(bool wide, signed char idOrAll)
{
	unsigned short data = (wide ? 0x0000 : 0x0001);

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if((m_switchReg[id] & 0x0001) != data)
			{
				m_switchReg[id] &= 0x03fe;
				m_switchReg[id] |= data;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetSettleTime(5);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the signal routing
//
void C26122001::Tune(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ Units::FreqPair& freqLimits,
					 _Out_ size_t& band, signed char idOrAll)
{
	ASSERT(freq <= RF_HIGH_FREQ_HZ + WIDE_BW_HZ / 2); // VCP allows tuning down to DC
	band = GetBand(freq, rxBw, bandSelect, freqLimits);
	unsigned short data = BAND_SETTINGS_[band].data;

	if(rxBw <= NARROW_BW_HZ)
	{
		data |= 0x0001;
	}

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? int(MAX_INSTANCES) : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if((m_switchReg[id] & 0x003f) != data)
			{
				m_switchReg[id] &= 0x03c0;
				m_switchReg[id] |= data;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetSettleTime(5);
			}
		}
	}

	return;
}
