/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#pragma once

#include "Units.h"
#include "Vme.h"

class C80842082
:
	// Inheritance
	protected virtual CVme
{
public:
	// Constants
	static const unsigned long FINAL_IF_FREQ_HZ = 2048000; // Hz
	static const unsigned long FIRST_IF_FREQ_HZ = 70000000; // Hz
	static const unsigned long TUNE_STEP_200 = 200000; // Hz
	static const unsigned long TUNE_STEP_250 = 250000; // Hz
	static const unsigned long RF_HIGH_FREQ_HZ = 30000000; // Hz
	static const unsigned long RF_LOW_FREQ_HZ = 1500000; // Hz

	// Functions
	static Units::Frequency GetIfOffset(Units::Frequency freq, bool step250kHz = true);

protected:
	// Types
	struct SCardStatus
	{
		unsigned short id : 3;
		unsigned short cardType : 3;
		unsigned short cardClass : 2;
		unsigned short lo1Ok : 1;
		unsigned short lo2Ok : 1;
		unsigned short ext10MhzOk : 1;
		unsigned short : 2;
		unsigned short version : 3;
	};

	// Functions
	C80842082(void);
	virtual ~C80842082(void);
	SCardStatus GetCardStatus(bool force = false) const;
	unsigned short GetControlReg(void) const { return Peek(CONTROL_REG); }
	unsigned char GetVersion(void) const;
	bool Is250kHzStep(void) const { return (Peek(CONTROL_REG) & 0x4000) != 0; }
	bool IsPresent(void) const { return m_present; }
	void SetBiteGen(bool on);
	void SetLo1(Units::Frequency freq, bool step250kHz);
	Units::Frequency Tune(Units::Frequency freq, Units::Frequency rxBw, bool step250kHz = true);

	// Data
	mutable CMutex m_mutex;

private:
	// Constants
	static const unsigned int CARD_CLASS = 0xc;
	static const unsigned int CARD_TYPE = 0x5;
	static const unsigned int MAX_INSTANCES = 1;
	static const unsigned int VME_BASE_ADDR = (0x770000 | (CARD_CLASS << 12) | (CARD_TYPE << 8));
	static const unsigned int STATUS_REG = 0x00;
	static const unsigned int CONTROL_REG = 0x02;
	static LPCTSTR MUTEX_NAME;

	// Functions
	unsigned short Peek(unsigned char reg) const { return CVme::Peek(VME_BASE_ADDR + reg); }
	void Poke(unsigned char reg, unsigned short data) { CVme::Poke(VME_BASE_ADDR + reg, data); }
	virtual void SetSettleTime(unsigned int) {}

	// Data
	bool m_first;
	bool m_present;
};
