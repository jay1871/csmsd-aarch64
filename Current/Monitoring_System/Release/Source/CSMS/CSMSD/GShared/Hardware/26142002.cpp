/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2012-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "StdAfx.h"

#include <algorithm>
#include <memory>

#include "26142001.h"
#include "26142002.h"
#include "Units.h"
#include "Vme.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
//
// Static data
//
LPCTSTR C26142002::MUTEX_NAME = _T("Global\\TCI.{75AE6889-A950-414F-B167-5E11F46D8C38}.Mutex");
LPCTSTR C26142002::SHMEM_NAME = _T("Global\\TCI.{75AE6889-A950-414F-B167-5E11F46D8C38}.ShMem");


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C26142002::C26142002(void)
:
    m_exceedUsecWait(0),
	m_intHandler(false),
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get())
{
	CSingleLock lock(&m_mutex, TRUE);
	m_first = GetSharedMemory<SShMem[MAX_INSTANCES]>(SHMEM_NAME, m_shMem);

	for(unsigned char id = 0; id < MAX_INSTANCES; ++id)
	{
		SCardStatus status = GetCardStatus(id, true);
		m_present[id] = (status.cardType == CARD_TYPE && status.id == id);

		if(m_present[id])
		{
			if(m_first)
			{
				UPllReg reg;

				// Init ADC clock to 153.6 MHz
				reg.raw = 0;
				reg.hmc704lp4e.a = 1;
				reg.hmc704lp4e.powerDown.cenSpi = 1;
				SetPllReg(reg, ADC, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 2;
				reg.hmc704lp4e.refDiv.r = 5;
				SetPllReg(reg, ADC, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 6;
				reg.hmc704lp4e.deltaSigmaConfig.selSeed = 2;
				reg.hmc704lp4e.deltaSigmaConfig.selMod = 2;
				reg.hmc704lp4e.deltaSigmaConfig.reserved1 = 4;
				reg.hmc704lp4e.deltaSigmaConfig.autoSeed = 1;
				reg.hmc704lp4e.deltaSigmaConfig.selMdclk = 1;
				reg.hmc704lp4e.deltaSigmaConfig.selCreClk = 1;
				reg.hmc704lp4e.deltaSigmaConfig.enMcore = 1;
				reg.hmc704lp4e.deltaSigmaConfig.reserved2 = 48;
				SetPllReg(reg, ADC, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 7;
				reg.hmc704lp4e.lockDetect.ldW = 5;
				reg.hmc704lp4e.lockDetect.ldEn = 1;
				reg.hmc704lp4e.lockDetect.ldType = 1;
				reg.hmc704lp4e.lockDetect.ldDw = 3;
				reg.hmc704lp4e.lockDetect.ldTper = 2;
				SetPllReg(reg, ADC, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 8;
				reg.hmc704lp4e.analogEnable.enIcbias = 1;
				reg.hmc704lp4e.analogEnable.enCp = 1;
				reg.hmc704lp4e.analogEnable.enPd = 1;
				reg.hmc704lp4e.analogEnable.enRbuf = 1;
				reg.hmc704lp4e.analogEnable.enRfBuf = 1;
				reg.hmc704lp4e.analogEnable.enSdo = 1;
				reg.hmc704lp4e.analogEnable.spare = 1;
				reg.hmc704lp4e.analogEnable.enRfdiv = 1;
				reg.hmc704lp4e.analogEnable.enPreClk = 1;
				reg.hmc704lp4e.analogEnable.enPreBias = 1;
				reg.hmc704lp4e.analogEnable.enCpamp = 1;
				reg.hmc704lp4e.analogEnable.biasRfBuf = 3;
				reg.hmc704lp4e.analogEnable.biasDiv = 3;
				SetPllReg(reg, ADC, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 10;
				reg.hmc704lp4e.auxSpiTrigger.zero = 0;
				reg.hmc704lp4e.auxSpiTrigger.reserved = 0x1800;
				SetPllReg(reg, ADC, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 11;
				reg.hmc704lp4e.phaseDetector.pdDel = 1;
				reg.hmc704lp4e.phaseDetector.pdUEn = 1;
				reg.hmc704lp4e.phaseDetector.pdDEn = 1;
				reg.hmc704lp4e.phaseDetector.cpBias = 3;
				reg.hmc704lp4e.phaseDetector.mctrGt = 3;
				SetPllReg(reg, ADC, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 15;
				reg.hmc704lp4e.genPurposeOut.gpoSel = 1;
				reg.hmc704lp4e.genPurposeOut.inhSpiMux = 1;
				reg.hmc704lp4e.genPurposeOut.inhSpiLdet = 1;
				SetPllReg(reg, ADC, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 9;
				reg.hmc704lp4e.chargePump.cpD = 93;
				reg.hmc704lp4e.chargePump.cpU = 93;
				reg.hmc704lp4e.chargePump.cpO = 83;
				SetPllReg(reg, ADC, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 12;
				reg.hmc704lp4e.exactFreq.numChan = 625;
				SetPllReg(reg, ADC, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 3;
				reg.hmc704lp4e.intPart.nInt = 30;
				SetPllReg(reg, ADC, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 4;
				reg.hmc704lp4e.fracPart.nFrac = 12079596;
				SetPllReg(reg, ADC, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 9;
				reg.hmc704lp4e.chargePump.cpD = 93;
				reg.hmc704lp4e.chargePump.cpU = 93;
				reg.hmc704lp4e.chargePump.cpO = 83;
				reg.hmc704lp4e.chargePump.dirU = 1;
				SetPllReg(reg, ADC, id);

				// Init LO1
				reg.raw = 0;
				reg.hmc704lp4e.a = 1;
				reg.hmc704lp4e.powerDown.cenSpi = 1;
				SetPllReg(reg, LO1, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 2;
				reg.hmc704lp4e.refDiv.r = 2;
				SetPllReg(reg, LO1, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 6;
				reg.hmc704lp4e.deltaSigmaConfig.selSeed = 2;
				reg.hmc704lp4e.deltaSigmaConfig.selMod = 2;
				reg.hmc704lp4e.deltaSigmaConfig.reserved1 = 4;
				reg.hmc704lp4e.deltaSigmaConfig.autoSeed = 1;
				reg.hmc704lp4e.deltaSigmaConfig.selMdclk = 1;
				reg.hmc704lp4e.deltaSigmaConfig.selCreClk = 1;
				reg.hmc704lp4e.deltaSigmaConfig.enMcore = 1;
				reg.hmc704lp4e.deltaSigmaConfig.reserved2 = 48;
				SetPllReg(reg, LO1, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 7;
				reg.hmc704lp4e.lockDetect.ldW = 5;
				reg.hmc704lp4e.lockDetect.ldEn = 1;
				reg.hmc704lp4e.lockDetect.ldType = 1;
				reg.hmc704lp4e.lockDetect.ldDw = 2;
				SetPllReg(reg, LO1, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 8;
				reg.hmc704lp4e.analogEnable.enIcbias = 1;
				reg.hmc704lp4e.analogEnable.enCp = 1;
				reg.hmc704lp4e.analogEnable.enPd = 1;
				reg.hmc704lp4e.analogEnable.enRbuf = 1;
				reg.hmc704lp4e.analogEnable.enRfBuf = 1;
				reg.hmc704lp4e.analogEnable.enSdo = 1;
				reg.hmc704lp4e.analogEnable.spare = 1;
				reg.hmc704lp4e.analogEnable.enRfdiv = 1;
				reg.hmc704lp4e.analogEnable.enPreClk = 1;
				reg.hmc704lp4e.analogEnable.enPreBias = 1;
				reg.hmc704lp4e.analogEnable.enCpamp = 1;
				reg.hmc704lp4e.analogEnable.biasRfBuf = 3;
				reg.hmc704lp4e.analogEnable.biasDiv = 3;
				SetPllReg(reg, LO1, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 10;
				reg.hmc704lp4e.auxSpiTrigger.zero = 0;
				reg.hmc704lp4e.auxSpiTrigger.reserved = 0x1800;
				SetPllReg(reg, LO1, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 11;
				reg.hmc704lp4e.phaseDetector.pdDel = 1;
				reg.hmc704lp4e.phaseDetector.pdPolr = 1;
				reg.hmc704lp4e.phaseDetector.pdUEn = 1;
				reg.hmc704lp4e.phaseDetector.pdDEn = 1;
				reg.hmc704lp4e.phaseDetector.cpBias = 3;
				reg.hmc704lp4e.phaseDetector.mctrGt = 3;
				SetPllReg(reg, LO1, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 15;
				reg.hmc704lp4e.genPurposeOut.gpoSel = 1;
				reg.hmc704lp4e.genPurposeOut.inhSpiMux = 1;
				reg.hmc704lp4e.genPurposeOut.inhSpiLdet = 1;
				SetPllReg(reg, LO1, id);

				// Init LO2
				reg.raw = 0;
				reg.hmc704lp4e.a = 1;
				reg.hmc704lp4e.powerDown.cenSpi = 1;
				SetPllReg(reg, LO2, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 2;
				reg.hmc704lp4e.refDiv.r = 2;
				SetPllReg(reg, LO2, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 6;
				reg.hmc704lp4e.deltaSigmaConfig.selSeed = 2;
				reg.hmc704lp4e.deltaSigmaConfig.selMod = 2;
				reg.hmc704lp4e.deltaSigmaConfig.reserved1 = 4;
				reg.hmc704lp4e.deltaSigmaConfig.autoSeed = 1;
				reg.hmc704lp4e.deltaSigmaConfig.selMdclk = 1;
				reg.hmc704lp4e.deltaSigmaConfig.selCreClk = 1;
				reg.hmc704lp4e.deltaSigmaConfig.enMcore = 1;
				reg.hmc704lp4e.deltaSigmaConfig.reserved2 = 48;
				SetPllReg(reg, LO2, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 7;
				reg.hmc704lp4e.lockDetect.ldW = 5;
				reg.hmc704lp4e.lockDetect.ldEn = 1;
				reg.hmc704lp4e.lockDetect.ldType = 2;
				reg.hmc704lp4e.lockDetect.ldDw = 2;
				reg.hmc704lp4e.lockDetect.ldTper = 0;
				SetPllReg(reg, LO2, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 8;
				reg.hmc704lp4e.analogEnable.enIcbias = 1;
				reg.hmc704lp4e.analogEnable.enCp = 1;
				reg.hmc704lp4e.analogEnable.enPd = 1;
				reg.hmc704lp4e.analogEnable.enRbuf = 1;
				reg.hmc704lp4e.analogEnable.enRfBuf = 1;
				reg.hmc704lp4e.analogEnable.enSdo = 1;
				reg.hmc704lp4e.analogEnable.spare = 1;
				reg.hmc704lp4e.analogEnable.enRfdiv = 1;
				reg.hmc704lp4e.analogEnable.enPreClk = 1;
				reg.hmc704lp4e.analogEnable.enPreBias = 1;
				reg.hmc704lp4e.analogEnable.enCpamp = 1;
				reg.hmc704lp4e.analogEnable.biasRfBuf = 3;
				reg.hmc704lp4e.analogEnable.biasDiv = 3;
				SetPllReg(reg, LO2, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 10;
				reg.hmc704lp4e.auxSpiTrigger.zero = 0;
				reg.hmc704lp4e.auxSpiTrigger.reserved = 0x1800;
				SetPllReg(reg, LO2, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 11;
				reg.hmc704lp4e.phaseDetector.pdDel = 1;
				reg.hmc704lp4e.phaseDetector.pdUEn = 1;
				reg.hmc704lp4e.phaseDetector.pdDEn = 1;
				reg.hmc704lp4e.phaseDetector.cpBias = 3;
				reg.hmc704lp4e.phaseDetector.mctrGt = 3;
				SetPllReg(reg, LO2, id);
				reg.raw = 0;
				reg.hmc704lp4e.a = 15;
				reg.hmc704lp4e.genPurposeOut.gpoSel = 1;
				reg.hmc704lp4e.genPurposeOut.inhSpiMux = 1;
				reg.hmc704lp4e.genPurposeOut.inhSpiLdet = 1;
				SetPllReg(reg, LO2, id);
				Tune(98750000, 4000000, id);

				// Init Cal
				reg.raw = 0;
				reg.adf4351.control = 5;
				reg.adf4351.lockDetect.reserved = 3;
				reg.adf4351.lockDetect.ldP = 1;
				SetPllReg(reg, CAL, id);
				reg.raw = 0;
				reg.adf4351.control = 4;
				reg.adf4351.configuration.pOut = 1;
				reg.adf4351.configuration.clkDiv = 100;
				reg.adf4351.configuration.selFdbk = 1;
				SetPllReg(reg, CAL, id);
				reg.raw = 0;
				reg.adf4351.control = 3;
				reg.adf4351.cycleSlipConfig.clkMode = 1;
				SetPllReg(reg, CAL, id);
				reg.raw = 0;
				reg.adf4351.control = 2;
				reg.adf4351.rCounter.pwrDn = 1;
				reg.adf4351.rCounter.pdPol = 1;
				reg.adf4351.rCounter.cpCur = 15;
				reg.adf4351.rCounter.buffDbl = 1;
				reg.adf4351.rCounter.rCtr = 1;
				reg.adf4351.rCounter.mux = 6;
				SetPllReg(reg, CAL, id);
				reg.raw = 0;
				reg.adf4351.control = 1;
				reg.adf4351.deltaSigmaMod.mod = 1600; // 10 MHz / 6.25 kHz
				reg.adf4351.deltaSigmaMod.dsP = 1;
				reg.adf4351.deltaSigmaMod.presclr = 1;
				SetPllReg(reg, CAL, id);
				SetCalTone(1500000000, false, id);
				Tune(100000000, 4000000, id);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C26142002::~C26142002(void)
{
	FreeSharedMemory<C26142002::SShMem[2]>(m_shMem);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the 100 MHz OCXO status
//
C26142002::S100MHzStatus C26142002::Get100MHzStatus(unsigned char id) const
{
	union
	{
		unsigned short reg;
		S100MHzStatus status;
	} status = { 0 };

	if(m_present[id])
	{
		status.reg = Peek(id, GPS_STATUS_REG);

		// Copy in inhibited bit
		status.status.inhibited = ((Peek(id, CONFIG_REG) & 0x0800) == 0x0800);
	}

	return status.status;
}


//////////////////////////////////////////////////////////////////////
//
// Get closest cal freq. Cal freqs are at 6.25 kHz steps
//
Units::Frequency C26142002::GetCalFreq(Units::Frequency freq)
{
	if(freq <= CAL_LOW_FREQ_HZ)
	{
		return CAL_LOW_FREQ_HZ;
	}
	else if(freq >= CAL_HIGH_FREQ_HZ)
	{
		return CAL_HIGH_FREQ_HZ;
	}
	else
	{
		return (freq + CAL_STEP_HZ / 2).Hz<unsigned long long>() / CAL_STEP_HZ * CAL_STEP_HZ;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the card status
//
C26142002::SCardStatus C26142002::GetCardStatus(unsigned char id, bool force) const
{
	unsigned short reg = 0;

	if(force || m_present[id])
	{
		reg = Peek(id, STATUS_REG);
	}

	union
	{
		unsigned short reg;
		SCardStatus status;
	} status = { reg };

	return status.status;
}


//////////////////////////////////////////////////////////////////////
//
// Get the time-of-day
//
FILETIME C26142002::GetTime(unsigned char id) const
{
	unsigned int tries = 1;
	unsigned short frac = Peek(id, GPS_FRAC_SEC_REG);
	unsigned short secHi = Peek(id, GPS_SEC_HI_REG);
	unsigned short secLo = Peek(id, GPS_SEC_LO_REG);

	if(frac == 0 || frac == 1)
	{
		unsigned short frac2 = Peek(id, GPS_FRAC_SEC_REG);

		while(frac2 != frac)
		{
			frac = frac2;
			secHi = Peek(id, GPS_SEC_HI_REG);
			secLo = Peek(id, GPS_SEC_LO_REG);
			frac2 = Peek(id, GPS_FRAC_SEC_REG);
			++tries;
		}

		if(frac2 == 0)
		{
			// Adjust for delayed rollover of seconds counter
			if(++secLo == 0)
			{
				++secHi;
			}
		}
	}

	// Convert to FILETIME
	ULARGE_INTEGER time;
	time.QuadPart = 10000000 * (65536ull * secHi + secLo) + 200 * frac + 116444736000000000;
	FILETIME filetime = { time.LowPart, time.HighPart };

	return filetime;
}


//////////////////////////////////////////////////////////////////////
//
// Get the board version
//
unsigned char C26142002::GetVersion(unsigned char id) const
{
	return GetCardStatus(id).version;
}


//////////////////////////////////////////////////////////////////////
//
// Inhibit the GPS sync
//
void C26142002::InhibitGpsSync(bool inhibit, unsigned char id)
{
	CSingleLock lock(&m_mutex, TRUE);
	unsigned short reg = Peek(id, CONFIG_REG);

	if(inhibit != ((reg & 0x0800) != 0))
	{
		reg &= 0x3171;

		if(inhibit)
		{
			reg |= 0x0800;
		}

		Poke(id, CONFIG_REG, reg);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Sets the calibration tone frequency and state. Returns the actual frequency
//
Units::Frequency C26142002::SetCalTone(Units::Frequency freq, bool on, signed char idOrAll)
{
	Units::Frequency calFreq = GetCalFreq(freq);
	CSingleLock lock(&m_mutex, TRUE);

	for(unsigned char id = (idOrAll < 0 ? 0 : idOrAll); id < (idOrAll < 0 ? int(MAX_INSTANCES) : idOrAll + 1); ++id)
	{
		if(IsPresent(id))
		{
			unsigned short reg = Peek(id, CONFIG_REG);
			bool state = ((reg & 0x0001u) == 0);

			if(on != state || calFreq != (*m_shMem)[id].calFreqSixAndQuarterKhz * CAL_STEP_HZ)
			{
				// Program config reg
				reg &= 0x0970u; // Implicitly set PLL to CAL

				if(!on)
				{
					reg |= 0x0001;
				}

				Poke(id, CONFIG_REG, reg);

				static const struct
				{
					unsigned long long calFreq;
					unsigned char divisor;
					unsigned int selDiv;
				} CAL_SETTINGS[] =
				{
					{   68750000, 64, 0x6 },
					{  137500000, 32, 0x5 },
					{  275000000, 16, 0x4 },
					{  550000000,  8, 0x3 },
					{ 1100000000,  4, 0x2 },
					{ 2200000000,  2, 0x1 },
					{ 4400000000,  1, 0x0 }
				};

				unsigned char band;

				for(band = 0; band < _countof(CAL_SETTINGS); ++band)
				{
					if(calFreq < CAL_SETTINGS[band].calFreq)
					{
						break;
					}
				}

				if(band == _countof(CAL_SETTINGS))
				{
					--band;
				}

				unsigned long long vcoFreq = calFreq.Hz<unsigned long long>() * CAL_SETTINGS[band].divisor;

				// Program PLL
				UPllReg pllReg;
				pllReg.raw = 0;
				pllReg.adf4351.control = 4;
				pllReg.adf4351.configuration.pOut = 1;
				pllReg.adf4351.configuration.enOut = (on ? 1 : 0);
				pllReg.adf4351.configuration.clkDiv = 100;
				pllReg.adf4351.configuration.selDiv = CAL_SETTINGS[band].selDiv;
				pllReg.adf4351.configuration.selFdbk = 1;
				SetPllReg(pllReg, CAL, id);
				pllReg.raw = 0;
				pllReg.adf4351.control = 2;
				pllReg.adf4351.rCounter.pwrDn = (on ? 0 : 1);
				pllReg.adf4351.rCounter.pdPol = 1;
				pllReg.adf4351.rCounter.cpCur = 15;
				pllReg.adf4351.rCounter.buffDbl = 1;
				pllReg.adf4351.rCounter.rCtr = 1;
				pllReg.adf4351.rCounter.mux = 6;
				SetPllReg(pllReg, CAL, id);
				pllReg.raw = 0;
				pllReg.adf4351.control = 0;
				pllReg.adf4351.nIntFrac.nInt = vcoFreq / 10000000; // Fpfd = 10 MHz
				pllReg.adf4351.nIntFrac.nFrac = (vcoFreq - pllReg.adf4351.nIntFrac.nInt * 10000000ull) / CAL_STEP_HZ;
				SetPllReg(pllReg, CAL, id);
				(*m_shMem)[id].calFreqSixAndQuarterKhz = unsigned long(calFreq.Hz<unsigned long long>() / CAL_STEP_HZ);
				SetSettleTime(on ? (state ? 250 : 200) : 3000);
			}
		}
	}

	return calFreq;
}


//////////////////////////////////////////////////////////////////////
//
// Set LO1
//
void C26142002::SetLo1(unsigned short freqSixAndQuarterMhz, unsigned char id)
{
	static const struct 
	{
		unsigned short freqSixAndQuarterMhz;
		unsigned int r;
		unsigned int ldDw;
		unsigned int ldTper;
		unsigned int iCp; // uA
		unsigned int hiKcp;
		unsigned short config;
	} SETTINGS[] = 
		{
			{ 153, 3, 2, 2, 1500, 0, 0x0000 }, // 0-956.25 (not used)
			{ 160, 3, 2, 2, 1500, 0, 0x0010 }, // 962.5-1000
			{ 247, 2, 2, 2, 2300, 0, 0x0010 }, // 1006.25-1543.75
			{ 373, 1, 0, 1,  300, 1, 0x0000 }, // 1550-2331.25 (not used)
			{ 440, 1, 0, 1,  700, 1, 0x0020 }, // 2337.5-2750
			{ 508, 1, 0, 1, 1200, 1, 0x0030 }, // 2756.25-3175
			{ 566, 1, 0, 1, 1700, 1, 0x0040 }, // 3181.25-3537.5
			{ 633, 1, 0, 1, 2300, 1, 0x0060 }  // 3543.75-3956.25
		};
	static const unsigned int C_START_SAME_CONFG_IDX = 1;	//1st index where the config value is the same.

	static unsigned int s_prevband = 0;
	unsigned int band;

	for(band = 1; band < _countof(SETTINGS); ++band)
	{
		if(freqSixAndQuarterMhz <= SETTINGS[band].freqSixAndQuarterMhz)
		{
			break;
		}
	}

	ASSERT(band > 0 && band < _countof(SETTINGS) && band != 3);

	if(band == _countof(SETTINGS))
	{
		band = _countof(SETTINGS) - 1;
	}

	unsigned int cp = unsigned int(SETTINGS[band - 1].iCp + float(freqSixAndQuarterMhz - SETTINGS[band - 1].freqSixAndQuarterMhz) *
		(SETTINGS[band].iCp - SETTINGS[band - 1].iCp) / (SETTINGS[band].freqSixAndQuarterMhz - SETTINGS[band - 1].freqSixAndQuarterMhz) + 10) / 20;
	unsigned int cpO = std::min(unsigned int((5 + 1280.0f / freqSixAndQuarterMhz) / SETTINGS[band].r * cp + 2.5f) / 5, 127u);
	CSingleLock lock(&m_mutex, TRUE);
	unsigned short reg = Peek(id, CONFIG_REG);

#ifndef TCI_INCLUDE_FAST_MODE
	SetSettleTime(1000); // Enforce ECCN 5A001.b.5 export restriction
#endif

	//@note: 01/03/17 From Parthi: If the r value changes, it must have 2.0 msec settle time.
	if (SETTINGS[s_prevband].r != SETTINGS[band].r)
	{
		SetSettleTime(2000);
	}
	else if (((reg ^ SETTINGS[band].config) & 0x0070) != 0)
	{
		//Estonia changed from 500 to 700 usec.
		SetSettleTime(700);
	}
	else if((*m_shMem)[id].lo1FreqSixAndQuarterMhz != freqSixAndQuarterMhz)
	{
#ifdef _DEBUG
		unsigned int settleTime = unsigned int(280 * pow(abs(float((*m_shMem)[id].lo1FreqSixAndQuarterMhz) - float(freqSixAndQuarterMhz)), 0.25f)) - 150;
		SetSettleTime(settleTime);
#else
		SetSettleTime(unsigned int(280 * pow(abs(float((*m_shMem)[id].lo1FreqSixAndQuarterMhz) - float(freqSixAndQuarterMhz)), 0.25f)) - 150);
#endif
	}

	s_prevband = band;
		
	(*m_shMem)[id].lo1FreqSixAndQuarterMhz = freqSixAndQuarterMhz;
	(*m_shMem)[id].rfFreqOneAndNineSixteenthMhz = 4 * freqSixAndQuarterMhz -
		unsigned short(((*m_shMem)[id].narrowband ? FINAL_IF_FREQ_NARROW_BW_HZ : FINAL_IF_FREQ_WIDE_BW_HZ) / 1562500) - (*m_shMem)[id].lo2FreqOneAndNineSixteenthMhz;
	Poke(id, CONFIG_REG, (reg & 0x3901) | SETTINGS[band].config);
	UPllReg pllReg;
	pllReg.raw = 0;
	pllReg.hmc704lp4e.a = 2;
	pllReg.hmc704lp4e.refDiv.r = SETTINGS[band].r;
	SetPllReg(pllReg, LO1, id);
	pllReg.raw = 0;
	pllReg.hmc704lp4e.a = 7;
	pllReg.hmc704lp4e.lockDetect.ldW = 5;
	pllReg.hmc704lp4e.lockDetect.ldEn = 1;
	pllReg.hmc704lp4e.lockDetect.ldType = 1;
	pllReg.hmc704lp4e.lockDetect.ldDw = SETTINGS[band].ldDw;
	pllReg.hmc704lp4e.lockDetect.ldTper = SETTINGS[band].ldTper;
	SetPllReg(pllReg, LO1, id);
	pllReg.raw = 0;
	pllReg.hmc704lp4e.a = 9;
	pllReg.hmc704lp4e.chargePump.cpD = cp;
	pllReg.hmc704lp4e.chargePump.cpU = cp;
	pllReg.hmc704lp4e.chargePump.cpO = cpO;
	pllReg.hmc704lp4e.chargePump.hiKcp = SETTINGS[band].hiKcp;
	SetPllReg(pllReg, LO1, id);
	pllReg.raw = 0;
	pllReg.hmc704lp4e.a = 3;
	pllReg.hmc704lp4e.intPart.nInt = freqSixAndQuarterMhz * SETTINGS[band].r / 16;
	ASSERT(pllReg.hmc704lp4e.intPart.nInt >= 20);
	SetPllReg(pllReg, LO1, id);

	//Parthi request.
	{
		pllReg.raw = 0;
		pllReg.hmc704lp4e.a = 4;
		pllReg.hmc704lp4e.fracPart.nFrac = (freqSixAndQuarterMhz * SETTINGS[band].r - 16 * pllReg.hmc704lp4e.intPart.nInt) * 1048576 /*+ SETTINGS[band].r*/;
		SetPllReg(pllReg, LO1, id);
		Wait(C_LO1_REG4_WAIT_USEC); //Wait 100 usec. as requested by Parthi.
	}

	pllReg.raw = 0;
	pllReg.hmc704lp4e.a = 9;
	pllReg.hmc704lp4e.chargePump.cpD = cp;
	pllReg.hmc704lp4e.chargePump.cpU = cp;
	pllReg.hmc704lp4e.chargePump.cpO = cpO;
	pllReg.hmc704lp4e.chargePump.dirD = 1;
	pllReg.hmc704lp4e.chargePump.hiKcp = SETTINGS[band].hiKcp;
	SetPllReg(pllReg, LO1, id);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set LO2
//
void C26142002::SetLo2(unsigned short freqOneAndNineSixteenthMhz, unsigned char id)
{
	static const struct 
	{
		unsigned short lo2FreqOneAndNineSixteenthMhz;
		unsigned short config;
	} SETTINGS[] = 
		{
			{  513, 0x0000 }, // Not used
			{  529, 0x0000 }, // 803.125-826.5625
			{ 1169, 0x0100 }, // Not used
			{ 1185, 0x0100 }  // 1828.125-1851.5625
		};

	unsigned int band;

	for(band = 0; band < _countof(SETTINGS); ++band)
	{
		if(freqOneAndNineSixteenthMhz <= SETTINGS[band].lo2FreqOneAndNineSixteenthMhz)
		{
			break;
		}
	}

	ASSERT(band > 0 && band < _countof(SETTINGS) && band != 2);
	auto extDiv2 = (SETTINGS[band].config == 0x0000);
	unsigned int cpO = std::min(unsigned int((5 + (extDiv2 ? 2560.0f : 5120.0f) / freqOneAndNineSixteenthMhz) / 2 * 75 + 2.5f) / 5, 127u);
	CSingleLock lock(&m_mutex, TRUE);
	SetSettleTime(50);
	(*m_shMem)[id].lo2FreqOneAndNineSixteenthMhz = freqOneAndNineSixteenthMhz;
	(*m_shMem)[id].rfFreqOneAndNineSixteenthMhz = 4 * (*m_shMem)[id].lo1FreqSixAndQuarterMhz -
		unsigned short(((*m_shMem)[id].narrowband ? FINAL_IF_FREQ_NARROW_BW_HZ : FINAL_IF_FREQ_WIDE_BW_HZ) / 1562500) - freqOneAndNineSixteenthMhz;
	unsigned short reg = Peek(id, CONFIG_REG);

	if(((reg ^ SETTINGS[band].config) & 0x0100) != 0)
	{
		SetSettleTime(120);
	}

#ifndef TCI_INCLUDE_FAST_MODE
	SetSettleTime(1000); // Enforce ECCN 5A001.b.5 export restriction
#endif
	Poke(id, CONFIG_REG, (reg & 0x3871) | SETTINGS[band].config);
	UPllReg pllReg;
	pllReg.raw = 0;
	pllReg.hmc704lp4e.a = 9;
	pllReg.hmc704lp4e.chargePump.cpD = 75;
	pllReg.hmc704lp4e.chargePump.cpU = 75;
	pllReg.hmc704lp4e.chargePump.cpO = cpO;
	SetPllReg(pllReg, LO2, id);
	pllReg.raw = 0;
	pllReg.hmc704lp4e.a = 3;
	pllReg.hmc704lp4e.intPart.nInt = freqOneAndNineSixteenthMhz / (extDiv2 ? 16 : 32);
	ASSERT(pllReg.hmc704lp4e.intPart.nInt >= 20);
	SetPllReg(pllReg, LO2, id);
	pllReg.raw = 0;
	pllReg.hmc704lp4e.a = 4;
	pllReg.hmc704lp4e.fracPart.nFrac = (freqOneAndNineSixteenthMhz * (extDiv2 ? 4 : 2) - 64 * pllReg.hmc704lp4e.intPart.nInt) * 262144 /*+ (extDiv2 ? 4 : 2)*/; 
	SetPllReg(pllReg, LO2, id);
	pllReg.raw = 0;
	pllReg.hmc704lp4e.a = 9;
	pllReg.hmc704lp4e.chargePump.cpD = 75;
	pllReg.hmc704lp4e.chargePump.cpU = 75;
	pllReg.hmc704lp4e.chargePump.cpO = cpO;
	pllReg.hmc704lp4e.chargePump.dirU = 1;
	SetPllReg(pllReg, LO2, id);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set a PLL register
//
void C26142002::SetPllReg(const UPllReg& reg, EPll pll, unsigned char id)
{
	{
		CSingleLock lock(&m_mutex, TRUE);
		Poke(id, CONFIG_REG, (Peek(id, CONFIG_REG) & 0x0973) | (pll << 12));
	}

	Poke(id, PLL_CTRL_HI_REG, reg.hi);
	Poke(id, PLL_CTRL_LO_REG, reg.lo);

	// Busy wait
	static const unsigned long MAX_WAIT = 1000000;
	unsigned long i;

	for(i = 0; (Peek(id, CONFIG_REG) & 0x0004u) == 0 && i < MAX_WAIT; ++i)
	{
		_mm_pause();
	}

#ifdef _DEBUG
	if(i == MAX_WAIT)
	{
		TRACE(_T("2614-2002 PLL control timed out\n"));
	}
#endif

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the seconds registers (uses time_t semantics)
//
void C26142002::SetSeconds(time_t secs, unsigned char id)
{
	do
	{
		Poke(id, GPS_SEC_HI_REG, unsigned short(secs >> 16));
		Poke(id,GPS_SEC_LO_REG, unsigned short(secs & 0xffff));
	} while(Peek(id, GPS_FRAC_SEC_REG) == 0);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Tune the hardware. Returns final IF freq
//
Units::Frequency C26142002::Tune(Units::Frequency freq, Units::Frequency rxBw, signed char idOrAll)
{
	// Validate
	ASSERT(freq <= RF_HIGH_FREQ_HZ + rxBw / 2); // VCP allows tuning down to DC

	unsigned short freqOneAndNineSixteenthMhz = GetTuneStepNumber(freq);
	int bwIdx = (rxBw <= NARROW_BW_HZ ? 0 : 1);
	CSingleLock lock(&m_mutex, TRUE);
	unsigned short lo1FreqSixAndQuarterMhz = 0;
	unsigned short lo2FreqOneAndNineSixteenthMhz = 0;

	for(unsigned char id = (idOrAll < 0 ? 0 : idOrAll); id < (idOrAll < 0 ? int(MAX_INSTANCES) : idOrAll + 1); ++id)
	{
		if(IsPresent(id))
		{
			(*m_shMem)[id].narrowband = (bwIdx == 0);

			static const struct 
			{
				unsigned short rFreqOneAndNineSixteenthMhz;
				unsigned short if1FreqSixAndQuarterMhz;
				unsigned short if2FreqOneAndNineSixteenthMhz;
			} SETTINGS[][2] =
			{
				// Narrowband		 Wideband
				{ { 384, 151, FINAL_IF_FREQ_NARROW_BW_HZ / 1562500 }, { 384, 150, FINAL_IF_FREQ_WIDE_BW_HZ / 1562500 } }, // 20-599 MHz
				{ {  896, 315, FINAL_IF_FREQ_NARROW_BW_HZ / 1562500 }, {  896, 314, FINAL_IF_FREQ_WIDE_BW_HZ / 1562500 } }, // 600-1399 MHz
				{ { 1934, 151, FINAL_IF_FREQ_NARROW_BW_HZ / 1562500 }, { 1934, 150, FINAL_IF_FREQ_WIDE_BW_HZ / 1562500 } }  // 1400-3000 MHz
			};

			unsigned int band;

			for(band = 0; band < _countof(SETTINGS); ++band)
			{
				if(freqOneAndNineSixteenthMhz < SETTINGS[band][bwIdx].rFreqOneAndNineSixteenthMhz)
				{
					break;
				}
			}

			ASSERT(band < _countof(SETTINGS));

			if(band ==_countof(SETTINGS))
			{
				band = _countof(SETTINGS) - 1;
			}

			// Program LO1
			lo1FreqSixAndQuarterMhz = (freqOneAndNineSixteenthMhz + 2) / 4 + SETTINGS[band][bwIdx].if1FreqSixAndQuarterMhz;

			// LO1 mustn't drop below 981.25 MHz in wideband
			if(bwIdx == 1 && lo1FreqSixAndQuarterMhz < 157)
			{
				lo1FreqSixAndQuarterMhz = 157;
			}

			if(lo1FreqSixAndQuarterMhz != (*m_shMem)[id].lo1FreqSixAndQuarterMhz)
			{
				SetLo1(lo1FreqSixAndQuarterMhz, id);
			}

			// Program LO2
			lo2FreqOneAndNineSixteenthMhz = 4 * lo1FreqSixAndQuarterMhz - freqOneAndNineSixteenthMhz -  SETTINGS[band][bwIdx].if2FreqOneAndNineSixteenthMhz;

			// If LO1 has been constrained, don't let LO2 go too high to compensate
			if(bwIdx == 1 && lo2FreqOneAndNineSixteenthMhz < 1177 && lo2FreqOneAndNineSixteenthMhz > 529)
			{
				lo2FreqOneAndNineSixteenthMhz = 529;
			}

			if(lo2FreqOneAndNineSixteenthMhz != (*m_shMem)[id].lo2FreqOneAndNineSixteenthMhz)
			{
				SetLo2(lo2FreqOneAndNineSixteenthMhz, id);
			}
		}
	}

	return (4 * lo1FreqSixAndQuarterMhz - lo2FreqOneAndNineSixteenthMhz) * MIN_TUNE_STEP - freq;
}


//////////////////////////////////////////////////////////////////////
//
// Use the PIT to delay. Uses first synthesizer only
//
void C26142002::Wait(unsigned long us)
{
	if(us == 0)
	{
		return;
	}

	ASSERT(us < 131072);
	ASSERT(C_LO1_REG4_WAIT_USEC <= C_MAX_SFTW_WAIT_USEC);

	if (us > C_MAX_SFTW_WAIT_USEC)
	{
        //Do not wait more than allowable maximum.
		m_exceedUsecWait = us;
		us = C_MAX_SFTW_WAIT_USEC;
		ASSERT(false);
	}

	// Use busy wait for short delays
	LARGE_INTEGER done;
	QueryPerformanceCounter(&done);
	LARGE_INTEGER count = done;
	LARGE_INTEGER freq;
	QueryPerformanceFrequency(&freq);
	done.QuadPart += us * freq.QuadPart / 1000000;

	while ((done.QuadPart - count.QuadPart) > 0)	// Note: count < done doesn't work because of wraparound
	{
		QueryPerformanceCounter(&count);
	}

	return;
}
