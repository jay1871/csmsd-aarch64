/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2011-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include <array>
#include <deque>
#include <map>
#include <memory>
#include <vector>

#include "3220ProductOptions.h"
#if _WIN32_WINNT >= _WIN32_WINNT_VISTA
	#include "ConditionVariable.h"
#endif
#include "IppVec.h"
#if _WIN32_WINNT >= _WIN32_WINNT_VISTA
	#include "SlimRWLock.h"
#endif
#include "Timer.h"
#include "Units.h"

class C32202001
{
public:
	// Constants
	static const int ADC_FULL_SCALE_COUNT = 32768;
	static const float ADC_FULL_SCALE_DBM;
	static const float ADC_FULL_SCALE_WATTS;

	//Block data size must be at least 2 * decimation
	static const unsigned int C_MIN_BLK_SZ_IN_DECI_CNT = 2;
	static const unsigned long MAX_BB_SEQUENCER_BLOCK_SIZE = 0xffffe;
	static const unsigned long MIN_DDC_DECIMATION = 8;
	static const unsigned int NUM_CHANS = 256;
	static const unsigned int NUM_DMAS = 8;
	static const unsigned int NUM_RF_CHANS = 2;
	static LPCTSTR WBDIGI_DEVICE_DESC;

	// Types
	enum EFwType { UNKNOWN, GOLDEN, SMS_2612, SMS_2614, BLACKBIRD };

	struct STciWinDrvInfo
	{
		bool		found;	//true: The driver is found in the system.
							//false: The driver not in the system.
		//The rest of the fields have valid values only if found == true
		CString		versionStr;
		unsigned long tciTnumber; //Driver TCI T-number.
								//FPGA will have the Firmware image value.
								//Non TCI driver will have value of 0.
		unsigned long majorVersion;
		unsigned long minorVersion;
		unsigned long builtNum;
	};

	// Functions
	unsigned int AdcCountsFromSamples(unsigned int samples, unsigned char ch) const;
	unsigned long GetBoardId(void) const { return PeekUser(0x02); }
	unsigned long GetDateStamp(void) const { return PeekUser(0x04); }
	unsigned long GetError(void) const { return PeekUser(0x08) & 0x7ff8; }
	COleDateTime GetFpgaFwDate(void) const;
	unsigned long GetFpgaFwImage(void) const;
	unsigned long GetFpgaFwRev(void) const;
	unsigned long GetImage(void) const { return PeekUser(0x05); }
	Units::Frequency GetMeasuredSampleRate(void) const { return HasTimestampCapability() ? PeekUser(m_type == SMS_2614 ? 0xc8 : 0x75) : 0; }
	unsigned char GetNumDdcs(void) const { return m_type == BLACKBIRD ? unsigned char(PeekUser(0x80)) - 4 : 0; }
	unsigned long GetNwLogicVersion(void) const { return PeekDma(0x1001); }
	unsigned long GetPciExpressCoreVersion(void) const { return PeekDma(0x1002); }
	Units::TimeSpan GetPpsAverage(void) const;

	Units::TimeSpan GetPpsDrift(void) const
	{
		return HasTimestampCapability() ?
			Units::TimeSpan(double(int(PeekUser(m_type == SMS_2614 ? 0xc9 : 0x76))) / PeekUser(m_type == SMS_2614 ? 0xc7 : 0x74)) : Units::TimeSpan(0);
	};

	Units::TimeSpan GetPpsError(void) const
	{
		return HasTimestampCapability() ?
			Units::TimeSpan(double(short(m_type == SMS_2614 ? (PeekUser(0xc1) & 0x0000fff0) >> 4 : PeekUser(0x71) & 0x0000ff00) >> 8) / PeekUser(m_type == SMS_2614 ? 0xc7 : 0x74)) : Units::TimeSpan(0);
	}

	unsigned long GetRevision(void) const { return PeekUser(0x03); }
	unsigned long GetSerialNumber(void) const;

	Units::TimeSpan GetSmoothedPpsDrift(void) const
	{
		return HasTimestampCapability() ?
			Units::TimeSpan(double(int(PeekUser(m_type == SMS_2614 ? 0xcc : 0x77))) / PeekUser(m_type == SMS_2614 ? 0xc7 : 0x74)) : Units::TimeSpan(0);
	};

	Units::TimeSpan GetSmoothedPpsError(void) const
	{
		return HasTimestampCapability() ?
			Units::TimeSpan(double(m_type == SMS_2614 ? short((PeekUser(0xcb) & 0x00000fff) << 4) >> 4 : short((PeekUser(0x71) & 0x000000ff) << 8) >> 8) / PeekUser(m_type == SMS_2614 ? 0xc7 : 0x74)) : Units::TimeSpan(0);
	}

	_Success_(drvInfo.found) static void GetTciWinDriverInfo(_In_ LPCTSTR desc, STciWinDrvInfo& drvInfo);
	Units::Timestamp GetTimeStamp(void) const;
	EFwType GetFwType(void) const { return m_type; }
	unsigned short GetTtlData(void) const { return unsigned short(PeekUser(0x47)); } 
	bool IsPpsDetected(void) const { return HasTimestampCapability() ? (PeekUser(m_type == SMS_2614 ? 0xc1 : 0x71) & 0x40000000) == 0 : false; }
	bool IsPpsLocked(void) const { return HasTimestampCapability() ? (PeekUser(m_type == SMS_2614 ? 0xc1 : 0x71) & 0x80000000) != 0 : false; }
	void ResetPpsDrift(void) { if(HasTimestampCapability()) PokeUser(m_type == SMS_2614 ? 0xc9 : 0x76, 0); }
	void SetBite(bool on, bool comb, unsigned char chMask);
	void SetSeconds(unsigned long seconds) { if(HasTimestampCapability()) PokeUser(m_type == SMS_2614 ? 0xc5 : 0x72, seconds); }
	void SetStreamingAgcWindowSize(unsigned long window, unsigned char ch);
	void SetTtlData(unsigned char data, unsigned char mask = 0xff) { PokeUser(0x47, data, mask); }

protected:
#ifdef DEBUG
    //Has the count of missing interrupt.
	static size_t s_missedIntr;
#endif
	// Types
	typedef void (*NewSamplesCallback)(const Ipp16sc* samples, unsigned int count, void* param);

	// Functions
	C32202001(void);
	virtual ~C32202001(void);
	void AdjustDecimation(_Inout_ unsigned long& decimation, bool adjustDown = false, Units::Frequency sampleRate = 0) const;
	void CollectCounts(_Inout_ unsigned int& count,
		_Out_writes_all_(4) unsigned int (&counts)[4],
		_Out_writes_all_(4) unsigned short (&thresh)[4], _In_range_(0, 1) unsigned char ch);
	void CollectCounts(_Inout_ unsigned int& count,
		_Out_writes_all_(4) unsigned int (&countA)[4],
		_Out_writes_all_(4) unsigned int (&countB)[4],
		_Out_writes_all_(4) unsigned short (&threshA)[4],
		_Out_writes_all_(4) unsigned short (&threshB)[4]);
	void CollectSamples(unsigned int count, unsigned int blockCount = 1, signed char chOrBoth = -1);
	void DoneWithSamples(unsigned char dma, bool multipleUsers = false, unsigned int sampleCount = 0);
	void EnableDma2Flush(bool enable);
	void EnableLoopback(bool enable, unsigned char ch, size_t offset);
	void EnableStreaming(bool enable, unsigned char ch, size_t packetSize);
	bool FifoHasOverrun(unsigned char dma);
	_Ret_opt_count_(count) const Ipp16sc* FindSamples(unsigned long long sampleCount, unsigned int count, unsigned char ch, DWORD timeout);
	float GetAdcFullScaleDbm(void) const { return ADC_FULL_SCALE_DBM; }
	float GetAdcFullScaleWatts(void) const { return ADC_FULL_SCALE_WATTS; }
	Ipp16sc* GetBuffer(unsigned char dma) const { ASSERT(dma > 3); return const_cast<Ipp16sc*>(reinterpret_cast<const volatile Ipp16sc*>(m_buffer[dma])); }
	size_t GetBufferSize(unsigned char dma) const { return 2 * m_bufSize[dma]; } // Samples, not words
	bool GetClockStatus(void) const;
	float GetCutoff(unsigned char ch) const;
	unsigned short GetDataOut(void) const;
	unsigned long GetDdcSampleCount(unsigned char source) const;
	unsigned long GetDecimation(unsigned char ch) const;
	CString GetDriverVersion(void) const;
	float GetLatency(Units::Frequency sampleRate, unsigned long decimation) const;
	unsigned int GetSampleCount(unsigned char dma) const;
	void GetSampleCount(_Out_writes_all_(NUM_RF_CHANS) unsigned int(&count)[NUM_RF_CHANS]) const;
	Units::Frequency GetSampleRate(void) const;
	_Success_(return) bool GetThresholdCounters(_Out_writes_all_(4) unsigned int (&counts)[4],
		_Out_writes_all_(4) unsigned short(&thresh)[4], unsigned char ch);
	_Success_(return) bool GetThresholdCounters(_Out_writes_all_(4) unsigned int(&countA)[4],
		_Out_writes_all_(4) unsigned int(&countB)[4],
		_Out_writes_all_(4) unsigned short(&threshA)[4],
		_Out_writes_all_(4) unsigned short(&threshB)[4]);
	CString GetVersion(void);
	std::vector<float> GetTemperatures(void) const;
	std::vector<std::array<float, 2> > GetTempLimits(void) const;
	std::vector<std::array<float, 2> > GetVoltageLimits(void) const;
	std::vector<float> GetVoltages(void) const;
	bool HasProblem(void) const;
	bool HasTimestampCapability(void) const { return m_type == SMS_2614 || m_type == BLACKBIRD; }
	void Init(void);
	bool IsMaster(void) const { return m_master; }
	bool IsPresent(void) const { return m_handle != INVALID_HANDLE_VALUE; }
	void MarkSamplesInUse(unsigned int sampleCount, unsigned int count, unsigned char dma, unsigned char numUsers = 1);
	void MeasureNoiseDbm(_Out_writes_all_(2) float(&power)[2]);
	void MeasureToneDbm(_Out_writes_all_(2) float(&power)[2]);
	void ProcessSamples(_In_count_(count) const Ipp16sc* samples, unsigned int count, unsigned char dma);
	void RegisterNewSamplesCallback(_In_ NewSamplesCallback callback, _In_opt_ void* param, unsigned char ch);
	void ResetDdcFifo(unsigned char source);
	void ResetDdcSampleCount(unsigned char source);
	void ResetSampling(void);
	void ResetThresholdCounters(signed char chOrBoth = -1);
	void SetDataOut(unsigned short data);
	bool SetDecimation(unsigned long decimation, unsigned char ch, unsigned char gainShift = 0);
	void SetInterruptModeration(unsigned long rate, unsigned char dma);
	void SetInput(unsigned char source, unsigned char ch);
	void SetReverseDmaRate(unsigned char rate, unsigned char dma);
	void SetSequencer(const std::vector<std::pair<unsigned short, unsigned long>>& block);
	void SetSequencer(const std::vector<unsigned short>& pattern, unsigned int delay, unsigned long samples);
	void SetThresholdCounters(_In_reads_(4) const unsigned short (&threshA)[4],
		_In_reads_(4) const unsigned short (&threshB)[4]);
	void Settle(bool wait);
	void StartSampling(unsigned int count, unsigned int blockCount = 1, bool delayedStart = false, DATE startTime = 0);
	void StartSampling(unsigned int count, unsigned char ch, unsigned int blockCount = 1);
	void StartSequencer(signed char chOrBoth = -1);
	void Tune(Units::Frequency freq, bool invertSpectrum, bool shift, unsigned char ch);
	void Tune(Units::Frequency freq, Units::Frequency sampleRate, unsigned char ch);
	void TuneDemod(Units::Frequency freq, bool invertSpectrum);
	void UnregisterNewSamplesCallback(unsigned char ch);
	void UseSampleRate(Units::Frequency sampleRate);
	void WaitForBufferSpace(unsigned int count, unsigned char dma) const;
	void WaitForSequencer(unsigned char ch = 1);
#ifdef DMA_STATS_LOG
	void CopyCtrs(_Out_writes_all_(NUM_DMAS * 5) int(&buf)[NUM_DMAS * 5]) { for (int i = 0; i < NUM_DMAS * 5; i++) { buf[i] = std::atomic_exchange(&m_dmaActivityCnt[i], 0); } };
#endif

private:
	// Constants
	static const int DMA_PIPELINE_SIZE_LONGLONGS = 2048; // TODO
	static const unsigned int MIN_QUEUED_BLOCKS = 2;
	static LPCTSTR MUTEX_NAME;
	static const unsigned int THREAD_TIMEOUT = 10000; // ms
	static LPCTSTR WBDIGI_DEVICE;

	// Types
	struct SECURITY_ATTRIBUTES_ALL_ACCESS : public SECURITY_ATTRIBUTES
	{
		SECURITY_ATTRIBUTES_ALL_ACCESS(void)
		{
			if(!InitializeSecurityDescriptor(&m_secDesc, SECURITY_DESCRIPTOR_REVISION))
			{
				AfxThrowResourceException();
			}

#pragma warning(suppress : 6248) // Null DACL
			if(!SetSecurityDescriptorDacl(&m_secDesc, TRUE, nullptr, TRUE))
			{
				AfxThrowResourceException();
			}

			nLength = sizeof(SECURITY_ATTRIBUTES);
			lpSecurityDescriptor = &m_secDesc;
			bInheritHandle = FALSE;

			return;
		}

	private:
		SECURITY_DESCRIPTOR m_secDesc;
	};

	struct SInUse
	{
		unsigned int sampleCount;
		unsigned char numUsers;
	};

	class CRegLock
	{
	public:
		CRegLock(HANDLE handle);
		~CRegLock(void);

	private:
		HANDLE m_handle;
	};

	// Functions
	unsigned long CalcFreqWord(Units::Frequency freq, Units::Frequency sampleRate) const;
	static void Deinterleave(_In_reads_(count) const volatile unsigned long long* src, size_t count, unsigned char ch, _Inout_ Ipp16scVec& dest,
		size_t offset = 0);
	void GetThresholds(_Out_writes_all_(4) unsigned short (&thresh)[4], unsigned char ch) const;
	void GetThresholds(_Out_writes_all_(4) unsigned short(&threshA)[4], _Out_writes_all_(4) unsigned short(&threshB)[4]) const;
	static float InvErf(float x);
	void OnFail(DWORD error, _In_ LPCTSTR description);
	void OnProcessed(_In_range_(4, 7) unsigned char dma, size_t offset, size_t size);
	void OnSamples(_In_range_(0, 2) unsigned char dma, size_t offset, size_t size);
	unsigned long PeekDma(unsigned short reg) const;
	unsigned long PeekUser(unsigned char reg) const;
	virtual void PingWatchDog(void) {} // Override if watchdog is needed
	void PokeDma(unsigned char reg, unsigned long data, unsigned long mask = 0xfffffffful);
	void PokeUser(unsigned char reg, unsigned long data, unsigned long mask = 0xfffffffful);
	void StartThreads(void);
	void StopThreads(void);
	void Thread(unsigned long sourceMask);

	// Data
	const volatile unsigned long long* m_buffer[NUM_DMAS]; // Memory-mapped buffer pointers
	int m_bufSize[NUM_DMAS]; // Buffer size in long long words
	NewSamplesCallback m_callbacks[NUM_DMAS]; // Callback functions
	Ipp16scVec m_callbackBuffer[NUM_DMAS];
	void* m_callbackParams[NUM_DMAS]; // Params for callbacks
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
	mutable CCriticalSection m_dataCritSect[NUM_DMAS];
#else
	mutable CRWLock m_dataCritSect[NUM_DMAS];
#endif
	unsigned long m_decimation[NUM_CHANS];
	std::map<unsigned long, unsigned long> m_decimations;
	size_t m_dmaCount[NUM_DMAS]; // Each count is 64 bits; 2 interleaved 16-bit samples for DMA0 for SMS, 2 sequential samples otherwise
	size_t m_dmaCountOffset[NUM_DMAS];
	Ipp16scVec m_findBuffer[NUM_CHANS];
	Units::Frequency m_freq[NUM_CHANS];
	HANDLE m_handle;
	bool m_invertSpectrum[NUM_CHANS];
	bool m_master;
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
	mutable CEvent m_moreBufferSpace;
#else
	mutable CConditionVariable m_moreBufferSpace;
#endif
	bool m_needRetune;
	bool m_needWait;
	unsigned int m_newestInUse[NUM_RF_CHANS];
	const volatile unsigned long long* m_newestSample[NUM_DMAS];
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
	mutable CEvent m_newSamples[NUM_DMAS];
#else
	mutable CConditionVariable m_newSamples[NUM_DMAS];
#endif
	std::deque<SInUse> m_oldestInUse[NUM_RF_CHANS]; // Sample count of oldest in-use samples
//	LARGE_INTEGER m_perfFreq;
//	UINT m_periodMin;
	volatile unsigned long* m_regs;
	Units::Frequency m_sampleRate;
	unsigned long m_sequencerBlockSize;
	unsigned char m_sequencerCount;
	CEvent m_sequencerDone[NUM_RF_CHANS];
	unsigned long m_sequencerLength;
	DWORD m_sequencerTime;
	bool m_shift[NUM_CHANS];
	CEvent m_shutdown;
//	LARGE_INTEGER m_startTime;
	bool m_streaming[3];
	CWinThread* m_thread[4];
//	LARGE_INTEGER m_time1, m_time2, m_time3, m_time4;
	std::unique_ptr<CTimer> m_timer[2];
	EFwType m_type;
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
	mutable CCriticalSection m_wdCritSect;
#else
	mutable CRWLock m_wdCritSect;
#endif
	mutable CMutex m_mutex; // Cross-process mutex. Must be declared last so that GetLastError() can be used in the constructor body

#ifdef DMA_STATS_LOG
	std::atomic_uint m_dmaActivityCnt[NUM_DMAS*5]; // Counters to view DMA activity 
#endif
};
