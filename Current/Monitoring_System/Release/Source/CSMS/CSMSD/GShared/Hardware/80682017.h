/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "Vme.h"

class C80682017
:
	// Inheritance
	protected virtual CVme
{
public:
	// Constants
	static const unsigned char ATTEN_STEP = 6; // dB
	static const unsigned long BITE_FREQ_HZ = 6250000; // Hz
	static const float IF_MUX_LOSS_HF; // dB
	static const float IF_MUX_LOSS_UHF; // dB
	static const unsigned char MAX_ATTEN = 18; // dB
	static const unsigned char MIN_ATTEN = 0; // dB
	static const signed char MAX_GAIN = 20; // dB

	// Types
	enum EPoeHeadType { UNKNOWN = -1, VUHF_76412017 = 0, HF_72362002 = 4, USHF_76472017 = 5, USHF_76472017_NOEXT = 6 };
	enum EPoePort { UHF = 0, VHF = 1, HF = 2,
					NUM_POE_PORT_TYPE // This must always be the last member
				  };

	struct SRemoteStatus
	{
		EPoeHeadType headType;
		unsigned char version;
		unsigned char antType;
		unsigned int loopback;
		bool powerAlarmOr10MHzOk; // For whip power on 7236-2002 or 10 MHz OK on 7234-2004
		bool whipPresentOrLoOk; // For whip on 7236-2002 or LO OK on 7234-2002
		float voltages[3]; // +15 and -12 for 7641-2017; +12 and +5 for 7236-2002; +12, +5 and +8 for 7234-2002
		float temp;
	};

	// Functions
	unsigned char GetRemoteVersion(EPoePort poePort) const { return m_remoteVersion[poePort]; }
	static void PingCpuOk(void) { if(m_instance != nullptr) m_instance->PingCpuOk_(); }
	static void Refresh(void) { if(m_instance != nullptr) m_instance->Refresh_(); }

protected:
	// Types
	enum ECpuOk { ON = 0x0, SLOW_FLASH = 0x2, MEDIUM_FLASH = 0x4, FAST_FLASH = 0x6 };
	enum ELed { DSP_A = 0x0001, DSP_B = 0x0002, TEN_MHZ_OK = 0x0004, AD_CLOCK_OK = 0x0008, SYNTH_FAULT = 0x0010, OVERTEMP = 0x0020 };

	struct SCardStatus
	{
		unsigned short id : 3;
		unsigned short cardType : 3;
		unsigned short cardClass : 2;
		unsigned short inhibit : 1;
		unsigned short cpuFault : 1;
		unsigned short uhfPoeFault : 1;
		unsigned short vhfPoeFault : 1;
		unsigned short hfPoeFault : 1;
		unsigned short version : 3;
	};

	struct SShMem
	{
		unsigned short statusReg;
		unsigned short ledPowerReg;
		unsigned short muxControlReg;
		unsigned char uhfSwitchReg;
		unsigned char vhfSwitchReg;
		unsigned char hfSwitchReg;
	};

	// Functions
	C80682017(void); // Can only be constructed by a derived class
	virtual ~C80682017(void);
	unsigned int GetAntennaData(EPoePort poePort) const;
	unsigned char GetAtten(void) const;
	SCardStatus GetCardStatus(void) const;
	float GetGainRatio(void) const;
	void GetIfMux(_Out_ bool& hf, _Out_ bool& lowBand) const;
	EPoeHeadType GetPoeHeadType(EPoePort poePort) const { return m_poeHeadType[poePort]; }
	SRemoteStatus GetRemoteStatus(EPoePort poePort);
	unsigned char GetRemoteAntType(EPoePort poePort) const { return m_remoteAntType[poePort]; }
	unsigned char GetVersion(void) const { return m_version; }
	void GetVoltages(_Out_writes_all_(8) float (&voltages)[8], _Out_ bool& no48V) const;
	bool IsPresent(void) const { return m_present; }
	void Set48V(bool on);
	void SetAntennaControl(EPoePort poePort, bool vme, unsigned char data);
	unsigned char SetAtten(unsigned char atten);
	void SetCpuOk(ECpuOk cpuOk);
	void SetIfMux(bool hf, bool lowBand);
	void SetLed(ELed led, bool on);
	void SetRemote(unsigned char output, bool on);
	void SetTestTone(bool ref, bool sample);
	void SetWatchDog(bool on);
	void ToggleLed(ELed led);
	bool UsingPoe(void) const { return m_usingPoe; }
	void WriteShMemRegs(void);

	// Data
	mutable CMutex m_mutex;
	volatile SShMem* m_shMem;

private:
	// Constants
	static const unsigned int CARD_CLASS = 0xd;
	static const unsigned int CARD_TYPE_POE = 0x0;
	static const unsigned int CARD_TYPE_PARALLEL = 0x3;
	static const unsigned int MAX_INSTANCES = 1;
	static const unsigned int VME_BASE_ADDR_POE = (0x770000 | (CARD_CLASS << 12) | (CARD_TYPE_POE << 8));
	static const unsigned int VME_BASE_ADDR_PARALLEL = (0x770000 | (CARD_CLASS << 12) | (CARD_TYPE_PARALLEL << 8));
	static const unsigned int STATUS_REG = 0x00;
	static const unsigned int MUX_CONTROL_REG = 0x02;
	static const unsigned int LED_POWER_REG = 0x04;
	static const unsigned int UHF_SWITCH_REG = 0x08;
	static const unsigned int VHF_SWITCH_REG = 0x0a;
	static const unsigned int HF_SWITCH_REG = 0x0c;
	static LPCTSTR MUTEX_NAME;
	static LPCTSTR SHMEM_NAME;

	// Functions
	static float adc2temp(unsigned short adc);
    template<typename T> static T Filter(_Inout_ const T (&data)[3]);
	static void On80682017Interrupt(_In_ void* param, unsigned int) { static_cast<C80682017*>(param)->On80682017Interrupt(); }
	virtual void On80682017Interrupt(void);
	unsigned short Peek(unsigned char reg) const { return CVme::Peek(m_address + reg); }
	void PingCpuOk_(void);
	void Poke(unsigned char reg, unsigned short data) { CVme::Poke(m_address + reg, data); }
	void Refresh_(void);
	virtual void SetSettleTime(unsigned int) {}

	// Data
	unsigned long m_address;
	bool m_first;
	static C80682017* m_instance;
	EPoeHeadType m_poeHeadType[NUM_POE_PORT_TYPE];
	bool m_present;
	unsigned char m_remoteAntType[NUM_POE_PORT_TYPE];
	unsigned char m_remoteVersion[NUM_POE_PORT_TYPE];
	bool m_usingPoe;
	unsigned char m_version;
};


//////////////////////////////////////////////////////////////////////
//
// Use a median filter to remove spurious data
//
template<typename T> T C80682017::Filter(_Inout_ const T (&data)[3])
{
	size_t maxIdx = 0;
	size_t minIdx = 0;

	for(size_t i = 1; i < 3; ++i)
	{
		if(data[i] > data[maxIdx])
		{
			maxIdx = i;
		}

		if(data[i] < data[minIdx])
		{
			minIdx = i;
		}
	}

	if(maxIdx == minIdx)
	{
		return data[maxIdx];
	}
	else
	{
		size_t middleIdx = 3 - maxIdx - minIdx;

		if(data[middleIdx] > (data[maxIdx] + data[minIdx]) / 2)
		{
			return (data[middleIdx] + data[maxIdx]) / 2;
		}
		else
		{
			return (data[minIdx] + data[middleIdx]) / 2;
		}
	}
}


