/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2012-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include <math.h>

#include "DownConverter.h"
#include "Units.h"
#include "Vme.h"

class C26142001
:
	// Inheritance
	public CDownConverter, 
	protected virtual CVme
{
	// Friends
	friend class C26142002;

public:
	// Constants
	static const unsigned long ADC_CLOCK_RATE = 153600000; // Hz
	static const unsigned char ATTEN_STEP = 2; // dB
	static const unsigned char MAX_ATTEN = 84; // dB
	static const signed char MAX_GAIN = 69; // dB
	static const unsigned char MIN_CONGESTED_ATTEN = 10; // dB
	static const unsigned long NARROW_BW_HZ = 4000000; // Hz
	static const unsigned long RF_HIGH_FREQ_HZ = 3000000000; // Hz
	static const unsigned long RF_LOW_FREQ_HZ = 20000000; // Hz
	static const unsigned long WIDE_BW_HZ = 40000000; // Hz

protected:
	// Constants
	static const SBandSettings BAND_SETTINGS_[];
	static const float NOMINAL_TEMP; // �C

	// Functions
	C26142001(void); // Can only be constructed by a derived class
	template<size_t N> C26142001(const CDownConverter::EGainMode (&gainModes)[N]);
	virtual ~C26142001(void);
	unsigned char GetAtten(unsigned char id) const { return m_atten[id]; }
	unsigned short GetAttenRaw(unsigned char id) const { return Peek(id, ATTEN_REG); }
	static size_t GetBand(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ Units::FreqPair& freqLimits);
	bool GetDetector(unsigned char id) const;
	static float GetGainAdjustmentDb(float temp) { return (temp - NOMINAL_TEMP) * GAIN_SLOPE; }
	CDownConverter::EGainMode GetGainMode(Units::Frequency freq) const { return CDownConverter::GetGainMode(freq, BAND_SETTINGS); }
	float GetGainRatio(float temp, unsigned char id) const;
	unsigned short GetSwitchReg(unsigned char id) const { return Peek(id, SWITCH_REG); }
	unsigned char GetVersion(unsigned char id) const;
	bool IsPresent(unsigned char id) const { return m_present[id]; }
	bool IsWideband(unsigned char id) const { return m_wideband[id]; }
	unsigned char MinAtten(CDownConverter::EGainMode mode) const { return mode == CDownConverter::CONGESTED ? MIN_CONGESTED_ATTEN : 0; }
	unsigned char MinAtten(Units::Frequency freq) const { return MinAtten(GetGainMode(freq)); }
	unsigned short Peek(unsigned char id, unsigned char reg) const { return CVme::Peek(m_address[id] + reg); }
	void Poke(unsigned char id, unsigned char reg, unsigned short data) { CVme::Poke(m_address[id] + reg, data); }
	unsigned char SetAtten(unsigned char atten, Units::Frequency freq, signed char idOrAll = -1);
	unsigned char SetAtten(unsigned char atten, CDownConverter::EGainMode mode, signed char idOrAll = -1);
	void SetAttenRaw(unsigned short bits, signed char idOrAll = -1);
	void SetBand(unsigned char band, signed char idOrAll = -1);
	unsigned long SetCalTone(bool on, signed char idOrAll = -1);
	void SetDetector(bool on, signed char idOrAll = -1);
	void SetInput(unsigned char input, signed char idOrAll = -1);
	void SetWideBw(bool wide, signed char idOrAll = -1);
	void Tune(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ Units::FreqPair& freqLimits,
		_Out_ size_t& band, signed char idOrAll = -1);

private:
	// Constants
	static const BandSettings BAND_SETTINGS;
	static const unsigned int CARD_TYPE_NARROW = 0x1;
	static const unsigned int CARD_TYPE_WIDE = 0x0;

	//Cal-tone on settle time is 17 msec = 17,000 usec.
	//@note: Email from ECS (Marty Rosa) indicates start-up time of 15 msec
	//	     for ECS-3953M-1250-BN synthesizer.
	static const unsigned int CAL_TONE_ON_SETTLE_TIME = 17000;
	static const unsigned int CAL_TONE_OFF_SETTLE_TIME = 200; //Cal-tone off settle time is 0.2 msec = 200 usec.
	static const unsigned int MAX_INSTANCES = 3;
	static const unsigned int VME_BASE_ADDR_NARROW = (0x780000 | (CARD_TYPE_NARROW << 12));
	static const unsigned int VME_BASE_ADDR_WIDE = (0x780000 | (CARD_TYPE_WIDE << 12));
	static const unsigned int STATUS_REG = 0x00;
	static const unsigned int SWITCH_REG = 0x02;
	static const unsigned int ATTEN_REG = 0x04;
	static const float GAIN_SLOPE; // dB / �C
	static LPCTSTR MUTEX_NAME;

	// Functions
	_Ret_ virtual const CDownConverter::SReceiverGainModes& GetGainModes(void) const { return m_gainModes; }
	virtual void SetSettleTime(unsigned int) {}

	// Data
	unsigned long m_address[MAX_INSTANCES];
	unsigned char m_atten[MAX_INSTANCES];
	unsigned short m_attenReg[MAX_INSTANCES];
	bool m_first;
	CDownConverter::SReceiverGainModes m_gainModes;
	mutable CMutex m_mutex;
	bool m_present[MAX_INSTANCES];
	unsigned short m_switchReg[MAX_INSTANCES];
	bool m_wideband[MAX_INSTANCES];
};

// Use selectany to put this here rather than in the .cpp so the size of the array can be determined by users of the class
__declspec(selectany) const CDownConverter::SBandSettings C26142001::BAND_SETTINGS_[] =
{ // low 20 dB, low 1 dB, high 1 dB, high 20 dB, data
	{   12000000,   20000000,   45000000,   80000000, 0x0000 },
	{   23000000,   35000000,   85000000,  158000000, 0x0008 },
	{   41000000,   70000000,  165000000,  251000000, 0x0010 },
	{  103000000,  135000000,  270000000,  395000000, 0x0018 },
	{  147000000,  230000000,  440000000,  622000000, 0x0020 },
	{  313000000,  400000000,  620000000,  700000000, 0x0028 },
	{  518000000,  580000000,  980000000, 1160000000, 0x0002 },
	{  816000000,  940000000, 1420000000, 1640000000, 0x000a },
	{ 1130000000, 1380000000, 1720000000, 2040000000, 0x0004 },
	{ 1250000000, 1680000000, 2120000000, 2450000000, 0x000c },
	{ 1580000000, 2080000000, 2520000000, 2900000000, 0x0014 },
	{ 1860000000, 2480000000, 3020000000, 3480000000, 0x001c }
};


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
template<size_t N> C26142001::C26142001(const CDownConverter::EGainMode (&gainModes)[N])
:
	CVme(),
	m_gainModes(&gainModes[0], &gainModes[_countof(BAND_SETTINGS_)]),
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get())
{
	static_assert(N >= _countof(BAND_SETTINGS_), "Not enough values in gainModes array");
	m_first = !(GetLastError() == ERROR_ALREADY_EXISTS); // Status from m_mutex construction

	for(unsigned char id = 0; id < MAX_INSTANCES; ++id)
	{
		m_address[id] = VME_BASE_ADDR_WIDE + 256 * id;
		m_present[id] = ((Peek(id, STATUS_REG) & 0x00ffu) == ((CARD_TYPE_WIDE << 4) | id));
		m_wideband[id] = true;

		if(!m_present[id])
		{
			m_address[id] = VME_BASE_ADDR_NARROW + 256 * id;
			m_present[id] = ((Peek(id, STATUS_REG) & 0x00ffu) == ((CARD_TYPE_NARROW << 4) | id));
			m_wideband[id] = false;
		}

		if(m_present[id])
		{
			if(m_first)
			{
				m_attenReg[id] = 0x0fff;
				Poke(id, ATTEN_REG, m_attenReg[id]);
				m_switchReg[id] = 0x0000;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetAtten(MAX_ATTEN, 100000000, id);
				SetInput(0, id);
				SetCalTone(false, id);
				SetDetector(false, id);
				Units::FreqPair freqLimits;
				size_t band;
				Tune(100000000, NARROW_BW_HZ, OPTIMUM, freqLimits, band, id);
			}
			else
			{
				m_attenReg[id] = Peek(id, ATTEN_REG);
				m_switchReg[id] = Peek(id, SWITCH_REG);
			}
		}
	}

	return;
}
