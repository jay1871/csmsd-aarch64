/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "StdAfx.h"

#include <memory>

#include "26222001.h"
#include "Digitizer.h"
#include "Utility.h"

const Units::Frequency C26222001::C_2622_BW(C26222001::RF_HIGH_FREQ_HZ);
const float C26222001::IF_MUX_LOSS_2612RX_UHF = 0.8f; // dB
const float C26222001::IF_MUX_LOSS_2614RX_UHF = 0.8f; // dB

//Create new GUID for 2622, no longer shared 5141 GUID since 2622 can be
// installed internal C-Band in 5185.
LPCTSTR C26222001::MUTEX_NAME = _T("Global\\TCI.{087DFEA1-B000-4325-AD77-6C152FD38A38}");

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C26222001::C26222001(void)
:
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get()),
	m_muxSet2Hf(false)
{
	m_first = !(GetLastError() == ERROR_ALREADY_EXISTS); // Status from m_mutex construction

	if(m_first)
	{
		SetAtten(MAX_ATTEN);
		SetMux(false);
	}
	else
	{
        //Should only be created once.
		ASSERT(false);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C26222001::~C26222001(void)
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the receiver gain
//
float C26222001::GetGainRatio(float max_adj) const
{
	return pow(10.0f, (float(MAX_GAIN - m_atten) + max_adj)/ 10.0f);
}


//////////////////////////////////////////////////////////////////////
//
// Set attenuation
//
unsigned char C26222001::SetAtten(unsigned char atten)
{
	if(atten > MAX_ATTEN)
	{
		atten = MAX_ATTEN;
	}

	SetAttenRaw(atten / ATTEN_STEP);

	return m_atten;
}


//////////////////////////////////////////////////////////////////////
//
// Set attenuation
//
void C26222001::SetAttenRaw(unsigned char bits)
{
	m_digitizer->SetTtlData(bits, 0x1f);
	m_atten = ATTEN_STEP * bits;
	SetSettleTime(5);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the mux
//
void C26222001::SetMux(bool hf)
{
	//Save the HF setting since C26222001 SetTtlData shared the same address
    // space as C51412010 SetTtlData.
	m_muxSet2Hf = hf;
	m_digitizer->SetTtlData(hf ? 0x20 : 0x00, 0x20);
	SetSettleTime(5);

	return;
}
