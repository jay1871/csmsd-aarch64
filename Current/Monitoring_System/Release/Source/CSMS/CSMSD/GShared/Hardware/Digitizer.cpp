/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2011-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "StdAfx.h"

#include <array>
#include <ipp.h>
#include <memory>
#include <vector>

#include "32202001.h"
#include "Digitizer.h"
#include "P7140.h"
#include "WatchDog.h"


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CDigitizer::CDigitizer(void)
:
	C32202001(),
	CP7140(),
	m_prevDataOut(0),
	m_type{C32202001::IsPresent() ? TCI3220 : (CP7140::IsPresent() ? P7140 : NONE)},
	m_watchdog()
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CDigitizer::~CDigitizer(void)
{
	return;
}


/////////////////////////////////////////////////////////////////////
//
// Adjust decimation to closest legal value
//
void CDigitizer::AdjustDecimation(_Inout_ unsigned long& decimation, _In_ bool adjustDown) const
{
	//@note: No hardware register is accessed therefore no need to protect with
	//	     CCriticalSection.
	switch(m_type)
	{
	case P7140:
		CP7140::AdjustDecimation(decimation, adjustDown);
		break;

	case TCI3220:
		C32202001::AdjustDecimation(decimation, adjustDown);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Collect threshold counts
//
void CDigitizer::CollectCounts(unsigned int count,
	_Out_writes_all_(4) unsigned int (&countA)[4],
	_Out_writes_all_(4) unsigned int (&countB)[4],
	_Out_writes_all_(4) unsigned short (&threshA)[4],
	_Out_writes_all_(4) unsigned short (&threshB)[4])
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. 3220 has CRegLock to lock driver registers.
	switch( m_type )
	{
	case P7140:
		{
			unsigned short maxA;
			unsigned short maxB;
			CP7140::CollectCounts(count, countA, countB, threshA, threshB, maxA, maxB);
		}

		break;

	case TCI3220:
		C32202001::CollectCounts(count, countA, countB, threshA, threshB);
		break;

	default:
		for(auto i = 0; i < 4; ++i)
		{
			countA[i] = 0;
			countB[i] = 0;
			threshA[i] = 0;
			threshB[i] = 0;
		}

		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Collect samples
//
void CDigitizer::CollectSamples(unsigned int count, unsigned int blockCount, signed char chOrBoth)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread does not call this method.
	switch( m_type )
	{
	case P7140:
		CP7140::CollectSamples(count);
		break;

	case TCI3220:
		C32202001::CollectSamples(count, blockCount, chOrBoth);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Mark samples as OK to overwrite
//
void CDigitizer::DoneWithSamples(unsigned char dma)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. C32202001::DoneWithSamples has its own CCriticalSection protection.
	switch( m_type )
	{
	case P7140:
		ASSERT(dma == 0);
		CP7140::DoneWithSamples();
		break;

	case TCI3220:
		C32202001::DoneWithSamples(dma);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Enable streaming on a channel
//
void CDigitizer::EnableStreaming(bool enable, unsigned char ch, size_t packetSize)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. It is only accessed by CHwControl at dtor & thread exiting.
	switch( m_type )
	{
	case P7140:
		CP7140::SetGate(enable, ch == 2 ? 1 : 0);
		break;

	case TCI3220:
		C32202001::EnableStreaming(enable, ch == 0 ? 1 : ch, packetSize);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Enable the watchdog
//
void CDigitizer::EnableWatchDog(bool enable)
{
	//@note: No need to protect with CCriticalSection. 3220 Hardware registers
	//		 are not accessed.
	switch(m_type)
	{
	case P7140:
		CP7140::EnableWatchDog(enable);
		break;

	case TCI3220:
		if(enable)
		{
			m_watchdog = std::make_unique<CSingleton<CWatchDog>>();
			m_watchdogPingSource = (*m_watchdog)->RegisterPingSource(_T("3220 Digitizer DMA"));
		}
		else
		{
			(*m_watchdog)->UnregisterPingSource(m_watchdogPingSource.exchange(nullptr));
			m_watchdog = nullptr;
		}

		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Check for FIFO overrun
//
bool CDigitizer::FifoHasOverrun(unsigned char ch)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. Only a single access to 3220 hardware register.
	//		 3. CAudio thread not accessing this method.
	switch(m_type)
	{
	case P7140:
		return CP7140::FifoHasOverrun(ch);

	case TCI3220:
		return C32202001::FifoHasOverrun(ch == 0 ? 1 : ch);

	default:
		return FALSE;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Find samples in buffer (waiting if necessary)
//
_Ret_opt_count_(count) const Ipp16sc* CDigitizer::FindSamples(unsigned int sampleCount, unsigned int count, unsigned char ch, DWORD timeout)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. C32202001::FindSamples has its own CCriticalSection protection.
	switch( m_type )
	{
	case P7140:
		return CP7140::FindSamples(sampleCount, count, ch,timeout);

	case TCI3220:
		return C32202001::FindSamples(sampleCount, count, ch, timeout);

	default:
		return nullptr;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get A/D max counts
//
int CDigitizer::GetAdcFullScaleCounts(void) const
{
	//@note: No need to protect with CCriticalSection for method that
	//		 returning constant value.
	switch(m_type)
	{
	case P7140:
		return CP7140::ADC_FULL_SCALE_COUNT;

	case TCI3220:
		return C32202001::ADC_FULL_SCALE_COUNT;

	default:
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get A/D conversion factor
//
float CDigitizer::GetAdcFullScaleWatts(void) const
{
	//@note: No need to protect with CCriticalSection for method that
	//		 returning constant value.
	switch( m_type )
	{
	case P7140:
		return CP7140::GetAdcFullScaleWatts();

	case TCI3220:
		return C32202001::GetAdcFullScaleWatts();

	default:
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the external clock status
//
bool CDigitizer::GetClockStatus(void) const
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		return CP7140::GetClockStatus();

	case TCI3220:
		return C32202001::GetClockStatus();

	default:
		return false;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the cutoff as a fraction of the decimated bandwidth
//
float CDigitizer::GetCutoff(unsigned char ch) const
{
	//@note: No need to protect with CCriticalSection for method that
	//		 returning constant value.
	switch( m_type )
	{
	case P7140:
		return CP7140::GetFirBw(ch) / 100.0f;

	case TCI3220:
		return C32202001::GetCutoff(ch == 0 ? 1 : ch);

	default:
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get LVDS data
//
unsigned short CDigitizer::GetDataOut(void) const
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. Only a single access to 3220 hardware register.
	//		 3. CAudio thread not accessing this method.
	switch(m_type)
	{
	case P7140:
		return CP7140::GetDataOut();

	case TCI3220:
		return C32202001::GetDataOut();

	default:
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get decimation
//
unsigned long CDigitizer::GetDecimation(unsigned char ch) const
{
	//@note: No hardware register is accessed therefore no need to protect with
	//	     CCriticalSection.
	switch(m_type)
	{
	case P7140:
		return CP7140::GetDecimation(ch);

	case TCI3220:
		return C32202001::GetDecimation(ch == 0 ? 1 : ch);

	default:
		return 1;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get driver version
//
CString CDigitizer::GetDriverVersion(void) const
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		return CP7140::GetDriverVersion();

	case TCI3220:
		return C32202001::GetDriverVersion();

	default:
		return _T("N/A");
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the FIR bandwidth
//
unsigned char CDigitizer::GetFirBw(unsigned char ch) const
{
	//@note: No hardware register is accessed therefore no need to protect with
	//	     CCriticalSection.
	switch(m_type)
	{
	case P7140:
		return CP7140::GetFirBw(ch);

	case TCI3220:
		return 80; // Only value supported

	default:
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get FPGA firmware date
//
COleDateTime CDigitizer::GetFpgaFwDate(void) const
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. Only a single access to 3220 hardware register.
	//		 3. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		return CP7140::GetFpgaFwDate();

	case TCI3220:
		return C32202001::GetFpgaFwDate();

	default:
		return time_t(0);
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get FPGA firmware revision
//
unsigned long CDigitizer::GetFpgaFwRev(void) const
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. Only a single access to 3220 hardware register.
	//		 3. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		return CP7140::GetFpgaFwRev();

	case TCI3220:
		return C32202001::GetFpgaFwRev();

	default:
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the DDR latency in seconds
//
float CDigitizer::GetLatency(Units::Frequency sampleRate, unsigned long decimation)
{
	//@note: No need to protect with CCriticalSection for method that
	//		 returning constant value.
	switch( m_type )
	{
	case P7140:
		return CP7140::GetLatency(sampleRate, decimation);

	case TCI3220:
		return C32202001::GetLatency(sampleRate, decimation);

	default:
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get sample counter values
//
void CDigitizer::GetSampleCount(_Out_writes_all_(NUM_DATA_CHANS) unsigned int(& count)[NUM_DATA_CHANS]) const
{
	//@note: No hardware register is accessed therefore no need to protect with
	//	     CCriticalSection.
	switch( m_type )
	{
	case P7140:
		CP7140::GetSampleCount(count);
		break;

	case TCI3220:
		C32202001::GetSampleCount(count);
		break;

	default:
		for(auto i = 0; i < NUM_DATA_CHANS; ++i)
		{
			count[i] = 0;
		}

		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the sample rate in use
//
Units::Frequency CDigitizer::GetSampleRate(void) const
{
	//@note: No hardware register is accessed therefore no need to protect with
	//	     CCriticalSection.
	switch( m_type )
	{
	case P7140:
		return CP7140::GetSampleRate();

	case TCI3220:
		return C32202001::GetSampleRate();

	default:
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get board temperatures
//
std::vector<float> CDigitizer::GetTemperatures(void) const
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		return CP7140::GetTemperatures();

	case TCI3220:
		return C32202001::GetTemperatures();

	default:
		return std::vector<float>();
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get board temperatures
//
std::vector<std::array<float, 2> > CDigitizer::GetTempLimits(void) const
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		return CP7140::GetTempLimits();

	case TCI3220:
		return C32202001::GetTempLimits();

	default:
		return std::vector<std::array<float, 2> >();
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get threshold counter values
//
_Success_(return) bool CDigitizer::GetThresholdCounters(_Out_writes_all_(4) unsigned int(&countA)[4],
								  _Out_writes_all_(4) unsigned int (&countB)[4],
								  _Out_writes_all_(4) unsigned short (&threshA)[4],
								  _Out_writes_all_(4) unsigned short (&threshB)[4])
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		{
			unsigned short maxA;
			unsigned short maxB;
			CP7140::GetThresholdCounters(countA, countB, threshA, threshB, maxA, maxB);
			return true;
		}

		break;

	case TCI3220:
		return C32202001::GetThresholdCounters(countA, countB, threshA, threshB);
		break;

	default:
		return false;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get TTL I/O data (3220 only)
//
unsigned short CDigitizer::GetTtlData(void) const
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. Only a single access to 3220 hardware register.
	//		 3. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		return 0;

	case TCI3220:
		return C32202001::GetTtlData();

	default:
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get version string
//
CString CDigitizer::GetVersion(void)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2  3220 hardware register with constant values are returned.
	//		 3. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		return CP7140::GetVersion();

	case TCI3220:
		return C32202001::GetVersion();

	default:
		return _T("No digitizer");
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get board voltage limits
//
std::vector<std::array<float, 2> > CDigitizer::GetVoltageLimits(void) const
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		return CP7140::GetVoltageLimits();

	case TCI3220:
		return C32202001::GetVoltageLimits();

	default:
		return std::vector<std::array<float, 2> >();
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get board voltages
//
std::vector<float> CDigitizer::GetVoltages(void) const
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		return CP7140::GetVoltages();

	case TCI3220:
		return C32202001::GetVoltages();

	default:
		return std::vector<float>();
	}
}


//////////////////////////////////////////////////////////////////////
//
// Problem?
//
bool CDigitizer::HasProblem(void) const
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread not accessing this method.
	return C32202001::HasProblem();
}


//////////////////////////////////////////////////////////////////////
//
// Has accurate timestamp capability
//
bool CDigitizer::HasTimestampCapability(void) const
{
	//@note: No need to protect with CCriticalSection for method that
	//		 returning constant value.
	switch( m_type )
	{
	case P7140:
		return false;

	case TCI3220:
		return C32202001::HasTimestampCapability();

	default:
		return false;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Initialize the board
//
void CDigitizer::Init(void)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		CP7140::Init();
		break;

	case TCI3220:
		C32202001::Init();
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Mark samples as in-use
//
void CDigitizer::MarkSamplesInUse(unsigned int sampleCount, unsigned int count, unsigned char dma)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. C32202001::MarkSamplesInUse has its own CCriticalSection protection.
	switch( m_type )
	{
	case P7140:
		ASSERT(FALSE);
		break;

	case TCI3220:
		ASSERT(C32202001::GetFwType() == C32202001::BLACKBIRD);
		C32202001::MarkSamplesInUse(sampleCount, count, dma);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Mark samples as in-use
//
void CDigitizer::MarkSamplesInUse(_In_reads_(NUM_DATA_CHANS) const unsigned int(& sampleCount)[NUM_DATA_CHANS], unsigned int count)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. C32202001::MarkSamplesInUse has its own CCriticalSection protection.
	switch( m_type )
	{
	case P7140:
		CP7140::MarkSamplesInUse(sampleCount, count);
		break;

	case TCI3220:
		ASSERT(C32202001::GetFwType() != C32202001::BLACKBIRD);
		C32202001::MarkSamplesInUse(sampleCount[0], count, 0);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Measure noise power using threshold counters
//
void CDigitizer::MeasureNoiseDbm(_Out_writes_all_(2) float (&power)[2])
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 3. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		CP7140::MeasureNoiseDbm(power);
		break;

	case TCI3220:
		C32202001::MeasureNoiseDbm(power);
		break;

	default:
		power[0] = power[1] = -std::numeric_limits<float>::infinity();
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Measure tone power using threshold counters
//
void CDigitizer::MeasureToneDbm(_Out_writes_all_(2) float (&power)[2])
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		CP7140::MeasureToneDbm(power);
		break;

	case TCI3220:
		C32202001::MeasureToneDbm(power);
		break;

	default:
		power[0] = power[1] = -std::numeric_limits<float>::infinity();
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Ping the watchdog (virtual call from C32202001)
//
void CDigitizer::PingWatchDog(void)
{
	//@note: No need to protect with CCriticalSection. 3220 Hardware registers
	//		 are not accessed.
	if( CWatchDog::HPINGSOURCE(m_watchdogPingSource) != nullptr )
	{
		(*m_watchdog)->Ping(m_watchdogPingSource);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Register a callback
//
void CDigitizer::RegisterNewSamplesCallback(_In_ NewSamplesCallback callback, _In_opt_ void* param, unsigned char ch)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. C32202001::RegisterNewSamplesCallback has its own
	//		    CCriticalSection protection.
	//		 3. It is only accessed by CAudio class.
	switch(m_type)
	{
	case P7140:
		CP7140::RegisterNewSamplesCallback(callback, param, ch);
		break;

	case TCI3220:
		C32202001::RegisterNewSamplesCallback(callback, param, ch);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset sampling chains
//
void CDigitizer::ResetSampling(void)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		CP7140::ResetSampling();
		break;

	case TCI3220:
		C32202001::ResetSampling();
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset threshold counters
//
void CDigitizer::ResetThresholdCounters(void)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. Only a single access to 3220 hardware register.
	//		 3. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		CP7140::ResetThresholdCounters();
		break;

	case TCI3220:
		C32202001::ResetThresholdCounters();
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the LVDS out
//
void CDigitizer::SetDataOut(unsigned short data)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. Only a single access to 3220 hardware register.
	//		 3. CAudio thread not accessing this method.
	m_prevDataOut = data;   //Save the value.

	switch (m_type)
	{
	case P7140:
		CP7140::SetDataOut(data);
		break;

	case TCI3220:
		C32202001::SetDataOut(data);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the DDC decimation
//
bool CDigitizer::SetDecimation(unsigned long decimation, unsigned char ch)
{
	CSingleLock lock(&m_critSect, TRUE);
	switch(m_type)
	{
	case P7140:
		{
			bool changed =  CP7140::SetDecimation(decimation, ch);

			if(ch == 0)
			{
				// Channel 0 implies both channels 0 and 1
				changed = (CP7140::SetDecimation(decimation, 1) || changed);
			}

			return changed;
		}

	case TCI3220:
		return C32202001::SetDecimation(decimation, ch);

	default:
		return false;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Set the DDC FIR bandwidth
//
void CDigitizer::SetFirBw(unsigned char firBw, unsigned char ch)
{
	//@note: No need to protect with CCriticalSection. 3220 Hardware registers
	//		 are not accessed.
	switch( m_type )
	{
	case P7140:
		CP7140::SetFirBw(firBw, ch);
		break;

	case TCI3220:
// TODO		ASSERT(firBw == 80); // Only value supported
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Setup sequencer
//
void CDigitizer::SetSequencer(const std::vector<unsigned short>& pattern, unsigned int delay, unsigned long samples)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		ASSERT(samples <= 65535);
		CP7140::SetSequencer(pattern, delay, unsigned short(samples));
		break;

	case TCI3220:
		C32202001::SetSequencer(pattern, delay, samples);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for filters etc to settle
//
void CDigitizer::Settle(bool wait)
{
	CSingleLock lock(&m_critSect, TRUE);
	switch( m_type )
	{
	case P7140:
		CP7140::Settle(wait);
		break;

	case TCI3220:
		C32202001::Settle(wait);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set threshold counter levels
//
void CDigitizer::SetThresholdCounters(_In_reads_(4) const unsigned short (&threshA)[4],
		_In_reads_(4) const unsigned short (&threshB)[4])
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. 3220 has CRegLock to lock driver registers.
	//		 3. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		CP7140::SetThresholdCounters(threshA, threshB);
		break;

	case TCI3220:
		C32202001::SetThresholdCounters(threshA, threshB);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set TTL I/O data (3220 only)
//
void CDigitizer::SetTtlData(unsigned char data, unsigned char mask)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. Only a single access to 3220 hardware register.
	//		 3. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		return;

	case TCI3220:
		return C32202001::SetTtlData(data, mask);

	default:
		return;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Start collecting a block of samples
//
void CDigitizer::StartSampling(unsigned int count, unsigned int blockCount, bool delayedStart, DATE startTime, unsigned long pollInterval, _In_opt_ CTimer::PollCallback pollCallback, _In_opt_ void* param)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		CP7140::StartSampling(count, pollInterval, pollCallback, param);
		break;

	case TCI3220:
		ASSERT(pollInterval == 0 && pollCallback == nullptr);
		C32202001::StartSampling(count, blockCount, delayedStart, startTime);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Start the sequencer. Returns sampling time in us
//
void CDigitizer::StartSequencer(unsigned long pollInterval, _In_opt_ CTimer::PollCallback pollCallback, _In_opt_ void* param)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		CP7140::StartSequencer(pollInterval, pollCallback, param);
		break;

	case TCI3220:
		ASSERT(pollInterval == 0 && pollCallback == nullptr);
		C32202001::StartSequencer();
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Tune the DDRs (chs 0 and 1)
//
void CDigitizer::Tune(Units::Frequency freq, bool invertSpectrum, bool shift, unsigned char ch)
{
	CSingleLock lock(&m_critSect, TRUE);
	
	switch( m_type )
	{
	case P7140:
		ASSERT(ch == 1);
		CP7140::Tune(freq, invertSpectrum, shift);
		break;

	case TCI3220:
		C32202001::Tune(freq, invertSpectrum, shift, ch);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Tune the DDRs (ch 2)
//
void CDigitizer::TuneDemod(Units::Frequency freq, bool invertSpectrum)
{
	CSingleLock lock(&m_critSect, TRUE);
	switch(m_type)
	{
	case P7140:
		CP7140::TuneDemod(freq, invertSpectrum);
		break;

	case TCI3220:
		C32202001::TuneDemod(freq, invertSpectrum);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Unregister a callback
//
void CDigitizer::UnregisterNewSamplesCallback(unsigned char ch)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. C32202001::UnregisterNewSamplesCallback has its own
	//		    CCriticalSection protection.
	//		 3. It is only accessed by CAudio class.
	switch( m_type )
	{
	case P7140:
		CP7140::UnregisterNewSamplesCallback(ch);
		break;

	case TCI3220:
		C32202001::UnregisterNewSamplesCallback(ch);
		break;

	default:
		break;
	}

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Set the sampling rate to be used
//
void CDigitizer::UseSampleRate(Units::Frequency sampleRate)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		CP7140::SetSampleRate(sampleRate);
		break;

	case TCI3220:
		C32202001::UseSampleRate(sampleRate);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for enough free space in a DMA buffer
//
void CDigitizer::WaitForBufferSpace(unsigned int count, unsigned char dma) const
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. C32202001::WaitForBufferSpace has its own CCriticalSection protection.
	//		 3. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		ASSERT(dma == 0);
		CP7140::WaitForBufferSpace(count);
		break;

	case TCI3220:
		C32202001::WaitForBufferSpace(count, dma);
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for the sample collection to complete
//
void CDigitizer::WaitForSampling(void)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. C32202001::WaitForSequencer has its own CSingleLock protection.
	//		 3. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		CP7140::WaitForSampling();
		break;

	case TCI3220:
		C32202001::WaitForSequencer();
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for the sequencer to complete
//
void CDigitizer::WaitForSequencer(void)
{
	//@note: Not protected by CCriticalSection for the following reasons:
	//	     1. 3220: 0x82 DDC: Channel Select register not accessed.
	//		 2. C32202001::WaitForSequencer has its own CSingleLock protection.
	//		 3. CAudio thread not accessing this method.
	switch( m_type )
	{
	case P7140:
		CP7140::WaitForSequencer();
		break;

	case TCI3220:
		C32202001::WaitForSequencer();
		break;

	default:
		break;
	}

	return;
}
