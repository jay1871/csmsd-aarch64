/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2011-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include <array>
#include <atomic>
#include <ipp.h>
#include <vector>

#include "32202001.h"
#include "P7140.h"
#include "Timer.h"
#include "Units.h"
#include "WatchDog.h"

class CDigitizer
:
	// Inheritance
	public C32202001,
	public CP7140
{
	// Friends
	friend class CSingleton<CDigitizer>;

public:
	// Constants
	//LVDS Port Selection bit.
	static const unsigned short B_HF_SELECT = 0x0400;
	static const unsigned short B_UHF_SELECT = 0x0100;
	static const unsigned short B_VHF_SELECT = 0x0200;

	static const unsigned int NUM_DATA_CHANS = 2;
	static const unsigned int SAMPLES_TIMEOUT = 2000; // ms
	static const unsigned int SAMPLES_NARROW_BW = 5000; // Hertz
	static const unsigned int SAMPLES_NARROW_BW_TIMEOUT = 5000; // ms

	// Types
	enum EType { NONE, P7140, TCI3220 };
	typedef void (*NewSamplesCallback)(const Ipp16sc* samples, unsigned int count, void* param);

	// Functions
	void AdjustDecimation(_Inout_ unsigned long& decimation, _In_ bool adjustDown = false) const;
	void CollectCounts(unsigned int count,
		_Out_writes_all_(4) unsigned int (&countA)[4],
		_Out_writes_all_(4) unsigned int (&countB)[4],
		_Out_writes_all_(4) unsigned short (&threshA)[4],
		_Out_writes_all_(4) unsigned short (&threshB)[4]);
	void CollectSamples(unsigned int count, unsigned int blockCount = 1, signed char chOrBoth = -1);
	void DoneWithSamples(unsigned char dma = 0);
	void EnableStreaming(bool enable, unsigned char ch, size_t packetSize);
	void EnableWatchDog(bool enable);
	bool FifoHasOverrun(unsigned char ch);
	_Ret_opt_count_(count) const Ipp16sc* FindSamples(unsigned int sampleCount, unsigned int count, unsigned char ch, DWORD timeout = SAMPLES_TIMEOUT);
	int GetAdcFullScaleCounts(void) const;
	float GetAdcFullScaleWatts(void) const;
	bool GetClockStatus(void) const;
	float GetCutoff(unsigned char ch) const;
	unsigned short GetDataOut(void) const;
	unsigned long GetDecimation(unsigned char ch) const;
	CString GetDriverVersion(void) const;
	unsigned char GetFirBw(unsigned char ch) const;
	COleDateTime GetFpgaFwDate(void) const;
	unsigned long GetFpgaFwRev(void) const;
	float GetLatency(Units::Frequency sampleRate, unsigned long decimation);
	void GetSampleCount(_Out_writes_all_(NUM_DATA_CHANS) unsigned int(&count)[NUM_DATA_CHANS]) const;
	Units::Frequency GetSampleRate(void) const;
	_Success_(return) bool GetThresholdCounters(_Out_writes_all_(4) unsigned int(&countA)[4],
		_Out_writes_all_(4) unsigned int (&countB)[4],
		_Out_writes_all_(4) unsigned short (&threshA)[4],
		_Out_writes_all_(4) unsigned short (&threshB)[4]);
	EType GetType(void) const { return m_type; }
	CString GetVersion(void);
	std::vector<std::array<float, 2> > GetTempLimits(void) const;
	std::vector<float> GetTemperatures(void) const;
	unsigned short GetTtlData(void) const;
	std::vector<std::array<float, 2> > GetVoltageLimits(void) const;
	std::vector<float> GetVoltages(void) const;
	bool HasProblem(void) const;
	bool HasTimestampCapability(void) const;
	void Init(void);
	bool IsHfSelected(void) const { return((m_prevDataOut & B_HF_SELECT) != 0); }
	bool IsMaster(void) const { return m_type == P7140 ? CP7140::IsMaster() : C32202001::IsMaster(); }
	bool IsPresent(void) const { return m_type != NONE; }
	bool IsTimestampAccurate(void) const { return m_type == P7140 ? false : C32202001::IsPpsLocked(); }
	void MarkSamplesInUse(unsigned int sampleCount, unsigned int count, unsigned char dma);
	void MarkSamplesInUse(_In_reads_(NUM_DATA_CHANS) const unsigned int(& sampleCount)[NUM_DATA_CHANS], unsigned int count);
	void MeasureNoiseDbm(_Out_writes_all_(2) float (&power)[2]);
	void MeasureToneDbm(_Out_writes_all_(2) float (&power)[2]);
	void RegisterNewSamplesCallback(_In_ NewSamplesCallback callback, _In_opt_ void* param, unsigned char ch);
	void ResetSampling(void);
	void ResetThresholdCounters(void);
	void SelectHf(void);
	void SetDataOut(unsigned short data);
	bool SetDecimation(unsigned long decimation, unsigned char ch);
	void SetFirBw(unsigned char firBw, unsigned char ch);
	void SetSequencer(const std::vector<unsigned short>& pattern, unsigned int delay, unsigned long samples);
	void SetThresholdCounters(_In_reads_(4) const unsigned short (&threshA)[4],
		_In_reads_(4) const unsigned short (&threshB)[4]);
	void Settle(bool wait);
	void SetTtlData(unsigned char data, unsigned char mask = 0xff);
	void StartSampling(unsigned int count, unsigned int blockCount = 1, bool delayedStart = false, DATE startTime = 0.0, unsigned long pollInterval = 0, _In_opt_ CTimer::PollCallback pollCallback = nullptr, _In_opt_ void* param = nullptr);
	void StartSequencer(unsigned long pollInterval = 0, _In_opt_ CTimer::PollCallback pollCallback = nullptr, _In_opt_ void* param = nullptr);
	void Tune(Units::Frequency freq, bool invertSpectrum, bool shift, unsigned char ch = 1);
	void TuneDemod(Units::Frequency freq, bool invertSpectrum);
	void UnregisterNewSamplesCallback(unsigned char ch);
	void UseSampleRate(Units::Frequency sampleRate);
	void WaitForBufferSpace(unsigned int count, unsigned char dma = 0) const;
	void WaitForSampling(void);
	void WaitForSequencer(void);

protected:
	// Functions
	CDigitizer(void);
	~CDigitizer(void);

private:
	// Functions
	virtual void PingWatchDog(void);

	// Data
	//Critical section used to protect access between Audio & CHwControl threads.
	//The following methods are protected: SetDecimation, TuneDemod.
	CCriticalSection m_critSect;
	unsigned short m_prevDataOut;
	EType m_type;
	std::unique_ptr<CSingleton<CWatchDog>> m_watchdog;
	std::atomic<CWatchDog::HPINGSOURCE> m_watchdogPingSource;
};

//////////////////////////////////////////////////////////////////////
/// <summary>
/// Configure FPGA to output TTLData to HF board.
/// </summary>
/// <remarks>
/// 1. To avoid unnecessary work, IsHfSelected should be called and verified
///    that HF has not been selected.
/// 2. Caller must make the necessary wait to get HF selection to settle.
/// </remarks>
inline void CDigitizer::SelectHf(void)
{
	unsigned short pattern = (m_prevDataOut & 0x003f);
	SetDataOut(pattern);
	pattern |= B_HF_SELECT;
	SetDataOut(pattern);

	//Indicate HF is selected now.
	m_prevDataOut = (m_prevDataOut & ~(B_HF_SELECT | B_UHF_SELECT | B_VHF_SELECT)) | B_HF_SELECT;
	return;
}