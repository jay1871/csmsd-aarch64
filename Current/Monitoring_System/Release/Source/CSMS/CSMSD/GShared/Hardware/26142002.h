/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2012-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "Units.h"
#include "Vme.h"

class C26142002
:
	// Inheritance
	protected virtual CVme
{
public:
	// Constants
	static const unsigned long long CAL_HIGH_FREQ_HZ = 4400000000; // Hz
	static const unsigned long CAL_LOW_FREQ_HZ = 34375000; // Hz
	static const unsigned long FINAL_IF_FREQ_NARROW_BW_HZ = 139062500; // Hz
	static const unsigned long FINAL_IF_FREQ_WIDE_BW_HZ = 114062500; // Hz
	static const unsigned long FIRST_IF_BAND_BREAK_FREQ1_HZ = 600000000; // Hz
	static const unsigned long FIRST_IF_BAND_BREAK_FREQ2_HZ = 1400000000; // Hz
	static const unsigned int MIN_TUNE_STEP = 1562500; // Hz
	static const unsigned long NARROW_BW_HZ = 4000000; // Hz
	static const unsigned short OXCO_VERY_ACCURATE_INTERVAL = 2;
	static const unsigned short OXCO_ACCURATE_INTERVAL = 10;
	static const unsigned short OXCO_INACCURATE_INTERVAL = 13;
	static const unsigned short OXCO_DAC_BAD_LO = 2;
	static const unsigned short OXCO_DAC_BAD_HI = 29;
	static const unsigned short OXCO_DAC_MARGINAL_LO = 7;
	static const unsigned short OXCO_DAC_MARGINAL_HI = 25;
	static const unsigned long RF_HIGH_FREQ_HZ = 3000000000; // Hz
	static const unsigned long RF_LOW_FREQ_HZ = 20000000; // Hz
	static const unsigned long WIDE_BW_HZ = 40000000; // Hz

	// Functions
	static Units::Frequency GetCalFreq(Units::Frequency freq);
	static unsigned short GetFilterOffsetNumber(Units::Frequency freq) { return GetTuneStepNumber(freq) % 4; } // 2nd IF step in 1st IF step
	static Units::Frequency GetIfOffset(Units::Frequency freq)
		{ return MIN_TUNE_STEP * GetTuneStepNumber(freq) - freq; }
	static unsigned short GetTuneStepNumber(Units::Frequency freq)
		{ return unsigned short((freq + MIN_TUNE_STEP / 2) / Units::Frequency(MIN_TUNE_STEP)); }

protected:
	// Constants
	static const unsigned int MAX_INSTANCES = 2;

	// Types
	struct S100MHzStatus
	{
		//@note: If struct S100MHzStatus is changed, the code in
		//       CHwControl::Transfer2614StatusTo2612 that transfers the value from
		//       2614 to 2612 must be changed accordingly.
		unsigned short outOfLock : 1;
		unsigned short inhibited : 1; // This bit is copied in from the Config register
		unsigned short : 1;
		unsigned short currInterval : 4;
		unsigned short prevInterval : 4;
		unsigned short dac : 5;
	};

	struct SCardStatus
	{
		//@note: If struct SCardStatus is changed, the code in
		//       CHwControl::Transfer2612CardStatusTo2614 that transfers the value from
		//       2612 to 2614 must be changed accordingly (caller code may also need
		//       to change).
		unsigned short id : 4;
		unsigned short cardType : 4;
		unsigned short calOk : 1;
		unsigned short lo1Ok : 1;
		unsigned short lo2Ok : 1;
		unsigned short adcOk : 1;
		unsigned short : 1;
		unsigned short version : 3;
	};

	struct SShMem
	{
		unsigned long calFreqSixAndQuarterKhz;
		unsigned short rfFreqOneAndNineSixteenthMhz;
		unsigned short lo1FreqSixAndQuarterMhz;
		unsigned short lo2FreqOneAndNineSixteenthMhz;
		bool narrowband;
	};

	// Functions
	C26142002(void); // Can only be constructed by a derived class
	virtual ~C26142002(void);
	S100MHzStatus Get100MHzStatus(unsigned char id = 0) const;
	SCardStatus GetCardStatus(unsigned char id = 0, bool force = false) const;
	unsigned short GetConfigReg(unsigned char id = 0) const { return Peek(id, CONFIG_REG); }
	unsigned long GetMaxSoftWaitExceedVal(void){ unsigned long exceeded = m_exceedUsecWait; m_exceedUsecWait = 0; return(exceeded); }
	FILETIME GetTime(unsigned char id = 0) const;
	unsigned char GetVersion(unsigned char id) const;
	void InhibitGpsSync(bool inhibit, unsigned char id = 0);
	bool IsPresent(unsigned char id = 0) const { return m_present[id]; }
	Units::Frequency SetCalTone(Units::Frequency freq, bool on, signed char idOrAll = -1);
	void SetLo1(unsigned short freqSixAndQuarterMhz, unsigned char id);
	void SetLo2(unsigned short freqOneAndNineSixteenthMhz, unsigned char id);
	void SetSeconds(time_t secs, unsigned char id = 0);
	void Wait(unsigned long us);
	Units::Frequency Tune(Units::Frequency freq, Units::Frequency rxBw, signed char idOrAll = -1);

	// Data
	mutable CMutex m_mutex;
	volatile SShMem (*m_shMem)[MAX_INSTANCES];

private:
	// Constants
	static const unsigned int CARD_TYPE = 0x2;

	static const unsigned long C_LO1_REG4_WAIT_USEC = 100; //100 usec
	static const unsigned long C_MAX_SFTW_WAIT_USEC = 200; //200 usec is the max allowable wait.
	static const unsigned int VME_BASE_ADDR = (0x780000 | (CARD_TYPE << 12));
	static const unsigned int STATUS_REG = 0x00;
	static const unsigned int CONFIG_REG = 0x02;
	static const unsigned int PLL_CTRL_LO_REG = 0x04;
	static const unsigned int PLL_CTRL_HI_REG = 0x06;
	static const unsigned int GPS_STATUS_REG = 0x08;
	static const unsigned int GPS_FRAC_SEC_REG = 0x0a;
	static const unsigned int GPS_SEC_LO_REG = 0x0c;
	static const unsigned int GPS_SEC_HI_REG = 0x0e;
	static const unsigned int PIT_REG = 0x10;
	static const unsigned long CAL_STEP_HZ = 6250;
	static LPCTSTR MUTEX_NAME;
	static LPCTSTR SHMEM_NAME;

	// Types
#pragma warning(push)
#pragma warning(disable : 4480) // Non-standard extension
	enum EPll : unsigned short { CAL = 0x0, LO1 = 0x1, LO2 = 0x2, ADC = 0x3 };
#pragma warning(pop)

	union UPllReg
	{
		unsigned int raw;

		struct
		{
			unsigned short lo;
			unsigned short hi;
		};

		union
		{
			union
			{
				struct // a = 1
				{
					unsigned int : 1;
					unsigned int cenPinSel : 1;
					unsigned int cenSpi : 1;
					unsigned int biasCenOvrd : 1;
					unsigned int pfdCenOvrd : 1;
					unsigned int cpCenOvrd : 1;
					unsigned int refCenOvrd : 1;
					unsigned int vcoCenOvrd : 1;
					unsigned int gpoCenOvrd : 1;
				} powerDown;

				struct // a = 2
				{
					unsigned int : 1;
					unsigned int r : 14;
				} refDiv;

				struct // a = 3
				{
					unsigned int : 1;
					unsigned int nInt : 19;
				} intPart;

				struct // a = 4
				{
					unsigned int : 1;
					unsigned int nFrac : 24;
				} fracPart;

				struct // a = 6
				{
					unsigned int : 1;
					unsigned int selSeed : 2;
					unsigned int selMod : 2;
					unsigned int reserved1 : 3;
					unsigned int modBps : 1;
					unsigned int autoSeed : 1;
					unsigned int selMdclk : 1;
					unsigned int selCreClk : 1;
					unsigned int enMcore : 1;
					unsigned int reserved2 : 6;
					unsigned int enBist : 1;
					unsigned int selBstc : 2;
				} deltaSigmaConfig;

				struct // a = 7
				{
					unsigned int : 1;
					unsigned int ldW : 3;
					unsigned int ldEn : 1;
					unsigned int : 2;
					unsigned int ldType : 1;
					unsigned int ldDw : 3;
					unsigned int ldTper : 2;
					unsigned int ldTest : 1;
					unsigned int ldAuto : 1;
				} lockDetect;

				struct // a = 8
				{
					unsigned int : 1;
					unsigned int enIcbias : 1;
					unsigned int enCp : 1;
					unsigned int enPd : 1;
					unsigned int enRbuf : 1;
					unsigned int enRfBuf : 1;
					unsigned int enSdo : 1;
					unsigned int spare : 1;
					unsigned int enRfdiv : 1;
					unsigned int : 1;
					unsigned int enPreClk : 1;
					unsigned int enPreBias : 1;
					unsigned int enCpamp : 1;
					unsigned int biasRfBuf : 3;
					unsigned int biasDiv : 3;
					unsigned int : 1;
					unsigned int enDiv2 : 1;
					unsigned int : 1;
					unsigned int enRefHiF : 1;
				} analogEnable;

				struct // a = 9
				{
					unsigned int : 1;
					unsigned int cpD : 7;
					unsigned int cpU : 7;
					unsigned int cpO : 7;
					unsigned int dirU : 1;
					unsigned int dirD : 1;
					unsigned int hiKcp : 1;
				} chargePump;

				struct // a = 10
				{
					unsigned int zero: 1;
					unsigned int reserved : 23;
				} auxSpiTrigger;

				struct // a = 11
				{
					unsigned int : 1;
					unsigned int pdDel : 3;
					unsigned int shortPdInput : 1;
					unsigned int pdPolr : 1;
					unsigned int pdUEn : 1;
					unsigned int pdDEn : 1;
					unsigned int csp : 2;
					unsigned int forceCpUp : 1;
					unsigned int forceCpDn : 1;
					unsigned int forceCpMid : 1;
					unsigned int psBias : 3;
					unsigned int cpBias : 2;
					unsigned int mctrGt : 2;
					unsigned int : 1;
					unsigned int divPw : 2;
				} phaseDetector;

				struct // a = 12
				{
					unsigned int : 1;
					unsigned int numChan : 19;
				} exactFreq;

				struct // a = 15
				{
					unsigned int : 1;
					unsigned int gpoSel : 5;
					unsigned int gpoDat : 1;
					unsigned int inhSpiMux : 1;
					unsigned int inhSpiLdet : 1;
					unsigned int disblePfet : 1;
					unsigned int disbleNfet : 1;
				} genPurposeOut;
			};

			struct
			{
				unsigned int : 25;
				unsigned int a : 6;
				unsigned int w : 1;
			};
		} hmc704lp4e;

		union
		{
			union
			{
				struct // control = 0
				{
					unsigned int : 3;
					unsigned int nFrac : 12;
					unsigned int nInt : 16;
				} nIntFrac;

				struct // control = 1
				{
					unsigned int : 3;
					unsigned int mod : 12;
					unsigned int dsP : 12;
					unsigned int presclr : 1;
					unsigned int phaseAdj : 1;
				} deltaSigmaMod;

				struct // control = 2
				{
					unsigned int : 3;
					unsigned int rstCtr : 1;
					unsigned int cpTri : 1;
					unsigned int pwrDn : 1;
					unsigned int pdPol : 1;
					unsigned int ldPwr : 1;
					unsigned int ldFrac : 1;
					unsigned int cpCur : 4;
					unsigned int buffDbl : 1;
					unsigned int rCtr : 10;
					unsigned int rDiv2 : 1;
					unsigned int rX2 : 1;
					unsigned int mux : 3;
					unsigned int selMode : 2;
				} rCounter;

				struct // control = 3
				{
					unsigned int : 3;
					unsigned int clkDiv : 12;
					unsigned int enClkDiv : 2;
					unsigned int : 1;
					unsigned int csRed : 1;
					unsigned int : 2;
					unsigned int chCncl : 1;
					unsigned int abp : 1;
					unsigned int clkMode : 1;
				} cycleSlipConfig;

				struct // control = 4
				{
					unsigned int : 3;
					unsigned int pOut : 2;
					unsigned int enOut : 1;
					unsigned int pAux : 2;
					unsigned int enAux : 1;
					unsigned int selAux : 1;
					unsigned int outMute : 1;
					unsigned int enVco : 1;
					unsigned int clkDiv : 8;
					unsigned int selDiv : 3;
					unsigned int selFdbk : 1;
				} configuration;

				struct // control = 5
				{
					unsigned int : 3;
					unsigned int: 16;
					unsigned int reserved: 2;
					unsigned int: 1;
					unsigned int ldP : 2;
				} lockDetect;
			};

			struct
			{
				unsigned int control : 3;
			};
		} adf4351;
	};

	// Functions
	unsigned short Peek(unsigned char id, unsigned char reg) const { return CVme::Peek(VME_BASE_ADDR + 256 * id + reg); }
	void Poke(unsigned char id, unsigned char reg, unsigned short data) { CVme::Poke(VME_BASE_ADDR + 256 * id + reg, data); }
	void SetPllReg(const UPllReg& reg, EPll pll, unsigned char id);
	virtual void SetSettleTime(unsigned int) {}

	// Data
	unsigned long m_exceedUsecWait;
	bool m_first;
	bool m_intHandler;
	bool m_present[MAX_INSTANCES];
};
