/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#include "StdAfx.h"

#include "7232200376442005.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
//
// Static data
//
const C7232200376442005::ESource C7232200376442005::GRAY_CODE[NUM_ANTS] = { ANT_4, ANT_5, ANT_1, ANT_3, ANT_2 };


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C7232200376442005::C7232200376442005(void)
:
	m_controlByte(0xff),
	m_digitizer(),
	m_is7644(false),
	m_validateFailCount(0)
{
	SetSwitch(false, TERM, TERM);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C7232200376442005::~C7232200376442005(void)
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the control byte
//
unsigned char C7232200376442005::GetControlByte(bool notchTxBite, ESource refRf, ESource sampleRf)
{
	ASSERT(refRf <= BITE && sampleRf <= BITE);
	unsigned char controlByte = ((unsigned char(refRf) << 4) | unsigned char(sampleRf));

	if(!notchTxBite)
	{
		controlByte |= 0x08;
	}

	if(refRf != BITE && sampleRf != BITE)
	{
		controlByte |= 0x80;
	}


	return controlByte;
}


//////////////////////////////////////////////////////////////////////
//
// Get the settle time for a switch change
//
unsigned int C7232200376442005::GetSettleTime(unsigned char controlByte) const
{
	CSingleLock lock(&m_critSect, TRUE);

	if(controlByte == m_controlByte)
	{
		return 0;
	}
	else if((controlByte & 0x80) == 0x00 && (m_controlByte & 0x80) == 0x80)
	{
		// BITE source on
		return 20000;
	}
	else if(m_is7644 && (controlByte & 0x08) == 0x00 && (m_controlByte & 0x08) == 0x08)
	{
		// 7644-2004 Tx BITE source on
		return 20000;
	}
	else
	{
		return 20; // Default 20 us
	}
}


//////////////////////////////////////////////////////////////////////
//
// Set the switch by programming the LVDS bus
//
void C7232200376442005::SetSwitch(unsigned char controlByte, bool force)
{
	CSingleLock lock(&m_critSect, TRUE);

	if(force || controlByte != m_controlByte)
	{
		m_controlByte = controlByte;
		m_digitizer->SetDataOut(0x0000 | controlByte);
		m_digitizer->SetDataOut(0x0200 | controlByte);
		m_digitizer->SetDataOut(0x0000 | controlByte);
		m_digitizer->SetDataOut(0x0000);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the switch
//
void C7232200376442005::SetSwitch(bool notchTxBite, ESource refRf, ESource sampleRf)
{
	unsigned char controlByte = GetControlByte(notchTxBite, refRf, sampleRf);
	SetSettleTime(GetSettleTime(controlByte));
	SetSwitch(controlByte, false);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate loopback data against local state
//
void C7232200376442005::ValidateLoopback(unsigned char loopback)
{
	CSingleLock lock(&m_critSect, TRUE);

	if(m_controlByte != loopback)
	{
		if(++m_validateFailCount >= 10)
		{
			// Force switch control
			TRACE(_T("7232200376442005 control reset\n"));
			C80682017::WriteShMemRegs();
			SetSwitch(m_controlByte, true);
			m_validateFailCount = 0;
		}
	}
	else
	{
		m_validateFailCount = 0;
	}

	return;
}

