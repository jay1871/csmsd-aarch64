/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2012-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "StdAfx.h"

#include <algorithm>
#include <memory>

#include "26142001.h"
#include "26142002.h"
#include "Units.h"
#include "Vme.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
//
// Static data
//
const CDownConverter::BandSettings C26142001::BAND_SETTINGS(BAND_SETTINGS_, &BAND_SETTINGS_[_countof(BAND_SETTINGS_)]);
const float C26142001::GAIN_SLOPE = -0.065f; // dB / �C 
LPCTSTR C26142001::MUTEX_NAME = _T("Global\\TCI.{E6CF214C-9329-4C1C-AD6B-A86020FD8320}");
const float C26142001::NOMINAL_TEMP = 25; // �C 

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C26142001::C26142001(void)
:
	CVme(),
	m_gainModes(),
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get())
{
	m_first = !(GetLastError() == ERROR_ALREADY_EXISTS); // Status from m_mutex construction

	for(unsigned char id = 0; id < MAX_INSTANCES; ++id)
	{
		m_address[id] = VME_BASE_ADDR_WIDE + 256 * id;
		m_present[id] = ((Peek(id, STATUS_REG) & 0x00ffu) == ((CARD_TYPE_WIDE << 4) | id));
		m_wideband[id] = true;

		if(!m_present[id])
		{
			m_address[id] = VME_BASE_ADDR_NARROW + 256 * id;
			m_present[id] = ((Peek(id, STATUS_REG) & 0x00ffu) == ((CARD_TYPE_NARROW << 4) | id));
			m_wideband[id] = false;
		}

		if(m_present[id])
		{
			if(m_first)
			{
				m_attenReg[id] = 0x0fff;
				Poke(id, ATTEN_REG, m_attenReg[id]);
				m_switchReg[id] = 0x0000;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetAtten(MAX_ATTEN, 100000000, id);
				SetInput(0, id);
				SetCalTone(false, id);
				SetDetector(false, id);
				Units::FreqPair freqLimits;
				size_t band;
				Tune(100000000, NARROW_BW_HZ, OPTIMUM, freqLimits, band, id);
			}
			else
			{
				m_attenReg[id] = Peek(id, ATTEN_REG);
				m_switchReg[id] = Peek(id, SWITCH_REG);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C26142001::~C26142001(void)
{
	for(signed char id = 0; id < MAX_INSTANCES; ++id)
	{
		if(m_present[id] && m_first)
		{
			SetAtten(84, id);
			SetInput(3, id);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the band
//
size_t C26142001::GetBand(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ Units::FreqPair& freqLimits)
{
	// Round freq like the synth does so we stay in sync
	Units::Frequency roundedFreq = C26142002::MIN_TUNE_STEP * C26142002::GetTuneStepNumber(freq);

	// 600 MHz and 1400 MHz breaks are special
	if(roundedFreq > C26142002::FIRST_IF_BAND_BREAK_FREQ1_HZ - WIDE_BW_HZ && roundedFreq < C26142002::FIRST_IF_BAND_BREAK_FREQ1_HZ)
	{
		freqLimits = Units::FreqPair(freq - rxBw / 2, freq + rxBw / 2);
		return 5;
	}
	else if(roundedFreq >= C26142002::FIRST_IF_BAND_BREAK_FREQ1_HZ && roundedFreq < C26142002::FIRST_IF_BAND_BREAK_FREQ1_HZ + WIDE_BW_HZ)
	{
		freqLimits = Units::FreqPair(freq - rxBw / 2, freq + rxBw / 2);
		return 6;
	}
	else if(roundedFreq > C26142002::FIRST_IF_BAND_BREAK_FREQ2_HZ - WIDE_BW_HZ && roundedFreq < C26142002::FIRST_IF_BAND_BREAK_FREQ2_HZ)
	{
		freqLimits = Units::FreqPair(freq - rxBw / 2, freq + rxBw / 2);
		return 7;
	}
	else if(roundedFreq >= C26142002::FIRST_IF_BAND_BREAK_FREQ2_HZ && roundedFreq < C26142002::FIRST_IF_BAND_BREAK_FREQ2_HZ + WIDE_BW_HZ)
	{
		freqLimits = Units::FreqPair(freq - rxBw / 2, freq + rxBw / 2);
		return 8;
	}
	else
	{
		// Default behavior
		return CDownConverter::GetBand(freq, rxBw, bandSelect, freqLimits, BAND_SETTINGS);
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the detector state
//
bool C26142001::GetDetector(unsigned char id) const
{
	return (Peek(id, STATUS_REG) & 0x0100) != 0x0000;
}


//////////////////////////////////////////////////////////////////////
//
// Get the gain compensated for temperature (ratio)
//
float C26142001::GetGainRatio(float temp, unsigned char id) const
{
	return pow(10.0f, (MAX_GAIN - m_atten[id] + GetGainAdjustmentDb(temp)) / 10.0f);
}


//////////////////////////////////////////////////////////////////////
//
// Get the board version
//
unsigned char C26142001::GetVersion(unsigned char id) const
{
	return unsigned char((Peek(id, STATUS_REG) & 0xe000u) >> 13);
}


//////////////////////////////////////////////////////////////////////
//
// Set the attenuation
//
unsigned char C26142001::SetAtten(unsigned char atten, Units::Frequency freq, signed char idOrAll)
{
	return SetAtten(atten, GetGainMode(freq), idOrAll);
}


//////////////////////////////////////////////////////////////////////
//
// Set the attenuation
//
	unsigned char C26142001::SetAtten(unsigned char atten, CDownConverter::EGainMode mode, signed char idOrAll)
{
	static const unsigned short ATTEN_DATA[][3] = 
	{	// Rural, urban, congested, attenuation (decimal)
		{ 0x0000, 0x0004, 0x0001 }, // 0
		{ 0x0100, 0x0004, 0x0001 }, // 2
		{ 0x0200, 0x0004, 0x0001 }, // 4
		{ 0x0300, 0x0006, 0x0001 }, // 6
		{ 0x0400, 0x0106, 0x0001 }, // 8
		{ 0x0500, 0x0206, 0x0001 }, //10
		{ 0x0600, 0x0306, 0x0003 }, //12
		{ 0x0602, 0x0308, 0x0103 }, //14
		{ 0x0702, 0x0408, 0x0203 }, //16
		{ 0x0802, 0x0508, 0x0303 }, //18
		{ 0x0804, 0x0501, 0x0403 }, //20
		{ 0x0904, 0x0601, 0x0503 }, //22
		{ 0x0a04, 0x0701, 0x0505 }, //24
		{ 0x0a06, 0x0703, 0x0605 }, //26
		{ 0x0b06, 0x0803, 0x0705 }, //28
		{ 0x0c06, 0x0903, 0x0707 }, //30
		{ 0x0c08, 0x0905, 0x0807 }, //32
		{ 0x0d08, 0x0a05, 0x0809 }, //34
		{ 0x0e08, 0x0b05, 0x0909 }, //36
		{ 0x0e01, 0x0b07, 0x090b }, //38
		{ 0x0f01, 0x0c07, 0x0a0b }, //40
		{ 0x0f21, 0x0d07, 0x0a0d }, //42
		{ 0x0f23, 0x0d09, 0x0b0d }, //44
		{ 0x0f43, 0x0e09, 0x0b0f }, //46
		{ 0x0f63, 0x0f09, 0x0c0f }, //48
		{ 0x0f65, 0x0f0b, 0x0c11 }, //50
		{ 0x0f85, 0x0f2b, 0x0d11 }, //52
		{ 0x0f87, 0x0f4b, 0x0d13 }, //54
		{ 0x0fa7, 0x0f4d, 0x0e13 }, //56
		{ 0x0fa9, 0x0f6d, 0x0e15 }, //56
		{ 0x0fc9, 0x0f8d, 0x0f15 }, //58
		{ 0x0fcb, 0x0f8f, 0x0f17 }, //60
		{ 0x0feb, 0x0faf, 0x0f37 }, //62
		{ 0x0fed, 0x0fb1, 0x0f39 }, //64
		{ 0x0fef, 0x0fd1, 0x0f59 }, //66
		{ 0x0ff1, 0x0fd3, 0x0f79 }, //70
		{ 0x0ff3, 0x0ff3, 0x0f7b }, //72
		{ 0x0ff5, 0x0ff5, 0x0f9b }, //74
		{ 0x0ff7, 0x0ff7, 0x0fbb }, //76
		{ 0x0ff9, 0x0ff9, 0x0fbd }, //78
		{ 0x0ffb, 0x0ffb, 0x0fdd }, //80
		{ 0x0ffd, 0x0ffd, 0x0ffd }, //82
		{ 0x0fff, 0x0fff, 0x0fff }  //84
	};

	if(mode > CDownConverter::CONGESTED)
	{
		mode = CDownConverter::CONGESTED;
	}

	if(mode == CDownConverter::CONGESTED && atten < 10)
	{
		atten = 10;
	}

	if(atten / 2 >= _countof(ATTEN_DATA))
	{
		atten = 2 * _countof(ATTEN_DATA) - 2;
	}

	unsigned short data = ATTEN_DATA[atten / 2][mode];

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if(m_attenReg[id] != data)
			{
				m_attenReg[id] = data;
				Poke(id, ATTEN_REG, m_attenReg[id]);
				SetSettleTime(100);
			}

			m_atten[id] = atten / 2 * 2;
		}
	}

	return m_atten[idOrAll == -1 ? 0 : idOrAll];
}


//////////////////////////////////////////////////////////////////////
//
// Set the attenuation
//
void C26142001::SetAttenRaw(unsigned short bits, signed char idOrAll)
{
	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if(m_attenReg[id] != bits)
			{
				m_attenReg[id] = bits;
				Poke(id, ATTEN_REG, m_attenReg[id]);
				SetSettleTime(5);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the RF band
//
void C26142001::SetBand(unsigned char band, signed char idOrAll)
{
	unsigned short data = BAND_SETTINGS_[band].data;

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? int(MAX_INSTANCES) : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if((m_switchReg[id] & 0x003eu) != data)
			{
				m_switchReg[id] &= 0x03c1u;
				m_switchReg[id] |= data;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetSettleTime(5);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Turn the cal tone on or off (returns frequency)
//
unsigned long C26142001::SetCalTone(bool on, signed char idOrAll)
{
	unsigned short data = (on ? 0x0100 : 0x0000);

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if((m_switchReg[id] & 0x0100) != data)
			{
				m_switchReg[id] &= 0x02ff;
				m_switchReg[id] |= data;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetSettleTime(on ? CAL_TONE_ON_SETTLE_TIME : CAL_TONE_OFF_SETTLE_TIME);
			}
		}
	}

	return 25000000;
}


//////////////////////////////////////////////////////////////////////
//
// Turn the detector on or off
//
void C26142001::SetDetector(bool on, signed char idOrAll)
{
	unsigned short data = (on ? 0x0200 : 0x0000);

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if((m_switchReg[id] & 0x0200) != data)
			{
				m_switchReg[id] &= 0x01ff;
				m_switchReg[id] |= data;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetSettleTime(20000);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the RF input
//
void C26142001::SetInput(unsigned char input, signed char idOrAll)
{
	unsigned short data = (input << 6);

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if((m_switchReg[id] & 0x00c0) != data)
			{
				m_switchReg[id] &= 0x033f;
				m_switchReg[id] |= data;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetSettleTime(5);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set wide bandwidth
//
void C26142001::SetWideBw(bool wide, signed char idOrAll)
{
	unsigned short data = (wide ? 0x0000 : 0x0001);

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? MAX_INSTANCES : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if((m_switchReg[id] & 0x0001) != data)
			{
				m_switchReg[id] &= 0x03fe;
				m_switchReg[id] |= data;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetSettleTime(5);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the signal routing
//
void C26142001::Tune(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ Units::FreqPair& freqLimits,
					 _Out_ size_t& band, signed char idOrAll)
{
	ASSERT(freq <= RF_HIGH_FREQ_HZ + WIDE_BW_HZ / 2); // VCP allows tuning down to DC
	band = GetBand(freq, rxBw, bandSelect, freqLimits);
	unsigned short data = BAND_SETTINGS_[band].data;

	if(rxBw <= NARROW_BW_HZ)
	{
		data |= 0x0001;
	}

	for(unsigned char id = (idOrAll == -1 ? 0 : idOrAll); id < (idOrAll == -1 ? int(MAX_INSTANCES) : idOrAll + 1); ++id)
	{
		if(m_present[id])
		{
			if((m_switchReg[id] & 0x003f) != data)
			{
				m_switchReg[id] &= 0x03c0;
				m_switchReg[id] |= data;
				Poke(id, SWITCH_REG, m_switchReg[id]);
				SetSettleTime(5);
			}
		}
	}

	return;
}
