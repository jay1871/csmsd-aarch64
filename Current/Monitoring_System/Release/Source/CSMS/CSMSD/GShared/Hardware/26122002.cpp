/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "StdAfx.h"

#include <memory>

#include "26122001.h"
#include "26122002.h"
#include "Units.h"
#include "Vme.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
//
// Static data
//
LPCTSTR C26122002::MUTEX_NAME = _T("Global\\TCI.{47B022ED-54F1-4dad-91DC-95664C954FC8}.Mutex");
LPCTSTR C26122002::SHMEM_NAME = _T("Global\\TCI.{47B022ED-54F1-4dad-91DC-95664C954FC8}.ShMem");


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C26122002::C26122002(void)
:
	m_expired(FALSE, FALSE),
	m_intHandler(false),
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get())
{
	CSingleLock lock(&m_mutex, TRUE);
	m_first = GetSharedMemory<SShMem[MAX_INSTANCES]>(SHMEM_NAME, m_shMem);

	for(unsigned char id = 0; id < MAX_INSTANCES; ++id)
	{
		SCardStatus status = GetCardStatus(id, true);
		m_present[id] = (status.cardClass == (CARD_CLASS & 0x3) && status.cardType == CARD_TYPE && status.id == id);

		if(m_present[id])
		{
			if(m_first) // Fix this for two synthesizers
			{
				SetCalTone(1500000000, false, id);
				SetSampleRate(81920000, id);
				Tune(98500000, 2000000, id);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C26122002::~C26122002(void)
{
	UnregisterInterruptHandler(GEF_VME_INT_VIRQ5, OnTimer, ((CARD_CLASS & 0x3) << 6) | (CARD_TYPE << 3));
	FreeSharedMemory<C26122002::SShMem[2]>(m_shMem);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the 100 MHz OCXO status
//
C26122002::S100MHzStatus C26122002::Get100MHzStatus(unsigned char id) const
{
	union
	{
		unsigned short reg;
		S100MHzStatus status;
	} status = { 0 };

	if(m_present[id])
	{
		status.reg = Peek(id, GPS_STATUS_REG);

		// Copy in inhibited bit
		status.status.inhibited = ((Peek(id, CONFIG_REG) & 0x0800) == 0x0800);
	}

	return status.status;
}


//////////////////////////////////////////////////////////////////////
//
// Get closest cal freq in same downconverter band
//
Units::Frequency C26122002::GetCalFreq(Units::Frequency freq)
{
	ASSERT(_countof(C26122001::BAND_SETTINGS_) > 1);
	size_t band;

	for(band = 0; band < _countof(C26122001::BAND_SETTINGS_) - 1; ++band)
	{
		if(freq < (C26122001::BAND_SETTINGS_[band].high1dbFreq + C26122001::BAND_SETTINGS_[band + 1].low1dbFreq) / 2)
		{
			break;
		}
	}

	if(band == 0)
	{
		return GetCalSettings(freq, C26122001::BAND_SETTINGS_[0].low1dbFreq,
			(C26122001::BAND_SETTINGS_[0].high1dbFreq + C26122001::BAND_SETTINGS_[1].low1dbFreq) / 2 - 1).calFreq;
	}
	else if(band == _countof(C26122001::BAND_SETTINGS_) - 1)
	{
		return GetCalSettings(freq,
			(C26122001::BAND_SETTINGS_[band - 1].high1dbFreq + C26122001::BAND_SETTINGS_[band].low1dbFreq) / 2,
			C26122001::BAND_SETTINGS_[band].high1dbFreq - 1).calFreq;
	}
	else
	{
		return GetCalSettings(freq,
			(C26122001::BAND_SETTINGS_[band - 1].high1dbFreq + C26122001::BAND_SETTINGS_[band].low1dbFreq) / 2,
			(C26122001::BAND_SETTINGS_[band].high1dbFreq + C26122001::BAND_SETTINGS_[band + 1].low1dbFreq) / 2 - 1).calFreq;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Helper function
//
C26122002::SCalSettings C26122002::GetCalSettings(Units::Frequency freq, Units::Frequency loFreq, Units::Frequency hiFreq)
{
	ASSERT(freq >= loFreq && freq <= hiFreq);

	static const SCalSettings SETTINGS[] = 
		{
			{   46875000, 64, 0x0007 },
			{   93750000, 32, 0x0006 },
			{  187500000, 16, 0x0005 },
			{  375000000,  8, 0x0004 },
			{  750000000,  4, 0x0003 },
			{ 1500000000,  2, 0x0002 },
			{ 0xffffffff,  1, 0x0001 },
		};

	unsigned char band;

	for(band = 0; band < _countof(SETTINGS); ++band)
	{
		if(freq < SETTINGS[band].calFreq)
		{
			break;
		}
	}

	if(band == _countof(SETTINGS))
	{
		--band;
	}

	Units::Frequency step = 10000000 / SETTINGS[band].divisor;
	freq = (freq + step / 2) / step * step;

	if(freq < CAL_LOW_FREQ_HZ)
	{
		freq = CAL_LOW_FREQ_HZ;
	}
	else if(freq > CAL_HIGH_FREQ_HZ)
	{
		freq = CAL_HIGH_FREQ_HZ;
	}
	else if((band > 0 && freq < SETTINGS[band - 1].calFreq) || freq < loFreq)
	{
		freq += step;
	}
	else if(freq >= SETTINGS[band].calFreq || freq > hiFreq)
	{
		freq -= step;
	}

	SCalSettings settings = { freq, SETTINGS[band].divisor, SETTINGS[band].config };

	return settings;
}


//////////////////////////////////////////////////////////////////////
//
// Get the card status
//
C26122002::SCardStatus C26122002::GetCardStatus(unsigned char id, bool force) const
{
	unsigned short reg = 0;

	if(force || m_present[id])
	{
		reg = Peek(id, STATUS_REG);
	}

	union
	{
		unsigned short reg;
		SCardStatus status;
	} status = { reg };

	return status.status;
}


//////////////////////////////////////////////////////////////////////
//
// Get the time-of-day
//
FILETIME C26122002::GetTime(unsigned char id) const
{
	unsigned int tries = 1;
	unsigned short frac = Peek(id, GPS_FRAC_SEC_REG);
	unsigned short secHi = Peek(id, GPS_SEC_HI_REG);
	unsigned short secLo = Peek(id, GPS_SEC_LO_REG);

	if(frac == 0 || frac == 1)
	{
		unsigned short frac2 = Peek(id, GPS_FRAC_SEC_REG);

		while(frac2 != frac)
		{
			frac = frac2;
			secHi = Peek(id, GPS_SEC_HI_REG);
			secLo = Peek(id, GPS_SEC_LO_REG);
			frac2 = Peek(id, GPS_FRAC_SEC_REG);
			++tries;
		}

		if(frac2 == 0)
		{
			// Adjust for delayed rollover of seconds counter
			if(++secLo == 0)
			{
				++secHi;
			}
		}
	}

	// Convert to FILETIME
	ULARGE_INTEGER time;
	time.QuadPart = 10000000 * (65536ull * secHi + secLo) + 200 * frac + 116444736000000000;
	FILETIME filetime = { time.LowPart, time.HighPart };

	return filetime;
}


//////////////////////////////////////////////////////////////////////
//
// Get the board version
//
unsigned char C26122002::GetVersion(unsigned char id) const
{
	return GetCardStatus(id).version;
}


//////////////////////////////////////////////////////////////////////
//
// Inhibit the GPS sync
//
void C26122002::InhibitGpsSync(bool inhibit)
{
	CSingleLock lock(&m_mutex, TRUE);
	bool state = ((Peek(0, CONFIG_REG) & 0x0800) != 0);

	if(inhibit != state)
	{
		unsigned short config = (Peek(0, CONFIG_REG) & 0x7177);

		if(inhibit)
		{
			config |= 0x0800;
		}

		Poke(0, CONFIG_REG, config);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Timer interupt handler
//
void C26122002::OnTimer(void)
{
	m_expired.SetEvent();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Sets the calibration tone frequency and state. Returns the actual frequency
//
Units::Frequency C26122002::SetCalTone(Units::Frequency freq, bool on, signed char idOrAll)
{
	SCalSettings settings = GetCalSettings(freq);
	CSingleLock lock(&m_mutex, TRUE);

	for(unsigned char id = (idOrAll < 0 ? 0 : idOrAll); id < (idOrAll < 0 ? int(MAX_INSTANCES) : idOrAll + 1); ++id)
	{
		if(IsPresent(id))
		{
			bool settle = false;
			bool state = ((Peek(id, CONFIG_REG) & 0x0007u) != 0);

			if(on != state || (on && freq != (*m_shMem)[id].calFreq))
			{
				// Program config reg
				unsigned short config = Peek(id, CONFIG_REG) & 0x7970u;

				if(on)
				{
					config |= settings.config;
				}

				Poke(id, CONFIG_REG, config);
				settle = true;
			}

			if(freq != (*m_shMem)[id].calFreq)
			{
				// Program PLL
				unsigned short pllFreq10Mhz = (settings.calFreq * settings.divisor / 10000000).Hz<unsigned short>();
				unsigned short p = (pllFreq10Mhz <= 240 ? 0 : 1);
				unsigned short cp = (pllFreq10Mhz <= 240 ? 3 : 7);
				UPllLatch latch = { 0 };
				latch.controlStatus.pll = CAL;
				latch.initLatch.c = 3;
				latch.initLatch.f1 = 0;
				latch.initLatch.pd1 = 0;
				latch.initLatch.m = 5;
				latch.initLatch.f2 = 0;
				latch.initLatch.f3 = 0;
				latch.initLatch.f4 = 0;
				latch.initLatch.f5 = 0;
				latch.initLatch.tc = 0;
				latch.initLatch.cp1 = cp;
				latch.initLatch.cp2 = cp;
				latch.initLatch.pd2 = 0;
				latch.initLatch.p = p;
				SetPllLatch(latch, id);
				latch.allBits = 0;
				latch.controlStatus.pll = CAL;
				latch.functionLatch.c = 2;
				latch.functionLatch.f1 = 0;
				latch.functionLatch.pd1 = 0;
				latch.functionLatch.m = 5;
				latch.functionLatch.f2 = 0;
				latch.functionLatch.f3 = 0;
				latch.functionLatch.f4 = 0;
				latch.functionLatch.f5 = 0;
				latch.functionLatch.tc = 0;
				latch.functionLatch.cp1 = cp;
				latch.functionLatch.cp2 = cp;
				latch.functionLatch.pd2 = 0;
				latch.functionLatch.p = p;
				SetPllLatch(latch, id);
				latch.allBits = 0;
				latch.controlStatus.pll = CAL;
				latch.refCounterLatch.c = 0;
				latch.refCounterLatch.r = 10;
				latch.refCounterLatch.abp = 2;
				latch.refCounterLatch.t = 0;
				latch.refCounterLatch.lpd = 1;
				SetPllLatch(latch, id);
				latch.allBits = 0;
				latch.controlStatus.pll = CAL;
				latch.nCounterLatch.c = 1;
				latch.nCounterLatch.a = pllFreq10Mhz % (8 * (p + 1));
				latch.nCounterLatch.b = pllFreq10Mhz / (8 * (p + 1));
				latch.nCounterLatch.g = 0;
				SetPllLatch(latch, id);
				(*m_shMem)[id].calFreq = settings.calFreq;
				settle = true;
			}

			if(settle)
			{
				SetSettleTime(on ? (state ? 5000 : 10000) : 100);
			}
		}
	}

	return settings.calFreq;
}


//////////////////////////////////////////////////////////////////////
//
// Set LO1
//
void C26122002::SetLo1(unsigned short freqMhz, unsigned char id)
{
	static const struct 
	{
		unsigned short freqMhz;
		unsigned short p;
		unsigned short r;
		unsigned short cp;
		unsigned short config;
	} SETTINGS[] = 
		{
			{	1540, 0, 20, 3, 0x0070 }, // 960-1540
			{   2400, 0, 20, 3, 0x0060 }, // 2340-2400
			{   2840, 1, 10, 3, 0x0060 }, // 2400-2840
			{   3240, 1, 10, 4, 0x0050 }, // 2840-3240
			{   3590, 1, 10, 2, 0x0000 }, // 3240-3590
			{ 0xffff, 1, 10, 7, 0x0040 }, // 3590-3940
		};

	unsigned int band;

	for(band = 0; band < _countof(SETTINGS); ++band)
	{
		if(freqMhz <= SETTINGS[band].freqMhz)
		{
			break;
		}
	}

	CSingleLock lock(&m_mutex, TRUE);
	(*m_shMem)[id].lo1FreqMhz = freqMhz;
	(*m_shMem)[id].rfFreqHalfMhz = 2 * freqMhz + 280 - (*m_shMem)[id].lo2FreqHalfMhz;

	Poke(id, CONFIG_REG, (Peek(id, CONFIG_REG) & 0x7907) | SETTINGS[band].config);
	UPllLatch latch = { 0 };
	latch.controlStatus.pll = LO1;
	latch.initLatch.c = 3;
	latch.initLatch.f1 = 0;
	latch.initLatch.pd1 = 0;
	latch.initLatch.m = 5;
	latch.initLatch.f2 = 0;
	latch.initLatch.f3 = 0;
	latch.initLatch.f4 = 0;
	latch.initLatch.f5 = 0;
	latch.initLatch.tc = 0;
	latch.initLatch.cp1 = SETTINGS[band].cp;
	latch.initLatch.cp2 = SETTINGS[band].cp;
	latch.initLatch.pd2 = 0;
	latch.initLatch.p = SETTINGS[band].p;
	SetPllLatch(latch, id);
	latch.allBits = 0;
	latch.controlStatus.pll = LO1;
	latch.functionLatch.c = 2;
	latch.functionLatch.f1 = 0;
	latch.functionLatch.pd1 = 0;
	latch.functionLatch.m = 5;
	latch.functionLatch.f2 = 0;
	latch.functionLatch.f3 = 0;
	latch.functionLatch.f4 = 0;
	latch.functionLatch.f5 = 0;
	latch.functionLatch.tc = 0;
	latch.functionLatch.cp1 = SETTINGS[band].cp;
	latch.functionLatch.cp2 = SETTINGS[band].cp;
	latch.functionLatch.pd2 = 0;
	latch.functionLatch.p = SETTINGS[band].p;
	SetPllLatch(latch, id);
	latch.allBits = 0;
	latch.controlStatus.pll = LO1;
	latch.refCounterLatch.c = 0;
	latch.refCounterLatch.r = SETTINGS[band].r;
	latch.refCounterLatch.abp = 2;
	latch.refCounterLatch.t = 0;
	latch.refCounterLatch.lpd = 1;
	SetPllLatch(latch, id);
	latch.allBits = 0;
	latch.controlStatus.pll = LO1;
	latch.nCounterLatch.c = 1;
	latch.nCounterLatch.a = freqMhz / (100 / SETTINGS[band].r) % (8 * (SETTINGS[band].p + 1));
	latch.nCounterLatch.b = freqMhz / (100 / SETTINGS[band].r) / (8 * (SETTINGS[band].p + 1));
	latch.nCounterLatch.g = 0;
	SetPllLatch(latch, id);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set LO2
//
void C26122002::SetLo2(unsigned short freqHalfMhz, unsigned char id)
{
	CSingleLock lock(&m_mutex, TRUE);
	(*m_shMem)[id].lo2FreqHalfMhz = freqHalfMhz;
	(*m_shMem)[id].rfFreqHalfMhz = 2 * (*m_shMem)[id].lo1FreqMhz + 280 - freqHalfMhz;
	unsigned short config = Peek(id, CONFIG_REG) & 0x7877;

	if(freqHalfMhz < 4000)
	{
		freqHalfMhz *= 2;
		config |= 0x0100;
	}

	Poke(id, CONFIG_REG, config);

	unsigned short r = ((freqHalfMhz % 2) ? 200 : 100);
	UPllLatch latch = { 0 };
	latch.allBits = 0;
	latch.controlStatus.pll = LO2;
	latch.initLatch.c = 3;
	latch.initLatch.f1 = 0;
	latch.initLatch.pd1 = 0;
	latch.initLatch.m = 5;
	latch.initLatch.f2 = 1;
	latch.initLatch.f3 = 0;
	latch.initLatch.f4 = 1;
	latch.initLatch.f5 = 1;
	latch.initLatch.tc = 4;
	latch.initLatch.cp1 = 7;
	latch.initLatch.cp2 = 7;
	latch.initLatch.pd2 = 0;
	latch.initLatch.p = 0;
	SetPllLatch(latch, id);
	latch.allBits = 0;
	latch.controlStatus.pll = LO2;
	latch.functionLatch.c = 2;
	latch.functionLatch.f1 = 0;
	latch.functionLatch.pd1 = 0;
	latch.functionLatch.m = 5;
	latch.functionLatch.f2 = 1;
	latch.functionLatch.f3 = 0;
	latch.functionLatch.f4 = 1;
	latch.functionLatch.f5 = 1;
	latch.functionLatch.tc = 4;
	latch.functionLatch.cp1 = 7;
	latch.functionLatch.cp2 = 7;
	latch.functionLatch.pd2 = 0;
	latch.functionLatch.p = 0;
	SetPllLatch(latch, id);
	latch.allBits = 0;
	latch.controlStatus.pll = LO2;
	latch.refCounterLatch.c = 0;
	latch.refCounterLatch.r = r;
	latch.refCounterLatch.abp = 2;
	latch.refCounterLatch.t = 0;
	latch.refCounterLatch.lpd = 1;
	SetPllLatch(latch, id);
	latch.allBits = 0;
	latch.controlStatus.pll = LO2;
	latch.nCounterLatch.c = 1;
	latch.nCounterLatch.a = freqHalfMhz / (200 / r) % 8;
	latch.nCounterLatch.b = freqHalfMhz / (200 / r) / 8;
	latch.nCounterLatch.g = 0;
	SetPllLatch(latch, id);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set LO3
//
void C26122002::SetLo3(unsigned short freqMhz, unsigned char id)
{
	CSingleLock lock(&m_mutex, TRUE);
	(*m_shMem)[id].lo3FreqMhz = freqMhz;
	UPllLatch latch = { 0 };
	latch.initLatch.pll = LO3;
	latch.initLatch.c = 3;
	latch.initLatch.f1 = 0;
	latch.initLatch.pd1 = 0;
	latch.initLatch.m = 5;
	latch.initLatch.f2 = 1;
	latch.initLatch.f3 = 0;
	latch.initLatch.f4 = 1;
	latch.initLatch.f5 = 1;
	latch.initLatch.tc = 15;
	latch.initLatch.cp1 = 7;
	latch.initLatch.cp2 = 7;
	latch.initLatch.pd2 = 0;
	latch.initLatch.p = 0;
	SetPllLatch(latch, id);
	latch.allBits = 0;
	latch.functionLatch.pll = LO3;
	latch.functionLatch.c = 2;
	latch.functionLatch.f1 = 0;
	latch.functionLatch.pd1 = 0;
	latch.functionLatch.m = 5;
	latch.functionLatch.f2 = 1;
	latch.functionLatch.f3 = 0;
	latch.functionLatch.f4 = 1;
	latch.functionLatch.f5 = 1;
	latch.functionLatch.tc = 15;
	latch.functionLatch.cp1 = 7;
	latch.functionLatch.cp2 = 7;
	latch.functionLatch.pd2 = 0;
	latch.functionLatch.p = 0;
	SetPllLatch(latch, id);
	latch.allBits = 0;
	latch.refCounterLatch.pll = LO3;
	latch.refCounterLatch.c = 0;
	latch.refCounterLatch.r = 100;
	latch.refCounterLatch.abp = 2;
	latch.refCounterLatch.t = 0;
	latch.refCounterLatch.lpd = 1;
	SetPllLatch(latch, id);
	latch.allBits = 0;
	latch.nCounterLatch.pll = LO3;
	latch.nCounterLatch.c = 1;
	latch.nCounterLatch.a = freqMhz % 8;
	latch.nCounterLatch.b = freqMhz / 8;
	latch.nCounterLatch.g = 0;
	SetPllLatch(latch, id);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set a PLL IC latch
//
void C26122002::SetPllLatch(const UPllLatch& latch, unsigned char id)
{
	Poke(id, PLL_CTRL_HI_REG, latch.reg.hi);
	Poke(id, PLL_CTRL_LO_REG, latch.reg.lo);

	// Busy wait
	static const unsigned long MAX_WAIT = 1000000;
	unsigned long i;

	for(i = 0; (Peek(id, PLL_CTRL_LO_REG) & 0x0001u) == latch.controlStatus.done && i < MAX_WAIT; ++i)
	{
		_mm_pause();
	}

#ifdef _DEBUG
	if(i == MAX_WAIT)
	{
		TRACE(_T("2612-2002 PLL control timed out\n"));
	}
#endif

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the sample rate generator
//
void C26122002::SetSampleRate(Units::Frequency sampleRate, signed char idOrAll)
{
	CSingleLock lock(&m_mutex, TRUE);

	for(unsigned char id = (idOrAll < 0 ? 0 : idOrAll); id < (idOrAll < 0 ? int(MAX_INSTANCES) : idOrAll + 1); ++id)
	{
		if(IsPresent(id) && sampleRate != (*m_shMem)[id].sampleRate)
		{
			(*m_shMem)[id].sampleRate = sampleRate;

			// Set config word
			static const struct 
			{
				Units::Frequency rate;
				unsigned short config;
			} CONFIG_SETTINGS[] = 
				{
					{  25000000, 0x2000 },
					{  50000000, 0x1000 },
					{ 105000000, 0x0000 }
				};

			unsigned int band;

			for(band = 0; band < _countof(CONFIG_SETTINGS); ++band)
			{
				if(sampleRate < CONFIG_SETTINGS[band].rate)
				{
					break;
				}
			}

			ASSERT(band != _countof(CONFIG_SETTINGS));

			if(band == _countof(CONFIG_SETTINGS))
			{
				band = 0;
			}

			Poke(id, CONFIG_REG, (Peek(id, CONFIG_REG) & 0x0977) | CONFIG_SETTINGS[band].config);

			// Program PLL
			static const struct 
			{
				// sampleRate = (nDiv + (frac / mod)) * (10000000 / rDiv)
				unsigned long sampleRate;
				unsigned short rDiv;
				unsigned short nDiv;
				unsigned short mod;
				unsigned short frac;
				unsigned char cp;
			} PLL_SETTINGS[] = 
				{
					{  30720000, 10, 61,  25,  11, 7 },
					{  32768000,  5, 32, 125,  96, 3 },
					{  51200000,  8, 40,  25,  24, 5 },
					{  52428800,  5, 26, 625, 134, 5 },
					{  54613333,  8, 43, 375, 259, 5 },
					{  61440000, 10, 61,  25,  11, 7 },
					{  65536000,  5, 32, 125,  96, 3 },
					{  68266667,  5, 34,  15,   2, 4 },
					{  73728000,  5, 36, 375, 324, 4 },
					{  81920000,  4, 32, 125,  96, 3 },
					{  87381333,  5, 43, 375, 259, 5 },
					{  98304000,  4, 39, 625, 201, 5 },
					{ 102400000,  4, 40,  25,  24, 5 },
					{ 104857600,  5, 52, 625, 268, 5 }
				};

			unsigned int rate;

			for(rate = 0; rate < _countof(PLL_SETTINGS); ++rate)
			{
				if(sampleRate == PLL_SETTINGS[rate].sampleRate)
				{
					break;
				}
			}

			ASSERT(rate != _countof(PLL_SETTINGS));

			if(rate == _countof(PLL_SETTINGS))
			{
				rate = 0;
			}
			
			UPllLatch latch = { 0 };
			latch.noiseSpurLatch.pll = ADC;
			latch.noiseSpurLatch.c = 3;
			latch.noiseSpurLatch.t1 = 0;
			latch.noiseSpurLatch.t2 = 0;
			SetPllLatch(latch, id);
			latch.allBits = 0;
			latch.controlLatch.pll = ADC;
			latch.controlLatch.c = 2;
			latch.controlLatch.u1 = 0;
			latch.controlLatch.u2 = 0;
			latch.controlLatch.u3 = 0;
			latch.controlLatch.u4 = 0;
			latch.controlLatch.u5 = 0;
			latch.controlLatch.cp1 = PLL_SETTINGS[rate].cp;
			latch.controlLatch.cp2 = 0;
			latch.controlLatch.u6 = 0;
			SetPllLatch(latch, id);
			latch.allBits = 0;
			latch.rDividerLatch.pll = ADC;
			latch.rDividerLatch.c = 1;
			latch.rDividerLatch.m = PLL_SETTINGS[rate].mod;
			latch.rDividerLatch.r = PLL_SETTINGS[rate].rDiv;
			latch.rDividerLatch.p1 = 0;
			latch.rDividerLatch.mx = 5;
			latch.rDividerLatch.p3 = 0;
			SetPllLatch(latch, id);
			latch.allBits = 0;
			latch.nDividerLatch.pll = ADC;
			latch.nDividerLatch.c = 0;
			latch.nDividerLatch.f = PLL_SETTINGS[rate].frac;
			latch.nDividerLatch.n = PLL_SETTINGS[rate].nDiv;
			latch.nDividerLatch.fl = 0;
			SetPllLatch(latch, id);
			SetSettleTime(5000);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the seconds registers (uses time_t semantics)
//
void C26122002::SetSeconds(time_t secs, unsigned char id)
{
	do
	{
		Poke(id, GPS_SEC_HI_REG, unsigned short(secs >> 16));
		Poke(id,GPS_SEC_LO_REG, unsigned short(secs & 0xffff));
	} while(Peek(id, GPS_FRAC_SEC_REG) == 0);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Tune the hardware. Returns final IF freq
//
Units::Frequency C26122002::Tune(Units::Frequency freq, Units::Frequency rxBw, signed char idOrAll)
{
	// Validate
	ASSERT(freq <= RF_HIGH_FREQ_HZ + rxBw / 2); // VCP allows tuning down to DC

	unsigned short freqHalfMhz = unsigned short((freq + MIN_TUNE_STEP - FILTER_ASYMMETRY_HZ) / Units::Frequency(MIN_TUNE_STEP)); // Round to nearest 0.5 MHz (offset for filter asymmetry)
	CSingleLock lock(&m_mutex, TRUE);

	for(unsigned char id = (idOrAll < 0 ? 0 : idOrAll); id < (idOrAll < 0 ? int(MAX_INSTANCES) : idOrAll + 1); ++id)
	{
		if(IsPresent(id))
		{
			if(freqHalfMhz != (*m_shMem)[id].rfFreqHalfMhz)
			{
				static const struct 
				{
					unsigned short rfFreqHalfMhz;
					unsigned short lo1IfFreqMhz;
					unsigned short roundMhz;
					unsigned short config;
				} SETTINGS[] = 
					{
						{   1200,  940, 15, 0x0170 },
						{   1760, 1960, 10, 0x0060 },
						{   2560, 1960, 10, 0x0050 },
						{   2800, 1960, 10, 0x0000 },
						{   3800,  940, 15, 0x0160 },
						{   4600,  940, 15, 0x0150 },
						{   5300,  940, 15, 0x0100 },
						{ 0xffff,  940, 15, 0x0140 },
					};

				unsigned int band;

				for(band = 0; band < _countof(SETTINGS); ++band)
				{
					if(freqHalfMhz < SETTINGS[band].rfFreqHalfMhz)
					{
						break;
					}
				}

				// Settle times
#ifndef TCI_INCLUDE_FAST_MODE
				SetSettleTime(1000); // Enforce ECCN 5A001.a export restriction
#endif
				unsigned short config = Peek(id, CONFIG_REG);

				if((config ^ SETTINGS[band].config) & 0x0100)
				{
					// Band switch
					SetSettleTime(8000);
				}
				else if((config ^ SETTINGS[band].config) & 0x0070)
				{
					// Bank switch
					SetSettleTime(5000);
				}
				else
				{
					// LO1 step
					unsigned short lo1Step = unsigned short(abs((freqHalfMhz + SETTINGS[band].roundMhz) / 20 -
						((*m_shMem)[id].rfFreqHalfMhz + SETTINGS[band].roundMhz) / 20)); // 10 MHz units

					if(lo1Step > 0)
					{
						SetSettleTime(50 * lo1Step + 900);
					}

					// LO2 step
					unsigned short lo2Step = unsigned short(abs(freqHalfMhz % 20 - (*m_shMem)[id].rfFreqHalfMhz % 20)); // 0.5 MHz units

					if(SETTINGS[band].lo1IfFreqMhz == 940)
					{
						lo2Step *= 2;
					}

					if(lo2Step > 0)
					{
						SetSettleTime(50 * lo2Step + 2000);
					}
				}

				// Program LO1
				unsigned short lo1FreqMhz = (freqHalfMhz + SETTINGS[band].roundMhz) / 20 * 10 + SETTINGS[band].lo1IfFreqMhz; // 1 MHz units; multiple of 10 MHz

				if(lo1FreqMhz != (*m_shMem)[id].lo1FreqMhz)
				{
					SetLo1(lo1FreqMhz, id);
				}

				// Program LO2
				unsigned short lo2FreqHalfMhz = 2 * lo1FreqMhz - freqHalfMhz + 280; // 0.5 MHz units

				if(lo2FreqHalfMhz != (*m_shMem)[id].lo2FreqHalfMhz)
				{
					SetLo2(lo2FreqHalfMhz, id);
				}
			}

			// Program LO3
			unsigned short lo3FreqMhz =
				unsigned short(140 + (rxBw <= NARROW_BW_HZ ? FINAL_IF_FREQ_NARROW_BW_HZ : FINAL_IF_FREQ_WIDE_BW_HZ) / 1000000);

			if(lo3FreqMhz != (*m_shMem)[id].lo3FreqMhz)
			{
				SetLo3(lo3FreqMhz, id);
				SetSettleTime(4000); // LO3 settle time
			}
		}
	}

	return (rxBw <= NARROW_BW_HZ ? FINAL_IF_FREQ_NARROW_BW_HZ : FINAL_IF_FREQ_WIDE_BW_HZ) + MIN_TUNE_STEP * freqHalfMhz - freq;
}


//////////////////////////////////////////////////////////////////////
//
// Use the PIT to delay
//
void C26122002::Wait(unsigned long us)
{
	if(us == 0)
	{
		return;
	}

	ASSERT(us < 131072);

	if(us < 20)
	{
		// Use busy wait for short delays
		LARGE_INTEGER done;
		QueryPerformanceCounter(&done);
		LARGE_INTEGER count = done;
		LARGE_INTEGER freq;
		QueryPerformanceFrequency(&freq);
		done.QuadPart += us * freq.QuadPart / 1000000;
		
		while((done.QuadPart - count.QuadPart) > 0)	// Note: count < done doesn't work because of wraparound
		{
			QueryPerformanceCounter(&count);
		}
	}
	else
	{
		if(!m_intHandler)
		{
			m_intHandler = true;
			RegisterInterruptHandler(GEF_VME_INT_VIRQ5, OnTimer, this, ((CARD_CLASS & 0x3) << 6) | (CARD_TYPE << 3));
		}

		Poke(0, PIT_REG, unsigned short(us / 2));

		if(!m_expired.Lock(us / 1000 + 2))
		{
			TRACE(_T("Hardware timer failed to fire\n"));
		}
	}

	return;
}
