/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "Digitizer.h"
#include "Singleton.h"

class C26222001
{
public:
	// Constants
	static const unsigned char ATTEN_STEP = 2; // dB
	static const Units::Frequency C_2622_BW;
	static const float IF_MUX_LOSS_2612RX_UHF; // dB
	static const float IF_MUX_LOSS_2614RX_UHF; // dB
	static const unsigned char MAX_ATTEN = 62; // dB
	static const signed char MAX_GAIN = 65; // dB
	static const unsigned char MIN_ATTEN = 0; // dB
	static const short NOISE_LEVEL_DBMHZ = -155; // dBm/Hz
	static const unsigned long RF_HIGH_FREQ_HZ = 30000000; // Hz
	static const unsigned long RF_LOW_FREQ_HZ = 0; // Hz
	static const unsigned char RFCH = 0; // On channel 0

	// Functions
	//@note: 2622 has no internal cal-tone, therefore always returns 0.
	static Units::Frequency GetCalFreq(Units::Frequency ) { return 0; }

	C26222001(void);
	~C26222001(void);
	unsigned char GetAtten() const { return m_atten; }
	float GetGainRatio(float max_adj = 0.0f) const;
	bool IsMuxHf(void) const { return (m_muxSet2Hf); }
	unsigned char SetAtten(unsigned char atten);
	void SetMux(bool hf);

protected:
	// Functions
	unsigned short GetControlReg(void) const { return m_digitizer->GetTtlData(); }
	void SetAttenRaw(unsigned char bits);

	// Data

private:
	// Constants
	static LPCTSTR MUTEX_NAME;

	// Types
	struct SECURITY_ATTRIBUTES_ALL_ACCESS : public SECURITY_ATTRIBUTES
	{
		SECURITY_ATTRIBUTES_ALL_ACCESS(void)
		{
			if(!InitializeSecurityDescriptor(&m_secDesc, SECURITY_DESCRIPTOR_REVISION))
			{
				AfxThrowFileException(CFileException::genericException, GetLastError());
			}

#pragma warning(suppress : 6248) // Null DACL
			if(!SetSecurityDescriptorDacl(&m_secDesc, TRUE, nullptr, TRUE))
			{
				AfxThrowFileException(CFileException::genericException, GetLastError());
			}

			nLength = sizeof(SECURITY_ATTRIBUTES);
			lpSecurityDescriptor = &m_secDesc;
			bInheritHandle = FALSE;

			return;
		}

	private:
		SECURITY_DESCRIPTOR m_secDesc;
	};

	// Functions
	virtual void SetSettleTime(unsigned int) {}

	// Data
	unsigned char m_atten;
	CSingleton<CDigitizer> m_digitizer;
	bool m_first;
	mutable CMutex m_mutex;
	bool m_muxSet2Hf;       //true: Mux output is set to HF sample.
                            //false: Mux output is set to V/U/SHF sample.
};
