/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2011-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "StdAfx.h"

#include <algorithm>
#include <memory>

#include "26212001.h"
#include "DownConverter.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
const CDownConverter::BandSettings C26212001::BAND_SETTINGS(BAND_SETTINGS_, &BAND_SETTINGS_[_countof(BAND_SETTINGS_)]);
const CDownConverter::BandSettings C26212001::FULLBAND_SETTINGS(FULLBAND_SETTINGS_, &FULLBAND_SETTINGS_[_countof(FULLBAND_SETTINGS_)]);
const float C26212001::IF_MUX_LOSS_2612RX_UHF = 0.8f; // dB
const float C26212001::IF_MUX_LOSS_2614RX_UHF = 1.2f; // dB
LPCTSTR C26212001::MUTEX_NAME = _T("Global\\TCI.{D8186CBF-5517-4542-B540-4B9D3DF3817E}");
const CDownConverter::BandSettings C26212001::WIDEBAND_SETTINGS(WIDEBAND_SETTINGS_, &WIDEBAND_SETTINGS_[_countof(WIDEBAND_SETTINGS_)]);


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C26212001::C26212001(void)
:
	CVme(),
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get())
{
	m_first = !(GetLastError() == ERROR_ALREADY_EXISTS); // Status from m_mutex construction
	SCardStatus status = GetCardStatus(true);
	m_present = (status.cardClass == (CARD_CLASS & 0x3) && status.cardType == CARD_TYPE && status.id == 0);

	if(m_present)
	{
		if(m_first)
		{
			SetAtten(MAX_ATTEN);
			bool bypass;
			Units::FreqPair freqLimits;
			size_t band;
			Tune(30000000, 0, OPTIMUM, bypass, freqLimits, band);
			SetInput(TERM);
			SetMux(false);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the bypass state
//
bool C26212001::GetBypass(unsigned char ch) const
{
	return (Peek(CONTROL_REG_1) & (ch == 0 ? 0x4000 : 0x0040)) != 0;
}


//////////////////////////////////////////////////////////////////////
//
// Get the card status
//
C26212001::SCardStatus C26212001::GetCardStatus(bool force) const
{
	unsigned short reg = 0;

	if(force || m_present)
	{
		reg = Peek(STATUS_REG);
	}

	return *reinterpret_cast<SCardStatus*>(&reg);
}


//////////////////////////////////////////////////////////////////////
//
// Get the receiver gain
//
float C26212001::GetGainRatio(unsigned char ch) const
{
	return pow(10.0f, (GetBypass(ch) ? BYPASS_GAIN : MAX_GAIN - m_atten[ch]) / 10.0f);
}


//////////////////////////////////////////////////////////////////////
//
// Get the board version
//
unsigned char C26212001::GetVersion(void) const
{
	return GetCardStatus().version;
}


//////////////////////////////////////////////////////////////////////
//
// Is the mux set to HF?
//
bool C26212001::IsMuxHf(unsigned char ch) const
{
	if(ch == 0)
	{
		return (Peek(CONTROL_REG_1) & 0x8000) == 0;
	}
	else
	{
		return (Peek(CONTROL_REG_1) & 0x0080) != 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Set the attenuation
//
unsigned char C26212001::SetAtten(unsigned char atten, signed char chOrBoth)
{
	ASSERT(chOrBoth >= -1 && chOrBoth < 2);

	if(atten > MAX_ATTEN)
	{
		atten = MAX_ATTEN;
	}

	SetAttenRaw(atten / 2, chOrBoth);

	return chOrBoth == 0 ? m_atten[0] : m_atten[1];
}


//////////////////////////////////////////////////////////////////////
//
// Set the atten
//
void C26212001::SetAttenRaw(unsigned char bits, signed char chOrBoth)
{
	if(m_present)
	{
		unsigned short data = 0;
		unsigned short mask = 0;

		if(chOrBoth == -1 || chOrBoth == 0)
		{
			data |= (bits << 11);
			mask |= 0xf800;
			m_atten[0] = 2 * bits;
		}

		if(chOrBoth == -1 || chOrBoth == 1)
		{
			data |= (bits << 3);
			mask |= 0x00f8;
			m_atten[1] = 2 * bits;
		}

		unsigned short reg = Peek(CONTROL_REG_2);

		if((reg & mask) != data)
		{
			Poke(CONTROL_REG_2, (reg & ~mask) | data);
			SetSettleTime(5);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the RF band (bypass is band 0)
//
void C26212001::SetBand(unsigned char band, signed char chOrBoth)
{
	ASSERT(band < _countof(BAND_SETTINGS_));

	if(band < _countof(BAND_SETTINGS_))
	{
		unsigned short bandData = BAND_SETTINGS_[band].data;

		if(m_present)
		{
			unsigned short data = 0;
			unsigned short mask = 0;

			if(chOrBoth == -1 || chOrBoth == 0)
			{
				data |= (bandData << 8);
				mask |= 0x7c00;
			}

			if(chOrBoth == -1 || chOrBoth == 1)
			{
				data |= bandData;
				mask |= 0x007c;
			}

			unsigned short reg = Peek(CONTROL_REG_1);

			if((reg & mask) != data)
			{
				Poke(CONTROL_REG_1, (reg & ~mask) | data);
				SetSettleTime(5);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Put in bypass mode
//
void C26212001::SetBypass(bool on, signed char chOrBoth)
{
	if(m_present)
	{
		unsigned short data = 0;
		unsigned short mask = 0;

		if(chOrBoth == -1 || chOrBoth == 0)
		{
			data |= (on ? 0x4000 : 0x0000);
			mask |= 0x4000;
		}

		if(chOrBoth == -1 || chOrBoth == 1)
		{
			data |= (on ? 0x0040 : 0x0000);
			mask |= 0x0040;
		}

		unsigned short reg = Peek(CONTROL_REG_1);

		if((reg & mask) != data)
		{
			Poke(CONTROL_REG_1, (reg & ~mask) | data);
			SetSettleTime(5);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set to full band (2-30 MHz)
//
void C26212001::SetFullband(unsigned char band, signed char chOrBoth)
{
	ASSERT(band < _countof(FULLBAND_SETTINGS_));

	if(band < _countof(FULLBAND_SETTINGS_))
	{
		unsigned short bandData = FULLBAND_SETTINGS_[band].data;

		if(m_present)
		{
			unsigned short data = 0;
			unsigned short mask = 0;

			if(chOrBoth == -1 || chOrBoth == 0)
			{
				data |= (bandData << 8);
				mask |= 0x7c00;
			}

			if(chOrBoth == -1 || chOrBoth == 1)
			{
				data |= bandData;
				mask |= 0x007c;
			}

			unsigned short reg = Peek(CONTROL_REG_1);

			if((reg & mask) != data)
			{
				Poke(CONTROL_REG_1, (reg & ~mask) | data);
				SetSettleTime(5);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the input source
//
void C26212001::SetInput(EInput input, signed char chOrBoth)
{
	static const unsigned short DATA1[] = // [input]
	{
		0x0002, // RF1
		0x0002, // RF2
		0x0002, // TERM
		0x0003, // CAL
		0x0001, // INT_NOISE
		0x0000  // EXT_NOISE
	};

	static const unsigned short DATA2[][2] = // [input][ch]
	{
		{ 0x0100, 0x0006 }, // RF1
		{ 0x0300, 0x0004 }, // RF2
		{ 0x0500, 0x0000 }, // TERM
		{ 0x0600, 0x0003 }, // CAL
		{ 0x0600, 0x0003 }, // INT_NOISE
		{ 0x0600, 0x0003 }  // EXT_NOISE
	};

	if(m_present)
	{
		unsigned short reg1 = Peek(CONTROL_REG_1);
		unsigned short data1 = (reg1 & 0x0003);
		unsigned short reg2 = Peek(CONTROL_REG_2);

		if((chOrBoth != 1 || (reg2 & 0x0600) != 0x0600) && (chOrBoth != 0 || (reg2 & 0x0006) != 0x002))
		{
			// Opposite channel is not in a cal mode
			data1 = DATA1[input];
		}

		unsigned short data2 = 0;
		unsigned short mask2 = 0;

		if(chOrBoth == -1 || chOrBoth == 0)
		{
			data2 |= DATA2[input][0];
			mask2 |= 0x0700;
		}

		if(chOrBoth == -1 || chOrBoth == 1)
		{
			data2 |= DATA2[input][1];
			mask2 |= 0x0007;
		}

		if((reg1 & 0x0003) != data1)
		{
			Poke(CONTROL_REG_1, (reg1 & 0xfffc) | data1);
			SetSettleTime(input == INT_NOISE || input == CAL ? 300 : 5);
		}

		if((reg2 & mask2) != data2)
		{
			Poke(CONTROL_REG_2, (reg2 & ~mask2) | data2);
			SetSettleTime(input == INT_NOISE || input == CAL ? 300 : 5);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the mux routing
//
void C26212001::SetMux(bool hf, signed char chOrBoth)
{
	if(m_present)
	{
		unsigned short data = 0;
		unsigned short mask = 0;

		if(chOrBoth == -1 || chOrBoth == 0)
		{
			data |= (hf ? 0x0000 : 0x8000);
			mask |= 0x8000;
		}

		if(chOrBoth == -1 || chOrBoth == 1)
		{
			data |= (hf ? 0x0080 : 0x0000);
			mask |= 0x0080;
		}

		unsigned short reg = Peek(CONTROL_REG_1);

		if((reg & mask) != data)
		{
			Poke(CONTROL_REG_1, (reg & ~mask) | data);
			SetSettleTime(5);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set to test mode
//
void C26212001::SetTestMode(signed char chOrBoth)
{
	if(m_present)
	{
		unsigned short data = 0;
		unsigned short mask = 0;

		if(chOrBoth == -1 || chOrBoth == 0)
		{
			data |= 0x2000;
			mask |= 0x7c00;
		}

		if(chOrBoth == -1 || chOrBoth == 1)
		{
			data |= 0x0020;
			mask |= 0x007c;
		}

		unsigned short reg = Peek(CONTROL_REG_1);

		if((reg & mask) != data)
		{
			Poke(CONTROL_REG_1, (reg & ~mask) | data);
			SetSettleTime(5);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set a wideband band (bypass is band 0)
//
void C26212001::SetWideband(unsigned char band, signed char chOrBoth)
{
	ASSERT(band < _countof(WIDEBAND_SETTINGS_));

	if(band < _countof(WIDEBAND_SETTINGS_))
	{
		unsigned short bandData = WIDEBAND_SETTINGS_[band].data;

		if(m_present)
		{
			unsigned short data = 0;
			unsigned short mask = 0;

			if(chOrBoth == -1 || chOrBoth == 0)
			{
				data |= (bandData << 8);
				mask |= 0x7c00;
			}

			if(chOrBoth == -1 || chOrBoth == 1)
			{
				data |= bandData;
				mask |= 0x007c;
			}

			unsigned short reg = Peek(CONTROL_REG_1);

			if((reg & mask) != data)
			{
				Poke(CONTROL_REG_1, (reg & ~mask) | data);
				SetSettleTime(5);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the signal routing
//
void C26212001::Tune(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ bool& bypass,
					 _Out_ Units::FreqPair& freqLimits, _Out_ size_t& band, signed char chOrBoth)
{
/*	if(rxBw <= BAND_SETTINGS.back().high1dbFreq - BAND_SETTINGS.back().low1dbFreq)
	{
		TuneNarrowband(freq, rxBw, bandSelect, bypass, freqLimits, band, chOrBoth);
	}
	else if(rxBw <= WIDEBAND_SETTINGS.back().high1dbFreq - WIDEBAND_SETTINGS.back().low1dbFreq)
	{
		TuneWideband(freq, rxBw, bandSelect, bypass, freqLimits, band, chOrBoth);
		band += BAND_SETTINGS.size() - 1;
	}
	else
	{
		TuneFullband(freq, rxBw, bandSelect, bypass, freqLimits, band, chOrBoth);
		band += BAND_SETTINGS.size() + WIDEBAND_SETTINGS.size() - 2;
	} */

	// Use the narrowest band scheme that allows rxBw to be covered in one or two bands
	if(GetBand(freq, rxBw, bandSelect, freqLimits, BAND_SETTINGS), freqLimits.first <= freq - rxBw / 2 &&
		freqLimits.second >= std::min(Units::Frequency(RF_HIGH_FREQ_HZ), freq + rxBw / 2))
	{
		// Narrowband using one band
		TuneNarrowband(freq, rxBw, bandSelect, bypass, freqLimits, band, chOrBoth);
	}
	else if(GetBand(freq, rxBw, bandSelect, freqLimits, WIDEBAND_SETTINGS), freqLimits.first <= freq - rxBw / 2 &&
		freqLimits.second >= std::min(Units::Frequency(RF_HIGH_FREQ_HZ), freq + rxBw / 2))
	{
		// Wideband using one band
		TuneWideband(freq, rxBw, bandSelect, bypass, freqLimits, band, chOrBoth);
		band += BAND_SETTINGS.size() - 1;
	}
	else if(GetBand(freq, rxBw, bandSelect, freqLimits, FULLBAND_SETTINGS), freqLimits.first <= freq - rxBw / 2 &&
		freqLimits.second >= std::min(Units::Frequency(RF_HIGH_FREQ_HZ), freq + rxBw / 2))
	{
		// Fullband using one band
		TuneFullband(freq, rxBw, bandSelect, bypass, freqLimits, band, chOrBoth);
		band += BAND_SETTINGS.size() + WIDEBAND_SETTINGS.size() - 2;
	}
	else if(GetBand(freq, rxBw, UPPER, freqLimits, BAND_SETTINGS) == GetBand(freq, rxBw, LOWER, freqLimits, BAND_SETTINGS) + 1)
	{
		// Narrowband using two bands
		TuneNarrowband(freq, rxBw, bandSelect, bypass, freqLimits, band, chOrBoth);
	}
	else if(GetBand(freq, rxBw, UPPER, freqLimits, WIDEBAND_SETTINGS) == GetBand(freq, rxBw, LOWER, freqLimits, WIDEBAND_SETTINGS) + 1)
	{
		// Wideband using two bands
		TuneWideband(freq, rxBw, bandSelect, bypass, freqLimits, band, chOrBoth);

		if(band > 0)
		{
			band += BAND_SETTINGS.size() - 1;
		}
	}
	else
	{
		// Fullband
		TuneFullband(freq, rxBw, bandSelect, bypass, freqLimits, band, chOrBoth);

		if(band > 0)
		{
			band += BAND_SETTINGS.size() + WIDEBAND_SETTINGS.size() - 2;
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the signal routing
//
void C26212001::TuneFullband(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ bool& bypass,
					 _Out_ Units::FreqPair& freqLimits, _Out_ size_t& band, signed char chOrBoth)
{
	ASSERT(freq <= RF_HIGH_FREQ_HZ + rxBw / 2);
	band = GetBand(freq, rxBw, bandSelect, freqLimits, FULLBAND_SETTINGS);
	bypass = (band == 0);
	SetFullband(unsigned char(band), chOrBoth);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the signal routing
//
void C26212001::TuneNarrowband(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ bool& bypass,
					 _Out_ Units::FreqPair& freqLimits, _Out_ size_t& band, signed char chOrBoth)
{
	ASSERT(freq <= RF_HIGH_FREQ_HZ + rxBw / 2);
	band = GetBand(freq, rxBw, bandSelect, freqLimits, BAND_SETTINGS);
	bypass = (band == 0);
	SetBand(unsigned char(band), chOrBoth);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the signal routing
//
void C26212001::TuneWideband(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ bool& bypass,
					 _Out_ Units::FreqPair& freqLimits, _Out_ size_t& band, signed char chOrBoth)
{
	ASSERT(freq <= RF_HIGH_FREQ_HZ + rxBw / 2);
	band = GetBand(freq, rxBw, bandSelect, freqLimits, WIDEBAND_SETTINGS);
	bypass = (band == 0);
	SetWideband(unsigned char(band), chOrBoth);

	return;
}
