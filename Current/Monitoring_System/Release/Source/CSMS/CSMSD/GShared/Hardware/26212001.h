/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2011 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#pragma once

#include "DownConverter.h"
#include "Units.h"
#include "Vme.h"

class C26212001
:
	// Inheritance
	public CDownConverter,
	protected virtual CVme
{
	// Friends
	friend class C26212001Page; // VCP needs direct access to set 10 MHz filters (not used outside VCP)

public:
	// Constants
	static const unsigned char ATTEN_STEP = 2; // dB
	static const signed char BYPASS_GAIN = 8; // dB
	static const float IF_MUX_LOSS_2612RX_UHF; // dB
	static const float IF_MUX_LOSS_2614RX_UHF; // dB
	static const unsigned char MAX_ATTEN = 62; // dB
	static const signed char MAX_GAIN = 58; // dB
	static const unsigned char MIN_ATTEN = 0; // dB
	static const unsigned long NARROW_BW_HZ = 2000000; // Hz
	static const short NOISE_LEVEL_DBMHZ = -124; // dBm/Hz
	static const unsigned long NOISE_LOW_FREQ_HZ = 2000000; // Hz
	static const unsigned long RF_HIGH_FREQ_HZ = 30000000; // Hz
	static const unsigned long RF_LOW_FREQ_HZ = 2000000; // Hz
	static const unsigned long WIDE_BW_HZ = 20000000; // Hz

	// Functions
	static Units::Frequency GetCalFreq(Units::Frequency freq) { return freq < NOISE_LOW_FREQ_HZ ? 0 : freq; }

protected:
	// Constants
	static const BandSettings BAND_SETTINGS;
	static const BandSettings FULLBAND_SETTINGS;
	static const BandSettings WIDEBAND_SETTINGS;

	// Types
	enum EInput { RF1, RF2, TERM, CAL, INT_NOISE, EXT_NOISE };

	struct SCardStatus
	{
		unsigned short id : 3;
		unsigned short cardType : 3;
		unsigned short cardClass : 2;
		unsigned short : 5;
		unsigned short version : 3;
	};

	// Functions
	C26212001(void);
	~C26212001(void) {}
	unsigned short GetControlReg1(void) const { return Peek(CONTROL_REG_1); }
	unsigned short GetControlReg2(void) const { return Peek(CONTROL_REG_2); }
	unsigned char GetVersion(void) const;
	bool IsPresent(void) const { return m_present; }
	unsigned char GetAtten(unsigned char ch) const { return m_atten[ch]; }
	bool GetBypass(unsigned char ch) const;
	SCardStatus GetCardStatus(bool force = false) const;
	float GetGainRatio(unsigned char ch) const;
	bool IsMuxHf(unsigned char ch) const;
	unsigned char SetAtten(unsigned char atten, signed char chOrBoth = -1);
	void SetAttenRaw(unsigned char bits, signed char chOrBoth = -1);
	void SetBand(unsigned char band, signed char chOrBoth = -1);
	void SetBypass(bool bypass, signed char chOrBoth = -1);
	void SetFullband(unsigned char band, signed char chOrBoth = -1);
	void SetInput(EInput input, signed char chOrBoth = -1);
	void SetMux(bool hf, signed char chOrBoth = -1);
	void SetTestMode(signed char chOrBoth = -1);
	void SetWideband(unsigned char band, signed char chOrBoth = -1);
	void Tune(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ bool& bypass, _Out_ Units::FreqPair& freqLimits,
		_Out_ size_t& band, signed char chOrBoth = -1);
	void TuneFullband(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ bool& bypass, _Out_ Units::FreqPair& freqLimits,
		_Out_ size_t& band, signed char chOrBoth = -1);
	void TuneNarrowband(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ bool& bypass, _Out_ Units::FreqPair& freqLimits,
		_Out_ size_t& band, signed char chOrBoth = -1);
	void TuneWideband(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ bool& bypass, _Out_ Units::FreqPair& freqLimits,
		_Out_ size_t& band, signed char chOrBoth = -1);

private:
	// Constants
	static const SBandSettings BAND_SETTINGS_[];
	static const unsigned int CARD_CLASS = 0xe;
	static const unsigned int CARD_TYPE = 0x6;
	static const SBandSettings FULLBAND_SETTINGS_[];
	static const unsigned int VME_BASE_ADDR = (0x770000 | (CARD_CLASS << 12) | (CARD_TYPE << 8));
	static const unsigned int STATUS_REG = 0x00;
	static const unsigned int CONTROL_REG_1 = 0x02;
	static const unsigned int CONTROL_REG_2 = 0x04;
	static LPCTSTR MUTEX_NAME;
	static const SBandSettings WIDEBAND_SETTINGS_[];

	// Functions
	unsigned short Peek(unsigned char reg) const { return CVme::Peek(VME_BASE_ADDR + reg); }
	void Poke(unsigned char reg, unsigned short data) { CVme::Poke(VME_BASE_ADDR + reg, data); }
	virtual void SetSettleTime(unsigned int) {}

	// Data
	unsigned char m_atten[2];
	bool m_first;
	mutable CMutex m_mutex;
	bool m_present;
};

// Use selectany to put these here rather than in the .cpp so the size of the array can be determined by users of the class
__declspec(selectany) const CDownConverter::SBandSettings C26212001::BAND_SETTINGS_[] =
{ // low 20 dB, low 3 dB, high 3 dB, high 20 dB, data
	{        0,     9000,  2000000,  2000000, 0x0060 },
	{  1800000,  1900000,  4000000,  4800000, 0x0000 },
	{  2750000,  3250000,  6000000,  7000000, 0x0004 },
	{  4300000,  5250000,  9000000, 10000000, 0x000c },
	{  6750000,  7750000, 11250000, 12000000, 0x0008 },
	{  9250000, 10750000, 15250000, 18000000, 0x0010 },
	{ 13000000, 14750000, 20250000, 22000000, 0x0014 },
	{ 16000000, 19250000, 25750000, 29000000, 0x001c },
	{ 22000000, 24750000, 30500000, 31500000, 0x0018 }
};

__declspec(selectany) const CDownConverter::SBandSettings C26212001::FULLBAND_SETTINGS_[] =
{ // low 20 dB, low 3 dB, high 3 dB, high 20 dB, data
	{        0,     9000,  2000000,  2000000, 0x0060 },
	{  1500000,  1900000, 30500000, 35000000, 0x0038 }
};

__declspec(selectany) const CDownConverter::SBandSettings C26212001::WIDEBAND_SETTINGS_[] =
{ // low 20 dB, low 3 dB, high 3 dB, high 20 dB, data
	{        0,     9000,  2000000,  2000000, 0x0060 },
	{  1450000,  1900000, 15000000, 20000000, 0x0030 },
	{ 80000000,  9500000, 22000000, 28000000, 0x0034 },
	{ 18000000, 20000000, 30500000, 32500000, 0x003c }
};

