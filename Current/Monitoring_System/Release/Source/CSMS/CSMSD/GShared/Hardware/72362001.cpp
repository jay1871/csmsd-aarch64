/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2008 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#include "StdAfx.h"

#include "72362001.h"
#include "Units.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
const Units::Frequency C72362001::CAL_BASE_FREQ = Units::Frequency(40455000) / 64; // Hz

const C72362001::SCalSettings C72362001::CAL_SETTINGS[] = 
{
	{ CAL_BASE_FREQ * 41 / 8, 0x18, 0x00 },
	{ CAL_BASE_FREQ * 42 / 8, 0x18, 0x04 },
	{ CAL_BASE_FREQ * 43 / 8, 0x18, 0x08 },
	{ CAL_BASE_FREQ * 44 / 8, 0x18, 0x0c },
	{ CAL_BASE_FREQ * 45 / 8, 0x18, 0x10 },
	{ CAL_BASE_FREQ * 46 / 8, 0x18, 0x14 },
	{ CAL_BASE_FREQ * 47 / 8, 0x18, 0x18 },
	{ CAL_BASE_FREQ * 48 / 8, 0x18, 0x1c },
	{ CAL_BASE_FREQ * 41 / 4, 0x10, 0x00 },
	{ CAL_BASE_FREQ * 42 / 4, 0x10, 0x04 },
	{ CAL_BASE_FREQ * 43 / 4, 0x10, 0x08 },
	{ CAL_BASE_FREQ * 44 / 4, 0x10, 0x0c },
	{ CAL_BASE_FREQ * 45 / 4, 0x10, 0x10 },
	{ CAL_BASE_FREQ * 46 / 4, 0x10, 0x14 },
	{ CAL_BASE_FREQ * 47 / 4, 0x10, 0x18 },
	{ CAL_BASE_FREQ * 48 / 4, 0x10, 0x1c },
	{ CAL_BASE_FREQ * 41 / 2, 0x08, 0x00 },
	{ CAL_BASE_FREQ * 42 / 2, 0x08, 0x04 },
	{ CAL_BASE_FREQ * 43 / 2, 0x08, 0x08 },
	{ CAL_BASE_FREQ * 44 / 2, 0x08, 0x0c },
	{ CAL_BASE_FREQ * 45 / 2, 0x08, 0x10 },
	{ CAL_BASE_FREQ * 46 / 2, 0x08, 0x14 },
	{ CAL_BASE_FREQ * 47 / 2, 0x08, 0x18 },
	{ CAL_BASE_FREQ * 48 / 2, 0x08, 0x1c },
	{ CAL_BASE_FREQ * 41,     0x00, 0x00 },
	{ CAL_BASE_FREQ * 42,     0x00, 0x04 },
	{ CAL_BASE_FREQ * 43,     0x00, 0x08 },
	{ CAL_BASE_FREQ * 44,     0x00, 0x0c },
	{ CAL_BASE_FREQ * 45,     0x00, 0x10 },
	{ CAL_BASE_FREQ * 46,     0x00, 0x14 },
	{ CAL_BASE_FREQ * 47,     0x00, 0x18 }
};


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C72362001::C72362001(void)
:
	C7236200276342017(HF)
{
	SetSwitch(0, RECEIVE, TERM, TERM);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C72362001::~C72362001(void)
{
	SetSwitch(0, RECEIVE, TERM, TERM);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get a cal frequency
//
Units::Frequency C72362001::GetCalFreq(size_t index)
{
	return index < _countof(CAL_SETTINGS) ? CAL_SETTINGS[index].calFreq : 0;
}


//////////////////////////////////////////////////////////////////////
//
// Get the closest cal settings
//
_Ret_ const C72362001::SCalSettings& C72362001::GetCalSettings(Units::Frequency calFreq)
{
	for(size_t i = 0; i < _countof(CAL_SETTINGS) - 1; ++i)
	{
		if(calFreq.Hz<double>() < sqrt(CAL_SETTINGS[i].calFreq.Hz<double>() * CAL_SETTINGS[i + 1].calFreq.Hz<double>()))
		{
			return CAL_SETTINGS[i];
		}
	}

	return CAL_SETTINGS[_countof(CAL_SETTINGS) - 1];
}


//////////////////////////////////////////////////////////////////////
//
// Get the control bytes
//
void C72362001::GetControlBytes(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf,
					 _Out_writes_all_(3) unsigned char (&controlBytes)[3])
{
	// Set address bits and default state (mode = RECEIVE, ref = REF, sample = TERM)
	const SCalSettings& calSettings = GetCalSettings(freq);
	controlBytes[0] = 0x00;
	controlBytes[1] = calSettings.control1;
	controlBytes[2] = calSettings.control2;

	switch(mode)
	{
	case RECEIVE:
		switch(sampleRf)
		{
		case ANT_1:
		case ANT_2:
		case ANT_3:
		case ANT_4:
		case ANT_5:
		case ANT_6:
		case ANT_7:
		case ANT_8:
		case AUX_1:
		case AUX_2:
			ASSERT(refRf != BITE && refRf != NOISE);
			controlBytes[0] |= sampleRf;
			break;

		case TERM:
			controlBytes[0] |= 0x0b; // CH-sel terminate
			break;

		case NOISE:
			ASSERT(refRf != BITE);
			controlBytes[0] |= 0x0f; // CH-sel calibrator
			controlBytes[1] |= 0x03; // Noise-Gen & Noise-SW
			break;

		case BITE:
			ASSERT(refRf != NOISE);
			controlBytes[0] |= 0x0f; // CH-sel calibrator
			controlBytes[1] |= 0x04; // Cal-Enable
			break;

		default:
			ASSERT(FALSE);
			controlBytes[0] |= 0x2f; // Terminate ref and sample
			break;
		}

		switch(refRf)
		{
		case TERM:
			controlBytes[0] |= 0x20; // Ref-Term
			break;

		case MONITOR:
			if(freq < CROSSOVER_FREQ_HZ)
			{
				controlBytes[2] |= 0x02; // LB-bypass
			}
			else
			{
				controlBytes[0] |= 0x20; // Ref-Term
				controlBytes[2] |= 0x01; // Ref-Source
			}

			break;

		case REF:
			break;

		case BITE:
			controlBytes[0] |= 0x30; // Cal-to-Ref & Ref-Term
			controlBytes[1] |= 0x04; // Cal-Enable
			break;

		case NOISE:
			controlBytes[0] |= 0x30; // Cal-to-Ref & Ref-Term
			controlBytes[1] |= 0x03; // Noise-Gen & Noise-SW
			break;

		default:
			ASSERT(FALSE);
			controlBytes[0] |= 0x20; // Ref-Term
			break;
		}

		break;

	case TX_BITE:
		{
			controlBytes[1] |= 0x24; // Cal-to-Ant, Cal-Enable

			switch(sampleRf)
			{
			case TERM:
				controlBytes[0] |= 0x0b; // Terminate
				break;

			case ANT_1:
			case ANT_2:
			case ANT_3:
			case ANT_4:
			case ANT_5:
			case ANT_6:
			case ANT_7:
			case ANT_8:
			case AUX_1:
			case AUX_2:
				controlBytes[0] |= sampleRf; // ANT_n
				break;

			case NOISE:
				controlBytes[0] |= 0x0f; // CH-sel calibrator
				controlBytes[1] |= 0x03; // Noise-Gen & Noise-SW
				break;

			default:
				ASSERT(FALSE);
				controlBytes[0] |= 0x0b; // Terminate
				break;
			}

			switch(refRf)
			{
			case MONITOR:
				controlBytes[2] |= 0x01; // Ref-Source
				break;

			case BITE:
				// Get this by leakage through the switches
				break;

			case NOISE:
				controlBytes[0] |= 0x10; // Cal-to-Ref
				controlBytes[1] |= 0x03; // Noise-Gen & Noise-SW
				break;

			default:
				ASSERT(FALSE);
				break;
			}
		}

		break;

	default:
		ASSERT(FALSE);
		controlBytes[0] |= 0x20; // Ref-Term
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the settle time for a switch change
//
unsigned int C72362001::GetSettleTime(_In_reads_(3) const unsigned char(&controlBytes)[3]) const
{
	CSingleLock lock(&m_critSect, TRUE);

	if(memcmp(controlBytes, const_cast<const unsigned char*>(m_controlBytes), sizeof(controlBytes)) == 0)
	{
		return 0;
	}
	else if((controlBytes[1] & 0x04) != 0 && ((controlBytes[1] ^ m_controlBytes[1]) & 0x04) != 0)
	{
		// Turning on cal
		return 50000;
	}
	else if((controlBytes[1] & 0x02) != 0 && ((controlBytes[1] ^ m_controlBytes[1]) & 0x02) != 0)
	{
		// Turning on noise
		return 5000;
	}
	else if((controlBytes[1] & 0x04) != 0 && ((controlBytes[2] ^ m_controlBytes[2]) & 0x1c) != 0)
	{
		// Cal PLL change
		return 5000;
	}
	else
	{
		return 5;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Set the switch
//
void C72362001::SetSwitch(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf)
{
	unsigned char controlBytes[3];
	GetControlBytes(freq, mode, refRf, sampleRf, controlBytes);
	SetSettleTime(GetSettleTime(controlBytes));
	C7236200276342017::SetSwitch(controlBytes, false);

	return;
}

