/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "DownConverter.h"
#include "Units.h"
#include "Vme.h"

class C80842081
:
	// Inheritance
	public CDownConverter,
	protected virtual CVme
{
public:
	// Constants
	static const unsigned char ATTEN_STEP = 2; // dB
	static const unsigned long CAL_HIGH_FREQ_HZ = 30000000; // Hz
	static const unsigned long CAL_LOW_FREQ_HZ = 2000000; // Hz
	static const unsigned char MAX_ATTEN = 78; // dB
	static const signed char MAX_GAIN = 72; // dB
	static const unsigned char MIN_CONGESTED_ATTEN = 12; // dB
	static const unsigned long NARROW_BW_HZ = 500000; // Hz
	static const unsigned long RF_HIGH_FREQ_HZ = 30000000; // Hz
	static const unsigned long RF_LOW_FREQ_HZ = 1500000; // Hz
	static const unsigned long WIDE_BW_HZ = 2000000; // Hz

	// Types
	enum ETemperature { COLD = 0, COOL = 1, WARM = 3, HOT = 7, OVERTEMP = 15 };

	// Functions
	static Units::Frequency GetCalFreq(Units::Frequency freq);

protected:
	// Constants
	static const SBandSettings BAND_SETTINGS_[];

	// Types
	struct SCardStatus
	{
		unsigned short id : 3;
		unsigned short cardType : 3;
		unsigned short cardClass : 2;
		unsigned short temp : 4;
		unsigned short detector : 1;
		unsigned short version : 3;
	};

	// Functions
	C80842081(void);
	template<size_t N> C80842081(const CDownConverter::EGainMode (&gainModes)[N]);
	virtual ~C80842081(void);
	unsigned char GetAtten(unsigned char id) const { return m_atten[id]; }
	unsigned short GetAttenRaw(unsigned char id) const { return Peek(id, ATTEN_REG); }
	static size_t GetBand(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ Units::FreqPair& freqLimits)
		{ return CDownConverter::GetBand(freq, rxBw, bandSelect, freqLimits, BAND_SETTINGS); }
	SCardStatus GetCardStatus(unsigned char id, bool force = false) const;
	bool GetDetector(unsigned char id) const { return GetCardStatus(id).detector; }
	float GetGainRatio(unsigned char id) const;
	float GetGainAdjustmentDb(void) const;
	unsigned short GetModeReg(unsigned char id) const { return Peek(id, MODE_REG); }
	unsigned char GetVersion(unsigned char id) const;
	bool IsPresent(unsigned char id) const { return m_present[id]; }
	unsigned char MinAtten(CDownConverter::EGainMode mode) const { return mode == CDownConverter::CONGESTED ? MIN_CONGESTED_ATTEN : 0; }
	unsigned char MinAtten(Units::Frequency freq) const { return MinAtten(GetGainMode(freq, BAND_SETTINGS)); }
	unsigned char SetAtten(unsigned char atten, Units::Frequency freq, signed char idOrAll = -1);
	unsigned char SetAtten(unsigned char atten, CDownConverter::EGainMode mode, signed char idOrAll = -1);
	void SetAttenRaw(unsigned short bits, signed char idOrAll = -1);
	void SetBand(unsigned char band, signed char idOrAll = -1);
	void SetBiteComb(bool on, signed char idOrAll = -1);
	void SetBlanking(bool blank, signed char idOrAll = -1);
	void SetBypass(bool bypass, signed char idOrAll = -1);
	void SetCalRelay(bool on, signed char idOrAll = -1);
	void SetDetector(bool on, signed char idOrAll = -1);
	void SetWideBw(bool wide, signed char idOrAll = -1);
	void Tune(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ bool& bypass, _Out_ Units::FreqPair& freqLimits,
		_Out_ size_t& band, signed char idOrAll = -1);

private:
	// Constants
	static const BandSettings BAND_SETTINGS;
	static const unsigned int CARD_CLASS = 0xc;
	static const unsigned int CARD_TYPE = 0x3;
	static const unsigned int MAX_INSTANCES = 2;
	static const unsigned int VME_BASE_ADDR = (0x770000 | (CARD_CLASS << 12) | (CARD_TYPE << 8));
	static const unsigned int STATUS_REG = 0x00;
	static const unsigned int ATTEN_REG = 0x02;
	static const unsigned int MODE_REG = 0x04;
	static LPCTSTR MUTEX_NAME;

	// Functions
	static Units::Frequency GetCalFreq(Units::Frequency freq, Units::Frequency loFreq, Units::Frequency hiFreq);
	_Ret_ virtual const CDownConverter::SReceiverGainModes& GetGainModes(void) const { return m_gainModes; }
	unsigned short Peek(unsigned char id, unsigned char reg) const { return CVme::Peek(VME_BASE_ADDR + 32 * id + reg); }
	void Poke(unsigned char id, unsigned char reg, unsigned short data) { CVme::Poke(VME_BASE_ADDR + 32 * id + reg, data); }
	virtual void SetSettleTime(unsigned int) {}

	// Data
	unsigned char m_atten[MAX_INSTANCES];
	bool m_first;
	CDownConverter::SReceiverGainModes m_gainModes;
	mutable CMutex m_mutex;
	bool m_present[MAX_INSTANCES];
};

// Use selectany to put this here rather than in the .cpp so the size of the array can be determined by users of the class
__declspec(selectany) const CDownConverter::SBandSettings C80842081::BAND_SETTINGS_[] =
{ // low 20 dB, low 1 dB, high 1 dB, high 20 dB, data
	{        0,     1000,  2300000,  3000000, 0x0100 },
	{  1200000,  1500000,  3900000,  4800000, 0x0000 },
	{  2300000,  2800000,  6000000,  7200000, 0x0004 },
	{  3900000,  4400000,  8100000,  9200000, 0x0008 },
	{  5800000,  6500000, 10300000, 11800000, 0x000c },
	{  7000000,  8000000, 14500000, 16500000, 0x0010 },
	{ 10400000, 12000000, 18500000, 20700000, 0x0014 },
	{ 13500000, 15000000, 25000000, 27600000, 0x0018 },
	{ 17300000, 21500000, 30012500, 35000000, 0x001c }
};


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
template<size_t N> C80842081::C80842081(const CDownConverter::EGainMode (&gainModes)[N])
:
	CVme(),
	m_gainModes(&gainModes[0], &gainModes[_countof(BAND_SETTINGS_)]),
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get())
{
	static_assert(N >= _countof(BAND_SETTINGS_), "Not enough values in gainModes array");
	m_first = !(GetLastError() == ERROR_ALREADY_EXISTS); // Status from m_mutex construction

	for(unsigned char id = 0; id < MAX_INSTANCES; ++id)
	{
		SCardStatus status = GetCardStatus(id, true);
		m_present[id] = (status.cardClass == (CARD_CLASS & 0x3) && status.cardType == CARD_TYPE && status.id == id);

		if(m_present[id])
		{
			if(m_first)
			{
				SetAtten(MAX_ATTEN, 30000000, id);
				SetBlanking(true, id);
				SetBypass(false, id);
				SetBiteComb(false, id);
				SetCalRelay(false, id);
				SetDetector(false, id);
				bool bypass;
				Units::FreqPair freqLimits;
				size_t band;
				Tune(30000000, NARROW_BW_HZ, OPTIMUM, bypass, freqLimits, band, id);
			}
		}
	}

	return;
}


