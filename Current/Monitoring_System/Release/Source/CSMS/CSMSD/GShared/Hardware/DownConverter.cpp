/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2007 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#include "StdAfx.h"

#include <algorithm>

#include "DownConverter.h"
#include "Units.h"
#include "Utility.h"


/////////////////////////////////////////////////////////////////////////////
//
// Find the preselector needed
//
size_t CDownConverter::GetBand(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ Units::FreqPair& freqLimits,
							   _In_ const BandSettings& bandSettings)
{
	switch(bandSelect)
	{
	case LOWER:
		for(size_t band = 0; band < bandSettings.size() - 1; ++band)
		{
			ASSERT(bandSettings[band].low1dbFreq < bandSettings[band + 1].low1dbFreq &&
				bandSettings[band].high1dbFreq < bandSettings[band + 1].high1dbFreq);

			if(freq - rxBw / 2 < bandSettings[band + 1].low1dbFreq)
			{
				freqLimits = Units::FreqPair(std::max(freq - rxBw / 2, bandSettings[band].low1dbFreq),
					freq + rxBw / 2 > bandSettings[band].high1dbFreq ?
					(bandSettings[band + 1].low1dbFreq + bandSettings[band].high1dbFreq) / 2 : bandSettings[band].high1dbFreq);
				return band;
			}
		}

		freqLimits = Units::FreqPair(
			freq - rxBw / 2,
			std::min(freq + rxBw / 2, bandSettings.back().high1dbFreq));
		return bandSettings.size() - 1;

	case UPPER:
		for(size_t band = bandSettings.size() - 1; band > 0; --band)
		{
			ASSERT(bandSettings[band - 1].low1dbFreq < bandSettings[band].low1dbFreq &&
				bandSettings[band - 1].high1dbFreq < bandSettings[band].high1dbFreq);

			if(freq + rxBw / 2 > bandSettings[band - 1].high1dbFreq)
			{
				freqLimits = Units::FreqPair(
					freq - rxBw / 2 < bandSettings[band].low1dbFreq ?
					(bandSettings[band - 1].high1dbFreq + bandSettings[band].low1dbFreq) / 2 : bandSettings[band].low1dbFreq,
					std::min(freq + rxBw / 2, bandSettings[band].high1dbFreq));
				return band;
			}
		}

		freqLimits = Units::FreqPair(
			std::max(freq - rxBw / 2, bandSettings[0].low1dbFreq),
			freq + rxBw / 2);
		return 0;

	case OPTIMUM:
		for(size_t band = 0; band < bandSettings.size() - 1; ++band)
		{
			ASSERT(bandSettings[band].low1dbFreq < bandSettings[band + 1].low1dbFreq &&
				bandSettings[band].high1dbFreq < bandSettings[band + 1].high1dbFreq);

			if(freq < (bandSettings[band].high1dbFreq + bandSettings[band + 1].low1dbFreq) / 2)
			{
				freqLimits = Units::FreqPair(
					std::max(freq - rxBw / 2, bandSettings[band].low1dbFreq),
					std::min(freq + rxBw / 2, bandSettings[band].high1dbFreq));
				return band;
			}
		}

		freqLimits = Units::FreqPair(
			std::max(freq - rxBw / 2, bandSettings.back().low1dbFreq),
			std::min(freq + rxBw / 2, bandSettings.back().high1dbFreq));
		return bandSettings.size() - 1;

	default:
		THROW_LOGIC_ERROR();
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the gain mode
//
CDownConverter::EGainMode CDownConverter::GetGainMode(Units::Frequency freq, _In_ const BandSettings& bandSettings) const
{
	const SReceiverGainModes& gainModes = GetGainModes();
	EGainMode mode = RURAL;

	for(size_t band = 0; band < std::min(bandSettings.size(), gainModes.size()); ++band)
	{
		if(freq >= bandSettings[band].low20dbFreq && freq <= bandSettings[band].high20dbFreq)
		{
			mode = std::max(mode, gainModes[band]);
		}

		if(freq < bandSettings[band].low1dbFreq)
		{
			break;
		}
	}

	return mode;
}
