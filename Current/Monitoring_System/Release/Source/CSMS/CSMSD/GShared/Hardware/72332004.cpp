/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#include "StdAfx.h"

#include "72332004.h"
#include "Units.h"
#include "Utility.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
//
// Static data
//
const C72332004::ESource C72332004::GRAY_CODE[2][NUM_ANTS] =
{
	{ ANT_8, ANT_9, ANT_1, ANT_3, ANT_2, ANT_6, ANT_7, ANT_5, ANT_4 },
	{ ANT_8H, ANT_9H, ANT_1H, ANT_3H, ANT_2H, ANT_6H, ANT_7H, ANT_5H, ANT_4H }
};

const float C72332004::MONITOR_SPLITTER_LOSS_DBM = 4.0f;


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C72332004::C72332004(void)
:
	m_controlByte(0xff),
	m_digitizer(),
	m_validateFailCount(0)
{
	SetSwitch(98500000, RECEIVE, TERM, TERM);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C72332004::~C72332004(void)
{
	SetSwitch(98500000, RECEIVE, TERM, TERM);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the control byte
//
unsigned char C72332004::GetControlByte(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf)
{
	ASSERT(refRf != ANT_1 && refRf != ANT_2 && refRf != ANT_3 && refRf != ANT_4 && refRf != ANT_5 &&
		refRf != ANT_6 && refRf != ANT_7 && refRf != ANT_8 && refRf != ANT_9 && refRf != AUX &&
		refRf != ANT_1H && refRf != ANT_2H && refRf != ANT_3H && refRf != ANT_4H && refRf != ANT_5H &&
		refRf != ANT_6H && refRf != ANT_7H && refRf != ANT_8H && refRf != ANT_9H && refRf != AUX_H);
	unsigned char controlByte;

	switch(mode)
	{
	case RECEIVE:
		switch(sampleRf)
		{
		case VHF:
		case NOISE:
			ASSERT(sampleRf == refRf);
			controlByte = unsigned char(sampleRf);
			break;

		case ANT_1:
		case ANT_2:
		case ANT_3:
		case ANT_4:
		case ANT_5:
		case ANT_6:
		case ANT_7:
		case ANT_8:
		case ANT_9:
			ASSERT(refRf == REF);
			controlByte = unsigned char(sampleRf);
			break;

		case ANT_1H:
		case ANT_2H:
		case ANT_3H:
		case ANT_4H:
		case ANT_5H:
		case ANT_6H:
		case ANT_7H:
		case ANT_8H:
		case ANT_9H:
			ASSERT(refRf == REF_HORIZON);
			controlByte = unsigned char(sampleRf);
			break;

		case AUX:
		case AUX_H:
			ASSERT(refRf != TERM && refRf != BITE && refRf != EXT);
			controlByte = unsigned char(sampleRf);
			break;

		case BITE:
			switch(refRf)
			{
			case TERM:
				controlByte = 0x31;
				break;

			case BITE:
				controlByte = 0x51;
				break;

			default:
				ASSERT(FALSE);
				controlByte = 0x11;
				break;
			}

			break;

		case EXT:
			switch(refRf)
			{
			case TERM:
				controlByte = 0x71;
				break;

			case EXT:
				controlByte = 0x81;
				break;

			default:
				ASSERT(FALSE);
				controlByte = 0x11;
				break;
			}

			break;

		case TERM:
			switch(refRf)
			{
			case REF:
				controlByte = C_REF_VERTICAL_ON_SAMPLE_TERM;
				break;

			case REF_HORIZON:
				controlByte = C_REF_HORIZONTAL_ON_SAMPLE_TERM;
				break;

			case BITE:
				controlByte = N_REF_BITE_SAMPLE_TERM | N_SAMPLE_VERT_ANT_OFF;
				break;

			case EXT:
				controlByte = N_REF_EXT_SAMPLE_TERM | N_SAMPLE_VERT_ANT_OFF;
				break;

			case TERM:
				controlByte = N_REF_TERM_SAMPLE_TERM | N_SAMPLE_VERT_ANT_OFF;
				break;

			default:
				ASSERT(FALSE);
				controlByte = N_REF_TERM_SAMPLE_TERM | N_SAMPLE_VERT_ANT_OFF;
				break;
			}

			break;

		default:
			ASSERT(FALSE);
			controlByte = 0x11;
			break;
		}

		break;

	case TX_BITE:
		switch(refRf)
		{
		case BITE:
			controlByte = 0x02;
			break;

		case TERM:
			controlByte = C_BITE_TX_REFOUT_TERMINATED;
			break;

		default:
			ASSERT(FALSE);
			controlByte = C_BITE_TX_REFOUT_TERMINATED;
			break;
		}

		switch(sampleRf)
		{
		case ANT_1H:
		case ANT_2H:
		case ANT_3H:
		case ANT_4H:
		case ANT_5H:
		case ANT_6H:
		case ANT_7H:
		case ANT_8H:
		case ANT_9H:
		case AUX_H:
			ASSERT( controlByte == C_BITE_TX_REFOUT_TERMINATED );

			//High band BITE tx not supported.
			ASSERT( freq <= CROSSOVER_FREQ_HZ );

			//Horizontal only supports BITE tx on Ref Antenna and REF output
			// terminated.
			controlByte = C_BITE_TX_REFOUT_TERMINATED;
			//Fall through.
		case VHF:
		case ANT_1:
		case ANT_2:
		case ANT_3:
		case ANT_4:
		case ANT_5:
		case ANT_6:
		case ANT_7:
		case ANT_8:
		case ANT_9:
		case AUX:
			controlByte ^= unsigned char(sampleRf);
			break;

		default:
			ASSERT(FALSE);
			controlByte = 0x10;
			break;
		}

		break;

	default:
		ASSERT(FALSE);
		controlByte = 0x11;
		break;
	}

	if(freq > CROSSOVER_FREQ_HZ)
	{
		controlByte |= B_HIGH_BAND;
	}

	return controlByte;
}


//////////////////////////////////////////////////////////////////////
//
// Get the settle time for a switch change
//
unsigned int C72332004::GetSettleTime(unsigned char controlByte) const
{
	CSingleLock lock(&m_critSect, TRUE);

	if(controlByte == m_controlByte)
	{
		return 0;
	}
	else if((controlByte & 0xf7) == 0x21 || (controlByte & 0xf7) == 0x26)
	{
		// Noise source on
		return 5000;
	}
	else if((controlByte & 0xf7) == 0x31 || (controlByte & 0xf7) == 0x41 || (controlByte & 0xf7) == 0x51 ||
		(controlByte & 0x07) == 0x02 || (controlByte & 0x07) == 0x04 ||
		(controlByte & 0xf7) == 0x36 || (controlByte & 0xf7) == 0x46 ||
		(controlByte & 0xf7) == 0x56 || (controlByte & 0x07) == 0x05 || (controlByte & 0x07) == 0x03)
	{
		// Cal source on
		return 80000;
	}
	else if((controlByte ^ m_controlByte) & 0x08)
	{
		// Band change
		return 50;
	}
	else
	{
		return 5; // Default 5 us
	}
}


//////////////////////////////////////////////////////////////////////
//
// Set the switch by programming the LVDS bus
//
void C72332004::SetSwitch(unsigned char controlByte, bool force)
{
	CSingleLock lock(&m_critSect, TRUE);

	if(force || controlByte != m_controlByte)
	{
		m_controlByte = controlByte;
		m_digitizer->SetDataOut(0x0000 | controlByte);
		m_digitizer->SetDataOut(0x0100 | controlByte);
		m_digitizer->SetDataOut(0x0000 | controlByte);

        //Keeps the band setting.
		m_digitizer->SetDataOut(static_cast<unsigned short>(controlByte & B_HIGH_BAND));
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the switch
//
void C72332004::SetSwitch(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf)
{
	unsigned char controlByte = GetControlByte(freq, mode, refRf, sampleRf);
	SetSettleTime(GetSettleTime(controlByte));
	SetSwitch(controlByte, false);

	return;
}


//////////////////////////////////////////////////////////////////////
/// <summary>
/// Validate loopback data against local state.
/// </summary>
/// <param name="loopback">
/// loopback value to be checked.
/// </param>
/// <remarks>
/// @note: This method is never called by 5185 system, therefore the call
///        to SetSwitch can be done without going through the Digitizer.
/// </remarks>
void C72332004::ValidateLoopback(unsigned char loopback)
{
	CSingleLock lock(&m_critSect, TRUE);

	if(m_controlByte != loopback && ++m_validateFailCount == 10)
	{
		// Force switch control
		TRACE(_T("72332004 control reset\n"));
		C80682017::WriteShMemRegs();
		SetSwitch(m_controlByte, true);
		m_validateFailCount = 0;
	}
	else
	{
		m_validateFailCount = 0;
	}

	return;
}
