/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "StdAfx.h"

#include <memory>

#include "80842082.h"
#include "Units.h"
#include "Vme.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
//
// Static data
//
LPCTSTR C80842082::MUTEX_NAME = _T("Global\\TCI.{F8DFA5BD-135D-4675-95C5-AE12B27B9547}");


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C80842082::C80842082(void)
:
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get())
{
	m_first = !(GetLastError() == ERROR_ALREADY_EXISTS); // Status from m_mutex construction
	SCardStatus status = GetCardStatus(true);
	m_present = (status.cardClass == (CARD_CLASS & 0x3) && status.cardType == CARD_TYPE && status.id == 0);

	if(m_present)
	{
		CSingleLock lock(&m_mutex, TRUE);

		if(m_first)
		{
				SetBiteGen(false);
				Tune(30000000, 50000);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C80842082::~C80842082(void)
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the card status
//
C80842082::SCardStatus C80842082::GetCardStatus(bool force) const
{
	unsigned short reg = 0;

	if(force || m_present)
	{
		reg = Peek(STATUS_REG);
	}

	union
	{
		unsigned short reg;
		SCardStatus status;
	} status = { reg };

	return status.status;
}


//////////////////////////////////////////////////////////////////////
//
// Get the IF offset
//
Units::Frequency C80842082::GetIfOffset(Units::Frequency freq, bool step250kHz)
{
	Units::Frequency step = (step250kHz ? C80842082::TUNE_STEP_250 : C80842082::TUNE_STEP_200);
	Units::Frequency rounded = (freq + step / 2) - (freq + step / 2) % step;

	if(rounded < RF_LOW_FREQ_HZ)
	{
		return 0;
	}
	else
	{
		return rounded - freq;
	}
}

//////////////////////////////////////////////////////////////////////
//
// Get the board version
//
unsigned char C80842082::GetVersion(void) const
{
	return GetCardStatus().version;
}


//////////////////////////////////////////////////////////////////////
//
// Turn on the BITE generator
//
void C80842082::SetBiteGen(bool on)
{
	if(m_present)
	{
		unsigned short data = (on ? 0x0800 : 0x0000);
		unsigned short reg = Peek(CONTROL_REG);

		if((reg & 0x0800) != data)
		{
			reg &= 0x70ff;
			reg |= data;
			Poke(CONTROL_REG, reg);
			SetSettleTime(10000);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Tune Lo1
//
void C80842082::SetLo1(Units::Frequency freq, bool step250kHz)
{
	if(m_present)
	{
		unsigned short data = (step250kHz ?
			0x4000 | unsigned short((128000000 - freq.Hz<unsigned int>()) / TUNE_STEP_250) :
			0x3000 | unsigned short((102400000 - freq.Hz<unsigned int>()) / TUNE_STEP_200));
		unsigned short reg = Peek(CONTROL_REG);

		if((reg & 0x70ff) != data)
		{
			unsigned int settleTime = 3000;

			if(((reg ^ data) & 0x7000) || (reg & 0x00ff) > (data & 0x00ff) + 40 || (reg & 0x00ff) + 40 < (data & 0x00ff))
			{
				settleTime += 2000;
			}

			reg &= 0x0800;
			reg |= data;
			Poke(CONTROL_REG, reg);
			SetSettleTime(settleTime);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Tune the synthesizer. Returns the IF frequency
//
Units::Frequency C80842082::Tune(Units::Frequency freq, Units::Frequency rxBw, bool step250kHz)
{
	Units::Frequency rounded = freq;

	if(m_present)
	{
		Units::Frequency step = (step250kHz ? C80842082::TUNE_STEP_250 : C80842082::TUNE_STEP_200);
		rounded = (freq + step / 2) - (freq + step / 2) % step;

		if(rounded < RF_LOW_FREQ_HZ + rxBw / 2)
		{
			rounded = RF_LOW_FREQ_HZ + rxBw / 2;
		}

		SetLo1(rounded + FIRST_IF_FREQ_HZ, step250kHz);
	}

	return FINAL_IF_FREQ_HZ + rounded - freq;
}
