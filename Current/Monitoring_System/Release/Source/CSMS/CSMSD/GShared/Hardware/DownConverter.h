/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2007 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#pragma once

#include <vector>

#include "Units.h"

class CDownConverter
{
protected:
	// Types
	struct SBandSettings
	{
		Units::Frequency low20dbFreq;
		Units::Frequency low1dbFreq;
		Units::Frequency high1dbFreq;
		Units::Frequency high20dbFreq;
		unsigned short data;
	};

public:
	// Types
	enum EBandSelect { LOWER, UPPER, OPTIMUM };
	enum EGainMode { RURAL = 0, URBAN = 1, CONGESTED = 2 };
	typedef std::vector<SBandSettings> BandSettings;
	typedef std::vector<EGainMode> SReceiverGainModes;

	// Functions
	static size_t GetBand(Units::Frequency freq, Units::Frequency rxBw, EBandSelect bandSelect, _Out_ Units::FreqPair& freqLimits,
		_In_ const BandSettings& bandSettings);

protected:
	// Functions
	virtual ~CDownConverter(void) {}
	EGainMode GetGainMode(Units::Frequency freq, _In_ const BandSettings& bandSettings) const;

private:
	// Functions
	_Ret_ virtual const SReceiverGainModes& GetGainModes(void) const { static SReceiverGainModes noGainModes; return noGainModes; }
};
