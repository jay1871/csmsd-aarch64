/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2009-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "80682017.h"
#include "Digitizer.h"
#include "Singleton.h"

class C7236200276342017
:
	// Inheritance
	protected virtual C80682017
{
protected:
	// Functions
	C7236200276342017(EPoePort poePort); // Can only be constructed by a derived class
	virtual ~C7236200276342017(void) {}
	void GetCurrentControlBytes(_Out_writes_all_(3) unsigned char(&controlBytes)[3]) const
		{ CSingleLock lock(&m_critSect, TRUE); memcpy(&controlBytes[0], const_cast<const unsigned char*>(&m_controlBytes[0]), 3); return; }
	void SetSwitch(_In_reads_(3) const unsigned char (&controlBytes)[3], bool force);
	void ValidateLoopback(unsigned int loopback);

	// Data
	volatile unsigned char m_controlBytes[3];
	mutable CCriticalSection m_critSect;

private:
	// Data
	CSingleton<CDigitizer> m_digitizer;
	unsigned short m_poePortBits;
	unsigned int m_validateFailCount;
};

