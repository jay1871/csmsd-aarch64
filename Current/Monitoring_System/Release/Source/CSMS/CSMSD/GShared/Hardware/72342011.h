/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2008-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "7236200276472017.h"
#include "DownConverter.h"
#include "EquipCtrlMsg.h"
#include "Units.h"

class C72342011
:
	// Inheritance
	protected C7236200276342017,
	public CDownConverter
{
public:
	// Constants
	static const unsigned long ANT_CROSSOVER_FREQ_HZ = 2500000000;
	static const unsigned long long MAX_FREQ_HZ = 8000000000;
	static const unsigned long long LOW_FREQ_HZ = 20000000; // Hz
	
	//Gordon/Trung verified that 3GHz Horizontal works in 643-3 and should
	// work for 647-D since it has the same Antenna.
	static const unsigned long long MAX_HORIZ_FREQ_HZ = 3000000000;
	static const float NOMINAL_TEMP; // �C
	static const unsigned char NUM_ANTS = 9;

	enum ESource // Some values match hardware - don't change!
	//ANT_1H to ANT_9 value matches with lower 6-bit Control Value.
	{
		TERM	= 0,
		REF		= -1,
		BITE	= -2,
		NOISE	= -3,
		REF_HORIZON = -4,
		ANT_1H	= 0x01,
		ANT_2H	= 0x02,
		ANT_3H	= 0x03,
		ANT_4H	= 0x04,
		ANT_5H	= 0x05,
		ANT_6H	= 0x06,
		ANT_7H	= 0x07,
		ANT_8H	= 0x08,
		ANT_9H	= 0x09,
		ANT_1	= 0x11,
		ANT_2	= 0x12,
		ANT_3	= 0x13,
		ANT_4	= 0x14,
		ANT_5	= 0x15,
		ANT_6	= 0x16,
		ANT_7	= 0x17,
		ANT_8	= 0x18,
		ANT_9	= 0x19,
	};
	enum EDfElemBandSel { AUTO, UHF, SHF };
	enum ERefMode { RECEIVE, TX_BITE };

	//============= Define lower 6-bit control constants (AC10 - AC15) ====
	//Cal tone & noise sample selection is 0x0f.
	static const unsigned char C_CAL_OR_NOISE_SAMPLE_SELECTION = 0x0f;
	static const unsigned char B_VERTICAL_POL = 0x10;
	static const unsigned char C_HORIZONTAL_VAL = 0;

	//Terminated sample selection is 0x00.
	static const unsigned char C_TERMINATED_SAMPLE_SELECTION = 0x00;
	//=============												===========
	
	//============= Define middle 6-bit control constants (AC20 - AC25) ===
	static const unsigned char B_SHF = 0x10;
	static const unsigned char C_NO_CONVERTER = 0;
	//=============												===========
	
	//============= Define upper 6-bit control constants (AC30 - AC35) ====
	static const unsigned char B_CAL_2_REF = 0x08;
	static const unsigned char B_CAL_2_ANTENNA = 0x10;

	static const unsigned char C_REFOUT_2_REF_ANTENNA = 0x00;
	static const unsigned char C_NOISE_ON = 0x03;
	static const unsigned char C_REFOUT_2_THERMAL_NOISE = 0x0B;
	
	//Terminated Reference selection is 0x0C where CAL Tone is turned-off
	// and Reference output is connected to the terminated CAL Tone off path.
	static const unsigned char C_TERMINATED_REF_SELECTION = 0x0C;
	//=============												===========
	
	// Constants
	static const unsigned char C_VERTICAL_IDX = 0;

	//Row Index of 5 element antenna connected to 9 inputs switch.
	//@note: It must always be the last row.
	static const unsigned char C_5_ELEMENT_IDX = 2;

	// @note: 3rd row is used for mapping, it is NOT A GRAY_CODE for programming 7234 switch
	//	1st row has the gray code for vertical polarized antenna.
	//	2nd row has the gray code for horizontal polarized antenna.
	//	3rd row of the GRAY_CODE has the mapping of 649-8 type 2 low-band
	//        5 element to a 9 inputs switch. TERM is used as input that is not
	//        used. Therefore 3rd should only be used during Processing, not
	//		  Hardware sampling.
	static const ESource GRAY_CODE[C_5_ELEMENT_IDX + 1][NUM_ANTS];

	// Functions
	static unsigned int CalcSettleTime(_In_reads_(3) const unsigned char(&currCtrlByte)[3],
                        _In_reads_(3) const unsigned char(&newCtrlByte)[3]);
	static Units::Frequency Get647DHorMetricLowerFreqLimit() { return( Units::Frequency(LOW_FREQ_HZ) ); }
	static Units::Frequency Get647DHorMetricUpperFreqLimit() { return( Units::Frequency(MAX_HORIZ_FREQ_HZ) ); }
	static Units::Frequency Get647DHorDFLowerFreqLimit() { return( Units::Frequency(LOW_FREQ_HZ) ); };
	static Units::Frequency Get647DHorDFUpperFreqLimit() { return( Units::Frequency(MAX_HORIZ_FREQ_HZ) ); }
	static float GetCalDbm(size_t index, size_t ch);
	static Units::Frequency GetCalFreq(Units::Frequency freq, bool useShf) { return GetCalSettings(freq, useShf).freq; }
	static Units::Frequency GetCalFreq(size_t index);
	static float GetCalHalfRangeDbm(size_t index);
	static float GetCalLevelAdjustmentDb(float temp) { return (temp - NOMINAL_TEMP) * CAL_LEVEL_SLOPE; }
	static float GetCalRefDbm(Units::Frequency freq, float temp)
		{ ASSERT(GetCalSettings(freq, true).freq == freq); return GetCalSettings(freq, true).refDbm + GetCalLevelAdjustmentDb(temp); }
	static void GetControlBytes(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf,
		_Out_writes_all_(3) unsigned char(&controlBytes)[3], EDfElemBandSel band = AUTO);
	static float GetConverterGainAdjustmentDb(float temp) { return (temp - NOMINAL_TEMP) * CONVERTER_GAIN_SLOPE; }
	static Units::FreqPair GetRfFreqs(Units::Frequency rfFreq, _In_ const Units::FreqPair& rxFreqs);
	static Units::Frequency GetRxFreq(Units::Frequency rfFreq);
	static float GetSwitchGainAdjustmentDb(float temp) { return (temp - NOMINAL_TEMP) * SWITCH_GAIN_SLOPE; }
	static size_t Index(ESource source);
	static bool IsSpectrumInverted(Units::Frequency freq) { return GetBand(freq) != 0; }
	static ESource Source(size_t index, bool horiz);

protected:
	// Types
	struct SCalSettings
	{
		Units::Frequency freq;
		float refDbm;
		float sampleDbm;
		float passRangeDbm;		//Half range where the measured value should lay.
		unsigned char control;
	};

	// Constants
	static const BandSettings BAND_SETTINGS;
	static const SCalSettings CAL_SETTINGS[];
	static const Units::Frequency LO_SETTINGS[];

	// Functions
	C72342011(void); // Can only be constructed by a derived class
	template<size_t N> C72342011(const CDownConverter::EGainMode (&gainModes)[N]);
	virtual ~C72342011(void);
	CDownConverter::EGainMode GetGainMode(Units::Frequency freq) const { return CDownConverter::GetGainMode(freq, BAND_SETTINGS); }
	static void SetCrossOverFreq(Units::Frequency freq) { s_crossOverFreq = freq; return; }
	void SetSwitch(_In_reads_(3) const unsigned char(&controlBytes)[3], bool force) { C7236200276342017::SetSwitch(controlBytes, force); }
	void SetSwitch(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf, EDfElemBandSel band = AUTO);
	static ESource Source(SEquipCtrlMsg::EAnt ant);

private:
	// Constants
	static const SBandSettings BAND_SETTINGS_[];
	static const float CAL_LEVEL_SLOPE; // dB / �C
	static const float CONVERTER_GAIN_SLOPE; // dB / �C
	static const float SWITCH_GAIN_SLOPE; // dB / �C

	// Functions
	static size_t GetBand(Units::Frequency freq);
	_Ret_ static const SCalSettings& GetCalSettings(Units::Frequency calFreq, bool useShf);
	_Ret_ virtual const CDownConverter::SReceiverGainModes& GetGainModes(void) const { return m_gainModes; }
	unsigned int GetSettleTime(_In_reads_(3) const unsigned char (&controlByte)[3]) const;
	virtual void SetSettleTime(unsigned int) {}

	// Data
	static Units::Frequency s_crossOverFreq; //Antenna dependent switch-over frequency of 7234 switch.
	CDownConverter::SReceiverGainModes m_gainModes;
};


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
template<size_t N> C72342011::C72342011(const CDownConverter::EGainMode (&gainModes)[N])
:
	C7236200276342017(C80682017::UHF),
	CDownConverter(),
	m_gainModes(&gainModes[0], &gainModes[_countof(BAND_SETTINGS_)])
{
	static_assert(N >= _countof(BAND_SETTINGS_), "Not enough values in gainModes array");
	SetSwitch(0, RECEIVE, TERM, TERM, UHF);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// ++ operator for ESource
//
inline C72342011::ESource& operator++(C72342011::ESource& s)
{
	switch(s)
	{
	case C72342011::ANT_1:
	case C72342011::ANT_2:
	case C72342011::ANT_3:
	case C72342011::ANT_4:
	case C72342011::ANT_5:
	case C72342011::ANT_6:
	case C72342011::ANT_7:
	case C72342011::ANT_8:
	case C72342011::ANT_1H:
	case C72342011::ANT_2H:
	case C72342011::ANT_3H:
	case C72342011::ANT_4H:
	case C72342011::ANT_5H:
	case C72342011::ANT_6H:
	case C72342011::ANT_7H:
	case C72342011::ANT_8H:
		s = static_cast<C72342011::ESource>(s + 1);
		break;

	case C72342011::ANT_9:
		s = C72342011::ANT_1;
		break;

	case C72342011::ANT_9H:
		s = C72342011::ANT_1H;
		break;

	default:
		break;
	}

	return s;
}


//////////////////////////////////////////////////////////////////////
//
// Conversion function for ESource
//
inline C72342011::ESource C72342011::Source(size_t index, bool horiz)
{
	ASSERT(index < NUM_ANTS);

	if(horiz)
	{
		return static_cast<ESource>(index + 0x11);
	}
	else
	{
		return static_cast<ESource>(index + 1);
	}
}


//////////////////////////////////////////////////////////////////////
//
// Conversion function for ESource
//
inline C72342011::ESource C72342011::Source(SEquipCtrlMsg::EAnt ant)
{
	switch(ant)
	{
	case SEquipCtrlMsg::ANT1:
		return C72342011::REF;

	case SEquipCtrlMsg::ANT1H:
		return C72342011::REF_HORIZON;

	case SEquipCtrlMsg::ANT2:
	case SEquipCtrlMsg::ANT3:
	case SEquipCtrlMsg::SMPL_RF2:
	case SEquipCtrlMsg::SMPL_RF3:
		return C72342011::TERM;

	case SEquipCtrlMsg::DF_ANT_1V:
		return C72342011::ANT_1;

	case SEquipCtrlMsg::DF_ANT_2V:
		return C72342011::ANT_2;

	case SEquipCtrlMsg::DF_ANT_3V:
		return C72342011::ANT_3;

	case SEquipCtrlMsg::DF_ANT_4V:
		return C72342011::ANT_4;

	case SEquipCtrlMsg::DF_ANT_5V:
		return C72342011::ANT_5;

	case SEquipCtrlMsg::DF_ANT_6V:
		return C72342011::ANT_6;

	case SEquipCtrlMsg::DF_ANT_7V:
		return C72342011::ANT_7;

	case SEquipCtrlMsg::DF_ANT_8V:
		return C72342011::ANT_8;

	case SEquipCtrlMsg::DF_ANT_9V:
		return C72342011::ANT_9;

	case SEquipCtrlMsg::DF_ANT_1H:
		return C72342011::ANT_1H;

	case SEquipCtrlMsg::DF_ANT_2H:
		return C72342011::ANT_2H;

	case SEquipCtrlMsg::DF_ANT_3H:
		return C72342011::ANT_3H;

	case SEquipCtrlMsg::DF_ANT_4H:
		return C72342011::ANT_4H;

	case SEquipCtrlMsg::DF_ANT_5H:
		return C72342011::ANT_5H;

	case SEquipCtrlMsg::DF_ANT_6H:
		return C72342011::ANT_6H;

	case SEquipCtrlMsg::DF_ANT_7H:
		return C72342011::ANT_7H;

	case SEquipCtrlMsg::DF_ANT_8H:
		return C72342011::ANT_8H;

	case SEquipCtrlMsg::DF_ANT_9H:
		return C72342011::ANT_9H;

	default:
		ASSERT(FALSE);
		return C72342011::TERM;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Conversion function for ESource to get DF antenna index
//
inline size_t C72342011::Index(ESource source)
{
	switch(source)
	{
	case ANT_1:
	case ANT_2:
	case ANT_3:
	case ANT_4:
	case ANT_5:
	case ANT_6:
	case ANT_7:
	case ANT_8:
	case ANT_9:
		return source - static_cast<size_t> (ANT_1);

	case ANT_1H:
	case ANT_2H:
	case ANT_3H:
	case ANT_4H:
	case ANT_5H:
	case ANT_6H:
	case ANT_7H:
	case ANT_8H:
	case ANT_9H:
		return source - static_cast<size_t> (ANT_1H);

	case TERM:
		//TERM is valid due to 649-8 Type-2 5 elements sample mapping to the 9
		// inputs of 7234.
		return 0;

	default:
		ASSERT(FALSE);
		return 0;
	}
}

// Use selectany to put this here rather than in the .cpp so the size of the array can be determined by users of the class
__declspec(selectany) const CDownConverter::SBandSettings C72342011::BAND_SETTINGS_[] =
{ // low 20 dB, low 1 dB, high 1 dB, high 20 dB, data
	{   20000000,   20000000, 2700000000, 2700000000, 0x00 },
	{ 2300000000, 2700000000, 3200000000, 3700000000, 0x09 },
	{ 2800000000, 3200000000, 4000000000, 4500000000, 0x0a },
	{ 3300000000, 4000000000, 4800000000, 5300000000, 0x0b },
	{ 3900000000, 4800000000, 5600000000, 6200000000, 0x0c },
	{ 4900000000, 5600000000, 6400000000, 7000000000, 0x0d },
	{ 5500000000, 6400000000, 7200000000, 8100000000, 0x0e },
	{ 6000000000, 7200000000, 8000000000, 8800000000, 0x0f }
};

