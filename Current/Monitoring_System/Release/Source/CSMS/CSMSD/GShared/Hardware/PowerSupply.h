/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013 - 2015 TCI International, Inc. All rights reserved       *
**************************************************************************/

#pragma once

//@note: CPowerSupply is not available for XP built.
#ifndef _USING_V110_SDK71_
#include <array>
#include <Dbt.h>
#include <utility>
#include <vector>

#include "SLABCP2112.h"
#include "SlimRWLock.h"

class CPowerSupply
{
public:
	// Constants
	static const signed char BATTERY_OVERTEMP = 60; // �C
	static const unsigned short CRITICAL_RUN_TIME = 2; // minutes
	static const unsigned short LOW_RUN_TIME = 15; // minutes
	static const float DEPLETED_VOLTAGE;
	static const float LOW_VOLTAGE;
	static const float MINIMUM_VOLTAGE;
	static const float ZERO_VOLTAGE;

	// Types
	enum ELed { OFF = 0x0, RED = 0x1, GREEN = 0x2, YELLOW = 0x3 }; // Values match hardware
	enum ETemperature { OVERTEMP = 0x0, HOT = 0x2, OK = 0x3, INVALID }; // Values match hardware
	enum EPowerSource { SOURCE_SAME, INTERNAL_BATTERY, INTERNAL_BATT_AND_AUX, EXTERNAL_BATTERY, EXTERNAL_BATT_AND_AUX, EXTERNAL_AUX, INADEQUATE, UNKNOWN };
	enum EPowerStatus
	{
		C_POWER_STATUS_NO_CHANGE,
        C_POWER_OFF,
        C_INTERNAL_CRITICAL,
		C_EXTERNAL_CRITICAL,
		C_INTERNAL_LOW,
		C_EXTERNAL_LOW,
        C_RESTORED,
	};

	struct SInternalBatteryStatus
	{
		bool valid;
		unsigned short timeToEmpty; // min
		unsigned short voltage; // mV
		short current; // mA
		signed char temperature; // �C
		unsigned char chargeLevel; // %
		bool charged;
		bool discharged;
		bool needsConditioning;
		bool needsInitializing;
	};

	// Functions
	CPowerSupply(void);
	~CPowerSupply(void);
	EPowerStatus ChkPower(_Out_ EPowerSource & batterySource, _Out_ unsigned short & runTime);
	std::vector<SInternalBatteryStatus> GetInternalBatteryStatus(void) const;
	std::pair<float, CPowerSupply::ETemperature> GetTemperatures(void) const;
	std::array<float, 3> GetExternalVoltages(void) const;
	static EPowerSource GetPowerSource(_In_ const std::vector<SInternalBatteryStatus>& internalBatteryStatus,
		_In_ const std::array<float, 3> externalVoltages);
	bool IsPresent(void) const { return m_isPresent; }
	bool IsPowerOffInProgress(void);
	void OnDeviceEvent(DWORD dwEventType, _In_ const DEV_BROADCAST_HDR* devBroadcastHdr);
	void SetLed(ELed color);
	void TurnOffPower(void);

private:
	// Constants
	static const BYTE ADC_ADDRESS = 0x90;
	static const BYTE BATTERY_ADDRESS = 0x16;
	static const BYTE MUX_ADDRESS = 0xe0;
	static const BYTE NUM_INTERNAL_BATTERIES = 4;
	static const WORD PID = 0xea90;
	static LPCSTR SLABSMBUSDLL;
	static const WORD VID = 0x10c4;

	// Types
	class CDeviceChangeWindow
	:
		public CWnd
	{
		DECLARE_MESSAGE_MAP()

	public:
		CDeviceChangeWindow(_In_ CPowerSupply* powerSupply) : CWnd(), powerSupply(powerSupply) {}

	private:
		BOOL OnDeviceChange(UINT nEventType, DWORD_PTR dwData)
		{
			if(nEventType == DBT_DEVICEARRIVAL || nEventType == DBT_DEVICEREMOVECOMPLETE)
			{
				powerSupply->OnDeviceEvent(nEventType, reinterpret_cast<const DEV_BROADCAST_HDR*>(dwData));
			}

			return TRUE;
		}

		CPowerSupply* powerSupply;
	};

	// Functions
	static void Check(HID_SMBUS_STATUS status) { if(status != HID_SMBUS_SUCCESS) throw status; }
	void Close(void);
	static UINT DeviceChangeThread(_In_ void* arg);
	static void CALLBACK OnTimer(_Inout_ PTP_CALLBACK_INSTANCE, _Inout_opt_ void* context, _Inout_ PTP_TIMER) { if(context) static_cast<CPowerSupply*>(context)->OnTimer(); }
	void OnTimer(void) const;
	short ReadAdc(BYTE input, BYTE pga) const;
	template<typename T> T ReadSMBus(BYTE address, BYTE command) const;
	void SelectMux(BYTE input) const;
	void TryOpen(void);
	void WriteSMBus(BYTE address, _In_ const std::vector<BYTE>& data) const;
	template<typename T> void WriteSMBus(BYTE address, BYTE command, T data) const;

	// Data
	mutable CRWLock m_critSect;
	CWinThread* m_deviceChangeThread;
	HDEVNOTIFY m_devNotify;
	HID_SMBUS_DEVICE m_hidSmbusDevice;
	bool m_isPresent;
	bool m_powerOffInProgress;
	PTP_TIMER m_timer;
};


//////////////////////////////////////////////////////////////////////
//
// Read from an SMBus device
//
template<typename T> T CPowerSupply::ReadSMBus(BYTE address, BYTE command) const
{
	HID_SMBUS_S0 status = HID_SMBUS_S0_IDLE;
	WORD numRetries = 1;
	WORD bytesRead = 0;

	for(int attempt = 0; attempt < 5 && (status == HID_SMBUS_S0_BUSY || status == HID_SMBUS_S0_ERROR || numRetries > 0); ++attempt)
	{
		Check(HidSmbus_AddressReadRequest(m_hidSmbusDevice, address, sizeof(T), sizeof(command), &command));
//		Check(HidSmbus_AddressReadRequest(m_hidSmbusDevice, address, sizeof(T) + 1, sizeof(command), &command));
		status = HID_SMBUS_S0_BUSY;
		HID_SMBUS_S1 detailedStatus;

		for(int attempt2 = 0; attempt2 < 10 && status == HID_SMBUS_S0_BUSY; ++attempt2)
		{
			Check(HidSmbus_TransferStatusRequest(m_hidSmbusDevice));
			Check(HidSmbus_GetTransferStatusResponse(m_hidSmbusDevice, &status, &detailedStatus, &numRetries, &bytesRead));
		}

		if(status == HID_SMBUS_S0_BUSY)
		{
			HidSmbus_CancelTransfer(m_hidSmbusDevice);
		}
	}

	if(status != HID_SMBUS_S0_COMPLETE || status == HID_SMBUS_S0_ERROR || numRetries > 0)
	{
		HidSmbus_CancelTransfer(m_hidSmbusDevice);
		throw HID_SMBUS_READ_ERROR;
	}

	Check(HidSmbus_ForceReadResponse(m_hidSmbusDevice, sizeof(T)));
//	Check(HidSmbus_ForceReadResponse(m_hidSmbusDevice, sizeof(T) + 1));
	BYTE buffer[122];
	BYTE numBytesRead;

//	for(BYTE numBytesNeeded = sizeof(T) + 1; numBytesNeeded > 0 && status != HID_SMBUS_S0_ERROR; numBytesNeeded -= numBytesRead)
	for(BYTE numBytesNeeded = sizeof(T); numBytesNeeded > 0 && status != HID_SMBUS_S0_ERROR; numBytesNeeded -= numBytesRead)
	{
//		Check(HidSmbus_GetReadResponse(m_hidSmbusDevice, &status, &buffer[sizeof(T) + 1 - numBytesNeeded], sizeof(buffer), &numBytesRead));
		Check(HidSmbus_GetReadResponse(m_hidSmbusDevice, &status, &buffer[sizeof(T) - numBytesNeeded], sizeof(buffer), &numBytesRead));
	}

	if(status == HID_SMBUS_S0_ERROR)
	{
		throw HID_SMBUS_READ_ERROR;
	}

	return reinterpret_cast<T&>(*buffer);
}


//////////////////////////////////////////////////////////////////////
//
// Write to an SMBus device
//
template<typename T> void CPowerSupply::WriteSMBus(BYTE address, BYTE command, T data) const
{
	std::vector<BYTE> buffer(sizeof(command) + sizeof(data));
	buffer[0] = command;
	memcpy_s(&buffer[1], sizeof(buffer) - 1, &data, sizeof(data));
	WriteSMBus(address, buffer);

	return;
}

#endif  //End of #ifndef _USING_V110_SDK71_