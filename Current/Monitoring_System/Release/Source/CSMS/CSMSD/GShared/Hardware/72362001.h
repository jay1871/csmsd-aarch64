/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2008 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#pragma once

#include "7236200276472017.h"
#include "EquipCtrlMsg.h"
#include "Units.h"

class C72362001
:
	// Inheritance
	protected C7236200276342017
{
public:
	// Constants
	static const long BITE_DBM = -30;
	static const unsigned long CROSSOVER_FREQ_HZ = 285000;
	static const unsigned char NUM_ANTS = 8;

	enum ESource // Some values match hardware - don't change!
	{
		TERM	= 0,
		MONITOR	= -1,
		REF		= -2,
		BITE	= -3,
		NOISE	= -4,
		ANT_1	= 0x01,
		ANT_2	= 0x02,
		ANT_3	= 0x03,
		ANT_4	= 0x04,
		ANT_5	= 0x05,
		ANT_6	= 0x06,
		ANT_7	= 0x07,
		ANT_8	= 0x08,
		AUX_1	= 0x09,
		AUX_2	= 0x0a
	};

	// Functions
	static Units::Frequency GetCalFreq(Units::Frequency freq) { return GetCalSettings(freq).calFreq; }
	static Units::Frequency GetCalFreq(size_t index);
	static size_t Index(ESource source);
	static ESource Source(size_t index);
	static const unsigned char NON_GRAY = 0;

protected:
	// Types
	enum ERefMode { RECEIVE, TX_BITE };

	// Functions
	C72362001(void); // Can only be constructed by a derived class
	virtual ~C72362001(void);
	static void GetControlBytes(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf,
		_Out_writes_all_(3) unsigned char (&controlBytes)[3]);
	using C7236200276342017::SetSwitch;
	void SetSwitch(_In_reads_(3) const unsigned char(&controlBytes)[3], bool force) { C7236200276342017::SetSwitch(controlBytes, force); }
	void SetSwitch(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf);
	static ESource Source(SEquipCtrlMsg::EAnt ant);

private:
	// Types
	struct SCalSettings
	{
		Units::Frequency calFreq;
		unsigned char control1;
		unsigned char control2;
	};

	// Constants
	static const Units::Frequency C72362001::CAL_BASE_FREQ; // Hz
	static const SCalSettings CAL_SETTINGS[];

	// Functions
	_Ret_ static const SCalSettings& GetCalSettings(Units::Frequency calFreq);
	unsigned int GetSettleTime(_In_reads_(3) const unsigned char (&controlByte)[3]) const;
	virtual void SetSettleTime(unsigned int) {}
};


//////////////////////////////////////////////////////////////////////
//
// ++ operator for ESource
//
inline C72362001::ESource& operator++(C72362001::ESource& s)
{
	switch(s)
	{
	case C72362001::ANT_1:
	case C72362001::ANT_2:
	case C72362001::ANT_3:
	case C72362001::ANT_4:
	case C72362001::ANT_5:
	case C72362001::ANT_6:
	case C72362001::ANT_7:
		s = static_cast<C72362001::ESource>(s + 1);
		break;

	case C72362001::ANT_8:
		s = C72362001::ANT_1;
		break;

	default:
		break;
	}

	return s;
}


//////////////////////////////////////////////////////////////////////
//
// Conversion function for ESource
//
inline C72362001::ESource C72362001::Source(size_t index)
{
	ASSERT(index < NUM_ANTS);

	return static_cast<ESource>(index + 1);
}


//////////////////////////////////////////////////////////////////////
//
// Conversion function for ESource
//
inline C72362001::ESource C72362001::Source(SEquipCtrlMsg::EAnt ant)
{
	switch(ant)
	{
	case SEquipCtrlMsg::ANT1:
		return C72362001::MONITOR;

	case SEquipCtrlMsg::ANT2:
	case SEquipCtrlMsg::ANT3:
	case SEquipCtrlMsg::SMPL_RF2:
	case SEquipCtrlMsg::SMPL_RF3:
		return C72362001::TERM;

	case SEquipCtrlMsg::DF_ANT_1V:
		return C72362001::ANT_1;

	case SEquipCtrlMsg::DF_ANT_2V:
		return C72362001::ANT_2;

	case SEquipCtrlMsg::DF_ANT_3V:
		return C72362001::ANT_3;

	case SEquipCtrlMsg::DF_ANT_4V:
		return C72362001::ANT_4;

	case SEquipCtrlMsg::DF_ANT_5V:
		return C72362001::ANT_5;

	case SEquipCtrlMsg::DF_ANT_6V:
		return C72362001::ANT_6;

	case SEquipCtrlMsg::DF_ANT_7V:
		return C72362001::ANT_7;

	case SEquipCtrlMsg::DF_ANT_8V:
		return C72362001::ANT_8;

	default:
		ASSERT(FALSE);
		return C72362001::TERM;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Conversion function for ESource to get DF antenna index
//
inline size_t C72362001::Index(ESource source)
{
	switch(source)
	{
	case ANT_1:
	case ANT_2:
	case ANT_3:
	case ANT_4:
	case ANT_5:
	case ANT_6:
	case ANT_7:
	case ANT_8:
		return source - 1;

	default:
		ASSERT(FALSE);
		return 0;
	}
}
