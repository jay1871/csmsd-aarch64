/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2011-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "StdAfx.h"

#include <array>
#include <Cfgmgr32.h>
#include <deque>
#include <devguid.h>
#include <map>
#include <math.h>
#include <mmsystem.h>
#include <setupapi.h>
#include <stdexcept>
#include <vector>

//Uncomment this to enable Log.
//#define ENABLE_DBG_LOG 1

#include "32202001.h"
#include "IppVec.h"
#include "Log.h"
#include "Service.h"
#if _WIN32_WINNT >= _WIN32_WINNT_VISTA
	#include "SlimRWLock.h"
#endif
#include "Timer.h"
#include "Units.h"
#include "Utility.h"
#include "WbDigi.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
const float C32202001::ADC_FULL_SCALE_DBM = 8.0f;
const float C32202001::ADC_FULL_SCALE_WATTS = pow(10.0f, (C32202001::ADC_FULL_SCALE_DBM - 30.0f) / 10.0f);
LPCTSTR C32202001::MUTEX_NAME = _T("Global\\TCI.{A5EF37B7-02EC-47CD-9AB2-52817F3D0FF4}");
LPCTSTR C32202001::WBDIGI_DEVICE = _T("\\\\.\\WBDIGIFD");
LPCTSTR C32202001::WBDIGI_DEVICE_DESC = _T("TCI Wideband Digitizer");

#ifdef DEBUG
//Has the count of missing interrupt.
size_t C32202001::s_missedIntr = 0;
#endif

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C32202001::C32202001(void)
:
	m_handle(CreateFile(WBDIGI_DEVICE, GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, nullptr)),
	m_needRetune(true),
	m_needWait(true),
	m_sampleRate(0),
	m_regs(nullptr),
	m_sequencerBlockSize(0),
	m_sequencerCount(0),
	m_sequencerDone(), // Unsignalled, automatic reset
	m_sequencerTime(0),
	m_shutdown(FALSE, TRUE), // Unsignalled, manual reset
	m_type(UNKNOWN),
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get())
{
	// Check result from m_mutex CreateMutex to see if we created the global mutex
	m_master = (m_handle != INVALID_HANDLE_VALUE && (GetLastError() == ERROR_SUCCESS));

	// Init arrays
	for(auto ch = 0u; ch < NUM_DMAS; ++ch)
	{
		m_callbacks[ch] = nullptr;
		m_callbackParams[ch] = nullptr;
	}

	for(auto ch = 0u; ch < NUM_CHANS; ++ch)
	{
		m_decimation[ch] = 0;
		m_freq[ch] = 0;
		m_invertSpectrum[ch] = false;
		m_shift[ch] = false;
	}

	m_streaming[0] = m_streaming[1] = false;

	for(auto t = 0u; t < _countof(m_thread); ++t)
	{
		m_thread[t] = nullptr;
	}

//	QueryPerformanceFrequency(&m_perfFreq);

	// Setup high precision timers
//	TIMECAPS tc;
//	timeGetDevCaps(&tc, sizeof(tc));
//	m_periodMin = tc.wPeriodMin;
//	timeBeginPeriod(m_periodMin);

	if(m_handle != INVALID_HANDLE_VALUE)
	{
		SWbDigiRegs regs;
		DWORD count;

		if(!Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_GET_Regs, nullptr, 0, &regs, sizeof(regs), count) ||
			count != sizeof(regs) || regs.size != 0x10000 || regs.Regs == nullptr)
		{
			CloseHandle(m_handle);
			m_handle = INVALID_HANDLE_VALUE;
			return;
		}

		m_regs = static_cast<unsigned long*>(regs.Regs);

		// Get firmware type
		switch(PeekUser(0x05))
		{
		case 0:
			m_type = GOLDEN;
			break;

		case 1:
			if(PeekUser(0x03) >= 0x02000000)
			{
				m_type = SMS_2614;
			}
			else
			{
				m_type = SMS_2612;

				// Init allowable decimations table for SMS_2612
				m_decimations[1] = 0x00000;
				m_decimations[2] = 0x20000;
				m_decimations[4] = 0x30000;

				for(unsigned short cic0 = 1; cic0 < 256; ++ cic0)
				{
					for(unsigned short cic1 = 0; cic1 < 256; ++ cic1)
					{
						unsigned long decimation = 4 * (cic0 + 1) * (cic1 + 1);
						auto entry = m_decimations.find(4 * (cic0 + 1) * (cic1 + 1));

						if(entry == m_decimations.end())
						{
							m_decimations[decimation] = (0x30000 | (cic1 << 8) | cic0);
						}
						else
						{
							// Pick equivalent entry with cic0 and cic1 closest
							float ratio = float(cic0 + 1) / (cic1 + 1);

							if(ratio < 1)
							{
								ratio = 1 / ratio;
							}

							float ratio2 = float((entry->second & 0x000ff) + 1) / (((entry->second >> 8) & 0x000ff) + 1);

							if(ratio2 < 1)
							{
								ratio2 = 1 / ratio2;
							}

							if(ratio < ratio2)
							{
								// Update entry
								m_decimations[decimation] = (0x30000 | (cic1 << 8) | cic0);
							}
						}
					}
				}
			}

			break;

		case 16:
			m_type = BLACKBIRD;
			break;

		default:
			m_type = UNKNOWN;
			break;
		}

		if(m_master)
		{
			for(ULONG dma = 0; dma < (m_type == BLACKBIRD ? NUM_DMAS : 2); dma == 2 ? dma += 2 : ++dma)
			{
				SWbDigiDMAMemory memory;

				if(!Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_GET_DMA_MEMORY, &dma, sizeof(dma), &memory, sizeof(memory), count) ||
					count != sizeof(memory) || memory.Mem == nullptr)
				{
					CloseHandle(m_handle);
					m_handle = INVALID_HANDLE_VALUE;
					return;
				}

				m_buffer[dma] = static_cast<const volatile unsigned long long*>(memory.Mem);
				m_bufSize[dma] = memory.size / sizeof(*m_buffer[dma]);
			}

			m_timer[1].reset(new CTimer(CTimer::SHORT));

			// Turn on flash
			PokeUser(0x20, 0x38);

			// Write-protect golden image
			for(unsigned char sector = 0; sector < 80; ++sector)
			{
				PokeUser(0x23, sector * 0x10000); // Address of sector
				PokeUser(0x22, 0x0f); // Read lock register
				PokeUser(0x20, 0x39); // Execute command
				m_timer[1]->Delay(10);

				if((PeekUser(0x21) & 0x3) != 0x3)
				{
					PokeUser(0x22, 0x08); // Write enable
					PokeUser(0x20, 0x39); // Execute command
					m_timer[1]->Delay(10);
					PokeUser(0x22, 0x310); // Irreversible lock
					PokeUser(0x20, 0x39); // Execute command
					m_timer[1]->Delay(10);
				}
			}

			// Write protect application image
			for(unsigned char sector = 80; sector < 255; ++sector)
			{
				PokeUser(0x23, sector * 0x10000); // Address of sector
				PokeUser(0x22, 0x0f); // Read lock register
				PokeUser(0x20, 0x39); // Execute command
				m_timer[1]->Delay(10);

				if((PeekUser(0x21) & 0x3) != 0x1)
				{
					PokeUser(0x22, 0x08); // Write enable
					PokeUser(0x20, 0x39); // Execute command
					m_timer[1]->Delay(10);
					PokeUser(0x22, 0x110); // Reversible lock
					PokeUser(0x20, 0x39); // Execute command
					m_timer[1]->Delay(10);
				}
			}

			// Write protect serial number
			PokeUser(0x23, 0xff0000); // Address of sector
			PokeUser(0x22, 0x0f); // Read lock register
			PokeUser(0x20, 0x39); // Execute command
			m_timer[1]->Delay(10);

			if((PeekUser(0x21) & 0x3) != 0x3)
			{
				PokeUser(0x22, 0x08); // Write enable
				PokeUser(0x20, 0x39); // Execute command
				m_timer[1]->Delay(10);
				PokeUser(0x22, 0x310); // Irreversible lock
				PokeUser(0x20, 0x39); // Execute command
				m_timer[1]->Delay(10);
			}

			// Turn off flash
			PokeUser(0x20, 0x00);

			// Reset board
			ResetSampling();

			// Initialize decimations etc
			if(m_type == BLACKBIRD)
			{
				UseSampleRate(153600000);
				SetDecimation(96, 0);
				SetDecimation(96, 1);
			}
			else
			{
				UseSampleRate(81920000);
				SetDecimation(1024, 1);
				SetDecimation(1024, 2);
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C32202001::~C32202001(void)
{
	if(m_handle != INVALID_HANDLE_VALUE)
	{
		StopThreads();

		if(m_master)
		{
			PokeUser(0x00, 0x00); // Internal clock
		}

		CloseHandle(m_handle);
	}

//	timeEndPeriod(m_periodMin);

	return;
}


/////////////////////////////////////////////////////////////////////
//
// How many A/D counts will be used for the threshold counters when samples are collected
//
unsigned int C32202001::AdcCountsFromSamples(unsigned int samples, unsigned char ch) const
{
	switch(m_type)
	{
	case BLACKBIRD:
		return (samples - 1) * m_decimation[ch];

	case SMS_2612:
		return samples * m_decimation[1];

	case SMS_2614:
		return (samples - 1) * m_decimation[1];

	default:
		return 0;
	}
}


/////////////////////////////////////////////////////////////////////
//
// Adjust decimation to closest legal value
//
void C32202001::AdjustDecimation(_Inout_ unsigned long& decimation, bool adjustDown, Units::Frequency sampleRate) const
{
	switch(m_type)
	{
	case SMS_2612:
		if(decimation == 0)
		{
			decimation = 2;
		}
		else if(decimation >= m_decimations.rbegin()->first)
		{
			decimation = m_decimations.rbegin()->first;
		}
		else
		{
			if(adjustDown)
			{
				auto entry = m_decimations.upper_bound(decimation);
				decimation = (--entry)->first;
			}
			else
			{
				auto entry = m_decimations.lower_bound(decimation);

				if(entry->first != decimation)
				{
					unsigned long decimation1 = entry->first;
					unsigned long decimation2 = (--entry)->first;

					if(decimation * decimation < decimation1 * decimation2)
					{
						decimation = decimation2;
					}
					else
					{
						decimation = decimation1;
					}
				}
			}
		}

		break;

	case SMS_2614:
		if(decimation < 2)
		{
			decimation = 2;
		}
		else if(decimation >= 6 * 65536)
		{
			decimation = 6 * 65536;
		}
		else if(decimation > 6)
		{
			// Try 
			unsigned long decimation1 = std::max(decimation / 4 * 4, std::max(decimation / 5 * 5, decimation / 6 * 6));

			if(adjustDown)
			{
				decimation = decimation1;
			}
			else
			{
				unsigned long decimation2 = std::min((decimation + 3) / 4 * 4, std::min((decimation + 4) / 5 * 5, (decimation + 5) / 6 * 6));
				decimation = (decimation2 - decimation > decimation - decimation1 ? decimation1 : decimation2);
			}
		}

		break;

	case BLACKBIRD:
		if(decimation < 8)
		{
			decimation = 8;
		}
		else if(decimation >= 4 * 16384)
		{
			decimation = 4 * 16384;
		}
		else
		{
			if(adjustDown)
			{
				decimation = decimation / 4 * 4;
			}
			else
			{
				decimation = (decimation + 2) / 4 * 4;
			}

			if(sampleRate != 0)
			{
				while(sampleRate / decimation * decimation != sampleRate && decimation > 8)
				{
					decimation -= 4;
				}
			}
		}

		break;

	default:
		ASSERT(FALSE);
		break;
	}

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Helper function
//
unsigned long C32202001::CalcFreqWord(Units::Frequency freq, Units::Frequency sampleRate) const
{
	return unsigned long(freq.Hz<double>() * 4294967296 / sampleRate.Hz<double>());
}


//////////////////////////////////////////////////////////////////////
//
// Collect threshold counts from a channel
//
void  C32202001::CollectCounts(_Inout_ unsigned int& count,
	_Out_writes_all_(4) unsigned int(&counts)[4],
	_Out_writes_all_(4) unsigned short(&thresh)[4], _In_range_(0, 1) unsigned char ch)
{
	ASSERT(m_type == BLACKBIRD && ch < 2);

	// Ensure count is a multiple of 2 * decimation
	count = count / m_decimation[ch] / 2 * 2 * m_decimation[ch];

	if(m_master)
	{
		if(!(PeekUser(0x61) & (ch == 0 ? 0x0000100 : 0x1000000)))
		{
			TRACE(_T("AGC FIFO not empty\n"));
			ResetThresholdCounters(ch);
		}

		PokeUser(ch == 0 ? 0x44 : 0x46, count / m_decimation[ch]);

		// Collect
		m_sequencerDone[ch].ResetEvent();
//		QueryPerformanceCounter(&m_startTime);
//		m_time1 = m_time2 = m_time3 = m_time4 = m_startTime;
		SWbDigiFwdDMA params = { ch, count / m_decimation[ch] * sizeof(Ipp16sc), 1 };
		DWORD dummy;
		VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_PROG_FWD_DMA, &params, sizeof(params), nullptr, 0, dummy));
		WaitForSequencer(ch);
		m_dmaCount[ch] += count / m_decimation[ch] / 2;

		// Adjust count for actual number of A/D samples
		count = AdcCountsFromSamples(count / m_decimation[ch], ch);

		// Read AGC counters
		if(!GetThresholdCounters(counts, thresh, ch))
		{
			counts[0] = counts[1] = counts[2] = counts[3] = 0;
		}
	}
	else
	{
		counts[0] = counts[1] = counts[2] = counts[3] = 0;
		thresh[0] = thresh[1] = thresh[2] = thresh[3] = 0;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Collect threshold counts from both channels
//
void  C32202001::CollectCounts(_Inout_ unsigned int& count,
	_Out_writes_all_(4) unsigned int(&countA)[4],
	_Out_writes_all_(4) unsigned int(&countB)[4],
	_Out_writes_all_(4) unsigned short(&threshA)[4],
	_Out_writes_all_(4) unsigned short(&threshB)[4])
{
	ASSERT(m_type == SMS_2612 || m_type == SMS_2614);

	//Verify the count specify must satisfy required minimum decimation multiple.
	if( count < (C_MIN_BLK_SZ_IN_DECI_CNT * m_decimation[1]) )
	{
		TRACE(_T("Threshold count %u is increased to 2x decimation, which is %u\n"),
				count, C_MIN_BLK_SZ_IN_DECI_CNT * m_decimation[1]);
		count = C_MIN_BLK_SZ_IN_DECI_CNT * m_decimation[1];
	}
	else
	{
		// Ensure count is a multiple of decimation
		count = count / m_decimation[1] * m_decimation[1];
	}

	if(m_master)
	{
		if(!(PeekUser(0x41) & 0x00080))
		{
			TRACE(_T("Sequencer FIFO not empty\n"));
			PokeUser(0x40, 0x2);
		}

		if(!(PeekUser(0x61) & 0x1000100))
		{
			TRACE(_T("AGC FIFO not empty\n"));
			ResetThresholdCounters();
		}

		PokeUser(0xc0, 0x0000, 0x4000); // always immediate start
		PokeUser(0x42, PeekUser(0x43)); // Copy current antenna setting to FIFO
		PokeUser(0x42, 0); // No delay
		PokeUser(0x42, count / m_decimation[1]); // Number of samples
		
		// Collect
		m_sequencerDone[1].ResetEvent();
//		QueryPerformanceCounter(&m_startTime);
//		m_time1 = m_time2 = m_time3 = m_time4 = m_startTime;
		SWbDigiFwdDMA params = { 0, count / m_decimation[1] * 2 * sizeof(Ipp16sc), 1 };
		DWORD dummy;
		VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_PROG_FWD_DMA, &params, sizeof(params), nullptr, 0, dummy));
//		m_timer->Start(m_sequencerTime);
		WaitForSequencer(1);
		m_dmaCount[0] += count / m_decimation[1];

		// Adjust count for actual number of A/D samples
		count = AdcCountsFromSamples(count / m_decimation[1], 1);

		// Read AGC counters
		if(!GetThresholdCounters(countA, countB, threshA, threshB))
		{
			countA[0] = countA[1] = countA[2] = countA[3] = 0;
			countB[0] = countB[1] = countB[2] = countB[3] = 0;
		}
	}
	else
	{
		countA[0] = countA[1] = countA[2] = countA[3] = 0;
		countB[0] = countB[1] = countB[2] = countB[3] = 0;
		threshA[0] = threshA[1] = threshA[2] = threshA[3] = 0;
		threshB[0] = threshB[1] = threshB[2] = threshB[3] = 0;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Collect samples
//
void C32202001::CollectSamples(unsigned int count, unsigned int blockCount, signed char chOrBoth)
{
	ASSERT(chOrBoth == -1 || m_type == BLACKBIRD);

	if(m_master)
	{
		if(chOrBoth == -1)
		{
			if(m_type == BLACKBIRD)
			{
				StartSampling(count, 0, blockCount);
				StartSampling(count, 1, blockCount);
				WaitForSequencer(0);
				WaitForSequencer(1);
			}
			else
			{
				StartSampling(count, blockCount);
				WaitForSequencer(1);
			}
		}
		else
		{
			StartSampling(count, chOrBoth, blockCount);
			WaitForSequencer(chOrBoth);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Helper to deinterleave samples
//
void C32202001::Deinterleave(_In_count_(count) const volatile unsigned long long* src, size_t count, unsigned char ch, _Inout_ Ipp16scVec& dest,
	size_t offset)
{
	if(dest.size() < offset + count)
	{
		dest.resize(offset + count);
	}

	size_t dummy;
	size_t phase = ch;
	ippsSampleDown(const_cast<const Ipp16sc*>(reinterpret_cast<const volatile Ipp16sc*>(src)), 2 * count, &dest[offset], dummy, 2, phase);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Mark samples as OK to overwrite
//
void C32202001::DoneWithSamples(unsigned char dma, bool multipleUsers, unsigned int sampleCount)
{
#if _WIN32_WINNT< _WIN32_WINNT_VISTA
	CSingleLock lock(&m_dataCritSect[dma], TRUE);
#else
	CLockGuard lock(m_dataCritSect[dma]);
#endif
	if(multipleUsers)
	{
		for(auto inUse = m_oldestInUse[dma].begin(); inUse != m_oldestInUse[dma].end(); ++inUse)
		{
			if(inUse->sampleCount == sampleCount)
			{
				if(--inUse->numUsers == 0)
				{
					if(inUse == m_oldestInUse[dma].begin())
					{
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
						m_moreBufferSpace.SetEvent();
#else
						m_moreBufferSpace.Wake();
#endif
					}

					m_oldestInUse[dma].erase(inUse);
					return;
				}

				return;
			}
		}

		ASSERT(FALSE); // Not found
		return;
	}
	else
	{
		auto inUse = m_oldestInUse[dma].begin();
		ASSERT(inUse != m_oldestInUse[dma].end());
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
		m_moreBufferSpace.SetEvent();
#else
		m_moreBufferSpace.Wake();
#endif
		m_oldestInUse[dma].erase(inUse);
		return;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Enable the dummy data generator to flush DMA2
//
void C32202001::EnableDma2Flush(bool enable)
{
	ASSERT(m_type == BLACKBIRD);
	CRegLock lock(m_handle);
	PokeUser(0x82, 3);
	PokeUser(0x85, enable ? 0x100000 : 0x00000);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Enable loopback on a channel
//
void C32202001::EnableLoopback(bool enable, unsigned char ch, size_t offset)
{
	ASSERT(m_type == BLACKBIRD && ch < 2 && int(offset) < m_bufSize[ch] * 2);

	if(m_master)
	{
		ULONG dma = ch + 4;
		DWORD dummy;

		if(enable)
		{
			SWbDigiRevDMA params = { dma, ch, offset * sizeof(Ipp16sc), 16384, USER_OFFSET_CONTROL };
			VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_PROG_REV_DMA, &params, sizeof(params), nullptr, 0, dummy));
		}
		else
		{
			SWbDigiRevDMA params = { dma, ch, 0, 0, BLOCK_MODE_CONTROL };
			VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_PROG_REV_DMA, &params, sizeof(params), nullptr, 0, dummy));
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Enable streaming on a channel
//
void C32202001::EnableStreaming(bool enable, unsigned char ch, size_t packetSize)
{
	ASSERT(!enable || ((packetSize & 1) == 0 && packetSize >= 4));

	if(m_master)
	{
		ULONG dma = (m_type == BLACKBIRD ? (ch > 1 ? 2 : ch) : (ch == 2 ? 1 : 0));
		ASSERT(enable != m_streaming[dma]);
		DWORD dummy;

		if(enable)
		{
			m_streaming[dma] = true;
			SWbDigiFwdDMA params = { dma, packetSize * sizeof(Ipp16sc), 0 };
			VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_PROG_FWD_DMA, &params, sizeof(params), nullptr, 0, dummy));
		}
		else
		{
			VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_STOP_FWD_DMA, &dma, sizeof(dma), nullptr, 0, dummy));
			m_streaming[dma] = false;
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Check for FIFO overrun
//
bool C32202001::FifoHasOverrun(unsigned char ch)
{
	ASSERT(ch < 3);
	return ch < (m_type == BLACKBIRD ? 1 : 2) ? (PeekUser(0x08) & 0x0040) == 0x0040 : (PeekUser(0x08) & 0x0080) == 0x0080;
}


//////////////////////////////////////////////////////////////////////
//
// Find samples in buffer (waiting if necessary)
// Channel 0 and 1 samples are interleaved in DMA0 for SMS, separate in DMAs 0 and 1 for Blackbird. Blackbird DMA 2 samples are 64 bits.
//
_Ret_opt_count_(count) const Ipp16sc* C32202001::FindSamples(unsigned long long sampleCount, unsigned int count, unsigned char ch, DWORD timeout)
{
	ASSERT(ch < (m_type == BLACKBIRD ? 3 : 2));
	unsigned char dma = (m_type == BLACKBIRD ? ch : 0);
	bool wrapped = false;

	if(m_buffer[dma] == nullptr)
	{
		CString msg;
		msg.Format(_T("C32202001::FindSamples;m_buffer[dma] == nullptr"));
		CLog::Log(FFT_PROCESS, LOG_CATEGORY_SERVICE, msg);
		return nullptr;
	}

	unsigned int dmaCount = unsigned int((m_type == BLACKBIRD && dma != 2 ? sampleCount / 2 : sampleCount) & 0x00000000ffffffff);

	if(m_type == BLACKBIRD)
	{
		count /= 2;
	}

	ASSERT(int(count) < m_bufSize[dma]);
	int offset; // Offset of start of found samples in buffer
	volatile const unsigned long long* found;
#if _WIN32_WINNT< _WIN32_WINNT_VISTA
	CSingleLock lock(&m_dataCritSect[dma], TRUE);
	CSingleLock wait(&m_newSamples[dma]);
#else
	CLockGuard lock(m_dataCritSect[dma]);
#endif

	// Loop until we find them or timeout
	while(true)
	{
		if(dmaCount > m_dmaCountOffset[dma])
		{
			offset = dmaCount - m_dmaCountOffset[dma];
		}
		else
		{
			offset = -int(m_dmaCountOffset[dma] - dmaCount);
		}

#ifdef ENABLE_DBG_LOG
		CString msgGlobal;
		msgGlobal.Format(_T("C32202001::FindSamples; Channel = %u, Offset = %u, sampleCount =%llu, dmaCount = %u, count = %u"), dma, offset, sampleCount, dmaCount, count);
#endif

		if(offset < 0)
		{
			// Insert point has wrapped
			found = m_buffer[dma] + offset + m_bufSize[dma];

			if(offset < -m_bufSize[dma] || found < m_newestSample[dma])
			{
				TRACE(_T("Samples have been overwritten in buffer for channel %u, offset = %d, found = %p, m_newestSample = %p\n"),
					ch, offset, found, m_newestSample[dma]);
#ifdef DMA_STATS_LOG
				++m_dmaActivityCnt[NUM_DMAS * (2 + ch) + 0];
#endif // DMA_STATS_LOG
#ifdef ENABLE_DBG_LOG
				CLog::Log(FFT_PROCESS, LOG_CATEGORY_SERVICE, msgGlobal);
				CString msg;

				// Get tick count
				unsigned long tick = Utility::GetXpTickCount();
				msg.Format(_T("C32202001::FindSamples: Samples have been overwritten in buffer for channel %u, offset = %d, found = %p, m_newestSample = %p, tick =%lu\n"),
					ch, offset, found, m_newestSample[dma], tick);
				CLog::Log(FFT_PROCESS, LOG_CATEGORY_SERVICE, msg);
#endif
				return nullptr;
			}
			else if(found + count <= m_buffer[dma] + m_bufSize[dma])
			{
				// Found them
				break;
			}
			else if(found + count <= m_newestSample[dma] + m_bufSize[dma])
			{
				// Found but wrapped
				wrapped = true;
				break;
			}
			else
			{
				if(timeout > 0)
				{
					// Need to wait for new samples
#if _WIN32_WINNT< _WIN32_WINNT_VISTA
					m_newSamples[dma].ResetEvent();
					lock.Unlock();

					if(!wait.Lock(timeout))
					{
						TRACE(_T("Timed out waiting for %u samples from %I64u for channel %u\n"), count, sampleCount, ch);
						return nullptr;
					}

					wait.Unlock();
					lock.Lock();
#else
					if(!m_newSamples[dma].Sleep(m_dataCritSect[dma], timeout))
					{
						TRACE(_T("Timed out waiting for %u samples from %I64u for channel %u\n"), count, sampleCount, ch);
#ifdef DMA_STATS_LOG
						++m_dmaActivityCnt[NUM_DMAS * (2 + ch) + 1];
#endif // DMA_STATS_LOG
#ifdef ENABLE_DBG_LOG
						CLog::Log(FFT_PROCESS, LOG_CATEGORY_SERVICE, msgGlobal);
						CString msg;

						// Get tick count
						unsigned long tick = Utility::GetXpTickCount();
						msg.Format(_T("C32202001::FindSamples;Timed out waiting for %u samples from %I64u for channel %u, tick =%lu\n"),
							count, sampleCount, ch, tick);
						CLog::Log(FFT_PROCESS, LOG_CATEGORY_SERVICE, msg);
#endif
						return nullptr;
					}
#endif
				}
				else
				{
					return nullptr;
				}
			}
		}
		else
		{
			found = m_buffer[dma] + offset;

			if(found + count > m_newestSample[dma])
			{
				if(timeout > 0)
				{
					// Need to wait for new samples
#if _WIN32_WINNT< _WIN32_WINNT_VISTA
					m_newSamples[dma].ResetEvent();
					lock.Unlock();

					if(!wait.Lock(timeout))
					{
						TRACE(_T("Timed out waiting for %u samples from %I64u for channel %u\n"), count, sampleCount, ch);
						return nullptr;
					}

					wait.Unlock();
					lock.Lock();
#else
					if(!m_newSamples[dma].Sleep(m_dataCritSect[dma], timeout))
					{
						TRACE(_T("Timed out waiting for %u samples from %I64u for channel %u\n"), count, sampleCount, ch);
#ifdef DMA_STATS_LOG
						++m_dmaActivityCnt[NUM_DMAS * (2 + ch) + 2];
#endif // DMA_STATS_LOG
#ifdef ENABLE_DBG_LOG
						CLog::Log(FFT_PROCESS, LOG_CATEGORY_SERVICE, msgGlobal);
						CString msg;
						msg.Format(_T("C32202001::FindSamples;Timed out waiting for %u samples from %I64u for channel %u\n"), count, sampleCount, ch);
						CLog::Log(FFT_PROCESS, LOG_CATEGORY_SERVICE, msg);
#endif
						return nullptr;
					}
#endif
				}
				else
				{
					return nullptr;
				}
			}
			else
			{
				// Found them
				break;
			}
		}
	}

	if(count == 0)
	{
		return nullptr;
	}

	// Deinterleave
	if(wrapped)
	{
		// Wrapped
		if(m_type != BLACKBIRD)
		{
			Deinterleave(found, m_buffer[dma] + m_bufSize[dma] - found, ch, m_findBuffer[ch]);
			Deinterleave(m_buffer[dma], found + count - m_buffer[dma] - m_bufSize[dma], ch, m_findBuffer[ch], m_buffer[dma] + m_bufSize[dma] - found);
		}
		else
		{
			m_findBuffer[ch].resize(2 * count);
			memcpy_s(&m_findBuffer[ch][0], m_findBuffer[ch].size() * sizeof(m_findBuffer[ch][0]),
				const_cast<const unsigned long long*>(found), (m_buffer[dma] + m_bufSize[dma] - found) * sizeof(m_buffer[dma][0]));
			memcpy_s(&m_findBuffer[ch][0] + (m_buffer[dma] + m_bufSize[dma] - found) * sizeof(m_buffer[dma][0]) / sizeof(m_findBuffer[ch][0]),
				(count - (m_buffer[ch] + m_bufSize[dma] - found)) * sizeof(m_buffer[dma][0]),
				const_cast<const unsigned long long*>(m_buffer[dma]), (found + count - m_buffer[dma] - m_bufSize[dma]) * sizeof(m_buffer[dma][0]));
		}

		return &m_findBuffer[ch][0];
	}
	else
	{
		// Not wrapped
		if(m_type != BLACKBIRD)
		{
			Deinterleave(found, count, ch, m_findBuffer[ch]);
			return &m_findBuffer[ch][0];
		}
		else
		{
			return const_cast<const Ipp16sc*>(reinterpret_cast<const volatile Ipp16sc*>(found));
		}
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the clock status
//
bool C32202001::GetClockStatus(void) const
{
	bool ok = !(PeekUser(0x08) & 0x0008);
	const_cast<C32202001*>(this)->PokeUser(0x08, 0x38);

	return ok;
}


//////////////////////////////////////////////////////////////////////
//
// Get the cutoff as a fraction of the decimated bandwidth
//
float C32202001::GetCutoff(unsigned char) const
{
	return 0.8f;
}


//////////////////////////////////////////////////////////////////////
//
// Get LVDS data
//
unsigned short C32202001::GetDataOut(void) const
{
	return unsigned short(PeekUser(m_type == BLACKBIRD ? 0x43 : 0x45));
}



//////////////////////////////////////////////////////////////////////
//
// Get DDC input sample count
//
unsigned long C32202001::GetDdcSampleCount(unsigned char source) const
{
	ASSERT(m_type == BLACKBIRD && source < 7);
	unsigned long count;

	// Poll until count ready
	for(count = PeekUser(0x92 + source); (count & 0x1) != 0; count = PeekUser(0x92 + source))
	{
		_mm_pause();
	}

	return count;
}


//////////////////////////////////////////////////////////////////////
//
// Get decimation
//
unsigned long C32202001::GetDecimation(unsigned char ch) const
{
	return m_decimation[m_type != BLACKBIRD && ch == 0 ? 1 : ch];
}


//////////////////////////////////////////////////////////////////////
//
// Get driver version
//
CString C32202001::GetDriverVersion(void) const
{
	STciWinDrvInfo drvInfo;
	GetTciWinDriverInfo(WBDIGI_DEVICE_DESC, drvInfo);

	if(drvInfo.found)
	{
		return drvInfo.versionStr;
	}
	else
	{
		return _T("Unknown");
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get FPGA firmware date
//
COleDateTime C32202001::GetFpgaFwDate(void) const
{
	unsigned short reg = unsigned short(PeekUser(0x04));
	return COleDateTime(2010 + ((reg >> 12) & 0xf), ((reg >> 8) & 0xf), 10 * ((reg >> 4) & 0xf) + (reg & 0xf), 0, 0, 0);
}


//////////////////////////////////////////////////////////////////////
//
// Get FPGA firmware image
//
unsigned long C32202001::GetFpgaFwImage(void) const
{
	return unsigned long(PeekUser(0x05));
}


//////////////////////////////////////////////////////////////////////
//
// Get FPGA firmware revision
//
unsigned long C32202001::GetFpgaFwRev(void) const
{
	return unsigned long(PeekUser(0x03));
}


//////////////////////////////////////////////////////////////////////
//
// Get the DDR latency in seconds
//
float C32202001::GetLatency(Units::Frequency sampleRate, unsigned long decimation) const
{
	return (19 + (decimation == 2 ? 23 : 8 * decimation)) / sampleRate.Hz<float>();
}


//////////////////////////////////////////////////////////////////////
//
// Get PPS average
//
Units::TimeSpan C32202001::GetPpsAverage(void) const
{
	if(HasTimestampCapability())
	{
		short temp = ((PeekUser(m_type == SMS_2614 ? 0xc1 : 0x71) & 0x0fff0000) >> 16);

		if(temp > 2047)
		{
			temp -= 4096;
		}

		return Units::TimeSpan(double(temp) / PeekUser(m_type == SMS_2614 ? 0xc7 : 0x74));
	}
	else
	{
		return Units::TimeSpan(0);
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get sample counter value
//
unsigned int C32202001::GetSampleCount(unsigned char dma) const
{
	return m_type == BLACKBIRD && dma != 2 ? 2 * m_dmaCount[dma] : m_dmaCount[dma];
}


//////////////////////////////////////////////////////////////////////
//
// Get sample counter values
//
void C32202001::GetSampleCount(_Out_writes_all_(NUM_RF_CHANS) unsigned int(&count)[NUM_RF_CHANS]) const
{
	count[0] = (m_type == BLACKBIRD ? 2 * m_dmaCount[0] : m_dmaCount[0]);
	count[1] = (m_type == BLACKBIRD ? 2 * m_dmaCount[1] : m_dmaCount[0]);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the sample rate in use
//
Units::Frequency C32202001::GetSampleRate(void) const
{
	return m_sampleRate;
}


//////////////////////////////////////////////////////////////////////
//
// Get board serial number
//
unsigned long C32202001::GetSerialNumber(void) const
{
	CRegLock lock(m_handle);
	C32202001* mutableThis = const_cast<C32202001*>(this);
	mutableThis->PokeUser(0x20, 0x38); // Turn on flash
	mutableThis->PokeUser(0x22, 0x04); // Fast read
	mutableThis->PokeUser(0x23, 0xff0000); // Address
	mutableThis->PokeUser(0x24, 9); // 4 bytes
	mutableThis->PokeUser(0x08, 0x10000); // Clear flash interrupt
	mutableThis->PokeUser(0x20, 0x39); // Execute command

	while((PeekUser(0x08) & 0x10000) == 0x00000)
	{
		_mm_pause(); // Wait for interrupt
	}

	mutableThis->PokeUser(0x08, 0x10000); // Clear flash interrupt
	unsigned long serialNumber = PeekUser(0x26); // Read data (not byteswapped!)

	// HACK: need a second read sometimes
	mutableThis->PokeUser(0x20, 0x39); // Execute command

	while((PeekUser(0x08) & 0x10000) == 0x00000)
	{
		_mm_pause(); // Wait for interrupt
	}

	mutableThis->PokeUser(0x08, 0x10000); // Clear flash interrupt
	serialNumber = PeekUser(0x26); // Read data (not byteswapped!)
	mutableThis->PokeUser(0x20, 0x00); // Turn off flash

	return serialNumber;
}


//////////////////////////////////////////////////////////////////////
/// <summary>
/// Obtain TCI driver info.
/// </summary>
/// <param name="desc">
/// String that has the driver description.
/// </param>
/// <param name="drvInfo">
/// Upon return the driver info will be stored in the fields of the structure
///  or found will be set to false.
/// @note: 1. Caller must set the appropriate drvId that corresponds to the
///           description string.
///        2. drvInfo.driverInfoStr will have the appropriate string regardless
///           of whether driver is found or not.
/// </param>
_Success_(drvInfo.found) void C32202001::GetTciWinDriverInfo(_In_ LPCTSTR desc, STciWinDrvInfo& drvInfo)
{
	drvInfo.found = false;
	HDEVINFO devInfoSet = SetupDiGetClassDevs(&GUID_DEVCLASS_SYSTEM, nullptr, nullptr, DIGCF_PRESENT);
	SP_DEVINFO_DATA devInfo;
	devInfo.cbSize = sizeof(devInfo);
	BYTE buffer[1024] = {};

	for(int devIndex = 0; SetupDiEnumDeviceInfo(devInfoSet, devIndex, &devInfo); ++devIndex)
	{
		if(SetupDiGetDeviceRegistryProperty(devInfoSet, &devInfo, SPDRP_DEVICEDESC, nullptr, buffer, sizeof(buffer), nullptr))
		{
#pragma warning(suppress: 6102) // Spurious warning
			if(_tcscmp(reinterpret_cast<LPCTSTR>(buffer), desc) == 0)
			{
				SP_DEVINSTALL_PARAMS devInstallParams;
				devInstallParams.cbSize = sizeof(devInstallParams);
				SetupDiGetDeviceInstallParams(devInfoSet, &devInfo, &devInstallParams);
				devInstallParams.FlagsEx |= DI_FLAGSEX_INSTALLEDDRIVER;
				SetupDiSetDeviceInstallParams(devInfoSet, &devInfo, &devInstallParams);
				SetupDiBuildDriverInfoList(devInfoSet, &devInfo, SPDIT_COMPATDRIVER);
				SP_DRVINFO_DATA drvInfoData;
				drvInfoData.cbSize = sizeof(drvInfoData);

				if(SetupDiEnumDriverInfo(devInfoSet, &devInfo, SPDIT_COMPATDRIVER, 0, &drvInfoData))
				{
					drvInfo.tciTnumber = (drvInfoData.DriverVersion & 0xffff000000000000) >> 48;
					drvInfo.majorVersion = (drvInfoData.DriverVersion & 0xffff00000000) >> 32;
					drvInfo.minorVersion = (drvInfoData.DriverVersion & 0xffff0000) >> 16;
					drvInfo.builtNum = drvInfoData.DriverVersion & 0xffff;
					
					SYSTEMTIME driverDate;

					if(FileTimeToSystemTime(&drvInfoData.DriverDate, &driverDate))
					{
						COleDateTime date(driverDate);
						drvInfo.versionStr.Format(_T(" %s, %I32u.%I32u.%I32u.%I32u"),
							LPCTSTR(date.Format(VAR_DATEVALUEONLY)), drvInfo.tciTnumber,
							drvInfo.majorVersion, drvInfo.minorVersion, drvInfo.builtNum);
						drvInfo.found = true;
					}

					break;	//Exit for loop.
				}
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get board temperatures
//
std::vector<float> C32202001::GetTemperatures(void) const
{
	CRegLock lock(m_handle);
	std::vector<float> temps;

	if(m_type == SMS_2612)
	{
		CTimer timer(CTimer::SHORT);
		const_cast<C32202001*>(this)->PokeUser(0xe0, 0x0001, 0x0007);
		timer.Delay(10);
		temps.push_back(PeekUser(0xe2) * 503.975f / 1024 - 273.15f);
	}
	else
	{
		const_cast<C32202001*>(this)->PokeUser(0xe0, 0x0000, 0x007f);
		unsigned long data;

		while(((data = PeekUser(0xe1)) & 0x10000) == 0)
		{
			_mm_pause();
		}

		temps.push_back((data & 0xffff) * 503.975f / 65536 - 273.15f);
	}

	return temps;
}


//////////////////////////////////////////////////////////////////////
//
// Get board temperature limits
//
std::vector<std::array<float, 2> > C32202001::GetTempLimits(void) const
{
	if(m_type == SMS_2612)
	{
		static const std::array<float, 2> LIMITS = { 0, 85 };
		return std::vector<std::array<float, 2> >(&LIMITS, &LIMITS + 1);
	}
	else
	{
		CRegLock lock(m_handle);
		const_cast<C32202001*>(this)->PokeUser(0xe0, 0x0050, 0x007f);
		unsigned long data;

		while(((data = PeekUser(0xe1)) & 0x10000) == 0)
		{
			_mm_pause();
		}

		std::vector<std::array<float, 2> > limits(1);
		limits[0][0] = 0; // Hardcoded as chip doen't have undertemp warining
		limits[0][1] = (data & 0xffff) * 503.975f / 65536 - 273.15f; 
		return limits;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get threshold counter values for a single channel
//
_Success_(return) bool C32202001::GetThresholdCounters(_Out_writes_all_(4) unsigned int(&counts)[4],
	_Out_writes_all_(4) unsigned short(&thresh)[4], unsigned char ch)
{
	if(m_master)
	{
		GetThresholds(thresh, ch);
		unsigned long status;

		if((status = PeekUser(0x61)) & (ch == 0 ? 0x0000200 : 0x02000000))
		{
			TRACE(_T("AGC FIFO %u overflow\n"), ch);
			ResetThresholdCounters(ch);
			return false;
		}

		unsigned short count = (ch == 0 ? status & 0x00000ff : unsigned short((status & 0x0ff0000) >> 16));
		count |= (ch == 0 ? (status & 0x0001c00) >> 2 : (status & 0x1c000000) >> 18);

		if(count < 4)
		{
			return false;
		}

		CRegLock lock(m_handle);
		PokeUser(0x60, ch == 0 ? 0x0 : 0x4);

		for(size_t i = 0; i < 4; ++i)
		{
			counts[i] = PeekUser(0x64);
		}
	}
	else
	{
		counts[0] = counts[1] = counts[2] = counts[3];
		thresh[0] = thresh[1] = thresh[2] = thresh[3];
	}

	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Get threshold counter values for both channels
//
_Success_(return) bool C32202001::GetThresholdCounters(_Out_writes_all_(4) unsigned int(&countA)[4],
	_Out_writes_all_(4) unsigned int(&countB)[4],
	_Out_writes_all_(4) unsigned short(&threshA)[4],
	_Out_writes_all_(4) unsigned short(&threshB)[4])
{
	if(m_master)
	{
		GetThresholds(threshA, threshB);
		unsigned long status;

		if((status = PeekUser(0x61)) & 0x2000200)
		{
			TRACE(_T("AGC FIFO overflow\n"));
			ResetThresholdCounters();
			return false;
		}

		unsigned short countsA = (status & 0x00000ff);
		countsA |= ((status & 0x0001c00) >> 10);
		unsigned short countsB = unsigned char((status & 0x0ff0000) >> 16);
		countsB |= ((status & 0x1c000000) >> 26);

		if(countsA != countsB || countsA == 0 || countsA % 4 != 0)
		{
			TRACE(_T("AGC FIFO counts bad (%u, %u)\n"), countsA, countsB);
			ResetThresholdCounters();
			return false;
		}

		{
			CRegLock lock(m_handle);
			PokeUser(0x60, 0x0);

			for(size_t i = 0; i < 4; ++i)
			{
				countA[i] = PeekUser(0x64);
			}

			PokeUser(0x60, 0x4);

			for(size_t i = 0; i < 4; ++i)
			{
				countB[i] = PeekUser(0x64);
			}
		}

		if(countA[0] < countA[1] || countA[1] < countA[2] || countA[2] < countA[3] ||
			countB[0] < countB[1] || countB[1] < countB[2] || countB[2] < countB[3])
		{
			TRACE(_T("AGC FIFO values bad (%u, %u, %u, %u), (%u, %u, %u, %u)\n"),
				countA[0], countA[1], countA[2], countA[3], countB[0], countB[1], countB[2], countB[3]);
			return false;
		}

		return true;
	}
	else
	{
		return false;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the AGC thresholds for a single channel
//
void C32202001::GetThresholds(_Out_writes_all_(4) unsigned short (&thresh)[4], unsigned char ch) const
{
	CRegLock lock(m_handle);
	const_cast<C32202001*>(this)->PokeUser(0x60, ch == 0 ? 0x0 : 0x4);
	unsigned long reg = PeekUser(0x62);
	thresh[0] = reg & 0xffff;
	thresh[1] = (reg >> 16) & 0xffff;
	reg = PeekUser(0x63);
	thresh[2] = reg & 0xffff;
	thresh[3] = (reg >> 16) & 0xffff;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the AGC thresholds for both channels
//
void C32202001::GetThresholds(_Out_writes_all_(4) unsigned short(&threshA)[4], _Out_writes_all_(4) unsigned short(&threshB)[4]) const
{
	CRegLock lock(m_handle);
	const_cast<C32202001*>(this)->PokeUser(0x60, 0x0);
	unsigned long reg = PeekUser(0x62);
	threshA[0] = reg & 0xffff;
	threshA[1] = (reg >> 16) & 0xffff;
	reg = PeekUser(0x63);
	threshA[2] = reg & 0xffff;
	threshA[3] = (reg >> 16) & 0xffff;
	const_cast<C32202001*>(this)->PokeUser(0x60, 0x4);
	reg = PeekUser(0x62);
	threshB[0] = reg & 0xffff;
	threshB[1] = (reg >> 16) & 0xffff;
	reg = PeekUser(0x63);
	threshB[2] = reg & 0xffff;
	threshB[3] = (reg >> 16) & 0xffff;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the current timestamp
//
Units::Timestamp C32202001::GetTimeStamp(void) const
{
	Units::Timestamp timestamp;

	if(HasTimestampCapability())
	{
		unsigned long sampleRate = PeekUser(m_type == SMS_2614 ? 0xc7 : 0x74);

		if(sampleRate == 0)
		{
			return timestamp;
		}

		unsigned long long seconds1 = PeekUser(m_type == SMS_2614 ? 0xc5 : 0x72);

		if(seconds1 == 0)
		{
			return timestamp;
		}

		unsigned long long offset = PeekUser(m_type == SMS_2614 ? 0xc6 : 0x73);
		unsigned long long seconds2 = PeekUser(m_type == SMS_2614 ? 0xc5 : 0x72);

		if(seconds1 != seconds2)
		{
			offset = PeekUser(m_type == SMS_2614 ? 0xc6 : 0x73);
		}

		timestamp.m_timestamp = (seconds2 << 32) | ((offset << 32) / sampleRate);

		return timestamp;
	}
	else
	{
		return timestamp;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get version string
//
CString C32202001::GetVersion(void)
{
	if(m_handle != INVALID_HANDLE_VALUE)
	{
		CString version;
		version.Format(_T("FPGA firmware type %u, %s rev 0x%x, serial number %x"), PeekUser(0x05), LPCTSTR(GetFpgaFwDate().Format(_T("%x"))), GetFpgaFwRev(), GetSerialNumber());
		return version;
	}
	else
	{
		return _T("Board not responding");
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get board voltage limits
//
std::vector<std::array<float, 2> > C32202001::GetVoltageLimits(void) const
{
	if(m_type == SMS_2612)
	{
		static const std::array<float, 2> LIMITS[2] = { { 0.95f, 1.05f }, { 2.375f, 2.625f } };
		return std::vector<std::array<float, 2> >(LIMITS, LIMITS + 2);
	}
	else
	{
		CRegLock lock(m_handle);
		const_cast<C32202001*>(this)->PokeUser(0xe0, 0x0055, 0x007f);
		unsigned long data;

		while(((data = PeekUser(0xe1)) & 0x10000) == 0)
		{
			_mm_pause();
		}

		std::vector<std::array<float, 2> > limits(2);
		limits[0][0] = (data & 0xffff) * 3.0f / 65536; 
		const_cast<C32202001*>(this)->PokeUser(0xe0, 0x0051, 0x007f);

		while(((data = PeekUser(0xe1)) & 0x10000) == 0)
		{
			_mm_pause();
		}

		limits[0][1] = (data & 0xffff) * 3.0f / 65536;
		const_cast<C32202001*>(this)->PokeUser(0xe0, 0x0056, 0x007f);

		while(((data = PeekUser(0xe1)) & 0x10000) == 0)
		{
			_mm_pause();
		}

		limits[1][0] = (data & 0xffff) * 3.0f / 65536;
		const_cast<C32202001*>(this)->PokeUser(0xe0, 0x0052, 0x007f);

		while(((data = PeekUser(0xe1)) & 0x10000) == 0)
		{
			_mm_pause();
		}

		limits[1][1] = (data & 0xffff) * 3.0f / 65536;
		return limits;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get board voltages
//
std::vector<float> C32202001::GetVoltages(void) const
{
	CRegLock lock(m_handle);
	std::vector<float> voltages;

	if(m_type == SMS_2612)
	{
		CTimer timer(CTimer::SHORT);
		const_cast<C32202001*>(this)->PokeUser(0xe0, 0x0003, 0x0007);
		timer.Delay(10);
		voltages.push_back(PeekUser(0xe2) * 3.0f / 1024);
		const_cast<C32202001*>(this)->PokeUser(0xe0, 0x0005, 0x0007);
		timer.Delay(20);
		voltages.push_back(PeekUser(0xe2) * 3.0f / 1024);
	}
	else
	{
		const_cast<C32202001*>(this)->PokeUser(0xe0, 0x0001, 0x007f);
		unsigned long data;

		while(((data = PeekUser(0xe1)) & 0x10000) == 0)
		{
			_mm_pause();
		}

		voltages.push_back((data & 0xffff) * 3.0f / 65536);
		const_cast<C32202001*>(this)->PokeUser(0xe0, 0x0002, 0x007f);

		while(((data = PeekUser(0xe1)) & 0x10000) == 0)
		{
			_mm_pause();
		}

		voltages.push_back((data & 0xffff) * 3.0f / 65536);
	}

	return voltages;
}


//////////////////////////////////////////////////////////////////////
//
// Is the device in a "problem" state
//
bool C32202001::HasProblem(void) const
{
	HDEVINFO devInfoSet = SetupDiGetClassDevs(&GUID_DEVCLASS_SYSTEM, nullptr, nullptr, DIGCF_PRESENT);
	SP_DEVINFO_DATA devInfo;
	devInfo.cbSize = sizeof(devInfo);

	for(int devIndex = 0; SetupDiEnumDeviceInfo(devInfoSet, devIndex, &devInfo); ++devIndex)
	{
		BYTE buffer[1024] = {};

		if(SetupDiGetDeviceRegistryProperty(devInfoSet, &devInfo, SPDRP_DEVICEDESC, nullptr, buffer, sizeof(buffer), nullptr))
		{
			ULONG status;
			ULONG problem;

#pragma warning(suppress: 6102) // Spurious warning
			if(_tcscmp(reinterpret_cast<LPCTSTR>(buffer), WBDIGI_DEVICE_DESC) == 0 &&
				CM_Get_DevNode_Status(&status, &problem, devInfo.DevInst, 0) == CR_SUCCESS)
			{
				return (status & DN_HAS_PROBLEM) != 0; 
			}
		}
	}

	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Initialize the board
//
void C32202001::Init(void)
{
	ResetSampling();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Calculate an approximation to the inverse error function
//
float C32202001::InvErf(float x)
{
	static const float A = 8 * (Units::PI - 3) / (3 * Units::PI * (4 - Units::PI));
	static const float PI_A = 8 * (Units::PI - 3) / (3 * (4 - Units::PI));
	float temp1 = log(1 - x * x);
	float temp2 = (2 / PI_A + temp1 / 2);
	float invErf = sqrt(sqrt(temp2 * temp2 - temp1 / A) - temp2);

	return x < 0 ? -invErf : invErf;
}


//////////////////////////////////////////////////////////////////////
//
// Mark samples as in-use
//
void C32202001::MarkSamplesInUse(unsigned int sampleCount, unsigned int count, unsigned char dma, unsigned char numUsers)
{
	if(m_buffer[dma] == nullptr)
	{
		return;
	}

	ASSERT(count <= unsigned int(m_bufSize[dma]));
#if _WIN32_WINNT< _WIN32_WINNT_VISTA
	CSingleLock lock(&m_dataCritSect[dma], TRUE);
#else
	CLockGuard lock(m_dataCritSect[dma]);
#endif
	m_newestInUse[dma] = sampleCount + count;
	SInUse inUse = { sampleCount, numUsers };
	m_oldestInUse[dma].push_back(inUse);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Measure noise power using threshold counters
//
void C32202001::MeasureNoiseDbm(_Out_writes_all_(2) float(&power)[2])
{
	unsigned int count = 102400;
	static const float SQRT2 = sqrtf(2);
	unsigned int counts[2][4];
	unsigned short thresh[2][4];

	if(m_type == BLACKBIRD)
	{
		unsigned int count0 = count;
		CollectCounts(count0, counts[0], thresh[0], 0);
		CollectCounts(count, counts[1], thresh[1], 1);
	}
	else
	{
		CollectCounts(count, counts[0], counts[1], thresh[0], thresh[1]);
	}

	for(size_t i = 0; i < 2; ++i)
	{
		float sum = 0;
		unsigned int numGood = 0;

		for(unsigned int counter = 0; counter < 4; ++counter)
		{
			ASSERT(counts[i][counter] <= count + 8);
			float fraction = float(counts[i][counter]) / count;

			if(fraction > 0.05f && fraction < 0.95f)
			{
				float sigma = thresh[i][counter] / (SQRT2 * InvErf(1 - fraction));
				sum += sigma;
				++numGood;
			}
		}

		if(numGood > 0)
		{
			power[i] = 20 * log10(SQRT2 * sum / numGood / ADC_FULL_SCALE_COUNT) + GetAdcFullScaleDbm();
		}
		else if(float(counts[i][2]) / count < 0.5f)
		{
			power[i] = -std::numeric_limits<float>::infinity(); // underflow
		}
		else
		{
			power[i] = std::numeric_limits<float>::infinity(); // overflow
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Measure tone power using threshold counters
//
void C32202001::MeasureToneDbm(_Out_writes_all_(2) float(&power)[2])
{
	unsigned int count = 102400;
	unsigned int counts[2][4];
	unsigned short thresh[2][4];

	if(m_type == BLACKBIRD)
	{
		unsigned int count0 = count;
		CollectCounts(count0, counts[0], thresh[0], 0);
		CollectCounts(count, counts[1], thresh[1], 1);
	}
	else
	{
		CollectCounts(count, counts[0], counts[1], thresh[0], thresh[1]);
	}

	for(size_t i = 0; i < 2; ++i)
	{
		float sum = 0;
		unsigned int numGood = 0;

		for(unsigned int counter = 0; counter < 4; ++counter)
		{
			ASSERT(counts[i][counter] <= count + 8);
			float fraction = float(counts[i][counter]) / count;

			if(fraction > 0.05f && fraction < 0.95f)
			{
				float peak = thresh[i][counter] / sin(Units::HALF_PI * (1 - fraction));

				if(peak <= ADC_FULL_SCALE_COUNT)
				{
					sum += peak;
					++numGood;
				}
			}
		}

		if(numGood > 0)
		{
			power[i] = 20 * log10(sum / numGood / ADC_FULL_SCALE_COUNT) + GetAdcFullScaleDbm();
		}
		else if(float(counts[i][2]) / count < 0.5f)
		{
			power[i] = -std::numeric_limits<float>::infinity(); // underflow
		}
		else
		{
			power[i] = std::numeric_limits<float>::infinity(); // overflow
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback for thread failure
//
void C32202001::OnFail(DWORD error, _In_ LPCTSTR description)
{
	CLog::Log(SMS_SERVICE_FAILED, LOG_CATEGORY_SERVICE, description);
	CService::Instance()->OnAbort(error);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reverse DMA completed
//
void C32202001::OnProcessed(_In_range_(4, 7) unsigned char dma, size_t offset, size_t size)
{
	ASSERT(m_type == BLACKBIRD);

	if(m_callbacks[dma] != nullptr)
	{
		m_callbacks[dma](const_cast<const Ipp16sc*>(reinterpret_cast<const volatile Ipp16sc*>(m_buffer[dma] + offset / sizeof(m_buffer[dma][0]))),
			size / sizeof(Ipp16sc), m_callbackParams[dma]);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// New samples in DMA buffer
//
void C32202001::OnSamples(_In_range_(0, 2) unsigned char dma, size_t offset, size_t size)
{
#if _WIN32_WINNT< _WIN32_WINNT_VISTA
	CSingleLock lock(&m_dataCritSect[dma], TRUE);
#else
	CLockGuard lock(m_dataCritSect[dma]);
#endif
	const volatile unsigned long long* newestSample = m_buffer[dma] + (offset + size) / sizeof(m_buffer[dma][0]);
	ASSERT(m_type == BLACKBIRD || dma < 2);
	ASSERT(newestSample <= m_buffer[dma] + m_bufSize[dma]); // Buffer overrun

	if(newestSample > m_buffer[dma] + m_bufSize[dma])
	{
		CStringA error;
		error.Format("DMA %u buffer overrun: buffer start 0x%p, buffer size 0x%X, offset 0x%X bytes, size 0x%X bytes\n", dma, m_buffer[dma], m_bufSize[dma], offset * sizeof(m_buffer[dma][0]), size * sizeof(m_buffer[dma][0]));
		OutputDebugStringA(error);
#ifdef DMA_STATS_LOG
		++m_dmaActivityCnt[NUM_DMAS + dma];
#endif
		return;
	}

	if(m_streaming[dma])
	{
		m_dmaCount[dma] += size / sizeof(m_buffer[dma][0]);
	}


	// Signal new data arrived
#if _WIN32_WINNT < _WIN32_WINNT_VISTA
	m_newSamples[dma].SetEvent();
#else
	m_newSamples[dma].WakeAll();
#endif

	// Call callbacks
	auto prevNewestSample = m_newestSample[dma];
	m_newestSample[dma] = newestSample;
	auto Callback = (m_type == BLACKBIRD ? m_callbacks[dma] : m_callbacks[dma + 1]);
	auto callbackParams = (m_type == BLACKBIRD ? m_callbackParams[dma] : m_callbackParams[dma + 1]);
	lock.Unlock();

	if(newestSample < prevNewestSample)
	{
		// Buffer wrapped
		bool getPartial = true;

		if (m_type != BLACKBIRD)
		{
			//In SMS, discard some of the streaming data if it causes
			// out-of-memory error.
			if (newestSample != prevNewestSample + size / sizeof(m_buffer[dma][0]) - m_bufSize[dma])
			{
#ifdef DEBUG
				// Get tick count
				unsigned long tick = Utility::GetXpTickCount();
				TRACE(_T("DMA %u missed interrupts (buffer wrapped), newestSample=%p, prevNewestSample=%p, tick = %lu\n"),
					dma, newestSample, prevNewestSample, tick);
				++s_missedIntr;
#endif

				//Only skip SMS streaming data if there is a possibility of out of memory.
				if (dma == 1)
				{
					const size_t C_MAX_LATENCY_CNT = 640;
					bool skipedData = false;

					if (prevNewestSample + size / sizeof(m_buffer[dma][0]) * C_MAX_LATENCY_CNT <= (m_buffer[dma] + m_bufSize[dma]))
					{
						skipedData = true;
					}
					else
					{
						ptrdiff_t wrappedSize = prevNewestSample + size / sizeof(m_buffer[dma][0]) * C_MAX_LATENCY_CNT -
							(m_buffer[dma] + m_bufSize[dma]);
						if (m_buffer[dma] + wrappedSize < newestSample)
						{
							skipedData = true;
						}
					}

					if (skipedData)
					{
						//Need to move prevNewestSample to skip-over missing interrupt data.
						if (newestSample < (m_buffer[dma] + size / sizeof(m_buffer[dma][0])))
						{
							//Move prevNewestSample to the beginning of the data.
							prevNewestSample = (m_buffer[dma] + m_bufSize[dma]) -
								(size / sizeof(m_buffer[dma][0]) - (newestSample - m_buffer[dma]));
						}
						else
						{
							getPartial = false;
							prevNewestSample = newestSample - size / sizeof(m_buffer[dma][0]);
						}
					}
				}
			}
		}
#ifdef DEBUG
		else
		{
			if (newestSample != prevNewestSample + size / sizeof(m_buffer[dma][0]) - m_bufSize[dma])
			{
				// Get tick count
				unsigned long tick = Utility::GetXpTickCount();
				TRACE(_T("DMA %u missed interrupts (buffer wrapped), newestSample=%p, prevNewestSample=%p, tick = %lu\n"),
					dma, newestSample, prevNewestSample, tick);
				++s_missedIntr;
			}
		}
#endif

		m_dmaCountOffset[dma] += m_bufSize[dma];

		if(Callback != nullptr)
		{
			if (getPartial && (prevNewestSample < m_buffer[dma] + m_bufSize[dma]))
			{
				Callback(const_cast<const Ipp16sc*>(reinterpret_cast<const volatile Ipp16sc*>(prevNewestSample)),
					2 * unsigned int(m_buffer[dma] + m_bufSize[dma] - prevNewestSample), callbackParams);
			}

			if(newestSample > m_buffer[dma])
			{
				Callback(const_cast<const Ipp16sc*>(reinterpret_cast<const volatile Ipp16sc*>(m_buffer[dma])),
					2 * unsigned int(newestSample - m_buffer[dma]), callbackParams);
			}
		}
	}
	else if(newestSample > prevNewestSample)
	{
#ifdef DEBUG
		if(newestSample != prevNewestSample + size / sizeof(m_buffer[dma][0]))
		{
			// Get tick count
			unsigned long tick = Utility::GetXpTickCount();

			TRACE(_T("DMA %u missed %u interrupts (got 0x%x, expected 0x%x), newestSample = %p, prevNewestSample = %p, m_buffer[dma] %p, m_bufSize[dma] %p, tick = %lu\n"),
				dma, (newestSample - prevNewestSample) * sizeof(m_buffer[dma][0]) / size - 1,
				(newestSample - prevNewestSample) * sizeof(m_buffer[dma][0]), size,
				newestSample, prevNewestSample, m_buffer[dma], m_bufSize[dma], tick);
			++s_missedIntr;
		}
#endif

		if(Callback != nullptr)
		{
			Callback(const_cast<const Ipp16sc*>(reinterpret_cast<const volatile Ipp16sc*>(prevNewestSample)),
				2 * unsigned int(newestSample - prevNewestSample), callbackParams);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Read a DMA register
//
unsigned long C32202001::PeekDma(unsigned short reg) const
{
	return m_regs[reg];
}


//////////////////////////////////////////////////////////////////////
//
// Read a user register
//
unsigned long C32202001::PeekUser(unsigned char reg) const
{
	return m_regs[2 * reg + 0x2000];
}


//////////////////////////////////////////////////////////////////////
//
// Write a DMA register
//
void C32202001::PokeDma(unsigned char reg, unsigned long data, unsigned long mask)
{
	if(mask != 0xfffffffful)
	{
		ASSERT(!(data & ~mask));
		data |= (PeekDma(reg) & ~mask);
	}

	m_regs[reg] = data;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Write a user register
//
void C32202001::PokeUser(unsigned char reg, unsigned long data, unsigned long mask)
{
	if(mask != 0xfffffffful)
	{
		ASSERT(!(data & ~mask));
		data |= (PeekUser(reg) & ~mask);
	}

	m_regs[2 * reg + 0x2000] = data;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Process samples using a reverse DMA channel
//
void  C32202001::ProcessSamples(_In_reads_(count) const Ipp16sc* samples, unsigned int count, unsigned char dma)
{
	ASSERT(m_type == BLACKBIRD && dma > 3);

	if(reinterpret_cast<const unsigned long long*>(samples) < m_buffer[dma] || reinterpret_cast<const unsigned long long*>(samples + count) > m_buffer[dma] + m_bufSize[dma])
	{
		throw std::runtime_error("Samples not in DMA buffer");
	}

	SWbDigiRevDMA params = { dma, 0, (reinterpret_cast<const unsigned long long*>(samples) - m_buffer[dma]) * sizeof(m_buffer[dma][0]), count * sizeof(Ipp16sc), BLOCK_MODE_CONTROL };
	DWORD dummy;
	VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_PROG_REV_DMA, &params, sizeof(params), nullptr, 0, dummy));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Register a callback for new samples arrived or samples processed
//
void C32202001::RegisterNewSamplesCallback(_In_ NewSamplesCallback callback, _In_opt_ void* param, unsigned char ch)
{
	ASSERT(ch < NUM_DMAS);
	__analysis_assume(ch < NUM_DMAS);

	if(m_master)
	{
#if _WIN32_WINNT< _WIN32_WINNT_VISTA
		CSingleLock lock(&m_dataCritSect[ch], TRUE);
#else
		CLockGuard lock(m_dataCritSect[ch]);
#endif
		m_callbacks[ch] = callback;
		m_callbackParams[ch] = param;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset DDC input FIFO
//
void C32202001::ResetDdcFifo(unsigned char source)
{
	ASSERT(m_type == BLACKBIRD && source < 6);
	PokeUser(0x90, 0x1 << source, 0x00003f);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset DDC input sample count
//
void C32202001::ResetDdcSampleCount(unsigned char source)
{
	ASSERT(m_type == BLACKBIRD && source < 7);
	PokeUser(0x90, 0x10000 | (source << 17), 0xf0000);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset sampling chains
//
void C32202001::ResetSampling(void)
{
	if(m_master)
	{
		for(size_t i = 0; i < NUM_DMAS; ++i)
		{
			m_dmaCount[i] = 0;
			m_newestSample[i] = m_buffer[i];
			m_dmaCountOffset[i] = 0;
		}

		unsigned short thresh[2][4];
		GetThresholds(thresh[0], thresh[1]);

		// Reset board
		DWORD count;
		VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_RESET_DIGITIZER, nullptr, 0, nullptr, 0, count));
		m_timer[1]->Delay(100);

		// Init board
		PokeUser(0x00, 0x20); // External clock

		if(m_type != BLACKBIRD)
		{
			PokeUser(0x40, 0x2); // Flush sequencer FIFO
		}

		PokeUser(0x43, 0x000); // LVDS Out
		PokeUser(0xa0, 0x808); // X2 gain, DF output mode. Note: Blackbird image has different register definition, but initial value is the same
		PokeUser(0xc0, 0x01c); // DC block, low gain, no dither, no randomize
		SetThresholdCounters(thresh[0], thresh[1]);

		// Reset driver
		for(ULONG dma = 0; dma < (m_type == BLACKBIRD ? NUM_DMAS : 2u); dma == 2 ? (dma += 2) : ++dma)
		{
			VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_RESET_DMA, &dma, sizeof(dma), nullptr, 0, count));
			VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_FLUSH_DMA_QUEUE, &dma, sizeof(dma), nullptr, 0, count));
		}

		VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_FLUSH_INTERRUPT_QUEUE, nullptr, 0, nullptr, 0, count));

		// Restart threads
		StopThreads();
		StartThreads();

		// Clear clock loss interrupts
		PokeUser(0x08, 0x00038);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset threshold counters
//
void C32202001::ResetThresholdCounters(signed char chOrBoth)
{
	if(m_master)
	{
		if(m_type == BLACKBIRD)
		{
			// Sync to updates in streaming mode
			CRegLock lock(m_handle);

			if(chOrBoth == 0 || chOrBoth == -1)
			{
				PokeUser(0x60, 0x0);
				PokeUser(0x65, PeekUser(0x65));
			}

			if(chOrBoth == 1 || chOrBoth == -1)
			{
				PokeUser(0x60, 0x4);
				PokeUser(0x65, PeekUser(0x65));
			}
		}

		PokeUser(0x60, chOrBoth == -1 ? 0xb : (chOrBoth == 0 ? 0x1 : 0x2));

#ifdef _DEBUG
		static size_t s_resetThresholdCnt = 0;
		static bool s_outAlways = false;

		bool outTrace = true;
		++s_resetThresholdCnt;

		if (!s_outAlways && s_resetThresholdCnt > 0x100 && ((s_resetThresholdCnt & 0x1ff) != 0))
		{
			outTrace = false;
		}

		if (outTrace)
		{
			// Get tick count
			unsigned long tick = Utility::GetXpTickCount();
			TRACE(_T("C32202001::ResetThresholdCounters have been called for %u times, Tick = %lu\n"), s_resetThresholdCnt, tick);
		}
#endif
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the streaming mode AGC window size
//
void C32202001::SetStreamingAgcWindowSize(unsigned long window, unsigned char ch)
{
	ASSERT(m_type == BLACKBIRD && ch < 2);
	CRegLock lock(m_handle);
	PokeUser(0x60, ch << 2);
	PokeUser(0x65, window - 1);

	//Flush the FIFO in according to FPGA doc.
	PokeUser(0x60, ch == 0 ? 0x1 : 0x2);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Control the BITE generator
//
void C32202001::SetBite(bool on, bool comb, unsigned char chMask)
{
	if(m_master)
	{
		unsigned short data = 0x0;

		if(on)
		{
			data = (chMask << 5) | (m_type == BLACKBIRD ? (comb ? 0x0100 : 0x0000) : (comb ? 0x0000 : 0x0100));
		}

		PokeUser(0xc0, data, 0x0360);
		PokeUser(0xa0, on ? 0x000 : 0x808, 0x808); // Turn off X2 for BITE
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the LVDS data out
//
void C32202001::SetDataOut(unsigned short data)
{
	if(m_master)
	{
		PokeUser(0x43, data);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the DDC decimation
//
bool C32202001::SetDecimation(unsigned long decimation, unsigned char ch, unsigned char gainShift)
{
	if(m_master)
	{
		if(m_type != BLACKBIRD && ch == 0)
		{
			ch = 1;
		}

		if(decimation != m_decimation[ch])
		{
			m_decimation[ch] = decimation;
			m_needWait = true;

			switch(m_type)
			{
			case SMS_2612:
				{
					auto entry = m_decimations.find(decimation);
					ASSERT(gainShift == 0 && entry != m_decimations.end());
					PokeUser(0x82, ch); // Select channel
					PokeUser(0x84, entry->second);
				}

				break;

			case SMS_2614:
				{
					unsigned char fir;

					for(fir = 6; fir > 0; --fir)
					{
						if(decimation % fir == 0)
						{
							unsigned int rate = decimation / fir;
							ASSERT(rate == 1 || fir > 3); // Poor SFDR if DDC is used with low-rate FIR
							PokeUser(0x82, ch); // Select channel
							PokeUser(0x84, (fir << 16) | (rate - 1));

							if(rate > 1)
							{
								int shift = int(5 * (16 - log(float(rate)) / Units::LN2));
								int gain = int(pow(2.0, (91 - shift)) / rate / rate / rate / rate / rate);
								ASSERT(shift < 76 && gain < 4096);
								PokeUser(0x85, ((shift + gainShift) << 12) | gain);
							}

							break;
						}
					}

					ASSERT(fir > 0); // Achievable
				}

				break;

			case BLACKBIRD:
				{
					if(ch == 0)
					{
						// ZIF DDC A
						ASSERT(decimation % 3 == 0);
						unsigned int rate = decimation / 3;
						PokeUser(0xc7, rate - 1, 0x00ff);

						if(rate > 1)
						{
							int shift = int(5 * (5 - log(float(rate)) / Units::LN2));
							int gain = int(pow(2.0, (32 - shift)) / rate / rate / rate / rate / rate);
							ASSERT(shift < 21 && gain < 256);
							PokeUser(0xc8, ((shift + gainShift) << 8) | gain, 0x0000ffff);
						}
					}
					else if(ch == 1)
					{
						// ZIF DDC B
						ASSERT(decimation % 3 == 0);
						unsigned int rate = decimation / 3;
						PokeUser(0xc7, (rate - 1) << 8, 0xff00);

						if(rate > 1)
						{
							int shift = int(5 * (5 - log(float(rate)) / Units::LN2));
							int gain = int(pow(2.0, (32 - shift)) / rate / rate / rate / rate / rate);
							ASSERT(shift < 21 && gain < 256);
							PokeUser(0xc8, ((shift + gainShift) << 24) | (gain << 16), 0xffff0000);
						}
					}
					else if(ch < 4)
					{
						// Wideband DDCs @ channels 2 & 3 implemented as channels 0 and 1 of bank 0
						ASSERT(decimation % 4 == 0);
						CRegLock lock(m_handle);
						PokeUser(0x82, ch == 2 ? 0 : 1);
						unsigned int rate = decimation / 4;

						if(rate == 0)
						{
							PokeUser(0x85, 0x000000, 0x100000); // Turn off
						}
						else
						{
							// Set decimation
							PokeUser(0x84, rate - 1);

							// Set gain
							int shift = int(5 * (14 - log(float(rate)) / Units::LN2));
							int gain = int(pow(2.0, (81 - shift)) / rate / rate / rate / rate / rate);
							ASSERT(shift < 66 && gain < 4096);
							PokeUser(0x85, 0x100000 | ((shift + gainShift) << 12) | gain); // Implicitly turn on
						}
					}
					else
					{
						// Narrowband DDCs
						ASSERT(ch < PeekUser(0x80) && decimation % 4 == 0);
						CRegLock lock(m_handle);
						PokeUser(0x82, ch); // Select channel
						unsigned int rate = decimation / 4;

						if(rate == 0)
						{
							PokeUser(0x85, 0x000000, 0x100000); // Turn off
						}
						else
						{
							// Set decimation
							PokeUser(0x84, rate - 1);

							// Set gain
							int shift = int(5 * (14 - log(float(rate)) / Units::LN2));
							int gain = int(pow(2.0, (81 - shift)) / rate / rate / rate / rate / rate);
							ASSERT(shift < 66 && gain < 4096);
							PokeUser(0x85, 0x100000 | ((shift + gainShift) << 12) | gain); // Implicitly turn on
						}
					}
				}

				break;
			}
		}
	}

	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Set Blackbird DDC input source
//
void C32202001::SetInput(unsigned char source, unsigned char ch)
{
	ASSERT(m_type == BLACKBIRD && ch > 1);
	CRegLock lock(m_handle);
	PokeUser(0x82, ch < 4 ? ch - 2 : ch); // Wideband DDCs (ch 2 and 3) implemented as channels 0 and 1 of bank 0
	PokeUser(0x86, source);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the interrupt moderation rate for a streaming DMA
//
void C32202001::SetInterruptModeration(unsigned long rate, unsigned char dma)
{
	ASSERT(dma < 3);
	SWbDigiDMAStreamPacketCount params = { dma, rate };
	DWORD dummy;
	VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_DMA_STREAM_PACKET_COUNT, &params, sizeof(params), nullptr, 0, dummy));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Setup sequencer
//
void C32202001::SetReverseDmaRate(unsigned char rate, unsigned char dma)
{
	ASSERT(m_type == BLACKBIRD && dma > 3 && dma < 8 && rate < 4);
	PokeUser(0x90, rate << 2 * dma, 0x3 << 2 * dma);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Setup sequencer
//
void C32202001::SetSequencer(const std::vector<std::pair<unsigned short, unsigned long>>& block)
{
	ASSERT(m_type == BLACKBIRD && block.size() < 256);

	if(!(PeekUser(0x41) & 0x00800080))
	{
		TRACE(_T("Sequencer FIFO not empty\n"));
		PokeUser(0x40, 0x2);
	}

	m_sequencerLength = 0;
	m_sequencerCount = unsigned char(block.size());

	for(size_t step = 0; step < m_sequencerCount; ++step)
	{
		ASSERT((block[step].first & 0xfffff000) == 0 && (block[step].second & 0xfff00000) == 0);
		PokeUser(0x42, (block[step].second << 12) | block[step].first);
		m_sequencerLength += block[step].second;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Setup sequencer
//
void C32202001::SetSequencer(const std::vector<unsigned short>& pattern, unsigned int delay, unsigned long samples)
{
	ASSERT(m_type != BLACKBIRD);

	if(!(PeekUser(0x41) & 0x0080))
	{
		TRACE(_T("Sequencer FIFO not empty\n"));
		PokeUser(0x40, 0x2);
	}

	m_sequencerCount = unsigned char(pattern.size());
	m_sequencerBlockSize = samples;

	PokeUser(0xc0, 0x0000, 0x4000); // always immediate start

	for(size_t step = 0; step < m_sequencerCount; ++step)
	{
		PokeUser(0x42, pattern[step]); // Antenna setting
		PokeUser(0x42, delay); // Delay
		PokeUser(0x42, m_sequencerBlockSize); // Number of samples
	}

//	m_sequencerTime = DWORD(m_sequencerCount * (m_sequencerBlockSize * m_decimation[1] + delay) * 1000000ull / m_sampleRate.Hz<DWORD>() +
//		GetLatency(m_sampleRate, m_decimation[1]) * 1000000); // us

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the AGC slice levels
//
void C32202001::SetThresholdCounters(_In_reads_(4) const unsigned short (&threshA)[4], _In_reads_(4) const unsigned short (&threshB)[4])
{
	if(m_master)
	{
		CRegLock lock(m_handle);
		PokeUser(0x60, 0x0);
		PokeUser(0x62, (threshA[1] << 16) | threshA[0]);
		PokeUser(0x63, (threshA[3] << 16) | threshA[2]);
		PokeUser(0x60, 0x4);
		PokeUser(0x62, (threshB[1] << 16) | threshB[0]);
		PokeUser(0x63, (threshB[3] << 16) | threshB[2]);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for filters etc to settle
//
void C32202001::Settle(bool wait)
{
	if(m_needRetune)
	{
		m_needRetune = false;

		for(unsigned char ch = (m_type == BLACKBIRD ? 0 : 1); ch < (m_type == BLACKBIRD ? 2 : 3); ++ch)
		{
			Units::Frequency freq = m_freq[ch];
			m_freq[ch] = 0;
			Tune(freq, m_invertSpectrum[ch], m_shift[ch], ch);
		}
	}

	if(m_needWait || wait)
	{
		m_needWait = false;
		m_timer[1]->Delay(unsigned long(GetLatency(m_sampleRate, m_decimation[1]) / (m_sampleRate.Hz<float>() / 1e6f))); // TODO Do we need this?
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Start collecting a block of samples (dual channel)
//
void C32202001::StartSampling(unsigned int count, unsigned int blockCount, bool delayedStart, DATE startTime)
{
	ASSERT(m_type != BLACKBIRD);

	if(count >= unsigned int(m_bufSize[0]))
	{
		throw std::runtime_error("DMA buffer too small");
	}

	ASSERT(blockCount > 0 && count / blockCount >= 2);

	if(!(PeekUser(0x41) & 0x00080))
	{
		TRACE(_T("Sequencer FIFO not empty\n"));
		PokeUser(0x40, 0x2);
	}

	if(!(PeekUser(0x61) & 0x1000100))
	{
		TRACE(_T("AGC FIFO not empty\n"));
		ResetThresholdCounters();
	}

	if(delayedStart && m_type == SMS_2614)
	{
		Units::Timestamp timeStamp(startTime);
		PokeUser(0xcd, unsigned long(timeStamp.m_timestamp >> 32));  // seconds
		PokeUser(0xce, unsigned long(((timeStamp.m_timestamp & 0xffffffff) * m_sampleRate.Hz<unsigned long>()) >> 32));  // fraction in clock intervals
		PokeUser(0xc0, 0x4000, 0x4000);
	}
	else
	{
		PokeUser(0xc0, 0x0000, 0x4000);
	}

	PokeUser(0x42, PeekUser(0x43)); // Copy current antenna setting to FIFO
	PokeUser(0x42, 0); // No delay
	PokeUser(0x42, count); // Number of samples
	m_sequencerDone[1].ResetEvent();
//	QueryPerformanceCounter(&m_startTime);
//	m_time1 = m_time2 = m_time3 = m_time4 = m_startTime;
	SWbDigiFwdDMA params = { 0, count * 2 * sizeof(Ipp16sc), blockCount };
	DWORD dummy;
	VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_PROG_FWD_DMA, &params, sizeof(params), nullptr, 0, dummy));
//	m_timer->Start(m_sequencerTime);
	m_dmaCount[0] += count;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Start collecting a block of samples (single channel - Blackbird only)
//
void C32202001::StartSampling(unsigned int count, unsigned char ch, unsigned int blockCount)
{
	ASSERT(m_type == BLACKBIRD && ch < 2);
	__analysis_assume(ch < 2);

	if(count >= unsigned int(m_bufSize[ch]))
	{
		throw std::runtime_error("DMA buffer too small");
	}

	ASSERT(blockCount > 0 && count / blockCount >= 2);

	if(!(PeekUser(0x61) & (ch == 0 ? 0x0000100 : 0x1000000)))
	{
		TRACE(_T("AGC FIFO not empty\n"));
		ResetThresholdCounters(ch);
	}

	PokeUser(ch == 0 ? 0x44 : 0x46, count);
	m_sequencerDone[ch].ResetEvent();
	SWbDigiFwdDMA params = { ch, count * sizeof(Ipp16sc), blockCount };
	DWORD dummy;
	VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_PROG_FWD_DMA, &params, sizeof(params), nullptr, 0, dummy));
	m_dmaCount[ch] += count / 2;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Start the sequencer
//
void C32202001::StartSequencer(signed char chOrBoth)
{
	if(m_type == BLACKBIRD)
	{
		ASSERT(chOrBoth == -1 || chOrBoth == 1);

		if(m_sequencerLength >= unsigned int(m_bufSize[0]))
		{
			throw std::runtime_error("DMA buffer too small");
		}

		if(!(PeekUser(0x61) & 0x1000100))
		{
			TRACE(_T("AGC FIFO not empty: 0x%08x\n"), PeekUser(0x61));
			ResetThresholdCounters();
		}

		if(chOrBoth == -1)
		{
			m_sequencerDone[0].ResetEvent();
		}

		m_sequencerDone[1].ResetEvent();
	//	QueryPerformanceCounter(&m_startTime);
	//	m_time1 = m_time2 = m_time3 = m_time4 = m_startTime;
		SWbDigiFwdDMA params = { chOrBoth == -1 ? 0x201 : 0x101, m_sequencerLength * sizeof(Ipp16sc),
			std::min(unsigned long(m_sequencerCount), std::max(1ul, unsigned long(1000 * m_sequencerLength / m_sampleRate.Hz<float>()))) };
		DWORD dummy;
		VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_PROG_FWD_DMA, &params, sizeof(params), nullptr, 0, dummy));

		if(chOrBoth == -1)
		{
			m_dmaCount[0] += m_sequencerLength / 2;
		}

		m_dmaCount[1] += m_sequencerLength / 2;
	}
	else
	{
		ASSERT(chOrBoth == -1);

		if(m_sequencerBlockSize * m_sequencerCount >= unsigned int(m_bufSize[0]))
		{
			throw std::runtime_error("DMA buffer too small");
		}

		if(!(PeekUser(0x61) & 0x1000100))
		{
			TRACE(_T("AGC FIFO not empty\n"));
			ResetThresholdCounters();
		}

		m_sequencerDone[1].ResetEvent();
	//	QueryPerformanceCounter(&m_startTime);
	//	m_time1 = m_time2 = m_time3 = m_time4 = m_startTime;
		SWbDigiFwdDMA params = { 0, m_sequencerBlockSize * 2 * sizeof(Ipp16sc) * m_sequencerCount, m_sequencerCount };
		DWORD dummy;
		VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_PROG_FWD_DMA, &params, sizeof(params), nullptr, 0, dummy));
		m_dmaCount[0] += m_sequencerCount * m_sequencerBlockSize;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Start all threads
//
void C32202001::StartThreads(void)
{
		m_shutdown.ResetEvent();
		unsigned long sourceMask[_countof(m_thread)] = { WbDigi_SQF_EOT1_V1 , WbDigi_DMA0_V1 | WbDigi_DMA1_V1, 0, 0 };

		if(m_type == BLACKBIRD)
		{
			sourceMask[0] |= WbDigi_OSQF_EOT0_V1;
			sourceMask[2] |= WbDigi_DMA2_V1;
			sourceMask[3] |= WbDigi_DMA4_V1 | WbDigi_DMA5_V1 | WbDigi_DMA6_V1 | WbDigi_DMA7_V1;
		}

		for(auto i = 0; i < _countof(m_thread); ++i)
		{
			if(sourceMask[i] != 0)
			{
				auto arg = new Utility::SThreadWrapperArg<C32202001, unsigned long>(this, &C32202001::Thread, sourceMask[i], &C32202001::OnFail, "C32202001::Thread");
				m_thread[i] = AfxBeginThread(arg->ThreadWrapper, arg, THREAD_PRIORITY_TIME_CRITICAL, 0, CREATE_SUSPENDED, nullptr);
				m_thread[i]->m_bAutoDelete = FALSE;
				m_thread[i]->ResumeThread();
			}
		}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Stop all threads
//
void C32202001::StopThreads(void)
{
	m_shutdown.SetEvent();

	for(auto i = 0; i < _countof(m_thread); ++i)
	{
		if(m_thread[i] != nullptr)
		{
			if(WaitForSingleObject(m_thread[i]->m_hThread, THREAD_TIMEOUT) == WAIT_TIMEOUT)
			{
				CLog::Log(SMS_SERVICE_FAILED, LOG_CATEGORY_SERVICE, _T("C32202001::StopThreads timed out"));
				_exit(WAIT_TIMEOUT);
			}

			delete m_thread[i];
			m_thread[i] = nullptr;
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Thread function
//
void C32202001::Thread(unsigned long sourceMask)
{
	Utility::SetThreadName("C32202001::Thread");
	OVERLAPPED o;
	memset(&o, 0, sizeof(o));
	auto handle = CreateFile(WBDIGI_DEVICE, GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, nullptr);

	if(handle != INVALID_HANDLE_VALUE && (o.hEvent = CreateEvent(nullptr, TRUE, FALSE, nullptr)) != nullptr)
	{
		HANDLE events[] = { m_shutdown.m_hObject, o.hEvent };
		WbDigi_INTERRUPT data;
		DWORD count;
		bool noOverlapped;
		DWORD event;

		if(DeviceIoControl(handle, IOCTL_WbDigi_WAIT_FOR_INTERRUPT, &sourceMask, sizeof(sourceMask), &data, sizeof(data), &count, &o))
		{
			SetEvent(o.hEvent);
			noOverlapped = true;
		}
		else
		{
			ASSERT(GetLastError() == ERROR_IO_PENDING);
			noOverlapped = false;
		}

		while((event = WaitForMultipleObjects(_countof(events), events, FALSE, INFINITE)) != WAIT_OBJECT_0)
		{
			ASSERT(event ==  WAIT_OBJECT_0 + 1);

			// Interrupt or CancelIo completed
			if(noOverlapped || GetOverlappedResult(handle, &o, &count, TRUE))
			{
				ASSERT(count == sizeof(data) && (data.source & ~sourceMask) == 0);

				switch(data.source)
				{
				case WbDigi_DMA0_V1:
//					QueryPerformanceCounter(time4);
#ifdef DMA_STATS_LOG
					++m_dmaActivityCnt[0];
#endif
					OnSamples(0, data.offset, data.size);
//					TRACE(_T("DMA 0: 0x%08x, 0x%08x\n"), data.offset, data.size);
					break;

				case WbDigi_DMA1_V1:
#ifdef DMA_STATS_LOG
					++m_dmaActivityCnt[1];
#endif
					OnSamples(1, data.offset, data.size);
//					TRACE(_T("DMA 1: 0x%08x, 0x%08x\n"), data.offset, data.size);
					break;

				case WbDigi_DMA2_V1:
#ifdef DMA_STATS_LOG
					++m_dmaActivityCnt[2];
#endif
					OnSamples(2, data.offset, data.size);
//					TRACE(_T("DMA 2: 0x%08x, 0x%08x\n"), data.offset, data.size);
					break;

				case WbDigi_DMA4_V1:
#ifdef DMA_STATS_LOG
					++m_dmaActivityCnt[3];
#endif
					OnProcessed(4, data.offset, data.size);
//					TRACE(_T("DMA 4: 0x%08x, 0x%08x\n"), data.offset, data.size);
					break;

				case WbDigi_DMA5_V1:
#ifdef DMA_STATS_LOG
					++m_dmaActivityCnt[4];
#endif
					OnProcessed(5, data.offset, data.size);
//					TRACE(_T("DMA 5: 0x%08x, 0x%08x\n"), data.offset, data.size);
					break;

				case WbDigi_DMA6_V1:
#ifdef DMA_STATS_LOG
					++m_dmaActivityCnt[5];
#endif
					OnProcessed(6, data.offset, data.size);
//					TRACE(_T("DMA 6: 0x%08x, 0x%08x\n"), data.offset, data.size);
					break;

				case WbDigi_DMA7_V1:
#ifdef DMA_STATS_LOG
					++m_dmaActivityCnt[6];
#endif
					OnProcessed(7, data.offset, data.size);
//					TRACE(_T("DMA 7: 0x%08x, 0x%08x\n"), data.offset, data.size);
					break;

				case WbDigi_OSQF_EOT0_V1:
//					QueryPerformanceCounter(&m_time2);
					m_sequencerDone[0].SetEvent();
					break;

				case WbDigi_SQF_EOT1_V1:
//					QueryPerformanceCounter(&m_time3);
					m_sequencerDone[1].SetEvent();
					break;

				case WbDigi_BLKF_EOT2_V1:
//					QueryPerformanceCounter(&m_time1);
					break;

				default:
					ASSERT(FALSE);
					break;
				}
			}

			// Repost wait
			o.Internal = o.InternalHigh = 0;

			if(DeviceIoControl(handle, IOCTL_WbDigi_WAIT_FOR_INTERRUPT, &sourceMask, sizeof(sourceMask), &data, sizeof(data), &count, &o))
			{
				SetEvent(o.hEvent);
				noOverlapped = true;
			}
			else
			{
				ASSERT(GetLastError() == ERROR_IO_PENDING);
				noOverlapped = false;
			}

			// Ping watchdog
#if _WIN32_WINNT< _WIN32_WINNT_VISTA
			CSingleLock lock(&m_wdCritSect, TRUE);
#else
			CLockGuard lock(m_wdCritSect);
#endif
			PingWatchDog();
		}

		// Shutdown
		CancelIo(handle);
		GetOverlappedResult(handle, &o, &count, TRUE);
		auto source = 0ul;
		Utility::DeviceIoControlSync(handle, IOCTL_WbDigi_WAIT_FOR_INTERRUPT, &source, sizeof(source), &data, sizeof(data), count);
		CloseHandle(o.hEvent);
		CloseHandle(handle);
	}

	return;
	
}


//////////////////////////////////////////////////////////////////////
//
// Tune the DDRs
//
void C32202001::Tune(Units::Frequency freq, bool invertSpectrum, bool shift, unsigned char ch)
{
	ASSERT(ch < NUM_RF_CHANS || (m_type != BLACKBIRD && ch == 2));

	if(m_type != BLACKBIRD && ch == 0)
	{
		ch = 1;
	}

	if(m_master && (freq != m_freq[ch] || invertSpectrum != m_invertSpectrum[ch] || shift != m_shift[ch]))
	{
		m_needRetune = false;
		m_needWait = true;

		if(ch < NUM_RF_CHANS)
		{
			if(m_type == BLACKBIRD)
			{
				PokeUser(ch == 0 ? 0xc5 : 0xc6, CalcFreqWord(freq, m_sampleRate)); // NCO
				unsigned long mix = (invertSpectrum ? 0xa0 : 0x00);

				if(shift)
				{
					mix ^= 0x30;
				}

				if(ch == 0)
				{
					PokeUser(0xa0, mix, 0x000f0);
				}
				else
				{
					mix <<= 8;
					PokeUser(0xa0, mix, 0x0f000);
				}
			}
			else
			{
				PokeUser(0x82, 1); // Channel select
				PokeUser(0x83, CalcFreqWord(freq, m_sampleRate)); // NCO
				unsigned long mix = (invertSpectrum ? 0xa : 0x0);

				if(shift)
				{
					mix ^= 0x3;
				}

				PokeUser(0xa3, mix);
				PokeUser(0x82, 0); // Mix has to be written twice
				PokeUser(0xa3, mix);
			}
		}
		else
		{
			CRegLock lock(m_handle);
			PokeUser(0x82, ch); // Channel select
			PokeUser(0x83, CalcFreqWord(freq, m_sampleRate)); // NCO
		}

		m_freq[ch] = freq;
		m_invertSpectrum[ch] = invertSpectrum;
		m_shift[ch] = shift;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Tune the Blackbird DDRs 
//
void C32202001::Tune(Units::Frequency freq, Units::Frequency sampleRate, unsigned char ch)
{
	ASSERT(m_type == BLACKBIRD && ch > 1);
	CRegLock lock(m_handle);
	PokeUser(0x82, ch < 4 ? ch - 2 : ch); // Wideband DDCs (ch 2 and 3) implemented as channels 0 and 1 of bank 0
	PokeUser(0x83, CalcFreqWord(freq, sampleRate)); // NCO

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Tune the DDRs (ch 2)
//
void C32202001::TuneDemod(Units::Frequency freq, bool invertSpectrum)
{
	ASSERT(m_type != BLACKBIRD);

	if(m_master)
	{
		Tune(freq, invertSpectrum, false, 2);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Unregister a callback
//
void C32202001::UnregisterNewSamplesCallback(unsigned char ch)
{
	ASSERT(ch < NUM_DMAS);
	__analysis_assume(ch < NUM_DMAS);

#if _WIN32_WINNT< _WIN32_WINNT_VISTA
	CSingleLock lock(&m_dataCritSect[ch], TRUE);
#else
	CLockGuard lock(m_dataCritSect[ch]);
#endif
	m_callbacks[ch] = nullptr;
	m_callbackParams[ch] = nullptr;

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Set the sampling rate to be used
//
void C32202001::UseSampleRate(Units::Frequency sampleRate)
{
	ASSERT(m_type == BLACKBIRD ? sampleRate == 153600000 : sampleRate >= 10000000 && sampleRate <= (m_type == SMS_2612 ? 130000000 : 160000000 ));

	if(sampleRate != m_sampleRate)
	{
		m_sampleRate = sampleRate;
		m_needRetune = true;

		if(m_type == SMS_2614)
		{
			PokeUser(0xc7, sampleRate.Hz<unsigned long>());
			PokeUser(0xc9, 0);
			PokeUser(0xca, 0x554);
		}
		else if(m_type == BLACKBIRD)
		{
			PokeUser(0x74, sampleRate.Hz<unsigned long>());
			PokeUser(0x76, 0);
			PokeUser(0x70, 0x40, 0x70);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for enough free space in a DMA buffer
//
void C32202001::WaitForBufferSpace(unsigned int count, unsigned char dma) const
{
	ASSERT(int(count) <= m_bufSize[dma]); // Check buffer big enough
	unsigned int sampleCount[NUM_RF_CHANS];
#if _WIN32_WINNT< _WIN32_WINNT_VISTA
	CSingleLock lock(&m_dataCritSect[dma], TRUE);
	CSingleLock wait(&m_moreBufferSpace);
#else
	CLockGuard lock(m_dataCritSect[dma]);
#endif

	while(m_oldestInUse[dma].size() > 0)
	{
		GetSampleCount(sampleCount);
		unsigned int freeSpace = (m_bufSize[dma] + m_oldestInUse[dma].front().sampleCount - sampleCount[dma]) % m_bufSize[dma];
		unsigned int queuedBlocksSize;

		if(m_oldestInUse[dma].size() == 1)
		{
			queuedBlocksSize = sampleCount[dma] - m_oldestInUse[dma].front().sampleCount;
		}
		else if(m_oldestInUse[dma].size() <= MIN_QUEUED_BLOCKS)
		{
			queuedBlocksSize = m_newestInUse[dma] - m_oldestInUse[dma].front().sampleCount;
		}
		else
		{
			queuedBlocksSize = m_oldestInUse[dma][MIN_QUEUED_BLOCKS].sampleCount - m_oldestInUse[dma].front().sampleCount;
		}

		if(freeSpace >= count && int(freeSpace + queuedBlocksSize) + DMA_PIPELINE_SIZE_LONGLONGS >= m_bufSize[dma])
		{
			// Both enough free space and not too many blocks pending
			break;
		}

		// Need to wait
#if _WIN32_WINNT< _WIN32_WINNT_VISTA
		m_moreBufferSpace.ResetEvent();
		lock.Unlock();
		wait.Lock();
		wait.Unlock();
		lock.Lock();
#else
		m_moreBufferSpace.Sleep(m_dataCritSect[dma]);
#endif
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for the sequencer to complete
//
void C32202001::WaitForSequencer(unsigned char ch)
{
	ASSERT(ch < 2);
	__analysis_assume(ch < 2);
	int priority = GetThreadPriority(GetCurrentThread());
	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST);
	CSingleLock wait(&m_sequencerDone[ch], TRUE);
	SetThreadPriority(GetCurrentThread(), priority);

//	LARGE_INTEGER time;
//	QueryPerformanceCounter(&time);
//	TRACE(_T("%g %g %g %g %g %x\n"), (m_time1.QuadPart - m_startTime.QuadPart) / double(m_perfFreq.QuadPart), (m_time2.QuadPart - m_startTime.QuadPart) / double(m_perfFreq.QuadPart),
//		(m_time3.QuadPart - m_startTime.QuadPart) / double(m_perfFreq.QuadPart), (m_time4.QuadPart - m_startTime.QuadPart) / double(m_perfFreq.QuadPart),
//		(time.QuadPart - m_startTime.QuadPart) / double(m_perfFreq.QuadPart), PeekUser(0x1)); 

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Constructor for helper class
//
C32202001::CRegLock::CRegLock(HANDLE handle)
:
	m_handle(handle)
{
	DWORD count;
	VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_LOCK_Regs, nullptr, 0, nullptr, 0, count));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor for helper class
//
C32202001::CRegLock::~CRegLock(void)
{
	DWORD count;
	VERIFY(Utility::DeviceIoControlSync(m_handle, IOCTL_WbDigi_UNLOCK_Regs, nullptr, 0, nullptr, 0, count));

	return;
}
