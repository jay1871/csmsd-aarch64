/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2008 - 2017  TCI International, Inc. All rights reserved      *
**************************************************************************/

#include "StdAfx.h"

#include "72342011.h"
#include "DownConverter.h"
#include "Units.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
const CDownConverter::BandSettings C72342011::BAND_SETTINGS(BAND_SETTINGS_, &BAND_SETTINGS_[_countof(BAND_SETTINGS_)]);

//06/20/17: 4.5 GHz Ref nominal used Parthi's figure based on
//		    Ukraine unit testing results that has 2nd Generation 7234 switch.
//			These are the numbers that Parthi comes-up from calculation:
//			{  281250000, -29.0, -36.0, 5.0, 0x07 },
//			{ 2250000000, -34.5, -41.0, 8.0, 0x06 },
//			{ 4500000000, -38.5, -44.5, 10.0, 0x05 }
const C72342011::SCalSettings C72342011::CAL_SETTINGS[] =
{
	{  281250000, -29.0, -35.0, 5.0, 0x07 },
	{ 2250000000, -36.0, -41.0, 8.0, 0x06 },
	{ 4500000000, -38.5, -44.0, 10.0, 0x05 }
};

const float C72342011::CAL_LEVEL_SLOPE = -0.020f; // dB / �C
const float C72342011::CONVERTER_GAIN_SLOPE = -0.020f; // dB / �C

// @note: 3rd row is used for mapping, it is NOT A GRAY_CODE for programming 7234 switch.
//	1st row has the gray code for vertical polarized antenna.
//	2nd row has the gray code for horizontal polarized antenna.
//	3rd row of the GRAY_CODE has the mapping of 649 - 8 type 2 low - band
//        5 elements to a 9 inputs switch. TERM is used as input that is not
//        used. The mapping used as followed (switch input -> ant element):
//        1->1,  2->TERM,  3->2,  4->TERM, 5->3,  6->TERM, 7->4,  8->TERM, 9->5.
//		  Therefore 3rd should only be used during Processing, should not be
//		  used for Hardware/Switch setting.
const C72342011::ESource C72342011::GRAY_CODE[C72342011::C_5_ELEMENT_IDX + 1][NUM_ANTS] =
{
	{ ANT_8, ANT_9, ANT_1, ANT_3, ANT_2, ANT_6, ANT_7, ANT_5, ANT_4 },
	{ ANT_8H, ANT_9H, ANT_1H, ANT_3H, ANT_2H, ANT_6H, ANT_7H, ANT_5H, ANT_4H },
	{ TERM, ANT_5, ANT_1, ANT_2, TERM, TERM, ANT_4, ANT_3, TERM },
};

const Units::Frequency C72342011::LO_SETTINGS[] = 
{
			 0,
	4600000000,
	5600000000,
	6400000000,
	7200000000,
	8000000000,
	8800000000,
	9600000000
};

const float C72342011::NOMINAL_TEMP = 25; // �C
const float C72342011::SWITCH_GAIN_SLOPE = 0.008f; // dB / �C

Units::Frequency C72342011::s_crossOverFreq(C72342011::ANT_CROSSOVER_FREQ_HZ);

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C72342011::C72342011(void)
:
	C7236200276342017(C80682017::UHF),
	CDownConverter(),
	m_gainModes()
{
	SetSwitch(0, RECEIVE, TERM, TERM, UHF);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C72342011::~C72342011(void)
{
	SetSwitch(0, RECEIVE, TERM, TERM, UHF);

	return;
}

//////////////////////////////////////////////////////////////////////
/// <summary>
/// Return the expected settle time in usec.
/// </summary>
/// <param name="currCtrlByte">
/// Has the current array [3] of 7234 control byte value.
/// </param>
/// <param name="newCtrlByte">
/// Has the array [3] of 7234 control byte to be output.
/// </param>
/// <returns>
/// Expected settle time in usec.
/// </returns>
inline unsigned int C72342011::CalcSettleTime(_In_reads_(3) const unsigned char(&currCtrlByte)[3],
	_In_reads_(3) const unsigned char(&newCtrlByte)[3])
{
	if( memcmp(newCtrlByte, const_cast<const unsigned char*>(currCtrlByte), sizeof(newCtrlByte)) == 0 )
	{
		return 0;
	}
	else if( (newCtrlByte[2] & 0x03) != 0 && (newCtrlByte[2] & 0x04) != 0 &&
		((currCtrlByte[2] & 0x03) == 0 || (currCtrlByte[2] & 0x04) == 0) )
	{
		// Turning on cal source
		return 40000;
	}
	else if( (newCtrlByte[2] & 0x03) != 0 && (newCtrlByte[2] & 0x04) == 0 &&
		((currCtrlByte[2] & 0x03) == 0 || (currCtrlByte[2] & 0x04) != 0) )
	{
		// Turning on noise source
		return 10000;
	}
	else if( (newCtrlByte[1] & 0x08) != 0 && (currCtrlByte[1] & 0x08) == 0 )
	{
		// Turning on LO
		return 15000;
	}
	else if( ((newCtrlByte[1] ^ currCtrlByte[1]) & 0x07) != 0 && (newCtrlByte[1] & 0x08) != 0 )
	{
		// Change in LO
		return 10000;
	}
	else
	{
		// Default
		return 5;
	}
}

//////////////////////////////////////////////////////////////////////
//
// Get the appropriate band settings
//
size_t C72342011::GetBand(Units::Frequency freq)
{
	for(size_t i = 0; i < _countof(BAND_SETTINGS_); ++i)
	{
		if(freq <= BAND_SETTINGS_[i].high1dbFreq)
		{
			return i;
		}
	}

	return _countof(BAND_SETTINGS_) - 1;
}


//////////////////////////////////////////////////////////////////////
//
// Get the closest cal settings
//
float C72342011::GetCalDbm(size_t index, size_t ch)
{
	return index < _countof(CAL_SETTINGS) ? (ch == 0 ? CAL_SETTINGS[index].refDbm : CAL_SETTINGS[index].sampleDbm) : 0;
}

//////////////////////////////////////////////////////////////////////
//
// Get the closest cal settings
//
Units::Frequency C72342011::GetCalFreq(size_t index)
{
	return index < _countof(CAL_SETTINGS) ? CAL_SETTINGS[index].freq : 0;
}

//////////////////////////////////////////////////////////////////////
/// <summary>
/// Get the half range dBm value in passing BIST test.
/// </summary>
/// <param name="index">
/// Frequency index.
/// </param>
/// <returns>
/// 10.0 dBm if the index is incorrect.
/// Otherwise the value in CAL_SETTINGS is used.
/// </returns>
float C72342011::GetCalHalfRangeDbm(size_t index)
{
	return index < _countof(CAL_SETTINGS) ? CAL_SETTINGS[index].passRangeDbm : 10.0f;
}

//////////////////////////////////////////////////////////////////////
//
// Get the closest cal settings
//
_Ret_ const C72342011::SCalSettings& C72342011::GetCalSettings(Units::Frequency calFreq, bool useShf)
{
	size_t i;

	for(i = 0; i < _countof(CAL_SETTINGS) - 1 && (useShf || CAL_SETTINGS[i].freq < BAND_SETTINGS[0].high1dbFreq); ++i)
	{
		if(calFreq.Hz<double>() < sqrt(CAL_SETTINGS[i].freq.Hz<double>() * CAL_SETTINGS[i + 1].freq.Hz<double>()))
		{
			return CAL_SETTINGS[i];
		}
	}

	//Do not use 4.5 GHz BITE tone if useShf == false.
	if( !useShf && (i == _countof(CAL_SETTINGS) - 1) )
	{
		return CAL_SETTINGS[i-1];
	}

	return CAL_SETTINGS[i];
}


//////////////////////////////////////////////////////////////////////
//
// Get the control bytes
//
void C72342011::GetControlBytes(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf,
					 _Out_writes_all_(3) unsigned char (&controlBytes)[3], EDfElemBandSel band)
{
	const SCalSettings& calSettings = GetCalSettings(freq, true);

	// Setting the block converter and cband_sw is currently independent of polarization.
	controlBytes[1] = unsigned char(BAND_SETTINGS[GetBand(freq)].data);

#ifdef _DEBUG
	bool	isHorizontal = false;
#endif
	// TODO: This is trying to guess polarization.  Would be better to add additional argument to function for polarization.
	if( ((sampleRf <= ANT_9H) && (sampleRf >= ANT_1H)) || refRf == REF_HORIZON)
	{
		//Horizontal polarization is not supported by SHF antenna array.
		band = UHF;
#ifdef _DEBUG
		isHorizontal = true;
#endif
	}
	else
	{
		//Vertical polarization can be measured by either UHF or SHF antenna array.
		if(band == AUTO)
		{
			band = (freq <= s_crossOverFreq ? UHF : SHF);
		}

		if(band == SHF)
		{
			controlBytes[1] |= B_SHF;  // c-band array element select
		}
	}
	switch(mode)
	{
		case RECEIVE:
		{
			//Default is to use vertical polarization.
			unsigned char pol = B_VERTICAL_POL;

			switch(refRf)
			{
				case TERM:
					ASSERT((sampleRf != BITE) && (sampleRf != NOISE));
					controlBytes[2] = C_TERMINATED_REF_SELECTION;
#ifdef _DEBUG
					if( isHorizontal )
					{
						pol = C_HORIZONTAL_VAL;
					}
#endif
					break;

				case REF_HORIZON:
					//Save the Horizontal polarization.
					pol = C_HORIZONTAL_VAL;
					// Fall through
				case REF:
					controlBytes[2] = C_REFOUT_2_REF_ANTENNA;
					break;

				case BITE:
					controlBytes[2] = calSettings.control | B_CAL_2_REF;
#ifdef _DEBUG
					if( isHorizontal )
					{
						pol = C_HORIZONTAL_VAL;
					}
#endif
					break;

				case NOISE:
					controlBytes[2] = C_REFOUT_2_THERMAL_NOISE;
#ifdef _DEBUG
					if( isHorizontal )
					{
						pol = C_HORIZONTAL_VAL;
					}
#endif
					break;

				default:
					ASSERT(FALSE);
					//Connect Reference to Terminated for unknown mode.
					controlBytes[2] = C_TERMINATED_REF_SELECTION;
					break;
			}

			switch(sampleRf)
			{
				case ANT_1:
				case ANT_2:
				case ANT_3:
				case ANT_4:
				case ANT_5:
				case ANT_6:
				case ANT_7:
				case ANT_8:
				case ANT_9:
					//Assert if Ref polarization does not match sample.
					ASSERT (pol == B_VERTICAL_POL);
					controlBytes[0] = static_cast<unsigned char>(sampleRf);
					break;

				case ANT_1H:
				case ANT_2H:
				case ANT_3H:
				case ANT_4H:
				case ANT_5H:
				case ANT_6H:
				case ANT_7H:
				case ANT_8H:
				case ANT_9H:
					//Assert if Ref polarization does not match sample.
					ASSERT (pol == C_HORIZONTAL_VAL);
					controlBytes[0] = static_cast<unsigned char>(sampleRf);
					break;

				case TERM:
					controlBytes[0] = pol | C_TERMINATED_SAMPLE_SELECTION;
					break;

				case NOISE:
					ASSERT(refRf != BITE);
					controlBytes[0] = pol | C_CAL_OR_NOISE_SAMPLE_SELECTION;
					controlBytes[2] |= C_NOISE_ON;
					break;

				case BITE:
					ASSERT(refRf != NOISE);
					controlBytes[0] = pol | C_CAL_OR_NOISE_SAMPLE_SELECTION;
					controlBytes[2] |= calSettings.control;
					break;

				default:
					ASSERT(FALSE);
					//Undefine sample selection set it to Terminated.
					controlBytes[0] = pol | C_TERMINATED_SAMPLE_SELECTION;
					break;
			}
			break;
		}

		case TX_BITE:
		{
			//Default is to use vertical polarization.
			unsigned char pol = B_VERTICAL_POL;
#ifdef _DEBUG
			bool	dontChk = true;
#endif
			switch(refRf)
			{
				case TERM:
					controlBytes[2] = calSettings.control | B_CAL_2_ANTENNA;  // Much leakage as Ref is floating not terminated.
					break;

				case REF_HORIZON:
					pol = C_HORIZONTAL_VAL;
					//fall through.
				case REF:
					ASSERT(FALSE);
					controlBytes[2] = calSettings.control | B_CAL_2_ANTENNA;  // Ref terminated, much leakage as Ref is floating not terminated.
#ifdef _DEBUG
					dontChk = false;
#endif
					break;

				case BITE:
					controlBytes[2] = calSettings.control | B_CAL_2_ANTENNA | B_CAL_2_REF;  // Via switch leakage
					break;

				case NOISE:
					ASSERT(FALSE);
					controlBytes[2] = calSettings.control | B_CAL_2_ANTENNA;  // Ref terminated, much leakage as Ref is floating not terminated.
					break;

				default:
					ASSERT(FALSE);
					//Undefine Ref setting, set it to CAL tone.
					controlBytes[2] = calSettings.control | B_CAL_2_ANTENNA | B_CAL_2_REF;
					break;
			}

			switch(sampleRf)
			{
				case ANT_1:
				case ANT_2:
				case ANT_3:
				case ANT_4:
				case ANT_5:
				case ANT_6:
				case ANT_7:
				case ANT_8:
				case ANT_9:
					//Assert if Ref polarization does not match sample.
					ASSERT (dontChk || pol == B_VERTICAL_POL);
					controlBytes[0] = static_cast<unsigned char>(sampleRf);
					break;

				case ANT_1H:
				case ANT_2H:
				case ANT_3H:
				case ANT_4H:
				case ANT_5H:
				case ANT_6H:
				case ANT_7H:
				case ANT_8H:
				case ANT_9H:
					//Assert if Ref polarization does not match sample.
					ASSERT (dontChk || pol == C_HORIZONTAL_VAL);
					controlBytes[0] = static_cast<unsigned char>(sampleRf);
					break;

				case TERM:
					controlBytes[0] = pol | C_TERMINATED_SAMPLE_SELECTION;
					break;

				case NOISE:
					ASSERT(FALSE);
					controlBytes[0] = pol | C_TERMINATED_SAMPLE_SELECTION;  // Sample terminated
					break;

				case BITE:
					ASSERT(FALSE);
					controlBytes[0] = pol | C_TERMINATED_SAMPLE_SELECTION;  // Sample terminated
					break;

				default:
					ASSERT(FALSE);
					//Undefine sample selection set it to Terminated.
					controlBytes[0] = pol | C_TERMINATED_SAMPLE_SELECTION;
					break;
			}

			break;
		}
		default:
			ASSERT(FALSE);
			//Undefine reference Antenna mode, set sample and Ref to Terminated.
			controlBytes[0] = B_VERTICAL_POL | C_TERMINATED_SAMPLE_SELECTION;
			controlBytes[2] = C_TERMINATED_REF_SELECTION;
			break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the RF frequencies corresponding to a pair of receiver frequencies
//
Units::FreqPair C72342011::GetRfFreqs(Units::Frequency rfFreq, _In_ const Units::FreqPair& rxFreqs)
{
	Units::Frequency loFreq = LO_SETTINGS[GetBand(rfFreq)];
	return loFreq == 0 ? rxFreqs : Units::FreqPair(loFreq - rxFreqs.second, loFreq - rxFreqs.first);
}


//////////////////////////////////////////////////////////////////////
//
// Get the receiver frequency corresponding to an RF frequency
//
Units::Frequency C72342011::GetRxFreq(Units::Frequency rfFreq)
{
	Units::Frequency loFreq = LO_SETTINGS[GetBand(rfFreq)];
	return loFreq == 0 ? rfFreq : loFreq - rfFreq;
}


//////////////////////////////////////////////////////////////////////
//
// Get the settle time for a switch change
//
unsigned int C72342011::GetSettleTime(_In_reads_(3) const unsigned char (&controlBytes)[3]) const
{
	CSingleLock lock(&m_critSect, TRUE);
	unsigned char currCtrlBytes[3];
	currCtrlBytes[0] = m_controlBytes[0]; 
	currCtrlBytes[1] = m_controlBytes[1];
	currCtrlBytes[2] = m_controlBytes[2];
	lock.Unlock();
	return(CalcSettleTime(currCtrlBytes, controlBytes));
}


//////////////////////////////////////////////////////////////////////
//
// Set the switch
//
void C72342011::SetSwitch(Units::Frequency freq, ERefMode mode, ESource refRf, ESource sampleRf, EDfElemBandSel band)
{
	unsigned char controlBytes[3];
	GetControlBytes(freq, mode, refRf, sampleRf, controlBytes, band);
	SetSettleTime(GetSettleTime(controlBytes));
	C7236200276342017::SetSwitch(controlBytes, false);

	return;
}
