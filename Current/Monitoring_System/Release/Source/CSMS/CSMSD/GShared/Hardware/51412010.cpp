/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013-2014 TCI International, Inc. All Rights reserved         *
**************************************************************************/

#include "StdAfx.h"

#include <memory>

#include "51412010.h"
#include "DownConverter.h"
#include "Units.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
const CDownConverter::BandSettings C51412010::BAND_SETTINGS(BAND_SETTINGS_, &BAND_SETTINGS_[_countof(BAND_SETTINGS_)]);
const float C51412010::CONVERTER_GAIN_SLOPE = -0.020f; // dB / �C

const Units::Frequency C51412010::LO_SETTINGS[] = 
{
			 0,
	4600000000,
	5600000000,
	6400000000,
	7200000000,
	8000000000,
	8800000000,
	9600000000
};

LPCTSTR C51412010::MUTEX_NAME = _T("Global\\TCI.{F4B3F554-E654-454B-BAC0-1E175D600F61}"); // Intentionally same GUID as 26222001 which shares same control register
const float C51412010::NOMINAL_TEMP = 25; // �C

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C51412010::C51412010(void)
:
	CDownConverter(),
	m_gainModes(),
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get())
{
	m_first = !(GetLastError() == ERROR_ALREADY_EXISTS); // Status from m_mutex construction

	if(m_first)
	{
		Tune(0);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C51412010::~C51412010(void)
{
	if(m_first)
	{
		Tune(0);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the appropriate band settings
//
size_t C51412010::GetBand(Units::Frequency freq)
{
	for(size_t i = 0; i < _countof(BAND_SETTINGS_); ++i)
	{
		if(freq <= BAND_SETTINGS_[i].high1dbFreq)
		{
			return i;
		}
	}

	return _countof(BAND_SETTINGS_) - 1;
}


//////////////////////////////////////////////////////////////////////
//
// Get the control byte
//
unsigned char C51412010::GetControlByte(Units::Frequency freq)
{
	return unsigned char(BAND_SETTINGS[GetBand(freq)].data);
}


//////////////////////////////////////////////////////////////////////
//
// Get the RF frequencies corresponding to a pair of receiver frequencies
//
Units::FreqPair C51412010::GetRfFreqs(Units::Frequency rfFreq, _In_ const Units::FreqPair& rxFreqs)
{
	Units::Frequency loFreq = LO_SETTINGS[GetBand(rfFreq)];
	return loFreq == 0 ? rxFreqs : Units::FreqPair(loFreq - rxFreqs.second, loFreq - rxFreqs.first);
}


//////////////////////////////////////////////////////////////////////
//
// Get the receiver frequency corresponding to an RF frequency
//
Units::Frequency C51412010::GetRxFreq(Units::Frequency rfFreq)
{
	Units::Frequency loFreq = LO_SETTINGS[GetBand(rfFreq)];
	return loFreq == 0 ? rfFreq : loFreq - rfFreq;
}


//////////////////////////////////////////////////////////////////////
//
// Get the settle time for a switch change
//
unsigned int C51412010::GetSettleTime(unsigned char controlByte) const
{
	unsigned char currentControlByte = GetCurrentControlByte();

	if((controlByte & 0x38) != 0 && (currentControlByte & 0x38) == 0)
	{
		// Turning on LO
		return 15000;
	}
	else if(((controlByte ^ currentControlByte) & 0x38) != 0 && (controlByte & 0x38) != 0)
	{
		// Change in LO
		return 10000;
	}
	else
	{
		// Default
		return 5;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Set the frequency band
//
void C51412010::Tune(Units::Frequency freq)
{
	unsigned char controlByte = GetControlByte(freq);
	SetSettleTime(GetSettleTime(controlByte));
	SetBand(controlByte);

	return;
}
