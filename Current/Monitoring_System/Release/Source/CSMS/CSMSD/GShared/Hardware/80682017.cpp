/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "StdAfx.h"

#include <algorithm>
#include <math.h>
#include <memory>

#include "80682017.h"
#include "Timer.h"
#include "Vme.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
//
// Static data
//
const float C80682017::IF_MUX_LOSS_HF = 2; // dB
const float C80682017::IF_MUX_LOSS_UHF = 3; // dB
LPCTSTR C80682017::MUTEX_NAME = _T("Global\\TCI.{B93E439D-098E-478a-B362-0D6322A7AF8B}.Mutex");
LPCTSTR C80682017::SHMEM_NAME = _T("Global\\TCI.{B93E439D-098E-478a-B362-0D6322A7AF8B}.ShMem");
C80682017* C80682017::m_instance = nullptr;


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
C80682017::C80682017(void)
:
	CVme(),
	m_mutex(FALSE, MUTEX_NAME, std::unique_ptr<SECURITY_ATTRIBUTES_ALL_ACCESS>(new SECURITY_ATTRIBUTES_ALL_ACCESS).get()),
	m_usingPoe(true)
{
	ASSERT(m_instance == nullptr);
	m_instance = this;

	// Only supports board ID 0
	m_address = VME_BASE_ADDR_POE;
	m_poeHeadType[0] = m_poeHeadType[1] = m_poeHeadType[2] = UNKNOWN;
	m_present = ((Peek(STATUS_REG) & 0x00ffu) == (((CARD_CLASS & 0x3) << 6) | ((CARD_TYPE_POE & 0x7) << 3)));

	for( unsigned i = 0; i < NUM_POE_PORT_TYPE; ++i )
	{
		m_remoteVersion[i] = 0;
		m_remoteAntType[i] = 0;
	}
	
	if(!m_present)
	{
		// Look for -31 version
		m_address = VME_BASE_ADDR_PARALLEL;
		m_present = ((Peek(STATUS_REG) & 0x00ffu) == (((CARD_CLASS & 0x3) << 6) | ((CARD_TYPE_PARALLEL & 0x7) << 3)));
		m_usingPoe = false;
	}

	CSingleLock lock(&m_mutex, TRUE);
	m_first = GetSharedMemory<SShMem>(SHMEM_NAME, m_shMem);

	if(m_present)
	{
		SCardStatus status = GetCardStatus();
		m_version = status.version;

		if(m_first)
		{
			// CPU LED slow flash
			m_shMem->statusReg = 0x0002;

			// Set mux to defaults
			m_shMem->muxControlReg = 0x0063;

			// Set VME control to defaults
			m_shMem->uhfSwitchReg = 0x0000;
			m_shMem->vhfSwitchReg = 0x0000;
			m_shMem->hfSwitchReg = 0x0000;

			// Initially turn on all LEDs and turn off A/Ds
			if(m_version <= 1)
			{
				m_shMem->ledPowerReg = 0x0c3c;
			}
			else
			{
				m_shMem->ledPowerReg = 0x083c;
			}

			WriteShMemRegs();
		}

		// Read the voltages to setup the ADCs
		float voltages[8];
		bool no48V;
		GetVoltages(voltages, no48V);

		if(m_usingPoe)
		{
			// Get remote status
			GetRemoteStatus(UHF);
			GetRemoteStatus(VHF);
			GetRemoteStatus(HF);

			// Setup handler for interrupt
			RegisterInterruptHandler(GEF_VME_INT_VIRQ5, On80682017Interrupt, this, ((CARD_CLASS & 0x3) << 6) | (CARD_TYPE_POE << 3));
		}
		else
		{
			// Turn off 48V
			Set48V(false);

			// Setup handler forinterrupt
			RegisterInterruptHandler(GEF_VME_INT_VIRQ5, On80682017Interrupt, this, ((CARD_CLASS & 0x3) << 6) | (CARD_TYPE_PARALLEL << 3));
		}
	}

	return;
}



//////////////////////////////////////////////////////////////////////
//
// Destructor
//
C80682017::~C80682017(void)
{
	m_instance = nullptr;

	if(m_present)
	{
		CSingleLock lock(&m_mutex, TRUE);

		if(m_first)
		{
			// Turn off all LEDs
			Poke(LED_POWER_REG, 0x0c03);
		}

		UnregisterInterruptHandler(GEF_VME_INT_VIRQ5, On80682017Interrupt, ((CARD_CLASS & 0x3) << 6) | ((m_usingPoe ? CARD_TYPE_POE : CARD_TYPE_PARALLEL ) << 3));
		FreeSharedMemory(m_shMem);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Convert 12-bit A/D data to temperature
//
float C80682017::adc2temp(unsigned short adc)
{
	static const struct
	{
		unsigned short adc;
		float temp;
	} LOOKUP[] = 
	{
		{   43, -50 },
		{   61, -45 },
		{   86, -40 },
		{  118, -35 },
		{  161, -30 },
		{  215, -25 },
		{  283, -20 },
		{  369, -15 },
		{  472, -10 },
		{  597,  -5 },
		{  741,   0 },
		{  907,   5 },
		{ 1090,  10 },
		{ 1290,  15 },
		{ 1500,  20 },
		{ 1717,  25 },
		{ 1936,  30 },
		{ 2150,  35 },
		{ 2356,  40 },
		{ 2552,  45 },
		{ 2733,  50 },
		{ 2897,  55 },
		{ 3046,  60 },
		{ 3180,  65 },
		{ 3297,  70 },
		{ 3399,  75 },
		{ 3487,  80 },
		{ 3568,  85 },
		{ 3633,  90 },
		{ 3912, 125 }
	};

	// Binary search and interpolation
	float temp = 0;
	int low = 0;
	int high = _countof(LOOKUP) - 1;

	while(low <= high)
	{
		int mid = (low + high) / 2;

		if(LOOKUP[mid].adc > adc)
		{
			high = mid - 1;
		}
		else if(LOOKUP[mid].adc < adc)
		{
			low = mid + 1;
		}
		else
		{
			temp = LOOKUP[mid].temp;
			break;
		}
	}

	if(high == -1)
	{
		high = 0;
		low = 1;
	}
	else if(low == _countof(LOOKUP))
	{
		high = _countof(LOOKUP) - 2;
		low = _countof(LOOKUP) - 1;
	}

	if(high < low)
	{
		temp = LOOKUP[high].temp + (LOOKUP[low].temp - LOOKUP[high].temp) * (adc - LOOKUP[high].adc) /
			(LOOKUP[low].adc - LOOKUP[high].adc);
	}

	return temp;
}


//////////////////////////////////////////////////////////////////////
/// <summary>
/// Get the antenna loopback data
/// </summary>
/// <param name="poePort">
/// Identify the 8068-2017 Switch control register. Its value is the hardware
///  specified function register * 2.
/// </param>
/// <returns>
/// 0: For any error condition (antenna in error/not ready/not present).
/// 2nd generation POE Module, the following 32 bits definition will be returned:
///  AC10 - AC15 (Bit 0 - 5).
///  AC20 - AC25 (Bit 6 - 11).
///  AC30 - AC35 (Bit 12 - 17).
///  Bit 18 - 31 are reserved and cleared.
/// 1st generation POE Module, the following 32 bits definition will be returned:
///  TODO 2: (Bit 0 - 7) value read from mode 0 byte.
///  Bit 8 - 31 are reserved and cleared.
/// </returns>
/// <remarks>
/// This method issues status word request to the Antenna module.
/// It returns the 3 Antenna Control Readback value (6 bits each).
/// </remarks>
unsigned int C80682017::GetAntennaData(EPoePort poePort) const
{
	ASSERT(poePort == UHF || poePort == VHF || poePort == HF);

	if(!m_present || !m_usingPoe)
	{
		return 0;
	}

	SCardStatus status = GetCardStatus();

	if(poePort == UHF && status.uhfPoeFault || poePort == VHF && status.vhfPoeFault || poePort == HF && status.hfPoeFault)
	{
		return 0;
	}

	CSingleLock lock(&m_mutex, TRUE);
	CTimer timer(CTimer::SHORT);

	unsigned char reg = (poePort == UHF ? UHF_SWITCH_REG : (poePort == VHF ? VHF_SWITCH_REG : HF_SWITCH_REG));
	unsigned short switchData = (poePort == UHF ? m_shMem->uhfSwitchReg : (poePort == VHF ? m_shMem->vhfSwitchReg : m_shMem->hfSwitchReg)) & 0x00ff;
	C80682017* mutableThis = const_cast<C80682017*>(this);

	// Mode 1
	mutableThis->Poke(reg, switchData | 0x0100);
	timer.Delay(10);
	unsigned short rawData[3];
	rawData[0] = Peek(reg);
	timer.Delay(10);
	rawData[1] = Peek(reg);
	timer.Delay(10);
	rawData[2] = Peek(reg);
	unsigned short data = Filter(rawData);

	if(data & 0x4000)
	{
		// 7236-2002 or 7234 head

		// Mode 2 (loopback data)
		mutableThis->Poke(reg, switchData | 0x0200);
		timer.Delay(10);
		rawData[0] = Peek(reg);
		timer.Delay(10);
		rawData[1] = Peek(reg);
		timer.Delay(10);
		rawData[2] = Peek(reg);
		data = Filter(rawData);

		// Mode 8 (remainder of loopback data and whip status
		mutableThis->Poke(reg, switchData | 0x0800);
		timer.Delay(10);
		rawData[0] = Peek(reg) & 0x000f;
		timer.Delay(10);
		rawData[1] = Peek(reg) & 0x000f;
		timer.Delay(10);
		rawData[2] = Peek(reg) & 0x000f;
		mutableThis->Poke(reg, switchData);

		return (unsigned int(Filter(rawData) & 0x0003) << 16) | unsigned int(data);
	}
	else
	{
		// 7641-1017 head

		// Mode 0 (loopback data)
		mutableThis->Poke(reg, switchData | 0x0000);
		timer.Delay(10);
		rawData[0] = Peek(reg);
		timer.Delay(10);
		rawData[1] = Peek(reg);
		timer.Delay(10);
		rawData[2] = Peek(reg);
		data = Filter(rawData);

		return data & 0x00ff;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the IF mux attenuation
//
unsigned char C80682017::GetAtten(void) const
{
	if(m_present)
	{
		CSingleLock lock(&m_mutex, TRUE);
		return unsigned char(6 * (((m_shMem->muxControlReg & 0x000c) ^ 0x000c) >> 2));
	}
	else
	{
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the card status
//
C80682017::SCardStatus C80682017::GetCardStatus(void) const
{
	unsigned short reg = Peek(STATUS_REG);

	union
	{
		unsigned short reg;
		SCardStatus status;
	} status = { reg };

	return status.status;
}


//////////////////////////////////////////////////////////////////////
//
// Get the bypass gain
//
float C80682017::GetGainRatio(void) const
{
	return pow(10.0f, (MAX_GAIN - GetAtten()) / 10.0f);
}


//////////////////////////////////////////////////////////////////////
//
// Get the IF mux state
//
void C80682017::GetIfMux(_Out_ bool& hf, _Out_ bool& lowBand) const
{
	if(m_present)
	{
		CSingleLock lock(&m_mutex, TRUE);
		hf = (m_shMem->muxControlReg & 0x0001) == 0;
		lowBand = (m_shMem->muxControlReg & 0x0002) == 0;
	}
	else
	{
		hf = false;
		lowBand = false;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the remote status from a POE head
//
C80682017::SRemoteStatus C80682017::GetRemoteStatus(EPoePort poePort)
{
	ASSERT(poePort == UHF || poePort == VHF || poePort == HF);
	SRemoteStatus remote;
	SCardStatus status = GetCardStatus();

	if(!m_present || !m_usingPoe ||
		poePort == UHF && status.uhfPoeFault || poePort == VHF && status.vhfPoeFault || poePort == HF && status.hfPoeFault)
	{
		remote.headType = UNKNOWN;
		remote.version = 0;
		remote.antType = 0;
		remote.loopback = 0x00000000;
		remote.powerAlarmOr10MHzOk = false;
		remote.whipPresentOrLoOk = false;
		remote.voltages[0] = 0;
		remote.voltages[1] = 0;
		remote.voltages[2] = 0;
		remote.temp = 0;

		return remote;
	}

	CSingleLock lock(&m_mutex, TRUE);
	CTimer timer(CTimer::SHORT);

	unsigned char reg = (poePort == UHF ? UHF_SWITCH_REG : (poePort == VHF ? VHF_SWITCH_REG : HF_SWITCH_REG));
	unsigned short switchData =
		(poePort == UHF ? m_shMem->uhfSwitchReg : (poePort == VHF ? m_shMem->vhfSwitchReg : m_shMem->hfSwitchReg)) & 0x00ff;

	// Mode 1 (version)
	Poke(reg, switchData | 0x0100);
	timer.Delay(10);
	unsigned short rawData[3];
	rawData[0] = Peek(reg);
	timer.Delay(10);
	rawData[1] = Peek(reg);
	timer.Delay(10);
	rawData[2] = Peek(reg);
	unsigned short data = Filter(rawData);

	if((data & 0x4000) != 0)
	{
		// 7236-2002 or 7647-2017 head
		remote.antType = unsigned char(data & 0x00ff);
		remote.version = unsigned char((data & 0x0f00) >> 8);
		remote.headType = EPoeHeadType((data & 0xf000) >> 12);

		// Mode 2 (loopback data)
		Poke(reg, switchData | 0x0200);
		timer.Delay(10);
		rawData[0] = Peek(reg);
		timer.Delay(10);
		rawData[1] = Peek(reg);
		timer.Delay(10);
		rawData[2] = Peek(reg);
		remote.loopback = Filter(rawData);

		// Mode 8 (remainder of loopback data and whip or block downconverter status
		Poke(reg, switchData | 0x0800);
		timer.Delay(10);
		rawData[0] = Peek(reg) & 0x000f;
		timer.Delay(10);
		rawData[1] = Peek(reg) & 0x000f;
		timer.Delay(10);
		rawData[2] = Peek(reg) & 0x000f;
		data = Filter(rawData);
		remote.loopback |= (unsigned int(data & 0x0003) << 16);

		if(remote.headType == USHF_76472017)
		{
			 // 7647-2017
			remote.powerAlarmOr10MHzOk = ((data & 0x0008) != 0);
			remote.whipPresentOrLoOk = ((data & 0x0004) != 0);
		}
		else
		{
			 // 7236-2002
			remote.powerAlarmOr10MHzOk = ((data & 0x0008) != 0);
			remote.whipPresentOrLoOk = remote.powerAlarmOr10MHzOk || ((data & 0x0004) == 0);
		}

		// Mode 4 (+12V)
		Poke(reg, switchData | 0x0400);
		timer.Delay(500);
		rawData[0] = Peek(reg) & 0x1fff;
		timer.Delay(10);
		rawData[1] = Peek(reg) & 0x1fff;
		timer.Delay(10);
		rawData[2] = Peek(reg) & 0x1fff;
		data = Filter(rawData);
		remote.voltages[0] = (data & 0x0fff) * 18.05f / 4096;

		// Mode 36 (+5V)
		Poke(reg, switchData | 0x2400);
		timer.Delay(500);
		rawData[0] = Peek(reg) & 0x1fff;
		timer.Delay(10);
		rawData[1] = Peek(reg) & 0x1fff;
		timer.Delay(10);
		rawData[2] = Peek(reg) & 0x1fff;
		data = Filter(rawData);
		remote.voltages[1] = (data & 0x0fff) * 5.50f / 4096;

		// Mode 68 (temp)
		Poke(reg, switchData | 0x4400);
		timer.Delay(500);
		rawData[0] = Peek(reg) & 0x1fff;
		timer.Delay(10);
		rawData[1] = Peek(reg) & 0x1fff;
		timer.Delay(10);
		rawData[2] = Peek(reg) & 0x1fff;
		data = Filter(rawData);
		remote.temp = adc2temp(data & 0x0fff);

		if(remote.headType == USHF_76472017)
		{
			// Mode 100 (+8V)
			Poke(reg, switchData | 0x6400);
			timer.Delay(500);
			rawData[0] = Peek(reg) & 0x1fff;
			timer.Delay(10);
			rawData[1] = Peek(reg) & 0x1fff;
			timer.Delay(10);
			rawData[2] = Peek(reg) & 0x1fff;
			data = Filter(rawData);
			remote.voltages[2] = (data & 0x0fff) * 10.00f / 4096;
		}
		else
		{
			remote.voltages[2] = 0;
		}

		// Careful not to put ADC into sleep mode
		Poke(reg, switchData | 0x0400);
		timer.Delay(10);
		Poke(reg, switchData);
	}
	else
	{
		// 7641-2017 head
		remote.headType = VUHF_76412017;
		remote.whipPresentOrLoOk = false;

		if((data & 0x0700) == 0x0100)
		{
			remote.antType = unsigned char(data & 0x00ff);
			remote.version = unsigned char((data & 0x3800) >> 11);
		}
		else
		{
			remote.antType = 0;
			remote.version = 0;
		}

		// Mode 0 (loopback)
		Poke(reg, switchData | 0x0000);
		timer.Delay(10);
		rawData[0] = Peek(reg);
		timer.Delay(10);
		rawData[1] = Peek(reg);
		timer.Delay(10);
		rawData[2] = Peek(reg);
		data = Filter(rawData);

		if((rawData[0] & 0x0700) == 0x0000)
		{
			remote.loopback = (data & 0xff);
			remote.powerAlarmOr10MHzOk = ((data & 0x8000) ? true : false);
		}
		else
		{
			remote.loopback = 0x00000000;
			remote.powerAlarmOr10MHzOk = false;
		}


		// Mode 3 (+15V)
		Poke(reg, switchData | 0x0300);
		timer.Delay(500);
		rawData[0] = Peek(reg);
		timer.Delay(10);
		rawData[1] = Peek(reg);
		timer.Delay(10);
		rawData[2] = Peek(reg);
		data = Filter(rawData);

		if((data & 0x0700) == 0x0300)
		{
			remote.voltages[0] = (data & 0x00ff) * 18.05f / 256;
		}
		else
		{
			remote.voltages[0] = 0;
		}

		// Mode 4 (-12V)
		Poke(reg, switchData | 0x0400);
		timer.Delay(500);
		rawData[0] = Peek(reg);
		timer.Delay(10);
		rawData[1] = Peek(reg);
		timer.Delay(10);
		rawData[2] = Peek(reg);
		data = Filter(rawData);

		if((data & 0x0700) == 0x0400)
		{
			remote.voltages[1] = (data & 0x00ff) * 24.6f / 256 - 5 * 3.92f;
		}
		else
		{
			remote.voltages[1] = 0;
		}

		remote.voltages[2] = 0;

		// Mode 5 (temp)
		Poke(reg, switchData | 0x0500);
		timer.Delay(500);
		rawData[0] = Peek(reg);
		timer.Delay(10);
		rawData[1] = Peek(reg);
		timer.Delay(10);
		rawData[2] = Peek(reg);
		data = Filter(rawData);

		if((data & 0x0700) == 0x0500)
		{
			remote.temp = adc2temp((data & 0x00ff) << 4);
		}
		else
		{
			remote.temp = 0;
		}
	}

	m_poeHeadType[poePort] = remote.headType;
	m_remoteAntType[poePort] = remote.antType;
	m_remoteVersion[poePort] = remote.version;

	// Back to mode 0
	Poke(reg, switchData);

	return remote;
}


//////////////////////////////////////////////////////////////////////
//
// Get the power supply voltages
//
void C80682017::GetVoltages(_Out_writes_all_(8) float(&voltages)[8], _Out_ bool& no48V) const
{
	if(!m_present)
	{
		for(unsigned short sel = 0; sel < 8; ++sel)
		{
			voltages[sel] = 0;
		}

		return;
	}

	CSingleLock lock(&m_mutex, TRUE);
	no48V = ((m_shMem->muxControlReg & 0x0080) == 0x0080);
	CTimer timer(CTimer::SHORT);
	C80682017* mutableThis = const_cast<C80682017*>(this);

	if(m_version <= 1)
	{
		// Need to turn on the test tone generator
		mutableThis->Poke(MUX_CONTROL_REG, m_shMem->muxControlReg | 0x0010);
	}
	else
	{
		// Need to enable the A/D clock
		mutableThis->Poke(LED_POWER_REG, m_shMem->ledPowerReg | 0x0400);
	}

	timer.Delay(10);
	unsigned short data = (m_shMem->ledPowerReg & 0x03ff);
	unsigned short adc[8][3];

	for(unsigned short sel = 0; sel < 8; ++sel)
	{
		for(size_t i = 0; i < 3; ++i)
		{
			if(m_version <= 1)
			{
				mutableThis->Poke(LED_POWER_REG, data | (sel << 13) | 0x0800);
				timer.Delay(5);
				mutableThis->Poke(LED_POWER_REG, data | (sel << 13) | 0x0000);
				timer.Delay(5);
				mutableThis->Poke(LED_POWER_REG, data | (sel << 13) | 0x0400);
				timer.Delay(5);
				mutableThis->Poke(LED_POWER_REG, data | (sel << 13) | 0x0000);
				timer.Delay(5);
				mutableThis->Poke(LED_POWER_REG, data | 0x0400);
				timer.Delay(20);
				adc[sel][i] = Peek(LED_POWER_REG) & 0x0fff;
			}
			else
			{
				for(int j = 0; (Peek(LED_POWER_REG) & 0x1000) == 0x0000 && j < 5; ++j)
				{
					timer.Delay(15);
				}

				mutableThis->Poke(LED_POWER_REG, data | (sel << 13) | 0x0400);
				timer.Delay(300);
				adc[sel][i] = Peek(LED_POWER_REG) & 0x1fff;

				for(int j = 0; (adc[sel][i] & 0x1000) == 0x0000 && j < 5; ++j)
				{
					timer.Delay(15);
					adc[sel][i] = Peek(LED_POWER_REG) & 0x1fff;
				}

				if((adc[sel][i] & 0x1000) == 0)
				{
					adc[sel][i] = 0x0000;
				}
				else
				{
					adc[sel][i] &= 0x0fff;
				}
			}
		}

		unsigned short filtered = Filter(adc[sel]);

		switch(sel)
		{
		case 0:
			// +5V
			voltages[sel] = filtered * 5.5f / 4095;
			break;

		case 1:
			// +5V or temperature
			if(m_version <= 1)
			{
				// +5 V
				voltages[sel] = filtered * 5.5f / 4095;
			}
			else
			{
				// Temperature
				voltages[sel] = adc2temp(filtered);
			}

			break;

		case 2:
		case 3:
			// +12V
			voltages[sel] = filtered * 13.25f / 4095;
			break;

		case 4:
			// +24V
			voltages[sel] = filtered * 24.6f / 4095;
			break;

		case 5:
		case 6:
			// -12V
			voltages[sel] = filtered * 18.05f / 4095 - voltages[0] * 2.61f;
			break;

		case 7:
			// -48V
			voltages[sel] = filtered * 67.0f / 4095 - voltages[0] * 12.4f;
			break;

		default:
			ASSERT(FALSE);
		}

		if(m_version <= 1)
		{
			timer.Delay(5);
			mutableThis->Poke(LED_POWER_REG, data | 0x0c00);
			timer.Delay(5);
		}
	}

	if(m_version <= 1)
	{
		mutableThis->Poke(MUX_CONTROL_REG, m_shMem->muxControlReg);
	}
	else
	{
		mutableThis->Poke(LED_POWER_REG, m_shMem->ledPowerReg);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Button interrupt handler (overrideable)
//
void C80682017::On80682017Interrupt(void)
{
	TRACE(_T("8068-2017 interrupt - no handler\n"));

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Ping the CPU OK function
//
void C80682017::PingCpuOk_(void)
{
	if(m_present)
	{
		CSingleLock lock(&m_mutex, TRUE);
		m_shMem->statusReg ^= 0x0001;
		Poke(STATUS_REG, m_shMem->statusReg);

		// Also toggle the discrete output
		m_shMem->ledPowerReg ^= 0x0200;
		Poke(LED_POWER_REG, m_shMem->ledPowerReg);
	}

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Refresh the write-only registers
//
void C80682017::Refresh_(void)
{
	if(m_present)
	{
		CSingleLock lock(&m_mutex, TRUE);
		Poke(STATUS_REG, m_shMem->statusReg);
		Poke(LED_POWER_REG, m_shMem->ledPowerReg);
		Poke(MUX_CONTROL_REG, m_shMem->muxControlReg);
		Poke(UHF_SWITCH_REG, m_shMem->uhfSwitchReg);
		Poke(VHF_SWITCH_REG, m_shMem->vhfSwitchReg);
		Poke(HF_SWITCH_REG, m_shMem->hfSwitchReg);
	}

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Turn the POE 48V on or off
//
void C80682017::Set48V(bool on)
{
	if(m_present)
	{
		CSingleLock lock(&m_mutex, TRUE);
		m_shMem->muxControlReg &= 0xff7f;
		m_shMem->muxControlReg |= (on ? 0x0000 : 0x0080);
		Poke(MUX_CONTROL_REG, m_shMem->muxControlReg);
	}

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Set antenna control source and data
//
void C80682017::SetAntennaControl(EPoePort poePort, bool vme, unsigned char data)
{
	if(m_present)
	{
		unsigned short mask;
		CSingleLock lock(&m_mutex, TRUE);

		switch(poePort)
		{
		case C80682017::UHF:
			mask = 0x0100;
			m_shMem->uhfSwitchReg &= 0xff00;
			m_shMem->uhfSwitchReg |= data;
			Poke(UHF_SWITCH_REG, data);
			break;

		case C80682017::VHF:
			mask = 0x0200;
			m_shMem->vhfSwitchReg &= 0xff00;
			m_shMem->vhfSwitchReg |= data;
			Poke(VHF_SWITCH_REG, data);
			break;

		case C80682017::HF:
			mask = 0x0400;
			m_shMem->hfSwitchReg &= 0xff00;
			m_shMem->hfSwitchReg |= data;
			Poke(HF_SWITCH_REG, data);
			break;

		default:
			return;
		}

		m_shMem->muxControlReg &= ~mask;

		if(vme)
		{
			m_shMem->muxControlReg |= mask;
		}

		Poke(MUX_CONTROL_REG, m_shMem->muxControlReg);
		SetSettleTime(5);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the IF mux attenuation
//
unsigned char C80682017::SetAtten(unsigned char atten)
{
	if(m_present)
	{
		atten = std::min(MAX_ATTEN, unsigned char((atten + 3) / 6 * 6));
		unsigned short data = (((atten / 6) << 2) ^ 0x000c);
		CSingleLock lock(&m_mutex, TRUE);

		if((m_shMem->muxControlReg & 0x000c) != data)
		{
			m_shMem->muxControlReg &= 0xfff3;
			m_shMem->muxControlReg |= data;
			Poke(MUX_CONTROL_REG, m_shMem->muxControlReg);
			SetSettleTime(10);
		}
	}

	return atten;
}


/////////////////////////////////////////////////////////////////////
//
// Set the flash rate of the CPU OK LED
//
void C80682017::SetCpuOk(ECpuOk cpuOk)
{
	if(m_present)
	{
		CSingleLock lock(&m_mutex, TRUE);
		m_shMem->statusReg = unsigned short(cpuOk);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the IF mux
//
void C80682017::SetIfMux(bool hf, bool lowBand)
{
	if(m_present)
	{
		unsigned short data = ((hf ? 0x0000 : 0x0001) | (lowBand ? 0x0000 : 0x0002));
		CSingleLock lock(&m_mutex, TRUE);

		if((m_shMem->muxControlReg & 0x0003) != data)
		{
			m_shMem->muxControlReg &= 0xfffc;
			m_shMem->muxControlReg |= data;
			Poke(MUX_CONTROL_REG, m_shMem->muxControlReg);
			SetSettleTime(10);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Turn a LED on or off
//
void C80682017::SetLed(ELed led, bool on)
{
	if(m_present)
	{
		CSingleLock lock(&m_mutex, TRUE);

		if(led == DSP_A || led == DSP_B)
		{
			// Sense of DSP LEDs is inverted
			on = !on;
		}

		m_shMem->ledPowerReg &= ~led;
		m_shMem->ledPowerReg |= (on ? led : 0x0000);
		Poke(LED_POWER_REG, m_shMem->ledPowerReg);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set an aux output
//
void C80682017::SetRemote(unsigned char output, bool on)
{
	ASSERT(output >= 1 && output <= 3);

	if(m_present && output >= 1 && output <= 3)
	{
		unsigned short mask = (0x0020 << output);
		CSingleLock lock(&m_mutex, TRUE);
		m_shMem->ledPowerReg &= ~mask;
		m_shMem->ledPowerReg |= (on ? mask : 0x0000);
		Poke(LED_POWER_REG, m_shMem->ledPowerReg);
	}

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Set the 6.25MHz -10dBm test tone
//
// Note: the test tone only works if low band is also selected. When the tone is turned off we
//  go to either V/UHF or L/MF.
//
void C80682017::SetTestTone(bool ref, bool sample)
{
	if(m_present)
	{
		CSingleLock lock(&m_mutex, TRUE);
		SetSettleTime((ref | sample) && !(m_shMem->muxControlReg & 0x0010) ? 10000 : 5);
		bool vuhf = (m_shMem->muxControlReg & 0x0001);
		m_shMem->muxControlReg &= 0xff8d;
		m_shMem->muxControlReg |= (ref ? 0x0010 : 0x0020) | (sample ? 0x0010 : 0x0040) | (!ref && !sample && vuhf ? 0x0002 : 0x0000);
		Poke(MUX_CONTROL_REG, m_shMem->muxControlReg);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the watchdog output
//
void C80682017::SetWatchDog(bool on)
{
	if(m_present)
	{
		CSingleLock lock(&m_mutex, TRUE);
		m_shMem->ledPowerReg &= 0xfdff;
		m_shMem->ledPowerReg |= (on ? 0x0200 : 0x0000);
		Poke(LED_POWER_REG, m_shMem->ledPowerReg);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Toggle a LED
//
void C80682017::ToggleLed(ELed led)
{
	if(m_present)
	{
		CSingleLock lock(&m_mutex, TRUE);
		m_shMem->ledPowerReg ^= led;
		Poke(LED_POWER_REG, m_shMem->ledPowerReg);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Wite out the shared memory registers to the hardware
//
void C80682017::WriteShMemRegs(void)
{
	CSingleLock lock(&m_mutex, TRUE);
	Poke(STATUS_REG, m_shMem->statusReg);
	Poke(MUX_CONTROL_REG, m_shMem->muxControlReg);
	Poke(UHF_SWITCH_REG, m_shMem->uhfSwitchReg);
	Poke(VHF_SWITCH_REG, m_shMem->vhfSwitchReg);
	Poke(HF_SWITCH_REG, m_shMem->hfSwitchReg);
	Poke(LED_POWER_REG, m_shMem->ledPowerReg);

	return;
}
