/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#pragma once

#include "Units.h"
#include "Vme.h"

class C26122002
:
	// Inheritance
	protected virtual CVme
{
public:
	// Constants
	static const unsigned long CAL_HIGH_FREQ_HZ = 3000000000; // Hz
	static const unsigned long CAL_LOW_FREQ_HZ = 23437500; // Hz
	static const unsigned long FILTER_ASYMMETRY_HZ = 125000; // Hz
	static const unsigned long FINAL_IF_FREQ_NARROW_BW_HZ = 8000000; // Hz
	static const unsigned long FINAL_IF_FREQ_WIDE_BW_HZ = 14000000; // Hz
	static const unsigned long FIRST_IF_BAND_BREAK_FREQ1_HZ = 600000000; // Hz
	static const unsigned long FIRST_IF_BAND_BREAK_FREQ2_HZ = 1400000000; // Hz
	static const unsigned int MIN_TUNE_STEP = 500000; // Hz
	static const unsigned long NARROW_BW_HZ = 2000000; // Hz
	static const unsigned short OXCO_VERY_ACCURATE_INTERVAL = 2;
	static const unsigned short OXCO_ACCURATE_INTERVAL = 10;
	static const unsigned short OXCO_INACCURATE_INTERVAL = 13;
	static const unsigned short OXCO_DAC_BAD_LO = 2;
	static const unsigned short OXCO_DAC_BAD_HI = 29;
	static const unsigned short OXCO_DAC_MARGINAL_LO = 7;
	static const unsigned short OXCO_DAC_MARGINAL_HI = 25;
	static const unsigned long RF_HIGH_FREQ_HZ = 3000000000; // Hz
	static const unsigned long RF_LOW_FREQ_HZ = 20000000; // Hz
	static const unsigned long WIDE_BW_HZ = 20000000; // Hz

	// Functions
	static Units::Frequency GetCalFreq(Units::Frequency freq);
	static unsigned short GetFilterOffsetNumber(Units::Frequency freq) { return GetTuneStepNumber(freq) % 20; } // 2nd IF step
	static Units::Frequency GetIfOffset(Units::Frequency freq)
		{ return MIN_TUNE_STEP * GetTuneStepNumber(freq) - freq; }
	static unsigned short GetTuneStepNumber(Units::Frequency freq)
		{ return unsigned short((freq + MIN_TUNE_STEP / 2 + FILTER_ASYMMETRY_HZ) / Units::Frequency(MIN_TUNE_STEP)); }

protected:
	// Constants
	static const unsigned int MAX_INSTANCES = 2;

	// Types
	struct S100MHzStatus
	{
		//@note: If struct S100MHzStatus is changed, the code in
		//       CHwControl::Transfer2614StatusTo2612 that transfers the value from
		//       2614 to 2612 must be changed accordingly.
		unsigned short outOfLock : 1;
		unsigned short inhibited : 1; // This bit is copied in from the Config register
		unsigned short : 1;
		unsigned short currInterval : 4;
		unsigned short prevInterval : 4;
		unsigned short dac : 5;
	};

	struct SCalSettings
	{
		Units::Frequency calFreq;
		unsigned char divisor;
		unsigned short config;
	};

	struct SCardStatus
	{
		//@note: If struct SCardStatus is changed, the code in
		//       CHwControl::Transfer2612CardStatusTo2614 that transfers the value from
		//       2612 to 2614 must be changed accordingly (caller code may also need
		//       to change).
		unsigned short id : 3;
		unsigned short cardType : 3;
		unsigned short cardClass : 2;
		unsigned short calOk : 1;
		unsigned short lo1Ok : 1;
		unsigned short lo2Ok : 1;
		unsigned short lo3Ok : 1;
		unsigned short adcOk : 1;
		unsigned short version : 3;
	};

	struct SShMem
	{
		Units::Frequency sampleRate;
		Units::Frequency calFreq;
		unsigned short rfFreqHalfMhz;
		unsigned short lo1FreqMhz;
		unsigned short lo3FreqMhz;
		unsigned short lo2FreqHalfMhz;
	};

	// Functions
	C26122002(void); // Can only be constructed by a derived class
	virtual ~C26122002(void);
	S100MHzStatus Get100MHzStatus(unsigned char id = 0) const;
	static SCalSettings GetCalSettings(Units::Frequency freq, Units::Frequency loFreq = 0, Units::Frequency hiFreq = 0xffffffff);
	SCardStatus GetCardStatus(unsigned char id = 0, bool force = false) const;
	unsigned short GetConfigReg(unsigned char id = 0) const { return Peek(id, CONFIG_REG); }
	FILETIME GetTime(unsigned char id = 0) const;
	unsigned char GetVersion(unsigned char id) const;
	void InhibitGpsSync(bool inhibit);
	bool IsPresent(unsigned char id = 0) const { return m_present[id]; }
	Units::Frequency SetCalTone(Units::Frequency freq, bool on, signed char idOrAll = -1);
	void SetLo1(unsigned short freqMhz, unsigned char id);
	void SetLo2(unsigned short freqHalfMhz, unsigned char id);
	void SetLo3(unsigned short freqMhz, unsigned char id);
	void SetSampleRate(Units::Frequency sampleRate, signed char idOrAll = -1);
	void SetSeconds(time_t secs, unsigned char id = 0);
	void Wait(unsigned long us);
	Units::Frequency Tune(Units::Frequency freq, Units::Frequency rxBw, signed char idOrAll = -1);

	// Data
	mutable CMutex m_mutex;
	volatile SShMem (*m_shMem)[MAX_INSTANCES];

private:
	// Constants
	static const unsigned int CARD_CLASS = 0xc;
	static const unsigned int CARD_TYPE = 0x0;
	static const unsigned int VME_BASE_ADDR = (0x770000 | (CARD_CLASS << 12) | (CARD_TYPE << 8));
	static const unsigned int STATUS_REG = 0x00;
	static const unsigned int CONFIG_REG = 0x02;
	static const unsigned int PLL_CTRL_LO_REG = 0x04;
	static const unsigned int PLL_CTRL_HI_REG = 0x06;
	static const unsigned int GPS_STATUS_REG = 0x08;
	static const unsigned int GPS_FRAC_SEC_REG = 0x0a;
	static const unsigned int GPS_SEC_LO_REG = 0x0c;
	static const unsigned int GPS_SEC_HI_REG = 0x0e;
	static const unsigned int PIT_REG = 0x10;
	static LPCTSTR MUTEX_NAME;
	static LPCTSTR SHMEM_NAME;

	// Types
	enum EPll { CAL = 0x01, LO1 = 0x02, LO2 = 0x04, LO3 = 0x08, ADC = 0x10 };

	union UPllLatch
	{
		unsigned int allBits;

		struct
		{
			unsigned short lo;
			unsigned short hi;
		} reg;

		struct
		{
			unsigned int done : 1;
			unsigned int : 2;
			unsigned int pll : 5;
			unsigned int c : 2;
			unsigned int : 22;
		} controlStatus;

		struct
		{
			unsigned int done : 1;
			unsigned int : 2;
			unsigned int pll : 5;
			unsigned int c : 2; // 3
			unsigned int f1 : 1;
			unsigned int pd1 : 1;
			unsigned int m : 3;
			unsigned int f2 : 1;
			unsigned int f3 : 1;
			unsigned int f4 : 1;
			unsigned int f5 : 1;
			unsigned int tc : 4;
			unsigned int cp1 : 3;
			unsigned int cp2 : 3;
			unsigned int pd2 : 1;
			unsigned int p : 2;
		} initLatch;

		struct
		{
			unsigned int done : 1;
			unsigned int : 2;
			unsigned int pll : 5;
			unsigned int c : 2; // 2
			unsigned int f1 : 1;
			unsigned int pd1 : 1;
			unsigned int m : 3;
			unsigned int f2 : 1;
			unsigned int f3 : 1;
			unsigned int f4 : 1;
			unsigned int f5 : 1;
			unsigned int tc : 4;
			unsigned int cp1 : 3;
			unsigned int cp2 : 3;
			unsigned int pd2 : 1;
			unsigned int p : 2;
		} functionLatch;

		struct
		{
			unsigned int done : 1;
			unsigned int : 2;
			unsigned int pll : 5;
			unsigned int c : 2; // 0
			unsigned int r : 14;
			unsigned int abp : 2;
			unsigned int t : 2;
			unsigned int lpd : 1;
			unsigned int : 3;
		} refCounterLatch;

		struct
		{
			unsigned int done : 1;
			unsigned int : 2;
			unsigned int pll : 5;
			unsigned int c : 2; // 1
			unsigned int a : 6;
			unsigned int b : 13;
			unsigned int g : 1;
			unsigned int : 2;
		} nCounterLatch;

		struct
		{
			unsigned int done : 1;
			unsigned int : 2;
			unsigned int pll : 5;
			unsigned int c : 2; // 3
			unsigned int t1 : 1;
			unsigned int : 3;
			unsigned int t2 : 4;
			unsigned int : 14;
		} noiseSpurLatch;

		struct
		{
			unsigned int done : 1;
			unsigned int : 2;
			unsigned int pll : 5;
			unsigned int c : 2; // 2
			unsigned int u1 : 1;
			unsigned int u2 : 1;
			unsigned int u3 : 1;
			unsigned int u4 : 1;
			unsigned int u5 : 1;
			unsigned int cp1 : 3;
			unsigned int cp2 : 1;
			unsigned int u6 : 1;
			unsigned int : 12;
		} controlLatch;

		struct
		{
			unsigned int done : 1;
			unsigned int : 2;
			unsigned int pll : 5;
			unsigned int c : 2; // 1
			unsigned int m : 12;
			unsigned int r : 4;
			unsigned int p1 : 1;
			unsigned int : 1;
			unsigned int mx : 3;
			unsigned int p3 : 1;
		} rDividerLatch;

		struct
		{
			unsigned int done : 1;
			unsigned int : 2;
			unsigned int pll : 5;
			unsigned int c : 2; // 2
			unsigned int f : 12;
			unsigned int n : 9;
			unsigned int fl : 1;
		} nDividerLatch;
	};

	// Functions
	static void OnTimer(void* param, unsigned int) { static_cast<C26122002*>(param)->OnTimer(); }
	void OnTimer(void);
	unsigned short Peek(unsigned char id, unsigned char reg) const { return CVme::Peek(VME_BASE_ADDR + 32 * id + reg); }
	void Poke(unsigned char id, unsigned char reg, unsigned short data) { CVme::Poke(VME_BASE_ADDR + 32 * id + reg, data); }
	void SetPllLatch(const UPllLatch& latch, unsigned char id);
	virtual void SetSettleTime(unsigned int) {}

	// Data
	CEvent m_expired;
	bool m_first;
	bool m_intHandler;
	bool m_present[MAX_INSTANCES];
};
