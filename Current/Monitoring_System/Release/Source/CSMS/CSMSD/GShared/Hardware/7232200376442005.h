/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#pragma once

#include "80682017.h"
#include "Digitizer.h"
#include "EquipCtrlMsg.h"
#include "Singleton.h"

class C7232200376442005
:
	// Inheritance
	protected virtual C80682017
{
public:
	// Constants
	static const unsigned long BITE_FREQ_HZ = 25000000;
	static const unsigned long BITE_7644_TX_FREQ_HZ = 241500000;
	static const signed char BITE_7232_DBM = -25;
	static const signed char BITE_7644_DBM = -31;
	static const signed char BITE_7644_TX_DBM = -30;
	static const unsigned long MAX_FREQ_7232_HZ = 175000000;
	static const unsigned long MAX_FREQ_7644_HZ = 500000000;
	static const unsigned long MAX_FREQ_7644_DF_HZ = 250000000;
	static const unsigned long MIN_FREQ_7644_HZ = 50000000;
	static const unsigned char NUM_ANTS = 5;

	// Types
	enum ESource // Values match hardware - don't change!
	{
		TERM	= 0,
		ANT_1	= 1,
		ANT_2	= 2,
		ANT_3	= 3,
		ANT_4	= 4,
		ANT_5	= 5,
		BITE	= 6
	};

	// Constants
	static const ESource GRAY_CODE[NUM_ANTS];

	// Functions
	inline static size_t Index(ESource source);
	inline static ESource C7232200376442005::Source(SEquipCtrlMsg::EAnt ant);

protected:
	// Friends
	friend ESource& operator++(ESource& s);

	// Functions
	C7232200376442005(void); // Can only be constructed by a derived class
	virtual ~C7232200376442005(void);
	static unsigned char GetControlByte(bool notchTxBite, ESource refRf, ESource sampleRf);
	unsigned char GetCurrentControlByte(void) const { CSingleLock lock(&m_critSect, TRUE); return m_controlByte; }
	bool Is7644(void) const { return m_is7644; }
	void Set7644(bool is7644) { m_is7644 = is7644; }
	void SetSwitch(unsigned char data, bool force);
	void SetSwitch(bool notchTxBite, ESource refRf, ESource sampleRf);
	void ValidateLoopback(unsigned char loopback);

private:
	// Functions
	unsigned int GetSettleTime(unsigned char controlByte) const;
	virtual void SetSettleTime(unsigned int) {}

	// Data
	volatile unsigned char m_controlByte;
	mutable CCriticalSection m_critSect;
	CSingleton<CDigitizer> m_digitizer;
	bool m_is7644;
	unsigned int m_validateFailCount;
};


//////////////////////////////////////////////////////////////////////
//
// ++ operator for ESource
//
inline C7232200376442005::ESource& operator++(C7232200376442005::ESource& s)
{
	switch(s)
	{
	case C7232200376442005::ANT_1:
	case C7232200376442005::ANT_2:
	case C7232200376442005::ANT_3:
	case C7232200376442005::ANT_4:
		s = static_cast<C7232200376442005::ESource>(s + 1);
		break;

	case C7232200376442005::ANT_5:
		s = C7232200376442005::ANT_1;
		break;

	default:
		ASSERT(FALSE);
		break;
	}

	return s;
}


//////////////////////////////////////////////////////////////////////
//
// Conversion function for ESource to get DF antenna index
//
inline size_t C7232200376442005::Index(ESource source)
{
	switch(source)
	{
	case ANT_1:
	case ANT_2:
	case ANT_3:
	case ANT_4:
	case ANT_5:
		return source - 1;

	default:
		ASSERT(FALSE);
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Conversion function for ESource
//
inline C7232200376442005::ESource C7232200376442005::Source(SEquipCtrlMsg::EAnt ant)
{
	switch(ant)
	{
	case SEquipCtrlMsg::ANT1:
		return C7232200376442005::ANT_1;

	case SEquipCtrlMsg::ANT2:
	case SEquipCtrlMsg::ANT3:
	case SEquipCtrlMsg::SMPL_RF2:
	case SEquipCtrlMsg::SMPL_RF3:
		return C7232200376442005::TERM;

	case SEquipCtrlMsg::DF_ANT_1V:
		return C7232200376442005::ANT_1;

	case SEquipCtrlMsg::DF_ANT_2V:
		return C7232200376442005::ANT_2;

	case SEquipCtrlMsg::DF_ANT_3V:
		return C7232200376442005::ANT_3;

	case SEquipCtrlMsg::DF_ANT_4V:
		return C7232200376442005::ANT_4;

	case SEquipCtrlMsg::DF_ANT_5V:
		return C7232200376442005::ANT_5;

	default:
		ASSERT(FALSE);
		return C7232200376442005::TERM;
	}
}


