/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2013 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#pragma once

#pragma warning(disable : 4480) // Non-standard extension

struct SRotatorMsg
{
	// Message definitions
	enum EType : unsigned short
	{
		GET_STATUS,
		STATUS_RESPONSE,
        GET_STATUS_V1,
        STATUS_RESPONSE_V1,
    };

    static const int ELEVATION_UNKNOWN = 0xFFFF;

	// Message layout
	struct SHdr
	{
		EType type;
		unsigned short bodyLen;
	} hdr;

	// STATUS_RESPONSE
	struct SStatus
	{
		enum EPolarization : unsigned short { HORIZONTAL, VERTICAL, INVALID };
		static const size_t MAX_NAME_CHARS = 32;

		wchar_t name[MAX_NAME_CHARS];
		unsigned short azimuth;
        EPolarization polarization;
        bool connected;
		bool moving;
	};
    // V1 of STATUS_RESPONSE that supports Elevation, etc.
    struct SStatusV1
    {
        enum EPolarization : unsigned short { HORIZONTAL, VERTICAL, PLUS_45, MINUS_45, INVALID };
        static const size_t MAX_NAME_CHARS = 32;
        static const int TILT_CENTER_POSITION = 90; // The center position of the Tilt/Eelevation. Set to 90 Degrees.

        wchar_t name[MAX_NAME_CHARS];
        unsigned short azimuth;
        int elevation;
        EPolarization polarization;
        bool connected;
        bool moving;
    };

	union
	{
		// NoBody getStatus
        SStatus     statusResponse;
        SStatusV1   statusResponseV1;
	};
};
