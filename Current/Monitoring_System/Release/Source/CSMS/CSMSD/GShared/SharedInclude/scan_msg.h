/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2005 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/
#ifndef SCAN_MSG_H
#define SCAN_MSG_H

#include "ScanDefs.h"
#include "sms_msg.h"

// message subtypes for message type SCAN_CTRL
#define START_SCANDF			1L
#define SUSPEND_SCANDF			2L
#define RESUME_SCANDF			3L
#define ACK_SCANDF_DATA			4L
#define VALIDATE_SCANDF			7L
#define GET_TASK_STATE			8L
#define TERMINATE_SCANDF		9L
#define SEND_SCANDF_RESPONSE	32770L
#define VALIDATE_SCANDF_RESPONSE 65537L
#define TASK_STATUS				65538L
#define SCANDF_DATA_RESPONSE	65540L
#define SCANDF_STATE_RESP		65544L
#define SCANDF_SOLICIT_STATE_RESP 65545L
#define SCANDF_FREQUENCY_RESULT	65546L
#define DSP_SCANDF_START		131073L
#define DSP_SCANDF_TERMINATE	131074L
#define DSP_SCANDF_SUSPEND		131075L
#define DSP_SCANDF_RESUME		131076L
#define DSP_SCANDF_STARTED		196609L
#define DSP_SCANDF_TERMINATED	196610L
#define DSP_SCANDF_SUSPENDED	196611L
#define DSP_SCANDF_RESUMED		196612L

// Scan Control Command message body
#define MAX_SCANDFINCLUDES 10
#define MAX_SCANDFEXCLUDES 100
#define MAX_SCANDFCHANNELS 2400

// following define includeExclude values
#define SCANDF_INCLUDE 0
#define SCANDF_EXCLUDE 1

// following define signalType values
#define SCANDF_SIGNAL_CONV 0
#define SCANDF_SIGNAL_GSM 1

typedef struct 
{
	unsigned long numBands;		// number of frequency bands
	unsigned long lowFrequency[MAX_SCANDFINCLUDES+MAX_SCANDFEXCLUDES];	// low frequency (Hz)
	unsigned long highFrequency[MAX_SCANDFINCLUDES+MAX_SCANDFEXCLUDES];	// high frequency (Hz), = low frequency for single channel
	unsigned long channelBandwidth[MAX_SCANDFINCLUDES+MAX_SCANDFEXCLUDES]; // (Hz)
	unsigned long includeExclude[MAX_SCANDFINCLUDES+MAX_SCANDFEXCLUDES];
	unsigned long signalType[MAX_SCANDFINCLUDES+MAX_SCANDFEXCLUDES];
	unsigned long storageTime;		// seconds
	unsigned long measurementTime;	// seconds
	unsigned long scanDFThreshold;	// dB above NOISE
	unsigned long numAzimuths;
	unsigned long confidence;       // 0 to 100
	unsigned long recordAudioDF;	// 0 = don't record; >0 = record (RA3 function)
	unsigned long recordHoldoff;	// record holdoff time in sec (RA3 function)
	RcvrCtrlCmd rcvrCtrl;			// for recording (RA3 function)
} getScanDFCmd;

typedef struct
{
	unsigned long taskID;			// equipcontrol ID for task (from start msg)
	unsigned long key;				// equipcontrol key for task (from start msg)
} terminateScanDFCmd;

// Scan DF Response DSP message body
typedef struct
{
	unsigned long	taskID;
	long			status;
} DSPScanDFStatusResp;

typedef struct
{
	unsigned long	taskID;
	long	status;
	int		index;
} sendScanDFResponse;

// Scan DF Command DSP message body
typedef struct 
{
	unsigned long taskID;			// unique ID assigned by equipcontrol
	unsigned long numIncludes;		// number of frequency bands
	scanDFBandInclude include[MAX_SCANDFINCLUDES];
	unsigned long numExcludes;
	scanDFBandExclude exclude[MAX_SCANDFEXCLUDES];
	unsigned long scanDFThreshold;	// dB above NOISE
	unsigned long dwellTime;		// length of measurement in DSP
	unsigned long AGCTimeout;		// ms
} DSPScanDFCmd;

typedef struct 
{
	unsigned long taskID;			// unique ID assigned by equipcontrol
} DSPScanDFTerminate;

#define STATE_IDLE 0
#define STATE_RUNNING 1
#define STATE_SUSPENDED 2
typedef struct
{
	int state;
	double completionTime;
} scanDFStateResp;

typedef struct
{
	long			status;
	getScanDFCmd	scanDFCmd;
} validateScanDFResp;

typedef struct
{
	long			status;
	getScanDFCmd	avdCmd;
} validateAvdResp;

typedef struct
{
	long			status;
	GPS_RESPONSE	GPSResponse;
	unsigned long	taskID;
	unsigned long	msgFlag;
	unsigned long	numTotalChannels;
	unsigned long	firstChannel;
	unsigned long	numChannels;
} scanHeader;

typedef struct
{
	scanHeader scanHdr;
	int numAzimuths;
	unsigned long Frequencies[MAX_SCANDFCHANNELS];
	int numBands;
	unsigned long lowFrequency[MAX_SCANDFINCLUDES+MAX_SCANDFEXCLUDES];	// low frequency (Hz)
	unsigned long highFrequency[MAX_SCANDFINCLUDES+MAX_SCANDFEXCLUDES];	// high frequency (Hz), = low frequency for single channel
	unsigned long numChannels[MAX_SCANDFINCLUDES+MAX_SCANDFEXCLUDES];
} scanFreqVsChannelResp;

typedef struct
{
	scanHeader scanHdr;
	unsigned long scanDFData[MAX_SCANDFCHANNELS];
} scanDFDataResp;

#define INTERIMSCANDF 111
#define FINALSCANDF 222
typedef struct
{
	unsigned long	taskID;
	unsigned long	msgFlag;	// INTERIM or FINAL
	unsigned long	firstChannel;
	unsigned long	numChannels;
} ackScanDFDataMsg;

typedef struct
{
	unsigned long taskID;			// equipcontrol ID for task (from start msg)
	unsigned long key;				// equipcontrol key for task (from start msg)
} suspendScanDFCmd;

typedef struct
{
	unsigned long taskID;			// equipcontrol ID for task (from start msg)
	unsigned long key;				// equipcontrol key for task (from start msg)
} resumeScanDFCmd;

#endif
