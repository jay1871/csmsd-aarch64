/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "ErrorCodes.h"
#include "SmsMsg.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4480)
#else
#include <cassert>
#include <cstddef>
#include "MSCompat.h"
#endif

struct SEquipCtrlMsg
:
	SSmsMsg
{
	// Message subtypes for message type DSP_CTRL
	enum EDspCtrl
	{
		DO_DSP_INIT				= 1,
		DO_DSP_INIT_RESPONSE	= 65537
	};

	// Message subtypes for message type EQUIPMENT_CTRL
	enum EEquipmentCtrl
	{
		TASK_TERMINATE	= 1,
		GET_TASK_STATUS	= 2,
		TASK_STATUS		= 65538
	};

	// Message subtypes for message type AUTOVIOLATE_CTRL, OCCUPANCY_CTRL and OCCUPANCYDF_CTRL
	enum EOccupancyDfCtrl
	{
		GET_OCCUPANCY =					1,
		SUSPEND_OCCUPANCY =				2,
		RESUME_OCCUPANCY =				3,
		RESTART_OCCUPANCY =				4,		//Obsolete.
		OCC_DATA_RESTART_RESPONSE =		5,		//Obsolete.
		OCC_NODATA_RESTART_RESPONSE =	6,		//Obsolete.
		VALIDATE_OCCUPANCY =			7,
		GET_TASK_STATE =				8,
		GET_OCCUPANCY_SCANDF =			9,
		VALIDATE_OCCUPANCY_SCANDF =		10,
		GET_AUTOVIOLATE =				11,		// added for AVD
		VALIDATE_AUTOVIOLATE =			12,		// added for AVD
		NEXT_OCCUPANCY =				32769,
		SEND_OCCUPANCY_RESPONSE =		32770,
		AVD_MEAS =						32771,	// added for AVD
		VALIDATE_OCC_RESPONSE =			65537,
// Duplicate		TASK_STATUS =					65538,
		OCC_CHANNEL_RESPONSE =			65539,
		EFLD_CHANNEL_RESPONSE =			65540,
		OCC_TIMEOFDAY_RESPONSE =		65541,
		MSGLEN_CHANNEL_RESPONSE =		65542,
		MSGLEN_DIST_RESPONSE =			65543,
		OCCUPANCY_STATE_RESP =			65544,
		OCCUPANCY_SOLICIT_STATE_RESP =	65545,
		OCC_FREQUENCY_RESULT =			65546,
		OCC_CHANNEL_RESULT =			65547,
		EFLD_CHANNEL_RESULT =			65548,
		OCC_TIMEOFDAY_RESULT =			65549,
		MSGLEN_CHANNEL_RESULT =			65550,
		OCC_SPECGRAM_RESULT =			65551,
		OCC_DATA_RESTART_REQUEST =		65552,
		SCANDF_DATA_RESULT =			65553,
		AUTOVIOLATE_FREQMEAS_RESULT =	65556,	// added for AVD
		AUTOVIOLATE_BWMEAS_RESULT =		65557,	// added for AVD
		AVD_FREQUENCY_RESULT =			65558,
		AVD_CHANNEL_RESULT =			65559,
		OCC_INTER_DATA_RESULT =			65564,	// this is RA3 intermediate data
		OCC_EFLD_TIMEOFDAY_RESULT =     65565,
	};

	// Message subtypes for message type WB_DIGITIZER_SAMPLE
	enum EWbDigitizerSample
	{
		SNGL_SAMPLE =		1,
		SNGL_SAMPLE_RESP =	65537
	};


	// Message version data
	static const SVersionData VERSION_DATA[];

	// Message structures
	struct SAgcOnOffCmd // AGC_ONOFF
	{
		enum : unsigned long
		{
			OFF = 0,
			ON = 1
		} onOff;
	};

	struct SDspWbSmplCmd // SNGL_SAMPLE
	{
		unsigned long taskId;
		unsigned long rxBandwidth; // s/b 25000, 100000, or 10000000
		unsigned long sampleRate;  // s/b 3200000 or 25600000 or 38400000
		unsigned long decimation;  // s/b 1 for bypass mode or 64 to 65536
		unsigned long ddcFirBw;    // s/b 17, 34, 68, 80, 100, or 150 (normally 80)
		unsigned long sampleSize;  // s/b 64 to 4096 (per burst for DSP_FAST_SAMPLE)
		unsigned long channel;     // s/b 0=chan 1 only; 1=chan 2 only; 2=both channels
		unsigned long ch1Thresh1;  // s/b 1 to 15
		unsigned long ch1Thresh2;  // s/b 1 to 15
		unsigned long ch2Thresh1;  // s/b 1 to 15
		unsigned long ch2Thresh2;  // s/b 1 to 15
		unsigned long inputType;   // s/b from definitions for inputType
		unsigned long outputType;  // s/b from definitions for outputType
		unsigned long rxAtten;     // 0 to 78 by 2 for V/UHF
		unsigned long numBins;     // only applicable for outputType = OUT_FREQ
		unsigned long numDwell;    // only applicable for outputType = OUT_FREQ
		unsigned long numBursts;   // number of bursts for DSP_FAST_SAMPLE
	};

	struct SDspWbSmplResp // SNGL_SAMPLE_RESP
	{
		unsigned long taskId;
		ErrorCodes::EErrorCode status;
		unsigned long ch1Thrsh1Cntr;  // 0 to 16777215 (24 bits)
		unsigned long ch1Thrsh2Cntr;  // 0 to 16777215 (24 bits)
		unsigned long ch2Thrsh1Cntr;  // 0 to 16777215 (24 bits)
		unsigned long ch2Thrsh2Cntr;  // 0 to 16777215 (24 bits)
		long data[2048];     // 2 16-bit "packed" values/word
	};

	struct SEquipTaskStatusResp // TASK_STATUS
	{
		unsigned long taskId; // unique ID assigned by equipcontrol
		unsigned long key; // unique key needed for other commands

		enum EStatus : unsigned long
		{
			STARTED		= 1,
			COMPLETED	= 2,
			TERMINATED	= 3,
			RESTARTED	= 4,
			ACTIVE		= 5
		}; 

		EStatus status;
		double dateTime;
	};

	struct SGetIonogram // GET_IONOGRAM
	{
		unsigned long taskId;
	};

	struct SGetIonogramResp // GET_IONOGRAM_RESPONSE
	{
		unsigned long taskId;
		ErrorCodes::EErrorCode status;
		SIonogramDataMsg ionoData;
	};

	struct SStateResp // OCCUPANCY_SOLICIT_STATE_RESP, OCCUPANCY_STATE_RESP
	{
		enum EState : int
		{
			IDLE		= 0,
			RUNNING		= 1,
			SUSPENDED	= 2
		};

		EState state;
		double completionTime;
	};

	struct STaskIdKey // RESUME_OCCUPANCY, SUSPEND_OCCUPANCY, TASK_TERMINATE
	{
		unsigned long taskId;			// equipcontrol ID for task (from start msg)
		unsigned long key;				// equipcontrol key for task (from start msg)
	};

	struct SValidateMeasurementResp // VALIDATE_MEAS_RESPONSE
	{
		ErrorCodes::EErrorCode status;
		unsigned long	bwDwellTime;
		unsigned long	freqDwellTime;
		unsigned long	modulationDwellTime;
		unsigned long	fieldStrengthDwellTime;
		unsigned long	dfDwellTime;
		unsigned long	totalTime;
	};

	struct SValidateOccupancyRespV1; // Forward declaration

	struct SValidateOccupancyRespV0 // VALIDATE_OCC_RESP V0
	{
		inline operator SValidateOccupancyRespV1(void) const; // Convert to V1

		ErrorCodes::EErrorCode status;
		SGetOccupancyCmdV0 occCmd;
	};

	struct SValidateOccupancyRespV1 // VALIDATE_OCC_RESP V1
	{
		operator SValidateOccupancyRespV0(void) const // Convert to V0
		{
			SValidateOccupancyRespV0 v0;
			v0.status = status;
			v0.occCmd = occCmd;

			return v0;
		}

		ErrorCodes::EErrorCode status;
		SGetOccupancyCmdV1 occCmd;
	};

	struct SValidateOccupancyRespV3; // Forward declaration

	//@note: SValidateOccupancyRespV2 is fixed size eventhough it contains
	//       SGetOccupancyCmdV2 that is of variable length. To make it fixed,
	//		 the code only populate the 1st band array and discards the
	//		 remaining band info.
	struct SValidateOccupancyRespV2 // VALIDATE_OCC_RESP V2.
	{
		operator SValidateOccupancyRespV1(void) const // Convert to V1
		{
#ifndef CSMS_2016
			ASSERT(!IsOnStack(this) || offsetof(SEquipCtrlMsg, body.validateOccupancyRespV2.occCmd.band[occCmd.numBands]) <= sizeof(SEquipCtrlMsg));
#endif
			SValidateOccupancyRespV1 v1;
			v1.status = status;
			v1.occCmd = occCmd;

			return v1;
		}

		SValidateOccupancyRespV2& operator =(SValidateOccupancyRespV1 v1) // Convert from V1
		{
#ifndef CSMS_2016
			ASSERT(!IsOnStack(this) || offsetof(SEquipCtrlMsg, body.validateOccupancyRespV2.occCmd.band[occCmd.numBands]) <= sizeof(SEquipCtrlMsg));
#endif
			status = v1.status;
			occCmd = v1.occCmd;

			return *this;
		}

		inline SValidateOccupancyRespV2& operator =(const SValidateOccupancyRespV3& v3); // Convert from V3

		ErrorCodes::EErrorCode status;
		SGetOccupancyCmdV2 occCmd;
	};

	struct SValidateOccupancyRespV4; // Forward declaration

	//@note: SValidateOccupancyRespV3 is fixed size eventhough it contains
	//       SGetOccupancyCmdV3 that is of variable length. To make it fixed,
	//		 the code only populate the 1st band array and discards the
	//		 remaining band info.
	struct SValidateOccupancyRespV3 // VALIDATE_OCC_RESP V3 (Fixed due to single band array element)
	{
		operator SValidateOccupancyRespV2(void) const // Convert to V2
		{
#ifndef CSMS_2016
			ASSERT(!IsOnStack(this) || offsetof(SEquipCtrlMsg, body.validateOccupancyRespV3.occCmd.band[occCmd.numBands]) <= sizeof(SEquipCtrlMsg));
#endif
			SValidateOccupancyRespV2 v2;
			v2.status = status;
			v2.occCmd = occCmd;

			return v2;
		}

		SValidateOccupancyRespV3& operator =(const SValidateOccupancyRespV2& v2) // Convert from V2
		{
			ASSERT(!IsOnStack(&v2) || offsetof(SEquipCtrlMsg, body.validateOccupancyRespV2.occCmd.band[v2.occCmd.numBands]) <= sizeof(SEquipCtrlMsg));
			status = v2.status;
			occCmd = v2.occCmd;

			return *this;
		}

		inline SValidateOccupancyRespV3& operator =(const SValidateOccupancyRespV4& v4); //Down-Convert from V4

		ErrorCodes::EErrorCode status;
		SGetOccupancyCmdV3 occCmd;
	};

	struct SValidateOccupancyRespV5; // Forward declaration

	//@note: SValidateOccupancyRespV4 is fixed size eventhough it contains
	//       SGetOccupancyCmdV4 that is of variable length. To make it fixed,
	//		 the code only populate the 1st band array and discards the
	//		 remaining band info.
	struct SValidateOccupancyRespV4 // VALIDATE_OCC_RESP V4 (Fixed due to single band array element)
	{
		operator SValidateOccupancyRespV3(void) const // Convert to V3
		{
#ifndef CSMS_2016
			ASSERT(!IsOnStack(this) || offsetof(SEquipCtrlMsg, body.validateOccupancyRespV4.occCmd.band[occCmd.numBands]) <= sizeof(SEquipCtrlMsg));
#endif
			SValidateOccupancyRespV3 v3;
			v3.status = status;
			v3.occCmd = occCmd;

			return v3;
		}

		SValidateOccupancyRespV4& operator =(const SValidateOccupancyRespV3& v3) // Convert from V3
		{
			ASSERT(!IsOnStack(&v3) || offsetof(SEquipCtrlMsg, body.validateOccupancyRespV3.occCmd.band[v3.occCmd.numBands]) <= sizeof(SEquipCtrlMsg));
			status = v3.status;
			occCmd = v3.occCmd;

			return *this;
		}

		ErrorCodes::EErrorCode status;
		SGetOccupancyCmdV4 occCmd;
	};

	//@note: SValidateOccupancyRespV5 is fixed size eventhough it contains
	//       SGetOccupancyCmdV5 that is of variable length. To make it fixed,
	//		 the code only populate the 1st band array and discards the
	//		 remaining band info.
	struct SValidateOccupancyRespV5 // VALIDATE_OCC_RESP V5 (Fixed due to single band array element)
	{
		operator SValidateOccupancyRespV4(void) const // Convert to V4
		{
#ifndef CSMS_2016
			ASSERT(!IsOnStack(this) || offsetof(SEquipCtrlMsg, body.validateOccupancyRespV5.occCmd.band[occCmd.numBands]) <= sizeof(SEquipCtrlMsg));
#endif
			SValidateOccupancyRespV4 v4;
			v4.status = status;
			v4.occCmd = occCmd;

			return v4;
		}

		SValidateOccupancyRespV5& operator =(const SValidateOccupancyRespV4& v4) // Convert from V4
		{
			ASSERT(!IsOnStack(&v4) || offsetof(SEquipCtrlMsg, body.validateOccupancyRespV4.occCmd.band[v4.occCmd.numBands]) <= sizeof(SEquipCtrlMsg));
			status = v4.status;
			occCmd = v4.occCmd;

			return *this;
		}

		ErrorCodes::EErrorCode status;
		SGetOccupancyCmdV5 occCmd;
	};

	typedef SValidateOccupancyRespV5 SValidateOccupancyResp;


	// Message format
#pragma pack(push, 4)
	SHdr hdr;

	union UBody
	{
		SAgcOnOffCmd agcOnOffCmd;
		SAntGetSetCtrlResp antGetSetCtrlResp;
		SAntSetCtrlCmd antSetCtrlCmd;
		SAudioParamsCmd audioParamsCmd;
		SAudioParamsCmdV1 audioParamsCmdV1;
		SAudioParamsCmdV2 audioParamsCmdV2;
		SAudioParamsResp audioParamsResp;
		SAudioParamsRespV1 audioParamsRespV1;
		SAudioParamsRespV2 audioParamsRespV2;
		SAudioSwitchStatusResp audioSwitchStatusResp;
		SAvdMeasureResult avdMeasureResult; // Variable length
		SAvdMeasureResultV0 avdMeasureResultV0;
		SAvdMeasureResultV1 avdMeasureResultV1; // Variable length
		SDspWbSmplCmd dspWbSmplCmd;
		SDspWbSmplResp dspWbSmplResp;
		SEquipTaskStatusResp equipTaskStatusResp;
		SFreeAudioChannelCmd freeAudioChannelCmd;
		SFreeAudioChannelCmdV1 freeAudioChannelCmdV1;
		SFrequencyVsChannelResp frequencyVsChannelResp; // Variable length
		SFrequencyVsChannelRespV0 frequencyVsChannelRespV0;
		SFrequencyVsChannelRespV1 frequencyVsChannelRespV1; // Variable length
		SGenericResp genericResp;
		SGetAutoViolateCmd getAutoViolateCmd; // Variable length
		SGetAutoViolateCmdV0 getAutoViolateCmdV0;
		SGetAutoViolateCmdV1 getAutoViolateCmdV1;
		SGetAutoViolateCmdV2 getAutoViolateCmdV2; // Variable length
		SGetAutoViolateCmdV3 getAutoViolateCmdV3; // Variable length
		SGetBandResp getBandResp;
		SGetBandRespV0 getBandRespV0;
		SGetBandRespV1 getBandRespV1;
		SGetBandRespV2 getBandRespV2;
		SGetBandRespV3 getBandRespV3;
		SGetBandRespV4 getBandRespV4;
		SGetBandRespV5 getBandRespV5;
		SGetBandRespV6 getBandRespV6;
		SGetBistResp getBistResp; // Variable length
		SGetBistRespOld getBistRespOld;
		SGetDwellCmd getDwellCmd;
		SGetDwellCmdV0 getDwellCmdV0;
		SGetDwellCmdV1 getDwellCmdV1;
		SGetDwellResp getDwellResp;
		SGetFaultRespV1 getFaultRespV1;
		SGetFaultResp getFaultResp;
		SGetCsmsFaultRespV1 getCsmsFaultRespV1;
		SGetCsmsFaultRespV2 getCsmsFaultRespV2;
		SGetCsmsFaultResp getCsmsFaultResp;
		SGetGpsResp getGpsResp;
		SGetHeadingResp getHeadingResp;
		SGetIonogram getIonogram;
		SGetIonogramResp getIonogramResp;
		SGetMeasCmd getMeasCmd;
		SGetMeasCmdV0 getMeasCmdV0;
		SGetMeasCmdV1 getMeasCmdV1;
		SGetMeasCmdV2 getMeasCmdV2;
		SGetMeasCmdV3 getMeasCmdV3;
		SGetMeasCmdV4 getMeasCmdV4;
		SGetMeasCmdV5 getMeasCmdV5;
		SGetMeasResp getMeasResp;
		SGetMeasRespV0 getMeasRespV0;
		SGetMeasRespV1 getMeasRespV1;
		SGetMeasRespV2 getMeasRespV2;
		SGetMeasRespV3 getMeasRespV3;
		SIqDataResp iqDataResp;
		SIqDataRespV0 iqDataRespV0;
		SIqDataRespV1 iqDataRespV1;
		SGetMsgVersionsData getMsgVersionsData; // Variable length
		SGetOccupancyCmd getOccupancyCmd; // Variable length
		SGetOccupancyCmdV0 getOccupancyCmdV0;
		SGetOccupancyCmdV1 getOccupancyCmdV1;
		SGetOccupancyCmdV2 getOccupancyCmdV2; // Variable length
		SGetOccupancyCmdV3 getOccupancyCmdV3; // Variable length
		SGetOccupancyCmdV4 getOccupancyCmdV4; // Variable length
		SGetOccupancyCmdV5 getOccupancyCmdV5; // Variable length
		SGetPanCmd getPanCmd;
		SGetPanCmdV0 getPanCmdV0;
		SGetPanCmdV1 getPanCmdV1;
		SGetPanCmdV2 getPanCmdV2;
		SGetPanResp getPanResp; // Variable length
		SGetPanRespV0 getPanRespV0;
		SGetPanRespV1 getPanRespV1; // Variable length
		SGetScanDfCmd getScanDfCmd; // Variable length
		SGetScanDfCmdV0 getScanDfCmdV0;
		SGetScanDfCmdV1 getScanDfCmdV1; // Variable length
		SInitializeDf initializeDf;
		SInitializeDfV1 initializeDfV1; // V0 has no body
		SInitializeDfV2 initializeDfV2;
		SInitializeDfResp initializeDfResp; // Variable length
		SInitializeDfRespV0 initializeDfRespV0;
		SInitializeDfRespV1 initializeDfRespV1; // Variable length
		SInitializeDfRespV2 initializeDfRespV2; // Variable length
		SInitializeDfRespV3 initializeDfRespV3; // Variable length
		SInitializeDfRespV4 initializeDfRespV4; // Variable length
		SLoadIonogram loadIonogram;
		SLoadIonogramResp loadIonogramResp;
		SMsgLengthDistributionResp msgLengthDistributionResp;
		SOccResult occResult;
		SStateResp stateResp;
		SRcvrCtrlCmd rcvrCtrlCmd;
		SRcvrCtrlCmdV0 rcvrCtrlCmdV0;
		SRcvrCtrlCmdV1 rcvrCtrlCmdV1;
		SRcvrManCmd rcvrManCmd;
//		SPanCapabilitiesResponse panCapabilitiesResponse;
		SPanParaCmd panParaCmd;
		SPanParaCmdV1 panParaCmdV1;
		SScanDfVsChannelResp scanDfVsChannelResp;
		SSetAudioSwitchCmd setAudioSwitchCmd;
		SSetAutoAnswerCmd setAutoAnswerCmd;
		SRcvrLanCmd rcvrLanCmd;
		SSetPhoneHookCmd setPhoneHookCmd;
		STaskIdKey taskIdKey;
		SValidateMeasurementResp validateMeasurementResp;
		SValidateOccupancyResp validateOccupancyResp; // Fixed size with 1 band.
		SValidateOccupancyRespV0 validateOccupancyRespV0;
		SValidateOccupancyRespV1 validateOccupancyRespV1;
		SValidateOccupancyRespV2 validateOccupancyRespV2; // Fixed size with 1 band.
		SValidateOccupancyRespV3 validateOccupancyRespV3; // Fixed size with 1 band.
		SValidateOccupancyRespV4 validateOccupancyRespV4; // Fixed size with 1 band.
		SValidateOccupancyRespV5 validateOccupancyRespV5; // Fixed size with 1 band.
		SSounderSetupCmd sounderSweepParamsSetCmd;
		SSounderSetupCmdV1 sounderSweepParamsSetCmdV1;
		SBlankerListCmd sounderBlankerFreqParamsSetCmd;
		SBlankerListCmdV1 sounderBlankerFreqParamsSetCmdV1;
		SSounderSweepCtrlCmd sounderSweepOnOffCtrlCmd;
		SSounderSweepCtrlCmdV1 sounderSweepOnOffCtrlCmdV1;
		SSounderSelfTestCmd sounderSelfTestCmd;
		SSounderSelfTestCmdV1 sounderSelfTestCmdV1;
		SGetSounderStatusResp sounderStatusResp;
		SGetSounderStatusRespV1 sounderStatusRespV1;
		SSounderGenericResp   sounderGenericResp;
		SSounderGenericRespV1   sounderGenericRespV1;
	} body;
#pragma pack(pop)
};

// Version conversion operators
inline SEquipCtrlMsg::SValidateOccupancyRespV0::operator SEquipCtrlMsg::SValidateOccupancyRespV1(void) const
{
	SValidateOccupancyRespV1 v1;
	v1.status = status;
	v1.occCmd = occCmd;

	return v1;
}

inline SEquipCtrlMsg::SValidateOccupancyRespV2& SEquipCtrlMsg::SValidateOccupancyRespV2::operator =(const SValidateOccupancyRespV3& v3) // Convert from V3
{
	ASSERT(!IsOnStack(&v3) || offsetof(SEquipCtrlMsg, body.validateOccupancyRespV3.occCmd.band[v3.occCmd.numBands]) <= sizeof(SEquipCtrlMsg));
	status = v3.status;
	occCmd = v3.occCmd;

	return *this;
}

inline SEquipCtrlMsg::SValidateOccupancyRespV3& SEquipCtrlMsg::SValidateOccupancyRespV3::operator =(const SValidateOccupancyRespV4& v4) //Down-Convert from V4
{
	ASSERT(!IsOnStack(&v4) || offsetof(SEquipCtrlMsg, body.validateOccupancyRespV4.occCmd.band[v4.occCmd.numBands]) <= sizeof(SEquipCtrlMsg));
	status = v4.status;
	occCmd = v4.occCmd;

	return *this;
}

// Message versions (selectany lets this go in the .h file)
#ifdef _MSC_VER
__declspec(selectany)
#else
__attribute__((__weak__))
#endif
const SEquipCtrlMsg::SVersionData SEquipCtrlMsg::VERSION_DATA[] =
{
	{ { SEquipCtrlMsg::AUTOVIOLATE_CTRL, SEquipCtrlMsg::AUTOVIOLATE_BWMEAS_RESULT }, { 2, 1 } },
	{ { SEquipCtrlMsg::AUTOVIOLATE_CTRL, SEquipCtrlMsg::AUTOVIOLATE_FREQMEAS_RESULT }, { 2, 1 } },
	{ { SEquipCtrlMsg::AUTOVIOLATE_CTRL, SEquipCtrlMsg::GET_AUTOVIOLATE }, { 3, 1 } },
	{ { SEquipCtrlMsg::AUTOVIOLATE_CTRL, SEquipCtrlMsg::OCC_FREQUENCY_RESULT }, { 3, 1 } },
	{ { SEquipCtrlMsg::AUTOVIOLATE_CTRL, SEquipCtrlMsg::VALIDATE_AUTOVIOLATE }, { 3, 5 } },
	{ { SEquipCtrlMsg::AUTOVIOLATE_CTRL, SEquipCtrlMsg::VALIDATE_OCC_RESPONSE }, { 3, 5 } },
	{ { SEquipCtrlMsg::DEMOD_CTRL, SEquipCtrlMsg::SET_RCVR }, { 1, 0 } },
	{ { SEquipCtrlMsg::DEMOD_CTRL, SEquipCtrlMsg::SET_PAN_PARA }, { 1, 0 } },
#ifndef SMS_SRV_BUILT
	{ { SEquipCtrlMsg::DEMOD_CTRL, SEquipCtrlMsg::SET_AUDIO_PARAMS }, { 2, 2 } },
	{ { SEquipCtrlMsg::DEMOD_CTRL, SEquipCtrlMsg::SET_AUDIO_PARAMS_RESP }, { 2, 2 } },
	{ { SEquipCtrlMsg::DEMOD_CTRL, SEquipCtrlMsg::FREE_AUDIO_CHANNEL }, { 1, 0 } },
	{ { SEquipCtrlMsg::DEMOD_CTRL, SEquipCtrlMsg::FREE_AUDIO_CHANNEL_RESP }, { 1, 0 } },
#endif
	{ { SEquipCtrlMsg::DF_CTRL, SEquipCtrlMsg::INITIALIZE_DF }, { 2, 4 } },
	{ { SEquipCtrlMsg::DF_CTRL, SEquipCtrlMsg::INITIALIZE_DF_RESPONSE }, { 2, 4 } },
	{ { SEquipCtrlMsg::METRICS_CTRL, SEquipCtrlMsg::GET_BAND_REQUEST }, { 0, 6 } },
	{ { SEquipCtrlMsg::METRICS_CTRL, SEquipCtrlMsg::GET_BAND_RESPONSE }, { 0, 6 } },
	{ { SEquipCtrlMsg::METRICS_CTRL, SEquipCtrlMsg::GET_DWELL }, { 1, 0 } },
	{ { SEquipCtrlMsg::METRICS_CTRL, SEquipCtrlMsg::GET_DWELL_RESPONSE }, { 1, 0 } },
	{ { SEquipCtrlMsg::METRICS_CTRL, SEquipCtrlMsg::GET_MEAS }, { 5, 3 } },
	{ { SEquipCtrlMsg::METRICS_CTRL, SEquipCtrlMsg::GET_MEAS_IQDATA_RESP }, { 0, 1 } },
	{ { SEquipCtrlMsg::METRICS_CTRL, SEquipCtrlMsg::GET_MEAS_RESPONSE }, { 5, 3 } },
	{ { SEquipCtrlMsg::METRICS_CTRL, SEquipCtrlMsg::VALIDATE_MEAS }, { 4, 0 } },
	{ { SEquipCtrlMsg::OCCUPANCY_CTRL, SEquipCtrlMsg::GET_OCCUPANCY }, { 5, 1 } },
	{ { SEquipCtrlMsg::OCCUPANCY_CTRL, SEquipCtrlMsg::OCC_FREQUENCY_RESULT }, { 5, 1 } },
	{ { SEquipCtrlMsg::OCCUPANCY_CTRL, SEquipCtrlMsg::VALIDATE_OCCUPANCY }, { 4, 5 } },
	{ { SEquipCtrlMsg::OCCUPANCY_CTRL, SEquipCtrlMsg::VALIDATE_OCC_RESPONSE }, { 4, 5 } },
	{ { SEquipCtrlMsg::OCCUPANCYDF_CTRL, SEquipCtrlMsg::GET_OCCUPANCY_SCANDF }, { 1, 1 } },
	{ { SEquipCtrlMsg::OCCUPANCYDF_CTRL, SEquipCtrlMsg::OCC_FREQUENCY_RESULT }, { 1, 1 } },
	{ { SEquipCtrlMsg::OCCUPANCYDF_CTRL, SEquipCtrlMsg::VALIDATE_OCCUPANCY_SCANDF }, { 1, 5 } },
	{ { SEquipCtrlMsg::OCCUPANCYDF_CTRL, SEquipCtrlMsg::VALIDATE_OCC_RESPONSE }, { 1, 5 } },
	{ { SEquipCtrlMsg::PAN_DISP_CTRL, SEquipCtrlMsg::GET_PAN }, { 2, 1 } },
	{ { SEquipCtrlMsg::PAN_DISP_CTRL, SEquipCtrlMsg::GET_PAN_AGC }, { 2, 1 } },
	{ { SEquipCtrlMsg::PAN_DISP_CTRL, SEquipCtrlMsg::GET_PAN_RESPONSE }, { 2, 1 } },
	{ { SEquipCtrlMsg::STATUS_CTRL, SEquipCtrlMsg::GET_FAULT_REQUEST }, { 1, 1 } },
#ifndef CSMS_SRV_BUILT
	{ { SEquipCtrlMsg::STATUS_CTRL, SEquipCtrlMsg::GET_FAULT_RESP }, { 1, 1 } },
#endif
#ifndef SMS_SRV_BUILT
	{ { SEquipCtrlMsg::STATUS_CTRL, SEquipCtrlMsg::GET_CSMS_FAULT_RESP }, { 1, 2 } },
#endif
	{ { SEquipCtrlMsg::SOUNDER_CTRL, SEquipCtrlMsg::SOUNDER_SWEEP_SETUP }, { 1, 1 } },
	{ { SEquipCtrlMsg::SOUNDER_CTRL, SEquipCtrlMsg::SOUNDER_SETUP_RESPONSE }, { 1, 1 } },
	{ { SEquipCtrlMsg::SOUNDER_CTRL, SEquipCtrlMsg::SOUNDER_BLANKER_FREQ }, { 1, 1 } },
	{ { SEquipCtrlMsg::SOUNDER_CTRL, SEquipCtrlMsg::SOUNDER_BLANKER_FREQ_RESPONSE }, { 1, 1 } },
	{ { SEquipCtrlMsg::SOUNDER_CTRL, SEquipCtrlMsg::SOUNDER_START_STOP }, { 1, 1 } },
	{ { SEquipCtrlMsg::SOUNDER_CTRL, SEquipCtrlMsg::SOUNDER_START_STOP_RESPONSE }, { 1, 1 } },
	{ { SEquipCtrlMsg::SOUNDER_CTRL, SEquipCtrlMsg::SOUNDER_TEST }, { 1, 1 } },
	{ { SEquipCtrlMsg::SOUNDER_CTRL, SEquipCtrlMsg::SOUNDER_TEST_RESPONSE }, { 1, 1 } },
	{ { SEquipCtrlMsg::SOUNDER_CTRL, SEquipCtrlMsg::SOUNDER_IONOREQUEST }, { 1, 1 } },
	{ { SEquipCtrlMsg::SOUNDER_CTRL, SEquipCtrlMsg::SOUNDER_IONO_RESPONSE }, { 1, 1 } },
	{ { SEquipCtrlMsg::SOUNDER_CTRL, SEquipCtrlMsg::SOUNDER_STATUS_REQ }, { 1, 1 } },
	{ { SEquipCtrlMsg::SOUNDER_CTRL, SEquipCtrlMsg::SOUNDER_STATUS_RESPONSE }, { 1, 1 } }
};

#ifdef _MSC_VER
#pragma warning(pop)
#endif
