/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2005 TCI. ALL RIGHTS RESERVED                                 *
**************************************************************************/

#ifndef msgtypesHdr
#define msgtypesHdr

// message types
#define AUDIO_SWITCH			(6)
#define RF_IF_MUX				(7)
#define CLOCK_SYNTH				(8)
#define IO_DIGITAL_IO			(10)
#define NB_ADC_SAMPLE			(19)
#define WB_DIGITIZER_SAMPLE		(20)
#define INTER_BITE_CTRL			(24)
#define DF_CTRL					(25)
#define METRICS_CTRL			(26)
#define MEASURE_CTRL			(27)
#define DEMOD_CTRL				(28)
#define PAN_DISP_CTRL			(29)
#define OCCUPANCY_CTRL			(30)
#define BITE_CTRL				(31)
#define SCAN_CTRL				(32)
#define OCCUPANCYDF_CTRL		(33)
#define AUTOVIOLATE_CTRL		(34)
#define FAST_MODE_CTRL			(35)
#define EQUIPMENT_CTRL			(50)
#define DSP_CTRL				(51)
#define STATUS_CTRL				(60)
#define ANT_CTRL				(61)
#define SYSTEM_STATE_CTRL		(62)
#define SMS_ERROR_REPORT		(9999)
#define DSP_SHUTDOWN			(8765)
#define MEASUREMENT_BASE			(0x2172)
#define MEASUREMENT_PAN_REQ			(MEASUREMENT_BASE+1)
#define MEASUREMENT_PAN_RES			(MEASUREMENT_BASE+2)
#define MEASUREMENT_GREETING_REQ	(MEASUREMENT_BASE+3)
#define MEASUREMENT_GREETING_RES	(MEASUREMENT_BASE+4)
#define MEASUREMENT_ERROR_REPORT	(MEASUREMENT_BASE+5)

#endif
