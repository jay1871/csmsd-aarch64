/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2014 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "Units.h"

// Message definitions for messages from port 3307 (tci-equiprealtime)
//
// Send a header only to request a version other then 1. Version 0 turns off a specific message
struct SSmsRealtimeMsg
{
	// Realtime port msgType definitions
	enum EMsgType
	{
		RT_SPECTRUM_START	= 1,
		RT_SPECTRUM_STOP	= 2,
		RT_SPECTRUM			= 3,
		RT_DF_START			= 4,
		RT_DF_STOP			= 5,
		RT_DF_DATA			= 6,
		RT_IQ_DATA			= 7
	};

	// Message structure for RT_SPECTRUM_START / RT_DF_START msgType
	struct SStart  // msgTypeVersion = 1
	{
		static const unsigned int MAX_OCCBANDS = 100;

		unsigned long	taskId;							// Task ID (unique number, although can repeat -- not a good collection key)
		unsigned long	numBands;						// Number of tasked bands for this task ID
		unsigned long	firstChanFreq[MAX_OCCBANDS];	// Center RF frequency of the first (lowest) channel (Hz)
		unsigned long	chanSize[MAX_OCCBANDS];		// Channel spacing or bandwidth (Hz)
		unsigned long	numChan[MAX_OCCBANDS];		// Number of channels
	};

	//  this is a variable length message -- only numBands worth of data is sent
	struct SStartV2  // msgTypeVersion = 2
	{
		unsigned long	taskId;		// Task ID (unique number, although can repeat -- not a good collection key)
		unsigned long	numBands;	// Number of tasked bands for this task ID

		struct SBand
		{
			Units::Frequency::Raw	firstChanFreq;	// Center RF frequency of the first (lowest) channel
			Units::Frequency::Raw	chanSize;		// Channel spacing or bandwidth
			unsigned long			numChan;		// Number of channels
		} band[1];
	};

	// Message structure for RT_SPECTRUM_STOP / RT_DF_STOP msgType
	struct SStop  // msgTypeVersion = 1
	{
		unsigned long	taskId;					// taskID (same as RT_SPECTRUM_START / RT_DF_START msgType taskID)
	};

	// Message structure for RT_SPECTRUM_DATA msgType
	//  this is a variable length message -- only numChan worth of data is sent
	struct SSpectrum  // msgTypeVersion = 1
	{
		unsigned long	taskId;			// Task ID (same as RT_SPECTRUM_START msgType taskID)
		unsigned long	bandIndex;		// Band index (for each task ID, goes from 0 to the number of tasked bands - 1)
		unsigned long	firstChanFreq;	// Center RF frequency of the first (lowest) channel (Hz)
		unsigned long	chanSize;		// Channel spacing or bandwidth (Hz)
		unsigned long	numChan;		// Number of channels
		unsigned char	noiseFloor;		// Noise floor (dBm + 200)

		//Variable length array, the size is specifed by numChan above.
		unsigned char	chanData[1];	// Channel spectrum data (dBm + 200)
	};

	struct SSpectrumV2  // msgTypeVersion = 2
	{
		unsigned long			taskId;			// Task ID (same as RT_SPECTRUM_START msgType taskID)
		unsigned long			bandIndex;		// Band index (for each task ID, goes from 0 to the number of tasked bands - 1)
		Units::Frequency::Raw	firstChanFreq;	// Center RF frequency of the first (lowest) channel
		Units::Frequency::Raw	chanSize;		// Channel spacing or bandwidth (Hz)
		unsigned long			numChan;		// Number of channels
		unsigned char			noiseFloor;		// Noise floor (dBm + 200)

		//Variable length array, the size is specifed by numChan above.
		unsigned char			chanData[1];	// Channel spectrum data (dBm + 200)
	};

	//Version 3 has extra field-strength value compared to version 2.
	struct SSpectrumV3  // msgTypeVersion = 3
	{
		unsigned long			taskId;			// Task ID (same as RT_SPECTRUM_START msgType taskID)
		unsigned long			bandIndex;		// Band index (for each task ID, goes from 0 to the number of tasked bands - 1)
		Units::Frequency::Raw	firstChanFreq;	// Center RF frequency of the first (lowest) channel
		Units::Frequency::Raw	chanSize;		// Channel spacing or bandwidth (Hz)

		//Define union to optimize the clearing of unused field.
		union
		{
			unsigned long		zeroVal;
			unsigned char		noiseFloor;	// Noise floor (dBm + 200)
											//unsigned char unused[3]; Always 0.
		};

		unsigned long		numChan;		//Number of channels.

		//Both chanData & efield are variable size array, whose size are
		// determined by numChan value above.
		//@note: efield can only be accessed through pointer due to chanData
		//		 being variable size.
		unsigned char		chanData[1];  	// Channel spectrum data (dBm + 200)
		signed char         efield[1];      //Field strength dbuV/m.
	};

	//This number must match with the latest SSpectrum version.
	static const unsigned long C_LATEST_RT_SPECTRUM_VERSION = 3;

	// Message structure for RT_DF_DATA msgType
	//  this is a variable length message -- only numChan worth of data is sent
	struct SDfData  // msgTypeVersion = 1
	{
		unsigned long	taskId;			// Task ID (same as RT_DF_START msgType taskID)
		unsigned long	bandIndex;		// Band index (for each task ID, goes from 0 to the number of tasked bands - 1)
		unsigned long	firstChanFreq;	// Center RF frequency of the first (lowest) channel (Hz)
		unsigned long	chanSize;		// Channel spacing or bandwidth (Hz)
		unsigned long	numChan;		// Number of channels
		unsigned char	noiseFloor;		// Noise floor (dBm + 200)

		struct SChanData
		{
			unsigned short	azimData;	// Channel lob data (1/100 degree; 0 to 35999)
										// 65535 = no DF result(signal not present, low confidence, etc)
			unsigned char	specData;	// Channel spectrum data (dBm + 200)
		} chanData[1];
	};

	//  this is a variable length message -- only numChan worth of data is sent
	struct SDfDataV2  // msgTypeVersion = 2
	{
		unsigned long	taskId;			// Task ID (same as RT_DF_START msgType taskID)
		unsigned long	bandIndex;		// Band index (for each task ID, goes from 0 to the number of tasked bands - 1)
		unsigned long	firstChanFreq;	// Center RF frequency of the first (lowest) channel (Hz)
		unsigned long	chanSize;		// Channel spacing or bandwidth (Hz)
		unsigned long	numChan;		// Number of channels DF'd
		unsigned char	noiseFloor;		// Noise floor (dBm + 200)
		bool			horizPol;		// Horizontal polarization

		struct SChanData
		{
			unsigned short	chan;		// Channel number (zero-based)
			unsigned short	azimData;	// Channel lob data (1/100 degree; 0 to 35999)
			unsigned short	conf;		// DF confidence (1/10th %, 0 - 1000)
			unsigned char	specData;	// Channel spectrum data (dBm + 200)
			unsigned char	dfSpecData;	// DF antenna spectrum data (dBm + 200)
		} chanData[1];
	};

	//  this is a variable length message -- only numChan worth of data is sent
	struct SDfDataV3  // msgTypeVersion = 3
	{
		unsigned long			taskId;			// Task ID (same as RT_DF_START msgType taskID)
		unsigned long			bandIndex;		// Band index (for each task ID, goes from 0 to the number of tasked bands - 1)
		Units::Frequency::Raw	firstChanFreq;	// Center RF frequency of the first (lowest) channel (Hz)
		Units::Frequency::Raw	chanSize;		// Channel spacing or bandwidth (Hz)
		unsigned long			numChan;		// Number of channels DF'd
		unsigned char			noiseFloor;		// Noise floor (dBm + 200)
		bool					horizPol;		// Horizontal polarization

		struct SChanData
		{
			unsigned short	chan;		// Channel number (zero-based)
			unsigned short	azimData;	// Channel lob data (1/100 degree; 0 to 35999)
			unsigned short	conf;		// DF confidence (1/10th %, 0 - 1000)
			unsigned char	specData;	// Channel spectrum data (dBm + 200)
			unsigned char	dfSpecData;	// DF antenna spectrum data (dBm + 200)
		} chanData[1];
	};

	// Message structure for RT_IQ_DATA msgType
	//  this is a variable length message -- only numIq (up to MAX_IQ) worth of data is sent
	struct SIqData  // msgTypeVersion = 1
	{
		static const unsigned short MAX_IQ = 1024;

		DATE			dateTime;	// Time of first sample (may be 0 if not tagged)
		unsigned long	freq;		// Center frequency (may be 0 if not tagged)
		unsigned long	bw;			// Bandwidth (may be 0 if not tagged)
		unsigned long	sampleRate;	// Sampling rate
		unsigned short	numIq;		// Number of samples
		unsigned char	rxAtten;	// Receiver attenuation
		signed char		scale;		// Scale factor (dB - FSD in IQ data represents 0 dBm before scaling)
		char			rxChan;		// Channel (0, 1)

		struct SIq
		{
			short q;
			short i;
		} iq[MAX_IQ];
	};

	//  this is a variable length message -- only numIq (up to MAX_IQ) worth of data is sent
	struct SIqDataV2  // msgTypeVersion = 2
	{
		static const unsigned short MAX_IQ = 1024;

		DATE					dateTime;	// Time of first sample (may be 0 if not tagged)
		Units::Frequency::Raw	freq;		// Center frequency (may be 0 if not tagged)
		Units::Frequency::Raw	bw;			// Bandwidth (may be 0 if not tagged)
		unsigned long			sampleRate;	// Sampling rate
		unsigned short			numIq;		// Number of samples
		unsigned char			rxAtten;	// Receiver attenuation
		signed char				scale;		// Scale factor (dB - FSD in IQ data represents 0 dBm before scaling)
		char					rxChan;		// Channel (0, 1)

		struct SIq
		{
			short q;
			short i;
		} iq[MAX_IQ];
	};

	//  this is a variable length message -- only numIq (up to MAX_IQ) worth of data is sent
	// Since body will start on 4-byte alignment (pack(4) in message format), this struct also uses pack(4) so that the
	// double and long long variables will be properly aligned.
#pragma pack(push, 4)
	struct SIqDataV3  // msgTypeVersion = 3
	{
		static const unsigned short MAX_IQ = 1024;
// NOTE: Because header is 12 bytes long and pack is 4, the body starts on a 4-byte alignment.
//		 So, the order of the variables in this version has been changed to give correct alignment
		unsigned long			sampleRate;	// Sampling rate
		DATE					dateTime;	// Time of first sample (may be 0 if not tagged)
		Units::Frequency::Raw	freq;		// Center frequency (may be 0 if not tagged)
		Units::Frequency::Raw	bw;			// Bandwidth (may be 0 if not tagged)
		unsigned short			numIq;		// Number of samples
		unsigned char			rxAtten;	// Receiver attenuation
		signed char				scale;		// Scale factor (dB - FSD in IQ data represents 0 dBm before scaling)
		unsigned char			rxChan;		// Channel (0, 4)
		unsigned char 			pad[3];		// Make sure alignment of iq[] is on 4-byte boundary

		struct SIq
		{
			short q;
			short i;
		} iq[MAX_IQ];
	};
#pragma pack(pop)

	typedef struct { short re; short im; }   TciInt16c;    // same as Ipp16sc
	typedef struct { int re;   int im; }     TciInt32c;    // same as Ipp32sc
	typedef struct { float re; float im; }   TciFloat32c;  // same as Ipp16fc

	enum DataType {
		DataType_Int16c = 0,
		DataType_Int32c = 1,
		DataType_Float32c = 2
	};

#pragma pack(push, 4)
	// IQ_DATA
	struct SIqDataV4  // msgTypeVersion = 4
	{
		static const unsigned short MAX_SAMPLES = 1024;

		unsigned long	       streamID;          // Stream ID 
		Units::Timestamp::Raw  streamStartTime;   // Time of the very first sample for the stream 
		unsigned long long     sampleOffset;       // Offset of this data set from the beginning of the stream 
		Units::Frequency::Raw  freq;	          // Center frequency (may be 0 if not tagged)
		double		 		   actualSampleRate;        // sampling rate
		Units::Frequency::Raw  actualBW;		          // Bandwidth (may be 0 if not tagged)
		unsigned char          ddcChannel;        // Channel (0, 4)
		unsigned short         seqNumber ;       // Frame sequence number (starts with 0, wraps) (to compute lost msg/blocks)
		double                 scaleFactor;       // TBD
		unsigned char          inputPort;         // Input/Antenna port (enums TBD)	
		unsigned char	       rxAtten;	          // Receiver attenuation (dB)
		DataType               dataType;          // type of data delivered below 
		unsigned char 		   EOS;               // non-zero means EOS  
		unsigned short         numSamples;        // Can be less than MAX_SAMPLES; if so message length is truncated; 0 => QUERY response
//		unsigned char 			pad[n];		      // Make sure alignment of iq[] is on 4-byte boundary

		union SIqType {
			TciInt16c samplesInt16[MAX_SAMPLES];     // Samples
			TciInt32c samplesInt32[MAX_SAMPLES];    // Samples
			TciFloat32c samplesFloat32[MAX_SAMPLES]; // Samples
		} SIq;
	};
#pragma pack(pop)

	// Message format
#pragma pack(push, 4)
	struct SHdr
	{
		unsigned long msgType;
		unsigned long msgTypeVersion;
		unsigned long bodySize;
	} hdr;

	union
	{
		SStart		start;
		SStartV2	startV2;
		SStop		stop;
		SSpectrum	spectrum;
		SSpectrumV2	spectrumV2;
		SSpectrumV3	spectrumV3;
		SDfData		dfData;
		SDfDataV2	dfDataV2;
		SDfDataV3	dfDataV3;
		SIqData		iqData;
		SIqDataV2	iqDataV2;
		SIqDataV3	iqDataV3;
		SIqDataV4	iqDataV4;
	} body;
#pragma pack(pop)

	// new and delete operators for variable-length messages longer than the union body
	static void* operator new(size_t size, size_t sizeOverride = 0)
		{ return malloc(sizeOverride <= size ? size : sizeOverride); }
	static void operator delete(void* ptr, size_t) { free(ptr); }

	static void Realloc(SSmsRealtimeMsg*& msg, size_t size)
	{
		void* newMsg = realloc(msg, size);
	
		if(newMsg == nullptr)
		{
			ASSERT(FALSE);
			throw std::bad_alloc();
		}

		msg = static_cast<SSmsRealtimeMsg*>(newMsg);
	}

#ifdef _DEBUG
	static void* operator new(size_t size, LPCSTR fileName, int line, size_t sizeOverride = 0)
		{ return _malloc_dbg(sizeOverride <= size ? size : sizeOverride, _NORMAL_BLOCK, fileName, line); }
	static void operator delete(void* ptr, LPCSTR, int, size_t) { _free_dbg(ptr, _NORMAL_BLOCK); }

	static void Realloc(SSmsRealtimeMsg*& msg, size_t size, LPCSTR fileName, int line)
	{
		void* newMsg = _realloc_dbg(msg, size, _NORMAL_BLOCK, fileName, line);
	
		if(newMsg == nullptr)
		{
			ASSERT(FALSE);
			throw std::bad_alloc();
		}

		msg = static_cast<SSmsRealtimeMsg*>(newMsg);
	}
#endif
};
