/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "EquipControl.h"
//#include "Metrics.h"
#include "MetricsMsg.h"
#include "NetConnection.h"
#include "RWLock.h"
#include "Singleton.h"

class CMetrics;

class CMetricsNet : public CNetConnection<CMetricsNet>
{
public:
	// Types
	enum ESaveClientInfoResult
	{
        SAVED = 0,
        NULL_SOCK_PTR = 1,
        NOT_AF_INET = 2
	};

	// Functions
	CMetricsNet(LPCTSTR service); // Server
	CMetricsNet(SOCKET socket, _In_ CMetricsNet* server); // Dynamic connection to server
	~CMetricsNet();
	SOCKADDR_STORAGE GetLocalAddress(void) const;
	static _Success_(return == SAVED) ESaveClientInfoResult SaveClientInfo(_Out_ SMetricsMsg::SClientInfo & clientInfo, _In_ const ADDRINFOT* pPeer);
	static bool	Send(_In_ CMetricsNet* client, _In_bytecount_(size) const void* msg, size_t size)
		{ return CNetConnection<CMetricsNet>::Send(client, msg, size); }
	virtual bool Send(_In_bytecount_(size) const void* msg, size_t size);
	static void Send(_In_ CMetricsNet* client, _In_ const SMetricsMsg& msg, _In_opt_ LPCTSTR log = nullptr, ...);
	void SendError(_In_ const SMetricsMsg::SHdr& hdr, unsigned long code);
	void SendImmediateResp(_In_ const SMetricsMsg& msg, _In_opt_ LPCTSTR pLog = nullptr);

private:
	// Functions
	inline virtual size_t BodySize(_In_ const void* hdr) const;
	static bool DownConvert(_Inout_ const SMetricsMsg*& msg);
	virtual size_t HeaderSize(void) const { return sizeof(SMetricsMsg::SHdr); }
	static bool IsBodyCompressed(_In_ const SMetricsMsg& msg);
	virtual void OnClose(int errorCode); // Callback
	virtual void OnConnect(void); // Callback
	virtual void OnMessage(_In_bytecount_(size) const void* msg, size_t size); // Callback
	void OnStatusCtrl(_In_ const SMetricsMsg& msg);
	void UpConvert(_Inout_ SMetricsMsg*& msg);
	void UpConvertMeasureCtrl(_Inout_ SMetricsMsg*& msg);

	// Data
	SMetricsMsg::VersionMap m_clientVersionMap;
	static SMetricsMsg::CompressionMap m_compressionMap;
	static CRWLock m_critSect;
	CSingleton<CMetrics> m_metrics; // Put this before m_equipControl so it's constructed first
	CSingleton<CEquipControl> m_equipControl;
	DWORD m_maxRecvBodySize;
	SMetricsMsg* m_recvMsg;
	void* m_sendMsg;
//	bool m_usesVersions;
	static SMetricsMsg::VersionMap m_versionMap;

};

