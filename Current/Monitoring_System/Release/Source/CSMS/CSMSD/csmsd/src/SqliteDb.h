/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "sqlite3.h"

class CSqliteDb
{
public:
	CSqliteDb(void);
	~CSqliteDb(void);

protected:
	// Functions
	void CheckHr(int hr) const;
	void LogHr(int hr) const;

	// Data
	sqlite3* m_db;

private:
	static bool CopyFile(const char* inFile, const char* outFile);
	bool Execute(const char* sql, int& value);
	bool Execute(const char* sql, std::vector<std::string>& value);
    void OpenRecover(void);

	// Data
	std::string m_dbFile;
};

