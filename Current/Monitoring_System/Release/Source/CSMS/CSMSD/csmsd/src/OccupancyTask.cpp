/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2015 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"

#include "OccupancyTask.h"
#include "Utility.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
const float COccupancyTask::FRACTION_DONE_NEEDED = 0.8f;
const float COccupancyTask::NOISE_TIME_CONSTANT = 0.5f;


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
COccupancyTask::COccupancyTask(_In_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source) try :
	CTask(msg, source /*, source == nullptr ? BACKGROUND : IMMEDIATE*/), // CTask is a virtual base
	CSuspendableTask(),
	m_count(0),
	m_currTodBin(0),
	m_eFieldData(),
	m_msgLenDistData(),
	m_noise(m_taskParams.size()),
	m_numLooks(0),
	m_numTods(0),
	m_occData(),
	m_prevNumLooks(0),
	m_resetDwell(true),
	m_spectrogramData(),
	m_todData(),
	m_todEFields(),
	m_todNumLooks(),
	m_todPrevNumLooks(),
	m_watts()
{
	const SEquipCtrlMsg::SGetOccupancyCmd& cmd = msg->body.getOccupancyCmd;

	// Initialize
	m_fixedThresh1 = (cmd.thresholdMethod != SEquipCtrlMsg::NOISE ? pow(10.0f, cmd.occPrimaryThreshold[0] / 10.0f) : 0.0f);
//	m_fixedThresh2 = (cmd.thresholdMethod != SEquipCtrlMsg::NOISE ? pow(10.0f, cmd.occSecondaryThreshold[0] / 10.0f) : 0.0f);
	m_minGap = cmd.occupancyMinGap / double(Units::SECONDS_PER_DAY);
	m_noiseThresh1 = (cmd.thresholdMethod != SEquipCtrlMsg::FIXED ? pow(10.0f, cmd.occPrimaryThreshold[1] / 10.0f) : 0.0f);
//	m_noiseThresh2 = (cmd.thresholdMethod != SEquipCtrlMsg::FIXED ? pow(10.0f, cmd.occSecondaryThreshold[1] / 10.0f) : 0.0f);
	m_outputFlags = cmd.output;

	if (m_outputFlags.occupancyVsChannel)
	{
		m_occData.resize(m_bandOffset.back() + m_bandSize.back());
	}

	if (m_outputFlags.eFieldVsChannel)
	{
		m_eFieldData.resize(m_bandOffset.back() + m_bandSize.back());
	}

	if (m_outputFlags.msglengthVsChannel || m_outputFlags.msglengthDistribution)
	{
		m_msgLenData.resize(m_bandOffset.back() + m_bandSize.back());
	}

	if (m_outputFlags.spectrogram)
	{
		m_spectrogramData.resize(m_bandOffset.back() + m_bandSize.back());
	}

	m_useSecondaryThreshold = cmd.useSecondaryThreshold;

	if (m_useSecondaryThreshold)
	{
		throw ErrorCodes::INVALIDTHRESHOLDMETHOD; // TODO
	}

	if (cmd.saveIntermediateData)
	{
		throw ErrorCodes::INVALIDOCCDATA; // TODO
	}

	// Check threshold method
	if (cmd.thresholdMethod != SEquipCtrlMsg::FIXED &&
		cmd.thresholdMethod != SEquipCtrlMsg::NOISE &&
		cmd.thresholdMethod != SEquipCtrlMsg::BOTH)
	{
		throw ErrorCodes::INVALIDTHRESHOLDMETHOD;
	}

	// Calculate the factor used in the confidence level duration method
	// This function returns a value that very closely approximates
	// the value from ITU-R SM.182-4 for determining the number of
	// required dependent samples to achieve a desired accuracy
	if (cmd.durationMethod == SEquipCtrlMsg::CONFIDENCE)
	{
		if (!cmd.output.occupancyVsChannel || cmd.output.occupancyVsTimeOfDay)
		{
			throw ErrorCodes::INVALIDDURATIONMETHOD;
		}

		double z;

		if (cmd.confidenceLevel == 90)
		{
			z = 1.644853;
		}
		else if (cmd.confidenceLevel == 95)
		{
			z = 1.959961082;
		}
		else if (cmd.confidenceLevel == 99)
		{
			z = 2.575834515;
		}
		else
		{
			throw ErrorCodes::OCCINVALIDCONFLEVEL;
		}

		if (cmd.desiredAccuracy < 5 || cmd.desiredAccuracy > 100)
		{
			throw ErrorCodes::OCCINVALIDACCURACY;
		}

		double accuracy = cmd.desiredAccuracy / 100.0; // Relative accuracy
		m_confFactor = 3.156 * (z / accuracy) * (z / accuracy); // 3.156 is empirically determined
		m_measurementTime = Units::SECONDS_PER_DAY;
	}
	else if (cmd.durationMethod == SEquipCtrlMsg::TIME)
	{
		if (cmd.measurementTime < cmd.storageTime)
		{
			throw ErrorCodes::INVALIDSTORAGETIME;
		}

		m_confFactor = 0;
		m_measurementTime = cmd.measurementTime;
	}
	else
	{
		throw ErrorCodes::INVALIDDURATIONMETHOD;
	}

	bool hasTod = false;

	if (m_outputFlags.occupancyVsTimeOfDay)
	{
		if (cmd.storageTime == 0)
		{
			throw ErrorCodes::INVALIDSTORAGETIME;
		}

		if ((m_numTods = std::min(86400ul, cmd.measurementTime) / cmd.storageTime) > 288)
		{
			// Too many intervals in a day
			throw ErrorCodes::INVALIDSTORAGETIME;
		}

		m_todData.resize(m_numTods * (m_bandOffset.back() + m_bandSize.back()));
		m_todNumLooks.assign(m_numTods, 0);
		m_todPrevNumLooks.assign(m_numTods, 0);
		hasTod = true;
	}
	else
	{
//		m_report.body.occResult.occHdr.numTimeOfDays = 0;
	}

	if (m_outputFlags.occupancyFlag.flag.eFieldVsTOD != 0)
	{
		if (!hasTod)
		{
			if (cmd.storageTime == 0)
			{
				throw ErrorCodes::INVALIDSTORAGETIME;
			}

			if ((m_numTods = std::min(86400ul, cmd.measurementTime) / cmd.storageTime) > 288)
			{
				// Too many intervals in a day
				throw ErrorCodes::INVALIDSTORAGETIME;
			}

			m_todNumLooks.assign(m_numTods, 0);
			m_todPrevNumLooks.assign(m_numTods, 0);
		}

		m_todEFields.resize(m_numTods * (m_bandOffset.back() + m_bandSize.back()));
	}

	// Get start time rounded down to second
//	TciGps::GetDATE(m_startTime);
	m_startTime = Utility::CurrentTimeAsDATE();
	m_startTime -= fmod(m_startTime, 1.0 / Units::SECONDS_PER_DAY);

	// Size vectors
	for (size_t band = 0; band < m_noise.size(); ++band)
	{
		m_noise[band].resize(m_taskParams[band].numBlocks);
	}

	return;
}
catch(std::bad_alloc)
{
	throw ErrorCodes::OCCUPANCYTOOMANYCHANS;
}


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
COccupancyTask::COccupancyTask(_In_ SRestartData& restartData) :
	CTask(restartData), // CTask is a virtual base
	CSuspendableTask(restartData),
	m_count(0),
	m_noise(),
	m_resetDwell(true),
	m_watts()
{
	// Read from blob into member data
	ReadData(restartData, m_confFactor);
	ReadData(restartData, m_fixedThresh1);
//	ReadData(restartData, m_fixedThresh2);
	ReadData(restartData, m_measurementTime);
	ReadData(restartData, m_minGap);
	ReadData(restartData, m_noiseThresh1);
//	ReadData(restartData, m_noiseThresh2);
	ReadData(restartData, m_numLooks);
	ReadData(restartData, m_numTods);
	ReadData(restartData, m_prevNumLooks);
	ReadData(restartData, m_currTodBin);
	ReadData(restartData, m_startTime);
	ReadData(restartData, m_outputFlags);
	ReadData(restartData, m_useSecondaryThreshold);
	ReadData(restartData, m_eFieldData);
	ReadData(restartData, m_msgLenData);
	ReadData(restartData, m_msgLenDistData);
	ReadData(restartData, m_occData);
	ReadData(restartData, m_spectrogramData);
	ReadData(restartData, m_todData);
	ReadData(restartData, m_todEFields);
	ReadData(restartData, m_todNumLooks);
	ReadData(restartData, m_todPrevNumLooks);
//	unsigned int checksum1 = m_checksum;
//	unsigned int checksum2;
//	ReadData(restartData, checksum2);
//
//	if(checksum1 != checksum2)
//	{
//		throw ErrorCodes::UNABLETOGETDATA;
//	}

	// Size vectors
	m_noise.resize(m_taskParams.size());

	for(size_t band = 0; band < m_noise.size(); ++band)
	{
		m_noise[band].resize(m_taskParams[band].numBlocks);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
COccupancyTask::~COccupancyTask(void)
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Abandon the current dwell so task can yield immediately
//
void COccupancyTask::AbandonDwell(void)
{
//	TRACE("COccupancyTask::AbandonDwell %d -> true\n", m_resetDwell);
	// Base class
	CTask::AbandonDwell();

	// Member data
	m_resetDwell = true;
}


//////////////////////////////////////////////////////////////////////
//
// Update occupancy measurements
//
void COccupancyTask::AddBlock(_In_ const Ne10F32Vec& watts, bool partialBlock)
{
//	TRACE("COccupancyTask::AddBlock %u %d\n", watts.size(),  m_resetDwell);
	if (m_resetDwell)
	{
		m_watts.resize(watts.size());
		m_watts = 0;
		m_count = 0;
		m_resetDwell = false;
	}
	else
	{
		ASSERT(m_watts.size() == watts.size());
	}

#if 0
	// DEBUG CODE:
	if (m_count == 0)
	{
		bool nonzero = false;
		for (size_t i = 0; i < watts.size(); ++i)
		{
			if (!nonzero && watts[i] != 0)
			{
				printf("non-zero watts at %u\n", i);
				nonzero = true;
			}
			if (nonzero && watts[i] == 0)
			{
				printf("zero watts at %u\n", i);
				nonzero = false;
			}
		}
		nonzero = false;
		for (size_t i = 0; i < m_watts.size(); ++i)
		{
			if (!nonzero && m_watts[i] != 0)
			{
				printf("non-zero m_watts at %u\n", i);
				nonzero = true;
			}
			if (nonzero && m_watts[i] == 0)
			{
				printf("zero m_watts at %u\n", i);
				nonzero = false;
			}
		}
	}
#endif
	m_watts += watts;

	if (!partialBlock)
	{
		++m_count;
	}
//	printf("AddBlock: set m_watts %u %u\n", m_watts.size(), m_count);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Add terminated data to the current block
//
void COccupancyTask::AddTerminatedDataBlock(_In_ const Ne10F32Vec& terminatedData, size_t step, size_t block, bool partialBlock)
{
	// This must be called before AddBlock() - think about combining it

	if (m_hasTerminatedData[step][block])
	{
		return;
	}
	if (m_terminatedData[step][block].size() == 0)
	{
		m_terminatedData[step][block] = terminatedData;
//		printf("AddTerminatedDataBlock: set m_terminatedData %u %u %u\n", step, block, terminatedData.size());
	}
	else
	{
		ASSERT(m_terminatedData[step][block].size() == terminatedData.size());
		m_terminatedData[step][block] += terminatedData;
//		printf("AddTerminatedDataBlock: update m_terminatedData %u %u %u\n", step, block, terminatedData.size());
	}

	if (!partialBlock)
	{
		m_hasTerminatedData[step][block] = true;
//		printf("AddTerminatedDataBlock: set m_hasTerminatedData %u %u true\n", step, block);
	}
//	else
//	{
//		printf("AddTerminatedDataBlock: partialBlock %u %u\n", step, block);
//	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Convert the power value collected into Field Strength's dbuvm.
//
inline SEquipCtrlMsg::SOccResult::SResultData COccupancyTask::CalcEFieldValue(_Inout_ SEFieldDataValue& value, unsigned long numLooks)
{
	SEquipCtrlMsg::SOccResult::SResultData data;

	if (numLooks > 0)
	{
		if (value.numAbove > 0)
		{
			int dbuvm = int(10 * log10(value.sumEField / value.numAbove) + 0.5f);

			if (dbuvm > 126)
			{
				dbuvm = 126;
			}
			else if (dbuvm < -128)
			{
				dbuvm = -128;
			}

			data.avg = static_cast<signed char>(dbuvm);
			dbuvm = int(10 * log10(value.maxEField) + 0.5f);

			if (dbuvm > 126)
			{
				dbuvm = 126;
			}
			else if (dbuvm < -128)
			{
				dbuvm = -128;
			}

			data.max = static_cast<signed char>(dbuvm);
		}
		else
		{
			data.avg = 127;
			data.max = 127;
		}
	}
	else
	{
		data.avg = 127;
		data.max = 127;
	}

	return data;
}


//////////////////////////////////////////////////////////////////////
//
// Create task from message and return shared pointer
//
COccupancyTask::OccupancyTask COccupancyTask::Create(_In_ const SEquipCtrlMsg* cmd, _In_opt_ CNetConnection<void>* source)
{
	OccupancyTask task(new COccupancyTask(cmd, source), Deleter());
	task->RegisterTask(true);

	return task;
}


//////////////////////////////////////////////////////////////////////
//
// Create task from restart data
//
void COccupancyTask::Create(_In_ SRestartData& restartData)
{
	RegisterTask(OccupancyTask(new COccupancyTask(restartData), Deleter()));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Apply enhanced dynamic range algorithm to block
//
void COccupancyTask::EnhanceBlock(_In_ const Ne10F32Vec& terminatedData, unsigned long numBins, float noiseFloor)
{
	// Note: when this is called, m_watts is the sum of m_count spectra. terminatedData is from one spectrum.
	// TODO: save the averaged spectrum in the class to be be used in GetChanWatts(?)
	if (m_watts.size() == terminatedData.size())
	{
		if (m_count > 1) m_watts /= float(m_count);
		float noise = CNoiseEstimate::Create(m_watts, numBins, 0)->GetNoise();
		m_watts -= terminatedData;
		ne10sThreshold_LTVal(m_watts, noiseFloor, noise);
		if (m_count > 1) m_watts *= float(m_count);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback to fill in message data structure
//
SEquipCtrlMsg::SOccResult::SResultData COccupancyTask::GetChannelMsgLenData(_Inout_ SMsgLenValue& value, size_t)
{
	SEquipCtrlMsg::SOccResult::SResultData data;
	double avg = (value.numMsg ? value.sumLen * Units::SECONDS_PER_DAY / value.numMsg : 0);

	if (avg > 127)
	{
		data.avg = 127;
	}
	else
	{
		data.avg = static_cast<signed char>(avg + 0.5);
	}

	double max = value.maxLen * Units::SECONDS_PER_DAY;

	if(max > 127)
	{
		data.max = 127;
	}
	else
	{
		data.max = static_cast<signed char>(max + 0.5);
	}

	return data;
}


//////////////////////////////////////////////////////////////////////
//
// Callback to fill in message data structure
//
SEquipCtrlMsg::SOccResult::SResultData COccupancyTask::GetChannelOccData(_Inout_ SOccDataValue& value, size_t)
{
	if (m_numLooks > m_prevNumLooks)
	{
		float maxOcc = float((value.numAbove - value.prevNumAbove)) / (m_numLooks - m_prevNumLooks);
		ASSERT(maxOcc <= 1);

		if (maxOcc > value.maxOcc)
		{
			value.maxOcc = maxOcc;
		}
	}

	value.prevNumAbove = value.numAbove;
	SEquipCtrlMsg::SOccResult::SResultData data;
	ASSERT(value.numAbove <= m_numLooks);
	data.avg = (m_numLooks ? static_cast<signed char>(100 * double(value.numAbove) / m_numLooks + 0.5) : 0);
	data.max = static_cast<signed char>(100 * value.maxOcc + 0.5f);

	return data;
}


//////////////////////////////////////////////////////////////////////
//
// Callback to fill in message data structure
//
SEquipCtrlMsg::SOccResult::SResultData COccupancyTask::GetChannelTodData(_Inout_ SOccDataValue& value, size_t chan)
{
	size_t todBin = chan % m_numTods;
	SEquipCtrlMsg::SOccResult::SResultData data;
	ASSERT(value.numAbove <= m_todNumLooks[todBin]);
	data.avg = (m_todNumLooks[todBin] ? static_cast<signed char>(100 * double(value.numAbove) / m_todNumLooks[todBin] + 0.5) : 0);
	data.max = std::max(data.avg, static_cast<signed char>(100 * value.maxOcc + 0.5f));

	return data;
}


//////////////////////////////////////////////////////////////////////
//
// Get power at end of dwell and convert bins to channels
//
void COccupancyTask::GetChanWatts(size_t band, size_t block, unsigned long binsPerChan, unsigned long numChan, Ne10F32Vec& wattsRT, float enbw)
{
	if (m_count > 0)
	{
		if (m_count > 1)
		{
			m_watts /= float(m_count);
		}

		// Update noise estimate from m_watt before resize
		if (m_noise[band][block])
		{
			m_noise[band][block]->Update(m_watts, NOISE_TIME_CONSTANT, enbw);
		}
		else
		{
			m_noise[band][block] = CNoiseEstimate::Create(m_watts, numChan, 0, CNoiseEstimate::DEFAULT_SNR, binsPerChan, enbw);
		}

		m_outputFlags.occupancyFlag.flag.spurTest = 0; // force sum method for spur test for now

		if (m_outputFlags.occupancyFlag.flag.spurTest == 0)
		{
			for (unsigned int chan = 0; chan < numChan; ++chan)
			{
				// Sum bin in channel - note: was Max bin before 2/16/17
				ne10sSum(&m_watts[chan * binsPerChan + 1], binsPerChan - 2, &wattsRT[chan]); // Exclude guard bins
			}
			m_watts = wattsRT;
		}
		else
		{
			int maxbinindex;
			for (unsigned int chan = 0; chan < numChan; ++chan)
			{
				// Max bin in channel
				ne10sMaxandMaxIndex(&m_watts[chan * binsPerChan + 1], binsPerChan - 2, &wattsRT[chan], maxbinindex); // Exclude guard bins
				if (maxbinindex > 2 && maxbinindex < int (binsPerChan - 3))
				{
					ne10sSum(&m_watts[chan * binsPerChan + 1 + maxbinindex - 3], 5, &wattsRT[chan]); // sum 4 bins around max bin
				}
				else
				{
					ne10sSum(&m_watts[chan * binsPerChan + 1], binsPerChan - 2, &wattsRT[chan]); // Exclude guard bins
				}
				// Sum bins in channel
				ne10sSum(&m_watts[chan * binsPerChan + 1], binsPerChan - 2, &m_watts[chan]); // Exclude guard bins
			}
		}
		m_watts.resize(numChan);
		m_watts /= enbw;
		wattsRT /= enbw;		// SS 05/04/17
		m_count = 0;
	}
	else
	{
		m_watts.resize(numChan);
		m_watts = 0;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback to fill in message data structure
//
SEquipCtrlMsg::SOccResult::SResultData COccupancyTask::GetEFieldData(_Inout_ SEFieldDataValue& value, size_t)
{
	return(CalcEFieldValue(value, m_numLooks));
}


//////////////////////////////////////////////////////////////////////
//
// Callback to fill in Field Strength's dbuvm of the message data structure.
//
SEquipCtrlMsg::SOccResult::SResultData COccupancyTask::GetEFieldTodData(_Inout_ SEFieldDataValue& value, size_t chan)
{
	size_t todBin = chan % m_numTods;
	ASSERT(value.numAbove <= m_todNumLooks[todBin]);
	return(CalcEFieldValue(value, m_todNumLooks[todBin]));
}


//////////////////////////////////////////////////////////////////////
//
// Callback to fill in message data structure
//
SEquipCtrlMsg::SOccResult::SResultData COccupancyTask::GetSpectrogramData(_Inout_ SEFieldDataValue& value, size_t)
{
	SEquipCtrlMsg::SOccResult::SResultData data;
	int dbuvm = int(10 * log10(value.sumEField / value.numAbove) + 0.5f);

	if(dbuvm > 126)
	{
		dbuvm = 126;
	}
	else if(dbuvm < -128)
	{
		dbuvm = -128;
	}

	data.avg = static_cast<signed char>(dbuvm);
	dbuvm = int(10 * log10(value.maxEField) + 0.5f);

	if(dbuvm > 126)
	{
		dbuvm = 126;
	}
	else if(dbuvm < -128)
	{
		dbuvm = -128;
	}

	data.max = static_cast<signed char>(dbuvm);

	return data;
}


//////////////////////////////////////////////////////////////////////
//
// Save data for restart
//
void COccupancyTask::SaveRestartData(_Inout_ std::vector<unsigned char>& data) const
{
	// Base classes
	CTask::SaveRestartData(data);
	CSuspendableTask::SaveRestartData(data);

	// Member data
	SaveData(data, m_confFactor);
	SaveData(data, m_fixedThresh1);
//	SaveData(data, m_fixedThresh2);
	SaveData(data, m_measurementTime);
	SaveData(data, m_minGap);
	SaveData(data, m_noiseThresh1);
//	SaveData(data, m_noiseThresh2);
	SaveData(data, m_numLooks);
	SaveData(data, m_numTods);
	SaveData(data, m_prevNumLooks);
	SaveData(data, m_currTodBin);
	SaveData(data, m_startTime);
	SaveData(data, m_outputFlags);
	SaveData(data, m_useSecondaryThreshold);
	SaveData(data, m_eFieldData);
	SaveData(data, m_msgLenData);
	SaveData(data, m_msgLenDistData);
	SaveData(data, m_occData);
	SaveData(data, m_spectrogramData);
	SaveData(data, m_todData);
	SaveData(data, m_todEFields);
	SaveData(data, m_todNumLooks);
	SaveData(data, m_todPrevNumLooks);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Report interim or final results
//
void COccupancyTask::SendReports(bool last)
{
	if (m_outputFlags.occupancyVsChannel)
	{
		m_report.body.occResult.occHdr.status = ErrorCodes::SUCCESS;
		m_report.body.occResult.occHdr.numTimeOfDays = 0;
		CSuspendableTask::SendReports(SEquipCtrlMsg::OCC_CHANNEL_RESULT, _T("OCC_CHANNEL_RESULT"),
			m_report.body.occResult, m_report.body.occResult.resultData, m_occData, &COccupancyTask::GetChannelOccData, last);
	}

	if (m_outputFlags.eFieldVsChannel || m_outputFlags.timegram)
	{
		m_report.body.occResult.occHdr.status = ErrorCodes::SUCCESS;
		m_report.body.occResult.occHdr.numTimeOfDays = 0;
		CSuspendableTask::SendReports(SEquipCtrlMsg::EFLD_CHANNEL_RESULT, _T("EFLD_CHANNEL_RESULT"),
			m_report.body.occResult, m_report.body.occResult.resultData, m_eFieldData, &COccupancyTask::GetEFieldData, last);
	}

	m_prevNumLooks = m_numLooks;

	if (m_outputFlags.occupancyVsTimeOfDay)
	{
		m_report.body.occResult.occHdr.status = ErrorCodes::SUCCESS;
		m_report.body.occResult.occHdr.numTimeOfDays = m_numTods;
		CSuspendableTask::SendReports(SEquipCtrlMsg::OCC_TIMEOFDAY_RESULT, _T("OCC_TIMEOFDAY_RESULT"),
			m_report.body.occResult, m_report.body.occResult.resultData, m_todData, &COccupancyTask::GetChannelTodData, last);
	}

	if (m_outputFlags.occupancyFlag.flag.eFieldVsTOD != 0)
	{
		m_report.body.occResult.occHdr.status = ErrorCodes::SUCCESS;
		m_report.body.occResult.occHdr.numTimeOfDays = m_numTods;
		CSuspendableTask::SendReports(SEquipCtrlMsg::OCC_EFLD_TIMEOFDAY_RESULT, _T("OCC_EFLD_TIMEOFDAY_RESULT"),
			m_report.body.occResult, m_report.body.occResult.resultData, m_todEFields, &COccupancyTask::GetEFieldTodData, last);
	}

	if (m_outputFlags.msglengthVsChannel)
	{
		m_report.body.occResult.occHdr.status = ErrorCodes::SUCCESS;
		m_report.body.occResult.occHdr.numTimeOfDays = 0;
		CSuspendableTask::SendReports(SEquipCtrlMsg::MSGLEN_CHANNEL_RESULT, _T("MSGLEN_CHANNEL_RESULT"),
			m_report.body.occResult, m_report.body.occResult.resultData, m_msgLenData, &COccupancyTask::GetChannelMsgLenData, last);
	}

	if (m_outputFlags.msglengthDistribution)
	{
		m_report.body.occResult.occHdr.status = ErrorCodes::SUCCESS;
		m_report.body.occResult.occHdr.numTimeOfDays = 0;
		CSuspendableTask::SendReports(SEquipCtrlMsg::MSGLEN_DIST_RESPONSE, _T("MSGLEN_DIST_RESPONSE"),
			m_report.body.msgLengthDistributionResp, m_report.body.msgLengthDistributionResp.histData,
			m_msgLenDistData, &COccupancyTask::GetChannelMsgLenDistData, last);
		m_msgLenDistData.clear();
	}

	if (m_outputFlags.spectrogram)
	{
		m_report.body.occResult.occHdr.status = ErrorCodes::SUCCESS;
		m_report.body.occResult.occHdr.numTimeOfDays = 0;
		CSuspendableTask::SendReports(SEquipCtrlMsg::OCC_SPECGRAM_RESULT, _T("OCC_SPECGRAM_RESULT"),
			m_report.body.occResult, m_report.body.occResult.resultData, m_spectrogramData, &COccupancyTask::GetSpectrogramData, last);
		m_spectrogramData.assign(m_spectrogramData.size(), SEFieldDataValue());
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update data at end of dwell
//
void COccupancyTask::Update(size_t band, size_t block, Units::Frequency firstChanFreq, float switchTemp, DATE dataTime)
{
	size_t chanOffset = m_bandOffset[band] + size_t(block * GetProcBw(band) / m_taskParams[band].bw);

	if (band == m_firstStep && block == 0)
	{
		unsigned long todBin = (m_measurementTime >= Units::SECONDS_PER_DAY ?
			static_cast<unsigned long>(m_numTods * fmod(dataTime, 1)) :
			std::min(m_numTods - 1, static_cast<unsigned long>(m_numTods * Units::SECONDS_PER_DAY * (dataTime - m_startTime) / m_measurementTime )));
		bool todOn = (m_outputFlags.occupancyVsTimeOfDay != 0 || m_outputFlags.occupancyFlag.flag.eFieldVsTOD != 0);

		if (todOn && (todBin != m_currTodBin || Completed()))
		{
			if (m_todNumLooks[m_currTodBin] > m_todPrevNumLooks[m_currTodBin] &&
				m_outputFlags.occupancyVsTimeOfDay)
			{
				for (size_t todChan = m_currTodBin; todChan < m_todData.size(); todChan += m_numTods)
				{
					float maxOcc = float((m_todData[todChan].numAbove - m_todData[todChan].prevNumAbove)) /
						(m_todNumLooks[m_currTodBin] - m_todPrevNumLooks[m_currTodBin]);
					ASSERT(maxOcc <= 1);

					if (maxOcc > m_todData[todChan].maxOcc)
					{
						m_todData[todChan].maxOcc = maxOcc;
					}

					m_todData[todChan].prevNumAbove = m_todData[todChan].numAbove;
				}

				m_todPrevNumLooks[m_currTodBin] = m_todNumLooks[m_currTodBin];
			}

			m_currTodBin = todBin;
		}

		++m_numLooks;

		if (todOn)
		{
			++m_todNumLooks[m_currTodBin];
		}
	}

	for (size_t blockChan = 0; blockChan < m_watts.size(); ++blockChan)
	{
		size_t chan = chanOffset + blockChan;
		if (chan >= m_bandOffset.back() + m_bandSize.back())
		{
			break;
		}
		size_t todChan = size_t(chan * m_numTods) + m_currTodBin;
		Units::Frequency freq = firstChanFreq + blockChan * m_taskParams[band].bw;

		if (!Excluded(freq, m_taskParams[band].bw, m_taskParams[band].pol))
		{
			float fieldStrength = FieldStrength(m_watts[blockChan], freq, switchTemp, band);

			// Maximum field strength
			if (m_outputFlags.eFieldVsChannel && fieldStrength > m_eFieldData[chan].maxEField)
			{
				m_eFieldData[chan].maxEField = fieldStrength;
			}

			if (m_outputFlags.occupancyFlag.flag.eFieldVsTOD != 0 &&
				fieldStrength > m_todEFields[todChan].maxEField)
			{
				m_todEFields[todChan].maxEField = fieldStrength;
			}

			// Spectrogram
			if (m_outputFlags.spectrogram)
			{
				++m_spectrogramData[chan].numAbove;
				m_spectrogramData[chan].sumEField += fieldStrength;

				if (fieldStrength > m_spectrogramData[chan].maxEField)
				{
					m_spectrogramData[chan].maxEField = fieldStrength;
				}
			}

			if (band < m_noise.size() && m_noise[band][block] != nullptr)
			{

				if (fieldStrength > std::max(m_fixedThresh1,
					m_noiseThresh1 * FieldStrength(m_noise[band][block]->GetNoise(blockChan), freq, switchTemp, band)))
				{
					// Signal is up
					if (m_outputFlags.occupancyVsChannel)
					{
						++m_occData[chan].numAbove;
					}

					if (m_outputFlags.eFieldVsChannel)
					{
						++m_eFieldData[chan].numAbove;
						m_eFieldData[chan].sumEField += fieldStrength;
					}

					if (m_outputFlags.occupancyVsTimeOfDay)
					{
						++m_todData[todChan].numAbove;
					}

					if (m_outputFlags.occupancyFlag.flag.eFieldVsTOD != 0)
					{
						++m_todEFields[todChan].numAbove;
						m_todEFields[todChan].sumEField += fieldStrength;
					}

					if (m_outputFlags.msglengthVsChannel || m_outputFlags.msglengthDistribution)
					{
						if (m_msgLenData[chan].timeOff > 0)
						{
							m_msgLenData[chan].timeOff = 0;

							if(m_msgLenData[chan].timeOn == 0)
							{
								// New message segment, save time
								m_msgLenData[chan].timeOn = dataTime;
							}
						}
					}
				}
				else
				{
					// Signal is down
					if (m_outputFlags.msglengthVsChannel || m_outputFlags.msglengthDistribution)
					{
						if (m_msgLenData[chan].timeOff == 0)
						{
							m_msgLenData[chan].timeOff = dataTime;
						}
						else
						{
							// Check gap length
							if (m_msgLenData[chan].timeOn > 0 && dataTime - m_msgLenData[chan].timeOff > m_minGap)
							{
								// Gap found - update statistics
								DATE length = m_msgLenData[chan].timeOff - m_msgLenData[chan].timeOn;
								m_msgLenData[chan].timeOn = 0;
								++m_msgLenData[chan].numMsg;
								m_msgLenData[chan].sumLen += length;

								if(length > m_msgLenData[chan].maxLen)
								{
									m_msgLenData[chan].maxLen = length;
								}

								if (m_outputFlags.msglengthDistribution)
								{
									SEquipCtrlMsg::SMsgLengthDistributionResp::SMsgLenHistData data =
										{ long(chan), float(length * Units::SECONDS_PER_DAY) };
									m_msgLenDistData.push_back(data);
								}
							}
						}
					}
				}
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update completion time for confidence-based method
//
void COccupancyTask::UpdateCompletionTime(DATE dataTime)
{
	if (m_confFactor > 0)
	{
		const size_t NUM_HIST_BINS = 100;
		unsigned int hist[NUM_HIST_BINS + 1] = { 0 };

		// Determine fraction of mission completed when using confidence-based method
		for (size_t chan = 0; chan < m_occData.size(); ++chan)
		{
			// Calculate per-channel fraction done
			double occupancy = (m_numLooks > 0 ? m_occData[chan].numAbove / m_numLooks : 0);
			double fracDone = m_numLooks * std::max(0.05, occupancy) / m_confFactor;

			// Add to histogram of fraction done
			size_t bin = size_t(NUM_HIST_BINS * fracDone);

			if (bin > NUM_HIST_BINS)
			{
				bin = NUM_HIST_BINS;
			}

			++hist[bin];
		}

		// Find bin such that sum from bin to NUM_HIST_BINS == (1 - FRACTION_DONE_NEEDED) * total
		size_t numChannelsNeeded = size_t(m_occData.size() * FRACTION_DONE_NEEDED + 0.5f);
		unsigned int sum = 0;
		float fractionDone = 1;

		for (size_t bin = 0; bin < NUM_HIST_BINS; bin++)
		{
			sum += hist[bin]; // Sum = no. of channels for which fracDone < i + 1 / NUM

			if (sum > m_occData.size() - numChannelsNeeded)
			{
				fractionDone = float(bin) / NUM_HIST_BINS;
				break;
			}
		}

		// Update completion time unless task is flagged for termination
		std::unique_lock<std::mutex> lock(m_taskIdMutex);

		if (m_completionTime > 0)
		{
			if (fractionDone > 0)
			{
				m_completionTime = m_startTime + (dataTime - m_startTime) / fractionDone;
			}
			else
			{
				m_completionTime = SEquipCtrlMsg::MAX_DATE;
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate a command
//
void COccupancyTask::Validate(_In_ const SEquipCtrlMsg::SGetOccupancyCmd& cmd)
{
	size_t bodySize = offsetof(SEquipCtrlMsg::SGetOccupancyCmd, band[cmd.numBands]);
	std::shared_ptr<SEquipCtrlMsg> msg(new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg);

	msg->hdr.msgType = SEquipCtrlMsg::OCCUPANCY_CTRL;
	msg->hdr.msgSubType = SEquipCtrlMsg::VALIDATE_OCCUPANCY;
	msg->hdr.bodySize = static_cast<unsigned long>(bodySize);
	memcpy(&msg->body.getOccupancyCmd, &cmd, bodySize);
	COccupancyTask(msg.get(), nullptr); // Will throw on error

	return;
}
