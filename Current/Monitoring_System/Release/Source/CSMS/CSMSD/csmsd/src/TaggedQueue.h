/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "Ne10Vec.h"
#include "PriorityQueue.h"

// Constants

// Types
namespace NTaggedQueue
{
	struct SDfData
	{
		unsigned long tag;
		enum EStatus : unsigned long { SUCCESS, FAILURE };
		EStatus status;
		std::set<unsigned char> remainingAnts;		// will contain the indices of the remaining antenna data
		std::vector< std::vector<unsigned long> > iq;		// 16-bit iq data
		std::vector<Ne10F32Vec> watts;
		std::vector<Ne10F32cVec2> volts;
	};
}

class CTaggedQueueDf : public CPriorityQueue<std::unique_ptr<NTaggedQueue::SDfData>, 32>
{
public:
	static const int DEPTH = 32;


	bool Enqueue(std::unique_ptr<NTaggedQueue::SDfData>& dfData, unsigned int timeout = 2000)
	{
		try
		{
//				TRACE("Enqueue dfdata with tag = %lu\n", dfData->tag);
			this->AddItem(std::move(dfData), 0, timeout);	// drop if full
		}
//		catch (CPriorityQueue<std::unique_ptr<NTaggedQueue::SDfData>, 32>::EProblem problem)
		catch (CTaggedQueueDf::EProblem problem)
		{
			switch (problem)
			{
			case CTaggedQueueDf::CLOSED:
				break;	// ignore this
			case CTaggedQueueDf::TIMEDOUT:
				TRACE("Dropped message in tagged response queue\n");
				break;
			default:
				break;
			}
			return false;
		}
		return true;
	}


	bool Dequeue(unsigned long tag, std::unique_ptr<NTaggedQueue::SDfData>& dfData, unsigned int timeout = 1000)
	{
		for (size_t i = 0; i < DEPTH; ++i)	// absolute maximum number of times to loop
		{
			unsigned long itemTag = 0;
			try
			{
				dfData = std::move(this->GetItemIfMatches([&](std::unique_ptr<NTaggedQueue::SDfData>& item)
					{ itemTag = item->tag; return item->tag == tag; }, timeout));
//					TRACE("Dequeue dfdata with tag = %lu\n", dfData->tag);
				return true;
			}
			catch (CTaggedQueueDf::EProblem problem)
			{
				switch (problem)
				{
					case CTaggedQueueDf::CLOSED:
						return false;
					case CTaggedQueueDf::TIMEDOUT:
						TRACE("DequeueTaggedResponse timed out\n");
						return false;
					case CTaggedQueueDf::NOTMATCHED:
						TRACE("DequeueTaggedResponse tag not matched %lu %lu\n", itemTag, tag);
						if (itemTag > tag && itemTag - tag < LONG_MAX) return false;
						if (itemTag < tag && tag - itemTag > LONG_MAX) return false;
						break;
					default:
						return false;
				}
			}
			// If here, then not "gotten" but itemTag < tag and should be flushed and try again
			TRACE("DequeueTaggedResponse removing dfdata with tag %lu\n", itemTag);
			this->RemoveItem([&](std::unique_ptr<NTaggedQueue::SDfData>& item)
				{ return item->tag == itemTag; });
		}
		return false;
	}

	size_t Flush(void)
	{
		size_t count = 0;
		while (this->RemoveItem([&](std::unique_ptr<NTaggedQueue::SDfData>&) { return true; } )) ++count;
		return count;
	}

private:
};

class CTaggedQueue : public CPriorityQueue<std::unique_ptr<SDfCtrlMsg>, 32>
{
public:
	static const int DEPTH = 32;

	bool Enqueue(const SDfCtrlMsg& dfMsg)
	{
		if (dfMsg.hdr.sourceAddr != 0)	// tagged response
		{
			// Create a new queue message and enqueue the response, if this is the client
			size_t msgSize = offsetof(SDfCtrlMsg, body) + dfMsg.hdr.bodySize;
			auto q = std::unique_ptr<SDfCtrlMsg>((msgSize <= sizeof(SDfCtrlMsg) ? new SDfCtrlMsg : new(msgSize) SDfCtrlMsg));
			memcpy(q.get(), &dfMsg, msgSize);	// *q = dfMsg;	// Copy message to newly allocated buffer
			try
			{
//					TRACE("Enqueue dfctrlmsg %lu with tag = %lu\n", q->hdr.msgSubType, q->hdr.sourceAddr);
				this->AddItem(std::move(q), 0, 0);	// drop if full
			}
			catch (CPriorityQueue<std::unique_ptr<SDfCtrlMsg>, DEPTH>::EProblem problem)
			{
				switch (problem)
				{
				case CTaggedQueue::CLOSED:
					break;	// ignore this
				case CTaggedQueue::TIMEDOUT:
					TRACE("Dropped message in tagged response queue\n");
					break;
				default:
					break;
				}
				return false;
			}
		}
		return true;
	}

	bool Dequeue(unsigned long tag, std::unique_ptr<SDfCtrlMsg>& dfMsg, unsigned int timeout = 1000)
	{
		for (size_t i = 0; i < DEPTH; ++i)	// absolute maximum number of times to loop
		{
			unsigned long itemTag = 0;
			try
			{
				dfMsg = std::move(this->GetItemIfMatches([&](std::unique_ptr<SDfCtrlMsg>& item)
					{ itemTag = item->hdr.sourceAddr; return item->hdr.sourceAddr == tag; }, timeout));
//					TRACE("Dequeue dfctrlmsg %lu with tag = %lu\n", dfMsg->hdr.msgSubType, dfMsg->hdr.sourceAddr);
				return true;
			}
			catch (CPriorityQueue<std::unique_ptr<SDfCtrlMsg>, DEPTH>::EProblem problem)
			{
				switch (problem)
				{
					case CTaggedQueue::CLOSED:
						return false;
					case CTaggedQueue::TIMEDOUT:
#ifdef CSMS_DEBUG
						timespec ts;
						clock_gettime(CLOCK_MONOTONIC, &ts);
						TRACE("DequeueTaggedResponse(#%lu) timed out, sec = %ld nsec =%ld;\n",
								tag, ts.tv_sec, ts.tv_nsec);
#endif
						return false;
					case CTaggedQueue::NOTMATCHED:
						TRACE("DequeueTaggedResponse(i %u) tag not matched %lu %lu\n", i, itemTag, tag);
						if (itemTag > tag && itemTag - tag < LONG_MAX) return false;
						if (itemTag < tag && tag - itemTag > LONG_MAX) return false;
						break;
					default:
						return false;
				}
			}
			// If here, then not "gotten" but itemTag < tag and should be flushed and try again
			TRACE("DequeueTaggedResponse removing item with tag %lu\n", itemTag);
			this->RemoveItem([&](std::unique_ptr<SDfCtrlMsg>& item)
				{ return item->hdr.sourceAddr == itemTag; });
		}
		return false;

	}
	size_t Flush(void)
	{
		size_t count = 0;
		while (this->RemoveItem([&](std::unique_ptr<SDfCtrlMsg>&) { return true; } )) ++count;
		return count;
	}

private:
};


