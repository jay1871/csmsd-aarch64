/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "ResultsTbl.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CResultsTbl::CResultsTbl(void) :
	m_pStmtUpdate(nullptr),
	m_pStmtInsert(nullptr),
	m_pStmtDelete(nullptr),
	m_pStmtSelect1(nullptr)
{
	Open();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CResultsTbl::~CResultsTbl(void)
{
	if (m_pStmtUpdate)
	{
		sqlite3_finalize(m_pStmtUpdate);
	}
	if (m_pStmtInsert)
	{
		sqlite3_finalize(m_pStmtInsert);
	}
	if (m_pStmtDelete)
	{
		sqlite3_finalize(m_pStmtDelete);
	}
	if (m_pStmtSelect1)
	{
		sqlite3_finalize(m_pStmtSelect1);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Delete rows from the table (all rows matching measureId and msgSubType)
//
int CResultsTbl::DeleteRows(unsigned long measureId, unsigned long msgSubType)
{
	if (!m_pStmtDelete)
		return SQLITE_ERROR;

	int rv;
	if ((rv = sqlite3_reset(m_pStmtDelete)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtDelete, 1, measureId)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtDelete, 2, msgSubType)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_step(m_pStmtDelete)) != SQLITE_DONE)
	{
		printf("CResultsTbl::DeleteRows: delete step failed\n");
		return rv;
	}

	return rv;
}


//////////////////////////////////////////////////////////////////////
//
// Get a row
//
int CResultsTbl::GetFirstRow(unsigned long measureId, unsigned long msgSubType)
{
	if (!m_pStmtSelect1)
		return SQLITE_ERROR;

	int rv;

	if ((rv = sqlite3_reset(m_pStmtSelect1)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtSelect1, 1, measureId)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtSelect1, 2, msgSubType)) != SQLITE_OK)
		return rv;

	rv = sqlite3_step(m_pStmtSelect1);

	if (rv != SQLITE_DONE && rv != SQLITE_ROW)
	{
		printf("CResultsTbl::GetFirstRow: select step failed\n");
	}

	return rv;
}


//////////////////////////////////////////////////////////////////////
//
// Get a row with data
//
int CResultsTbl::GetFirstRow(unsigned long measureId, unsigned long msgSubType, std::vector<unsigned char>& data)
{
	if (!m_pStmtSelect1)
		return SQLITE_ERROR;

	int rv;

	if ((rv = sqlite3_reset(m_pStmtSelect1)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtSelect1, 1, measureId)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtSelect1, 2, msgSubType)) != SQLITE_OK)
		return rv;

	rv = sqlite3_step(m_pStmtSelect1);
	if (rv == SQLITE_ROW)
	{
		/*unsigned long firstChan = */ sqlite3_column_int(m_pStmtSelect1, 0);
		const unsigned char* pData = static_cast<const unsigned char*>(sqlite3_column_blob(m_pStmtSelect1, 1));
		if (pData)
		{
			int numBytes = sqlite3_column_bytes(m_pStmtSelect1, 1);
			data.assign(pData, pData + numBytes);
		}
	}
	else if (rv != SQLITE_DONE)
	{
		printf("CResultsTbl::GetFirstRow: select step failed: %d\n", rv);
	}

	return rv;
}


//////////////////////////////////////////////////////////////////////
//
// "Open" the table (see if it exists)
//
int CResultsTbl::Open(void)
{
	char* errstr = nullptr;
	m_openHr = sqlite3_exec(m_db, "SELECT COUNT(*) FROM Results", nullptr, nullptr, &errstr);
	if (m_openHr != SQLITE_OK)
	{
		if (errstr)
		{
			printf("error: %s\n", errstr);
			sqlite3_free(errstr);
			errstr = nullptr;
		}
	}
	else
	{
		int hr;

		hr = sqlite3_prepare_v2(m_db, "UPDATE Results SET MsgBody = ? WHERE MeasureId = ? AND MsgSubType = ? AND FirstChan = ?;",
			-1, &m_pStmtUpdate, nullptr);
		if (hr != SQLITE_OK || !m_pStmtUpdate)
		{
			printf("CResultsTbl::prepare update failed: %d\n", hr);
			return hr;
		}

		hr = sqlite3_prepare_v2(m_db, "INSERT INTO Results (MeasureId, MsgSubType, FirstChan, MsgBody) VALUES (?,?,?,?);",
			-1, &m_pStmtInsert, nullptr);
		if (hr != SQLITE_OK || !m_pStmtInsert)
		{
			printf("CResultsTbl::prepare insert failed: %d\n", hr);
			return hr;
		}

		hr = sqlite3_prepare_v2(m_db, "DELETE FROM Results WHERE MeasureId = ? AND MsgSubType = ?", -1, &m_pStmtDelete, nullptr);
		if (hr != SQLITE_OK || !m_pStmtDelete)
		{
			printf("CResultsTbl::prepare delete failed: %d\n", hr);
			return hr;
		}

		hr = sqlite3_prepare_v2(m_db, "SELECT FirstChan,MsgBody FROM Results WHERE MeasureId = ? AND MsgSubType = ?;",
			-1, &m_pStmtSelect1, nullptr);
		if (hr != SQLITE_OK || !m_pStmtSelect1)
		{
			printf("CResultsTbl::prepare select1 failed: %d\n", hr);
			return hr;
		}
	}
	return m_openHr;
}


//////////////////////////////////////////////////////////////////////
//
// Update or insert a row in the table
//
int CResultsTbl::UpdateRow(unsigned long measureId, unsigned long msgSubType,
	unsigned long firstChan, const void* data, unsigned long size)
{
	if (!m_pStmtUpdate)
		return SQLITE_ERROR;

	int rv;
	if ((rv = sqlite3_reset(m_pStmtUpdate)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_blob(m_pStmtUpdate, 1, data, size, SQLITE_STATIC)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate, 2, measureId)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate, 3, msgSubType)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate, 4, firstChan)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_step(m_pStmtUpdate)) != SQLITE_DONE)
	{
		printf("CResultsTbl::UpdateRow: update step failed\n");
		return rv;
	}
	// Check the row count. If zero, insert instead.
	if (sqlite3_changes(m_db) > 0)
		return rv;

	if (!m_pStmtInsert)
		return SQLITE_ERROR;

	if ((rv = sqlite3_reset(m_pStmtInsert)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtInsert, 1, measureId)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtInsert, 2, msgSubType)) != SQLITE_OK)
		return rv;
	if ((rv = sqlite3_bind_int(m_pStmtInsert, 3, firstChan)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_blob(m_pStmtInsert, 4, data, size, SQLITE_STATIC)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_step(m_pStmtInsert)) != SQLITE_DONE)
	{
		printf("CResultsTbl::UpdateRow: insert step failed\n");
		return rv;
	}

	return rv;
}


