/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2017 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include <clocale>

#include "VCPCtrlNet.h"
#include "ErrorCodes.h"
#include "VCPMsg.h"
#include "Failure.h"
#include "Log.h"
#include "NetConnection.h"
#include "Utility.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
SSmsMsg::VersionMap CVCPCtrlNet::m_versionMap;
bool CVCPCtrlNet::VERBOSE = true;
std::mutex CVCPCtrlNet::m_vcpCritSect;
CVCPCtrlNet* CVCPCtrlNet::m_vcpControlClient = nullptr;

//////////////////////////////////////////////////////////////////////
//
// Constructor - server
//
CVCPCtrlNet::CVCPCtrlNet(LPCTSTR service) :
	CNetConnection<CVCPCtrlNet>(service)
{
	// Build message version maps
	for(unsigned int entry = 0; entry < std::extent<decltype(SVCPMsg::VERSION_DATA)>::value; ++entry)
	{
		m_versionMap[SVCPMsg::VERSION_DATA[entry].msgId] = SVCPMsg::VERSION_DATA[entry].version;
	}


	SetFail(&CFailure::OnFail);
	Startup();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Constructor - dynamic
//
CVCPCtrlNet::CVCPCtrlNet(SOCKET socket, _In_ CVCPCtrlNet*) :
#ifdef CSMS_DEBUG
			CNetConnection<CVCPCtrlNet>(socket)
#else
			CNetConnection<CVCPCtrlNet>(socket, 30000, 5000)
#endif
{
	static const int ON = 1;
	SetSockOpt(TCP_NODELAY, &ON, sizeof(ON), IPPROTO_TCP);
	static const int SNDBUF = 1048576;
	SetSockOpt(SO_SNDBUF, &SNDBUF, sizeof(SNDBUF), SOL_SOCKET);
	Startup();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the body size. Protects against buffer overruns
//
size_t CVCPCtrlNet::BodySize(_In_ const void* hdr) const
{
	return static_cast<const SVCPMsg::SHdr*>(hdr)->bodySize;
}


//////////////////////////////////////////////////////////////////////
//
// Downconvert a response message to the requested version
//
void CVCPCtrlNet::DownConvert(_Inout_ const SVCPMsg*& msg)
{
//	SVCPMsg* newMsg = nullptr;

	switch(msg->hdr.msgType)
	{
	default:
		THROW_LOGIC_ERROR();
	}

//	ASSERT(newMsg);
//	msg = newMsg;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CVCPCtrlNet::OnClose(int errorCode)
{
	//	OnClientClose(this);
	if (GetMode() == DYNAMIC)
	{
		std::lock_guard<std::mutex> lock(m_vcpCritSect);
		if (this == m_vcpControlClient)
		{
			// This is the current VCP control client
			m_vcpControlClient = nullptr;
		}
	}

	if (auto peer = GetPeerAddress())
	{
		CLog::Log(CLog::INFORMATION, "%s disconnected from VCPControl port, code = %d", peer->ai_canonname, errorCode);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CVCPCtrlNet::OnConnect(void)
{
	if (auto peer = GetPeerAddress())
	{
		CLog::Log(CLog::INFORMATION, "%s connected to VCPControl port", peer->ai_canonname);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CVCPCtrlNet::OnMessage(_In_bytecount_(size) const void* msg, size_t size)
{
	const SVCPMsg* vcMsg = static_cast<const SVCPMsg*>(msg);
	ASSERT(size == offsetof(SVCPMsg, body) + vcMsg->hdr.bodySize);
	auto peer = GetPeerAddress();
	LPCTSTR peerName(peer == nullptr ? _T("Unknown") : peer->ai_canonname);

	if (size != offsetof(SVCPMsg, body) + vcMsg->hdr.bodySize)
	{
		// Corrupt message
		CLog::Log(CLog::ERRORS, "VCPControl: corrupt message from %s", peerName);
		Close();
		return;
	}

	// Check command version
	SVCPMsg::SVersionKey key = { vcMsg->hdr.msgType, vcMsg->hdr.msgSubType };
	SVCPMsg::VersionMap::const_iterator entry = m_clientVersionMap.find(key);

	if (m_clientVersionMap.size() > 0 &&
		((entry == m_clientVersionMap.end() && (vcMsg->hdr.cmdVersion > 0 || vcMsg->hdr.respVersion > 0)) ||
		(entry != m_clientVersionMap.end() &&
		(entry->second.cmdVersion != vcMsg->hdr.cmdVersion || entry->second.respVersion != vcMsg->hdr.respVersion))))
	{
		// Invalid version (client mismatch with itself!)
		SVCPMsg::SHdr hdr(vcMsg->hdr);

		if (entry == m_clientVersionMap.end())
		{
			hdr.cmdVersion = 0;
			hdr.respVersion = 0;
		}
		else
		{
			hdr.cmdVersion = entry->second.cmdVersion;
			hdr.respVersion = entry->second.respVersion;
		}

		CLog::Log(CLog::ERRORS, "VCPControl: bad version from %s: type=%u subtype=%lu cmdVer=%u respVer=%u", peerName,
				vcMsg->hdr.msgType, vcMsg->hdr.msgSubType, vcMsg->hdr.cmdVersion, vcMsg->hdr.respVersion);
		SendError(hdr, ErrorCodes::INVALIDVERSION);

		return;
	}

	entry = m_versionMap.find(key);

	if ((entry == m_versionMap.end() && vcMsg->hdr.cmdVersion > 0) ||
		(entry != m_versionMap.end() && entry->second.cmdVersion < vcMsg->hdr.cmdVersion))
	{
		// Invalid version (client mismatch with me)
		SVCPMsg::SHdr hdr(vcMsg->hdr);

		if (entry == m_versionMap.end())
		{
			hdr.cmdVersion = 0;
			hdr.respVersion = 0;
		}
		else
		{
			hdr.cmdVersion = entry->second.cmdVersion;
			hdr.respVersion = entry->second.respVersion;
		}

		CLog::Log(CLog::ERRORS, "VCPControl: bad version from %s: type=%u subtype=%lu cmdVer=%u", peerName,
				vcMsg->hdr.msgType, vcMsg->hdr.msgSubType, vcMsg->hdr.cmdVersion);
		SendError(hdr, ErrorCodes::INVALIDVERSION);
		return;
	}

	// Handle GET_MSG_VERSIONS here
	if (vcMsg->hdr.msgType == SVCPMsg::STATUS_CTRL && vcMsg->hdr.msgSubType == SVCPMsg::GET_MSG_VERSIONS)
	{
		CLog::Log(CLog::INFORMATION, "VCPControl: received from %s: GET_MSG_VERSIONS", peerName);

		// Remember merged client versions
		for (size_t i = 0; vcMsg->hdr.bodySize > 0 && i < vcMsg->body.getMsgVersionsData.count; ++i)
		{
			entry = m_versionMap.find(vcMsg->body.getMsgVersionsData.versionData[i].msgId);

			if (entry != m_versionMap.end())
			{
				SVCPMsg::SVersionValue version;
				version.cmdVersion = std::min(vcMsg->body.getMsgVersionsData.versionData[i].version.cmdVersion, entry->second.cmdVersion);
				version.respVersion = std::min(vcMsg->body.getMsgVersionsData.versionData[i].version.respVersion, entry->second.respVersion);
				m_clientVersionMap[vcMsg->body.getMsgVersionsData.versionData[i].msgId] = version;
			}
		}

		unsigned long count = m_versionMap.size();
		auto msgSize = offsetof(SVCPMsg, body.getMsgVersionsData.versionData) + count * sizeof(SVCPMsg::SVersionData);
		std::shared_ptr<SVCPMsg> resp(new(msgSize) SVCPMsg);
		resp->hdr = vcMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::GET_MSG_VERSIONS_RESP;
		resp->hdr.bodySize = offsetof(SVCPMsg::SGetMsgVersionsData, versionData) + count * sizeof(SVCPMsg::SVersionData);
		resp->body.getMsgVersionsData.count = count;
		size_t index = 0;

		for (entry = m_versionMap.begin(); entry != m_versionMap.end(); ++entry)
		{
			resp->body.getMsgVersionsData.versionData[index].msgId = entry->first;
			resp->body.getMsgVersionsData.versionData[index++].version = entry->second;
		}

		Send(resp.get(), offsetof(SVCPMsg, body) + resp->hdr.bodySize);
		CLog::Log(CLog::INFORMATION, "VCPControl: sent GET_MSG_VERSIONS_RESP to %s", peerName);

		return;
	}

	bool errorReturn = false;
	bool converted = false;
	std::unique_ptr<SVCPMsg> tmpMsg;	// For modifying vcMsg, if necessary
	try
	{
		if (entry != m_versionMap.end() && entry->second.cmdVersion > vcMsg->hdr.cmdVersion)
		{
			// Upconvert to latest version
			UpConvert(vcMsg);
			converted = true;
		}

		// Pass to VCP Control
		switch(vcMsg->hdr.msgType)
		{

		case SVCPMsg::VCP_CTRL:
			OnVCPCtrl(vcMsg, this);
			break;

		default:
			CLog::Log(CLog::ERRORS, "VCPControl: invalid message type from %s: type %u subtype %lu", peerName,
					vcMsg->hdr.msgType, vcMsg->hdr.msgSubType);
			throw ErrorCodes::INVALIDMSGTYPE;
		}
	}
	catch(ErrorCodes::EErrorCode& error)
	{
		SendError(vcMsg->hdr, error);
	}
	// Catch all the possible fatal exceptions in as much detail as possible
	catch(const std::exception& ex)
	{
		CLog::Log(CLog::ERRORS, "VCPControl: service failed %s", ex.what());
		errorReturn = true;
	}
	catch(...)
	{
		CLog::Log(CLog::ERRORS, "VCPControl: service failed An unexpected error occurred");
		errorReturn = true;
	}

	if (converted)
	{
		// Delete the copy
		delete vcMsg;
	}

	if (errorReturn)
	{
		throw std::runtime_error("Fatal error");
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Handle VCP_CTRL message
//
void CVCPCtrlNet::OnVCPCtrl(_In_ const SVCPMsg* msg, _In_ CVCPCtrlNet* source)
{
	auto peer = (source != nullptr ? source->GetPeerAddress() : nullptr);

	std::unique_ptr<SVCPMsg> resp(new SVCPMsg);
	ErrorCodes::EErrorCode status = ErrorCodes::SUCCESS;

	switch (msg->hdr.msgSubType)
	{
		// SVCPMsg::GET_MSG_VERSIONS handled earlier

		case SVCPMsg::SET_VCP_MONITOR:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("VCP monitor mode = %d from %s"), msg->body.setMonitorMode.bMonitorMode ,peer->ai_canonname);
			}
			if (!msg->body.setMonitorMode.bMonitorMode)	// GOTO control mode (like fast mode)
			{
				std::unique_lock<std::mutex> lock(m_vcpCritSect);
				if (m_vcpControlClient != nullptr && m_vcpControlClient != source)
				{
					// Already have controlling client and it's not this one!
					throw ErrorCodes::CMDINVALIDVCPMODE;
				}
				if (m_vcpControlClient != nullptr)
				{
					// Already in control mode, not an error but nothing to do.
					status = ErrorCodes::CMDINVALIDVCPMODE;
					lock.unlock();
				}
				else
				{
					// Not in control mode, put it there.
					m_vcpControlClient = source;
					lock.unlock();
				}
			}
			else	// GOTO monitor mode (like standard mode)
			{
				std::unique_lock<std::mutex> lock(m_vcpCritSect);
				if (m_vcpControlClient != nullptr && m_vcpControlClient != source)
				{
					// Have controlling client and it's not this one!
					throw ErrorCodes::CMDINVALIDVCPMODE;
				}
				if (m_vcpControlClient == nullptr)
				{
					// Already in monitor mode, not an error but nothing to do.
					status = ErrorCodes::CMDINVALIDSTDMODE;
					lock.unlock();
				}
				else
				{
					m_vcpControlClient = nullptr;
					lock.unlock();
				}
			}

			resp->hdr = msg->hdr;
			resp->hdr.msgSubType = SVCPMsg::SET_VCP_MONITOR_RESP;
			resp->hdr.bodySize = sizeof(SVCPMsg::SSetMonitorModeResp);
			resp->body.setMonitorModeResp.status = status;
			resp->body.setMonitorModeResp.bMonitorMode = msg->body.setMonitorMode.bMonitorMode;
			Send(source, *resp, _T("SET_VCP_MONITOR_RESP"));
			break;

		case SVCPMsg::GET_REG_VALS:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_REG_VALS from %s"), peer->ai_canonname);
			}
			status = GetRegValues(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_REG_VALS_RESP"));
			break;

		case SVCPMsg::GET_RX_DATA:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_RX_DATA from %s"), peer->ai_canonname);
			}
			status = GetReceiverData(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_RX_DATA_RESP"));
			break;

		case SVCPMsg::GET_RX_CAPABILITIES:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_RX_CAPABILITIES from %s"), peer->ai_canonname);
			}
			status = GetRxCapabilities(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_RX_CAPABILITIES_RESP"));
			break;

		case SVCPMsg::GET_RX_MOC:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_RX_MOC from %s"), peer->ai_canonname);
			}
			status = GetRxMoc(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_RX_MOC_RESP"));
			break;

		case SVCPMsg::GET_RX_ATTEN:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_RX_ATTEN from %s"), peer->ai_canonname);
			}
			status = GetRxAtten(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_RX_ATTEN_RESP"));
			break;

		case SVCPMsg::GET_RX_CALGEN:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_RX_CALGEN from %s"), peer->ai_canonname);
			}
			status = GetRxCalgen(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_RX_CALGEN_RESP"));
			break;

		case SVCPMsg::GET_RX_OCXO:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_RX_OCXO from %s"), peer->ai_canonname);
			}
			status = GetRxOcxo(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_RX_OCXO_RESP"));
			break;

		case SVCPMsg::GET_RX_TEMPS:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_RX_TEMPS from %s"), peer->ai_canonname);
			}
			status = GetRxTemps(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_RX_TEMPS_RESP"));
			break;

		case SVCPMsg::GET_DIGI_TEMPS:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_DIGI_TEMPS from %s"), peer->ai_canonname);
			}
			status = GetDigiTemps(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_DIGI_TEMPS_RESP"));
			break;

		case SVCPMsg::GET_DIGI_VOLTS:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_DIGI_VOLTS from %s"), peer->ai_canonname);
			}
			status = GetDigiVolts(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_DIGI_VOLTS_RESP"));
			break;

		case SVCPMsg::RX_TUNE:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got RX_TUNE from %s"), peer->ai_canonname);
			}
			ValidateVCPControlClient(source);
			status = RxTune(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("RX_TUNE_RESP"));
			break;

		case SVCPMsg::SET_RX_ATTEN:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got SET_RX_ATTEN from %s"), peer->ai_canonname);
			}
			ValidateVCPControlClient(source);
			status = SetRxAtten(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("SET_RX_ATTEN_RESP"));
			break;

		case SVCPMsg::SET_RX_CALGEN:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got SET_RX_CALGEN from %s"), peer->ai_canonname);
			}
			ValidateVCPControlClient(source);
			status = SetRxCalgen(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("SET_RX_CALGEN_RESP"));
			break;

		case SVCPMsg::GET_SPECTRUM_DATA:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got GET_SPECTRUM_DATA from %s"), peer->ai_canonname);
			}
			ValidateVCPControlClient(source);
			status = GetSpectrumData(msg, resp);
			if (status != ErrorCodes::SUCCESS) throw status;
			Send(source, *resp, _T("GET_SPECTRUM_DATA_RESP"));
			break;

		default:
			if (peer != nullptr)
			{
				CLog::Log(CLog::INFORMATION, _T("Got invalid VCP_CTRL subtype %u from %s"), msg->hdr.msgSubType, peer->ai_canonname);
			}
			throw ErrorCodes::INVALIDSUBTYPE;
			break;

	}
}


//////////////////////////////////////////////////////////////////////
//
// Send a message
//
void CVCPCtrlNet::Send(_In_ CNetConnection<void>* client, _In_ const SVCPMsg& msg, _In_ LPCTSTR log)
{
	if (CVCPCtrlNet::Send(static_cast<CVCPCtrlNet*>(client), &msg) && log != nullptr && CLog::GetLogLevel() >= CLog::VERBOSE)
	{
		auto peer = (client != nullptr ? client->GetPeerAddress() : nullptr);
		if (peer != nullptr)
		{
			CLog::Log(CLog::INFORMATION, _T("Sent %s to %s"), log, peer->ai_canonname);
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send a message, downconverting the version if necessary
//
bool CVCPCtrlNet::Send(_In_ CNetConnection<CVCPCtrlNet>* connection, _In_ const SVCPMsg* msg)
{
	// Check response version
	SSmsMsg::SVersionKey key = { msg->hdr.msgType, msg->hdr.msgSubType };
	SSmsMsg::VersionMap::const_iterator entry = m_versionMap.find(key);
	bool converted = false;
	unsigned char myRespVersion = (entry != m_versionMap.end() ? entry->second.respVersion : 0);

	if(myRespVersion > msg->hdr.respVersion)
	{
		// Downconvert to requested version (making a local copy)
		DownConvert(msg);
		converted = true;
	}
	else if(myRespVersion < msg->hdr.respVersion)
	{
		// Make a local copy and set response version to my version
		size_t size = offsetof(SVCPMsg, body) + msg->hdr.bodySize;
		SVCPMsg* newMsg = new(size) SVCPMsg;
		memcpy(newMsg, msg, size);
		newMsg->hdr.respVersion = myRespVersion;
		msg = newMsg;
		converted = true;
	}

	bool status = CNetConnection<CVCPCtrlNet>::Send(connection, msg, offsetof(SVCPMsg, body) + msg->hdr.bodySize);

	if(converted)
	{
		// Delete the local copy
		delete msg;
	}

	return status;
}


//////////////////////////////////////////////////////////////////////
//
// Send an error message
//
void CVCPCtrlNet::SendError(_In_ const SVCPMsg::SHdr& hdr, unsigned long code)
{
	SVCPMsg::SHdr msg(hdr);
	msg.msgType = SVCPMsg::SMS_ERROR_REPORT;
	msg.msgSubType = code;
	msg.cmdVersion = 0;
	msg.respVersion = 0;
	msg.bodySize = 0;

	if(Send(&msg, sizeof(msg)) && CVCPCtrlNet::VERBOSE)
	{
		CLog::Log(CLog::INFORMATION, "VCPControl: sent SMS_ERROR_REPORT %lu", code);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Upconvert a message to the latest version
//
void CVCPCtrlNet::UpConvert(const SVCPMsg*& /*msg*/)
{
	THROW_LOGIC_ERROR();
	return;
}

