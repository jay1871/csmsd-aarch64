/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "ScheduleTbl.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CScheduleTbl::CScheduleTbl(void) :
	m_pStmtUpdate2(nullptr),
	m_pStmtUpdate2a(nullptr),
	m_pStmtUpdate3(nullptr),
	m_pStmtUpdate4(nullptr),
	m_pStmtUpdate5(nullptr),
	m_pStmtUpdate6(nullptr),
	m_pStmtDelete(nullptr),
	m_pStmtSelect0(nullptr),
	m_pStmtSelect1(nullptr),
	m_pStmtInsert(nullptr)
{
	Open();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CScheduleTbl::~CScheduleTbl(void)
{
	if (m_pStmtUpdate2)
	{
		sqlite3_finalize(m_pStmtUpdate2);
	}
	if (m_pStmtUpdate2a)
	{
		sqlite3_finalize(m_pStmtUpdate2a);
	}
	if (m_pStmtUpdate3)
	{
		sqlite3_finalize(m_pStmtUpdate3);
	}
	if (m_pStmtUpdate4)
	{
		sqlite3_finalize(m_pStmtUpdate4);
	}
	if (m_pStmtUpdate5)
	{
		sqlite3_finalize(m_pStmtUpdate5);
	}
	if (m_pStmtUpdate6)
	{
		sqlite3_finalize(m_pStmtUpdate6);
	}
	if (m_pStmtDelete)
	{
		sqlite3_finalize(m_pStmtDelete);
	}
	if (m_pStmtSelect0)
	{
		sqlite3_finalize(m_pStmtSelect0);
	}
	if (m_pStmtSelect0a)
	{
		sqlite3_finalize(m_pStmtSelect0a);
	}
	if (m_pStmtSelect1)
	{
		sqlite3_finalize(m_pStmtSelect1);
	}
	if (m_pStmtSelect1a)
	{
		sqlite3_finalize(m_pStmtSelect1a);
	}
	if (m_pStmtInsert)
	{
		sqlite3_finalize(m_pStmtInsert);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Delete a row from the table
//
int CScheduleTbl::DeleteRow(unsigned long measureId)
{
	if (!m_pStmtDelete)
		return SQLITE_ERROR;

	int rv;
	if ((rv = sqlite3_reset(m_pStmtDelete)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtDelete, 1, measureId)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_step(m_pStmtDelete)) != SQLITE_DONE)
	{
		printf("CScheduleTbl::DeleteRow: delete step failed\n");
		return rv;
	}

	return rv;
}


//////////////////////////////////////////////////////////////////////
//
// Get a row from the table
//
int CScheduleTbl::GetRow(EGetMode mode, unsigned long& measureId, unsigned long& taskId,
	unsigned long& key, SMetricsMsg::SMeasureCtrlMsgStateResponse::EState& state, DATE& startTime, DATE& stopTime, DATE& updateTime,
	SMetricsMsg* msg)
{
	if (!m_pStmtSelect0 || !m_pStmtSelect0a || !m_pStmtSelect1 || !m_pStmtSelect1a)
		return SQLITE_ERROR;

	int rv = SQLITE_OK;
	sqlite3_stmt* pStmtSelect = nullptr;

	switch(mode)
	{
	case FIRST:
		pStmtSelect = (msg == nullptr ? m_pStmtSelect0 : m_pStmtSelect0a);
		if ((rv = sqlite3_reset(pStmtSelect)) != SQLITE_OK)
			return rv;
		break;

	case NEXT:
		pStmtSelect = (msg == nullptr ? m_pStmtSelect0 : m_pStmtSelect0a);
		break;

	case FIND:
		pStmtSelect = (msg == nullptr ? m_pStmtSelect1 : m_pStmtSelect1a);
		if ((rv = sqlite3_reset(pStmtSelect)) != SQLITE_OK)
			return rv;

		if ((rv = sqlite3_bind_int(pStmtSelect, 1, measureId)) != SQLITE_OK)
			return rv;

		break;

//	case CURRENT:
//		hr = S_OK;
//		break;

	default:
		THROW_LOGIC_ERROR();
	}

	rv = sqlite3_step(pStmtSelect);
	if (rv == SQLITE_ROW)
	{
		measureId = sqlite3_column_int(pStmtSelect, 0);
		taskId = sqlite3_column_int(pStmtSelect, 1);
		key = sqlite3_column_int(pStmtSelect, 2);
		state = static_cast<SMetricsMsg::SMeasureCtrlMsgStateResponse::EState>(sqlite3_column_int(pStmtSelect, 3));
		startTime = sqlite3_column_double(pStmtSelect, 4);
		stopTime = sqlite3_column_double(pStmtSelect, 5);
		updateTime = sqlite3_column_double(pStmtSelect, 6);
		if (msg != nullptr)
		{
			// TODO: add checksum??
			auto pData = sqlite3_column_blob(pStmtSelect, 7);
			if (pData)
			{
				int numBytes = sqlite3_column_bytes(pStmtSelect, 7);
				memcpy(msg, pData, numBytes);
			}
			else
			{
				rv = SQLITE_ERROR;
			}
		}
	}
	else if (rv != SQLITE_DONE)
	{
		printf("CScheduleTbl::GetRow: select step failed: %d\n", rv);
	}

	return rv;
}


//////////////////////////////////////////////////////////////////////
//
// Insert a row in the table
//
int CScheduleTbl::InsertRow(unsigned long& measureId, unsigned long taskId, unsigned long key,
	SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE startTime, DATE stopTime, DATE updateTime, _In_ const SMetricsMsg& msg)
{
	if (!m_pStmtInsert)
		return SQLITE_ERROR;

	int rv;
	if ((rv = sqlite3_reset(m_pStmtInsert)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtInsert, 1, taskId)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtInsert, 2, key)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtInsert, 3, state)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_double(m_pStmtInsert, 4, startTime)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_double(m_pStmtInsert, 5, stopTime)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_double(m_pStmtInsert, 6, updateTime)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_blob(m_pStmtInsert, 7, &msg, offsetof(SMetricsMsg, body) + msg.hdr.bodySize, SQLITE_STATIC)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_step(m_pStmtInsert)) != SQLITE_DONE)
	{
		printf("CScheduleTbl::InsertRow: insert step failed\n");
		return rv;
	}

	// Need to get measureId to return to caller

	if (rv == SQLITE_OK || rv == SQLITE_DONE)
	{
		auto insertid = sqlite3_last_insert_rowid(m_db);
		printf("insertid = %llu\n", insertid);
		measureId = insertid;
	}
	else
	{
		measureId = 0;
	}

	return rv;
}


//////////////////////////////////////////////////////////////////////
//
// "Open" the table (see if it exists)
//
int CScheduleTbl::Open(void)
{
	char* errstr = nullptr;
	m_openHr = sqlite3_exec(m_db, "SELECT COUNT(*) FROM Schedule", nullptr, nullptr, &errstr);
	if (m_openHr != SQLITE_OK)
	{
		if (errstr)
		{
			printf("error: %s\n", errstr);
			sqlite3_free(errstr);
			errstr = nullptr;
		}
		return m_openHr;
	}

	int hr;

	hr = sqlite3_prepare_v2(m_db, "UPDATE Schedule SET State = ? WHERE MeasureId = ?;",
		-1, &m_pStmtUpdate2, nullptr);
	if (hr != SQLITE_OK || !m_pStmtUpdate2)
	{
		printf("CScheduleTbl::prepare update2 failed: %d\n", hr);
		return hr;
	}

	hr = sqlite3_prepare_v2(m_db, "UPDATE Schedule SET UpdateTime = ? WHERE MeasureId = ?;",
		-1, &m_pStmtUpdate2a, nullptr);
	if (hr != SQLITE_OK || !m_pStmtUpdate2a)
	{
		printf("CScheduleTbl::prepare update2a failed: %d\n", hr);
		return hr;
	}

	hr = sqlite3_prepare_v2(m_db, "UPDATE Schedule SET State = ?, UpdateTime = ? WHERE MeasureId = ?;",
		-1, &m_pStmtUpdate3, nullptr);
	if (hr != SQLITE_OK || !m_pStmtUpdate3)
	{
		printf("CScheduleTbl::prepare update3 failed: %d\n", hr);
		return hr;
	}

	hr = sqlite3_prepare_v2(m_db, "UPDATE Schedule SET State = ?, StopTime = ?, UpdateTime = ? WHERE MeasureId = ?;",
		-1, &m_pStmtUpdate4, nullptr);
	if (hr != SQLITE_OK || !m_pStmtUpdate4)
	{
		printf("CScheduleTbl::prepare update4 failed: %d\n", hr);
		return hr;
	}

	hr = sqlite3_prepare_v2(m_db, "UPDATE Schedule SET TaskId = ?, \"Key\" = ?, State = ?, UpdateTime = ? WHERE MeasureId = ?;",
		-1, &m_pStmtUpdate5, nullptr);
	if (hr != SQLITE_OK || !m_pStmtUpdate5)
	{
		printf("CScheduleTbl::prepare update5 failed: %d\n", hr);
		return hr;
	}

	hr = sqlite3_prepare_v2(m_db, "UPDATE Schedule SET TaskId = ?, \"Key\" = ?, State = ?, StopTime = ?, UpdateTime = ? WHERE MeasureId = ?;",
		-1, &m_pStmtUpdate6, nullptr);
	if (hr != SQLITE_OK || !m_pStmtUpdate6)
	{
		printf("CScheduleTbl::prepare update6 failed: %d\n", hr);
		return hr;
	}

	hr = sqlite3_prepare_v2(m_db, "DELETE FROM Schedule WHERE MeasureId = ?", -1, &m_pStmtDelete, nullptr);
	if (hr != SQLITE_OK || !m_pStmtDelete)
	{
		printf("CScheduleTbl::prepare delete failed: %d\n", hr);
		return hr;
	}

	hr = sqlite3_prepare_v2(m_db, "SELECT MeasureId,taskId,\"key\",state,startTime,stopTime,updateTime FROM Schedule;",
		-1, &m_pStmtSelect0, nullptr);
	if (hr != SQLITE_OK || !m_pStmtSelect0)
	{
		printf("CScheduleTbl::prepare select0 failed: %d\n", hr);
		return hr;
	}

	hr = sqlite3_prepare_v2(m_db, "SELECT MeasureId,taskId,\"key\",state,startTime,stopTime,updateTime,Msg FROM Schedule;",
		-1, &m_pStmtSelect0a, nullptr);
	if (hr != SQLITE_OK || !m_pStmtSelect0a)
	{
		printf("CScheduleTbl::prepare select0 failed: %d\n", hr);
		return hr;
	}

	hr = sqlite3_prepare_v2(m_db, "SELECT MeasureId,taskId,\"key\",state,startTime,stopTime,updateTime FROM Schedule WHERE MeasureId = ?;",
		-1, &m_pStmtSelect1, nullptr);
	if (hr != SQLITE_OK || !m_pStmtSelect1)
	{
		printf("CScheduleTbl::prepare select1 failed: %d\n", hr);
		return hr;
	}

	hr = sqlite3_prepare_v2(m_db, "SELECT MeasureId,taskId,\"key\",state,startTime,stopTime,updateTime,Msg FROM Schedule WHERE MeasureId = ?;",
		-1, &m_pStmtSelect1a, nullptr);
	if (hr != SQLITE_OK || !m_pStmtSelect1a)
	{
		printf("CScheduleTbl::prepare select1a failed: %d\n", hr);
		return hr;
	}

	hr = sqlite3_prepare_v2(m_db, "INSERT INTO Schedule "
		"(TaskId, \"Key\", State, StartTime, StopTime, UpdateTime, Msg) VALUES (?,?,?,?,?,?,?);",
		-1, &m_pStmtInsert, nullptr);
	if (hr != SQLITE_OK || !m_pStmtInsert)
	{
		printf("CScheduleTbl::prepare insert failed: %d\n", hr);
		return hr;
	}
    return m_openHr;
}


//////////////////////////////////////////////////////////////////////
//
// Update a row in the table
//
int CScheduleTbl::UpdateRow(unsigned long measureId, SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state)
{
	if (!m_pStmtUpdate2)
		return SQLITE_ERROR;

	int rv;
	if ((rv = sqlite3_reset(m_pStmtUpdate2)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate2, 1, state)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate2, 2, measureId)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_step(m_pStmtUpdate2)) != SQLITE_DONE)
	{
		printf("CScheduleTbl::UpdateRow: update step failed\n");
		return rv;
	}
	return rv;
}

int CScheduleTbl::UpdateRow(unsigned long measureId, DATE updateTime)
{
	if (!m_pStmtUpdate2a)
		return SQLITE_ERROR;

	int rv;
	if ((rv = sqlite3_reset(m_pStmtUpdate2a)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_double(m_pStmtUpdate2a, 1, updateTime)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate2a, 2, measureId)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_step(m_pStmtUpdate2a)) != SQLITE_DONE)
	{
		printf("CScheduleTbl::UpdateRow: update step failed\n");
		return rv;
	}
	return rv;

}

int CScheduleTbl::UpdateRow(unsigned long measureId, SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE updateTime)
{
	if (!m_pStmtUpdate3)
		return SQLITE_ERROR;

	int rv;
	if ((rv = sqlite3_reset(m_pStmtUpdate3)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate3, 1, state)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_double(m_pStmtUpdate3, 2, updateTime)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate3, 3, measureId)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_step(m_pStmtUpdate3)) != SQLITE_DONE)
	{
		printf("CScheduleTbl::UpdateRow: update step failed\n");
		return rv;
	}
	return rv;
}


int CScheduleTbl::UpdateRow(unsigned long measureId, SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE stopTime, DATE updateTime)
{
	if (!m_pStmtUpdate4)
		return SQLITE_ERROR;

	int rv;
	if ((rv = sqlite3_reset(m_pStmtUpdate4)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate4, 1, state)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_double(m_pStmtUpdate4, 2, stopTime)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_double(m_pStmtUpdate4, 3, updateTime)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate4, 4, measureId)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_step(m_pStmtUpdate4)) != SQLITE_DONE)
	{
		printf("CScheduleTbl::UpdateRow: update step failed\n");
		return rv;
	}
	return rv;
}

int CScheduleTbl::UpdateRow(unsigned long measureId, unsigned long taskId, unsigned long key,
	SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE updateTime)
{
	if (!m_pStmtUpdate5)
		return SQLITE_ERROR;

	int rv;
	if ((rv = sqlite3_reset(m_pStmtUpdate5)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate5, 1, taskId)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate5, 2, key)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate5, 3, state)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_double(m_pStmtUpdate5, 4, updateTime)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate5, 5, measureId)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_step(m_pStmtUpdate5)) != SQLITE_DONE)
	{
		printf("CScheduleTbl::UpdateRow: update step failed\n");
		return rv;
	}
	return rv;
}

int CScheduleTbl::UpdateRow(unsigned long measureId, unsigned long taskId, unsigned long key,
	SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE stopTime, DATE updateTime)
{
	if (!m_pStmtUpdate6)
		return SQLITE_ERROR;

	int rv;
	if ((rv = sqlite3_reset(m_pStmtUpdate6)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate6, 1, taskId)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate6, 2, key)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate6, 3, state)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_double(m_pStmtUpdate6, 4, stopTime)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_double(m_pStmtUpdate6, 5, updateTime)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_bind_int(m_pStmtUpdate6, 6, measureId)) != SQLITE_OK)
		return rv;

	if ((rv = sqlite3_step(m_pStmtUpdate6)) != SQLITE_DONE)
	{
		printf("CScheduleTbl::UpdateRow: update step failed\n");
		return rv;
	}
	return rv;
}

