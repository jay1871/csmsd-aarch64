/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include <clocale>

//#include "Config.h"
#include "EquipCtrlMsg.h"
#include "EquipCtrlNet.h"
#include "ErrorCodes.h"
#include "VCPMsg.h"
#include "Failure.h"
#include "Log.h"
#include "NetConnection.h"
#include "Task.h"
#include "Utility.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
SSmsMsg::VersionMap CEquipCtrlNet::m_versionMap;
bool CEquipCtrlNet::VERBOSE = true;

//////////////////////////////////////////////////////////////////////
//
// Constructor - server
//
CEquipCtrlNet::CEquipCtrlNet(LPCTSTR service)
:
	CNetConnection<CEquipCtrlNet>(service),
	m_equipControl()
{
	// Build message version maps
	for(unsigned int entry = 0; entry < std::extent<decltype(SEquipCtrlMsg::VERSION_DATA)>::value; ++entry)
	{
		m_versionMap[SEquipCtrlMsg::VERSION_DATA[entry].msgId] = SEquipCtrlMsg::VERSION_DATA[entry].version;
	}
	for(unsigned int entry = 0; entry < std::extent<decltype(SVCPMsg::VERSION_DATA)>::value; ++entry)
	{
		m_versionMap[SVCPMsg::VERSION_DATA[entry].msgId] = SVCPMsg::VERSION_DATA[entry].version;
	}


	SetFail(&CFailure::OnFail);
	Startup();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Constructor - dynamic
//
CEquipCtrlNet::CEquipCtrlNet(SOCKET socket, _In_ CEquipCtrlNet*)
:
#ifdef CSMS_DEBUG
			CNetConnection<CEquipCtrlNet>(socket),
#else
			CNetConnection<CEquipCtrlNet>(socket, 30000, 5000),
#endif
			m_equipControl()
{
	static const int ON = 1;
	SetSockOpt(TCP_NODELAY, &ON, sizeof(ON), IPPROTO_TCP);
	static const int SNDBUF = 1048576;
	SetSockOpt(SO_SNDBUF, &SNDBUF, sizeof(SNDBUF), SOL_SOCKET);
	Startup();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the body size. Protects against buffer overruns
//
size_t CEquipCtrlNet::BodySize(_In_ const void* hdr) const
{
	return static_cast<const SEquipCtrlMsg::SHdr*>(hdr)->bodySize;
}


//////////////////////////////////////////////////////////////////////
//
// Downconvert a response message to the requested version
//
void CEquipCtrlNet::DownConvert(_Inout_ const SEquipCtrlMsg*& msg)
{
	SEquipCtrlMsg* newMsg = nullptr;

	switch(msg->hdr.msgType)
	{
	case SEquipCtrlMsg::AUTOVIOLATE_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SEquipCtrlMsg::AUTOVIOLATE_BWMEAS_RESULT:
		case SEquipCtrlMsg::AUTOVIOLATE_FREQMEAS_RESULT:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg = new SEquipCtrlMsg;
				newMsg->hdr = msg->hdr;
				newMsg->body.avdMeasureResultV0 = msg->body.avdMeasureResult;
				newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SAvdMeasureResultV0);
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			break;

		case SEquipCtrlMsg::OCC_FREQUENCY_RESULT:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg = new SEquipCtrlMsg;
				newMsg->hdr = msg->hdr;
				newMsg->body.frequencyVsChannelRespV0 = msg->body.frequencyVsChannelResp;
				newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SFrequencyVsChannelRespV0);
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			break;

		case SEquipCtrlMsg::VALIDATE_OCC_RESPONSE:
			DownConvertValidateOccResponse(msg, newMsg);
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SEquipCtrlMsg::DF_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SEquipCtrlMsg::INITIALIZE_DF_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:	// fixed length
				{
					newMsg = new SEquipCtrlMsg;
					newMsg->hdr = msg->hdr;
					unsigned long bodySizeV3 = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SInitializeDfRespV3,
						pbCalStatus[msg->body.initializeDfResp.numPbCalStatus]));
					auto v3 = std::unique_ptr<SEquipCtrlMsg>(new(offsetof(SEquipCtrlMsg, body) + bodySizeV3) SEquipCtrlMsg);
					unsigned long bodySizeV2 = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SInitializeDfRespV2,
						pbCalStatus[msg->body.initializeDfResp.numPbCalStatus]));
					auto v2 = std::unique_ptr<SEquipCtrlMsg>(new(offsetof(SEquipCtrlMsg, body) + bodySizeV2) SEquipCtrlMsg);
					unsigned long bodySizeV1 = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SInitializeDfRespV1,
						pbCalStatus[msg->body.initializeDfResp.numPbCalStatus]));
					auto v1 = std::unique_ptr<SEquipCtrlMsg>(new(offsetof(SEquipCtrlMsg, body) + bodySizeV1) SEquipCtrlMsg);
					newMsg->body.initializeDfRespV0 = v1->body.initializeDfRespV1 = v2->body.initializeDfRespV2 = v3->body.initializeDfRespV3 = msg->body.initializeDfResp;;
					newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SInitializeDfRespV0);
				}
				break;

			case 1:
				{
					unsigned long bodySize = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SInitializeDfRespV1,
						pbCalStatus[msg->body.initializeDfResp.numPbCalStatus]));
					newMsg = new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg;
					newMsg->hdr = msg->hdr;
					unsigned long bodySizeV3 = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SInitializeDfRespV3,
						pbCalStatus[msg->body.initializeDfResp.numPbCalStatus]));
					auto v3 = std::unique_ptr<SEquipCtrlMsg>(new(offsetof(SEquipCtrlMsg, body) + bodySizeV3) SEquipCtrlMsg);
					unsigned long bodySizeV2 = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SInitializeDfRespV2,
						pbCalStatus[msg->body.initializeDfResp.numPbCalStatus]));
					auto v2 = std::unique_ptr<SEquipCtrlMsg>(new(offsetof(SEquipCtrlMsg, body) + bodySizeV2) SEquipCtrlMsg);
					newMsg->body.initializeDfRespV1 = v2->body.initializeDfRespV2 = v3->body.initializeDfRespV3 = msg->body.initializeDfResp;
					newMsg->hdr.bodySize = bodySize;
				}
				break;

			case 2:
				{
					unsigned long bodySize = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SInitializeDfRespV2,
						pbCalStatus[msg->body.initializeDfResp.numPbCalStatus]));
					newMsg = new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg;
					newMsg->hdr = msg->hdr;
					unsigned long bodySizeV3 = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SInitializeDfRespV3,
						pbCalStatus[msg->body.initializeDfResp.numPbCalStatus]));
					auto v3 = std::unique_ptr<SEquipCtrlMsg>(new(offsetof(SEquipCtrlMsg, body) + bodySizeV3) SEquipCtrlMsg);
					newMsg->body.initializeDfRespV2 = v3->body.initializeDfRespV3 = msg->body.initializeDfResp;
					newMsg->hdr.bodySize = bodySize;
				}
				break;

			case 3:
				{
					unsigned long bodySize = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SInitializeDfRespV3,
						pbCalStatus[msg->body.initializeDfResp.numPbCalStatus]));
					newMsg = new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg;
					newMsg->hdr = msg->hdr;
					newMsg->body.initializeDfRespV3 = msg->body.initializeDfResp;
					newMsg->hdr.bodySize = bodySize;
				}
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SEquipCtrlMsg::METRICS_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SEquipCtrlMsg::GET_BAND_RESPONSE:
		{
			SEquipCtrlMsg::SGetBandRespV5 v5 = msg->body.getBandResp;
			if (msg->hdr.respVersion == 5)
			{
				newMsg = new SEquipCtrlMsg;
				newMsg->hdr = msg->hdr;
				newMsg->body.getBandRespV5 = v5;
				newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetBandRespV5);
			}
			else{

				SEquipCtrlMsg::SGetBandRespV4 v4 = v5;
				if (msg->hdr.respVersion == 4)
				{
					newMsg = new SEquipCtrlMsg;
					newMsg->hdr = msg->hdr;
					newMsg->body.getBandRespV4 = v4;
					newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetBandRespV4);
				}
				else
				{
					SEquipCtrlMsg::SGetBandRespV3 V3 = v4;
					if (msg->hdr.respVersion == 3)
					{
						newMsg = new SEquipCtrlMsg;
						newMsg->hdr = msg->hdr;
						newMsg->body.getBandRespV3 = V3;
						newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetBandRespV3);
					}
					else
					{
						SEquipCtrlMsg::SGetBandRespV2 V2 = V3;
						if (msg->hdr.respVersion == 2)
						{
							newMsg = new SEquipCtrlMsg;
							newMsg->hdr = msg->hdr;
							newMsg->body.getBandRespV2 = V2;
							newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetBandRespV2);
						}
						else
						{
							SEquipCtrlMsg::SGetBandRespV1 V1 = V2;
							if (msg->hdr.respVersion == 1)
							{
								newMsg = new SEquipCtrlMsg;
								newMsg->hdr = msg->hdr;
								newMsg->body.getBandRespV1 = V1;
								newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetBandRespV1);
							}
							else if (msg->hdr.respVersion == 0)
							{
								newMsg = new SEquipCtrlMsg;
								newMsg->hdr = msg->hdr;
								newMsg->body.getBandRespV0 = V1;
								newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetBandRespV0);
							}
							else
							{
								THROW_LOGIC_ERROR();
							}
						}
					}
				}
			}

			break;
		}

		case SEquipCtrlMsg::GET_MEAS_RESPONSE:
		{
			SEquipCtrlMsg::SGetMeasRespV2 V2 = msg->body.getMeasResp;
			if(msg->hdr.respVersion == 2)
			{
				newMsg = new SEquipCtrlMsg;
				newMsg->hdr = msg->hdr;
				newMsg->body.getMeasRespV2 = V2;
				newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetMeasRespV2);
			}
			else
			{
				SEquipCtrlMsg::SGetMeasRespV1 V1 = V2;
				if(msg->hdr.respVersion == 1)
				{
					newMsg = new SEquipCtrlMsg;
					newMsg->hdr = msg->hdr;
					newMsg->body.getMeasRespV1 = V1;
					newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetMeasRespV1);
				}
				else if(msg->hdr.respVersion == 0)
				{
					newMsg = new SEquipCtrlMsg;
					newMsg->hdr = msg->hdr;
					newMsg->body.getMeasRespV0 = V1;
					newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetMeasRespV0);
				}
				else
				{
					THROW_LOGIC_ERROR();
				}
			}

			break;
		}

		case SEquipCtrlMsg::GET_MEAS_IQDATA_RESP:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg = new SEquipCtrlMsg;
				newMsg->hdr = msg->hdr;
				newMsg->body.iqDataRespV0 = msg->body.iqDataResp;
				newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SIqDataRespV0);
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SEquipCtrlMsg::OCCUPANCY_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SEquipCtrlMsg::OCC_FREQUENCY_RESULT:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg = new SEquipCtrlMsg;
				newMsg->hdr = msg->hdr;
				newMsg->body.frequencyVsChannelRespV0 = msg->body.frequencyVsChannelResp;
				newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SFrequencyVsChannelRespV0);
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			break;

		case SEquipCtrlMsg::VALIDATE_OCC_RESPONSE:
			DownConvertValidateOccResponse(msg, newMsg);
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SEquipCtrlMsg::OCCUPANCYDF_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SEquipCtrlMsg::OCC_FREQUENCY_RESULT:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg = new SEquipCtrlMsg;
				newMsg->hdr = msg->hdr;
				newMsg->body.frequencyVsChannelRespV0 = msg->body.frequencyVsChannelResp;
				newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SFrequencyVsChannelRespV0);
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			break;

		case SEquipCtrlMsg::VALIDATE_OCC_RESPONSE:
			DownConvertValidateOccResponse(msg, newMsg);
			break;

		default:
			THROW_LOGIC_ERROR();
		}

		break;

	case SEquipCtrlMsg::PAN_DISP_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SEquipCtrlMsg::GET_PAN_RESPONSE:
			switch(msg->hdr.respVersion)
			{
			case 0:
				newMsg = new SEquipCtrlMsg;
				newMsg->hdr = msg->hdr;
				newMsg->body.getPanRespV0 = msg->body.getPanRespV1;
				newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetPanRespV0);
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			break;

			default:
				THROW_LOGIC_ERROR();
		}

		break;

	case SEquipCtrlMsg::STATUS_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SEquipCtrlMsg::GET_CSMS_FAULT_RESP:
			switch(msg->hdr.respVersion)
			{
			case 0:
				THROW_LOGIC_ERROR();

			case 1:	// from v2 to v1
				newMsg = new SEquipCtrlMsg;
				newMsg->hdr = msg->hdr;
				newMsg->body.getCsmsFaultRespV1 = msg->body.getCsmsFaultRespV2;
				newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetCsmsFaultRespV1);
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			break;

		default:
			THROW_LOGIC_ERROR();
		}
		break;
	case SEquipCtrlMsg::DEMOD_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SEquipCtrlMsg::SET_AUDIO_PARAMS_RESP:
			switch(msg->hdr.respVersion)
			{
				case 1: // from v2 to v1
					newMsg = new SEquipCtrlMsg;
					newMsg->hdr = msg->hdr;
					newMsg->body.audioParamsRespV1 = msg->body.audioParamsRespV2;
					newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SAudioParamsRespV1);
				break;
				default:
					THROW_LOGIC_ERROR();
			}
			break;
		default:
			THROW_LOGIC_ERROR();
		}
		break;
	default:
		THROW_LOGIC_ERROR();
	}

	ASSERT(newMsg);
	msg = newMsg;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Downconvert a response message to the requested version
//
void CEquipCtrlNet::DownConvert(_Inout_ const SVCPMsg*& msg)
{
//	SVCPMsg* newMsg = nullptr;

	switch(msg->hdr.msgType)
	{
	default:
		THROW_LOGIC_ERROR();
	}

//	ASSERT(newMsg);
//	msg = newMsg;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Downconvert a SEquipCtrlMsg::VALIDATE_OCC_RESPONSE message to the requested version
//
void CEquipCtrlNet::DownConvertValidateOccResponse(_In_ const SEquipCtrlMsg* msg, _Out_ SEquipCtrlMsg*& newMsg)
{
	switch(msg->hdr.respVersion)
	{
		case 0:
			{
				size_t	maxSize = offsetof(SEquipCtrlMsg::SValidateOccupancyRespV3,
					occCmd.band[msg->body.validateOccupancyResp.occCmd.numBands]);
				if (maxSize < sizeof(newMsg->body.validateOccupancyRespV0))
				{
					maxSize = sizeof(newMsg->body.validateOccupancyRespV0);
				}
				newMsg = new(offsetof(SEquipCtrlMsg, body) + maxSize) SEquipCtrlMsg;
				newMsg->hdr = msg->hdr;
				newMsg->body.validateOccupancyRespV0 =
					SEquipCtrlMsg::SValidateOccupancyRespV1(SEquipCtrlMsg::SValidateOccupancyRespV2(
					SEquipCtrlMsg::SValidateOccupancyRespV3(
					newMsg->body.validateOccupancyRespV4 = msg->body.validateOccupancyResp)));
				newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SValidateOccupancyRespV0);
			}
			break;

		case 1:
			{
				size_t	maxSize = offsetof(SEquipCtrlMsg::SValidateOccupancyRespV3,
					occCmd.band[msg->body.validateOccupancyResp.occCmd.numBands]);
				if (maxSize < sizeof(newMsg->body.validateOccupancyRespV1))
				{
					maxSize = sizeof(newMsg->body.validateOccupancyRespV1);
				}
				newMsg = new(offsetof(SEquipCtrlMsg, body) + maxSize) SEquipCtrlMsg;
				newMsg->hdr = msg->hdr;
				newMsg->body.validateOccupancyRespV1 = (SEquipCtrlMsg::SValidateOccupancyRespV2(
					SEquipCtrlMsg::SValidateOccupancyRespV3(
                 	newMsg->body.validateOccupancyRespV4 = msg->body.validateOccupancyResp)));
				newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SValidateOccupancyRespV1);
			}
			break;

		case 2:
			{
				auto bodySize = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SValidateOccupancyRespV2,
					occCmd.band[msg->body.validateOccupancyResp.occCmd.numBands]));
				newMsg = new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg;
				newMsg->hdr = msg->hdr;
				newMsg->body.validateOccupancyRespV2 = SEquipCtrlMsg::SValidateOccupancyRespV3(
					    newMsg->body.validateOccupancyRespV4 = msg->body.validateOccupancyResp);
				newMsg->hdr.bodySize = bodySize;
			}
			break;

		case 3:
			{
				auto bodySize = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SValidateOccupancyRespV3,
					occCmd.band[msg->body.validateOccupancyResp.occCmd.numBands]));
				newMsg = new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg;
				newMsg->hdr = msg->hdr;
				newMsg->body.validateOccupancyRespV3 = SEquipCtrlMsg::SValidateOccupancyRespV4(
                                        msg->body.validateOccupancyResp);
				newMsg->hdr.bodySize = bodySize;
			}
			break;

		case 4:
		{
			auto bodySize = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SValidateOccupancyRespV4,
				occCmd.band[msg->body.validateOccupancyResp.occCmd.numBands]));
			newMsg = new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg;
			newMsg->hdr = msg->hdr;
			newMsg->body.validateOccupancyRespV4 = msg->body.validateOccupancyResp;
			newMsg->hdr.bodySize = bodySize;
		}
			break;

		default:
			THROW_LOGIC_ERROR();
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the response version for a message
//
unsigned char CEquipCtrlNet::GetResponseVersion(_In_ const CEquipCtrlNet* client, _In_ const SEquipCtrlMsg::SHdr& hdr)
{
	// NOTE: Assumes that "client" is in existence (unless nullptr); i.e., this is NOT a "safe" method
	// TODO: Add SafeCall method to CNetConnection:  bool SafeCall(client, callback, arg)
	auto& theMap = (client != nullptr ? client->m_clientVersionMap : m_versionMap);

	SSmsMsg::SVersionKey key = { hdr.msgType, hdr.msgSubType };
	SSmsMsg::VersionMap::const_iterator entry = theMap.find(key);
	return (entry != theMap.end() ? entry->second.respVersion : 0);
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CEquipCtrlNet::OnClose(int errorCode)
{
	m_equipControl->OnClientClose(this);

	if(auto peer = GetPeerAddress())
	{
		CLog::Log(CLog::INFORMATION, "%s disconnected from EquipControl port, code = %d", peer->ai_canonname, errorCode);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CEquipCtrlNet::OnConnect(void)
{
	if(auto peer = GetPeerAddress())
	{
		CLog::Log(CLog::INFORMATION, "%s connected to EquipControl port", peer->ai_canonname);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Callback
//
void CEquipCtrlNet::OnMessage(_In_bytecount_(size) const void* msg, size_t size)
{
	const SEquipCtrlMsg* ecMsg = static_cast<const SEquipCtrlMsg*>(msg);
	ASSERT(size == offsetof(SEquipCtrlMsg, body) + ecMsg->hdr.bodySize);
	auto peer = GetPeerAddress();
	LPCTSTR peerName(peer == nullptr ? _T("Unknown") : peer->ai_canonname);

	if(size != offsetof(SEquipCtrlMsg, body) + ecMsg->hdr.bodySize)
	{
		// Corrupt message
		CLog::Log(CLog::ERRORS, "EquipControl: corrupt message from %s", peerName);
		Close();
		return;
	}

	// Check command version
	SEquipCtrlMsg::SVersionKey key = { ecMsg->hdr.msgType, ecMsg->hdr.msgSubType };
	SEquipCtrlMsg::VersionMap::const_iterator entry = m_clientVersionMap.find(key);

	if(m_clientVersionMap.size() > 0 &&
		((entry == m_clientVersionMap.end() && (ecMsg->hdr.cmdVersion > 0 || ecMsg->hdr.respVersion > 0)) ||
		(entry != m_clientVersionMap.end() &&
		(entry->second.cmdVersion != ecMsg->hdr.cmdVersion || entry->second.respVersion != ecMsg->hdr.respVersion))))
	{
		// Invalid version (client mismatch with itself!)
		SEquipCtrlMsg::SHdr hdr(ecMsg->hdr);

		if(entry == m_clientVersionMap.end())
		{
			hdr.cmdVersion = 0;
			hdr.respVersion = 0;
		}
		else
		{
			hdr.cmdVersion = entry->second.cmdVersion;
			hdr.respVersion = entry->second.respVersion;
		}

		CLog::Log(CLog::ERRORS, "EquipControl: bad version from %s: type=%u subtype=%lu cmdVer=%u respVer=%u", peerName,
				ecMsg->hdr.msgType, ecMsg->hdr.msgSubType, ecMsg->hdr.cmdVersion, ecMsg->hdr.respVersion);
		SendError(hdr, ErrorCodes::INVALIDVERSION);

		return;
	}

	entry = m_versionMap.find(key);

	if((entry == m_versionMap.end() && ecMsg->hdr.cmdVersion > 0) ||
		(entry != m_versionMap.end() && entry->second.cmdVersion < ecMsg->hdr.cmdVersion))
	{
		// Invalid version (client mismatch with me)
		SEquipCtrlMsg::SHdr hdr(ecMsg->hdr);

		if(entry == m_versionMap.end())
		{
			hdr.cmdVersion = 0;
			hdr.respVersion = 0;
		}
		else
		{
			hdr.cmdVersion = entry->second.cmdVersion;
			hdr.respVersion = entry->second.respVersion;
		}

		CLog::Log(CLog::ERRORS, "EquipControl: bad version from %s: type=%u subtype=%lu cmdVer=%u", peerName,
				ecMsg->hdr.msgType, ecMsg->hdr.msgSubType, ecMsg->hdr.cmdVersion);
		SendError(hdr, ErrorCodes::INVALIDVERSION);
		return;
	}

	// Handle GET_MSG_VERSIONS here
	if(ecMsg->hdr.msgType == SEquipCtrlMsg::STATUS_CTRL && ecMsg->hdr.msgSubType == SEquipCtrlMsg::GET_MSG_VERSIONS)
	{
//		CLog::Log(CLog::INFORMATION, "EquipControl: received from %s: GET_MSG_VERSIONS", peerName);

		// Remember merged client versions
		for(size_t i = 0; ecMsg->hdr.bodySize > 0 && i < ecMsg->body.getMsgVersionsData.count; ++i)
		{
			entry = m_versionMap.find(ecMsg->body.getMsgVersionsData.versionData[i].msgId);

			if(entry != m_versionMap.end())
			{
				SEquipCtrlMsg::SVersionValue version;
				version.cmdVersion = std::min(ecMsg->body.getMsgVersionsData.versionData[i].version.cmdVersion, entry->second.cmdVersion);
				version.respVersion = std::min(ecMsg->body.getMsgVersionsData.versionData[i].version.respVersion, entry->second.respVersion);
				m_clientVersionMap[ecMsg->body.getMsgVersionsData.versionData[i].msgId] = version;
			}
		}

		unsigned long count = m_versionMap.size();
		auto msgSize = offsetof(SEquipCtrlMsg, body.getMsgVersionsData.versionData) + count * sizeof(SEquipCtrlMsg::SVersionData);
		std::shared_ptr<SEquipCtrlMsg> resp(new(msgSize) SEquipCtrlMsg);
		resp->hdr = ecMsg->hdr;
		resp->hdr.msgSubType = SEquipCtrlMsg::GET_MSG_VERSIONS_RESP;
		resp->hdr.bodySize = offsetof(SEquipCtrlMsg::SGetMsgVersionsData, versionData) + count * sizeof(SEquipCtrlMsg::SVersionData);
		resp->body.getMsgVersionsData.count = count;
		size_t index = 0;

		for(entry = m_versionMap.begin(); entry != m_versionMap.end(); ++entry)
		{
			resp->body.getMsgVersionsData.versionData[index].msgId = entry->first;
			resp->body.getMsgVersionsData.versionData[index++].version = entry->second;
		}

		Send(resp.get(), offsetof(SEquipCtrlMsg, body) + resp->hdr.bodySize);
//		CLog::Log(CLog::INFORMATION, "EquipControl: sent GET_MSG_VERSIONS_RESP to %s", peerName);

		return;
	}

	bool errorReturn = false;
	bool converted = false;
	std::unique_ptr<SEquipCtrlMsg> tmpMsg;	// For modifying ecMsg, if necessary
	try
	{
		if(entry != m_versionMap.end() && entry->second.cmdVersion > ecMsg->hdr.cmdVersion)
		{
			// Upconvert to latest version
			UpConvert(ecMsg);
			converted = true;
		}

		// Pass to Equip Control
		switch(ecMsg->hdr.msgType)
		{
		case SEquipCtrlMsg::ANT_CTRL:
			m_equipControl->OnAntCtrl(ecMsg, this);
			break;
#if 0
		case SEquipCtrlMsg::AUDIO_SWITCH:
			m_equipControl->OnAudioSwitch(ecMsg, this);
			break;
#endif
		case SEquipCtrlMsg::AUTOVIOLATE_CTRL:
			m_equipControl->OnAvdCtrl(ecMsg, this);
			break;

		case SEquipCtrlMsg::BIST_CTRL:
			m_equipControl->OnBistCtrl(ecMsg, this);
			break;

		case SEquipCtrlMsg::DEMOD_CTRL:
			m_equipControl->OnDemodCtrl(ecMsg, this);
			break;

			case SEquipCtrlMsg::DF_CTRL:
			m_equipControl->OnDfCtrl(ecMsg, this);
			break;

#if 0
			case SEquipCtrlMsg::DSP_CTRL:
			m_equipControl->OnDspCtrl(ecMsg, this);
			break;

#endif
		case SEquipCtrlMsg::EQUIPMENT_CTRL:
			m_equipControl->OnEquipmentCtrl(ecMsg, this);
			break;

		case SEquipCtrlMsg::METRICS_CTRL:
			if (ecMsg->hdr.msgSubType == SEquipCtrlMsg::GET_MEAS && ecMsg->body.getMeasCmd.iqCmd.outputType != SEquipCtrlMsg::NONE)
			{
				tmpMsg.reset(new(offsetof(SEquipCtrlMsg, body) + ecMsg->hdr.bodySize) SEquipCtrlMsg);
				tmpMsg->hdr = ecMsg->hdr;
				tmpMsg->body.getMeasCmd = ecMsg->body.getMeasCmd;
				tmpMsg->body.getMeasCmd.iqCmd.startTime = 0;		// Set to zero whether or not .tdoa is true (for now)
#if ALLOW_EQUIPCONTROL_TDOA == 1
				if (tmpMsg->body.getMeasCmd.iqCmd.tdoa)
				{
//					TciGps::GetDATE(tmpMsg->body.getMeasCmd.iqCmd.startTime);
//					tmpMsg->body.getMeasCmd.iqCmd.startTime += 2. / 86400.;		// 2 sec in the future
					tmpMsg->body.getMeasCmd.iqCmd.startTime = Utility::CurrentTimeAsDATE() + 2. / 86400.;	// 2 sec in the future
				}
#endif
				ecMsg = tmpMsg.get();
			}
			m_equipControl->OnMetricsCtrl(ecMsg,
//				ecMsg->hdr.msgSubType == SEquipCtrlMsg::GET_MEAS && ecMsg->body.getMeasCmd.iqCmd.outputType != SEquipCtrlMsg::NONE && ecMsg->body.getMeasCmd.iqCmd.tdoa ? CTask::SYSTEM : CTask::IMMEDIATE,
				this);
			break;

		case SEquipCtrlMsg::OCCUPANCY_CTRL:
			m_equipControl->OnOccupancyCtrl(ecMsg, this);
			break;

		case SEquipCtrlMsg::OCCUPANCYDF_CTRL:
			m_equipControl->OnOccupancyDfCtrl(ecMsg, this);
			break;

		case SEquipCtrlMsg::PAN_DISP_CTRL:
			m_equipControl->OnPanDispCtrl(ecMsg, this);
			break;

		case SEquipCtrlMsg::STATUS_CTRL:
			switch(ecMsg->hdr.msgSubType)
			{
			case SEquipCtrlMsg::PING_SERVER:
//				CLog::Log(CLog::INFORMATION, "EquipControl: received from %s: PING_SERVER", peerName);
				break;

			case SEquipCtrlMsg::PING_SERVER_RMS:
				CLog::Log(CLog::INFORMATION, "EquipControl: received from %s: PING_SERVER_RMS", peerName);
				break;

			case SEquipCtrlMsg::GET_FAULT_REQUEST:
				m_equipControl->OnStatusCtrl(ecMsg, this);
				break;

			// SEquipCtrlMsg::GET_MSG_VERSIONS handled earlier

			default:
				CLog::Log(CLog::ERRORS, "EquipControl: invalid message subtype from %s: type %u subtype %lu", peerName,
						ecMsg->hdr.msgType, ecMsg->hdr.msgSubType);
				throw ErrorCodes::INVALIDSUBTYPE;
			}

			break;

			case SEquipCtrlMsg::VCP_CTRL:
				m_equipControl->OnVCPCtrl(reinterpret_cast<const SVCPMsg*>(ecMsg), this);
				break;

#if 0
			case SEquipCtrlMsg::WB_DIGITIZER_SAMPLE:
			m_equipControl->OnWbDigitizerSample(ecMsg, this);
			break;

		case SEquipCtrlMsg::SOUNDER_CTRL:
			m_equipControl->OnSounderCtrl(ecMsg, this);
			break;
#endif
		default:
			CLog::Log(CLog::ERRORS, "EquipControl: invalid message type from %s: type %u subtype %lu", peerName,
					ecMsg->hdr.msgType, ecMsg->hdr.msgSubType);
			throw ErrorCodes::INVALIDMSGTYPE;
		}
	}
	catch(ErrorCodes::EErrorCode& error)
	{
		SendError(ecMsg->hdr, error);
	}
	// Catch all the possible fatal exceptions in as much detail as possible
	catch(const std::exception& ex)
	{
		CLog::Log(CLog::ERRORS, "EquipControl: service failed %s", ex.what());
		errorReturn = true;
	}
	catch(...)
	{
		CLog::Log(CLog::ERRORS, "EquipControl: service failed An unexpected error occurred");
		errorReturn = true;
	}

	if(converted)
	{
		// Delete the copy
		delete ecMsg;
	}

	if(errorReturn)
	{
		throw std::runtime_error("Fatal error");
//		CService::Instance()->OnAbort(errorReturn);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send an error message
//
void CEquipCtrlNet::SendError(_In_ const SEquipCtrlMsg::SHdr& hdr, unsigned long code)
{
	SEquipCtrlMsg::SHdr msg(hdr);
	msg.msgType = SEquipCtrlMsg::SMS_ERROR_REPORT;
	msg.msgSubType = code;
	msg.cmdVersion = 0;
	msg.respVersion = 0;
	msg.bodySize = 0;

	if(Send(&msg, sizeof(msg)) && CEquipCtrlNet::VERBOSE)
	{
		CLog::Log(CLog::INFORMATION, "EquipControl: sent SMS_ERROR_REPORT %lu", code);
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Upconvert a message to the latest version
//
void CEquipCtrlNet::UpConvert(_Inout_ const SEquipCtrlMsg*& msg)
{
	switch(msg->hdr.msgType)
	{
	case SEquipCtrlMsg::AUTOVIOLATE_CTRL:
		{
			SEquipCtrlMsg* newMsg;
			unsigned long bodySize;

			switch(msg->hdr.msgSubType)
			{
			case SEquipCtrlMsg::GET_AUTOVIOLATE:
			case SEquipCtrlMsg::VALIDATE_AUTOVIOLATE:
				switch(msg->hdr.cmdVersion)
				{
				case 0:
					{
						if(msg->hdr.bodySize != sizeof(msg->body.getAutoViolateCmdV0))
						{
							throw ErrorCodes::INVALIDVERSION;
						}

						bodySize = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SGetAutoViolateCmd, band[msg->body.getAutoViolateCmdV0.numBands]));
						size_t	tempBodySize = offsetof(SEquipCtrlMsg::SGetAutoViolateCmdV2, band[msg->body.getAutoViolateCmdV0.numBands]);
						newMsg = new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg;
						std::unique_ptr<SEquipCtrlMsg> apTempMsg(new(offsetof(SEquipCtrlMsg, body) + tempBodySize) SEquipCtrlMsg);
						apTempMsg->body.getAutoViolateCmdV2 = SEquipCtrlMsg::SGetAutoViolateCmdV1(msg->body.getAutoViolateCmdV0);
						newMsg->body.getAutoViolateCmd = apTempMsg->body.getAutoViolateCmdV2;
					}
					break;

				case 1:
					{
						if(msg->hdr.bodySize != sizeof(msg->body.getAutoViolateCmdV1))
						{
							throw ErrorCodes::INVALIDVERSION;
						}

						bodySize = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SGetAutoViolateCmd, band[msg->body.getAutoViolateCmdV1.numBands]));
						size_t	tempBodySize = offsetof(SEquipCtrlMsg::SGetAutoViolateCmdV2, band[msg->body.getAutoViolateCmdV1.numBands]);
						newMsg = new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg;
						std::unique_ptr<SEquipCtrlMsg> apTempMsg(new(offsetof(SEquipCtrlMsg, body) + tempBodySize) SEquipCtrlMsg);
						apTempMsg->body.getAutoViolateCmdV2 = msg->body.getAutoViolateCmdV1;
						newMsg->body.getAutoViolateCmd = apTempMsg->body.getAutoViolateCmdV2;;
					}
					break;

				case 2:
					if(msg->hdr.bodySize != static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SGetAutoViolateCmdV2, band[msg->body.getAutoViolateCmdV2.numBands])))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					bodySize = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SGetAutoViolateCmd, band[msg->body.getAutoViolateCmdV2.numBands]));
					newMsg = new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg;
					newMsg->body.getAutoViolateCmd = msg->body.getAutoViolateCmdV2;
					break;

				default:
					THROW_LOGIC_ERROR();
				}

				newMsg->hdr = msg->hdr;
				newMsg->hdr.bodySize = bodySize;
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			msg = newMsg;
		}

		break;

	case SEquipCtrlMsg::DEMOD_CTRL:
		{
			SEquipCtrlMsg* newMsg = new SEquipCtrlMsg;
			newMsg->hdr = msg->hdr;

			switch(msg->hdr.msgSubType)
			{
			case SEquipCtrlMsg::SET_RCVR:
				switch(msg->hdr.cmdVersion)
				{
				case 0:
					if(msg->hdr.bodySize != sizeof(msg->body.rcvrCtrlCmdV0))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					newMsg->body.rcvrCtrlCmdV1 = msg->body.rcvrCtrlCmdV0;
					break;

				default:
					THROW_LOGIC_ERROR();
				}

				newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SRcvrCtrlCmd);
				break;
			case SEquipCtrlMsg::SET_AUDIO_PARAMS:
				switch (msg->hdr.cmdVersion)
				{
				case 1:
					if (msg->hdr.bodySize != sizeof(msg->body.audioParamsCmdV1))
					{
						throw ErrorCodes::INVALIDVERSION;
					}
					newMsg->body.audioParamsCmd = msg->body.audioParamsCmdV1;
					break;
				default:
					THROW_LOGIC_ERROR();
				}
				newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SAudioParamsCmd);
				break;
			default:
				THROW_LOGIC_ERROR();
			}

			msg = newMsg;
		}

		break;

	case SEquipCtrlMsg::DF_CTRL:
		{
			SEquipCtrlMsg* newMsg = new SEquipCtrlMsg;
			newMsg->hdr = msg->hdr;

			switch(msg->hdr.msgSubType)
			{
			case SEquipCtrlMsg::INITIALIZE_DF:
				switch(msg->hdr.cmdVersion)
				{
				case 0:
					if(msg->hdr.bodySize != 0)
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					newMsg->body.initializeDf.freq = Units::Frequency(0).GetRaw();
					newMsg->body.initializeDf.rxBw = Units::Frequency(0).GetRaw();
					newMsg->body.initializeDf.numIntegrations = 0;
					newMsg->body.initializeDf.hf = false;
					break;

				case 1:
					if(msg->hdr.bodySize != sizeof(SEquipCtrlMsg::SInitializeDfV1))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					newMsg->body.initializeDf = msg->body.initializeDfV1;
					break;

				default:
					THROW_LOGIC_ERROR();
				}

				newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SInitializeDf);
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			msg = newMsg;
		}

		break;

	case SEquipCtrlMsg::METRICS_CTRL:
		{
			SEquipCtrlMsg* newMsg = new SEquipCtrlMsg;
			newMsg->hdr = msg->hdr;

			switch(msg->hdr.msgSubType)
			{
			case SEquipCtrlMsg::GET_DWELL:
				switch(msg->hdr.cmdVersion)
				{
				case 0:
					if(msg->hdr.bodySize != sizeof(msg->body.getDwellCmdV0))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					newMsg->body.getDwellCmd = msg->body.getDwellCmdV0;
					newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetDwellCmd);
					break;

				default:
					THROW_LOGIC_ERROR();
				}

				break;

			case SEquipCtrlMsg::GET_MEAS:
			case SEquipCtrlMsg::VALIDATE_MEAS:
				switch(msg->hdr.cmdVersion)
				{
				case 0:
					if(msg->hdr.bodySize != sizeof(msg->body.getMeasCmdV0))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					newMsg->body.getMeasCmd = newMsg->body.getMeasCmdV4 = newMsg->body.getMeasCmdV3 = newMsg->body.getMeasCmdV2 = newMsg->body.getMeasCmdV1 = msg->body.getMeasCmdV0;
					newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetMeasCmd);
					break;

				case 1:
					if(msg->hdr.bodySize != sizeof(msg->body.getMeasCmdV1))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					newMsg->body.getMeasCmd = newMsg->body.getMeasCmdV4 = newMsg->body.getMeasCmdV3 = newMsg->body.getMeasCmdV2 = msg->body.getMeasCmdV1;
					newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetMeasCmd);
					break;

				case 2:
					if(msg->hdr.bodySize != sizeof(msg->body.getMeasCmdV2))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					newMsg->body.getMeasCmd = newMsg->body.getMeasCmdV4 = newMsg->body.getMeasCmdV3 = msg->body.getMeasCmdV2;
					newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetMeasCmd);
					break;

				case 3:
					if(msg->hdr.bodySize != sizeof(msg->body.getMeasCmdV3))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					newMsg->body.getMeasCmd = newMsg->body.getMeasCmdV4 = msg->body.getMeasCmdV3;
					newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetMeasCmd);
					break;

				case 4:
					if(msg->hdr.bodySize != sizeof(msg->body.getMeasCmdV4))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					newMsg->body.getMeasCmd = msg->body.getMeasCmdV4;
					newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetMeasCmd);
					break;

				default:
					THROW_LOGIC_ERROR();
				}
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			msg = newMsg;
		}

		break;

	case SEquipCtrlMsg::OCCUPANCY_CTRL:
		{
			SEquipCtrlMsg* newMsg;
			unsigned long bodySize;

			switch(msg->hdr.msgSubType)
			{
			case SEquipCtrlMsg::GET_OCCUPANCY:
			case SEquipCtrlMsg::VALIDATE_OCCUPANCY:
				switch(msg->hdr.cmdVersion)
				{
				case 0:
					{
						if(msg->hdr.bodySize != sizeof(msg->body.getOccupancyCmdV0))
						{
							throw ErrorCodes::INVALIDVERSION;
						}

						bodySize = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SGetOccupancyCmd, band[msg->body.getOccupancyCmdV0.numBands]));
						size_t	tempBodySize2 = offsetof(SEquipCtrlMsg::SGetOccupancyCmdV2, band[msg->body.getOccupancyCmdV0.numBands]);
						size_t	tempBodySize3 = offsetof(SEquipCtrlMsg::SGetOccupancyCmdV3, band[msg->body.getOccupancyCmdV0.numBands]);
						size_t	tempBodySize4 = offsetof(SEquipCtrlMsg::SGetOccupancyCmdV4, band[msg->body.getOccupancyCmdV0.numBands]);
						newMsg = new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg;
						std::unique_ptr<SEquipCtrlMsg> apTempMsg2(new(offsetof(SEquipCtrlMsg, body) + tempBodySize2) SEquipCtrlMsg);
						std::unique_ptr<SEquipCtrlMsg> apTempMsg3(new(offsetof(SEquipCtrlMsg, body) + tempBodySize3) SEquipCtrlMsg);
						std::unique_ptr<SEquipCtrlMsg> apTempMsg4(new(offsetof(SEquipCtrlMsg, body) + tempBodySize4) SEquipCtrlMsg);
						apTempMsg2->body.getOccupancyCmdV2 = SEquipCtrlMsg::SGetOccupancyCmdV1(msg->body.getOccupancyCmdV0);
						apTempMsg3->body.getOccupancyCmdV3 = apTempMsg2->body.getOccupancyCmdV2;
						apTempMsg4->body.getOccupancyCmdV4 = apTempMsg3->body.getOccupancyCmdV3;
						newMsg->body.getOccupancyCmd = apTempMsg4->body.getOccupancyCmdV4;
					}
					break;

				case 1:
					{
						if(msg->hdr.bodySize != sizeof(msg->body.getOccupancyCmdV1))
						{
							throw ErrorCodes::INVALIDVERSION;
						}

						bodySize = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SGetOccupancyCmd, band[msg->body.getOccupancyCmdV1.numBands]));
						size_t	tempBodySize2 = offsetof(SEquipCtrlMsg::SGetOccupancyCmdV2, band[msg->body.getOccupancyCmdV1.numBands]);
						size_t	tempBodySize3 = offsetof(SEquipCtrlMsg::SGetOccupancyCmdV3, band[msg->body.getOccupancyCmdV1.numBands]);
						size_t	tempBodySize4 = offsetof(SEquipCtrlMsg::SGetOccupancyCmdV4, band[msg->body.getOccupancyCmdV1.numBands]);
						newMsg = new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg;
						std::unique_ptr<SEquipCtrlMsg> apTempMsg2(new(offsetof(SEquipCtrlMsg, body) + tempBodySize2) SEquipCtrlMsg);
						std::unique_ptr<SEquipCtrlMsg> apTempMsg3(new(offsetof(SEquipCtrlMsg, body) + tempBodySize3) SEquipCtrlMsg);
						std::unique_ptr<SEquipCtrlMsg> apTempMsg4(new(offsetof(SEquipCtrlMsg, body) + tempBodySize4) SEquipCtrlMsg);
						apTempMsg2->body.getOccupancyCmdV2 = msg->body.getOccupancyCmdV1;
						apTempMsg3->body.getOccupancyCmdV3 = apTempMsg2->body.getOccupancyCmdV2;
						apTempMsg4->body.getOccupancyCmdV4 = apTempMsg3->body.getOccupancyCmdV3;
						newMsg->body.getOccupancyCmd = apTempMsg4->body.getOccupancyCmdV4;
					}
					break;

				case 2:
					{
						if(msg->hdr.bodySize != static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SGetOccupancyCmdV2, band[msg->body.getOccupancyCmdV2.numBands])))
						{
							throw ErrorCodes::INVALIDVERSION;
						}

						bodySize = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SGetOccupancyCmd, band[msg->body.getOccupancyCmdV2.numBands]));
						size_t	tempBodySize3 = offsetof(SEquipCtrlMsg::SGetOccupancyCmdV3, band[msg->body.getOccupancyCmdV2.numBands]);
						size_t	tempBodySize4 = offsetof(SEquipCtrlMsg::SGetOccupancyCmdV4, band[msg->body.getOccupancyCmdV2.numBands]);
						newMsg = new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg;
						std::unique_ptr<SEquipCtrlMsg> apTempMsg3(new(offsetof(SEquipCtrlMsg, body) + tempBodySize3) SEquipCtrlMsg);
						std::unique_ptr<SEquipCtrlMsg> apTempMsg4(new(offsetof(SEquipCtrlMsg, body) + tempBodySize4) SEquipCtrlMsg);
						apTempMsg3->body.getOccupancyCmdV3 = msg->body.getOccupancyCmdV2;
						apTempMsg4->body.getOccupancyCmdV4 = apTempMsg3->body.getOccupancyCmdV3;
						newMsg->body.getOccupancyCmd = apTempMsg4->body.getOccupancyCmdV4;
					}
					break;

				case 3:
				{
					if (msg->hdr.bodySize != static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SGetOccupancyCmdV3, band[msg->body.getOccupancyCmdV3.numBands])))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					bodySize = static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SGetOccupancyCmd, band[msg->body.getOccupancyCmdV3.numBands]));
					size_t	tempBodySize4 = offsetof(SEquipCtrlMsg::SGetOccupancyCmdV4, band[msg->body.getOccupancyCmdV2.numBands]);
					newMsg = new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg;
					std::unique_ptr<SEquipCtrlMsg> apTempMsg4(new(offsetof(SEquipCtrlMsg, body) + tempBodySize4) SEquipCtrlMsg);
					apTempMsg4->body.getOccupancyCmdV4 = msg->body.getOccupancyCmdV3;
					newMsg->body.getOccupancyCmd = apTempMsg4->body.getOccupancyCmdV4;
				}
					break;

				case 4:
					if(msg->hdr.bodySize != static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SGetOccupancyCmdV4, band[msg->body.getOccupancyCmdV4.numBands])))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					bodySize =  static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SGetOccupancyCmd, band[msg->body.getOccupancyCmdV4.numBands]));
					newMsg = new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg;
					newMsg->body.getOccupancyCmd = msg->body.getOccupancyCmdV4;
					break;

				default:
					THROW_LOGIC_ERROR();
				}

				newMsg->hdr = msg->hdr;
				newMsg->hdr.bodySize = bodySize;
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			msg = newMsg;
		}

		break;

	case SEquipCtrlMsg::OCCUPANCYDF_CTRL:
		{
			SEquipCtrlMsg* newMsg;
			unsigned long bodySize;

			switch(msg->hdr.msgSubType)
			{
			case SEquipCtrlMsg::GET_OCCUPANCY_SCANDF:
			case SEquipCtrlMsg::VALIDATE_OCCUPANCY_SCANDF:
				switch(msg->hdr.cmdVersion)
				{
				case 0:
					if(msg->hdr.bodySize != sizeof(msg->body.getScanDfCmdV0))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					bodySize =  static_cast<unsigned long>(offsetof(SEquipCtrlMsg::SGetScanDfCmd, band[msg->body.getScanDfCmdV0.numBands]));
					newMsg = new(offsetof(SEquipCtrlMsg, body) + bodySize) SEquipCtrlMsg;
					newMsg->body.getScanDfCmd = msg->body.getScanDfCmdV0;
					break;

				default:
					THROW_LOGIC_ERROR();
				}

				newMsg->hdr = msg->hdr;
				newMsg->hdr.bodySize = bodySize;
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			msg = newMsg;
		}

		break;

	case SEquipCtrlMsg::PAN_DISP_CTRL:
		{
			SEquipCtrlMsg* newMsg = new SEquipCtrlMsg;
			newMsg->hdr = msg->hdr;

			switch(msg->hdr.msgSubType)
			{
			case SEquipCtrlMsg::GET_PAN:
			case SEquipCtrlMsg::GET_PAN_AGC:
				switch(msg->hdr.cmdVersion)
				{
				case 0:
					if(msg->hdr.bodySize != sizeof(msg->body.getPanCmdV0))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					newMsg->body.getPanCmd = newMsg->body.getPanCmdV1 = msg->body.getPanCmdV0;
					newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetPanCmd);
					break;

				case 1:
					if(msg->hdr.bodySize != sizeof(msg->body.getPanCmdV1))
					{
						throw ErrorCodes::INVALIDVERSION;
					}

					newMsg->body.getPanCmd = msg->body.getPanCmdV1;
					newMsg->hdr.bodySize = sizeof(SEquipCtrlMsg::SGetPanCmd);
					break;

				default:
					THROW_LOGIC_ERROR();
				}

				break;

			default:
				THROW_LOGIC_ERROR();
			}

			msg = newMsg;
		}

		break;

	default:
		THROW_LOGIC_ERROR();
	}

	return;
}

