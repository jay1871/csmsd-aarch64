/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/
#pragma once
#include "Units.h"
#include "hidapi.h"

namespace Utility
{
	#define VENDOR_ID 0x20ce // MiniCircuits Vendor ID
	#define PRODUCT_ID 0x0022 // MiniCircuits HID USB RUDAT Product ID
	#define PATHLEN 2
	#define SEND_PACKET_LEN 64

	enum ETimeFormat { SEC, MSEC, USEC, NSEC };
	extern hid_device *handle;
	extern unsigned char PACKET[SEND_PACKET_LEN];

	DATE CurrentTimeAsDATE(void);
	timespec CurrentTimeAsTimespec(void);
	Units::Timestamp CurrentTimeAsTimestamp(void);
	std::string CurrentTimeAsString(ETimeFormat = SEC);
	std::string TimespecAsString(const timespec& ts, ETimeFormat = SEC);
	int64_t GetTickCount(void);
	void ToU16String(const char* str, size_t maxSize, std::u16string& out);
	void RTrim(std::string& s);
	void LTrim(std::string& s);
	void Trim(std::string& s);
	bool IsQuitting(void);
	void SetQuitAll(void);
	void Delay(unsigned long usec);
	void SetSoftwareCompatibleoff();
	bool GetSoftwareCompatiblFlag();
#ifdef CSMS_2016
	bool RFSwitchInit();
	void RFSwitchDisconnect();
	bool GetRFSwitchPort(unsigned char * State);
	bool SetRFSwitchPort(unsigned char *State);
#endif
}

namespace LinuxUtility
{
	void ShutdownSystem(unsigned long countSec, bool shutdown, void(*PreShutdown)());
}
