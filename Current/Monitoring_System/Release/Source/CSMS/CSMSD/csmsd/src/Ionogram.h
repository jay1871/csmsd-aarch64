/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "Config.h"
#include "EquipCtrlMsg.h"
#include "Singleton.h"
#include "Units.h"

class CIonogram
{
	// Friends
	friend class CSingleton<CIonogram>;

public:
	float Range(float el, Units::Frequency freq) const;

private:
	// Functions
	CIonogram(void);

	// Data
	CSingleton<const CConfig> m_config;
	mutable std::mutex m_critSect;
	SEquipCtrlMsg::SIonogramDataMsg m_ionoData;
	const CConfig::SMutableConfig& m_mutableConfig; // Must be after m_config
};

