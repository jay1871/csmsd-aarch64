/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once
#include "Config.h"
#include "EquipControl.h"
#include "MetricsMsg.h"
#include "PersistentData.h"
#include "RWLock.h"
#include "Singleton.h"

class CMetricsNet;

class CMetrics
{
	// Friends
	friend class CSingleton<CMetrics>;

public:
	// Functions
	void ForwardToEquipControl(_In_ const SMetricsMsg& msg, _In_opt_ CMetricsNet* source, unsigned long measureId = 0);
	void ForwardStatusCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	unsigned long GetClientId(_In_ const CMetricsNet* client) const;
	void OnAntCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void OnAvdCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void OnBistCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void OnClientClose(_In_ const CMetricsNet* client);
	void OnClientConnect(_In_ const CMetricsNet* client, _In_ const ADDRINFOT* pPeer);
	void OnDemodCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void OnMeasureCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void OnMeasurementGreetingReq(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void OnMeasurementPanReq(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void OnOccupancyCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void OnOccupancyDfCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void OnSystemStateCtrl(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void SendAntNameChangeResp(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void SendStatusCtrlConnResp(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	bool TranslateAndSend(_In_ CMetricsNet* client, _In_ const SEquipCtrlMsg& msg) const;

private:
	// Constants
	static const size_t	STATIONNAME_LEN = SMetricsMsg::MAX_STATIONNAME_LEN;
	static constexpr const DATE TDOA_EARLY_START_INTERVAL = 700.0 / Units::MILLISECONDS_PER_DAY; // 700 ms

	// Types
	struct SMetricClientData
	{
		SMetricClientData(void) : isNetted(false)
        {
			client.clientId = 0;
			client.clientName[0] = u'\0';
			client.ipType = SMetricsMsg::IP_V4;
            stationName[0] = '\0';

        }

		SMetricsMsg::SClientInfo client;
		bool isNetted;
		char stationName[STATIONNAME_LEN];
	};
	typedef std::map<const CMetricsNet*, SMetricClientData> ClientDataMap;

	// Functions
	CMetrics();
	~CMetrics();
	void DeleteResults(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* source);
	void ForwardAvdOccupancyDfCtrl(_In_ const SMetricsMsg& msg, unsigned long measureId);
	void ForwardMeasureCtrl(_In_ const SMetricsMsg& msg, _In_opt_ CMetricsNet* client, unsigned long measureId);
	unsigned long GetStationId(_In_ const CMetricsNet* client) const;
	void GetStationName(_In_ const CMetricsNet* client, char(&name)[STATIONNAME_LEN]) const;
	bool IsNetted(_In_ const CMetricsNet* client) const;
	void RequestState(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void RetrieveIqData(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void RetrieveMeasurement(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void ScheduleAvd(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void ScheduleMeasurement(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void ScheduleOccupancy(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void ScheduleOccupancyDf(_In_ const SMetricsMsg& msg, _In_ CMetricsNet* client);
	void SendError(_In_ CMetricsNet* client, _In_ const SMetricsMsg::SHdr& hdr, ErrorCodes::EErrorCode error) const;

	// Data
	ClientDataMap m_clientDataMap;
	CSingleton<CConfig> m_config;
	size_t m_connBufLen;
	mutable CRWLock m_critSect;
	CSingleton<CPersistentData> m_database;
	CSingleton<CEquipControl> m_equipControl;
	CMetricsNet* m_interactiveClient;
	SMetricsMsg::EAnt m_interactiveClientAnt;
	SMetricsMsg::SClientData m_interactiveClientData;
	long m_interactiveClientKey;
	SMetricsMsg* m_pConnRespMsg;
	bool m_priorityMode;
};

