/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

// These are in stdafx.h
#include "stdafx.h"

#include "AvdTask.h"
#include "EquipCtrlMsg.h"
#include "ErrorCodes.h"
#include "Log.h"
#include "Metrics.h"
#include "MetricsMsg.h"
//#include "MetricsNet.h"
#include "OccupancyTask.h"
#include "PersistentData.h"
#include "ScanDfTask.h"
#include "Singleton.h"
#include "sqlite3.h"
#include "Units.h"
#include "Utility.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CPersistentData::CPersistentData(void) :
	CSqliteDb(),	// Virtual base class
	CResultsTbl(),
	CScheduleTbl(),
	m_config(),
	m_critSect(),
	m_queue(),
	m_scheduledMeasureId(0),
	m_scheduledTime(SMetricsMsg::MAX_DATE)
//	m_thread(nullptr)
{
	if (CResultsTbl::m_openHr != SQLITE_OK || CScheduleTbl::m_openHr != SQLITE_OK)
	{
		printf("Recreate tables\n");
		// Try to recreate the tables
		RecreateTables();
		CheckHr(CResultsTbl::Open());
		CheckHr(CScheduleTbl::Open());
	}
	printf("Ready to start thread\n");

	m_thread = std::thread(&CPersistentData::Thread, this);

	printf("CPersistentData ctor done\n");
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CPersistentData::~CPersistentData(void)
{
	if (m_thread.joinable())
	{
		m_queue.Close();
		m_thread.join();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Add a message to the database queue
//
void CPersistentData::AddMsg(QueueMsg msg, int priority, unsigned int timeout)
{
	m_queue.AddItem(msg, priority, timeout);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Add a message to the database queue
//
void CPersistentData::AddMsg(_In_ const SEquipCtrlMsg& msg, int priority, unsigned int timeout)
{
	SEquipCtrlMsg* copy = new(offsetof(SEquipCtrlMsg, body) + msg.hdr.bodySize) SEquipCtrlMsg;
	memcpy(copy, &msg, offsetof(SEquipCtrlMsg, body) + msg.hdr.bodySize);
	m_queue.AddItem(QueueMsg(copy), priority, timeout);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Delete a measurement result
//
ErrorCodes::EErrorCode CPersistentData::DeleteResults(unsigned long measureId, unsigned long msgSubType)
{
	std::unique_lock<std::recursive_mutex> lock(m_critSect);
	int hr = sqlite3_exec(m_db, "BEGIN TRANSACTION;", nullptr, nullptr, nullptr);

	if (hr == SQLITE_OK)
	{
		hr = CResultsTbl::DeleteRows(measureId, msgSubType);

		if (hr == SQLITE_OK || hr == SQLITE_DONE)
		{
			hr = sqlite3_exec(m_db, "COMMIT TRANSACTION;", nullptr, nullptr, nullptr);
		}
		else
		{
			hr = sqlite3_exec(m_db, "ROLLBACK TRANSACTION;", nullptr, nullptr, nullptr);
		}
	}

	if (hr == SQLITE_OK || hr == SQLITE_DONE)
	{
		return ErrorCodes::TASK_COMPLETED;
	}
	else
	{
		LogHr(hr);
		ASSERT(FALSE);
		return ErrorCodes::REQUEST_FAIL_DELETE;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Delete a task
//
ErrorCodes::EErrorCode CPersistentData::DeleteTask(unsigned long measureId)
{
	std::unique_lock<std::recursive_mutex> lock(m_critSect);
	int hr = sqlite3_exec(m_db, "BEGIN TRANSACTION;", nullptr, nullptr, nullptr);

	if (hr == SQLITE_OK)
	{
		SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;
		unsigned long taskId;
		unsigned long key;
		DATE startTime;
		DATE stopTime;
		DATE updateTime;
		hr = CScheduleTbl::GetRow(CScheduleTbl::FIND, measureId, taskId, key, state, startTime, stopTime, updateTime);

		if (hr == SQLITE_OK || SQLITE_ROW)
		{
			if (state == SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING || state == SMetricsMsg::SMeasureCtrlMsgStateResponse::SUSPENDED)
			{
				hr = sqlite3_exec(m_db, "ROLLBACK TRANSACTION;", nullptr, nullptr, nullptr);
				return ErrorCodes::REQUEST_FAIL_DELETE;
			}

			// Deleting the Schedule entry will also delete any results because of the foreign key constraint
			hr = CScheduleTbl::DeleteRow(measureId);

			if (hr == SQLITE_OK || hr == SQLITE_DONE)
			{
				hr = sqlite3_exec(m_db, "COMMIT TRANSACTION;", nullptr, nullptr, nullptr);
			}
			else
			{
				hr = sqlite3_exec(m_db, "ROLLBACK TRANSACTION;", nullptr, nullptr, nullptr);
			}
		}
		else
		{
			hr = sqlite3_exec(m_db, "ROLLBACK TRANSACTION;", nullptr, nullptr, nullptr);
		}
	}

	if (measureId == m_scheduledMeasureId)
	{
		// Update schedule
		SEquipCtrlMsg msg;
		msg.hdr.msgType = SEquipCtrlMsg::METRICS_CTRL;
		msg.hdr.msgSubType = SEquipCtrlMsg::CAL_SCHED;
		msg.hdr.bodySize = 0;
		AddMsg(msg);
	}

	if (hr == SQLITE_OK /*|| hr == DB_E_NOTFOUND*/)
	{
		return ErrorCodes::TASK_COMPLETED;
	}
	else
	{
		LogHr(hr);
		ASSERT(FALSE);
		return ErrorCodes::REQUEST_FAIL_DELETE;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Convert task type to string
//
std::string CPersistentData::GetTaskType(const SMetricsMsg::SHdr& msgHdr)
{
	switch(msgHdr.msgType)
	{
	case SMetricsMsg::AUTOVIOLATE_CTRL:
		return _T("AVD");

	case SMetricsMsg::MEASURE_CTRL:
		switch(msgHdr.msgSubType)
		{
		case SMetricsMsg::SCHED_AVD_REQUEST:
			return _T("AVD");

		case SMetricsMsg::SCHED_OCCUP_REQUEST:
			return _T("Occupancy");

		case SMetricsMsg::SCHED_REQUEST:
			return _T("Metrics");

		case SMetricsMsg::SCHED_SCANDF_REQUEST:
			return _T("DF Scan");

		default:
			return _T("Unknown");
		}

	case SMetricsMsg::OCCUPANCY_CTRL:
		return _T("Occupancy");

	case SMetricsMsg::OCCUPANCYDF_CTRL:
		return _T("DF Scan");

	default:
		return _T("Unknown");
	}
}


//////////////////////////////////////////////////////////////////////
//
// Fill in a SServerWorkloadResponse structure
//
void CPersistentData::GetWorkload(const SMetricsMsg::SServerWorkloadRequest& cmd, SMetricsMsg::SServerWorkloadResponse& resp)
{
	std::unique_lock<std::recursive_mutex> lock(m_critSect);
	resp.metricsWorkload.measType = SMetricsMsg::SServerWorkloadResponse::SMeasWorkload::METRICS;
	resp.metricsWorkload.numMeasurements = 0;
	resp.avdWorkload.measType = SMetricsMsg::SServerWorkloadResponse::SMeasWorkload::AVD;
	resp.avdWorkload.numMeasurements = 0;
	resp.dfScanWorkload.measType = SMetricsMsg::SServerWorkloadResponse::SMeasWorkload::DFSCAN;
	resp.dfScanWorkload.numMeasurements = 0;
	resp.specOccWorkload.measType = SMetricsMsg::SServerWorkloadResponse::SMeasWorkload::OCCUPANCY;
	resp.specOccWorkload.numMeasurements = 0;

	// Scan the table
	unsigned long measureId = 0;
	unsigned long taskId;
	unsigned long key;
	SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;
	DATE startTime;
	DATE stopTime;
	DATE updateTime;
	SMetricsMsg msg;
	int hr;

	for (hr = CScheduleTbl::GetRow(CScheduleTbl::FIRST, measureId, taskId, key, state, startTime, stopTime, updateTime, &msg);
		hr == SQLITE_ROW;
		hr = CScheduleTbl::GetRow(CScheduleTbl::NEXT, measureId, taskId, key, state, startTime, stopTime, updateTime, &msg))
	{
		if (hr == SQLITE_ROW && (cmd.clientOptions.clientId == 0 || state == SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING ||
			state == SMetricsMsg::SMeasureCtrlMsgStateResponse::RESUMED ||
			state == SMetricsMsg::SMeasureCtrlMsgStateResponse::SUSPENDED ||
			state == SMetricsMsg::SMeasureCtrlMsgStateResponse::IDLE) &&
			startTime <= cmd.clientOptions.endMeasureDateTime && stopTime >= cmd.clientOptions.startMeasureDateTime)
		{
			// Found an applicable task
			switch(msg.hdr.msgSubType)
			{
			case SMetricsMsg::SCHED_REQUEST:
				if(cmd.workloadRequest.reqMetricsMeasurements &&
					resp.metricsWorkload.numMeasurements < SMetricsMsg::SServerWorkloadResponse::SMeasWorkload::MAX_WORKLOAD_ITEMS)
				{
					resp.metricsWorkload.info[resp.metricsWorkload.numMeasurements].clientId =
						msg.body.schedMsgRequest.clientOptions.clientId;
					resp.metricsWorkload.info[resp.metricsWorkload.numMeasurements].startTime = startTime;
					resp.metricsWorkload.info[resp.metricsWorkload.numMeasurements].endTime = stopTime;
					resp.metricsWorkload.info[resp.metricsWorkload.numMeasurements].state = state;
					strncpy(resp.metricsWorkload.info[resp.metricsWorkload.numMeasurements].clientName,
						msg.body.schedMsgRequest.clientOptions.clientName, sizeof(resp.metricsWorkload.info[resp.metricsWorkload.numMeasurements].clientName));
//					strcpy_s(resp.metricsWorkload.info[resp.metricsWorkload.numMeasurements].ipAddr,
//						msg.body.schedMsgRequest.clientOptions.ipAddr);
					strncpy(resp.metricsWorkload.info[resp.metricsWorkload.numMeasurements].ipAddr,
						msg.body.schedMsgRequest.clientOptions.ipAddr, sizeof(resp.metricsWorkload.info[resp.metricsWorkload.numMeasurements].ipAddr));
					++resp.metricsWorkload.numMeasurements;
				}

				break;

			case SMetricsMsg::SCHED_AVD_REQUEST:
				if(cmd.workloadRequest.reqAvdMeasurements &&
					resp.avdWorkload.numMeasurements < SMetricsMsg::SServerWorkloadResponse::SMeasWorkload::MAX_WORKLOAD_ITEMS)
				{
					resp.avdWorkload.info[resp.avdWorkload.numMeasurements].clientId =
						msg.body.schedMsgAutoViolateRequest.clientOptions.clientId;
					resp.avdWorkload.info[resp.avdWorkload.numMeasurements].startTime = startTime;
					resp.avdWorkload.info[resp.avdWorkload.numMeasurements].endTime = stopTime;
					resp.avdWorkload.info[resp.avdWorkload.numMeasurements].state = state;
					strncpy(resp.avdWorkload.info[resp.avdWorkload.numMeasurements].clientName,
						msg.body.schedMsgAutoViolateRequest.clientOptions.clientName, sizeof(resp.avdWorkload.info[resp.avdWorkload.numMeasurements].clientName));
//					strcpy_s(resp.avdWorkload.info[resp.avdWorkload.numMeasurements].ipAddr,
//						msg.body.schedMsgAutoViolateRequest.clientOptions.ipAddr);
					//strncpy(resp.avdWorkload.info[resp.avdWorkload.numMeasurements].ipAddr,
					//	msg.body.schedMsgAutoViolateRequest.clientOptions.ipAddr, sizeof(resp.avdWorkload.info[resp.avdWorkload.numMeasurements].ipAddr));
					memcpy(resp.avdWorkload.info[resp.avdWorkload.numMeasurements].ipAddr,
						msg.body.schedMsgAutoViolateRequest.clientOptions.ipAddr, sizeof(resp.avdWorkload.info[resp.avdWorkload.numMeasurements].ipAddr));
					++resp.avdWorkload.numMeasurements;
				}

				break;

			case SMetricsMsg::SCHED_SCANDF_REQUEST:
				if(cmd.workloadRequest.reqDfScanMeasurements &&
					resp.dfScanWorkload.numMeasurements < SMetricsMsg::SServerWorkloadResponse::SMeasWorkload::MAX_WORKLOAD_ITEMS)
				{
					resp.dfScanWorkload.info[resp.dfScanWorkload.numMeasurements].clientId =
						msg.body.schedMsgScanDfRequest.clientOptions.clientId;
					resp.dfScanWorkload.info[resp.dfScanWorkload.numMeasurements].startTime = startTime;
					resp.dfScanWorkload.info[resp.dfScanWorkload.numMeasurements].endTime = stopTime;
					resp.dfScanWorkload.info[resp.dfScanWorkload.numMeasurements].state = state;
					strncpy(resp.dfScanWorkload.info[resp.dfScanWorkload.numMeasurements].clientName,
						msg.body.schedMsgScanDfRequest.clientOptions.clientName, sizeof(resp.dfScanWorkload.info[resp.dfScanWorkload.numMeasurements].clientName));
//					strcpy_s(resp.dfScanWorkload.info[resp.dfScanWorkload.numMeasurements].ipAddr,
//						msg.body.schedMsgScanDfRequest.clientOptions.ipAddr);
					strncpy(resp.dfScanWorkload.info[resp.dfScanWorkload.numMeasurements].ipAddr,
						msg.body.schedMsgScanDfRequest.clientOptions.ipAddr, sizeof(resp.dfScanWorkload.info[resp.dfScanWorkload.numMeasurements].ipAddr));
					++resp.dfScanWorkload.numMeasurements;
				}

				break;

			case SMetricsMsg::SCHED_OCCUP_REQUEST:
				if(cmd.workloadRequest.reqMetricsMeasurements &&
					resp.specOccWorkload.numMeasurements < SMetricsMsg::SServerWorkloadResponse::SMeasWorkload::MAX_WORKLOAD_ITEMS)
				{
					resp.specOccWorkload.info[resp.specOccWorkload.numMeasurements].clientId =
						msg.body.schedMsgOccupancyRequest.clientOptions.clientId;
					resp.specOccWorkload.info[resp.specOccWorkload.numMeasurements].startTime = startTime;
					resp.specOccWorkload.info[resp.specOccWorkload.numMeasurements].endTime = stopTime;
					resp.specOccWorkload.info[resp.specOccWorkload.numMeasurements].state = state;
					strncpy(resp.specOccWorkload.info[resp.specOccWorkload.numMeasurements].clientName,
						msg.body.schedMsgOccupancyRequest.clientOptions.clientName, sizeof(resp.specOccWorkload.info[resp.specOccWorkload.numMeasurements].clientName));
//					strcpy_s(resp.specOccWorkload.info[resp.specOccWorkload.numMeasurements].ipAddr,
//						msg.body.schedMsgOccupancyRequest.clientOptions.ipAddr);
					strncpy(resp.specOccWorkload.info[resp.specOccWorkload.numMeasurements].ipAddr,
						msg.body.schedMsgOccupancyRequest.clientOptions.ipAddr, sizeof(resp.specOccWorkload.info[resp.specOccWorkload.numMeasurements].ipAddr));
					++resp.specOccWorkload.numMeasurements;
				}

				break;

			default:
				hr = SQLITE_ERROR;
				break;
			}
		}
	}

	resp.statusOfRequest = (hr == SQLITE_DONE ? ErrorCodes::TASK_COMPLETED : ErrorCodes::SERVER_FAIL_WORKLOAD);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Process an AUTOVIOLATE_CTRL message
//
void CPersistentData::OnAvdCtrl(_In_ const SEquipCtrlMsg* msg)
{
	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::AUTOVIOLATE_BWMEAS_RESULT:
	case SEquipCtrlMsg::AUTOVIOLATE_FREQMEAS_RESULT:
		SaveResult(msg->hdr.destAddr, msg->hdr.msgSubType, msg->body.avdMeasureResult);
		break;

	case SEquipCtrlMsg::OCC_CHANNEL_RESULT:
		SaveResult(msg->hdr.destAddr, msg->hdr.msgSubType, msg->body.occResult);
		break;

	case SEquipCtrlMsg::OCC_FREQUENCY_RESULT:
		SaveResult(msg->hdr.destAddr, msg->hdr.msgSubType, msg->body.frequencyVsChannelResp);
		break;

	case SEquipCtrlMsg::OCCUPANCY_SOLICIT_STATE_RESP:
	case SEquipCtrlMsg::OCCUPANCY_STATE_RESP:
		{
			SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;

			switch(msg->body.stateResp.state)
			{
			case SEquipCtrlMsg::SStateResp::IDLE:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::IDLE;
				break;

			case SEquipCtrlMsg::SStateResp::RUNNING:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING;
				break;

			case SEquipCtrlMsg::SStateResp::SUSPENDED:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::SUSPENDED;
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			DATE now = Utility::CurrentTimeAsDATE();
//			TciGps::GetDATE(now);
			UpdateSchedule(msg->hdr.destAddr, state, msg->body.stateResp.completionTime, now);
		}

		break;

	case SEquipCtrlMsg::TASK_STATUS:
		{
			SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;

			switch(msg->body.equipTaskStatusResp.status)
			{
			case SEquipCtrlMsg::SEquipTaskStatusResp::ACTIVE:
			case SEquipCtrlMsg::SEquipTaskStatusResp::RESTARTED:
			case SEquipCtrlMsg::SEquipTaskStatusResp::STARTED:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING;
				UpdateSchedule(msg->hdr.destAddr, msg->body.equipTaskStatusResp.taskId, msg->body.equipTaskStatusResp.key,
					state, msg->body.equipTaskStatusResp.dateTime);
				break;

			case SEquipCtrlMsg::SEquipTaskStatusResp::COMPLETED:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::COMPLETED;
				CLog::Log(CLog::INFORMATION, "AVD task %u completed", msg->hdr.destAddr);
				UpdateSchedule(msg->hdr.destAddr, msg->body.equipTaskStatusResp.taskId, msg->body.equipTaskStatusResp.key,
					state, msg->body.equipTaskStatusResp.dateTime, msg->body.equipTaskStatusResp.dateTime);
				break;

			case SEquipCtrlMsg::SEquipTaskStatusResp::TERMINATED:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::TERMINATED;
				CLog::Log(CLog::INFORMATION, "AVD task %u terminated", msg->hdr.destAddr);
				UpdateSchedule(msg->hdr.destAddr, msg->body.equipTaskStatusResp.taskId, msg->body.equipTaskStatusResp.key,
					state, msg->body.equipTaskStatusResp.dateTime, msg->body.equipTaskStatusResp.dateTime);
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			if (state == SMetricsMsg::SMeasureCtrlMsgStateResponse::COMPLETED ||
				state == SMetricsMsg::SMeasureCtrlMsgStateResponse::TERMINATED)
			{
				// Clear the restart data
				DeleteResults(msg->hdr.destAddr, SEquipCtrlMsg::AVD_RESTART_DATA);
			}
		}

		break;

	default:
		THROW_LOGIC_ERROR();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Process an OCCUPANCY_CTRL message
//
void CPersistentData::OnOccupancyCtrl(_In_ const SEquipCtrlMsg* msg)
{
	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::EFLD_CHANNEL_RESULT:
	case SEquipCtrlMsg::MSGLEN_CHANNEL_RESULT:
	case SEquipCtrlMsg::OCC_CHANNEL_RESULT:
	case SEquipCtrlMsg::OCC_SPECGRAM_RESULT:
	case SEquipCtrlMsg::OCC_TIMEOFDAY_RESULT:
	case SEquipCtrlMsg::OCC_EFLD_TIMEOFDAY_RESULT:
		SaveResult(msg->hdr.destAddr, msg->hdr.msgSubType, msg->body.occResult);
		break;

	case SEquipCtrlMsg::MSGLEN_DIST_RESPONSE:
		SaveResult(msg->hdr.destAddr, msg->hdr.msgSubType, msg->body.msgLengthDistributionResp);
		break;

	case SEquipCtrlMsg::OCC_FREQUENCY_RESULT:
		SaveResult(msg->hdr.destAddr, msg->hdr.msgSubType, msg->body.frequencyVsChannelResp);
		break;

	case SEquipCtrlMsg::OCCUPANCY_SOLICIT_STATE_RESP:
	case SEquipCtrlMsg::OCCUPANCY_STATE_RESP:
		{
			SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;

			switch(msg->body.stateResp.state)
			{
			case SEquipCtrlMsg::SStateResp::IDLE:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::IDLE;
				break;

			case SEquipCtrlMsg::SStateResp::RUNNING:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING;
				break;

			case SEquipCtrlMsg::SStateResp::SUSPENDED:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::SUSPENDED;
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			DATE now = Utility::CurrentTimeAsDATE();
//			TciGps::GetDATE(now);
			UpdateSchedule(msg->hdr.destAddr, state, msg->body.stateResp.completionTime, now);
		}

		break;

	case SEquipCtrlMsg::TASK_STATUS:
		{
			SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;

			switch(msg->body.equipTaskStatusResp.status)
			{
			case SEquipCtrlMsg::SEquipTaskStatusResp::ACTIVE:
			case SEquipCtrlMsg::SEquipTaskStatusResp::RESTARTED:
			case SEquipCtrlMsg::SEquipTaskStatusResp::STARTED:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING;
				UpdateSchedule(msg->hdr.destAddr, msg->body.equipTaskStatusResp.taskId, msg->body.equipTaskStatusResp.key,
					state, msg->body.equipTaskStatusResp.dateTime);
				break;

			case SEquipCtrlMsg::SEquipTaskStatusResp::COMPLETED:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::COMPLETED;
				CLog::Log(CLog::INFORMATION, "Occupancy task %lu completed", msg->hdr.destAddr);
				UpdateSchedule(msg->hdr.destAddr, msg->body.equipTaskStatusResp.taskId, msg->body.equipTaskStatusResp.key,
					state, msg->body.equipTaskStatusResp.dateTime, msg->body.equipTaskStatusResp.dateTime);
				break;

			case SEquipCtrlMsg::SEquipTaskStatusResp::TERMINATED:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::TERMINATED;
				CLog::Log(CLog::INFORMATION, "Occupancy task %lu terminated", msg->hdr.destAddr);
				UpdateSchedule(msg->hdr.destAddr, msg->body.equipTaskStatusResp.taskId, msg->body.equipTaskStatusResp.key,
					state, msg->body.equipTaskStatusResp.dateTime, msg->body.equipTaskStatusResp.dateTime);
				break;

			default:
				THROW_LOGIC_ERROR();
			}


			if (state == SMetricsMsg::SMeasureCtrlMsgStateResponse::COMPLETED ||
				state == SMetricsMsg::SMeasureCtrlMsgStateResponse::TERMINATED)
			{
				auto hr = CResultsTbl::GetFirstRow(msg->hdr.destAddr, SEquipCtrlMsg::OCC_FREQUENCY_RESULT);
				if (hr == SQLITE_OK || hr == SQLITE_ROW)
				{
					// Clear the restart data
					DeleteResults(msg->hdr.destAddr, SEquipCtrlMsg::OCCUP_RESTART_DATA);
				}
				else
				{
					// No results - delete the whole task
					DeleteTask(msg->hdr.destAddr);
				}
			}
		}

		break;

	default:
		THROW_LOGIC_ERROR();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Process an OCCUPANCYDF_CTRL message
//
void CPersistentData::OnOccupancyDfCtrl(_In_ const SEquipCtrlMsg* msg)
{
	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::OCC_FREQUENCY_RESULT:
		SaveResult(msg->hdr.destAddr, msg->hdr.msgSubType, msg->body.frequencyVsChannelResp);
		break;

	case SEquipCtrlMsg::SCANDF_DATA_RESULT:
		SaveResult(msg->hdr.destAddr, msg->hdr.msgSubType, msg->body.scanDfVsChannelResp);
		break;

	case SEquipCtrlMsg::OCCUPANCY_SOLICIT_STATE_RESP:
	case SEquipCtrlMsg::OCCUPANCY_STATE_RESP:
		{
			SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;

			switch(msg->body.stateResp.state)
			{
			case SEquipCtrlMsg::SStateResp::IDLE:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::IDLE;
				break;

			case SEquipCtrlMsg::SStateResp::RUNNING:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING;
				break;

			case SEquipCtrlMsg::SStateResp::SUSPENDED:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::SUSPENDED;
				break;

			default:
				THROW_LOGIC_ERROR();
			}

			DATE now = Utility::CurrentTimeAsDATE();
//			TciGps::GetDATE(now);
			UpdateSchedule(msg->hdr.destAddr, state, msg->body.stateResp.completionTime, now);
		}

		break;

	case SEquipCtrlMsg::TASK_STATUS:
		{
			SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;

			switch(msg->body.equipTaskStatusResp.status)
			{
			case SEquipCtrlMsg::SEquipTaskStatusResp::ACTIVE:
			case SEquipCtrlMsg::SEquipTaskStatusResp::RESTARTED:
			case SEquipCtrlMsg::SEquipTaskStatusResp::STARTED:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING;
				UpdateSchedule(msg->hdr.destAddr, msg->body.equipTaskStatusResp.taskId, msg->body.equipTaskStatusResp.key,
					state, msg->body.equipTaskStatusResp.dateTime);
				break;

			case SEquipCtrlMsg::SEquipTaskStatusResp::COMPLETED:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::COMPLETED;
				CLog::Log(CLog::INFORMATION, "DF Scan task %lu completed", msg->hdr.destAddr);
				UpdateSchedule(msg->hdr.destAddr, msg->body.equipTaskStatusResp.taskId, msg->body.equipTaskStatusResp.key,
					state, msg->body.equipTaskStatusResp.dateTime, msg->body.equipTaskStatusResp.dateTime);
				break;

			case SEquipCtrlMsg::SEquipTaskStatusResp::TERMINATED:
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::TERMINATED;
				CLog::Log(CLog::INFORMATION, "DF Scan task %lu terminated", msg->hdr.destAddr);
				UpdateSchedule(msg->hdr.destAddr, msg->body.equipTaskStatusResp.taskId, msg->body.equipTaskStatusResp.key,
					state, msg->body.equipTaskStatusResp.dateTime, msg->body.equipTaskStatusResp.dateTime);
				break;

			default:
				THROW_LOGIC_ERROR();
			}


			if (state == SMetricsMsg::SMeasureCtrlMsgStateResponse::COMPLETED ||
				state == SMetricsMsg::SMeasureCtrlMsgStateResponse::TERMINATED)
			{
				auto hr = CResultsTbl::GetFirstRow(msg->hdr.destAddr, SEquipCtrlMsg::OCC_FREQUENCY_RESULT);
				if (hr == SQLITE_OK || hr == SQLITE_ROW)
				{
					// Clear the restart data
					DeleteResults(msg->hdr.destAddr, SEquipCtrlMsg::OCCUP_RESTART_DATA);	// TODO: Is this correct?
				}
				else
				{
					// No results - delete the whole task
					DeleteTask(msg->hdr.destAddr);
				}
			}
		}

		break;

	default:
		THROW_LOGIC_ERROR();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Process a message from my queue
//
void CPersistentData::ProcessMsg(DWORD timeout)
{
	QueueMsg msg = m_queue.GetItem(timeout);

	switch (msg->hdr.msgType)
	{
	case SEquipCtrlMsg::AUTOVIOLATE_CTRL:
		OnAvdCtrl(msg.get());
		break;

	case SEquipCtrlMsg::METRICS_CTRL:
		switch(msg->hdr.msgSubType)
		{
		case SEquipCtrlMsg::CAL_SCHED:
			ScheduleNextTaskAndPurge();
			break;

		case SEquipCtrlMsg::GET_MEAS_RESPONSE:
			SaveMeasurement(msg->hdr.destAddr, msg->body.getMeasResp);
			break;

		case SEquipCtrlMsg::GET_MEAS_IQDATA_RESP:
			SaveIqData(msg->hdr.destAddr, msg->body.iqDataResp);
			break;

		case SEquipCtrlMsg::AVD_RESTART_DATA:
		case SEquipCtrlMsg::OCCUP_RESTART_DATA:
		case SEquipCtrlMsg::SCANDF_RESTART_DATA:
			SaveRestartData(msg->hdr.destAddr, msg->hdr.msgSubType, &msg->body, msg->hdr.bodySize);
			break;

		default:
			break;
		}

		break;

	case SEquipCtrlMsg::OCCUPANCY_CTRL:
		OnOccupancyCtrl(msg.get());
		break;

	case SEquipCtrlMsg::OCCUPANCYDF_CTRL:
		OnOccupancyDfCtrl(msg.get());
		break;

	default:
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Recreate database tables
//
void CPersistentData::RecreateTables(void)
{
	// Drop and create tables

	CheckHr(sqlite3_exec(m_db,
		"BEGIN TRANSACTION; "
		"DROP TABLE IF EXISTS Results; "
		"DROP TABLE IF EXISTS Schedule; "
		"CREATE TABLE Results"
		"("
			"MeasureId INT NOT NULL,"
			"MsgSubType INT NOT NULL,"
			"FirstChan INT NOT NULL,"
			"MsgBody BLOB NOT NULL,"
			"FOREIGN KEY(MeasureId) REFERENCES Schedule(MeasureId) ON DELETE CASCADE"
		"); "
		"CREATE UNIQUE INDEX IDX_Results ON Results(MeasureId, MsgSubType, FirstChan); "
		"CREATE TABLE Schedule"
		"("
			"MeasureId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
			"TaskId INT NOT NULL,"
			"\"Key\" INT NOT NULL,"
			"State INT NOT NULL,"
			"StartTime DOUBLE NOT NULL,"
			"StopTime DOUBLE NOT NULL,"
			"UpdateTime DOUBLE NOT NULL,"
			"Msg BLOB NOT NULL"
		"); "
		"COMMIT TRANSACTION;",
		nullptr, nullptr, nullptr));

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Scan tasks at startup to restart running tasks
//
void CPersistentData::RestartTasks(void)
{
	std::unique_lock<std::recursive_mutex> lock(m_critSect);
	unsigned long measureId = 0;
	unsigned long taskId;
	unsigned long key;
	SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;
	DATE startTime;
	DATE stopTime;
	DATE updateTime;
	DATE now = Utility::CurrentTimeAsDATE();
//	TciGps::GetDATE(now);

	// Scan schedule table
	for (int hr = CScheduleTbl::GetRow(CScheduleTbl::FIRST, measureId, taskId, key, state, startTime, stopTime, updateTime);
		hr == SQLITE_ROW;
		hr = CScheduleTbl::GetRow(CScheduleTbl::NEXT, measureId, taskId, key, state, startTime, stopTime, updateTime))
	{
		if (state == SMetricsMsg::SMeasureCtrlMsgStateResponse::RESUMED ||
			state == SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING ||
			state == SMetricsMsg::SMeasureCtrlMsgStateResponse::SUSPENDED)
		{
			// Task was previously active

			// See if there's restart data
			CTask::SRestartData restartData;
			if (CResultsTbl::GetFirstRow(measureId, SEquipCtrlMsg::AVD_RESTART_DATA, restartData.data) == SQLITE_ROW && !restartData.data.empty())
			{
				// Recreate the task suspended and mark it resumed so the scheduler resumes it
				CAvdTask::Create(restartData);
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::RESUMED;
			}
			else if (CResultsTbl::GetFirstRow(measureId, SEquipCtrlMsg::OCCUP_RESTART_DATA, restartData.data) == SQLITE_ROW && !restartData.data.empty())
			{
				// Recreate the task suspended and mark it resumed so the scheduler resumes it
				COccupancyTask::Create(restartData);
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::RESUMED;
			}
			else if (CResultsTbl::GetFirstRow(measureId, SEquipCtrlMsg::SCANDF_RESTART_DATA, restartData.data) == SQLITE_ROW && !restartData.data.empty())
			{
				// Recreate the task suspended and mark it resumed so the scheduler resumes it
				CScanDfTask::Create(restartData);
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::RESUMED;
			}
			else
			{
				// Reset state to idle so the scheduler starts it up from the beginning
				state = SMetricsMsg::SMeasureCtrlMsgStateResponse::IDLE;
			}

			// Update task state
			if (sqlite3_exec(m_db, "BEGIN TRANSACTION;", nullptr, nullptr, nullptr) == SQLITE_OK)
			{
				CScheduleTbl::UpdateRow(measureId, state, now);
				sqlite3_exec(m_db, "COMMIT TRANSACTION;", nullptr, nullptr, nullptr);
			}
			else
			{
				printf("CPersistentData::RestartTasks BEGIN failed\n");
			}
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Retrieve a measurement result
//
ErrorCodes::EErrorCode CPersistentData::RetrieveMeasurement(unsigned long measureId, SMetricsMsg::SGetMeasResp& measurement)
{
	memset(&measurement, 0, sizeof(measurement));
	std::unique_lock<std::recursive_mutex> lock(m_critSect);
	SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;
	unsigned long taskId;
	unsigned long key;
	DATE startTime;
	DATE stopTime;
	DATE updateTime;
	int  hr = CScheduleTbl::GetRow(CScheduleTbl::FIND, measureId, taskId, key, state, startTime, stopTime, updateTime);

	switch (hr)
	{
	case SQLITE_ROW:
		if (state == SMetricsMsg::SMeasureCtrlMsgStateResponse::COMPLETED)
		{
			break;
		}
		else if(state == SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING)
		{
			return ErrorCodes::MEASURE_IN_PROGRESS;
		}
		else if(state == SMetricsMsg::SMeasureCtrlMsgStateResponse::IDLE)
		{
			return ErrorCodes::MEASURE_SCHEDULED;
		}
		else
		{
			return ErrorCodes::REQUEST_UNKNOWN;
		}

	case SQLITE_DONE:
		return ErrorCodes::REQUEST_FAIL_NOT_FOUND;

	default:
		LogHr(hr);
		return ErrorCodes::REQUEST_UNKNOWN;
	}

	hr = CResultsTbl::GetRow(CResultsTbl::FIND, measureId, SEquipCtrlMsg::GET_MEAS_RESPONSE, measurement);

	switch(hr)
	{
	case SQLITE_ROW:
		return ErrorCodes::TASK_COMPLETED;

	case SQLITE_DONE:
		return ErrorCodes::REQUEST_FAIL_NOT_FOUND;

	default:
		LogHr(hr);
		return ErrorCodes::REQUEST_UNKNOWN;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Retrieve schedule data
//
ErrorCodes::EErrorCode CPersistentData::RetrieveSchedule(unsigned long measureId, unsigned long& taskId, unsigned long& key,
	SMetricsMsg::SMeasureCtrlMsgStateResponse::EState& state, DATE& startTime, DATE& stopTime, DATE& updateTime, SMetricsMsg* msg)
{
	state = SMetricsMsg::SMeasureCtrlMsgStateResponse::UNKNOWN;
	updateTime = SMetricsMsg::MAX_DATE;
	std::unique_lock<std::recursive_mutex> lock(m_critSect);

	switch (int hr = CScheduleTbl::GetRow(CScheduleTbl::FIND, measureId, taskId, key, state, startTime, stopTime, updateTime, msg))
	{
	case SQLITE_ROW:
		return ErrorCodes::TASK_COMPLETED;

	case SQLITE_DONE:
		state = SMetricsMsg::SMeasureCtrlMsgStateResponse::UNKNOWN;
		startTime = SMetricsMsg::MAX_DATE;
		stopTime = SMetricsMsg::MAX_DATE;
		updateTime = SMetricsMsg::MAX_DATE;
		return ErrorCodes::REQUEST_FAIL_NOT_FOUND;

	default:
		LogHr(hr);
		state = SMetricsMsg::SMeasureCtrlMsgStateResponse::FAILED;
		startTime = SMetricsMsg::MAX_DATE;
		stopTime = SMetricsMsg::MAX_DATE;
		updateTime = SMetricsMsg::MAX_DATE;
		return ErrorCodes::REQUEST_UNKNOWN;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Run a scheduled task
//
void CPersistentData::RunScheduledTask(void)
{
	std::unique_lock<std::recursive_mutex> lock(m_critSect);
	unsigned long taskId;
	unsigned long key;
	SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;
	DATE startTime;
	DATE stopTime;
	DATE updateTime;
	SMetricsMsg msg;
	msg.hdr.msgType = SEquipCtrlMsg::SMS_ERROR_REPORT;

	try
	{
		CWeakSingleton<CMetrics> metrics;

		if (/*!metrics->InPriorityMode() &&*/ CWeakSingleton<CEquipControl>()->VCPControlClient() == nullptr)
		{
			ErrorCodes::EErrorCode error = RetrieveSchedule(m_scheduledMeasureId, taskId, key, state, startTime, stopTime,
				updateTime, &msg);

			if (error != ErrorCodes::TASK_COMPLETED)
			{
				throw error;
			}

//			TciGps::GetDATE(updateTime);
			updateTime = Utility::CurrentTimeAsDATE();

			if (state == SMetricsMsg::SMeasureCtrlMsgStateResponse::RESUMED)
			{
				// Resume a restarted task
				switch(msg.hdr.msgType)
				{
				case SMetricsMsg::AUTOVIOLATE_CTRL:
					msg.hdr.msgSubType = SMetricsMsg::AVD_RESUME_REQUEST;
					break;

				case SMetricsMsg::OCCUPANCY_CTRL:
					msg.hdr.msgSubType = SMetricsMsg::OCCUP_RESUME_REQUEST;
					break;

				case SMetricsMsg::OCCUPANCYDF_CTRL:
					msg.hdr.msgSubType = SMetricsMsg::SCANDF_RESUME_REQUEST;
					break;

				default:
					THROW_LOGIC_ERROR();
				}

				msg.hdr.bodySize = sizeof(SMetricsMsg::SMeasureCtrlMsgGenericRequest);
				msg.body.measureCtrlMsgGenericRequest.measureId = m_scheduledMeasureId;
			}

			metrics->ForwardToEquipControl(msg, nullptr, m_scheduledMeasureId);
			CLog::Log(CLog::INFORMATION, "%s task %lu %s", GetTaskType(msg.hdr).c_str(), m_scheduledMeasureId,
				(state == SMetricsMsg::SMeasureCtrlMsgStateResponse::RESUMED ? "restarted" : "started"));
			state = (msg.hdr.msgType == SMetricsMsg::MEASURE_CTRL ?
				SMetricsMsg::SMeasureCtrlMsgStateResponse::RUNNING : SMetricsMsg::SMeasureCtrlMsgStateResponse::UNKNOWN);
			UpdateSchedule(m_scheduledMeasureId, state, updateTime);
		}
	}
	catch(ErrorCodes::EErrorCode error)
	{
		if (error != ErrorCodes::CMDINVALIDVCPMODE && error != ErrorCodes::HARDWAREBUSY)
		{
			// Failed
			state = SMetricsMsg::SMeasureCtrlMsgStateResponse::FAILED;
			UpdateSchedule(m_scheduledMeasureId, state, updateTime);
			CLog::Log(CLog::INFORMATION, "Failed to %s %s task %lu with error code %d\n",
				(state == SMetricsMsg::SMeasureCtrlMsgStateResponse::RESUMED ? "restart" : "start"),
				GetTaskType(msg.hdr).c_str(), m_scheduledMeasureId, error);
		}
	}
	catch(CWeakSingleton<CMetrics>::NoInstance&)
	{
		// Ignore
	}
	catch(CWeakSingleton<CEquipControl>::NoInstance&)
	{
		// Ignore
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Save an IQ data result
//
void CPersistentData::SaveIqData(unsigned long measureId, _In_ const SMetricsMsg::SIqDataResp& iqData)
{
	std::unique_lock<std::recursive_mutex> lock(m_critSect);
	int hr = sqlite3_exec(m_db, "BEGIN TRANSACTION;", nullptr, nullptr, nullptr);

	if (hr == SQLITE_OK)
	{
		hr = CResultsTbl::UpdateRow(measureId, SEquipCtrlMsg::GET_MEAS_IQDATA_RESP, iqData.packetIndex, iqData);
	}

	hr = sqlite3_exec(m_db, "COMMIT TRANSACTION;", nullptr, nullptr, nullptr);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Save a measurement result
//
ErrorCodes::EErrorCode CPersistentData::SaveMeasurement(unsigned long measureId, _In_ const SMetricsMsg::SGetMeasResp& measurement)
{
	std::unique_lock<std::recursive_mutex> lock(m_critSect);
	DATE now = Utility::CurrentTimeAsDATE();
//	TciGps::GetDATE(now);

	int hr = sqlite3_exec(m_db, "BEGIN TRANSACTION;", nullptr, nullptr, nullptr);

	if (hr == SQLITE_OK)
	{
		hr = CResultsTbl::UpdateRow(measureId, SEquipCtrlMsg::GET_MEAS_RESPONSE, 0, measurement);
	}

	ErrorCodes::EErrorCode status;

	switch (hr)
	{
	case SQLITE_OK:
	case SQLITE_DONE:
		{
			SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state = SMetricsMsg::SMeasureCtrlMsgStateResponse::COMPLETED;
			CScheduleTbl::UpdateRow(measureId, state, now);
			status = ErrorCodes::TASK_COMPLETED;
		}

		break;

	default:
		LogHr(hr);
		SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state = SMetricsMsg::SMeasureCtrlMsgStateResponse::FAILED;
		CScheduleTbl::UpdateRow(measureId, state, now);
		status = ErrorCodes::MEASURE_DATA_NOT_STORED;
		break;
	}

	hr = sqlite3_exec(m_db, "COMMIT TRANSACTION;", nullptr, nullptr, nullptr);

	CLog::Log(CLog::INFORMATION, "Metrics task %lu completed", measureId);
	return status;
}


//////////////////////////////////////////////////////////////////////
//
// Save restart data
//
ErrorCodes::EErrorCode CPersistentData::SaveRestartData(unsigned long measureId, unsigned long msgSubType,
	const void* data, unsigned long size)
{
	std::unique_lock<std::recursive_mutex> lock(m_critSect);
	DATE now = Utility::CurrentTimeAsDATE();
//	TciGps::GetDATE(now);

	int hr = sqlite3_exec(m_db, "BEGIN TRANSACTION;", nullptr, nullptr, nullptr);

	if (hr == SQLITE_OK)
	{
		hr = CResultsTbl::UpdateRow(measureId, msgSubType, 0, data, size);
	}

	if (hr == SQLITE_DONE)
	{
		hr = CScheduleTbl::UpdateRow(measureId, now);
	}

	ErrorCodes::EErrorCode status;

	switch (hr)
	{
	case SQLITE_DONE:
		status = ErrorCodes::TASK_COMPLETED;
		break;

	default:
		LogHr(hr);
		CScheduleTbl::UpdateRow(measureId, SMetricsMsg::SMeasureCtrlMsgStateResponse::FAILED, now);
		status = ErrorCodes::MEASURE_DATA_NOT_STORED;
		break;
	}

	hr = sqlite3_exec(m_db, "COMMIT TRANSACTION;", nullptr, nullptr, nullptr);

	return status;
}


//////////////////////////////////////////////////////////////////////
//
// Save a schedule
//
ErrorCodes::EErrorCode CPersistentData::SaveSchedule(_Out_ unsigned long& measureId, unsigned long taskId, unsigned long key,
	SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE startTime, DATE stopTime, DATE updateTime, _In_ const SMetricsMsg& msg)
{
	std::unique_lock<std::recursive_mutex> lock(m_critSect);
	int hr = sqlite3_exec(m_db, "BEGIN TRANSACTION;", nullptr, nullptr, nullptr);

	if (hr == SQLITE_OK)
	{
		hr = CScheduleTbl::InsertRow(measureId, taskId, key, state, startTime, stopTime, updateTime, msg);
	}

	ErrorCodes::EErrorCode status;

	switch(hr)
	{
	case SQLITE_OK:
	case SQLITE_DONE:
		{
			status = ErrorCodes::TASK_COMPLETED;

			// Update schedule
			SEquipCtrlMsg resched;
			resched.hdr.msgType = SEquipCtrlMsg::METRICS_CTRL;
			resched.hdr.msgSubType = SEquipCtrlMsg::CAL_SCHED;
			resched.hdr.bodySize = 0;
			AddMsg(resched);
		}

		break;

	default:
		LogHr(hr);
		status = ErrorCodes::MEASURE_DATA_NOT_STORED;
		measureId = 0;
		break;
	}

	hr = sqlite3_exec(m_db, "COMMIT TRANSACTION;", nullptr, nullptr, nullptr);

	return status;
}


//////////////////////////////////////////////////////////////////////
//
// Schedule the next task
//
void CPersistentData::ScheduleNextTaskAndPurge(void)
{
	// Scan the table for idle (scheduled) tasks and tasks that need to be purged
	unsigned long prevScheduledMeasureId = m_scheduledMeasureId;
	m_scheduledMeasureId = 0;
	m_scheduledTime = SMetricsMsg::MAX_DATE;
	DATE purgeAge = m_config->GetMutableConfig().miscConfig.databasePurgeAge;
	std::unique_lock<std::recursive_mutex> lock(m_critSect);
	unsigned long measureId = 0;
	unsigned long taskId;
	unsigned long key;
	SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;
	DATE startTime;
	DATE stopTime;
	DATE updateTime;
	DATE now = Utility::CurrentTimeAsDATE();
//	TciGps::GetDATE(now);

	for (int hr = CScheduleTbl::GetRow(CScheduleTbl::FIRST, measureId, taskId, key, state, startTime, stopTime, updateTime);
		hr == SQLITE_ROW;
		hr = CScheduleTbl::GetRow(CScheduleTbl::NEXT, measureId, taskId, key, state, startTime, stopTime, updateTime))
	{
		if ((state == SMetricsMsg::SMeasureCtrlMsgStateResponse::RESUMED || state == SMetricsMsg::SMeasureCtrlMsgStateResponse::IDLE) &&
			startTime < m_scheduledTime)
		{
			m_scheduledMeasureId = measureId;
			m_scheduledTime = startTime;
		}
		else if (((state == SMetricsMsg::SMeasureCtrlMsgStateResponse::COMPLETED ||
			state == SMetricsMsg::SMeasureCtrlMsgStateResponse::TERMINATED) &&
			now - stopTime > purgeAge) || ((state == SMetricsMsg::SMeasureCtrlMsgStateResponse::FAILED ||
			state == SMetricsMsg::SMeasureCtrlMsgStateResponse::UNKNOWN) && now - updateTime > purgeAge))
		{
			// Task older than purge age
			hr = sqlite3_exec(m_db, "BEGIN TRANSACTION;", nullptr, nullptr, nullptr);
			if (hr == SQLITE_OK)
			{
				hr = CScheduleTbl::DeleteRow(measureId);

				if (hr == SQLITE_OK || hr == SQLITE_DONE)
				{
					hr = sqlite3_exec(m_db, "COMMIT TRANSACTION;", nullptr, nullptr, nullptr);
					CLog::Log(CLog::INFORMATION, "Task %lu purged from database", measureId);
				}
				else
				{
					hr = sqlite3_exec(m_db, "ROLLBACK TRANSACTION;", nullptr, nullptr, nullptr);
				}
			}

			if (hr != SQLITE_OK)
			{
				LogHr(hr);
			}
		}
	}

	if (m_scheduledMeasureId != 0 && m_scheduledMeasureId == prevScheduledMeasureId && m_scheduledTime < now)
	{
		// Same task as before so wait a while before trying again
//		TciGps::GetDATE(m_scheduledTime);
		m_scheduledTime = Utility::CurrentTimeAsDATE();
		m_scheduledTime += RETRY_DELAY;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send IQ data results
//
bool CPersistentData::SendIqDataResults(CMetricsNet* client, SMetricsMsg& resp, const SMetricsMsg::SMeasureCtrlMsgIqDataRequest& request,
		unsigned long stationId, SMetricsMsg::SMeasureCtrlMsgResultsIqDataResponse& body)
{
	bool sent = false;
	body.clientRequest.clientId = request.clientId;
	body.clientRequest.measureId = request.measureId;
	body.stationId = stationId;
	resp.hdr.msgSubType = SMetricsMsg::MEASUREMENT_IQDATA_RESPONSE;
	resp.hdr.bodySize = sizeof(SMetricsMsg::SMeasureCtrlMsgResultsIqDataResponse);

	// Get the task state
	unsigned long taskId, key;
	SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state;
	DATE startTime, stopTime, updateTime;
	RetrieveSchedule(request.measureId, taskId, key, state, startTime, stopTime, updateTime, nullptr);

	// Get and send all the records
	std::unique_lock<std::mutex> resultsLock(m_sendResultsCritSect);
	std::unique_lock<std::recursive_mutex> lock(m_critSect);

	if (state == SMetricsMsg::SMeasureCtrlMsgStateResponse::COMPLETED || state == SMetricsMsg::SMeasureCtrlMsgStateResponse::TERMINATED ||
			state == SMetricsMsg::SMeasureCtrlMsgStateResponse::FAILED)
	{
		for (int hr = CResultsTbl::GetRow(CResultsTbl::FIND, request.measureId, SEquipCtrlMsg::GET_MEAS_IQDATA_RESP, body.iqData);
			hr == SQLITE_ROW;
			hr = CResultsTbl::GetRow(CResultsTbl::NEXT, request.measureId, SEquipCtrlMsg::GET_MEAS_IQDATA_RESP, body.iqData))
		{
			lock.unlock();

			if (body.iqData.packetIndex >= request.startingPacketIndex)
			{
				CMetricsNet::Send(client, resp, _T("MEASUREMENT_IQDATA_RESPONSE"));
				printf("CMetricsNet::Send MEASUREMENT_IQDATA_RESPONSE %lu %hu\n", body.iqData.packetIndex, body.iqData.numIqData);
				sent = true;
			}

			lock.lock();
		}
	}

	return sent;
}


//////////////////////////////////////////////////////////////////////
//
// Thread function
//
void CPersistentData::Thread(void)
{
	RestartTasks();
	ScheduleNextTaskAndPurge();

	while (true)
	{
		try
		{
			unsigned int timeout = INFINITE;

			if (m_scheduledTime != SMetricsMsg::MAX_DATE)
			{
				// Get delay until next scheduled task
				DATE now = Utility::CurrentTimeAsDATE();
//				TciGps::GetDATE(now);
				timeout = (m_scheduledTime < now ? 0 : static_cast<unsigned int>((m_scheduledTime - now) * Units::MILLISECONDS_PER_DAY)); // ms
			}

			// Process a message from the queue
			ProcessMsg(timeout);
		}
		catch (Queue::EProblem problem)
		{
			switch (problem)
			{
			case Queue::TIMEDOUT:
				// Time to run a scheduled task
				RunScheduledTask();
				ScheduleNextTaskAndPurge();
				break;

			case Queue::CLOSED:
				// Shutdown
				return;

			default:
				THROW_LOGIC_ERROR();
			}
		}
	}
}


//////////////////////////////////////////////////////////////////////
//
// Update a schedule
//
ErrorCodes::EErrorCode CPersistentData::UpdateSchedule(unsigned long measureId, SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state,
	DATE updateTime)
{
	std::unique_lock<std::recursive_mutex> lock(m_critSect);

	int hr = sqlite3_exec(m_db, "BEGIN TRANSACTION;", nullptr, nullptr, nullptr);

	if (hr == SQLITE_OK)
	{
		hr = CScheduleTbl::UpdateRow(measureId, state, updateTime);
	}

	ErrorCodes::EErrorCode status = ErrorCodes::REQUEST_UNKNOWN;

	switch(hr)
	{
	case SQLITE_DONE:
		status = (sqlite3_changes(m_db) > 0 ? ErrorCodes::TASK_COMPLETED : ErrorCodes::REQUEST_FAIL_NOT_FOUND);
		break;

	default:
		LogHr(hr);
		break;
	}

	hr = sqlite3_exec(m_db, "COMMIT TRANSACTION;", nullptr, nullptr, nullptr);

	return status;
}

ErrorCodes::EErrorCode CPersistentData::UpdateSchedule(unsigned long measureId, SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state,
	DATE stopTime, DATE updateTime)
{
	std::unique_lock<std::recursive_mutex> lock(m_critSect);

	int hr = sqlite3_exec(m_db, "BEGIN TRANSACTION;", nullptr, nullptr, nullptr);

	if (hr == SQLITE_OK)
	{
		hr = CScheduleTbl::UpdateRow(measureId, state, stopTime, updateTime);
	}

	ErrorCodes::EErrorCode status = ErrorCodes::REQUEST_UNKNOWN;

	switch(hr)
	{
	case SQLITE_OK:
	case SQLITE_DONE:
		status = (sqlite3_changes(m_db) > 0 ? ErrorCodes::TASK_COMPLETED : ErrorCodes::REQUEST_FAIL_NOT_FOUND);
		break;

	default:
		LogHr(hr);
		break;
	}

	hr = sqlite3_exec(m_db, "COMMIT TRANSACTION;", nullptr, nullptr, nullptr);

	return status;

}

ErrorCodes::EErrorCode CPersistentData::UpdateSchedule(unsigned long measureId, unsigned long taskId, unsigned long key,
	SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE updateTime)
{
	std::unique_lock<std::recursive_mutex> lock(m_critSect);

	int hr = sqlite3_exec(m_db, "BEGIN TRANSACTION;", nullptr, nullptr, nullptr);

	if (hr == SQLITE_OK)
	{
		hr = CScheduleTbl::UpdateRow(measureId, taskId, key, state, updateTime);
	}

	ErrorCodes::EErrorCode status = ErrorCodes::REQUEST_UNKNOWN;

	switch(hr)
	{
	case SQLITE_OK:
	case SQLITE_DONE:
		status = (sqlite3_changes(m_db) > 0 ? ErrorCodes::TASK_COMPLETED : ErrorCodes::REQUEST_FAIL_NOT_FOUND);
		break;

	default:
		LogHr(hr);
		break;
	}

	hr = sqlite3_exec(m_db, "COMMIT TRANSACTION;", nullptr, nullptr, nullptr);

	return status;
}

ErrorCodes::EErrorCode CPersistentData::UpdateSchedule(unsigned long measureId, unsigned long taskId, unsigned long key,
	SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE stopTime, DATE updateTime)
{
	std::unique_lock<std::recursive_mutex> lock(m_critSect);

	int hr = sqlite3_exec(m_db, "BEGIN TRANSACTION;", nullptr, nullptr, nullptr);

	if (hr == SQLITE_OK)
	{
		hr = CScheduleTbl::UpdateRow(measureId, taskId, key, state, stopTime, updateTime);
	}

	ErrorCodes::EErrorCode status = ErrorCodes::REQUEST_UNKNOWN;

	switch(hr)
	{
	case SQLITE_OK:
	case SQLITE_DONE:
		status = (sqlite3_changes(m_db) > 0 ? ErrorCodes::TASK_COMPLETED : ErrorCodes::REQUEST_FAIL_NOT_FOUND);
		break;

	default:
		LogHr(hr);
		break;
	}

	hr = sqlite3_exec(m_db, "COMMIT TRANSACTION;", nullptr, nullptr, nullptr);

	return status;
}


