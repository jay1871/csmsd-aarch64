/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include <list>
#include "Config.h"
#include "EquipCtrlMsg.h"
#include "Ne10Vec.h"
#include "Units.h"

template <typename T> class CSingleton;

class CPbCal
{
	// Friends
	friend class CSingleton<CPbCal>;

public:
	// Constants
	static const unsigned int MIN_INTEGRATIONS_DF = 128;
	static const unsigned long long VUSHF_CAL_FREQS[];	// Should these come from FPI's?
	static const unsigned long long HF_CAL_FREQS[][2];	// Should these come from FPI's?

	// Functions
	void AddBlock(Units::Frequency tuneFreq, Units::Frequency rxHwBw, size_t band, Units::Frequency binSize, bool hf, bool direct,
		const Ne10F32cVec& ratio, Ne10F32Vec(&amplitude)[2], bool first, bool last);
	void Clear(void);
	bool GetStatus(std::vector<SEquipCtrlMsg::SInitializeDfResp::SPbCalStatus>& status) const;
	bool CheckifPbCalStarted();

private:
	// Types
	struct SCalKey // Map key
	{
		Units::Frequency freq;
		Units::Frequency rxBw;
		size_t band;
		bool hf;
		bool direct;
	};

	struct SCalData // Map value
	{
		Units::Frequency binSize;
		Ne10F32cVec correction;
		Ne10F32Vec amplitude[2]; // Ref and sample
		Ne10F32cVec numIntegrations;
		SEquipCtrlMsg::SInitializeDfResp::SPbCalStatus status;
	};

	class CCalKeyLess // Map comparison predicate object
	:
		public std::binary_function<SCalKey, SCalKey, bool>
	{
	public:
		bool operator()(_In_ const SCalKey& a, _In_ const SCalKey& b) const;

	private:
//		CSingleton<CConfig> m_config;
	};

	typedef std::map<SCalKey, SCalData, CCalKeyLess> CalData;
	typedef std::list<size_t> MruList;

	// Functions
	CPbCal(void);
	static void CalcPbCalStats(const Ne10F32cVec& correction, Units::Frequency rxBw, SEquipCtrlMsg::SInitializeDfResp::SPbCalStatus& status);

	// Data
	CalData m_calData;
	mutable MruList m_mruList;
};

__attribute__((__weak__)) const unsigned long long CPbCal::VUSHF_CAL_FREQS[] =
{
	250000000, 850000000, 1360000000, 1890000000, 2530000000, 3980000000, 5610000000, 6890000000
};

__attribute__((__weak__)) const unsigned long long CPbCal::HF_CAL_FREQS[][2] =
{
	{ 2000000, 30000000 }
};
