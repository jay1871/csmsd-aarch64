/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "EquipControl.h"
#include "EquipCtrlMsg.h"
#include "VCPMsg.h"
#include "NetConnection.h"
#include "Singleton.h"

class CEquipCtrlNet : public CNetConnection<CEquipCtrlNet>
{
public:
	// Functions
	CEquipCtrlNet(LPCTSTR service); // Server
	CEquipCtrlNet(SOCKET socket, _In_ CEquipCtrlNet*); // Dynamic connection
	~CEquipCtrlNet(void) {  TRACE("CEquipCtrlNet dtor1 %d\n", GetMode()); Shutdown(); TRACE("CEquipCtrlNet dtor2 %d\n", GetMode()); }
	static unsigned char GetResponseVersion(_In_ const CEquipCtrlNet* client, _In_ const SEquipCtrlMsg::SHdr& hdr);
	template<typename T> static bool Send(_In_ CNetConnection<CEquipCtrlNet>* connection, _In_ const T* msg);
	virtual bool Send(_In_bytecount_(size) const void* msg, size_t size) { return CNetConnection<CEquipCtrlNet>::Send(msg, size); }
	void SendError(_In_ const SEquipCtrlMsg::SHdr& hdr, unsigned long code);

private:
	// Functions
	inline virtual size_t BodySize(_In_ const void* hdr) const;
	static void DownConvert(_Inout_ const SEquipCtrlMsg*& msg);
	static void DownConvert(_Inout_ const SVCPMsg*& msg);
	static void DownConvertValidateOccResponse(_In_ const SEquipCtrlMsg* msg, _Out_ SEquipCtrlMsg* & newMsg);
	virtual size_t HeaderSize(void) const { return sizeof(SEquipCtrlMsg::SHdr); }
	virtual void OnClose(int errorCode); // Callback
	virtual void OnConnect(void); // Callback
	virtual void OnMessage(_In_bytecount_(size) const void* msg, size_t size); // Callback
	static void UpConvert(_Inout_ const SEquipCtrlMsg*& msg);

	// Data
	SSmsMsg::VersionMap m_clientVersionMap;
	CSingleton<CEquipControl> m_equipControl;
	static SSmsMsg::VersionMap m_versionMap;
	static bool VERBOSE;
};


//////////////////////////////////////////////////////////////////////
//
// Send a message, downconverting the version if necessary
//
template<typename T> bool CEquipCtrlNet::Send(_In_ CNetConnection<CEquipCtrlNet>* connection, _In_ const T* msg)
{
	// Check response version
	SSmsMsg::SVersionKey key = { msg->hdr.msgType, msg->hdr.msgSubType };
	SSmsMsg::VersionMap::const_iterator entry = m_versionMap.find(key);
	bool converted = false;
	unsigned char myRespVersion = (entry != m_versionMap.end() ? entry->second.respVersion : 0);

	if(myRespVersion > msg->hdr.respVersion)
	{
		// Downconvert to requested version (making a local copy)
		DownConvert(msg);
		converted = true;
	}
	else if(myRespVersion < msg->hdr.respVersion)
	{
		// Make a local copy and set response version to my version
		size_t size = offsetof(T, body) + msg->hdr.bodySize;
		T* newMsg = new(size) T;
		memcpy(newMsg, msg, size);
		newMsg->hdr.respVersion = myRespVersion;
		msg = newMsg;
		converted = true;
	}

	bool status = CNetConnection<CEquipCtrlNet>::Send(connection, msg, offsetof(T, body) + msg->hdr.bodySize);

	if(converted)
	{
		// Delete the local copy
		delete msg;
	}

	return status;
}



