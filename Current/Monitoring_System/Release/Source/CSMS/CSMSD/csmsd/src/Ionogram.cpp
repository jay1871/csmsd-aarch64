/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"
#include "Ionogram.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CIonogram::CIonogram(void) :
	m_config(),
//	m_dfNet(),
	m_mutableConfig(m_config->GetMutableConfig())
//	m_dataInterval(0),
//	m_fftSpec(NULL),
//	m_goodGps(false),
//	m_hiBin(350),
//	m_lastLine(2),
//	m_lowBin(30),
//	m_numSamples(0),
//	m_rxLat(float(Units::TWO_PI / 4)),
//	m_rxLon(0),
//	m_rxTxDist(0),
//	m_sampleRate(0),
//	m_startFreq(0),
//	m_sweepRate(0),
//	m_txLat(float(Units::TWO_PI / 4)),
//	m_txLon(0)
{
	m_ionoData.timestamp.hour = 0;
	m_ionoData.timestamp.min = 0;
	m_ionoData.timestamp.sec = 0;
	m_ionoData.timestamp.yday = 0;
	m_ionoData.finishedIonogram.numPoints = 2;
	m_ionoData.finishedIonogram.ionData[0].freq = 0;
	m_ionoData.finishedIonogram.ionData[0].trueHeight = m_mutableConfig.hfConfig.defaultIonoHeight;
	m_ionoData.finishedIonogram.ionData[0].virtualHeight = m_mutableConfig.hfConfig.defaultIonoHeight;
	m_ionoData.finishedIonogram.ionData[1].freq = 30;
	m_ionoData.finishedIonogram.ionData[1].trueHeight = m_mutableConfig.hfConfig.defaultIonoHeight;
	m_ionoData.finishedIonogram.ionData[1].virtualHeight = m_mutableConfig.hfConfig.defaultIonoHeight;

#if 0
	if(strlen(m_mutableConfig.hfConfig.ionogramServer) > 0)
	{
		// Create client net connection for ionogram server
		try
		{
			m_dfNet.reset(new CDfNet(CA2T(m_mutableConfig.hfConfig.ionogramServer)));
		}
		catch(CFileException* error)
		{
			CString message;
			Utility::GetErrorMessage(error, message);
			CLog::Log(SMS_CONNECT_FAILED, LOG_CATEGORY_SERVICE, CA2T(m_mutableConfig.hfConfig.ionogramServer), message);
			error->Delete();
		}
	}
#endif
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Convert elevation to range (el in radians)
//
float CIonogram::Range(float el, Units::Frequency freq) const
{
	static const float MIN_EL = 5.0f * Units::D2R;

	if (m_config->AntennaCanSSL())
	{
		std::lock_guard<std::mutex> lock(m_critSect);

		if (el <= MIN_EL)
		{
			return 0;
		}

		if (m_ionoData.finishedIonogram.numPoints <= 1)
		{
			return 0;
		}

		if (m_ionoData.finishedIonogram.ionData[0].trueHeight == 0)
		{
			return 0;
		}

		int i;
		const float fcf2 = m_ionoData.finishedIonogram.ionData[m_ionoData.finishedIonogram.numPoints - 1].freq / 0.99f;
		const float cosEl = cos(el);
		float sinth = cosEl * (Units::EARTH_RADIUS_KM / (Units::EARTH_RADIUS_KM + m_ionoData.finishedIonogram.ionData[0].trueHeight));
		float fr1 = m_ionoData.finishedIonogram.ionData[0].freq / sqrt(1 - sinth * sinth);
		const float f = freq.Hz<float>() / 1e6f; // MHz

		if (f < fr1)
		{
			return 0.0;
		}

		float fr2 = fr1;

		for (i = 1; i < m_ionoData.finishedIonogram.numPoints; ++i)
		{
			sinth = cosEl * (Units::EARTH_RADIUS_KM / (Units::EARTH_RADIUS_KM + m_ionoData.finishedIonogram.ionData[i].trueHeight));
			fr2 = m_ionoData.finishedIonogram.ionData[i].freq / sqrt(1 - sinth * sinth);

			if (fr2 < fr1 || (f >= fr1 && f <= fr2))
			{
				break;
			}

			fr1 = fr2;
		}

		if (fr2 < fr1 || i == m_ionoData.finishedIonogram.numPoints)
		{
			return 0.0;
		}

		// Interpolate the height from ionogram
		const float factor = (f - fr1) / (fr2 - fr1);
		const float hi = m_ionoData.finishedIonogram.ionData[i - 1].trueHeight +
			factor * (m_ionoData.finishedIonogram.ionData[i].trueHeight - m_ionoData.finishedIonogram.ionData[i - 1].trueHeight);
		const float vf = m_ionoData.finishedIonogram.ionData[i - 1].freq +
			factor * (m_ionoData.finishedIonogram.ionData[i].freq - m_ionoData.finishedIonogram.ionData[i - 1].freq);
		float hv = m_ionoData.finishedIonogram.ionData[i - 1].virtualHeight +
			factor * (m_ionoData.finishedIonogram.ionData[i].virtualHeight - m_ionoData.finishedIonogram.ionData[i - 1].virtualHeight);
		const float c = (hv - hi) / Units::EARTH_RADIUS_KM;
		const float cf = (f * f - vf * vf) * (hi * c + 2 * (Units::EARTH_RADIUS_KM + hi) * c * c) / (fcf2 * fcf2);
		hv += cf;
		const float theta = asin(cosEl * Units::EARTH_RADIUS_KM / (Units::EARTH_RADIUS_KM + hv));

		return 2 * Units::EARTH_RADIUS_KM * (Units::HALF_PI - el - theta);
	}
	else
	{
		return 0;
	}
}



