/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/
#include "stdafx.h"

#include "51432017.h"
#include "Digitizer.h"
#include "Fft.h"
#include "RadioEquip.h"
#include "VCPCtrl.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CVCPCtrl::CVCPCtrl(void):
m_config()
{

}

//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CVCPCtrl::~CVCPCtrl(void)
{

}


//////////////////////////////////////////////////////////////////////
//
// Specific message handlers
//
ErrorCodes::EErrorCode CVCPCtrl::GetRegValues(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	try
	{
		CWeakSingleton<CDigitizer> digitizer;

		unsigned int start = VCPMsg->body.getRegValsData.start;
		unsigned int end = VCPMsg->body.getRegValsData.end;
		if (end < start)
		{
			return ErrorCodes::INVALIDVCPPARAMETER;
		}
		int count = std::max(int(end - start + 1), 0);
		auto msgSize = offsetof(SVCPMsg, body.getRegValsDataResp.regValsData) + count * sizeof(unsigned long);
		resp.reset(new(msgSize) SVCPMsg);

		resp->hdr = VCPMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::GET_REG_VALS_RESP;
		resp->hdr.bodySize = offsetof(SVCPMsg::SGetRegValsDataResp, regValsData) + count * sizeof(unsigned long);
		resp->body.getRegValsDataResp.start = VCPMsg->body.getRegValsData.start;
		resp->body.getRegValsDataResp.end = VCPMsg->body.getRegValsData.end;

		for (unsigned int i = start, j = 0; i <= end; ++i, ++j)
		{
			resp->body.getRegValsDataResp.regValsData[j] =  digitizer->Peek(static_cast<C3230Fdd::EReg>(i));
		}
	}
	catch(CWeakSingleton<CDigitizer>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}
	return ErrorCodes::SUCCESS;
}


ErrorCodes::EErrorCode CVCPCtrl::GetReceiverData(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	try
	{
		CWeakSingleton<CRadioEquip> radioEquip;

		resp->hdr = VCPMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::GET_RX_DATA_RESP;
		resp->hdr.bodySize = sizeof(SVCPMsg::SGetReceiverDataResp);
		auto& rxdata = resp->body.getReceiverDataResp;

		S2630Msg::SRxSettings rxsettings;
		radioEquip->ReadRxSettings(rxsettings);
		rxdata.rffreq = rxsettings.currentFrequency;
		rxdata.lo1freq = rxsettings.currentLo1Frequency;
		rxdata.lo2freq = rxsettings.currentLo2Frequency;
		rxdata.if1freq = rxsettings.currentIf1Frequency;
		rxdata.if2freq = rxsettings.currentIf2Frequency;
		rxdata.lolockstatus = rxsettings.currentLoLockStatus; // lo1 lock bit 0 and lock2 lock bit 1
		rxdata.psFreqLow = rxsettings.currentPsFreqLow;
		rxdata.psFreqHigh = rxsettings.currentPsFreqHigh;
		rxdata.antSetting = rxsettings.currentAntennaSetting;
		rxdata.attenuation = rxsettings.currentAttenuation;
		rxdata.bandwidth = rxsettings.currentBandWidth;
		rxdata.AttMode = rxsettings.currentMode;
		rxdata.FPIndex = rxsettings.currentFPI;
		rxdata.lnastatus = rxsettings.currentLnaStatus;
		rxdata.spectralsense = rxsettings.currentSpectralSense;
	}
	catch(CWeakSingleton<CRadioEquip>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}

	return ErrorCodes::SUCCESS;
}


ErrorCodes::EErrorCode CVCPCtrl::GetRxCapabilities(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	try
	{
		CWeakSingleton<CRadioEquip> radioEquip;
		std::vector<unsigned long> pslow, pshigh;
		radioEquip->GetfreqLowHighFromTable(pslow, pshigh);
		unsigned int tablecount = pshigh.size();

		auto msgSize = offsetof(SVCPMsg, body.getRxCapabilitiesResp.psfreq) + tablecount * sizeof(SVCPMsg::SGetRxCapabilitiesResp::SPsFreq);
		resp.reset(new(msgSize) SVCPMsg);
		resp->hdr = VCPMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::GET_RX_CAPABILITIES_RESP;
		resp->hdr.bodySize =  offsetof(SVCPMsg::SGetRxCapabilitiesResp, psfreq) + tablecount * sizeof(SVCPMsg::SGetRxCapabilitiesResp::SPsFreq);
		resp->body.getRxCapabilitiesResp.freqtableCount = tablecount;

		for (unsigned int i = 0; i <  tablecount; ++i)
		{
			if (pslow.size() > i)
				resp->body.getRxCapabilitiesResp.psfreq[i].low = pslow[i];
			if (pshigh.size() > i)
				resp->body.getRxCapabilitiesResp.psfreq[i].high = pshigh[i];
		}

		S2630Msg::SRxCapabilities rxcap;
		if (radioEquip->ReadRxCapabilities(rxcap))
		{
			resp->body.getRxCapabilitiesResp.firmwareDate = rxcap.firmwareDate;
			resp->body.getRxCapabilitiesResp.firmwareVersion = rxcap.firmwareVersion;
			resp->body.getRxCapabilitiesResp.flashSize = 0;
			resp->body.getRxCapabilitiesResp.flashVersion = rxcap.flashVersion;
			resp->body.getRxCapabilitiesResp.fpgaVersion = rxcap.fpgaVersion;
		}
		else
		{
			resp->body.getRxCapabilitiesResp.firmwareDate = 0;
			resp->body.getRxCapabilitiesResp.firmwareVersion = 0;
			resp->body.getRxCapabilitiesResp.flashSize = 0;
			resp->body.getRxCapabilitiesResp.flashVersion = 0;
			resp->body.getRxCapabilitiesResp.fpgaVersion = 0;
		}
	}
	catch(CWeakSingleton<CRadioEquip>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}
	return ErrorCodes::SUCCESS;
}


ErrorCodes::EErrorCode CVCPCtrl::GetRxMoc(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	try
	{
		CWeakSingleton<CRadioEquip> radioEquip;
		resp->hdr = VCPMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::GET_RX_MOC_RESP;
		resp->hdr.bodySize =  sizeof(SVCPMsg::SRxMocResp);
		auto& moc = resp->body.rxMocResp;
		unsigned char ant;
		auto rv = radioEquip->ReadMOC(moc.freqMHz, moc.fpi, moc.bw, ant);
		moc.status = (rv ? ErrorCodes::SUCCESS : ErrorCodes::HARDWAREDOWN);
		moc.rfInput = SVCPMsg::ERfInput(ant);
	}
	catch(CWeakSingleton<CRadioEquip>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}
	return ErrorCodes::SUCCESS;
}


ErrorCodes::EErrorCode CVCPCtrl::GetRxAtten(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	try
	{
		CWeakSingleton<CRadioEquip> radioEquip;
		resp->hdr = VCPMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::RX_ATTEN_RESP;
		resp->hdr.bodySize =  sizeof(SVCPMsg::SRxAttenResp);
		auto& body = resp->body.rxAttenResp;
		C2630::EGainMode setGainMode;
		bool rv = radioEquip->ReadAttenuation(body.atten, setGainMode, body.lna);
		body.status = (rv ? ErrorCodes::SUCCESS : ErrorCodes::HARDWAREDOWN);
		switch(setGainMode)
		{
			case C2630::EGainMode::NORMAL: body.gainMode = SVCPMsg::NORMAL; break;
			case C2630::EGainMode::RURAL: body.gainMode = SVCPMsg::RURAL; break;
			case C2630::EGainMode::URBAN: body.gainMode = SVCPMsg::URBAN; break;
			case C2630::EGainMode::CONGESTED: body.gainMode = SVCPMsg::CONGESTED; break;
			default: body.gainMode = SVCPMsg::NORMAL; break;
		}
	}
	catch(CWeakSingleton<CRadioEquip>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}
	return ErrorCodes::SUCCESS;
}


ErrorCodes::EErrorCode CVCPCtrl::GetRxCalgen(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	try
	{
		CWeakSingleton<CRadioEquip> radioEquip;
		resp->hdr = VCPMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::RX_CALGEN_RESP;
		resp->hdr.bodySize =  sizeof(SVCPMsg::SRxCalgenResp);
		auto& body = resp->body.rxCalgenResp;

		auto rv = radioEquip->ReadCalTone(body.on);
		body.status = (rv ? ErrorCodes::SUCCESS : ErrorCodes::RXCALGENERROR);
		if (rv)
		{
			rv = radioEquip->ReadCalToneMHz(body.freqMHz);
			if (!rv) body.status = ErrorCodes::RXCALGENFREQERROR;
		}
	}
	catch(CWeakSingleton<CRadioEquip>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}
	return ErrorCodes::SUCCESS;
}


ErrorCodes::EErrorCode CVCPCtrl::GetRxOcxo(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	try
	{
		CWeakSingleton<CRadioEquip> radioEquip;
		resp->hdr = VCPMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::GET_RX_OCXO_RESP;
		resp->hdr.bodySize =  sizeof(SVCPMsg::SGetRxOcxoResp);
		auto& body = resp->body.getRxOcxoResp;

		auto rv = radioEquip->ReadOcxo(body.loopStatus, body.dacCode, body.interval);
		body.status = (rv ? ErrorCodes::SUCCESS : ErrorCodes::RXOCXOERROR);
	}
	catch(CWeakSingleton<CRadioEquip>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}
	return ErrorCodes::SUCCESS;
}


ErrorCodes::EErrorCode CVCPCtrl::GetRxTemps(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	try
	{
		CWeakSingleton<CRadioEquip> radioEquip;
		resp->hdr = VCPMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::GET_RX_TEMPS_RESP;
		resp->hdr.bodySize =  sizeof(SVCPMsg::SGetRxTempsResp);
		auto& body = resp->body.getRxTempsResp;

		std::vector<float> temps;
		auto rv = radioEquip->ReadRxTemperatures(temps);
		body.status = (rv ? ErrorCodes::SUCCESS : ErrorCodes::RXTEMPSERROR);
		body.nios = (S2630Msg::NIOS < temps.size() ? temps[S2630Msg::NIOS] : std::numeric_limits<float>::quiet_NaN());
		body.sf2450 = (S2630Msg::SF2450 < temps.size() ? temps[S2630Msg::SF2450] : std::numeric_limits<float>::quiet_NaN());
		body.ocxo = (S2630Msg::OCXO < temps.size() ? temps[S2630Msg::OCXO] : std::numeric_limits<float>::quiet_NaN());
	}
	catch(CWeakSingleton<CRadioEquip>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}
	return ErrorCodes::SUCCESS;
}


ErrorCodes::EErrorCode CVCPCtrl::GetDigiTemps(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	try
	{
		CWeakSingleton<CDigitizer> digitizer;

		resp->hdr = VCPMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::GET_DIGI_TEMPS_RESP;
		resp->hdr.bodySize =  sizeof(SVCPMsg::SGetDigiTempsResp);
		auto& body = resp->body.getDigiTempsResp;

		std::vector<float> temps = digitizer->GetTemperatures();
		body.status = (temps.size() == 2 ? ErrorCodes::SUCCESS : ErrorCodes::DIGITEMPSERROR);
		body.ZYNQ_INTERNAL_TEMP = (temps.size() > 0 ? temps[0] : std::numeric_limits<float>::quiet_NaN());
		body.BOARD_TEMP = (temps.size() > 1 ? temps[1] : std::numeric_limits<float>::quiet_NaN());
	}
	catch(CWeakSingleton<CDigitizer>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}
	return ErrorCodes::SUCCESS;
}


ErrorCodes::EErrorCode CVCPCtrl::GetDigiVolts(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	try
	{
		CWeakSingleton<CDigitizer> digitizer;

		resp->hdr = VCPMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::GET_DIGI_VOLTS_RESP;
		resp->hdr.bodySize =  sizeof(SVCPMsg::SGetDigiVoltsResp);
		auto& body = resp->body.getDigiVoltsResp;

		std::vector<float> volts = digitizer->GetVoltages();
		body.status = (volts.size() == 21 ? ErrorCodes::SUCCESS : ErrorCodes::DIGIVOLTSERROR);
		body.ZYNQ_INTERNAL_VCCINT = (volts.size() > 0 ? volts[0] : std::numeric_limits<float>::quiet_NaN());
		body.ZYNQ_INTERNAL_VCCAUX = (volts.size() > 1 ? volts[1] : std::numeric_limits<float>::quiet_NaN());
		body.ZYNQ_INTERNAL_VCCBRAM = (volts.size() > 2 ? volts[2] : std::numeric_limits<float>::quiet_NaN());
		body.ZYNQ_INTERNAL_VCCPINT = (volts.size() > 3 ? volts[3] : std::numeric_limits<float>::quiet_NaN());
		body.ZYNQ_INTERNAL_VCCPAUX = (volts.size() > 4 ? volts[4] : std::numeric_limits<float>::quiet_NaN());
		body.ZYNQ_INTERNAL_VCCODDR = (volts.size() > 5 ? volts[5] : std::numeric_limits<float>::quiet_NaN());
		body.BOARD_5p3V_INPUT = (volts.size() > 6 ? volts[6] : std::numeric_limits<float>::quiet_NaN());
		body.DIGITAL_3p3V = (volts.size() > 7 ? volts[7] : std::numeric_limits<float>::quiet_NaN());
		body.DIGITAL_2p5V = (volts.size() > 8 ? volts[8] : std::numeric_limits<float>::quiet_NaN());
		body.MGT_1p0V = (volts.size() > 9 ? volts[9] : std::numeric_limits<float>::quiet_NaN());
		body.MGT_1p2V = (volts.size() > 10 ? volts[10] : std::numeric_limits<float>::quiet_NaN());
		body.MGT_1p8V = (volts.size() > 11 ? volts[11] : std::numeric_limits<float>::quiet_NaN());
		body.PS_DDR3_VTT = (volts.size() > 12 ? volts[12] : std::numeric_limits<float>::quiet_NaN());
		body.PL_DDR3_VTT = (volts.size() > 13 ? volts[13] : std::numeric_limits<float>::quiet_NaN());
		body.BOARD_TOTAL_CURR_5p3V = (volts.size() > 14 ? volts[14] : std::numeric_limits<float>::quiet_NaN());
		body.ADC_3p0V_ANALOG = (volts.size() > 15 ? volts[15] : std::numeric_limits<float>::quiet_NaN());
		body.ADC_1p8V_ANALOG = (volts.size() > 16 ? volts[16] : std::numeric_limits<float>::quiet_NaN());
		body.ADC_1p2V_ANALOG = (volts.size() > 17 ? volts[17] : std::numeric_limits<float>::quiet_NaN());
		body.CLOCK_3p3_ANALOG = (volts.size() > 18 ? volts[18] : std::numeric_limits<float>::quiet_NaN());
		body.CLOCK_3p3_DIGITAL = (volts.size() > 19 ? volts[19] : std::numeric_limits<float>::quiet_NaN());
		body.TEMP_REF_2p5V = (volts.size() > 20 ? volts[20] : std::numeric_limits<float>::quiet_NaN());
	}
	catch(CWeakSingleton<CDigitizer>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}
	return ErrorCodes::SUCCESS;
}


ErrorCodes::EErrorCode CVCPCtrl::GetAntennaSwitch(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	try
	{
		CWeakSingleton<CDigitizer> digitizer;

		resp->hdr = VCPMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::GET_ANTENNA_SWITCH_RESP;
		resp->hdr.bodySize =  sizeof(SVCPMsg::SGetAntennaSwitchResp);
		auto& body = resp->body.getAntennaSwitchResp;

		unsigned long controlWord = digitizer->GetAntennaControl();
		body.status = ErrorCodes::SUCCESS;
		body.isHf = (controlWord & C3230Fdd::CSequencerAntennaControl::HF_ANT_SELECT) != 0;
		if (body.isHf)
		{
			body.uSw.hf.calToAnt = (controlWord & C3230Fdd::CSequencerAntennaControlHF::CAL_TO_ANT) != 0;
			body.uSw.hf.calToRef = (controlWord & C3230Fdd::CSequencerAntennaControlHF::CAL_TO_REF) != 0;
			body.uSw.hf.calEnable = (controlWord & C3230Fdd::CSequencerAntennaControlHF::CAL_EN) != 0;
			body.uSw.hf.noiseGen = (controlWord & C3230Fdd::CSequencerAntennaControlHF::NOISE_GEN) != 0;
			body.uSw.hf.noiseSwitch = (controlWord & C3230Fdd::CSequencerAntennaControlHF::NOISE_SW) != 0;
			body.uSw.hf.refSource = (controlWord & C3230Fdd::CSequencerAntennaControlHF::REF_SOURCE) != 0;
			body.uSw.hf.lbBypass = (controlWord & C3230Fdd::CSequencerAntennaControlHF::LB_BYPASS) != 0;
			body.uSw.hf.refTerm = (controlWord & C3230Fdd::CSequencerAntennaControlHF::REF_TERM) != 0;
			body.uSw.hf.chanSelect = (controlWord & C3230Fdd::CSequencerAntennaControlHF::CHAN_SELECT) >> C3230Fdd::CSequencerAntennaControlHF::ANT_SHIFT;
		}
		else
		{
			body.uSw.vushf.calToAnt = (controlWord & C3230Fdd::CSequencerAntennaControl::CAL_TO_ANT) != 0;
			body.uSw.vushf.calToRef = (controlWord & C3230Fdd::CSequencerAntennaControl::CAL_TO_REF) != 0;
			body.uSw.vushf.cal0 = (controlWord & C3230Fdd::CSequencerAntennaControl::CAL0) != 0;
			body.uSw.vushf.cal1 = (controlWord & C3230Fdd::CSequencerAntennaControl::CAL1) != 0;
			body.uSw.vushf.calToneNoiseSource = (controlWord & C3230Fdd::CSequencerAntennaControl::CALTONE_NOISESOURCE) != 0;
			body.uSw.vushf.cBandVuhfElement = (controlWord & C3230Fdd::CSequencerAntennaControl::CBAND_VUHF_ELEMENT) != 0;
			body.uSw.vushf.cBandVuhfAmp = (controlWord & C3230Fdd::CSequencerAntennaControl::CBAND_VUHF_AMP) != 0;
			body.uSw.vushf.vertHoriz = (controlWord & C3230Fdd::CSequencerAntennaControl::VERT_HORIZ) != 0;
			body.uSw.vushf.defaultAnt = (controlWord & C3230Fdd::CSequencerAntennaControl::DEFAULT_ANTENNA) >> C3230Fdd::CSequencerAntennaControl::ANT_SHIFT;
		}
	}
	catch(CWeakSingleton<CDigitizer>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}
	return ErrorCodes::SUCCESS;
}


ErrorCodes::EErrorCode CVCPCtrl::SetAntennaSwitch(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	try
	{
		CWeakSingleton<CDigitizer> digitizer;
		CWeakSingleton<C51432017> antSwitch;

		if (VCPMsg->body.setAntennaSwitch.isHf)
		{
			antSwitch->C72362001::SetSwitch(VCPMsg->body.setAntennaSwitch.uSw.hf);
		}
		else
		{
			antSwitch->C7234201102::SetSwitch(VCPMsg->body.setAntennaSwitch.uSw.vushf);
		}
		resp->hdr = VCPMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::SET_ANTENNA_SWITCH_RESP;
		resp->hdr.bodySize =  sizeof(SVCPMsg::SSetAntennaSwitchResp);
		auto& body = resp->body.setAntennaSwitchResp;

		unsigned long controlWord = digitizer->GetAntennaControl();
		body.status = ErrorCodes::SUCCESS;
		body.isHf = (controlWord & C3230Fdd::CSequencerAntennaControl::HF_ANT_SELECT) != 0;
		if (body.isHf)
		{
			body.uSw.hf.calToAnt = (controlWord & C3230Fdd::CSequencerAntennaControlHF::CAL_TO_ANT) != 0;
			body.uSw.hf.calToRef = (controlWord & C3230Fdd::CSequencerAntennaControlHF::CAL_TO_REF) != 0;
			body.uSw.hf.calEnable = (controlWord & C3230Fdd::CSequencerAntennaControlHF::CAL_EN) != 0;
			body.uSw.hf.noiseGen = (controlWord & C3230Fdd::CSequencerAntennaControlHF::NOISE_GEN) != 0;
			body.uSw.hf.noiseSwitch = (controlWord & C3230Fdd::CSequencerAntennaControlHF::NOISE_SW) != 0;
			body.uSw.hf.refSource = (controlWord & C3230Fdd::CSequencerAntennaControlHF::REF_SOURCE) != 0;
			body.uSw.hf.lbBypass = (controlWord & C3230Fdd::CSequencerAntennaControlHF::LB_BYPASS) != 0;
			body.uSw.hf.refTerm = (controlWord & C3230Fdd::CSequencerAntennaControlHF::REF_TERM) != 0;
			body.uSw.hf.chanSelect = (controlWord & C3230Fdd::CSequencerAntennaControlHF::CHAN_SELECT) >> C3230Fdd::CSequencerAntennaControlHF::ANT_SHIFT;
		}
		else
		{
			body.uSw.vushf.calToAnt = (controlWord & C3230Fdd::CSequencerAntennaControl::CAL_TO_ANT) != 0;
			body.uSw.vushf.calToRef = (controlWord & C3230Fdd::CSequencerAntennaControl::CAL_TO_REF) != 0;
			body.uSw.vushf.cal0 = (controlWord & C3230Fdd::CSequencerAntennaControl::CAL0) != 0;
			body.uSw.vushf.cal1 = (controlWord & C3230Fdd::CSequencerAntennaControl::CAL1) != 0;
			body.uSw.vushf.calToneNoiseSource = (controlWord & C3230Fdd::CSequencerAntennaControl::CALTONE_NOISESOURCE) != 0;
			body.uSw.vushf.cBandVuhfElement = (controlWord & C3230Fdd::CSequencerAntennaControl::CBAND_VUHF_ELEMENT) != 0;
			body.uSw.vushf.cBandVuhfAmp = (controlWord & C3230Fdd::CSequencerAntennaControl::CBAND_VUHF_AMP) != 0;
			body.uSw.vushf.vertHoriz = (controlWord & C3230Fdd::CSequencerAntennaControl::VERT_HORIZ) != 0;
			body.uSw.vushf.defaultAnt = (controlWord & C3230Fdd::CSequencerAntennaControl::DEFAULT_ANTENNA) >> C3230Fdd::CSequencerAntennaControl::ANT_SHIFT;
		}
	}
	catch(CWeakSingleton<CDigitizer>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}
	catch(CWeakSingleton<C51432017>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}
	return ErrorCodes::SUCCESS;
}


ErrorCodes::EErrorCode CVCPCtrl::RxTune(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	if (m_vcpSpectrum != VCPMsg->body.rxTune)
	{
		m_vcpSpectrum.haveRxTune = true;
		m_vcpSpectrum.spect.resize(0);
		m_vcpSpectrum.rxTune = VCPMsg->body.rxTune;
	}
	auto frequency = Units::Frequency(VCPMsg->body.rxTune.freq);
	auto rxBw = VCPMsg->body.rxTune.bw;
	bool hf = VCPMsg->body.rxTune.rfInput == SVCPMsg::ERfInput::HFANT || VCPMsg->body.rxTune.rfInput == SVCPMsg::ERfInput::HFCAL;
	auto autoFpi = (VCPMsg->body.rxTune.fpi == 0);
	auto fpi = VCPMsg->body.rxTune.fpi;
	S2630Msg::EAntennaType rxInput;
	switch(VCPMsg->body.rxTune.rfInput)
	{
		case SVCPMsg::TERMINATE: rxInput = S2630Msg::TERMINATE; break;
		case SVCPMsg::VUSHFANT1: rxInput = S2630Msg::VUSHFANT1; break;
		case SVCPMsg::VUSHFANT2: rxInput = S2630Msg::VUSHFANT2; break;
		case SVCPMsg::HFANT: rxInput = S2630Msg::HFANT; break;
		case SVCPMsg::VUSHFCAL: rxInput = S2630Msg::VUSHFCAL; break;
		case SVCPMsg::HFCAL: rxInput = S2630Msg::HFCAL; break;
		default: rxInput = S2630Msg::VUSHFANT1; break;
	}

	try
	{
		CWeakSingleton<CRadioEquip> radioEquip;
		resp->hdr = VCPMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::RX_TUNE_RESP;
		resp->hdr.bodySize =  sizeof(SVCPMsg::SRxTuneResp);
		auto& body = resp->body.rxTuneResp;

		Units::Frequency radioFreq;
		Units::Frequency ifFreq;
		bool rv = radioEquip->TuneRaw(frequency, rxBw, hf, autoFpi, fpi, rxInput, body.inverted, body.direct, radioFreq, ifFreq);
		body.radioFreq = radioFreq.GetRaw();
		body.ifFreq = ifFreq.GetRaw();

		auto& moc = body.moc;
		moc.status = (rv ? ErrorCodes::SUCCESS : ErrorCodes::RXTUNEERROR);
		unsigned char ant;
		radioEquip->ReadMOC(moc.freqMHz, moc.fpi, moc.bw, ant);
		rxbwused = moc.bw;
		moc.rfInput = SVCPMsg::ERfInput(ant);
	}
	catch(CWeakSingleton<CRadioEquip>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}
	return ErrorCodes::SUCCESS;
}


ErrorCodes::EErrorCode CVCPCtrl::SetRxAtten(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	C2630::EGainMode gainMode;
	switch(VCPMsg->body.setRxAtten.gainMode)
	{
		case SVCPMsg::NORMAL: gainMode = C2630::EGainMode::NORMAL; break;
		case SVCPMsg::RURAL: gainMode = C2630::EGainMode::RURAL; break;
		case SVCPMsg::URBAN: gainMode = C2630::EGainMode::URBAN; break;
		case SVCPMsg::CONGESTED: gainMode = C2630::EGainMode::CONGESTED; break;
		default: gainMode = C2630::EGainMode::NORMAL; break;
	}

	try
	{
		CWeakSingleton<CRadioEquip> radioEquip;
		resp->hdr = VCPMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::RX_ATTEN_RESP;
		resp->hdr.bodySize =  sizeof(SVCPMsg::SRxAttenResp);
		auto& body = resp->body.rxAttenResp;
		C2630::EGainMode setGainMode;
		bool rv = radioEquip->SetAttenuation(gainMode, VCPMsg->body.setRxAtten.atten, body.atten, setGainMode, body.lna);
		body.status = (rv ? ErrorCodes::SUCCESS : ErrorCodes::RXATTENERROR);
		switch(setGainMode)
		{
			case C2630::EGainMode::NORMAL: body.gainMode = SVCPMsg::NORMAL; break;
			case C2630::EGainMode::RURAL: body.gainMode = SVCPMsg::RURAL; break;
			case C2630::EGainMode::URBAN: body.gainMode = SVCPMsg::URBAN; break;
			case C2630::EGainMode::CONGESTED: body.gainMode = SVCPMsg::CONGESTED; break;
			default: body.gainMode = SVCPMsg::NORMAL; break;
		}
	}
	catch(CWeakSingleton<CRadioEquip>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}
	return ErrorCodes::SUCCESS;
}


ErrorCodes::EErrorCode CVCPCtrl::SetRxCalgen(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	try
	{
		CWeakSingleton<CRadioEquip> radioEquip;
		resp->hdr = VCPMsg->hdr;
		resp->hdr.msgSubType = SVCPMsg::RX_CALGEN_RESP;
		resp->hdr.bodySize =  sizeof(SVCPMsg::SRxCalgenResp);
		auto& body = resp->body.rxCalgenResp;

		bool rv = radioEquip->SetCalTone(VCPMsg->body.setRxCalgen.on, body.on);
		body.status = (rv ? ErrorCodes::SUCCESS : ErrorCodes::RXCALGENERROR);
		if (rv)
		{
			rv = radioEquip->SetCalToneMHz(VCPMsg->body.setRxCalgen.freqMHz, body.freqMHz);
			if (!rv) body.status = ErrorCodes::RXCALGENFREQERROR;
		}
	}
	catch(CWeakSingleton<CRadioEquip>::NoInstance&)
	{
		return ErrorCodes::HARDWAREDOWN;
	}
	return ErrorCodes::SUCCESS;
}


ErrorCodes::EErrorCode CVCPCtrl::GetSpectrumData(const SVCPMsg* VCPMsg, std::unique_ptr<SVCPMsg>& resp)
{
	float avg = VCPMsg->body.getSpectrumData.average;
	if (avg < 0 || avg >= 1.0f)
	{
		return ErrorCodes::INVALIDVCPPARAMETER;
	}
	if (m_vcpSpectrum != VCPMsg->body.getSpectrumData)
	{
		m_vcpSpectrum.haveGetSpectrumData = true;
		m_vcpSpectrum.spect.resize(0);
		m_vcpSpectrum.getSpectrumData = VCPMsg->body.getSpectrumData;
	}

	// This always gets the full spectrum (80 MHz settings)
	CConfig::SProcParams procParams;
	/*if (rxbwused == 10)
	{
		procParams.blockSize = 0;	// not needed
		procParams.decimations.zifCICDecimation = 8;
		procParams.decimations.zifFIRDecimation = 3;
		procParams.decimations.upResample = 5;
		procParams.decimations.downResample = 6;
		procParams.decimations.ddcCICDecimation = 1;
		procParams.decimations.ddcFIRDecimation = 1;
		procParams.rxProcBw = Units::Frequency(10000000);
		procParams.sampleRate = Units::Frequency(C3230::ADC_CLOCK_RATE);
		procParams.sampleSize = 512;
		procParams.iqCic = 0;	// not needed

	}
	else
	{*/
		procParams.blockSize = 0;	// not needed
		procParams.decimations.zifCICDecimation = 1;
		procParams.decimations.zifFIRDecimation = 3;
		procParams.decimations.upResample = 5;
		procParams.decimations.downResample = 6;
		procParams.decimations.ddcCICDecimation = 1;
		procParams.decimations.ddcFIRDecimation = 1;
		procParams.rxProcBw = Units::Frequency(80000000);
		procParams.sampleRate = Units::Frequency(C3230::ADC_CLOCK_RATE);
		procParams.sampleSize = 512;
		procParams.iqCic = 0;	// not needed

	//}

	unsigned long numBins = 0;
	auto status = ErrorCodes::UNABLETOGETDATA;
	Ne10F32Vec watts;
	unsigned long sampleCount;

	try
	{
		CWeakSingleton<CRadioEquip> radioEquip;
		CWeakSingleton<CDigitizer> digitizer;

		C2630::S2630State state;
		radioEquip->GetState(state);

		// Collection - psd only
		digitizer->SetDecimation(procParams.decimations, 0);
		bool inverted = digitizer->AdjustInversion(state.direct, state.inverted);
		Units::Frequency finalIfFreq;
		if (state.direct)
		{
			finalIfFreq = Units::Frequency(state.frequency);
		}
		else
		{
			auto radioFreq = state.ifreq * 1000000.0;
			auto offset = (state.inverted ? radioFreq - state.frequency : state.frequency - radioFreq);
			finalIfFreq = Units::Frequency(radioEquip->C2630::m_configData.finalIfFreqMHz * 1000000.0 + offset);
		}
		digitizer->Tune(finalIfFreq, inverted, state.direct, false);

		auto temp = procParams.sampleSize * digitizer->GetDecimation() + 0.5;
		unsigned long adcCount = std::min(ULONG_MAX, static_cast<unsigned long>(temp));
		auto collectionTimeSec = temp / procParams.sampleRate.Hz<double>();	// sec

		if (!digitizer->m_sdma.WaitForBufferSpace(procParams.sampleSize, sampleCount) ||
			!digitizer->m_sdma.MarkSamplesInUse(sampleCount, procParams.sampleSize))
		{
			// sampleSize > dma buffer - task cannot be done
			return ErrorCodes::HARDWAREDOWN;
		}

		std::vector<unsigned long> pattern;
		digitizer->CollectSamples(sampleCount, procParams.sampleSize, adcCount, 15, false, true, false,
			true, collectionTimeSec, 0, Units::Timestamp(), C3230::EDfMode::NODF, pattern);

		unsigned int counts[4];
		unsigned short thresh[4];
		digitizer->GetThresholdCounters(counts, thresh);	// Get and ignore

		// Processing

		size_t rfGainTableIdx;
		long ifGainTableIdx;
		float gainAdjTemp;
		float rxGain = radioEquip->GetRxGainRatio(rfGainTableIdx, ifGainTableIdx, gainAdjTemp);
		float gainAdj = digitizer->GetAdcFullScaleWatts() / (rxGain * digitizer->GetGainRatio(state.direct));

		uncached_ptr found;
		auto foundData = digitizer->m_sdma.FindSamples(sampleCount, procParams.sampleSize, found);
		if (foundData && found)
		{
			numBins = procParams.sampleSize;
			Ne10F32Vec adjPower;

			// This is essentially GetCorrections():
			//if (VCPMsg->body.getSpectrumData.gainEqualization)
			if (m_config->HasGainEqualization())
			{
				gainAdj *= (rxGain / gainAdjTemp);
				adjPower.resize(numBins);
				radioEquip->GetCorrection(Units::Frequency(state.frequency), Units::Frequency(state.ifreq * 1000000ULL), Units::Frequency(state.bw * 1000000), rfGainTableIdx,
					ifGainTableIdx, procParams.GetBinSize(), state.atten, state.gainMode, adjPower);
			}
			else
			{
				Units::Frequency midFrequency = Units::Frequency(state.frequency);
				auto adj = radioEquip->GetCorrection(midFrequency, Units::Frequency(state.ifreq * 1000000ULL), rfGainTableIdx,
					ifGainTableIdx, state.atten, state.gainMode);
				if (adj < 0.00000001)
					adj = 1;

				float midCorrection = rxGain / (gainAdjTemp * adj);
				if (fabsf(midCorrection - 1.0f) > 1.e-6)
				{
					gainAdj *= midCorrection;
				}
			}

			CFft::GetPowerSpectrumWatts2(numBins, procParams.sampleSize, found, gainAdj, watts);
			//if (VCPMsg->body.getSpectrumData.gainEqualization)
			if (m_config->HasGainEqualization())
			{
				watts /= adjPower;
				// Filter out divide by zero and set watts to zero vs infinity
				for (size_t i = 0; i < watts.size(); ++i)
				{
					if (adjPower[i] <= 0.f) watts[i] = 0.f;
				}
			}
			status = ErrorCodes::SUCCESS;
		}
		else
			printf("data not found\n");

		digitizer->m_sdma.DoneWithSamples(sampleCount);

	}
	catch(CWeakSingleton<CRadioEquip>::NoInstance&)
	{
		throw ErrorCodes::HARDWAREDOWN;
	}
	catch(CWeakSingleton<CDigitizer>::NoInstance&)
	{
		throw ErrorCodes::HARDWAREDOWN;
	}
	// At this point, watts contains the current spectrum data - check averaging

	if (m_vcpSpectrum.spect.size() != watts.size() || avg == 0.f)
	{
		m_vcpSpectrum.spect = watts;
	}
	else
	{
		m_vcpSpectrum.spect -= watts;
		m_vcpSpectrum.spect *= avg;
		m_vcpSpectrum.spect += watts;
	}

	if (std::isinf(m_vcpSpectrum.spect[1]))
		printf("m_vcpSpectrum.spect is infinite!");
	auto msgSize = offsetof(SVCPMsg, body.getSpectrumDataResp.binData) + numBins * sizeof(unsigned long);
	resp.reset(new(msgSize) SVCPMsg);

	resp->hdr = VCPMsg->hdr;
	resp->hdr.msgSubType = SVCPMsg::GET_SPECTRUM_DATA_RESP;
	resp->hdr.bodySize = offsetof(SVCPMsg::SGetSpectrumDataResp, binData) + numBins * sizeof(float);

	auto& body = resp->body.getSpectrumDataResp;
	body.status = status;
	body.binSize = procParams.GetBinSize().GetRaw();
	body.numBins = numBins;
	if (numBins > 0)
	{
		Ne10F32Vec dbm(CFft::WattsToDbm(m_vcpSpectrum.spect));
		// Remove infinities - not using NE10 function since destination may not be properly aligned.
		for (size_t i = 0; i < numBins; ++i)
		{
			if (std::isinf(dbm[i]))
				body.binData[i] = std::min(dbm[i], -150.f);
			else
				body.binData[i] = std::max(dbm[i], -200.f);
		}
	}
	else
		printf("num bins = 0\n");


	return ErrorCodes::SUCCESS;
}




