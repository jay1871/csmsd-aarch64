/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2014-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"
#include <sys/utsname.h>
#include <netpacket/packet.h>
#include "2630.h"
#include "7234201102.h"
#include "Antenna.h"
#include "Config.h"
#include "Log.h"
#include "ProcessXML.h"
#include "Utility.h"

// Statics
const Units::Frequency CConfig::C_20_MHZ(20000000); //20 MHz in Units::Frequency.
const Units::Frequency CConfig::C_40_MHZ(40000000); //40 MHz in Units::Frequency.
const Units::Frequency CConfig::LPA_LOW_FREQ(30000000); //30 MHz lowest freq of LPA antenna.
const Units::Frequency CConfig::LPA_HIGH_FREQ(6000000000); //6 GHz highest freq of LPA antenna.
const Units::Frequency CConfig::HORN_HIGH_FREQ(40000000000); //40 GHz highest freq of Horn 2 antenna.
const Units::Frequency CConfig::HORIZONTAL_647_MAX_FREQ(3000000000); //3 GHz is the upper limit freq of 647 Horizontal monitor output.
const Units::Frequency CConfig::HORIZONTAL_647_MIN_FREQ(30000000); //30 MHz is the lower limit freq of 647 Horizontal monitor output.
const Units::Frequency CConfig::N9010A_526_MAX_FREQ(26500000000); //26.5 GHz is the upper limit freq of Keysight N9010A-526.
const Units::Frequency CConfig::N9010A_532_MAX_FREQ(32000000000); //32 GHz is the upper limit freq of Keysight N9010A-532.
const Units::Frequency CConfig::N9010A_544_MAX_FREQ(44000000000); //44 GHz is the upper limit freq of Keysight N9010A-544.
const Units::Frequency CConfig::VERTICAL_647_MAX_FREQ(6000000000); //6 GHz is the upper limit freq of 647 Horizontal monitor output.
const Units::Frequency CConfig::VERTICAL_647_MIN_FREQ(30000000); //30 MHz is the lower limit freq of 647 Horizontal monitor output.

std::string CConfig::s_configFileName = "csmsConfig.xml";
Units::Frequency CConfig::s_shfIfFreq(322500000); //322.5 MHz is set as default IF frequency.
Units::FreqPair CConfig::s_shfExtensionHorizRfFrequencyLimits(0,0);
Units::FreqPair CConfig::s_shfExtensionVertRfFrequencyLimits(0,0);
std::string CConfig::s_shfExtIPAddr = _T("10.16.101.244");	///@todo: Must be set to 192.168.255.111
LPCTSTR CConfig::s_shfExtIPPort = _T("5025");

const CConfig::SSmsMsgAnt2AntType CConfig::SMSMSG_ANT_MAP[] =
{
        {SSmsMsg::ANT1_LPA, CAntenna::EAntennaType::CUSTOM_LPA_ANT,
        		std::shared_ptr<CAntenna>(CAntenna::Create(CAntenna::EAntennaType::CUSTOM_LPA_ANT))},
        {SSmsMsg::ANT1_647V_MON, CAntenna::EAntennaType::USHF_647D_VERT_MON_SWTCH,
        		std::shared_ptr<CAntenna>(CAntenna::Create(CAntenna::EAntennaType::USHF_647D_VERT_MON_SWTCH))},
        {SSmsMsg::ANT1_647H_MON, CAntenna::EAntennaType::USHF_647D_HOR_MON_SWTCH,
        		std::shared_ptr<CAntenna>(CAntenna::Create(CAntenna::EAntennaType::USHF_647D_HOR_MON_SWTCH))},
};
const size_t CConfig::SMSMSG_ANT_MAP_CNT = sizeof(SMSMSG_ANT_MAP) / sizeof(SMSMSG_ANT_MAP[0]);

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CConfig::CConfig(void) :
//	m_declination(0),
	m_dualAux1(false),
	m_mediaReadWrite(false),
	m_hfMetricVLowFreqUnits(SSmsMsg::NO_VERTICAL_LOW_FREQ),
	m_hfMetricVHighFreqUnits(SSmsMsg::NO_VERTICAL_HIGH_FREQ),
	m_hfDfVLowFreqUnits(SSmsMsg::NO_VERTICAL_LOW_FREQ),
	m_hfDfVHighFreqUnits(SSmsMsg::NO_VERTICAL_HIGH_FREQ),
	m_shfExtLicenseValid(false),
	m_sysHasHorAnt(false),
	m_vushfMetricHLowFreq(SSmsMsg::NO_HORIZON_LOW_FREQ),
	m_vushfMetricHHighFreq(SSmsMsg::NO_HORIZON_HIGH_FREQ),
	m_vushfMetricVLowFreq(SSmsMsg::NO_VERTICAL_LOW_FREQ),
	m_vushfMetricVHighFreq(SSmsMsg::NO_VERTICAL_HIGH_FREQ),
	m_vushfDfHLowFreq(SSmsMsg::NO_HORIZON_LOW_FREQ),
	m_vushfDfHHighFreq(SSmsMsg::NO_HORIZON_HIGH_FREQ),
	m_vushfDfVLowFreq(SSmsMsg::NO_VERTICAL_LOW_FREQ),
	m_vushfDfVHighFreq(SSmsMsg::NO_VERTICAL_HIGH_FREQ),
	m_minFreq(SSmsMsg::NO_VERTICAL_LOW_FREQ),
	m_maxFreq(SSmsMsg::NO_VERTICAL_HIGH_FREQ),
	m_ddrtype(false),
	m_fastseqenabled(true)
{

	//Set antenna ok flag to true.
	for(size_t i = 0; i < C_NUM_CONNECTED_VUHF_RF; ++i)
	{
		m_antOk[i] = true;
	}

	char buffer[PATH_MAX + 1];
	auto len = readlink("/proc/self/exe", buffer, sizeof(buffer) - 1);

	if(len != -1)
	{
		printf("My exeName: %s\n", buffer);
		m_exeName = std::string(buffer, len);
		struct stat statbuf;
		if (stat(m_exeName.c_str(), &statbuf) != -1)
		{
			printf("exe ctime = %s atime = %s mtime = %s\n",
				Utility::TimespecAsString(statbuf.st_ctim).c_str(),
				Utility::TimespecAsString(statbuf.st_atim).c_str(),
				Utility::TimespecAsString(statbuf.st_mtim).c_str());
			m_exeTime = Utility::TimespecAsString(statbuf.st_mtim);
		}
	}

	// Get a list of the current mounts and extract the info for /media
    auto fp = fopen("/proc/self/mountinfo", "r");
    if (fp != nullptr)
    {
    	char buffer[1024];
    	while (fgets(buffer, 1024, fp))
    	{
    		char* p;
    		if ((p = strstr(buffer, "/media")) != nullptr)
    		{
    			// Find the last space in the line
    			if ((p = strrchr(p, ' ')) != nullptr)
    			{
    				if (strncmp(p + 1, "ro", 2) == 0)
    				{
    					m_mediaReadWrite = false;
    				}
    				else if (strncmp(p + 1, "rw", 2) == 0)
    				{
    					m_mediaReadWrite = true;
    				}
    			}
    		}
    	}
    	fclose(fp);
    }
    TRACE("/media is mounted %s\n", m_mediaReadWrite ? "readwrite" : "readonly");

	m_filePath = TCIPaths::configDir;	// "/media/tci/csms/etc/";
	LoadProcessorNodes();

	///@note: LoadSystemType must be called prior to LoadAntennaParams.
	LoadSystemType();
	LoadProcParams();
	LoadTdoaParams();
	LoadMutableConfig(); //LoadMutableConfig must be called before LoadAntennaParams.
	LoadAntennaParams(); //Read the note above on the call order.
	LoadDfParams();
	LoadAvdParams();
	LoadGainModes();
	LoadRestrictedFreqParams();

	s_shfExtIPAddr = m_mutableConfig.vushfConfig.shfExt.shfExtIP;

	if(IsProcessorDfSlave())
	{
		//Set Fixed Configuration for Slave node.
		SetSlaveNodeFixedConfig();
	}

	// Get some system and version properties
	char hostname[128];
	if (gethostname(hostname, sizeof(hostname)) == 0)
	{
		printf("My hostname: %s\n", hostname);
		m_serverName = hostname;
	}

	// Read some linux system information
	utsname utsbuf;
	if (uname(&utsbuf) == 0)
	{
		m_sysinfo.sysname = utsbuf.sysname;
		m_sysinfo.release = utsbuf.release;
		m_sysinfo.version = utsbuf.version;
		m_sysinfo.machine = utsbuf.machine;
	}
	printf("sysinfo: %s %s %s %s\n", m_sysinfo.sysname.c_str(), m_sysinfo.release.c_str(), m_sysinfo.version.c_str(), m_sysinfo.machine.c_str());

#ifdef CSMS_2016
	std::string etcFiles[2] = { "/etc/product", "/etc/productversion" };
#else
	std::string etcFiles[2] = { "/etc/product", "/etc/version" };
#endif
	std::string* r[2] = { &m_sysinfo.product, &m_sysinfo.fwversion };

	for (size_t i = 0; i < 2; ++i)
	{
		auto fp = fopen(etcFiles[i].c_str(), "r");
		if (fp != nullptr)
		{
			char buffer[64];
			auto& s = *(r[i]);
			s = (fgets(buffer, 64, fp) ? buffer : "Unknown");
			Utility::Trim(s);
			fclose(fp);
		}
	}
	GetIpAddresses(m_ipAddress, m_macAddress);
	ReadEeprom(m_eeprom);

	// Extract the version info	// "1.00.16 Thu, 06 Oct 2016 14:44:00 -0700"
	int vsn[3];
	if (sscanf(m_sysinfo.fwversion.c_str(), "%x.%x.%x", &vsn[0], &vsn[1], &vsn[2]) == 3)
	{
		unsigned long rev = ((vsn[0] & 0xff) << 24) + ((vsn[1] & 0xff) << 16) + (vsn[2] & 0xffff);
		char s[21];
		snprintf(s, 21, "%08lx vs %08lx", MIN_CSMS_PRODUCT_VERSION, rev);
		std::string versinfo("Csms product version min vs actual ");
		versinfo += s;
		CLog::Log(CLog::INFORMATION, "%s", versinfo.c_str());
		if (rev < MIN_CSMS_PRODUCT_VERSION)
		{
			char s[21];
			snprintf(s, 21, "%08lx vs %08lx", MIN_CSMS_PRODUCT_VERSION, rev);
			std::string err("Csms product version must be at least ");
			err += s;
			CLog::Log(CLog::ERRORS, "%s", err.c_str());
			throw std::runtime_error(err);
		}

		if (rev < MIN_CSMS_FASTSEQ_VERSION)
		{
			m_fastseqenabled = false;
			char s[21];
			snprintf(s, 21, "%08lx vs %08lx", MIN_CSMS_FASTSEQ_VERSION, rev);
			printf("fast sequencer mode deactivated- min csms version must be at least %s", s);
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Calculate cable loss
//
float CConfig::CalcCableLoss(ECableType type, Units::Frequency freq, float length) const
{
	auto it = m_cableCoeffs.find(type);
	if (it == m_cableCoeffs.end())
		return 0;

	return length * (it->second.k1 * sqrt(freq.Hz<float>()) * 1e-5f +it->second.k2 * freq.Hz<float>() * 1e-8f);
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Verify that the antenna value specified is valid for the RF port.
///</summary>
///
///@param[in] rfPort: RF port id.
///
///@param[in] antType: Antenna type to be checked.
///
///@retval: true: The specified antenna is compatible with the RF port.
///	  		false: he specified antenna is not compatible with the RF port.
///
bool CConfig::AntValid4RfPort(ERfPort rfPort, CAntenna::EAntennaType antType) const
{
	bool retVal = false;

	if(rfPort == ERfPort::C_RF1)
	{
		if(antType != CAntenna::EAntennaType::HF_632 &&
				antType != CAntenna::EAntennaType::HF_7235_COMPASS &&
				antType != CAntenna::EAntennaType::HF_7235 &&
				antType != CAntenna::EAntennaType::HF_620 &&
				antType != CAntenna::EAntennaType::HF_7031 &&
				antType != CAntenna::EAntennaType::VUHF_AUX1 &&
				antType != CAntenna::EAntennaType::USHF_647D_HOR_MON_SWTCH &&
				antType != CAntenna::EAntennaType::USHF_647D_VERT_MON_SWTCH &&
				antType != CAntenna::EAntennaType::USHF_DUAL_AUX1)
		{
			retVal = true;
		}
	}
	else if(rfPort == ERfPort::C_RF2)
	{
		if(antType == CAntenna::EAntennaType::NO_ANTENNA ||
				antType == CAntenna::EAntennaType::VUHF_640_DSC ||
				antType == CAntenna::EAntennaType::SHF_640_2 ||
				antType == CAntenna::EAntennaType::VUHF_AUX1 ||
				antType == CAntenna::EAntennaType::CUSTOM_ANT ||
				antType == CAntenna::EAntennaType::CUSTOM_LPA_ANT ||
				antType == CAntenna::EAntennaType::USHF_DUAL_AUX1)
		{
			retVal = true;
		}
	}

	return(retVal);
}

//////////////////////////////////////////////////////////////////////
//
// Calculate magnetic declination
//
// Taken from the World Magnetic Model at http://www.ngdc.noaa.gov/geomag/WMM/soft.shtml
//
float CConfig::CalcDeclination(float lat, float lon, float alt, timespec ts) const
{
	tm tmbuffer;
	gmtime_r(&ts.tv_sec, &tmbuffer);
	float time = 1900.f + tmbuffer.tm_year + tmbuffer.tm_yday / 365.25f;
	printf("CalcDeclination at: %f\n", time);

	alt /= 1000; // Convert to km
	unsigned n, m, j;
	const int MAXORD = 13;
	float tc[MAXORD][MAXORD], dp[MAXORD][MAXORD], p[MAXORD][MAXORD],
		sp[MAXORD], cp[MAXORD], fn[MAXORD], fm[MAXORD], pp[MAXORD], k[MAXORD][MAXORD],
		flnmj, dt, rlon, rlat, srlon, srlat, crlon, crlat, srlat2,
		crlat2, q, q1, q2, ct, st, r2, r, d, ca, sa, aor, ar, br, bt, bp, bpp,
		par, temp1, temp2, parp, bx, by/*, bz*/;

	// Initialize constants
	sp[0] = 0;
	cp[0] = p[0][0] = pp[0] = 1;
	dp[0][0] = 0;
	fm[0] = fn[0] = 0;
	const float A = 6378.137f;
	const float B = 6356.7523142f;
	const float RE = 6371.2f;
	const float A2 = A * A;
	const float B2 = B * B;
	const float C2 = A2 - B2;
	const float A4 = A2 * A2;
	const float B4 = B2 * B2;
	const float C4 = A4 - B4;
	float epoch = 2005.0f;

	// Read coefficients
	float c[MAXORD][MAXORD] = { 0 };
	float cd[MAXORD][MAXORD] = { 0 };

	std::ifstream wmmCof(m_filePath + "WMM.COF");
	if (!wmmCof)
	{
		throw std::system_error(errno, std::generic_category(), "WMM.COF");
	}
	std::string line;

	float gnm, hnm, dgnm, dhnm;

	if (!std::getline(wmmCof, line))
	{
		// No header line
		throw std::system_error(errno, std::generic_category(), "WMM.COF has no header line");
	}

	if (sscanf(line.c_str(), "%f", &epoch) != 1)
	{
		throw std::system_error(errno, std::generic_category(), "WMM.COF has invalid epoch");
	}
	while (!std::getline(wmmCof, line).eof())
	{
		if (line.substr(0, 4) == "9999")
		{
			break;
		}

		if (sscanf(line.c_str(), "%u %u %f %f %f %f", &n, &m, &gnm, &hnm, &dgnm, &dhnm) != 6)
		{
			throw std::system_error(errno, std::generic_category(), "WMM.COF has bad format");
		}

		assert(m < MAXORD && n < MAXORD && m <= n);

		if (m >= MAXORD || n >= MAXORD || m > n)
		{
			throw std::system_error(errno, std::generic_category(), "WMM.COF has bad format");
		}

		c[m][n] = gnm;
		cd[m][n] = dgnm;

		if(m != 0)
		{
			c[n][m - 1] = hnm;
			cd[n][m - 1] = dhnm;
		}
	}

	// Convert Schmidt normalized gauss cofficients to unnormalized
	for(n = 1; n < MAXORD; ++n)
	{
		p[0][n] = p[0][n - 1] * float(2 * n - 1) / n;
		j = 2;

		for(m = 0; m < n + 1; ++m)
		{
			k[m][n] = float((n - 1) * (n - 1) - m * m) / ((2 * n - 1) * (2 * n - 3));

			if(m > 0)
			{
				flnmj = float((n - m + 1) * j) / (n + m);
				p[m][n] = p[m - 1][n] * sqrt(flnmj);
				j = 1;
				c[n][m - 1] = p[m][n] * c[n][m - 1];
				cd[n][m - 1] = p[m][n] * cd[n][m - 1];
			}

			c[m][n] = p[m][n] * c[m][n];
			cd[m][n] = p[m][n] * cd[m][n];
		}

		fn[n] = float(n + 1);
		fm[n] = float(n);
	}

	k[1][1] = 0;
	dt = time - epoch;
	rlon = lon * Units::D2R;
	rlat = lat * Units::D2R;
	srlon = sin(rlon);
	srlat = sin(rlat);
	crlon = cos(rlon);
	crlat = cos(rlat);
	srlat2 = srlat * srlat;
	crlat2 = crlat * crlat;
	sp[1] = srlon;
	cp[1] = crlon;

	// Convert from geodetic coords to spherical coords
	q = sqrt(A2 - C2 * srlat2);
	q1 = alt * q;
	q2 = ((q1 + A2) / (q1 + B2)) * ((q1 + A2) / (q1 + B2));
	ct = srlat / sqrt(q2 * crlat2 + srlat2);
	st = sqrt(1 - (ct * ct));
	r2 = (alt * alt) + 2 * q1 + (A4 - C4 * srlat2) / (q * q);
	r = sqrt(r2);
	d = sqrt(A2 * crlat2 + B2 * srlat2);
	ca = (alt + d) / r;
	sa = C2 * crlat * srlat / (r * d);

	for(m = 2; m < MAXORD; ++m)
	{
		sp[m] = sp[1] * cp[m - 1] + cp[1] * sp[m - 1];
		cp[m] = cp[1] * cp[m - 1] - sp[1] * sp[m - 1];
	}

	aor = RE / r;
	ar = aor * aor;
	br = bt = bp = bpp = 0;

	for(n = 1; n < MAXORD; ++n)
	{
		ar = ar * aor;

		for(m = 0; m < n+ 1; ++m)
		{
			// Compute unnormalized associated Legendre polynomials
			//  and derivatives via recursion relations
			if(n == m)
			{
				p[m][n] = st * p[m - 1][n - 1];
				dp[m][n] = st * dp[m - 1][n - 1] + ct * p[m - 1][n - 1];
			}
			else if(n == 1 && m == 0)
			{
				p[m][n] = ct * p[m][n - 1];
				dp[m][n] = ct * dp[m][n - 1] - st * p[m][n - 1];
			}
			else if(n > 1 && n != m)
			{
				if(m > n - 2)
				{
					p[m][n - 2] = 0;
					dp[m][n - 2] = 0;
				}

				p[m][n] = ct * p[m][n - 1] - k[m][n] * p[m][n - 2];
				dp[m][n] = ct * dp[m][n - 1] - st * p[m][n - 1] - k[m][n] * dp[m][n - 2];
			}

			// Time adjust the Gauss coefficients
			tc[m][n] = c[m][n] + dt * cd[m][n];

			if(m != 0)
			{
				tc[n][m - 1] = c[n][m - 1] + dt * cd[n][m - 1];
			}

			// Accumulate terms of the spherical harmonic expansions
			par = ar * p[m][n];

			if(m == 0)
			{
				temp1 = tc[m][n] * cp[m];
				temp2 = tc[m][n] * sp[m];
			}
			else
			{
				temp1 = tc[m][n] * cp[m] + tc[n][m - 1] * sp[m];
				temp2 = tc[m][n] * sp[m] - tc[n][m - 1] * cp[m];
			}

			bt = bt - ar * temp1 * dp[m][n];
			bp += fm[m] * temp2 * par;
			br += fn[n] * temp1 * par;

			// Special case:  North/South geographic poles
			if(st == 0 && m == 1)
			{
				if(n == 1)
				{
					pp[n] = pp[n - 1];
				}
				else
				{
					pp[n] = ct * pp[n - 1] - k[m][n] * pp[n - 2];
				}

				parp = ar * pp[n];
				bpp += fm[m] * temp2 * parp;
			}
		}
	}

	if(st == 0)
	{
		bp = bpp;
	}
	else
	{
		bp /= st;
	}

	// Rotate magnetic vector components from spherical to geodetic coordinates
	bx = -bt * ca - br * sa;
	by = bp;
//	bz = bt * sa - br * ca;

	// Compute declination
	return atan2(by, bx) * Units::R2D;
}


//////////////////////////////////////////////////////////////////////
//
// Check if antenna can do SSL
//
bool CConfig::AntennaCanSSL(void) const
{
	return CAntenna::CanSSL(m_mutableConfig.hfConfig.antCable.antenna.get());
}


//////////////////////////////////////////////////////////////////////
//
// Convert the demod mode from msg value to internal value
//
bool CConfig::ConvertDemod(SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode detMode, C3230::EDemodMode& mode)
{
	switch(detMode)
	{
	case SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::OFF:
		mode = C3230::EDemodMode::OFF;
		break;

	case SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::AM:
		mode = C3230::EDemodMode::AM;
		break;

	case SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::CW:
		mode = C3230::EDemodMode::CW;
		break;

	case SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::FM:
		mode = C3230::EDemodMode::FM;
		break;

	case SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::USB:
		mode = C3230::EDemodMode::USB;
		break;

	case SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::LSB:
		mode = C3230::EDemodMode::LSB;
		break;

	case SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::EXTERNAL:
		mode = C3230::EDemodMode::IQ;
		break;
#ifdef CSMS_2016
	case SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::WIDE_IQ:
		mode = C3230::EDemodMode::WIDE_IQ;
		break;
#endif
	default:
		mode = C3230::EDemodMode::OFF;
		return false;
	}

	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Convert the demod mode from internal value to msg value
//
bool CConfig::ConvertDemod(C3230::EDemodMode mode, SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode& detMode)
{
	switch(mode)
	{
	case C3230::EDemodMode::OFF:
		detMode = SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::OFF;
		break;

	case C3230::EDemodMode::AM:
		detMode = SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::AM;
		break;

	case C3230::EDemodMode::CW:
		detMode = SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::CW;
		break;

	case C3230::EDemodMode::FM:
		detMode = SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::FM;
		break;

	case C3230::EDemodMode::USB:
		detMode = SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::USB;
		break;

	case C3230::EDemodMode::LSB:
		detMode = SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::LSB;
		break;

	case C3230::EDemodMode::IQ:
		detMode = SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::EXTERNAL;
		break;

#ifdef CSMS_2016
	case C3230::EDemodMode::WIDE_IQ:
		detMode = SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::WIDE_IQ;
		break;
#endif

	default:
		detMode = SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode::OFF;
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////
/// Default SHF Extension parameters.
///
///@param[out] contains the default value for SHF extension.
///
void CConfig::DefaultShfExt(SMutableConfig::SShfExt & shfExt)
{
  	shfExt.shfExtType = EShfExt::NO_SHF_EXT; ///N9010A_544_B25 TODO: Should set to NO_SHF_EXT
  	shfExt.shfExtCableType = ECableType::LMR_400;
  	shfExt.shfExtCableLength = 5.0f; //Set to 5 meter.
  	shfExt.shfExtIP = _T("10.16.101.244"); ///@TODO: Must be set to 192.168.255.111
  	return;
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Return the maximum frequency supported by the specified analyzer.
///</summary>
///
///@param[in] "analyzer"  has the analyzer type.
///
///@retval Maximum frequency supported by the specified analyzer.
///
Units::Frequency CConfig::GetAnalyzerMaxFreq(EShfExt analyzer) const
{
	Units::Frequency maxFreq(0);

	switch(analyzer)
	{
	  case EShfExt::N9010A_544_B25:
		//Flow through
	  case EShfExt::N9010A_544_B40:
		maxFreq = N9010A_544_MAX_FREQ;
	  	break;

	  case EShfExt::N9010A_526_B40:
		maxFreq = N9010A_526_MAX_FREQ;
	  	break;

	  case EShfExt::N9010A_532_B40:
		maxFreq = N9010A_532_MAX_FREQ;
	  	break;

	  default:
		break;

	}

	return(maxFreq);
}

//////////////////////////////////////////////////////////////////////
//
// Get antenna polarization
//
CConfig::EPolarization CConfig::GetAntPolarization(SEquipCtrlMsg::EAnt ant)
{
	auto pol = EPolarization::VERT;

	if( (ant == SSmsMsg::ANT1H) ||
		(ant >= SSmsMsg::DF_ANT_1H && ant <= SSmsMsg::DF_ANT_9H) )
		pol = EPolarization::HORIZ;

	return pol ;
}

//////////////////////////////////////////////////////////////////////
//
// Get antenna
//
bool CConfig::GetAntCable(bool hf, SEquipCtrlMsg::EAnt ant, Units::Frequency freq, SMutableConfig::SAntennaAndCable& antCable) const
{
	if (hf)
	{
		antCable = m_mutableConfig.hfConfig.antCable;
	}
	else if (m_mutableConfig.vushfConfig.antPolicy == EAntPolicy::AUTO_FREQ)
	{
		if (freq <= m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].freqHigh)
		{
			antCable = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1];
		}
		else
		{
			antCable = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2];
		}
	}
	else if (m_mutableConfig.vushfConfig.antPolicy == EAntPolicy::CLIENT_NAME)
	{
		if (ant == SEquipCtrlMsg::ANT2 || ant == SEquipCtrlMsg::SMPL_RF2)	// AUX1
		{
			if(ant == SEquipCtrlMsg::SMPL_RF2 && HasDualAux1())
			{
				antCable = m_mutableConfig.vushfConfig.antCable[ERfPort::C_SLAVE_RF2];
			}
			else
			{
				antCable = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2];
			}
		}
		else
		{
			antCable = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1];
		}
	}
	else
	{
		return false;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Return the antenna type that corresponds to Scorpio specified antenna.
///</summary>
///
/// <param name="ant">
///@param[in] Has the Scorpio antenna value.
/// </param>
///
/// <param name="antType">
///@param[out] Successful output will have the corresponding antenna type.
/// </param>
///
/// <returns>
///@return: index of SMSMSG_ANT_MAP that has corresponding ant value.
/// </returns>
///
size_t CConfig::GetAntType(SEquipCtrlMsg::EAnt ant, CAntenna::EAntennaType & antType) const
{
	size_t idx = 0;
	bool antTypeFound = false;

	for(; idx < SMSMSG_ANT_MAP_CNT && !antTypeFound; ++idx)
	{
		if(SMSMSG_ANT_MAP[idx].ant == ant)
		{
			antType = SMSMSG_ANT_MAP[idx].antType;
			antTypeFound = true;
			break;
		}
	}

	return(idx);
}

//////////////////////////////////////////////////////////////////////
//
// Get antenna factor
//
float CConfig::GetAntennaFactor(bool hf, SEquipCtrlMsg::EAnt ant, Units::Frequency freq, bool isHorizon) const
{
	std::shared_ptr<CAntenna> antenna;

	if (IsTermAnt(ant))
	{
		return 120.f;
	}

	if (hf)
	{
		antenna = m_mutableConfig.hfConfig.antCable.antenna;
	}
	else if (m_mutableConfig.vushfConfig.antPolicy == EAntPolicy::AUTO_FREQ)
	{
		if (freq <= m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].freqHigh)
		{
			antenna = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna;
		}
		else
		{
			antenna = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antenna;
		}
	}
	else if (m_mutableConfig.vushfConfig.antPolicy == EAntPolicy::CLIENT_NAME)
	{
		if (ant == SEquipCtrlMsg::ANT2 || ant == SEquipCtrlMsg::SMPL_RF2)	// AUX1
		{
			if(ant == SEquipCtrlMsg::SMPL_RF2 && HasDualAux1())
			{
				antenna = m_mutableConfig.vushfConfig.antCable[ERfPort::C_SLAVE_RF2].antenna;
			}
			else
			{
				antenna = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antenna;
			}
		}
		else
		{
			antenna = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna;
		}
	}

	if (antenna)
	{
		auto factors = (isHorizon ? CAntenna::GetHAntennaFactors(antenna.get()) : CAntenna::GetVAntennaFactors(antenna.get()));
		return CConfig::GetFreqDepValue(factors, freq);
	}
	return 120.f;
}


//////////////////////////////////////////////////////////////////////
//
// Get the antenna index from the subblock index
//
size_t CConfig::GetAntFromSubBlock(size_t subBlock, SEquipCtrlMsg::EAnt eAnt, bool hf)
{
	size_t ant;
	switch(eAnt)
	{
		case SEquipCtrlMsg::DF_ANT_3V:
			ant = subBlock;
			break;

		case SEquipCtrlMsg::DF_ANT_5V:
			// U/SHF vertical
			ant = C7234201102::Index5(C7234201102::GRAY_CODE5[subBlock]);
			break;

		case SEquipCtrlMsg::DF_ANT_9H:
			// U/SHF horizontal
			ant = C7234201102::Index9(C7234201102::GRAY_CODE9[1][subBlock]);
			break;

		case SEquipCtrlMsg::DF_ANT_9V:
			if (hf)
			{
				ant = subBlock;
			}
			else
			{
				// U/SHF vertical
				ant = C7234201102::Index9(C7234201102::GRAY_CODE9[0][subBlock]);
			}
			break;

		default:
			ASSERT(FALSE);
			ant = 0;
			break;
	}
	return ant;
}


//////////////////////////////////////////////////////////////////////
//
// Fill in a SBandwidthList structure
//
void CConfig::GetBandwidths(bool hf, EMode mode, Units::Frequency narrowRxBw, Units::Frequency wideRxBw,
	_Out_ SEquipCtrlMsg::SGetBandResp::SBandwidthList& bandwidthList, bool force) const
{
	STaskParams params;
	params.hf = hf;
	params.mode = mode;
	params.bw = 0;
	params.rxHwBw = narrowRxBw;
	params.startFreq = params.stopFreq = 0;
	memset(&bandwidthList, 0, sizeof(bandwidthList));

	if ((hf && m_mutableConfig.hfConfig.rxType != SMutableConfig::SHfConfig::NO_HFRX) ||
		(!hf &&  m_mutableConfig.vushfConfig.rxType != SMutableConfig::SVushfConfig::NO_VUHFRX) ||
		force)
	{
		Units::Frequency rxBw(narrowRxBw);
		bool notDone = true;
		bool chk40MHzLimit = false;

		while (notDone)
		{
			for (ProcParamsMap::const_iterator entry = m_procParams.lower_bound(params);
				entry != m_procParams.end() && entry->first.hf == hf && entry->first.mode == mode &&
				(entry->first.rxHwBw == rxBw) &&
				bandwidthList.numBandwidths < SEquipCtrlMsg::SGetBandResp::MAX_BANDWIDTHS;
			++entry)
			{
				size_t bwIdx;

				for (bwIdx = 0; bwIdx < bandwidthList.numBandwidths; ++bwIdx)
				{
					if (Units::Frequency(entry->first.bw) == bandwidthList.bandwidth[bwIdx])
					{
						// Duplicate bandwidth
						break;
					}
				}

				if(chk40MHzLimit && entry->first.bw > C_40_MHZ)
				{
					//Do not report above 40 MHz bandwidth.
					break;
				}
				else if (bwIdx == bandwidthList.numBandwidths)
				{
					bandwidthList.bandwidth[bandwidthList.numBandwidths++] = entry->first.bw.GetRaw();
				}
			}

			if (rxBw < wideRxBw)
			{
				params.rxHwBw = rxBw = wideRxBw;

				if(HasShfExt() && mode == EMode::MEASURE)
				{
					//System with SHF extension has 40 MHz measurement limit.
					chk40MHzLimit = true;
			}
			}
			else
			{
				notDone = false;
			}
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get normalized dual pol fourier antenna coefficients - should this be in CAntenna?
//
bool CConfig::GetDualPolFourierPattern(const CAntenna::SWfaParams2& wfaParams2, unsigned long numAnts,
	Units::Frequency freq, bool interpolate, CAntenna::DualPolPattern& outPat) const
{
	// Check MRU cache first
	MDualPolCache::const_iterator it = dualPolCache.find(freq);
	if (it != dualPolCache.end())
	{
		outPat = it->second;
		return true;
	}

	// Calculate pattern coefficients and add to cache
	if (!CAntenna::GetDualPolFourierPattern(wfaParams2, numAnts, freq, interpolate, outPat))
	{
		return false;
	}

	// Add these coefficients to the cache
	if (dualPolCacheList.size() == maxDualPolCache)	// remove first/oldest item from cache
	{
		dualPolCache.erase(dualPolCacheList.front());
		dualPolCacheList.pop_front();
	}
	std::pair<MDualPolCache::iterator, bool> pr = dualPolCache.insert(MDualPolCache::value_type(freq, outPat));
	ASSERT(pr.second);
	dualPolCacheList.push_back(pr.first);

	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Get non-DF dwell time iseconds
//
float CConfig::GetDwellTime(_In_ const SProcParams& procParams) const
{
	return 1 / procParams.GetBinSize().Hz<float>();
}


//////////////////////////////////////////////////////////////////////
//
// Fill in a SDwellTimes structure
//
void CConfig::GetDwellTimes(bool hf, Units::Frequency narrowRxBw, Units::Frequency wideRxBw, unsigned char numAnts,
	_Out_ SEquipCtrlMsg::SGetBandResp::SDwellTimeList& dwellTimeList,
	float (*GetEstimatedLatency)(Units::Frequency sampleRate, C3230::SDecimation decimations)) const
{
	STaskParams params;
	params.hf = hf;
	params.mode = EMode::MEASURE;
	params.bw = 0;
	params.rxHwBw = narrowRxBw;
	params.startFreq = params.stopFreq = 0;
	memset(&dwellTimeList, 0, sizeof(dwellTimeList));

	if (!hf || m_mutableConfig.hfConfig.rxType != SMutableConfig::SHfConfig::NO_HFRX)
	{
		for (ProcParamsMap::const_iterator entry = m_procParams.lower_bound(params);
			entry != m_procParams.end() && entry->first.hf == hf && entry->first.mode == EMode::MEASURE && entry->first.rxHwBw <= wideRxBw &&
				dwellTimeList.numBandwidths < SEquipCtrlMsg::SGetBandResp::MAX_BANDWIDTHS;
			++entry)
		{
			unsigned long dwellTime = std::max(1ul, static_cast<unsigned long>(1000 * GetDwellTime(entry->second))); // ms
			dwellTimeList.frequencyDwell[dwellTimeList.numBandwidths] = dwellTime;
			dwellTimeList.bandwidthDwell[dwellTimeList.numBandwidths] = dwellTime;
			dwellTimeList.modulationDwell[dwellTimeList.numBandwidths] = dwellTime;
			dwellTimeList.fieldStrengthDwell[dwellTimeList.numBandwidths++] = dwellTime;
		}
	}

	params.mode = EMode::DF;
	unsigned long numDfBandwidths = 0;

	for (ProcParamsMap::const_iterator entry = m_procParams.lower_bound(params);
		entry != m_procParams.end() && entry->first.hf == hf && entry->first.mode == EMode::DF && entry->first.rxHwBw <= wideRxBw &&
			numDfBandwidths < SEquipCtrlMsg::SGetBandResp::MAX_BANDWIDTHS;
		++entry)
	{
		dwellTimeList.directionFindingDwell[numDfBandwidths++] = std::max(1ul, static_cast<unsigned long>(1000 * numAnts *
			(GetDwellTime(entry->second) + GetEstimatedLatency(entry->second.sampleRate, entry->second.decimations)))); //ms
	}

	if (numDfBandwidths > dwellTimeList.numBandwidths)
	{
		dwellTimeList.numBandwidths = numDfBandwidths;
	}

	return;
}


float CConfig::GetFreqDepValue(const FreqDepParams& table, Units::Frequency freq)
{
	FreqDepParams::const_iterator entry = table.lower_bound(freq);

	if (entry == table.end())
	{
		// Frequency beyond end of table, use last value
//		ASSERT(!table.empty());
		return table.empty() ? 0 : table.rbegin()->second;
	}

	if (entry == table.begin())
	{
		// Below first point in table
		return table.begin()->second;
	}

	// Interpolate
	FreqDepParams::const_iterator prev(entry);
	--prev;

	return (freq - prev->first).Hz<float>() * (entry->second - prev->second) /
		(entry->first - prev->first).Hz<float>() + prev->second;
}


//////////////////////////////////////////////////////////////////////
//
// Get Gain Mode based on frequency
//
C2630::EGainMode CConfig::GetGainMode(Units::Frequency freq, bool hf) const
{
	auto& table = (hf ? m_hfGainModes : m_vushfGainModes);

	size_t gainIdx;
	for (gainIdx = 0; gainIdx < table.size() - 1 && freq > table[gainIdx].freqHigh; ++gainIdx) ;
	return table[gainIdx].gainMode;
}


//////////////////////////////////////////////////////////////////////
//
// Get frequency limits for hf
//
void CConfig::GetHfDfLimits(SSmsMsg::SGetBandResp& resp) const
{
	resp.hfDfLowFreq = m_hfDfVLowFreqUnits.GetRaw();
	resp.hfDfHighFreq = m_hfDfVHighFreqUnits.GetRaw();
	return;
}

void CConfig::GetHfMetricLimits(SSmsMsg::SGetBandResp& resp) const
{
	resp.hfMetricLowFreq = m_hfMetricVLowFreqUnits.GetRaw();
	resp.hfMetricHighFreq = m_hfMetricVHighFreqUnits.GetRaw();
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get local IP addresses (not loopback or link local)
//
void CConfig::GetIpAddresses(std::string& ipAddress, std::string& macAddress)
{
//	std::string addrs;
	int errcode = 0;
	ifaddrs* ifAddrs;
	errcode = getifaddrs(&ifAddrs);

	if (errcode != -1 && ifAddrs != nullptr)
	{
		for(auto ifAddr = ifAddrs; ifAddr != nullptr; ifAddr = ifAddr->ifa_next)
		{
			auto addr = ifAddr->ifa_addr;

			if((addr->sa_family == AF_INET && reinterpret_cast<sockaddr_in*>(addr)->sin_addr.s_addr != htonl(INADDR_LOOPBACK)) ||
				(addr->sa_family == AF_INET6 && !IN6_IS_ADDR_LOOPBACK(&reinterpret_cast<sockaddr_in6*>(addr)->sin6_addr) &&
				!IN6_IS_ADDR_LINKLOCAL(&reinterpret_cast<sockaddr_in6*>(addr)->sin6_addr)))
			{
				char buffer[INET6_ADDRSTRLEN];
				inet_ntop(addr->sa_family, addr->sa_family == AF_INET ? static_cast<void*>(&reinterpret_cast<sockaddr_in*>(addr)->sin_addr) :
						static_cast<void*>(&reinterpret_cast<sockaddr_in6*>(addr)->sin6_addr), buffer, INET6_ADDRSTRLEN);


				if(!ipAddress.empty())
				{
					ipAddress += ", ";
				}

				ipAddress += buffer;
			}

			if (addr && addr->sa_family == AF_PACKET && strncmp(ifAddr->ifa_name, "lo", 2) != 0)
			{
				auto s = reinterpret_cast<sockaddr_ll*>(addr);
				std::string sbuf;
				for (unsigned char i = 0; i < reinterpret_cast<sockaddr_ll*>(addr)->sll_halen; ++i)
				{
					char buf[3];
					snprintf(buf, 3, "%02x", reinterpret_cast<sockaddr_ll*>(addr)->sll_addr[i]);
					sbuf += buf;
					if (i != s->sll_halen - 1) sbuf += ":";
				}
				if (!macAddress.empty())
				{
					macAddress += ", ";
				}
				macAddress += sbuf;
			}
		}

		freeifaddrs(ifAddrs);
	}
	TRACE("GetIpAddresses: %s %s\n", ipAddress.c_str(), macAddress.c_str());
}


//////////////////////////////////////////////////////////////////////
//
// Get latency (in seconds)
//
double CConfig::GetLatency(Units::Frequency bw, const C3230::SDecimation& decimations) const
{
	// NOTE: does not include the extra clocks for "direct" path. Don't use for TDOA.

	unsigned long csms = 0;
	auto it = m_smsTdoaMap.find(bw);
	if (it != m_smsTdoaMap.end())
	{
		csms = it->second.csms;
	}

	double latency = static_cast<double>(csms) / C3230::ADC_CLOCK_RATE;
	if (decimations.ddcCICDecimation > 1)	// DDC is used
	{
		latency += m_ddcExtraSamples * decimations.GetTotal() / C3230::ADC_CLOCK_RATE;
	}
	return latency;
}

//////////////////////////////////////////////////////////////////////
//
// Get lightning protection gain
//
float CConfig::GetLightningProtectGain(bool hf, SEquipCtrlMsg::EAnt ant, Units::Frequency freq) const
{
	SMutableConfig::SAntennaAndCable antCable;
	if (GetAntCable(hf, ant, freq, antCable) && antCable.lightningProt)
	{
		return GetFreqDepValue(m_lightningProtectGain, freq);
	}
	return 0;
}


//////////////////////////////////////////////////////////////////////
//
// Get processing parameters
//
void CConfig::GetNetworkInfo(std::string& ipAddress, std::string& macAddress) const
{
	ipAddress = m_ipAddress;
	macAddress = m_macAddress;
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get processing parameters
//
CConfig::SProcParams CConfig::GetProcParams(const STaskParams& taskParams, bool bestBw) const
{
	const auto params = m_procParams.lower_bound(taskParams);

	if(params == m_procParams.end())
	{
		throw ErrorCodes::INVALIDBANDWIDTH;
	}
	else if(!(taskParams == params->first))
	{
		if (taskParams.mode != params->first.mode || taskParams.rxHwBw != params->first.rxHwBw)
		{
			throw ErrorCodes::INVALIDBANDWIDTH;
		}
		else if(bestBw && params->first.stopFreq - params->first.startFreq == 0)
		{
			return params->second;
		}
		else
		{
			throw ErrorCodes::INVALIDBANDWIDTH;
		}
	}

	return params->second;
}


//////////////////////////////////////////////////////////////////////
//
// Get reference antenna from measurement antenna
//
SEquipCtrlMsg::EAnt CConfig::GetRefAnt(SEquipCtrlMsg::EAnt ant)
{
	switch(ant)
	{
	case SSmsMsg::ANT1:
	case SSmsMsg::DF_ANT_1V:
	case SSmsMsg::DF_ANT_2V:
	case SSmsMsg::DF_ANT_3V:
	case SSmsMsg::DF_ANT_4V:
	case SSmsMsg::DF_ANT_5V:
	case SSmsMsg::DF_ANT_6V:
	case SSmsMsg::DF_ANT_7V:
	case SSmsMsg::DF_ANT_8V:
	case SSmsMsg::DF_ANT_9V:
		return SSmsMsg::ANT1;

	case SSmsMsg::ANT1H:
	case SSmsMsg::DF_ANT_1H:
	case SSmsMsg::DF_ANT_2H:
	case SSmsMsg::DF_ANT_3H:
	case SSmsMsg::DF_ANT_4H:
	case SSmsMsg::DF_ANT_5H:
	case SSmsMsg::DF_ANT_6H:
	case SSmsMsg::DF_ANT_7H:
	case SSmsMsg::DF_ANT_8H:
	case SSmsMsg::DF_ANT_9H:
		return SSmsMsg::ANT1H;

	case SSmsMsg::ANT2:
	case SSmsMsg::SMPL_RF2:
		return SSmsMsg::ANT2;

	case SSmsMsg::REF_TERM:
	case SSmsMsg::SMPL_TERM_1:
		return SSmsMsg::REF_TERM;	// Or should it be ANT1?

	default:
		return SSmsMsg::ANT1;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the restricted frequency range for index
//
bool CConfig::GetRestrictedFreqPair(size_t index, Units::FreqPair& freqPair) const
{
	if (index >= m_restrictedFreq.size()) return false;
	freqPair = m_restrictedFreq[index];
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Get RF cable loss
//
float CConfig::GetRfCableLoss(bool hf, SEquipCtrlMsg::EAnt ant, Units::Frequency freq) const
{
	float loss; // dB

	loss = 0;
	if (IsTermAnt(ant))
	{
		return loss;
	}
	if (hf)
	{
		loss = m_mutableConfig.hfConfig.antCable.fixedLoss +
			CalcCableLoss(m_mutableConfig.hfConfig.antCable.cableType, freq, m_mutableConfig.hfConfig.antCable.cableLength);
	}
	else
	{
		ERfPort i;

		if (m_mutableConfig.vushfConfig.antPolicy == EAntPolicy::AUTO_FREQ)
		{
			i = (freq <= m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].freqHigh ? ERfPort::C_RF1 : ERfPort::C_RF2);
		}
		else if (m_mutableConfig.vushfConfig.antPolicy == EAntPolicy::CLIENT_NAME)
		{
			i = ((ant == SEquipCtrlMsg::ANT2 || ant == SEquipCtrlMsg::SMPL_RF2) ? ERfPort::C_RF2 : ERfPort::C_RF1);
		}
		else
		{
			i = ERfPort::C_RF1;	// default
		}

		//SHF Extension always connect to RF2(Aux1) and it has 2 cables:
		// 1 from antenna to Analyzer, the other from Analyzer to Processor (csms).
		if( i == 1 && GetShfExtSetting() != EShfExt::NO_SHF_EXT )
		{
		    loss = CalcCableLoss(GetShfExtCableType(), freq, GetShfExtCableLength()) +
			    m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].fixedLoss +
			    GetShfExtIfCableLoss(i);
		}
		else
		{
			loss = m_mutableConfig.vushfConfig.antCable[i].fixedLoss +
				CalcCableLoss(m_mutableConfig.vushfConfig.antCable[i].cableType, freq,
				              m_mutableConfig.vushfConfig.antCable[i].cableLength);
		}
	}

	return loss;
}


//////////////////////////////////////////////////////////////////////
//
// Get single entry from config file
//
std::string CConfig::GetConfigString(const std::string& keyWanted)
{
	std::ifstream file((m_filePath + "csmsConfig.txt").c_str());
	if (file)
	{
		// Get values from file
		std::string line;
		while(!std::getline(file, line).eof())
		{
			try
			{
				std::istringstream lineStream(line);
				std::string key;
				std::string value;

				std::getline(lineStream, key, '=');
				while (!key.empty() && key.front() == ' ') key.erase(key.begin());
				while (!key.empty() && key.back() == ' ') key.pop_back();

				std::getline(lineStream, value);
				switch(m_mutableConfig.version)
				{
				case CURRENT_NVRAM_VERSION: // Current version
					if (key == keyWanted)
						return value;
					break;
				default:
					break;
				}
			}
			catch(std::invalid_argument&)
			{
				// Ignore bad lines
			}
		}
	}
	return std::string();
}


//////////////////////////////////////////////////////////////////////
//
// Returns the sample channel attenuation db against the reference channel.
//
signed char CConfig::GetSampleGainOffset(Units::Frequency freq, bool isHorizon, bool isHf) const
{

	//In HF: Sample and Reference have the same gain (no adjustment).
	if (isHf)
	{
		return 0;
	}


	auto antenna = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna;	// df antenna is always [0]
	if (!antenna || !CAntenna::IsDfAntenna(antenna.get()))
	{
		return 0;
	}
	auto hOffset = CAntenna::GetHSampleGainOffset(antenna.get());
	auto vOffset = CAntenna::GetVSampleGainOffset(antenna.get());

	signed char offsetVal = 0;
	if (isHorizon && !hOffset.empty())
	{
		float fOffset = GetFreqDepValue(hOffset, freq);

		//Positive number round-up, negative round-down.
		if (fOffset > 0.0f)
			offsetVal = static_cast<signed char>(fOffset + 1.0f) / 2 * 2;
		else
			offsetVal = static_cast<signed char>(fOffset - 1.0f) / 2 * 2;
	}
	else if (!isHorizon && !vOffset.empty())
	{
		float fOffset = GetFreqDepValue(vOffset, freq );

		//Positive number round-up, negative round-down.
		if (fOffset > 0.0f )
			offsetVal = static_cast<signed char>(fOffset + 1.0f) / 2 * 2;
		else
			offsetVal = static_cast<signed char>(fOffset - 1.0f) / 2 * 2;
	}
	return offsetVal;
}

//////////////////////////////////////////////////////////////////////
/// Get SHF Extension Analyzer gain in dB.
///
///@note: It is assumed that the caller has checked that SHF Extension is used
///	  before calling this method.
///
float CConfig::GetShfExtensionGain(Units::Frequency freq) const
{
	ASSERT(GetShfExtSetting() != EShfExt::NO_SHF_EXT);
	return GetFreqDepValue(m_shfExtensionGain, freq);
}

//////////////////////////////////////////////////////////////////////
/// Get cable loss from Analyzer to the input of csms Processor.
///
///@param[in] rfIndex: RF input index. RF1 = 0, RF2(Aux1): 1.
///
///@note: It is assumed that the caller has checked that SHF Extension is used
///	  before calling this method.
///
float CConfig::GetShfExtIfCableLoss(ERfPort rfIndex) const
{
	ASSERT(GetShfExtSetting() != EShfExt::NO_SHF_EXT);
	ASSERT(rfIndex <= 1);
  	return(CalcCableLoss(m_mutableConfig.vushfConfig.antCable[rfIndex].cableType,
	                     GetShfExtIfFreq(), m_mutableConfig.vushfConfig.antCable[rfIndex].cableLength));
}

//////////////////////////////////////////////////////////////////////
//
// Get the switch gain
//
float CConfig::GetSwitchGain(bool hf, SEquipCtrlMsg::EAnt ant, Units::Frequency freq) const
{
	std::shared_ptr<CAntenna> antenna;
	std::shared_ptr<CSwitch> sw;
	bool low = false;
	if (hf)
	{
		antenna = m_mutableConfig.hfConfig.antCable.antenna;
		sw = m_mutableConfig.hfConfig.antCable.sw;
		if (freq < CAntenna::GetHighBandCrossoverFreq(antenna.get()))
		{
			low = true;
		}
	}
	else	// vushf
	{
		if (m_mutableConfig.vushfConfig.antPolicy == EAntPolicy::AUTO_FREQ)
		{
			if (freq <= m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].freqHigh)
			{
				sw = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].sw;
			}
			else
			{
				sw = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].sw;
			}
		}
		else if (m_mutableConfig.vushfConfig.antPolicy == EAntPolicy::CLIENT_NAME)
		{
			if (ant == SEquipCtrlMsg::ANT2 || ant == SEquipCtrlMsg::SMPL_RF2)	// AUX1
			{
				sw = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].sw;
			}
			else
			{
				sw = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].sw;
			}
		}
	}
	if (sw)
	{
		auto gains = CSwitch::GetSwitchGains(sw.get(), low);
//		printf("CConfig::GetSwitchGain: low = %d gains = %u freq = %f ant = %d gain = %f\n",
//			low, gains.size(), freq.Hz<double>(), ant, GetFreqDepValue(gains, freq));
		return GetFreqDepValue(gains, freq);
	}
	return 0;
}


//////////////////////////////////////////////////////////////////////
//
// Get the DF Symmetry parameters
//
const CAntenna::SSymmetryDfParams* CConfig::GetSymmetryDfParams(bool horiz, bool shf) const
{
	auto ant = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna.get();
	return CAntenna::GetSymmetryDfParams(ant, horiz, shf);
}


//////////////////////////////////////////////////////////////////////
//
// Get tdoa adjustment (in Units::TimeSpan)
//
void CConfig::GetTdoaAdjustment(Units::Frequency bw, const C3230::SDecimation& decimations, bool direct,
	Units::TimeSpan& adjTdoa, Units::TimeSpan& adjStart) const
{
	unsigned long csms = 0;
	unsigned long sms = 0;
	auto it = m_smsTdoaMap.find(bw);
	if (it != m_smsTdoaMap.end())
	{
		csms = it->second.csms;
		sms = it->second.sms;
	}

	double adjust = static_cast<double>(csms) / C3230::ADC_CLOCK_RATE;
	if (direct)
	{
		adjust += static_cast<double>(m_directExtraClocks) / C3230::ADC_CLOCK_RATE;
	}
	if (decimations.ddcCICDecimation > 1)	// DDC is used
	{
		adjust += m_ddcExtraSamples * decimations.GetTotal() / C3230::ADC_CLOCK_RATE;
	}
	adjust -= static_cast<double>(sms) / C3230::ADC_CLOCK_RATE;
	adjTdoa = Units::TimeSpan(adjust);

	double adjustStart = static_cast<double>(m_tdoaStart.csms) / C3230::ADC_CLOCK_RATE;
	adjustStart -= static_cast<double>(m_tdoaStart.sms) / C3230::ADC_CLOCK_RATE;
	adjStart = Units::TimeSpan(adjustStart);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get frequency limits for vushf
//
void CConfig::GetVushfDfLimits(SSmsMsg::SGetBandResp& resp) const
{
	resp.vushfDfVLowFreq = m_vushfDfVLowFreq.GetRaw();
	resp.vushfDfVHighFreq = m_vushfDfVHighFreq.GetRaw();
	resp.vushfDfHLowFreq = m_vushfDfHLowFreq.GetRaw();
	resp.vushfDfHHighFreq = m_vushfDfHHighFreq.GetRaw();
	return;
}

void CConfig::GetVushfMetricLimits(SSmsMsg::SGetBandResp& resp) const
{
	resp.vushfMetricVLowFreq = m_vushfMetricVLowFreq.GetRaw();
	resp.vushfMetricVHighFreq = m_vushfMetricVHighFreq.GetRaw();
	resp.vushfMetricHLowFreq = m_vushfMetricHLowFreq.GetRaw();
	resp.vushfMetricHHighFreq = m_vushfMetricHHighFreq.GetRaw();
	return;
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Load the antenna factor values from the file specified in csmsGeneral.xml
///</summary>
///
/// <param name="pAnt">
///@param[in] Pointer to the antenna object, which will have the antenna
///		 		factor values saved to.
/// </param>
///
///
void CConfig::LoadAntFactors(CAntenna* pAnt)
{
	bool noErr = true;
	std::string filename;

	if (CAntenna::HasHorizontalPolarization(pAnt, 0))
	{
		noErr = LoadFreqDepParams(CAntenna::GetHAntennaFactors(pAnt),
				m_filePath + "csmsGeneral.xml", CAntenna::GetBaseName(pAnt) + ".HAntennaFactors", filename);
		CAntenna::SetAntFactorLoadedFlag(pAnt, noErr);
		CAntenna::SaveAntFactorFilename(pAnt, filename);
		if (CAntenna::IsDfAntenna(pAnt))
		{
			LoadFreqDepParams(CAntenna::GetHSampleGainOffset(pAnt), m_filePath + "csmsGeneral.xml",
					CAntenna::GetBaseName(pAnt) + ".HSampleGainOffset", filename);
		}
	}
	if (CAntenna::HasVerticalPolarization(pAnt, 0))
	{
		auto loaded = LoadFreqDepParams(CAntenna::GetVAntennaFactors(pAnt),
				m_filePath + "csmsGeneral.xml", CAntenna::GetBaseName(pAnt) + ".VAntennaFactors", filename);

		//Only save Vertical antenna factor status if there is no error.
		if(noErr)
		{
			CAntenna::SetAntFactorLoadedFlag(pAnt, loaded);
			CAntenna::SaveAntFactorFilename(pAnt, filename);
		}

		if (CAntenna::IsDfAntenna(pAnt))
		{
			LoadFreqDepParams(CAntenna::GetVSampleGainOffset(pAnt), m_filePath + "csmsGeneral.xml",
					CAntenna::GetBaseName(pAnt) + ".VSampleGainOffset", filename);
		}
	}

	return;

}

//////////////////////////////////////////////////////////////////////
//
// Load AVD params
//
void CConfig::LoadAvdParams(void)
{
	XMLProcessor xmlParse;
	if (xmlParse.ReadLoadXML(m_filePath + "csmsGeneral.xml"))
	{
		try
		{
			XMLProcessor::VectStr v;
			v = xmlParse.GetXMLVectorValue("AVDMeasurement.BW_dwellTime");
			if (v.size() == 1) m_avdMeasParams.bwDwellTime = std::stoul(v[0]);
			v = xmlParse.GetXMLVectorValue("AVDMeasurement.BW_betaParam");
			if (v.size() == 1) m_avdMeasParams.bwBetaParam = std::stof(v[0]);
			v = xmlParse.GetXMLVectorValue("AVDMeasurement.BW_yParam");
			if (v.size() == 1) m_avdMeasParams.bwYParam = std::stof(v[0]);
			v = xmlParse.GetXMLVectorValue("AVDMeasurement.BW_x1Param");
			if (v.size() == 1) m_avdMeasParams.bwX1Param = std::stof(v[0]);
			v = xmlParse.GetXMLVectorValue("AVDMeasurement.BW_x2Param");
			if (v.size() == 1) m_avdMeasParams.bwX2Param = std::stof(v[0]);
			v = xmlParse.GetXMLVectorValue("AVDMeasurement.BW_repeatCount");
			if (v.size() == 1) m_avdMeasParams.bwRepeatCount = std::stoul(v[0]);
			v = xmlParse.GetXMLVectorValue("AVDMeasurement.BW_aveMethod");
			if (v.size() == 1) m_avdMeasParams.bwAveMethod = static_cast<SEquipCtrlMsg::EAveMethod>(std::stoul(v[0]));
			v = xmlParse.GetXMLVectorValue("AVDMeasurement.Freq_freqMethod");
			if (v.size() == 1) m_avdMeasParams.freqFreqMethod = static_cast<SEquipCtrlMsg::SGetFreqCmd::EFreqMethod>(std::stoul(v[0]));
			v = xmlParse.GetXMLVectorValue("AVDMeasurement.Freq_dwellTime");
			if (v.size() == 1) m_avdMeasParams.freqDwellTime = std::stoul(v[0]);
			v = xmlParse.GetXMLVectorValue("AVDMeasurement.Freq_repeatCount");
			if (v.size() == 1) m_avdMeasParams.freqRepeatCount = std::stoul(v[0]);
			v = xmlParse.GetXMLVectorValue("AVDMeasurement.Freq_aveMethod");
			if (v.size() == 1) m_avdMeasParams.freqAveMethod = static_cast<SEquipCtrlMsg::EAveMethod>(std::stoul(v[0]));
			v = xmlParse.GetXMLVectorValue("AVDMeasurement.autorate_mu");
			if (v.size() == 1) m_avdMeasParams.autorateMu = std::stof(v[0]);
			v = xmlParse.GetXMLVectorValue("AVDMeasurement.autorate_k");
			if (v.size() == 1) m_avdMeasParams.autorateK = std::stof(v[0]);
			v = xmlParse.GetXMLVectorValue("AVDMeasurement.autorate_goal");
			if (v.size() == 1) m_avdMeasParams.autorateGoal = std::stof(v[0]);
			v = xmlParse.GetXMLVectorValue("AVDMeasurement.autorate_maxtime");
			if (v.size() == 1) m_avdMeasParams.autorateMaxTime = std::stof(v[0]);
		}
		catch(std::invalid_argument&)
		{
			// Ignore bad lines
		}
	}
	else
	{
		TRACE("LoadAvdParams: Unable to open csmsGeneral.xml\n");
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get cable loss coefficients
//
CConfig::SCableCoeffs CConfig::LoadCableCoeffs(_In_ const std::string& section)
{
	SCableCoeffs cableCoeffs = { 0, 0, 0};

	XMLProcessor xmlParse;
	if (xmlParse.ReadLoadXML(m_filePath + "csmsGeneral.xml"))
	{
		try
		{
			XMLProcessor::VectStr v;
			v = xmlParse.GetXMLVectorValue(section + ".k1");
			if (v.size() == 1) cableCoeffs.k1 = std::stof(v[0]);
			v = xmlParse.GetXMLVectorValue(section + ".k2");
			if (v.size() == 1) cableCoeffs.k2 = std::stof(v[0]);
			v = xmlParse.GetXMLVectorValue(section + ".velocityPercent");
			if (v.size() == 1) cableCoeffs.velocityPercent = std::stof(v[0]);
		}
		catch(std::invalid_argument&)
		{
			// Ignore bad lines
		}
	}
	else
	{
		TRACE("LoadCableCoeffs: Unable to open csmsGeneral.xml\n");
	}

	return cableCoeffs;
}


//////////////////////////////////////////////////////////////////////
//
// Get field strength params (cables, antenna factors, etc.
//
void CConfig::LoadAntennaParams(void)
{
	m_cableCoeffs[ECableType::LMR_240] = LoadCableCoeffs("lmr240");
	m_cableCoeffs[ECableType::LMR_400] = LoadCableCoeffs("lmr400");
	m_cableCoeffs[ECableType::FOAM_3_8] = LoadCableCoeffs("38foam");
	m_cableCoeffs[ECableType::FOAM_1_2] = LoadCableCoeffs("12foam");
	m_cableCoeffs[ECableType::FOAM_7_8] = LoadCableCoeffs("78foam");
	std::string filename;

	LoadFreqDepParams(m_lightningProtectGain, m_filePath + "csmsGeneral.xml", "lightningprotect", filename);

	if (m_mutableConfig.hfConfig.antCable.antenna)
	{
		auto a = m_mutableConfig.hfConfig.antCable.antenna.get();
		LoadFreqDepParams(CAntenna::GetVAntennaFactors(a), m_filePath + "csmsGeneral.xml",
				CAntenna::GetBaseName(a) + ".VAntennaFactors", filename);
		if (m_mutableConfig.hfConfig.antCable.sw)
		{
			auto s = m_mutableConfig.hfConfig.antCable.sw.get();
			LoadFreqDepParams(CSwitch::GetSwitchGains(s, false),
					m_filePath + "csmsGeneral.xml", "hfswitch", filename);
			LoadFreqDepParams(CSwitch::GetSwitchGains(s, true),
					m_filePath + "csmsGeneral.xml", "hfswitchlf", filename);
		}
	}
	if (m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna)
	{
		if (IsProcWithAntSelectionSwitch())
		{
			//Multiple antenna going to RF1, so must load antenna factor of each one.
			for(size_t idx = 0; idx < SMSMSG_ANT_MAP_CNT; ++idx)
			{
				auto a = SMSMSG_ANT_MAP[idx].antenna.get();
#ifdef CSMS_DEBUG
				TRACE("CConfig::LoadAntennaParams: idx = %u, base %s, name %s\n",
						idx, CAntenna::GetBaseName(a).c_str(), CAntenna::GetFullName(a).c_str());
#endif
				LoadAntFactors(a);
			}

			//Assign default value.
			m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna = SMSMSG_ANT_MAP[0].antenna;
		}
		else
		{
			auto a = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna.get();
			LoadAntFactors(a);
		}

		if (m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].sw)
		{
			auto s = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].sw.get();
			LoadFreqDepParams(CSwitch::GetSwitchGains(s, false), m_filePath + "csmsGeneral.xml",
					"ushfswitch", filename);
		}
	}
	if (m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antenna)
	{
		auto a = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antenna.get();
#ifdef CSMS_DEBUG
		TRACE("CConfig::LoadAntennaParams: Aux1 base %s, name %s\n",
				CAntenna::GetBaseName(a).c_str(), CAntenna::GetFullName(a).c_str());
#endif
		LoadAntFactors(a);
		if (m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].sw)
		{
			auto s = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].sw.get();
			LoadFreqDepParams(CSwitch::GetSwitchGains(s, false), m_filePath + "csmsGeneral.xml",
					"ushfswitch", filename);
		}

		if (HasDualAux1())
		{
			//Dual band Aux1: RF2 has VUHF_AUX1 & C_SLAVE_RF2 has CUSTOM_ANT.
			// The Low & High frequency limit is specified by antenna factor limit.
			auto antFactors = CAntenna::GetVAntennaFactors(a);
			auto iter = antFactors.begin();
			CAntenna::SetVLowFreq(a, iter->first);
			auto rIter = antFactors.rbegin();
			CAntenna::SetVHighFreq(a, rIter->first);
			m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].freqHigh = rIter->first;
			a = m_mutableConfig.vushfConfig.antCable[ERfPort::C_SLAVE_RF2].antenna.get();
			LoadAntFactors(a);

			antFactors = CAntenna::GetVAntennaFactors(a);
			iter = antFactors.begin();
			CAntenna::SetVLowFreq(a, iter->first);
			rIter = antFactors.rbegin();
			CAntenna::SetVHighFreq(a, rIter->first);
			m_mutableConfig.vushfConfig.antCable[ERfPort::C_SLAVE_RF2].freqHigh = rIter->first;
		}
	}

	auto shfExt = GetShfExtSetting();

	if( shfExt != EShfExt::NO_SHF_EXT )
	{
		///@note: For now we only support Keysight N9010A 44 GHz & B/W 25 MHz.
		if(shfExt == EShfExt::N9010A_544_B25)
		{
			LoadFreqDepParams(m_shfExtensionGain, m_filePath + "csmsGeneral.xml", "n9010a544b25", filename);
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Load the df parameters
//
void CConfig::LoadDfParams(void)
{
	if (m_mutableConfig.hfConfig.antCable.antenna)
	{
		auto a = m_mutableConfig.hfConfig.antCable.antenna.get();
		CAntenna::LoadDfParams(a, m_filePath + "csmsDf.ini");
	}
	if (m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna)
	{
		auto a = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna.get();
		CAntenna::LoadDfParams(a, m_filePath + "csmsDf.ini");
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Load frequency-dependent parameters from file
//
bool CConfig::LoadFreqDepValues(const std::string& filename, FreqDepParams& table) const
{
	table.clear();

	// Read in data and store them - format is frequency(Hz),value
	std::ifstream file((m_filePath + filename).c_str());
	if (!file)
	{
		return false;
	}

	std::string line;
	while(!std::getline(file, line).eof())
	{
		unsigned long long freq;
		float value;

		if (sscanf(line.c_str(), "%llu, %f", &freq, &value) == 2)
		{
			table[freq] = value;
		}
		else
		{
			TRACE("Bad line in %s: %s\n", filename.c_str(), line.c_str());
		}
	}
	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Load frequency-dependent parameters
//
bool CConfig::LoadFreqDepParams(FreqDepParams& table, const std::string& fileName,
		const std::string& section, std::string& freqFilename, bool notify)
{
	bool isOk = false;
	XMLProcessor xmlParse;
	if (xmlParse.ReadLoadXML(fileName))
	{
		XMLProcessor::VectStr v = xmlParse.GetXMLVectorValue(section + ".filename");
		if (v.empty() || !LoadFreqDepValues(v[0].c_str(), table))
		{
			if (notify)
			{
				std::string fileEntry;

				if (v.empty())
				{
					fileEntry = fileName;
				}
				else
				{
					fileEntry = v[0];
				}

				// Output warning to Log.
				CLog::Log(CLog::WARNINGS, "Config data missing: Key %s not found in Section %s", fileEntry.c_str(), section.c_str());
			}

			// Use defaults from ini file
			XMLProcessor::VectStr v1 = xmlParse.GetXMLVectorValue(section + ".factor.freq");
			XMLProcessor::VectStr v2 = xmlParse.GetXMLVectorValue(section + ".factor.value");
			if (v1.size() != v2.size())
			{
				TRACE("factor.freq and factor.value are different size\n");
			}
			else
			{
				try
				{
					for (unsigned int i = 0; i < v1.size(); ++i)
					{
						auto freq = Units::Frequency(std::stod(v1[i]));
						table[freq] = std::stof(v2[i]);
					}
				}
				catch(std::invalid_argument&)
				{
					// Ignore bad lines
				}

			}
		}
		else
		{
			//Value is loaded.
			isOk = true;
		}

		if (!v.empty())
		{
			freqFilename = v[0];
		}
	}
	else
	{
		TRACE("Unable to open %s\n", fileName.c_str());
	}

	// DEBUG OUTPUT
//	for (auto& it : table)
//	{
//		TRACE("%s: %f => %f\n", section.c_str(), it.first.Hz<double>(), it.second);
//	}
	return(isOk);
}


//////////////////////////////////////////////////////////////////////
//
// Load frequency-pair parameters
//
bool CConfig::LoadFreqPairParams(FreqPairVec& freqVec, const std::string& fileName, const std::string& section, bool notify)
{
	// Note: The start frequency specified in the data must be in ascending order.
	bool success = true;
	ELoadStatus loadStatus;

	XMLProcessor xmlParse;
	if (xmlParse.ReadLoadXML(fileName))
	{
		XMLProcessor::VectStr v = xmlParse.GetXMLVectorValue(section + ".filename");
		if (v.empty() || (loadStatus = LoadFreqPairValues(v[0].c_str(), freqVec)) == C_FILE_ERR)
		{
			if (notify)
			{
				std::string fileEntry;

				if (v.empty())
				{
					fileEntry = fileName;
				}
				else
				{
					fileEntry = v[0];
				}

				// Output warning to Log.
				CLog::Log(CLog::WARNINGS, "Config data missing: Key %s not found in Section %s", fileEntry.c_str(), section.c_str());
			}

			// Use defaults from ini file
			XMLProcessor::VectStr v1 = xmlParse.GetXMLVectorValue(section + ".freq1");
			XMLProcessor::VectStr v2 = xmlParse.GetXMLVectorValue(section + ".freq2");
			if (v1.size() != v2.size())
			{
				TRACE("freq1 and freq2 are different size\n");
			}
			else
			{
				try
				{
					Units::Frequency prevFreq(0);
					for (unsigned int i = 0; success && i < v1.size(); ++i)
					{
						auto freq1 = Units::Frequency(std::stoull(v1[i]));
						auto freq2 = Units::Frequency(std::stoull(v2[i]));
						if (i > 0 && (prevFreq >= freq1) && (freq1 > freq2))
						{
							TRACE("Start frequency not in ascending order\n");
							success = false;
						}
						prevFreq = freq1;
						freqVec.push_back(std::make_pair(freq1, freq2));
					}
				}
				catch(std::invalid_argument&)
				{
					// Ignore bad lines
				}

			}
		}
		else if (loadStatus != C_OK)
		{
			success = false;
		}
	}
	else
	{
		TRACE("Unable to open %s\n", fileName.c_str());
	}

	// DEBUG OUTPUT
//	for (auto& it : freqVec)
//	{
//		TRACE("%s: %f => %f\n", section.c_str(), it.first.Hz<double>(), it.second.Hz<double>());
//	}
	return success;
}


//////////////////////////////////////////////////////////////////////
//
// Load frequency pair parameters from file
//
CConfig::ELoadStatus CConfig::LoadFreqPairValues(const std::string& filename, FreqPairVec& freqVec) const
{
	// Return C_OK: parameters loaded without error
	//        C_FILE_ERR: Unable to open the file.
	//        C_FREQ_ERR: Error in the frequency pairs in the file.
	bool success = true;
	ELoadStatus loadStatus = C_OK;
	freqVec.clear();

	// Read in data and store them - format is freq1(Hz),freq2(Hz)
	std::ifstream file((m_filePath + filename).c_str());
	if (!file)
	{
		return C_FILE_ERR;
	}

	unsigned long long prevFreq = 0;
	bool hasPrevFreq = false;

	std::string line;
	while(success && !std::getline(file, line).eof())
	{
		unsigned long long freq1;
		unsigned long long freq2;

		if (sscanf(line.c_str(), "%llu, %llu", &freq1, &freq2) == 2)
		{
			freqVec.push_back(std::make_pair(Units::Frequency(freq1), Units::Frequency(freq2)));
			if (hasPrevFreq)
			{
				if (prevFreq >= freq1 || freq1 > freq2)
				{
					success = false;
				}
			}
			else if (freq1 > freq2)
			{
				success = false;
			}
			prevFreq = freq1;
			hasPrevFreq = true;
		}
		else
		{
			if (!line.empty() && line[0] != '#')
			{
				success = false;
			}
		}
	}
	if (!success)
	{
		TRACE("Bad line in %s: %s\n", filename.c_str(), line.c_str());
		loadStatus = C_FREQ_ERR;
	}
	return loadStatus;

}


//////////////////////////////////////////////////////////////////////
//
// Load gain modes from config file
//
void CConfig::LoadGainModes(void)
{
	LoadGainModes(m_hfGainModes, _T("HF.GainModes"));
	LoadGainModes(m_vushfGainModes, _T("VUSHF.GainModes"));
	return;
}


void CConfig::LoadGainModes(std::vector<SGainMode>& table, const std::string& section)
{
	XMLProcessor xmlParse;
	if (xmlParse.ReadLoadXML(m_filePath + s_configFileName))
	{
		XMLProcessor::VectStr v1 = xmlParse.GetXMLVectorValue(section + ".gainmode.freqHigh");
		XMLProcessor::VectStr v2 = xmlParse.GetXMLVectorValue(section + ".gainmode.mode");
		if (v1.size() != v2.size())
		{
			TRACE("gainmode.freqHigh and gainmode.mode are different size\n");
		}
		else
		{
			table.resize(v1.size());
			try
			{
				for (unsigned int i = 0; i < v1.size(); ++i)
				{
					table[i] = SGainMode(Units::Frequency(std::stod(v1[i])), SGainMode::StrToEnum(v2[i]));
				}
			}
			catch(std::invalid_argument&)
			{
				// Ignore bad lines
			}
		}
	}
	else
	{
		TRACE("Unable to open csmsConfig.xml\n");
	}

	// DEBUG OUTPUT
//	for (auto it : table)
//	{
//		TRACE("%f => %d\n", it.freqHigh.Hz<double>(), it.gainMode);
//	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Load mutable configuration from config file
//
void CConfig::LoadMutableConfig(void)
{
	// load / create default m_mutableConfig from config file
	// Set default values
	m_mutableConfig.version = CURRENT_NVRAM_VERSION;
	m_mutableConfig.miscConfig.databasePurgeAge = 30.0; // days
	m_mutableConfig.miscConfig.langIdx = 0;
	strcpy_s(m_mutableConfig.miscConfig.partNum, "0000-0000-00");
	strcpy_s(m_mutableConfig.miscConfig.rev, "ZZ");
	strcpy(m_mutableConfig.miscConfig.serialNumber, "AA00000");
	strcpy_s(m_mutableConfig.miscConfig.customer, "Customer");
	m_mutableConfig.miscConfig.algoType = EAlgo::NO_ALGO; // no gain equal
	m_mutableConfig.miscConfig.enhancedDynRng = false;
	m_mutableConfig.miscConfig.disableWatchdog = false;
	m_mutableConfig.miscConfig.compassBaud = 9600;
	strcpy(m_mutableConfig.miscConfig.compassPort, "/dev/null");
	m_mutableConfig.miscConfig.compassType = ECompassType::NOT_PRESENT;
	m_mutableConfig.miscConfig.gpsAvailable = EGpsAvailable::INTERNAL;
	m_mutableConfig.miscConfig.fixedLatitude = 90.0;
	m_mutableConfig.miscConfig.fixedLongitude = 0.0;
	m_mutableConfig.miscConfig.fixedAltitude = 0.0;
	m_mutableConfig.miscConfig.allowShutdown = 0;
	m_mutableConfig.miscConfig.mastDownInverted = false;
	m_mutableConfig.hfConfig.rxType = SMutableConfig::SHfConfig::HF_2630;
	m_mutableConfig.hfConfig.antCable.antenna.reset(new C7031_ant);
	m_mutableConfig.hfConfig.antCable.antType = CAntenna::EAntennaType::HF_7031;
	m_mutableConfig.hfConfig.antCable.antName = CAntenna::GetFullName(m_mutableConfig.hfConfig.antCable.antenna.get());
	m_mutableConfig.hfConfig.antCable.freqHigh = Units::Frequency(30000000.);
	m_mutableConfig.hfConfig.antCable.cableType = ECableType::LMR_240;
	m_mutableConfig.hfConfig.antCable.cableLength = 5.f;
	m_mutableConfig.hfConfig.antCable.fixedLoss = 0.f;
	m_mutableConfig.hfConfig.antCable.isMag = true;
	m_mutableConfig.hfConfig.antCable.orientation = 0;
	m_mutableConfig.hfConfig.antCable.lightningProt = false;
	m_mutableConfig.hfConfig.antCable.sw.reset();	// goes with HF_7031 default
	m_mutableConfig.hfConfig.antCable.switchType = CSwitch::ESwitchType::NO_SWITCH;
	m_mutableConfig.hfConfig.antPolicy = EAntPolicy::AUTO_FREQ;
	m_mutableConfig.hfConfig.defaultIonoHeight = 300;	// km
	m_mutableConfig.hfConfig.antCoords[0].x = 0; // m
	m_mutableConfig.hfConfig.antCoords[0].y = 0;
	m_mutableConfig.hfConfig.antCoords[0].z = 0;
	m_mutableConfig.hfConfig.antCoords[1].x = 4.5f;
	m_mutableConfig.hfConfig.antCoords[1].y = 0;
	m_mutableConfig.hfConfig.antCoords[1].z = 0;
	m_mutableConfig.hfConfig.antCoords[2].x = 0;
	m_mutableConfig.hfConfig.antCoords[2].y = 4.5f;
	m_mutableConfig.hfConfig.antCoords[2].z = 0;
	m_mutableConfig.hfConfig.antCoords[3].x = -11.2f;
	m_mutableConfig.hfConfig.antCoords[3].y = 0;
	m_mutableConfig.hfConfig.antCoords[3].z = 0;
	m_mutableConfig.hfConfig.antCoords[4].x = 0;
	m_mutableConfig.hfConfig.antCoords[4].y = -11.2f;
	m_mutableConfig.hfConfig.antCoords[4].z = 0;
	m_mutableConfig.hfConfig.antCoords[5].x = 28.0f;
	m_mutableConfig.hfConfig.antCoords[5].y = 0;
	m_mutableConfig.hfConfig.antCoords[5].z = 0;
	m_mutableConfig.hfConfig.antCoords[6].x = 0;
	m_mutableConfig.hfConfig.antCoords[6].y = 28.0f;
	m_mutableConfig.hfConfig.antCoords[6].z = 0;
	m_mutableConfig.hfConfig.antCoords[7].x = -90.0f;
	m_mutableConfig.hfConfig.antCoords[7].y = 0;
	m_mutableConfig.hfConfig.antCoords[7].z = 0;
	m_mutableConfig.hfConfig.antCoords[8].x = 0;
	m_mutableConfig.hfConfig.antCoords[8].y = -90.0f;
	m_mutableConfig.hfConfig.antCoords[8].z = 0;

	m_mutableConfig.vushfConfig.rxType = SMutableConfig::SVushfConfig::WIDE_2630;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna.reset(new C640_DSC_ant);
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antType = CAntenna::EAntennaType::VUHF_640_DSC;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antName = CAntenna::GetFullName(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna.get());
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].freqHigh = Units::Frequency(3000000000.);
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].cableType = ECableType::LMR_240;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].cableLength = 5.f;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].fixedLoss = 0.f;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].isMag = true;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].orientation = 0;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].lightningProt = false;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].sw.reset();	// goes with C640_DSC_ant default
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].switchType = CSwitch::ESwitchType::NO_SWITCH;

	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antenna.reset(new C640_2_ant);
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antType = CAntenna::EAntennaType::SHF_640_2;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antName = CAntenna::GetFullName(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antenna.get());
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].freqHigh = Units::Frequency(8500000000.);
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].cableType = ECableType::LMR_240;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].cableLength = 5.f;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].fixedLoss = 0.f;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].isMag = true;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].orientation = 0;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].lightningProt = false;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].sw.reset();	// goes with C640_2_ant default
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].switchType = CSwitch::ESwitchType::NO_SWITCH;

	m_mutableConfig.vushfConfig.antPolicy = EAntPolicy::AUTO_FREQ;
	DefaultShfExt(m_mutableConfig.vushfConfig.shfExt);

	unsigned long version = 0;
	XMLProcessor xmlParse;
	if (xmlParse.ReadLoadXML(m_filePath + s_configFileName))
	{
		try
		{
			XMLProcessor::VectStr v;
			XMLProcessor::VectStr v2;
			v = xmlParse.GetXMLVectorValue("CSMSConfiguration");
			if (v.size() == 1)
			{
				version = std::stoul(v[0]);
				m_mutableConfig.version = version;
			}
			switch(version)
			{
			case CURRENT_NVRAM_VERSION: // Current version
				// Rf inputs
				v = xmlParse.GetXMLVectorValue("RF1.freqHigh");
				if (v.size() == 1) m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].freqHigh = Units::Frequency(std::stod(v[0]));
				v = xmlParse.GetXMLVectorValue("RF1.antType");
				if (v.size() == 1)
				{
					auto antType = CAntenna::StrToEnum(v[0]);

					if(!AntValid4RfPort(ERfPort::C_RF1, antType))
					{
						antType = CAntenna::EAntennaType::NO_ANTENNA;
						m_antOk[ERfPort::C_RF1] = false;
						m_errAntStr[ERfPort::C_RF1] = v[0];
					}
					m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antType = antType;
					m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna.reset(CAntenna::Create(m_mutableConfig.vushfConfig.antCable[0].antType));
				}
				m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antName = CAntenna::GetFullName(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna.get());
				v = xmlParse.GetXMLVectorValue("RF1.cableType");
				if (v.size() == 1) m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].cableType = SMutableConfig::CableStrToEnum(v[0]);
				v = xmlParse.GetXMLVectorValue("RF1.cableLength");
				if (v.size() == 1) m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].cableLength = std::stof(v[0]);
				v = xmlParse.GetXMLVectorValue("RF1.fixedLoss");
				if (v.size() == 1) m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].fixedLoss = std::stof(v[0]);
				v = xmlParse.GetXMLVectorValue("RF1.magTrue");
				if (v.size() == 1) m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].isMag = strcasecmp(v[0].c_str(), "Mag") == 0;
				v = xmlParse.GetXMLVectorValue("RF1.orientation");
				if (v.size() == 1) m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].orientation = std::stof(v[0]);
				v = xmlParse.GetXMLVectorValue("RF1.lightning");
				if (v.size() == 1) m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].lightningProt = (std::stoul(v[0]) != 0);
				// TODO: Use new config parameter to set the switch
				v = xmlParse.GetXMLVectorValue("RF1.switchType");
				if (v.size() == 1)
				{
					m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].switchType = CSwitch::StrToEnum(v[0]);
					m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].sw.reset(CSwitch::Create(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].switchType));
				}
				else if (CAntenna::IsDfAntenna(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna.get()))
				{
					m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].switchType = CSwitch::ESwitchType::SW_7234_REF;
					m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].sw.reset(CSwitch::Create(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].switchType));
				}

				m_dualAux1 = false;
				v = xmlParse.GetXMLVectorValue("RF2.freqHigh");
				if (v.size() == 1) m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].freqHigh = Units::Frequency(std::stod(v[0]));
				v = xmlParse.GetXMLVectorValue("RF2.antType");
				if (v.size() == 1)
				{
					auto antType = CAntenna::StrToEnum(v[0]);
					if(!AntValid4RfPort(ERfPort::C_RF2, antType))
					{
						antType = CAntenna::EAntennaType::NO_ANTENNA;
						m_antOk[ERfPort::C_RF2] = false;
						m_errAntStr[ERfPort::C_RF2] = v[0];
					}
					else if(antType == CAntenna::EAntennaType::USHF_DUAL_AUX1)
					{
						antType = CAntenna::EAntennaType::VUHF_AUX1;
						m_dualAux1 = true;
					}

					m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antType = antType;
					m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antenna.reset(CAntenna::Create(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antType));
				}
				m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antName = CAntenna::GetFullName(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antenna.get());
				v = xmlParse.GetXMLVectorValue("RF2.cableType");
				if (v.size() == 1) m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].cableType = SMutableConfig::CableStrToEnum(v[0]);
				v = xmlParse.GetXMLVectorValue("RF2.cableLength");
				if (v.size() == 1) m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].cableLength = std::stof(v[0]);
				v = xmlParse.GetXMLVectorValue("RF2.fixedLoss");
				if (v.size() == 1) m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].fixedLoss = std::stof(v[0]);
				v = xmlParse.GetXMLVectorValue("RF2.magTrue");
				if (v.size() == 1) m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].isMag = strcasecmp(v[0].c_str(), "Mag") == 0;
				v = xmlParse.GetXMLVectorValue("RF2.orientation");
				if (v.size() == 1) m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].orientation = std::stof(v[0]);
				v = xmlParse.GetXMLVectorValue("RF2.lightning");
				if (v.size() == 1) m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].lightningProt = (std::stoul(v[0]) != 0);
				// TODO: Use new config parameter to set the switch
				v = xmlParse.GetXMLVectorValue("RF2.switchType");
				if (v.size() == 1)
				{
					m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].switchType = CSwitch::StrToEnum(v[0]);
					m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].sw.reset(CSwitch::Create(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].switchType));
				}
				//No switch on RF2 if not specified

				if(HasDualAux1())
				{
					//Setup C_SLAVE_RF2 port parameters.
					SetSlaveRf2Para();
				}

				v = xmlParse.GetXMLVectorValue("RF3.freqHigh");
				if (v.size() == 1) m_mutableConfig.hfConfig.antCable.freqHigh = Units::Frequency(std::stod(v[0]));
				v = xmlParse.GetXMLVectorValue("RF3.antType");
				if (v.size() == 1)
				{
					m_mutableConfig.hfConfig.antCable.antType = CAntenna::StrToEnum(v[0]);
					m_mutableConfig.hfConfig.antCable.antenna.reset(CAntenna::Create(m_mutableConfig.hfConfig.antCable.antType));
				}
				m_mutableConfig.hfConfig.antCable.antName = CAntenna::GetFullName(m_mutableConfig.hfConfig.antCable.antenna.get());
				v = xmlParse.GetXMLVectorValue("RF3.cableType");
				if (v.size() == 1) m_mutableConfig.hfConfig.antCable.cableType = SMutableConfig::CableStrToEnum(v[0]);
				v = xmlParse.GetXMLVectorValue("RF3.cableLength");
				if (v.size() == 1) m_mutableConfig.hfConfig.antCable.cableLength = std::stof(v[0]);
				v = xmlParse.GetXMLVectorValue("RF3.fixedLoss");
				if (v.size() == 1) m_mutableConfig.hfConfig.antCable.fixedLoss = std::stof(v[0]);
				v = xmlParse.GetXMLVectorValue("RF3.magTrue");
				if (v.size() == 1) m_mutableConfig.hfConfig.antCable.isMag = strcasecmp(v[0].c_str(), "Mag") == 0;
				v = xmlParse.GetXMLVectorValue("RF3.orientation");
				if (v.size() == 1) m_mutableConfig.hfConfig.antCable.orientation = std::stof(v[0]);
				v = xmlParse.GetXMLVectorValue("RF3.lightning");
				if (v.size() == 1) m_mutableConfig.hfConfig.antCable.lightningProt = (std::stoul(v[0]) != 0);
				// TODO: Use new config parameter to set the switch
				v = xmlParse.GetXMLVectorValue("RF3.switchType");
				if (v.size() == 1)
				{
					m_mutableConfig.hfConfig.antCable.switchType = CSwitch::StrToEnum(v[0]);
					m_mutableConfig.hfConfig.antCable.sw.reset(CSwitch::Create(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].switchType));
				}
				else if (CAntenna::IsDfAntenna(m_mutableConfig.hfConfig.antCable.antenna.get()))
				{
					m_mutableConfig.hfConfig.antCable.switchType = CSwitch::ESwitchType::SW_7236_REF;
					m_mutableConfig.hfConfig.antCable.sw.reset(CSwitch::Create(m_mutableConfig.hfConfig.antCable.switchType));
				}

				// Misc
				v = xmlParse.GetXMLVectorValue("Misc.databasePurgeAge");
				if (v.size() == 1) m_mutableConfig.miscConfig.databasePurgeAge = std::stod(v[0]);
				v = xmlParse.GetXMLVectorValue("Misc.langIdx");
				if (v.size() == 1) m_mutableConfig.miscConfig.langIdx = std::stoul(v[0]);
				v = xmlParse.GetXMLVectorValue("Misc.partNum");
				if (v.size() == 1) strncpy(m_mutableConfig.miscConfig.partNum, v[0].c_str(), sizeof(m_mutableConfig.miscConfig.partNum));
				v = xmlParse.GetXMLVectorValue("Misc.rev");
				if (v.size() == 1) strncpy(m_mutableConfig.miscConfig.rev, v[0].c_str(), sizeof(m_mutableConfig.miscConfig.rev));
				v = xmlParse.GetXMLVectorValue("Misc.serialNumber");
				if (v.size() == 1) strncpy(m_mutableConfig.miscConfig.serialNumber, v[0].c_str(), sizeof(m_mutableConfig.miscConfig.serialNumber));
				v = xmlParse.GetXMLVectorValue("Misc.customer");
				if (v.size() == 1) strncpy(m_mutableConfig.miscConfig.customer, v[0].c_str(), sizeof(m_mutableConfig.miscConfig.customer));
				v = xmlParse.GetXMLVectorValue("Misc.algoType");
				if (v.size() == 1) m_mutableConfig.miscConfig.algoType = static_cast<EAlgo>(std::stoul(v[0]));
				v = xmlParse.GetXMLVectorValue("Misc.enhancedDynRng");
				if (v.size() == 1) m_mutableConfig.miscConfig.enhancedDynRng = (std::stoul(v[0]) != 0);
				v = xmlParse.GetXMLVectorValue("Misc.disableWatchdog");
				if (v.size() == 1) m_mutableConfig.miscConfig.disableWatchdog = (std::stoul(v[0]) != 0);
				v = xmlParse.GetXMLVectorValue("Compass.compassBaud");
				if (v.size() == 1) m_mutableConfig.miscConfig.compassBaud = std::stoul(v[0]);
				v = xmlParse.GetXMLVectorValue("Compass.compassPort");
				if (v.size() == 1) strncpy(m_mutableConfig.miscConfig.compassPort, (std::string("/dev/") + v[0]).c_str(), sizeof(m_mutableConfig.miscConfig.compassPort));
				v = xmlParse.GetXMLVectorValue("Compass.compassType");
				if (v.size() == 1) m_mutableConfig.miscConfig.compassType = CompassStrToEnum(v[0]);
				v = xmlParse.GetXMLVectorValue("GPS.gpsAvailable");
				if (v.size() == 1) m_mutableConfig.miscConfig.gpsAvailable = EGpsAvailable(std::stoul(v[0]));
				v = xmlParse.GetXMLVectorValue("GPS.fixedLatitude");
				if (v.size() == 1) m_mutableConfig.miscConfig.fixedLatitude = std::stod(v[0]);
				v = xmlParse.GetXMLVectorValue("GPS.fixedLongitude");
				if (v.size() == 1) m_mutableConfig.miscConfig.fixedLongitude = std::stod(v[0]);
				v = xmlParse.GetXMLVectorValue("GPS.fixedAltitude");
				if (v.size() == 1) m_mutableConfig.miscConfig.fixedAltitude = std::stod(v[0]);
				v = xmlParse.GetXMLVectorValue("Misc.allowShutdown");
				if (v.size() == 1) m_mutableConfig.miscConfig.allowShutdown = std::stoi(v[0]);
				v = xmlParse.GetXMLVectorValue("Misc.mastDownInverted");
				if (v.size() == 1) m_mutableConfig.miscConfig.mastDownInverted = (std::stoul(v[0]) != 0);

				// HF
				v = xmlParse.GetXMLVectorValue("HF.rxType");
				if (v.size() == 1) m_mutableConfig.hfConfig.rxType = SMutableConfig::SHfConfig::StrToEnum(v[0]);
				v = xmlParse.GetXMLVectorValue("HF.antPolicy");
				if (v.size() == 1) m_mutableConfig.hfConfig.antPolicy = AntPolicyStrToEnum(v[0]);

				// VUSHF
				v = xmlParse.GetXMLVectorValue("VUSHF.rxType");
				if (v.size() == 1) m_mutableConfig.vushfConfig.rxType = SMutableConfig::SVushfConfig::StrToEnum(v[0]);
				v = xmlParse.GetXMLVectorValue("VUSHF.antPolicy");
				if (v.size() == 1) m_mutableConfig.vushfConfig.antPolicy = AntPolicyStrToEnum(v[0]);
				LoadMutableConfigShfExtVushf(xmlParse);
				break;

			default:
				break;
			}
		}
		catch(std::invalid_argument&)
		{
			// Ignore bad lines
		}
	}
	else
	{
		TRACE("Unable to parse CSMSConfig.xml\n");
	}
	return;
}

//////////////////////////////////////////////////////////////////////
/// Load SHF Extension Parameters from Mutable Config XML file
///
///@param[in] xmlParse has the XML parser object.
///
void CConfig::LoadMutableConfigShfExtVushf(XMLProcessor & xmlParse)
{
	XMLProcessor::VectStr v;
  	v = xmlParse.GetXMLVectorValue("VUSHF.ShfExt.analyzerType");
  	//if (v.size() == 1) m_mutableConfig.vushfConfig.shfExt.shfExtType = SMutableConfig::SShfExt::StrToEnum(v[0]);

  	///@TODO: Uncomment above and take this out.
  	if(v.size() == 1)
  	{
  		TRACE(v[0].c_str());
  		m_mutableConfig.vushfConfig.shfExt.shfExtType = SMutableConfig::SShfExt::StrToEnum(v[0]);
  	}
  	else
  	{
  		TRACE("v.size() = %u\n", v.size());
  	}

  	v = xmlParse.GetXMLVectorValue("VUSHF.ShfExt.analyzerIP");
  	if (v.size() == 1) m_mutableConfig.vushfConfig.shfExt.shfExtIP = v[0];
  	v = xmlParse.GetXMLVectorValue("VUSHF.ShfExt.cableType");
  	if (v.size() == 1) m_mutableConfig.vushfConfig.shfExt.shfExtCableType = SMutableConfig::CableStrToEnum(v[0]);
  	v = xmlParse.GetXMLVectorValue("VUSHF.ShfExt.cableLength");
  	if (v.size() == 1) m_mutableConfig.vushfConfig.shfExt.shfExtCableLength = std::stof(v[0]);

}

//////////////////////////////////////////////////////////////////////
//
// Load Processor nodes info from config file
//
void CConfig::LoadProcessorNodes(void)
{
	// Set default values
	m_processorNodes.nodeId = 0;
	m_processorNodes.masterIP = "";
	XMLProcessor xmlParse;
	if (xmlParse.ReadLoadXML(m_filePath + s_configFileName))
	{
		try
		{
			XMLProcessor::VectStr v1;
			XMLProcessor::VectStr v2;
			v1 = xmlParse.GetXMLVectorValue("Processors.masterIP");
			if (v1.size() == 1) m_processorNodes.masterIP = v1[0];
			v1 = xmlParse.GetXMLVectorValue("Processors.nodeId");
			if (v1.size() == 1) m_processorNodes.nodeId = std::stoul(v1[0]);
			v1 = xmlParse.GetXMLVectorValue("Processors.node.id");
			v2 = xmlParse.GetXMLVectorValue("Processors.node.ipAddress");
			if (v1.size() != v2.size())
			{
				CLog::Log(CLog::ERRORS, "Processors.node.id and ipAddress have different sizes");
			}
			else
			{
				m_processorNodes.nodes.resize(v1.size());
				for (size_t i = 0; i < v1.size(); ++i)
				{
					SProcessorNodes::SNode node;
					m_processorNodes.nodes[i] = SProcessorNodes::SNode(std::stoul(v1[i]), v2[i]);
				}
			}


		}
		catch(std::invalid_argument&)
		{
			// Ignore bad lines
		}
	}
	else
	{
		TRACE("Unable to open CSMSConfig.xml\n");
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Load the list of restricted frequency ranges
//
void CConfig::LoadRestrictedFreqParams(void)
{
	if (!LoadFreqPairParams(m_restrictedFreq, m_filePath + "csmsGeneral.xml", "restrictedHz", false))
	{
		throw std::runtime_error("The restrictedHz data is invalid.");
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Load system type info from config file
//
void CConfig::LoadSystemType(void)
{
	// Set default values
	m_mutableConfig.miscConfig.systemtype = ESystemType::TDOASYSTEM;

	XMLProcessor xmlParse;
	if (xmlParse.ReadLoadXML(m_filePath + s_configFileName))
	{
		try
		{
			XMLProcessor::VectStr v1;
			v1 = xmlParse.GetXMLVectorValue("Misc.systemType");
			if (v1.size() == 1)
			{
				m_mutableConfig.miscConfig.systemtype = (ESystemType) std::stoul(v1[0]);
				m_ddrtype = (m_mutableConfig.miscConfig.systemtype == ESystemType::DDRSYSTEM);
			}


		}
		catch(std::invalid_argument&)
		{
			// Ignore bad lines
		}
	}
	else
	{
		TRACE("Unable to open CSMSConfig.xml\n");
	}

	///@note: 709 with multiple antenna always has Horizontal antenna as 1 of its input.
	m_sysHasHorAnt = (m_mutableConfig.miscConfig.systemtype == ESystemType::TDOA_MULTIPLE_ANT);
	return;
}

//////////////////////////////////////////////////////////////////////
//
// Load processing parameters from a .csv file
//
void CConfig::LoadProcParams(void)
{
	std::ifstream file((m_filePath + "/ProcParamsV2.csv").c_str());

	if(!file)
	{
		throw std::system_error(errno, std::generic_category(), "ProcParamsV2.csv");
	}

	std::string line;

	m_vushfDemodBws.clear();
	m_vushfDemodBws.resize(int(C3230::EDemodMode::NUM_MODES));
	m_hfDemodBws.clear();
	m_hfDemodBws.resize(int(C3230::EDemodMode::NUM_MODES));

	while(!std::getline(file, line).eof())
	{
		try
		{
			std::istringstream lineStream(line);
			std::string token;
			STaskParams task;
			std::getline(lineStream, token, ',');
			task.hf = (std::stoi(token) != 0);
			std::getline(lineStream, token, ',');
			task.mode = static_cast<EMode>(std::stoi(token));
			std::getline(lineStream, token, ',');
			task.bw = std::stoul(token);
			std::getline(lineStream, token, ',');
			task.rxHwBw = std::stoul(token);
			task.startFreq = 0;
			std::getline(lineStream, token, ',');
			task.stopFreq = std::stoull(token);

			SProcParams params;
			std::getline(lineStream, token, ',');
			params.rxProcBw = std::stoul(token);
			std::getline(lineStream, token, ',');
			params.sampleRate = std::stoul(token);
			std::getline(lineStream, token, ',');
			params.decimations.zifCICDecimation = std::stoul(token);
			std::getline(lineStream, token, ',');
			params.decimations.zifFIRDecimation = std::stoul(token);
			std::getline(lineStream, token, ',');
			params.decimations.upResample = std::stoul(token);
			std::getline(lineStream, token, ',');
			params.decimations.downResample = std::stoul(token);
			std::getline(lineStream, token, ',');
			params.decimations.ddcCICDecimation = std::stoul(token);
			std::getline(lineStream, token, ',');
			params.decimations.ddcFIRDecimation = std::stoul(token);
			std::getline(lineStream, token, ',');
			params.sampleSize = std::stoul(token);
			params.blockSize = params.sampleSize;		// Needed for IQ data collection
			if (params.decimations.zifCICDecimation > C3230::SDecimation::MAX_ZIF_CIC_DECIMATION)
			{
				throw std::runtime_error("invalid zifCICDecimation");
			}
			params.iqCic = 0;

			if (task.mode == EMode::DEMOD)
			{
				for (auto i = static_cast<int>(C3230::EDemodMode::FM); i <= static_cast<int>(C3230::EDemodMode::IQ); ++i)
				{
					std::getline(lineStream, token, ',');
					auto val = std::stoul(token);
					if (val != 0)
					{
						auto& v = (task.hf ? m_hfDemodBws[i] : m_vushfDemodBws[i]);
						v.bandwidths.insert(task.bw);
						if (i == static_cast<int>(C3230::EDemodMode::IQ))
						{
//							if (val > 16384)
//							{
//								printf("Changing IQ CIC value from %lu to 16384\n", val);
//								val = 16384;
//							}
							params.iqCic = val;
						}
					}
				}
			}
			m_procParams[task] = params;
//			printf("%d %u %lu %lu %lu %llu %lu %lu %lu %lu %lu %lu %lu %lu %lu\n",
//				task.hf, task.mode, task.bw.Hz<unsigned long>(), task.rxBw.Hz<unsigned long>(),
//				task.startFreq.Hz<unsigned long>(), task.stopFreq.Hz<unsigned long long>(),
//				params.sampleRate.Hz<unsigned long>(), params.decimations.zifCICDecimation, params.decimations.zifFIRDecimation,
//				params.decimations.upResample, params.decimations.downResample, params.decimations.ddcCICDecimation,
//				params.decimations.ddcFIRDecimation, params.sampleSize, params.iqCic);
		}
		catch(std::invalid_argument&)
		{
			// Ignore bad lines
		}
	}

//	static const char* modeNames[] = {"OFF","FM ","AM ","CW ","USB","LSB"};
//	for (auto hf = 0; hf < 2; ++hf)
//	{
//		for (auto i = static_cast<int>(SDemodBws::FM); i < static_cast<int>(SDemodBws::NUM_MODES); ++i)
//		{
//			printf("%s %s ", (hf ? "HF  " : "VUSHF"), modeNames[i]);
//			auto& v = (hf ? m_hfDemodBws[i] : m_vushfDemodBws[i]);
//			for (auto& f : v.bandwidths)
//			{
//				printf("%10.1f ",f.Hz<double>());
//			}
//			printf("\n");
//		}
//	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Load tdoa parameters from a .txt file
//
void CConfig::LoadTdoaParams(void)
{
	m_smsTdoaMap.clear();
	m_ddcExtraSamples = 256;		// Default value (for earlier files)
	m_directExtraClocks = 12;		// Default value
	m_tdoaStart = { 0, 0 };
	unsigned long version = 0;

	// Read in data and store them - format is bandwidth(Hz),correction in 3230 clocks
	std::ifstream file((m_filePath + "/sms_tdoa.txt").c_str());
	if (!file)
	{
		TRACE("No sms_tdoa.txt file found\n");
		return;
	}

	std::string line;
	while(!std::getline(file, line).eof())
	{
		unsigned long bw;
		unsigned long csmsClocks;
		unsigned long smsClocks;
		unsigned long extra;
		unsigned long v;
		unsigned long v1;
		if (sscanf(line.c_str(), "version %lu", &v) == 1)
		{
			version = v;
		}
		if (sscanf(line.c_str(), "ddc %lu", &v) == 1)
		{
			m_ddcExtraSamples = v;
		}
		else if (sscanf(line.c_str(), "direct %lu", &v) == 1)
		{
			m_directExtraClocks = v;
		}
		else if (sscanf(line.c_str(), "start %lu,%lu", &v, &v1) == 2)
		{
			m_tdoaStart = { v, v1 };
		}
		else if (version == 0 && sscanf(line.c_str(), "%lu", &extra) == 1)
		{
			m_ddcExtraSamples = extra;
		}
		else if (sscanf(line.c_str(), "%lu,%lu,%lu", &bw, &csmsClocks, &smsClocks) == 3)
		{
			m_smsTdoaMap[bw] = { csmsClocks, smsClocks };
		}
	}

	if (m_ddcExtraSamples != C3230::DDC_EXTRA_SAMPLES_REQUIRED)
	{
		CLog::Log(CLog::ERRORS, "sms_tdoa.txt incompatible with digitizer implementation!!");
	}
	CLog::Log(CLog::INFORMATION, "sms_tdoa.txt version = %lu, ddc_extraSamples = %lu, directExtraClocks = %lu, tdoaStart = (%lu,%lu)",
		version, m_ddcExtraSamples, m_directExtraClocks, m_tdoaStart.csms, m_tdoaStart.sms);

//	for (auto& it : m_smsTdoaMap)
//	{
//		TRACE("smstdoa: %lu => %lu,%lu\n", it.first.Hz<unsigned long>(), it.second.csms, it.second.sms);
//	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Check if the antenna is a mobile type
//
bool CConfig::HasMobileAntenna(bool hf) const
{
	if (hf)
	{
		return CAntenna::IsMobileAntenna(m_mutableConfig.hfConfig.antCable.antenna.get());
	}
	else
	{
		return CAntenna::IsMobileAntenna(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna.get());
	}
}


//////////////////////////////////////////////////////////////////////
//
// Check if the frequency specified is within a restricted frequency range.
//
bool CConfig::InRestrictedFreq(Units::Frequency freq) const
{
	if (m_restrictedFreq.empty()) return false;

	for (size_t i = 0; i < m_restrictedFreq.size(); ++i)
	{
		if (freq < m_restrictedFreq[i].first)
		{
			break;
		}
		if (freq >= m_restrictedFreq[i].first && freq <= m_restrictedFreq[i].second)
		{
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Check if the frequency/bandwidth specified is within a restricted frequency range.
//
bool CConfig::InRestrictedFreqBw(Units::Frequency freq, Units::Frequency bw) const
{
	return InRestrictedFreqRange(freq - bw / 2, freq + bw / 2);
}


//////////////////////////////////////////////////////////////////////
//
// Check if the frequency range specified is within a restricted frequency range.
//
bool CConfig::InRestrictedFreqRange(Units::Frequency lowFreq, Units::Frequency highFreq) const
{
	if (m_restrictedFreq.empty()) return false;

	for (size_t i = 0; i < m_restrictedFreq.size(); ++i)
	{
		if (highFreq < m_restrictedFreq[i].first)
		{
			break;
		}
		if (lowFreq <= m_restrictedFreq[i].second && highFreq >= m_restrictedFreq[i].first)
		{
			return true;
		}
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Verify whether the frequency specified is within the supported range.
//
bool CConfig::IsFreqInRange(Units::Frequency freq, bool isHorizontal, bool isDf, _In_ SEquipCtrlMsg::EAnt ant)
{
	if (IsTermAnt(ant))
	{
		// Check against absolute min/max
		return (freq >= m_minFreq && freq <= m_maxFreq);
	}

	bool inRange = false;
	if (isDf)
	{
		if (isHorizontal)
		{
			inRange = (freq >= m_vushfDfHLowFreq && freq <= m_vushfDfHHighFreq);
		}
		else
		{
			inRange = (freq >= m_vushfDfVLowFreq && freq <= m_vushfDfVHighFreq);
		}
	}
	else
	{
		// Not DF. If policy is CLIENT_NAME, valid also depends on which ant is specified.
		bool ant2Check = true;
		if (m_mutableConfig.vushfConfig.antPolicy == CConfig::EAntPolicy::CLIENT_NAME)
		{
			// If the antenna is the slave antenna, can't check frequency range.
			if (ant != SEquipCtrlMsg::SMPL_RF2)
			{
				if (ant == SEquipCtrlMsg::ANT2)
				{
					ant2Check = (m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antenna &&
							freq <= m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].freqHigh);

					if(ant2Check)
					{
						if(HasShfExt())
						{
						      //CLIENT_NAME(User Select) Policy, RF2 frequency must be >= RF1.
						      inRange = (freq >= m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].freqHigh);

						}
						else
						{
							if(HasDualAux1())
							{
								inRange = (freq >= CAntenna::GetVLowFreq(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antenna.get()));
							}
							else
							{
								inRange = (freq >= m_vushfMetricVLowFreq);
							}
						}

						//Range has been checked/specified.
						ant2Check = false;
					}
				}
				else
				{
					ant2Check = (m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna &&
							freq <= m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].freqHigh);
#ifdef CSMS_DEBUG
					if(!ant2Check)
					{
						TRACE("CConfig::IsFreqInRange ant2Check = false antenna = %d freqHigh =%lf, freq =%lf\n",
								(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna ? 1 : 0),
								m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].freqHigh.Hz<double>(),
								freq.Hz<double>());
					}
#endif
				}
			}
			else if(HasDualAux1())
			{
				//Only Dual Aux1 can have SMPL_RF2.
				auto ant = m_mutableConfig.vushfConfig.antCable[ERfPort::C_SLAVE_RF2].antenna.get();
				inRange = (freq >= CAntenna::GetVLowFreq(ant) && freq <= CAntenna::GetVHighFreq(ant));

				//Range has been checked/specified.
				ant2Check = false;
			}
		}
		else if(ant == SEquipCtrlMsg::ANT1 || ant == SEquipCtrlMsg::ANT2)
		{
		    inRange = (freq >= m_vushfMetricVLowFreq && freq <= m_vushfMetricVHighFreq);
		    //Range has been checked/specified.
		    ant2Check = false;
#ifdef CSMS_DEBUG
			if(!inRange)
			{
				TRACE("CConfig::IsFreqInRange inRange = false m_vushfMetricVLowFreq = %lf freqHigh =%lf, freq =%lf\n",
						m_vushfMetricVLowFreq.Hz<double>(), m_vushfMetricVHighFreq.Hz<double>(), freq.Hz<double>());
		}
#endif
		}

		if (isHorizontal)
		{
			inRange = ant2Check && (freq >= m_rf1VushfHLowFreq && freq <= m_rf1VushfHHighFreq);
#ifdef CSMS_DEBUG
			if(!inRange)
			{
				TRACE("CConfig::IsFreqInRange inRange = false m_rf1VushfHLowFreq = %lf m_rf1VushfHHighFreq =%lf, freq =%lf\n",
						m_rf1VushfHLowFreq.Hz<double>(), m_rf1VushfHHighFreq.Hz<double>(), freq.Hz<double>());
			}
#endif
		}
		else
		{
			if (ant2Check)
			{
				// If vertical pol is specified, then its use depends on frequency (e.g., 649)
				auto a = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna;
				if (a && CAntenna::IsValidDfElement(a.get(), ant))
				{
					auto pAnt = a.get();
				    ant2Check = (ant >= SEquipCtrlMsg::DF_ANT_1V &&
				    		ant <= CAntenna::GetMAnt(pAnt, isHorizontal, freq));

				    if(!ant2Check)
				    {
						//If !DF && UHF antenna mapping to 7234 switch input has
						// skip element, then always used the number of High band antennas.
						auto forceHigh = (!isDf && CAntenna::HasLowbandSkippedElement(pAnt));
						if(forceHigh)
						{
						    ant2Check = (ant >= SEquipCtrlMsg::DF_ANT_1V &&
						    		ant <= CAntenna::GetMAnt(pAnt, isHorizontal, freq, forceHigh));
						}
				    }
				}
				inRange = ant2Check && (freq >= m_rf1VushfVLowFreq && freq <= m_rf1VushfVHighFreq);
#ifdef CSMS_DEBUG
				if(!inRange)
				{
					TRACE("CConfig::IsFreqInRange inRange = false m_rf1VushfVLowFreq = %lf m_rf1VushfVHighFreq =%lf, freq =%lf\n",
							m_rf1VushfVLowFreq.Hz<double>(), m_rf1VushfVHighFreq.Hz<double>(), freq.Hz<double>());
				}
#endif
			}
		}
	}

	return inRange;
}

//////////////////////////////////////////////////////////////////////
/// Is SHF Extension being used?
///
///@param[in,out] ant User selected antenna.
///		      For Auto antenna policy, if SHF Extension ant value can be changed from
///			ANT1, ANT1H to ANT2.
///
///@param[in] freq Tuned frequency
///
///@retval true: SHF Extension is used.
///	   false: SHF Extension is not used.
///
bool CConfig::IsShfExtUsed(_In_ SEquipCtrlMsg::EAnt & ant, _In_ Units::Frequency freq) const
{
	bool shfUsed = false;

	if(HasShfExt())
	{
		if( ((ant == SEquipCtrlMsg::EAnt::ANT1) || (ant == SEquipCtrlMsg::EAnt::ANT1H)) &&
		    (m_mutableConfig.vushfConfig.antPolicy == EAntPolicy::AUTO_FREQ))
		{
		    if(IsInShfExtFreqRange(freq))
		    {
			ant = SEquipCtrlMsg::EAnt::ANT2;
		    }
		}

		if(ant == SEquipCtrlMsg::EAnt::ANT2)
		{
		    shfUsed = true;
		}
	}

	return(shfUsed);
}

//////////////////////////////////////////////////////////////////////
//
// Check if this is a slave antenna ID
//
bool CConfig::IsSlaveAntenna(SEquipCtrlMsg::EAnt ant, bool hf) const
{
	if (IsProcessorDfMaster())
	{
		if (ant == SEquipCtrlMsg::EAnt::SMPL_RF2 || ant == SEquipCtrlMsg::EAnt::SMPL_TERM_1)
		{
			return true;
		}
		return IsValidDfElement(ant, hf);
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Check if this is one of the termination antenna selections
//
bool CConfig::IsTermAnt(SEquipCtrlMsg::EAnt ant)
{
	return (ant == SEquipCtrlMsg::EAnt::REF_TERM || ant == SEquipCtrlMsg::EAnt::SMPL_TERM_1);
}

//////////////////////////////////////////////////////////////////////
//
// Check valid antenna ID
//
bool CConfig::IsValidAntenna(SEquipCtrlMsg::EAnt ant, bool hf, bool noHorizChk) const
{
	if (ant == SEquipCtrlMsg::INVALID_ANT || IsTermAnt(ant))
	{
		// Always succeed
		return true;
	}
	// First, deal with reference antenna selection
	if (hf)
	{
		// HF
		if (m_mutableConfig.hfConfig.antCable.antenna && m_mutableConfig.hfConfig.antCable.antType != CAntenna::EAntennaType::NO_ANTENNA)
		{
			if (ant == SEquipCtrlMsg::ANT1)
			{
				return true;
			}
		}
	}
	else
	{
		auto& a0 = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna;
		auto& a1 = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].antenna;
		auto dontChkHor = (m_sysHasHorAnt || noHorizChk);
		if (a0 || a1)
		{
			// First, deal with the reference antenna
			if (m_mutableConfig.vushfConfig.antPolicy == CConfig::EAntPolicy::AUTO_FREQ)
			{
				if (ant == SEquipCtrlMsg::ANT1)
				{
					return true;
				}

				if ((ant == SEquipCtrlMsg::ANT1H) &&
						(a1 && CAntenna::HasHorizontalPolarization(a1.get())))
				{
					return true;
				}
				else if ((ant == SEquipCtrlMsg::ANT1H) && (dontChkHor ||
					(a0 && CAntenna::HasHorizontalPolarization(a0.get()))))
				{
					return true;
				}
			}
			else if (m_mutableConfig.vushfConfig.antPolicy == CConfig::EAntPolicy::CLIENT_NAME)
			{
				if ((ant == SEquipCtrlMsg::ANT1 && a0) ||
					(ant == SEquipCtrlMsg::ANT2 && a1))
				{
					return true;
				}
				else if ((ant == SEquipCtrlMsg::ANT1H) && (dontChkHor ||
					 (a0 && CAntenna::HasHorizontalPolarization(a0.get()))))
				{
					return true;
				}
			}
		}
	}
	// If we're here, need to check for valid slave element - NOTE: not checking frequency here, just existence of element
	if (IsSlaveAntenna(ant, hf))
	{
		return true;
	}

	// Bad antenna
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Check valid DF antenna ID
//
bool CConfig::IsValidDfElement(SEquipCtrlMsg::EAnt ant, bool hf) const
{
	if (IsProcessorDfMaster())
	{
		auto& a = (hf ? m_mutableConfig.hfConfig.antCable.antenna : m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna);
		if (a && CAntenna::IsValidDfElement(a.get(), ant))
		{
			return true;
		}
	}
	return false;
}

//////////////////////////////////////////////////////////////////////
//
// Read data from the i2c eeprom
//
void CConfig::ReadEeprom(SEeprom& eeprom)
{
	memset(&eeprom, 0, sizeof(eeprom));
	auto fd = open(EEPROM_FILENAME, O_RDWR);
	if (fd == -1)
	{
		CLog::Log(CLog::ERRORS, "Unable to access eeprom: %s %s", EEPROM_FILENAME, strerror(errno));
		return;
	}
	auto numRead = read(fd, &eeprom, sizeof(eeprom));

	if (numRead == 0)
	{
		CLog::Log(CLog::ERRORS, "End-of-file reading eeprom: %s", EEPROM_FILENAME);
	}
	else if (numRead == -1)
	{
		CLog::Log(CLog::ERRORS, "Error reading eeprom: %s %s", EEPROM_FILENAME, strerror(errno));
	}
	else if (numRead != sizeof(eeprom))
	{
		CLog::Log(CLog::ERRORS, "Incorrect number of bytes reading eeprom: %s %d != %d", EEPROM_FILENAME, numRead, sizeof(eeprom));
	}

	close(fd);
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Reset the frequency limits
//
void CConfig::ResetLowFreq(const char* str, Units::Frequency& current, Units::Frequency value)
{
	if (current == 0 || current < value)
	{
		printf("Changing %s low freq %f to %f\n", str, current.Hz<double>(), value.Hz<double>());
		current = value;
	}
	else
	{
		printf("Leaving %s low freq at %f\n", str, current.Hz<double>());
	}
	return;
}

void CConfig::ResetHighFreq(const char* str, Units::Frequency& current, Units::Frequency value)
{
	if (current == 0 || current > value)
	{
		printf("Changing %s high freq %f to %f\n", str, current.Hz<double>(), value.Hz<double>());
		current = value;
	}
	else
	{
		printf("Leaving %s high freq at %f\n", str, current.Hz<double>());
	}
	return;
}

void CConfig::SetAbsoluteFreqs(Units::Frequency low, Units::Frequency high)
{
	printf("Setting absolute low-high freq %f - %f\n", low.Hz<double>(), high.Hz<double>());
	m_minFreq = low;
	m_maxFreq = high;
	return;
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Associate the antenna specified to the requested RF port, i.e.: The
///  specified antenna is connected to the requested RF port.
///</summary>
///
/// <param name="rfPort">
///@param[in] Has the RF port number.
///		0: RF1. 1: RF2
/// </param>
///
/// <param name="ant">
///@param[in] Has antenna value that is connected to the RF port.
/// </param>
///
void CConfig::SetAntFreqLimits(SEquipCtrlMsg::EAnt ant)
{
	switch(ant)
	{
		case SEquipCtrlMsg::EAnt::ANT1_LPA:
			m_rf1VushfHLowFreq = m_rf1VushfVLowFreq = LPA_LOW_FREQ;
			m_rf1VushfHHighFreq = m_rf1VushfVHighFreq = LPA_HIGH_FREQ;
			break;

		case SEquipCtrlMsg::EAnt::ANT1_647H_MON:
			m_rf1VushfHLowFreq = HORIZONTAL_647_MIN_FREQ;
			m_rf1VushfHHighFreq = HORIZONTAL_647_MAX_FREQ;
			break;

		case SEquipCtrlMsg::EAnt::ANT1_647V_MON:
			m_rf1VushfVLowFreq = VERTICAL_647_MIN_FREQ;
			m_rf1VushfVHighFreq = VERTICAL_647_MAX_FREQ;
			break;

		default:
			break;
	}

	return;
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Associate the antenna specified to the requested RF port, i.e.: The
///  specified antenna is connected to the requested RF port.
///</summary>
///
/// <param name="rfPort">
///@param[in] Has the RF port number.
///		0: RF1. 1: RF2
/// </param>
///
/// <param name="ant">
///@param[in] Has antenna value that is connected to the RF port.
/// </param>
///
void CConfig::SetNewAntType(ERfPort rfPort, SEquipCtrlMsg::EAnt ant)
{
	ASSERT(rfPort <= 1);
	CAntenna::EAntennaType antType;
	auto idx = GetAntType(ant, antType);

	if(idx < SMSMSG_ANT_MAP_CNT)
	{
		m_mutableConfig.vushfConfig.antCable[rfPort].antType = antType;
		m_mutableConfig.vushfConfig.antCable[rfPort].antenna = SMSMSG_ANT_MAP[idx].antenna;
		SetAntFreqLimits(ant);
	}

	return;
}

void CConfig::SetHfMetricFreqs(Units::Frequency low, Units::Frequency high)
{
	if(HasHfAnt())
	{
		printf("Setting HfMetrics low-high freq %f - %f\n", low.Hz<double>(), high.Hz<double>());
		m_hfMetricVLowFreqUnits = low;
		m_hfMetricVHighFreqUnits = high;
	}

	return;
}

void CConfig::SetVushfMetricFreqs(Units::Frequency low, Units::Frequency high)
{
	bool usedShfExtHighFreq = HasShfExt();

	if(  usedShfExtHighFreq &&
	    (high < s_shfExtensionVertRfFrequencyLimits.second))
	{
		high = s_shfExtensionVertRfFrequencyLimits.second;
	}

  	auto hasAntSelection = IsProcWithAntSelectionSwitch();

  	if(hasAntSelection)
  	{
  		printf("Setting VushfMetrics H low-high freq %f - %f\n",
  			       HORIZONTAL_647_MIN_FREQ.Hz<double>(), HORIZONTAL_647_MAX_FREQ.Hz<double>());
  		m_rf1VushfHLowFreq = m_vushfMetricHLowFreq = HORIZONTAL_647_MIN_FREQ;
  		m_rf1VushfHHighFreq = HORIZONTAL_647_MAX_FREQ;
  		m_vushfMetricHHighFreq = (usedShfExtHighFreq ? high : HORIZONTAL_647_MAX_FREQ);
  		printf("Setting VushfMetrics V low-high freq %f - %f\n",
  		     VERTICAL_647_MIN_FREQ.Hz<double>(), VERTICAL_647_MAX_FREQ.Hz<double>());
  		m_rf1VushfVLowFreq = m_vushfMetricVLowFreq = VERTICAL_647_MIN_FREQ;
  		m_rf1VushfVHighFreq = VERTICAL_647_MAX_FREQ;
  		m_vushfMetricVHighFreq = (usedShfExtHighFreq ? high : VERTICAL_647_MAX_FREQ);
  	}
  	else if (CAntenna::HasHorizontalPolarization(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].antenna.get()))
  	{
  		printf("Setting VushfMetrics H & V low-high freq %f - %f\n", low.Hz<double>(), high.Hz<double>());
  		m_rf1VushfVLowFreq = m_vushfMetricVLowFreq = m_rf1VushfHLowFreq = m_vushfMetricHLowFreq = low;
  		m_rf1VushfVHighFreq = m_vushfMetricVHighFreq = m_rf1VushfHHighFreq = m_vushfMetricHHighFreq = high;
  	}
  	else
  	{
  		printf("Setting VushfMetrics V low-high freq %f - %f\n", low.Hz<double>(), high.Hz<double>());
  		m_rf1VushfVLowFreq = m_vushfMetricVLowFreq = low;
  		m_rf1VushfVHighFreq = m_vushfMetricVHighFreq = high;
  	}
  	return;
}


//////////////////////////////////////////////////////////////////////
//
// Set the mutable config data and save it to config file
//
void CConfig::SaveMutableConfig(const SMutableConfig& mutableConfig)
{
	if (&m_mutableConfig != &mutableConfig)
	{
		m_mutableConfig = mutableConfig;
	}
	// TODO: Copy m_mutableConfig back to the csmsConfig.xml file

	return;
}


#if 0
//////////////////////////////////////////////////////////////////////
//
// Set auto-detected hardware settings
//
void CConfig::SaveNewHardwareSetting(const SMutableConfig& mutableConfig)
{
	SaveMutableConfig(mutableConfig);

	// Reload antenna specific parameters
	switch(m_mutableConfig.vushfConfig.rxType)
	{
	case SMutableConfig::SVushfConfig::NARROW_2630:
		m_vushfMetricVLowFreq = Units::Frequency(C2630::m_configData.minVushfFreqMHz * 1000000.0);
		m_vushfMetricVHighFreq = Units::Frequency(C2630::m_configData.maxVushfFreqMHz * 1000000.0);
		break;

	case SMutableConfig::SVushfConfig::WIDE_2630:
		m_vushfMetricVLowFreq = Units::Frequency(C2630::m_configData.minVushfFreqMHz * 1000000.0);
		m_vushfMetricVHighFreq = Units::Frequency(C2630::m_configData.maxVushfFreqMHz * 1000000.0);
		break;


	case SMutableConfig::SVushfConfig::NARROW_DUAL_2630:
	case SMutableConfig::SVushfConfig::WIDE_DUAL_2630:
	default:
		break;
	}
	m_hfDfVLowFreqUnits = std::numeric_limits<Units::Frequency>::max();

	return;
}
#endif


//////////////////////////////////////////////////////////////////////
//
// Set Antenna specific parameters
//
void CConfig::SetAntennaSpecificParameters(void)
{
//	m_hfDfVLowFreqUnits = Units::Frequency(285000UL);

//	if (Is632Lf())
//	{
//		m_hfDfVLowFreqUnits = Units::Frequency(9000UL);
//	}

	if(HasShfExt())
	{
		Units::Frequency maxFreq = GetAnalyzerMaxFreq(GetShfExtSetting());

		//Take the samller of the specified frequency and analyzer limit.
		if(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].freqHigh < maxFreq)
		{
			maxFreq = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2].freqHigh;
		}

		s_shfExtensionHorizRfFrequencyLimits = s_shfExtensionVertRfFrequencyLimits =
	    			Units::FreqPair(m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1].freqHigh, maxFreq);
		SetVushfMetricFreqs(m_vushfMetricVLowFreq, maxFreq);
	}


	return;
}

//////////////////////////////////////////////////////////////////////
///<summary>
///Set the Fixed Configuration for this Slave unit.
///</summary>
///
///@note: These are the parameters if only this is a Slave Processor.
///
void CConfig::SetSlaveNodeFixedConfig(void)
{
	//RF port selection is decided by the Master not this slave unit.
	m_mutableConfig.hfConfig.antPolicy = EAntPolicy::CLIENT_NAME;
	m_mutableConfig.vushfConfig.antPolicy = EAntPolicy::CLIENT_NAME;
	return;
}

//////////////////////////////////////////////////////////////////////
///<summary>
///Set the C_SLAVE_RF2 port parameters when USHF_DUAL_AUX1 is selected by
/// user for RF port 2.
///</summary>
///
///@note: CUSTOM_ANT will be configured for the slave RF2.
///
void CConfig::SetSlaveRf2Para(void)
{
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_SLAVE_RF2] =
			m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2];
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_SLAVE_RF2].antType = CAntenna::EAntennaType::CUSTOM_ANT;
	//m_mutableConfig.vushfConfig.antCable[ERfPort::C_SLAVE_RF2].antenna = nullptr;
	m_mutableConfig.vushfConfig.antCable[ERfPort::C_SLAVE_RF2].antenna.reset(
			CAntenna::Create(m_mutableConfig.vushfConfig.antCable[ERfPort::C_SLAVE_RF2].antType));
	return;
}

//////////////////////////////////////////////////////////////////////
///<summary>
///Translate and verify the antenna value of Client request in
/// TCI5143_MULTIPLE_ANT system. It returns the
/// appropriate antenna that corresponds to the csms RF input.
///</summary>
///
/// <param name="ant">
///@param[in] Has the requested antenna value.
/// </param>
///
/// <param name="modAnt">
///@param[out] Has the corrected antenna value if return is true, otherwise
///				it will have the input ant value.
/// </param>
///
/// <param name="forcedHf">
///@param[out] If return is true, this will have valid value
///			   true: Forced HF.
/// </param>
///
/// <param name="forcedVuhf">
///@param[out] If return is true, this will have valid value
///			   true: Forced VUHF.
/// </param>
///
/// <returns>
///@retval: true: forcedHf & forcedVuhf have valid values.
///			false: No translation is made to the antenna value, forcedHf
///				   & forcedVuhf are not modified.
/// </returns>
///
/// <remarks>
/// @note: 1. This method will throw when error is found.
/// </remarks>
///
bool CConfig::TranslateVerify5143MultAntValue(
		SEquipCtrlMsg::EAnt ant, SEquipCtrlMsg::EAnt & modAnt, bool & forcedHf, bool & forcedVuhf) const
{
	auto isValid = false;
	modAnt = ant;

	ASSERT(IsProcWithAntSelectionSwitch());

	//Only verify custom defined antenna.
	if(ant >= SEquipCtrlMsg::EAnt::ANT1_HF && ant <= SEquipCtrlMsg::EAnt::ANT1_647H_MON)
	{
		isValid = true;
		forcedHf = (ant == SEquipCtrlMsg::EAnt::ANT1_HF ? true : false);
		forcedVuhf = !forcedHf;

		if(forcedHf)
		{
			modAnt = SEquipCtrlMsg::EAnt::ANT1;
		}
		else
		{
			if(ant == SEquipCtrlMsg::EAnt::ANT1_647H_MON)
			{
				modAnt = SEquipCtrlMsg::EAnt::ANT1H;
			}
			else if(ant >= SEquipCtrlMsg::EAnt::ANT1_LPA && ant <= SEquipCtrlMsg::EAnt::ANT1_647V_MON)
			{
				modAnt = SEquipCtrlMsg::EAnt::ANT1;
			}

		}

		//Do not check Horizontal polarization.
		if(!IsValidAntenna(modAnt, forcedHf, true))
		{
			throw ErrorCodes::INVALIDANTENNAID;
		}
	}

	return(isValid);
}

//////////////////////////////////////////////////////////////////////
//
// Update antenna params (frequency limits)
//
void CConfig::UpdateAntennaParams(void)
{
	// Initially, the various low and high metrics frequency limits are set by the radio for both pol

	auto& rf1 = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF1];
	auto& rf2 = m_mutableConfig.vushfConfig.antCable[ERfPort::C_RF2];
	auto& slaveRf2 = m_mutableConfig.vushfConfig.antCable[ERfPort::C_SLAVE_RF2];
	auto& rf3 = m_mutableConfig.hfConfig.antCable;
	auto& a1 = rf1.antenna;
	auto& a2 = rf2.antenna;
	auto& slaveAnt = slaveRf2.antenna;
	auto& a3 = rf3.antenna;

	// HF Metrics limits are based on RF3 in the config file
	ResetLowFreq("HF Metric", m_hfMetricVLowFreqUnits, CAntenna::GetVLowFreq(a3.get()));
	ResetHighFreq("HF Metric", m_hfMetricVHighFreqUnits, std::min(CAntenna::GetVHighFreq(a3.get()), rf3.freqHigh));

	// VUSHF Metrics limits are based on both RF1 and RF2 inputs in the config file
	///@TODO: Multiple antenna selection may need to check for SHF Extension.
	if ((a1 || a2) && !IsProcWithAntSelectionSwitch())
	{
		Units::Frequency low;
		Units::Frequency high;

		// H pol
		if (!a2) low = CAntenna::GetHLowFreq(a1.get());
		else if (!a1) low = CAntenna::GetHLowFreq(a2.get());
		else low = std::min(CAntenna::GetHLowFreq(a1.get()), CAntenna::GetHLowFreq(a2.get()));
		ResetLowFreq("VUSHF Metric H", m_vushfMetricHLowFreq, low);
		m_rf1VushfHLowFreq = m_vushfMetricHLowFreq;

		if (!a2) high = std::min(CAntenna::GetHHighFreq(a1.get()), rf1.freqHigh);
		else if (!a1) high = std::min(CAntenna::GetHHighFreq(a2.get()), rf2.freqHigh);
		else high = std::max(std::min(CAntenna::GetHHighFreq(a1.get()), rf1.freqHigh),
			std::min(CAntenna::GetHHighFreq(a2.get()), rf2.freqHigh));
		ResetHighFreq("VUSHF Metric H", m_vushfMetricHHighFreq, high);
		m_rf1VushfHHighFreq = m_vushfMetricHHighFreq;

		// V pol
		if (!a2) low = CAntenna::GetVLowFreq(a1.get());
		else if (!a1) low = CAntenna::GetVLowFreq(a2.get());
		else low = std::min(CAntenna::GetVLowFreq(a1.get()), CAntenna::GetVLowFreq(a2.get()));

		ResetLowFreq("VUSHF Metric V", m_vushfMetricVLowFreq, low);
		m_rf1VushfVLowFreq = m_vushfMetricVLowFreq;

		if(HasDualAux1() && m_vushfMetricVLowFreq > CAntenna::GetVLowFreq(slaveAnt.get()))
		{
			//Save the lowest value supported.
			m_vushfMetricVLowFreq = CAntenna::GetVLowFreq(slaveAnt.get());
		}

		if (!a2) high = std::min(CAntenna::GetVHighFreq(a1.get()), rf1.freqHigh);
		else if (!a1) high = std::min(CAntenna::GetVHighFreq(a2.get()), rf2.freqHigh);
		else high = std::max(std::min(CAntenna::GetVHighFreq(a1.get()), rf1.freqHigh),
			std::min(CAntenna::GetVHighFreq(a2.get()), rf2.freqHigh));
		ResetHighFreq("VUSHF Metric V", m_vushfMetricVHighFreq, high);
		m_rf1VushfVHighFreq = m_vushfMetricVHighFreq;

		if(HasDualAux1() && m_vushfMetricVHighFreq < CAntenna::GetVHighFreq(slaveAnt.get()))
		{
			//Save the highest value supported.
			m_vushfMetricVHighFreq = CAntenna::GetVHighFreq(slaveAnt.get());
		}

	}

	// HF DF
	if (CAntenna::IsDfAntenna(a3.get()) && IsProcessorDfMaster())
	{
		ResetLowFreq("HF DF", m_hfDfVLowFreqUnits, CAntenna::GetDfVLowFreq(a3.get()));
		ResetHighFreq("HF DF", m_hfDfVHighFreqUnits, std::min(CAntenna::GetDfVHighFreq(a3.get()), rf3.freqHigh));
	}

	// VUSHF DF
	if (CAntenna::IsDfAntenna(a1.get()) && IsProcessorDfMaster())
	{
		ResetLowFreq("VUSHF DF H", m_vushfDfHLowFreq, CAntenna::GetDfHLowFreq(a1.get()));
		ResetHighFreq("VUSHF DF H", m_vushfDfHHighFreq, std::min(CAntenna::GetDfHHighFreq(a1.get()), rf1.freqHigh));
		ResetLowFreq("VUSHF DF V", m_vushfDfVLowFreq, CAntenna::GetDfVLowFreq(a1.get()));
		ResetHighFreq("VUSHF DF V", m_vushfDfVHighFreq, std::min(CAntenna::GetDfVHighFreq(a1.get()), rf1.freqHigh));
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate the demod parameters
//
bool CConfig::ValidateDemod(bool hf, SEquipCtrlMsg::SRcvrCtrlCmd::EDetMode detMode, Units::Frequency demodBw) const
{
	C3230::EDemodMode mode;
	if (!ConvertDemod(detMode, mode))
		return false;

	if (mode == C3230::EDemodMode::OFF)
	{
		return true;	// OFF is always ok
	}

	auto demodBws = (hf ? m_hfDemodBws : m_vushfDemodBws);
	auto& s = demodBws[int(mode)].bandwidths;
	if (s.find(demodBw) == s.end()) return false;

	return true;
}

