/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"
#include <sys/mman.h>
#include "SharedMemory.h"


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CSharedMemory::CSharedMemory(const char* shMemName, size_t stateSize) :
	m_master(false),
	m_shmDesc(-1),
	m_shMem(nullptr),
	m_shmSize(0),
	m_shMemName(shMemName)
{
	// Define value of master state based on executable file name (could be better)
	auto iAmMaster = EMaster::CSMSD_MASTER;
	auto otherIsMaster = EMaster::VCP_MASTER;

	char buffer[PATH_MAX + 1];
	auto len = readlink("/proc/self/exe", buffer, sizeof(buffer) - 1);
	if (len != -1)
	{
		std::string exeName = std::string(buffer, len);
		if (exeName.rfind("csmsd.elf") == std::string::npos)
		{
			iAmMaster = EMaster::VCP_MASTER;
			otherIsMaster = EMaster::CSMSD_MASTER;
		}
	}

	m_shmSize = offsetof(SSharedMemory, state) + stateSize;
	m_shmDesc = shm_open(m_shMemName.c_str(), O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
	if (m_shmDesc == -1)
		throw std::runtime_error("CSharedMemory shm_open failed");

	struct stat statbuf;
	if (fstat(m_shmDesc, &statbuf) == 0)
	{
		printf("CSharedMemory: size is %lu, wanted is %u\n", statbuf.st_size, m_shmSize);
	}

	if (ftruncate(m_shmDesc, m_shmSize) != 0)
		throw std::runtime_error("CSharedMemory ftruncate failed");

	void* shMem = mmap(nullptr, m_shmSize, PROT_READ | PROT_WRITE, MAP_SHARED, m_shmDesc, 0);
	if (shMem == MAP_FAILED)
		throw std::runtime_error("CSharedMemory mmap failed");

	m_shMem = reinterpret_cast<SSharedMemory*>(shMem);
	printf("CSharedMemory: master = %d iAmMaster = %d\n", m_shMem->master, iAmMaster);
	if (m_shMem->master == iAmMaster)
	{
		// Still locked from previous crash - was supposed to be cleaned up automatically but not
		m_shMem->sharedMutex.Recreate();	// Not sure about this workaround
	}
	int rv = m_shMem->sharedMutex.Lock();
	if (rv != 0)
	{
		printf("CSharedMemory: Failed to lock sharedMutex, error = %d\n", rv);
		if (rv == EOWNERDEAD || rv == ENOTRECOVERABLE)
		{
			m_shMem->sharedMutex.Recreate();
			if (m_shMem->sharedMutex.Lock() != 0)
				throw std::runtime_error("CSharedMemory: Unable to recreate and lock sharedMutex");
		}
	}
	// While we are locked, determine if we will be master of the 2630
	if (m_shMem->master != otherIsMaster)
	{
		m_shMem->master = iAmMaster;
		m_master = true;
	}

	m_shMem->sharedMutex.Unlock();
	return;
}

//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CSharedMemory::~CSharedMemory(void)
{
	if (m_shmDesc != -1)
	{
		// If we are master, relinquish control
		if (m_master)
		{
			int rv = m_shMem->sharedMutex.Lock();
			if (rv == 0)
			{
				m_shMem->master = EMaster::NO_MASTER;
				m_shMem->sharedMutex.Unlock();
				printf("CSharedMemory: I am no longer master\n");
			}
		}
		munmap(m_shMem, m_shmSize);
		shm_unlink(m_shMemName.c_str());
	}
}
