/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

//#include "3230.h"
#include "51432017.h"
#include "Config.h"
#include "Digitizer.h"
#include "RfSelSwitch.h"
#include "Singleton.h"
#include "EquipCtrlMsg.h"
#include "MetricsMsg.h"
#include "Navigation.h"
#include "PbCal.h"
#include "RadioEquip.h"
#include "RwLock.h"
#include "Singleton.h"
#include "Task.h"
#include "WatchDog.h"

namespace BistString
{
	const size_t NUM_LANGS = 2;
	constexpr const char* IDS_1S[NUM_LANGS] =
	{
		"%1$s",
		"%1$s"
	};
	constexpr const char* IDS_EMPTY[NUM_LANGS] =
	{
		"",
		""
	};
	constexpr const char* IDS_IS[NUM_LANGS] =
	{
		"is",
		"is"
	};
	constexpr const char* IDS_ISNOT[NUM_LANGS] =
	{
		"is not",
		"is not"
	};
	constexpr const char* IDS_NOTREADY[NUM_LANGS] =
	{
		"Not ready",
		"Not ready"
	};
	constexpr const char* IDS_READY[NUM_LANGS] =
	{
		"Ready",
		"Ready"
	};
	constexpr const char* IDS_YES[NUM_LANGS] =
	{
		"yes",
		"yes"
	};
	constexpr const char* IDS_NO[NUM_LANGS] =
	{
		"no",
		"no"
	};
	constexpr const char* IDS_DOWN[NUM_LANGS] =
	{
		"down",
		"down"
	};
	constexpr const char* IDS_UP[NUM_LANGS] =
	{
		"up",
		"up"
	};
	constexpr const char* IDS_NOTPRESENT[NUM_LANGS] =
	{
		"not present",
		"not present"
	};
	constexpr const char* IDS_BIST[NUM_LANGS] =
	{
		"Built-In Self Test",
		"Built-In Self Test"
	};
	constexpr const char* IDS_BIST_3230_TEMP[NUM_LANGS] =
	{
		"%1$s Temperature: %2$s C (%3$s:%4$s)",
		"%1$s Temperature: %2$s C (%3$s:%4$s)"
	};
	constexpr const char* IDS_BIST_3230_VOLTS[NUM_LANGS] =
	{
		"%1$s %2$s (%3$s:%4$s)",
		"%1$s %2$s (%3$s:%4$s)"
	};
	constexpr const char* IDS_BIST_3230_MEMCHECK[NUM_LANGS] =
	{
		"MemoryCheck: %1$s",
		"MemoryCheck: %1$s"
	};
	constexpr const char* IDS_BIST_BOARD_VERSION[NUM_LANGS] =
	{
		"Digital Board %1$s Rev %2$s Status %3$s",
		"Digital Board %1$s Rev %2$s Status %3$s"
	};
	constexpr const char* IDS_BIST_BOARD_DEVICEDNA[NUM_LANGS] =
	{
		"Device DNA %1$s",
		"Device DNA %1$s"
	};
	constexpr const char* IDS_3230_CLOCK[NUM_LANGS] =
	{
		"%1s for TDOA: %2$s %3$s Status: %4$s",
		"%1s for TDOA: %2$s %3$s Status: %4$s"
	};
	constexpr const char* IDS_BIST_DRV_VERSION[NUM_LANGS] =
	{
		"Driver version %1$s %2$s",
		"Driver version %1$s %2$s"
	};
	constexpr const char* IDS_BIST_FW_VERSION[NUM_LANGS] =
	{
		"Firmware version %1$s %2$s %3$s %4$s",
		"Firmware version %1$s %2$s %3$s %4$s"
	};
	constexpr const char* IDS_BIST_MAX10_ID[NUM_LANGS] =
	{
		"MAX10 unique id %1$s",
		"MAX10 unique id %1$s"
	};
	constexpr const char* IDS_BIST_ID[NUM_LANGS] =
	{
		"Diagnostics run on %1$s (part number %2$s rev %3$s, serial number %4$s) at %5$s",
		"Diagnostics run on %1$s (part number %2$s rev %3$s, serial number %4$s) at %5$s"
	};
	constexpr const char* IDS_BIST_CUSTOMER[NUM_LANGS] =
	{
		"Customer: %1$s",
		"Customer: %1$s"
	};
	constexpr const char* IDS_BIST_SW_VERSION[NUM_LANGS] =
	{
		"Software version %1$s %2$s (%3$s)",
		"Software version %1$s %2$s (%3$s)"
	};
	constexpr const char* IDS_BIST_SW_VERSIONMISMATCH[NUM_LANGS] =
	{
		"Software version %1$s %2$s (%3$s) - Slave software version mismatch!",
		"Software version %1$s %2$s (%3$s) - Slave software version mismatch!"
	};
	constexpr const char* IDS_BIST_EXEFILE[NUM_LANGS] =
	{
		"Executable file: %1$s date: %2$s",
		"Executable file: %1$s date: %2$s"
	};
	constexpr const char* IDS_BIST_MEDIARW[NUM_LANGS] =
	{
		"media is mounted readwrite: %1$s",
		"media is mounted readwrite: %1$s"
	};
	constexpr const char* IDS_BIST_SYSINFO[NUM_LANGS] =
	{
		"System name: %1$s release: %2$s version: %3$s machine: %4$s",
		"System name: %1$s release: %2$s version: %3$s machine: %4$s"
	};
	constexpr const char* IDS_BIST_SYSINFO2[NUM_LANGS] =
	{
		"Product name: %1$s fwversion: %2$s",
		"Product name: %1$s fwversion: %2$s"
	};
	constexpr const char* IDS_BIST_NTPVERSION[NUM_LANGS] =
	{
		"ntpd version %1$s",
		"ntpd version %1$s"
	};

	constexpr const char* IDS_BIST_GAINEQUALIZATION[NUM_LANGS] =
	{
		"Gain equalization: %1$s",
		"Gain equalization: %1$s"
	};

	constexpr const char* IDS_BIST_ENHANCEDDYNAMICRANGE[NUM_LANGS] =
	{
		"Enhanced dynamic range: %1$s",
		"Enhanced dynamic range: %1$s"
	};

	constexpr const char* IDS_LICENSE[NUM_LANGS] =
	{
		"License",
		"License"
	};

	constexpr const char* IDS_BIST_PRECISIONTIME_LICENSE[NUM_LANGS] =
	{
		"Precision Time",
		"Precision Time"
	};

	constexpr const char* IDS_BIST_80MHZ_LICENSE[NUM_LANGS] =
	{
		"80 MHz Bandwidth",
		"80 MHz Bandwidth"
	};

	constexpr const char* IDS_BIST_HF_LICENSE[NUM_LANGS] =
	{
		"HF Capability",
		"HF Capability"
	};

	constexpr const char* IDS_BIST_SHF_EXT_LICENSE[NUM_LANGS] =
	{
		"SHF Extension",
		"SHF Extension"
	};

	constexpr const char* IDS_BIST_VUSHF_LICENSE[NUM_LANGS] =
	{
		"VUSHF Capability up to %1$u MHz",
		"VUSHF Capability up to %1$u MHz"
	};

	constexpr const char* IDS_BIST_NETWORK[NUM_LANGS] =
	{
		"IP Address: %1$s MAC Address: %2$s",
		"IP Address: %1$s MAC Address: %2$s"
	};

	constexpr const char* IDS_BIST_EEPROM[NUM_LANGS] =
	{
		"EEPROM: MAC Address: %1$s",
		"EEPROM: MAC Address: %1$s"
	};

	constexpr const char* IDS_PROCESSORNODES[NUM_LANGS] =
	{
		"Processor Nodes",
		"Processor Nodes"
	};

	constexpr const char* IDS_BIST_SINGLEMASTER[NUM_LANGS] =
	{
		"Single Master",
		"Single Master"
	};

	constexpr const char* IDS_BIST_SINGLE_MULT_ANT[NUM_LANGS] =
	{
		"Single Master with multiple antennas at RF1",
		"Single Master with multiple antennas at RF1"
	};

	constexpr const char* IDS_BIST_RFSWITCH_STATUS[NUM_LANGS] =
	{
		"RF Switch: %1$s",
		"RF Switch: %1$s"
	};

	constexpr const char* IDS_BIST_DFMASTER[NUM_LANGS] =
	{
		"Df Master: nodes = %1$s",
		"Df Master: nodes = %1$s"
	};

	constexpr const char* IDS_BIST_DFSLAVE[NUM_LANGS] =
	{
		"Df Slave: %1$u",
		"Df Slave: %1$u"
	};

	constexpr const char* IDS_BIST_NO_DFSLAVE[NUM_LANGS] =
	{
		"Unable to connect to Slave node.",
		"Unable to connect to Slave node."
	};

	constexpr const char* IDS_BIST_RF2_DUAL_BAND_SPECS[NUM_LANGS] =
	{
		"Dual Band RF2: Aux1(%1$llu - %2$llu) Hz; Sample RF2(%3$llu - %4$llu) Hz.",
		"Dual Band RF2: Aux1(%1$llu - %2$llu) Hz; Sample RF2(%3$llu - %4$llu) Hz.",
	};

	constexpr const char* IDS_BIST_RF2_DUAL_BAND_WRONG_SYS[NUM_LANGS] =
	{
		"Dual Band RF2: can only be set for DF System",
		"Dual Band RF2: can only be set for DF System"
	};

	constexpr const char* IDS_DIGITIZER[NUM_LANGS] =
	{
		"Digitizer",
		"Digitizer"
	};

	constexpr const char* IDS_ANTENNA[NUM_LANGS] =
	{
		"Antennas",
		"Antennas"
	};

	constexpr const char* IDS_BIST_RF_ANT_ERR[NUM_LANGS] =
	{
			"Antenna %1$s is not compatible with Rf input %2$u",
			"Antenna %1$s is not compatible with Rf input %2$u"
	};

	constexpr const char* IDS_BIST_ANTENNA[NUM_LANGS] =
	{
		"Rf input %1$u has antenna %2$s with %3$s cable length configured as %4$s m",
		"Rf input %1$u has antenna %2$s with %3$s cable length configured as %4$s m"
	};

	constexpr const char* IDS_BIST_RF3ANTENNA_NOHF[NUM_LANGS] =
	{
		"Rf input %1$u disabled with HF disabled",
		"Rf input %1$u disabled with HF disabled"
	};

	constexpr const char* IDS_BIST_ANT_FACTORS_NOTFOUND[NUM_LANGS] =
	{
		"V/U/SHF RF%1$u: Antenna Factors file (%2$s) not found.",
		"V/U/SHF RF%1$u: Antenna Factors file (%2$s) not found."
	};


	constexpr const char* IDS_COMPASS[NUM_LANGS] =
	{
		"Compass",
		"Compass"
	};
	constexpr const char* IDS_BIST_VERSION[NUM_LANGS] =
	{
		"Version %1$s",
		"Version %1$s"
	};
	constexpr const char* IDS_BIST_COMPASS_CALIBRATION[NUM_LANGS] =
	{
		"Calibrated %1$u times, noise score %2$u, magnetic environment score %3$u",
		"Calibrated %1$u times, noise score %2$u, magnetic environment score %3$u"
	};
	constexpr const char* IDS_BIST_COMPASS_HEADING[NUM_LANGS] =
	{
		"Magnetic heading %1$s, declination %2$s",
		"Magnetic heading %1$s, declination %2$s"
	};
	constexpr const char* IDS_BIST_COMPASS_MAST[NUM_LANGS] =
	{
		"Antenna mast is %1$s",
		"Antenna mast is %1$s"
	};
	constexpr const char* IDS_BIST_COMPASS_CORRECTIONS[NUM_LANGS] =
	{
		"Mast down corrections: %1$s",
		"Mast down corrections: %1$s"
	};
	constexpr const char* IDS_GPS[NUM_LANGS] =
	{
		"GPS",
		"GPS"
	};
	constexpr const char* IDS_BIST_GPS[NUM_LANGS] =
	{
		"GPS status: %1$s, tracking %2$u sats, location %3$s, %4$s, altitude %5$s m, time(%6$s) %7$s, code %8$s",
		"GPS status: %1$s, tracking %2$u sats, location %3$s, %4$s, altitude %5$s m, time(%6$s) %7$s, code %8$s"
	};
	constexpr const char* IDS_NTP[NUM_LANGS] =
	{
		"NTP",
		"NTP"
	};
	constexpr const char* IDS_BIST_NTP[NUM_LANGS] =
	{
		"Time %1$s accurate, offset at %2$f ms",
		"Time %1$s accurate, offset at %2$f ms"
	};
	constexpr const char* IDS_PBCAL[NUM_LANGS] =
	{
		"Passband Calibration",
		"Passband Calibration"
	};
	constexpr const char* IDS_DF_PBCAL[NUM_LANGS] =
	{
		"DF PbCal",
		"DF PbCal"
	};
	constexpr const char* IDS_BIST_PBCAL[NUM_LANGS] =
	{
//		"Freq %1$s MHz, bandwidth %2$s MHz, band %3$u: Amplitude mean %4$s dB, std dev %5$s dB; phase mean %6$s, std dev %7$s; %8$u integrations",
//		"Freq %1$s MHz, bandwidth %2$s MHz, band %3$u: Amplitude mean %4$s dB, std dev %5$s dB; phase mean %6$s, std dev %7$s; %8$u integrations"
		"Freq %1$s MHz, Bw %2$s MHz, band %3$u: Ampl mean %4$s dB, stdev %5$s dB; delay %6$s, phase mean %7$s, stdev %8$s; %9$u ints",
		"Freq %1$s MHz, Bw %2$s MHz, band %3$u: Ampl mean %4$s dB, stdev %5$s dB; delay %6$s, phase mean %7$s, stdev %8$s; %9$u ints"
	};
	constexpr const char* IDS_BIST_RFCAL[NUM_LANGS] =
	{
		"RfTable Index %1$u Start %2$s MHz, Step %3$s MHz, IFIndex %4$u: AttIndex %5$u %6$u %7$u %8$u",
		"RfTable Index %1$u Start %2$s MHz, Step %3$s MHz, IFIndex %4$u: AttIndex %5$u %6$u %7$u %8$u"
	};
	constexpr const char* IDS_BIST_RFCAL1[NUM_LANGS] =
	{
		"RfTable Index %1$u Start %2$s KHz, Step %3$s KHz, < %4$s KHz Start %5$s KHz, Step %6$s KHz, IFIndex %7$u: AttIndex %8$u %9$u %10$u %11$u",
		"RfTable Index %1$u Start %2$s KHz, Step %3$s KHz, < %4$s KHz Start %5$s KHz, Step %6$s KHz, IFIndex %7$u: AttIndex %8$u %9$u %10$u %11$u"
	};
	constexpr const char* IDS_BIST_IFCAL[NUM_LANGS] =
	{
		"IfTable Index %1$u Start %2$s MHz, Step %3$s MHz",
		"IfTable Index %1$u Start %2$s MHz, Step %3$s MHz"
	};
	constexpr const char* IDS_BIST_ATTCAL[NUM_LANGS] =
	{
		"AttTable[%1$u] Index %2$u Start %3$s dB, Step %4$s dB",
		"AttTable[%1$u] Index %2$u Start %3$s dB, Step %4$s dB"
	};
	constexpr const char* IDS_BIST_CALGENCAL[NUM_LANGS] =
	{
		"CalGenTable Index %1$u Start %2$s MHz, Step %3$s MHz",
		"CalGenTable Index %1$u Start %2$s MHz, Step %3$s MHz"
	};
	constexpr const char* IDS_BIST_TEMPCAL[NUM_LANGS] =
	{
		"TempTable Index %1$u Nominal Temp %2$s C",
		"TempTable Index %1$u Nominal Temp %2$s C"
	};

	constexpr const char* IDS_BIST_CAL_TONE[NUM_LANGS] =
	{
		"Radio cal tone amplitude is %1$s dB from %2$s dBm(nominal) @ %3$u MHz in %4$s band(%5$u)%6$s",
		"Radio cal tone amplitude is %1$s dB from %2$s dBm(nominal) @ %3$u MHz in %4$s band(%5$u)%6$s"
	};
	constexpr const char* IDS_NARROW[NUM_LANGS] =
	{
		"narrow",
		"narrow"
	};
	constexpr const char* IDS_WIDE[NUM_LANGS] =
	{
		"wide",
		"wide"
	};
	constexpr const char* IDS_AFTER_RETRY[NUM_LANGS] =
	{
		" after retry",
		" after retry"
	};
	constexpr const char* IDS_RADIO[NUM_LANGS] =
	{
		"Rf Board",
		"Rf Board"
	};
	constexpr const char* IDS_DATABASE[NUM_LANGS] =
	{
		"Database",
		"Database"
	};
	constexpr const char* IDS_BIST_NOT_DETECTED[NUM_LANGS] =
	{
		"Not responding",
		"Not responding"
	};
	constexpr const char* IDS_BIST_DATABASE_TASK[NUM_LANGS] =
	{
		"%1$s from %2$s: Start %3$s, end %4$s, %5$s",
		"%1$s from %2$s: Start %3$s, end %4$s, %5$s"
	};

	constexpr const char* IDS_DF[NUM_LANGS] =
	{
		"DF",
		"DF"
	};
	constexpr const char* IDS_DF_SWITCH_NOT_DETECTED[NUM_LANGS] =
	{
		"Antenna Switch not detected",
		"Antenna Switch not detected"
	};
	constexpr const char* IDS_BIST_WRONG_HARDWARE[NUM_LANGS] =
	{
		"Wrong hardware (check configuration) %1$s",
		"Wrong hardware (check configuration) %1$s"
	};
	constexpr const char* IDS_BIST_SWITCH_HARDWARE[NUM_LANGS] =
	{
		"Switch hardware correct %1$s",
		"Switch hardware correct %1$s"
	};
	constexpr const char* IDS_BIST_SWITCH_CONFIG_HARDWARE[NUM_LANGS] =
	{
		"Switch hardware is assumed to be %1$s",
		"Switch hardware is assumed to be %1$s"
	};

	constexpr const char* IDS_BIST_HFWHIP[NUM_LANGS] =
	{
		"HF whip connected",
		"HF whip connected"
	};

	constexpr const char* IDS_BIST_NOHFWHIP[NUM_LANGS] =
	{
		"HF whip disconnected",
		"HF whip disconnected"
	};

	constexpr const char* IDS_3230_ZYNQ_INTERNAL_TEMP[NUM_LANGS] =
	{
		"Zynq Internal",
		"Zynq Internal"
	};
	constexpr const char* IDS_3230_ZYNQ_INTERNAL_VCCINT[NUM_LANGS] =
	{
		"Zynq Internal VCCINT(Volts)",
		"Zynq Internal VCCINT(Volts)"
	};
	constexpr const char* IDS_3230_ZYNQ_INTERNAL_VCCAUX[NUM_LANGS] =
	{
		"Zynq Internal VCCAUX(Volts)",
		"Zynq Internal VCCAUX(Volts)"
	};
	constexpr const char* IDS_3230_ZYNQ_INTERNAL_VCCBRAM[NUM_LANGS] =
	{
		"Zynq Internal VCCBRAM(Volts)",
		"Zynq Internal VCCBRAM(Volts)"
	};
	constexpr const char* IDS_3230_ZYNQ_INTERNAL_VCCPINT[NUM_LANGS] =
	{
		"Zynq Internal VCCPINT(Volts)",
		"Zynq Internal VCCPINT(Volts)"
	};
	constexpr const char* IDS_3230_ZYNQ_INTERNAL_VCCPAUX[NUM_LANGS] =
	{
		"Zynq Internal VCCPAUX(Volts)",
		"Zynq Internal VCCPAUX(Volts)"
	};
	constexpr const char* IDS_3230_ZYNQ_INTERNAL_VCCODDR[NUM_LANGS] =
	{
		"Zynq Internal VCCODDR(Volts)",
		"Zynq Internal VCCODDR(Volts)"
	};
	constexpr const char* IDS_3230_BOARD_5p3V_INPUT[NUM_LANGS] =
	{
		"Board 5.3V Input(Volts)",
		"Board 5.3V Input(Volts)"
	};
	constexpr const char* IDS_3230_DIGITAL_3p3V[NUM_LANGS] =
	{
		"3.3V Digital(Volts)",
		"3.3V Digital(Volts)"
	};
	constexpr const char* IDS_3230_DIGITAL_2p5V[NUM_LANGS] =
	{
		"2.5V Digital(Volts)",
		"2.5V Digital(Volts)"
	};
	constexpr const char* IDS_3230_MGT_1p0V[NUM_LANGS] =
	{
		"MGT 1.0V Supply(Volts)",
		"MGT 1.0V Supply(Volts)"
	};
	constexpr const char* IDS_3230_MGT_1p2V[NUM_LANGS] =
	{
		"MGT 1.2V Supply(Volts)",
		"MGT 1.2V Supply(Volts)"
	};
	constexpr const char* IDS_3230_MGT_1p8V[NUM_LANGS] =
	{
		"MGT 1.8V Supply(Volts)",
		"MGT 1.8V Supply(Volts)"
	};
	constexpr const char* IDS_3230_PS_DDR3_VTT[NUM_LANGS] =
	{
		"PS DDR3 VTT Supply(Volts)",
		"PS DDR3 VTT Supply(Volts)"
	};
	constexpr const char* IDS_3230_PL_DDR3_VTT[NUM_LANGS] =
	{
		"PL DDR3 VTT Supply(Volts)",
		"PL DDR3 VTT Supply(Volts)"
	};
	constexpr const char* IDS_3230_BOARD_TEMP[NUM_LANGS] =
	{
		"Board",
		"Board"
	};
	constexpr const char* IDS_3230_BOARD_TOTAL_CURR_5p3V[NUM_LANGS] =
	{
		"Board Total Current @ 5.3V Input(Amps)",
		"Board Total Current @ 5.3V Input(Amps)"
	};
	constexpr const char* IDS_3230_ADC_3p0V_ANALOG[NUM_LANGS] =
	{
		"ADC 3.0V Analog Supply(Volts)",
		"ADC 3.0V Analog Supply(Volts)"
	};
	constexpr const char* IDS_3230_ADC_1p8V_ANALOG[NUM_LANGS] =
	{
		"ADC 1.8V Analog Supply(Volts)",
		"ADC 1.8V Analog Supply(Volts)"
	};
	constexpr const char* IDS_3230_ADC_1p2V_ANALOG[NUM_LANGS] =
	{
		"ADC 1.2V Analog Supply(Volts)",
		"ADC 1.2V Analog Supply(Volts)"
	};
	constexpr const char* IDS_3230_CLOCK_3p3_ANALOG[NUM_LANGS] =
	{
		"Clock Device 3.3V Analog Supply(Volts)",
		"Clock Device 3.3V Analog Supply(Volts)"
	};
	constexpr const char* IDS_3230_CLOCK_3p3_DIGITAL[NUM_LANGS]=
	{
		"Clock Device 3.3V Digital Supply(Volts)",
		"Clock Device 3.3V Digital Supply(Volts)"
	};
	constexpr const char* IDS_3230_TEMP_REF_2p5V[NUM_LANGS] =
	{
		"2.5V Reference from Temp Sensor",
		"2.5V Reference from Temp Sensor"
	};

	constexpr const char* const* IDS_3230_VOLTAGES[] =
	{
		IDS_3230_ZYNQ_INTERNAL_VCCINT,
		IDS_3230_ZYNQ_INTERNAL_VCCAUX,
		IDS_3230_ZYNQ_INTERNAL_VCCBRAM,
		IDS_3230_ZYNQ_INTERNAL_VCCPINT,
		IDS_3230_ZYNQ_INTERNAL_VCCPAUX,
		IDS_3230_ZYNQ_INTERNAL_VCCODDR,
		IDS_3230_BOARD_5p3V_INPUT,
		IDS_3230_DIGITAL_3p3V,
		IDS_3230_DIGITAL_2p5V,
		IDS_3230_MGT_1p0V,
		IDS_3230_MGT_1p2V,
		IDS_3230_MGT_1p8V,
		IDS_3230_PS_DDR3_VTT,
		IDS_3230_PL_DDR3_VTT,
		IDS_3230_BOARD_TOTAL_CURR_5p3V,
		IDS_3230_ADC_3p0V_ANALOG,
		IDS_3230_ADC_1p8V_ANALOG,
		IDS_3230_ADC_1p2V_ANALOG,
		IDS_3230_CLOCK_3p3_ANALOG,
		IDS_3230_CLOCK_3p3_DIGITAL,
		IDS_3230_TEMP_REF_2p5V
	};
	constexpr const char* const* IDS_3230_TEMPS[] = {IDS_3230_ZYNQ_INTERNAL_TEMP, IDS_3230_BOARD_TEMP};

	constexpr const char* IDS_2630_MAX10_INTERNAL_TEMP[NUM_LANGS] =
	{
		"MAX10 Internal",
		"MAX10 Internal"
	};
	constexpr const char* IDS_2630_BOARD_TEMP1[NUM_LANGS] =
	{
		"Radio Temp1",
		"Radio Temp1"
	};
	constexpr const char* IDS_2630_BOARD_TEMP2[NUM_LANGS] =
	{
		"Radio Temp2",
		"Radio Temp2"
	};
	constexpr const char* const* IDS_2630_TEMPS[] = {IDS_2630_MAX10_INTERNAL_TEMP, IDS_2630_BOARD_TEMP1, IDS_2630_BOARD_TEMP2};

	constexpr const char* IDS_BIST_2630_TEMP[NUM_LANGS] =
	{
		"%1$s Temperature: %2$s C (%3$s:%4$s)",
		"%1$s Temperature: %2$s C (%3$s:%4$s)"
	};
	constexpr const char* IDS_BIST_2630_TEMP_FAILED[NUM_LANGS] =
	{
		"Radio Temperature - failed to read",
		"Radio Temperature - failed to read"
	};

	constexpr const char* IDS_BIST_2630_OCXO[NUM_LANGS] =
	{
		"Ocxo: Loop status: %1$s Dac code: %2$s interval: %3$s",
		"Ocxo: Loop status: %1$s Dac code: %2$s interval: %3$s"
	};

	constexpr const char* IDS_BIST_2630_INTERNAL_BIST_FAILED[NUM_LANGS] =
	{
		"Internal Bist failed to read",
		"Internal Bist failed to read"
	};
	constexpr const char* IDS_BIST_2630_TABLE_VERSIONS[NUM_LANGS] =
	{
		"Table versions: Normal:%1$s Rural:%2$s Urban:%3$s Congested:%4$s FPI10 IBW:%5$s FPI80 IBW:%6$s Presel:%7$s LO1:%8$s LO2:%9$s CalGen:%10$s BistRssi:%11$s",
		"Table versions: Normal:%1$s Rural:%2$s Urban:%3$s Congested:%4$s FPI10 IBW:%5$s FPI80 IBW:%6$s Presel:%7$s LO1:%8$s LO2:%9$s CalGen:%10$s BistRssi:%11$s"
	};

	constexpr const char* IDS_BIST_2630_CALTABLE_VERSIONS[NUM_LANGS] =
	{
		"CalTable versions: RF:%1$s RF(LNA):%2$s IF:%3$s Normal:%4$s Rural:%5$s Urban:%6$s Congested:%7$s CalGen:%8$s Temp:%9$s Temp(LNA):%10$s VTF:%11$s",
		"CalTable versions: RF:%1$s RF(LNA):%2$s IF:%3$s Normal:%4$s Rural:%5$s Urban:%6$s Congested:%7$s CalGen:%8$s Temp:%9$s Temp(LNA):%10$s VTF:%11$s"
	};

	constexpr const char* IDS_BIST_2630_VOLTS[NUM_LANGS] =
	{
		"Voltages: %1$s",
		"Voltages: %1$s"
	};

	constexpr const char* IDS_BIST_2630_BWTEST[NUM_LANGS] =
	{
		"Bandwidth Test %1$s:%2$s %3$s:%4$s",
		"Bandwidth Test %1$s:%2$s %3$s:%4$s"
	};

	constexpr const char* IDS_BIST_2630_BWTEST_DETAILS[NUM_LANGS] =
	{
		"Bandwidth Test failed @ %1$s MHz level:%2$s (%3$s) dbm",
		"Bandwidth Test failed @ %1$s MHz level:%2$s (%3$s) dbm"
	};

	constexpr const char* IDS_BIST_2630_CALGENTEST[NUM_LANGS] =
	{
		"CalGen Test Vushf %1$s present %2$s dbm hf %3$s present %4$s dbm",
		"CalGen Test Vushf %1$s present %2$s dbm hf %3$s present %4$s dbm"
	};

	constexpr const char* IDS_BIST_2630_SCANTEST[NUM_LANGS] =
	{
		"Scan FPI:%1$u @ %2$u MHz/%3$u %4$s in:%5$s dbm, out:%6$s dbm (%7$s present thresh:%8$s) dbm, att:%9$u dB %10$s, RSSI:%11$s, Locked: LO1:%12$s, LO2:%13$s, Calgen:%14$s",
		"Scan FPI:%1$u @ %2$u MHz/%3$u %4$s in:%5$s dbm, out:%6$s dbm (%7$s present thresh:%8$s) dbm, att:%9$u dB %10$s, RSSI:%11$s, Locked: LO1:%12$s, LO2:%13$s, Calgen:%14$s"
	};

	constexpr const char* IDS_BIST_2630_DACTABLEVERSION[NUM_LANGS] =
	{
		"Dac Table Version:%1$s",
		"Dac Table Version:%1$s"
	};

	constexpr const char* IDS_BIST_2630_TEMP_SENSOR1[NUM_LANGS] =
	{
		"Sensor 1 Temperature:%1$s",
		"Sensor 1 Temperature:%1$s"
	};

	constexpr const char* IDS_BIST_2630_TEMP_SENSOR2[NUM_LANGS] =
	{
		"Sensor 2 Temperature:%1$s",
		"Sensor 2 Temperature:%1$s"
	};

	constexpr const char* IDS_BIST_2630_TEMP_SENSOR_TEST[NUM_LANGS] =
	{
		"Temperature Sensor Test:%1$s",
		"Temperature Sensor Test:%1$s"
	};

	constexpr const char* IDS_BIST_2630_INPUT_CLOCK[NUM_LANGS] =
	{
		"2630 Input Clock:%1$s",
		"2630 Input Clock:%1$s"
	};

	constexpr const char* IDS_BIST_2630_OUTPUT_CLOCK[NUM_LANGS] =
	{
		"2630 Output Clock:%1$s",
		"2630 Output Clock:%1$s"
	};

	constexpr const char* IDS_BIST_2630_RSSI[NUM_LANGS] =
	{
		"RSSI Gain Values:%1$s",
		"RSSI Gain Values:%1$s"
	};

	constexpr const char* IDS_BIST_2630_QSPITEST[NUM_LANGS] =
	{
		"QSPI Flash size:%1$u MB free pages:%2$u Status:%3$s Locked:%4$s",
		"QSPI Flash size:%1$u MB free pages:%2$u Status:%3$s Locked:%4$s"
	};

	constexpr const char* IDS_UHF_SWITCH[NUM_LANGS] =
	{
		"U/SHF Antenna Switch",
		"U/SHF Antenna Switch"
	};
	constexpr const char* IDS_HF_SWITCH[NUM_LANGS] =
	{
		"HF Antenna Switch",
		"HF Antenna Switch"
	};
	constexpr const char* IDS_BIST_NOMINAL_ADJUSTMENT[NUM_LANGS] =
	{
		"Nominal value is adjusted by %1$s dB",
		"Nominal value is adjusted by %1$s dB"
	};
	constexpr const char* IDS_BIST_NOMINAL_MIN_MAX[NUM_LANGS] =
	{
		"Nominal value = %1$s dBm (Adjusted Min = %2$s dBm, Adjusted Max = %3$s dBm)",
		"Nominal value = %1$s dBm (Adjusted Min = %2$s dBm, Adjusted Max = %3$s dBm)"
	};
	constexpr const char* IDS_BIST_SWITCH_CAL_TONE[NUM_LANGS] =
	{
		"%1$s channel cal tone amplitude @ %2$s MHz is %3$s dBm (nominal %4$s dBm)",
		"%1$s channel cal tone amplitude @ %2$s MHz is %3$s dBm (nominal %4$s dBm)"
	};
	constexpr const char* IDS_BIST_ANT_SWITCH_NOISE[NUM_LANGS] =
	{
		"%1$s channel noise source amplitude is %2$s dBm (nominal %3$s dBm)",
		"%1$s channel noise source amplitude is %2$s dBm (nominal %3$s dBm)"
	};
	constexpr const char* IDS_BIST_ANTPOL[NUM_LANGS] =
	{
		"%1$s channel element %2$u (%3$s polarization) cal tone amplitude is %4$s dBm (nominal %5$s dBm)",
		"%1$s channel element %2$u (%3$s polarization) cal tone amplitude is %4$s dBm (nominal %5$s dBm)"
	};
	constexpr const char* IDS_BIST_ANTVARPOL[NUM_LANGS] =
	{
		"BITE tone variation (%1$s polarization) is %2$s dB",
		"BITE tone variation (%1$s polarization) is %2$s dB"
	};
	constexpr const char* IDS_UHF_ANTENNA[NUM_LANGS] =
	{
		"UHF Antenna",
		"UHF Antenna"
	};
	constexpr const char* IDS_SHF_ANTENNA[NUM_LANGS] =
	{
		"SHF Antenna",
		"SHF Antenna"
	};
	constexpr const char* IDS_HF_ANTENNA[NUM_LANGS] =
	{
		"HF Antenna",
		"HF Antenna"
	};

	constexpr const char* IDS_REFERENCE[NUM_LANGS] =
	{
		"Reference",
		"Reference"
	};
	constexpr const char* IDS_SAMPLE[NUM_LANGS] =
	{
		"Sample",
		"Sample"
	};
	constexpr const char* IDS_OPPOSITE[NUM_LANGS] =
	{
		"Opposite",
		"Opposite"
	};
	constexpr const char* IDS_MONITOR[NUM_LANGS] =
	{
		"Monitor",
		"Monitor"
	};
	constexpr const char* IDS_HIGH[NUM_LANGS] =
	{
		"high",
		"hIgh"
	};
	constexpr const char* IDS_HORIZONTAL[NUM_LANGS] =
	{
		"horizontal",
		"horizontal"
	};
	constexpr const char* IDS_VERTICAL[NUM_LANGS] =
	{
		"vertical",
		"vertical"
	};

	constexpr const char* IDS_RESTRICTED_FREQ[NUM_LANGS] =
	{
		"Restricted Frequencies",
		"Restricted Frequencies"
	};

	constexpr const char* IDS_BIST_NO_RESTRICTED_FREQ[NUM_LANGS] =
	{
		"None defined.",
		"None defined."
	};

	constexpr const char* IDS_BIST_RESTRICTED_FREQ[NUM_LANGS] =
	{
		"%1$llu - %2$llu Hz",
		"%1$llu - %2$llu Hz"
	};

	constexpr const char* IDS_WATCHDOG[NUM_LANGS] =
	{
		"Watchdog",
		"Watchdog"
	};
	constexpr const char* IDS_BIST_WATCHDOG_STATUS[NUM_LANGS] =
	{
		"watchdog %1$s enabled",
		"watchdog %1$s enabled"
	};

	//Used in SHF Extension.
	constexpr const char* IDS_BLK_CONVERTER[NUM_LANGS] =
	{
		"Block Converter",
		"Block Converter"
	};
	constexpr const char* IDS_ENABLE[NUM_LANGS] =
	{
		"Enabled",
		"Enabled"
	};
	constexpr const char* IDS_OPTION[NUM_LANGS] =
	{
		"Option",
		"Option"
	};
	constexpr const char* IDS_SHF[NUM_LANGS] =
	{
		"SHF Extension",
		"SHF Extension"
	};
	constexpr const char* IDS_SHF_EXT_COMM_ERR[NUM_LANGS] =
	{
		"Unable to connect to signal analyzer",
		"Unable to connect to signal analyzer"
	};
	constexpr const char* IDS_SHFEXT_ANALYZER_ERR[NUM_LANGS] =
	{
		"Incompatible signal analyzer",
		"Incompatible signal analyzer"
	};
	constexpr const char* IDS_SHFEXT_ANALYZER_MODE_ERR[NUM_LANGS] =
	{
		"Incorrect signal analyzer mode",
		"Incorrect signal analyzer mode"
	};
	constexpr const char* IDS_STATUS[NUM_LANGS] =
	{
		"%1$s: %2$s",
		"%1$s: %2$s"
	};
	constexpr const char* IDS_UNKNOWN[NUM_LANGS] =
	{
		"(unknown)",
		"(unknown)"
	};

};

class CBist
{
public:
	// Types
	struct SBistResult
	{
		SEquipCtrlMsg::SGetBistResp::EResult result;
		std::u16string test;
		std::u16string text;
		bool last;
		bool send;
	};
private:
	// Types

protected:
	// Types
	typedef std::vector<SBistResult> BistResults;
	static SSmsMsg::SGetBistResp::EResult m_OverallResult;

	// Functions
	CBist(void);
	virtual ~CBist(void);
	const BistResults GetBistResults(void);
	void DoBist(_In_ const CTask::Task& task);
	SEquipCtrlMsg::EErrStatus GetFinalSummary(void) const { return m_bistSummary; }
	void SendBistResult(_In_ const CTask::Task& task, _In_ const SBistResult& bistResult, bool last = false) const;

private:
	// Constants
	static const unsigned int RETRY = 5;

	// Types
	typedef std::unique_ptr<SDfCtrlMsg> SDfCtrlMsgPtr;

	class CBistSlave
	{
	public:
		CBistSlave(void) : m_powerOk(false), m_tag(0), m_tag2(0), m_tuneOk(false) {}

		void Init(Units::Frequency rfFreq, Units::Frequency rxHwBw, Units::Frequency procBw, bool hf, CRadioEquip::EBandSelect band);
		void Request(SDfCtrlMsg::SCcdfCollect::EPowerType powerType, unsigned char atten, SDfCtrlMsg::EGainMode gainMode);
		void RequestQuick(SDfCtrlMsg::SCcdfCollect::EPowerType powerType, unsigned char atten, SDfCtrlMsg::EGainMode gainMode);
		void GetResults(std::vector<float>& power);

	private:
		unsigned long MeasureSlavePower(SDfCtrlMsg::SCcdfCollect::EPowerType powerType, unsigned char atten, SDfCtrlMsg::EGainMode gainMode,
			std::vector<size_t>& sentIndex);
		unsigned long TuneSlaveRx(Units::Frequency rfFreq, Units::Frequency rxHwBw, Units::Frequency procBw, bool hf,
			CRadioEquip::EBandSelect band, std::vector<size_t>& sentIndex);
		bool WaitForSlaves(const std::vector<size_t> sentIndex, unsigned long tag, std::vector<SDfCtrlMsgPtr>& responses);

		// Data
		bool m_powerOk;
		std::vector<size_t> m_sentIndex;
		std::vector<size_t> m_sentIndex2;
		unsigned long m_tag;
		unsigned long m_tag2;
		bool m_tuneOk;
	};

	// Functions
	SSmsMsg::SGetBistResp::EResult DoBist2630(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBist3230(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBist72342011(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBist72362001(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistCompass(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistDatabase(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistDfSwitch(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistDfPbCal(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistGps(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistHfAntenna(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistLicense(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistNtp(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistPbCal(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistProcessorNodes(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistRestrictedFreq(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistRfInputs(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistShfExt(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistSystem(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistUshfAntenna(_In_ const CTask::Task& task);
	SSmsMsg::SGetBistResp::EResult DoBistWatchdog(_In_ const CTask::Task& task);
	std::string Format(DATE date) const;
	std::string Format(double x, unsigned int places) const;
	std::string Format(unsigned long x) const;
	std::string Format(BYTE x) const;
	SSmsMsg::SGetBistResp::EResult ListDatabaseTasks(_In_ const CTask::Task& task, _In_ const SMetricsMsg::SServerWorkloadResponse::SMeasWorkload& workload);
	SSmsMsg::SGetBistResp::EResult OutputShfExtStatus(_In_ const CTask::Task& task, const char* const testName[], CConfig::EShfExtStatus shfExtStat,
	                                                 _In_ const std::string & idStr,
	                                  		_In_ SSmsMsg::SGetBistResp::EResult passed);
	void RecordDfBistResult(_In_ const CTask::Task& task, bool last, bool send, const char* const testName[], SSmsMsg::SGetBistResp::EResult result,
			const char* const textStr[] = nullptr, ...);
	void RecordBistResult(_In_ const CTask::Task& task, bool last, bool send, const char* const testName[], SSmsMsg::SGetBistResp::EResult result,
			const char* const textStr[] = nullptr, ...);
	void RecordBistResult(size_t resIndex, _In_ const CTask::Task& task, bool last, bool send, const char* const testName[], SSmsMsg::SGetBistResp::EResult result,
			const char* const textStr[] = nullptr, va_list args = va_list());
	void RecordBistResult(_In_ const CTask::Task& task, bool last, bool send, _In_ const SBistResult& bistResult, _In_ const std::string& logStr);
	static void UpdateOverallResult(SSmsMsg::SGetBistResp::EResult result, _Inout_ SSmsMsg::SGetBistResp::EResult& overall);

	// Data
	CSingleton<CRfSelSwitch> m_antSwitch;
	CSingleton<CNavigation> m_navigation;		// Keep navigation before Digitizer
	std::vector<BistResults> m_bistResults;
	SEquipCtrlMsg::EErrStatus m_bistSummary;
	CSingleton<const CConfig> m_config;
	mutable CRWLock m_critSect;
	CSingleton<CDigitizer> m_digitizer;				// Keep navigation before Digitizer
	const CConfig::SMutableConfig& m_mutableConfig; // Must be after m_config
	int m_numSlaves;
	CSingleton<const CPbCal> m_pbCal;
	CSingleton<CRadioEquip> m_radioEquip;
	CSingleton<C51432017> m_switch;		// should be after m_digitizer
	CSingleton<CWatchdog> m_watchdog;
	bool m_senddetail;

};
