/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "MetricsMsg.h"
#include "SqliteDb.h"

class CScheduleTbl :
	// Inheritance
	protected virtual CSqliteDb
{
public:
	// Functions
	virtual ~CScheduleTbl(void);

protected:
	// Types
	enum EGetMode { FIRST, NEXT, FIND, /*CURRENT*/ };

	// Functions
	CScheduleTbl(void);
	int DeleteRow(unsigned long measureId);
	int GetRow(EGetMode mode, unsigned long& measureId, unsigned long& taskId, unsigned long& key,
		SMetricsMsg::SMeasureCtrlMsgStateResponse::EState& state, DATE& startTime, DATE& stopTime, DATE& updateTime,
		SMetricsMsg* msg = nullptr);
	int InsertRow(unsigned long& measureId, unsigned long taskId, unsigned long key,
		SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE startTime, DATE stopTime, DATE updateTime, const SMetricsMsg& msg);
	int Open(void);
	int UpdateRow(unsigned long measureId, SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state);
	int UpdateRow(unsigned long measureId, DATE updateTime);
	int UpdateRow(unsigned long measureId, SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE updateTime);
	int UpdateRow(unsigned long measureId, SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE stopTime, DATE updateTime);
	int UpdateRow(unsigned long measureId, unsigned long taskId, unsigned long key,
		SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE updateTime);
	int UpdateRow(unsigned long measureId, unsigned long taskId, unsigned long key,
		SMetricsMsg::SMeasureCtrlMsgStateResponse::EState state, DATE stopTime, DATE updateTime);

	// Data
	int m_openHr;
	sqlite3_stmt* m_pStmtUpdate2;
	sqlite3_stmt* m_pStmtUpdate2a;
	sqlite3_stmt* m_pStmtUpdate3;
	sqlite3_stmt* m_pStmtUpdate4;
	sqlite3_stmt* m_pStmtUpdate5;
	sqlite3_stmt* m_pStmtUpdate6;
	sqlite3_stmt* m_pStmtDelete;
	sqlite3_stmt* m_pStmtSelect0;
	sqlite3_stmt* m_pStmtSelect0a;
	sqlite3_stmt* m_pStmtSelect1;
	sqlite3_stmt* m_pStmtSelect1a;
	sqlite3_stmt* m_pStmtInsert;
};
