/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "Antenna.h"
#include "CsmsLicense.h"
#include "Log.h"
#include "ProcessorNode.h"
#include "RadioEquip.h"


/////////////////////////////////////////////////////////////////////
//
// Constructor
//
CRadioEquip::CRadioEquip() :
	C2630(CSingleton<CProcessorNode>()->IsProcessorDf()),
	m_config(),
	m_mutableConfig(m_config->GetMutableConfig()),
	m_mruList(),
	m_radioTemp(40.f),	// nominal temp
	m_shfExt()
{
	TRACE("CRadioEquip ctor\n");
	return;
}


/////////////////////////////////////////////////////////////////////
//
// Destructor
//
CRadioEquip::~CRadioEquip()
{
	TRACE("CRadioEquip dtor\n");

}


/////////////////////////////////////////////////////////////////////
//
// Calculate sample channel gain from offset table
//
unsigned char CRadioEquip::CalcSampleGain(Units::Frequency rfFreq, bool isHorizon, bool hf) const
{
	auto sampleGainOffset = m_config->GetSampleGainOffset(rfFreq, isHorizon, hf);
	long tempAtt = long(C2630::GetAtten()) + long(sampleGainOffset);
	if (tempAtt < 0) tempAtt = 0;
	else if (tempAtt > UCHAR_MAX) tempAtt = UCHAR_MAX;
	return static_cast<unsigned char>(tempAtt);
}


/////////////////////////////////////////////////////////////////////
//
// Set Radio for BIST calibration
//
bool CRadioEquip::DoBistCal(size_t fpi, bool narrow, unsigned long& calFreqMHz, float& rxInputDbm, bool& direct, float&expectedDbmAt3230)
{
	float NOMINAL_CAL_DBM = -20.f;	// This is the level wanted at the A/D
//	unsigned char NOMINAL_CAL_ATTEN = 60;	// Just try this for now; adjust later
	C2630::EGainMode CAL_GAINMODE = C2630::EGainMode::RURAL;
//	float NOMINAL_CAL_ATTEN_LIMIT = 5.0f;

	// Find center of fpi and round to radio frequency
	Units::Frequency freq;
	bool hf = false;
	S2630Msg::EAntennaType rxInput = S2630Msg::TERMINATE;

	if (fpi < m_hfBands.size())
	{
		if (m_configData.hfBwMHz == 0)
			return false;	// no HF capability

		freq =  (m_hfBands[fpi].f1 + m_hfBands[fpi].f2) / 2;
		hf = true;
		rxInput = S2630Msg::HFCAL;
	}
	else if (fpi < m_hfBands.size() + m_vushfBands.size())
	{
		if (m_configData.narrowBwMHz == 0)
			return false;	// no VUHF capability

		freq = (m_vushfBands[fpi - m_hfBands.size()].f1 + m_vushfBands[fpi - m_hfBands.size()].f2) / 2;
		if (freq > Units::Frequency(m_configData.maxVushfFreqMHz * 1000000.0))
			return false;	// frequency out of range

		hf = false;
		rxInput = S2630Msg::VUSHFCAL;
	}
	else
	{
		assert(false);
		return false;
	}

	auto rxFreq = C2630::CalcRxFreqMHz(hf, freq);

	// Adjust the caltone frequency according to its rules
	if (C2630::SetCalToneMHz(rxFreq, calFreqMHz))	// Adjusts the caltone frequency according to its rules
	{
//		if (calFreqMHz != rxFreq)
//		{
//			printf("DoBistCal: calFreq %lu != rxFreq %lu\n", calFreqMHz, rxFreq);		// TODO: Remove this check.
//		}
	}
	else
	{
		return false;
	}
	// Find the caltable for this frequency and FPI - no interpolation necessary and no overlap in frequencies
	auto calFreqHz = calFreqMHz * 1000000.0;
	size_t calGenIdx = m_calGenTable.size() - 1;	// last index
	if (fpi <= 2)
	{
		calGenIdx = fpi;	// 0, 1, 2
	}
	else
	{
		for (size_t i = 4; i < m_calGenTable.size(); ++i)
		{
			// Table includes n points from fstart to fstart + (n-1)*fstep
			if (calFreqHz < m_calGenTable[i].startValue)
			{
				calGenIdx = i - 1;
				break;
			}
		}
	}
	if (calGenIdx < m_calGenTable.size())
	{
		size_t calIdx = static_cast<size_t>((calFreqHz - m_calGenTable[calGenIdx].startValue) / m_calGenTable[calGenIdx].stepValue);
		calIdx = std::min(calIdx, m_calGenTable[calGenIdx].data.size());
		rxInputDbm = m_calGenTable[calGenIdx].data[calIdx];
//		rxInputDbm = InterpolateGain(calFreqHz, m_calGenTable[calGenIdx].startValue, m_calGenTable[calGenIdx].stepValue, m_calGenTable[calGenIdx].data, true);
	}
	else
	{
		rxInputDbm = 0;
	}
	Units::Frequency radioFreq;
	bool inverted;
	Units::Frequency f1, f2;
	size_t band;
	Units::Frequency rxBw = GetRxBandwidth(hf, narrow);
	if (hf && fpi == 0 && calFreqMHz > 15)	// Force  use of the correct FPI
	{
		rxBw = Units::Frequency(2000000 * calFreqMHz);
	}
//	Units::Frequency finalIfFreq =
	Units::Frequency retVal;
	if (!C2630::Tune(Units::Frequency(calFreqMHz * 1000000.0), rxBw, Units::Frequency(), hf,
		rxInput, CRadioEquip::EBandSelect::OPTIMUM, inverted, direct, band, radioFreq, f1, f2, retVal))
	{
		return false;
	}

	if (m_configData.rxProcessor >= 41)
	{
		//SetAttenForSpecifiedOutput(rxInputDbm, NOMINAL_CAL_DBM, C2630::EGainMode::CONGESTED, expectedDbmAt3230);
		SetAttenForSpecifiedOutput(rxInputDbm, NOMINAL_CAL_DBM, C2630::EGainMode::URBAN, expectedDbmAt3230);
	}
	else
	{
		//if (m_configData.firmwareVersion > 0x1060015) // 2-26-18 cutoff version for staying with nonurban radio cal
		if (m_bistResults.attenCalTableUrbanVersion > 0x1060015)
		{
			SetAttenForSpecifiedOutput(rxInputDbm, NOMINAL_CAL_DBM, C2630::EGainMode::URBAN, expectedDbmAt3230);
		}
		else
		{
			if (fpi >= 21 && fpi <= 25)
				SetAttenForSpecifiedOutput(rxInputDbm, NOMINAL_CAL_DBM, C2630::EGainMode::NORMAL, expectedDbmAt3230);
			else
				SetAttenForSpecifiedOutput(rxInputDbm, NOMINAL_CAL_DBM, CAL_GAINMODE, expectedDbmAt3230);
		}
	}
#if 0
	unsigned char atten = NOMINAL_CAL_ATTEN;	// Just try this for now; adjust later
	atten = RoundAtten(atten);

	unsigned char setAtten;
	C2630::EGainMode setGainMode;
	bool setLNA;
	size_t rfGainTableIdx;
	long ifGainTableIdx;
	float gainAdjustmentTemp;

	SetAttenuation(CAL_GAINMODE, atten, setAtten, setGainMode, setLNA);

	// Get the actual gain for the current radio settings
	auto rxGain = GetRxGainRatio(rfGainTableIdx, ifGainTableIdx, gainAdjustmentTemp);

	expectedDbmAt3230 = rxInputDbm + 10.f * log10(rxGain);
//	printf("DoBistCal: freq = %lu, expectedDbmAt3230 = %f\n", calFreqMHz, expectedDbmAt3230);
	auto deltaAtten = expectedDbmAt3230 - NOMINAL_CAL_DBM;
	if (fabs(deltaAtten) < NOMINAL_CAL_ATTEN_LIMIT)
	{
		return true;
	}
	// Try to adjust the attenuation
	auto tempAtten = atten + deltaAtten;
	unsigned char newAtten = 0;
	if (tempAtten > 0)
	{
		newAtten = static_cast<unsigned char>(tempAtten + 0.5f);
		auto maxAtten = CurrentMaxAtten(CAL_GAINMODE);
		if (newAtten > maxAtten) newAtten = maxAtten;
	}
	newAtten = RoundAtten(newAtten);
	if (newAtten == atten)
	{
		return true;
	}
	atten = newAtten;
	SetAttenuation(C2630::EGainMode::RURAL, atten, setAtten, setGainMode, setLNA);
	rxGain = GetRxGainRatio(rfGainTableIdx, ifGainTableIdx, gainAdjustmentTemp);
	expectedDbmAt3230 = rxInputDbm + 10.f * log10(rxGain);
//	printf("DoBistCal[2]: freq = %lu, expectedDbmAt3230 = %f\n", calFreqMHz, expectedDbmAt3230);
#endif
	return true;
}


/////////////////////////////////////////////////////////////////////
//
// Get the receiver attenuation
//
void CRadioEquip::GetAtten(unsigned char& atten)
{
	long temp = (long) C2630::GetAtten();
	if (temp > UCHAR_MAX)
		temp = UCHAR_MAX;
	atten = static_cast<unsigned char>(temp);
	return;
}


/////////////////////////////////////////////////////////////////////
//
// Get the receiver maximum attenuation
//
unsigned char CRadioEquip::GetMaxMaxAtten(_In_ EGainMode /*gainMode*/) const
{
	return C2630::m_configData.maxAtten;
}


/////////////////////////////////////////////////////////////////////
//
// Get the attenuation parameters
//
void CRadioEquip::GetCurrentAttenLimits(unsigned char& min, unsigned char& max) const
{
	min = 0;
	max = CurrentMaxAtten();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get the overall and detailed calibration status
//
bool CRadioEquip::GetCalStatus(_Out_ std::vector<SGainTable>& status) const
{
	status.clear();

	bool overall = true;

	if (m_rfGainTable.empty())
	{
		overall = false;
	}
	else
	{
		for (size_t i = 0; i < m_rfGainTable.size(); ++i)
		{
			auto& g = m_rfGainTable[i];
			SGainTable s;
			s.type = S2630Msg::RFCALDATA;
			s.subType = 0;
			s.startValue = g.startValue;
			s.stepValue = g.stepValue;
			s.ifIndex = g.ifIndex;
			for (size_t j = 0; j < size_t(C2630::EGainMode::NUM_GAINMODES); ++j)
				s.attIndexes[j] = g.attIndexes[j];
			s.lowFreq = g.lowFreq;
			if (g.lowFreq != 0)
			{
				s.low.startValue = g.lowTable.startValue;
				s.low.stepValue = g.lowTable.stepValue;
			}
			s.status = ErrorCodes::SUCCESS;
			status.push_back(s);
		}
	}

	if (m_ifGainTable.empty())
	{
		overall = false;
	}
	else
	{
		for (size_t i = 0; i < m_ifGainTable.size(); ++i)
		{
			auto& g = m_ifGainTable[i];
			SGainTable s;
			s.type = S2630Msg::IFCALDATA;
			s.subType = 0;
			s.startValue = g.startValue;
			s.stepValue = g.stepValue;
			s.status = ErrorCodes::SUCCESS;
			status.push_back(s);
		}
	}

	for (size_t j = 0; j < size_t(C2630::EGainMode::NUM_GAINMODES); ++j)
	{
		auto& attenModeTable = m_attenTables[j];
		if (attenModeTable.empty())
		{
			overall = false;
		}
		else
		{
			for (size_t i = 0; i < attenModeTable.size(); ++i)
			{
				auto& g = attenModeTable[i];
				SGainTable s;
				s.type = S2630Msg::ATTCALDATA;
				s.subType = j;
				s.startValue = g.startValue;
				s.stepValue = g.stepValue;
				s.status = ErrorCodes::SUCCESS;
				status.push_back(s);
			}
		}
	}

	if (m_calGenTable.empty())
	{
		overall = false;
	}
	else
	{
		for (size_t i = 0; i < m_calGenTable.size(); ++i)
		{
			auto& g = m_calGenTable[i];
			SGainTable s;
			s.type = S2630Msg::CALGENDATA;
			s.subType = 0;
			s.startValue = g.startValue;
			s.stepValue = g.stepValue;
			s.status = ErrorCodes::SUCCESS;
			status.push_back(s);
		}
	}

	if (m_tempGainTable.empty())
	{
		overall = false;
	}
	else
	{
		for (size_t i = 0; i < m_tempGainTable.size(); ++i)
		{
			SGainTable s;
			s.type = S2630Msg::TEMP_COMP;
			s.subType = 0;
			s.startValue = m_tempGainTable[i].nominalTemp;
			s.status = ErrorCodes::SUCCESS;
			status.push_back(s);
		}
	}

	return overall;
}


//////////////////////////////////////////////////////////////////////
//
// Get calibration correction for single frequency
//
float CRadioEquip::GetCorrection(Units::Frequency frequency, Units::Frequency radioFreq, size_t rfGainTableIdx,
	long ifGainTableIdx, bool hf, unsigned char atten) const
{
	auto gainMode = m_config->GetGainMode(frequency, hf);
	return GetCorrection(frequency, radioFreq, rfGainTableIdx, ifGainTableIdx, atten, gainMode);
}

float CRadioEquip::GetCorrection(Units::Frequency frequency, Units::Frequency radioFreq, size_t rfGainTableIdx,
	long ifGainTableIdx, unsigned char atten, EGainMode gainMode) const
{
	if (rfGainTableIdx >= m_rfGainTable.size())
	{
		return 1.0f;
	}
	auto& rfGainTable = m_rfGainTable[rfGainTableIdx];

	float rfGain;
	if (rfGainTable.lowFreq != 0 && frequency < rfGainTable.lowFreq)
	{
		rfGain = InterpolateGain(frequency.Hz<double>(), rfGainTable.lowTable.startValue, rfGainTable.lowTable.stepValue, rfGainTable.lowTable.data, true);
	}
	else
	{
		rfGain = InterpolateGain(frequency.Hz<double>(), rfGainTable.startValue, rfGainTable.stepValue, rfGainTable.data, true);
	}

	if (ifGainTableIdx >= long(m_ifGainTable.size()))
	{
		return 1.0f;
	}

	float ifGain = 1.f;
	if (ifGainTableIdx >= 0)
	{
		auto& ifGainTable = m_ifGainTable[ifGainTableIdx];
		auto offsetFrequency = frequency - radioFreq;
		ifGain = InterpolateGain(offsetFrequency.Hz<double>(), ifGainTable.startValue, ifGainTable.stepValue, ifGainTable.data, false);
	}

	// Temperature correction not done here, done at center frequency only

	size_t attenTableIdx = rfGainTable.attIndexes[size_t(gainMode)];
	auto& attenModeTable = m_attenTables[size_t(gainMode)];
	if (attenTableIdx >= attenModeTable.size())
	{
		return 1.0f;
	}
	auto& attenTable = attenModeTable[attenTableIdx];
	unsigned char attIdx = atten / attenTable.stepValue;

	if (attIdx >= attenTable.data.size())
	{
		return 1.0f;
	}
	auto attenValue = attenTable.data[attIdx];
	if (atten != attIdx * attenTable.stepValue)
	{
		attenValue *= pow(10.0f, float(atten - attIdx * attenTable.stepValue) / 10.0f);
	}
	float gain = rfGain * ifGain / attenValue;

	return gain;
}


//////////////////////////////////////////////////////////////////////
//
// Get calibration correction for entire passband
//
void CRadioEquip::GetCorrection(Units::Frequency rfFreq, Units::Frequency radioFreq, Units::Frequency rxHwBw, size_t rfGainTableIdx,
	long ifGainTableIdx, Units::Frequency binSize, bool hf, unsigned char atten, Ne10F32Vec& adjPower) const
{
	auto gainMode = m_config->GetGainMode(rfFreq, hf);
	GetCorrection(rfFreq, radioFreq, rxHwBw, rfGainTableIdx, ifGainTableIdx, binSize, atten, gainMode, adjPower);
	return;
}

void CRadioEquip::GetCorrection(Units::Frequency rfFreq, Units::Frequency radioFreq, Units::Frequency rxHwBw, size_t rfGainTableIdx,
	long ifGainTableIdx, Units::Frequency binSize, unsigned char atten, EGainMode gainMode, Ne10F32Vec& adjPower) const
{
	bool valid = true;
	if (rfGainTableIdx >= m_rfGainTable.size())
	{
		printf("GetCorrection: rfGainTable index error\n");
		valid = false;
	}
	auto& rfGainTable = m_rfGainTable[rfGainTableIdx];
	if (ifGainTableIdx >= long(m_ifGainTable.size()))
	{
		printf("GetCorrection: ifGainTable index error\n");
		valid = false;
	}

	size_t attenTableIdx = rfGainTable.attIndexes[size_t(gainMode)];
	auto& attenModeTable = m_attenTables[size_t(gainMode)];
	if (attenTableIdx >= attenModeTable.size())
	{
		printf("GetCorrection: attenTable index error\n");
		valid = false;
	}
	auto& attenTable = attenModeTable[attenTableIdx];
	unsigned char attIdx = atten / attenTable.stepValue;

	if (!valid)
	{
		if (adjPower.size() > 0)
		{
			adjPower = 1.f;
		}
		return;
	}

	// Search in cache
	for (MruList::iterator cache = m_mruList.begin(); cache != m_mruList.end(); ++cache)
	{
		auto& corr = m_cachedCorrections[*cache];
		if (rfFreq == corr.freq && rxHwBw == corr.rxHwBw && binSize == corr.binSize && radioFreq == corr.radioFreq &&
			atten == corr.atten && rfGainTableIdx == corr.rfGainTableIdx && adjPower.size() == corr.cachedAdjustPower.size())
		{
			// Found
			adjPower = corr.cachedAdjustPower;

			if (cache != m_mruList.begin())
			{
				// Move to beginning of list
				m_mruList.push_front(*cache);
				m_mruList.erase(cache);
			}

			return;
		}
	}

	// Update MRU list
	size_t cache;

	if (m_mruList.size() == NUM_CACHED_CALS)
	{
		// Remove oldest entry
		cache = m_mruList.back();
		m_mruList.pop_back();
	}
	else
	{
		// New entry
		cache = m_mruList.size();
	}

	m_mruList.push_front(cache);

	// Scale (and shift) amplitude data
	auto& corr = m_cachedCorrections[cache];
	auto& adjust = corr.cachedAdjustPower;
	adjust.resize(adjPower.size());

	if (attIdx < attenTable.data.size())
	{
		auto attenValue = attenTable.data[attIdx];
		if (atten != attIdx * attenTable.stepValue)
		{
			attenValue *= pow(10.0f, float(atten - attIdx * attenTable.stepValue) / 10.0f);
		}
		for (int bin = 0; bin < int(adjPower.size()); ++bin)
		{
			auto frequency = rfFreq + (bin - int(adjPower.size() / 2)) * binSize;
			auto offsetFrequency = frequency - radioFreq;
			// Get rfGain at this bin
			float rfGain;
			if (rfGainTable.lowFreq != 0 && frequency < rfGainTable.lowFreq)
			{
				rfGain = InterpolateGain(frequency.Hz<double>(), rfGainTable.lowTable.startValue, rfGainTable.lowTable.stepValue, rfGainTable.lowTable.data, true);
			}
			else
			{
				rfGain = InterpolateGain(frequency.Hz<double>(), rfGainTable.startValue, rfGainTable.stepValue, rfGainTable.data, true);
			}
			// Get ifGain at the offset frequency - radioFrequency
			float ifGain = 1.f;
			if (ifGainTableIdx >= 0)
			{
				auto& ifGainTable = m_ifGainTable[ifGainTableIdx];
				ifGain = InterpolateGain(offsetFrequency.Hz<double>(), ifGainTable.startValue, ifGainTable.stepValue, ifGainTable.data, false);
			}

			// Temperature correction not done here, done at center frequency only
			adjust[bin] = rfGain * ifGain / attenValue;
		}
	}
	else
	{
		for (int bin = 0; bin < int(adjPower.size()); ++bin)
		{
			adjust[bin] = 0;
		}
	}
	// Update cache
	corr.freq = rfFreq;
	corr.rxHwBw = rxHwBw;
	corr.binSize = binSize;
	corr.radioFreq = radioFreq;
	corr.atten = atten;
	corr.rfGainTableIdx = rfGainTableIdx;

	adjPower = adjust;

//	for (size_t bin = 0; bin < adjPower.size(); ++bin)
//	{
//		printf("bin %d: adjPower:%f\n", bin, adjPower[bin]);
//	}
	return;
}


/////////////////////////////////////////////////////////////////////
//
// Get the receiver maximum HF frequency
//
Units::Frequency CRadioEquip::GetMaxHfFreq(void) const
{
	return Units::Frequency(C2630::m_configData.maxHfFreqMHz * 1000000.0);
}


/////////////////////////////////////////////////////////////////////
//
// Get the receiver bandwidth
//
Units::Frequency CRadioEquip::GetRxBandwidth(bool hf, bool narrow) const
{
	if (hf)
	{
		return Units::Frequency(C2630::m_configData.hfBwMHz * 1000000.0);
	}
	return Units::Frequency((narrow ? C2630::m_configData.narrowBwMHz : C2630::m_configData.tuneBwMHz) * 1000000.0);
}

Units::Frequency CRadioEquip::GetRxBandwidth(_In_ const CConfig::SMutableConfig& /*mutableConfig*/, bool hf, bool narrow) const
{
	if (hf)
	{
		return Units::Frequency(C2630::m_configData.hfBwMHz * 1000000.0);
	}
	return Units::Frequency((narrow ? C2630::m_configData.narrowBwMHz : C2630::m_configData.tuneBwMHz) * 1000000.0);
}


/////////////////////////////////////////////////////////////////////
//
// Get the receiver gain
//
float CRadioEquip::GetRxGainRatio(_Out_ size_t& rfGainTableIdx, _Out_ long& ifGainTableIdx, float& gainAdjustmentTemp) const
{
	if (m_config->HasNoRadioGain())
	{
		gainAdjustmentTemp = 1.0f;
		return 1.0f;
	}
	float gain = C2630::GetGainRatio(m_radioTemp, rfGainTableIdx, ifGainTableIdx, gainAdjustmentTemp);

	return gain;
}



/////////////////////////////////////////////////////////////////////
//
// Round an attenuation
//
unsigned char CRadioEquip::RoundAtten(unsigned char atten) const
{
	if (C2630::m_configData.attenStep == 1)
		return atten;

	return (atten + C2630::m_configData.attenStep / 2) / C2630::m_configData.attenStep * C2630::m_configData.attenStep;
}


/////////////////////////////////////////////////////////////////////
//
// Run the internal radio BIST
//
bool CRadioEquip::RunBist(S2630Msg::SBistResults& bistResults)
{
	// Make sure that the bandwidth is set to 80 if available
	unsigned char setBw;
	C2630::SetBandWidth(80, setBw);
	bool success = C2630::RunBist(bistResults);
	m_bistResults = bistResults;
	return success;
}


/////////////////////////////////////////////////////////////////////
//
// Set the attenuation
//
void CRadioEquip::SetAtten(unsigned char refAtten, Units::Frequency freq, bool hf)
{
	auto gainMode = m_config->GetGainMode(freq, hf);
//	TRACE("CRadioEquip::SetAtten %d GainMode %d\n", refAtten, gainMode);
	unsigned char setAtten;
	C2630::EGainMode setGainMode;
	bool setLNA;
	C2630::SetAttenuation(gainMode, refAtten, setAtten, setGainMode, setLNA);

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Set the attenuation to achieve a specified signal level at the input to the digitizer
//
void CRadioEquip::SetAttenForSpecifiedOutput(float rxInputDbm, float wantedDbmAt3230, EGainMode gainMode, float& expectedDbmAt3230)
{
	static const unsigned char NOMINAL_ATTEN = 60;
	static const float NOMINAL_ATTEN_LIMIT = 5.0f;

	unsigned char atten = NOMINAL_ATTEN;	// Just try this for now; adjust later
	atten = RoundAtten(atten);

	unsigned char setAtten;
	C2630::EGainMode setGainMode;
	bool setLNA;
	size_t rfGainTableIdx;
	long ifGainTableIdx;
	float gainAdjustmentTemp;

	SetAttenuation(gainMode, atten, setAtten, setGainMode, setLNA);

	// Get the actual gain for the current radio settings
	auto rxGain = GetRxGainRatio(rfGainTableIdx, ifGainTableIdx, gainAdjustmentTemp);

	expectedDbmAt3230 = rxInputDbm + 10.f * log10(rxGain);
//	printf("DoBistCal: freq = %lu, expectedDbmAt3230 = %f\n", calFreqMHz, expectedDbmAt3230);
	auto deltaAtten = expectedDbmAt3230 - wantedDbmAt3230;
	if (fabsf(deltaAtten) < NOMINAL_ATTEN_LIMIT)
	{
		return;		// within +/- NOMINAL_ATTEN_LIMIT of wanted value
	}
	// Try to adjust the attenuation
	auto tempAtten = atten + deltaAtten;
	unsigned char newAtten = 0;
	if (tempAtten > 0)
	{
		newAtten = static_cast<unsigned char>(tempAtten + 0.5f);
		auto maxAtten = CurrentMaxAtten(gainMode);
		if (newAtten > maxAtten) newAtten = maxAtten;
	}
	newAtten = RoundAtten(newAtten);
	if (newAtten == atten)
	{
		return;
	}
	atten = newAtten;
	//SetAttenuation(C2630::EGainMode::RURAL, atten, setAtten, setGainMode, setLNA);
	SetAttenuation(gainMode, atten, setAtten, setGainMode, setLNA);
	rxGain = GetRxGainRatio(rfGainTableIdx, ifGainTableIdx, gainAdjustmentTemp);
	expectedDbmAt3230 = rxInputDbm + 10.f * log10(rxGain);
	return;
}


#if 0
/////////////////////////////////////////////////////////////////////
//
// Set the antenna input
//
void CRadioEquip::SetRfInput(SEquipCtrlMsg::EAnt ant)
{
	unsigned char input;

	switch(ant)
	{
	case SEquipCtrlMsg::INVALID_ANT:
		input = 3;
		break;

	case SEquipCtrlMsg::ANT1:
	case SEquipCtrlMsg::ANT1H:
		input = 0;
		break;

	case SEquipCtrlMsg::ANT2:
	case SEquipCtrlMsg::SMPL_RF2:
		input = 1;
		break;

	case SEquipCtrlMsg::ANT3:
	case SEquipCtrlMsg::SMPL_RF3:
		input = 2;
		break;

	case SEquipCtrlMsg::DF_ANT_1V:
	case SEquipCtrlMsg::DF_ANT_2V:
	case SEquipCtrlMsg::DF_ANT_3V:
	case SEquipCtrlMsg::DF_ANT_4V:
	case SEquipCtrlMsg::DF_ANT_5V:
	case SEquipCtrlMsg::DF_ANT_6V:
	case SEquipCtrlMsg::DF_ANT_7V:
	case SEquipCtrlMsg::DF_ANT_8V:
	case SEquipCtrlMsg::DF_ANT_9V:
	case SEquipCtrlMsg::DF_ANT_1H:
	case SEquipCtrlMsg::DF_ANT_2H:
	case SEquipCtrlMsg::DF_ANT_3H:
	case SEquipCtrlMsg::DF_ANT_4H:
	case SEquipCtrlMsg::DF_ANT_5H:
	case SEquipCtrlMsg::DF_ANT_6H:
	case SEquipCtrlMsg::DF_ANT_7H:
	case SEquipCtrlMsg::DF_ANT_8H:
	case SEquipCtrlMsg::DF_ANT_9H:
		input = 0;
		break;

	default:
		THROW_LOGIC_ERROR();
	}

	unsigned char setInput;
	C2630::SetAntenna(input, setInput);

	return;
}
#endif

//////////////////////////////////////////////////////////////////////
//
// Tune the hardware. Returns IF frequency, receiver preselector limits and passband calibration band
//
bool CRadioEquip::Tune(Units::Frequency rfFreq, Units::Frequency rxBw, Units::Frequency procBw, bool hf, C2630::EBandSelect bandSelect,
	_Out_ Units::Frequency& finalIfFreq, _Out_ bool& inverted, _Out_ Units::FreqPair& freqLimits, _Out_ Units::Frequency& radioFreq, _Out_ size_t& band, _Out_ bool& direct,
	 _In_ const SEquipCtrlMsg::EAnt ant)
{
	S2630Msg::EAntennaType rxInput;
	if (hf)
	{
		if (!m_mutableConfig.hfConfig.antCable.antenna)
		{
			printf("CRadioEquip::Tune: set hf input with no antenna\n");
		}
		rxInput = S2630Msg::HFANT;
	}
	else
	{
		if (m_mutableConfig.vushfConfig.antPolicy == CConfig::EAntPolicy::AUTO_FREQ)
		{
			if (m_config->HasShfExt())
			{
				//SHF Extension has set the appropriate input, no
				// frequency checking is needed.
				if(ant == SEquipCtrlMsg::ANT2)
				{
					rxInput = S2630Msg::VUSHFANT2;
				}
				else
				{
					rxInput = S2630Msg::VUSHFANT1;
				}

			}
			else
			{
				// vushf - select antenna based on frequency
				if (rfFreq <= m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].freqHigh)
				{
					if (!m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna)
					{
						printf("CRadioEquip::Tune: set vushf to input1 with no antenna\n");
					}
					rxInput = S2630Msg::VUSHFANT1;
				}
				else
				{
					if (!m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF2].antenna)
					{
						printf("CRadioEquip::Tune: set vushf to input2 with no antenna\n");
					}
					rxInput = S2630Msg::VUSHFANT2;
				}
			}
		}
		else if (m_mutableConfig.vushfConfig.antPolicy == CConfig::EAntPolicy::CLIENT_NAME)
		{
			// vushf - select antenna based on client name
			if (CConfig::IsTermAnt(ant))
			{
				rxInput = S2630Msg::TERMINATE;
			}
			else
			{
				switch(ant)
				{
				case SEquipCtrlMsg::INVALID_ANT:
				case SEquipCtrlMsg::ANT1:
				case SEquipCtrlMsg::ANT1H:
				case SEquipCtrlMsg::ANT3:
				case SEquipCtrlMsg::SMPL_RF3:
				case SEquipCtrlMsg::DF_ANT_1V:
				case SEquipCtrlMsg::DF_ANT_2V:
				case SEquipCtrlMsg::DF_ANT_3V:
				case SEquipCtrlMsg::DF_ANT_4V:
				case SEquipCtrlMsg::DF_ANT_5V:
				case SEquipCtrlMsg::DF_ANT_6V:
				case SEquipCtrlMsg::DF_ANT_7V:
				case SEquipCtrlMsg::DF_ANT_8V:
				case SEquipCtrlMsg::DF_ANT_9V:
				case SEquipCtrlMsg::DF_ANT_1H:
				case SEquipCtrlMsg::DF_ANT_2H:
				case SEquipCtrlMsg::DF_ANT_3H:
				case SEquipCtrlMsg::DF_ANT_4H:
				case SEquipCtrlMsg::DF_ANT_5H:
				case SEquipCtrlMsg::DF_ANT_6H:
				case SEquipCtrlMsg::DF_ANT_7H:
				case SEquipCtrlMsg::DF_ANT_8H:
				case SEquipCtrlMsg::DF_ANT_9H:
					if (!m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna)
					{
						printf("CRadioEquip::Tune: set vushf to input1 with no antenna\n");
					}
					rxInput = S2630Msg::VUSHFANT1;
					break;

				case SEquipCtrlMsg::ANT2:
				case SEquipCtrlMsg::SMPL_RF2:
					if (!m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF2].antenna)
					{
						printf("CRadioEquip::Tune: set vushf to input2 with no antenna\n");
					}
					rxInput = S2630Msg::VUSHFANT2;
					break;

				default:
					printf("illegal ant value %d to CRadioEquip::Tune\n", ant);	//	THROW_LOGIC_ERROR();
					rxInput = S2630Msg::VUSHFANT1;
				}
			}
		}
		else
		{
			printf("illegal ant value %d to CRadioEquip::Tune\n", ant);	//	THROW_LOGIC_ERROR();
			rxInput = S2630Msg::VUSHFANT1;
		}
	}

	if (!C2630::Tune(rfFreq, rxBw, procBw, hf, rxInput, bandSelect, inverted, direct, band,
		radioFreq, freqLimits.first, freqLimits.second, finalIfFreq))
	{
		return false;
	}

	return true;
}


//////////////////////////////////////////////////////////////////////
//
// Update the radio config based on license, etc.
//
void CRadioEquip::UpdateConfig(void)
{
	CConfig::SMutableConfig mutableConfig = m_mutableConfig;		// Make a local copy

	{
		// Handle license features
		CSingleton<CCsmsLicense> license;
		license->Init();
		m_config->SetShfExtLicense(license->HasShfExt());

		if (!license->HasHF())
		{
			m_configData.hfBwMHz = 0;
			m_configData.minHfFreqHz = 0;
			m_configData.maxHfFreqMHz = 0;
		}
		unsigned long maxVushfFreqMHz = license->HasVUHF();
		if (maxVushfFreqMHz < m_configData.maxVushfFreqMHz)
		{
			m_configData.maxVushfFreqMHz = maxVushfFreqMHz;
		}
		if (m_configData.maxVushfFreqMHz == 0)
		{
			m_configData.narrowBwMHz = 0;
			m_configData.tuneBwMHz = 0;
			m_configData.minVushfFreqMHz = 0;
		}
		if (!license->Has80MHzRadio())
		{
			m_configData.tuneBwMHz = m_configData.narrowBwMHz;
		}
	}
	if (C2630::HasVushf())
	{
		mutableConfig.vushfConfig.rxType = (C2630::IsWideband() ?
			CConfig::SMutableConfig::SVushfConfig::WIDE_2630 : CConfig::SMutableConfig::SVushfConfig::NARROW_2630);
	}
	else
	{
		mutableConfig.vushfConfig.rxType = CConfig::SMutableConfig::SVushfConfig::NO_VUHFRX;
	}

	mutableConfig.hfConfig.rxType = (C2630::HasHf() ?
		CConfig::SMutableConfig::SHfConfig::HF_2630 : CConfig::SMutableConfig::SHfConfig::NO_HFRX);

	// SaveNewHardwareSetting(mutableConfig);
	m_config->SaveMutableConfig(mutableConfig);

	// Reload antenna specific parameters - this will be done again if this is the master processor by UpdateAntennaParams
	switch (m_mutableConfig.hfConfig.rxType)
	{
	case CConfig::SMutableConfig::SHfConfig::HF_2630:
		m_config->SetHfMetricFreqs(Units::Frequency(C2630::m_configData.minHfFreqHz),
			Units::Frequency(C2630::m_configData.maxHfFreqMHz * 1000000.0));
		break;
	default:
		break;
	}

	switch (m_mutableConfig.vushfConfig.rxType)
	{
	case CConfig::SMutableConfig::SVushfConfig::NARROW_2630:
	case CConfig::SMutableConfig::SVushfConfig::WIDE_2630:
		m_config->SetVushfMetricFreqs(Units::Frequency(C2630::m_configData.minVushfFreqMHz * 1000000.0),
			Units::Frequency(C2630::m_configData.maxVushfFreqMHz * 1000000.0));
		break;
	default:
		break;
	}
	m_config->SetAbsoluteFreqs(Units::Frequency(C2630::m_configData.minHfFreqHz), Units::Frequency(C2630::m_configData.maxVushfFreqMHz * 1000000.0));

	return;
}
