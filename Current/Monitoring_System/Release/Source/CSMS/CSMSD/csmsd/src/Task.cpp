/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "Antenna.h"
#include "Digitizer.h"
#include "PbCal.h"
#include "ProcessorNode.h"
#include "RadioEquip.h"
#include "Task.h"
#include "Utility.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
std::mutex CTask::m_taskIdMutex;
unsigned long CTask::m_nextTaskId = 0;
CTask::TaskMap CTask::m_taskMap;

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CTask::CTask(_In_opt_ const SEquipCtrlMsg* msg,
	_In_opt_ CNetConnection<void>* source, SEquipCtrlMsg::EAnt ant) :
//	CRadioEquip(),
	m_ant(ant),
	m_cmdAnt(ant),		//Make sure m_cmdAnt has valid value.
//	m_bfoFreq(0),
	m_completionTime(SEquipCtrlMsg::MAX_DATE),
//	m_demodBw(0),
//	m_demodFreq(0),
//	m_detMode(SEquipCtrlMsg::SRcvrCtrlCmd::OFF),
	m_msgHdr(),
	m_parent(),
	m_powerBw(0),
	m_rcvrAtten(SEquipCtrlMsg::SGetPanCmd::AGC),
	m_source(source),
	m_spurCal(false),
	m_spurCalEnabled(false),
	m_taskIdKey(),
	m_abandonDwell(false),
	m_config(),
	m_currentBlock(0),
	m_currentStep(0),
	m_excludeBands(),
	m_firstStep(0xffffffff),
	m_lastStep(0),
	m_procParams(),
	m_taskParams(),
	m_endOfDwell(true),
	m_mutableConfig(m_config->GetMutableConfig()),
//	m_priority(priority),
	m_taskGroup(ETaskGroup::PAN)
{
	m_taskIdKey.taskId = 0;
	m_taskIdKey.key = 0;

	if(msg == nullptr)
	{
		// Empty task
		return;
	}

	m_msgHdr = msg->hdr;

	// Build control data
	switch(m_msgHdr.msgType)
	{
	case SEquipCtrlMsg::ANT_CTRL:
		InitAntCtrl(msg);
		break;

	case SEquipCtrlMsg::AUTOVIOLATE_CTRL:
		InitAvdCtrl(msg);
		break;

	case SEquipCtrlMsg::BIST_CTRL:
		InitBistCtrl(msg);
		break;

	case SEquipCtrlMsg::DEMOD_CTRL:
		InitDemodCtrl(msg);
		break;

		case SEquipCtrlMsg::DF_CTRL:
		InitDfCtrl(msg);
		break;
#if 0
	case SEquipCtrlMsg::DSP_CTRL:
		InitDspCtrl(msg);
		break;
#endif
	case SEquipCtrlMsg::EQUIPMENT_CTRL:
		InitEquipmentCtrl(msg);
		break;
	case SEquipCtrlMsg::STATUS_CTRL:
		InitStatusCtrl(msg);
		break;

	case SEquipCtrlMsg::METRICS_CTRL:
		InitMetricsCtrl(msg);
		break;

	case SEquipCtrlMsg::PAN_DISP_CTRL:
		InitPanDispCtrl(msg);
		break;

		case SEquipCtrlMsg::OCCUPANCY_CTRL:
		InitOccupancyCtrl(msg);
		break;

	case SEquipCtrlMsg::OCCUPANCYDF_CTRL:
		InitOccupancyDfCtrl(msg);
		break;

#if 0
		case SEquipCtrlMsg::WB_DIGITIZER_SAMPLE:
		InitWbDigitizerSample(msg);
		break;

	case SEquipCtrlMsg::SOUNDER_CTRL:
		InitSounderCtrl(msg);
		break;
#endif
	default:
		THROW_LOGIC_ERROR(); // Should have already been filtered in CEquipControl
	}

	// Build the exclude bands
	BuildExcludeBands();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CTask::CTask(_In_ SRestartData& restartData) :
//	m_checksum(0),
//	m_demodBw(0),
//	m_demodFreq(0),
//	m_detMode(SEquipCtrlMsg::SRcvrCtrlCmd::OFF),
	m_parent(),
//	m_priority(BACKGROUND),
	m_powerBw(0),
	m_rcvrAtten(SEquipCtrlMsg::SGetPanCmd::AGC),
	m_source(nullptr),
	m_spurCal(false),
	m_spurCalEnabled(false),
	m_abandonDwell(false),
	m_config(),
	m_currentBlock(0),
	m_currentStep(0),
	m_excludeBands(),
	m_firstStep(0xffffffff),
	m_lastStep(0),
	m_endOfDwell(true),
	m_mutableConfig(m_config->GetMutableConfig())
{
	// Read from ISequentialStream into member data
	ReadData(restartData, m_ant);
	ReadData(restartData, m_completionTime);
	ReadData(restartData, m_msgHdr);
	ReadData(restartData, m_reportInterval);
	ReadData(restartData, m_reportTime);
	ReadData(restartData, m_taskIdKey);
	ReadData(restartData, m_procParams);
	ReadData(restartData, m_taskParams);

	// Build the exclude bands
	BuildExcludeBands();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Copy constructor
//
CTask::CTask(_In_ const CTask& task, _In_ const CTask::Task& parent) :
	m_agcTime(task.m_agcTime),
	m_ant(task.m_ant),
	m_cmdAnt(task.m_cmdAnt),
//	m_bfoFreq(task.m_bfoFreq),
	m_completionTime(task.m_completionTime),
//	m_demodBw(task.m_demodBw),
//	m_demodFreq(task.m_demodFreq),
//	m_detMode(task.m_detMode),
	m_msgHdr(task.m_msgHdr),
	m_parent(parent),
	m_powerBw(task.m_powerBw),
//	m_checksum(task.m_checksum),
	m_rcvrAtten(task.m_rcvrAtten),
	m_reportInterval(task.m_reportInterval),
	m_source(task.m_source),
	m_spurCal(task.m_spurCal),
	m_spurCalEnabled(task.m_spurCalEnabled),
	m_taskIdKey(task.m_taskIdKey),
	m_abandonDwell(task.m_abandonDwell),
	m_blockState(task.m_blockState),
	m_config(task.m_config),
	m_currentBlock(task.m_currentBlock),
	m_currentStep(task.m_currentStep),
	m_excludeBands(task.m_excludeBands),
	m_firstStep(task.m_firstStep),
	m_lastStep(task.m_lastStep),
	m_procParams(task.m_procParams),
	m_reportTime(task.m_reportTime),
	m_taskParams(task.m_taskParams),
	m_endOfDwell(task.m_endOfDwell),
	m_mutableConfig(task.m_mutableConfig),
//	m_priority(task.m_priority),
	m_taskGroup(task.m_taskGroup)
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CTask::~CTask()
{
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Assignment operator
//
_Ret_ CTask& CTask::operator=(_In_ const CTask& right)
{
	if(this != &right)
	{
		m_abandonDwell = right.m_abandonDwell;
		m_agcTime = right.m_agcTime;
		m_ant = right.m_ant;
		m_cmdAnt = right.m_cmdAnt;
//		m_bfoFreq = right.m_bfoFreq;
		m_blockState = right.m_blockState;
		m_completionTime = right.m_completionTime;
		m_currentBlock = right.m_currentBlock;
		m_currentStep = right.m_currentStep;
//		m_demodBw = right.m_demodBw;
//		m_demodFreq = right.m_demodFreq;
//		m_detMode = right.m_detMode;
		m_endOfDwell = right.m_endOfDwell;
		m_excludeBands = right.m_excludeBands;
		m_firstStep = right.m_firstStep;
		m_lastStep = right.m_lastStep;
		m_msgHdr = right.m_msgHdr;
		m_parent = right.m_parent;
//		m_priority = right.m_priority;
		m_powerBw = right.m_powerBw;
		m_procParams = right.m_procParams;
		m_rcvrAtten = right.m_rcvrAtten;
		m_reportInterval = right.m_reportInterval;
		m_reportTime = right.m_reportTime;
		m_source = right.m_source;
		m_spurCal = right.m_spurCal;
		m_spurCalEnabled = right.m_spurCalEnabled;
		m_taskIdKey = right.m_taskIdKey;
		m_taskGroup = right.m_taskGroup;
		m_taskParams = right.m_taskParams;
	}

	return *this;
}


//////////////////////////////////////////////////////////////////////
//
// Abandon the current dwell so task can yield immediately
//
void CTask::AbandonDwell(void)
{
	SetAbandonDwell(false);
}


//////////////////////////////////////////////////////////////////////
//
// Adjust frequency limits for channel bandwidth constraints
//
void CTask::AdjustFreqLimits(_Inout_ CConfig::STaskParams& taskParams)
{
	taskParams.startFreq -= taskParams.bw / 2;
	taskParams.stopFreq = taskParams.startFreq +
		(taskParams.stopFreq - taskParams.startFreq + taskParams.bw / 2) / taskParams.bw * taskParams.bw;

	return;
}

//////////////////////////////////////////////////////////////////////
///<summary>
/// Adjust PAN taskParams due to Analyzer 40 MHz bandwidth limitation.
///</summary>
///
/// <param name="taskParams">
///@param[out] Updated bw & numBlocks taskParams that account for Analyzer
///		bandwidth limitation.
/// </param>
///
/// <param name="freq">
///@param[in] PAN tuned frequency.
/// </param>
///
/// <param name="bw">
///@param[in] Requested PAN bandwidth.
/// </param>
///
/// <remarks>
///@note: If m_ant is ANT1 or ANT1H, it will be automatically set to ANT2
///	  (RF2) if the specified frequency is in SHF Extension range & Antenna
///	  Policy is set to Auto.
/// </remarks>
///
inline void CTask::AdjustShfExtPanTaskParams(CConfig::STaskParams & taskParams,
                                             Units::Frequency freq,
                                             Units::Frequency bw)
{
	if(m_config->IsShfExtUsed(m_ant, freq) && bw > CConfig::C_40_MHZ)
  	{
  		//It is agreed to limit PAN going through Analyzer to 40 MHz B/W.
		throw ErrorCodes::INVALIDBANDWIDTH;
		/*
  		ASSERT(bw == 2 *CConfig::C_40_MHZ);
  		taskParams.bw = CConfig::C_40_MHZ;
  		taskParams.numBlocks = 2;
  		*/
  	}
  	else
  	{
  		taskParams.bw = bw;
  		taskParams.numBlocks = 1;
  	}

	 return;
}

//////////////////////////////////////////////////////////////////////
//
// Build the exclude bands and set the initial task state
//
void CTask::BuildExcludeBands(void)
{
	for(size_t band = 0; band < m_taskParams.size(); ++band)
	{
		if(m_taskParams[band].numBlocks > 0)
		{
			// Included band/step
			if(m_firstStep == 0xffffffff)
			{
				m_firstStep = band;
			}

			m_lastStep = band;
		}
		else
		{
			// Excluded band
			m_excludeBands.insert(m_taskParams[band]);
		}
	}

	m_currentStep = (m_firstStep == 0xffffffff ? 0 : m_firstStep);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Check the antenna ID for non-df tasks
//
void CTask::CheckAntenna(SEquipCtrlMsg::EAnt ant, bool hf) const
{
	if (m_config->IsValidAntenna(ant, hf))
		return;

	// Bad antenna
	throw ErrorCodes::INVALIDANTENNAID;
}


//////////////////////////////////////////////////////////////////////
//
// Clean up the task map
//
void CTask::CleanUp(void)
{
	std::unique_lock<std::mutex> lock(m_taskIdMutex);
	m_taskMap.clear();

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Delete a task object from its ID and key
//
void CTask::DeleteTask(_In_ const SEquipCtrlMsg::STaskIdKey& taskIdKey)
{
	std::unique_lock<std::mutex> lock(m_taskIdMutex);
	TaskMap::const_iterator task = m_taskMap.find(taskIdKey.taskId);

	if (task != m_taskMap.end() && task->second->m_taskIdKey.key == taskIdKey.key)
	{
		m_taskMap.erase(task);
//		TRACE("CTask::DeleteTask deleted task with key %lu\n", taskIdKey.key);
	}
	else
	{
//		TRACE("CTask::DeleteTask failed to delete task with key %lu\n", taskIdKey.key);
		assert(FALSE); // Deleting a task that doesn't exist
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Find a task object from its ID and key
//
CTask::Task CTask::FindTask(_In_ const SEquipCtrlMsg::STaskIdKey& taskIdKey)
{
	std::unique_lock<std::mutex> lock(m_taskIdMutex);
	TaskMap::const_iterator task = m_taskMap.find(taskIdKey.taskId);

	if (task != m_taskMap.end() && task->second->m_taskIdKey.key == taskIdKey.key)
	{
		return task->second;
	}
	else
	{
		return Task();
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the EAnt value that matches polarization
//
SEquipCtrlMsg::EAnt CTask::GetEAntValue(SEquipCtrlMsg::EAnt eAnt, CConfig::EPolarization pol)
{
	SEquipCtrlMsg::EAnt retVal = eAnt;

	if( pol == CConfig::EPolarization::VERT )
	{
		if( eAnt == SEquipCtrlMsg::ANT1H )
		{
			//Convert Reference horizontal to Reference vertical.
			retVal = SEquipCtrlMsg::ANT1;
		}
		else if( (eAnt >= SEquipCtrlMsg::DF_ANT_1H) && (eAnt <= SEquipCtrlMsg::DF_ANT_9H) )
		{
			//Convert Horizontal element to Vertical element.
			auto antVal = eAnt - SEquipCtrlMsg::DF_ANT_1H + SEquipCtrlMsg::DF_ANT_1V;
			retVal = static_cast<SEquipCtrlMsg::EAnt> (antVal);
		}
	}
	else if( pol == CConfig::EPolarization::HORIZ )
	{
		if( eAnt == SEquipCtrlMsg::ANT1 )
		{
			//Convert Reference vertical to Reference horizontal .
			retVal = SEquipCtrlMsg::ANT1H;
		}
		else if( (eAnt >= SEquipCtrlMsg::DF_ANT_1V) && (eAnt <= SEquipCtrlMsg::DF_ANT_9V) )
		{
			//Convert Horizontal element to Vertical element.
			auto antVal = eAnt - SEquipCtrlMsg::DF_ANT_1V + SEquipCtrlMsg::DF_ANT_1H;
			retVal = static_cast<SEquipCtrlMsg::EAnt> (antVal);
		}
	}

	return( retVal );
}


//////////////////////////////////////////////////////////////////////
//
// Get the frequency for the first channel of the specified task state
//
Units::Frequency CTask::GetFirstChanFreq(size_t step, size_t block) const
{
	if (m_taskParams[step].startFreq == m_taskParams[step].stopFreq)
	{
		ASSERT(false); // Is this ever used?
		return GetTuneFreq(step, block) - m_taskParams[step].bw / 2;
	}
	else
	{
		return GetTuneFreq(step, block) - (GetProcBw(step) - m_taskParams[step].bw) / 2;
	}
}

//////////////////////////////////////////////////////////////////////
//
// Get the first measure step and the total number of measure steps
//
void CTask::GetMeasureSteps(size_t& firstMeasureStep, size_t& numMeasureSteps) const
{
	// Find first measurement step and total number of measurement steps
	firstMeasureStep = 0;
	numMeasureSteps = 0;

	for (size_t step = 0; step < m_taskParams.size(); ++step)
	{
		if (m_taskParams[step].mode == CConfig::EMode::MEASURE)
		{
			if (numMeasureSteps++ == 0)
			{
				firstMeasureStep = step;
			}
		}
		else if (numMeasureSteps > 0)
		{
			break;
		}
	}
	return;
}
//////////////////////////////////////////////////////////////////////
//
// Get the processed bandwidth
//
Units::Frequency CTask::GetProcBw(_In_ const CConfig::STaskParams& taskParams, Units::Frequency rxProcBw)
{
	Units::Frequency procBw;
	Units::Frequency sweepWidth = taskParams.stopFreq - taskParams.startFreq;

	if (sweepWidth > 0)
	{
		if (rxProcBw == 0 || taskParams.bw == 0)
		{
			procBw = sweepWidth;
		}
		else if (rxProcBw % taskParams.bw == 0)
		{
			procBw = rxProcBw;
		}
		else
		{
			procBw = rxProcBw / taskParams.bw * taskParams.bw;
		}

		if (procBw > sweepWidth)
		{
			procBw = sweepWidth;
		}
	}
	else
	{
		procBw = taskParams.bw;
	}

	return procBw;
}


//////////////////////////////////////////////////////////////////////
//
// Get the processing frequency offset for the current task state
//
Units::Frequency CTask::GetProcOffset(void) const
{
	Units::Frequency sweepWidth = m_taskParams[m_currentStep].stopFreq - m_taskParams[m_currentStep].startFreq;

	if(sweepWidth > 0)
	{
		// Cases where we have "channels" (the bandwidth is for the channel) - offset half a bin for even number of bins
		return m_procParams[m_currentStep].GetNumBins(GetProcBw()) % 2 == 0 ? m_procParams[m_currentStep].GetBinSize() / 2 : 0;
	}
	else
	{
		// Cases where the bandwidth is the whole measurement
		return 0;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Get the tune frequency
//

Units::Frequency CTask::GetTuneFreq(unsigned int step, unsigned int block) const
{
	if(m_taskParams[step].startFreq == m_taskParams[step].stopFreq)
	{
		return m_taskParams[step].startFreq;
	}
	else if(m_taskParams[step].numBlocks == 1)
	{
		return (m_taskParams[step].stopFreq + m_taskParams[step].startFreq) / 2;
	}
	// numBlocks > 1
	else if (m_taskParams[step].mode == CConfig::EMode::PB_CAL)
	{
		return (m_taskParams[step].stopFreq + m_taskParams[step].startFreq) / 2;
	}
	else	// numBlocks > 1 and not PB_CAL
	{
		return m_taskParams[step].startFreq + block * GetProcBw(step) + GetProcBw(step) / 2;
	}
}


//////////////////////////////////////////////////////////////////////
//
// Validate an ANT_CTRL task
//
void CTask::InitAntCtrl(_In_ const SEquipCtrlMsg* msg)
{
	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_ANT:
		// Check message length and requested response version
		if(msg->hdr.bodySize != 0)
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		break;

	case SEquipCtrlMsg::SET_ANT:
		// Check message length and requested response version
		if(msg->hdr.bodySize != sizeof(SEquipCtrlMsg::SAntSetCtrlCmd))
		{
			throw ErrorCodes::INVALIDVERSION;
		}

//		ChkPanAntValue(msg->body.antSetCtrlCmd.antenna);

		break;

	default:
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate and initialize an AUTOVIOLATE_CTRL task
//
void CTask::InitAvdCtrl(_In_ const SEquipCtrlMsg* msg)
{
	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_AUTOVIOLATE:
	case SEquipCtrlMsg::VALIDATE_AUTOVIOLATE:
		{
			m_taskGroup = ETaskGroup::BKGD;

			const SEquipCtrlMsg::SGetAutoViolateCmd& cmd = msg->body.getAutoViolateCmd;

			if (msg->hdr.bodySize != offsetof(SEquipCtrlMsg::SGetAutoViolateCmd, band) +
				cmd.numBands * sizeof(SEquipCtrlMsg::SGetAutoViolateCmd::band))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			if (cmd.numBands == 0)
			{
				throw ErrorCodes::AVDNOBANDS;
			}

			// Check storage time
			if (cmd.storageTime == 0)
			{
				throw ErrorCodes::INVALIDSTORAGETIME;
			}

			DATE now = Utility::CurrentTimeAsDATE();
//			TciGps::GetDATE(now);
			m_reportInterval = cmd.storageTime / double(Units::SECONDS_PER_DAY);
			m_reportTime = now + m_reportInterval;
			m_completionTime = now + cmd.measurementTime / double(Units::SECONDS_PER_DAY);
			auto multRf1Input = m_config->IsProcWithAntSelectionSwitch();
			bool translatedAnt = false;
			bool forcedHf = false;
			bool forcedVuhf = false;
			m_cmdAnt = cmd.ant;

			if(multRf1Input)
			{
				translatedAnt = m_config->TranslateVerify5143MultAntValue(cmd.ant,
						m_ant, forcedHf, forcedVuhf);
			}
			else
			{
				m_ant = cmd.ant;
			}

			// Bands
			bool someIncludes = false;

			for (unsigned int band = 0; band < cmd.numBands; ++band)
			{
				if (!cmd.band[band].exclude)
				{
					someIncludes = true;
				}

				// Get processing parameters
				CConfig::STaskParams taskParams;
				taskParams.bw = cmd.band[band].channelBandwidth;

				//Defensive programming: ProcParams maximum channel bandwidth
				// is 25 MHz. Checking is not needed, do it anyway in case we
				// support greater than 40 MHz AVD channelBandwidth in the future.
				if(m_config->HasShfExt() && m_ant == SEquipCtrlMsg::EAnt::ANT2 &&
						taskParams.bw > CConfig::C_40_MHZ)
				{
					throw ErrorCodes::INVALIDBANDWIDTH;
				}

				bool isHorizontal = (cmd.band[band].sType.signalType.horizPol != 0);
				taskParams.pol = (isHorizontal ? CConfig::EPolarization::HORIZ : CConfig::EPolarization::VERT);
				if (CConfig::GetAntPolarization(m_ant) != taskParams.pol)
				{
					if(translatedAnt)
					{
						//Used polarization specified in the antenna.
						taskParams.pol = CConfig::GetAntPolarization(m_ant);
					}
					else
					{
						throw ErrorCodes::INVALIDANTENNAID;
					}
				}
				auto isHf = ValidateFrequencyRange(cmd.band[band].lowFrequency,
					cmd.band[band].highFrequency, isHorizontal, false, m_ant);
				taskParams.mode = CConfig::EMode::AVD;
				taskParams.startFreq = cmd.band[band].lowFrequency;
				taskParams.stopFreq = cmd.band[band].highFrequency;
				AdjustFreqLimits(taskParams);
				SEquipCtrlMsg::EAnt eAnt;
				bool force40MHzProcBw = false;

				if(translatedAnt)
				{
					eAnt = m_ant;
					taskParams.hf = forcedHf;

					if(forcedHf)
					{
						if(!m_config->IsInHfFreqRange(taskParams.startFreq) ||
								!m_config->IsInHfFreqRange(taskParams.stopFreq))
						{
							throw ErrorCodes::INVALIDFREQUENCY;
						}
					}
					else
					{
						if(!m_config->IsFreqAboveVuhfMin(m_ant, taskParams.startFreq) ||
								!m_config->IsFreqAboveVuhfMin(m_ant, taskParams.stopFreq))
						{
							throw ErrorCodes::INVALIDFREQUENCY;
						}
					}
				}
				else
				{
					taskParams.hf = isHf;
					eAnt = GetEAntValue(m_ant, taskParams.pol);

					if(!isHf && m_config->HasShfExt() && ((taskParams.stopFreq - taskParams.startFreq) > CConfig::C_40_MHZ))
					{
						if(m_config->IsInShfExtFreqRange(taskParams.stopFreq))
						{
							//Forced to use 40 MHz Process Bandwidth since the
							// analyzer B/W is limited to 40 MHz.
							force40MHzProcBw = true;
						}
					}
				}

				CheckAntenna(eAnt, taskParams.hf);

				if (taskParams.rxHwBw < taskParams.stopFreq - taskParams.startFreq)
				{
					// Use wideband mode
					taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, false);
				}
				else
				{
					taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, true);
				}

				if (taskParams.rxHwBw < taskParams.bw)
				{
					throw ErrorCodes::INVALIDBANDWIDTH;
				}

				//Only check if the band is not excluded.
				if (!cmd.band[band].exclude &&
					m_config->InRestrictedFreqRange(taskParams.startFreq, taskParams.stopFreq))
				{
					throw ErrorCodes::FREQUENCYRESTRICTED;
				}

				auto modTaskParams(taskParams);

				if(force40MHzProcBw)
				{
					modTaskParams.stopFreq = modTaskParams.startFreq + CConfig::C_40_MHZ;
				}

				// Have enough of taskParams to retrieve ProcParams
				auto procParams = m_config->GetProcParams(modTaskParams);
				auto rxProcBw = GetProcBw(modTaskParams, procParams.rxProcBw);
				if (cmd.band[band].exclude)
				{
					taskParams.numBlocks = 0;
				}
				else
				{
					taskParams.numBlocks = static_cast<unsigned int>((taskParams.stopFreq - taskParams.startFreq) / rxProcBw);
					if (taskParams.numBlocks * rxProcBw < taskParams.stopFreq - taskParams.startFreq) ++taskParams.numBlocks;
				}
				taskParams.withIq = false;
				taskParams.withPsd = true;
				taskParams.withFft = false;
				m_taskParams.push_back(taskParams);
				m_procParams.push_back(procParams);

#ifdef CSMS_DEBUG
				static Units::Frequency l_prevFreq(0);

				if(l_prevFreq != taskParams.startFreq)
				{
					l_prevFreq = taskParams.startFreq;
					TRACE("CTask::InitAvdCtrl: eAnt = %u startFreq = %fMhz stopFreq = %fMhz rxHwBw = %fMhz rxProcBw = %f numBlocks =%u\n",
					      eAnt, taskParams.startFreq.Hz<float>()/1000000, taskParams.stopFreq.Hz<float>()/1000000,
					      taskParams.rxHwBw.Hz<float>()/1000000, rxProcBw.Hz<float>(), taskParams.numBlocks);

		}
#endif

			}

			if (!someIncludes)
			{
				throw ErrorCodes::AVDNOBANDS;
			}
		}

		break;

	case SEquipCtrlMsg::GET_TASK_STATE:
		if (msg->hdr.bodySize != sizeof(SEquipCtrlMsg::STaskIdKey))
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		TaskIdKeyCheck(msg->body.taskIdKey);
		break;

	case SEquipCtrlMsg::RESUME_OCCUPANCY:
		if (msg->hdr.bodySize != sizeof(SEquipCtrlMsg::STaskIdKey))
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		TaskIdKeyCheck(msg->body.taskIdKey);
		break;

	case SEquipCtrlMsg::SUSPEND_OCCUPANCY:
		if (msg->hdr.bodySize != sizeof(SEquipCtrlMsg::STaskIdKey))
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		TaskIdKeyCheck(msg->body.taskIdKey);
		break;

	default:
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate and initialize a BIST_CTRL task
//
void CTask::InitBistCtrl(_In_ const SEquipCtrlMsg* msg)
{
	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_BIST:
	case SEquipCtrlMsg::GET_DIAGNOSTICS:
	case SEquipCtrlMsg::GET_BIST_RESULT:
		{
			m_taskGroup = ETaskGroup::SYSTEM;

			// Check message length and requested response version
			if(msg->hdr.bodySize != 0 && msg->hdr.bodySize != 4)
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			CConfig::STaskParams taskParams;
			taskParams.mode = CConfig::EMode::BIST;
			taskParams.numBlocks = 1;

			//Put in Vertical as default.
			taskParams.pol = CConfig::EPolarization::VERT;
			m_taskParams.push_back(taskParams);
		}

		break;

	default:
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	RegisterTask(false);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate and initialize a DEMOD_CTRL task
//
void CTask::InitDemodCtrl(_In_ const SEquipCtrlMsg* msg)
{
	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::AGC_ONOFF:
		// Check message length
		if(msg->hdr.bodySize != sizeof(SEquipCtrlMsg::SAgcOnOffCmd))
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		break;

	case SEquipCtrlMsg::SET_LAN_AUDIO:
		throw ErrorCodes::NETWORKNOTENABLED;

	case SEquipCtrlMsg::SET_RCVR:
		{
			const SEquipCtrlMsg::SRcvrCtrlCmd& cmd = msg->body.rcvrCtrlCmd;

			// Check message length and requested response version
			if(msg->hdr.bodySize != sizeof(SEquipCtrlMsg::SRcvrCtrlCmd))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			// Check detection mode
#ifdef CSMS_2016
			if (!RangeCheck(cmd.detMode, SEquipCtrlMsg::SRcvrCtrlCmd::OFF, SEquipCtrlMsg::SRcvrCtrlCmd::WIDE_IQ))
#else
			if(!RangeCheck(cmd.detMode, SEquipCtrlMsg::SRcvrCtrlCmd::OFF, SEquipCtrlMsg::SRcvrCtrlCmd::EXTERNAL))
#endif
			{
				throw ErrorCodes::INVALIDDETMODE;
			}

			// Check agcTime
			if(!RangeCheck(cmd.agcTime, 0ul, 100000ul))
			{
				throw ErrorCodes::INVALIDAGCTIME;
			}

			// Check BFO
			if (cmd.detMode == SEquipCtrlMsg::SRcvrCtrlCmd::CW &&
				!RangeCheck(Units::Frequency(cmd.bfo), -Units::Frequency(8000), Units::Frequency(8000)))
			{
				throw ErrorCodes::INVALIDBFO;
			}

			// Get processing parameters
			CConfig::STaskParams taskParams;
			taskParams.pol = CConfig::GetAntPolarization(m_ant);
			taskParams.bw = cmd.bandwidth;
			taskParams.hf = ValidateFrequency(cmd.freq, cmd.bandwidth, taskParams.pol == CConfig::EPolarization::HORIZ, false, m_ant);
			taskParams.mode = CConfig::EMode::DEMOD;
			taskParams.numBlocks = 1;
			taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, true);
			taskParams.startFreq = taskParams.stopFreq = cmd.freq;
			m_taskParams.push_back(taskParams);
			m_procParams.push_back(m_config->GetProcParams(taskParams));

			// Check if detection mode is valid for this bw
			if (!m_config->ValidateDemod(taskParams.hf, cmd.detMode, taskParams.bw))
			{
				throw ErrorCodes::INVALIDDETMODE;
			}
//			m_demodDecimation = m_procParams[0].decimations;
//			if (cmd.detMode == SEquipCtrlMsg::SRcvrCtrlCmd::EXTERNAL)
//			{
//				m_demodDecimation.ddcCICDecimation = m_procParams[0].iqCic;
//			}
		}

		break;

	case SEquipCtrlMsg::SET_RCVR_MAN:
		{
			const SEquipCtrlMsg::SRcvrManCmd& cmd = msg->body.rcvrManCmd;

			// Check message length and requested response version
			if(msg->hdr.bodySize != sizeof(SEquipCtrlMsg::SRcvrManCmd))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			// ValidateFrequency is called below.

			Units::Frequency freq(cmd.freq);
			bool hf = ValidateFrequency(cmd.freq, cmd.bandwidth, false, false, SEquipCtrlMsg::ANT1);
			unsigned char maxAtten = m_radioEquip->GetMaxMaxAtten(m_config->GetGainMode(freq, hf));

			// Check atten
			if (!RangeCheck<unsigned long>(cmd.atten, 0ul, maxAtten))
			{
				throw ErrorCodes::INVALIDRFATTEN;
			}

			// Get processing parameters
			CConfig::STaskParams taskParams;
			taskParams.hf = hf;
			taskParams.pol = CConfig::EPolarization::VERT;
			taskParams.mode = CConfig::EMode::PAN; // Same parameters as PAN
			AdjustShfExtPanTaskParams(taskParams, freq, cmd.bandwidth);
			taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, true);
			taskParams.startFreq = taskParams.stopFreq = freq;
			m_taskParams.push_back(taskParams);
			m_procParams.push_back(m_config->GetProcParams(taskParams));
		}

		break;

	case SEquipCtrlMsg::SET_PAN_PARA:
		{
			// Check message length and requested response version
			if (msg->hdr.bodySize != sizeof(SEquipCtrlMsg::SPanParaCmd))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

//			ChkPanAntValue(msg->body.panParaCmd.antenna);
			const SEquipCtrlMsg::SRcvrCtrlCmd& cmd = msg->body.panParaCmd.rcvr;
			m_cmdAnt = msg->body.panParaCmd.antenna;

			// Check detection mode
#ifdef CSMS_2016
			if (!RangeCheck(cmd.detMode, SEquipCtrlMsg::SRcvrCtrlCmd::OFF, SEquipCtrlMsg::SRcvrCtrlCmd::WIDE_IQ))
#else
			if(!RangeCheck(cmd.detMode, SEquipCtrlMsg::SRcvrCtrlCmd::OFF, SEquipCtrlMsg::SRcvrCtrlCmd::EXTERNAL))
#endif
			{
				throw ErrorCodes::INVALIDDETMODE;
			}

			// Check agcTime
			if(!RangeCheck(cmd.agcTime, 0ul, 100000ul))
			{
				throw ErrorCodes::INVALIDAGCTIME;
			}

			// Check BFO
			if((cmd.detMode == SEquipCtrlMsg::SRcvrCtrlCmd::CW || cmd.detMode == SEquipCtrlMsg::SRcvrCtrlCmd::LSB ||
				cmd.detMode == SEquipCtrlMsg::SRcvrCtrlCmd::USB) &&
				!RangeCheck(Units::Frequency(cmd.bfo), -Units::Frequency(8000), Units::Frequency(8000)))
			{
				throw ErrorCodes::INVALIDBFO;
			}

			// Get processing parameters
			CConfig::STaskParams taskParams;
			taskParams.pol = CConfig::GetAntPolarization(m_ant);
			taskParams.bw = cmd.bandwidth;
			bool forcedVuhf = (m_config->IsVushfAntSpecified(msg->body.panParaCmd.antenna) ? true : false);
			taskParams.hf = ValidateFrequency(cmd.freq, cmd.bandwidth, taskParams.pol == CConfig::EPolarization::HORIZ, false, m_ant, forcedVuhf);
			taskParams.mode = CConfig::EMode::DEMOD;
			taskParams.numBlocks = 1;
			taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, true);

			if(taskParams.rxHwBw < taskParams.bw)
			{
				taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, false);
			}

			if(m_config->HasShfExt() && m_ant == SEquipCtrlMsg::EAnt::ANT2 &&
					taskParams.rxHwBw > CConfig::C_40_MHZ)
			{
				throw ErrorCodes::INVALIDBANDWIDTH;
			}

			taskParams.startFreq = taskParams.stopFreq = cmd.freq;

			if (forcedVuhf && taskParams.hf)
			{
				//Multiple antenna system does not do auto-switch
				// into HF, therefore if Horizontal antenna is
				// specified, hf flag must not be set, otherwise
				// CheckAntenna will throw invalid antenna
				taskParams.hf = false;
			}
			CheckAntenna(m_ant, taskParams.hf);

			// Check if detection mode is valid for this bw
			if (!m_config->ValidateDemod(taskParams.hf, cmd.detMode, taskParams.bw))
			{
				throw ErrorCodes::INVALIDDETMODE;
			}

			m_taskParams.push_back(taskParams);
			m_procParams.push_back(m_config->GetProcParams(taskParams));
			return;
		}

		break;

	case SEquipCtrlMsg::SET_AUDIO_PARAMS:
		{
			// Check message length and requested response version
			if (msg->hdr.bodySize != sizeof(SEquipCtrlMsg::SAudioParamsCmd))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			const SEquipCtrlMsg::SAudioParamsCmd& cmd = msg->body.audioParamsCmd;

			// Check channel number
#ifdef CSMS_2016
			if (!cmd.anyChannel && !RangeCheck(cmd.channel, 0UL, C3230::m_configData.numAudioChannels * 2))
#else
			if (!cmd.anyChannel && !RangeCheck(cmd.channel, 0UL, C3230::m_configData.numAudioChannels * 2))
#endif
			{
				throw ErrorCodes::INVALIDAUDIOCHANNEL;
			}

			// Check detection mode
#ifdef CSMS_2016
			if (!RangeCheck(cmd.detMode, SEquipCtrlMsg::SRcvrCtrlCmd::OFF, SEquipCtrlMsg::SRcvrCtrlCmd::WIDE_IQ))
#else

			if (!RangeCheck(cmd.detMode, SEquipCtrlMsg::SRcvrCtrlCmd::OFF, SEquipCtrlMsg::SRcvrCtrlCmd::EXTERNAL))
#endif
			{
				throw ErrorCodes::INVALIDDETMODE;
			}

			// Check BFO
			if ((cmd.detMode == SEquipCtrlMsg::SRcvrCtrlCmd::CW || cmd.detMode == SEquipCtrlMsg::SRcvrCtrlCmd::LSB ||
				cmd.detMode == SEquipCtrlMsg::SRcvrCtrlCmd::USB) &&
				!RangeCheck(Units::Frequency(cmd.bfo), -Units::Frequency(8000), Units::Frequency(8000)))
			{
				throw ErrorCodes::INVALIDBFO;
			}

			// Get processing parameters
			CConfig::STaskParams taskParams;
			taskParams.pol = CConfig::GetAntPolarization(m_ant);
			taskParams.bw = cmd.bandwidth;
			taskParams.hf = ValidateFrequency(cmd.freq, cmd.bandwidth, taskParams.pol == CConfig::EPolarization::HORIZ, false, m_ant);
			taskParams.mode = CConfig::EMode::DEMOD;
			taskParams.numBlocks = 1;
			taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, true);
			taskParams.startFreq = taskParams.stopFreq = cmd.freq;
			m_taskParams.push_back(taskParams);
#ifdef CSMS_2016
			if (cmd.detMode == SEquipCtrlMsg::SRcvrCtrlCmd::EXTERNAL || cmd.detMode == SEquipCtrlMsg::SRcvrCtrlCmd::WIDE_IQ)
#else
			if (cmd.detMode == SEquipCtrlMsg::SRcvrCtrlCmd::EXTERNAL)
#endif
			{
				// Get the first entry from ProcParams that matches everything except bandwidth and then adjust decimation
				// Note: we are done with local data taskParams at this point so we modify it.
				taskParams.bw = 0;
				auto procParams = m_config->GetProcParams(taskParams, true);
				procParams.iqCic = CDigitizer::AdjustAudioDdcDecimation(cmd.bandwidth, procParams.decimations);
				m_procParams.push_back(procParams);
			}
			else
			{
				m_procParams.push_back(m_config->GetProcParams(taskParams));

				// Check if detection mode is valid for this bw
				if (!m_config->ValidateDemod(taskParams.hf, cmd.detMode, taskParams.bw))
				{
					throw ErrorCodes::INVALIDDETMODE;
				}
			}
			return;
		}

	case SEquipCtrlMsg::FREE_AUDIO_CHANNEL:
		{
			// Check message length
			if (msg->hdr.bodySize != sizeof(SEquipCtrlMsg::SFreeAudioChannelCmd))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			const SEquipCtrlMsg::SFreeAudioChannelCmd& cmd = msg->body.freeAudioChannelCmd;

			// Check channel number
#ifdef CSMS_2016
			if (!RangeCheck(cmd.channel, 0UL, C3230::m_configData.numAudioChannels * 2))
#else
			if (!RangeCheck(cmd.channel, 0UL, C3230::m_configData.numAudioChannels))
#endif
			{
				throw ErrorCodes::INVALIDAUDIOCHANNEL;
			}
			return;
		}

	default:
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate and initialize a DF_CTRL task
//
void CTask::InitDfCtrl(_In_ const SEquipCtrlMsg* msg)
{
	switch(msg->hdr.msgSubType)
	{
#if 0
	case SEquipCtrlMsg::GET_IONOGRAM:
		// Check message length
		if(msg->hdr.bodySize != sizeof(SEquipCtrlMsg::SGetIonogram))
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		break;
#endif
	case SEquipCtrlMsg::INITIALIZE_DF:
		// NOTE: Currently only handles the "full" pbcal
		{
			if (!CSingleton<CProcessorNode>()->AreNodesReady())
			{
				throw ErrorCodes::HARDWARENOTPRESENT;
			}

			m_taskGroup = (msg->body.initializeDf.numIntegrations == 0 ? ETaskGroup::SYSTEM : ETaskGroup::BKGD);

			// Check message length
			if (msg->hdr.bodySize != sizeof(SEquipCtrlMsg::SInitializeDf))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			unsigned int numIntegrations =
				(msg->body.initializeDf.numIntegrations == 0 ? CPbCal::MIN_INTEGRATIONS_DF : msg->body.initializeDf.numIntegrations);
			m_ant = SEquipCtrlMsg::ANT1;

			// VUSHF pb calibration
			// Need to have a df antenna
			if (CAntenna::IsDfAntenna(m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get()) &&
				Units::Frequency(msg->body.initializeDf.freq) == 0)
			{
				CConfig::STaskParams taskParams;
				taskParams.bw = 0;
				taskParams.mode = CConfig::EMode::PB_CAL;
				taskParams.hf = false;
				taskParams.pol = CConfig::EPolarization::VERT;
				taskParams.numBlocks = numIntegrations;
				taskParams.withIq = false;
				taskParams.withPsd = true;
				taskParams.withFft = true;

				Units::Frequency prevHwBw = 0;
				bool narrow = false;
				for (size_t ibw = 0; ibw < 2; ++ibw)	// Two possible bandwidths
				{
					taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_mutableConfig, taskParams.hf, narrow); // wide then narrow
					if (taskParams.rxHwBw == prevHwBw)
						break;

					for (size_t step = 0; step < std::extent<decltype(CPbCal::VUSHF_CAL_FREQS)>::value; ++step)
					{
						if (m_config->IsFreqInRange(CPbCal::VUSHF_CAL_FREQS[step], false, true, SEquipCtrlMsg::EAnt::ANT1))
						{
							taskParams.startFreq = CPbCal::VUSHF_CAL_FREQS[step] - taskParams.rxHwBw / 2;
							taskParams.stopFreq = CPbCal::VUSHF_CAL_FREQS[step] + taskParams.rxHwBw / 2;
							m_taskParams.push_back(taskParams);
							m_procParams.push_back(m_config->GetProcParams(taskParams));
						}
					}
					prevHwBw = taskParams.rxHwBw;
					narrow = !narrow;
				}
			}
			// HF pb calibration
			// Need to have a df antenna
			if (CAntenna::IsDfAntenna(m_mutableConfig.hfConfig.antCable.antenna.get()) &&
				Units::Frequency(msg->body.initializeDf.freq) == 0)
			{
				CConfig::STaskParams taskParams;
				taskParams.bw = 0;
				taskParams.mode = CConfig::EMode::PB_CAL;
				taskParams.hf = true;
				taskParams.pol = CConfig::EPolarization::VERT;
				taskParams.numBlocks = numIntegrations;
				taskParams.withIq = false;
				taskParams.withPsd = true;
				taskParams.withFft = true;
				taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_mutableConfig, taskParams.hf, false);
				for (size_t step = 0; step < std::extent<decltype(CPbCal::HF_CAL_FREQS)>::value; ++step)
				{
					taskParams.startFreq = CPbCal::HF_CAL_FREQS[step][0];
					taskParams.stopFreq = CPbCal::HF_CAL_FREQS[step][1];
					m_taskParams.push_back(taskParams);
					m_procParams.push_back(m_config->GetProcParams(taskParams));
				}
			}
		}
		if (m_taskParams.empty())
		{
			throw ErrorCodes::HARDWARENOTPRESENT;
		}

		break;

#if 0
		case SEquipCtrlMsg::LOAD_IONOGRAM:
		// Check message length
		if(msg->hdr.bodySize != sizeof(SEquipCtrlMsg::SLoadIonogram))
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		if(msg->body.loadIonogram.ionoData.finishedIonogram.numPoints > SEquipCtrlMsg::SIonogramDataMsg::MAX_IONOGRAM_POINTS)
		{
			throw ErrorCodes::INVALIDFREQUENCYBAND;
		}

		break;
#endif
	default:
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate and initialize an EQUIPMENT_CTRL task
//
void CTask::InitEquipmentCtrl(_In_ const SEquipCtrlMsg* msg)
{
	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::TASK_TERMINATE:
	case SEquipCtrlMsg::GET_TASK_STATUS:
		if (msg->hdr.bodySize != sizeof(SEquipCtrlMsg::STaskIdKey))
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		TaskIdKeyCheck(msg->body.taskIdKey);
		break;

	default:
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;

}


//////////////////////////////////////////////////////////////////////
//
// Validate and initialize a METRICS_CTRL task
//
void CTask::InitMetricsCtrl(_In_ const SEquipCtrlMsg* msg)
{
	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_BAND_REQUEST:
		// Check message length
		if(msg->hdr.bodySize != 0 && msg->hdr.bodySize != 4)
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		break;


	case SEquipCtrlMsg::GET_DWELL:
		{
			// Check message length
			if(msg->hdr.bodySize != sizeof(SEquipCtrlMsg::SGetDwellCmd))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			// Task and processing parameters
			CConfig::STaskParams taskParams;
			taskParams.mode = CConfig::EMode::MEASURE;
			taskParams.startFreq = taskParams.stopFreq = msg->body.getDwellCmd.freq;
			taskParams.hf = ValidateFrequency(msg->body.getDwellCmd.freq, msg->body.getDwellCmd.bandwidth, false, false, SEquipCtrlMsg::ANT1);

			//@Note: Initialize to VERT so that it has valid value, as GET_DWELL
			//       does not use polarization value.
			taskParams.pol = CConfig::EPolarization::VERT;
			taskParams.bw = msg->body.getDwellCmd.bandwidth;
			taskParams.numBlocks = 1;
			taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_mutableConfig, taskParams.hf, true);

			if(taskParams.rxHwBw < msg->body.getDwellCmd.bandwidth)
			{
				taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_mutableConfig, taskParams.hf, false);
			}

			m_taskParams.push_back(taskParams);
			m_procParams.push_back(m_config->GetProcParams(taskParams));
		}

		break;

		case SEquipCtrlMsg::GET_GPS:
		// Check message length
		if (msg->hdr.bodySize != 0 && msg->hdr.bodySize != 4)
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		break;

		case SEquipCtrlMsg::GET_HEADING:
		// Check message length
		if(msg->hdr.bodySize != 0 && msg->hdr.bodySize != 4)
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		break;

	case SEquipCtrlMsg::GET_MEAS:
	case SEquipCtrlMsg::VALIDATE_MEAS:
		{
			const SEquipCtrlMsg::SGetMeasCmd& cmd = msg->body.getMeasCmd;
			m_taskGroup = (cmd.iqCmd.tdoa ? ETaskGroup::SYSTEM : ETaskGroup::MEASURE);
			m_rcvrAtten = cmd.rcvrAtten;
			auto multRf1Input = m_config->IsProcWithAntSelectionSwitch();
			bool translatedAnt = false;
			bool forcedHf = false;
			bool forcedVuhf = false;
			m_cmdAnt = cmd.ant;

			if(multRf1Input)
			{
				translatedAnt = m_config->TranslateVerify5143MultAntValue(cmd.ant,
						m_ant, forcedHf, forcedVuhf);
			}
			else
			{
				m_ant = cmd.ant;
			}

			CConfig::EPolarization pol = CConfig::GetAntPolarization(m_ant);

			if(translatedAnt)
			{
				if(forcedHf)
				{
					if(!m_config->IsInHfFreqRange(cmd.freq))
					{
						throw ErrorCodes::INVALIDFREQUENCY;
					}
				}
				else
				{
					if(!m_config->IsFreqAboveVuhfMin(m_ant, cmd.freq))
					{
						throw ErrorCodes::INVALIDFREQUENCY;
					}
				}
			}
			else if(m_config->HasShfExt() && m_ant == SEquipCtrlMsg::EAnt::ANT2 &&
					Units::Frequency(cmd.bandwidth) > CConfig::C_40_MHZ)
			{
				throw ErrorCodes::INVALIDBANDWIDTH;
			}

			// Check frequency.
			bool hf = ValidateFrequency(cmd.freq, cmd.bandwidth, pol != CConfig::EPolarization::VERT, false, m_ant, forcedVuhf);

			if(translatedAnt)
			{
				hf = forcedHf;
			}


			// Check bandwidth for VLF
			if(cmd.bandwidth / 2 + 1000 > cmd.freq)
			{
				throw ErrorCodes::INVALIDBANDWIDTH;
			}

			if (cmd.iqCmd.outputType != SEquipCtrlMsg::NONE)
			{
				// IQ Data command
				if (!RangeCheck(cmd.iqCmd.numSamples, 32ul, 2000000ul))
				{
					throw ErrorCodes::INVALIDSAMPLESIZE;
				}

//				if (!RangeCheck(cmd.iqCmd.bwFactor, 0.1f, 16.f))
				if (cmd.iqCmd.bwFactor != 1.0f)		// Only allow bwFactor = 1.
				{
					throw ErrorCodes::INVALIDBWFACTOR;
				}

				// Check antenna
				CheckAntenna(m_ant, hf);

				// Task and processing parameters
				CConfig::STaskParams taskParams;
				taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_mutableConfig, hf, true);
				if (taskParams.rxHwBw < Units::Frequency(Units::Frequency(cmd.bandwidth).Hz<double>() * double(cmd.iqCmd.bwFactor)))
				{
					taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_mutableConfig, hf, false);
				}

				taskParams.pol = pol;
				taskParams.mode = CConfig::EMode::IQDATA;
				taskParams.startFreq = taskParams.stopFreq = cmd.freq;
				taskParams.hf = hf;
				taskParams.bw = cmd.bandwidth; // doesn't include bwFactor
				taskParams.numBlocks = 1;
				taskParams.withIq = true;
				taskParams.withPsd = false;
				taskParams.withFft = false;
				m_taskParams.push_back(taskParams);

				CConfig::SProcParams procParams = m_config->GetProcParams(taskParams); // Used to have bestBw = true

				if (!C3230::m_configData.isLoaded)
				{
					throw ErrorCodes::HARDWARENOTPRESENT;
				}
				procParams.sampleSize = cmd.iqCmd.numSamples;  // Will be updated by CMeasurementTask constructor if TDOA measurement
				// Limit iq collection to 1000 seconds.
				if (procParams.sampleSize > 1000. * procParams.sampleRate.Hz<double>() / procParams.decimations.GetTotal())
				{
					throw ErrorCodes::INVALIDSAMPLESIZE;
				}
				m_procParams.push_back(procParams);
			}

			// Check measurements
			bool someNonDf = false;
			bool needIq = false;
			bool needPsd = false;

			if (cmd.bwCmd.outputType != SEquipCtrlMsg::NONE)
			{
				// Bandwidth command
				if (!RangeCheck(cmd.bwCmd.dwellTime, 50ul, 10000ul))
				{
					throw ErrorCodes::INVALIDDWELLTIME;
				}

				if (!RangeCheck(cmd.bwCmd.betaParam, 800ul, 999ul))
				{
					throw ErrorCodes::INVALIDBETAPARAM;
				}

				if (!RangeCheck(cmd.bwCmd.yParam, 0ul, 600ul))
				{
					throw ErrorCodes::INVALIDYPARAM;
				}

				if (!RangeCheck(cmd.bwCmd.x1Param, 0ul, 600ul))
				{
					throw ErrorCodes::INVALIDX1PARAM;
				}

				if (!RangeCheck(cmd.bwCmd.x2Param, 0ul, 600ul))
				{
					throw ErrorCodes::INVALIDX2PARAM;
				}

				if (!RangeCheck(cmd.bwCmd.repeatCount, 1ul, 1000ul))
				{
					throw ErrorCodes::INVALIDREPEATCOUNT;
				}

				if (cmd.bwCmd.aveMethod != SEquipCtrlMsg::MEAN_AVE &&
					cmd.bwCmd.aveMethod != SEquipCtrlMsg::RMS_AVE &&
					cmd.bwCmd.aveMethod != SEquipCtrlMsg::MAX_HOLD)
				{
					throw ErrorCodes::INVALIDAVEMETHOD;
				}

				someNonDf = true;
				needPsd = true;
			}

			if (cmd.freqCmd.outputType != SEquipCtrlMsg::NONE)
			{
				// Frequency command
				if (!RangeCheck(cmd.freqCmd.dwellTime, 50ul, 10000ul))
				{
					throw ErrorCodes::INVALIDDWELLTIME;
				}

				if (cmd.freqCmd.freqMethod != SEquipCtrlMsg::SGetFreqCmd::IFM &&
					cmd.freqCmd.freqMethod != SEquipCtrlMsg::SGetFreqCmd::FFT)
				{
					throw ErrorCodes::INVALIDFREQMETHOD;
				}

				if (!RangeCheck(cmd.freqCmd.repeatCount, 1ul, 1000ul))
				{
					throw ErrorCodes::INVALIDREPEATCOUNT;
				}

				if (cmd.freqCmd.aveMethod != SEquipCtrlMsg::MEAN_AVE &&
					cmd.freqCmd.aveMethod != SEquipCtrlMsg::RMS_AVE &&
					cmd.freqCmd.aveMethod != SEquipCtrlMsg::MAX_HOLD)
				{
					throw ErrorCodes::INVALIDAVEMETHOD;
				}

				someNonDf = true;
				if (cmd.freqCmd.freqMethod == SEquipCtrlMsg::SGetFreqCmd::IFM)
				{
					if (m_config->HasEnhancedDynamicRange())
					{
						needPsd = true;
					}
					else
					{
						needIq = true;
					}
				}
				else if (cmd.freqCmd.freqMethod == SEquipCtrlMsg::SGetFreqCmd::FFT)
				{
					needPsd = true;
				}
			}

			if (cmd.modulationCmd.outputType != SEquipCtrlMsg::NONE)
			{
				// Modulation command
				if (!RangeCheck(cmd.modulationCmd.dwellTime, 50ul, 10000ul))
				{
					throw ErrorCodes::INVALIDDWELLTIME;
				}

				if (!RangeCheck(cmd.modulationCmd.repeatCount, 1ul, 1000ul))
				{
					throw ErrorCodes::INVALIDREPEATCOUNT;
				}

				if (cmd.modulationCmd.aveMethod != SEquipCtrlMsg::MEAN_AVE &&
					cmd.modulationCmd.aveMethod != SEquipCtrlMsg::RMS_AVE &&
					cmd.modulationCmd.aveMethod != SEquipCtrlMsg::MAX_HOLD)
				{
					throw ErrorCodes::INVALIDAVEMETHOD;
				}

				someNonDf = true;
				needIq = true;
			}

			if (cmd.fieldStrengthCmd.outputType != SEquipCtrlMsg::NONE)
			{
				// Field strength command
				if (!RangeCheck(cmd.fieldStrengthCmd.dwellTime, 50ul, 10000ul))
				{
					throw ErrorCodes::INVALIDDWELLTIME;
				}

				if (!RangeCheck(cmd.fieldStrengthCmd.repeatCount, 1ul, 1000ul))
				{
					throw ErrorCodes::INVALIDREPEATCOUNT;
				}

				if (cmd.fieldStrengthCmd.aveMethod != SEquipCtrlMsg::MEAN_AVE &&
					cmd.fieldStrengthCmd.aveMethod != SEquipCtrlMsg::RMS_AVE &&
					cmd.fieldStrengthCmd.aveMethod != SEquipCtrlMsg::MAX_HOLD)
				{
					throw ErrorCodes::INVALIDAVEMETHOD;
				}

				if (cmd.fieldStrengthCmd.fieldMethod != SEquipCtrlMsg::LINAVERAGE &&
					cmd.fieldStrengthCmd.fieldMethod != SEquipCtrlMsg::LOGAVERAGE &&
					cmd.fieldStrengthCmd.fieldMethod != SEquipCtrlMsg::RMS &&
					cmd.fieldStrengthCmd.fieldMethod != SEquipCtrlMsg::PEAK)
				{
					throw ErrorCodes::INVALIDFIELDMETHOD;
				}

				someNonDf = true;
				if (m_config->HasEnhancedDynamicRange())
				{
					needPsd = true;
				}
				else
				{
					needIq = true;
				}
			}

			if (someNonDf)
			{
				// Check antenna		- already did this if iqdata requested.
				CheckAntenna(m_ant, hf);

				// Task and processing parameters
				CConfig::STaskParams taskParams;
				taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), hf, true);
				taskParams.pol = pol;

				if (taskParams.rxHwBw < cmd.bandwidth)
				{
					taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), hf, false);
				}

				taskParams.mode = CConfig::EMode::MEASURE;
				taskParams.startFreq = taskParams.stopFreq = cmd.freq;
				taskParams.hf = hf;
				taskParams.bw = cmd.bandwidth;
				taskParams.numBlocks = 1; // Will be updated by CMeasurementTask constructor
				taskParams.withIq = needIq;
				taskParams.withPsd = needPsd;
				taskParams.withFft = false;
				m_taskParams.push_back(taskParams);
				m_procParams.push_back(m_config->GetProcParams(taskParams));
			}

			// Check DF
			if (cmd.dfCmd.outputType != SEquipCtrlMsg::SGetMeasCmd::SGetDfCmd::NONE)
			{
				if (!CSingleton<CProcessorNode>()->AreNodesReady())
				{
					throw ErrorCodes::HARDWARENOTPRESENT;
				}

				if (hf && Units::Frequency(cmd.freq) < m_config->GetHfDfVLowFreq())
				{
					throw ErrorCodes::INVALIDFREQUENCY;
				}

				if (!RangeCheck(cmd.dfCmd.dwellTime, 50ul, 100000ul))
				{
					throw ErrorCodes::INVALIDDWELLTIME;
				}

				if (!RangeCheck(cmd.dfCmd.repeatCount, 1ul, 500ul))
				{
					throw ErrorCodes::INVALIDREPEATCOUNT;
				}

				if (!RangeCheck(cmd.dfCmd.confThreshold, uint8_t(0u), uint8_t(100u)))
				{
					throw ErrorCodes::INVALIDCONFTHRESHOLD;
				}

				if (Units::Frequency(cmd.bandwidth) != 0 && Units::Frequency(cmd.dfCmd.dfBandwidth) > Units::Frequency(cmd.bandwidth))
				{
					throw ErrorCodes::INVALIDDFBANDWIDTH;
				}

				CConfig::STaskParams taskParams;
				taskParams.mode = CConfig::EMode::DF;
				taskParams.startFreq = taskParams.stopFreq = cmd.freq;
				taskParams.hf = hf;

				if (cmd.dfCmd.outputType == SEquipCtrlMsg::SGetMeasCmd::SGetDfCmd::SUMMARY_V ||
					cmd.dfCmd.outputType == SEquipCtrlMsg::SGetMeasCmd::SGetDfCmd::CUTS_V ||
					cmd.dfCmd.outputType == SEquipCtrlMsg::SGetMeasCmd::SGetDfCmd::VOLTS_V)
				{
					taskParams.pol = CConfig::EPolarization::VERT;
				}
				else if (cmd.dfCmd.outputType == SEquipCtrlMsg::SGetMeasCmd::SGetDfCmd::SUMMARY_H ||
					cmd.dfCmd.outputType == SEquipCtrlMsg::SGetMeasCmd::SGetDfCmd::CUTS_H ||
					cmd.dfCmd.outputType == SEquipCtrlMsg::SGetMeasCmd::SGetDfCmd::VOLTS_H)
				{
					taskParams.pol = CConfig::EPolarization::HORIZ;
				}
				else if (cmd.dfCmd.outputType == SEquipCtrlMsg::SGetMeasCmd::SGetDfCmd::SUMMARY_VH ||
					cmd.dfCmd.outputType == SEquipCtrlMsg::SGetMeasCmd::SGetDfCmd::CUTS_VH ||
					cmd.dfCmd.outputType == SEquipCtrlMsg::SGetMeasCmd::SGetDfCmd::VOLTS_VH)
				{
					taskParams.pol = CConfig::EPolarization::BOTH;
				}
				else
				{
					throw ErrorCodes::INVALIDSIGNALTYPE;
				}

				if (taskParams.hf && !CAntenna::IsDfAntenna(m_mutableConfig.hfConfig.antCable.antenna.get()))
				{
					// No HF DF antenna - try to force V/UHF
					try
					{
						taskParams.hf = ValidateFrequency(cmd.freq, cmd.bandwidth, taskParams.pol != CConfig::EPolarization::VERT, true, cmd.ant, true);
					}
					catch(ErrorCodes::EErrorCode)
					{
						throw ErrorCodes::HARDWARENOTPRESENT;
					}
				}

				if ((taskParams.hf && !CAntenna::IsDfAntenna(m_mutableConfig.hfConfig.antCable.antenna.get())) ||
					(!taskParams.hf && !CAntenna::IsDfAntenna(m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get())))
				{
					throw ErrorCodes::HARDWARENOTPRESENT;
				}

				if (taskParams.pol != CConfig::EPolarization::VERT)
				{
//					if (taskParams.hf || !m_config->HasVushfHorizPolorization(taskParams.stopFreq))
					if (taskParams.hf || !CAntenna::HasHorizontalPolarization(
						m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get(), taskParams.stopFreq))
					{
						throw ErrorCodes::HARDWARENOTPRESENT;
					}
				}

				ValidateFrequency(cmd.freq, cmd.dfCmd.dfBandwidth, pol != CConfig::EPolarization::VERT, true, m_ant, forcedVuhf);

				taskParams.bw = std::max(Units::Frequency(cmd.bandwidth), Units::Frequency(cmd.dfCmd.dfBandwidth));
				taskParams.numBlocks = 1; // Will be updated by CMeasurementTask constructor
				taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, true);

				if (taskParams.rxHwBw < taskParams.bw)
				{
					taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, false);
				}

				taskParams.withIq = false;
				taskParams.withPsd = true;
				taskParams.withFft = true;
				CConfig::SProcParams procParams = m_config->GetProcParams(taskParams, true); // Round DF bandwidth up to next legal value
				m_taskParams.push_back(taskParams);
				m_procParams.push_back(procParams);
#ifdef CSMS_DEBUG
				TRACE("CTask::InitMetricsCtrl DF: dwellTime %lu, cuts %lu; dfBandwidth %f, cmd.bw = %fHz, Bw %f,rxHwBw %f, outputType %d, src %d, thres %u\n",
						cmd.dfCmd.dwellTime, cmd.dfCmd.repeatCount, Units::Frequency(cmd.dfCmd.dfBandwidth).Hz<float>(),
						Units::Frequency(cmd.bandwidth).Hz<float>(),
						taskParams.bw.Hz<float>(), taskParams.rxHwBw.Hz<float>(),
						int(cmd.dfCmd.outputType), int(cmd.dfCmd.srcOfRequest), unsigned(cmd.dfCmd.confThreshold));
#endif
			}
		}

		break;

	case SEquipCtrlMsg::INIT_FIELDSTRENGTH:
		// Check message length and requested response version
		if(msg->hdr.bodySize != 0 && msg->hdr.bodySize != 4)
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		break;

	default:
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate and initialize an OCCUPANCY_CTRL task
//
void CTask::InitOccupancyCtrl(_In_ const SEquipCtrlMsg* msg)
{
	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_OCCUPANCY:
	case SEquipCtrlMsg::VALIDATE_OCCUPANCY:
		{
			m_taskGroup = ETaskGroup::BKGD;

			const SEquipCtrlMsg::SGetOccupancyCmd& cmd = msg->body.getOccupancyCmd;

			if (msg->hdr.bodySize != offsetof(SEquipCtrlMsg::SGetOccupancyCmd, band) +
				cmd.numBands * sizeof(SEquipCtrlMsg::SGetOccupancyCmd::band))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			if (cmd.numBands == 0)
			{
				throw ErrorCodes::OCCUPANCYNOBANDS;
			}

			// Initial report and terminate times
			DATE now = Utility::CurrentTimeAsDATE();
//			TciGps::GetDATE(now);
			m_reportInterval = cmd.storageTime / double(Units::SECONDS_PER_DAY);

			if (cmd.output.eFieldVsChannel == 0 && cmd.output.msglengthDistribution == 0 && cmd.output.msglengthVsChannel == 0 &&
				cmd.output.occupancyVsChannel == 0 && cmd.output.occupancyVsTimeOfDay == 0 && cmd.output.spectrogram == 0 &&
				cmd.output.timegram == 0 && cmd.output.occupancyFlag.flag.eFieldVsTOD == 0)
			{
				// Realtime only
				m_reportInterval = 0;
			}

			m_reportTime = now + m_reportInterval;

			if (cmd.durationMethod == SEquipCtrlMsg::CONFIDENCE)
			{
				m_completionTime = SEquipCtrlMsg::MAX_DATE;
			}
			else
			{
				m_completionTime = now + cmd.measurementTime / double(Units::SECONDS_PER_DAY);
			}

			auto multRf1Input = m_config->IsProcWithAntSelectionSwitch();
			bool translatedAnt = false;
			bool forcedHf = false;
			bool forcedVuhf = false;
			m_cmdAnt = cmd.ant;

			if(multRf1Input)
			{
				translatedAnt = m_config->TranslateVerify5143MultAntValue(cmd.ant,
						m_ant, forcedHf, forcedVuhf);
			}
			else
			{
				m_ant = cmd.ant;
			}


			// Bands
			bool someIncludes = false;

			for (unsigned int band = 0; band < cmd.numBands; ++band)
			{
				if (!cmd.band[band].exclude)
				{
					someIncludes = true;
				}

				// Get processing parameters
				CConfig::STaskParams taskParams;
				taskParams.bw = cmd.band[band].channelBandwidth;

				//Defensive programming: ProcParams maximum channel bandwidth
				// is 25 MHz. Checking is not needed, do it anyway in case we
				// support greater than 40 MHz Occupancy  channelBandwidth.
				if(m_config->HasShfExt() && m_ant == SEquipCtrlMsg::EAnt::ANT2 &&
						taskParams.bw > CConfig::C_40_MHZ)
				{
					throw ErrorCodes::INVALIDBANDWIDTH;
				}


				taskParams.pol = (cmd.band[band].sType.signalType.horizPol == 0 ? CConfig::EPolarization::VERT : CConfig::EPolarization::HORIZ);

				if (CConfig::GetAntPolarization(m_ant) != taskParams.pol)
				{
					if(translatedAnt)
					{
						//Used polarization specified in the antenna.
						taskParams.pol = CConfig::GetAntPolarization(m_ant);
					}
					else
					{
						throw ErrorCodes::INVALIDANTENNAID;
					}
				}

				auto isHf = ValidateFrequencyRange(cmd.band[band].lowFrequency,
					cmd.band[band].highFrequency, taskParams.pol == CConfig::EPolarization::HORIZ,
					false, m_ant, forcedVuhf);
				taskParams.mode = CConfig::EMode::OCCUPANCY;
				taskParams.startFreq = cmd.band[band].lowFrequency;
				taskParams.stopFreq = cmd.band[band].highFrequency;
				bool force40MHzProcBw = false;

				if(m_config->HasShfExt() && ((taskParams.stopFreq - taskParams.startFreq) > CConfig::C_40_MHZ))
				{
					if(m_config->IsInShfExtFreqRange(taskParams.stopFreq))
					{
						//Forced to use 40 MHz Process Bandwidth since the
						// analyzer B/W is limited to 40 MHz.
						force40MHzProcBw = true;
					}
				}

				AdjustFreqLimits(taskParams);
				SEquipCtrlMsg::EAnt eAnt;

				if(translatedAnt)
				{
					eAnt = m_ant;
					taskParams.hf = forcedHf;

					if(forcedHf)
					{
						if(!m_config->IsInHfFreqRange(taskParams.startFreq) ||
								!m_config->IsInHfFreqRange(taskParams.stopFreq))
						{
							throw ErrorCodes::INVALIDFREQUENCY;
						}
					}
					else
					{
						if(!m_config->IsFreqAboveVuhfMin(m_ant, taskParams.startFreq) ||
								!m_config->IsFreqAboveVuhfMin(m_ant, taskParams.stopFreq))
						{
							throw ErrorCodes::INVALIDFREQUENCY;
						}
					}
				}
				else
				{
					taskParams.hf = isHf;
					eAnt = GetEAntValue(m_ant, taskParams.pol);
				}


				CheckAntenna(eAnt, taskParams.hf);
				taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, true);

				if ((cmd.band[band].sType.signalType.narrow == 0) &&
					taskParams.rxHwBw < taskParams.stopFreq - taskParams.startFreq)
				{
					// Use wideband mode
					taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, false);
				}

				if (taskParams.rxHwBw < taskParams.bw)
				{
					throw ErrorCodes::INVALIDBANDWIDTH;
				}

				// Only check if the band is not excluded.
				if (!cmd.band[band].exclude &&
					m_config->InRestrictedFreqRange(taskParams.startFreq, taskParams.stopFreq))
				{
					throw ErrorCodes::FREQUENCYRESTRICTED;
				}

				auto modTaskParams(taskParams);

				if(force40MHzProcBw)
				{
					modTaskParams.stopFreq = modTaskParams.startFreq + CConfig::C_40_MHZ;
				}

				// Have enough of taskParams to retrieve ProcParams
				auto procParams = m_config->GetProcParams(modTaskParams);
				auto rxProcBw = GetProcBw(modTaskParams, procParams.rxProcBw);
				if (cmd.band[band].exclude)
				{
					taskParams.numBlocks = 0;
				}
				else
				{
					taskParams.numBlocks = static_cast<unsigned int>((taskParams.stopFreq - taskParams.startFreq) / rxProcBw);
					if (taskParams.numBlocks * rxProcBw < taskParams.stopFreq - taskParams.startFreq) ++taskParams.numBlocks;
				}
				taskParams.withIq = false;
				taskParams.withPsd = true;
				taskParams.withFft = false;

#ifdef CSMS_DEBUG
				static Units::Frequency l_prevFreq(0);

				if(l_prevFreq != taskParams.startFreq)
				{
					l_prevFreq = taskParams.startFreq;
					TRACE("CTask::InitOccupancyCtrl force40MHzProcBw %d taskParams: bw =%fHz, rxHwBw =%fMHz,startFreq =%fMHz, stopFreq =%fMHz, clientBand =%u, numBlocks = %u, rxProcBw =%fMHz\n",
					      (force40MHzProcBw ? 1 :0), taskParams.bw.Hz<float>(), taskParams.rxHwBw.Hz<float>()/1000000,
					      taskParams.startFreq.Hz<float>()/1000000, taskParams.stopFreq.Hz<float>()/1000000,
					      taskParams.clientBand, taskParams.numBlocks, rxProcBw.Hz<float>()/1000000);
					TRACE("CTask::InitOccupancyCtrl procParams: blockSize = %lu, rxProcBw =%fMHz,sampleRate =%fMHz, sampleSize =%lu, iqCic =%lu\n",
					      procParams.blockSize, procParams.rxProcBw.Hz<float>()/1000000, procParams.sampleRate.Hz<float>(),
					      procParams.sampleSize, procParams.iqCic);
					TRACE("CTask::InitOccupancyCtrl procParams: zifCICDecimation = %lu, zifFIRDecimation =%lu,upResample =%lu, downResample =%lu, ddcCICDecimation =%lu, ddcFIRDecimation =%lu\n",
					      procParams.decimations.zifCICDecimation, procParams.decimations.zifFIRDecimation,
					      procParams.decimations.upResample, procParams.decimations.downResample,
					      procParams.decimations.ddcCICDecimation, procParams.decimations.ddcFIRDecimation);

				}
#endif
				m_taskParams.push_back(taskParams);
				m_procParams.push_back(procParams);
			}

			if (!someIncludes)
			{
				throw ErrorCodes::OCCUPANCYNOBANDS;
			}
		}

		break;

	case SEquipCtrlMsg::GET_TASK_STATE:
		if (msg->hdr.bodySize != sizeof(SEquipCtrlMsg::STaskIdKey))
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		TaskIdKeyCheck(msg->body.taskIdKey);
		break;

	case SEquipCtrlMsg::RESUME_OCCUPANCY:
		if (msg->hdr.bodySize != sizeof(SEquipCtrlMsg::STaskIdKey))
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		TaskIdKeyCheck(msg->body.taskIdKey);
		break;

	case SEquipCtrlMsg::SUSPEND_OCCUPANCY:
		if (msg->hdr.bodySize != sizeof(SEquipCtrlMsg::STaskIdKey))
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		TaskIdKeyCheck(msg->body.taskIdKey);
		break;

	default:
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate and initialize an OCCUPANCYDF_CTRL task
//
void CTask::InitOccupancyDfCtrl(_In_ const SEquipCtrlMsg* msg)
{
	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_OCCUPANCY_SCANDF:
	case SEquipCtrlMsg::VALIDATE_OCCUPANCY_SCANDF:
		{
			m_taskGroup = ETaskGroup::BKGD;

			const SEquipCtrlMsg::SGetScanDfCmd& cmd = msg->body.getScanDfCmd;

			if (msg->hdr.bodySize != offsetof(SEquipCtrlMsg::SGetScanDfCmd, band) +
				cmd.numBands * sizeof(SEquipCtrlMsg::SGetScanDfCmd::band))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			if (cmd.numBands == 0)
			{
				throw ErrorCodes::SCANDFNOBANDS;
			}

			// Check confidence
			if (!RangeCheck(cmd.confidence, 0ul, 100ul))
			{
				throw ErrorCodes::INVALIDCONFTHRESHOLD;
			}

			// cmd.recordAudioDf not supported and ignored
#if 0
			// Check audio record
			if (cmd.recordAudioDf != 0)
			{
				if (cmd.numBands != 1)
				{
					throw ErrorCodes::SCANDFTOOMANYBANDS;
				}

				if (cmd.band[0].exclude)
				{
					throw ErrorCodes::SCANDFNOBANDS;
				}

				if (Units::Frequency(cmd.band[0].lowFrequency) != Units::Frequency(cmd.band[0].highFrequency))
				{
					throw ErrorCodes::SCANDFTOOMANYCHANS;
				}

				if (Units::Frequency(cmd.band[0].lowFrequency) != Units::Frequency(cmd.rcvrCtrl.freq))
				{
					throw ErrorCodes::INVALIDFREQUENCY;
				}

				if (cmd.recordHoldoff < 5 || cmd.recordHoldoff > 60)
				{
					throw ErrorCodes::INVALIDDWELLTIME;
				}

				if (!RangeCheck(cmd.rcvrCtrl.detMode, SEquipCtrlMsg::SRcvrCtrlCmd::AM, SEquipCtrlMsg::SRcvrCtrlCmd::LSB))
				{
					throw ErrorCodes::INVALIDDETMODE;
				}

				if (!RangeCheck(cmd.rcvrCtrl.agcTime, 0ul, 100000ul))
				{
					throw ErrorCodes::INVALIDAGCTIME;
				}

				if ((cmd.rcvrCtrl.detMode == SEquipCtrlMsg::SRcvrCtrlCmd::CW || cmd.rcvrCtrl.detMode == SEquipCtrlMsg::SRcvrCtrlCmd::LSB ||
					cmd.rcvrCtrl.detMode == SEquipCtrlMsg::SRcvrCtrlCmd::USB) &&
					!RangeCheck(Units::Frequency(cmd.rcvrCtrl.bfo), -Units::Frequency(8000), Units::Frequency(8000)))
				{
					throw ErrorCodes::INVALIDBFO;
				}

				CConfig::STaskParams taskParams;
				taskParams.pol = CConfig::EPolarization::VERT;
				taskParams.bw = cmd.rcvrCtrl.bandwidth;

				//Non-DF because the mode is CConfig::DEMOD.
				taskParams.hf = ValidateFrequency(cmd.rcvrCtrl.freq, cmd.rcvrCtrl.bandwidth, false, false, SEquipCtrlMsg::ANT1);
				taskParams.mode = CConfig::EMode::DEMOD;
				taskParams.numBlocks = 1;
				taskParams.rxHwBw = 0;  // or .bw
				taskParams.startFreq = taskParams.stopFreq = cmd.rcvrCtrl.freq;
				m_config->GetProcParams(taskParams);
			}
#endif
			DATE now = Utility::CurrentTimeAsDATE();
			m_reportInterval = cmd.storageTime / double(Units::SECONDS_PER_DAY);
			m_reportTime = now + m_reportInterval;
			m_completionTime = now + cmd.measurementTime / double(Units::SECONDS_PER_DAY);
			//@ note: Antenna type will not be used in DF calculation.
			m_ant = m_cmdAnt = SEquipCtrlMsg::ANT1;

			// Bands
			bool someIncludes = false;

			for (unsigned int band = 0; band < cmd.numBands; ++band)
			{
				if (!cmd.band[band].exclude)
				{
					someIncludes = true;
				}

				// Validate signal type
				if (cmd.band[band].signalType.unused != 0)
				{
					throw ErrorCodes::INVALIDSIGNALTYPE;
				}

				if (cmd.band[band].signalType.horizPol != 0 && !CAntenna::HasHorizontalPolarization(
					m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get(), cmd.band[band].highFrequency))
//					!m_config->HasVushfHorizPolorization(cmd.band[band].highFrequency))
				{
					// Horizontal polarization but Antenna does not support
					// Horizontal at the specified frequency range.
					throw ErrorCodes::HARDWARENOTPRESENT;
				}

				// Get processing parameters
				CConfig::STaskParams taskParams;
				taskParams.bw = cmd.band[band].channelBandwidth;
				taskParams.hf = ValidateFrequencyRange(cmd.band[band].lowFrequency,
								cmd.band[band].highFrequency,
								cmd.band[band].signalType.horizPol, true, m_ant);

				if (taskParams.hf && Units::Frequency(cmd.band[band].lowFrequency) < m_config->GetHfDfVLowFreq())
				{
					throw ErrorCodes::INVALIDFREQUENCYBAND;
				}

				if (taskParams.hf && !CAntenna::IsDfAntenna(m_mutableConfig.hfConfig.antCable.antenna.get()))
				{
					// No HF DF Antenna - Try to force V/UHF
					try
					{
						taskParams.hf = ValidateFrequencyRange(cmd.band[band].lowFrequency,
											cmd.band[band].highFrequency,
											cmd.band[band].signalType.horizPol, true, m_ant, true);
					}
					catch(ErrorCodes::EErrorCode & errCode)
					{
						throw (errCode == ErrorCodes::FREQUENCYRESTRICTED ? errCode : ErrorCodes::HARDWARENOTPRESENT);
					}
				}

				if (!CAntenna::IsDfAntenna(taskParams.hf ? m_mutableConfig.hfConfig.antCable.antenna.get()
				    : m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get()))
				{
					// No DF antenna
					throw ErrorCodes::HARDWARENOTPRESENT;
				}

// Already checked!
//				if( m_config->InRestrictedFreqRange(cmd.band[band].lowFrequency, cmd.band[band].highFrequency) )
//				{
//					throw ErrorCodes::FREQUENCYRESTRICTED;
//				}

				taskParams.pol = (cmd.band[band].signalType.horizPol ? CConfig::EPolarization::HORIZ : CConfig::EPolarization::VERT);
				taskParams.mode = (cmd.band[band].signalType.gsm ? CConfig::EMode::FAST_SCAN_DF : CConfig::EMode::SCAN_DF);
				taskParams.startFreq = cmd.band[band].lowFrequency;
				taskParams.stopFreq = cmd.band[band].highFrequency;
				AdjustFreqLimits(taskParams);
				taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, true);

				if (!cmd.band[band].signalType.narrow && taskParams.rxHwBw < taskParams.stopFreq - taskParams.startFreq)
				{
					// Use wideband mode
					taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, false);
				}

				if (taskParams.rxHwBw < taskParams.bw)
				{
					throw ErrorCodes::INVALIDBANDWIDTH;
				}

				auto procParams = m_config->GetProcParams(taskParams);
				auto rxProcBw = GetProcBw(taskParams, procParams.rxProcBw);
				if (cmd.band[band].exclude)
				{
					taskParams.numBlocks = 0;
				}
				else
				{
					taskParams.numBlocks = static_cast<unsigned int>((taskParams.stopFreq - taskParams.startFreq) / rxProcBw);
					if (taskParams.numBlocks * rxProcBw < taskParams.stopFreq - taskParams.startFreq) ++taskParams.numBlocks;
				}

				taskParams.withIq = false;
				taskParams.withPsd = true;
				taskParams.withFft = true;
				m_taskParams.push_back(taskParams);
				m_procParams.push_back(procParams);
			}

			if (!someIncludes)
			{
				throw ErrorCodes::SCANDFNOBANDS;
			}
		}

		break;

	case SEquipCtrlMsg::GET_TASK_STATE:
		if (msg->hdr.bodySize != sizeof(SEquipCtrlMsg::STaskIdKey))
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		TaskIdKeyCheck(msg->body.taskIdKey);
		break;

	case SEquipCtrlMsg::RESUME_OCCUPANCY:
		if (msg->hdr.bodySize != sizeof(SEquipCtrlMsg::STaskIdKey))
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		TaskIdKeyCheck(msg->body.taskIdKey);
		break;

	case SEquipCtrlMsg::SUSPEND_OCCUPANCY:
		if (msg->hdr.bodySize != sizeof(SEquipCtrlMsg::STaskIdKey))
		{
			throw ErrorCodes::INVALIDVERSION;
		}

		TaskIdKeyCheck(msg->body.taskIdKey);
		break;

	default:
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate and initialize a PAN_DISP_CTRL task
//
void CTask::InitPanDispCtrl(_In_ const SEquipCtrlMsg* msg)
{
	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_PAN:
	case SEquipCtrlMsg::GET_PAN_AGC:
		{
//			m_priority = IMMEDIATE;
			m_taskGroup = ETaskGroup::PAN;
			const SEquipCtrlMsg::SGetPanCmd& cmd = msg->body.getPanCmd;

			// Check message length
			if(msg->hdr.bodySize != sizeof(cmd))
			{
				throw ErrorCodes::INVALIDVERSION;
			}

			//In TDOA unit with multiple antenna at RF1 input, client specifies
			// antenna to use, therefore needs to translate the antenna
			// specified in the client command to csms RF input.
			if (m_config->IsVushfAntSpecified(m_cmdAnt))
			{
				m_ant = SEquipCtrlMsg::EAnt::ANT1;
			}

			// Get processing parameters
			UpdatePan(cmd.freq, cmd.bandwidth);
			m_rcvrAtten = (msg->hdr.msgSubType == SEquipCtrlMsg::GET_PAN ? cmd.rcvrAtten : SEquipCtrlMsg::SGetPanCmd::AGC);
			m_spurCal = true;
			m_spurCalEnabled = true;
			m_powerBw = cmd.bandwidth;
#ifdef CSMS_DEBUG
			static Units::Frequency l_freq(0);
			static Units::Frequency l_bw(0);

			auto taskParams = GetTaskParams();
			if(l_freq != GetTuneFreq() || l_bw != taskParams.bw)
			{
				auto procParams = GetProcParams();
				TRACE("CTask::InitPanDispCtrl Num(STaskParams %u; SProcParams %u)  taskParams: bw =%fHz, rxHwBw =%fMHz,freq =%fMHz, clientBand =%u, numBlocks = %u\n",
				      m_taskParams.size(), m_procParams.size(), taskParams.bw.Hz<float>(), taskParams.rxHwBw.Hz<float>()/1000000,
				      GetTuneFreq().Hz<float>()/1000000, taskParams.clientBand, taskParams.numBlocks);
				TRACE("CTask::InitPanDispCtrl procParams: blockSize = %lu, rxProcBw =%fMHz,sampleRate =%fMHz, sampleSize =%lu, iqCic =%lu\n",
				      procParams.blockSize, procParams.rxProcBw.Hz<float>()/1000000, procParams.sampleRate.Hz<float>(),
				      procParams.sampleSize, procParams.iqCic);
				TRACE("CTask::InitPanDispCtrl procParams: zifCICDecimation = %lu, zifFIRDecimation =%lu,upResample =%lu, downResample =%lu, ddcCICDecimation =%lu, ddcFIRDecimation =%lu\n",
				      procParams.decimations.zifCICDecimation, procParams.decimations.zifFIRDecimation,
				      procParams.decimations.upResample, procParams.decimations.downResample,
				      procParams.decimations.ddcCICDecimation, procParams.decimations.ddcFIRDecimation);
				l_freq = GetTuneFreq();
				l_bw = taskParams.bw;
			}


#endif
		}

		break;

	default:
		throw ErrorCodes::INVALIDSUBTYPE;
		break;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Validate and initialize a STATUS_CTRL task
//
void CTask::InitStatusCtrl(_In_ const SEquipCtrlMsg* msg)
{
	switch(msg->hdr.msgSubType)
	{
	case SEquipCtrlMsg::GET_FAULT_REQUEST:
		{
			// Check message length and requested response version
			if(msg->hdr.bodySize != 0 || msg->hdr.cmdVersion < 1)
			{
				throw ErrorCodes::INVALIDVERSION;
			}
		}

		break;

	default:
		throw ErrorCodes::INVALIDSUBTYPE;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Change the task state for the next block (skipping empty steps)
//
void CTask::NextBlock(void)
{
	if(++m_currentBlock >= m_taskParams[m_currentStep].numBlocks)
	{
		NextStep();
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Change the task state for the next step (skipping empty steps)
//
void CTask::NextStep(void)
{
	while(m_taskParams[m_currentStep = (m_currentStep == m_lastStep ? m_firstStep : m_currentStep + 1)].numBlocks == 0);
	m_currentBlock = 0;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Create task ID, key
//
void CTask::RegisterTask(bool addToMap)
{
	std::unique_lock<std::mutex> lock(m_taskIdMutex);

	if((m_taskIdKey.taskId = m_nextTaskId++) == 0)
	{
		// Initialize random number generator
		srand(static_cast<unsigned int>(Utility::GetTickCount() & 0xffffffff));
	}

	while (m_taskIdKey.taskId == 0 || (addToMap && m_taskMap.find(m_taskIdKey.taskId) != m_taskMap.end()))
	{
		// Skip task 0 and task IDs in use
		m_taskIdKey.taskId = m_nextTaskId++;
	}

	m_taskIdKey.key = rand();
	if (addToMap)
	{
		m_taskMap.insert(TaskMap::value_type(m_taskIdKey.taskId, shared_from_this()));
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Create shared_ptr in task map
//
void CTask::RegisterTask(_In_ const Task& task)
{
	std::unique_lock<std::mutex> lock(m_taskIdMutex);

#ifdef NDEBUG
	m_taskMap.insert(TaskMap::value_type(task->m_taskIdKey.taskId, task)).second;
#else
	assert(task->m_taskIdKey.taskId != 0);
	auto ok = m_taskMap.insert(TaskMap::value_type(task->m_taskIdKey.taskId, task)).second;
	assert(ok);
#endif
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Save data for restart
//
void CTask::SaveRestartData(_Inout_ std::vector<unsigned char>& data) const
{
	// Scalars
	SaveData(data, m_ant);
	SaveData(data, m_completionTime);
	SaveData(data, m_msgHdr);
	SaveData(data, m_reportInterval);
	SaveData(data, m_reportTime);
	SaveData(data, m_taskIdKey);
	SaveData(data, m_procParams);
	SaveData(data, m_taskParams);

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Return a copy of Task sliced to the base class
//
CTask::Task CTask::SpawnTask(void)
{
//	auto t = new CTask(*this, shared_from_this());
//	if (m_abandonDwell)
//	{
//		TRACE("Spawning: %d(%p) %d(%p)\n", m_abandonDwell, &m_abandonDwell, t->m_abandonDwell, &(t->m_abandonDwell));
//	}
//	return Task(t);
	return Task(new CTask(*this, shared_from_this()));
}


//////////////////////////////////////////////////////////////////////
//
// Check task ID and key for validity
//
void CTask::TaskIdKeyCheck(const SEquipCtrlMsg::STaskIdKey& taskIdKey)
{
	std::unique_lock<std::mutex> lock(m_taskIdMutex);
	TaskMap::const_iterator task = m_taskMap.find(taskIdKey.taskId);

	if (task == m_taskMap.end())
	{
		throw ErrorCodes::INVALIDTASKID;
	}
	else if (task->second->m_taskIdKey.key != taskIdKey.key)
	{
		throw ErrorCodes::INVALIDTASKKEY;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update the current pan task and proc params
//
void CTask::UpdatePan(Units::Frequency freq, Units::Frequency bw)
{
	if (m_taskParams.empty())
	{
		m_taskParams.resize(1);
		m_procParams.resize(1);
		m_currentStep = 0;
	}

	CConfig::STaskParams taskParams;
	AdjustShfExtPanTaskParams(taskParams, freq, bw);
	taskParams.pol = CConfig::GetAntPolarization(m_ant);
	bool forcedVuhf = ((m_config->IsProcWithAntSelectionSwitch()
			    && m_ant == SEquipCtrlMsg::ANT1H) ? true : false);

	if(!forcedVuhf && (m_config->IsProcWithAntSelectionSwitch()) &&
	    m_ant == SEquipCtrlMsg::ANT1)
	{
	    forcedVuhf = m_config->IsFreqAboveVuhfMin(m_ant, freq);
	}

	taskParams.hf = ValidateFrequency(freq, taskParams.bw,
	                                  taskParams.pol == CConfig::EPolarization::HORIZ, false, m_ant, forcedVuhf);
	if (forcedVuhf)
	{
		//Multiple antenna system does not do auto-switch
		// into HF, therefore if specified, hf flag must not be set,
		// otherwise CheckAntenna will throw invalid antenna or HF is
		// used incorrectly.
		taskParams.hf = false;
	}

	taskParams.mode = CConfig::EMode::PAN;
	taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, true);

	if (taskParams.rxHwBw < taskParams.bw)
	{
		taskParams.rxHwBw = m_radioEquip->GetRxBandwidth(m_config->GetMutableConfig(), taskParams.hf, false);
	}

	taskParams.startFreq = taskParams.stopFreq = freq;
	taskParams.withIq = false;
	taskParams.withPsd = true;
	taskParams.withFft = false;
	m_taskParams[m_currentStep] = taskParams;
	CConfig::SProcParams procParams = m_config->GetProcParams(taskParams);
	m_procParams[m_currentStep] = procParams;

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Update the report time
//
void CTask::UpdateReportTime(void)
{
	if (m_reportInterval > 0 && m_blockState.sampleTime.GetDATE() > m_reportTime)
	{
		m_reportTime = m_blockState.sampleTime.GetDATE() + m_reportInterval;
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Are we currently doing DF?
//
bool CTask::UsingDf(size_t step) const
{
	return !m_taskParams.empty() &&
		(m_taskParams[step].mode == CConfig::EMode::DF || m_taskParams[step].mode == CConfig::EMode::SCAN_DF ||
		m_taskParams[step].mode == CConfig::EMode::FAST_SCAN_DF);
}


//////////////////////////////////////////////////////////////////////
//
// Validate a frequency. Returns true if HF band
//
bool CTask::ValidateFrequency(Units::Frequency freq, Units::Frequency bw, bool isHorizontal,
	bool isDf, _In_ SEquipCtrlMsg::EAnt ant, bool forceVuhf) const
{
	bool hasRetcode = false;
	bool isHf = ( m_config->HasHfAnt() && (freq <= m_radioEquip->GetMaxHfFreq()));

	// Check HF band
	if (!forceVuhf && isHf)
	{
		hasRetcode = true;
	}

	// Check with SHF extension
	if (!hasRetcode && m_config->IsFreqInRange(freq, isHorizontal, isDf, ant) )
	{
		isHf = false;
		hasRetcode = true;
	}

	if (!hasRetcode)
	{
		throw ErrorCodes::INVALIDFREQUENCY;
	}

	if (m_config->InRestrictedFreqBw(freq, bw))
	{
		throw ErrorCodes::FREQUENCYRESTRICTED;
	}

	return isHf;
}


//////////////////////////////////////////////////////////////////////
//
// Validate a frequency range. Returns true if HF band
//
bool CTask::ValidateFrequencyRange(Units::Frequency freq1, Units::Frequency freq2,
	bool isHorizontal, bool isDf, SEquipCtrlMsg::EAnt ant, bool forceVuhf) const
{
	if (freq1 > freq2)
	{
		throw ErrorCodes::INVALIDFREQUENCYBAND;
	}

	bool hf = ValidateFrequency(freq2, 0, isHorizontal, isDf, ant, forceVuhf);

	if (ValidateFrequency(freq1, 0, isHorizontal, isDf, ant, !hf || forceVuhf) != hf)
	{
		throw ErrorCodes::INVALIDFREQUENCYBAND;
	}

	// throw will make sure there is no error at this point.
	if (m_config->InRestrictedFreqRange(freq1, freq2))
	{
		throw ErrorCodes::FREQUENCYRESTRICTED;
	}

	return hf;
}

