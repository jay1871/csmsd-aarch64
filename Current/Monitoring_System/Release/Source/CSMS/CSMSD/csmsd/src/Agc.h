/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2017 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once
#include "Digitizer.h"
#include "RadioEquip.h"
#include "Singleton.h"

class CAgc
{
public:
	// Types
	// Constants
	static const unsigned char INVALID_ATTEN = 0xff;

	// Functions
	CAgc(void);
	~CAgc(void) {}
	void AddInitialAgc(Units::Frequency freq, Units::Frequency rxBw, CRadioEquip::EBandSelect band, SEquipCtrlMsg::EAnt eAnt,
		unsigned char initAtten, signed char attenAdj, bool& gainChange, unsigned char& atten,
		std::chrono::milliseconds agcDecayTime);
	static signed char CalcAttenAdjust(unsigned char prevAtten, unsigned int total,
		const unsigned int (&counts)[4], const unsigned short (&thresh)[4], int adcFullScale,
		unsigned char minAtten, unsigned char maxAtten);
	bool GetAgc(void) { return m_agc; }
	bool GetAgcMaster(unsigned char& atten, Units::Frequency freq, Units::Frequency rxBw, CRadioEquip::EBandSelect band,
		SEquipCtrlMsg::EAnt eAnt, CRadioEquip::EGainMode gainMode, bool direct, std::chrono::milliseconds agcDecayTime);
	bool InitialAgc(C2630::EGainMode gainMode, bool direct, unsigned char& initAtten, signed char& attenAdj) const;
	void SetAgc(bool on) { m_agc = on; }
	void Timeout(void);
	bool TryGetAgc(Units::Frequency freq, Units::Frequency rxBw, CRadioEquip::EBandSelect band, SEquipCtrlMsg::EAnt eAnt,
		bool& gainChange, unsigned char& atten, std::chrono::milliseconds agcDecayTime);
	bool UpdateAgc(unsigned char numAnts, Units::Frequency freq, Units::Frequency rxBw, CRadioEquip::EBandSelect band,
		SEquipCtrlMsg::EAnt input, unsigned long adcCount);
	bool UpdateAgc(Units::Frequency freq, Units::Frequency rxBw, CRadioEquip::EBandSelect band,
		SEquipCtrlMsg::EAnt input, unsigned long adcCount, const unsigned int (&counts)[4], const unsigned short (&thresh)[4]);
	void Wakeup(void) { m_agcCond.notify_all(); }

protected:
private:
	// Constants
	static constexpr const float AGC_SET_POINT = 0.075f;
	static constexpr const std::chrono::seconds AGC_IDLE_TIMEOUT = std::chrono::seconds{60};
	static const signed char MAX_AGC_ATTEN_ADJ = 30; // dB
	static const signed char MAX_GOOD_ATTEN_ADJ = 12; // dB

	// Types
	struct SAgcKey // Key for AGC map
	{
		bool operator==(const SAgcKey& right) const
			{ return freq == right.freq && rxBw == right.rxBw && band == right.band && input == right.input; }

		Units::Frequency freq;
		Units::Frequency rxBw;
		CRadioEquip::EBandSelect band;
		SEquipCtrlMsg::EAnt input;
	};

	struct SAgcValue // Value for AGC map
	{
		SAgcValue(unsigned char c, unsigned char n) : lastAgcUse(std::chrono::system_clock::now()),
			lastNoAgcChange(std::chrono::time_point<std::chrono::system_clock>::min()),
			currAtten(c), nextAtten(n) { }

		std::chrono::system_clock::time_point lastAgcUse;
		std::chrono::system_clock::time_point lastNoAgcChange;
		unsigned char currAtten;
		unsigned char nextAtten;
	};

	struct SAgcHash // Hash for AGC map
	:
		public std::unary_function<SAgcHash, size_t>
	{
		size_t operator()(const SAgcKey& key) const { return std::hash<long long>()(key.freq.GetRaw().internal); }
	};

	typedef std::unordered_map<SAgcKey, SAgcValue, SAgcHash> AgcMap; // Map of AGC settings

	// Functions
	void SetGainChange(AgcMap::iterator& agc, signed char attenAdj, bool& gainChange, std::chrono::milliseconds agcDecayTime);

	// Data
	bool m_agc;
	std::condition_variable m_agcCond;
	AgcMap m_agcMap;
	std::mutex m_agcMutex;
	CSingleton<CDigitizer> m_digitizer;
	CSingleton<CRadioEquip> m_radioEquip;

};
