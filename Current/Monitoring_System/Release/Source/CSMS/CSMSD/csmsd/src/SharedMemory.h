/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include <sys/mman.h>

class CSharedMemory
{
private:
	class CSharedMutex
	{
		CSharedMutex(const CSharedMutex&) = delete;
		CSharedMutex(CSharedMutex&&) = delete;
		CSharedMutex& operator=(const CSharedMutex&) = delete;
		CSharedMutex& operator=(CSharedMutex&&) = delete;
	public:
		CSharedMutex(void) { Create(); }
		~CSharedMutex(void) { pthread_mutex_destroy(&mutex); }
		void Create(void)
		{
			pthread_mutexattr_t attr;
			int err = pthread_mutexattr_init(&attr);
			if (err) throw std::runtime_error("CSharedMutex failed");
			err = pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);
			if (err) throw std::runtime_error("CSharedMutex failed");
			err = pthread_mutexattr_setrobust(&attr, PTHREAD_MUTEX_ROBUST);
			if (err) throw std::runtime_error("CSharedMutex failed");
			err = pthread_mutex_init(&mutex, &attr);
			if (err) throw std::runtime_error("CSharedMutex pthread_mutex_init failed");
			err = pthread_mutexattr_destroy(&attr);
			if (err) throw std::runtime_error("CSharedMutex failed");
			return;
		}
		void Recreate(void)
		{
			pthread_mutex_destroy(&mutex);
			Create();
		}
		int Lock(void) { return pthread_mutex_lock(&mutex); }
		void Unlock(void) { pthread_mutex_unlock(&mutex); }

	private:
		pthread_mutex_t mutex;
	};

public:
	enum EMaster : unsigned char { NO_MASTER, CSMSD_MASTER, VCP_MASTER };

	struct SSharedMemory
	{
		mutable CSharedMutex sharedMutex;
		EMaster master;
		double state;		// Variable length with correct alignment
	};

	CSharedMemory(const char* shMemName, size_t stateSize);
	~CSharedMemory(void);

	bool IsMaster(void) const { return m_master; }
	const SSharedMemory* SharedMemory(void) const { return m_shMem; }
	SSharedMemory* SharedMemory(void) { return m_shMem; }
	const void* State(void) const { return &m_shMem->state; }
	void* State(void) { return &m_shMem->state; }

public:
	class CLockGuard
	{
		CLockGuard(const CLockGuard&) = delete;
		CLockGuard(CLockGuard&&) = delete;
		CLockGuard& operator=(const CLockGuard&) = delete;
		CLockGuard& operator=(CLockGuard&&) = delete;
	public:
		CLockGuard(CSharedMutex& shmutex) : locked(false), mutex(shmutex)
		{
			int rv = mutex.Lock();
			if (rv != 0)
			{
				std::string err("CSharedMutex::CLockGuard error = ");
				err += std::to_string(rv);
				throw std::runtime_error(err.c_str());
			}
			locked = true;
		}
		~CLockGuard(void)
		{
			if (locked)
			{
				locked = false;
				mutex.Unlock();
			}
		}
	private:
		bool locked;
		CSharedMutex& mutex;
	};

private:
	bool m_master;
	int m_shmDesc;
	SSharedMemory* m_shMem;
	size_t m_shmSize;
	std::string m_shMemName;
};
