/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once

#include "Ne10Vec.h"
#include "MeasurementTask.h"
#include "NoiseEstimate.h"
#include "SuspendableTask.h"

struct SEquipCtrlMsg;
template<typename T> class CNetConnection;


class CAvdTask :
	// Inheritance
	public CMeasurementTask,
	public CSuspendableTask
{
	// Friends
	friend CMeasurementTask::CMeasurementTask(const SEquipCtrlMsg* cmd, CNetConnection<void>* source/*, EPriority priority*/);
	friend void CMeasurementTask::UpdateBw(const Ne10F32Vec& watts, double binSize, size_t count);
	friend void CMeasurementTask::UpdateFreqDwell(size_t count, size_t step);

public:
	// Types
	typedef std::shared_ptr<CAvdTask> AvdTask;

	// Functions
	virtual void AbandonDwell(void);
	void AddBlock(_In_ const Ne10F32Vec& watts, bool partialBand);
	static AvdTask Create(_In_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source);
	static void Create(_In_ SRestartData& restartData);
	void GetChanWatts(unsigned long binsPerChan, unsigned long numChan, Ne10F32Vec& wattsRT, float enbw);
	float GetNoise(void) const { return m_noise->GetNoise(); }
	void SendReports(bool last);
	void UpdateBlock(size_t band, size_t block, Units::Frequency firstChanFreq);
	void UpdateTask(void);
	static void Validate(_In_ const SEquipCtrlMsg::SGetAutoViolateCmd& cmd);

private:
	// Constants

	// Types
	struct Deleter
	{
		void operator()(_In_ CAvdTask* task) { delete task; }
	};

	struct SDataKey
	{
		bool operator<(const SDataKey& b) const
			{ return band < b.band || (band == b.band && chan < b.chan); }

		size_t band;
		size_t chan;
	};

	struct SDataValue
	{
		SDataValue(void) : numAbove(0), prevNumAbove(0), maxOcc(0), up(false), bw(), freq(), lastTime(0), threshTime(0), deltaTime(0) {}

		unsigned int numAbove;
		unsigned int prevNumAbove;
		float maxOcc;
		bool up;
		CStatsNoHist bw;
		CStatsNoHist freq;
		DATE lastTime;
		double threshTime;	// seconds
		double deltaTime;	// seconds
	};

	typedef std::map<SDataKey, SDataValue> DataMap;

	// Functions
	CAvdTask(_In_ const SEquipCtrlMsg* cmd, _In_opt_ CNetConnection<void>* source);
	CAvdTask(_In_ SRestartData& restartData);
	virtual ~CAvdTask(void);

	virtual unsigned long RestartDataType(void) const { return SEquipCtrlMsg::AVD_RESTART_DATA; }
	virtual void SaveRestartData(_Inout_ std::vector<unsigned char>& data) const;

	SEquipCtrlMsg::SAvdMeasureResult::SMeasureData GetBwData(_Inout_ SDataValue& value, size_t chan);
	SEquipCtrlMsg::SOccResult::SResultData GetChannelOccData(_Inout_ SDataValue& value, size_t chan);
	SEquipCtrlMsg::SAvdMeasureResult::SMeasureData GetFreqData(_Inout_ SDataValue& value, size_t chan);
	void UpdateBw(double bw);
	void UpdateFreq(double freq);

	// Data
	size_t m_count;
	DataMap m_data;
	size_t m_firstAvdBand;
	DataMap::iterator m_measurement;
	CNoiseEstimate::NoiseEstimate m_noise;
	unsigned int m_numLooks;
	unsigned int m_prevNumLooks;
	SEquipCtrlMsg::EAvdRate m_rate;
	bool m_resetDwell;
	float m_snr;
	Ne10F32Vec m_watts;
};
