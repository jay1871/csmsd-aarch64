/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include "SqliteDb.h"

class CResultsTbl :
	// Inheritance
	protected virtual CSqliteDb
{
public:
	// Functions
	virtual ~CResultsTbl(void);

protected:
	// Types
	enum EGetMode { /*FIRST,*/ NEXT, FIND, /*CURRENT*/ };
	// Functions
	CResultsTbl(void);
	int DeleteRows(unsigned long measureId, unsigned long msgSubType);
	int GetFirstRow(unsigned long measureId, unsigned long msgSubType);
	int GetFirstRow(unsigned long measureId, unsigned long msgSubType, std::vector<unsigned char>& data);
	template<typename T> int GetRow(EGetMode mode, unsigned long measureId, unsigned long msgSubType, T& data);
	int Open(void);
	int UpdateRow(unsigned long measureId, unsigned long msgSubType, unsigned long firstChan, const void* data,
		unsigned long size);
	template<typename T> int UpdateRow(unsigned long measureId, unsigned long msgSubType, unsigned long firstChan, _In_ const T& results)
		{ return UpdateRow(measureId, msgSubType, firstChan, &results, sizeof(T)); }

	// Data
	int m_openHr;
	sqlite3_stmt* m_pStmtUpdate;
	sqlite3_stmt* m_pStmtInsert;
	sqlite3_stmt* m_pStmtDelete;
	sqlite3_stmt* m_pStmtSelect1;
};

//////////////////////////////////////////////////////////////////////
//
// Get a row from the table
//
template<typename T> int CResultsTbl::GetRow(EGetMode mode, unsigned long measureId, unsigned long msgSubType, T& data)
{
	if (!m_pStmtSelect1)
		return SQLITE_ERROR;

	int rv;

	switch(mode)
	{
	case NEXT:
		break;

	case FIND:
		if ((rv = sqlite3_reset(m_pStmtSelect1)) != SQLITE_OK)
			return rv;

		if ((rv = sqlite3_bind_int(m_pStmtSelect1, 1, measureId)) != SQLITE_OK)
			return rv;

		if ((rv = sqlite3_bind_int(m_pStmtSelect1, 2, msgSubType)) != SQLITE_OK)
			return rv;

		break;

	default:
		THROW_LOGIC_ERROR();
	}

	rv = sqlite3_step(m_pStmtSelect1);
	if (rv == SQLITE_ROW)
	{
		/*unsigned long firstChan = */ sqlite3_column_int(m_pStmtSelect1, 0);
		const unsigned char* pData = static_cast<const unsigned char*>(sqlite3_column_blob(m_pStmtSelect1, 1));
		if (pData)
		{
			int numBytes = sqlite3_column_bytes(m_pStmtSelect1, 1);
			if ((unsigned int) numBytes != sizeof(T))
				printf("blob size mismatch %d %d\n", numBytes, sizeof(T));
			if ((unsigned int) numBytes > sizeof(T))
				numBytes = sizeof(T);
			memcpy(&data, pData, numBytes);
		}
	}
	else if (rv != SQLITE_DONE)
	{
		printf("CResultsTbl::GetRow: select step failed: %d\n", rv);
	}

	return rv;
}

