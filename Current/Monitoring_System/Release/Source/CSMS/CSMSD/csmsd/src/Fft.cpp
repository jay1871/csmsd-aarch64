/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/
#include "stdafx.h"

#include "Fft.h"
#include "Units.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
Ne10F32cVec CFft::m_blackmanHarris;
ne10_float32_t CFft::m_enbw = 0;
unsigned int CFft::m_numSamples = 0;
unsigned int CFft::m_numWindow = 0;
ne10_float32_t CFft::m_scalePower = 0;


//////////////////////////////////////////////////////////////////////
//
// Calculate initialization data
//
void CFft::CalculateWindow(unsigned int numSamples)
{
	if (numSamples != m_numWindow)
	{
		// Shouldn't change FFT size if there's another active instance
		m_numWindow = numSamples;

		// Build Blackman-Harris window
		m_blackmanHarris.assign(m_numWindow, Ne10F32cVec::ZERO);
		const ne10_float32_t ARG = Units::TWO_PI / (m_numWindow - 1);
		ne10_float32_t sum = 0;
		ne10_float32_t sumSq = 0;

		for(size_t i = 0; i < m_numWindow / 2u; ++i)
		{
			const ne10_float32_t COS = cos(ARG * i);
			m_blackmanHarris[i].r = m_blackmanHarris[m_numWindow - 1 - i].r =
					0.35875f - 0.48829f * COS + 0.14128f * (2 * COS * COS - 1) - 0.01168f * (4 * COS * COS * COS - 3 * COS);
			sum += m_blackmanHarris[i].r;
			sumSq += m_blackmanHarris[i].r * m_blackmanHarris[i].r;
		}

		// Calculate equivalent noise bandwidth
		m_enbw = m_numWindow * sumSq / (2 * sum * sum);

		// Adjust window for unity gain
		m_blackmanHarris /= 2 * sum / m_numWindow;

		// Calculate scale factor for fpga fft
		m_scalePower = 1 / (4 * sum * sum);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get bandwidth correction for window function (in bins)
//
ne10_float32_t CFft::GetBwCorrection(ne10_float32_t)
{
	return 3; // TODO do this better
}


//////////////////////////////////////////////////////////////////////
//
// Get effective noise bandwidth
//
ne10_float32_t CFft::GetEnbw(unsigned int numSamples)
{
	CalculateWindow(numSamples);
	return m_enbw;
}


// TODO: Leave this in until the FPGA handles it.
#define SHIFT_FFT
//////////////////////////////////////////////////////////////////////
//
// Get power spectrum in watts from psd data - always does shift
//
void CFft::GetPowerSpectrumWatts2(unsigned long numBins, unsigned long sampleSize, const uncached_ptr& data,
	float gainAdj, Ne10F32Vec& watts)
{
	if (sampleSize / 2 < numBins / 2)
	{
		watts.resize(0);
		return;
	}
	watts.resize(numBins);

	auto factor = CFft::GetScalePower(sampleSize) * gainAdj;
	auto fpgaFft = const_cast<const ne10_float32_t*>(reinterpret_cast<volatile ne10_float32_t*>(data.get()));

	ne10sCopyUncached(&fpgaFft[sampleSize - numBins / 2], &watts[0], numBins / 2);	// -F/2 - 0);
	ne10sCopyUncached(&fpgaFft[0], &watts[numBins / 2], numBins - numBins / 2);				// 0 - F/2
	watts *= factor;
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get scale factor to apply from window
//
ne10_float32_t CFft::GetScalePower(unsigned int numSamples)
{
	CalculateWindow(numSamples);
	return m_scalePower;
}


//////////////////////////////////////////////////////////////////////
//
// Get voltages from either fft data - always does shift
//
void CFft::GetVoltages2(unsigned long numBins, unsigned long sampleSize, const uncached_ptr& dataI,
	const uncached_ptr& dataQ, float gainAdj, Ne10F32cVec2& volts)
{
	if (sampleSize / 2 < numBins / 2)
	{
		volts.resize(0);
		return;
	}

	volts.reserve(numBins);
	auto FftI = const_cast<const ne10_float32_t*>(reinterpret_cast<volatile ne10_float32_t*>(dataI.get()));
	auto FftQ = const_cast<const ne10_float32_t*>(reinterpret_cast<volatile ne10_float32_t*>(dataQ.get()));

	auto factor = sqrtf(CFft::GetScalePower(sampleSize) * gainAdj);
	ne10sCopyUncached(&FftI[sampleSize - numBins / 2], &volts.dataReal()[0], numBins / 2);		// F/2 - 0);
	ne10sCopyUncached(&FftQ[sampleSize - numBins / 2], &volts.dataImag()[0], numBins / 2);		// F/2 - 0);
	ne10sCopyUncached(&FftI[0], &volts.dataReal()[numBins / 2], numBins - numBins / 2);			// 0 - F/2
	ne10sCopyUncached(&FftQ[0], &volts.dataImag()[numBins / 2], numBins - numBins / 2);			// 0 - F/2
	volts *= factor;

	return;
}

// TODO: Leave this in until the FPGA handles it.
#undef SHIFT_FFT

void CFft::TraceSpectrum(const char* label, size_t nbeg, size_t nend, const ne10_float32_t* s)
{
	for (size_t i = nbeg; i < nend; i += 8)
	{
		char temp[30];
		snprintf(temp, 30, "%s:%3u", label, i);
		std::string str(temp);
		for (size_t j = i; j < i + 8; ++j)
		{
			if (j == nend) break;
			snprintf(temp, 30, " %15.5e", s[j]);
			str += temp;
		}
		printf("%s\n", str.c_str());
	}
	return;
}

void CFft::TraceIq(const char* label, size_t nbeg, size_t nend, ne10_fft_cpx_float32_t* s)
{
	for (size_t i = nbeg; i < nend; i += 8)
	{
		char temp[60];
		snprintf(temp, 60, "%s:%3u", label, i);
		std::string str(temp);
		for (size_t j = i; j < i + 8; ++j)
		{
			if (j == nend) break;
			snprintf(temp, 60, " (%15.5e,%15.5e)", s[j].r, s[j].i);
			str += temp;
		}
		printf("%s\n", str.c_str());
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Convert Watts to dBm
//
Ne10F32Vec CFft::WattsToDbm(const Ne10F32Vec& watts)
{
	Ne10F32Vec dbm;
	ne10sLog10(watts, dbm);
	dbm *= 10;
	dbm += 30;

	return dbm;
}


