/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2014 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

//////////////////////////////////////////////////////////////////////
//
// Definitions for common Microsoft types etc to allow shared components
// to be used without numerous conditional compilation wrappers
//

// Debug
#define _ASSERT(expr) assert(expr)
#define ASSERT(expr) assert(expr)
#define ATLASSERT(expr) assert(expr)
#ifdef NDEBUG
#define _RPT1(category, format, arg)
#define ATLTRACE(format, ...)
#define TRACE(format, ...)
#else
#define _RPT1(category, format,arg) printf(format, arg)
#define ATLTRACE(format, ...) printf(format, ##__VA_ARGS__)
#define TRACE(format, ...) printf(format, ##__VA_ARGS__)
#endif
#define UNREFERENCED_PARAMETER(P)
#define THROW_LOGIC_ERROR() do { ASSERT(FALSE); throw std::logic_error(static_cast<std::ostringstream&>(std::ostringstream() << std::flush << __FILE__ << ":" << __LINE__).str()); } while(false)

// SAL
#define _In_
#define _In_bytecount_(x)
#define _In_opt_
#define _Out_
#define _Out_bytecap_(x)
#define _Out_opt_
#define _Inout_
#define _Post_ptr_invalid_
#define _Post_writable_byte_size_(x)
#define _Pre_readable_byte_size_(x)
#define _Ret_
#define _Ret_notnull_
#define _Ret_opt_
#define _Ret_writes_bytes_to_(x,y)
#define _Success_(x)

// Types
typedef unsigned char BYTE;
typedef unsigned int DWORD;
typedef double DATE;
typedef int BOOL;
struct LARGE_INTEGER { long long QuadPart; };
typedef int SOCKET;
#define WINAPI

// Constants
static const unsigned int INFINITE = static_cast<unsigned int>(-1);
static const unsigned int COINIT_MULTITHREADED = 0;
static const int FALSE = 0;
static const int INVALID_SOCKET = -1;
static const int SOCKET_ERROR = -1;
#define MEMORY_ALLOCATION_ALIGNMENT 8
static const unsigned int WSA_INFINITE = static_cast<unsigned int>(-1);
#define MAX_COMPUTERNAME_LENGTH 15

// TCHAR
#define _tcscpy_s(dst, dstSize, src) strcpy(dst, src)
#define _tcslen strlen
#define _T(x) x
#define ADDRINFOT addrinfo
#define LPCSTR const char*
#define LPCTSTR const char*
#define TCHAR char

// Aligned memory
#define _aligned_malloc(s, a) aligned_alloc(a, s)
#define _aligned_free(p) free(p)

// Networking
#define FreeAddrInfo freeaddrinfo
#define GetAddrInfo getaddrinfo
#define GetNameInfo getnameinfo
#define InetNtop inet_ntop
#define IN_ADDR in_addr
#define ioctlsocket ioctl
#define closesocket close
#define SD_SEND SHUT_WR
#define SOCKADDR sockaddr
#define SOCKADDR_IN sockaddr_in
#define SOCKADDR_IN6 sockaddr_in6
#define SOCKADDR_STORAGE sockaddr_storage
#define WSAEADDRNOTAVAIL EADDRNOTAVAIL
#define WSAENOTCONN ENOTCONN
#define WSAETIMEDOUT ETIMEDOUT
#define WSAEWOULDBLOCK EWOULDBLOCK
#define WSAGetLastError() errno
#define WSASetLastError(e) errno = (e)

// Error handling
#define GetLastError() errno

#define AfxThrowMemoryException() throw std::bad_alloc()

// Safe functions
#define memcpy_s(dst, dstSize, src, count) memcpy(dst, src, dstSize < count ? dstSize : count)
#define strcpy_s(dst, src) strcpy(dst, src)

// Interlocked lists
struct SLIST_ENTRY
{
	SLIST_ENTRY* Next;
};

struct SLIST_HEADER
{
	friend void InitializeSListHead(SLIST_HEADER*);
	friend SLIST_ENTRY* InterlockedPopEntrySList(SLIST_HEADER*);
	friend SLIST_ENTRY* InterlockedPushEntrySList(SLIST_HEADER*, SLIST_ENTRY*);
	friend unsigned short QueryDepthSList(SLIST_HEADER*);
	friend SLIST_ENTRY* RtlFirstEntrySList(SLIST_HEADER*);

private:
	SLIST_ENTRY* head;
	std::atomic<unsigned short> depth;
} __attribute__((__aligned__(64))); // Worst case LDREX/STREX Exclusive Reservation Granule

__attribute__((__weak__)) void InitializeSListHead(SLIST_HEADER* head)
{
	head->head = nullptr;
	head->depth = 0;
}

__attribute__((__weak__)) SLIST_ENTRY* InterlockedPopEntrySList(SLIST_HEADER* head)
{
	--head->depth;
	SLIST_ENTRY* pop;
	register SLIST_ENTRY* popNext;
	register int fail;

	asm volatile("@ InterlockedPopEntrySList\n"
		"	dmb		sy\n"
		"1:	ldrex	%[pop],%[headHead]\n"
		"	teq		%[pop],#0\n"
		"	beq		2f\n"
		"	ldr		%[popNext],[%[pop]]\n"
		"	strex	%[fail],%[popNext],%[headHead]\n"
		"	teq		%[fail],#0\n"
		"	bne		1b\n"
		"2:	dmb		sy"

		: [pop] "=r" (pop), [fail] "=&r" (fail), [headHead] "+m" (head->head), [popNext] "=&r" (popNext)
		:
		: "cc");

	return pop;
}

__attribute__((__weak__)) SLIST_ENTRY* InterlockedPushEntrySList(SLIST_HEADER* head, SLIST_ENTRY* entry)
{

    SLIST_ENTRY* old;
    register int fail;

	asm volatile("@ InterlockedPushEntrySList\n"
		"	dmb		sy\n"
		"1:	ldrex	%[old],%[headHead]\n"
		"	str		%[old],%[entryNext]\n"
		"	strex	%[fail],%[entry],%[headHead]\n"
		"	teq		%[fail],#0\n"
		"	bne		1b\n"
		"	dmb		sy"
		: [old] "=r" (old), [fail] "=&r" (fail), [headHead] "+m" (head->head), [entryNext] "=m" (entry->Next)
		: [entry] "r" (entry)
		: "cc");

	++head->depth;

	return old;
}

__attribute__((__weak__)) unsigned short QueryDepthSList(SLIST_HEADER* head)
{
	return head->depth;
}

__attribute__((__weak__)) SLIST_ENTRY* RtlFirstEntrySList(SLIST_HEADER* head)
{
	return head->head;
}
