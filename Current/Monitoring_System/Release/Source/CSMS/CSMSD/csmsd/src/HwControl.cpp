/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2014-2017 TCI International, Inc. All rights reserved         *
**************************************************************************/

#include "stdafx.h"

#include "7234201102.h"
#include "72362001.h"
#include "Agc.h"
#include "Antenna.h"
#include "AvdTask.h"
#include "Bist.h"
#include "Config.h"
#include "DfCtrlMsg.h"
#include "Failure.h"
#include "HwControl.h"
#include "Log.h"
#include "MeasurementTask.h"
#include "PanTask.h"
#include "Processing.h"
#include "SuspendableTask.h"
#include "Units.h"
#include "Utility.h"

//////////////////////////////////////////////////////////////////////
//
// Static data
//
constexpr std::chrono::milliseconds CHwControl::DEFAULT_AGC_DECAY_TIME;
constexpr std::chrono::seconds CHwControl::MONITOR_POLLING_INTERVAL;
constexpr std::chrono::seconds CHwControl::PAN_TIMEOUT_POLLING_INTERVAL;
constexpr unsigned short CHwControl::THRESHOLDS_3230[4];
bool CHwControl::m_debug = false;
bool CHwControl::m_initDFdone = false;

#ifdef CSMS_DEBUG
size_t CHwControl::s_addAvd2ProcCnt = 0;
bool CHwControl::s_bwOfInterest = false;
bool CHwControl::s_traceAvd = false;
bool CHwControl::s_tracePan = false;
bool CHwControl::s_traceSlave = false;
size_t CHwControl::s_withTermCnt = 0;
bool CHwControl::s_withTermOn = false;

#endif

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CHwControl::CHwControl(void) :
	CBist(),
	m_navigation(),
	m_config(),
	m_controlThread(),
	m_digitizer(),
	m_monitor(m_config->GetNumSlaves()),
	m_mutableConfig(m_config->GetMutableConfig()),
	m_panTimeoutThread(),
	m_pbCal(),
	m_radioEquip(),
	m_sched(),
	m_shutdown(false),
	m_startupComplete(eventfd(0, EFD_SEMAPHORE)),
	m_switch(),
	m_cAgc(),
	m_timeoutcounter(0),
	m_exitFaultLedThread(false)
{
	// Check license machine id
	CCsmsLicense::SMachineId machineId;
	m_digitizer->GetDeviceDna(machineId.dna);
	m_radioEquip->GetMax10UniqueId(machineId.max10Id);
	m_license->CheckMachineId(machineId);

	// Update config data based on license, etc.
	m_radioEquip->UpdateConfig();
	m_config->UpdateAntennaParams();
	m_config->SetAntennaSpecificParameters();

	// Check all hardware is ready
	TRACE("Reset 3230 clock frequency\n");
	m_digitizer->ResetFrequency();

	// Check digitizer memory paths
	if (!m_digitizer->CheckMemory())
	{
		CLog::Log(CLog::WARNINGS, "Digitizer memory check failed\n");
	}

	// Check digitizer memory paths
	/*if (!m_digitizer->CheckMemory2())
	{
		CLog::Log(CLog::WARNINGS, "Digitizer memory check failed\n");
	}*/

	// Wait for 10 seconds
	for (size_t i = 0; i < 10; ++i)
	{
		auto clocks = m_digitizer->GetClockFrequencies();
		TRACE("368640000 clock[%u] = %lu\n", i, clocks[0]);
		sleep(1);
		if (Utility::IsQuitting())
		{
			CLog::Log(CLog::INFORMATION, "HwControl received QUIT signal");
			Stop();
			return;
		}
	}

	// Wait up to two minutes for timestamp to be ready
	for (size_t i = 0; i < 120; ++i)
	{
		if (Reset3230Time(false, true))		// No error logging.
			break;

		if ((i % 10) == 0)
			CLog::Log(CLog::INFORMATION, "3230 time not available: %u", i);
		sleep(1);
		if (Utility::IsQuitting())
		{
			CLog::Log(CLog::INFORMATION, "HwControl received QUIT signal");
			Stop();
			return;
		}
	}
	unsigned long vals[5];
	bool errs[5];
	if (m_digitizer->CheckTimestamp(true, true, vals, errs) && m_digitizer->m_setSeconds)
		CLog::Log(CLog::INFORMATION, "3230 time is available");
	else
		CLog::Log(CLog::WARNINGS, "3230 time is not available");

	// Monitor thread will set the time when the digital board is ready

	// Setup hardware
	m_digitizer->SetThresholdCounters(THRESHOLDS_3230);

	// Start the threads
	m_controlThread = std::thread(&CHwControl::ControlThread, this);
	m_agcTimeoutThread = std::thread(&CHwControl::AgcTimeoutThread, this);
	m_panTimeoutThread = std::thread(&CHwControl::PanTimeoutThread, this);
	m_monitorThread = std::thread(&CHwControl::MonitorThread, this);
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CHwControl::~CHwControl(void)
{
	Stop();
	TRACE("CHwControl dtor\n");
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Add a task to my queue
//
bool CHwControl::AddTask(const CTask::Task& task, unsigned int timeout, bool isRunnable, SEquipCtrlMsg::EMsgType flush)
{
	bool rv = false;

#ifdef CSMS_DEBUG
	if(s_traceAvd)
	{
		const CConfig::STaskParams& taskParams = task->GetTaskParams();
		TRACE("CHwControl::AddTask (AVD): %lf %lf %lf %lf %u, key =%lu, taskId = %lu, numSteps = %u\n",
			taskParams.startFreq.Hz<double>(), taskParams.stopFreq.Hz<double>(),
			taskParams.bw.Hz<double>(), taskParams.rxHwBw.Hz<double>(),
			taskParams.numBlocks, task->m_taskIdKey.key,
			task->m_taskIdKey.taskId, task->GetNumSteps());
	}
#endif

	if (task->GetNumSteps() > 0)
	{
		auto stratum = static_cast<int>(task->GetTaskGroup());
		if (m_sched.Count(stratum) >= m_sched.Depth())
		{
			TRACE("TaskSched: %u %u %u\n", m_sched.Count(stratum), m_sched.Depth(), stratum);
			throw TaskSched::TIMEDOUT;
		}
		if (flush != SEquipCtrlMsg::NO_MSGTYPE)
		{
			rv = m_sched.ReplaceItem([&](CTask::Task& item){ return item->m_msgHdr.msgType == flush && item->m_source == task->m_source; },
				task, stratum, isRunnable, timeout);
		}
		else
		{
			m_sched.AddItem(task, stratum, isRunnable, timeout);
		}
		try
		{
			CAudio& audio = dynamic_cast<CAudio&>(*this);
			audio.Enable(CanEnableAudio());
		}
		catch(const std::bad_cast&)
		{
			TRACE("AddTask bad_cast\n");
		}
	}

	return rv;
}


//////////////////////////////////////////////////////////////////////
//
// Add a task to the processing queue
//
void CHwControl::AddTaskToProcessingQueue(_In_ const CTask::Task& task)
{
	try
	{
		dynamic_cast<CProcessing&>(*this).AddTask(task, 0, INFINITE);
	}
	catch(const std::bad_cast&)
	{
		// Shutting down
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Thread to handle AGC idle timeouts
//
void CHwControl::AgcTimeoutThread(void)
{
	while (true)
	{
		{
			std::lock_guard<std::mutex> lock(m_shutdownMutex);
			if (m_shutdown)
			{
				printf("CHwControl::AgcTimeoutThread exiting\n");
				return;
			}
		}
		m_cAgc->Timeout();
	}
}


/////////////////////////////////////////////////////////////////////
//
// Calculate dma/ddr buffer size needed for data collection
//
unsigned long CHwControl::CalcDdrCount(unsigned long sampleSize, const CConfig::STaskParams& taskParams)
{
	unsigned long ddrCount = 0;
	if (taskParams.withIq)		// Should never happen
	{
		ddrCount += sampleSize;
	}
	if (taskParams.withPsd)
	{
		ddrCount += sampleSize;
	}
	if (taskParams.withFft)
	{
		ddrCount += 2 * sampleSize;	// float complex data.
	}
	return ddrCount;
}


/////////////////////////////////////////////////////////////////////
//
// Determine if audio can be enabled
//
bool CHwControl::CanEnableAudio(void) const
{
	unsigned long saveFreq = 0;
	bool canEnable = true;
	m_sched.ForEachItem([&](const CTask::Task& item)
	{
		bool suspendableType = item->m_msgHdr.msgType == SEquipCtrlMsg::AUTOVIOLATE_CTRL ||
			item->m_msgHdr.msgType == SEquipCtrlMsg::OCCUPANCY_CTRL ||
			item->m_msgHdr.msgType == SEquipCtrlMsg::OCCUPANCYDF_CTRL;
		bool noAudioType = item->m_msgHdr.msgType == SEquipCtrlMsg::BIST_CTRL;	// TODO: Add any others needed
		if (noAudioType)
		{
			canEnable = false;
			return false;	// break ForEachItem
		}
		if (suspendableType)
		{
			CSuspendableTask::SuspendableTask suspendTask(std::dynamic_pointer_cast<CSuspendableTask>(item));
			if (suspendTask && !suspendTask->IsSuspended())
			{
				canEnable = false;
				return false;	// break ForEachItem
			}
		}
		else	// !suspendableType
		{
			auto taskParams = item->GetAllTaskParams();
			if (!taskParams.empty())
			{
				auto rxFreq = m_radioEquip->CalcRxFreqMHz(taskParams[0].hf, taskParams[0].startFreq);
				if (saveFreq != 0 && rxFreq != saveFreq)
				{
					canEnable = false;
					return false;	// break ForEachItem
				}
				saveFreq = rxFreq;
			}
		}
		return true;
	});
	return canEnable;
}


/////////////////////////////////////////////////////////////////////
//
// Collect data, performing AGC
//
void CHwControl::CollectData(_Inout_ const CTask::Task& task, CRadioEquip::EBandSelect band, std::chrono::milliseconds agcDecayTime)
{
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CConfig::SProcParams& procParams = task->GetProcParams();
	SEquipCtrlMsg::EAnt eAnt = CTask::GetEAntValue(task->m_ant, taskParams.pol);
	Units::Frequency rfFreq = task->GetTuneFreq();
	Units::Frequency rxFreq = (m_config->IsShfExtUsed(eAnt, rfFreq) ? m_config->GetShfExtIfFreq() : rfFreq);
	CTask::SBlockState& blockState = task->BlockState();
	unsigned long ddrCount = CalcDdrCount(procParams.sampleSize, taskParams);
	auto temp = procParams.sampleSize * m_digitizer->GetDecimation() + 0.5;
	auto collectionTimeSec = temp / procParams.sampleRate.Hz<double>();	// sec

#ifdef CSMS_DEBUG
	static const size_t ARRAY_SIZE = 2;
	static unsigned l_numBlocks[ARRAY_SIZE] = { 0, 0};
	static Units::Frequency l_prevFreq[ARRAY_SIZE] = { Units::Frequency(0), Units::Frequency(0)};
	bool match = true;

	if(taskParams.numBlocks <= ARRAY_SIZE)
	{
		size_t idx = 0;
		match = false;

		for(; idx < ARRAY_SIZE; ++idx)
		{
			if(l_prevFreq[idx] == rfFreq)
			{
				match = true;
				break;
			}
			else if(l_prevFreq[idx] > rfFreq)
			{
				if(idx > 0)
				{
					--idx;
				}

				l_prevFreq[idx] = rfFreq;
				l_numBlocks[idx] = taskParams.numBlocks;
				break;
			}
		}

		if(idx == ARRAY_SIZE)
		{
		    --idx;
		    l_prevFreq[idx] = rfFreq;
		    l_numBlocks[idx] = taskParams.numBlocks;
		}

		if(match && l_numBlocks[idx] != taskParams.numBlocks )
		{
			l_numBlocks[idx] = taskParams.numBlocks;
			match = false;
		}

		if(!match)
		{
			TRACE("CollectData blockState1: freqLimits (%fMHz: %fMHz): radioFreq %fMHz, inverted %d,pbCalBand %u,pbCalDirect %d,sampleCount %lu,switchTemp %f,withTerm %d, task->m_rcvrAtten %u\n",
			      blockState.freqLimits.first.Hz<float>()/1000000, blockState.freqLimits.second.Hz<float>()/1000000,
			      blockState.radioFreq.Hz<float>()/1000000, (blockState.inverted ? 1 :0), blockState.pbCalBand,
			      (blockState.pbCalDirect ? 1 :0), blockState.sampleCount, blockState.switchTemp,
			      (blockState.withTerm ? 1 :0), unsigned (task->m_rcvrAtten));

		}
	}
#endif
	if (task->m_rcvrAtten == SEquipCtrlMsg::SGetPanCmd::AGC)
	{
		// AGC
		bool ok = false;

		for (size_t attempt = 0; !ok && attempt < MAX_COLLECTION_TRIES; ++attempt)
		{
			//AGC frequency is the tuned frequency.
			if (DoAgc(rfFreq, taskParams.rxHwBw, band, taskParams.hf, eAnt, false, blockState.pbCalDirect, blockState.slaveAtten, agcDecayTime) && task->m_spurCalEnabled)
			{
				task->m_spurCal = true;
			}
#ifdef CSMS_DEBUG
			else
			{
				TRACE("CHwControl::CollectData DoAgc returns false: rfFreq %lfHz,rxHwBw %fHz,band %d,hf %d,eAnt %d,pbCalDirect %d, slaveAtten %u, spurCalEnabled %d\n",
						rfFreq.Hz<double>(),taskParams.rxHwBw.Hz<float>(), int(band), (taskParams.hf ? 1 : 0),
						int(eAnt), (blockState.pbCalDirect ? 1 : 0), unsigned(blockState.slaveAtten),
						(task->m_spurCalEnabled ? 1 : 0));
			}
#endif
			auto spaceNeeded = ReserveBufferSpace(blockState, 1U, ddrCount, task->m_spurCal);
			auto sampleTime = Utility::CurrentTimeAsTimestamp();

			//CollectSamples uses frequency to UpdateAgc, therefore tuned frequency must be used.
			ok = CollectSamples(blockState.sampleCount, procParams.sampleSize, rfFreq, taskParams.rxHwBw, band, eAnt,
					taskParams.withIq, taskParams.withPsd, taskParams.withFft, !blockState.withTerm, collectionTimeSec);

			if (!ok)
			{
				TRACE("CollectSamples not ok\n");
				if (blockState.withTerm && spaceNeeded !=0)
				{
					m_digitizer->m_sdma.FlushSamples(blockState.sampleCount, spaceNeeded);
#ifdef CSMS_DEBUG
					TRACE("CHwControl::CollectData(FlushSamples): count %lu, ddr = %lu, spaceNeeded = %lu, rfFreq = %lfMHz, sampleSize %lu,rxHwBw %f,collectionTimeSec %lf\n",
							blockState.sampleCount, ddrCount, spaceNeeded, rfFreq.Hz<double>()/1000000,
							procParams.sampleSize, taskParams.rxHwBw.Hz<float>()/1000000, collectionTimeSec);
#endif
				}
			}

			if (!m_digitizer->GetTimeStamp(blockState.sampleTime))
			{
				blockState.sampleTime = sampleTime;
			}
		}
	}
	else
	{
		// MGC
		m_radioEquip->SetAtten(task->m_rcvrAtten, rxFreq, taskParams.hf);

		if (task->m_spurCalEnabled)
		{
			task->m_spurCal = true;
		}

		ReserveBufferSpace(blockState, 1U, ddrCount, task->m_spurCal);
		auto sampleTime = Utility::CurrentTimeAsTimestamp();

		//CollectSamples uses frequency to UpdateAgc, therefore tuned frequency must be used.
		CollectSamples(blockState.sampleCount, procParams.sampleSize, rfFreq, taskParams.rxHwBw, band, eAnt,
			taskParams.withIq, taskParams.withPsd, taskParams.withFft, !blockState.withTerm, collectionTimeSec);

		if (!m_digitizer->GetTimeStamp(blockState.sampleTime))
		{
			blockState.sampleTime = sampleTime;
		}

	}

	m_radioEquip->GetAtten(blockState.rcvrAtten);
	blockState.rxGain = m_radioEquip->GetRxGainRatio(blockState.rfGainTableIdx, blockState.ifGainTableIdx, blockState.gainAdjTemp);
	blockState.gainAdj = m_digitizer->GetAdcFullScaleWatts() / (blockState.rxGain * m_digitizer->GetGainRatio(blockState.pbCalDirect));

	if(m_config->IsShfExtUsed(eAnt, rfFreq))
	{
			blockState.gainAdjdB = m_config->GetShfExtensionGain(rfFreq);
	}

#ifdef CSMS_DEBUG
	if(!match)
	{
			C2630::s_printResult = true;
			blockState.rxGain = m_radioEquip->GetRxGainRatio(blockState.rfGainTableIdx, blockState.ifGainTableIdx, blockState.gainAdjTemp);
			auto rxGainUsed = blockState.rxGain;
			TRACE("CollectData %f MHz (numBlocks %u): rxGainUsed, rxGain,rfGainTableIdx,ifGainTableIdx,gainAdjTemp,gainAdj,rcvrAtten, gainAdjdB %f %f %u %ld %f %f %u %f\n",
					rfFreq.Hz<float>()/1000000, taskParams.numBlocks, rxGainUsed, blockState.rxGain,
					blockState.rfGainTableIdx, blockState.ifGainTableIdx, blockState.gainAdjTemp,
					blockState.gainAdj, unsigned(blockState.rcvrAtten), blockState.gainAdjdB);
			TRACE("CollectData blockState2: freqLimits (%fMHz: %fMHz): radioFreq %fMHz, inverted %d,pbCalBand %u,pbCalDirect %d,sampleCount %lu,switchTemp %f,withTerm %d\n",
					blockState.freqLimits.first.Hz<float>()/1000000, blockState.freqLimits.second.Hz<float>()/1000000,
					blockState.radioFreq.Hz<float>()/1000000, (blockState.inverted ? 1 :0), blockState.pbCalBand,
					(blockState.pbCalDirect ? 1 :0), blockState.sampleCount, blockState.switchTemp,
					(blockState.withTerm ? 1 :0));
	}
#endif

//	printf("CollectData: rxGain,rfGainTableIdx,ifGainTableIdx,gainAdjTemp,gainAdj %f %u %ld %f %f\n",
//		blockState.rxGain, blockState.rfGainTableIdx, blockState.ifGainTableIdx, blockState.gainAdjTemp, blockState.gainAdj);
	return;
}


/////////////////////////////////////////////////////////////////////
//
// Collect samples from both master and slave. Returns false for bad data
//
bool CHwControl::CollectSlaveSamples(const CTask::Task& task, unsigned long sampleCount, unsigned long count, Units::Frequency rfFreq,
	CRadioEquip::EBandSelect band, SEquipCtrlMsg::EAnt input, bool doSamples, double collectionTimeSec, unsigned long numDfBins,
	bool termRadio, const Units::Frequency& midFrequency, const std::vector<unsigned long>& pattern, unsigned int delay, unsigned long& slaveTaskKey, bool enableAudio)
{
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CTask::SBlockState& blockState = task->BlockState();

//	blockState.slaveBufferKey = 0;

	// Program sequencer
	unsigned char numAnts = pattern.size();

	auto temp = count * m_digitizer->GetDecimation() + 0.5;
	if (temp > ULONG_MAX)
	{
		TRACE("CollectSlaveSamples requires too many a/d samples\n");
		temp = ULONG_MAX;
	}
	unsigned long adcCount = static_cast<unsigned long>(temp);

	int iDataTypes = 0;
	if (taskParams.withIq) iDataTypes |= SDfCtrlMsg::SlaveDataTypes::IQ;
	if (taskParams.withPsd) iDataTypes |= SDfCtrlMsg::SlaveDataTypes::PSD;
	if (taskParams.withFft) iDataTypes |= SDfCtrlMsg::SlaveDataTypes::FFT;
	SDfCtrlMsg::SlaveDataTypes dataTypes = SDfCtrlMsg::SlaveDataTypes(iDataTypes);

#if CSMS_TIMING == 1
	auto startTime = std::chrono::steady_clock::now();
#endif
	// Setup the slaves before triggering a data collection
	std::vector<size_t> slaveSentIndex;
	auto slaveNumAnts = (taskParams.mode == CConfig::EMode::PB_CAL ? 0 : pattern.size());
	auto tag = SetupCollectSlaves(count, adcCount, delay, numDfBins, dataTypes,
		termRadio, blockState.slaveAtten, m_radioEquip->GetGainMode(),
		midFrequency, pattern, slaveSentIndex, slaveNumAnts, enableAudio);
//	printf("CHwControl::CollectSlaveSamples SetupCollectSlaves tag %lu using slaveAtten = %u\n", tag, slaveAtten);
	if (tag == 0)
	{
		TRACE("CHwControl::CollectSlaveSamples failed to send request\n");
	}
	else
	{
		// Wait for ready responses from slaves before firing sequencer
//		std::vector<CProcessorNode::SDfCtrlMsgPtr> readies;
		bool waitOk = WaitForSlaves(slaveSentIndex, tag);
		if (!waitOk)
		{
			tag = 0;
		}
	}
#if CSMS_TIMING == 1
	auto endTime = std::chrono::steady_clock::now();
	auto diffTime = endTime - startTime;
	auto micro = std::chrono::duration<double, std::micro>(diffTime).count();
	if (micro > 300000.0)
	{
		printf("CollectSlaveSamples A took %f microseconds\n", micro);
	}
	startTime = std::chrono::steady_clock::now();
#endif

	unsigned long newControlWord;
	if (taskParams.hf)
	{
		newControlWord = m_switch->C72362001::GetControlWord(pattern);
	}
	else
	{
		newControlWord = m_switch->C7234201102::GetControlWord(pattern);
	}

	//Only set to DFMASTER for DF measurement, all other (pattern.size() <= 1) used NODF.
	C3230::EDfMode dfMode = ( (pattern.size() <= 1 && taskParams.mode != CConfig::EMode::PB_CAL)
						? C3230::EDfMode::NODF : C3230::EDfMode::DFMASTER);
	m_digitizer->SetAntennaControl(newControlWord);	// preset the first antenna after attens are set in slave
	m_digitizer->CollectSamples(sampleCount, count, adcCount, delay, taskParams.withIq, taskParams.withPsd, taskParams.withFft,
		doSamples, collectionTimeSec, 0, Units::Timestamp(), dfMode, pattern);

#if CSMS_TIMING == 1
	endTime = std::chrono::steady_clock::now();
	diffTime = endTime - startTime;
	micro = std::chrono::duration<double, std::micro>(diffTime).count();
	if (micro > 300000.0)
	{
		printf("CollectSlaveSamples B took %f microseconds\n", micro);
	}
	startTime = std::chrono::steady_clock::now();
#endif
	// Wait for response that data has been collected plus slave CCDF data
	std::vector<CProcessorNode::SDfCtrlMsgPtr> responses;
	if (tag != 0)
	{
		bool waitOk = WaitForSlaves(slaveSentIndex, tag, responses);
		if (!waitOk)
		{
			TRACE("CHwControl::CollectSlaveSamples: WaitForSlaves (ccdf resp) failed\n");
			tag = 0;
		}
	}
#if CSMS_TIMING == 1
	endTime = std::chrono::steady_clock::now();
	diffTime = endTime - startTime;
	micro = std::chrono::duration<double, std::micro>(diffTime).count();
	if (micro > 300000.0)
	{
		printf("CollectSlaveSamples C took %f microseconds\n", micro);
	}
	startTime = std::chrono::steady_clock::now();
#endif
	auto refInput = CConfig::GetRefAnt(input);
	bool attenOk = m_cAgc->UpdateAgc(numAnts, rfFreq, taskParams.rxHwBw, band, refInput, adcCount);
	if (tag != 0 && !responses.empty() && numAnts == 1 && input != refInput)
	{
		attenOk = true;

#ifdef CSMS_DEBUG
		if(s_traceSlave)
		{
			TRACE("CHwControl::CollectSlaveSamples UpdateAgc tag %lu, input %d, refInput %d\n",
					tag, int(input), int(refInput));
		}
#endif
		// For single antenna collection, there should be only one response
		auto& r = responses[0]->body.slaveSetupCollectResp;
		bool slaveAttenOk = m_cAgc->UpdateAgc(rfFreq, taskParams.rxHwBw, band, input, adcCount, r.counts, r.thresh);
		if (!slaveAttenOk) attenOk = false;	// false if either one is false.
	}
	if (tag != 0)
	{
		slaveTaskKey = (responses.empty() ? 0 : responses[0]->body.slaveSetupCollectResp.key);
	}
	else
		printf("slave tag is 0!\n");
	if (m_digitizer->FifoHasOverrun())
	{
		TRACE(_T("Resetting sampling (FIFO overrun)\n"));
		m_digitizer->ResetFifos();
		attenOk = false;
	}
#if CSMS_TIMING == 1
	endTime = std::chrono::steady_clock::now();
	diffTime = endTime - startTime;
	micro = std::chrono::duration<double, std::micro>(diffTime).count();
	if (micro > 300000.0)
	{
		printf("CollectSlaveSamples D took %f microseconds\n", micro);
	}
#endif

	return attenOk;
}


/////////////////////////////////////////////////////////////////////
//
// Collect data, performing AGC
//
unsigned int CHwControl::CollectSlaveData(const CTask::Task& task, unsigned long numBins, CRadioEquip::EBandSelect band,
	std::vector<size_t>& sentIndex, unsigned long tag, std::chrono::milliseconds agcDecayTime, bool enableAudio)
{
	unsigned long spaceNeeded = 0;

	Units::Frequency rfFreq = task->GetTuneFreq();
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CConfig::SProcParams& procParams = task->GetProcParams();

	bool isDf = (taskParams.mode == CConfig::EMode::DF || taskParams.mode == CConfig::EMode::SCAN_DF || taskParams.mode == CConfig::EMode::FAST_SCAN_DF);
	unsigned int numAnts = 1;
	bool isHorizon = false;
	ESequencerType sequencerType;
	bool slaveAgc = true;
	SEquipCtrlMsg::EAnt ant;
	if (isDf)
	{
		// Determine numAnts, isHorizon and set task->m_ant and antenna switch
		if (taskParams.hf)
		{
			numAnts = CAntenna::NumAnts(m_mutableConfig.hfConfig.antCable.antenna.get(), rfFreq);
			task->m_ant = CAntenna::GetMAnt(m_mutableConfig.hfConfig.antCable.antenna.get(), false, rfFreq);
		}
		else
		{
			numAnts = CAntenna::NumAnts(m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get(), rfFreq);
			if (taskParams.pol == CConfig::EPolarization::VERT)
			{
				task->m_ant = CAntenna::GetMAnt(m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get(), false, rfFreq);
				// Vertical DF
			}
			else if (taskParams.pol == CConfig::EPolarization::HORIZ)
			{
				// Horizontal DF
				task->m_ant = CAntenna::GetMAnt(m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get(), true, rfFreq);
				isHorizon = true;
			}
			else // Both
			{
				if (task->GetVHCurrentPolarization() == CConfig::EPolarization::HORIZ )
				{
					task->m_ant = CAntenna::GetMAnt(m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get(), true, rfFreq);
					isHorizon = true;
				}
				else
				{
					task->m_ant = CAntenna::GetMAnt(m_mutableConfig.vushfConfig.antCable[CConfig::ERfPort::C_RF1].antenna.get(), false, rfFreq);
				}
			}
		}
		SetAntSwitches(task, rfFreq);
		ant = CConfig::GetRefAnt(task->m_ant);
		slaveAgc = false;		// No agc on slave for df
		sequencerType = ESequencerType::DF;
	}
	else
	{
		numAnts = 1;
		isHorizon = (taskParams.pol == CConfig::EPolarization::HORIZ);
		// Antenna switch already set in SetHardware and/or DoPbCal before this
		ant = task->m_ant;

		if(taskParams.mode == CConfig::EMode::PB_CAL)
		{
			sequencerType = ESequencerType::NOISE;
		}
		else if(ant == SEquipCtrlMsg::EAnt::SMPL_RF2)
		{
			sequencerType = ESequencerType::SINGLE_FAST;
		}
		else
		{
			sequencerType = ESequencerType::SINGLE_SLOW;
		}
	}

	// For Slave Collection, the ddr3 will have one or more of (N iq words + N fft words + N fftI/fftQ words)
	// For DF collection, there will be numAnts * (N fft words + N fftI/fftQ words)
	// In both cases, if EDR, this is followed by 1 more similar block of (N iq words + N fft words + N fftI/fftQ words)

	unsigned long ddrCount = CalcDdrCount(procParams.sampleSize, taskParams);
	unsigned long sampleSize = procParams.sampleSize;
	Units::Timestamp sampleTime;

	auto temp = sampleSize * m_digitizer->GetDecimation() + 0.5;
	auto collectionTimeSec = temp / procParams.sampleRate.Hz<double>();	// sec

	CTask::SBlockState& blockState = task->BlockState();
	auto midFrequency = CProcessing::BandMidFrequency(Units::FreqPair(task->GetTuneFreq() - task->GetProcBw() / 2, task->GetTuneFreq() + task->GetProcBw() / 2),
		blockState.freqLimits);

	// Wait for slave to have settled (from tune)
	bool waitOk = WaitForSlaves(sentIndex, tag);
	if (!waitOk)
	{
		// Problem with slaves.
		numAnts = 0;
		sentIndex.clear();
	}
	else
	{
		std::vector<unsigned long> pattern;		// This will just contain the antenna switch pattern
		unsigned long savedControlWord;
		unsigned int delay;
		bool restoreSwitch = CreateSequencerPattern(sequencerType, task->m_ant, taskParams.hf, taskParams.rxHwBw, pattern, savedControlWord, delay);

		blockState.slaveBufferKey = 0;
		blockState.slaveSentIndex.clear();
		if (task->m_rcvrAtten == SEquipCtrlMsg::SGetPanCmd::AGC)
		{
			// AGC
			bool ok = false;
			for (size_t attempt = 0; !ok && attempt < MAX_COLLECTION_TRIES; ++attempt)
			{
				if (DoAgc(rfFreq, taskParams.rxHwBw, band, taskParams.hf, task->m_ant, slaveAgc, blockState.pbCalDirect,
					blockState.slaveAtten, agcDecayTime) && task->m_spurCalEnabled)
				{
					task->m_spurCal = true;
				}
				spaceNeeded = ReserveBufferSpace(blockState, numAnts, ddrCount, task->m_spurCal);
				sampleTime = Utility::CurrentTimeAsTimestamp();

				unsigned long slaveTaskKey = 0;
				ok = CollectSlaveSamples(task, blockState.sampleCount, sampleSize, rfFreq, band, ant, !blockState.withTerm,
						//collectionTimeSec, numBins, false, midFrequency, pattern, 0x5454, slaveTaskKey);
					collectionTimeSec, numBins, false, midFrequency, pattern, delay, slaveTaskKey, enableAudio);
				// if data looks ok - request it from slave. else, flush it.
				//CLog::Log(CLog::INFORMATION, _T("CollectSlaveData - collect slave---ok = %d--- attempt = %u"), ok, attempt); //debug only
				blockState.slaveBufferKey = RequestCollectedData((ok || attempt == MAX_COLLECTION_TRIES - 1),
					slaveTaskKey, blockState.slaveSentIndex);

#ifdef CSMS_DEBUG
				if(s_traceSlave)
				{
					timespec ts;
					clock_gettime(CLOCK_MONOTONIC, &ts);
					TRACE("CHwControl::CollectSlaveData RequestCollectedData %d tag %lu key %lu, sec = %ld nsec =%ld\n",
							(ok || attempt == MAX_COLLECTION_TRIES - 1),
							blockState.slaveBufferKey, slaveTaskKey, ts.tv_sec, ts.tv_nsec);
				}
#endif
				if (!ok)
				{
					TRACE("CollectSlaveData: CollectSlaveSamples not ok\n");
					if (blockState.withTerm)
					{
						m_digitizer->m_sdma.FlushSamples(blockState.sampleCount, spaceNeeded);
					}
				}

			}
		}
		else
		{
			// MGC
			m_radioEquip->SetAtten(task->m_rcvrAtten, rfFreq, taskParams.hf);
			// TODO: Is it ok for MGC for slave use gain offset? It does in SMS.
			blockState.slaveAtten = m_radioEquip->CalcSampleGain(rfFreq, isHorizon, taskParams.hf);

			if (task->m_spurCalEnabled)
			{
				task->m_spurCal = true;
			}

			spaceNeeded = ReserveBufferSpace(blockState, numAnts, ddrCount, task->m_spurCal);
			sampleTime = Utility::CurrentTimeAsTimestamp();
			unsigned long slaveTaskKey = 0;
			CollectSlaveSamples(task, blockState.sampleCount, sampleSize, rfFreq, band, ant, !blockState.withTerm,
				collectionTimeSec, numBins, false, midFrequency, pattern, delay, slaveTaskKey);
			blockState.slaveBufferKey = RequestCollectedData(true, slaveTaskKey, blockState.slaveSentIndex);
			TRACE("CHwControl::CollectSlaveData RequestCollectedData MGC tag %lu key %lu\n",
				blockState.slaveBufferKey, slaveTaskKey);
		}
		if (!m_digitizer->GetTimeStamp(blockState.sampleTime))
		{
			blockState.sampleTime = sampleTime;
		}

		if (restoreSwitch)
		{
			m_digitizer->SetAntennaControl(savedControlWord);
		}

		m_radioEquip->GetAtten(blockState.rcvrAtten);
		blockState.rxGain = m_radioEquip->GetRxGainRatio(blockState.rfGainTableIdx, blockState.ifGainTableIdx, blockState.gainAdjTemp);
		blockState.gainAdj = m_digitizer->GetAdcFullScaleWatts() / (blockState.rxGain * m_digitizer->GetGainRatio(blockState.pbCalDirect));
	}
	if (numAnts == 0 || blockState.slaveBufferKey == 0 || blockState.slaveSentIndex.empty())
	{
		TRACE("CHwControl::CollectSlaveData numAnts = %d key = %lu index = %u\n",
			numAnts, blockState.slaveBufferKey, blockState.slaveSentIndex.size());
		if (spaceNeeded > 0)
		{
			m_digitizer->m_sdma.FlushSamples(blockState.sampleCount, spaceNeeded);
		}
		numAnts = 0;	// indicate failure
	}
	return numAnts;
}


/////////////////////////////////////////////////////////////////////
//
// Collect reference channel and set next AGC. Returns false if bad data
//
bool CHwControl::CollectSamples(unsigned long sampleCount, unsigned long count, Units::Frequency rfFreq, Units::Frequency rxBw,
	CRadioEquip::EBandSelect band, SEquipCtrlMsg::EAnt input, bool withIq, bool withPsd, bool withFft, bool doSamples, double collectionTimeSec)
{
#ifdef CSMS_DEBUG
	auto decicount = m_digitizer->GetDecimation();
	auto temp = count * decicount + 0.5;

	if(s_tracePan)
	{
		TRACE("CHwControl::CollectSamples: sampleCount %lu, count %lu, decicount %lf, adcCount %lf, rfFreq %lfHz, rxBw %fHz, band %d, ant %d, withIq %d, withPsd %d, withFft %d, doSamples %d, time %lf\n",
				sampleCount, count, decicount, temp, rfFreq.Hz<double>(), rxBw.Hz<float>(),
				int(band), int(input),
				(withIq ? 1 : 0), (withPsd? 1: 0), (withFft? 1 : 0), (doSamples ? 1 :0),
				collectionTimeSec);
	}
#else
	auto temp = count * m_digitizer->GetDecimation() + 0.5;
#endif
	if (temp > ULONG_MAX)
	{
		TRACE("CollectSamples requires too many a/d samples\n");
		temp = ULONG_MAX;
	}
	unsigned long adcCount = static_cast<unsigned long>(temp);
	std::vector<unsigned long> pattern;
	m_digitizer->CollectSamples(sampleCount, count, adcCount, 15, withIq, withPsd,
			withFft, doSamples, collectionTimeSec, 0, Units::Timestamp(),
			C3230::EDfMode::NODF, pattern);
//	TRACE("CollectSamples calling UpdateAgc. doSamples=%d\n", doSamples);

	bool attenOk = m_cAgc->UpdateAgc(1, rfFreq, rxBw, band, CConfig::GetRefAnt(input), adcCount);

#ifdef CSMS_DEBUG
	if(!attenOk)
	{
		TRACE("CHwControl::CollectSamples(!attenOk): input = %u, rfFreq = %lf MHz, rxBw = %f MHz, band = %d\n",
				input, rfFreq.Hz<double>()/ 1000000, rxBw.Hz<float>()/ 1000000, int(band));
	}

	if(s_tracePan && s_bwOfInterest)
	{
		sleep(5);
		TRACE("CHwControl::CollectSamples 20 MHz B/W");
		s_bwOfInterest = false;
	}
#endif

	if (m_digitizer->FifoHasOverrun())
	{
		TRACE(_T("Resetting sampling (FIFO overrun)\n"));
		m_digitizer->ResetFifos();
		attenOk = false;
	}

	return attenOk;
}


/////////////////////////////////////////////////////////////////////
//
// Control thread function
//
void CHwControl::ControlThread(void)
{
	CTask::Task lastPanTask;	// This is only used here and needs to go out of scope at exit

	while(true)
	{
		try
		{
			// Perform next task
			CTask::Task task(m_sched.GetItem(IDLE_TIMEOUT));

			switch(task->GetTaskParams().mode)
			{
			case CConfig::EMode::AVD:
				DoAvd(std::dynamic_pointer_cast<CAvdTask>(task));
#ifdef CSMS_DEBUG
				s_traceAvd = false;
#endif
				break;

			case CConfig::EMode::BIST:
				DoBist(task);
				break;

			case CConfig::EMode::DF:
			case CConfig::EMode::MEASURE:
			case CConfig::EMode::IQDATA:
				DoMeasure(std::dynamic_pointer_cast<CMeasurementTask>(task));
				break;

			case CConfig::EMode::FAST_SCAN_DF:
			case CConfig::EMode::OCCUPANCY:
			case CConfig::EMode::SCAN_DF:
				DoOccupancyOrScanDf(std::dynamic_pointer_cast<CSuspendableTask>(task));
				break;
#if 0
			case CConfig::INIT:
				DoInit(task);
				break;
#endif
			case CConfig::EMode::PAN:
				//save pan task parameters need to perform agc loop
				if (m_config->IsDDRSystem())
				{
					lastPanTask = task;
				}
				DoPan(std::dynamic_pointer_cast<CPanTask>(task));
				break;

			case CConfig::EMode::PB_CAL:
				DoPbCal(task);
				break;
#if 0
			case CConfig::MANUAL:
				DoManual(task);
				break;
#endif
			default:
				THROW_LOGIC_ERROR();
			}
		}
		catch(TaskSched::EProblem& problem)
		{
			switch(problem)
			{
			case TaskSched::CLOSED:
				// Exit thread
				eventfd_write(m_startupComplete, 2);
				printf("CHwControl::ControlThread exiting\n");
				return;

			case TaskSched::TIMEDOUT:
				// Queue a pan (will normally just update the active pan entry in scheduler
				if (m_config->IsDDRSystem() && lastPanTask)
				{
					CAudio& audio = dynamic_cast<CAudio&>(*this);
					if (audio.IQStreamRunning())
					{
					lastPanTask->m_source = nullptr;	// No response wanted
					try
					{
						AddTask(lastPanTask, 0, true, SEquipCtrlMsg::PAN_DISP_CTRL);
					}
					catch(CHwControl::TaskSched::EProblem&) {}
				}
				}

				try
				{
					CWeakSingleton<CProcessorNode> processorNode;
					if (processorNode->IsProcessorDfMaster() && processorNode->GetStartupDoneState())
					{
						m_timeoutcounter++;
						//printf("m_timeoutcounter = %lu  m_initDFdone=%d-- nodes ready=%d\n", m_timeoutcounter, m_initDFdone, processorNode->AreNodesReady());
						std::vector<SEquipCtrlMsg::SInitializeDfResp::SPbCalStatus> status;
						if (m_pbCal->CheckifPbCalStarted())
						{
							//printf("init df not empty!\n");
							m_initDFdone = true; // we will only auto pbcal once if not done!
						}
						// send init df 1 minute after startup done if master never was able to go through due to slave not ready
						if ((m_timeoutcounter % 60 == 0) && !m_initDFdone && processorNode->AreNodesReady())
						{

							CLog::Log(CLog::INFORMATION, _T("Init DF issued due to time out"));
							try
							{
								// Do passband cal
								SEquipCtrlMsg msg;
								msg.hdr.cmdVersion = 2;
								msg.hdr.respVersion = 2;
								msg.hdr.msgType = SEquipCtrlMsg::DF_CTRL;
								msg.hdr.msgSubType = SEquipCtrlMsg::INITIALIZE_DF;
								msg.hdr.bodySize = sizeof(SEquipCtrlMsg::SInitializeDf);
								msg.body.initializeDf.freq = Units::Frequency(0).GetRaw();
								msg.body.initializeDf.rxBw = Units::Frequency(0).GetRaw();
								msg.body.initializeDf.numIntegrations = 0;
								msg.body.initializeDf.hf = false;
								auto task = CTask::Task(new CTask(&msg, nullptr));
								task->RegisterTask(true);
								AddTask(task);
							}
							catch (TaskSched::EProblem problem) {}	// Ignore error here
							catch (ErrorCodes::EErrorCode error) {CLog::Log(CLog::INFORMATION, _T("auto start pbcal error %d"), error); }
						}
					}

				}
				catch(CWeakSingleton<CProcessorNode>::NoInstance&)
				{
					TRACE(" CHwControl::ControlThread Unable to access processorNode\n");
				}

				// Toggle LED
				m_digitizer->ToggleLed();
				break;

			default:
				// Something bad
				THROW_LOGIC_ERROR();
			}
		}
	}
}


//////////////////////////////////////////////////////////////////////
//
// Create the parameters needed for the sequencer
//
bool CHwControl::CreateSequencerPattern(ESequencerType type, SEquipCtrlMsg::EAnt ant, bool hf, Units::Frequency rxHwBw,
	std::vector<unsigned long>& pattern, unsigned long& currentControlWord, unsigned int& delay) const
{
	if (type == ESequencerType::SINGLE_FAST || type == ESequencerType::SINGLE_SLOW)
	{
		pattern.resize(1);
		delay = (type == ESequencerType::SINGLE_FAST ? 15 : CDigitizer::ADC_CLOCK_RATE / ANT_SWITCH_DELAY);
		if (hf)
		{
			pattern[0] = C72362001::Source(ant) & 0x0f;
		}
		else
		{
			pattern[0] = C7234201102::Source(ant) & 0x0f;
		}
		return false;
	}
	if (type == ESequencerType::NOISE)
	{
		pattern.resize(1);
		delay = CDigitizer::ADC_CLOCK_RATE / ANT_SWITCH_DELAY;	// 20 us
		if (hf)
		{
			pattern[0] = C72362001::ESource::ANT_NOISE;
		}
		else
		{
			pattern[0] = C7234201102::ESource::ANT_NOISE;
		}
		return false;
	}
	// Must be DF type
	pattern.clear();
	unsigned char numAnts = 1;
	delay = 0;
	bool ok = true;

	// ant specifies how many antennas to collect data from
	switch(ant)
	{
	case SEquipCtrlMsg::DF_ANT_3V:
		if (hf)
		{
			// HF loops
			m_switch->C72362001::GetCurrentControlWord(currentControlWord);
			numAnts = 3;
			delay += CDigitizer::ADC_CLOCK_RATE / 100000; // 10 us

			for (size_t subBlock = 0; subBlock < numAnts; ++subBlock)
			{
				pattern.push_back(C72362001::Source(subBlock) & 0x0f);
			}
		}

		break;

	case SEquipCtrlMsg::DF_ANT_5V:
		{
			// 649-8 Low band array
			Units::Frequency narrowBw = m_radioEquip->GetRxBandwidth(false, true);

			m_switch->C7234201102::GetCurrentControlWord(currentControlWord);
			numAnts = 5;
			delay += CDigitizer::ADC_CLOCK_RATE / (rxHwBw == narrowBw ? 125000 : 200000); // 8 or 5 us

			auto grayCode = C7234201102::GRAY_CODE5;
			for (size_t subBlock = 0; subBlock < numAnts; ++subBlock)
			{
				pattern.push_back(grayCode[subBlock]);
			}
		}

		break;

	case SEquipCtrlMsg::DF_ANT_9H:
	case SEquipCtrlMsg::DF_ANT_9V:
		if (hf)
		{
			assert(ant == SEquipCtrlMsg::DF_ANT_9V);
			if (ant == SEquipCtrlMsg::DF_ANT_9V)
			{
				// HF interferometer
				m_switch->C72362001::GetCurrentControlWord(currentControlWord);
				numAnts = 9;
				delay += CDigitizer::ADC_CLOCK_RATE / 100000; // 10 us

				for (size_t subBlock = 0; subBlock < numAnts; ++subBlock)
				{
					pattern.push_back(C72362001::Source(subBlock));
				}
			}
		}
		else
		{

			// U/SHF
			m_switch->C7234201102::GetCurrentControlWord(currentControlWord);
			numAnts = 9;
//			Units::Frequency narrowBw = m_radioEquip->GetRxBandwidth(false, true);
//			delay += CDigitizer::ADC_CLOCK_RATE / (rxBw == narrowBw ? 125000 : 200000); // 8 or 5 us
			delay += CDigitizer::ADC_CLOCK_RATE / ANT_SWITCH_DELAY; // 20 us	// TODO: trying long delay

			auto grayCode = C7234201102::GRAY_CODE9[ant == SEquipCtrlMsg::DF_ANT_9V ? 0 : 1];
			for (size_t subBlock = 0; subBlock < numAnts; ++subBlock)
			{
				pattern.push_back(grayCode[subBlock] & 0x0f);
			}
		}
		break;

	default:
		ASSERT(FALSE);
		ok = false;
		break;
	}
	return ok;
}


//////////////////////////////////////////////////////////////////////
//
// Delete a task from the scheduler
//
void CHwControl::DeleteTask(_In_ const SEquipCtrlMsg::STaskIdKey& taskIdKey)
{
//	bool rv =
		m_sched.RemoveItem([&](CTask::Task& item)
	{
		return item->m_taskIdKey.key == taskIdKey.key;
	});
//	TRACE("CHwControl::DeleteTask returned %d for key %lu\n", rv, taskIdKey.key);
	try
	{
		CAudio& audio = dynamic_cast<CAudio&>(*this);
		audio.Enable(CanEnableAudio());
	}
	catch(const std::bad_cast&)
	{
		TRACE("DeleteTask bad_cast\n");
	}
}

#ifdef CSMS_DEBUG
//////////////////////////////////////////////////////////////////////
//
// Disable AVD trace.
//
void CHwControl::DisableAvdTrace(void)
{
	s_traceAvd = false;
#ifdef CSMS_2016
	C3230::s_trace3220 = false;
#endif
	CProcessing::s_traceAvd = false;
	CSBufferBase::s_traceAvd = false;
}

//////////////////////////////////////////////////////////////////////
//
// Enable AVD trace.
//
void CHwControl::EnableAvdTrace(void)
{
	s_traceAvd = true;
#ifdef CSMS_2016
	C3230::s_trace3220 = true;
#endif
	CProcessing::s_traceAvd = true;
	CSBufferBase::s_traceAvd = true;
}

//////////////////////////////////////////////////////////////////////
//
// Enable PAN trace.
//
void CHwControl::EnablePanTrace(void)
{
	s_tracePan = true;
#ifdef CSMS_2016
	C3230::s_trace3220 = true;
#endif
	CProcessing::s_traceAvd = true;
	CSBufferBase::s_traceAvd = true;
}
#endif

//////////////////////////////////////////////////////////////////////
//
// Perform AGC
//
bool CHwControl::DoAgc(Units::Frequency freq, Units::Frequency rxBw, CRadioEquip::EBandSelect band,
	bool hf, SEquipCtrlMsg::EAnt eAnt, bool doSlave, bool direct, unsigned char& slaveAtten, std::chrono::milliseconds agcDecayTime)
{
	auto refInput = CConfig::GetRefAnt(eAnt);
	bool agcSlave = (doSlave && eAnt != refInput);

	unsigned char atten;
	auto gainMode = m_config->GetGainMode(freq, hf);
	bool gainChange = m_cAgc->GetAgcMaster(atten, freq, rxBw, band, refInput, gainMode, direct, agcDecayTime);
	m_radioEquip->SetAtten(atten, freq, hf);

	if (agcSlave)
	{
		slaveAtten = CAgc::INVALID_ATTEN;
		bool slaveChange = GetAgcSlave(slaveAtten, freq, rxBw, band, hf, eAnt, agcDecayTime);
		// merge results from slave and master
		if (slaveChange) gainChange = true;	// true if either one has a gain change
	}
	else
	{
		slaveAtten = m_radioEquip->CalcSampleGain(freq, CConfig::GetAntPolarization(eAnt) == CConfig::EPolarization::HORIZ, hf);
	}
//	printf("DoAgc setting atten to %u %f %d returning %d\n", atten, freq.Hz<double>(), direct, gainChange);
	return gainChange;
}


//////////////////////////////////////////////////////////////////////
//
// Perform AVD task
//
void CHwControl::DoAvd(_In_ const AvdTask& task)
{
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CConfig::SProcParams& procParams = task->GetProcParams();
#ifdef CSMS_DEBUG
	static Units::Frequency l_startFreq(0);
	static size_t l_count = 0;
	size_t loopCount = 1;
	static bool l_TraceOutput = false;
	bool endDwellDbgFlag = false;
	++l_count;

	if((l_startFreq != taskParams.startFreq) || s_traceAvd || ((l_count % 100) == 0))
	{
		l_startFreq = taskParams.startFreq;
		l_TraceOutput = true;
		TRACE("CHwControl::DoAvd: %lf %lf %lf %lf %u, l_count=%u, key =%lu, taskId = %lu\n",
			taskParams.startFreq.Hz<double>(), taskParams.stopFreq.Hz<double>(),
			taskParams.bw.Hz<double>(), taskParams.rxHwBw.Hz<double>(),
			taskParams.numBlocks, l_count, task->m_taskIdKey.key, task->m_taskIdKey.taskId);
#ifdef CSMS_2016
		if(s_traceAvd && C3230::ScratchRegsValid())
		{
			C3230::ScratchRegsOutTrace();
		}
#endif
	}

#endif

	// Check for shutdown
	if (/*Utility::IsQuitting() ||*/ m_sched.IsClosed())
	{
		m_sched.SetRunnable([&](CTask::Task& item){ return item->m_taskIdKey.key == task->m_taskIdKey.key; }, false);
		return;
	}

	// Start the dwell timer
	DATE endDwellTime = Utility::CurrentTimeAsDATE();
	task->ClearEndOfDwell();
	endDwellTime += CConfig::OCCUPANCY_DWELL_TIME_MS / double(Units::MILLISECONDS_PER_DAY);

	CRadioEquip::EBandSelect bandSelect = CRadioEquip::EBandSelect::BAND1;

	// Loop until we need to yield
	while (true)
	{
		if (task->CheckSuspend())
		{
			TRACE("CHwControl::DoAvd: CheckSuspend setting not runnable\n");
			m_sched.SetRunnable([&](CTask::Task& item){ return item->m_taskIdKey.key == task->m_taskIdKey.key; }, false);
			try
			{
				CAudio& audio = dynamic_cast<CAudio&>(*this);
				audio.Enable(CanEnableAudio());
			}
			catch(const std::bad_cast&)
			{
				TRACE("CHwControl::DoAvd bad_cast\n");
			}
			return;
		}

		CTask::SBlockState& blockState = task->BlockState();
		blockState.failedDf = false;
		bool needSlave = m_config->IsSlaveAntenna(task->m_ant, taskParams.hf);
		unsigned long tag = 0;
		std::vector<size_t> sentIndex;
		bool failedSlave = false;
		auto nextBand = SetHardware(task, bandSelect, needSlave, &tag, &sentIndex);

#ifdef CSMS_DEBUG
		if(blockState.withTerm && !s_withTermOn)
		{
			//Off turn to on.
			++s_withTermCnt;
			l_TraceOutput = s_withTermOn = true;
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			TRACE("CHwControl::DoAvd(withTermOn): sec = %ld nsec =%ld; termCnt = %u\n",
					ts.tv_sec, ts.tv_nsec, s_withTermCnt);
			//EnableAvdTrace();
		}
		else if (s_withTermOn && !blockState.withTerm)
		{
			//On turn to off
			l_TraceOutput = s_withTermOn = false;
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			TRACE("CHwControl::DoAvd(withTermOff): sec = %ld nsec =%ld; termCnt = %u\n",
					ts.tv_sec, ts.tv_nsec, s_withTermCnt);
			//DisableAvdTrace();
		}
#endif

		if (needSlave)
		{
			if (tag == 0 || sentIndex.empty())
			{
				TRACE("CHwControl::DoAvd: CHwControl::SetHardware returned tag = %lu, sentIndex = %u\n", tag, sentIndex.size());
				failedSlave = true;
				blockState.failedDf = true;
			}
		}

		auto numBins = procParams.GetNumBins(task->GetProcBw());
		if (!needSlave || !failedSlave)
		{
			if (needSlave)
			{
				if (CollectSlaveData(task, numBins, bandSelect, sentIndex, tag) == 0)
				{
					failedSlave = true;
					blockState.failedDf = true;
				}
			}
			else
			{
				CollectData(task, bandSelect);
			}
		}

		if(!failedSlave && task->m_spurCal && m_mutableConfig.miscConfig.enhancedDynRng)
		{
			DoSpurCal(needSlave, task, 1, numBins);
		}

		// Update bandSelect for next time
		bandSelect = nextBand;

		DATE now = Utility::CurrentTimeAsDATE();

		if (bandSelect == CRadioEquip::EBandSelect::BAND1)
		{
			// At end of dwell?
			if (now >= endDwellTime)
			{
#ifdef CSMS_DEBUG
				TRACE("CHwControl::DoAvd: SetEndOfDwell, rfFreq = %lf, loopCount %u\n",
						task->GetTuneFreq().Hz<double>(), loopCount);
				endDwellDbgFlag = true;
#endif
				task->SetEndOfDwell();
			}
		}
		// If there's a runnable system task in the scheduler, abandon the dwell
		if (m_sched.CountRunnable(static_cast<int>(CTask::ETaskGroup::SYSTEM)) > 0 && !task->AtDwellEnd())
		{
#ifdef CSMS_DEBUG
			if(endDwellDbgFlag)
			{
				TRACE("CHwControl::DoAvd: SetAbandonDwell, rfFreq = %lf, loopCount %u\n",
							task->GetTuneFreq().Hz<double>(), loopCount);
			}
#endif
			task->SetAbandonDwell(true);
		}

		bool yield = false;

		// Copy to processing queue
#ifdef CSMS_DEBUG
		++s_addAvd2ProcCnt;

		if(l_TraceOutput || endDwellDbgFlag)
		{
			TRACE("CHwControl::DoAvd: AddTaskToProcessingQueue(%u), rfFreq = %lf, loopCount %u\n",
					s_addAvd2ProcCnt, task->GetTuneFreq().Hz<double>(), loopCount);
		}
#endif
		if (!failedSlave || (needSlave && !sentIndex.empty()))
		{
			AddTaskToProcessingQueue(task->SpawnTask());
		}

		if (bandSelect == CRadioEquip::EBandSelect::BAND1)
		{
			if (task->AtEnd())
			{
				// Is it time to stop?
				if (m_sched.IsClosed() || task->Completed())
				{
					TRACE("CHwControl::DoAvd: Completed - setting non-runnable\n");
					m_sched.SetRunnable([&](CTask::Task& item){ return item->m_taskIdKey.key == task->m_taskIdKey.key; }, false);
					try
					{
						CAudio& audio = dynamic_cast<CAudio&>(*this);
						audio.Enable(CanEnableAudio());
					}
					catch(const std::bad_cast&)
					{
						TRACE("CHwControl::DoAvd bad_cast\n");
					}
					return;
				}
				task->UpdateReportTime();

				// Exit and get next task (which might be next cycle of this one)
#ifdef CSMS_DEBUG
				if(l_TraceOutput)
				{
					TRACE("CHwControl::DoAvd: AtEnd setting non-runnable and returning, loopCount %u\n", loopCount);
				}
#endif
				m_sched.SetRunnable([&](CTask::Task& item){ return item->m_taskIdKey.key == task->m_taskIdKey.key; }, false);
				return;
			}
			// Check for other tasks to yield
			if (task->GetAbandonDwell())
			{
				// Yield immediately
				yield = true;
			}
			else
			{
				yield = task->AtStepEnd();	// Yield at end of band
				if (!yield && m_sched.Count() > 1)	// There are other tasks
				{
					// Yield at end of dwell
					yield = task->AtDwellEnd();
				}
			}

			// Next iteration
			if (now >= endDwellTime)
			{
#ifdef CSMS_DEBUG
				if(l_TraceOutput || endDwellDbgFlag)
				{
					TRACE("CHwControl::DoAvd: Next iteration, loopCount %u\n", loopCount);

					if(!s_traceAvd)
					{
						l_TraceOutput = false;
					}
				}
#endif
				task->NextBlock();
				endDwellTime = Utility::CurrentTimeAsDATE();
				task->ClearEndOfDwell();
				endDwellTime += CConfig::OCCUPANCY_DWELL_TIME_MS / double(Units::MILLISECONDS_PER_DAY);
			}
		}

		if (yield)
		{
			TRACE("CHwControl::DoAvd: yielding\n");
			task->SetAbandonDwell(false);
			return;
		}

#ifdef CSMS_DEBUG
		endDwellDbgFlag = false;
		++loopCount;
#endif
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Perform self-test
//
void CHwControl::DoBist(_In_ const CTask::Task& task)
{
	// Mute demodulation
//	dynamic_cast<CAudio&>(*this).SetDemodMode(SSmsMsg::SRcvrCtrlCmd::OFF);

	// Do BIST
	CLog::Log(CLog::INFORMATION, _T("Starting BIST"));
	if (m_config->IsDDRSystem()) // fault led only apply to DDR systems for now
	{
		m_exitFaultLedThread = true;
		if (m_FaultLedThread.joinable())
		{
			m_FaultLedThread.join();
		}

		m_exitFaultLedThread = false;
		m_digitizer->SetFaultStatusLed(false);
		if (m_config->IsDDRSystem())
			m_FaultLedThread = std::thread(&CHwControl::FaultLEDThread, this);
		m_faultLedSate = EFaultLEDState::SLOW_BLINK;
		sleep(4); // debug only - to observe slow blink with fast bist completion
	}
	CBist::DoBist(task);

	if (m_config->IsDDRSystem()) // fault led only apply to DDR systems for now
	{
		if (CBist::m_OverallResult != SSmsMsg::SGetBistResp::FAIL)
		{
			// set solid light
			m_faultLedSate = EFaultLEDState::ON;
			m_digitizer->SetFaultStatusLed(true);
			m_exitFaultLedThread = true;
		}
		else
		{
			m_faultLedSate = EFaultLEDState::FAST_BLINK; // fast blink for failed bist
		}
	}
	if(task->m_source == nullptr)
	{
		eventfd_write(m_startupComplete, 2);
	}
	DeleteTask(task->m_taskIdKey);
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Perform measurement
//
void CHwControl::DoMeasure(_In_ const MeasurementTask& task)
{
//	TRACE("DoMeasure\n");
	//printf ("do measure called\n"); // debug only
	while (true)
	{
		// Check for shutdown
		if (Utility::IsQuitting() || m_sched.IsClosed())
		{
			m_sched.SetRunnable([&](CTask::Task& item){ return item->m_taskIdKey.key == task->m_taskIdKey.key; }, false);
			return;
		}

		const CConfig::SProcParams& procParams = task->GetProcParams();
		const CConfig::STaskParams& taskParams = task->GetTaskParams();

		if (task->IsAvd())
		{
#ifdef CSMS_DEBUG
			if(s_traceAvd)
			{
				TRACE("DoMeasure task IsAvd, mode = %u\n", unsigned(taskParams.mode));
#ifdef CSMS_2016
				if(C3230::ScratchRegsValid())
				{
					C3230::ScratchRegsOutTrace();
				}
#endif
			}
#endif
			CAvdTask::AvdTask avdTask(std::dynamic_pointer_cast<CAvdTask>(task));

			if (!avdTask)
			{
				THROW_LOGIC_ERROR();
			}

			if (avdTask->CheckSuspend())
			{
				return;
			}

			if (taskParams.mode == CConfig::EMode::AVD)
			{
				// Switch to AVD mode (AVD occupancy steps after AVD measurements)
				DoAvd(avdTask);

				return;
			}
		}

		// Set the hardware

		Units::Frequency rfFreq(taskParams.startFreq);
		CTask::SBlockState& blockState = task->BlockState();
		bool shfExtused = m_config->IsShfExtUsed(task->m_ant, rfFreq);
		Units::Frequency rxFreq(shfExtused ? m_config->GetShfExtIfFreq(): rfFreq);
		bool doSlave = (shfExtused ? false: true);

//		bool failedDf = false;
		std::vector<size_t> sentIndex;
		unsigned long tag = 0;
		bool needSlave = m_config->IsSlaveAntenna(task->m_ant, taskParams.hf);
		blockState.failedDf = false;

		SetHardware(task, CRadioEquip::EBandSelect::OPTIMUM, (needSlave || taskParams.mode == CConfig::EMode::DF), &tag, &sentIndex);

#ifdef CSMS_DEBUG
		if(blockState.withTerm && !s_withTermOn)
		{
			//Off turn to on.
			++s_withTermCnt;
			s_withTermOn = true;
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			TRACE("CHwControl::DoMeasure(withTermOn): sec = %ld nsec =%ld; termCnt = %u\n",
					ts.tv_sec, ts.tv_nsec, s_withTermCnt);
			//EnableAvdTrace();
		}
		else if (s_withTermOn && !blockState.withTerm)
		{
			//On turn to off
			s_withTermOn = false;
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			TRACE("CHwControl::DoMeasure(withTermOff): Disabled not called. sec = %ld nsec =%ld; termCnt = %u\n",
					ts.tv_sec, ts.tv_nsec, s_withTermCnt);
			//DisableAvdTrace();
		}
#endif

		if (taskParams.mode == CConfig::EMode::IQDATA)
		{
			// Note: This is always the first step in the task and has only 1 block
			TRACE("DoMeasure: IQDATA %f %f %f %u\n",
				taskParams.startFreq.Hz<double>(), taskParams.stopFreq.Hz<double>(), taskParams.rxHwBw.Hz<double>(), taskParams.numBlocks);

			// Note: can't use CollectData() since agc needs to be done before scheduling collection (or need to handle starttime)
			unsigned long ddrCount = CalcDdrCount(procParams.sampleSize, taskParams);
			if (needSlave)
			{
				// Wait for slave to have settled (from tune)
//				std::vector<CProcessorNode::SDfCtrlMsgPtr> responses;
				bool waitOk = WaitForSlaves(sentIndex, tag);
				if (!waitOk)
				{
					// Problem with slaves.
					printf("CHwControl::DoMeasure WaitForSlaves(tune) failed\n");
					m_digitizer->m_sdma.FlushSamples(blockState.sampleCount, ddrCount);
					m_sched.SetRunnable([&](CTask::Task& item){ return item->m_taskIdKey.key == task->m_taskIdKey.key; }, false);
					return;
				}
			}

			//AGC frequency is the tuned frequency.
			DoAgc(rfFreq, taskParams.rxHwBw, CRadioEquip::EBandSelect::OPTIMUM, taskParams.hf, task->m_ant, doSlave, blockState.pbCalDirect, blockState.slaveAtten, DEFAULT_AGC_DECAY_TIME);

			m_radioEquip->GetAtten(blockState.rcvrAtten);
			blockState.rxGain = m_radioEquip->GetRxGainRatio(blockState.rfGainTableIdx, blockState.ifGainTableIdx, blockState.gainAdjTemp);
			blockState.gainAdj = m_digitizer->GetAdcFullScaleWatts() / (blockState.rxGain * m_digitizer->GetGainRatio(blockState.pbCalDirect));

			//Get the SHF Extension Gain.
			blockState.gainAdjdB = (shfExtused ? m_config->GetShfExtensionGain(rfFreq) : 0);
			ReserveBufferSpace(blockState, 1U, ddrCount, false);
			auto temp = procParams.sampleSize * m_digitizer->GetDecimation() + 0.5;
			auto collectionTimeSec = temp / procParams.sampleRate.Hz<double>();	// sec
			if (temp > ULONG_MAX)
			{
				TRACE("DoMeasure::IQDATA requires too many a/d samples\n");
				temp = ULONG_MAX;
			}
			unsigned long adcCount = static_cast<unsigned long>(temp);

			Units::Timestamp sampleTime;
			Units::Timestamp iqStartTime;	// init to 0
			Units::TimeSpan tdoaAdjust;	// init to 0
			if (needSlave)
			{
				// TODO: not supporting tdoa here (yet)
				blockState.slaveBufferKey = 0;
				blockState.slaveSentIndex.clear();

				std::vector<unsigned long> pattern;		// This will just contain the antenna switch pattern
				unsigned long savedControlWord;
				unsigned int delay;
				CreateSequencerPattern(ESequencerType::SINGLE_FAST, task->m_ant, taskParams.hf, taskParams.rxHwBw, pattern, savedControlWord, delay);

				// AGC already done, buffer space reserved, gainAdj set - just need to collect the data
				sampleTime = Utility::CurrentTimeAsTimestamp();
				auto midFrequency = CProcessing::BandMidFrequency(Units::FreqPair(task->GetTuneFreq() - task->GetProcBw() / 2, task->GetTuneFreq() + task->GetProcBw() / 2),
					blockState.freqLimits);
				unsigned long slaveTaskKey = 0;
				bool ok = CollectSlaveSamples(task, blockState.sampleCount, procParams.sampleSize, rfFreq, CRadioEquip::EBandSelect::OPTIMUM,
					task->m_ant, true, collectionTimeSec, procParams.sampleSize, false, midFrequency, pattern, delay, slaveTaskKey);
				blockState.slaveBufferKey = RequestCollectedData(true, slaveTaskKey, blockState.slaveSentIndex);
				TRACE("CHwControl::DoMeasure IQDATA RequestCollectedData tag %lu key %lu\n",
					blockState.slaveBufferKey, slaveTaskKey);
				if (!ok)
				{
					printf("CollectSlaveSamples not ok\n");
				}
				// We have already done UpdateAGC so don't do it again later.
			}
			else
			{
				sampleTime = Utility::CurrentTimeAsTimestamp();
				iqStartTime = Units::Timestamp(task->GetIqStartTime());
				if (iqStartTime > Units::Timestamp())	// tdoa request
				{
					// Adjust the iqStartTime by the expected csms-sms delay
					Units::TimeSpan startAdjust;
					m_config->GetTdoaAdjustment(taskParams.bw, procParams.decimations, blockState.pbCalDirect, tdoaAdjust, startAdjust);
					if (iqStartTime < sampleTime + tdoaAdjust + startAdjust)
					{
						printf("*********** Unable to adjust iqStartTime\n");
					}
					else
					{
						printf("Adjust iq starttime by %ld %ld\n", tdoaAdjust.NanoSeconds<long>(), startAdjust.NanoSeconds<long>());
						iqStartTime -= (tdoaAdjust + startAdjust);
					}
				}
				std::vector<unsigned long> pattern;
				m_digitizer->CollectSamples(blockState.sampleCount, procParams.sampleSize, adcCount, 15, taskParams.withIq, taskParams.withPsd, taskParams.withFft,
					true, collectionTimeSec, task->GetDigitizerBlockCount(), iqStartTime, C3230::EDfMode::NODF, pattern);
			}
			// Now can update the actual startTime
			if (!m_digitizer->GetTimeStamp(blockState.sampleTime))
			{
				blockState.sampleTime = sampleTime;
			}
//			TRACE("DoMeasure::IQDATA requested %s actual %s\n",
//				Units::Timestamp(task->GetIqStartTime()).Format(true).c_str(), blockState.sampleTime.Format(true).c_str());
			if (iqStartTime > Units::Timestamp())
			{
				// Get the latency counter value
				// Adjust the starttime by the latency
#ifndef NDEBUG
				auto latency = m_digitizer->GetLatencyCounter();
				auto adjust = latency / procParams.sampleRate.Hz<double>();
#endif
				blockState.sampleTime += tdoaAdjust;
				TRACE("DoMeasure::IQDATA actual - requested = %lld nsec, latency = %lu, %f %lld\n",
					(blockState.sampleTime - Units::Timestamp(task->GetIqStartTime())).NanoSeconds<long long>(), latency, 1000000000. * adjust,
					tdoaAdjust.NanoSeconds<long long>());
			}
			AddTaskToProcessingQueue(task->SpawnTask());

			if (!needSlave)		// UpdateAgc was done in CollectSlaveSamples.
			{
				//AGC frequency is the tuned frequency.
				m_cAgc->UpdateAgc(1, rfFreq, taskParams.rxHwBw, CRadioEquip::EBandSelect::OPTIMUM, CConfig::GetRefAnt(task->m_ant), adcCount);
			}
		}
		else if (taskParams.mode == CConfig::EMode::MEASURE)
		{
			// Note: This will have only one step (measure), multiple blocks. Calibration has been removed.
			TRACE("DoMeasure: MEASURE %f %f %f %u\n",
				taskParams.startFreq.Hz<double>(), taskParams.stopFreq.Hz<double>(), taskParams.rxHwBw.Hz<double>(), taskParams.numBlocks);

			// This is the actual measurement step

			// Do measurements except DF
			if (task->GetBlock() == 0)
			{
				task->m_spurCal = true;
				task->m_spurCalEnabled = true;
			}

			// SetHardware has been called. Ready to collect.

			unsigned long numBins = procParams.GetNumBins(taskParams.bw) + 1;
			if (needSlave && (tag == 0 || sentIndex.empty()))
			{
				TRACE("CHwControl::DoMeasure(MEASURE): SetHardware returned tag = %lu, sentIndex = %u\n", tag, sentIndex.size());
				blockState.failedDf = true;
			}
			else	// !needSlave || (tag != 0 && !sentIndex.empty()) - not using slave or are using and no failure so far
			{
				if (needSlave)
				{
					if (CollectSlaveData(task, numBins, CRadioEquip::EBandSelect::OPTIMUM, sentIndex, tag) == 0)
					{
						blockState.failedDf = true;
					}
				}
				else
				{
					CollectData(task, CRadioEquip::EBandSelect::OPTIMUM, DEFAULT_AGC_DECAY_TIME);
				}
			}
			if (task->m_spurCal && m_mutableConfig.miscConfig.enhancedDynRng)
			{
#ifdef CSMS_DEBUG
				TRACE("CHwControl::DoMeasure: DoSpurCal %f %f %f %u\n",
					taskParams.startFreq.Hz<double>(), taskParams.stopFreq.Hz<double>(), taskParams.rxHwBw.Hz<double>(), taskParams.numBlocks);
#endif
				// Need to do spurCal (terminated) processing
				DoSpurCal(needSlave, task, 1, numBins);
			}
		}
		else if (taskParams.mode == CConfig::EMode::DF)
		{
			// Note: This is always the last step
			TRACE("DoMeasure: DF %f %f %f %u\n",
				taskParams.startFreq.Hz<double>(), taskParams.stopFreq.Hz<double>(), taskParams.rxHwBw.Hz<double>(), taskParams.numBlocks);

			if (tag == 0 || sentIndex.empty())
			{
				TRACE("CHwControl::DoMeasure(DF): SetHardware returned tag = %lu sentIndex = %u\n", tag, sentIndex.size());
//				failedDf = true;
			}
			else
			{
				if (task->GetBlock() == 0)
				{
					// First cut
					// TODO: sms skips this the hf, is that what we should do?
					task->m_spurCal = true;
					task->m_spurCalEnabled = true;
				}

				task->InitDf(Utility::CurrentTimeAsDATE());

				auto numAnts = CollectSlaveData(task, task->GetDfNumBins(), CRadioEquip::EBandSelect::OPTIMUM, sentIndex, tag);
				if (numAnts != 0)
				{
					if (task->m_spurCal && m_mutableConfig.miscConfig.enhancedDynRng)
					{
						// Need to do spurCal (terminated) processing
						DoSpurCal(false, task, numAnts, task->GetDfNumBins());	// Don't bother doing spurcal for slave
					}
				}
			}
		}
		else
		{
			THROW_LOGIC_ERROR();
		}

		task->m_spurCal = false;

		if(task->EnoughGoodCuts())
		{
			task->SetEndOfTask();
		}

		// Send to processing queue
		if (taskParams.mode != CConfig::EMode::IQDATA &&
				(!blockState.failedDf || (needSlave && !sentIndex.empty())))
		{
			// Send even if df failed since samples have already been flushed and this will clean up properly
			AddTaskToProcessingQueue(task->SpawnTask());
		}

		if (task->AtEnd())
		{
			m_sched.SetRunnable([&](CTask::Task& item){ return item->m_taskIdKey.key == task->m_taskIdKey.key; }, false);
			return;
		}

		task->NextBlock();
		// This is mostly for testing - prints the progress. Could send new message to client, if desired.
		if (task->GetNumBlocks() >= 5000)
		{
			if (task->GetBlock() % 1000 == 0)
			{
				printf("DoMeasure block %u / %u\n", task->GetBlock(), task->GetNumBlocks());
			}
		}

		// See if this task should continue or yield to scheduler
		// The measurement task can have one or more steps (IQDATA, MEASURE[n], DF) each of which contain multiple blocks
		// If there any other tasks in the scheduler, yield now (end of dwell)
		if (m_sched.Count() > 1)
		{
			if (task->AtDwellEnd())
				return;
		}

//		if (this group's count == 1 and count of all other groups == 0)
//		{
//			// No other tasks pending
//			continue;
//		}
//		else if (this group's count == 1 and count of background group > 0 and count of other groups == 0)
//		{
//			// Only other tasks are background tasks
//			if (task->AtStepEnd())
//				return;
//		}
//		else
//		{
//			// There are some other non-background tasks
//			if (task->AtDwellEnd())
//				return;
//		}
	}
}


//////////////////////////////////////////////////////////////////////
//
// Perform occupancy or scan DF task
//
void CHwControl::DoOccupancyOrScanDf(_In_ const SuspendableTask& task)
{
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CConfig::SProcParams& procParams = task->GetProcParams();

//	TRACE("DoOccupancyOrScanDf: %f %f %f %u\n",
//		taskParams.startFreq.Hz<double>(), taskParams.stopFreq.Hz<double>(), taskParams.rxHwBw.Hz<double>(), taskParams.numBlocks);

	// Check for shutdown
	if (/*Utility::IsQuitting() ||*/ m_sched.IsClosed())
	{
		m_sched.SetRunnable([&](CTask::Task& item){ return item->m_taskIdKey.key == task->m_taskIdKey.key; }, false);
		return;
	}

	// Start the dwell / scan rate timer
	DATE endDwellTime = Utility::CurrentTimeAsDATE();
	task->ClearEndOfDwell();

	if (taskParams.mode == CConfig::EMode::OCCUPANCY && task->m_reportInterval != 0)
	{
		endDwellTime += CConfig::OCCUPANCY_DWELL_TIME_MS / double(Units::MILLISECONDS_PER_DAY);
	}

	CRadioEquip::EBandSelect bandSelect = CRadioEquip::EBandSelect::BAND1;

	// Loop until we need to yield
	while (true)
	{
		if (task->CheckSuspend())
		{
			TRACE("DoOccupancyOrScanDf: setting not runnable\n");
			m_sched.SetRunnable([&](CTask::Task& item){ return item->m_taskIdKey.key == task->m_taskIdKey.key; }, false);
			try
			{
				CAudio& audio = dynamic_cast<CAudio&>(*this);
				audio.Enable(CanEnableAudio());
			}
			catch(const std::bad_cast&)
			{
				TRACE("DoOccupancyOrScanDf bad_cast\n");
			}
			return;
		}

		CTask::SBlockState& blockState = task->BlockState();
		blockState.failedDf = false;
		bool isDf = taskParams.mode == CConfig::EMode::SCAN_DF || taskParams.mode == CConfig::EMode::FAST_SCAN_DF;
		bool isSlaveAnt = m_config->IsSlaveAntenna(task->m_ant, taskParams.hf);
		bool needSlave = isDf || isSlaveAnt;
		bool slaveSpurCal = false;
		unsigned long tag = 0;
		std::vector<size_t> sentIndex;
		bool failedSlave = false;
		auto nextBand = SetHardware(task, bandSelect, needSlave, &tag, &sentIndex);

#ifdef CSMS_DEBUG
		if(blockState.withTerm && !s_withTermOn)
		{
			//Off turn to on.
			++s_withTermCnt;
			s_withTermOn = true;
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			TRACE("CHwControl::DoOccupancyOrScanDf(withTermOn): sec = %ld nsec =%ld; termCnt = %u\n",
					ts.tv_sec, ts.tv_nsec, s_withTermCnt);
			//EnableAvdTrace();
		}
		else if (s_withTermOn && !blockState.withTerm)
		{
			//On turn to off
			s_withTermOn = false;
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			TRACE("CHwControl::DoOccupancyOrScanDf(withTermOff): sec = %ld nsec =%ld; termCnt = %u\n",
					ts.tv_sec, ts.tv_nsec, s_withTermCnt);
			//DisableAvdTrace();
		}
#endif

		if (needSlave)
		{
			if (tag == 0 || sentIndex.empty())
			{
				TRACE("CHwControl::DoOccupancyOrScanDf: SetHardware returned tag = %lu, sentIndex = %u\n", tag, sentIndex.size());
				failedSlave = true;
				blockState.failedDf = true;
			}
		}
		if (task->IsTerminatedDataEmpty(task->GetStep(), task->GetBlock()))
		{
			task->m_spurCal = true;
			task->m_spurCalEnabled = true;
		}

		unsigned int numAnts = 0;
		auto numBins = procParams.GetNumBins(task->GetProcBw());
		if (taskParams.mode == CConfig::EMode::OCCUPANCY)
		{
			if (!isSlaveAnt || !failedSlave)	// either not using slave or are using slave and not failed already
			{
				if (isSlaveAnt)
				{
					numAnts = CollectSlaveData(task, numBins, bandSelect, sentIndex, tag);
					if (numAnts == 0)
					{
						failedSlave = true;
						blockState.failedDf = true;
					}
					else
					{
						slaveSpurCal = true;
					}
				}
				else
				{
					CollectData(task, bandSelect);
					numAnts = 1;
				}
			}
		}
		else if (isDf)
		{
			if (!failedSlave)
			{
				TRACE("CHwControl::DoOccupancyOrScanDf: taskId = %lu, step = %u, numBlocks = %u, procBw = %f, numbins = %lu\n",
						task->m_taskIdKey.taskId, task->GetStep(),
						task->GetNumBlocks(), task->GetProcBw().Hz<double>(),
						procParams.GetNumBins(task->GetProcBw()));
				numAnts = CollectSlaveData(task, procParams.GetNumBins(task->GetProcBw()), bandSelect, sentIndex, tag);
				if (numAnts == 0)
				{
					failedSlave = true;
					blockState.failedDf = true;
				}
			}
		}
		else
		{
			THROW_LOGIC_ERROR();
		}

		if (!failedSlave && task->m_spurCal && m_mutableConfig.miscConfig.enhancedDynRng)
		{
			// Need to do spurCal (terminated) processing. Buffer space has been reserved already.
			DoSpurCal(slaveSpurCal, task, numAnts, numBins);
		}

		task->m_spurCal = false;

//		printf("CHwControl::DoOccupancyOrScanDf: taskId = %lu, step = %u, block = %u, procBw = %f, numbins = %lu, bandSelect = %d, nextBand = %d\n",
//			task->m_taskIdKey.taskId, task->GetStep(), task->GetBlock(), task->GetProcBw().Hz<double>(), procParams.GetNumBins(task->GetProcBw()),
//			bandSelect, nextBand);

		// Update bandSelect for next time
		bandSelect = nextBand;

		DATE now = Utility::CurrentTimeAsDATE();
		if (bandSelect == CRadioEquip::EBandSelect::BAND1)
		{
			// At end of dwell?
			if (taskParams.mode == CConfig::EMode::SCAN_DF || taskParams.mode == CConfig::EMode::FAST_SCAN_DF || task->m_reportInterval == 0 || now >= endDwellTime)
			{
				task->SetEndOfDwell();
			}
		}
		// If there's a runnable system task in the scheduler, abandon the dwell
		if (m_sched.CountRunnable(static_cast<int>(CTask::ETaskGroup::SYSTEM)) > 0 && !task->AtDwellEnd())
		{
			task->SetAbandonDwell(true);
		}

		bool yield = false;

		if (!failedSlave || (needSlave && !sentIndex.empty()))
		{
			// Copy to processing queue only if no failure sending to slave.
			AddTaskToProcessingQueue(task->SpawnTask());
		}

		if (bandSelect == CRadioEquip::EBandSelect::BAND1)
		{
			// Is it time to stop?
//			if (m_sched.IsClosed() || (task->AtEnd() && task->Completed()) ||
//				((taskParams.mode == CConfig::EMode::SCAN_DF || taskParams.mode == CConfig::EMode::FAST_SCAN_DF) &&
//				task->Terminated()))
			if (m_sched.IsClosed() || (task->AtEnd() && task->Completed()))	// SS 05/26/2017
			{
				m_sched.SetRunnable([&](CTask::Task& item){ return item->m_taskIdKey.key == task->m_taskIdKey.key; }, false);
				try
				{
					CAudio& audio = dynamic_cast<CAudio&>(*this);
					audio.Enable(CanEnableAudio());
				}
				catch(const std::bad_cast&)
				{
					TRACE("DoOccupancyOrScanDf bad_cast\n");
				}
				return;
			}

			// Check for other tasks to yield
			if (task->GetAbandonDwell())
			{
				// Yield immediately
				yield = true;
			}
			else
			{
				yield = task->AtStepEnd();	// Yield at end of band
				if (!yield && m_sched.Count() > 1)	// There are other tasks
				{
					// Yield at end of dwell
					yield = task->AtDwellEnd();
				}
			}

			// Next iteration
			if (task->AtEnd())
			{
				task->UpdateReportTime();
			}

			if ((taskParams.mode == CConfig::EMode::SCAN_DF || taskParams.mode == CConfig::EMode::FAST_SCAN_DF || task->m_reportInterval == 0 || now >= endDwellTime))
			{
				// No integration or integration complete
				task->NextBlock();

				if (taskParams.mode == CConfig::EMode::OCCUPANCY)
				{
					endDwellTime = Utility::CurrentTimeAsDATE();
					task->ClearEndOfDwell();

					if (task->m_reportInterval != 0)
					{
						endDwellTime += CConfig::OCCUPANCY_DWELL_TIME_MS / double(Units::MILLISECONDS_PER_DAY);
					}
				}
			}
		}
		if (yield)
		{
			task->SetAbandonDwell(false);
			return;
		}
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Perform pan task
//
void CHwControl::DoPan(_In_ const PanTask& task)
{
	// Do pan
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	const CConfig::SProcParams& procParams = task->GetProcParams();
	CTask::SBlockState& blockState = task->BlockState();

	//PAN numBlocks should be 1, only 2 if SHF Extension is used that has
	// bandwidth < 80 MHz.
	auto numBlocks = task->GetNumBlocks();
	ASSERT(numBlocks == 1 || numBlocks == 2);
	Units::Frequency startFreq;
	Units::Frequency stopFreq;

	if(numBlocks == 2)
	{
		task->GetStartStopFreq(startFreq, stopFreq);
	}

	bool needSlave = m_config->IsSlaveAntenna(task->m_ant, taskParams.hf);

	for(size_t block = 0; block < numBlocks; ++block)
	{
		unsigned long tag = 0;
		std::vector<size_t> sentIndex;

			if(numBlocks == 2)
			{
				if(block == 0)
				{
					task->SetStartStopFreq(startFreq-CConfig::C_20_MHZ, stopFreq-CConfig::C_20_MHZ);
				}
				else
				{
					task->SetStartStopFreq(startFreq+CConfig::C_20_MHZ, stopFreq+CConfig::C_20_MHZ);
				}
			}

		SetHardware(task, CRadioEquip::EBandSelect::OPTIMUM, needSlave, &tag, &sentIndex);

	#ifdef CSMS_DEBUG
			if(blockState.withTerm && !s_withTermOn)
			{
				//Off turn to on.
				++s_withTermCnt;
				s_withTermOn = true;
				timespec ts;
				clock_gettime(CLOCK_MONOTONIC, &ts);
				TRACE("CHwControl::DoPan(withTermOn): sec = %ld nsec =%ld; termCnt = %u\n",
						ts.tv_sec, ts.tv_nsec, s_withTermCnt);
				//EnableAvdTrace();
			}
			else if (s_withTermOn && !blockState.withTerm)
			{
				//On turn to off
				s_withTermOn = false;
				timespec ts;
				clock_gettime(CLOCK_MONOTONIC, &ts);
				TRACE("CHwControl::DoPan(withTermOff): sec = %ld nsec =%ld; termCnt = %u\n",
						ts.tv_sec, ts.tv_nsec, s_withTermCnt);
				//DisableAvdTrace();
			}
	#endif

		blockState.failedDf = false;		// TODO: Consider eliminating this field completely.
		unsigned long numBins = procParams.GetNumBins(taskParams.bw) + 1;
		if (needSlave && (tag == 0 || sentIndex.empty()))
		{
			TRACE("CHwControl::DoPan SetHardware returned tag = %lu, sentIndex = %u\n", tag, sentIndex.size());
			blockState.failedDf = true;
		}
		else 	// not using slave or are using slave and no failure so far
		{
			if (needSlave)
			{
				if (CollectSlaveData(task, numBins, CRadioEquip::EBandSelect::OPTIMUM, sentIndex, tag, task->m_agcTime, true) == 0)
				{
					blockState.failedDf = true;
				}
			}
			else
			{
				//printf("collect 1\n");
				CollectData(task, CRadioEquip::EBandSelect::OPTIMUM, task->m_agcTime);
			}

			// Now have the gain adjustment and the timestamp in blockState - add to the audio class
			dynamic_cast<CAudio&>(*this).AddScaleFactor(blockState.sampleTime, blockState.gainAdj);

			if (task->m_spurCal && task->m_source != nullptr && m_mutableConfig.miscConfig.enhancedDynRng)
			{
				// Need to do spurCal (terminated) processing. Buffer space has been reserved already.
				//printf("collect 2 for spur cal\n");
				DoSpurCal(needSlave, task, 1, numBins, true);
			}
			// Turn off spur calculation for this task
			task->m_spurCal = false;

			if (!blockState.failedDf || (needSlave && !sentIndex.empty()))
			{
				AddTaskToProcessingQueue(task->SpawnTask());
			}
			task->NextBlock();
		}
	}

	if(numBlocks == 2)
	{
		//Resetore the start & stop frequency.
		task->SetStartStopFreq(startFreq, stopFreq);
	}

	if (task->AtEnd())
	{
		m_sched.SetRunnable([&](CTask::Task& item){ return item->m_taskIdKey.key == task->m_taskIdKey.key; }, false);
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Perform passband calibration
//
void CHwControl::DoPbCal(_In_ const CTask::Task& task)
{
	//task->SetEndOfTask(); // debug only--skip pbcal
	CRadioEquip::EBandSelect bandSelect = CRadioEquip::EBandSelect::BAND1;

	// Loop until done
	int numFails = 0;

	while (true)
	{
		// Check for shutdown
		if (Utility::IsQuitting() ||  m_sched.IsClosed() || numFails > 8)
		{
//			m_sched.SetRunnable([&](CTask::Task& item){ return item->m_taskIdKey.key == task->m_taskIdKey.key; }, false);
			task->SetEndOfTask();
		}

		const CConfig::SProcParams& procParams = task->GetProcParams();

		CTask::SBlockState& blockState = task->BlockState();
		blockState.failedDf = false;

		unsigned long tag = 0;
		std::vector<size_t> sentIndex;
		++numFails;		// increment counter now - will get reset if all is ok
		auto nextBand = SetHardware(task, bandSelect, true, &tag, &sentIndex);

#ifdef CSMS_DEBUG
		if(blockState.withTerm && !s_withTermOn)
		{
			//Off turn to on.
			++s_withTermCnt;
			s_withTermOn = true;
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			TRACE("CHwControl::DoPbCal(withTermOn): sec = %ld nsec =%ld; termCnt = %u\n",
					ts.tv_sec, ts.tv_nsec, s_withTermCnt);
			//EnableAvdTrace();
		}
		else if (s_withTermOn && !blockState.withTerm)
		{
			//On turn to off
			s_withTermOn = false;
			timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			TRACE("CHwControl::DoPbCal(withTermOff): sec = %ld nsec =%ld; termCnt = %u\n",
					ts.tv_sec, ts.tv_nsec, s_withTermCnt);
			//DisableAvdTrace();
		}
#endif
		if (tag == 0)
		{
			TRACE("CHwControl::DoPbCal SetHardware returned tag = %lu\n", tag);
			sentIndex.clear();
			blockState.failedDf = true;
		}
		else if (sentIndex.empty())
		{
			TRACE("CHwControl::DoPbCal SetHardware sentIndex is empty\n");
			blockState.failedDf = true;
		}
		else
		{
			// Turn on noise source for both channels
			if (task->GetTaskParams().hf)
			{
				if (m_switch->C51432017::IsPresent())
				{
					m_switch->C72362001::SetSwitch(task->GetTuneFreq(), C72362001::RECEIVE, C72362001::NOISE, C72362001::NOISE);
				}
			}
			else
			{
				if (m_switch->C51432017::IsPresent())
				{
					m_switch->C7234201102::SetSwitch(task->GetTuneFreq(), C7234201102::RECEIVE, C7234201102::NOISE, C7234201102::NOISE);
				}
			}
			auto agcDecay = std::chrono::milliseconds(0);
			if (CollectSlaveData(task, procParams.GetNumBins(task->GetProcBw()), bandSelect, sentIndex, tag, agcDecay) == 0)
			{
				blockState.failedDf = true;
			}
			else
			{
				// success, reset failed counter
				numFails = 0;
			}
		}
		bandSelect = nextBand;

		if (!blockState.failedDf && !sentIndex.empty())
		{
			AddTaskToProcessingQueue(task->SpawnTask());
		}

		if (bandSelect == CRadioEquip::EBandSelect::BAND1)
		{
			// Is it time to stop?
			if (m_sched.IsClosed() || task->AtEnd())
			{
				m_sched.SetRunnable([&](CTask::Task& item){ return item->m_taskIdKey.key == task->m_taskIdKey.key; }, false);
				try
				{
					CAudio& audio = dynamic_cast<CAudio&>(*this);
					audio.Enable(CanEnableAudio());
				}
				catch(const std::bad_cast&)
				{
					TRACE("DoPbCal bad_cast\n");
				}
				if (task->GetTaskParams().hf)
				{
					m_switch->C72362001::SetSwitch(0, C72362001::RECEIVE, C72362001::TERM, C72362001::TERM);
				}
				else
				{
					m_switch->C7234201102::SetSwitch(0, C7234201102::RECEIVE, C7234201102::TERM, C7234201102::TERM);
				}
				return;
			}
			task->NextBlock();
		}
	}
}


/////////////////////////////////////////////////////////////////////
//
// Perform spurCal (terminated) data collection & queue for processing
//
void CHwControl::DoSpurCal(bool needSlave, const CTask::Task& task, unsigned int numAnts, unsigned long numBins, bool enableAudio)
{
	const CConfig::SProcParams& procParams = task->GetProcParams();
	const CConfig::STaskParams& taskParams = task->GetTaskParams();

//	printf("DoSpurCal: freq=%f bw=%f\n", freq.Hz<double>(), taskParams.bw.Hz<double>());

	SEquipCtrlMsg::EAnt eAnt = CTask::GetEAntValue(task->m_ant, taskParams.pol);
	Units::Frequency rfFreq = task->GetTuneFreq();
	Units::Frequency rxfreq = (m_config->IsShfExtUsed(eAnt, rfFreq) ? m_config->GetShfExtIfFreq() : rfFreq);

	CTask::SBlockState& blockState = task->BlockState();
	unsigned long ddrCount = CalcDdrCount(procParams.sampleSize, taskParams);
	auto temp = procParams.sampleSize * m_digitizer->GetDecimation() + 0.5;
	auto collectionTimeSec = temp / procParams.sampleRate.Hz<double>();	// sec

	unsigned char currentAntenna = m_radioEquip->GetAntenna();
	unsigned char setAntenna;
	m_radioEquip->SetAntenna(S2630Msg::TERMINATE, setAntenna);

	// Save current agc on/off setting and turn agc off
	bool agc = m_cAgc->GetAgc();
	m_cAgc->SetAgc(false);

	auto sampleTime = Utility::CurrentTimeAsTimestamp();
	// Just collect one block of (iq,fft,fftiq)
	if (needSlave)
	{
		auto midFrequency = CProcessing::BandMidFrequency(Units::FreqPair(task->GetTuneFreq() - task->GetProcBw() / 2, task->GetTuneFreq() + task->GetProcBw() / 2),
			blockState.freqLimits);
		std::vector<unsigned long> pattern(1, m_digitizer->GetCurrentAntenna());
		blockState.slaveTermBufferKey = 0;
		blockState.slaveTermSentIndex.clear();
		auto delay = CDigitizer::ADC_CLOCK_RATE / ANT_SWITCH_DELAY; // 20 us	// TODO: trying long delay
		unsigned long slaveTaskKey = 0;
		//CLog::Log(CLog::INFORMATION, _T("collect for slave spur cal")); //debug only
		CollectSlaveSamples(task, blockState.sampleCount + numAnts * ddrCount, procParams.sampleSize, rxfreq, CRadioEquip::EBandSelect::OPTIMUM, eAnt,
			true, collectionTimeSec, numBins, true, midFrequency, pattern, delay, slaveTaskKey, enableAudio);
		blockState.slaveTermBufferKey = RequestCollectedData(true, slaveTaskKey, blockState.slaveTermSentIndex);
		TRACE("CHwControl::DoSpurCal RequestCollectedData tag %lu key %lu\n", blockState.slaveTermBufferKey, slaveTaskKey);
	}
	else
	{
		//CollectSamples uses frequency to UpdateAgc, therefore tuned frequency must be used.
		CollectSamples(blockState.sampleCount + numAnts * ddrCount, procParams.sampleSize, rfFreq, taskParams.rxHwBw, CRadioEquip::EBandSelect::OPTIMUM, eAnt,
			taskParams.withIq, taskParams.withPsd, taskParams.withFft, true, collectionTimeSec);

#ifdef CSMS_DEBUG
		static size_t l_sample0Count = 0;

		if(blockState.sampleCount == 0)
		{
			++l_sample0Count;
		}
	
		TRACE("CHwControl::DoSpurCal(FlushSamples): count %lu, numAnts %u, ddr = %lu, rfFreq = %lfMHz, sampleSize %lu,rxHwBw %f,collectionTimeSec %lf, l_sample0Count %u\n",
		      blockState.sampleCount, numAnts, ddrCount, rfFreq.Hz<double>()/1000000,
			  procParams.sampleSize, taskParams.rxHwBw.Hz<float>()/1000000, collectionTimeSec, l_sample0Count);
		static Units::Frequency l_prevFreq(0);

		if(rfFreq == Units::Frequency(10000000000) && (l_prevFreq != rfFreq))
		{
			l_prevFreq = rfFreq;
			TRACE("CHwControl::DoSpurCal 10 GHz: count,sampleSize,rxHwBw,collectionTimeSec %lu %lu %f %lf\n",
			      blockState.sampleCount + numAnts * ddrCount, procParams.sampleSize, taskParams.rxHwBw.Hz<float>()/1000000,
			      collectionTimeSec);

		}
		else if(rfFreq == Units::Frequency(3000000000) && (l_prevFreq != rfFreq))
		{
			l_prevFreq = rfFreq;
			TRACE("CHwControl::DoSpurCal 3 GHz: count,sampleSize,rxHwBw,collectionTimeSec %lu %lu %f %lf\n",
			      blockState.sampleCount + numAnts * ddrCount, procParams.sampleSize, taskParams.rxHwBw.Hz<float>()/1000000,
			      collectionTimeSec);
		}
		else if(rfFreq == Units::Frequency(322500000) && (l_prevFreq != rfFreq))
		{
			l_prevFreq = rfFreq;
			TRACE("CHwControl::DoSpurCal 322.5 MHz: count,sampleSize,rxHwBw,collectionTimeSec %lu %lu %f %lf\n",
				blockState.sampleCount + numAnts * ddrCount, procParams.sampleSize, taskParams.rxHwBw.Hz<float>()/1000000,
				collectionTimeSec);
	}
#endif

	}
	if (!m_digitizer->GetTimeStamp(blockState.sampleTime))
	{
		blockState.sampleTime = sampleTime;
	}

	// Restore agc on/off setting and antenna
	m_cAgc->SetAgc(agc);
	m_radioEquip->SetAntenna(currentAntenna, setAntenna);

	return;
}


////////////////////////////////////////////////////////////////////
//
// Perform startup tasks and wait for them to complete
//
void CHwControl::DoStartupTasks(void)
{
	TRACE("Entered DoStartupTasks\n");

	//Only Passband cal at startup if it is a DF system, otherwise we'll
	// encounter throw anyway.
	if(m_config->IsDfProcessor())
	{
		try
		{
			// Do passband cal
			SEquipCtrlMsg msg;
			msg.hdr.cmdVersion = 2;
			msg.hdr.respVersion = 2;
			msg.hdr.msgType = SEquipCtrlMsg::DF_CTRL;
			msg.hdr.msgSubType = SEquipCtrlMsg::INITIALIZE_DF;
			msg.hdr.bodySize = sizeof(SEquipCtrlMsg::SInitializeDf);
			msg.body.initializeDf.freq = Units::Frequency(0).GetRaw();
			msg.body.initializeDf.rxBw = Units::Frequency(0).GetRaw();
			msg.body.initializeDf.numIntegrations = 0;
			msg.body.initializeDf.hf = false;
			auto task = CTask::Task(new CTask(&msg, nullptr));
			task->RegisterTask(true);
			AddTask(task);
		}
		catch (TaskSched::EProblem problem) {}	// Ignore error here
		catch (ErrorCodes::EErrorCode error) { CLog::Log(CLog::INFORMATION, _T("Startup pbcal error = %d"), error); }
	}

	try
	{
		// Do BIST
		SEquipCtrlMsg msg;
		msg.hdr.cmdVersion = 0;
		msg.hdr.respVersion = 0;
		msg.hdr.msgType = SEquipCtrlMsg::BIST_CTRL;
		msg.hdr.msgSubType = SEquipCtrlMsg::GET_BIST;
		msg.hdr.bodySize = 0;
		AddTask(CTask::Task(new CTask(&msg, nullptr)));
	}
	catch (TaskSched::EProblem problem)
	{
		switch(problem)
		{
		case TaskSched::CLOSED:
			// Shutting down
			eventfd_write(m_startupComplete, 2);
			break;

		default:
			// Something bad
			THROW_LOGIC_ERROR();
		}
	}
	catch (ErrorCodes::EErrorCode)
	{
		CLog::Log(CLog::ERRORS, "DoStartupTasks: Failed to complete");
		eventfd_write(m_startupComplete, 2);
	}

	TRACE("DoStartupTasks waiting\n");

	// Wait for the tasks to complete, or for the thread to die
	epoll_event epollEvent;
	epollEvent.events = EPOLLIN;
	epollEvent.data.fd = m_startupComplete;
	epoll_ctl(m_startupEpoll, EPOLL_CTL_ADD, epollEvent.data.fd, &epollEvent);

	while(epoll_wait(m_startupEpoll, &epollEvent, 1, -1) == -1 && errno == EINTR);
	auto event = epollEvent.data.fd;
	eventfd_t value;
	eventfd_read(epollEvent.data.fd, &value);
	printf("startup event = %d\n", event);

	return;
}


bool CHwControl::GetAgcSlave(_Out_ unsigned char& atten, Units::Frequency freq, Units::Frequency rxBw, CRadioEquip::EBandSelect band,
	bool hf, SEquipCtrlMsg::EAnt eAnt, std::chrono::milliseconds agcDecayTime)
{
	// See if we have a stored setting
	bool gainChange = false;
	if (!m_cAgc->TryGetAgc(freq, rxBw, band, eAnt, gainChange, atten, agcDecayTime))
	{
		// Perform initial AGC
		unsigned char initAtten = CAgc::INVALID_ATTEN;
		signed char attenAdj = 0;
		if (!m_config->IsSlaveAntenna(eAnt, hf))
		{
			printf("GetAgcSlave: invalid antenna\n");
			atten = CAgc::INVALID_ATTEN;
			return false;
		}
//		printf("request initial agc from slave\n");
		std::vector<size_t> sentIndex;
		auto tag = InitialAgcSlaves(freq, m_config->GetGainMode(freq, hf), sentIndex);
		if (tag == 0 || sentIndex.empty())
		{
			printf("problem requesting initial AGC from slaves\n");
			atten = CAgc::INVALID_ATTEN;
			return false;
		}
		std::vector<CProcessorNode::SDfCtrlMsgPtr> responses;
		bool waitOk = WaitForSlaves(sentIndex, tag, responses);
		if (!waitOk)
		{
			// Problem
			printf("problem getting initial AGC from slaves - wait failed\n");
			atten = CAgc::INVALID_ATTEN;	// invalid atten
			return false;
		}
		// Handle responses from all the slaves - only one slave should be involved
		if (responses.empty())
		{
			printf("problem getting initial AGC from slaves - no responses\n");
			atten = CAgc::INVALID_ATTEN;	// invalid atten
			return false;
		}
		for (size_t i = 0; i < responses.size(); ++i)
		{
			if (responses[i])
			{
				auto& r = responses[i]->body.doInitialAgcResp;
				initAtten = r.initAtten;
				attenAdj = r.attenAdj;
				gainChange = true;
			}
		}
		if (initAtten == CAgc::INVALID_ATTEN)
		{
			printf("GetAgcSlave: failed to get valid atten\n");
			atten = CAgc::INVALID_ATTEN;
			return false;
		}
		// Create a new entry
		m_cAgc->AddInitialAgc(freq, rxBw, band, eAnt, initAtten, attenAdj, gainChange, atten, agcDecayTime);
	}
	return gainChange;
}

//////////////////////////////////////////////////////////////////////
//
// Get the most recent cache value of SGetCsmsFaultResp
//
void CHwControl::GetFaultDetails(_Out_ SEquipCtrlMsg::SGetCsmsFaultResp & faultStat) const
{
	m_monitor.GetFaultDetails(faultStat);
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Get a task from the scheduler
//
CTask::Task CHwControl::GetTask(_In_ const SEquipCtrlMsg::STaskIdKey& taskIdKey)
{
	CTask::Task task;
	if (m_sched.RetrieveItem([&](CTask::Task& item)
	{
		return item->m_taskIdKey.key == taskIdKey.key;
	}, task))
	{
		return task;
	}
	return CTask::Task();
}


bool CHwControl::GetTask(CNetConnection<void>* source, SEquipCtrlMsg::EMsgType msgType, CTask::Task& existingTask)
{
	if (m_sched.RetrieveItem([&](CTask::Task& item)
	{
		return item->m_msgHdr.msgType == msgType && item->m_source == source;
	}, existingTask))
	{
		return true;
	}
	return false;
}


//////////////////////////////////////////////////////////////////////
//
// Request initial AGC from slaves
//
unsigned long CHwControl::InitialAgcSlaves(Units::Frequency rfFreq, CRadioEquip::EGainMode gainMode, std::vector<size_t>& sentIndex)
{
	SDfCtrlMsg msg;
	msg.hdr.msgType = SDfCtrlMsg::INTERNAL_DF_CTRL;
	msg.hdr.msgSubType = SDfCtrlMsg::DO_INITIAL_AGC;
	msg.hdr.sourceAddr = 0;
	msg.hdr.destAddr = 0;
	msg.hdr.bodySize = sizeof(SDfCtrlMsg::SDoInitialAgc);

	msg.body.doInitialAgc.rfFreq = rfFreq.GetRaw();
	if (gainMode == CRadioEquip::EGainMode::NORMAL) msg.body.doInitialAgc.gainMode = SDfCtrlMsg::NORMAL;
	else if (gainMode == CRadioEquip::EGainMode::RURAL) msg.body.doInitialAgc.gainMode = SDfCtrlMsg::RURAL;
	else if (gainMode == CRadioEquip::EGainMode::URBAN) msg.body.doInitialAgc.gainMode = SDfCtrlMsg::URBAN;
	else if (gainMode == CRadioEquip::EGainMode::CONGESTED) msg.body.doInitialAgc.gainMode = SDfCtrlMsg::CONGESTED;
	else msg.body.doInitialAgc.gainMode = SDfCtrlMsg::NORMAL;

	unsigned long tag = 0;
	try
	{
		CWeakSingleton<CProcessorNode> procNode;
		tag = procNode->SendToAllNodes(msg, sentIndex, true);		// Send tagged message
	}
	catch(CWeakSingleton<CProcessorNode>::NoInstance&)
	{
		TRACE("CHwControl::InitialAgcSlaves Unable to access processorNode\n");
	}

	return tag;
}

//////////////////////////////////////////////////////////////////////
//
// Monitor Thread
//
void CHwControl::FaultLEDThread(void)
{
	try
	{
		if (m_config->IsDDRSystem())
		{
			const unsigned long slowUpdate = 33;
			const unsigned long fastUpdate = 2;
			unsigned long slowCounter = 0;
			unsigned long fastCounter = 0;
			while(true)
			{
				{
					std::lock_guard<std::mutex> lock(m_shutdownMutex);
					if (m_shutdown)
					{
						printf("CHwControl::FaultLEDThread exiting on shutdown\n");
						return;
					}
				}

				if(m_exitFaultLedThread)
				{
					printf("CHwControl::FaultLEDThread exiting passing BIST\n");
					return;
				}


				usleep(30000);
				fastCounter++;
				slowCounter++;

				if (fastCounter == fastUpdate)
				{
					fastCounter = 0;
					if (m_faultLedSate == EFaultLEDState::FAST_BLINK) // this loop runs 100 ms interval
						m_digitizer->ToggleFaultStatusLed();
				}

				if (slowCounter == slowUpdate)
				{
					slowCounter = 0;
					if (m_faultLedSate == EFaultLEDState::SLOW_BLINK) // this loop runs 100 ms interval
						m_digitizer->ToggleFaultStatusLed();
				}



			}
		}
	}
	catch(...)
	{
		CFailure::OnFail(std::current_exception());
	}

}

//////////////////////////////////////////////////////////////////////
//
// Monitor Thread
//
void CHwControl::MonitorThread(void)
{
	// Setup watchdog
	int monitor = m_watchdog->RegisterPingSource(_T("hardware monitor thread"));

	try
	{
		bool had3230Timestamp = Reset3230Time(true, false);
		bool hadNtpTime = m_navigation->IsNtpSyncd();
		if (had3230Timestamp && m_digitizer->m_setSeconds)
		{
			printf("MonitorThread: Resetting time and setting seconds\n");
		}

		const unsigned long slowUpdate = 10;
		unsigned long slowCounter = 0;

		// Poll until shutdown
		auto Polling = [this]
		{
			std::unique_lock<std::mutex> lock(m_shutdownMutex);
			return !m_shutdownCond.wait_for(lock, MONITOR_POLLING_INTERVAL, [this] { return m_shutdown; });
		};

		while(Polling())
		{
			// Ping the watchdog
			m_watchdog->Ping(monitor);

			// Ping the slave (no response to this message)
			try
			{
				CWeakSingleton<CProcessorNode> processorNode;
				if (processorNode->IsProcessorDfMaster())
				{
					processorNode->PingAllNodes();
				}
			}
			catch(CWeakSingleton<CProcessorNode>::NoInstance&) {}

			// Monitor everything
			// Note: This has to be the only thread that updates fault details!
			auto bistSummary = GetFinalSummary(); 	// Get BIST summary

			bool has3230Timestamp;
			if (!m_monitor.Monitor(slowCounter == 0, bistSummary, 50000, has3230Timestamp))
			{
				TRACE("Monitor failed\n");
			}

			// Request the slave's fault data (once every 10 seconds)
			if (slowCounter == 0)
			{
				try
				{
					CWeakSingleton<CProcessorNode> processorNode;
					if (processorNode->IsProcessorDfMaster())
					{
						// Send a message to the remote nodes
						std::vector<size_t> faultSentMask;	// 0 => not sent, 1 => sent. size = total number of nodes
						//auto numSent =
						processorNode->SendGetFaultToAllNodes(faultSentMask);

						// If anything was sent, wait for the responses now
						for (size_t node = 0; node < faultSentMask.size(); ++node)
						{
							bool gotResp = false;
							SDfCtrlMsg::SGetSlaveCsmsFaultResp slaveResp;
							if (faultSentMask[node] == 1)
							{
								for (size_t i = 0; i < 10; ++i)
								{
									if (processorNode->IsNodeFaultDone(node, slaveResp))
									{
										gotResp = true;
										break;
									}
									usleep(10000);	// 10 msec
								}
							}
							if (gotResp)
							{
								m_monitor.AddSlaveResponse(node, slaveResp);
							}
							else
							{
								m_monitor.AddSlaveResponse(node);
							}
						}
					}

				}
				catch(CWeakSingleton<CProcessorNode>::NoInstance&) {}

			}

			++slowCounter;
			if (slowCounter == slowUpdate)
			{
				slowCounter = 0;
				// check for disk read only state and reboot if catched
				// Get a list of the current mounts and extract the info for /media
				auto fp = fopen("/proc/self/mountinfo", "r");
				//auto fp = fopen("/media/tci/mountinfo", "r"); // debug only, so we can test ro case
				if (fp != nullptr)
				{
					char buffer[1024];
					while (fgets(buffer, 1024, fp))
					{
						char* p;
						if ((p = strstr(buffer, "/media")) != nullptr)
						{
							// Find the last space in the line
							if ((p = strrchr(p, ' ')) != nullptr)
							{
								if (strncmp(p + 1, "ro", 2) == 0)
								{
									// caught media turned read only, reboot and allow startup fsck to fix
									CLog::Log(CLog::ERRORS, "MonitorThread: media turned read only, rebooting to fix media state");
									// reboot the computer now
									LinuxUtility::ShutdownSystem(0, false, nullptr);
									break;
								}
								else if (strncmp(p + 1, "rw", 2) == 0)
								{
									// media is read/write, do nothing
								}
							}
						}
					}
					fclose(fp);
				}
			}

			// If timestamp status changes from bad to good, reset and set seconds again.
			bool hasNtpTime = m_navigation->IsNtpSyncd();
			bool changed3230 = !had3230Timestamp && has3230Timestamp;
			bool changedNtp = !hadNtpTime && hasNtpTime;
			if (changed3230 || changedNtp)
			{
				printf("Resetting time and setting seconds %s %s\n", (changed3230 ? "3230" : "    "), (changedNtp ? "ntp" : "   "));
				Reset3230Time(true, false);
			}
			had3230Timestamp = has3230Timestamp;
			hadNtpTime = hasNtpTime;

		}

		printf("CHwControl::MonitorThread exiting\n");
	}
	catch(...)
	{
		CFailure::OnFail(std::current_exception());
	}

	m_watchdog->UnregisterPingSource(monitor);

	return;
}


/////////////////////////////////////////////////////////////////////
//
// Pan timeout thread function
//
void CHwControl::PanTimeoutThread(void)
{
	try
	{
		// Poll until shutdown
		auto Polling = [this]
		{
			std::unique_lock<std::mutex> lock(m_shutdownMutex);
			return !m_shutdownCond.wait_for(lock, PAN_TIMEOUT_POLLING_INTERVAL, [this] { return m_shutdown; });
		};

		while(Polling())
		{
			size_t numRemoved = 0;
			DATE now = Utility::CurrentTimeAsDATE();
			while (m_sched.RemoveItem([&](CTask::Task& item)
			{
				bool match = item->m_msgHdr.msgType == SEquipCtrlMsg::PAN_DISP_CTRL && item->m_completionTime > 0 && item->m_completionTime <= now;
				if (match)
				{
					TRACE("                              Removing task %lu\n", item->m_taskIdKey.key);
				}
				return match;
			} ))
			{
				++numRemoved;
			}
			if (numRemoved > 0)
			{
				try
				{
					CAudio& audio = dynamic_cast<CAudio&>(*this);
					audio.Enable(CanEnableAudio());
				}
				catch(const std::bad_cast&)
				{
					TRACE("PanTimeoutThread bad_cast\n");
				}
			}
		}

		printf("CHwControl::PanTimeoutThread exiting\n");
	}
	catch(...)
	{
		CFailure::OnFail(std::current_exception());
	}

	return;

}

//////////////////////////////////////////////////////////////////////
//
// Replace or add a task to my queue
//
void CHwControl::ReplaceTask(const CTask::Task& task, unsigned int timeout, bool isRunnable)
{
	if(task->GetNumSteps() > 0)
	{
		auto stratum = static_cast<int>(task->GetTaskGroup());
		if (m_sched.Count(stratum) >= m_sched.Depth())
		{
			TRACE("TaskSched: %u %u %u\n", m_sched.Count(stratum), m_sched.Depth(), stratum);
			throw TaskSched::TIMEDOUT;
		}
		m_sched.ReplaceItem([&](CTask::Task& item){ return item->m_taskIdKey.key == task->m_taskIdKey.key; },
			task, stratum, isRunnable, timeout);
		try
		{
			CAudio& audio = dynamic_cast<CAudio&>(*this);
			audio.Enable(CanEnableAudio());
		}
		catch(const std::bad_cast&)
		{
			TRACE("ReplaceTask bad_cast\n");
		}
	}

	return;
}

//static int nloop = 0;
//////////////////////////////////////////////////////////////////////
//
// Request or flush the collected data from the slave
//
unsigned long CHwControl::RequestCollectedData(bool collect, unsigned long key, std::vector<size_t>& sentIndex)
{
	SDfCtrlMsg msg;
	msg.hdr.msgType = SDfCtrlMsg::INTERNAL_DF_CTRL;
	msg.hdr.msgSubType = SDfCtrlMsg::SLAVE_GET_SAMPLES;
	msg.hdr.sourceAddr = 0;
	msg.hdr.destAddr = 0;
	msg.hdr.bodySize = sizeof(SDfCtrlMsg::SSlaveGetSamples);

	msg.body.slaveGetSamples.action = (collect ? SDfCtrlMsg::SSlaveGetSamples::COLLECT : SDfCtrlMsg::SSlaveGetSamples::FLUSH);
	msg.body.slaveGetSamples.key = key;

#ifdef CSMS_DEBUG
	if(s_traceSlave)
	{
		TRACE("CHwControl::RequestCollectedData SLAVE_GET_SAMPLES, collect %d, key %lu\n",
				(collect ? 1 : 0), key);
	}
#endif
	unsigned long tag = 0;
	try
	{
		CWeakSingleton<CProcessorNode> procNode;
		//auto now = Utility::CurrentTimeAsTimestamp(); // debug only
//	#if CSMS_TIMING == 1
//	auto startTime = std::chrono::steady_clock::now();
//	#endif
		tag = procNode->SendToAllNodes(msg, sentIndex, true);		// Send tagged message
		//auto timeDiff = (Utility::CurrentTimeAsTimestamp() - now).NanoSeconds<long>(); // debug only
		//printf("%s  master sent SLAVE_GET_SAMPLES %ld\n", Utility::CurrentTimeAsTimestamp().Format(true).c_str(), timeDiff);//debug only
//#if CSMS_TIMING == 1
//	auto endTime = std::chrono::steady_clock::now();
//	auto diffTime = endTime - startTime;
//	auto micro = std::chrono::duration<double, std::micro>(diffTime).count();
//	if ((nloop++ % 10) == 0)
//		printf("master to slave took %f microseconds\n", micro);
//#endif
	}
	catch(CWeakSingleton<CProcessorNode>::NoInstance&)
	{
		TRACE("CHwControl::RequestCollectedData Unable to access processorNode\n");
	}

	return tag;
}


//////////////////////////////////////////////////////////////////////
//
// Reset the 3230 time from ntp time
//
bool CHwControl::Reset3230Time(bool logError, bool startup)
{
	bool hasNtpTime = m_navigation->IsNtpSyncd();
	unsigned long vals[5];
	bool errs[5];
	bool hasTimestamp = m_digitizer->CheckTimestamp(logError, startup, vals, errs);
	if (hasTimestamp && hasNtpTime)
	{
		auto stamp = Utility::CurrentTimeAsTimestamp();
		auto fraction = static_cast<unsigned long>(stamp.GetNTP() & 0xffffffff);
		size_t iSetSecs = 0;
		while (fraction < 0x60000000 || fraction > 0xa0000000)
		{
			usleep(10000);
			stamp = Utility::CurrentTimeAsTimestamp();
			fraction = static_cast<unsigned long>(stamp.GetNTP() & 0xffffffff);
			++iSetSecs;
		}
		m_digitizer->SetSeconds(stamp);

		Units::Timestamp debugStamp;
		m_digitizer->GetCurrentTime(debugStamp);
		TRACE("CHwControl: Set seconds[%u] to %s, get %s\n", iSetSecs, stamp.Format(true).c_str(), debugStamp.Format(true).c_str());
	}
	else if (logError)
	{
		CLog::Log(CLog::ERRORS, "CHwControl: CheckTimestamp error: not enabling 3230 timestamp");
	}
	m_digitizer->ResetDrift();

	return hasTimestamp;
}


/////////////////////////////////////////////////////////////////////
//
// Reserve space for data from digitizer
//
unsigned long CHwControl::ReserveBufferSpace(CTask::SBlockState& blockState, unsigned int numAnts, unsigned long ddrCount, bool spurCal) const
{
#ifdef CSMS_DEBUG
	auto bufTrace = CSBufferBase::s_traceAvd;
#endif
	unsigned long spaceNeeded = numAnts * ddrCount;
	if (spurCal && m_mutableConfig.miscConfig.enhancedDynRng)
	{
		spaceNeeded += ddrCount;
		blockState.withTerm = true;
#ifdef CSMS_DEBUG
		TRACE("CHwControl::ReserveBufferSpace (withTerm): freqLimits (%fMHz: %fMHz): numAnts %u, ddrCount %lu, space = %lu; radioFreq %fMHz, inverted %d,pbCalBand %u,pbCalDirect %d,sampleCount %lu,switchTemp %f\n",
				blockState.freqLimits.first.Hz<float>()/1000000, blockState.freqLimits.second.Hz<float>()/1000000,
				numAnts, ddrCount, spaceNeeded,
				blockState.radioFreq.Hz<float>()/1000000, (blockState.inverted ? 1 :0), blockState.pbCalBand,
				(blockState.pbCalDirect ? 1 :0), blockState.sampleCount, blockState.switchTemp);
		CSBufferBase::s_traceAvd = true;
		//Trace for terminated case.
		//C3230::s_trace3220 = true;
#endif
	}
	else
	{
		blockState.withTerm = false;
	}
	if (!m_digitizer->m_sdma.WaitForBufferSpace(spaceNeeded, blockState.sampleCount) ||
		!m_digitizer->m_sdma.MarkSamplesInUse(blockState.sampleCount, spaceNeeded))
	{
		// sampleSize > dma buffer - task cannot be done
		throw std::runtime_error("sampleSize too large");	// TODO: handle this better
	}

#ifdef CSMS_DEBUG
	if(blockState.withTerm)
	{
		//Restore the buffer trace flag.
		CSBufferBase::s_traceAvd = bufTrace;
	}
#endif

	return spaceNeeded;
}


//////////////////////////////////////////////////////////////////////
//
// Set the antenna switches
//
void CHwControl::SetAntSwitches(_In_ const CTask::Task& task, _In_ Units::Frequency /*freq*/)
{
	if (!m_switch->C51432017::IsPresent())
		return;

	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	auto pol = taskParams.pol;
	if (pol == CConfig::EPolarization::BOTH) pol = task->GetVHCurrentPolarization();

	SEquipCtrlMsg::EAnt eAnt = CTask::GetEAntValue(task->m_ant, pol);
	m_AntGetPan = eAnt;

	if( eAnt != SEquipCtrlMsg::ANT2 && eAnt != SEquipCtrlMsg::ANT3 && eAnt != SEquipCtrlMsg::INVALID_ANT &&
		eAnt != SEquipCtrlMsg::SMPL_RF2 && eAnt != SEquipCtrlMsg::SMPL_RF3 && !CConfig::IsTermAnt(eAnt))
	{
		// Antenna is ANT1 or ANT1H or [DF_ANT_1V,DF_ANT_9V] or [DF_ANT_1H,DF_ANT_9H]
		if (taskParams.hf)
		{
			// HF
			C72362001::ESource hfRefRf = C72362001::TERM;
			C72362001::ESource hfSampleRf = C72362001::TERM;

			if (!task->UsingDf() )
			{
				if (eAnt == SEquipCtrlMsg::ANT1 )
				{
					hfRefRf = C72362001::MONITOR;
				}
				else
				{
					hfRefRf = C72362001::MONITOR; // For demod
					hfSampleRf = C72362001::Source(eAnt);
				}
			}
			else
			{
				hfRefRf = (m_mutableConfig.hfConfig.antCable.antType == CAntenna::EAntennaType::HF_632 ? C72362001::REF : C72362001::MONITOR);
			}
			m_switch->C7234201102::SetSwitch(0, C7234201102::RECEIVE, C7234201102::TERM, C7234201102::TERM);
			m_switch->C72362001::SetSwitch(task->GetTuneFreq(), C72362001::RECEIVE, hfRefRf, hfSampleRf);
		}
		else
		{
			//VUSHF
			C7234201102::ESource ushfRefRf = C7234201102::TERM;
			C7234201102::ESource ushfSampleRf = C7234201102::TERM;

			if (!task->UsingDf() )
			{
				if (eAnt == SEquipCtrlMsg::ANT1 )
				{
					ushfRefRf = C7234201102::REF;
				}
				else if (eAnt == SEquipCtrlMsg::ANT1H )
				{
					ushfRefRf = C7234201102::REF_HORIZON;
				}
				else
				{
					if (pol == CConfig::EPolarization::HORIZ)
					{
						ushfRefRf = C7234201102::REF_HORIZON; // For demod
					}
					else
					{
						ushfRefRf = C7234201102::REF; // For demod
					}
					ushfSampleRf = m_switch->C7234201102::Source(eAnt);
				}
			}
			else
			{
				//Used the proper reference Antenna.
				if (pol == CConfig::EPolarization::HORIZ)
				{
					ushfRefRf = C7234201102::REF_HORIZON;
				}
				else
				{
					ushfRefRf = C7234201102::REF;
				}
			}
			m_switch->C7234201102::SetSwitch(task->GetTuneFreq(), C7234201102::RECEIVE, ushfRefRf, ushfSampleRf);
			m_switch->C72362001::SetSwitch(0, C72362001::RECEIVE, C72362001::TERM, C72362001::TERM);
		}
	}
	else
	{
		// Antenna is ANT2 or ANT3 or INVALID_ANT or SMPL_RF2 or SMPL_RF3 or IsTermAnt
		// Terminate all switches
		m_switch->C7234201102::SetSwitch(0, C7234201102::RECEIVE, C7234201102::TERM, C7234201102::TERM);
		m_switch->C72362001::SetSwitch(0, C72362001::RECEIVE, C72362001::TERM, C72362001::TERM);
	}
	return;
}


/////////////////////////////////////////////////////////////////////
//
// Setup all the hardware. Return true if two different preselectors are needed
//
CRadioEquip::EBandSelect CHwControl::SetHardware(const CTask::Task& task, CRadioEquip::EBandSelect bandSelect, bool isDf,
	unsigned long* tag, std::vector<size_t>* sentIndex)
{
	const CConfig::STaskParams& taskParams = task->GetTaskParams();
	Units::Frequency rxBw;
	Units::Frequency procBw;
	if (taskParams.stopFreq == taskParams.startFreq)		// pan or measurement
	{
		rxBw = std::min(taskParams.rxHwBw, taskParams.bw);
		procBw = Units::Frequency();
	}
	else	// scan task
	{
		rxBw = std::min(taskParams.rxHwBw, taskParams.stopFreq - taskParams.startFreq);
		procBw = task->GetProcBw();
	}

	Units::Frequency rfFreq = task->GetTuneFreq();
	SEquipCtrlMsg::EAnt eAnt = task->m_ant;
	bool shfExtUsed = m_config->IsShfExtUsed(eAnt, rfFreq);
	Units::Frequency rxFreq = (shfExtUsed ? m_config->GetShfExtIfFreq() : rfFreq);
	Units::Frequency finalIfFreq;
	bool invertRadio;
	CTask::SBlockState& blockState = task->BlockState();

#ifdef CSMS_DEBUG
	bool traceOut = false;
	bool traceBlock = false;
	static const size_t ARRAY_SIZE = 2;
	static Units::Frequency l_prevFreq[ARRAY_SIZE] = { Units::Frequency(0), Units::Frequency(0)};
	static SEquipCtrlMsg::EAnt l_prevAnt[ARRAY_SIZE] = { SEquipCtrlMsg::INVALID_ANT, SEquipCtrlMsg::INVALID_ANT};

	if(taskParams.numBlocks <= ARRAY_SIZE)
	{
		size_t idx = 0;

		for(; idx < ARRAY_SIZE; ++idx)
		{
			if(l_prevFreq[idx] == rfFreq)
			{
				break;
			}
			else if(l_prevFreq[idx] > rfFreq)
			{
				if(idx > 0)
				{
					--idx;
				}
				traceOut = true;
				C2630::s_printTuneInfo = true;
			    	l_prevFreq[idx] = rfFreq;
				l_prevAnt[idx] = eAnt;
				break;
			}
		}

		if(idx == ARRAY_SIZE)
		{
		    traceOut = true;
		    C2630::s_printTuneInfo = true;
		    --idx;
		    l_prevFreq[idx] = rfFreq;
		    l_prevAnt[idx] = eAnt;
		}

		if(!traceOut && l_prevAnt[idx] != eAnt )
		{
			l_prevAnt[idx] = eAnt;
			traceOut = true;
			C2630::s_printTuneInfo = true;
		}

		if(traceOut)
		{
			TRACE("CHwControl::SetHardware blockState: freqLimits (%fMHz: %fMHz): radioFreq %fMHz, inverted %d,pbCalBand %u,pbCalDirect %d,sampleCount %lu,switchTemp %f,withTerm %d, isDf %d, tag %d, sentIndex %d\n",
			      blockState.freqLimits.first.Hz<float>()/1000000, blockState.freqLimits.second.Hz<float>()/1000000,
			      blockState.radioFreq.Hz<float>()/1000000, (blockState.inverted ? 1 :0), blockState.pbCalBand,
			      (blockState.pbCalDirect ? 1 :0), blockState.sampleCount, blockState.switchTemp,
			      (blockState.withTerm ? 1 :0), (isDf ? 1 : 0), (tag ? 1 : 0), (sentIndex ? 1 : 0));
		}

		if(blockState.radioFreq == 0)
		{
			traceBlock = true;
		}

	}
#endif
	if (!m_radioEquip->Tune(rxFreq, rxBw, procBw, taskParams.hf, bandSelect, finalIfFreq, invertRadio, blockState.freqLimits,
		blockState.radioFreq, blockState.pbCalBand, blockState.pbCalDirect, eAnt))
	{
		TRACE("CHwControl::SetHardware RadioEquip tune failed\n");
		return CRadioEquip::EBandSelect::BAND1;
	}

	if(shfExtUsed)
	{
		//SHF Extension is used, do not need to set antenna switch
		//However, invertRadio must be changed accordingly.
		m_radioEquip->TuneShfFreq(rfFreq);
		invertRadio = (m_radioEquip->IsShfSpectrumInverted(rfFreq) ? !invertRadio : invertRadio);

#ifdef CSMS_DEBUG
		if(traceOut)
		{
			TRACE("CHwControl::SetHardware(shfExtUsed) eAnt = %u, rfFreq = %lf MHz, rxFreq = %f MHz, finalIfFreq = %f MHz\n",
			      eAnt, rfFreq.Hz<double>()/ 1000000, rxFreq.Hz<float>()/ 1000000,
			      finalIfFreq.Hz<float>()/ 1000000);
		}
#endif
	}
	else if (!m_config->IsProcWithAntSelectionSwitch())
	{
		SetAntSwitches(task, rfFreq);
#ifdef CSMS_DEBUG
		if(traceOut)
		{
			TRACE("CHwControl::SetHardware(SetAntSwitches) eAnt = %u, rfFreq = %f MHz, rxFreq = %f MHz, finalIfFreq = %f MHz\n",
			      eAnt, rfFreq.Hz<float>()/ 1000000, rxFreq.Hz<float>()/ 1000000,
			      finalIfFreq.Hz<float>()/ 1000000);
		}
#endif
	}
#ifdef CSMS_DEBUG
	else
	{
		if(traceOut)
		{
			TRACE("CHwControl::SetHardware eAnt = %u, rfFreq = %f MHz, rxFreq = %f MHz, finalIfFreq = %f MHz, numBlocks = %u, freqLimits (%fMHz: %fMHz)\n",
			      eAnt, rfFreq.Hz<float>()/ 1000000, rxFreq.Hz<float>()/ 1000000,
			      finalIfFreq.Hz<float>()/ 1000000, taskParams.numBlocks,
				  blockState.freqLimits.first.Hz<float>()/1000000, blockState.freqLimits.second.Hz<float>()/1000000);
		}
	}
#endif

	const CConfig::SProcParams& procParams = task->GetProcParams();
	m_digitizer->SetDecimation(procParams.decimations, 0);

//	There is an additional inversion in the digitizer:
//	   direct path due to nco < f/2, not-direct due to second Nyquist zone and nco > f/2
	blockState.inverted = m_digitizer->AdjustInversion(blockState.pbCalDirect, invertRadio);

	Units::Frequency procFreq = (blockState.inverted ? finalIfFreq - task->GetProcOffset() : finalIfFreq + task->GetProcOffset());

	if(shfExtUsed)
	{
		//Wait for SHF to tuned.
		m_radioEquip->Wait4Shf();
	}
	else if (m_config->IsProcWithAntSelectionSwitch() && !taskParams.hf &&
			(eAnt == SEquipCtrlMsg::EAnt::ANT1 || eAnt == SEquipCtrlMsg::EAnt::ANT1H))
	{
		SEquipCtrlMsg::EAnt ant;

		if(!m_antSwitch->ReadSwitchAntVal(ant))
		{
			CLog::Log(CLog::ERRORS, "CHwControl::SetHardware: Unable to set RF Switch to %d\n", int(ant));
			ASSERT(false);
		}
		else
		{
			//It is possible the switch is no longer connected to the antenna
			// specified for this task.
			auto cmdAnt = task->GetCmdAnt();
			if(cmdAnt != SEquipCtrlMsg::EAnt::ANT1 &&
					cmdAnt != SEquipCtrlMsg::EAnt::ANT1H && cmdAnt != ant)
			{
				TRACE("CHwControl::SetHardware: RF Switch is connected to %d, now change to %d\n",
						int(ant), int(cmdAnt));

				if(m_antSwitch->SetAnt(cmdAnt))
				{
					if(!m_antSwitch->ReadSwitchAntVal(ant))
					{
						CLog::Log(CLog::ERRORS, "CHwControl::SetHardware: Unable to read RF Switch after setting to %d\n", int(cmdAnt));
						ASSERT(false);
					}
				}
				else
				{
					CLog::Log(CLog::ERRORS, "CHwControl::SetHardware: Unable to set RF Switch to %d\n", int(cmdAnt));
					ASSERT(false);
				}

			}
		}
	}

	// TODO: For now, turn off the shift and do it in the FFT processing. This way the iq data is correct.
	m_digitizer->Tune(procFreq, blockState.inverted, blockState.pbCalDirect, false);

	Units::FreqPair radioLimits;
	m_radioEquip->GetFreqLimits(radioLimits);
	dynamic_cast<CAudio&>(*this).TuneDemods(rxFreq, finalIfFreq, invertRadio, blockState.inverted, radioLimits);

	// TODO: REMOVE THIS HACK WHEN IT IS FIXED IN THE DIGITIZER IN A LATER BUILD.
	// Set DDC 0 NCO again - it can get modified by the TuneDemods() call.
	m_digitizer->SetDDCNCO(0, 0);
	///////////////////////////////
	if (isDf && tag && sentIndex)
	{
		///@note: Choose to use rfFreq instead of rxFreq because DF
		///       does not have Analyzer to translate to lower frequency.
		*tag = TuneDfSlaves(rfFreq, rxBw, procBw, taskParams.hf, bandSelect, eAnt, procParams.decimations, task->GetProcOffset(),
			*sentIndex);
#ifdef CSMS_DEBUG
		if(s_tracePan && traceBlock)
		{
			TRACE("CHwControl::SetHardware tag %lu, sentIndex.empty %d\n",
					*tag, (sentIndex->empty() ? 1 : 0));
		}
#endif
	}

	CRadioEquip::EBandSelect nextBand = CRadioEquip::EBandSelect::BAND1;
	if (procBw != Units::Frequency())
	{
		///@note: rxFreq is used because freqLimits values are based on it.
		///	  stopFreq can't be used for checking for SHF Extension.
		if (blockState.freqLimits.second >= rxFreq + procBw / 2 || blockState.freqLimits.second >= taskParams.stopFreq)
			nextBand = CRadioEquip::EBandSelect::BAND1;
		else if (bandSelect == CRadioEquip::EBandSelect::BAND1)
			nextBand = CRadioEquip::EBandSelect::BAND2;
		else if (bandSelect == CRadioEquip::EBandSelect::BAND2)
			nextBand = CRadioEquip::EBandSelect::BAND3;
		else
		{
			assert(false);
		}

		///@note: Only compare with startFreq if SHF Extension not used.
		if (!shfExtUsed && (blockState.freqLimits.first < taskParams.startFreq))
			blockState.freqLimits.first = taskParams.startFreq;
		if (blockState.freqLimits.second > taskParams.stopFreq)
			blockState.freqLimits.second = taskParams.stopFreq;
	}

#ifdef CSMS_DEBUG
	if(traceBlock)
	{
		TRACE("CHwControl::SetHardware blockState2: freqLimits (%fMHz: %fMHz): radioFreq %fMHz, inverted %d,pbCalBand %u,pbCalDirect %d,sampleCount %lu,switchTemp %f,withTerm %d\n",
		      blockState.freqLimits.first.Hz<float>()/1000000, blockState.freqLimits.second.Hz<float>()/1000000,
		      blockState.radioFreq.Hz<float>()/1000000, (blockState.inverted ? 1 :0), blockState.pbCalBand,
		      (blockState.pbCalDirect ? 1 :0), blockState.sampleCount, blockState.switchTemp,
		      (blockState.withTerm ? 1 :0));
	}

	if(traceOut)
	{
		TRACE("CHwControl::SetHardware tune(%d.%d) nco: %lf f1f2:%f-%f stop:%f next:%d inverted:%d pbCalDirect:%d\n",
	    		task->GetBlock(), int(bandSelect), procFreq.Hz<double>(),
	    		blockState.freqLimits.first.Hz<double>(), blockState.freqLimits.second.Hz<double>(),
	    		taskParams.stopFreq.Hz<double>(), int(nextBand), blockState.inverted, blockState.pbCalDirect);
		TRACE("CHwControl::SetHardware zifCICDecimation = %lu zifFIRDecimation =%lu upResample =%lu downResample=%lu ddcCICDecimation=%lu ddcFIRDecimation=%lu, samplecount =%lu\n",
		      procParams.decimations.zifCICDecimation, procParams.decimations.zifFIRDecimation,
		      procParams.decimations.upResample, procParams.decimations.downResample,
		      procParams.decimations.ddcCICDecimation, procParams.decimations.ddcFIRDecimation, blockState.sampleCount);
	}
#endif
//	printf("tune(%d.%d) %f: nco: %f f1f2:%f-%f stop:%f next:%d\n",
//		task->GetBlock(), bandSelect, rfFreq.Hz<double>() * 1.e-6, procFreq.Hz<double>(),
//		blockState.freqLimits.first.Hz<double>(), blockState.freqLimits.second.Hz<double>(),
//		taskParams.stopFreq.Hz<double>(), nextBand);

	return nextBand;
}


/////////////////////////////////////////////////////////////////////
//
// Change the runnable status of a scheduled task
//
void CHwControl::SetRunnable(const SEquipCtrlMsg::STaskIdKey& taskIdKey, bool isRunnable, bool doAudio)
{
	if (m_sched.IsClosed() && isRunnable)
	{
		TRACE("CHwControl::SetRunnable when closed - ignoring\n");
		return;
	}
	m_sched.SetRunnable([&](CTask::Task& item){ return item->m_taskIdKey.key == taskIdKey.key; }, isRunnable);
	if (!doAudio)
		return;

	try
	{
		CAudio& audio = dynamic_cast<CAudio&>(*this);
		audio.Enable(CanEnableAudio());
	}
	catch(const std::bad_cast&)
	{
		TRACE("SetRunnable bad_cast\n");
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send message to slaves to setup for collecting samples for df (fft and fftiq)
//
unsigned long CHwControl::SetupCollectSlaves(unsigned long count, unsigned long adcCount, unsigned long delay,
	unsigned long numDfBins, SDfCtrlMsg::SlaveDataTypes dataTypes, bool termRadio, unsigned char slaveAtten, CRadioEquip::EGainMode gainMode, const Units::Frequency& midFrequency,
	const std::vector<unsigned long>& pattern, std::vector<size_t>& sentIndex, size_t numAnts, bool enableAudio)
{
	if (pattern.size() > SDfCtrlMsg::SSlaveSetupCollect::MAX_ANTS)
		return 0;

	SDfCtrlMsg msg;
	msg.hdr.msgType = SDfCtrlMsg::INTERNAL_DF_CTRL;
	msg.hdr.msgSubType = SDfCtrlMsg::SLAVE_SETUP_COLLECT;
	msg.hdr.sourceAddr = 0;
	msg.hdr.destAddr = 0;
	msg.hdr.bodySize = sizeof(SDfCtrlMsg::SSlaveSetupCollect);

	msg.body.slaveSetupCollect.midFrequency = midFrequency.GetRaw();
	msg.body.slaveSetupCollect.count = count;
	msg.body.slaveSetupCollect.adcCount = adcCount;
	msg.body.slaveSetupCollect.delay = delay;
	msg.body.slaveSetupCollect.fftDelay = C3230::CalcFftDelay(count, true, true);
	msg.body.slaveSetupCollect.numDfBins = numDfBins;
	msg.body.slaveSetupCollect.dataTypes = dataTypes; // SDfCtrlMsg::SlaveDataTypes(SDfCtrlMsg::SlaveDataTypes::PSD | SDfCtrlMsg::SlaveDataTypes::FFT);
	msg.body.slaveSetupCollect.termRadio = termRadio;
	msg.body.slaveSetupCollect.atten = slaveAtten;
	msg.body.slaveSetupCollect.enableAudio = enableAudio;
	if (gainMode == CRadioEquip::EGainMode::NORMAL) msg.body.slaveSetupCollect.gainMode = SDfCtrlMsg::NORMAL;
	else if (gainMode == CRadioEquip::EGainMode::RURAL) msg.body.slaveSetupCollect.gainMode = SDfCtrlMsg::RURAL;
	else if (gainMode == CRadioEquip::EGainMode::URBAN) msg.body.slaveSetupCollect.gainMode = SDfCtrlMsg::URBAN;
	else if (gainMode == CRadioEquip::EGainMode::CONGESTED) msg.body.slaveSetupCollect.gainMode = SDfCtrlMsg::CONGESTED;
	else msg.body.slaveSetupCollect.gainMode = SDfCtrlMsg::NORMAL;

	for (size_t i = 0; i < pattern.size(); ++i)
	{
		msg.body.slaveSetupCollect.pattern[i] = pattern[i];
	}

	msg.body.slaveSetupCollect.numAnts = numAnts;
//	if (termRadio)
//	{
//		printf("SetupCollectSlaves %f %lu %lu %lu %lu %u %d %u %u %u %lu\n",
//			midFrequency.Hz<double>(), count, adcCount, delay, numDfBins, dataTypes, termRadio,
//			slaveAtten, gainMode, pattern.size(), pattern.empty() ? 0xffff : pattern[0]);
//	}
	unsigned long tag = 0;
	try
	{
		CWeakSingleton<CProcessorNode> procNode;
		tag = procNode->SendToAllNodes(msg, sentIndex, true);		// Send tagged message
	}
	catch(CWeakSingleton<CProcessorNode>::NoInstance&)
	{
		TRACE("CHwControl::SetupCollectDfSlaves Unable to access processorNode\n");
	}

	return tag;
}


//////////////////////////////////////////////////////////////////////
//
// Stop threads
//
void CHwControl::Stop(void)
{
	m_sched.Close();
	if (m_controlThread.joinable())
	{
		m_controlThread.join();
	}

	{
		// Do this before notifying the other threads
		std::lock_guard<std::mutex> lock(m_shutdownMutex);
		m_shutdown = true;
	}

	m_cAgc->Wakeup();

	if (m_agcTimeoutThread.joinable())
	{
		m_agcTimeoutThread.join();
	}

	m_shutdownCond.notify_all();
	if (m_panTimeoutThread.joinable())
	{
		m_panTimeoutThread.join();
	}

	if (m_monitorThread.joinable())
	{
		m_monitorThread.join();
	}

	if (m_config->IsDDRSystem())
	{
		m_exitFaultLedThread = true;

		if (m_FaultLedThread.joinable())
		{
			m_FaultLedThread.join();
		}
	}

	return;
}


//////////////////////////////////////////////////////////////////////
//
// Send message to slaves to tune the radio and digitizer
//
unsigned long CHwControl::TuneDfSlaves(Units::Frequency rfFreq, Units::Frequency rxHwBw, Units::Frequency procBw, bool hf,
	CRadioEquip::EBandSelect band, SEquipCtrlMsg::EAnt ant, const C3230::SDecimation& decimations, Units::Frequency procOffset, std::vector<size_t>& sentIndex)
{
	SDfCtrlMsg msg;
	msg.hdr.msgType = SDfCtrlMsg::INTERNAL_DF_CTRL;
	msg.hdr.msgSubType = SDfCtrlMsg::SLAVE_TUNE;
	msg.hdr.sourceAddr = 0;
	msg.hdr.destAddr = 0;
	msg.hdr.bodySize = sizeof(SDfCtrlMsg::SSlaveTune);

	msg.body.slaveTune.rfFreq = rfFreq.GetRaw();
	msg.body.slaveTune.rxBw = rxHwBw.GetRaw();
	msg.body.slaveTune.procBw = procBw.GetRaw();
	msg.body.slaveTune.hf = hf;
	msg.body.slaveTune.bandSelect = static_cast<unsigned char>(band);
	msg.body.slaveTune.ant = ant;
	msg.body.slaveTune.tuneDigitizer = true;
	msg.body.slaveTune.decimations.zifCICDecimation = decimations.zifCICDecimation;
	msg.body.slaveTune.decimations.zifFIRDecimation = decimations.zifFIRDecimation;
	msg.body.slaveTune.decimations.upResample = decimations.upResample;
	msg.body.slaveTune.decimations.downResample = decimations.downResample;
	msg.body.slaveTune.decimations.ddcCICDecimation = decimations.ddcCICDecimation;
	msg.body.slaveTune.decimations.ddcFIRDecimation = decimations.ddcFIRDecimation;
	msg.body.slaveTune.procOffset = procOffset.GetRaw();

	unsigned long tag = 0;
	try
	{
		CWeakSingleton<CProcessorNode> procNode;
		tag = procNode->SendToAllNodes(msg, sentIndex, true);		// Send tagged message
	}
	catch(CWeakSingleton<CProcessorNode>::NoInstance&)
	{
		TRACE("CHwControl::TuneDfSlaves Unable to access processorNode\n");
	}
	return tag;
}


//////////////////////////////////////////////////////////////////////
//
// Wait for slaves to respond to request
//
bool CHwControl::WaitForSlaves(const std::vector<size_t>& sentIndex, unsigned long tag)
{
	std::vector<CProcessorNode::SDfCtrlMsgPtr> responses;
	return WaitForSlaves(sentIndex, tag, responses);
}

bool CHwControl::WaitForSlaves(const std::vector<size_t>& sentIndex, unsigned long tag, std::vector<CProcessorNode::SDfCtrlMsgPtr>& responses)
{
//	printf("CHwControl::WaitForSlaves waiting for tag = %lu %u\n", tag, sentIndex.size());
	bool failed = false;
	responses.clear();
	try
	{
		CWeakSingleton<CProcessorNode> procNode;
		if (!procNode->WaitForAllResponses(sentIndex, tag, responses))
		{
			failed = true;
		}
	}
	catch(CWeakSingleton<CProcessorNode>::NoInstance&)
	{
		TRACE("CHwControl::WaitForSlaves Unable to access processorNode\n");
	}
	// Check that all responses are successes, otherwise fail
	for (size_t i = 0; i < responses.size(); ++i)
	{
		auto& r = responses[i];
		if (!r)
		{
			failed = true;
		}
		else
		{
			switch(r->hdr.msgType)
			{
			case SDfCtrlMsg::SMS_ERROR_REPORT:
				failed = true;
				TRACE("CHwControl::WaitForSlaves received SMS_ERROR_REPORT\n");
				break;

			case SDfCtrlMsg::INTERNAL_DF_CTRL:
				switch(r->hdr.msgSubType)
				{
				case SDfCtrlMsg::SLAVE_TUNE_RESP:
					if (r->body.slaveTuneResp.status == SDfCtrlMsg::FAILURE)
					{
						failed = true;
						TRACE("CHwControl::WaitForSlaves received SLAVE_TUNE_RESP with status FAILURE\n");
					}
					break;

				case SDfCtrlMsg::SLAVE_SETUP_COLLECT_READY:
					if (r->body.slaveSetupCollectReady.status == SDfCtrlMsg::FAILURE)
					{
						failed = true;
						TRACE("CHwControl::WaitForSlaves received SLAVE_SETUP_COLLECT_READY with status FAILURE\n");
					}
					break;

				case SDfCtrlMsg::SLAVE_SETUP_COLLECT_RESP:
					if (r->body.slaveSetupCollectResp.status == SDfCtrlMsg::FAILURE)
					{
						failed = true;
						TRACE("CHwControl::WaitForSlaves received SLAVE_SETUP_COLLECT_RESP with status FAILURE\n");
					}
					break;

				case SDfCtrlMsg::DO_INITIAL_AGC_RESP:
					if (r->body.doInitialAgcResp.status == SDfCtrlMsg::FAILURE)
					{
						failed = true;
						TRACE("CHwControl::WaitForSlaves received DO_INITIAL_AGC_RESP with status FAILURE\n");
					}
					break;

			default:
					failed = true;
					TRACE("CHwControl::WaitForSlaves received unknown INTERNAL_DF_CTRL msgSubType %lu\n", r->hdr.msgSubType);
					break;
				}
				break;

			default:
				failed = true;
				TRACE("CHwControl::WaitForSlaves received unknown msgType %hu\n", r->hdr.msgType);
				break;
			}
		}
	}
	return !failed;
}

