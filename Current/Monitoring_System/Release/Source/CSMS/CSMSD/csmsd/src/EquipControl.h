/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2006-2016 TCI International, Inc. All rights reserved         *
**************************************************************************/

#pragma once
#include "Audio.h"
#include "Config.h"
#include "CsmsLicense.h"
#include "Digitizer.h"
#include "EquipCtrlMsg.h"
#include "HwControl.h"
#include "Ionogram.h"
#include "Log.h"
#include "Navigation.h"
#include "NetConnection.h"
#include "Processing.h"
#include "ProcessorNode.h"
#include "RfSelSwitch.h"
#include "VCPMsg.h"
#include "Singleton.h"
#include "Task.h"
#include "VCPCtrl.h"

class CEquipCtrlNet;

class CEquipControl :
	// Inheritance
	public CAudio,
	public CHwControl,
	public CProcessing,
	public CVCPCtrl
{
	// Friends
	friend class CSingleton<CEquipControl>;

public:
	// Functions
	void GetAudioParams(std::vector<CAudio::SAudioParam>& params);
	static unsigned char GetResponseVersion(_In_ const CNetConnection<void>* client, _In_ const SEquipCtrlMsg::SHdr& hdr);
	void OnAntCtrl(_In_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source = nullptr);
	void OnAvdCtrl(_In_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source = nullptr);
	void OnBistCtrl(_In_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source = nullptr);
	void OnClientClose(_In_ const CNetConnection<void>* client);
	void OnDemodCtrl(_In_ const SEquipCtrlMsg* msg, _In_ CNetConnection<void>* source);
	void OnDfCtrl(_In_ const SEquipCtrlMsg* msg, _In_ CNetConnection<void>* source);
	void OnEquipmentCtrl(_In_ const SEquipCtrlMsg* msg, _In_ CNetConnection<void>* source = nullptr);
	void OnMetricsCtrl(_In_ const SEquipCtrlMsg* msg, /*CTask::EPriority priority,*/ _In_opt_ CNetConnection<void>* source = nullptr);
	void OnOccupancyCtrl(_In_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source = nullptr);
	void OnOccupancyDfCtrl(_In_ const SEquipCtrlMsg* msg, _In_opt_ CNetConnection<void>* source = nullptr);
	void OnPanDispCtrl(_In_ const SEquipCtrlMsg* msg, _In_ CNetConnection<void>* source);
	void OnStatusCtrl(_In_ const SEquipCtrlMsg* msg, _In_ CNetConnection<void>* source);
	void OnVCPCtrl(_In_ const SVCPMsg* msg, _In_ CEquipCtrlNet* source);

	static void Send(_In_opt_ CNetConnection<void>* client, _In_ const SEquipCtrlMsg& msg,
		_In_opt_ LPCTSTR log = nullptr);
	static void Send(_In_ CNetConnection<void>* client, _In_ const SVCPMsg& msg,
		_In_opt_ LPCTSTR log = nullptr);
	static void SendPan(_In_opt_ CNetConnection<void>* client, _In_ const SEquipCtrlMsg& msg,
		_In_opt_ LPCTSTR log = nullptr);
	CEquipCtrlNet* VCPControlClient(void) const { return m_vcpControlClient; }

private:
	// Constants
	static const unsigned int PAN_IDLE_TIMEOUT = 2000; // ms

	// Types
	struct SPanParam
	{
		SPanParam(Units::Frequency bw, unsigned long agcTime, SEquipCtrlMsg::EAnt ant) : m_agcTime(agcTime), m_ant(ant), m_powerBw(bw) { }

		std::chrono::milliseconds m_agcTime;
		SEquipCtrlMsg::EAnt m_ant;
		Units::Frequency m_powerBw;
	};
	typedef std::unordered_map<const CNetConnection<void>*, SPanParam> PanParamMap;

	struct SVcpSpectrum	// used for spectrum averaging
	{
		SVcpSpectrum(void) : haveGetSpectrumData(false), haveRxTune(false) {}
		bool operator==(const SVCPMsg::SRxTune& s) const
		{
			return (haveRxTune &&
				rxTune.freq == s.freq && rxTune.fpi == s.fpi && rxTune.bw == s.bw && rxTune.rfInput == s.rfInput);
		}
		bool operator!=(const SVCPMsg::SRxTune& s) const
		{
			return !(*this == s);
		}
		bool operator==(const SVCPMsg::SGetSpectrumData& s) const
		{
			// Not quite a standard == operator!
			return (haveGetSpectrumData &&
				getSpectrumData.average == s.average && getSpectrumData.gainEqualization == s.gainEqualization);
		}
		bool operator!=(const SVCPMsg::SGetSpectrumData& s) const
		{
			return !(*this == s);
		}

		bool haveGetSpectrumData;
		bool haveRxTune;
		Ne10F32Vec spect;
		SVCPMsg::SGetSpectrumData getSpectrumData;
		SVCPMsg::SRxTune rxTune;
	};

	// Functions
	CEquipControl(void);
	virtual ~CEquipControl(void);
	SEquipCtrlMsg::EAnt CheckMultAntValue(SEquipCtrlMsg::EAnt ant, bool & hf) const;
	static float GetEstimatedLatency(Units::Frequency sampleRate, C3230::SDecimation decimations) { return CSingleton<CDigitizer>()->GetEstimatedLatency(sampleRate, decimations); }
	SEquipCtrlMsg::EMetricHw GetSystemType(void) const;
	void RemovePanEntry(_In_ const CNetConnection<void>* connection);
	void Resume(_Inout_ std::vector<SEquipCtrlMsg::STaskIdKey>& tasks);
	void SendFrequencyVsChannelResp(_In_opt_ CNetConnection<void>* source, _In_ SEquipCtrlMsg& resp, _In_
		const SEquipCtrlMsg::SGetOccupancyCmd& cmd, _In_ const CTask::TaskParams& taskParams) const;
	void SendRealtimeStart(_In_ const CTask::Task& task) const;
	SEquipCtrlMsg::EAnt ValidateMultAnt(SEquipCtrlMsg::EAnt ant, Units::Frequency freq) const;
	void ValidateVCPControlClient(const CEquipCtrlNet* source)
	{
		std::lock_guard<std::mutex> lock(m_vcpCritSect);
		if (m_vcpControlClient == nullptr) throw ErrorCodes::CMDINVALIDSTDMODE;	// No contol client.
		if (m_vcpControlClient != source) throw ErrorCodes::CMDINVALIDVCPMODE;	// This client is not the controlling one.
	}

	// Data
	bool m_dfNodesReady;
	CSingleton<const CNavigation> m_navigation;		// Keep navigation before digitizer
	CSingleton<CConfig> m_config;
	CSingleton<CDigitizer> m_digitizer;
	CSingleton<CCsmsLicense> m_license;

	mutable std::mutex m_vcpCritSect;
	CEquipCtrlNet* m_vcpControlClient;

	CSingleton<CIonogram> m_ionogram;
	const CConfig::SMutableConfig& m_mutableConfig; // Must be after m_config
	mutable std::mutex m_panParamMutex;
	PanParamMap m_panParamMap;
	CSingleton<CProcessorNode> m_processorNode;
	CSingleton<CRfSelSwitch> m_antSwitch;
	bool m_shutdown;
	bool m_pantoSlave;
	std::vector<SEquipCtrlMsg::STaskIdKey> m_suspendedStandardModeTasks;
};

