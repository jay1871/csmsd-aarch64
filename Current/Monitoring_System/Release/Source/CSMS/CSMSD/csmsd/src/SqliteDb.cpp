/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2015 TCI International, Inc. All rights reserved              *
**************************************************************************/

#include "stdafx.h"

#include "Log.h"
#include "SqliteDb.h"

//////////////////////////////////////////////////////////////////////
//
// Constructor
//
CSqliteDb::CSqliteDb(void) :
	m_db(nullptr),
	m_dbFile()
{
	// Check version of header vs library
	if (strcmp(sqlite3_sourceid(), SQLITE_SOURCE_ID) != 0)
	{
		CLog::Log(CLog::ERRORS, "SQLite header and source version mismatch\n%s\n%s\n", sqlite3_sourceid(), SQLITE_SOURCE_ID);
	    throw std::runtime_error("SQLite header and source version mismatch");
	}
	// TODO: Get the db filename from the config file.
	m_dbFile = std::string(TCIPaths::dataDir) + "csmsdb";	// "/media/tci/csms/data/csmsdb";

	// Get the db file name (append .db if not already there)
	auto pos = m_dbFile.find_last_of('.');
	if (pos == std::string::npos || m_dbFile.substr(pos) != ".db")
	{
		m_dbFile += ".db";
	}

#if 0
	std::string backupFile;

	// Create the name for the backup db  D:\data\csmsdb.db => D:\data\backup.db
	// /media/tci/csms/data/csmsdb.db
	pos = m_dbFile.find_last_of('/');
	if (pos == std::string::npos)
	{
		// csmsdb.db
		backupFile = "backup.db";
	}
	else
	{
		// /media/tci/csms/data/csmsdb.db
		backupFile = m_dbFile.substr(0, pos) + "/backup.db";
	}
	printf("dbFile: %s backupFile: %s\n", m_dbFile.c_str(), backupFile.c_str());
#endif
	// Open the database if it exists, else create it
    if (sqlite3_initialize() != SQLITE_OK)
    {
		CLog::Log(CLog::ERRORS, "Failed to initialize database subsystem");
		throw std::runtime_error("Failed to initialize database subsystem");
    }

    OpenRecover();

#if 0
    // Note: if directory does not exist, this will not create it!
    if (sqlite3_open_v2(m_dbFile.c_str(), &m_db,
		SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX, nullptr) != SQLITE_OK || m_db == nullptr)
	{
		sqlite3_shutdown();
    	throw std::runtime_error((std::string("Failed to open database:") + sqlite3_errmsg(m_db)).c_str());
	}

    std::vector<std::string> vStr;
    if (Execute("PRAGMA quick_check;", vStr))
    {
    	for (auto& s : vStr)
    	{
    		printf("quick_check = %s\n", s.c_str());
    	}
    }
    else
    {
    	printf("Failed to quick_check\n");
    }
#endif
    int maxPageCount;
    if (Execute("PRAGMA max_page_count=1048576;", maxPageCount))
    {
    	printf("maxPageCount = %d\n", maxPageCount);
    }
    else
    {
    	printf("Failed to set max_page_count\n");
    }

    char* errstr = nullptr;
    if (sqlite3_exec(m_db, "PRAGMA foreign_keys=ON;", nullptr, nullptr, &errstr) != SQLITE_OK)
    {
    	std::string error("Unable to enable foreign keys: ");
    	if (errstr)
		{
    		error += errstr;
			sqlite3_free(errstr);
			errstr = nullptr;
		}
		sqlite3_close(m_db);
    	sqlite3_shutdown();
		CLog::Log(CLog::ERRORS, "Sqlite3: %s", error.c_str());
    	throw std::runtime_error(error.c_str());
    }

    return;
}


//////////////////////////////////////////////////////////////////////
//
// Destructor
//
CSqliteDb::~CSqliteDb(void)
{
	sqlite3_close(m_db);
	sqlite3_shutdown();
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Check the error code and throw an exception if bad
//
void CSqliteDb::CheckHr(int hr) const
{
	if (hr != SQLITE_OK)
	{
		CLog::Log(CLog::ERRORS, "Database Error: %s %s", sqlite3_errstr(hr), sqlite3_errmsg(m_db));
		throw std::runtime_error((std::string("Database error: ") + sqlite3_errstr(hr) + + " " + sqlite3_errmsg(m_db)).c_str());
	}
	return;
}


//////////////////////////////////////////////////////////////////////
//
// Log a database error
//
void CSqliteDb::LogHr(int hr) const
{
	CLog::Log(CLog::ERRORS, "Database Error: %s %s", sqlite3_errstr(hr), sqlite3_errmsg(m_db));
	return;

}


//////////////////////////////////////////////////////////////////////
//
// Execute a single statement returning a single integer
//
bool CSqliteDb::Execute(const char* sql, int& value)
{
	bool rv = false;

	sqlite3_stmt* pStmt;
	auto hr = sqlite3_prepare_v2(m_db, sql, -1, &pStmt, nullptr);
	if (hr != SQLITE_OK || !pStmt)
		return rv;

	hr = sqlite3_step(pStmt);
	if (hr == SQLITE_ROW)
	{
		value = sqlite3_column_int(pStmt, 0);
		rv = true;
	}
	sqlite3_finalize(pStmt);
	return rv;
}


//////////////////////////////////////////////////////////////////////
//
// Execute a single statement returning a vector of strings
//
bool CSqliteDb::Execute(const char* sql, std::vector<std::string>& value)
{
	bool rv = false;

	sqlite3_stmt* pStmt;
	auto hr = sqlite3_prepare_v2(m_db, sql, -1, &pStmt, nullptr);
	if (hr != SQLITE_OK || !pStmt)
		return rv;

	for (size_t i = 0; i < 100; ++i)
	{
		hr = sqlite3_step(pStmt);
		if (hr != SQLITE_ROW)
			break;

		std::string s = reinterpret_cast<const char*>(sqlite3_column_text(pStmt, 0));
		value.push_back(s);
		rv = true;	// At least 1 row
	}
	sqlite3_finalize(pStmt);
	return rv;
}


//////////////////////////////////////////////////////////////////////
//
// Open database or recover from backup or create new one
//
void CSqliteDb::OpenRecover(void)
{
	// WHen this returns, the database will be open and a backup copy will be created.

	// Get paths for the backup and temp files
	std::string backupFile;
	std::string tempFile;
	std::string badFile;

	// Create the name for the backup db  D:\data\csmsdb.db => D:\data\backup.db
	// /media/tci/csms/data/csmsdb.db
	auto pos = m_dbFile.find_last_of('/');
	if (pos == std::string::npos)
	{
		// csmsdb.db
		backupFile = "backup.db";
		tempFile = "temp.db";
		badFile = "bad.db";
	}
	else
	{
		// /media/tci/csms/data/csmsdb.db
		backupFile = m_dbFile.substr(0, pos) + "/backup.db";
		tempFile = m_dbFile.substr(0, pos) + "/temp.db";
		badFile = m_dbFile.substr(0, pos) + "/bad.db";
	}
	printf("dbFile: %s backupFile: %s tempFile: %s badFile: %s\n", m_dbFile.c_str(), backupFile.c_str(), tempFile.c_str(), badFile.c_str());

	// Open existing db or create a new one
    // Note: if directory does not exist, this will not create it!
    if (sqlite3_open_v2(m_dbFile.c_str(), &m_db,
		SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX, nullptr) == SQLITE_OK && m_db != nullptr)
    {
    	auto isReadOnly = sqlite3_db_readonly(m_db, "main");
    	printf("sqlite readonly %s %d\n", "main", isReadOnly);
    	if (isReadOnly == 0)
    	{
			// Successfully opened or created database - run quick check
			std::vector<std::string> vStr;
			if (Execute("PRAGMA quick_check;", vStr))
			{
				if (vStr.size() == 1 && vStr[0] == "ok")
				{
					// Successfully opened and checked the db. Ready to wrap up.
					// Close the db and create the backup copy
					sqlite3_close(m_db);
					m_db = nullptr;

					// Make a temp copy of the db file
					remove(tempFile.c_str());
					if (CopyFile(m_dbFile.c_str(), tempFile.c_str()))
					{
						// Able to create tempfile
						// Delete the current backup file
						if (remove(backupFile.c_str()) != 0)
						{
							printf("Failed to remove backupFile\n");
						}

						// Rename the temp file to backup
						if (rename(tempFile.c_str(), backupFile.c_str()) != 0)
						{
							printf("Failed to rename tempFile to backupFile\n");
						}
					}

					// Re-open the db
					if (sqlite3_open_v2(m_dbFile.c_str(), &m_db,
						SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, nullptr) != SQLITE_OK || m_db == nullptr)
					{
						sqlite3_shutdown();
						CLog::Log(CLog::ERRORS, "Failed to open database: %s", sqlite3_errmsg(m_db));
						throw std::runtime_error((std::string("Failed to open database:") + sqlite3_errmsg(m_db)).c_str());
					}
					// Remove any temporary files
					remove(badFile.c_str());
					remove(tempFile.c_str());
					return;
				}
				else
				{
					for (auto& s : vStr)
					{
						printf("quick_check = %s\n", s.c_str());
					}
				}
			}
			else
			{
				printf("Failed to quick_check\n");
			}
			// Database was opened but quick check failed - Close the db
			sqlite3_close(m_db);
			m_db = nullptr;
		}
    	else
    	{
    		// database opened but is readonly - Close it and quit
    		sqlite3_close(m_db);
    		m_db = nullptr;
			sqlite3_shutdown();
			CLog::Log(CLog::ERRORS, "Database was not opened in readwrite mode");
			throw std::runtime_error("Database was not opened in readwrite mode");
    	}
    }
    // db is bad/corrupt. Rename it to "bad".
	if (remove(badFile.c_str()) != 0)
	{
		printf("Failed to remove badFile\n");
	}
	if (rename(m_dbFile.c_str(), badFile.c_str()) != 0)
	{
		printf("Failed to rename dbFile to badFile\n");
	}
	// Try to copy the backupdb to the current db
	if (!CopyFile(backupFile.c_str(), m_dbFile.c_str()))
	{
		printf("Failed to copy backupFile to dbFile\n");
	}
	// Re-open the db
	if (sqlite3_open_v2(m_dbFile.c_str(), &m_db,
		SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, nullptr) != SQLITE_OK || m_db == nullptr)
	{
		sqlite3_shutdown();
		CLog::Log(CLog::ERRORS, "Failed to open database: %s", sqlite3_errmsg(m_db));
    	throw std::runtime_error((std::string("Failed to open database:") + sqlite3_errmsg(m_db)).c_str());
	}

	// Remove any unwanted temporary files
	remove(tempFile.c_str());
    return;
}


//////////////////////////////////////////////////////////////////////
//
// Copy a binary file
//
bool CSqliteDb::CopyFile(const char* inFile, const char* outFile)
{
	auto infd = open(inFile, O_RDONLY);
	if (infd < 0)
	{
		printf("CopyFile: unable to open %s\n", inFile);
		return false;
	}
	auto outfd = open(outFile, O_CREAT | O_WRONLY | O_TRUNC);
	if (outfd < 0)
	{
		printf("Unable to open %s for copy\n", outFile);
		close(infd);
		return false;
	}
	// Both files open, do the copy
	char buf[4096];
	int bytes;
	while ((bytes = read(infd, buf, sizeof(buf))) > 0)
	{
		if (write(outfd, buf, bytes) != bytes)
		{
			printf("error writing to %s\n", outFile);
			close(outfd);
			close(infd);
			return false;
		}
	}
	close(outfd);
	close(infd);

	return true;
}

