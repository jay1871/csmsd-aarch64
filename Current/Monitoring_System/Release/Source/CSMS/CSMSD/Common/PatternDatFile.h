/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2017 TCI International, Inc. All rights reserved              *
**************************************************************************/
#ifdef	__GNUG__
#include "Ne10Vec.h"
#else
#include <vector>
#include <map>
#endif
#include "Units.h"

namespace CPatternDatFile
{
#ifdef	__GNUG__
#else
	struct cpx { float r; float i; };
	struct Ne10F32cVec
	{ 
		cpx& operator[](size_t i) { return vec[i]; }
		void resize(size_t size) { vec.resize(size); return; }
		size_t size(void) const { return vec.size(); }
	private:
		std::vector<cpx> vec;
	};

#endif
	typedef std::vector<Ne10F32cVec> WfaConjPattern; // Complex conjugate of pattern voltages[az][ant]
	typedef std::map<Units::Frequency, WfaConjPattern> WfaConjPatterns; // Lookup by frequency

	void Load(const char* patternFile, int numAnts, WfaConjPatterns& m_wfaConjPatterns);
};
