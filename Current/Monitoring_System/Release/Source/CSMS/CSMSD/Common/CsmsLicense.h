/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

#include <vector>
#include <map>
#include <fstream>

template<typename T> class CSingleton;

class CCsmsLicense
{
	// Friends
	friend class CSingleton<CCsmsLicense>;
	friend class CCsmsLicenseSign;
	friend class CLicenseManagerDlg;

public:
	// Types
	struct SMachineId
	{
		unsigned long dna[2];
		unsigned long max10Id[2];
	};

	// Functions
	bool Check(void) const;
	bool CheckMachineId(const SMachineId& machineId);
	bool GetStatus(std::vector<std::string>& status) const { status = m_status; return m_valid; }
	bool Has80MHzRadio(void) const {return(m_has80MHz);}
	bool HasHF(void) const {return(m_hasHf);}
	bool HasPrecisionTimestamp(void) const {return(m_hasPrecision);}
	bool HasShfExt(void) const {return(m_hasShfExt);}
	unsigned long HasVUHF(void) const {return(m_vushfMHz);}	// MHz
	void Init(void);

private:
#ifdef __GNUG__
	static constexpr const char* MEMERR = "memory allocation error %d";
#else
	static const char* MEMERR;
#endif
	CCsmsLicense(void);
	~CCsmsLicense(void);
	static void HashToString(const std::string& secData, std::string& hashString);
	bool Read80MHzLicense(void) const;
	bool ReadHFLicense(void) const;
	bool ReadPrecisionLicense(void) const;
	bool ReadShfExtLicense(void) const;
	unsigned long ReadVUHFLicense(void) const; // MHz
	bool SecurityCheck(std::ifstream& file);

	bool m_appSection;
	bool m_has80MHz;
	bool m_hasHf;
	bool m_hasPrecision;
	bool m_hasShfExt;
	bool m_initialized;
	std::map<std::string, std::string> m_keyVals;
	std::string m_machineId;
	std::vector<std::string> m_status;
	bool m_valid;
	unsigned long m_vushfMHz;
};

