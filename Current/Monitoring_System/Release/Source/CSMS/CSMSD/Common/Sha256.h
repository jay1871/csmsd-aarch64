/**************************************************************************
*                           CONFIDENTIAL                                  *
* Unauthorized access to, copying, use of or disclosure of this software, *
* or any of its features, is strictly prohibited. Your access to or       *
* possession of a copy of this software is persuant to a limited license; *
* ownership of the software and any associated media remains with TCI.    *
*                                                                         *
* Copyright 2016 TCI International, Inc. All rights reserved              *
**************************************************************************/

#pragma once

namespace Sha256
{
	/*********************************************************************
	* Filename:   sha256.h
	* Author:     Brad Conte (brad AT bradconte.com)
	* Copyright:
	* Disclaimer: This code is presented "as is" without any guarantees.
	* Details:    Defines the API for the corresponding SHA1 implementation.
	*********************************************************************/

	/****************************** MACROS ******************************/
	#define SHA256_BLOCK_SIZE 32            // SHA256 outputs a 32 byte digest

	/**************************** DATA TYPES ****************************/
	typedef unsigned char BYTE;             // 8-bit byte
	typedef unsigned int  WORD;             // 32-bit word, change to "long" for 16-bit machines

	typedef struct {
		BYTE data[64];
		WORD datalen;
		unsigned long long bitlen;
		WORD state[8];
	} SHA256_CTX;

	/*********************** FUNCTION DECLARATIONS **********************/
	void sha256_init(SHA256_CTX *ctx);
	void sha256_update(SHA256_CTX *ctx, const BYTE data[], size_t len);
	void sha256_final(SHA256_CTX *ctx, BYTE hash[]);
}
